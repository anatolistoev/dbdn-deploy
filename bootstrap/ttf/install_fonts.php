<?php

// Copy load_font.php in dompdf
$src = __DIR__ . '/load_font.php';
$dest = __DIR__ . '/../../vendor/dompdf/dompdf/load_font.php';
if (!copy($src, $dest)) {
    throw new Exception("Unable to copy '$src' to '$dest'");
}
$dimler_font_dir = __DIR__ . '/../../resources/fonts/';

$output = shell_exec("php $dest " . 'smart_special ' . $dimler_font_dir . 'smart/FORsmartSpecial.ttf');
echo "<pre>$output</pre>";

$output = shell_exec("php $dest " . 'smart_bold ' . $dimler_font_dir . 'smart/FORsmartBold.ttf');
echo "<pre>$output</pre>";

$output = shell_exec("php $dest " . 'smart_light ' . $dimler_font_dir . 'smart/FORsmartLight.ttf');
echo "<pre>$output</pre>";

$output = shell_exec("php $dest " . 'daimlerca-regular ' . $dimler_font_dir . 'daimler/DaimlerCA-Regular.ttf');
echo "<pre>$output</pre>";

$output = shell_exec("php $dest " . 'daimlercs-bold ' . $dimler_font_dir . 'daimler/DaimlerCS-Bold.ttf');
echo "<pre>$output</pre>";

$output = shell_exec("php $dest " . 'daimlercs-regular ' . $dimler_font_dir . 'daimler/DaimlerCS-Regular.ttf');
echo "<pre>$output</pre>";

$output = shell_exec("php $dest " . 'daimlercs-demi ' . $dimler_font_dir . 'daimler/DaimlerCS-Demi.ttf');
echo "<pre>$output</pre>";

$output = shell_exec("php $dest " . 'arial ' .  __DIR__ . '/arial.ttf');
echo "<pre>$output</pre>";

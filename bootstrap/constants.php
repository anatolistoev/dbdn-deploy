<?php

/*
  |--------------------------------------------------------------------------
  | GLOBAL
  |--------------------------------------------------------------------------
  |
 */
const DATE_TIME_ZERO = '0000-00-00 00:00:00';
const MAX_SERVER_LOAD = 3;


/*
  |--------------------------------------------------------------------------
  | Language ids
  |--------------------------------------------------------------------------
  |
 */
const LANG_EN       = 1; // English
const LANG_DE       = 2; // Deutch
/*
  |--------------------------------------------------------------------------
  | User roles ids
  |--------------------------------------------------------------------------
  |
 */
const USER_ROLE_MEMBER          = 0; // Member of site for FE access
const USER_ROLE_EDITOR          = 1; // Content Approver
const USER_ROLE_ADMIN           = 2; // Admin of site
const USER_ROLE_EXPORTER        = 3; // Export account for emails
const USER_ROLE_EDITOR_SMART    = 4;
const USER_ROLE_EDITOR_MB       = 5;
const USER_ROLE_EDITOR_DFS      = 6;
const USER_ROLE_EDITOR_DFM      = 7;
const USER_ROLE_EDITOR_DTF      = 8;
const USER_ROLE_EDITOR_FF       = 9;
const USER_ROLE_EDITOR_DP       = 10;
const USER_ROLE_EDITOR_TSS      = 11;
const USER_ROLE_EDITOR_BKK      = 12;
const USER_ROLE_NEWSLETTER_APPROVER = 13;
const USER_ROLE_NEWSLETTER_EDITOR = 14;
// New Brands and Roles for 2018
const USER_ROLE_EDITOR_DB       = 15;
const USER_ROLE_EDITOR_EB       = 16;
const USER_ROLE_EDITOR_SETRA    = 17;
const USER_ROLE_EDITOR_OP       = 18;
const USER_ROLE_EDITOR_BS       = 19;
const USER_ROLE_EDITOR_FUSO     = 20;
const USER_ROLE_EDITOR_DT 	    = 21;
const USER_ROLE_EDITOR_DMO      = 22;

/*
  |--------------------------------------------------------------------------
  | FileItem->getPath() constant call argument for physical file path
  |--------------------------------------------------------------------------
  |
 */
const FILEITEM_URL_PATH         = 0;
const FILEITEM_REAL_PATH        = 1;

/*
  |--------------------------------------------------------------------------
  | Page Group Access for Member
  |--------------------------------------------------------------------------
  |
 */
const ACCESS_MEMBER             = 1;

/*
  |--------------------------------------------------------------------------
  | Special pages for brands and company
  |--------------------------------------------------------------------------
  |
 */
const BRAND                     = 'brand';
const COMPANY                   = 'company';

const HOME                      = 1;

const DAIMLER                   = 979;
const MERCEDES_BENZ             = 1043;
const SMART                     = 1904;
const TRUCK_FINANCIAL           = 1138;
const FLEET_MANAGEMENT          = 1139;
const FUSO_FINANCIAL            = 1140;
const DAIMLER_MOBILITY          = 4691;
const FINANCIAL_SERVICES        = 1432;
const DAIMLER_PROTICS           = 1785;
const DAIMLER_TSS               = 2188;
const DAIMLER_BKK               = 2488;
// New Brands for 2018
const DAIMLER_BUSES             = 3605;
const EVOBUS                    = 3606;
const SETRA                     = 3607;
const OMNIPLUS                  = 3608;
const BUSSTORE                  = 3609;
const FUSO                      = 3610;
const DAIMLER_TRUCKS            = 3884;

/*
  |--------------------------------------------------------------------------
  | Action Access
  |--------------------------------------------------------------------------
  |
 */
const PERMANENT_DELETE          = 1;



const RECYCLE_BIN_EXCEED_EXCEPTION  = 1000;

const FILE_DEFAULT_ACCESS = 0755;
const EDIT_TIME_BUFFER_INTERVAL = "-30 seconds";

/*
  |--------------------------------------------------------------------------
  | Mininum matching for related
  |--------------------------------------------------------------------------
  |
 */

const MIN_MATCH = 3;

/*
  |--------------------------------------------------------------------------
  | Blocks to be shown in downloads overview section
  |--------------------------------------------------------------------------
  |
 */

const DOWNLOAD_BLOCKS_IN_SECTION = 12;

const BP_BLOCKS_PER_LOAD = 6;

const STATE_DRAFT = 'Draft';
const STATE_REVIEW = 'Review';
const STATE_APPROVED = 'Approved';
const SEARCH_RESULT_BY_PAGE = 10;
const STATE_SENDING = 'Sending';
const STATE_SENT = 'Sent';

/*
  |--------------------------------------------------------------------------
  | Tablet image max sizes
  |--------------------------------------------------------------------------
  |
 */

const TABLET_MAX_WIDTH = 538;
const TABLET_MAX_HEIGHT = 358;


/*
  |--------------------------------------------------------------------------
  | Page rendering mode
  |--------------------------------------------------------------------------
  |
 */
const PAGE_MODE_BROWSE = 'browse';
const PAGE_MODE_PDF    = 'pdf';
const PAGE_MODE_PRINT  = 'print';

/*
  |--------------------------------------------------------------------------
  | Max len of content
  |--------------------------------------------------------------------------
  |
 */
const UPDATE_ALERT_MAX_LEN = 295;

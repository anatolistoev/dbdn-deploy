<?php

use Illuminate\Support\Facades\Log;

if (!function_exists('elixir')) {
    /**
     * Get the path to a versioned Elixir file.
     *
     * @param  string $file
     * @param  string $buildDirectory
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    function elixir($file, $buildDirectory = 'build')
    {
        static $manifest = [];
        static $manifestPath;

        if (empty($manifest) || $manifestPath !== $buildDirectory) {
            $path = public_path($buildDirectory . '/rev-manifest.json');

            if (file_exists($path)) {
                $manifest = json_decode(file_get_contents($path), true);
                $manifestPath = $buildDirectory;
            }
        }

        $file = ltrim($file, '/');

        if (isset($manifest[$file])) {
            if (!preg_match('%-([a-z0-9]+)\.[a-z0-9]+$%i', $manifest[$file], $matches)) {
                throw new \Exception("The file {$manifest[$file]} defined in asset manifest has wrong format.");
            }

            //Is added asset beacuse of subfolder in domain
            return asset('/' . trim($file . '?' . $matches[1], '/'));
        }

        Log::critical('Missing versioned resource file: ' . $file);

        if (config('app.debug') == false) {
            $unversioned = public_path($file);

            if (file_exists($unversioned)) {
                //Is added asset beacuse of subfolder in domain
                return asset('/' . trim($file, '/'));
            }
        }
        throw new InvalidArgumentException("File {$file} not defined in asset manifest.");
    }
}

if (!function_exists('mb_ucfirst')) {
    function mb_ucfirst($string)
    {
        $firstChar = mb_strtoupper(mb_substr($string, 0, 1));

        return $firstChar . mb_substr($string, 1);
    }
}

if (!function_exists('imageantialias')) {
    function imageantialias($image, $enabled)
    {
        Log::error("Missing function imageantialias(\$image, \$enabled)");

        return false;
    }
}

function getSql($builder)
{
    if ($builder instanceof Illuminate\Database\Eloquent\Builder) {
        $builder = $builder->getQuery();
    }
    $sql = $builder->toSql();
    foreach ($builder->getBindings() as $binding) {
        $value = is_numeric($binding) ? $binding : "'" . $binding . "'";
        $sql = preg_replace('/\?/', $value, $sql, 1);
    }

    return $sql;
}

/**
 * Convert locale to lang ID.
 *
 * @param  string $locale
 * @return number
 *
 */
function getLangId($locale)
{
    $locales = config('app.locales');
    $key = array_search(strtolower($locale), $locales);
    if ($key !== false && $key !== null) {
        $langId = $key + 1;
    }

    return $langId;
}



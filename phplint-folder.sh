#!/usr/bin/env bash

# found on http://kamisama.me/2012/07/02/faster-php-lint/
PHP=/C/xampp/php/php.exe
declare -a arr_of_folders=(app bootstrap config database resources routes)
# Loop over names
for arr_name in "${arr_of_folders[@]}"
do
    find "${arr_name}/" -name "*.php" -print0 | xargs -0 -n1 -P8 $PHP -l
done

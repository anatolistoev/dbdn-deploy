/** 
 * Glossary Web Service
 */

QUnit.module("Test REST Glossary Web Services", {
	setup: function() {
		// prepare something for all following tests
		jQuery.ajaxSetup({timeout: 5000});

		jQuery(document).ajaxStart(function() {
			ok(true, "ajaxStart");
		}).ajaxStop(function() {
			ok(true, "ajaxStop");
			QUnit.start();
		}).ajaxSend(function() {
			ok(true, "ajaxSend");
		}).ajaxComplete(function(event, request, settings) {
			ok(true, "ajaxComplete: " + request.responseText);
		}).ajaxError(function() {
			ok(false, "ajaxError");
		}).ajaxSuccess(function() {
			ok(true, "ajaxSuccess");
		});
	},
	teardown: function() {
		// clean up after each test
		jQuery(document).unbind('ajaxStart');
		jQuery(document).unbind('ajaxStop');
		jQuery(document).unbind('ajaxSend');
		jQuery(document).unbind('ajaxComplete');
		jQuery(document).unbind('ajaxError');
		jQuery(document).unbind('ajaxSuccess');
	}
});

QUnit.test("DBDN REST create glossary - success", 6, function(assert) {
	QUnit.stop();
	
	loginUser();
	
	// insert test data
	var page = {parent_id:2,slug:"Test_Update_Page",type:"File",active:1,position:1,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0};
	var page = insertObject(page, SERVER_INDEX_URL + "pages/create");

	// insert test data
	var term = {name:"Test Term 1",lang:1,description:"Term description 1"};
	term = insertObject(term, SERVER_INDEX_URL + "term/create");
	
	var glossary = {page_id : page.id,term_id : term.id, position : 1};
	
	var expected_result = jQuery.extend({"error":false, "message": "Glossary was added.", "response": null}, {"response": glossary});	

	// url string    
	var urlREST = SERVER_INDEX_URL + "glossary/create";

	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(glossary),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Download was added");
			} //else
		}, // success
		complete: function(data){
			deleteGlossary(page.id, term.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page.id);
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/"+term.id);
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Create Glossary - term_id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	//insertUser();
	
	var glossary_item = {page_id : 1}

	// expected data
	var expected_result = {"error": true, "message": "Parameter term_id is required!"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "glossary/create";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(glossary_item),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Glossary create required");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			  
			expected_result.response.created_at = data.response.created_at;
			expected_result.response.updated_at = data.response.updated_at;
			assert.deepEqual(data, expected_result, "Glossary create required result");
          } // if
          else { // login was successful
        	assert.ok(false, "Glossary create required term id check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Glossary delete", 6, function( assert ) {
	// GET /pages/descedents
	QUnit.stop();
	var file1, file2, file3;
	var gl1, gl2, gl3;
	loginUser();
	var page = {parent_id:2,slug:"Test_Update_Page",type:"File",active:1,position:1,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0};
	var page = insertObject(page, SERVER_INDEX_URL + "pages/create");
	
	// insert test data
	var term1 = {name:"Test Term 1",lang:1,description:"Term description 1"};
	term1 = insertObject(term1, SERVER_INDEX_URL + "term/create");
		
	gl1 = addGlossary(page.id, term1.id);
	
	var term2 = {name:"Test Term 2",lang:1,description:"Term description 1"};
	term2 = insertObject(term2, SERVER_INDEX_URL + "term/create");
		
	gl2 = addGlossary(page.id, term2.id);
	
	var term3 = {name:"Test Term 3",lang:1,description:"Term description 1"};
	term3 = insertObject(term3, SERVER_INDEX_URL + "term/create");
		
	gl3 = addGlossary(page.id, term3.id);
	
	var urlREST =  SERVER_INDEX_URL + "glossary/remove";
				
	gl3.position = gl2.position;
	
	//var expected_data = [dw2];
	var expected_result = jQuery.extend({"error": false, "message": 'Glossary was deleted!', "response": null}, {"response": gl2});  

	jQuery.ajax({
		type: "DELETE",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data : JSON.stringify(gl2),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data){
			if (data.error) { // script returned error
				assert.ok(true, "error");
			} // if
			else { // login was successful
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
							
				assert.deepEqual(data, expected_result, "Download modfy");
			} //else
		}, // success
		complete: function(data){
			deleteGlossary(page.id, term1.id);
			deleteGlossary(page.id, term3.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page.id);
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/"+term1.id);
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/"+term2.id);
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/"+term3.id);
			logoutUser();
		} // always
	});	
});	


QUnit.test("DBDN REST Modify glossary ", 6, function( assert ) {
	// GET /pages/descedents
	QUnit.stop();
	var gl1, gl2, gl3;
	loginUser();
	var page = {parent_id:2,slug:"Test_Update_Page",type:"File",active:1,position:1,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0};
	var page = insertObject(page, SERVER_INDEX_URL + "pages/create");
	
	// insert test data
	var term1 = {name:"Test Term 1",lang:1,description:"Term description 1"};
	term1 = insertObject(term1, SERVER_INDEX_URL + "term/create");
		
	gl1 = addGlossary(page.id, term1.id);
	
	var term2 = {name:"Test Term 2",lang:1,description:"Term description 1"};
	term2 = insertObject(term2, SERVER_INDEX_URL + "term/create");
		
	gl2 = addGlossary(page.id, term2.id);
	
	var term3 = {name:"Test Term 3",lang:1,description:"Term description 1"};
	term3 = insertObject(term3, SERVER_INDEX_URL + "term/create");
		
	gl3 = addGlossary(page.id, term3.id);
	
	var urlREST =  SERVER_INDEX_URL + "glossary/modify";
				
	gl3.position = gl1.position;

	var expected_result = jQuery.extend({"error": false, "message": 'Glossary was updated.', "response": null}, {"response": gl3});  

	jQuery.ajax({
		type: "PUT",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data : JSON.stringify(gl3),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data){
			if (data.error) { // script returned error
				assert.ok(true, "error");
			} // if
			else { // login was successful
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
							
				assert.deepEqual(data, expected_result, "Glossary modify max");
			} //else
		}, // success
		complete: function(data){
			deleteGlossary(page.id, term1.id);
			deleteGlossary(page.id, term2.id);
			deleteGlossary(page.id, term3.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page.id);
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/"+term1.id);
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/"+term2.id);
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/"+term3.id);
			logoutUser();
		} // always
	});	
});	



QUnit.test("DBDN REST Modify glossary above max", 6, function( assert ) {
	// GET /pages/descedents
	QUnit.stop();
	var gl1, gl2, gl3;
	loginUser();
	var page = {parent_id:2,slug:"Test_Update_Page",type:"File",active:1,position:1,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0};
	var page = insertObject(page, SERVER_INDEX_URL + "pages/create");
	
	// insert test data
	var term1 = {name:"Test Term 1",lang:1,description:"Term description 1"};
	term1 = insertObject(term1, SERVER_INDEX_URL + "term/create");
		
	gl1 = addGlossary(page.id, term1.id);
	
	var term2 = {name:"Test Term 2",lang:1,description:"Term description 1"};
	term2 = insertObject(term2, SERVER_INDEX_URL + "term/create");
		
	gl2 = addGlossary(page.id, term2.id);
	
	var term3 = {name:"Test Term 3",lang:1,description:"Term description 1"};
	term3 = insertObject(term3, SERVER_INDEX_URL + "term/create");
		
	gl3 = addGlossary(page.id, term3.id);
	
	var urlREST =  SERVER_INDEX_URL + "glossary/modify";
				
	gl1.position = 6;
	var expected_result = jQuery.extend({"error": false, "message": 'Glossary was updated.', "response": null}, {"response": gl1});  

	jQuery.ajax({
		type: "PUT",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data : JSON.stringify(gl1),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data){
			if (data.error) { // script returned error
				assert.ok(true, "error");
			} // if
			else { // login was successful.
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				expected_result.response.position = 3;
				assert.deepEqual(data, expected_result, "Download delete");
			} //else
		}, // success
		complete: function(data){
			deleteGlossary(page.id, term1.id);
			deleteGlossary(page.id, term2.id);
			deleteGlossary(page.id, term3.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page.id);
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/"+term1.id);
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/"+term2.id);;
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/"+term3.id);
			logoutUser();
		} // always
	});	
});	

QUnit.test("DBDN REST Get Terms", 6, function( assert ) {
	// GET /pages/descedents
	QUnit.stop();
	var gl1, gl2, gl3;
	loginUser();
	var page = {parent_id:2,slug:"Test_Update_Page",type:"File",active:1,position:1,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0};
	var page = insertObject(page, SERVER_INDEX_URL + "pages/create");
	
	// insert test data
	var term1 = {name:"Test Term 1",lang:1,description:"Term description 1"};
	term1 = insertObject(term1, SERVER_INDEX_URL + "term/create");
		
	gl1 = addGlossary(page.id, term1.id);
	
	var term2 = {name:"Test Term 2",lang:1,description:"Term description 1"};
	term2 = insertObject(term2, SERVER_INDEX_URL + "term/create");
		
	gl2 = addGlossary(page.id, term2.id);
	
	var term3 = {name:"Test Term 3",lang:1,description:"Term description 1"};
	term3 = insertObject(term3, SERVER_INDEX_URL + "term/create");
		
	gl3 = addGlossary(page.id, term3.id);
	
	var urlREST =  SERVER_INDEX_URL + "glossary/glossary/"+page.id+"?lang=1";
	
	var expected_data = [term1, term2, term3];
	var expected_result = jQuery.extend({"error": false, "message": 'List of terms found.', "response": null}, {"response": expected_data});  

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data){
			if (data.error) { // script returned error
				assert.ok(true, "error");
			} // if
			else { // login was successful							
				assert.deepEqual(data, expected_result, "Get downloads");
			} //else
		}, // success
		complete: function(data){
			deleteGlossary(page.id, term1.id);
			deleteGlossary(page.id, term2.id);
			deleteGlossary(page.id, term3.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page.id);
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/"+term1.id);
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/"+term2.id);
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/"+term3.id);
			logoutUser();
		} // always
	});	
});

QUnit.test("DBDN REST Get Glossary - page_id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Parameter page_id is required!"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "glossary/glossary?lang=1";
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Glossary get required");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			  
			expected_result.response.created_at = data.response.created_at;
			expected_result.response.updated_at = data.response.updated_at;
			assert.deepEqual(data, expected_result, "Glossary get required result");
          } // if
          else { // login was successful
        	assert.ok(false, "Glossary get required term id check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Get Glossary - lang required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Parameter lang is required!"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "glossary/glossary/4";
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Glossary get required");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			  
			expected_result.response.created_at = data.response.created_at;
			expected_result.response.updated_at = data.response.updated_at;
			assert.deepEqual(data, expected_result, "Glossary get required result");
          } // if
          else { // login was successful
        	assert.ok(false, "Glossary get required term id check fail!");
          } //else
        }, // success
		complete : function (){
			logoutUser();
		}
	});
});
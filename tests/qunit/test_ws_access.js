/**
 * Content Web Service
 */

QUnit.module("Test REST Access Web Services", {
	setup: function() {
		// prepare something for all following tests
		jQuery.ajaxSetup({timeout: 5000});

		jQuery(document).ajaxStart(function() {
			ok(true, "ajaxStart");
		}).ajaxStop(function() {
			ok(true, "ajaxStop");
			QUnit.start();
		}).ajaxSend(function() {
			ok(true, "ajaxSend");
		}).ajaxComplete(function(event, request, settings) {
			ok(true, "ajaxComplete: " + request.responseText);
		}).ajaxError(function() {
			ok(false, "ajaxError");
		}).ajaxSuccess(function() {
			ok(true, "ajaxSuccess");
		});
	},
	teardown: function() {
		// clean up after each test
		jQuery(document).unbind('ajaxStart');
		jQuery(document).unbind('ajaxStop');
		jQuery(document).unbind('ajaxSend');
		jQuery(document).unbind('ajaxComplete');
		jQuery(document).unbind('ajaxError');
		jQuery(document).unbind('ajaxSuccess');
	}
});

QUnit.test("DBDN REST Grant Access - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
    var group = {name:"Admins",active:1,visible:1};
    var group = insertObject(group, SERVER_INDEX_URL + "group/create");
    var page = createAndActivatePage();

	var access = {group_id : group.id,page_id : page.id};

	// expected data
	var expected_result = jQuery.extend({"error":false, "message": "Access was granted.", "response": null}, {"response": access});

	// url string
	var urlREST = SERVER_INDEX_URL + "access/grant";

	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(access),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Favorite was added.");
			} //else
		}, // success
		complete: function(data){
			deleteAccess(page.u_id);
			deleteObject(SERVER_INDEX_URL + "group/forcedelete/"+group.id);
			if(page)
				forceDeletePage(page.u_id);
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Grant access - group_id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var access = {};

	// expected data
	var expected_result = {"error": true, "message": "Parameter group_id is required!"};

    // url string
	var urlREST =  SERVER_INDEX_URL + "access/grant";

    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(access),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Access grant required");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Access grant result");
          } // if
          else { // login was successful
        	assert.ok(false, "Access add group id check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});


QUnit.test("DBDN REST Revoke access - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
	var group = {name:"Admins",active:1,visible:1};
    var group = insertObject(group, SERVER_INDEX_URL + "group/create");
    var page = createAndActivatePage();

	var access = addAccess(group.id,page.id);

	var expected_data = access;
	var expected_result = jQuery.extend({"error":false, "message": "Access was revoked!", "response": null}, {"response": expected_data});
	// url string
	var urlREST = SERVER_INDEX_URL + "access/revoke";

		jQuery.ajax({
			type: "DELETE",
			url: urlREST, // URL of the REST web service
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(access),
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			}, // error
			// script call was successful
			// data contains the JSON values returned by the Perl script
			success: function(data) {
				if (data.error) { // script returned error
					assert.ok(false, "error");
				} // if
				else { // login was successful
					expected_result.response.created_at = data.response.created_at;
					expected_result.response.updated_at = data.response.updated_at;
					assert.deepEqual(data, expected_result, "Access revoke");
				} //else
			}, // success
			complete: function(data){
				deleteObject(SERVER_INDEX_URL + "group/forcedelete/"+group.id);
                deleteAccess(page.u_id);
                if(page)
                    forceDeletePage(page.u_id);
				logoutUser();
			} // always
		  });
});

QUnit.test("DBDN REST Revoke access - group_id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var access = {}

	// expected data
	var expected_result = {"error": true, "message": "Parameter group_id is required!"};

    // url string
	var urlREST =  SERVER_INDEX_URL + "access/revoke";

    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(access),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Access revoke required");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Access revoke result");
          } // if
          else { // login was successful
        	assert.ok(false, "Access revoke group id check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Revoke access - not found", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var access = {group_id : 1, page_id : 1}

	// expected data
	var expected_result = {"error": true, "message": "Access not found."};

    // url string
	var urlREST =  SERVER_INDEX_URL + "access/revoke";

    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(access),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Access revoke not found");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Access revoke not found result");
          } // if
          else { // login was successful
        	assert.ok(false, "Access revoke not found  check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});
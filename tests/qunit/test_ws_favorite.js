/** 
 * Content Web Service
 */

QUnit.module("Test REST Favorite Web Services", {
	setup: function() {
		// prepare something for all following tests
		jQuery.ajaxSetup({timeout: 5000});

		jQuery(document).ajaxStart(function() {
			ok(true, "ajaxStart");
		}).ajaxStop(function() {
			ok(true, "ajaxStop");
			QUnit.start();
		}).ajaxSend(function() {
			ok(true, "ajaxSend");
		}).ajaxComplete(function(event, request, settings) {
			ok(true, "ajaxComplete: " + request.responseText);
		}).ajaxError(function() {
			ok(false, "ajaxError");
		}).ajaxSuccess(function() {
			ok(true, "ajaxSuccess");
		});
	},
	teardown: function() {
		// clean up after each test
		jQuery(document).unbind('ajaxStart');
		jQuery(document).unbind('ajaxStop');
		jQuery(document).unbind('ajaxSend');
		jQuery(document).unbind('ajaxComplete');
		jQuery(document).unbind('ajaxError');
		jQuery(document).unbind('ajaxSuccess');
	}
});

QUnit.test("DBDN REST Create user favorite - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
	var user = {username:"proba1",password:"P1!as123sword",password_confirmation:"P1!as123sword",first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",last_login:"2014-01-14 16:46:45", logins: 15,
		role:1,newsletter:2,force_pwd_change:2, group : []};
    var user = insertObject(user, SERVER_INDEX_URL + "user/create");
	
	var page = {parent_id:1,slug:"My page 4",type:"File",active:1,position:1,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0};
	
	console.log(user);
	
	var page = insertObject(page, SERVER_INDEX_URL + "pages/create");

	var favorite = {user_id : user.id, page_id : page.id};
	
	// expected data
	var expected_result = jQuery.extend({"error":false, "message": "Favorite was added.", "response": null}, {"response": favorite});

	// url string    
	var urlREST = SERVER_INDEX_URL + "favorite/createfavorite";

	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(favorite),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Favorite was added.");
			} //else
		}, // success
		complete: function(data){
			deleteFavorite(user.id, page.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/"+page.id);
			logoutUser();
		} // always
	}); 
});

QUnit.test("DBDN REST Create user favorite - file_id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	//insertUser();
	
	var favorite = {user_id : 1}

	// expected data
	var expected_result = {"error": true, "message": "Parameter page_id is required!"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "favorite/createfavorite";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(favorite),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Favorite add required");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Favorite update result");
          } // if
          else { // login was successful
        	assert.ok(false, "Favorite add file id check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Get user favorites - success", 6, function(assert) {
	QUnit.stop();
	
	loginUser();
	var user = {username:"1234",password:"P1!as123sword",password_confirmation:"P1!as123sword",first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",last_login:"2014-01-14 16:46:45", logins: 15,
		role:1,newsletter:2,force_pwd_change:2, group : []};
    var user = insertObject(user, SERVER_INDEX_URL + "user/create");
	
	var page1 = {parent_id:1,slug:"My page 1",type:"File",active:1,position:1,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var page1 = insertObject(page1, SERVER_INDEX_URL + "pages/create");
	
	var favorite1 = {user_id : user.id,page_id : page1.id}
	var user_id = favorite1.user_id;
	
	favorite1 = addFavorite(favorite1.user_id,favorite1.page_id);
	favorite1  = jQuery.extend(favorite1,page1);
	
	delete favorite1.user_id;
	delete favorite1.page_id;
	
	var page2 = {parent_id:1,slug:"My page 2",type:"File",active:1,position:2,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var page2 = insertObject(page2, SERVER_INDEX_URL + "pages/create");
	favorite1  = jQuery.extend(favorite1,{'updated_at':page2.created_at});
	var favorite2 = {user_id : user.id,page_id : page2.id}
	
	favorite2 = addFavorite(favorite2.user_id,favorite2.page_id);
	favorite2  = jQuery.extend(favorite2,page2);

	delete favorite2.user_id;
	delete favorite2.page_id;
		
	var expected_data = [favorite1, favorite2];
	var expected_result = jQuery.extend({"error":false, "message": "List of pages found.", "response": null}, {"response": expected_data});
	// url string    
	var urlREST = SERVER_INDEX_URL + "favorite/favorites/"+user_id;

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				
			for(var i=0;i < expected_result.response.length;i++){
				delete expected_result.response[i].access;
				delete expected_result.response[i].restricted;
			}
				assert.deepEqual(data, expected_result, "List of pages found.");
			} //else
		}, // success
		complete: function(data){
			deleteFavorite(user.id, page1.id);
			deleteFavorite(user.id, page2.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/"+page1.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/"+page2.id);
			logoutUser();
		} // always
	}); 
});

QUnit.test("DBDN REST Remove user Favorite - success", 6, function(assert) {
	QUnit.stop();
	
	loginUser();
	var user = {username:"1234",password:"P1!as123sword",password_confirmation:"P1!as123sword",first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",last_login:"2014-01-14 16:46:45", logins: 15,
		role:1,newsletter:2,force_pwd_change:2, group : []};
    var user = insertObject(user, SERVER_INDEX_URL + "user/create");

	var page = {parent_id:1,slug:"My page 1",type:"File",active:1,position:1,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:1,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var page = insertObject(page, SERVER_INDEX_URL + "pages/create");
	
	var favorite = {user_id : user.id,page_id : page.id}
	
	favorite = addFavorite(favorite.user_id,favorite.page_id);

	var expected_data = favorite;
	var expected_result = jQuery.extend({"error":false, "message": "Favorite was deleted!", "response": null}, {"response": expected_data});
	// url string    
	var urlREST = SERVER_INDEX_URL + "favorite/removefavorite";

		jQuery.ajax({
			type: "DELETE",
			url: urlREST, // URL of the REST web service
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(favorite),
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			}, // error 
			// script call was successful 
			// data contains the JSON values returned by the Perl script 
			success: function(data) {
				if (data.error) { // script returned error
					assert.ok(false, "error");
				} // if
				else { // login was successful
					expected_result.response.created_at = data.response.created_at;
					expected_result.response.updated_at = data.response.updated_at;
					for(var i=0;i < expected_result.response.length;i++){
						delete expected_result.response[i].access;
						delete expected_result.response[i].restricted;
					}
			
					assert.deepEqual(data, expected_result, "List of files found.");
				} //else
			}, // success
			complete: function(data){
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/"+page.id);
				logoutUser();
			} // always
		  }); 
});
/** 
 * Content Web Service
 */

QUnit.module("Test REST Subscription Web Services", {
	setup: function() {
		// prepare something for all following tests
		jQuery.ajaxSetup({timeout: 5000});

		jQuery(document).ajaxStart(function() {
			ok(true, "ajaxStart");
		}).ajaxStop(function() {
			ok(true, "ajaxStop");
			QUnit.start();
		}).ajaxSend(function() {
			ok(true, "ajaxSend");
		}).ajaxComplete(function(event, request, settings) {
			ok(true, "ajaxComplete: " + request.responseText);
		}).ajaxError(function() {
			ok(false, "ajaxError");
		}).ajaxSuccess(function() {
			ok(true, "ajaxSuccess");
		});
	},
	teardown: function() {
		// clean up after each test
		jQuery(document).unbind('ajaxStart');
		jQuery(document).unbind('ajaxStop');
		jQuery(document).unbind('ajaxSend');
		jQuery(document).unbind('ajaxComplete');
		jQuery(document).unbind('ajaxError');
		jQuery(document).unbind('ajaxSuccess');
	}
});

QUnit.test("DBDN REST Create subscription - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
	var user = {username:"sub_crt_12345",password:"P1!as123sword",password_confirmation :"P1!as123sword",first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",last_login:"2014-01-14 16:46:45", logins: 15,
		role:1,newsletter:2,force_pwd_change:2, group : []};
    var user = insertObject(user, SERVER_INDEX_URL + "user/create");
    if (user.id == undefined) 
        assert.ok(false, "Cannot create user: " + JSON.stringify(user));

	var page = {parent_id:1,slug:"My_page_4",type:"File",active:1,position:1,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0};
	
	var page = insertObject(page, SERVER_INDEX_URL + "pages/create");
    if (page.id == undefined) 
        assert.ok(false, "Cannot create page: " + JSON.stringify(page));

	var subscription = {user_id : user.id, page_id : page.id};
	
	// expected data
	var expected_result = jQuery.extend({"error":false, "message": "Subscription was added.", "response": null}, {"response": subscription});

	// url string    
	var urlREST = SERVER_INDEX_URL + "subscription/create";

	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(subscription),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Subscription was added.");
			} //else
		}, // success
		complete: function(data){
			deleteSubscription(user.id, page.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/"+page.id);
			logoutUser();
		} // always
	}); 
});

QUnit.test("DBDN REST Create subscription - user_id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var subscription = {}

	// expected data
	var expected_result = {"error": true, "message": "Parameter user_id is required!"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "subscription/create";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(subscription),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Subscription add required");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Subscription add result");
          } // if
          else { // login was successful
        	assert.ok(false, "Subscription add user id check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});


QUnit.test("DBDN REST Remove subscription - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
	var user = {username:"sub_rmv_12345",password:"P1!as123sword",password_confirmation :"P1!as123sword",first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",last_login:"2014-01-14 16:46:45", logins: 15,
		role:1,newsletter:2,force_pwd_change:2, group : []};
    var user = insertObject(user, SERVER_INDEX_URL + "user/create");

	var page = {parent_id:1,slug:"My_page_1",type:"File",active:1,position:1,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0};
	
	var page = insertObject(page, SERVER_INDEX_URL + "pages/create");
	
	var subscription = {user_id : user.id, page_id : page.id}
	
	subscription = insertObject(subscription, SERVER_INDEX_URL + "subscription/create");

	var expected_data = subscription;
    var expected_result = jQuery.extend({"error":false, "message": "Subscription was deleted!", "response": null}, 
                                        {"response": expected_data});
	// url string    
	var urlREST = SERVER_INDEX_URL + "subscription/delete";

		jQuery.ajax({
			type: "DELETE",
			url: urlREST, // URL of the REST web service
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(subscription),
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			}, // error 
			// script call was successful 
			// data contains the JSON values returned by the Perl script 
			success: function(data) {
				if (data.error) { // script returned error
					assert.ok(false, "error");
				} // if
                else { // login was successful
                    expected_result.response.active = 1;
					expected_result.response.created_at = data.response.created_at;
					expected_result.response.updated_at = data.response.updated_at;
					assert.deepEqual(data, expected_result, "Subscription delete.");
				} //else
			}, // success
			complete: function(data){
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/"+page.id);
				logoutUser();
			} // always
		  }); 
});

QUnit.test("DBDN REST Subscription delete - user_id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var subscription = {}

	// expected data
	var expected_result = {"error": true, "message": "Parameter user_id is required!"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "subscription/delete";
    
    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(subscription),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Subscription delete required");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Subscription delete result");
          } // if
          else { // login was successful
        	assert.ok(false, "Subscription delete user id check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Subscription delete - not found", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var subscription = {user_id : 3, page_id : 3}

	// expected data
	var expected_result = {"error": true, "message": "Subscription not found."};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "subscription/delete";
    
    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(subscription),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Subscription delete not found");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Subscription delete not found result");
          } // if
          else { // login was successful
        	assert.ok(false, "Subscription delete not found  check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Get user subscriptions - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
	var user = {username:"sub_gt_usr_12345",password:"P1!as123sword",password_confirmation :"P1!as123sword",first_name:"Firts Name",last_name:"Last Name", email:"somemail2@gmail.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",last_login:"2014-01-14 16:46:45", logins: 15,
		role:1,newsletter:2,force_pwd_change:2, group : []};
    var user = insertObject(user, SERVER_INDEX_URL + "user/create");
	
	var page1 = {parent_id:1,slug:"My_page_1",type:"File",active:1,position:1,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:1,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	page1 = insertObject(page1, SERVER_INDEX_URL + "pages/create");
	//page1  = jQuery.extend(page1,{created_by : 1});
	
	var page2 = {parent_id:1,slug:"My_page_2",type:"File",active:1,position:2,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 2",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:1,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	page2 = insertObject(page2, SERVER_INDEX_URL + "pages/create");
    var expected_data = [page1, page2];
    var fieldsToRemove = ["access","restricted","workflow","assigned_by","due_date","editing_state",
                          "end_edit","featured_menu","front_block","user_edit_id","visits_search"];
	
	var subscription1 = {user_id : user.id,page_id : page1.id}	
	subscription1 = insertObject(subscription1, SERVER_INDEX_URL + "subscription/create");
	
	var subscription2 = {user_id : user.id,page_id : page2.id}	
	subscription2 = insertObject(subscription2, SERVER_INDEX_URL + "subscription/create");	
	
	var expected_result = jQuery.extend({"error":false, "message": "List of pages found.", "response": null}, {"response": expected_data});
	// url string    
	var urlREST = SERVER_INDEX_URL + "subscription/readbyuser/"+user.id;

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				for(var i=0;i < expected_result.response.length;i++){
                    fieldsToRemove.forEach(function(field) {
                        delete expected_result.response[i][field];
                    });
				}
				for(var i=0;i < data.response.length;i++){
                    fieldsToRemove.forEach(function(field) {
                        delete data.response[i][field];
                    });
				}
                
				assert.deepEqual(data, expected_result, "List of pages found.");
			} //else
		}, // success
		complete: function(data){
			deleteSubscription(user.id, page1.id);
			deleteSubscription(user.id, page2.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/"+page1.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/"+page2.id);
			logoutUser();
		} // always
	}); 
});

QUnit.test("DBDN REST Get page subscribers - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
	var user1 = {username:"sub_gt_pg_1_12345",password:"P1!as123sword",password_confirmation :"P1!as123sword",first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",last_login:"2014-01-14 16:46:45", logins: 15,
		role:1,newsletter:2,force_pwd_change:2,active:0, group : []};
    var user1 = insertObject(user1, SERVER_INDEX_URL + "user/create");
	 
	var user2 = {username:"sub_gt_pg_2_12345",password:"P1!as123sword",password_confirmation :"P1!as123sword",first_name:"Firts Name",last_name:"Last Name", email:"somemai2l@gmail.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",last_login:"2014-01-14 16:46:45", logins: 15,
		role:1,newsletter:2,force_pwd_change:2,active:0, group : []};
    var user2 = insertObject(user2, SERVER_INDEX_URL + "user/create");
	
	var page = {parent_id:1,slug:"My_page_1",type:"File",active:1,position:1,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:1};
	
	page = insertObject(page, SERVER_INDEX_URL + "pages/create");
	
	
	var subscription1 = {user_id : user1.id,page_id : page.id}	
	subscription1 = insertObject(subscription1, SERVER_INDEX_URL + "subscription/create");
	
	var subscription2 = {user_id : user2.id,page_id : page.id}	
	subscription2 = insertObject(subscription2, SERVER_INDEX_URL + "subscription/create");
    
	var user_addtion = {
		"force_pwd_change": 1,
		"last_session": null,
		"last_login": "-",		
        "logins": 0,
        "newsletter": 2,
        "newsletter_code": "",
        "newsletter_include": 0,
        "newsletter_lang": null,
        "newsletter_time": "0000-00-00 00:00:00",
        "unsubscribe_code": null,
    };
    user1 = jQuery.extend(user1, user_addtion);
    user2 = jQuery.extend(user2, user_addtion);

	var expected_data = [user1, user2];
    var expected_result = jQuery.extend({"error":false, "message": "List of users found.", "response": null}, 
                                        {"response": expected_data});
	// url string    
	var urlREST = SERVER_INDEX_URL + "subscription/readbypage/"+page.id;

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				assert.deepEqual(data, expected_result, "List of pages found.");
			} //else
		}, // success
		complete: function(data){
			deleteSubscription(user1.id, page.id);
			deleteSubscription(user2.id, page.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user2.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user1.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/"+page.id);
			logoutUser();
		} // always
	}); 
});
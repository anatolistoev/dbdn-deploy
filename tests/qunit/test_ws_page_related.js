QUnit.module("Test REST Page Related Web Services", {
  setup: function() {
    // prepare something for all following tests
	jQuery.ajaxSetup({ timeout: 5000 });
	
	jQuery( document ).ajaxStart(function(){
		ok( true, "ajaxStart" );
	}).ajaxStop(function(){
		ok( true, "ajaxStop" );
		QUnit.start();
	}).ajaxSend(function(){
		ok( true, "ajaxSend" );
	}).ajaxComplete(function(event, request, settings){ 
		ok( true, "ajaxComplete: " + request.responseText );
	}).ajaxError(function(){
		ok( false, "ajaxError" );
	}).ajaxSuccess(function(){
		ok( true, "ajaxSuccess" );
	});
  },
  teardown: function() {
    // clean up after each test
	jQuery( document ).unbind('ajaxStart');
	jQuery( document ).unbind('ajaxStop');
	jQuery( document ).unbind('ajaxSend');
	jQuery( document ).unbind('ajaxComplete');
	jQuery( document ).unbind('ajaxError');
	jQuery( document ).unbind('ajaxSuccess');
  }
});

QUnit.test("DBDN REST Page Related addRelated - success", 6, function(assert) {		//36
	// POST pages/addrelated
	QUnit.stop();
	loginUser();
	// insert test data
	var page1_id = insertPage();
	var page2_id = insertPage();
	
	// test fields
	var related = {"page_id":page1_id, "related_id": page2_id};
	
	// expected data
	var expected_result = jQuery.extend({"error":false, "message": "Related page was added.", "response": null}, {"response": related});
	

	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/addrelated";

	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data:JSON.stringify(related),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.position = data.response.position;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Add related pages result");
			} //else
		}, // success
		complete: function(data){
			// delete test data 
			if(related)
				deleteRelated(related);
			if(page1_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1_id);
			if(page2_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Page Related addRelated - duplicate", 6, function(assert) {
	// POST pages/addrelated
	QUnit.stop();
	loginUser();
	// insert test data
	var page1_id = insertPage();
	var page2_id = insertPage();
	var related1 = insertRelated(page1_id, page2_id);
	
	// test fields
	var related = {"page_id":page1_id, "related_id": page2_id};
	
	// expected data
	var expected_result = {"error":true, "message": "Duplicate record. This page relation already exists."};	

	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/addrelated";
	// Unregister error handler!
	jQuery( document ).unbind('ajaxError');
	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data:JSON.stringify(related),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.equal(XMLHttpRequest.status, 500, "HTTP Status code 500.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Related pages result");
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data) {
			if (data.error) { // script returned error
				//assert.ok(false, "error");
				assert.deepEqual(data, expected_result, "Related pages result");
			} // if
			else { // login was successful
				assert.ok(false, "Related page exists check fail!");
			} //else
		}, // success
		complete: function(data){
			// delete test data 
			if(related)
				deleteRelated(related);
			if(page1_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1_id);
			if(page2_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2_id);
			logoutUser()
		} // always
      });
});

QUnit.test("DBDN REST Page Related addRelated - input data missing", 6, function(assert) {
	// POST pages/addrelated
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
    loginUser();
	// expected data
	var expected_result = {"error":true, "message": "Input data missing!"};

	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/addrelated";

	jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Related pages result");
			logoutUser();
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Related pages result");
          } // if
          else { // login was successful
        	assert.ok(false, "Related pages input data missing check fail!");
          } //else
		  logoutUser();
        } // success
      });
});

QUnit.test("DBDN REST Page Related addRelated - validate faild", 6, function(assert) {
	// POST pages/addrelated
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// test fields
	var related = {"page_id":2, "related_id": "asd"};
	
	// expected data
	var expected_result = {"error":true, "message": "","response": null};

	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/addrelated";

	jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data: JSON.stringify(related),
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			expected_result.response = data.response;
			assert.deepEqual(data, expected_result, "Related pages result");
			logoutUser();
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Related pages result");
          } // if
          else { // login was successful
        	assert.ok(false, "Related pages validation faild check fail!");
          } //else
		  logoutUser();
        } // success
      });
});

QUnit.test("DBDN REST Page Related addRelated - same page error", 6, function(assert) {
	// POST pages/addrelated
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// test fields
	var related = {"page_id":2, "related_id": 2};
	
	// expected data
	var expected_result = {"error":true, "message": "", "response": null};

	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/addrelated";

	jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data: JSON.stringify(related),
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			expected_result.response = data.response;
			assert.deepEqual(data, expected_result, "Related pages result");
			logoutUser();
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Related pages result");
          } // if
          else { // login was successful
        	assert.ok(false, "Related pages validation faild check fail!");
          } //else
		  logoutUser();
        } // success
      });
});

QUnit.test("DBDN REST Page Related getRelated - success", 6, function(assert) {
	// GET pages/related/pageId/lang
	QUnit.stop();
    loginUser();
	// insert test data
	var page1 = {parent_id:0,slug:"Test Delete Page",type:"File",active:1,position:1,in_navigation:1,authorization:0,
			visits:1,template:"Template Page for Delete",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0};
	var page2 = {parent_id:0,slug:"Test Delete Page",type:"File",active:1,position:1,in_navigation:1,authorization:0,
			visits:1,template:"Template Page for Delete",langs:3,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0};
	var page3 = {parent_id:0,slug:"Test Delete Page",type:"File",active:1,position:1,in_navigation:1,authorization:0,
			visits:1,template:"Template Page for Delete",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0};
	var page1_id = insertPage(page1);
	var page2_id = insertPage(page2);
	var page3_id = insertPage(page3);
	var related1 = insertRelated(page1_id, page2_id);
	var related2 = insertRelated(page1_id, page3_id);
	
	var content1 = {page_id:page2_id,lang:1,position:2,section:"section unnown",format:3,body:"This is test text ....",searchable:0};
	var content2 = {page_id:page2_id,lang:1,position:2,section:"body",format:3,body:"This is test text ....",searchable:0};
	var content3 = {page_id:page2_id,lang:1,position:2,section:"title",format:3,body:"Test title",searchable:0};
	var content_title = content3.body;
	content1 = insertObject(content1, SERVER_INDEX_URL + "contents/create");
	content2 = insertObject(content2, SERVER_INDEX_URL + "contents/create");
	content3 = insertObject(content3, SERVER_INDEX_URL + "contents/create");
	// test fields
	var lang = 1;
	
	// expected data
	var expected_result = {"error":false, "message": "List of related pages.", "response": [{"id": page2_id,"title": content_title, "position" : related1.position}]};

	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/related/" + page1_id + "/" + lang;

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				assert.deepEqual(data, expected_result, "List of related pages result");
			} //else
		}, // success
		complete: function(data){
			// delete test data 
			if(content1.id)
				deleteObject(SERVER_INDEX_URL + "contents/delete/" + content1.id);
			if(content2.id)
				deleteObject(SERVER_INDEX_URL + "contents/delete/" + content2.id);
			if(content3.id)
				deleteObject(SERVER_INDEX_URL + "contents/delete/" + content3.id);
			if(related1)
				deleteRelated(related1);
			if(related2)
				deleteRelated(related2);
			if(page1_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1_id);
			if(page2_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2_id);
			if(page3_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page3_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Page Related getRelated - success, lang from session", 6, function(assert) {
	// GET pages/related/pageId
	QUnit.stop();
	loginUser();
	// insert test data
	var page1 = {parent_id:0,slug:"Test Delete Page",type:"File",active:1,position:1,in_navigation:1,authorization:0,
			visits:1,template:"Template Page for Delete",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0};
	var page2 = {parent_id:0,slug:"Test Delete Page",type:"File",active:1,position:1,in_navigation:1,authorization:0,
			visits:1,template:"Template Page for Delete",langs:3,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0};
	var page3 = {parent_id:0,slug:"Test Delete Page",type:"File",active:1,position:1,in_navigation:1,authorization:0,
			visits:1,template:"Template Page for Delete",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0};
	var page1_id = insertPage(page1);
	var page2_id = insertPage(page2);
	var page3_id = insertPage(page3);
	var related1 = insertRelated(page1_id, page2_id);
	var related2 = insertRelated(page1_id, page3_id);
	
	var content1 = {page_id:page3_id,lang:3,position:2,section:"section unnown",format:3,body:"This is test text ....",searchable:0};
	var content2 = {page_id:page3_id,lang:2,position:2,section:"body",format:3,body:"This is test text ....",searchable:0};
	var content3 = {page_id:page3_id,lang:2,position:2,section:"title",format:3,body:"Test title",searchable:0};
	var content_title = content3.body;
	var content1 = insertObject(content1, SERVER_INDEX_URL + "contents/create");
	var content2 = insertObject(content2, SERVER_INDEX_URL + "contents/create");
	var content3 = insertObject(content3, SERVER_INDEX_URL + "contents/create");
	
	// expected data
	var expected_result = {"error":false, "message": "List of related pages.", "response": [{"id": page3_id,"title": content_title, "position":related2.position}]};

	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/related/" + page1_id;

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				assert.deepEqual(data, expected_result, "List of related pages result");
			} //else
		}, // success
		complete: function(data){
			// delete test data 
			if(content1.id)
				deleteObject(SERVER_INDEX_URL + "contents/delete/" + content1.id);
			if(content2.id)
				deleteObject(SERVER_INDEX_URL + "contents/delete/" + content2.id);
			if(content3.id)
				deleteObject(SERVER_INDEX_URL + "contents/delete/" + content3.id);
			if(related1)
				deleteRelated(related1);
			if(related2)
				deleteRelated(related2);
			if(page1_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1_id);
			if(page2_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2_id);
			if(page3_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page3_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Page Related getRelated - id required", 6, function(assert) {
	// GET pages/related/
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error":true, "message": "Parameter ID is required."};

	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/related";

	jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Related pages result");
			logoutUser();
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Related pages result");
          } // if
          else { // login was successful
        	assert.ok(false, "Related pages ID check fail!");
          } //else
		  logoutUser();
        } // success
      });
});

QUnit.test("DBDN REST Page Related getRelated - not found", 6, function(assert) {
	// GET pages/related/999999
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error":true, "message": "Related not found."};

	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/related/999999";

	jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Related pages result");
			logoutUser();
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Related pages result");
          } // if
          else { // login was successful
        	assert.ok(false, "Related pages not found check fail!");
          } //else
		  logoutUser();
        } // success
      });
});

QUnit.test("DBDN REST Page Related removeRelated - success", 6, function(assert) {	//45
	// DELETE pages/removerelated
	QUnit.stop();
	loginUser();
	// insert test data
	var page1_id = insertPage();
	var page2_id = insertPage();
	var related1 = insertRelated(page1_id, page2_id);
	
	// test fields
	var related = {"page_id":page1_id, "related_id":page2_id};

	// expected data
	var expected_result = jQuery.extend({"error":false, "message": "Related page was deleted.", "response": null}, {"response": related1});

	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/removerelated";

	jQuery.ajax({
		type: "DELETE",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data:JSON.stringify(related),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error " + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
				if(related)					
					deleteRelated(related1);
			} // if
			else { // login was successful
				assert.deepEqual(data, expected_result, "Add related pages result");
			} //else
		}, // success
		complete: function(data){
			// delete test data 
			if(page1_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1_id);
			if(page2_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Page Related removeRelated - input data missing", 6, function(assert) {
	// DELETE pages/removerelated
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error":true, "message": "Input data missing!"};

	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/removerelated";

	jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Related pages result");
			logoutUser();
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Related pages result");
          } // if
          else { // login was successful
        	assert.ok(false, "Related pages input data missing check fail!");
          } //else
		  logoutUser();
        } // success
      });
});

QUnit.test("DBDN REST Page Related removeRelated - validate faild", 6, function(assert) {
	// DELETE pages/removerelated
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// test fields
	var related = {"page_id":2, "related_id": "asd"};
	
	// expected data
	var expected_result = {"error":true, "message": "",
							"response": {"related_id": ["The related id must be an integer."]}};

	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/removerelated";

	jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data: JSON.stringify(related),
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Related pages result");
			logoutUser();
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Related pages result");
          } // if
          else { // login was successful
        	assert.ok(false, "Related pages validation faild check fail!");
          } //else
		  logoutUser();
        } // success
      });
});

QUnit.test("DBDN REST Page Related removeRelated - not found", 6, function(assert) {
	// DELETE pages/removerelated
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// test fields
	var related = {"page_id":999999, "related_id":2};
	
	// expected data
	var expected_result = {"error":true, "message": "Related not found!"};

	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/removerelated";

	jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data: JSON.stringify(related),
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Related pages result");
			logoutUser();
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Related pages result");
          } // if
          else { // login was successful
        	assert.ok(false, "Related pages not found check fail!");
          } //else
		  logoutUser();
        } // success
      });
});

QUnit.test("DBDN REST Page Related editRelated - success", 6, function(assert) {	//49
	// PUT pages/editrelated
	QUnit.stop();
    loginUser();
	// insert test data
	var page1_id = insertPage();
	var page2_id = insertPage();
	var page3_id = insertPage();
	var page4_id = insertPage();
	var page5_id = insertPage();
	var page6_id = insertPage();
	var related1 = insertRelated(page1_id, page2_id, 1);
	var related2 = insertRelated(page1_id, page3_id, 2);
	var related3 = insertRelated(page1_id, page4_id, 3);
	var related4 = insertRelated(page1_id, page5_id, 4);
	var related5 = insertRelated(page1_id, page6_id, 5);
	
	// test fields
	var new_position = 2;
	var related = {"page_id":page1_id, "related_id": page6_id, "position":new_position};
	
	// expected data
	var expected_result = jQuery.extend({"error":false, "message": "Related possition updated.", "response": null}, {"response": related5});
	
	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/editrelated";

	jQuery.ajax({
		type: "PUT",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data:JSON.stringify(related),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error"  + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.position = new_position;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Add related pages result");
			} //else
		}, // success
		complete: function(data){
			// delete test data 
			if(related1)
				deleteRelated(related1);
			if(related2)
				deleteRelated(related2);
			if(related3)
				deleteRelated(related3);
			if(related4)
				deleteRelated(related4);
			if(related5)
				deleteRelated(related5);
			if(page1_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1_id);
			if(page2_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2_id);
			if(page3_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page3_id);
			if(page4_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page4_id);
			if(page5_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page5_id);
			if(page6_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page6_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Page Related editRelated - not needed update", 6, function(assert) {	//49
	// PUT pages/editrelated
	QUnit.stop();
    loginUser();
	// insert test data
	var page1_id = insertPage();
	var page2_id = insertPage();
	var page3_id = insertPage();
	var page4_id = insertPage();
	var page5_id = insertPage();
	var page6_id = insertPage();
	var related1 = insertRelated(page1_id, page2_id, 1);
	var related2 = insertRelated(page1_id, page3_id, 2);
	var related3 = insertRelated(page1_id, page4_id, 3);
	var related4 = insertRelated(page1_id, page5_id, 4);
	var related5 = insertRelated(page1_id, page6_id, 5);
	
	// test fields
	var new_position = 3;
	var related = {"page_id":page1_id, "related_id": page4_id, "position":new_position};
	
	// expected data
	var expected_result = jQuery.extend({"error":false, "message": "Not needed update.", "response": null}, {"response": related3});
	
	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/editrelated";

	jQuery.ajax({
		type: "PUT",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data:JSON.stringify(related),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error"  + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.position = 3;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Add related pages result");
			} //else
		}, // success
		complete: function(data){
			// delete test data 
			if(related1)
				deleteRelated(related1);
			if(related2)
				deleteRelated(related2);
			if(related3)
				deleteRelated(related3);
			if(related4)
				deleteRelated(related4);
			if(related5)
				deleteRelated(related5);
			if(page1_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1_id);
			if(page2_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2_id);
			if(page3_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page3_id);
			if(page4_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page4_id);
			if(page5_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page5_id);
			if(page6_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page6_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Page Related editRelated - max position fix", 6, function(assert) {	//49
	// PUT pages/editrelated
	QUnit.stop();
	loginUser();
	// insert test data
	var page1_id = insertPage();
	var page2_id = insertPage();
	var page3_id = insertPage();
	var page4_id = insertPage();
	var page5_id = insertPage();
	var page6_id = insertPage();
	var related1 = insertRelated(page1_id, page2_id, 1);
	var related2 = insertRelated(page1_id, page3_id, 2);
	var related3 = insertRelated(page1_id, page4_id, 3);
	var related4 = insertRelated(page1_id, page5_id, 4);
	var related5 = insertRelated(page1_id, page6_id, 5);
	
	// test fields
	var new_position = 12;
	var related = {"page_id":page1_id, "related_id": page4_id, "position":new_position};
	
	// expected data
	var expected_result = jQuery.extend({"error":false, "message": "Related possition updated.", "response": null}, {"response": related3});
	
	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/editrelated";

	jQuery.ajax({
		type: "PUT",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data:JSON.stringify(related),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error"  + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.position = 5;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Add related pages result");
			} //else
		}, // success
		complete: function(data){
			// delete test data 
			if(related1)
				deleteRelated(related1);
			if(related2)
				deleteRelated(related2);
			if(related3)
				deleteRelated(related3);
			if(related4)
				deleteRelated(related4);
			if(related5)
				deleteRelated(related5);
			if(page1_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1_id);
			if(page2_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2_id);
			if(page3_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page3_id);
			if(page4_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page4_id);
			if(page5_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page5_id);
			if(page6_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page6_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Page Related editRelated - move down", 6, function(assert) {	//49
	// PUT pages/editrelated
	QUnit.stop();
	loginUser();
	// insert test data
	var page1_id = insertPage();
	var page2_id = insertPage();
	var page3_id = insertPage();
	var page4_id = insertPage();
	var page5_id = insertPage();
	var page6_id = insertPage();
	var related1 = insertRelated(page1_id, page2_id, 1);
	var related2 = insertRelated(page1_id, page3_id, 2);
	var related3 = insertRelated(page1_id, page4_id, 3);
	var related4 = insertRelated(page1_id, page5_id, 4);
	var related5 = insertRelated(page1_id, page6_id, 5);
	
	// test fields
	var new_position = 2;
	var related = {"page_id":page1_id, "related_id": page5_id, "position":new_position};
	
	// expected data
	var expected_result = jQuery.extend({"error":false, "message": "Related possition updated.", "response": null}, {"response": related4});
	
	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/editrelated";

	jQuery.ajax({
		type: "PUT",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data:JSON.stringify(related),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error"  + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.position = new_position;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Add related pages result");
			} //else
		}, // success
		complete: function(data){
			// delete test data 
			if(related1)
				deleteRelated(related1);
			if(related2)
				deleteRelated(related2);
			if(related3)
				deleteRelated(related3);
			if(related4)
				deleteRelated(related4);
			if(related5)
				deleteRelated(related5);
			if(page1_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1_id);
			if(page2_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2_id);
			if(page3_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page3_id);
			if(page4_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page4_id);
			if(page5_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page5_id);
			if(page6_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page6_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Page Related editRelated - move up", 6, function(assert) {	//49
	// PUT pages/editrelated
	QUnit.stop();
	loginUser();
	// insert test data
	var page1_id = insertPage();
	var page2_id = insertPage();
	var page3_id = insertPage();
	var page4_id = insertPage();
	var page5_id = insertPage();
	var page6_id = insertPage();
	var related1 = insertRelated(page1_id, page2_id, 1);
	var related2 = insertRelated(page1_id, page3_id, 2);
	var related3 = insertRelated(page1_id, page4_id, 3);
	var related4 = insertRelated(page1_id, page5_id, 4);
	var related5 = insertRelated(page1_id, page6_id, 5);
	
	// test fields
	var new_position = 4;
	var related = {"page_id":page1_id, "related_id": page3_id, "position":new_position};
	
	// expected data
	var expected_result = jQuery.extend({"error":false, "message": "Related possition updated.", "response": null}, {"response": related2});
	
	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/editrelated";

	jQuery.ajax({
		type: "PUT",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data:JSON.stringify(related),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error"  + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.position = new_position;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Add related pages result");
			} //else
		}, // success
		complete: function(data){
			// delete test data 
			if(related1)
				deleteRelated(related1);
			if(related2)
				deleteRelated(related2);
			if(related3)
				deleteRelated(related3);
			if(related4)
				deleteRelated(related4);
			if(related5)
				deleteRelated(related5);
			if(page1_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1_id);
			if(page2_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2_id);
			if(page3_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page3_id);
			if(page4_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page4_id);
			if(page5_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page5_id);
			if(page6_id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page6_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Page Related editRelated - input data missing", 6, function(assert) {	//50
	// PUT pages/editrelated
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error":true, "message": "Input data missing!"};

	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/editrelated";

	jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Related pages result");
			logoutUser();
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Related pages result");
          } // if
          else { // login was successful
        	assert.ok(false, "Related pages input data missing check fail!");
          } //else
		  logoutUser();
        } // success
      });
});

QUnit.test("DBDN REST Page Related editRelated - validate faild", 6, function(assert) {
	// PUT pages/editrelated
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// test fields
	var related = {"page_id":2, "related_id":2, "position": "asd"};
	
	// expected data
	var expected_result = {"error":true, "message": "",
							"response": {"position": ["The position must be an integer."],"related_id": ["The selected page cannot be added as a related link, because it appears to be the same as the current one."]}};

	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/editrelated";

	jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data: JSON.stringify(related),
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Related pages result");
			logoutUser();
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Related pages result");
          } // if
          else { // login was successful
        	assert.ok(false, "Related pages validation faild check fail!");
          } //else
		  logoutUser();
        } // success
      });
});

QUnit.test("DBDN REST Page Related editRelated - not found", 6, function(assert) {	//52
	// PUT pages/editrelated
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// test fields
	var related = {"page_id":999999, "related_id": 2, "position":1};
	
	// expected data
	var expected_result = {"error":true, "message": "Related not found!"};

	// url string    
	var urlREST = SERVER_INDEX_URL + "pages/editrelated";

	jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data: JSON.stringify(related),
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Related pages result");
			logoutUser();
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Related pages result");
          } // if
          else { // login was successful
        	assert.ok(false, "Related pages not found check fail!");
          } //else
		  logoutUser();
        } // success
      });
});

/** 
 * Page Hierarchy Web Service
 */

QUnit.module("Test REST Page Hiearachy Web Services", {
  setup: function() {
    // prepare something for all following tests
	jQuery.ajaxSetup({ timeout: 5000 });
	
	jQuery( document ).ajaxStart(function(){
		ok( true, "ajaxStart" );
	}).ajaxStop(function(){
		ok( true, "ajaxStop" );
		QUnit.start();
	}).ajaxSend(function(){
		ok( true, "ajaxSend" );
	}).ajaxComplete(function(event, request, settings){ 
		ok( true, "ajaxComplete: " + request.responseText );
	}).ajaxError(function(){
		ok( false, "ajaxError" );
	}).ajaxSuccess(function(){
		ok( true, "ajaxSuccess" );
	});
  },
  teardown: function() {
    // clean up after each test
	jQuery( document ).unbind('ajaxStart');
	jQuery( document ).unbind('ajaxStop');
	jQuery( document ).unbind('ajaxSend');
	jQuery( document ).unbind('ajaxComplete');
	jQuery( document ).unbind('ajaxError');
	jQuery( document ).unbind('ajaxSuccess');
  }
});

var fieldsToRemove = ["access","restricted","workflow","assigned_by","due_date","editing_state",
"end_edit","featured_menu","front_block","user_edit_id","visits_search", "updated_at"];

QUnit.test("DBDN REST Get List of Pages Descedents with parent_id 0 - success", 6, function( assert ) {
	// GET /pages/descedents
	QUnit.stop();
	loginUser();
	// insert test data
	var page1 = {parent_id:0,slug:"My_page_1",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content1 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 1",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
	
	var page2 = {parent_id:1,slug:"My_page_2",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content2 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 2",searchable:1,
					created_at:'0000-00-00 00:00:00 ',updated_at:'0000-00-00 00:00:00'};
	
	var page3 = {parent_id:2,slug:"My_page_3",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content3 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 3",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
	
	var page4 = {parent_id:3,slug:"My_page_4",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content4 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 4",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
	
	var page1 = insertObject(page1, SERVER_INDEX_URL + "pages/create");
	content1.page_u_id = page1.id;
	content1 = insertObject(content1, SERVER_INDEX_URL + "contents/create");
    
	page2.parent_id = page1.id;
	var page2 = insertObject(page2, SERVER_INDEX_URL + "pages/create");
	content2.page_u_id = page2.id;
	content2 = insertObject(content2, SERVER_INDEX_URL + "contents/create");
	
	page3.parent_id = page2.id;
	var page3 = insertObject(page3, SERVER_INDEX_URL + "pages/create");
	content3.page_u_id = page3.id;
	content3 = insertObject(content3, SERVER_INDEX_URL + "contents/create");
	
	page4.parent_id = page3.id;
	var page4 = insertObject(page4, SERVER_INDEX_URL + "pages/create");
	content4.page_u_id = page4.id;
	content4 = insertObject(content4, SERVER_INDEX_URL + "contents/create");
	
	jQuery.extend(page1, {"level": 0,'title':"Body 1", 'updated_at':content1.updated_at});
	jQuery.extend(page2, {"level": 1,'title':"Body 2", 'updated_at':content2.updated_at});
	jQuery.extend(page3, {"level": 2,'title':"Body 3", 'updated_at':content3.updated_at});
	jQuery.extend(page4, {"level": 3,'title':"Body 4", 'updated_at':content4.updated_at});
	
	// expected data
	var expected_page = [page2, page3, page4];
	var expected_result = jQuery.extend({"error": false, "message": "List of pages found.", "response": null}, {"response": expected_page});
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "pages/descendants/" + page1.id + "?depth=0&lang=1";
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			for(var i=0;i < expected_result.response.length;i++){
                fieldsToRemove.forEach(function(field) {
                    delete expected_result.response[i][field];
                });
			}
			for(var i=0;i < data.response.length;i++){
                fieldsToRemove.forEach(function(field) {
                    delete data.response[i][field];
                });
            }
			assert.deepEqual(data, expected_result, "Page list result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page4.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page4.id);
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page3.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page3.id);
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page2.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2.id);
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page1.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1.id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Get List of Pages Descedents - success", 6, function( assert ) {
	// GET /pages/descedents
	QUnit.stop();
	loginUser();
	// insert test data
	var page1 = {parent_id:0,slug:"My_page_1",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content1 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 1",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
	
	var page2 = {parent_id:1,slug:"My_page_2",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content2 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 2",searchable:1,
					created_at:'0000-00-00 00:00:00 ',updated_at:'0000-00-00 00:00:00'};
	
	var page3 = {parent_id:2,slug:"My_page_3",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content3 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 3",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
	
	var page4 = {parent_id:3,slug:"My_page_4",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content4 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 4",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
	
	var page1 = insertObject(page1, SERVER_INDEX_URL + "pages/create");
	content1.page_u_id = page1.id;
	content1 = insertObject(content1, SERVER_INDEX_URL + "contents/create");
	
	page2.parent_id = page1.id;
	var page2 = insertObject(page2, SERVER_INDEX_URL + "pages/create");
	content2.page_u_id = page2.id;
	content2 = insertObject(content2, SERVER_INDEX_URL + "contents/create");
	
	page3.parent_id = page2.id;
	var page3 = insertObject(page3, SERVER_INDEX_URL + "pages/create");
	content3.page_u_id = page3.id;
	content3 = insertObject(content3, SERVER_INDEX_URL + "contents/create");
	
	page4.parent_id = page3.id;
	var page4 = insertObject(page4, SERVER_INDEX_URL + "pages/create");
	content4.page_u_id = page4.id;
	content4 = insertObject(content4, SERVER_INDEX_URL + "contents/create");
	
	jQuery.extend(page1, {"level": 0,'title':"Body 1", 'updated_at':content1[0].updated_at});
	jQuery.extend(page2, {"level": 1,'title':"Body 2", 'updated_at':content2[0].updated_at});
	jQuery.extend(page3, {"level": 2,'title':"Body 3", 'updated_at':content3[0].updated_at});
	jQuery.extend(page4, {"level": 3,'title':"Body 4", 'updated_at':content4[0].updated_at});
	
	// expected data
	var expected_page = [page2, page3, page4];
	var expected_result = jQuery.extend({"error": false, "message": "List of pages found.", "response": null}, {"response": expected_page});
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "pages/descendants/"+page1.id+"?depth=0&lang=1";
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			for(var i=0;i < expected_result.response.length;i++){
                fieldsToRemove.forEach(function(field) {
                    delete expected_result.response[i][field];
                });
			}
			for(var i=0;i < data.response.length;i++){
                fieldsToRemove.forEach(function(field) {
                    delete data.response[i][field];
                });
            }
			assert.deepEqual(data, expected_result, "Page list result");
          } //else
		  
        }, // success
		complete: function(data){
			// delete test data 
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page4.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page4.id);
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page3.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page3.id);
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page2.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2.id);
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page1.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1.id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Get List of Pages Descedents (No lang) - success", 6, function( assert ) {
	// GET /pages/descedents
	QUnit.stop();
	loginUser();
	// insert test data
	var page1 = {parent_id:0,slug:"My_page_1",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content1 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 1",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
	
	var page2 = {parent_id:1,slug:"My_page_2",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content2 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 2",searchable:1,
					created_at:'0000-00-00 00:00:00 ',updated_at:'0000-00-00 00:00:00'};
	
	var page3 = {parent_id:2,slug:"My_page_3",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content3 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 3",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
	
	var page4 = {parent_id:3,slug:"My_page_4",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content4 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 4",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
	
	var page1 = insertObject(page1, SERVER_INDEX_URL + "pages/create");
	content1.page_u_id = page1.id;
	content1 = insertObject(content1, SERVER_INDEX_URL + "contents/create");
	
	page2.parent_id = page1.id;
	var page2 = insertObject(page2, SERVER_INDEX_URL + "pages/create");
	content2.page_u_id = page2.id;
	content2 = insertObject(content2, SERVER_INDEX_URL + "contents/create");
	
	page3.parent_id = page2.id;
	var page3 = insertObject(page3, SERVER_INDEX_URL + "pages/create");
	content3.page_u_id = page3.id;
	content3 = insertObject(content3, SERVER_INDEX_URL + "contents/create");
	
	page4.parent_id = page3.id;
	var page4 = insertObject(page4, SERVER_INDEX_URL + "pages/create");
	content4.page_u_id = page4.id;
	content4 = insertObject(content4, SERVER_INDEX_URL + "contents/create");
	
	jQuery.extend(page1, {"created_by": 2,"level": 0, "updated_at" : content1[0].updated_at});
	jQuery.extend(page2, {"created_by": 2,"level": 1, "updated_at" : content2[0].updated_at});
	jQuery.extend(page3, {"created_by": 2,"level": 2, "updated_at" : content3[0].updated_at});
	jQuery.extend(page4, {"created_by": 2,"level": 3, "updated_at" : content4[0].updated_at});
	
	// expected data
	var expected_page = [page2, page3, page4];
	var expected_result = jQuery.extend({"error": false, "message": "List of pages found.", "response": null}, {"response": expected_page});
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "pages/descendants/"+page1.id;
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			for(var i=0;i < expected_result.response.length;i++){
                fieldsToRemove.forEach(function(field) {
                    delete expected_result.response[i][field];
                });
			}
			for(var i=0;i < data.response.length;i++){
                fieldsToRemove.forEach(function(field) {
                    delete data.response[i][field];
                });
            }
			assert.deepEqual(data, expected_result, "Page list result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page1.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1.id);
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page2.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2.id);
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page3.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page3.id);
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page4.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page4.id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Get List of Pages (Depth  = 1) - success", 6, function( assert ) {
	// GET /pages/descedents
	QUnit.stop();
	loginUser();
	// insert test data
	var page1 = {parent_id:0,slug:"My_page_1",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content1 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 1",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
	
	var page2 = {parent_id:1,slug:"My_page_2",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content2 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 2",searchable:1,
					created_at:'0000-00-00 00:00:00 ',updated_at:'0000-00-00 00:00:00'};
	
	var page1 = insertObject(page1, SERVER_INDEX_URL + "pages/create");
	content1.page_u_id = page1.id;
	content1 = insertObject(content1, SERVER_INDEX_URL + "contents/create");
	
	page2.parent_id = page1.id;
	var page2 = insertObject(page2, SERVER_INDEX_URL + "pages/create");
	content2.page_u_id = page2.id;
	content2 = insertObject(content2, SERVER_INDEX_URL + "contents/create");
	
	jQuery.extend(page1, {"created_by": 2,"level": 0,'title':"Body 1", "updated_at" : content1[0].updated_at});
	jQuery.extend(page2, {"created_by": 2,"level": 1,'title':"Body 2", "updated_at" : content2[0].updated_at});
	
	// expected data
	var expected_page = [page2];
	var expected_result = jQuery.extend({"error": false, "message": "List of pages found.", "response": null}, {"response": expected_page});
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "pages/descendants/"+page1.id+"?depth=1&lang=1";
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			for(var i=0;i < expected_result.response.length;i++){
                fieldsToRemove.forEach(function(field) {
                    delete expected_result.response[i][field];
                });
			}
			for(var i=0;i < data.response.length;i++){
                fieldsToRemove.forEach(function(field) {
                    delete data.response[i][field];
                });
            }
			assert.deepEqual(data, expected_result, "Page list result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page2.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2.id);
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page1.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1.id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Get List of Pages Ascedents - success", 6, function( assert ) {
	// GET /pages/ascedents/{$pageId}?depth=0&lang=1
	QUnit.stop();
	loginUser();
	// insert test data
	var page1 = {parent_id:0,slug:"My_page_1",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:3,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content1 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 1",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
	
	var page2 = {parent_id:1,slug:"My_page_2",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:3,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content2 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 2",searchable:1,
					created_at:'0000-00-00 00:00:00 ',updated_at:'0000-00-00 00:00:00'};
	
	var page3 = {parent_id:2,slug:"My_page_3",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:3,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content3 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 3",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
	
	var page4 = {parent_id:3,slug:"My_page_4",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:3,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content4 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 4",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
	
	var page1 = insertObject(page1, SERVER_INDEX_URL + "pages/create");
	content1.page_u_id = page1.id;
	content1 = insertObject(content1, SERVER_INDEX_URL + "contents/create");
	
	page2.parent_id = page1.id;
	var page2 = insertObject(page2, SERVER_INDEX_URL + "pages/create");
	content2.page_u_id = page2.id;
	content2 = insertObject(content2, SERVER_INDEX_URL + "contents/create");
	
	page3.parent_id = page2.id;
	var page3 = insertObject(page3, SERVER_INDEX_URL + "pages/create");
	content3.page_u_id = page3.id;
	content3 = insertObject(content3, SERVER_INDEX_URL + "contents/create");
	
	page4.parent_id = page3.id;
	var page4 = insertObject(page4, SERVER_INDEX_URL + "pages/create");
	content4.page_u_id = page4.id;
	content4 = insertObject(content4, SERVER_INDEX_URL + "contents/create");
	
	jQuery.extend(page1, {"created_by": 2,"level": 3,'title':"Body 1", "updated_at" : content1[0].updated_at});
	jQuery.extend(page2, {"created_by": 2,"level": 2,'title':"Body 2", "updated_at" : content2[0].updated_at});
	jQuery.extend(page3, {"created_by": 2,"level": 1,'title':"Body 3", "updated_at" : content3[0].updated_at});
	
	// expected data
	var expected_page = [page1, page2, page3];
	var expected_result = jQuery.extend({"error": false, "message": "List of pages found.", "response": null}, {"response": expected_page});
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "pages/ascendants/"+page4.id+"?depth=0&lang=1";
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			for(var i=0;i < expected_result.response.length;i++){
                fieldsToRemove.forEach(function(field) {
                    delete expected_result.response[i][field];
                });
			}
			for(var i=0;i < data.response.length;i++){
                fieldsToRemove.forEach(function(field) {
                    delete data.response[i][field];
                });
            }
			assert.deepEqual(data, expected_result, "Page list result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page4.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page4.id);
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page3.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page3.id);
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page2.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2.id);
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page1.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1.id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Get List of Pages Ascedents (No lang) - success", 6, function( assert ) {
	// GET /pages/ascedents/{$pageId}
	QUnit.stop();
	loginUser();
	// insert test data
	var page1 = {parent_id:0,slug:"My_page_1",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content1 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 1",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
	
	var page2 = {parent_id:1,slug:"My_page_2",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content2 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 2",searchable:1,
					created_at:'0000-00-00 00:00:00 ',updated_at:'0000-00-00 00:00:00'};
	
	var page3 = {parent_id:2,slug:"My_page_3",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content3 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 3",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
	
	var page4 = {parent_id:3,slug:"My_page_4",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content4 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 4",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
	
	var page1 = insertObject(page1, SERVER_INDEX_URL + "pages/create");
	content1.page_u_id = page1.id;
	content1 = insertObject(content1, SERVER_INDEX_URL + "contents/create");
	
	page2.parent_id = page1.id;
	var page2 = insertObject(page2, SERVER_INDEX_URL + "pages/create");
	content2.page_u_id = page2.id;
	content2 = insertObject(content2, SERVER_INDEX_URL + "contents/create");
	
	page3.parent_id = page2.id;
	var page3 = insertObject(page3, SERVER_INDEX_URL + "pages/create");
	content3.page_u_id = page3.id;
	content3 = insertObject(content3, SERVER_INDEX_URL + "contents/create");
	
	page4.parent_id = page3.id;
	var page4 = insertObject(page4, SERVER_INDEX_URL + "pages/create");
	content4.page_u_id = page4.id;
	content4 = insertObject(content4, SERVER_INDEX_URL + "contents/create");
	
	jQuery.extend(page1, {"created_by": 2,"level": 3, "updated_at" : content1[0].updated_at});
	jQuery.extend(page2, {"created_by": 2,"level": 2, "updated_at" : content2[0].updated_at});
	jQuery.extend(page3, {"created_by": 2,"level": 1, "updated_at" : content3[0].updated_at});
	
	// expected data
	var expected_page = [page1, page2, page3];
	var expected_result = jQuery.extend({"error": false, "message": "List of pages found.", "response": null}, {"response": expected_page});
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "pages/ascendants/"+page4.id;
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			for(var i=0;i < expected_result.response.length;i++){
                fieldsToRemove.forEach(function(field) {
                    delete expected_result.response[i][field];
                });
			}
			for(var i=0;i < data.response.length;i++){
                fieldsToRemove.forEach(function(field) {
                    delete data.response[i][field];
                });
            }
			assert.deepEqual(data, expected_result, "Page list result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page1.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1.id);
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page2.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2.id);
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page3.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page3.id);
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page4.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page4.id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Get List of Pages (Depth  = 1) - success", 6, function( assert ) {
	// GET /pages/descedents/{$pageId}?depth=1&lang=1
	QUnit.stop();
	loginUser();
	// insert test data
	var page1 = {parent_id:0,slug:"My_page_1",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:3,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content1 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 1",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
	
	var page2 = {parent_id:1,slug:"My_page_2",type:"File",active:1,position:1,in_navigation:1,authorization:0,
		visits:1,template:"Template Update 1",langs:3,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,home_accordion: 0, home_block: 0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	
	var content2 = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Body 2",searchable:1,
					created_at:'0000-00-00 00:00:00 ',updated_at:'0000-00-00 00:00:00'};
	
	var page1 = insertObject(page1, SERVER_INDEX_URL + "pages/create");
	content1.page_u_id = page1.id;
	content1 = insertObject(content1, SERVER_INDEX_URL + "contents/create");
	
	page2.parent_id = page1.id;
	var page2 = insertObject(page2, SERVER_INDEX_URL + "pages/create");
	content2.page_u_id = page2.id;
	content2 = insertObject(content2, SERVER_INDEX_URL + "contents/create");
	
	jQuery.extend(page1, {"created_by": 2,"level": 1,'title':"Body 1", "updated_at" : content1[0].updated_at});
	jQuery.extend(page2, {"created_by": 2,"level": 0,'title':"Body 2", "updated_at" : content2[0].updated_at});
	
	// expected data
	var expected_page = [page1];
	var expected_result = jQuery.extend({"error": false, "message": "List of pages found.", "response": null}, {"response": expected_page});
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "pages/ascendants/"+page2.id+"?depth=1&lang=1";
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			for(var i=0;i < expected_result.response.length;i++){
                fieldsToRemove.forEach(function(field) {
                    delete expected_result.response[i][field];
                });
			}
			for(var i=0;i < data.response.length;i++){
                fieldsToRemove.forEach(function(field) {
                    delete data.response[i][field];
                });
            }
			assert.deepEqual(data, expected_result, "Page list result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page1.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1.id);
			deleteObject(SERVER_INDEX_URL + "pages/delete/" + page2.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2.id);
		} // always
      });
});

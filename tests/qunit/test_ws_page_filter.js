/** 
 * Filter Web Service
 */

QUnit.module("Test REST Page Filter Web Services", {
  setup: function() {
    // prepare something for all following tests
	jQuery.ajaxSetup({ timeout: 5000 });
	
	jQuery( document ).ajaxStart(function(){
		ok( true, "ajaxStart" );
	}).ajaxStop(function(){
		ok( true, "ajaxStop" );
		QUnit.start();
	}).ajaxSend(function(){
		ok( true, "ajaxSend" );
	}).ajaxComplete(function(event, request, settings){ 
		ok( true, "ajaxComplete: " + request.responseText );
	}).ajaxError(function(){
		ok( false, "ajaxError" );
	}).ajaxSuccess(function(){
		ok( true, "ajaxSuccess" );
	});
  },
  teardown: function() {
    // clean up after each test
	jQuery( document ).unbind('ajaxStart');
	jQuery( document ).unbind('ajaxStop');
	jQuery( document ).unbind('ajaxSend');
	jQuery( document ).unbind('ajaxComplete');
	jQuery( document ).unbind('ajaxError');
	jQuery( document ).unbind('ajaxSuccess');
  }
});

QUnit.test("DBDN REST Filter Pages - by page name", 6, function( assert ) {
	// GET /pages/all?filter=My page
	QUnit.stop();
	loginUser();
	// test fields
	var page_name = "My filter page";
	
	// insert test data
	var page1 = {parent_id:1,slug:"My_filter_page_2",type:"File",active:1,position:1,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	var content1 = {lang:1,position:2,section:"title",format:3,body:page_name,searchable:0};
	
	var page2 = {parent_id:1,slug:"y_filter_page_1",type:"File",active:1,position:2,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	var content2 = {lang:1,position:2,section:"title",format:3,body:"y filter page",searchable:0};
	
	var page3 = {parent_id:1,slug:"My_filter_page",type:"File",active:1,position:3,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0,publish_date:"0000-00-00 00:00:00",unpublish_date:"0000-00-00 00:00:00"};
	var content3 = {lang:1,position:2,section:"title",format:3,body:page_name,searchable:0};
	var content1_title = content1.body;
	var content3_title = content3.body;
	
	var page1 = insertObject(page1, SERVER_INDEX_URL + "pages/create");
    jQuery.extend(content1, {"page_u_id": page1.id, "page_id": page1.id});
	var content1 = insertObject(content1, SERVER_INDEX_URL + "contents/create");
	var page2 = insertObject(page2, SERVER_INDEX_URL + "pages/create");
	jQuery.extend(content2, {"page_u_id": page2.id, "page_id": page2.id});
	var content2 = insertObject(content2, SERVER_INDEX_URL + "contents/create");
	var page3 = insertObject(page3, SERVER_INDEX_URL + "pages/create");
	jQuery.extend(content3, {"page_u_id": page3.id, "page_id": page3.id});
	content3 = insertObject(content3, SERVER_INDEX_URL + "contents/create");
	
	jQuery.extend(page1, {"created_by": 0, "level":1,"path":'/',"title":content1_title});
	jQuery.extend(page3, {"created_by": 0, "level":1,"path":'/',"title":content3_title});
	
	// expected data
	var expected_page = [page3, page1];
	var expected_result = jQuery.extend({"error": false, "message": "List of pages found.", "response": null}, {"response": expected_page});
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "pages/search?lang=1&filter=" + encodeURIComponent(page_name);
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
            var sameTitleCount = 0;
            data.response.forEach(function (tmpPage) {
                if (tmpPage.title.indexOf(page_name) !== -1) {
                    sameTitleCount = sameTitleCount + 1;
                }
            });
			assert.equal(data.response.length, sameTitleCount, "Page list result with the same title");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(content1.id)
				deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + content1.id);
			if(content2.id)
				deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + content2.id);
			if(content3.id)
				deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + content3.id);
			if(page1.id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page1.id);
			if(page2.id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page2.id);
			if(page3.id)
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + page3.id);
			
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Filter Pages - unexisting page name", 7, function( assert ) {
	// GET /pages/all?filter=моята страница
	QUnit.stop();
	loginUser();
	// test fields
	var page_name = "моята страница";
	
	// expected data
	var expected_page = new Array();
	var expected_result = jQuery.extend({"error": false, "message": "List of pages found.", "response": null}, {"response": expected_page});

	// url string
	var urlREST =  SERVER_INDEX_URL + "pages/search?lang=1&filter=" +  encodeURIComponent(page_name);
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			logoutUser();
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			assert.deepEqual(data, expected_result, "Page list result");
			assert.deepEqual(data.response, expected_page, "Page list result data");
          } //else
		  logoutUser();
        } // success
      });
});

QUnit.test("DBDN REST Filter Pages - unexisting page name (script)", 7, function( assert ) {
	// GET /pages/all?filter=' OR EXISTS(SELECT * FROM user WHERE 1=1) AND ''='
	QUnit.stop();
	loginUser();
	// test fields
	var script = "' OR EXISTS(SELECT * FROM user WHERE 1=1) AND ''='";
	var script = "0x270x200x4f0x520x200x450x580x490x530x540x530x280x530x450x4c0x450x430x540x200x2a0x200x460x520x4f0x4d0x200x750x730x650x720x200x570x480x450x520x450x200x310x3d0x310x290x200x410x4e0x440x200x270x270x3d0x27";
	var page_name = "test";
	
	// expected data
	var expected_page = new Array();
	var expected_result = jQuery.extend({"error": false, "message": "List of pages found.", "response": null}, {"response": expected_page});
	
	// url string
	var urlREST =  SERVER_INDEX_URL + "pages/search?lang=1&filter=" + script;
	
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			logoutUser();
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			assert.notEqual(data, expected_result, "Page list result");
			assert.notEqual(data.response, expected_page, "Page list result data");
          } //else
		  logoutUser();
        } // success
      });
});
/** 
 * Files Web Service
 */

QUnit.module("Test REST Files Web Services", {
  setup: function() {
    // prepare something for all following tests
	jQuery.ajaxSetup({ timeout: 5000 });
	
	jQuery( document ).ajaxStart(function(){
		ok( true, "ajaxStart" );
	}).ajaxStop(function(){
		ok( true, "ajaxStop" );
		QUnit.start();
	}).ajaxSend(function(){
		ok( true, "ajaxSend" );
	}).ajaxComplete(function(event, request, settings){ 
		ok( true, "ajaxComplete: " + request.responseText );
	}).ajaxError(function(){
		ok( false, "ajaxError" );
	}).ajaxSuccess(function(){
		ok( true, "ajaxSuccess" );
	});
  },
  teardown: function() {
    // clean up after each test
	jQuery( document ).unbind('ajaxStart');
	jQuery( document ).unbind('ajaxStop');
	jQuery( document ).unbind('ajaxSend');
	jQuery( document ).unbind('ajaxComplete');
	jQuery( document ).unbind('ajaxError');
	jQuery( document ).unbind('ajaxSuccess');
  }
});

QUnit.test("DBDN REST Make dir - create", 6, function (assert) {
	// GET /pages/descedents
	QUnit.stop();
	loginUser();
	var dirname = { dirname: "test1", path: "/" };

	// expected data
	var expected_result = jQuery.extend({ "error": false, "message": 'Directory was successfully created.', "response": null }, { "response": dirname });

	// url string
	var urlREST = SERVER_INDEX_URL + "files/makedir/downloads";

	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		data: JSON.stringify(dirname),
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Directory result");
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function (data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				delete expected_result.response.path;
				assert.deepEqual(data, expected_result, "Directory result");
			} //else
		}, // success
		complete: function (data) {
			deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads", '/' + dirname.dirname + '/');
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Make dir - delete", 6, function (assert) {
	// GET /pages/descedents
	QUnit.stop();
	loginUser();
	var dirname = { dirname: "test1", path: "/" };
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");

	data = { dirname: '/' + dirname.dirname + '/' };

	var expected_result = jQuery.extend({ "error": false, "message": "Directory was successfully deleted.", "response": null }, { "response": data });

	// url string

	var urlREST = SERVER_INDEX_URL + "files/deletedir/downloads";

	jQuery.ajax({
		type: "DELETE",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		data: JSON.stringify(data),
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function (data) {
			if (data.error) { // script returned error
				assert.ok(true, "error");
			} // if
			else { // login was successful
				delete expected_result.response.path;
				assert.deepEqual(data, expected_result, "Directory result");
			} //else
		}, // success
		complete: function (data) {
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Upload file", 6, function (assert) {
	// GET /pages/descedents
	QUnit.stop();
	loginUser();
	var dirname = { dirname: "test1", path: "/" };
	var file = {
		file_title: "test1\\icon_vote.png", filename: "test1\\icon_vote.png", zoompic: "test1\\icon_vote.png",
		thumbnail: "test\\icon_vote.png", created_at: "", updated_at: "",
		color_mode: 6, "copyright_notes": "", dimension_units: 2, dimensions: "128x128", resolution: "20", resolution_units: 1, pages: "3",
		software: "photoshop", "usage_terms_de": "", "usage_terms_en": "", version: "1.2", format: "4:3",
		lang: "EN,DE", 'title[1]': "Title", 'title[2]': "", 'description[1]': "Description", 'description[2]': "", lang_info: 1, mode: 'images'
	}

	$('#file_input').change(function () {
		//dirname.dirname = $('input[name="currentpath"]').val()
		console.log("Change of file_input");
		insertObject(dirname, SERVER_INDEX_URL + "files/makedir/images");
		file.filename = $('input[name="currentpath"]').val() + '' + $('input#file_input').val().split('\\').pop();
		file.thumbnail = $('input[name="currentpath"]').val() + '__thumbnail\/' + $('input#file_thumb').val().split('\\').pop();
		file.zoompic = $('input[name="currentpath"]').val() + '__zoompic\/' + $('input#file_zoom').val().split('\\').pop();
		// select the form and submit
		$('#upload').submit();
	});
	$('#file_input').click();
	$('#file_thumb').click();
	$('#file_zoom').click();


	// expected data
	var expected_result = jQuery.extend({ "error": false, "message": 'File was added.', "response": null }, { "response": file })

	$('#upload').ajaxForm({
		async: false,
		url: SERVER_INDEX_URL + "files/createfile",
		data: file,
		beforeSubmit: function (a, f, o) {
			o.dataType = "json";
		},
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error         
		success: function (data) {
			if (data.error) { // script returned error
				assert.ok(false, "error: Cannot create file!");
			} // if
			else { // login was successful
				file.id = data.response.id;
				expected_result.response.id = data.response.id;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				expected_result.response.extension = data.response.extension;
				expected_result.response.size = data.response.size;

				delete expected_result.response.description;
				delete expected_result.response.title;
				delete expected_result.response.lang_info;
				delete expected_result.response.mode;
				delete expected_result.response.file_title;

				delete file['title[1]'];
				delete file['title[2]'];
				delete file['description[1]'];
				delete file['description[2]'];
				expected_result.response = jQuery.extend(expected_result.response, { "type": "images" });

				assert.deepEqual(data, expected_result, "File result");
			}
		},
		complete: function () {
			deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/" + file.id);
			//deleteObject(SERVER_INDEX_URL + "files/deletedir/"+dirname.dirname);
			deleteDir(SERVER_INDEX_URL + "files/deletedir/images", '/' + dirname.dirname + '/');

			logoutUser();
		}
	});
});

// http://localhost:8000/tests/qunit/index.html?module=Test%20REST%20Files%20Web%20Services&testNumber=177
QUnit.test("DBDN REST Upload file zip", 6, function (assert) {
	// GET /pages/descedents
	QUnit.stop();
	loginUser();
	var dirname = { dirname: "test1", path: "/" };
	// var file = {filename : "test1\\icon_vote.png" ,filename : "test1\\icon_vote.png" ,filename : "test1\\icon_vote.png" , created_at: "", updated_at : "",
	// 	color_mode : "RGB", dimension_units : "pixels", dimensions : "128x128",	resolution : "20",resolution_units : "dpi", pages : "3",
	// 	software: "photoshop", version:"1.2", format : "4:3", lang : "EN,DE", 'title[1]': "Title", 'title[2]' : "", 'description[1]' : "Description",'description[2]' : "", lang_info : 1,mode: 'images'}
	var file = {
		file_title: "test1\\icon_vote.png", filename: "test1\\icon_vote.png",
		//        zoompic : "test1\\icon_vote.png",
		//        thumbnail : "test\\icon_vote.png" , 
		created_at: "", updated_at: "",
		color_mode: 6, "copyright_notes": "", dimension_units: 2, dimensions: "128x128", resolution: "20", resolution_units: 1, pages: "3",
		software: "photoshop", "usage_terms_de": "", "usage_terms_en": "", version: "1.2", format: "4:3",
		lang: "EN,DE", 'title[1]': "Title", 'title[2]': "", 'description[1]': "Description", 'description[2]': "",
		lang_info: 1, mode: 'downloads'
	};

	$('#file_input').change(function () {
		//dirname.dirname = $('input[name="path"]').val()
		insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");
		file.filename = $('input[name="currentpath"]').val() + '' + $('input#file_input').val().split('\\').pop();
		//file.thumbnail = $('input[name="currentpath"]').val()+'__thumbnail\/'+$('input#file_thumb').val().split('\\').pop();
		//file.zoompic = $('input[name="currentpath"]').val()+'__zoompic\/'+$('input#file_zoom').val().split('\\').pop();
		// Remove unused files.
		$("input#file_thumb").remove();
		$("input#file_zoom").remove();
		// select the form and submit
		$('#upload').submit();
	});
	$('#file_input').click();

	// expected data
	var expected_result = jQuery.extend({ "error": false, "message": 'File was added.', "response": null }, { "response": file });

	$('#upload').ajaxForm({
		async: false,
		url: SERVER_INDEX_URL + "files/createfile",
		data: file,
		beforeSubmit: function (a, f, o) {
			o.dataType = "json";
		},
		success: function (data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				file.id = data.response.id;
				expected_result.response.id = data.response.id;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				expected_result.response.extension = data.response.extension;
				expected_result.response.size = data.response.size;
				expected_result.response.zoompic = data.response.zoompic;

				delete expected_result.response.description;
				delete expected_result.response.title;
				delete expected_result.response.lang_info;
				delete expected_result.response.mode;
				delete expected_result.response.file_title;

				delete file['title[1]'];
				delete file['title[2]'];
				delete file['description[1]'];
				delete file['description[2]'];
				expected_result.response = jQuery.extend(expected_result.response, { "type": "downloads" });

				assert.deepEqual(data, expected_result, "File result");
			}
		},
		complete: function () {
			deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/" + file.id);
			deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads", '/' + dirname.dirname + '/');

			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Delete file", 6, function (assert) {
	// GET /pages/descedents
	QUnit.stop();
	loginUser();
	var dirname = { dirname: UNIT_TEST_DIR, path: "/" };
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");
	insertFile(SERVER_INDEX_URL + "files/createfile", function (file) {
		var urlREST = SERVER_INDEX_URL + "files/deletefile/" + file.response.id;
		jQuery.extend(file.response, {
			"brand_id": 0, "category_id": 0, type: "Downloads",
			protected: 0, "user_edit_id": null, "visible": 0
		});
		// expected data
		var expected_result = jQuery.extend({ "error": false, "message": 'File was deleted.', "response": null },
			{ "response": file.response });

		jQuery.ajax({
			type: "DELETE",
			url: urlREST, // URL of the REST web service
			processData: false,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			async: false,
			// script call was *not* successful
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			}, // error 
			// script call was successful 
			// data contains the JSON values returned by the Perl script 
			success: function (data) {
				if (data.error) { // script returned error
					assert.ok(true, "error");
				} // if
				else { // login was successful
					expected_result.response.size = expected_result.response.size;
					expected_result.response.updated_at = data.response.updated_at;
					// Replace string with number!
					expected_result.response.deleted = Number(expected_result.response.deleted);
					expected_result.response.thumb_visible = Number(expected_result.response.thumb_visible);

					assert.deepEqual(data, expected_result, "File result");
				} //else
			}, // success
			complete: function (data) {
				deleteObject(SERVER_INDEX_URL + "files/permanentdelete/" + file.response.id);
				deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads", '/' + dirname.dirname + '/');
				logoutUser();
			} // always
		});
	}).error(function (XMLHttpRequest, textStatus, errorThrown) {
		assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads", '/' + dirname.dirname + '/');
		QUnit.stop();
	});
});

QUnit.test("DBDN REST Get files By Type", 6, function (assert) {
	// GET /pages/descedents
	QUnit.stop();
	loginUser();
	var dirname = { dirname: UNIT_TEST_DIR, path: "/" };
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");
	var file1;

	insertFile(SERVER_INDEX_URL + "files/createfile", function (file) {
		var urlREST = SERVER_INDEX_URL + "files/filesbytype?type=ico";
		jQuery.extend(file.response, {
			"brand_id": 0, "category_id": 0, type: "Downloads",
			protected: 0, "user_edit_id": null, "visible": 0
		});

		var expected_data = [file.response];
		var expected_result = jQuery.extend({ "error": false, "message": 'List of files found.', "response": null },
			{ "response": expected_data });

		jQuery.ajax({
			type: "GET",
			url: urlREST, // URL of the REST web service
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			async: false,
			// script call was *not* successful
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			}, // error 
			// script call was successful 
			// data contains the JSON values returned by the Perl script 
			success: function (data) {
				if (data.error) { // script returned error
					assert.ok(true, "error");
				} // if
				else { // login was successful
					var i = 0;
					for (i; i < expected_result.response.length; i++) {
						// Replace string with number!
						expected_result.response[i].deleted = Number(expected_result.response[i].deleted);
						expected_result.response[i].thumb_visible = Number(expected_result.response[i].thumb_visible);
						expected_result.response[i].size = Number(expected_result.response[i].size);
					};
					assert.deepEqual(data, expected_result, "File result");
				} //else
			}, // success
			complete: function (data) {
				deleteObject(SERVER_INDEX_URL + "files/permanentdelete/" + file.response.id);
				deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads", '/' + dirname.dirname + '/');
				logoutUser();
			} // always
		});
	});

});

QUnit.test("DBDN REST Get files By Folder", 6, function (assert) {
	// GET /pages/descedents
	QUnit.stop();
	loginUser();
	var dirname = { dirname: UNIT_TEST_DIR, path: "/" };
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");

	insertFile(SERVER_INDEX_URL + "files/createfile", function (file) {
		var urlREST = SERVER_INDEX_URL + "files/all/downloads?path=/" + dirname.dirname + "/&sort-by=filename";
		jQuery.extend(file.response, { type: "Downloads", protected: 0 });

		var expected_data = [{
			'extension': file.response.extension,
			'filename': file.response.filename.substring(dirname.dirname.length + 2),
			'id': file.response.id,
			'size': file.response.size,
			'type': file.response.type,
			'updated_at': file.response.updated_at,
			protected: file.response.protected
		}];

		var expected_result = jQuery.extend({ "error": false, "message": 'List of files found.', "response": null },
			{ "response": expected_data });

		jQuery.ajax({
			type: "GET",
			url: urlREST, // URL of the REST web service
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			async: false,
			// script call was *not* successful
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			}, // error 
			// script call was successful 
			// data contains the JSON values returned by the Perl script 
			success: function (data) {
				if (data.error) { // script returned error
					assert.ok(true, "error");
				} // if
				else { // login was successful
					var i = 0;
					for (i; i < expected_result.response.length; i++) {
						// Replace string with number!
						expected_result.response[i].size = Number(expected_result.response[i].size);
					};
					assert.deepEqual(data, expected_result, "File result");
				} //else
			}, // success
			complete: function (data) {
				deleteObject(SERVER_INDEX_URL + "files/permanentdelete/" + file.response.id);
				deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads", '/' + dirname.dirname + '/');
				logoutUser();
			} // always
		});
	});

});

QUnit.test("DBDN REST Get All", 6, function (assert) {
	// GET /pages/descedents
	QUnit.stop();
	loginUser();
	var dirname = { dirname: UNIT_TEST_DIR, path: "/" };
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");

	insertFile(SERVER_INDEX_URL + "files/createfile", function (file1) {

		jQuery.extend(file1.response, { "brand_id": 0, "category_id": 0 });

		insertFile(SERVER_INDEX_URL + "files/createfile", 'test_second', function (file) {
			var urlREST = SERVER_INDEX_URL + "files/all/downloads?path=" + '/' + dirname.dirname + '/';
			jQuery.extend(file.response, { "brand_id": 0, "category_id": 0, type: "images" });

			var expected_data = [{
				'extension': file.response.extension,
				'filename': file.response.filename.substring(dirname.dirname.length + 2),
				'id': file.response.id,
				'size': file.response.size,
				'type': 'Downloads',
				'updated_at': file.response.updated_at,
				'protected': 0
			},
			{
				'extension': file1.response.extension,
				'filename': file1.response.filename.substring(dirname.dirname.length + 2),
				'id': file1.response.id,
				'size': file1.response.size,
				'type': 'Downloads',
				'updated_at': file1.response.updated_at,
				'protected': 0
			}];

			var expected_result = jQuery.extend({ "error": false, "message": 'List of files found.', "response": null },
				{ "response": expected_data });

			jQuery.ajax({
				type: "GET",
				url: urlREST, // URL of the REST web service
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				async: false,
				// script call was *not* successful
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					assert.ok(false, "error: " + textStatus + " : " + errorThrown);
				}, // error 
				// script call was successful 
				// data contains the JSON values returned by the Perl script 
				success: function (data) {
					if (data.error) { // script returned error
						assert.ok(true, "error");
					} // if
					else { // login was successful
						assert.deepEqual(data, expected_result, "File result");
					} //else
				}, // success
				complete: function (data) {
					deleteObject(SERVER_INDEX_URL + "files/permanentdelete/" + file1.response.id);
					deleteObject(SERVER_INDEX_URL + "files/permanentdelete/" + file.response.id);
					deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads", '/' + dirname.dirname + '/');
					logoutUser();
				} // always
			});

		});
	});
});

QUnit.test("DBDN REST Update file", 6, function (assert) {
	// GET /pages/descedents
	QUnit.stop();
	loginUser();
	var dirname = { dirname: UNIT_TEST_DIR, path: "/" };
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");

	var file1;
	insertFile(SERVER_INDEX_URL + "files/createfile", function (file) {

		file1 = file.response;
		jQuery.extend(file1, {
			"brand_id": 0,
			"category_id": 0,
			'title[1]': "Title 2",
			'title[2]': "Title 2",
			'description[1]': "Description 2",
			'description[2]': "Description 2",
			lang: "EN,DE",
			type: "Downloads",
			protected: 0
		});

		$('#upload').ajaxForm({
			async: false,
			url: SERVER_INDEX_URL + "files/updatefile/" + file1.id + "?mode=downloads",
			data: file1,
			beforeSubmit: function (a, f, o) {
				o.dataType = "json";
			},
			success: function (data) {
				if (data.error) { // script returned error
					assert.ok(false, "error");
				} // if
				else { // login was successful		
					file1 = data.response;

					var expected_data = file1;
					var expected_result = jQuery.extend({ "error": false, "message": 'File was updated.', "response": null },
						{ "response": expected_data });

					assert.deepEqual(data, expected_result, "File result");
				}
			}, // success
			complete: function (data) {
				deleteObject(SERVER_INDEX_URL + "files/permanentdelete/" + file1.id);
				deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads", '/' + dirname.dirname + '/');
				logoutUser();
			} // always
		});
		$("input#file_input").remove();
		$('input#file_zoom').unbind('change');
		$('input#file_zoom').change(function () {
			$('input[name="path"]').val(dirname.dirname);
			//file.filename = dirname.dirname+'\\'+$('input#file_input').val().split('\\').pop();
			file.thumbnail = dirname.dirname + '\\thumbnail\\' + $('input#file_thumb').val().split('\\').pop();
			file.zoompic = dirname.dirname + '\\zoompic\\' + $('input#file_zoom').val().split('\\').pop();
			// select the form and submit
			$('#upload').submit();
		});
		$('input#file_zoom').click();
		$('input#file_thumb').click();
	});
});


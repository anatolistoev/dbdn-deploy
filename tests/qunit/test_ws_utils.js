/**
 * Web Service Utills
 *
 * @param {Object} my_object Object to insert in DB
 * @param {String} urlREST URL where to POST the object
 */
function insertObject(my_object, urlREST, methodType){
    if (methodType === undefined)
        methodType = "POST"
	// Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	// Insert page for delete:
	var result_object = {};
	jQuery.ajax({
		type: methodType,
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(my_object),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//if(XMLHttpRequest.responseText.indexOf('Directory already exists') > -1)
			QUnit.ok(false, "error: " + textStatus + " : " + errorThrown + " result: " + JSON.stringify(XMLHttpRequest.responseJSON));
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data){
			if (data.error) { // script returned error
				QUnit.ok(false, "error result: " + JSON.stringify(XMLHttpRequest.responseJSON));
			} // if
			else { // login was successful
				result_object = data.response;
			} //else
		} // success
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
	return result_object;
}

function deleteObject(urlREST){
	// Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });
	if (urlREST.endsWith("/undefined")) {
		return null;
	}
	//var urlREST = SERVER_INDEX_URL + "pages/delete/" + id;
	var promise = jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		async: false,
//		data:JSON.stringify(new_page),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        	//assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
        	//assert.ok(false, "error");
          } // if
          else { // login was successful
			//expected_result.response.created_by = data.response.created_by;
			//assert.deepEqual(data, expected_result, "Page delete result");
			//assert.ok(false, "Page delete result");
          } //else
        } // success
      });
	// Enable global ajax events and asserts!
    $.ajaxSetup({ global: true });
    return promise;
}

function deleteDir(urlREST,dirname){
	// Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });
	var del_file = {dirname:dirname}
	jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(del_file),
		async: false,
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        	//assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
        	//assert.ok(false, "error");
          } // if
          else { // login was successful
			//expected_result.response.created_by = data.response.created_by;
			//assert.deepEqual(data, expected_result, "Page delete result");
			//assert.ok(false, "Page delete result");
          } //else
        } // success
      });
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
}

function insertPage(page){
	// insert page and return id
	if(typeof(page) === 'undefined' || page === null){
//	if(page === null){
		var page = {parent_id:0,slug:"Test_delete_page",type:"File",active:1,position:1,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
			visits:1,template:"Template Page for Delete",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0, access : [{"group_id" : 2},{"group_id" : 3}]};
	}
   page = insertObject(page, SERVER_INDEX_URL + "pages/create");
   return page.id;
}

function insertRelated(page1_id, page2_id, position){
   // Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	if(position == null){
		position = 1;
	}
	urlREST = SERVER_INDEX_URL + "pages/addrelated";
	var related = {"page_id":page1_id, "related_id":page2_id, "position":position};
	var _related = {};

	// Insert related pages:
	jQuery.ajax({
		type: "POST",
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(related),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data){
			if (data.error) { // script returned error
				//assert.ok(false, "error");
			} // if
			else { // login was successful
				_related = data.response;
			} //else
		} // success
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
	   return _related;
}

function deleteRelated(related){

   // Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_INDEX_URL + "pages/removerelated";
	//var related = {"page_id":page1_id, "related_id":page2_id};

	// Insert related pages:
	jQuery.ajax({
		type: "DELETE",
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(related),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data){
			if (data.error) { // script returned error
				//assert.ok(false, "error");
			} // if
			else { // login was successful
				// related = data.response;
			} //else
		} // success
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
}


function insertFile(urlREST, filename, callback, isProtected){
    // Stop global ajax events and asserts!
    $.ajaxSetup({ global: false });

    if ( arguments.length === 2 ) {
        callback = filename;
        filename = null;
        isProtected = false;
    }
    filename = filename ? (filename+'.ico') :'test.ico';

    var fileInfo = {
        currentpath: "/"  + UNIT_TEST_DIR + "/",
        'title[1]': "Title",
        'title[2]' : "Title",
        'description[1]' : "Description",
        'description[2]' : "Description",
        format : "4:3",
        deleted : 0,
        dimensions : "128x128",
        dimension_units : "pixels",
        resolution : "20",
        resolution_units : "dpi",
        color_mode : "RGB",
        lang : "EN,DE",
        pages : "3",
        version:"1.2",
        copyright_notes: "",
        usage_terms_de:"",
        usage_terms_en:"",
        software: "",
        filename: filename,
        file_title: 0,
        file_zoom_title:"",
        file_thumb_title:"",
        mode: 'downloads',
        zoompic: "",
        thumbnail: "",
        thumb_visible: 0,
        end_edit: "0000-00-00 00:00:00",
    }

    if (isProtected) {
        fileInfo.protected = 1;
    }

    var formData = new FormData();

    for (prop in fileInfo) {
        if(fileInfo.hasOwnProperty(prop)){
            formData.append(prop, fileInfo[prop]);
        }
    }

    var file = "data:image/x-icon;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKX/CACl/2AApf9oAKX/YACl/14Apf9kAKX/ZgCl/w4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKX/IACl/3IApf8OAKX/QACl/3wApf+AAKX/SgCl/wwApf9uAKX/LgAAAAAAAAAAAAAAAAAAAAAAAAAAAKX/DACl/3AApf8WAKX/wACl//8Apf//AKX//wCl//8Apf/QAKX/JACl/2gApf8WAAAAAAAAAAAAAAAAAAAAAACl/2QApf8MAKX/xACl//8Apf//AKX//wCl//8Apf//AKX//wCl/9wApf8OAKX/agAAAAAAAAAAAAAAAAAAAAAApf9oAKX/SgCl//8Apf//AKX/tgCl/6wApf//AKX//wCl//8Apf//AKX/ZACl/2YAAAAAAAAAAAAAAAAAAAAAAKX/ZgCl/4AApf//AKX//wCl/44Apf+AAKX//wCl//oApf/4AKX//wCl/5gApf9aAKX/CAAAAAAAAAAAAAAAAACl/2YApf94AKX//wCl//8Apf//AKX//wCl//8Apf+0AKX/pACl//8Apf+QAKX/YgCl/wIAAAAAAAAAAAAAAAAApf9oAKX/GgCl/1AApf9QAKX/TgCl/04Apf9OAKX/TgCl/04Apf9OAKX/IACl/2oAAAAAAAAAAAAAAAAAAAAAAKX/OgCl/zgAAAAAAAAAAAAAAAAApf+EAKX/VgAAAAAAAAAAAAAAAACl/zAApf9EAAAAAAAAAAAAAAAAAAAAAAAAAAAApf9kAKX/KgAAAAAAAAAAAKX/WgCl/2QApf9CAAAAAACl/yYApf9oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACl/1QApf9oAKX/IAAAAAAApf8wAKX/VgCl/2gApf9YAKX/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKX/BgCl/4QAAAAAAAAAAACl/3YApf8IAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAApf9gAKX/TgCl/2QApf9cAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKX/YACl/zoApf9QAKX/XAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKX/IACl/4YAAAAAAAAAAACl/3IApf8uAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACl/zQApf9wAKX/YACl/2IApf9kAKX/SgAAAAAAAAAAAAAAAAAAAAAAAAAA//8AAP9/AAD4HwAA8A8AAPAPAADgBwAA8AcAAP//AAD+/wAA//8AAP//AAD9/wAA//8AAP//AAD9/wAA//8AAA==";
    var fileBlob = dataURItoBlob(file);
    formData.append("files[]", fileBlob, filename);

    var result_promise = $.ajax({
        url : urlREST,
        type : 'POST',
        data : formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        success: function(data) {
            if (data.error) { // script returned error
            //assert.ok(false, "error");
            } // if
            else { // login was successful
                callback(data);
            }
        },
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            QUnit.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error
    });
    
    $.ajaxSetup({ global: true });
    return result_promise;
}

function insertProtectedFile(urlREST, filename, callback){
    return insertFile(urlREST, filename, callback, true);
}

function deleteCartItem(user_id,file_id){

   // Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_INDEX_URL + "cart/removecartitem";
	var cartitem = {user_id:user_id, file_id:file_id};

	// Insert related pages:
	jQuery.ajax({
		type: "DELETE",
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(cartitem),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data){
			if (data.error) { // script returned error
				//assert.ok(false, "error");
			} // if
			else { // login was successful
				// related = data.response;
			} //else
		} // success
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
}

function addCartItem(user_id, file_id){
   // Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_INDEX_URL + "cart/additemtocart";
	var cartitem = {"user_id":user_id, "file_id":file_id};
	var return_object = {};

	// Insert related pages:
	jQuery.ajax({
		type: "POST",
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(cartitem),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data){
			if (data.error) { // script returned error
				//assert.ok(false, "error");
			} // if
			else { // login was successful
				return_object = data.response;
			} //else
		} // success
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
	return return_object;
}

function forceDeleteDownload(page_u_id){

   // Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_INDEX_URL + "download/forcedeletedownloadbypage";
	var downloaditem = {page_u_id:page_u_id};

	// Insert related pages:
	jQuery.ajax({
		type: "DELETE",
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(downloaditem),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data){
			if (data.error) { // script returned error
				//assert.ok(false, "error");
			} // if
			else { // login was successful
				// related = data.response;
			} //else
		} // success
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
}

function addDownload(page_id, page_u_id, file_id){
   // Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_INDEX_URL + "download/createdownload";
	var downloaditem = {"page_id":page_id, page_u_id: page_u_id, "file_id":file_id};
	var return_object = {};

	// Insert related pages:
	jQuery.ajax({
		type: "POST",
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(downloaditem),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data){
			if (data.error) { // script returned error
				//assert.ok(false, "error");
			} // if
			else { // login was successful
				return_object = data.response;
			} //else
		} // success
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
	return return_object;
}

function deleteFavorite(user_id,page_id){

   // Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_INDEX_URL + "favorite/removefavorite";
	var favorite = {user_id:user_id, page_id:page_id};

	// Insert related pages:
	jQuery.ajax({
		type: "DELETE",
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(favorite),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {},
		success: function(data){}
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
}

function addFavorite(user_id, page_id){
   // Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_INDEX_URL + "favorite/createfavorite";
	var favorite = {"user_id":user_id, "page_id":page_id};
	var return_object = {};

	// Insert related pages:
	jQuery.ajax({
		type: "POST",
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(favorite),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {},
		success: function(data){
			if (data.error) {}
			else {
				return_object = data.response;
			}
		}
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
	return return_object;
}

function deleteGlossary(page_id,term_id){

   // Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_INDEX_URL + "glossary/remove";
	var glossaryitem = {page_id:page_id, term_id:term_id};

	jQuery.ajax({
		type: "DELETE",
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(glossaryitem),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data){
			if (data.error) { // script returned error
				//assert.ok(false, "error");
			} // if
			else { // login was successful
				// related = data.response;
			} //else
		} // success
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
}

function addGlossary(page_id, term_id){
   // Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_INDEX_URL + "glossary/create";
	var glossaryitem = {"page_id":page_id, "term_id":term_id};
	var return_object = {};

	jQuery.ajax({
		type: "POST",
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(glossaryitem),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data){
			if (data.error) { // script returned error
				//assert.ok(false, "error");
			} // if
			else { // login was successful
				return_object = data.response;
			} //else
		} // success
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
	return return_object;
}

function addUserToGroup(user_id, group_id){
   // Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_INDEX_URL + "group/addmember";
	var usergroupitem = {"user_id":user_id, "group_id":group_id};
	var return_object = {};

	jQuery.ajax({
		type: "POST",
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(usergroupitem),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data){
			if (data.error) { // script returned error
				//assert.ok(false, "error");
			} // if
			else { // login was successful
				return_object = data.response;
			} //else
		} // success
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
	return return_object;
}

function removeUserFromGroup(user_id, group_id){
   // Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_INDEX_URL + "group/removemember";
	var usergroupitem = {"user_id":user_id, "group_id":group_id};
	var return_object = {};

	jQuery.ajax({
		type: "DELETE",
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(usergroupitem),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data){
			if (data.error) { // script returned error
				//assert.ok(false, "error");
			} // if
			else { // login was successful
				return_object = data.response;
			} //else
		} // success
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
	return return_object;
}

function removeUserFromGroupForce(user_id, group_id){
   // Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_INDEX_URL + "group/forceremovemember";
	var usergroupitem = {"user_id":user_id, "group_id":group_id};
	var return_object = {};

	jQuery.ajax({
		type: "DELETE",
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(usergroupitem),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data){
			if (data.error) { // script returned error
				//assert.ok(false, "error");
			} // if
			else { // login was successful
				return_object = data.response;
			} //else
		} // success
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
	return return_object;
}

function deleteAccess(page_id){

   // Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_INDEX_URL + "access/forcedeletebypage";
	var access = {page_id:page_id};

	// Insert related pages:
	jQuery.ajax({
		type: "DELETE",
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(access),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {},
		success: function(data){}
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
}

function addAccess(group_id, page_id){
   // Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_INDEX_URL + "access/grant";
	var access = {"group_id":group_id, "page_id":page_id};
	var return_object = {};

	// Insert related pages:
	jQuery.ajax({
		type: "POST",
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(access),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {},
		success: function(data){
			if (data.error) {}
			else {
				return_object = data.response;
			}
		}
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
	return return_object;
}

function deleteRating(user_id,page_id){

   // Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_INDEX_URL + "rating/forcedeletebypage";
	var rating = {user_id:user_id, page_id:page_id};

	// Insert related pages:
	jQuery.ajax({
		type: "DELETE",
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(rating),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {},
		success: function(data){}
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
}

function deleteSubscription(user_id,page_id){

   // Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_INDEX_URL + "subscription/delete";
	var subscription = {user_id:user_id, page_id:page_id};

	// Insert related pages:
	jQuery.ajax({
		type: "DELETE",
		async: false,
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType:"json",
		data:JSON.stringify(subscription),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {},
		success: function(data){}
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
}

function loginUser(user){
	// Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_BASE_URL + "cms/login";
    if(!user){
        user = {username:'esof', password:'Pa55word!?', __unitTests : "1"};
    }

	// Insert related pages:
	jQuery.ajax({
		type: "POST",
		async: false,
		url: urlREST, // URL of the REST web service
		data:user,
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
            QUnit.ok(false, "Error on login!");
        },
		success: function(data){

        }
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });
}


function logoutUser(){
	// Stop global ajax events and asserts!
	$.ajaxSetup({ global: false });

	urlREST = SERVER_BASE_URL + "cms/logout";

	// Insert related pages:
	jQuery.ajax({
		type: "POST",
		async: false,
		url: urlREST, // URL of the REST web service
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {},
		success: function(data){}
	});
	// Enable global ajax events and asserts!
	$.ajaxSetup({ global: true });

}

function createUser(username){
    var user = {username:username? username: "tests_user",password:"P1!as123sword",password_confirmation:"P1!as123sword",first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",last_login:"2014-01-14 16:46:45", logins: 15,
		role:1,newsletter:2,force_pwd_change:2, group : []};
    user = insertObject(user, SERVER_INDEX_URL + "user/create");
    return user;
}

function createAndActivatePage(){
    var page = {parent_id:1,slug:"My_page_1",type:"File",active:1,position:1,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0};
		
    var dbPage = insertObject(page, SERVER_INDEX_URL + "pages/create");
        page.id = dbPage.id;
        page.u_id = dbPage.u_id;

	var workflow = dbPage.workflow;
	if (!workflow) {
		console.log("Wrong page - no workflow.");
		return dbPage;
	}
    workflow.editing_state = 'Approved';
    workflow.user_responsible = 2;
    workflow = insertObject(workflow, SERVER_INDEX_URL + "workflow/update", "PUT");

    page.active = 1;
    page = insertObject(page, SERVER_INDEX_URL + "pages/update", "PUT");
    return page;
}

function createPageContent(page){
    var content = {page_id:0,lang:1,position:1,section:'title',format:1,body:"Title 1",searchable:1,
					created_at:'0000-00-00 00:00:00',updated_at:'0000-00-00 00:00:00'};
    content.page_id = page.id;

    ///////////FIX
    content.page_u_id = page.id;

	content = insertObject(content, SERVER_INDEX_URL + "contents/create");
    return content;
}

function forceDeletePage(page_id){
    deleteObject(SERVER_INDEX_URL + "pages/forcedelete/"+page_id);
    deleteObject(SERVER_INDEX_URL + "workflow/forcedelete/"+page_id);
    deleteObject(SERVER_INDEX_URL + "history/forcedeletebypage/" + page_id);
}

function dataURItoBlob(dataURI){
    var byteString = atob(dataURI.split(',')[1]);

    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++)
    {
        ia[i] = byteString.charCodeAt(i);
    }

    var bb = new Blob([ab], { "type": mimeString });
    return bb;
}
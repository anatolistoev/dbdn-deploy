/**
 * Content Web Service
 */

QUnit.module("Test REST Download Cart Web Services", {
	setup: function() {
		// prepare something for all following tests
		jQuery.ajaxSetup({timeout: 5000});

		jQuery(document).ajaxStart(function() {
			ok(true, "ajaxStart");
		}).ajaxStop(function() {
			ok(true, "ajaxStop");
			QUnit.start();
		}).ajaxSend(function() {
			ok(true, "ajaxSend");
		}).ajaxComplete(function(event, request, settings) {
			ok(true, "ajaxComplete: " + request.responseText);
		}).ajaxError(function() {
			ok(false, "ajaxError");
		}).ajaxSuccess(function() {
			ok(true, "ajaxSuccess");
		});
	},
	teardown: function() {
		// clean up after each test
		jQuery(document).unbind('ajaxStart');
		jQuery(document).unbind('ajaxStop');
		jQuery(document).unbind('ajaxSend');
		jQuery(document).unbind('ajaxComplete');
		jQuery(document).unbind('ajaxError');
		jQuery(document).unbind('ajaxSuccess');
	}
});

QUnit.test("DBDN REST add item to cart - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
    var user = createUser();
	var dirname = {dirname:UNIT_TEST_DIR, path: "/"};
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");

	insertFile(SERVER_INDEX_URL + "files/createfile", function(file) {

		var cart_item = {user_id : user.id,file_id : file.response.id}

		// expected data
		var expected_result = jQuery.extend({"error":false, "message": "Cart Item was added.", "response": null}, {"response": cart_item});

		// url string
		var urlREST = SERVER_INDEX_URL + "cart/additemtocart";

		jQuery.ajax({
			type: "POST",
			url: urlREST, // URL of the REST web service
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(cart_item),
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			}, // error
			// script call was successful
			// data contains the JSON values returned by the Perl script
			success: function(data) {
				if (data.error) { // script returned error
					assert.ok(false, "error");
				} // if
				else { // login was successful
					expected_result.response.created_at = data.response.created_at;
					expected_result.response.updated_at = data.response.updated_at;

					assert.deepEqual(data, expected_result, "Cart Item was added.");
				} //else
			}, // success
			complete: function(data){
				deleteCartItem(user.id, file.response.id);
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + user.id);
				deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/" + file.response.id);
				deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads",'/'+dirname.dirname+'/');
				logoutUser();
			} // always
		  });
	 });
});

QUnit.test("DBDN REST Add Item to Cart - file_id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();

	var cart_item = {user_id : 1}

	// expected data
	var expected_result = {"error": true, "message": "Parameter file_id is required!"};

    // url string
	var urlREST =  SERVER_INDEX_URL + "cart/additemtocart";

    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(cart_item),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Cart add required");
			logoutUser();
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Page update result");
          } // if
          else { // login was successful
        	assert.ok(false, "Cart add file id check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST remove item to cart - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
    var user = createUser();
	var dirname = {dirname:UNIT_TEST_DIR, path: "/"};
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");

	insertFile(SERVER_INDEX_URL + "files/createfile", function(file) {

		var cart_item = {user_id : user.id,file_id : file.response.id}

		cart_item = addCartItem(cart_item.user_id,cart_item.file_id);

		// expected data
		var expected_result = jQuery.extend({"error":false, "message": "Cart item was deleted!", "response": null}, {"response": cart_item});
		// url string
		var urlREST = SERVER_INDEX_URL + "cart/removecartitem";

		jQuery.ajax({
			type: "DELETE",
			url: urlREST, // URL of the REST web service
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(cart_item),
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			}, // error
			// script call was successful
			// data contains the JSON values returned by the Perl script
			success: function(data) {
				if (data.error) { // script returned error
					assert.ok(false, "error");
				} // if
				else { // login was successful
					expected_result.response.active = data.response.active;
					assert.deepEqual(data, expected_result, "Cart Item was deleted!");
				} //else
			}, // success
			complete: function(data){
				deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file.response.id);
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
				deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads",'/'+dirname.dirname+'/');
				logoutUser();
			} // always
		  });
	 });
});

QUnit.test("DBDN REST get cart items - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
    var user = createUser();
	var dirname = {dirname:UNIT_TEST_DIR, path: "/"};
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");

	insertFile(SERVER_INDEX_URL + "files/createfile", function(file) {

		var cart_item = {user_id : user.id,file_id : file.response.id}
		var user_id = cart_item.user_id;

		cart_item = addCartItem(cart_item.user_id,cart_item.file_id);

		cart_item  = jQuery.extend(cart_item,file.response);
		cart_item  = jQuery.extend(cart_item,{"brand_id": 0,"category_id": 0, type:'Downloads', protected : 0, "user_edit_id": null, deleted: 0, thumb_visible: 0, visible: 0});

		delete cart_item.user_id;
		delete cart_item.file_id;

	    var expected_data = [cart_item];
		var expected_result = jQuery.extend({"error":false, "message": "List of files found.", "response": null}, {"response": expected_data});
		// url string
		var urlREST = SERVER_INDEX_URL + "cart/cartitems/"+user_id;

		jQuery.ajax({
			type: "GET",
			url: urlREST, // URL of the REST web service
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			}, // error
			// script call was successful
			// data contains the JSON values returned by the Perl script
			success: function(data) {
				if (data.error) { // script returned error
					assert.ok(false, "error");
				} // if
				else { // login was successful
					expected_result.response.created_at = data.response.created_at;
					expected_result.response.updated_at = data.response.updated_at;
					expected_result.response[0].description = data.response[0].description;
					expected_result.response[0].title = data.response[0].title;

					assert.deepEqual(data, expected_result, "List of files found.");
				} //else
			}, // success
			complete: function(data){
				deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file.response.id);
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
				deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads",'/'+dirname.dirname+'/');
				logoutUser();
			} // always
		  });
	 });
});

QUnit.test("DBDN REST Empty cart - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
    var user = createUser();
	var dirname = {dirname:UNIT_TEST_DIR, path: "/"};
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");

	insertFile(SERVER_INDEX_URL + "files/createfile", function(file) {

		var cart_item = {user_id : user.id,file_id : file.response.id}

		cart_item = addCartItem(cart_item.user_id,cart_item.file_id);

	    var expected_data = [cart_item];
		var expected_result = jQuery.extend({"error":false, "message": "Cart empty was successfull!", "response": null}, {"response": expected_data});
		// url string
		var urlREST = SERVER_INDEX_URL + "cart/emptycart/"+cart_item.user_id;

		jQuery.ajax({
			type: "DELETE",
			url: urlREST, // URL of the REST web service
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			}, // error
			// script call was successful
			// data contains the JSON values returned by the Perl script
			success: function(data) {
				if (data.error) { // script returned error
					assert.ok(false, "error");
				} // if
				else { // login was successful
					expected_result.response[0].active = data.response[0].active;
					assert.deepEqual(data, expected_result, "List of files found.");
				} //else
			}, // success
			complete: function(data){
				deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file.response.id);
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
				deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads",'/'+dirname.dirname+'/');
				logoutUser();
			} // always
		  });
	 });
});

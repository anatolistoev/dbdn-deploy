/**
 * Content Web Service
 */

QUnit.module("Test REST Web Services for FE Injections", {
	setup: function() {
		// prepare something for all following tests
		jQuery.ajaxSetup({timeout: 5000});

		jQuery(document).ajaxStart(function() {
			ok(true, "ajaxStart");
		}).ajaxStop(function() {
			ok(true, "ajaxStop");
			QUnit.start();
		}).ajaxSend(function() {
			ok(true, "ajaxSend");
		}).ajaxComplete(function(event, request, settings) {
			ok(true, "ajaxComplete: " + request.responseText);
		}).ajaxError(function() {
			ok(false, "ajaxError");
		}).ajaxSuccess(function() {
			ok(true, "ajaxSuccess");
		});
	},
	teardown: function() {
		// clean up after each test
		jQuery(document).unbind('ajaxStart');
		jQuery(document).unbind('ajaxStop');
		jQuery(document).unbind('ajaxSend');
		jQuery(document).unbind('ajaxComplete');
		jQuery(document).unbind('ajaxError');
		jQuery(document).unbind('ajaxSuccess');
	}
});

QUnit.test("DBDN help form - wrong messageText, name and email", 8, function(assert) {
    QUnit.stop();

	var data = {'messageText': '<div> Message </div>', 'name': '<p> NAME </p>', 'email': 'rwerwerwer', 'captcha': 'rwer'};
    // url string
    var urlREST = SERVER_BASE_URL +  "dialog/home?lang=en";
    jQuery.ajax({
        type: "POST",
		async: false,
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify(data),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data) {
            // data is HTML
            if (data.search('<div id="messageText_wrapper" class="error">') !== -1
                && data.search('Links can not be send via the help form') !== -1) { // script returned error
                assert.ok(true, "messageText handled");
            } // if
            else { // login was successful
                assert.ok(false, "messageText handled");
            } //else

            if (data.search('class="error" name="name"') !== -1
                && data.search("Invalid name. Only a-z, A-Z, '-', '’' symbols allowed.") !== -1) { // script returned error
                assert.ok(true, "name handled");
            } // if
            else { // login was successful
                assert.ok(false, "name handled");
            } //else

            if (data.search('class="error" name="email"') !== -1
                && data.search('The email format is invalid') !== -1) { // script returned error
                assert.ok(true, "email handled");
            } // if
            else { // login was successful
                assert.ok(false, "email handled");
            } //else

        }, // success
        complete: function(data){

        } // always
      });
});

QUnit.test("DBDN registration form - wrong all fields", 19, function(assert) {
    QUnit.stop();

	var data = {'username': '<div>username</div>', 'email': '<p> mail </p>', 'password': '<p>Pass55</p>', 'password_confirmation': '<p>Pass55</p>',
    first_name: '<div> Name </div>', last_name:'<div> Last name </div>', company:'<div>Company</div>', department:'<div>Deprarmnet</div>',
    position:'<div>Position</div>', address:'<div>Address street</div>', code: '<div>123</div>', city:'<div>Sofia</div>',  country: '<div>Italy</div>',
    phone: '<div>0247962</div>', fax:'<div>2478522</div>', agreed:'on', captcha: '47455647'};
    // url string
    var urlREST = SERVER_BASE_URL +  "registration?lang=en";
    jQuery.ajax({
        type: "POST",
		async: false,
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify(data),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data) {
            // data is HTML
            if (data.search('<div id="notification"  class="red" >') !== -1) { // script returned error
                assert.ok(true, "messageText handled");
            }
            else {
                assert.ok(false, "messageText not handled");
            }
            if (data.search('class="error input__field input__field--yoshiko" name="username"') !== -1
                && data.search('Invalid username: only a-z, A-Z, 0-9, ') !== -1) { // script returned error
                assert.ok(true, "username handled");
            }
            else {
                assert.ok(false, "username not handled");
            }

            if (data.search('class="error input__field input__field--yoshiko" name="email"') !== -1
                && data.search('The email format is invalid.') !== -1) { // script returned error
                assert.ok(true, "email handled");
            }
            else {
                assert.ok(false, "email not handled");
            }
//            if (data.search('class="error input__field input__field--yoshiko" name="password"') !== -1
//                && data.search('Invalid password:') !== -1) { // script returned error
//                assert.ok(true, "password handled");
//            }
//            else {
//                assert.ok(false, "password not handled");
//            }
            if (data.search('class="error input__field input__field--yoshiko" name="first_name"') !== -1
                && data.search("Invalid first name.") !== -1) { // script returned error
                assert.ok(true, "first_name handled");
            }
            else {
                assert.ok(false, "first_name not handled");
            }
            if (data.search('class="error input__field input__field--yoshiko" name="last_name"') !== -1
                && data.search("Invalid last name.") !== -1) { // script returned error
                assert.ok(true, "last_name handled");
            }
            else {
                assert.ok(false, "last_name not handled");
            }
            if (data.search('class="error input__field input__field--yoshiko" name="company"') !== -1
                && data.search("Invalid company.") !== -1) { // script returned error
                assert.ok(true, "company handled");
            }
            else {
                assert.ok(false, "company not handled");
            }
            if (data.search('class="error input__field input__field--yoshiko" name="department"') !== -1
                && data.search("Invalid department.") !== -1) { // script returned error
                assert.ok(true, "department handled");
            }
            else {
                assert.ok(false, "department not handled");
            }
            if (data.search('class="error input__field input__field--yoshiko" name="position"') !== -1
                && data.search("Invalid position.") !== -1) { // script returned error
                assert.ok(true, "position handled");
            }
            else {
                assert.ok(false, "position not handled");
            }
            if (data.search('class="error input__field input__field--yoshiko" name="address"') !== -1
                && data.search("Invalid address.") !== -1) { // script returned error
                assert.ok(true, "address handled");
            }
            else {
                assert.ok(false, "address not handled");
            }
            if (data.search('class="error input__field input__field--yoshiko" name="code"') !== -1
                && data.search("Invalid code.") !== -1) { // script returned error
                assert.ok(true, "code handled");
            }
            else {
                assert.ok(false, "code not handled");
            }
            if (data.search('class="error input__field input__field--yoshiko" name="city"') !== -1
                && data.search("Invalid city.") !== -1) { // script returned error
                assert.ok(true, "city handled");
            }
            else {
                assert.ok(false, "city not handled");
            }
            if (data.search('class="error input__field input__field--yoshiko" name="country"') !== -1
                && data.search('Invalid country.') !== -1) { // script returned error
                assert.ok(true, "country handled");
            }
            else {
                assert.ok(false, "country not handled");
            }
            if (data.search('class="error input__field input__field--yoshiko" name="phone"') !== -1
                && data.search('Invalid phone.') !== -1) { // script returned error
                assert.ok(true, "phone handled");
            }
            else {
                assert.ok(false, "phone not handled");
            }
            if (data.search('class="error input__field input__field--yoshiko" name="fax"') !== -1
                && data.search('Invalid fax.') !== -1) { // script returned error
                assert.ok(true, "fax handled");
            }
            else {
                assert.ok(false, "fax not handled");
            }

        }, // success
        complete: function(data){

        } // always
      });
});

QUnit.test("DBDN Request access to protected file form - wrong email, reason and contact name", 9, function(assert) {
    QUnit.stop();
	loginUser();
	var page = {parent_id: 1, slug: "Test_Page", active: 0, position: 1, in_navigation: 1, authorization: 1, home_accordion: 0, home_block: 0,
		visits: 1, overview: 0, template: "basic", langs: 3, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, "file_type": null,"is_visible": 1,
        "usage": null, "version": 0, access : [{group_id: "4"}]};
    var dbPage = insertObject(page, SERVER_INDEX_URL + "pages/create");
    page.id = dbPage.id;
    page.u_id = dbPage.u_id;
    var workflow = dbPage.workflow;
    workflow.editing_state = 'Approved';
    workflow.user_responsible = 2;
    workflow = insertObject(workflow, SERVER_INDEX_URL + "workflow/update", "PUT");
    var content = [{body: 'Title1', format: 1,lang: 1, page_id:page.id, page_u_id: page.u_id, position:0, searchable: 1, section: 'title'},
    {body: 'Title1', format: 1,lang: 2, page_id:page.id, page_u_id: page.u_id, position:0, searchable: 1, section: 'title'},
    {body:	'<h1>test </h1>',format: 1,lang: 1, page_id:page.id, page_u_id: page.u_id, position:0, searchable: 1, section: 'main'},
    {body:	'<h1>test </h1>',format: 1,lang: 2, page_id:page.id, page_u_id: page.u_id, position:0, searchable: 1, section: 'main'}
    ];
    content = insertObject(content, SERVER_INDEX_URL + "contents/create-many", "POST");

    page.active = 1;
    insertObject(page, SERVER_INDEX_URL + "pages/update", "PUT");
    var user = {username: "test_user_esof",password:"P1!as123sword",password_confirmation:"P1!as123sword",first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",last_login:"2014-01-14 16:46:45", logins: 15,
		role:0,newsletter:2,force_pwd_change:2, group : [] , active: 1};
    user = insertObject(user, SERVER_INDEX_URL + "user/create");
    logoutUser();
    var logindata = {username:"test_user_esof" , password:"P1!as123sword", __unitTests : "1"}
    loginUser(logindata);

	var data = {'rbChoice':0, 'text_area': '<div> Message in text area </div>', 'contact_name': '<p> NAME </p>', 'contact_email': '<p>email</p>', 'agreed':1, 'captcha': 'rwer'};
    // url string
    //var displaypage = insertObject({}, SERVER_BASE_URL +page.u_id + "?lang=en", "GET");
    var urlREST = SERVER_BASE_URL +  "requestaccess/"+page.u_id + "?lang=en";
    jQuery.ajax({
        type: "POST",
		async: false,
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify(data),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        },
        success: function(data) {
            // data is HTML
            if (data.search('<div id="notification"  class="red" >') !== -1) { // script returned error
                assert.ok(true, "messageText handled");
            }
            else {
                assert.ok(false, "messageText not handled");
            }
            if (data.search('name="text_area"> Message in text area </textarea>') !== -1) {
                assert.ok(true, "reason handled");
            }
            else {
                assert.ok(false, "reason not handled");
            }
            if (data.search('value=" NAME "') !== -1) {
                assert.ok(true, "contact person handled");
            }
            else {
                assert.ok(false, "contact not person handled");
            }
            if (data.search(' error " type="text" name="contact_email"')!== -1
                && data.search('Please ensure that you have entered a valid e-mail address.') !== -1) { // script returned error
                assert.ok(true, "email handled");
            }
            else { // login was successful
                assert.ok(false, "email not handled");
            }
        },
        complete: function(data){
            logoutUser();
            loginUser();
            if(content[0].id)
				deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + content[0].id);
            if(content[1].id)
				deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + content[1].id);
            if(content[2].id)
				deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + content[2].id);
            if(content[3].id)
				deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + content[3].id);
            deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + user.id);
            deleteAccess(page.u_id);
            forceDeletePage(page.u_id);
            logoutUser();
        }
      });
});
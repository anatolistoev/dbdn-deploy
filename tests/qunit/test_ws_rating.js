/**
 * Rating Web Service
 */

QUnit.module("Test REST Rating Web Services", {
	setup: function() {
		// prepare something for all following tests
		jQuery.ajaxSetup({timeout: 5000});

		jQuery(document).ajaxStart(function() {
			ok(true, "ajaxStart");
		}).ajaxStop(function() {
			ok(true, "ajaxStop");
			QUnit.start();
		}).ajaxSend(function() {
			ok(true, "ajaxSend");
		}).ajaxComplete(function(event, request, settings) {
			ok(true, "ajaxComplete: " + request.responseText);
		}).ajaxError(function() {
			ok(false, "ajaxError");
		}).ajaxSuccess(function() {
			ok(true, "ajaxSuccess");
		});
	},
	teardown: function() {
		// clean up after each test
		jQuery(document).unbind('ajaxStart');
		jQuery(document).unbind('ajaxStop');
		jQuery(document).unbind('ajaxSend');
		jQuery(document).unbind('ajaxComplete');
		jQuery(document).unbind('ajaxError');
		jQuery(document).unbind('ajaxSuccess');
	}
});

QUnit.test("DBDN REST Create Rating - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
    var user = createUser();
	var page = createAndActivatePage();

	var rating = {user_id : user.id,page_id : page.id, vote : 7};

	// expected data
	var expected_result = jQuery.extend({"error":false, "message": "Rating was added.", "response": null}, {"response": rating});

	// url string
	var urlREST = SERVER_INDEX_URL + "rating/create";

	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(rating),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Rating was added.");
			} //else
		}, // success
		complete: function(data){
			deleteRating(user.id, page.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
			forceDeletePage(page.u_id);
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Create Rating - page_id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var rating = {}

	// expected data
	var expected_result = {"error": true, "message": "Parameter page_id is required!"};

    // url string
	var urlREST =  SERVER_INDEX_URL + "rating/create";

    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(rating),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Rating create required");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Rating create result");
          } // if
          else { // login was successful
        	assert.ok(false, "Rating add user id check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});


QUnit.test("DBDN REST Remove Rating - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
    var user = createUser();
	var page = createAndActivatePage();

	var rating = {user_id : user.id,page_id : page.id, vote : 7};
	rating = insertObject(rating, SERVER_INDEX_URL + "rating/create");

	var expected_data = rating;
	var expected_result = jQuery.extend({"error":false, "message": "Rating was deleted!", "response": null}, {"response": expected_data});
	// url string
	var urlREST = SERVER_INDEX_URL + "rating/delete";

		jQuery.ajax({
			type: "DELETE",
			url: urlREST, // URL of the REST web service
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(rating),
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			}, // error
			// script call was successful
			// data contains the JSON values returned by the Perl script
			success: function(data) {
				if (data.error) { // script returned error
					assert.ok(false, "error");
				} // if
				else { // login was successful
					expected_result.response.created_at = data.response.created_at;
					expected_result.response.updated_at = data.response.updated_at;
					assert.deepEqual(data, expected_result, "Rating delete");
				} //else
			}, // success
			complete: function(data){
                deleteRating(user.id, page.id);
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
				forceDeletePage(page.u_id);
				logoutUser();
			} // always
		  });
});

QUnit.test("DBDN REST Rating delete - page_id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var rating = {}

	// expected data
	var expected_result = {"error": true, "message": "Parameter page_id is required!"};

    // url string
	var urlREST =  SERVER_INDEX_URL + "rating/delete";

    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(rating),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Rating delete required");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Rating delete result");
          } // if
          else { // login was successful
        	assert.ok(false, "Rating delete user id check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Rating delete - not found", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var rating = {user_id : 3, page_id : 3}

	// expected data
	var expected_result = {"error": true, "message": "Rating not found."};

    // url string
	var urlREST =  SERVER_INDEX_URL + "rating/delete";

    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(rating),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Rating delete not found");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Rating delete not found result");
          } // if
          else { // login was successful
        	assert.ok(false, "Rating delete not found  check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Modify Rating - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
    var user = createUser();
	var page = createAndActivatePage();

	var rating = {user_id : user.id,page_id : page.id, vote : 7};
	rating = insertObject(rating, SERVER_INDEX_URL + "rating/create");

	var update_rating = {user_id : user.id,page_id : page.id, vote : 8};

	var expected_data = update_rating;
	var expected_result = jQuery.extend({"error":false, "message": "Rating was updated.", "response": null}, {"response": expected_data});
	// url string
	var urlREST = SERVER_INDEX_URL + "rating/modify";

	jQuery.ajax({
		type: "PUT",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(update_rating),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Rating update");
			} //else
		}, // success
		complete: function(data){
			deleteRating(user.id, page.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
			forceDeletePage(page.u_id);
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Get Rating - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
    var user = createUser();
	var page = createAndActivatePage();

	var rating = {user_id : user.id,page_id : page.id, vote : 7};
	rating = insertObject(rating, SERVER_INDEX_URL + "rating/create");

	var expected_data = [rating.vote];
	delete rating.vote;

	var expected_result = jQuery.extend({"error":false, "message": "Rating found.", "response": null}, {"response": expected_data});
	// url string
	var urlREST = SERVER_INDEX_URL + "rating/read?page_id="+page.id+'&user_id='+user.id;

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Rating read");
			} //else
		}, // success
		complete: function(data){
			deleteRating(user.id, page.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
			forceDeletePage(page.u_id);
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Rating read - not found", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var rating = {user_id : 3, page_id : 3}

	// expected data
	var expected_result = {"error": true, "message": "Rating not found."};

    // url string
	var urlREST =  SERVER_INDEX_URL + "rating/read?page_id="+rating.page_id+'&user_id='+rating.user_id;

    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Rating read not found");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Rating read not found result");
          } // if
          else { // login was successful
        	assert.ok(false, "Rating read not found  check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Rating avg read - not found", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var rating = {user_id : 3, page_id : 3}

	// expected data
	var expected_result = {"error": true, "message": "Rating not found."};

    // url string
	var urlREST =  SERVER_INDEX_URL + "rating/readavg/"+rating.page_id;

    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Rating read not found");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Rating read not found result");
          } // if
          else { // login was successful
        	assert.ok(false, "Rating read not found  check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});


QUnit.test("DBDN REST Get Avg Rating - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
	var user1 = createUser();

	var user2 = createUser('1234');

	var user3 = createUser('1122');

	var page = createAndActivatePage();

	var rating1 = {user_id : user1.id,page_id : page.id, vote : 8};
	rating1 = insertObject(rating1, SERVER_INDEX_URL + "rating/create");

	var rating2 = {user_id : user2.id,page_id : page.id, vote : 7};
	rating2 = insertObject(rating2, SERVER_INDEX_URL + "rating/create");

	var rating3 = {user_id : user3.id,page_id : page.id, vote : 7};
	rating3 = insertObject(rating3, SERVER_INDEX_URL + "rating/create");

	var expected_data = [7.33];

	var expected_result = jQuery.extend({"error":false, "message": "Average Rating.", "response": null}, {"response": expected_data});
	// url string
	var urlREST = SERVER_INDEX_URL + "rating/readavg/"+page.id;

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "AAvergae Rating.");
			} //else
		}, // success
		complete: function(data){
			deleteRating(user1.id, page.id);
			deleteRating(user2.id, page.id);
			deleteRating(user3.id, page.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user1.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user2.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user3.id);
			forceDeletePage(page.u_id);
			logoutUser();
		} // always
	});
});
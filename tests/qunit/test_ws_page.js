/**
 * Page Web Service
 */

QUnit.module("Test REST Page Web Services", {
	setup: function () {
		// prepare something for all following tests
		jQuery.ajaxSetup({timeout: 5000});

		jQuery(document).ajaxStart(function () {
			ok(true, "ajaxStart");
		}).ajaxStop(function () {
			ok(true, "ajaxStop");
			QUnit.start();
		}).ajaxSend(function () {
			ok(true, "ajaxSend");
		}).ajaxComplete(function (event, request, settings) {
			ok(true, "ajaxComplete: " + request.responseText);
		}).ajaxError(function () {
			ok(false, "ajaxError");
		}).ajaxSuccess(function () {
			ok(true, "ajaxSuccess");
		});
	},
	teardown: function () {
		// clean up after each test
		jQuery(document).unbind('ajaxStart');
		jQuery(document).unbind('ajaxStop');
		jQuery(document).unbind('ajaxSend');
		jQuery(document).unbind('ajaxComplete');
		jQuery(document).unbind('ajaxError');
		jQuery(document).unbind('ajaxSuccess');
	}
});

QUnit.test("DBDN REST Register new Page - success", 6, function (assert) {
	QUnit.stop();
	loginUser();
	// insert test data
	var new_page = {parent_id: 0, slug: "Test_Page", type: "File", active: 0, position: 1, in_navigation: 1, authorization: 1, home_accordion: 0, home_block: 0,
		visits: 1, overview: 1, template: "Template 1", langs: 2, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, "file_type": null,"is_visible": 1,"usage": null, "version": 0,};

	// expected data
	var expected_page = jQuery.extend({u_id:2, id: 2, created_by: 2, created_at: "", updated_at: "", restricted: 0,  assigned_by: "",
    "due_date": "No Due Date", "editing_state": "Draft"}, new_page);
	var expected_result = jQuery.extend({"error": false, "message": "Page was added.", "response": null}, {"response": expected_page});

	// url string
	var urlREST = SERVER_INDEX_URL + "pages/create";

	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(new_page),
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function (data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_page.u_id = data.response.u_id;
				expected_result.response.id = data.response.id;
                expected_result.response.u_id = data.response.u_id;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
                expected_result.response.workflow = data.response.workflow;
				//delete expected_result.response.access;
				assert.deepEqual(data, expected_result, "Page add result");
			} //else
		}, // success
		complete: function (data) {
			// delete test data
			if (expected_page.u_id) {
                forceDeletePage(expected_page.u_id);
			}
			logoutUser();
		} // always
	});

});

QUnit.test("DBDN REST Update Page - success", 6, function (assert) {
	QUnit.stop();
	loginUser();
	// test fields
	var update_page = {parent_id: 2, slug: "Test_Update_Page", "featured_menu": 0, "file_type": null,"type": "File", "front_block": 0,"is_visible": 1, active: 0, position: 1, in_navigation: 1, authorization: 1, home_accordion: 0, home_block: 0,
		visits: 1, template: "Template Update 3", langs: 2, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, overview: 1, publish_date: "0000-00-00 00:00:00", unpublish_date: "0000-00-00 00:00:00"};

	// insert test data
	var page = {parent_id: 2, slug: "Test_Update_Page", type: "File", active: 1, position: 1, in_navigation: 1, authorization: 0, home_accordion: 0, home_block: 0,
		visits: 1, template: "Template Update 1", langs: 2, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, overview: 0, access: [], publish_date: "0000-00-00 00:00:00", unpublish_date: "0000-00-00 00:00:00"};
	var pageid = insertPage(page);
	update_page = jQuery.extend(update_page, {id: pageid, u_id:pageid});

	// expected data
	var expected_page = update_page;
    //parasite prop
    expected_page = jQuery.extend({assigned_by: "", "due_date": "No Due Date", "editing_state": "Draft", "end_edit": null,
        "restricted": 0, "usage": null, "user_edit_id": null, "version": 0,"visits_search": 0,},expected_page);
	var expected_result = jQuery.extend({"error": false, "message": "Page was updated.", "response": null}, {"response": expected_page});

	// url string
	var urlREST = SERVER_INDEX_URL + "pages/update";

	jQuery.ajax({
		type: "PUT",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(update_page),
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function (data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.created_by = data.response.created_by;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;

                expected_result.response.workflow = data.response.workflow;
				//delete expected_result.response.access;
				assert.deepEqual(data, expected_result, "Page update result");
			} //else
		}, // success
		complete: function (data) {
			// delete test data
			if (page.id) {
				forceDeletePage(page.u_id);
			}
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Update Page - id required", 6, function (assert) {
	QUnit.stop();
	jQuery(document).unbind('ajaxError');
	loginUser();
	// test fields
	var update_page = {};

	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required."};

	// url string
	var urlREST = SERVER_INDEX_URL + "pages/update";

	jQuery.ajax({
		type: "PUT",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(update_page),
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Page update result");
			logoutUser();
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function (data) {
			if (data.error) { // script returned error
				assert.deepEqual(data, expected_result, "Page update result");
			} // if
			else { // login was successful
				assert.ok(false, "Page update ID check fail!");
			} //else
		} // success
	});
});

QUnit.test("DBDN REST Update Page - not found", 6, function (assert) {
	QUnit.stop();
	jQuery(document).unbind('ajaxError');
	loginUser();
	// test fields
	var update_page = {id: 9999999, u_id: 9999999, parent_id: 2, slug: "Test Update Page", type: "File", active: 1, position: 1, in_navigation: 1, authorization: 0, home_accordion: 0, home_block: 0,
		visits: 1, template: "Template Update 1", langs: 2, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, overview: 0, access: [{"group_id": 2}, {"group_id": 3}]};

	// expected data
	var expected_result = {"error": true, "message": "Page not found."};

	// url string
	var urlREST = SERVER_INDEX_URL + "pages/update";

	jQuery.ajax({
		type: "PUT",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(update_page),
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Page update result");
			logoutUser();
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function (data) {
			if (data.error) { // script returned error
				assert.deepEqual(data, expected_result, "Page update result");
			} // if
			else { // login was successful
				assert.ok(false, "Page update unexisting page check fail!");
			} //else
			logoutUser();
		} // success
	});
});

QUnit.test("DBDN REST Get List of Pages - success", 6, function (assert) {
	// GET /pages/all
	QUnit.stop();
	loginUser();
	// insert test data
	var page1 = {parent_id: 1, slug: "Test_Parent_Page", type: "File", active: 0, position: 1, in_navigation: 1, home_accordion: 0, home_block: 0,
		visits: 1, template: "Template Update 1", langs: 2, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, overview: 0, publish_date: "0000-00-00 00:00:00", unpublish_date: "0000-00-00 00:00:00"};
	page1.u_id = insertPage(page1);
    page1.id = page1.u_id

	var page = {parent_id: page1.id, slug: "Test_Update_Page", type: "File", active: 0, position: 1, in_navigation: 1, home_accordion: 0, home_block: 0,
		visits: 1, template: "Template Update 1", langs: 2, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, overview: 0,publish_date: "0000-00-00 00:00:00",
        unpublish_date: "0000-00-00 00:00:00", "featured_menu": 0, "file_type": null, "front_block": 0,"is_visible": 1,"usage": null, "user_edit_id": null,
        "version": 0, "visits_search": 0,};
	page.id = insertPage(page);
    page.u_id = page.id;
	page = jQuery.extend(page, {level: 1, path: '//', view_pos: 1, "authorization": 0, isChildless: true, "editing_state": "Draft",
      "end_edit": null, "created_by": 2,});

	// expected data
	var expected_page = [page];
	var expected_result = jQuery.extend({"error": false, "message": "List of pages found.", "response": null}, {"response": expected_page});

	// url string
	var urlREST = SERVER_INDEX_URL + "pages/all?parent_id=" + page1.id + "&lang=0&depth=2";

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function (data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				//assert.notDeepEqual(data, expected_result, "Page list result");
				//check (error, message) - strict; response - must be array
				delete data.response[0].workflow;
				//delete expected_result.response[0].restricted;
                expected_result.response[0].created_at = data.response[0].created_at;
                expected_result.response[0].updated_at = data.response[0].updated_at;
                expected_result.response[0].draft = data.response[0].draft;
				assert.deepEqual(data, expected_result, "Page list result");
			} //else
		}, // success
		complete: function (data) {
			// delete test data
            if(page1){
                forceDeletePage(page1.u_id);
            }
			if (page.id) {
                forceDeletePage(page.u_id);
			}
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Get Page by ID - success", 6, function (assert) {

	QUnit.stop();
	loginUser();
	// insert test data
	var page = {parent_id: 2, slug: "Test_Update_Page", type: "File", active: 0, position: 1, in_navigation: 1, authorization: 1, home_accordion: 0, home_block: 0,
		visits: 1, template: "Template Update 1", langs: 2, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, overview: 0,  "access": [], publish_date: "0000-00-00 00:00:00", unpublish_date: "0000-00-00 00:00:00",
        "featured_menu": 0, "front_block": 0, "user_edit_id": null,"visits_search": 0,  "end_edit": null, "file_type": null,"is_visible": 1, "usage": null, version:0};
	page.id = insertPage(page);
    page.u_id = page.id;

	// expected data
	var expected_page = jQuery.extend(page, {"tags": []});
	var expected_result = jQuery.extend({"error": false, "message": "Page found.", "response": null}, {"response": expected_page});

	// test fields
	var page_id = page.id;

	// url string
	var urlREST = SERVER_INDEX_URL + "pages/read/" + page_id;

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function (data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.created_by = data.response.created_by;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Page read result");
			} //else
		}, // success
		complete: function (data) {
			// delete test data
			if (page.u_id) {
				forceDeletePage(page.u_id);
			}
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Get Page by ID - success 1", 6, function (assert) {

	QUnit.stop();
	loginUser();
	// insert test data
	var page = {parent_id: 2, slug: "Test_Update_Page", type: "File", active: 0, position: 1, in_navigation: 1, authorization: 1, home_accordion: 0, home_block: 0,
		visits: 1, template: "Template Update 1", langs: 2, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, overview: 0,  "access": [{"group_id": 2}, {"group_id": 3}], publish_date: "0000-00-00 00:00:00", unpublish_date: "0000-00-00 00:00:00",
        "featured_menu": 0, "front_block": 0, "user_edit_id": null,"visits_search": 0,  "end_edit": null, "file_type": null,"is_visible": 1, "usage": null, version:0};
	page.id = insertPage(page);
    page.u_id = page.id;
    var read_page = page;

	// expected data
	var expected_page = read_page;
	expected_page = jQuery.extend(expected_page, {"tags": [],access: [{"group_id": 2, "page_id": read_page.id}, {"group_id": 3, "page_id": read_page.id}]});
	var expected_result = jQuery.extend({"error": false, "message": "Page found.", "response": null}, {"response": expected_page});

	// test fields
	var page_id = read_page.id;

	// url string
	var urlREST = SERVER_INDEX_URL + "pages/read/" + page_id + "/1";

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function (data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.created_by = data.response.created_by;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				delete data.response.workflow;
				assert.deepEqual(data, expected_result, "Page read result");
			} //else
		}, // success
		complete: function (data) {
			// delete test data
			if (read_page.u_id) {
				forceDeletePage(read_page.u_id);
			}
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Register new Page - page validate failed", 6, function (assert) {
	QUnit.stop();
	// Unregister error handler!
	jQuery(document).unbind('ajaxError');
	loginUser();

	// insert test data
	var new_page = {parent_id: 0, slug: "For a VARCHAR column that stores multi-byte characters, the effective maximum number of characters is less. For example, utf8 characters can require up to three bytes per character, so a VARCHAR column that uses the utf8 character set can be declared to be a maximum of 21,844 characters Regarding the storage required in the table itself, it looks like you are correct (because 1, 2, 3 and 4 correlate to the maximum number of bytes per field type: 256, 65K, 16M, 4G) but earlier in the document it talks about 9 to 12 bytes but that's clearly wrong - I just quoted that bit verbatim. I'll update the post to reflect that now.", type: "File", active: 1, position: 1, in_navigation: 1, authorization: 0,
		visits: 1, home_accordion: 0, home_block: 0, template: "Template 1", langs: 2, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, overview: 2, access: [{"group_id": 2}, {"group_id": 3}]};

	// expected data
	var expected_page = jQuery.extend({id: 2, created_at: "", updated_at: ""}, new_page);
	var expected_result = {"error": true, "message": "", "response": null};

	// url string
	var urlREST = SERVER_INDEX_URL + "pages/create";

	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(new_page),
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.equal(XMLHttpRequest.status, 400, "HTTP Status code 400.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			expected_page.id = data.response.id;
			expected_result.response = data.response;
			assert.deepEqual(data, expected_result, "Page add validate faild result");
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function (data) {
			if (data.error) { // script returned error
				expected_page.id = data.response.id;
				expected_result.response = data.response;
				assert.deepEqual(data, expected_result, "Page add validate faild result");
			} // if
			else { // login was successful
				assert.ok(false, "error");
			} //else
		}, // success
		complete: function (data) {
			// delete test data
			if (expected_page.id) {
				deleteObject(SERVER_INDEX_URL + "pages/delete/" + expected_page.id);
				deleteObject(SERVER_INDEX_URL + "pages/forcedelete/" + expected_page.id);
			}
            logoutUser();
		} // always
	});

});

QUnit.test("DBDN REST Get Page by ID - id required", 6, function (assert) {

	QUnit.stop();
	jQuery(document).unbind('ajaxError');
	loginUser();

	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required."};

	// url string
	var urlREST = SERVER_INDEX_URL + "pages/read";

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Page read result");
			logoutUser();
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function (data) {
			if (data.error) { // script returned error
				assert.deepEqual(data, expected_result, "Page read result");
			} // if
			else { // login was successful
				assert.ok(false, "Page read ID check fail!");
			} //else
			logoutUser();
		} // success
	});
});

QUnit.test("DBDN REST Get Page by ID - not found", 6, function (assert) {

	QUnit.stop();
	jQuery(document).unbind('ajaxError');
	loginUser();

	// expected data
	var expected_result = {"error": true, "message": "Page not found."};

	// url string
	var urlREST = SERVER_INDEX_URL + "pages/read/" + 9999999;

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Page read result");
			logoutUser();
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function (data) {
			if (data.error) { // script returned error
				assert.deepEqual(data, expected_result, "Page read result");
			} // if
			else { // login was successful
				assert.ok(false, "Page read unexisting page check fail!");
			}
			logoutUser();
		} // success
	});
});

QUnit.test("DBDN REST Delete Page - success", 6, function (assert) {
	QUnit.stop();
	loginUser();
	// Insert page for delete:
	var delete_page = {parent_id: 2, slug: "Test_Update_Page", type: "File", active: 0, position: 1, in_navigation: 1, authorization: 1, home_accordion: 0, home_block: 0,
		visits: 1, template: "Template Update 1", langs: 2, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, overview: 0, publish_date: "0000-00-00 00:00:00", unpublish_date: "0000-00-00 00:00:00",
        "featured_menu": 0, "front_block": 0, "user_edit_id": null,"visits_search": 0,  "end_edit": null, "file_type": null,"is_visible": 1, "usage": null, version:0};
	delete_page.id = insertPage(delete_page);
    delete_page.u_id = delete_page.id;

	// test fields
	var page_id_for_delete = delete_page.id;

	// expected data
	var expected_result = jQuery.extend({"error": false, "message": "Page was deleted.", "response": null}, {"response": delete_page});

	// url string
	var urlREST = SERVER_INDEX_URL + "pages/delete/" + page_id_for_delete;

	jQuery.ajax({
		type: "DELETE",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function (data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.created_by = data.response.created_by;
                expected_result.response.updated_at = data.response.updated_at;
                expected_result.response.created_at = data.response.created_at;
				assert.deepEqual(data, expected_result, "Page delete result");
			} //else
		}, // success
       complete: function (data) {
			// delete test data
			if (delete_page.u_id) {
				forceDeletePage(delete_page.u_id);
			}
            logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Delete Page - id required", 6, function (assert) {
	QUnit.stop();
	jQuery(document).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required."};

	// url string
	var urlREST = SERVER_INDEX_URL + "pages/delete";

	jQuery.ajax({
		type: "DELETE",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Page delete result");
			logoutUser();
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function (data) {
			if (data.error) { // script returned error
				assert.deepEqual(data, expected_result, "Page delete result");
			} // if
			else { // login was successful
				assert.ok(false, "Page delete ID check fail!");
			} //else
			logoutUser();
		}, // success
        complete: function (data) {
            logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Delete Page - not found", 6, function (assert) {
	QUnit.stop();
	jQuery(document).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Page not found."};

	// url string
	var urlREST = SERVER_INDEX_URL + "pages/delete/" + 3;

	jQuery.ajax({
		type: "DELETE",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Page delete result");
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function (data) {
			if (data.error) { // script returned error
				assert.deepEqual(data, expected_result, "Page delete result");
			} // if
			else { // login was successful
				assert.ok(false, "Page delete unexisting page check fail!");
			} //else
		}, // success
        complete: function (data) {
            logoutUser();
		} // always
	});
});


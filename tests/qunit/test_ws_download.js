/**
 * Download Web Service
 */

QUnit.module("Test REST Dwonload Web Services", {
	setup: function() {
		// prepare something for all following tests
		jQuery.ajaxSetup({timeout: 5000});

		jQuery(document).ajaxStart(function() {
			ok(true, "ajaxStart");
		}).ajaxStop(function() {
			ok(true, "ajaxStop");
			QUnit.start();
		}).ajaxSend(function() {
			ok(true, "ajaxSend");
		}).ajaxComplete(function(event, request, settings) {
			ok(true, "ajaxComplete: " + request.responseText);
		}).ajaxError(function() {
			ok(false, "ajaxError");
		}).ajaxSuccess(function() {
			ok(true, "ajaxSuccess");
		});
	},
	teardown: function() {
		// clean up after each test
		jQuery(document).unbind('ajaxStart');
		jQuery(document).unbind('ajaxStop');
		jQuery(document).unbind('ajaxSend');
		jQuery(document).unbind('ajaxComplete');
		jQuery(document).unbind('ajaxError');
		jQuery(document).unbind('ajaxSuccess');
	}
});

QUnit.test("DBDN REST create download - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
	// insert test data
	var dirname = {dirname:UNIT_TEST_DIR, path: "/"};
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");
	var page = createAndActivatePage();

	insertFile(SERVER_INDEX_URL + "files/createfile", function(file) {

		var download = {page_id : page.id, page_u_id: page.u_id, file_id : file.response.id, position : 1};
		var expected_result = jQuery.extend({"error":false, "message": "Download was added.", "response": null}, {"response": download});

		// url string
		var urlREST = SERVER_INDEX_URL + "download/createdownload";

		jQuery.ajax({
			type: "POST",
			url: urlREST, // URL of the REST web service
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(download),
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			}, // error
			// script call was successful
			// data contains the JSON values returned by the Perl script
			success: function(data) {
				if (data.error) { // script returned error
					assert.ok(false, "error");
				} // if
				else { // login was successful
					expected_result.response.created_at = data.response.created_at;
					expected_result.response.updated_at = data.response.updated_at;

					assert.deepEqual(data, expected_result, "Download was added");
				} //else
			}, // success
			complete: function(data){
				forceDeleteDownload(page.id);
				deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file.response.id);
				forceDeletePage(page.u_id);
				deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads",'/'+dirname.dirname+'/');
				logoutUser();
			} // always
		  });
	 });
});

QUnit.test("DBDN REST Add Item to Cart - file_id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	//insertUser();

	var download_item = {page_id : 1, page_u_id: 1};

	// expected data
	var expected_result = {"error": true, "message": "Parameter file_id is required!"};

    // url string
	var urlREST =  SERVER_INDEX_URL + "download/createdownload";

    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(download_item),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Download create file id required");
			logoutUser();
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
        	assert.ok(false, "Download create required file id check fail!");
        } // success
      });
});

QUnit.test("DBDN REST Download delete", 6, function( assert ) {
	// GET /pages/descedents
	QUnit.stop();
	loginUser();
	var dirname = {dirname:UNIT_TEST_DIR, path: "/"};
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");
	var page = createAndActivatePage();

	insertFile(SERVER_INDEX_URL + "files/createfile",function(file) {
		var dw = addDownload(page.id, page.u_id, file.response.id);
        var expected_result = jQuery.extend({"error": false, "message": 'Download was deleted!', "response": null}, {"response": dw});
        var urlREST =  SERVER_INDEX_URL + "download/removedownload";

            jQuery.ajax({
                type: "DELETE",
                url: urlREST, // URL of the REST web service
                contentType: "application/json; charset=utf-8",
                dataType:"json",
                data : JSON.stringify(dw),
                // script call was *not* successful
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    assert.ok(false, "error: " + textStatus + " : " + errorThrown);
                }, // error
                // script call was successful
                // data contains the JSON values returned by the Perl script
                success: function(data){
                    if (data.error) { // script returned error
                        assert.ok(true, "error");
                    } // if
                    else { // login was successful
                        expected_result.response.created_at = data.response.created_at;
                        expected_result.response.updated_at = data.response.updated_at;

                        assert.deepEqual(data, expected_result, "Download modfy");
                    } //else
                }, // success
                complete: function(data){
                    forceDeleteDownload(page.u_id);
                    forceDeletePage(page.u_id);
                    deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file.response.id);
                    deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads",'/'+dirname.dirname+'/');
                    logoutUser();
                } // always
            });
    });
});


QUnit.test("DBDN REST Modify download ", 6, function( assert ) {
	// GET /pages/descedents
	QUnit.stop();
	loginUser();
	var dirname = {dirname:UNIT_TEST_DIR, path: "/"};
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");
	var file1, file2, file3;
	var dw1, dw2, dw3;
	var page = createAndActivatePage();

	insertFile(SERVER_INDEX_URL + "files/createfile",function(file) {

		file1 = file;
		dw1 = addDownload(page.id, page.u_id, file1.response.id)
		insertFile(SERVER_INDEX_URL + "files/createfile",'file2',function(file) {

			file2 = file;
			dw2 = addDownload(page.id, page.u_id, file2.response.id);
			insertFile(SERVER_INDEX_URL + "files/createfile", 'file3',function(file) {

				file3 = file;
				dw3 = addDownload(page.id, page.u_id, file3.response.id);
				var urlREST =  SERVER_INDEX_URL + "download/modifydownload";

				dw3.position = dw1.position;
				//var expected_data = [dw2];
				var expected_result = jQuery.extend({"error": false, "message": 'Download was updated.', "response": null}, {"response": dw3});

				jQuery.ajax({
					type: "PUT",
					url: urlREST, // URL of the REST web service
					contentType: "application/json; charset=utf-8",
					dataType:"json",
					data : JSON.stringify(dw3),
					// script call was *not* successful
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						assert.ok(false, "error: " + textStatus + " : " + errorThrown);
					}, // error
					// script call was successful
					// data contains the JSON values returned by the Perl script
					success: function(data){
						if (data.error) { // script returned error
							assert.ok(true, "error");
						} // if
						else { // login was successful
							expected_result.response.created_at = data.response.created_at;
							expected_result.response.updated_at = data.response.updated_at;

							assert.deepEqual(data, expected_result, "Download modify max");
						} //else
					}, // success
					complete: function(data){
                        forceDeleteDownload(page.u_id);
                        forceDeletePage(page.u_id);
						deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file1.response.id);
						deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file2.response.id);
						deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file3.response.id);
						deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads",'/'+dirname.dirname+'/');
						logoutUser();
					} // always
				});
			});
		});
	});
});



QUnit.test("DBDN REST Modify download above max", 6, function( assert ) {
	QUnit.stop();
	loginUser();
	var dirname = {dirname:UNIT_TEST_DIR, path: "/"};
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");
	var file1, file2, file3;
	var dw1, dw2, dw3;
	var page = createAndActivatePage();

	insertFile(SERVER_INDEX_URL + "files/createfile",function(file) {

		file1 = file;
		dw1 = addDownload(page.id, page.u_id, file1.response.id)
		insertFile(SERVER_INDEX_URL + "files/createfile", 'file2',function(file) {

			file2 = file;
			dw2 = addDownload(page.id, page.u_id, file2.response.id);
			insertFile(SERVER_INDEX_URL + "files/createfile", 'file3', function(file) {

				file3 = file;
				dw3 = addDownload(page.id, page.u_id, file3.response.id);
				var urlREST =  SERVER_INDEX_URL + "download/modifydownload";

				dw1.position = 6;
				var expected_result = jQuery.extend({"error": false, "message": 'Download was updated.', "response": null}, {"response": dw1});

				jQuery.ajax({
					type: "PUT",
					url: urlREST, // URL of the REST web service
					contentType: "application/json; charset=utf-8",
					dataType:"json",
					data : JSON.stringify(dw1),
					// script call was *not* successful
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						assert.ok(false, "error: " + textStatus + " : " + errorThrown);
					}, // error
					// script call was successful
					// data contains the JSON values returned by the Perl script
					success: function(data){
						if (data.error) { // script returned error
							assert.ok(true, "error");
						} // if
						else { // login was successful
							expected_result.response.created_at = data.response.created_at;
							expected_result.response.updated_at = data.response.updated_at;
							expected_result.response.position = 3;
							assert.deepEqual(data, expected_result, "Download delete");
						} //else
					}, // success
					complete: function(data){
                        forceDeleteDownload(page.u_id);
                        forceDeletePage(page.u_id);
						deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file1.response.id);
						deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file2.response.id);
						deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file3.response.id);
						deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads",'/'+dirname.dirname+'/');
						logoutUser();
					} // always
				});
			});

		});
	});
});

QUnit.test("DBDN REST Get Downlaods No lang ", 6, function( assert ) {
	// GET /pages/descedents
	QUnit.stop();
	loginUser();
	var dirname = {dirname:UNIT_TEST_DIR, path: "/"};
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");
	var file1, file2, file3;
	var dw1, dw2, dw3;
	var page = createAndActivatePage();

	insertFile(SERVER_INDEX_URL + "files/createfile",function(file) {

		file1 = file;
		jQuery.extend(file1.response, {"brand_id": 0,"category_id": 0,type:'Downloads', protected : 0, "user_edit_id": null, deleted: 0, thumb_visible: 0, visible: 0,});
		dw1 = addDownload(page.id, page.u_id, file1.response.id)
		insertFile(SERVER_INDEX_URL + "files/createfile",'file2',function(file) {

			file2 = file;
			jQuery.extend(file2.response, {"brand_id": 0,"category_id": 0, type:'Downloads', protected : 0, "user_edit_id": null, deleted: 0, thumb_visible: 0, visible: 0});
			dw2 = addDownload(page.id, page.u_id, file2.response.id);
			insertFile(SERVER_INDEX_URL + "files/createfile", 'file3', function(file) {

				file3 = file;
				jQuery.extend(file3.response, {"brand_id": 0,"category_id": 0, type:'Downloads', protected : 0, "user_edit_id": null, deleted: 0, thumb_visible: 0, visible: 0});
				dw3 = addDownload(page.id,page.u_id, file3.response.id);
				var urlREST =  SERVER_INDEX_URL + "download/downloads/"+page.id;

				var expected_data = [file1.response, file2.response, file3.response];
				var expected_result = jQuery.extend({"error": false, "message": 'List of files found.', "response": null}, {"response": expected_data});

				jQuery.ajax({
					type: "GET",
					url: urlREST, // URL of the REST web service
					contentType: "application/json; charset=utf-8",
					dataType:"json",
					// script call was *not* successful
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						assert.ok(false, "error: " + textStatus + " : " + errorThrown);
					}, // error
					// script call was successful
					// data contains the JSON values returned by the Perl script
					success: function(data){
						if (data.error) { // script returned error
							assert.ok(true, "error");
						} // if
						else { // login was successful
							assert.deepEqual(data, expected_result, "Get downloads");
						} //else
					}, // success
					complete: function(data){
                        forceDeleteDownload(page.u_id);
                        forceDeletePage(page.u_id);
						deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file1.response.id);
						deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file2.response.id);
						deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file3.response.id);
						deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads",'/'+dirname.dirname+'/');
						logoutUser();
					} // always
				});
			});

		});
	});
});

QUnit.test("DBDN REST Get Downlaods Lang Set ", 6, function( assert ) {
	// GET /pages/descedents
	QUnit.stop()
	loginUser();
	var dirname = {dirname:UNIT_TEST_DIR, path: "/"};
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");
	var file1, file2, file3;
	var dw1, dw2, dw3;
	var page = createAndActivatePage();

	insertFile(SERVER_INDEX_URL + "files/createfile",function(file) {

		file1 = file;
		jQuery.extend(file1.response, {"brand_id": 0,"category_id": 0,type:'Downloads', protected : 0, "user_edit_id": null, deleted: 0,
            thumb_visible: 0, visible: 0, description: "Description", title: "Title"});
		dw1 = addDownload(page.id,page.u_id, file1.response.id)
		insertFile(SERVER_INDEX_URL + "files/createfile",'file2',function(file) {

			file2 = file;
			jQuery.extend(file2.response, {"brand_id": 0,"category_id": 0, type:'Downloads', protected : 0, "user_edit_id": null, deleted: 0,
                thumb_visible: 0, visible: 0, description: "Description", title: "Title"});
			dw2 = addDownload(page.id,page.u_id, file2.response.id);

				var urlREST =  SERVER_INDEX_URL + "download/downloads/"+page.id+"?lang=1";

				var expected_data = [file1.response, file2.response];
				var expected_result = jQuery.extend({"error": false, "message": 'List of files found.', "response": null}, {"response": expected_data});

				jQuery.ajax({
					type: "GET",
					url: urlREST, // URL of the REST web service
					contentType: "application/json; charset=utf-8",
					dataType:"json",
					// script call was *not* successful
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						assert.ok(false, "error: " + textStatus + " : " + errorThrown);
					}, // error
					// script call was successful
					// data contains the JSON values returned by the Perl script
					success: function(data){
						if (data.error) { // script returned error
							assert.ok(true, "error");
						} // if
						else { // login was successful
							assert.deepEqual(data, expected_result, "Get downloads");
						} //else
					}, // success
					complete: function(data){
                        forceDeleteDownload(page.u_id);
                        forceDeletePage(page.u_id);
						deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file1.response.id);
						deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file2.response.id);
						deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads",'/'+dirname.dirname+'/');
						logoutUser();
					} // always
				});

		});
	});
});

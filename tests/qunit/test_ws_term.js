/** 
 * Page Web Service
 */

QUnit.module("Test REST Term Web Services", {
  setup: function() {
    // prepare something for all following tests
	jQuery.ajaxSetup({ timeout: 5000 });
	
	jQuery( document ).ajaxStart(function(){
		ok( true, "ajaxStart" );
	}).ajaxStop(function(){
		ok( true, "ajaxStop" );
		QUnit.start();
	}).ajaxSend(function(){
		ok( true, "ajaxSend" );
	}).ajaxComplete(function(event, request, settings){ 
		ok( true, "ajaxComplete: " + request.responseText );
	}).ajaxError(function(){
		ok( false, "ajaxError" );
	}).ajaxSuccess(function(){
		ok( true, "ajaxSuccess" );
	});
  },
  teardown: function() {
    // clean up after each test
	jQuery( document ).unbind('ajaxStart');
	jQuery( document ).unbind('ajaxStop');
	jQuery( document ).unbind('ajaxSend');
	jQuery( document ).unbind('ajaxComplete');
	jQuery( document ).unbind('ajaxError');
	jQuery( document ).unbind('ajaxSuccess');
  }
});

QUnit.test("DBDN REST Create Term - success", 6, function( assert ) {
	QUnit.stop();
	loginUser();	
	// insert test data
    var new_term = {name:"Test Term",lang:"1",description:"Term description"};

	// expected data
	var expected_term = jQuery.extend({id: 2, created_at:"", updated_at:""}, new_term);
	var expected_result = jQuery.extend({"error": false, "message": "Term was added.", "response": null}, {"response": expected_term});
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "term/create";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(new_term),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			expected_term.id	= data.response.id;
			expected_result.response.id = data.response.id;
			expected_result.response.created_at = data.response.created_at;
			expected_result.response.updated_at = data.response.updated_at;
			assert.deepEqual(data, expected_result, "Term add result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(expected_term.id)
				deleteObject(SERVER_INDEX_URL + "term/forcedelete/" + expected_term.id);
			logoutUser();
		} // always
      });
	
});

QUnit.test("DBDN REST Create Term - same term exists", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// insert test data
    var term = {name:"Test Term",lang:"1",description:"Term description"};
	term = insertObject(term, SERVER_INDEX_URL + "term/create");
	
	var new_term  = jQuery.extend({id: term.id+1, created_at:"", updated_at:"",lang : term.lang, name: term.name}, term);
	
	var expected_result = {"error": true, "message": "Term with same name already exists!"};
	// url string
    var urlREST =  SERVER_INDEX_URL + "term/create";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(new_term),
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Term update result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Page update result");
          } // if
          else { // login was successful
        	assert.ok(false, "Page update ID check fail!");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(term.id)
				deleteObject(SERVER_INDEX_URL + "term/forcedelete/" + term.id);
			logoutUser();
		} // always
      });
	
});

QUnit.test("DBDN REST Update Term - success", 6, function( assert ) {
	QUnit.stop();
	loginUser();
	// test fields
    var update_term = {name:"Test Term Update",lang:"1",description:"Term description U"};

	// insert test data
	var term = {name:"Test Term",lang:"1",description:"Term description"};
	var term = insertObject(term, SERVER_INDEX_URL + "term/create");
	update_term = jQuery.extend(update_term, {id:term.id});
		
	// expected data
	var expected_term = update_term;
	var expected_result = jQuery.extend({"error": false, "message": "Term was updated.", "response": null}, {"response": expected_term});
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "term/update";
    
    jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(update_term),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			expected_result.response.created_at = data.response.created_at;
			expected_result.response.updated_at = data.response.updated_at;
        	assert.deepEqual(data, expected_result, "Term update result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(term.id)
				deleteObject(SERVER_INDEX_URL + "term/forcedelete/" + term.id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Update Term - id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// test fields
    var update_term = {};

	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required!"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "term/update";
    
    jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(update_term),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Term update result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Page update result");
          } // if
          else { // login was successful
        	assert.ok(false, "Page update ID check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Update Term - not found", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// test fields
    var update_term = {id:9999999,name:"Test Term",lang:"1",description:"Term description"};

	// expected data
	var expected_result = jQuery.extend({"error": true, "message": "Term not found.", "response": null}, {"response": update_term});
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "term/update";
    
    jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(update_term),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Page update result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Page update result");
          } // if
          else { // login was successful
        	assert.ok(false, "Page update unexisting page check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Get Terms by Lang - success", 7, function( assert ) {
	QUnit.stop();
	loginUser();
	// insert test data
	var term1 = {name:"Test Term 1",lang:1,description:"Term description 1"};
	term1 = insertObject(term1, SERVER_INDEX_URL + "term/create");
    if (term1.id == undefined) 
        assert.ok(false, "Cannot create term1: " + JSON.stringify(term1));

	var term2 = {name:"Test Term 2",lang:1,description:"Term description 2"};
	term2 = insertObject(term2, SERVER_INDEX_URL + "term/create");
    if (term2.id == undefined) 
        assert.ok(false, "Cannot create term2: " + JSON.stringify(term2));
	
	var term3 = {name:"Test Term 3",lang:2,description:"Term description 3"};
	term3 = insertObject(term3, SERVER_INDEX_URL + "term/create");
    if (term3.id == undefined) 
        assert.ok(false, "Cannot create term3: " + JSON.stringify(term3));
	
	// expected data
	var expected_term = [term1, term2];
	var expected_result = {"error": false, "message": "List of Terms found", "response": null};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "term/allterms/1";
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			//assert.notDeepEqual(data, expected_result, "Page list result");
            //check (error, message) - strict; response - must be array
            var responseArray = data.response;
            data.response = null;
            
            assert.deepEqual(data, expected_result, "Page read result");
            // Search for terms and check lang
            var foundIDArray = [];
            responseArray.forEach(function(term) {
               if (term.lang != term1.lang){
                   assert.ok(false, "Wrong language!");
               }
               // Search for term1 and term2
               expected_term.forEach(function(tmpTerm) {
                if (QUnit.equiv(tmpTerm, term)) {
                    foundIDArray.push(tmpTerm.id);
                }
               });
            });

            assert.equal(expected_term.length, foundIDArray.length, "Inserted terms are not found!");

          } //else
        }, // success
		complete: function(data){
			// delete test data 
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/" + term1.id);
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/" + term2.id);
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/" + term3.id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Get Terms by Lang Filter - success", 6, function( assert ) {
	QUnit.stop();
	loginUser();
	// insert test data
	var term1 = {name:"Test Term 1",lang:1,description:"Term description 1"};
	term1 = insertObject(term1, SERVER_INDEX_URL + "term/create");
	
	var term2 = {name:"Test Term 2",lang:1,description:"Term description 2"};
	term2 = insertObject(term2, SERVER_INDEX_URL + "term/create");
	
	var term3 = {name:"Test Term 3",lang:2,description:"Term description 3"};
	term3 = insertObject(term3, SERVER_INDEX_URL + "term/create");
	
	// expected data
	var expected_term = [term1];
	var expected_result = jQuery.extend({"error": false, "message": "List of Terms found", "response": null}, {"response": expected_term});
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "term/allterms/1?filter=Term 1";
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			//assert.notDeepEqual(data, expected_result, "Page list result");
			//check (error, message) - strict; response - must be array
			assert.deepEqual(data, expected_result, "Page read result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/" + term1.id);
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/" + term2.id);
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/" + term3.id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Get Term by ID - success", 6, function( assert ) {

	QUnit.stop();
	loginUser();
	// insert test data
	var term = {name:"Test Term 1",lang:1,description:"Term description 1"};
	term = insertObject(term, SERVER_INDEX_URL + "term/create");
	
	// expected data
	var expected_page = term;
	var expected_result = jQuery.extend({"error": false, "message": "Term found.", "response": null}, {"response": expected_page});
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "term/termbyid/" + term.id;
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			expected_result.response.created_at = data.response.created_at;
			expected_result.response.updated_at = data.response.updated_at;
			assert.deepEqual(data, expected_result, "Page read result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(term.id)
				deleteObject(SERVER_INDEX_URL + "term/forcedelete/" + term.id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Create Term - validation faild", 6, function( assert ) {
	QUnit.stop();
	// Unregister error handler!
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// insert test data
    var new_term = {name:"Test Term 1",lang:1,description:""};

	// expected data
	var expected_page = jQuery.extend({id: 2, created_at:"", updated_at:""}, new_term);
	var expected_result = {"error": true, "message": "", "response": null};
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "term/create";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(new_term),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
			assert.equal(XMLHttpRequest.status, 400, "HTTP Status code 400.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			expected_page.id	= data.response.id;
			expected_result.response = data.response;
			assert.deepEqual(data, expected_result, "Term add validate faild result");
		}, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			expected_page.id	= data.response.id;
			expected_result.response = data.response;
			assert.deepEqual(data, expected_result, "Term add validate faild result");
          } // if
          else { // login was successful
			  assert.ok(false, "error");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(expected_page.id)
				deleteObject(SERVER_INDEX_URL + "term/forcedelete/" + expected_page.id);
			logoutUser();
		} // always
      });
	
});

QUnit.test("DBDN REST Get Term by ID - id required", 6, function( assert ) {

	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required!"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "term/termbyid" ;
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Page read result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Page read result");
          } // if
          else { // login was successful
        	assert.ok(false, "Page read ID check fail!");
          } //else
        }, // success
		complete : function (){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Get Term by ID - not found", 6, function( assert ) {

	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Term not found"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "term/termbyid/" + 3;
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Term read result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Term read result");
          } // if
          else { // login was successful
        	assert.ok(false, "Page read unexisting page check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Delete Term - success", 6, function( assert ) {
	QUnit.stop();
	loginUser();
	// Insert page for delete:
    var delete_term = {name:"Test Term 1",lang:1,description:"Term description 1"};
    var delete_term = insertObject(delete_term, SERVER_INDEX_URL + "term/create");
   
	// expected data
	var expected_result = jQuery.extend({"error": false, "message": "Term was deleted!", "response": null}, {"response": delete_term});
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "term/delete/" + delete_term.id;
    
    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
		  else { // login was successful
			expected_result.response.updated_at = data.response.updated_at;
			
			assert.deepEqual(data, expected_result, "Page delete result");
          } //else
        }, // success
		complete : function(){
			// delete test data 
			deleteObject(SERVER_INDEX_URL + "term/forcedelete/" + delete_term.id);
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Delete Term - id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required!"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "term/delete" ;
    
    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Term delete result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Term delete result");
          } // if
          else { // login was successful
        	assert.ok(false, "Term delete ID check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Delete Term - not found", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Term not found."};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "term/delete/" + 3;
    
    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Term delete result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Term delete result");
          } // if
          else { // login was successful
        	assert.ok(false, "Term delete unexisting page check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});
/** 
 * Content Web Service
 */

QUnit.module("Test REST Web Services for Injections", {
	setup: function() {
		// prepare something for all following tests
		jQuery.ajaxSetup({timeout: 5000});

		jQuery(document).ajaxStart(function() {
			ok(true, "ajaxStart");
		}).ajaxStop(function() {
			ok(true, "ajaxStop");
			QUnit.start();
		}).ajaxSend(function() {
			ok(true, "ajaxSend");
		}).ajaxComplete(function(event, request, settings) {
			ok(true, "ajaxComplete: " + request.responseText);
		}).ajaxError(function() {
			ok(false, "ajaxError");
		}).ajaxSuccess(function() {
			ok(true, "ajaxSuccess");
		});
	},
	teardown: function() {
		// clean up after each test
		jQuery(document).unbind('ajaxStart');
		jQuery(document).unbind('ajaxStop');
		jQuery(document).unbind('ajaxSend');
		jQuery(document).unbind('ajaxComplete');
		jQuery(document).unbind('ajaxError');
		jQuery(document).unbind('ajaxSuccess');
	}
});

QUnit.test("DBDN REST Grant Access - fail on injections", 6, function(assert) {
	QUnit.stop();
	loginUser();
	jQuery( document ).unbind('ajaxError');

    var group = {name:"Admins",active:1,visible:1};
    var group = insertObject(group, SERVER_INDEX_URL + "group/create");
	
	var page = {parent_id:1,slug:"My_page_4",type:"File",active:1,position:1,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0};
	
	var page = insertObject(page, SERVER_INDEX_URL + "pages/create");

	var access = {group_id : "1' or '1' = '1", page_id : "1' or '1' = '1"};
	
	// expected data
	var expected_result = {"error":true,"message":"",
		"response":{
			"group_id":["The group id must be an integer."],
			"page_id":["The page id must be an integer."]
		}
	};

	// url string    
	var urlREST = SERVER_INDEX_URL + "access/grant";

	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(access),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Grant Access result");			
		}, // error 
		// script call was successful 
		// data contains the JSON values returned by the Perl script 
		success: function(data) {
			assert.ok(false, "error: " + JSON.stringify(data));
		}, // success
		complete: function(data){
			deleteAccess(group.id, page.id);
			deleteObject(SERVER_INDEX_URL + "group/forcedelete/"+group.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/"+page.id);
			logoutUser();
		} // always
	}); 
});

QUnit.test("DBDN REST Filter Pages - Injection", 7, function( assert ) {
	// GET /pages/all?filter=моята страница
	QUnit.stop();
	loginUser();
	// test fields
	var page_name = "1' or 1=1 or '1' = '1";
	
	// expected data
	var expected_page = new Array();
	var expected_result = jQuery.extend({"error": false, "message": "List of pages found.", "response": null}, {"response": expected_page});

	// url string
	var urlREST =  SERVER_INDEX_URL + "pages/search?lang=1&filter=" +  encodeURIComponent(page_name);
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			logoutUser();
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			assert.deepEqual(data, expected_result, "Page list result");
			assert.deepEqual(data.response, expected_page, "Page list result data");
          } //else
		  logoutUser();
        } // success
      });
});

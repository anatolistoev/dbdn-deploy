QUnit.module("Test REST Workflow Web Services", {
	setup: function() {
		// prepare something for all following tests
		jQuery.ajaxSetup({timeout: 5000});

		jQuery(document).ajaxStart(function() {
			ok(true, "ajaxStart");
		}).ajaxStop(function() {
			ok(true, "ajaxStop");
			QUnit.start();
		}).ajaxSend(function() {
			ok(true, "ajaxSend");
		}).ajaxComplete(function(event, request, settings) {
			ok(true, "ajaxComplete: " + request.responseText);
		}).ajaxError(function() {
			ok(false, "ajaxError");
		}).ajaxSuccess(function() {
			ok(true, "ajaxSuccess");
		});
	},
	teardown: function() {
		// clean up after each test
		jQuery(document).unbind('ajaxStart');
		jQuery(document).unbind('ajaxStop');
		jQuery(document).unbind('ajaxSend');
		jQuery(document).unbind('ajaxComplete');
		jQuery(document).unbind('ajaxError');
		jQuery(document).unbind('ajaxSuccess');
	}
});

QUnit.test("DBDN REST Update workflow - success", 6, function (assert) {
	QUnit.stop();
    loginUser();

	// insert test data
	var user = {username:"1234",password:"P1!as123sword",password_confirmation:"P1!as123sword",first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",last_login:"2014-01-14 16:46:45", logins: 15,
		role:1,newsletter:2,force_pwd_change:2, group : []};
    user = insertObject(user, SERVER_INDEX_URL + "user/create");

	var page = {parent_id:1,slug:"My_page_4",type:"File",active:1,position:1,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0};
    page = insertObject(page, SERVER_INDEX_URL + "pages/create");
    
    var workflow = page.workflow;
    workflow.editing_state = 'Review';
    workflow.user_responsible = 6;
    workflow.assigned_by = page.created_by;

    // expected_result
    var expected_result = {"error": false, "message": "Workflow was updated.", "response": workflow};

    var urlREST = SERVER_INDEX_URL + "workflow/update";

    jQuery.ajax({
		type: "PUT",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(workflow),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
            else { // login was successful
                delete data.response.page;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Workflow was updated.");
			} //else
		}, // success
		complete: function(data){
            deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
			deleteObject(SERVER_INDEX_URL + "workflow/forcedelete/"+workflow.id);
			deleteObject(SERVER_INDEX_URL + "pages/forcedelete/"+page.id);
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Update workflow - missing id", 6, function (assert) {
    QUnit.stop();
	// Unregister error handler!
	jQuery( document ).unbind('ajaxError');    
	loginUser();
	// insert test data
    var workflow = {editing_state: 'Approved'};

    // expected_result
	var expected_result = {"error": true, "message": "Parameter ID is required!"};

    var urlREST = SERVER_INDEX_URL + "workflow/update";

    jQuery.ajax({
		type: "PUT",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(workflow),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.equal(XMLHttpRequest.status, 400, "HTTP Status code 400.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
            var data = JSON.parse(fixedResponse);
                        
			if (data.error) { // script returned error
				assert.deepEqual(data, expected_result, "Validation is successfull.");
			} // if
			else { // login was successful
				assert.ok(false, "error");
            } //else		
        }, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			assert.ok(false, "error");
		}, // success
		complete: function(data){
			logoutUser();
		} // always
    });
});

QUnit.test("DBDN REST Update workflow - not found", 6, function (assert) {
    QUnit.stop();
	// Unregister error handler!
	jQuery( document ).unbind('ajaxError');    
	loginUser();
	// insert test data
    var workflow = {id: 123098, editing_state: 'Approved'};

    // expected_result
	var expected_result = {"error": true, "message": "Workflow not found.", response: workflow};

    var urlREST = SERVER_INDEX_URL + "workflow/update";

    jQuery.ajax({
		type: "PUT",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(workflow),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.equal(XMLHttpRequest.status, 404, "HTTP Status code 404.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
            var data = JSON.parse(fixedResponse);

            if (data.error) { // script returned error
				assert.deepEqual(data, expected_result, "Validation is successfull");
			} // if
			else { // login was successful
				assert.ok(false, "error");
            } //else		
        }, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			assert.ok(false, "error");
		}, // success
		complete: function(data){
			logoutUser();
		} // always
	});
});
/**
 * Content Web Service
 */

QUnit.module("Test REST Content Web Services", {
	setup: function() {
		// prepare something for all following tests
		jQuery.ajaxSetup({timeout: 5000});

		jQuery(document).ajaxStart(function() {
			ok(true, "ajaxStart");
		}).ajaxStop(function() {
			ok(true, "ajaxStop");
			QUnit.start();
		}).ajaxSend(function() {
			ok(true, "ajaxSend");
		}).ajaxComplete(function(event, request, settings) {
			ok(true, "ajaxComplete: " + request.responseText);
		}).ajaxError(function() {
			ok(false, "ajaxError");
		}).ajaxSuccess(function() {
			ok(true, "ajaxSuccess");
		});
	},
	teardown: function() {
		// clean up after each test
		jQuery(document).unbind('ajaxStart');
		jQuery(document).unbind('ajaxStop');
		jQuery(document).unbind('ajaxSend');
		jQuery(document).unbind('ajaxComplete');
		jQuery(document).unbind('ajaxError');
		jQuery(document).unbind('ajaxSuccess');
	}
});

QUnit.test("DBDN REST Register new Content - success", 6, function(assert) {
	QUnit.stop();
    loginUser();
	// insert test data
	var page = createAndActivatePage();
	var new_content = {page_id:page.id, page_u_id: page.u_id,lang:1,position:2,section:"section unnown",format:3,body:"This is test text ....",searchable:0};

	// expected data
	var expected_result = jQuery.extend({"error":false, "message": "Content was added.", "response": null}, {"response": [new_content]});

	// url string
	var urlREST = SERVER_INDEX_URL + "contents/create";

	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(new_content),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response[0].id = data.response[0].id;
				expected_result.response[0].created_at = data.response[0].created_at;
				expected_result.response[0].updated_at = data.response[0].updated_at;
				delete data.response[0].page;

				assert.deepEqual(data, expected_result, "Content add result");
			} //else
		}, // success
		complete: function(data){
			// delete test data
			if(expected_result.response[0].id)
				deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + expected_result.response[0].id);
			if(page)
				forceDeletePage(page.u_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Register new Content Many - success", 6, function(assert) {
	QUnit.stop();
    loginUser();
	// insert test data
	var page = createAndActivatePage();
	var new_content = {page_id:page.id,page_u_id:page.u_id, lang:1,position:2,section:"section unnown",format:3,body:"This is test text ....",searchable:0};
	var new_content_2 = {page_id:page.id,page_u_id:page.u_id, lang:1,position:3,section:"section unnown",format:3,body:"This is test text 2 ....",searchable:0};

	// expected data
	var expected_result = jQuery.extend({"error":false, "message": "Content was added.", "response": null}, {"response": [new_content,new_content_2]});

	// url string
	var urlREST = SERVER_INDEX_URL + "contents/create-many";

	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify([new_content,new_content_2]),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response[0].id = data.response[0].id;
				expected_result.response[0].created_at = data.response[0].created_at;
				expected_result.response[0].updated_at = data.response[0].updated_at;
				delete data.response[0].page;
				expected_result.response[1].id = data.response[1].id;
				expected_result.response[1].created_at = data.response[1].created_at;
				expected_result.response[1].updated_at = data.response[1].updated_at;
				delete data.response[1].page;
				assert.deepEqual(data, expected_result, "Content add result");
			} //else
		}, // success
		complete: function(data){
			// delete test data
			if(expected_result.response[0].id)
				deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + expected_result.response[0].id);
			if(expected_result.response[1].id)
				deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + expected_result.response[1].id);
			if(page)
                forceDeletePage(page.u_id)
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Register new Content - page not found", 6, function(assert) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// insert test data
	var new_content = {page_id:999999, page_u_id: 999999,lang:1,position:2,section:"section unnown",format:3,body:"This is test text ....",searchable:0};

	// expected data
	var expected_result = {"error": true, "message": "Page not found!"};

	// url string
	var urlREST = SERVER_INDEX_URL + "contents/create";

	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(new_content),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Content add result");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Content add result");
          } // if
          else { // login was successful
        	assert.ok(false, "Content add unexisting page check fail!");
          } //else
        }, // success
		complete: function(data){
			logoutUser();
		}
	});

});

QUnit.test("DBDN REST Register new Content - page id required", 6, function(assert) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// test fields
	var new_content = {lang:1,position:2,section:"section unnown",format:3,body:"This is test text ....",searchable:0};

	// expected data
	var expected_result = {"error": true, "message": "Parameter page ID is required!"};

	// url string
	var urlREST = SERVER_INDEX_URL + "contents/create";

	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(new_content),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Content add result");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Content add result");
          } // if
          else { // login was successful
        	assert.ok(false, "Content add unexisting page check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});

});

QUnit.test("DBDN REST Register new Content - content validate failed", 6, function(assert) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// insert test data
	var page = createAndActivatePage();
	var new_content = {page_id:page.id, page_u_id:page.u_id, lang:1,position:2,section:"section unnown",format:3,body:"This is test text ...."};

	// expected data
	var expected_result = jQuery.extend({"error":true, "message": "", "response": null}, {"response": new_content});

	// url string
	var urlREST = SERVER_INDEX_URL + "contents/create";

	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(new_content),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			expected_result.response = data.response;
			assert.deepEqual(data, expected_result, "Content add result");
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				expected_result.response = data.response;
				assert.deepEqual(data, expected_result, "Content add result");
			} // if
			else { // login was successful
				assert.ok(false, "error");
			} //else
		}, // success
		complete: function(data){
			// delete test data
			if(expected_result.response && expected_result.response.id)
				deleteObject(SERVER_INDEX_URL + "contents/delete/" + expected_result.response.id);
			if(page)
                forceDeletePage(page.u_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Update Content - success", 6, function( assert ) {
	QUnit.stop();
	loginUser();
	// insert test data
	var page = createAndActivatePage();
	var content = {page_id:page.id,page_u_id:page.u_id,lang:1,position:2,section:"section unnown",format:3,body:"This is test text ....",searchable:0};
	var content = insertObject(content, SERVER_INDEX_URL + "contents/create");


	// test fields
    var update_content = {id:content[0].id,page_id:page.id,page_u_id:page.u_id,lang:1,position:3,section:"section unnown",format:2,body:"This is NEW test text ....",searchable:0};

	// expected data
	var expected_result = jQuery.extend({"error": false, "message": "Content was updated.", "response": null}, {"response": [update_content]});

    // url string
	var urlREST =  SERVER_INDEX_URL + "contents/update";

    jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(update_content),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			//expected_result.response.created_by = data.response.created_by;
			expected_result.response[0].created_at = data.response[0].created_at;
			expected_result.response[0].updated_at = data.response[0].updated_at;
			delete data.response[0].page;

        	assert.deepEqual(data, expected_result, "Content update result");
          } //else
        }, // success
		complete: function(data){
			// delete test data
			if(content[0].id)
				deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + content[0].id);
			if(page)
                forceDeletePage(page.u_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Update Content Many - success", 6, function( assert ) {
	QUnit.stop();
	loginUser();
	// insert test data
	var page = createAndActivatePage();
	var content = {page_id:page.id, page_u_id:page.u_id, lang:1,position:2,section:"section unnown",format:3,body:"This is test text ....",searchable:0};
	var content_2 = {page_id:page.id, page_u_id:page.u_id, lang:1,position:3,section:"section unnown",format:3,body:"This is test text 2 ....",searchable:0};
	var content = insertObject([content,content_2], SERVER_INDEX_URL + "contents/create-many");

	// test fields
    var update_content = {id:content[0].id,page_id:page.id, page_u_id:page.u_id,lang:1,position:3,section:"section unnown",format:2,body:"This is NEW test text ....",searchable:0};
	 var update_content_2 = {id:content[1].id,page_id:page.id, page_u_id:page.u_id,lang:1,position:3,section:"section unnown",format:2,body:"This is NEW test text 2 ....",searchable:0};

	// expected data
	var expected_result = jQuery.extend({"error": false, "message": "Content was updated.", "response": null}, {"response": [update_content,update_content_2]});

    // url string
	var urlREST =  SERVER_INDEX_URL + "contents/update-many";

    jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify([update_content,update_content_2]),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			//expected_result.response.created_by = data.response.created_by;
			expected_result.response[0].created_at = data.response[0].created_at;
			expected_result.response[0].updated_at = data.response[0].updated_at;
			delete data.response[0].page;
			expected_result.response[1].created_at = data.response[1].created_at;
			expected_result.response[1].updated_at = data.response[1].updated_at;
			delete data.response[1].page;

        	assert.deepEqual(data, expected_result, "Content update result");
          } //else
        }, // success
		complete: function(data){
			// delete test data
			if(content[0].id)
				deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + content[0].id);
			if(page)
                forceDeletePage(page.u_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Update Content - id reqired", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// insert test data
    var update_content = {page_id:999999,page_u_id: 999999, lang:1,position:2,section:"section unnown",format:3,body:"This is test text ....",searchable:0};

	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required!"};

    // url string
	var urlREST =  SERVER_INDEX_URL + "contents/update";

    jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(update_content),
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Content update result");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Content update result");
          } // if
          else { // login was successful
        	assert.ok(false, "Content update ID check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Update Content - not found", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// insert test data
    var update_content = {id:999999, page_id:999990, page_u_id: 999999, lang:1,position:2,section:"section unnown",format:3,body:"This is test text ....",searchable:0};

	// expected data
	var expected_result = jQuery.extend({"error": true, "message": "Content not found.", "response": null}, {"response": update_content});

    // url string
	var urlREST =  SERVER_INDEX_URL + "contents/update";

    jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(update_content),
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Content update result");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Content update result");
          } // if
          else { // login was successful
        	assert.ok(false, "Content update ID check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Get List of Content - success", 6, function( assert ) {
	// GET contents/page-id/{pageId}/lang/{lang}
	QUnit.stop();
	loginUser();
	// insert test data
	var page = createAndActivatePage();
	var content = {page_id:page.id, page_u_id:page.u_id, lang:3,position:2,section:"section unnown",format:3,body:"This is test text ....",searchable:0};
	var content = insertObject(content, SERVER_INDEX_URL + "contents/create");

	// test fields
	var pageId = page.id;
	var lang = 3;

	// expected data
	var expected_result = jQuery.extend({"error": false, "message": "List of contents found.", "response": Array({})}, {"response": [content]});

    // url string
	var urlREST =  SERVER_INDEX_URL + "contents/page-id/" + pageId + "/" + lang;

    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			//assert.deepEqual(data, expected_result, "Contents list result");
			if(data.error == expected_result.error && data.message == expected_result.message && (data.response instanceof Array)){
				assert.ok(true, "Contents list result");
			}else{
				assert.ok(false, "Contents list result error");
			}
          } //else
        }, // success
		complete: function(data){
			// delete test data
			if(content[0].id)
				deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + content[0].id);
			if(page)
                forceDeletePage(page.u_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Get List of Content - id required", 6, function( assert ) {
	// GET contents/page-id/{pageId}/lang/{lang}
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required!"};

    // url string
	var urlREST =  SERVER_INDEX_URL + "contents/page-id/";

    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Content add result");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Content add result");
          } // if
          else { // login was successful
        	assert.ok(false, "Content get ID check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Get Content by ID - success", 6, function( assert ) {

	QUnit.stop();
	loginUser();
	// insert test data
	var page = createAndActivatePage();
	var content = {page_id:page.id, page_u_id:page.u_id,lang:1,position:2,section:"section unnown",format:3,
					body:"This is test text ....",searchable:0};
	var content = insertObject(content, SERVER_INDEX_URL + "contents/create");

	// test fields
	var content_id = content[0].id;

	// expected data
	var expected_result = jQuery.extend({"error": false, "message": "Content found.", "response": null}, 
										{"response": content[0]});

    // url string
	var urlREST =  SERVER_INDEX_URL + "contents/read/" + content_id;

    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			//expected_result.response.created_by = data.response.created_by;
			expected_result.response.created_at = data.response.created_at;
			expected_result.response.updated_at = data.response.updated_at;
			delete expected_result.response.page;

			assert.deepEqual(data, expected_result, "Content read result");
          } //else
        }, // success
		complete: function(data){
			// delete test data
			if(content[0].id)
				deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + content[0].id);
			if(page)
                forceDeletePage(page.u_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Get Content by ID - id required", 6, function( assert ) {

	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required!"};

    // url string
	var urlREST =  SERVER_INDEX_URL + "contents/read";

    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Content add result");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Content add result");
          } // if
          else { // login was successful
        	assert.ok(false, "Content read ID check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Get Content by ID - not found", 6, function( assert ) {

	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Content not found."};

    // url string
	var urlREST =  SERVER_INDEX_URL + "contents/read/" + 999999;

    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Content read result");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Content read result");
          } // if
          else { // login was successful
        	assert.ok(false, "Content read ID check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Delete Content - success", 6, function( assert ) {
	QUnit.stop();
	loginUser();
	// insert test data
	var page = createAndActivatePage();
	var content = { page_id: page.id, page_u_id: page.u_id, lang: 1, position: 2, section: "section unnown", 
					format: 3, body: "This is test text ....", searchable: 0 };
	var content = insertObject(content, SERVER_INDEX_URL + "contents/create");

 	// test fields
	var content_id_for_delete = content[0].id;

	// expected data
	var expected_result = jQuery.extend({"error": false, "message": "Content was deleted.", "response": null}, 
										{"response": content[0]});

    // url string
	var urlREST =  SERVER_INDEX_URL + "contents/delete/" + content_id_for_delete;

    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			if(content.id)
				deleteObject(SERVER_INDEX_URL + "contents/delete/" + content.id);
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			expected_result.response.page.updated_at = data.response.page.updated_at;
			expected_result.response.updated_at = data.response.updated_at;
			assert.deepEqual(data, expected_result, "Content delete result");
          } //else
        }, // success
		complete: function(data){
			// delete test data
			deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + content_id_for_delete);
			if(page)
				forceDeletePage(page.u_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Delete Content - id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required!"};

    // url string
	var urlREST =  SERVER_INDEX_URL + "contents/delete";

    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Content delete result");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Content delete result");
          } // if
          else { // login was successful
        	assert.ok(false, "Content delete ID check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Delete Content - not found", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Content not found."};

    // url string
	var urlREST =  SERVER_INDEX_URL + "contents/delete/999999";

    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
       error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Content delete result");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Content delete result");
          } // if
          else { // login was successful
        	assert.ok(false, "Content delete not found check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});


/**
 * Content Web Service
 */

QUnit.module("Test REST History Web Services", {
	setup: function() {
		// prepare something for all following tests
		jQuery.ajaxSetup({timeout: 5000});

		jQuery(document).ajaxStart(function() {
			ok(true, "ajaxStart");
		}).ajaxStop(function() {
			ok(true, "ajaxStop");
			QUnit.start();
		}).ajaxSend(function() {
			ok(true, "ajaxSend");
		}).ajaxComplete(function(event, request, settings) {
			ok(true, "ajaxComplete: " + request.responseText);
		}).ajaxError(function() {
			ok(false, "ajaxError");
		}).ajaxSuccess(function() {
			ok(true, "ajaxSuccess");
		});
	},
	teardown: function() {
		// clean up after each test
		jQuery(document).unbind('ajaxStart');
		jQuery(document).unbind('ajaxStop');
		jQuery(document).unbind('ajaxSend');
		jQuery(document).unbind('ajaxComplete');
		jQuery(document).unbind('ajaxError');
		jQuery(document).unbind('ajaxSuccess');
	}
});

QUnit.test("DBDN REST History delete by ID - success", 6, function(assert) {
	QUnit.stop();
    loginUser();
	var page1 = {parent_id:1,slug:"My_page",type:"File",active:1,position:0,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0};
	var page1 = insertObject(page1, SERVER_INDEX_URL + "pages/create");
	var urlREST = SERVER_INDEX_URL + "history/all?page_id="+page1.id;
    $.ajaxSetup({ global: false });
	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
            QUnit.ok(false, "error: " + textStatus + " : " + errorThrown + " result: " + JSON.stringify(XMLHttpRequest.responseJSON));
		}, // error
		success: function(data) {
            var expected_result = jQuery.extend({"error":false, "message": "History was deleted.", "response": null}, {"response": data.response[0]});
            var urlREST = SERVER_INDEX_URL + "history/delete/"+data.response[0].id;
            jQuery.ajax({
                type: "DELETE",
                url: urlREST, // URL of the REST web service
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                // script call was *not* successful
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    assert.ok(false, "error: " + textStatus + " : " + errorThrown);
                }, // error
                // script call was successful
                // data contains the JSON values returned by the Perl script
                success: function(data) {
                    if (data.error) { // script returned error
                        assert.ok(false, "error");
                    } // if
                    else { // login was successful
                        assert.deepEqual(data, expected_result, "Delete History by id");
                    } //else
                }, // success
              });
		}, // success
		complete: function(data){
			forceDeletePage(page1.u_id);
			logoutUser();
		} // always
      });
      $.ajaxSetup({ global: true });
});

QUnit.test("DBDN REST History by page - success", 6, function(assert) {
	QUnit.stop();
    loginUser();
	var page1 = {parent_id:1,slug:"My_page4",type:"File",active:1,position:0,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0};
	var page1 = insertObject(page1, SERVER_INDEX_URL + "pages/create");
	var history1 = {page_slug : page1.slug,page_id : page1.id, content_section : "", content_id : 0, username: 'esof', user_id : 2, action : "created"};

	var content1 = {page_id:page1.id,page_u_id:page1.u_id,lang:1,position:2,section:"section unnown",format:3,body:"This is test text ....",searchable:0};
	var content1 = insertObject(content1, SERVER_INDEX_URL + "contents/create");
	var history2 = {page_slug : page1.slug,page_id : page1.id, content_section : content1[0].section, content_id : content1[0].id, username: 'esof', user_id : 2, action : "created"}

	deleteObject(SERVER_INDEX_URL + "contents/delete/" + content1[0].id);
	var history3 = {page_slug : page1.slug,page_id : page1.id, content_section : content1[0].section, content_id : content1[0].id, username: 'esof', user_id : 2, action : "removed"} ;

	deleteObject(SERVER_INDEX_URL + "pages/delete/" + page1.id);
	var history4 = {page_slug : page1.slug,page_id : page1.id, content_section : "", content_id : 0, username: 'esof', user_id : 2, action : "removed"};

	var expected_data = [history1, history2, history3, history4];
	var expected_result = jQuery.extend({"error":false, "message": "List of history items found.", "response": null}, {"response": expected_data});

	// url string
	var urlREST = SERVER_INDEX_URL + "history/all?page_id="+page1.id;

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful

				var h_c = 0;
				for(h_c = 0;h_c < data.response.length;h_c++){
					if(typeof(expected_result.response[h_c]) != "undefined"){
						expected_result.response[h_c].id = data.response[h_c].id;
						expected_result.response[h_c].ip = data.response[h_c].ip;
						expected_result.response[h_c].created_at = data.response[h_c].created_at;
						expected_result.response[h_c].updated_at = data.response[h_c].updated_at;
					}
				}
				assert.deepEqual(data, expected_result, "History get by page result");
			} //else
		}, // success
		complete: function(data){
			deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + content1.id);
            forceDeletePage(page1.u_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST History by user - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
	var page1 = {parent_id:1,slug:"My_page4",type:"File",active:1,position:0,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0};
	var page1 = insertObject(page1, SERVER_INDEX_URL + "pages/create");
	var history1 = {page_slug : page1.slug,page_id : page1.id, content_section : "", content_id : 0, username: 'esof', user_id : 2, action : "created"};

	var content1 = {page_id:page1.id,page_u_id:page1.u_id,lang:1,position:2,section:"section unnown",format:3,body:"This is test text ....",searchable:0};
	var content1 = insertObject(content1, SERVER_INDEX_URL + "contents/create");
	var history2 = {page_slug : page1.slug,page_id : page1.id, content_section : content1[0].section, content_id : content1[0].id, username: 'esof', user_id : 2, action : "created"}

	deleteObject(SERVER_INDEX_URL + "contents/delete/" + content1[0].id);
	var history3 = {page_slug : page1.slug,page_id : page1.id, content_section : content1[0].section, content_id : content1[0].id, username: 'esof', user_id : 2, action : "removed"} ;

	deleteObject(SERVER_INDEX_URL + "pages/delete/" + page1.id);
	var history4 = {page_slug : page1.slug,page_id : page1.id, content_section : "", content_id : 0, username: 'esof', user_id : 2, action : "removed"};

	var expected_data = [history1, history2, history3, history4];
	var expected_result = jQuery.extend({"error":false, "message": "List of history items found.", "response": null}, {"response": expected_data});

	// url string
	var urlREST = SERVER_INDEX_URL + "history/all?user_id=2&from="+page1.created_at;

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful

				var h_c = 0;
				for(h_c = 0;h_c < data.response.length;h_c++){
					if(typeof(expected_result.response[h_c]) != "undefined"){
						expected_result.response[h_c].id = data.response[h_c].id;
						expected_result.response[h_c].ip = data.response[h_c].ip;
						expected_result.response[h_c].created_at = data.response[h_c].created_at;
						expected_result.response[h_c].updated_at = data.response[h_c].updated_at;
					}
				}
				assert.deepEqual(data, expected_result, "History get by page result");
			} //else
		}, // success
		complete: function(data){
			deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + content1[0].id);
            forceDeletePage(page1.u_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST History by content - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
	var page1 = {parent_id:1,slug:"My_page_4",type:"File",active:1,position:0,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0};
	var page1 = insertObject(page1, SERVER_INDEX_URL + "pages/create");

	var content1 = {page_id:page1.id ,page_u_id:page1.u_id, lang:1,position:2,section:"section unnown",format:3,body:"This is test text ....",searchable:0};
	var content1 = insertObject(content1, SERVER_INDEX_URL + "contents/create");
	var history2 = {page_slug : page1.slug,page_id : page1.id, content_section : content1[0].section, content_id : content1[0].id, username: 'esof', user_id : 2, action : "created"}

	deleteObject(SERVER_INDEX_URL + "contents/delete/" + content1[0].id);
	var history3 = {page_slug : page1.slug,page_id : page1.id, content_section : content1[0].section, content_id : content1[0].id, username: 'esof', user_id : 2, action : "removed"} ;

	deleteObject(SERVER_INDEX_URL + "pages/delete/" + page1.id);

	var expected_data = [history2, history3];
	var expected_result = jQuery.extend({"error":false, "message": "List of history items found.", "response": null},
										{"response": expected_data});

	// url string
	var urlREST = SERVER_INDEX_URL + "history/all?content_id="+content1[0].id;

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful

				var h_c = 0;
				for(h_c = 0;h_c < data.response.length;h_c++){
					if(typeof(expected_result.response[h_c]) != "undefined"){
						expected_result.response[h_c].id = data.response[h_c].id;
						expected_result.response[h_c].ip = data.response[h_c].ip;
						expected_result.response[h_c].created_at = data.response[h_c].created_at;
						expected_result.response[h_c].updated_at = data.response[h_c].updated_at;
					}
				}
				//TODO: Find reason for content history about different page and the same content id!
				assert.deepEqual(data, expected_result, "History get by content result");
			} //else
		}, // success
		complete: function(data){
			deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + content1[0].id);
			forceDeletePage(page1.u_id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST History from date - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
	var page1 = {parent_id:1,slug:"My_page_4",type:"File",active:1,position:0,in_navigation:1,authorization:0,home_accordion: 0,home_block: 0,
		visits:1,template:"Template Update 1",langs:2,deletable:1,deleted:0,searchable:1,inline_glossary:1,overview:0,
		created_by:0};
	var page1 = insertObject(page1, SERVER_INDEX_URL + "pages/create");
	var history1 = {page_slug : page1.slug,page_id : page1.id, content_section : "", content_id : 0, username: 'esof', user_id : 2, action : "created"};

	var content1 = {page_id:page1.id, page_u_id:page1.u_id, lang:1,position:2,section:"section unnown",format:3,body:"This is test text ....",searchable:0};
	var content1 = insertObject(content1, SERVER_INDEX_URL + "contents/create");
	var history2 = {page_slug : page1.slug,page_id : page1.id, content_section : content1[0].section, content_id : content1[0].id, username: 'esof', user_id : 2, action : "created"}

	deleteObject(SERVER_INDEX_URL + "contents/delete/" + content1[0].id);
	var history3 = {page_slug : page1.slug,page_id : page1.id, content_section : content1[0].section, content_id : content1[0].id, username: 'esof', user_id : 2, action : "removed"} ;

	deleteObject(SERVER_INDEX_URL + "pages/delete/" + page1.id);
	var history4 = {page_slug : page1.slug,page_id : page1.id, content_section : "", content_id : 0, username: 'esof', user_id : 2, action : "removed"} ;

	var expected_data = [history1, history2, history3, history4];
	var expected_result = jQuery.extend({"error":false, "message": "List of history items found.", "response": null}, {"response": expected_data});

	// url string
	var urlREST = SERVER_INDEX_URL + "history/all?from=" + page1.created_at;

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful

				var h_c = 0;
				for(h_c = 0;h_c < data.response.length;h_c++){
					if(typeof(expected_result.response[h_c]) != "undefined"){
						expected_result.response[h_c].id = data.response[h_c].id;
						expected_result.response[h_c].ip = data.response[h_c].ip;
						expected_result.response[h_c].created_at = data.response[h_c].created_at;
						expected_result.response[h_c].updated_at = data.response[h_c].updated_at;
					}
				}
				assert.deepEqual(data, expected_result, "History get by content result");
			} //else
		}, // success
		complete: function(data){
			deleteObject(SERVER_INDEX_URL + "contents/forcedelete/" + content1[0].id);
            forceDeletePage(page1.u_id);
			logoutUser();
		} // always
      });
});
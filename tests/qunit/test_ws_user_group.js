/** 
 * User & Group Web Service
 */

QUnit.module("Test REST User & Group Web Services", {
  setup: function() {
    // prepare something for all following tests
	jQuery.ajaxSetup({ timeout: 5000 });
	
	jQuery( document ).ajaxStart(function(){
		ok( true, "ajaxStart" );
	}).ajaxStop(function(){
		ok( true, "ajaxStop" );
		QUnit.start();
	}).ajaxSend(function(){
		ok( true, "ajaxSend" );
	}).ajaxComplete(function(event, request, settings){ 
		ok( true, "ajaxComplete: " + request.responseText );
	}).ajaxError(function(){
		ok( false, "ajaxError" );
	}).ajaxSuccess(function(){
		ok( true, "ajaxSuccess" );
	});
  },
  teardown: function() {
    // clean up after each test
	jQuery( document ).unbind('ajaxStart');
	jQuery( document ).unbind('ajaxStop');
	jQuery( document ).unbind('ajaxSend');
	jQuery( document ).unbind('ajaxComplete');
	jQuery( document ).unbind('ajaxError');
	jQuery( document ).unbind('ajaxSuccess');
  }
});

QUnit.test("DBDN REST Create User - success", 6, function( assert ) {
	QUnit.stop();
	loginUser();
	// insert test data
	var new_user = {username:"usr_grp_crt_usr",password:"Pas55word!?",password_confirmation:"Pas55word!?",
		first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",
		company:"Daimler", position:"developer",department:"IT",
		address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",
//L4.0 only		last_login:undefined, logins: 0,
		role:1,newsletter:2,active:1, group : []};

	// expected data
	var expected_user = jQuery.extend({created_at:"", updated_at:""}, new_user);
	delete expected_user.password;
	delete expected_user.password_confirmation;
	delete expected_user.group;

	var expected_result = jQuery.extend({"error": false, "message": "User was added.", "response": null}, 
										{"response": expected_user});
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "user/create";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(new_user),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			expected_user.id	= data.response.id;
			expected_result.response.id = data.response.id;
			expected_result.response.unlock_code = data.response.unlock_code;
			expected_result.response.created_at = data.response.created_at;
			expected_result.response.updated_at = data.response.updated_at;
			assert.deepEqual(data, expected_result, "User add result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + expected_user.id);
			logoutUser();
		} // always
      });
	
});

QUnit.test("DBDN REST Create User - validation faild", 6, function( assert ) {
	QUnit.stop();
	// Unregister error handler!
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// insert test data
	var new_user = {username:"usr_grp_crt_usr_1",password:"pwd4",password_confirmation:"P1!as123sword",
		first_name:"First Name",last_name:"Last Name", email:"somemail",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",
		phone:"0889285",fax:"32423434",active:1,last_login:"2014-01-14 16:46",
		unlock_code:"123123",role:1,newsletter:2,force_pwd_change:2, group : []};

	// expected data
	var expected_user = jQuery.extend({created_at:"", updated_at:""}, new_user);
	delete expected_user.password;
	delete expected_user.group;
	var expected_result = {"error": true, "message": "", "response": null};
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "user/create";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(new_user),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
			assert.equal(XMLHttpRequest.status, 400, "HTTP Status code 400.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			expected_user.id	= data.response.id;
			expected_result.response = data.response;
			assert.deepEqual(data, expected_result, "User add validate faild result");
		}, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			expected_user.id	= data.response.id;
			expected_result.response = data.response;
			assert.deepEqual(data, expected_result, "User add validate faild result");
          } // if
          else { // login was successful3
			  expected_user.id	= data.response.id;
			  assert.ok(false, "error");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(expected_user.id)
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + expected_user.id);
			logoutUser();
		} // always
      });
	
});

QUnit.test("DBDN REST Create User - validation faild password too long", 6, function( assert ) {
	QUnit.stop();
	// Unregister error handler!
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// insert test data
    var new_user = {username:"usr_grp_crt_usr_2",password:"Pwd4!acdefghjklcdefghjkl26",password_confirmation:"Pwd4!acdefghjklcdefghjkl26",first_name:"First Name",last_name:"Last Name", email:"somemail@damler.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",active:1,last_login:"2014-01-14 16:46", logins: 15,
		unlock_code:"123123",role:1,newsletter:2,force_pwd_change:2, group :[]};

	// expected data
	var expected_user = jQuery.extend({created_at:"", updated_at:""}, new_user);
	delete expected_user.password;
	var expected_result = {"error": true, "message": "", "response": {"password":["The password may not be greater than 25 characters."]}};
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "user/create";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(new_user),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
			assert.equal(XMLHttpRequest.status, 400, "HTTP Status code 400.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
		}, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
          } // if
          else { // login was successful3
			  expected_user.id	= data.response.id;
			  assert.ok(false, "error");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(expected_user.id)
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + expected_user.id);
			logoutUser();
		} // always
      });
	
});

QUnit.test("DBDN REST Create User - validation faild password too short", 6, function( assert ) {
	QUnit.stop();
	// Unregister error handler!
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// insert test data
    var new_user = {username:"usr_grp_crt_usr_3",password:"Pwd4!",password_confirmation:"Pwd4!",first_name:"First Name",last_name:"Last Name", email:"somemail@damler.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",active:1,last_login:"2014-01-14 16:46", logins: 15,
		unlock_code:"123123",role:1,newsletter:2,force_pwd_change:2, group : []};

	// expected data
	var expected_user = jQuery.extend({id: 2, created_at:"", updated_at:""}, new_user);
	delete expected_user.password;
	delete expected_user.group;
	var expected_result = {"error": true, "message": "", "response": {"password":["The password must be at least 8 characters."]}};
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "user/create";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(new_user),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
			assert.equal(XMLHttpRequest.status, 400, "HTTP Status code 400.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
		}, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
          } // if
          else { // login was successful3
			  expected_user.id	= data.response.id;
			  assert.ok(false, "error");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(expected_user.id)
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + expected_user.id);
			logoutUser();
		} // always
      });
	
});

QUnit.test("DBDN REST Create User - validation faild password too short", 6, function( assert ) {
	QUnit.stop();
	// Unregister error handler!
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// insert test data
    var new_user = {username:"usr_grp_crt_usr_4",password:"Pwd4",password_confirmation:"Pwd4",first_name:"First Name",last_name:"Last Name", email:"somemail@damler.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",active:1,last_login:"2014-01-14 16:46", logins: 15,
		unlock_code:"123123",role:1,newsletter:2,force_pwd_change:2, group : []};

	// expected data
	var expected_user = jQuery.extend({id: 2, created_at:"", updated_at:""}, new_user);
	delete expected_user.password;
	delete expected_user.group;
	var expected_result = {"error": true, "message": "", "response": {"password":["The password must be at least 8 characters.","Invalid password: minimum 1 special character required (!\"#$%&'()*+,%;:=?_@>-)"]}};
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "user/create";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(new_user),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
			assert.equal(XMLHttpRequest.status, 400, "HTTP Status code 400.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
		}, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
          } // if
          else { // login was successful3
			  expected_user.id	= data.response.id;
			  assert.ok(false, "error");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(expected_user.id)
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + expected_user.id);
			logoutUser();
		} // always
      });
	
});

QUnit.test("DBDN REST Create User - validation faild password no upper case", 6, function( assert ) {
	QUnit.stop();
	// Unregister error handler!
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// insert test data
    var new_user = {username:"usr_grp_crt_usr_5",password:"pwd4!2%s",password_confirmation:"pwd4!2%s",first_name:"First Name",last_name:"Last Name", email:"somemail@damler.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",active:1,last_login:"2014-01-14 16:46", logins: 15,
		unlock_code:"123123",role:1,newsletter:2,force_pwd_change:2, group : []};

	// expected data
	var expected_user = jQuery.extend({created_at:"", updated_at:""}, new_user);
	delete expected_user.password;
	delete expected_user.group;
	var expected_result = {"error": true, "message": "", "response": {"password":["Invalid password: minimum 1 upper case letter required"]}};
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "user/create";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(new_user),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
			assert.equal(XMLHttpRequest.status, 400, "HTTP Status code 400.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
		}, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
          } // if
          else { // login was successful3
			  expected_user.id	= data.response.id;
			  assert.ok(false, "error");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(expected_user.id)
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + expected_user.id);
			logoutUser();
		} // always
      });
	
});

QUnit.test("DBDN REST Create User - validation faild password no lower case", 6, function( assert ) {
	QUnit.stop();
	// Unregister error handler!
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// insert test data
    var new_user = {username:"usr_grp_crt_usr_6",password:"PWD4!2%S",password_confirmation:"PWD4!2%S",first_name:"First Name",last_name:"Last Name", email:"somemail@damler.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",active:1,last_login:"2014-01-14 16:46", logins: 15,
		unlock_code:"123123",role:1,newsletter:2,force_pwd_change:2};

	// expected data
	var expected_user = jQuery.extend({created_at:"", updated_at:""}, new_user);
	delete expected_user.password;
	delete expected_user.group;
	var expected_result = {"error": true, "message": "", "response": {"password":["Invalid password: minimum 1 lower case letter required"]}};
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "user/create";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(new_user),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
			assert.equal(XMLHttpRequest.status, 400, "HTTP Status code 400.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
		}, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
          } // if
          else { // login was successful3
			  expected_user.id	= data.response.id;
			  assert.ok(false, "error");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(expected_user.id)
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + expected_user.id);
			logoutUser();
		} // always
      });
	
});

QUnit.test("DBDN REST Create User - validation faild password umlaut", 6, function( assert ) {
	QUnit.stop();
	// Unregister error handler!
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// insert test data
    var new_user = {username:"usr_grp_crt_usr_7",password:"PWD4!2%sö",password_confirmation:"PWD4!2%sö",first_name:"First Name",last_name:"Last Name", email:"somemail@damler.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",active:1,last_login:"2014-01-14 16:46", logins: 15,
		unlock_code:"123123",role:1,newsletter:2,force_pwd_change:2, group : []};

	// expected data
	var expected_user = jQuery.extend({created_at:"", updated_at:""}, new_user);
	delete expected_user.password;
	delete expected_user.group;
	var expected_result = {"error": true, "message": "", "response": {"password":["The password cannot contain umlaut symbols"]}};
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "user/create";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(new_user),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
			assert.equal(XMLHttpRequest.status, 400, "HTTP Status code 400.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
		}, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
          } // if
          else { // login was successful3
			  expected_user.id	= data.response.id;
			  assert.ok(false, "error");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(expected_user.id)
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + expected_user.id);
			logoutUser();
		} // always
      });
	
});

QUnit.test("DBDN REST Create User - validation faild password part of username", 6, function( assert ) {
	QUnit.stop();
	// Unregister error handler!
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// insert test data
    var new_user = {username:"usr_grp_crt_usr_8_serna",password:"PWD4!2%serna",password_confirmation:"PWD4!2%serna",first_name:"First Name",last_name:"Last Name", email:"somemail@damler.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",active:1,last_login:"2014-01-14 16:46", logins: 15,
		unlock_code:"123123",role:1,newsletter:2,force_pwd_change:2, group : []};

	// expected data
	var expected_user = jQuery.extend({created_at:"", updated_at:""}, new_user);
	delete expected_user.password;
	delete expected_user.group;
	var expected_result = {"error": true, "message": "", "response": {"password":["The password cannot contain part of the username"]}};
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "user/create";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(new_user),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
			assert.equal(XMLHttpRequest.status, 400, "HTTP Status code 400.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
		}, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
          } // if
          else { // login was successful3
			  expected_user.id	= data.response.id;
			  assert.ok(false, "error");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(expected_user.id)
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + expected_user.id);
			logoutUser();
		} // always
      });
	
});

QUnit.test("DBDN REST Create User - validation faild password no special sign", 6, function( assert ) {
	QUnit.stop();
	// Unregister error handler!
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// insert test data
    var new_user = {username:"usr_grp_crt_usr_9_serna",password:"PWD42serna",password_confirmation:"PWD42serna",first_name:"First Name",last_name:"Last Name", email:"somemail@damler.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",active:1,last_login:"2014-01-14 16:46", logins: 15,
		unlock_code:"123123",role:1,newsletter:2,force_pwd_change:2, group : []};

	// expected data
	var expected_user = jQuery.extend({created_at:"", updated_at:""}, new_user);
	delete expected_user.password;
	delete expected_user.group;
	var expected_result = {"error": true, "message": "", "response": {"password":["The password cannot contain part of the username","Invalid password: minimum 1 special character required (!\"#$%&'()*+,%;:=?_@>-)"]}};
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "user/create";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(new_user),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
			assert.equal(XMLHttpRequest.status, 400, "HTTP Status code 400.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
		}, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
          } // if
          else { // login was successful3
			  expected_user.id	= data.response.id;
			  assert.ok(false, "error");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(expected_user.id)
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + expected_user.id);
			logoutUser();
		} // always
      });
	
});

QUnit.test("DBDN REST Create User - validation faild password no count", 6, function( assert ) {
	QUnit.stop();
	// Unregister error handler!
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// insert test data
    var new_user = {username:"usr_grp_crt_usr_10_serna",password:"PWDserna@$",password_confirmation:"PWDserna@$",first_name:"First Name",last_name:"Last Name", email:"somemail@damler.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",active:1,last_login:"2014-01-14 16:46", logins: 15,
		unlock_code:"123123",role:1,newsletter:2,force_pwd_change:2, group : []};

	// expected data
	var expected_user = jQuery.extend({created_at:"", updated_at:""}, new_user);
	delete expected_user.password;
	delete expected_user.group;
	var expected_result = {"error": true, "message": "", "response": {"password":["Invalid password: minimum 1 digit required","The password cannot contain part of the username"]}};
	
	// url string
    var urlREST =  SERVER_INDEX_URL + "user/create";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(new_user),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
			assert.equal(XMLHttpRequest.status, 400, "HTTP Status code 400.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
		}, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			expected_user.id	= data.response.id;
			assert.deepEqual(data, expected_result, "User add validate faild result");
          } // if
          else { // login was successful3
			  expected_user.id	= data.response.id;
			  assert.ok(false, "error");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(expected_user.id)
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + expected_user.id);
			logoutUser();
		} // always
      });
	
});

QUnit.test("DBDN REST Create Group - success", 6, function( assert ) {
	QUnit.stop();
	loginUser();
	// insert test data
    var new_group = {name:"Admins",visible:1,active:1};

	// expected data
	var expected_group = jQuery.extend({created_at:"", updated_at:""}, new_group);
	var expected_result = jQuery.extend({"error": false, "message": "Group was added.", "response": null}, {"response": expected_group});
		
    // url string    
	var urlREST =  SERVER_INDEX_URL + "group/create";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(new_group),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			expected_group.id	= data.response.id;
			expected_result.response.created_at = data.response.created_at;
			expected_result.response.updated_at = data.response.updated_at;
        	assert.deepEqual(data, expected_result, "Group add result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(expected_group.id)
				deleteObject(SERVER_INDEX_URL + "group/forcedelete/" + expected_group.id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Delete User - success", 6, function( assert ) {
	QUnit.stop();
	loginUser();
	// Insert user for delete:
    var delete_user = {username:"usr_grp_del_usr",password:"P1!as123sword",password_confirmation:"P1!as123sword",
        first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com", company:"Daimler", position:"developer",
        department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",
        last_login:"2014-01-14 16:46:45", last_session: null, logins: 15,
        role:1,
        newsletter:2, 
        newsletter_include: 0,
        newsletter_lang: null,
        newsletter_time: "0000-00-00 00:00:00", 
        active:1, force_pwd_change:2, 
        unsubscribe_code: null,
        group : []};

    var delete_user = insertObject(delete_user, SERVER_INDEX_URL + "user/create");
	var user_addtion = {
		"force_pwd_change": 1,
		"last_session": null,
		"last_login": "-",		
        "logins": 0,
        "newsletter": 2,
        "newsletter_code": "",
        "newsletter_include": 0,
        "newsletter_lang": null,
        "newsletter_time": "0000-00-00 00:00:00",
        "unsubscribe_code": null,
	};
	delete_user = jQuery.extend(delete_user, user_addtion);

	// expected data
    var expected_result = jQuery.extend(
        {"error": false, "message": "User was deleted!", "response": null}, 
        {"response": delete_user}
    );
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "user/delete/" + delete_user.id;
    
    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			expected_result.response.newsletter_code = '';
			expected_result.response.unlock_code = '';
			expected_result.response.updated_at = data.response.updated_at;

			assert.deepEqual(data, expected_result, "User delete result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + delete_user.id);			
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Delete User - id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required!"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "user/delete" ;
    
    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "User delete result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "User delete result");
          } // if
          else { // login was successful
        	assert.ok(false, "User delete ID check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Delete User - not found", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "User not found."};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "user/delete/" + 10000000;
    
    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "User delete result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "User delete result");
          } // if
          else { // login was successful
        	assert.ok(false, "User delete unexisting page check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Delete Group - success", 6, function( assert ) {
	QUnit.stop();
	loginUser();
	// Insert page for delete:
    var delete_group = {name:"Admins",active:1,visible:1};
    var delete_group = insertObject(delete_group, SERVER_INDEX_URL + "group/create");
   
	// expected data
	var expected_result = jQuery.extend({"error": false, "message": "Group was deleted!", "response": null}, {"response": delete_group});
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "group/delete/" + delete_group.id;
    
    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
			// delete test data 
			if(delete_group.id)
				deleteObject(SERVER_INDEX_URL + "group/forcedelete/" + delete_group.id);
          } // if
          else { // login was successful
			  if(delete_group.id)
				deleteObject(SERVER_INDEX_URL + "group/forcedelete/" + delete_group.id);
			logoutUser();
			assert.deepEqual(data, expected_result, "Group delete result");
          } //else
        } // success
      });
});

QUnit.test("DBDN REST Delete Group - id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required!"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "group/delete" ;
    
    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Group delete result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Group delete result");
          } // if
          else { // login was successful
        	assert.ok(false, "Group delete ID check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Delete Group - not found", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Group not found."};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "group/delete/" + 999999;
    
    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "User delete result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Group delete result");
          } // if
          else { // login was successful
        	assert.ok(false, "Group delete unexisting page check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Update User - success New password", 6, function( assert ) {
	QUnit.stop();
	loginUser();
	// Insert page for delete:
   var update_user = {username:"usr_grp_upd_usr_2", password:"P1!as123sword", password_confirmation:"P1!as123sword",
			first_name:"Firts Name Update", last_name:"Last Name Update", email:"somemail@gmail.com",
			company:"Daimler", position:"developer",
			role:1, active:1};
	
	var user = {username:"usr_grp_upd_usr_1", password:"P1!as123sword2", password_confirmation:"P1!as123sword2",
			first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",
			company:"Daimler", position:"developer", department:"IT",
			address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",
			role:1, force_pwd_change:2, group : []};
    var user = insertObject(user, SERVER_INDEX_URL + "user/create");
   
    //delete update_user.password;
    update_user = jQuery.extend(user, update_user);
   
	// expected data
	var expected_user = update_user;
	var user_addtion = {
		"force_pwd_change": 1,
		"last_session": null,
		"last_login": "-",		
        "logins": 0,
        "newsletter": 0,
        "newsletter_code": "",
        "newsletter_include": 0,
        "newsletter_lang": null,
        "newsletter_time": "0000-00-00 00:00:00",
        "unsubscribe_code": null,
	};
	expected_user = jQuery.extend(expected_user, user_addtion);

	//delete expected_user.password;
	var expected_result = jQuery.extend({"error": false, "message": "User was updated.", "response": null}, 
										{"response": expected_user});
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "user/update";
    
    jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(update_user),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				delete expected_result.response.password;
				delete expected_result.response.password_confirmation;
				  
			  	expected_result.response.unlock_code = data.response.unlock_code;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				delete expected_user.group;

				assert.deepEqual(data, expected_result, "User update result");
			} //else
        }, // success
		complete: function(data){
			// delete test data 
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + user.id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Update User - success Old password", 6, function( assert ) {
	QUnit.stop();
	loginUser();
	// Insert page for delete:
   var update_user = {username:"usr_grp_upd_usr_4",password:"P1!as123sword",password_confirmation:"P1!as123sword",
		first_name:"Firts Name Update",last_name:"Last Name", email:"somemail@gmail.com",
		company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",
		role:1, active:1, group : []};
	
	var user = {username:"usr_grp_upd_usr_3",password:"P1!as123sword2",password_confirmation:"P1!as123sword2",
		first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",
		role:1, group :[]};
    var user = insertObject(user, SERVER_INDEX_URL + "user/create");
   
    delete update_user.password;
    update_user = jQuery.extend(user, update_user);
   
	// expected data
	var expected_user = update_user;
	var user_addtion = {
		"force_pwd_change": 1,
		"last_session": null,
		"last_login": "-",		
        "logins": 0,
        "newsletter": 0,
        "newsletter_code": "",
        "newsletter_include": 0,
        "newsletter_lang": null,
        "newsletter_time": "0000-00-00 00:00:00",
        "unsubscribe_code": null,
	};
	expected_user = jQuery.extend(expected_user, user_addtion);

	var expected_result = jQuery.extend({"error": false, "message": "User was updated.", "response": null}, 
										{"response": expected_user});
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "user/update";
    
    jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(update_user),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				delete expected_result.response.password_confirmation;
				expected_result.response.unlock_code = data.response.unlock_code;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				delete expected_user.group;

				assert.deepEqual(data, expected_result, "User update result");
			} //else
        }, // success
		complete: function(data){
			// delete test data 
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + user.id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Update User - success Empty password", 6, function( assert ) {
	QUnit.stop();
	loginUser();
	// Insert page for delete:
	var update_user = {
		username: "usr_grp_upd_usr_6", password: "", password_confirmation: "",
		first_name: "Firts Name Update", last_name: "Last Name", email: "somemail@gmail.com",
		company: "Daimler", position: "developer", department: "IT",
		address: "somewhere", code: "ST", city: "Stuttgart", country: "Germany", phone: "0889285", fax: "32423434",
		role: 1, active: 1, group: []
	};
	
	var user = {
		username: "usr_grp_upd_usr_5", password: "P1!as123sword2", password_confirmation: "P1!as123sword2", 
		first_name: "Firts Name", last_name: "Last Name", email: "somemail@gmail.com", 
		company: "Daimler", position: "developer", department: "IT", 
		address: "somewhere", code: "ST", city: "Stuttgart", country: "Germany", phone: "0889285", fax: "32423434",
		role: 1, group: []
	};
    var user = insertObject(user, SERVER_INDEX_URL + "user/create");
   
    update_user = jQuery.extend(user, update_user);
   
	// expected data
	var expected_user = update_user;
	var user_addtion = {
		"force_pwd_change": 1,
		"last_session": null,
		"last_login": "-",		
        "logins": 0,
        "newsletter": 0,
        "newsletter_code": "",
        "newsletter_include": 0,
        "newsletter_lang": null,
        "newsletter_time": "0000-00-00 00:00:00",
        "unsubscribe_code": null,
	};
	expected_user = jQuery.extend(expected_user, user_addtion);

	var expected_result = jQuery.extend({"error": false, "message": "User was updated.", "response": null}, {"response": expected_user});
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "user/update";
    
    jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(update_user),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				delete expected_result.response.password;
				delete expected_result.response.password_confirmation;
				expected_result.response.unlock_code = data.response.unlock_code;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				delete expected_user.group;

				assert.deepEqual(data, expected_result, "User update result");
			} //else
        }, // success
		complete: function(data){
			// delete test data 
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + user.id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Update User - id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// test fields
    var update_user = {};

	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required!"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "user/update";
    
    jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(update_user),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "User update result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "User update result");
          } // if
          else { // login was successful
        	assert.ok(false, "user update ID check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Update User - not found", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// test fields
    var update_user = {id:7};

	// expected data
	var expected_result = jQuery.extend({"error": true, "message": "User not found.", "response": null}, 
										{"response": update_user});
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "user/update";
    
    jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(update_user),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "User update result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "User update result");
          } // if
          else { // login was successful
        	assert.ok(false, "User update unexisting page check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Update Group - success", 6, function( assert ) {
	QUnit.stop();
	loginUser();
   var update_group = {name:"Admins",active:1,visible:1};
	
	var group = {name:"Admins 2",active:2,visible:2};
    var group = insertObject(group, SERVER_INDEX_URL + "group/create");
   
    update_group = jQuery.extend(update_group, {id:group.id});
   
	// expected data
	var expected_group = update_group;
	var expected_result = jQuery.extend({"error": false, "message": "Group was updated.", "response": null}, {"response": expected_group});
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "group/update";
    
    jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(update_group),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			expected_result.response.created_at = data.response.created_at;
			expected_result.response.updated_at = data.response.updated_at;
        	assert.deepEqual(data, expected_result, "Group update result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			if(group.id)
				deleteObject(SERVER_INDEX_URL + "group/forcedelete/" + group.id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Update Group - id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// test fields
    var update_group = {};

	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required!"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "group/update";
    
    jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(update_group),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Group update result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Group update result");
          } // if
          else { // login was successful
        	assert.ok(false, "Group update ID check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Update Group - not found", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// test fields
    var update_group = {id:999};

	// expected data
	var expected_result = jQuery.extend({"error": true, "message": "Group not found.", "response": null}, {"response": update_group});
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "group/update";
    
    jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(update_group),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Group update result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Group update result");
          } // if
          else { // login was successful
        	assert.ok(false, "Group update unexisting page check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Get All Users with filter - success", 6, function( assert ) {
	QUnit.stop();
	loginUser();
	// insert test data
	var username_prefix  = "usr_grp_gt_all_flt";
	var user1 = {username: "usr_grp_gt_all_flt_1",password:"P1!as123sword",password_confirmation:"P1!as123sword",
		first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",
		company:"Daimler", position:"developer", department:"IT",
		address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",
		role:1,newsletter:2, active: 1, group : []};
	var user1 = insertObject(user1, SERVER_INDEX_URL + "user/create");
	
	var user2 = {username:"usr_grp_gt_all_flt_2",password:"P1!as123sword",password_confirmation:"P1!as123sword",
		first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",
		company:"Daimler", position:"developer", department:"IT",
		address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",
		role:1,newsletter:2, active: 1, group : []};
	var user2 = insertObject(user2, SERVER_INDEX_URL + "user/create");
	
	var user3 = {username:"usr_grp_gt_all_flt_3",password:"P1!as123sword",password_confirmation:"P1!as123sword",
		first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",
		company:"Daimler", position:"developer", department:"IT",
		address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",
		role:1,newsletter:2, active: 1, group : []};
	var user3 = insertObject(user3, SERVER_INDEX_URL + "user/create");
	
	// expected data
	var fieldsToKeep = [
		'id', 'username', 'first_name', 'last_name', 'role',
		'newsletter', 'newsletter_include', 'active', 'last_login', 'logins'
	];
	var user_addtion = {
		"last_login": "-",		
        "logins": 0,
        "newsletter_include": 0
	};
	
	var expected_user = [user1, user2, user3];
	// Remove not needed fields
	for(var i=0;i < expected_user.length;i++){
		for (var property in expected_user[i]) {
			if (expected_user[i].hasOwnProperty(property)) {
				if (fieldsToKeep.indexOf(property) == -1) {
					delete expected_user[i][property];
				}
			}
		}
		expected_user[i] = jQuery.extend(expected_user[i], user_addtion);
	}	
	var expected_result = jQuery.extend({"error": false, "message": "List of Users found", "response": null}, 
										{"response": expected_user});
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "user/all?filter=" + username_prefix;
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			//assert.notDeepEqual(data, expected_result, "Page list result");
			//check (error, message) - strict; response - must be array
			assert.deepEqual(data, expected_result, "user read result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + user1.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + user2.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + user3.id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Get user from Group - success", 6, function( assert ) {
	QUnit.stop();
	loginUser();
	// insert test data
	var user1 = {
		username: "usr_grp_gt_usr_grp_1", password: "P1!as123sword", password_confirmation: "P1!as123sword", 
		first_name: "Firts Name", last_name: "Last Name", email: "somemail@gmail.com", 
		company: "Daimler", position: "developer", department: "IT", 
		address: "somewhere", code: "ST", city: "Stuttgart", country: "Germany", phone: "0889285", fax: "32423434",
		role: 1, newsletter: 2, active: 1, group: []
	};
    var user1 = insertObject(user1, SERVER_INDEX_URL + "user/create");
	
	var user2 = {
		username: "usr_grp_gt_usr_grp_2", password: "P1!as123sword", password_confirmation: "P1!as123sword", 
		first_name: "Firts Name", last_name: "Last Name", email: "somemail@gmail.com", 
		company: "Daimler", position: "developer", department: "IT", 
		address: "somewhere", code: "ST", city: "Stuttgart", country: "Germany", phone: "0889285", fax: "32423434",
		role: 1, newsletter: 2, active: 1, group: []
	};
    var user2 = insertObject(user2, SERVER_INDEX_URL + "user/create");
	
	var user3 = {
		username: "usr_grp_gt_usr_grp_3", password: "P1!as123sword", password_confirmation: "P1!as123sword", 
		first_name: "Firts Name", last_name: "Last Name", email: "somemail@gmail.com", 
		company: "Daimler", position: "developer", department: "IT", 
		address: "somewhere", code: "ST", city: "Stuttgart", country: "Germany", phone: "0889285", fax: "32423434",
		role: 1, newsletter: 2, active: 1, group: []
	};
    var user3 = insertObject(user3, SERVER_INDEX_URL + "user/create");
	
	var group = {name:"Admins",active:1,visible:1};
    var group = insertObject(group, SERVER_INDEX_URL + "group/create");
	
	addUserToGroup(user1.id, group.id);
	addUserToGroup(user2.id, group.id);
	addUserToGroup(user3.id, group.id);
	
	// expected data
	var user_addtion = {
		"force_pwd_change": 1,
		"last_session": null,
		"last_login": "-",		
        "logins": 0,
        "newsletter": 2,
        "newsletter_code": "",
        "newsletter_include": 0,
        "newsletter_lang": null,
        "newsletter_time": "0000-00-00 00:00:00",
        "unsubscribe_code": null,
	};
	user1 = jQuery.extend(user1, user_addtion);
	user2 = jQuery.extend(user2, user_addtion);
	user3 = jQuery.extend(user3, user_addtion);
	var expected_user = [user1, user2, user3];
	var expected_result = jQuery.extend({ "error": false, "message": "Group Users found", "response": null }, 
										{ "response": expected_user });
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "user/readfromgroup/"+group.id;
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			//assert.notDeepEqual(data, expected_result, "Page list result");
			//check (error, message) - strict; response - must be array
			assert.deepEqual(data, expected_result, "user read result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			removeUserFromGroup(user1.id,group.id);
			removeUserFromGroup(user2.id,group.id);
			removeUserFromGroup(user3.id,group.id);
			deleteObject(SERVER_INDEX_URL + "group/forcedelete/" + group.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + user1.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + user2.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + user3.id);
			logoutUser();
		} // always
      });
});

QUnit.test("DBDN REST Users from group - id required", 6, function( assert ) {

	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var group = {name:"Admins",active:1,visible:1};
    var group = insertObject(group, SERVER_INDEX_URL + "group/create");
	
	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required!"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "user/readfromgroup" ;
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Users read result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "users read result");
          } // if
          else { // login was successful
        	assert.ok(false, "Users read ID check fail!");
          } //else
        }, // success
		complete: function(data){
			deleteObject(SERVER_INDEX_URL + "group/forcedelete/" + group.id);
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Group by User id - success", 6, function( assert ) {

	QUnit.stop();
	loginUser();
	// insert test data
	var user = {username:"usr_grp_grp_by_usr_1",password:"P1!as123sword",password_confirmation:"P1!as123sword",first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",last_login:"2014-01-14 16:46:45", logins: 15,
		role:1,newsletter:2,force_pwd_change:2, group : []};
    var user = insertObject(user, SERVER_INDEX_URL + "user/create");
	
	var group = {name:"Admins",active:1,visible:1};
    var group = insertObject(group, SERVER_INDEX_URL + "group/create");
	
	addUserToGroup(user.id,group.id);
	
	var group_default = {id: 1,name:"Member",active:1,visible:1};
	
	// expected data
	var expected_group = [group_default,group];
	var expected_result = jQuery.extend({"error": false, "message": "Groups found", "response": null}, {"response": expected_group});
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "group/readbyuserid/" + user.id;
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			expected_result.response[0].created_at = data.response[0].created_at;
			expected_result.response[0].updated_at = data.response[0].updated_at;
			expected_result.response[1].created_at = data.response[1].created_at;
			expected_result.response[1].updated_at = data.response[1].updated_at;
			assert.deepEqual(data, expected_result, "Group read result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			removeUserFromGroup(user.id,group.id);
			removeUserFromGroupForce(user.id,group.id);
			removeUserFromGroup(user.id,1);
			removeUserFromGroupForce(user.id,1);
			deleteObject(SERVER_INDEX_URL + "group/forcedelete/" + group.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + user.id);
			logoutUser();
		} // always
      });
});



QUnit.test("DBDN REST Group by User id - id required", 6, function( assert ) {

	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required!"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "group/readbyuserid";
    
    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Group read result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Group read result");
          } // if
          else { // login was successful
        	assert.ok(false, "Group read unexisting page check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Add User to group - success", 6, function( assert ) {

	QUnit.stop();
	loginUser();
	// insert test data
	var user = {username:"usr_grp_add_usr_to_grp",password:"P1!as123sword",password_confirmation:"P1!as123sword",first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",last_login:"2014-01-14 16:46:45", logins: 15,
		role:1,newsletter:2,force_pwd_change:2, group : []};
    var user = insertObject(user, SERVER_INDEX_URL + "user/create");
	
	var group = {name:"Admins",active:1,visible:1};
    var group = insertObject(group, SERVER_INDEX_URL + "group/create");
	
	var usergroupitem = {user_id:user.id, group_id:group.id}
	
	// expected data
	var expected_item = usergroupitem;
	var expected_result = jQuery.extend({"error": false, "message": "User was added to group.", "response": null}, {"response": expected_item});
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "group/addmember";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(usergroupitem),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			expected_result.response.created_at = data.response.created_at;
			expected_result.response.updated_at = data.response.updated_at;
			assert.deepEqual(data, expected_result, "Group add result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			removeUserFromGroup(user.id,group.id);
			removeUserFromGroup(user.id,1);
			removeUserFromGroupForce(user.id,group.id);
			removeUserFromGroupForce(user.id,1);
			deleteObject(SERVER_INDEX_URL + "group/forcedelete/" + group.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + user.id);
			logoutUser();
		} // always
      });
});



QUnit.test("DBDN REST Add user to Group - id required", 6, function( assert ) {

	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Parameter user_id is required!"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "group/addmember";
    
    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Group add result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Group add result");
          } // if
          else { // login was successful
        	assert.ok(false, "Group add unexisting page check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});

QUnit.test("DBDN REST Remove User from group - success", 6, function( assert ) {

	QUnit.stop();
	loginUser();
	// insert test data
	var user = {username:"usr_grp_rmv_usr_frm_grp",password:"P1!as123sword",password_confirmation:"P1!as123sword",first_name:"Firts Name",last_name:"Last Name", email:"somemail@gmail.com",company:"Daimler", position:"developer",
		department:"IT",address:"somewhere",code:"ST",city:"Stuttgart",country:"Germany",phone:"0889285",fax:"32423434",last_login:"2014-01-14 16:46:45", logins: 15,
		role:1,newsletter:2,force_pwd_change:2, group : []};
    var user = insertObject(user, SERVER_INDEX_URL + "user/create");
	
	var group = {name:"Admins",active:1,visible:1};
    var group = insertObject(group, SERVER_INDEX_URL + "group/create");
	
	addUserToGroup(user.id,group.id);
	
	var usergroupitem = {user_id:user.id, group_id:group.id}
	
	// expected data
	var expected_item = usergroupitem;
	var expected_result = jQuery.extend({"error": false, "message": "User was deleted from group!", "response": null}, {"response": expected_item});
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "group/removemember";
    
    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(usergroupitem),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        	assert.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
        	assert.ok(false, "error");
          } // if
          else { // login was successful
			expected_result.response.created_at = data.response.created_at;
			expected_result.response.updated_at = data.response.updated_at;
			assert.deepEqual(data, expected_result, "Group remove result");
          } //else
        }, // success
		complete: function(data){
			// delete test data 
			removeUserFromGroup(user.id,1);
			removeUserFromGroupForce(user.id,group.id);
			removeUserFromGroupForce(user.id,1);
			deleteObject(SERVER_INDEX_URL + "group/forcedelete/" + group.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/" + user.id);
			logoutUser();
		} // always
      });
});



QUnit.test("DBDN REST Remove User from Group - id required", 6, function( assert ) {

	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Parameter user_id is required!"};
	
    // url string    
	var urlREST =  SERVER_INDEX_URL + "group/removemember";
    
    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Group remove result");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Group remove result");
          } // if
          else { // login was successful
        	assert.ok(false, "Group remove unexisting page check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
      });
});
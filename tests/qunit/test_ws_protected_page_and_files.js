QUnit.module("Test REST Protected (Files & Pages) Web Services", {
  setup: function() {
    // prepare something for all following tests
	jQuery.ajaxSetup({ timeout: 5000 });

	jQuery( document ).ajaxStart(function(){
		ok( true, "ajaxStart" );
	}).ajaxStop(function(){
		ok( true, "ajaxStop" );
		QUnit.start();
	}).ajaxSend(function(){
		ok( true, "ajaxSend" );
	}).ajaxComplete(function(event, request, settings){
		ok( true, "ajaxComplete: " + request.responseText );
	}).ajaxError(function(){
		ok( false, "ajaxError" );
	}).ajaxSuccess(function(){
		ok( true, "ajaxSuccess" );
	});
  },
  teardown: function() {
    // clean up after each test
	jQuery( document ).unbind('ajaxStart');
	jQuery( document ).unbind('ajaxStop');
	jQuery( document ).unbind('ajaxSend');
	jQuery( document ).unbind('ajaxComplete');
	jQuery( document ).unbind('ajaxError');
	jQuery( document ).unbind('ajaxSuccess');
  }
});

QUnit.test("DBDN REST Create protected page - success", 6, function(assert) {		//36
	QUnit.stop();
	loginUser();
	var new_page = {parent_id: 0, slug: "Test_Page", type: "File", active: 0, position: 1, in_navigation: 1, authorization: 1, home_accordion: 0, home_block: 0,
		visits: 1, overview: 1, template: "Template 1", langs: 2, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, "file_type": null,"is_visible": 1,
        "usage": null, "version": 0};

	// expected data
	var expected_page = jQuery.extend({u_id:2, id: 2, created_by: 2, created_at: "", updated_at: "", restricted: 1,  assigned_by: "",
    "due_date": "No Due Date", "editing_state": "Draft"}, new_page);

    new_page.access = [{group_id: "4"}, {group_id: "5"}];
	var expected_result = jQuery.extend({"error": false, "message": "Page was added.", "response": null}, {"response": expected_page});

	// url string
	var urlREST = SERVER_INDEX_URL + "pages/create";

	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(new_page),
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function (data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_page.u_id = data.response.u_id;
				expected_result.response.id = data.response.id;
                expected_result.response.u_id = data.response.u_id;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
                expected_result.response.workflow = data.response.workflow;
				//delete expected_result.response.access;
				assert.deepEqual(data, expected_result, "Page add result");
			} //else
		}, // success
		complete: function (data) {
			// delete test data
			if (expected_page.u_id) {
                deleteAccess(expected_page.u_id);
                forceDeletePage(expected_page.u_id);
			}
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Create page add protection - success", 6, function(assert) {		//36
	QUnit.stop();
	loginUser();
	var new_page = {parent_id: 0, slug: "Test_Page", type: "File", active: 0, position: 1, in_navigation: 1, authorization: 1, home_accordion: 0, home_block: 0,
		visits: 1, overview: 1, template: "Template 1", langs: 2, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, "file_type": null,"is_visible": 1,
        "usage": null, "version": 0};

    // expected data
    var expected_page = insertObject(new_page, SERVER_INDEX_URL + "pages/create");
    expected_page =jQuery.extend({ "unpublish_date": "0000-00-00 00:00:00",  "publish_date": "0000-00-00 00:00:00", "front_block": 0,
        "end_edit": null,  "featured_menu": 0, "user_edit_id": null, "visits_search": 0,}, expected_page);
    expected_page.restricted = 1;
	var expected_result = jQuery.extend({"error": false, "message": "Page was updated.", "response": null}, {"response": expected_page});

    new_page = jQuery.extend({access : [{group_id: "4"}, {group_id: "5"}], id: expected_page.id, u_id: expected_page.u_id },new_page);
	// url string
	var urlREST = SERVER_INDEX_URL + "pages/update";

	jQuery.ajax({
		type: "PUT",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(new_page),
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function (data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Page update result");
			}
		},
		complete: function (data) {
			// delete test data
			if (expected_page.u_id) {
                deleteAccess(expected_page.u_id);
                forceDeletePage(expected_page.u_id);
			}
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Remove protection from page - success", 6, function(assert) {		//36
	QUnit.stop();
	loginUser();
	var new_page = {parent_id: 0, slug: "Test_Page", type: "File", active: 0, position: 1, in_navigation: 1, authorization: 1, home_accordion: 0, home_block: 0,
		visits: 1, overview: 1, template: "Template 1", langs: 2, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, "file_type": null,"is_visible": 1,
        "usage": null, "version": 0, access : [{group_id: "4"}, {group_id: "5"}]};

    // expected data
    var expected_page = insertObject(new_page, SERVER_INDEX_URL + "pages/create");
    expected_page =jQuery.extend({ "unpublish_date": "0000-00-00 00:00:00",  "publish_date": "0000-00-00 00:00:00", "front_block": 0,
        "end_edit": null,  "featured_menu": 0, "user_edit_id": null, "visits_search": 0,}, expected_page);
    expected_page.restricted = 0;
    expected_page.authorization = 0;
	var expected_result = jQuery.extend({"error": false, "message": "Page was updated.", "response": null}, {"response": expected_page});

    new_page = jQuery.extend({id: expected_page.id, u_id: expected_page.u_id },new_page);
    new_page.access = [];
	// url string
	var urlREST = SERVER_INDEX_URL + "pages/update";

	jQuery.ajax({
		type: "PUT",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(new_page),
		// script call was *not* successful
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function (data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Page update result");
			}
		},
		complete: function (data) {
			// delete test data
			if (expected_page.u_id) {
                deleteAccess(expected_page.u_id);
                forceDeletePage(expected_page.u_id);
			}
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Create protected file - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
	var dirname = {dirname:UNIT_TEST_DIR, path: "/"};
   // deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads",'/'+dirname.dirname+'/');
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");
	var filename = "test22.ico";
    var fileInfo = {
        currentpath: "/"  + UNIT_TEST_DIR + "/",
        'title[1]': "Title",
        'title[2]' : "Title",
        'description[1]' : "Description",
        'description[2]' : "Description",
        format : "4:3",
        deleted : 0,
        dimensions : "128x128",
        dimension_units : "pixels",
        resolution : "20",
        resolution_units : "dpi",
        color_mode : "RGB",
        lang : "EN,DE",
        pages : "3",
        version:"1.2",
        copyright_notes: "",
        usage_terms_de:"",
        usage_terms_en:"",
        software: "",
        filename: filename,
        file_title: 0,
        file_zoom_title:"",
        file_thumb_title:"",
        mode: 'downloads',
        zoompic: "",
        thumbnail: "",
        thumb_visible: 0,
        end_edit: "0000-00-00 00:00:00",
        protected: 1
    }
    var formData = new FormData();

    for (prop in fileInfo) {
        if(fileInfo.hasOwnProperty(prop)){
            formData.append(prop, fileInfo[prop]);
        }
    }

    var file = "data:image/x-icon;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKX/CACl/2AApf9oAKX/YACl/14Apf9kAKX/ZgCl/w4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKX/IACl/3IApf8OAKX/QACl/3wApf+AAKX/SgCl/wwApf9uAKX/LgAAAAAAAAAAAAAAAAAAAAAAAAAAAKX/DACl/3AApf8WAKX/wACl//8Apf//AKX//wCl//8Apf/QAKX/JACl/2gApf8WAAAAAAAAAAAAAAAAAAAAAACl/2QApf8MAKX/xACl//8Apf//AKX//wCl//8Apf//AKX//wCl/9wApf8OAKX/agAAAAAAAAAAAAAAAAAAAAAApf9oAKX/SgCl//8Apf//AKX/tgCl/6wApf//AKX//wCl//8Apf//AKX/ZACl/2YAAAAAAAAAAAAAAAAAAAAAAKX/ZgCl/4AApf//AKX//wCl/44Apf+AAKX//wCl//oApf/4AKX//wCl/5gApf9aAKX/CAAAAAAAAAAAAAAAAACl/2YApf94AKX//wCl//8Apf//AKX//wCl//8Apf+0AKX/pACl//8Apf+QAKX/YgCl/wIAAAAAAAAAAAAAAAAApf9oAKX/GgCl/1AApf9QAKX/TgCl/04Apf9OAKX/TgCl/04Apf9OAKX/IACl/2oAAAAAAAAAAAAAAAAAAAAAAKX/OgCl/zgAAAAAAAAAAAAAAAAApf+EAKX/VgAAAAAAAAAAAAAAAACl/zAApf9EAAAAAAAAAAAAAAAAAAAAAAAAAAAApf9kAKX/KgAAAAAAAAAAAKX/WgCl/2QApf9CAAAAAACl/yYApf9oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACl/1QApf9oAKX/IAAAAAAApf8wAKX/VgCl/2gApf9YAKX/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKX/BgCl/4QAAAAAAAAAAACl/3YApf8IAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAApf9gAKX/TgCl/2QApf9cAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKX/YACl/zoApf9QAKX/XAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKX/IACl/4YAAAAAAAAAAACl/3IApf8uAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACl/zQApf9wAKX/YACl/2IApf9kAKX/SgAAAAAAAAAAAAAAAAAAAAAAAAAA//8AAP9/AAD4HwAA8A8AAPAPAADgBwAA8AcAAP//AAD+/wAA//8AAP//AAD9/wAA//8AAP//AAD9/wAA//8AAA==";
    var fileBlob = dataURItoBlob(file);
    formData.append("files[]", fileBlob, filename);
    fileInfo = jQuery.extend({"type":  "downloads","size": 1150,"extension": "ico",}, fileInfo);
    delete fileInfo['description[1]'];
    delete fileInfo['description[2]'];
    fileInfo.filename = fileInfo.currentpath + filename;
    delete fileInfo.currentpath;
    delete fileInfo.file_thumb_title;
    delete fileInfo.file_title;
    delete fileInfo.file_zoom_title;
    delete fileInfo.mode;
    delete fileInfo['title[1]'];
    delete fileInfo['title[2]'];
    fileInfo.deleted = '0';
    fileInfo.thumb_visible = 1;
    var expected_result = jQuery.extend({"error": false, "message": 'File was added.', "response": null}, {"response": fileInfo});
    var urlREST = SERVER_INDEX_URL + "files/createfile";
    var file;
    var result_promise = $.ajax({
        url : urlREST,
        type : 'POST',
        data : formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        success: function(data) {
            if (data.error) { // script returned error
                assert.ok(false, "error");
            } // if
            else { // login was successful
                file = data.response;
                expected_result.response.updated_at = file.updated_at;
                expected_result.response.created_at = file.created_at;
                expected_result.response.id = file.id
                assert.deepEqual(data, expected_result, "Protected file created");
            }
        },
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            QUnit.ok(false, "error: " + textStatus + " : " + errorThrown);
        }, // error
        complete: function(data){
            if(file)
				deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file.id);
            deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads",'/'+dirname.dirname+'/');
            logoutUser();
        } // always
    });
});

QUnit.test("DBDN REST Delete protected file - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
	var dirname = {dirname:UNIT_TEST_DIR, path: "/"};
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");
    insertProtectedFile(SERVER_INDEX_URL + "files/createfile", "test22.ico",function(data){
        var file = jQuery.extend({"brand_id": 0, "category_id": 0, "user_edit_id": null, "visible": 0},data.response);
        file.deleted = 0;
        file.type = "Downloads";
        var expected_result = jQuery.extend({"error": false, "message": 'File was deleted.', "response": null}, {"response": file});
        var urlREST = SERVER_INDEX_URL + "files/deletefile/"+file.id;
        $.ajax({
            url : urlREST,
            type : 'DELETE',
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function(data) {
                if (data.error) { // script returned error
                    assert.ok(false, "error");
                } // if
                else { // login was successful
                    expected_result.response.updated_at = data.response.updated_at;
                    expected_result.response.filename = data.response.filename;
                    assert.deepEqual(data, expected_result, "Protected file deleted");
                }
            },
            // script call was *not* successful
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                QUnit.ok(false, "error: " + textStatus + " : " + errorThrown);
            }, // error
            complete: function(data){
                if(file)
                    deleteObject(SERVER_INDEX_URL + "files/permanentdelete/"+file.id);
                deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads",'/'+dirname.dirname+'/');
                logoutUser();
            } // always
        });
    });
});

QUnit.test("DBDN REST Add protected file to protected page - success", 6, function(assert) {		//36
	QUnit.stop();
	loginUser();
	var page = {parent_id: 0, slug: "Test_Page", type: "File", active: 0, position: 1, in_navigation: 1, authorization: 1, home_accordion: 0, home_block: 0,
		visits: 1, overview: 1, template: "Template 1", langs: 2, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, "file_type": null,"is_visible": 1,
        "usage": null, "version": 0, access : [{group_id: "4"}, {group_id: "5"}]};
    page = insertObject(page, SERVER_INDEX_URL + "pages/create");

    var dirname = {dirname:UNIT_TEST_DIR, path: "/"};
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");

    insertProtectedFile(SERVER_INDEX_URL + "files/createfile", "test22.ico",function(file){
        var download = {page_id : page.id, page_u_id: page.u_id, file_id : file.response.id, position : 1};
		var expected_result = jQuery.extend({"error":false, "message": "Download was added.", "response": null}, {"response": download});

		var urlREST = SERVER_INDEX_URL + "download/createdownload";

		jQuery.ajax({
			type: "POST",
			url: urlREST, // URL of the REST web service
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(download),
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			}, // error
			// script call was successful
			// data contains the JSON values returned by the Perl script
			success: function(data) {
				if (data.error) { // script returned error
					assert.ok(false, "error");
				} // if
				else { // login was successful
					expected_result.response.created_at = data.response.created_at;
					expected_result.response.updated_at = data.response.updated_at;

					assert.deepEqual(data, expected_result, "Download was added");
				} //else
			}, // success
			complete: function(data){
				forceDeleteDownload(page.id);
				deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file.response.id);
                deleteAccess(page.u_id);
				forceDeletePage(page.u_id);
				deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads",'/'+dirname.dirname+'/');
				logoutUser();
			} // always
		  });
    });
});

QUnit.test("DBDN REST Add protected file to unprotected page - fail", 6, function(assert) {		//36
	QUnit.stop();
	loginUser();
	var page = {parent_id: 1, slug: "Test_Page", type: "File", active: 0, position: 1, in_navigation: 1, authorization: 1, home_accordion: 0, home_block: 0,
		visits: 1, overview: 1, template: "Template 1", langs: 2, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, "file_type": null,"is_visible": 1,
        "usage": null, "version": 0 };
    page = insertObject(page, SERVER_INDEX_URL + "pages/create");

    var dirname = {dirname:UNIT_TEST_DIR, path: "/"};
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");

    insertProtectedFile(SERVER_INDEX_URL + "files/createfile", "test22.ico",function(file){
        var download = {page_id : page.id, page_u_id: page.u_id, downloads:[file.response.id]};

        var expected_result = {"error": true, "message": "Protected files cannot be attached to unprotected pages. Please change accordingly either the file or page properties."}

		var urlREST = SERVER_INDEX_URL + "download/recreate";
        jQuery( document ).unbind('ajaxError');

		jQuery.ajax({
			type: "POST",
			url: urlREST, // URL of the REST web service
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(download),
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			}, // error
			// script call was successful
			// data contains the JSON values returned by the Perl script
			success: function(data) {
                if (data.error) { // script returned error
                  assert.deepEqual(data, expected_result, "File wasn't add.");
                }
                else {
                  assert.ok(false, "Page add file check fail!");
                }
			},
			complete: function(data){
				forceDeleteDownload(page.id);
				deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file.response.id);
                deleteAccess(page.u_id);
				forceDeletePage(page.u_id);
				deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads",'/'+dirname.dirname+'/');
				logoutUser();
			} // always
		  });
    });
});

QUnit.test("DBDN REST Authorized user get protected page - success", 6, function(assert) {		//36
	QUnit.stop();
	loginUser();
	var page = {parent_id: 1, slug: "Test_Page", active: 0, position: 1, in_navigation: 1, authorization: 1, home_accordion: 0, home_block: 0,
		visits: 1, overview: 1, template: "best_practice", langs: 2, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, "file_type": null,"is_visible": 1,
        "usage": null, "version": 0, access : [{group_id: "4"}, {group_id: "5"}]};
    var dbPage = insertObject(page, SERVER_INDEX_URL + "pages/create");
    page.id = dbPage.id;
    page.u_id = dbPage.u_id;

    var dirname = {dirname:UNIT_TEST_DIR, path: "/"};
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");

    insertProtectedFile(SERVER_INDEX_URL + "files/createfile", "test22.ico",function(file){
        var download = {page_id : page.id, page_u_id: page.u_id, file_id : file.response.id, position : 1};
        download = insertObject(download, SERVER_INDEX_URL + "download/createdownload");
        var workflow = dbPage.workflow;
        workflow.editing_state = 'Approved';
        workflow.user_responsible = 2;
        workflow = insertObject(workflow, SERVER_INDEX_URL + "workflow/update", "PUT");

        page.active = 1;
        insertObject(page, SERVER_INDEX_URL + "pages/update", "PUT");
		var urlREST = SERVER_BASE_URL + page.u_id;

		jQuery.ajax({
			type: "GET",
            async: false,
			url: urlREST, // URL of the REST web service
			contentType: "application/json; charset=utf-8",
			dataType: "html",
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                assert.ok(false, "error: " + textStatus + " : " + errorThrown);
            },
			success: function(data, textStatus, jqXHR ) {
				//page is loaded
                if (jqXHR.status == 200 && data.search('method="post" id="loginForm"') === -1) { // script returned error
					assert.ok(true, "User get the page.");
				} // if
				else { // login was successful
                    assert.ok(false, "error");
				} //else
			}, // success
			complete: function(data){
				forceDeleteDownload(page.id);
				deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file.response.id);
                deleteAccess(page.u_id);
				forceDeletePage(page.u_id);
				deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads",'/'+dirname.dirname+'/');
				logoutUser();
			} // always
		  });
    });
});

QUnit.test("DBDN REST Not authorized user check protected page - fail", 6, function(assert) {		//36
	QUnit.stop();
	loginUser();
	var page = {parent_id: 1, slug: "Test_Page", active: 0, position: 1, in_navigation: 1, authorization: 1, home_accordion: 0, home_block: 0,
		visits: 1, overview: 1, template: "best_practice", langs: 2, deletable: 1, deleted: 0, searchable: 1, inline_glossary: 1, "file_type": null,"is_visible": 1,
        "usage": null, "version": 0, access : [{group_id: "4"}, {group_id: "5"}]};
    var dbPage = insertObject(page, SERVER_INDEX_URL + "pages/create");
    page.id = dbPage.id;
    page.u_id = dbPage.u_id;

    var dirname = {dirname:UNIT_TEST_DIR, path: "/"};
	insertObject(dirname, SERVER_INDEX_URL + "files/makedir/downloads");

    insertProtectedFile(SERVER_INDEX_URL + "files/createfile", "test22.ico",function(file){
        var download = {page_id : page.id, page_u_id: page.u_id, file_id : file.response.id, position : 1};
        download = insertObject(download, SERVER_INDEX_URL + "download/createdownload");
        var workflow = dbPage.workflow;
        workflow.editing_state = 'Approved';
        workflow.user_responsible = 2;
        workflow = insertObject(workflow, SERVER_INDEX_URL + "workflow/update", "PUT");

        page.active = 1;
        insertObject(page, SERVER_INDEX_URL + "pages/update", "PUT");
		var urlREST = SERVER_BASE_URL + page.u_id;
        logoutUser();
        //jQuery( document ).unbind('ajaxError');

		jQuery.ajax({
			type: "GET",
            async: false,
			url: urlREST, // URL of the REST web service
			contentType: "application/json; charset=utf-8",
			dataType: "html",
            //async: false,
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                assert.ok(false, "error: " + textStatus + " : " + errorThrown);
            },
			success: function(data, textStatus, jqXHR ) {
				//page is loaded
                if (jqXHR.status == 200 && data.search('method="post" id="loginForm"') !== -1) { // script returned error
					assert.ok(true, "User is redirected to login.");
				} // if
				else { // login was successful
                    assert.ok(false, "error");
				} //else
			}, // success
			complete: function(data){
                loginUser();
				forceDeleteDownload(page.id);
				deleteObject(SERVER_INDEX_URL + "files/forcedeletefile/"+file.response.id);
                deleteAccess(page.u_id);
				forceDeletePage(page.u_id);
				deleteDir(SERVER_INDEX_URL + "files/deletedir/downloads",'/'+dirname.dirname+'/');
                logoutUser();
			} // always
		  });
    });
});
/**
 * Content Web Service
 */

QUnit.module("Test REST Comments Web Services", {
	setup: function() {
		// prepare something for all following tests
		jQuery.ajaxSetup({timeout: 5000});

		jQuery(document).ajaxStart(function() {
			ok(true, "ajaxStart");
		}).ajaxStop(function() {
			ok(true, "ajaxStop");
			QUnit.start();
		}).ajaxSend(function() {
			ok(true, "ajaxSend");
		}).ajaxComplete(function(event, request, settings) {
			ok(true, "ajaxComplete: " + request.responseText);
		}).ajaxError(function() {
			ok(false, "ajaxError");
		}).ajaxSuccess(function() {
			ok(true, "ajaxSuccess");
		});
	},
	teardown: function() {
		// clean up after each test
		jQuery(document).unbind('ajaxStart');
		jQuery(document).unbind('ajaxStop');
		jQuery(document).unbind('ajaxSend');
		jQuery(document).unbind('ajaxComplete');
		jQuery(document).unbind('ajaxError');
		jQuery(document).unbind('ajaxSuccess');
	}
});

QUnit.test("DBDN REST Create Comment - success", 6, function(assert) {
	QUnit.stop();
    loginUser();
    var user = createUser();
	var page = createAndActivatePage();
	var content = createPageContent(page);

	var comment = { user_id: user.id, page_id: page.id, lang: 1, subject: "Test Subject", body: "Test Body", active: 0 };

	var expected_comment = jQuery.extend({ page_title: content[0].body, username: user.username }, comment);

	// expected data
	var expected_result = jQuery.extend({"error":false, "message": "Comment was added.", "response": null}, {"response": expected_comment});

	// url string
	var urlREST = SERVER_INDEX_URL + "comments/create";

	jQuery.ajax({
		type: "POST",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(comment),
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				expected_comment.id = data.response.id;
				expected_result.response.id = data.response.id;
				expected_result.response.created_at = data.response.created_at;
				expected_result.response.updated_at = data.response.updated_at;
				assert.deepEqual(data, expected_result, "Comment was added.");
			} //else
		}, // success
		complete: function(data){
            deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+expected_comment.id);
            deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
			deleteObject(SERVER_INDEX_URL + "contents/forcedelete/"+content[0].id);
            forceDeletePage(page.u_id);
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Create Comment - user_id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var comment = {page_id : 1, lang : 1, subject : "Test Subject", body:"Test Body",active:0};

	// expected data
	var expected_result = {"error": true, "message": "Parameter user_id is required!"};

    // url string
	var urlREST =  SERVER_INDEX_URL + "comments/create";

    jQuery.ajax({
        type: "POST",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(comment),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Comment add required");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Comment add result");
          } // if
          else { // login was successful
        	assert.ok(false, "Comment add user id check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});


QUnit.test("DBDN REST Remove Comment - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
	var user = createUser();
	var page = createAndActivatePage();

	var comment = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject", body:"Test Body",active:0};

	comment = insertObject(comment, SERVER_INDEX_URL + "comments/create");

	var expected_data = comment;
	var expected_result = jQuery.extend({"error":false, "message": "Comment was deleted.", "response": null}, {"response": expected_data});
	// url string
	var urlREST = SERVER_INDEX_URL + "comments/delete/"+comment.id;

		jQuery.ajax({
			type: "DELETE",
			url: urlREST, // URL of the REST web service
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			}, // error
			// script call was successful
			// data contains the JSON values returned by the Perl script
			success: function(data) {
				if (data.error) { // script returned error
					assert.ok(false, "error");
				} // if
				else { // login was successful
					expected_result.response.created_at = data.response.created_at;
					expected_result.response.updated_at = data.response.updated_at;
					assert.deepEqual(data, expected_result, "Comment delete.");
				} //else
				deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+data.response.id);
			}, // success
			complete: function(data){
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
                forceDeletePage(page.u_id);
				logoutUser();
			} // always
		  });
});

QUnit.test("DBDN REST Comment delete - id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var subscription = {}

	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required!"};

    // url string
	var urlREST =  SERVER_INDEX_URL + "comments/delete";

    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(subscription),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Comment delete required");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Comment delete result");
          } // if
          else { // login was successful
        	assert.ok(false, "Comment delete user id check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Comment delete - not found", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	// expected data
	var expected_result = {"error": true, "message": "Comment not found."};

    // url string
	var urlREST =  SERVER_INDEX_URL + "comments/delete/"+4;

    jQuery.ajax({
        type: "DELETE",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Comment delete not found");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Comment delete not found result");
          } // if
          else { // login was successful
        	assert.ok(false, "Comment delete not found  check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Update Comment - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
	var user = createUser();
	var page = createAndActivatePage();

	var comment = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject", body:"Test Body",active:0};
	comment = insertObject(comment, SERVER_INDEX_URL + "comments/create");

	var new_comment = {id: comment.id, user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject 2", body:"Test Body",active:1};

	var expected_data = jQuery.extend({"username": user.username},new_comment);

	var expected_result = jQuery.extend({"error":false, "message": "Comment was updated.", "response": null}, {"response": expected_data});
	// url string
	var urlREST = SERVER_INDEX_URL + "comments/update";

		jQuery.ajax({
			type: "PUT",
			url: urlREST, // URL of the REST web service
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(new_comment),
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				assert.ok(false, "error: " + textStatus + " : " + errorThrown);
			}, // error
			// script call was successful
			// data contains the JSON values returned by the Perl script
			success: function(data) {
				if (data.error) { // script returned error
					assert.ok(false, "error");
				} // if
				else { // login was successful
					expected_result.response.created_at = data.response.created_at;
					expected_result.response.updated_at = data.response.updated_at;
					assert.deepEqual(data, expected_result, "Comment delete.");
				} //else
			}, // success
			complete: function(data){
				deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+new_comment.id);
				deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
                forceDeletePage(page.u_id);
				logoutUser();
			} // always
		  });
});

QUnit.test("DBDN REST Comment update - not found", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var user = createUser();
	deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);

	var comment = {id: 4};

	// expected data
	var expected_result = {"error": true, "message": "Comment not found.","response": comment};

    // url string
	var urlREST =  SERVER_INDEX_URL + "comments/update";

    jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(comment),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Comment update not found");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Comment update not found result");
          } // if
          else { // login was successful
        	assert.ok(false, "Comment update not found  check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}

	});
});

QUnit.test("DBDN REST Comment update - id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var comment = {};

	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required!"};

    // url string
	var urlREST =  SERVER_INDEX_URL + "comments/update";

    jQuery.ajax({
        type: "PUT",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
		data:JSON.stringify(comment),
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Comment update id required");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Comment update id required result");
          } // if
          else { // login was successful
        	assert.ok(false, "Comment update id check fail!");
          } //else
        },// success
		complete : function (){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Get comment - success", 6, function(assert) {
	QUnit.stop();
	loginUser();

    var user = createUser();

	var page = createAndActivatePage();

	var content = createPageContent(page);

	var comment = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject", body:"Test Body",active:0};

	comment = insertObject(comment, SERVER_INDEX_URL + "comments/create");

	comment  = jQuery.extend(comment,{username : user.username, page_title:content[0].body});
	var expected_data = comment;
	var expected_result = jQuery.extend({"error":false, "message": "Comment found.", "response": null}, 
										{"response": expected_data});
	// url string
	var urlREST = SERVER_INDEX_URL + "comments/read/"+comment.id;
    console.log(comment.id);
	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				assert.deepEqual(data, expected_result, "Comment found.");
			} //else
		}, // success
		complete: function(data){
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
			deleteObject(SERVER_INDEX_URL + "contents/forcedelete/"+content[0].id);
			forceDeletePage(page.u_id);
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Get comment - success (delete user)", 6, function(assert) {
	QUnit.stop();
	loginUser();
    var user = createUser();
	var page = createAndActivatePage();
	var content = createPageContent(page);

	var comment = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject", body:"Test Body",active:0};
	comment = insertObject(comment, SERVER_INDEX_URL + "comments/create");
	comment  = jQuery.extend(comment,{username : "anonymous", page_title:content[0].body});

	deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);

	var expected_data = comment;
	var expected_result = jQuery.extend({"error":false, "message": "Comment found.", "response": null}, {"response": expected_data});
	// url string
	var urlREST = SERVER_INDEX_URL + "comments/read/"+comment.id;

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				assert.deepEqual(data, expected_result, "Comment found.");
			} //else
		}, // success
		complete: function(data){
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment.id);
			deleteObject(SERVER_INDEX_URL + "contents/forcedelete/"+content[0].id);
			forceDeletePage(page.u_id);
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Comment get - id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var comment = {};

	// expected data
	var expected_result = {"error": true, "message": "Parameter ID is required!"};

    // url string
	var urlREST =  SERVER_INDEX_URL + "comments/read";

    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Comment read id required");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Comment read id required result");
          } // if
          else { // login was successful
        	assert.ok(false, "Comment read id check fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Comment get - not found", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var comment = {};

	// expected data
	var expected_result = {"error": true, "message": "Comment not found."};

    // url string
	var urlREST =  SERVER_INDEX_URL + "comments/read/99999";

    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Comment read not fount");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Comment read not found");
          } // if
          else { // login was successful
        	assert.ok(false, "Comment read not found fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});


QUnit.test("DBDN REST Get all comments - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
    var user = createUser();
	var page = createAndActivatePage();
	var content = createPageContent(page);

	var comment1 = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject", body:"Test Body",active:0};
	comment1 = insertObject(comment1, SERVER_INDEX_URL + "comments/create");
	comment1  = jQuery.extend(comment1,{username : user.username, page_title:content[0].body});

	var comment2 = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject 2", body:"Test Body 2",active:0};
	comment2 = insertObject(comment2, SERVER_INDEX_URL + "comments/create");
	comment2  = jQuery.extend(comment2,{username : user.username, page_title:content[0].body});

	var comment3 = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject 3", body:"Test Body 3",active:0};
	comment3 = insertObject(comment3, SERVER_INDEX_URL + "comments/create");
	comment3  = jQuery.extend(comment3,{username : user.username, page_title:content[0].body});

	var expected_data = [comment1, comment2, comment3];
	var expected_result = jQuery.extend({"error":false, "message": "List of comments found.", "response": null}, {"response": expected_data});
	// url string
	var urlREST = SERVER_INDEX_URL + "comments/all";

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				assert.deepEqual(data, expected_result, "List of comments found.");
			} //else
		}, // success
		complete: function(data){
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment1.id);
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment2.id);
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment3.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
			deleteObject(SERVER_INDEX_URL + "contents/forcedelete/"+content[0].id);
			forceDeletePage(page.u_id);
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Get all comments - success (lang set)", 6, function(assert) {
	QUnit.stop();
	loginUser();
    var user = createUser();
	var page = createAndActivatePage();
	var content = createPageContent(page);

	var comment1 = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject", body:"Test Body",active:0};
	comment1 = insertObject(comment1, SERVER_INDEX_URL + "comments/create");
	comment1  = jQuery.extend(comment1,{username : user.username, page_title:content[0].body});

	var comment2 = {user_id : user.id,page_id : page.id, lang : 2, subject : "Test Subject 2", body:"Test Body 2",active:0};
	comment2 = insertObject(comment2, SERVER_INDEX_URL + "comments/create");
	comment2  = jQuery.extend(comment2,{username : user.username, page_title:content[0].body});

	var comment3 = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject 3", body:"Test Body 3",active:0};
	comment3 = insertObject(comment3, SERVER_INDEX_URL + "comments/create");
	comment3  = jQuery.extend(comment3,{username : user.username, page_title:content[0].body});

	var expected_data = [comment1, comment3];
	var expected_result = jQuery.extend({"error":false, "message": "List of comments found.", "response": null}, {"response": expected_data});
	// url string
	var urlREST = SERVER_INDEX_URL + "comments/all/1";

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				assert.deepEqual(data, expected_result, "List of comments found.");
			} //else
		}, // success
		complete: function(data){
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment1.id);
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment2.id);
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment3.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
			deleteObject(SERVER_INDEX_URL + "contents/forcedelete/"+content[0].id);
			forceDeletePage(page.u_id);
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Get comments page - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
    var user = createUser();
	var page = createAndActivatePage();
	var content = createPageContent(page);

	var comment1 = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject", body:"Test Body",active:0};
	comment1 = insertObject(comment1, SERVER_INDEX_URL + "comments/create");
	comment1  = jQuery.extend(comment1,{username : user.username, page_title:content[0].body});

	var comment2 = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject 2", body:"Test Body 2",active:0};
	comment2 = insertObject(comment2, SERVER_INDEX_URL + "comments/create");
	comment2  = jQuery.extend(comment2,{username : user.username, page_title:content[0].body});

	var comment3 = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject 3", body:"Test Body 3",active:0};
	comment3 = insertObject(comment3, SERVER_INDEX_URL + "comments/create");
	comment3  = jQuery.extend(comment3,{username : user.username, page_title:content[0].body});

	var expected_data = [comment1, comment2, comment3];
	var expected_result = jQuery.extend({"error":false, "message": "Page Comments found.", "response": null}, {"response": expected_data});
	// url string
	var urlREST = SERVER_INDEX_URL + "comments/readbypage?page_id="+page.id;

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				assert.deepEqual(data, expected_result, "List of comments found.");
			} //else
		}, // success
		complete: function(data){
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment1.id);
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment2.id);
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment3.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
			deleteObject(SERVER_INDEX_URL + "contents/forcedelete/"+content[0].id);
			forceDeletePage(page.u_id);
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Get comments page - success (lang set)", 6, function(assert) {
	QUnit.stop();
	loginUser();
    var user = createUser();
	var page = createAndActivatePage();
	var content = createPageContent(page);

	var comment1 = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject", body:"Test Body",active:0};
	comment1 = insertObject(comment1, SERVER_INDEX_URL + "comments/create");
	comment1  = jQuery.extend(comment1,{username : user.username, page_title:content[0].body});

	var comment2 = {user_id : user.id,page_id : page.id, lang : 2, subject : "Test Subject 2", body:"Test Body 2",active:0};
	comment2 = insertObject(comment2, SERVER_INDEX_URL + "comments/create");
	comment2  = jQuery.extend(comment2,{username : user.username, page_title:content[0].body});

	var comment3 = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject 3", body:"Test Body 3",active:0};
	comment3 = insertObject(comment3, SERVER_INDEX_URL + "comments/create");
	comment3  = jQuery.extend(comment3,{username : user.username, page_title:content[0].body});

	var expected_data = [comment1, comment3];
	var expected_result = jQuery.extend({"error":false, "message": "Page Comments found.", "response": null}, {"response": expected_data});
	// url string
	var urlREST = SERVER_INDEX_URL + "comments/readbypage?page_id="+page.id+"&lang=1";

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				assert.deepEqual(data, expected_result, "List of comments found.");
			} //else
		}, // success
		complete: function(data){
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment1.id);
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment2.id);
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment3.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
			deleteObject(SERVER_INDEX_URL + "contents/forcedelete/"+content[0].id);
			forceDeletePage(page.u_id);
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Comment by page - not found", 6, function( assert ) {
	QUnit.stop();
	loginUser();

	var comment = {};

	// expected data
	var expected_result = {"error": false, "message": "Page Comments found.", response : []};

    // url string
	var urlREST =  SERVER_INDEX_URL + "comments/readbypage?page_id=5";

    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Comment read not fount");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.ok(false, "Comment read not found fail!");
          } // if
          else { // login was successful

			assert.deepEqual(data, expected_result, "Comment read not found");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Comment by page - page_id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var comment = {};

	// expected data
	var expected_result = {"error": true, "message": "Parameter page_id is required!"};

    // url string
	var urlREST =  SERVER_INDEX_URL + "comments/readbypage";

    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Comment read not fount");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Comment read not found");
          } // if
          else { // login was successful
        	assert.ok(false, "Comment read not found fail!");
          } //else
        }, // success
		complete : function (){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Get comments by user - success", 6, function(assert) {
	QUnit.stop();
	loginUser();
    var user = createUser();
	var page = createAndActivatePage();
	var content = createPageContent(page);

	var comment1 = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject", body:"Test Body",active:0};
	comment1 = insertObject(comment1, SERVER_INDEX_URL + "comments/create");
	comment1  = jQuery.extend(comment1,{username : user.username, page_title:content[0].body});

	var comment2 = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject 2", body:"Test Body 2",active:0};
	comment2 = insertObject(comment2, SERVER_INDEX_URL + "comments/create");
	comment2  = jQuery.extend(comment2,{username : user.username, page_title:content[0].body});

	var comment3 = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject 3", body:"Test Body 3",active:0};
	comment3 = insertObject(comment3, SERVER_INDEX_URL + "comments/create");
	comment3  = jQuery.extend(comment3,{username : user.username, page_title:content[0].body});

	var expected_data = [comment1, comment2, comment3];
	var expected_result = jQuery.extend({"error":false, "message": "User Comments found.", "response": null}, {"response": expected_data});
	// url string
	var urlREST = SERVER_INDEX_URL + "comments/readbyuser?user_id="+user.id;

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				assert.deepEqual(data, expected_result, "List of comments found.");
			} //else
		}, // success
		complete: function(data){
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment1.id);
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment2.id);
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment3.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
			deleteObject(SERVER_INDEX_URL + "contents/forcedelete/"+content[0].id);
			forceDeletePage(page.u_id);
			loginUser();
		} // always
	});
});

QUnit.test("DBDN REST Get comments by user - success (lang set)", 6, function(assert) {
	QUnit.stop();
	loginUser();
    var user = createUser();
	var page = createAndActivatePage();
	var content = createPageContent(page);

	var comment1 = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject", body:"Test Body",active:0};
	comment1 = insertObject(comment1, SERVER_INDEX_URL + "comments/create");
	comment1  = jQuery.extend(comment1,{username : user.username, page_title:content[0].body});

	var comment2 = {user_id : user.id,page_id : page.id, lang : 2, subject : "Test Subject 2", body:"Test Body 2",active:0};
	comment2 = insertObject(comment2, SERVER_INDEX_URL + "comments/create");
	comment2  = jQuery.extend(comment2,{username : user.username, page_title:content[0].body});

	var comment3 = {user_id : user.id,page_id : page.id, lang : 1, subject : "Test Subject 3", body:"Test Body 3",active:0};
	comment3 = insertObject(comment3, SERVER_INDEX_URL + "comments/create");
	comment3  = jQuery.extend(comment3,{username : user.username, page_title:content[0].body});

	var expected_data = [comment1, comment3];
	var expected_result = jQuery.extend({"error":false, "message": "User Comments found.", "response": null}, {"response": expected_data});
	// url string
	var urlREST = SERVER_INDEX_URL + "comments/readbyuser?user_id="+user.id+"&lang=1";

	jQuery.ajax({
		type: "GET",
		url: urlREST, // URL of the REST web service
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.ok(false, "error: " + textStatus + " : " + errorThrown);
		}, // error
		// script call was successful
		// data contains the JSON values returned by the Perl script
		success: function(data) {
			if (data.error) { // script returned error
				assert.ok(false, "error");
			} // if
			else { // login was successful
				assert.deepEqual(data, expected_result, "List of comments found.");
			} //else
		}, // success
		complete: function(data){
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment1.id);
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment2.id);
			deleteObject(SERVER_INDEX_URL + "comments/forcedelete/"+comment3.id);
			deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);
			deleteObject(SERVER_INDEX_URL + "contents/forcedelete/"+content[0].id);
			forceDeletePage(page.u_id);
			logoutUser();
		} // always
	});
});

QUnit.test("DBDN REST Comment by user - not found", 6, function( assert ) {
	QUnit.stop();
	loginUser();
	var user = createUser();
	deleteObject(SERVER_INDEX_URL + "user/forcedelete/"+user.id);

	var comment = {};

	// expected data
	var expected_result = {"error": false, "message": "User Comments found.", response : []};

    // url string
	var urlREST =  SERVER_INDEX_URL + "comments/readbyuser?user_id=" + user.id;

    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 404, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Comment read not fount");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.ok(false, "Comment read not found fail!");
          } // if
          else { // login was successful

			assert.deepEqual(data, expected_result, "Comment read not found");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});

QUnit.test("DBDN REST Comment by page - user_id required", 6, function( assert ) {
	QUnit.stop();
	jQuery( document ).unbind('ajaxError');
	loginUser();
	var comment = {};

	// expected data
	var expected_result = {"error": true, "message": "Parameter user_id is required!"};

    // url string
	var urlREST =  SERVER_INDEX_URL + "comments/readbyuser";

    jQuery.ajax({
        type: "GET",
        url: urlREST, // URL of the REST web service
        contentType: "application/json; charset=utf-8",
    	dataType:"json",
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) {
			assert.deepEqual(XMLHttpRequest.status, 400, "HTTP Status code.");
			var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
			var data = JSON.parse(fixedResponse);
			assert.deepEqual(data, expected_result, "Comment read not found");
        }, // error
        // script call was successful
        // data contains the JSON values returned by the Perl script
        success: function(data){
          if (data.error) { // script returned error
			assert.deepEqual(data, expected_result, "Comment read not found");
          } // if
          else { // login was successful
        	assert.ok(false, "Comment read not found fail!");
          } //else
        }, // success
		complete : function(){
			logoutUser();
		}
	});
});
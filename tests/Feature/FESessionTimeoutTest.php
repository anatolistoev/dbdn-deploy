<?php

namespace Tests\Feature;

use App\Models\Download;
use App\Models\FileItem;
use App\Models\Page;
use App\Models\User;
use App\Models\Usergroup;
use App\Helpers\FileHelper;
use App\Http\Controllers\RestController;
use App\Http\Controllers\FileController;
use App\Http\Middleware\BeAccessApprover;
use App\Http\Middleware\BeAccessCms;
use App\Http\Middleware\BeAccessNlOrCms;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class FESessionTimeoutTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp(){
        parent::setUp();
        putenv("TEST_CSRF=true");
    }

    public function tearDown(){
        parent::tearDown();
    }

    /**
     * WEB Routes
     */

    /**
     *  test /search no timeout for Anonimus
     *
     * @return void
     */
    public function testSearchSessionActiveAnonimus()
    {
        $response = $this->get('/search?lang=en');

        $data = [
            'search' => 'dev',
            '_token' => csrf_token()
        ];
        $response = $this->post('/search?lang=en', $data, []);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();
    }

    /**
     *  test /search timeout for Anonimus
     *
     * @return void
     */
    public function testSearchSessionTimeoutAnonimus()
    {
        $response = $this->get('/search?lang=en');

        $data = [
            'search' => 'dev',
            '_token' => 'ExpiredToken'
        ];
        $response = $this->post('/search?lang=en', $data, []);

        $response->assertStatus(302);
        $response->assertRedirect('/');
        $response->assertSessionHasErrors(['success']);
    }

    /**
     *  test /gallery timeout for Anonimus
     *
     * @return void
     */
    public function testGallerySessionTimeoutAnonimus()
    {
        $response = $this->get('/home?lang=en');
        $file = FileItem::where('type', '=', 'images')->first();

        $data = [
            'files'=>[$file->id],
            '_token' => 'ExpiredToken'
        ];
        $response = $this->post('/gallery?lang=en', $data, []);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();
    }

    /**
     *  test /cms/login no timeout for Admin
     *
     * @return void
     */
    public function testBELoginSessionActiveAutorized()
    {
        $response = $this->get('/cms/login');

        $user     = new User(['id' => 1111]);
        $user->role = \USER_ROLE_ADMIN;

        $data = [
            '_token' => csrf_token()
        ];

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->post('/cms/pages', $data, []);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();
    }

    /**
     *  test /cms/login timeout for Admin
     *
     * @return void
     */
    public function testBELoginSessionTimeoutAnonimus()
    {
        $response = $this->get('/cms/login');
        $xsrf_token = 'ExpiredToken';

        $user     = new User(['id' => 1111]);
        $user->role = \USER_ROLE_ADMIN;

        $response = $this->actingAs($user)
                    ->withSession(['isConfirmed' => true])
                    ->withHeader('X-CSRF-TOKEN' , $xsrf_token)
                    ->post('/cms/pages', [], []);

        $response->assertStatus(302);
        $response->assertRedirect('/cms');
        $response->assertSessionHasErrors(['username']);
    }

}
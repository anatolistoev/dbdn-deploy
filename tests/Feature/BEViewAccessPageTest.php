<?php

namespace Tests\Feature;

use App\Models\Group;
use App\Models\User;
use App\Models\Page;
use App\Models\Subscription;

use App\Http\Controllers\RestController;
use App\Http\Middleware\BeAccessFaqs;
use App\Http\Middleware\BeAccessApprover;
use App\Http\Middleware\BeAccessCms;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BEViewAccessPageTest extends TestCase
{
    use DatabaseTransactions;

    /**
     *  test /cms/pages Authorized
     *
     * @return void
     */
    public function testRouteCMSPagesAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach (BeAccessCms::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/pages');

            $response->assertStatus(200);
            $response->assertViewIs('backend.be_pages');
            //TODO: Check if all groups are correct for all roles!
            $response->assertViewHas(['ACTIVE' => 'content', 'GROUPS' => Group::all(), 'LANG']);
        }
    }

    /**
     *  test /cms/pages Authorized first step but without second step!
     *
     * @return void
     */
    public function testRouteCMSPagesAuthorizedWithoutConfirm()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR;

        $response = $this->actingAs($user)->get('/cms/pages');

        $response->assertStatus(200);
        $response->assertViewIs('backend.be_confirm');
        $response->assertViewMissing('ACTIVE');
    }

    /**
     *  test /api/v1/pages/all?lang=2 Missing required param
     *
     * @return void
     */
    public function testRouteAPIPageAllRequiredParam()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR_SMART;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/pages/all?lang=2');
        
        $response->assertStatus(400);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.require.parameter')));
    }

    /**
     *  test /api/v1/pages/all?lang=2&parent_id=0&depth=2 Authorized
     *
     * @return void
     */
    public function testRouteAPIPageAllAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach ([USER_ROLE_EDITOR_SMART, \USER_ROLE_ADMIN] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/pages/all?lang=2&parent_id=0&depth=2');
            
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, "List of pages found."));
            //TODO: Check the list
        }
    }

    /**
     *  test /api/v1/pages/all?lang=2&parent_id=0&depth=2 Filter Not Authorized
     *
     * @return void
     */
    public function testRouteAPIPageAllFilterNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR_DFM;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/pages/all?lang=2&parent_id=0&depth=2');

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'List of pages found.'));
        //TODO: Check the list
    }

    /**
     *  test /api/v1/pages/read/2207/access?lang=2 Authorized
     *
     * @return void
     */
    public function testRouteAPIPageReadAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach ([USER_ROLE_EDITOR_SMART, \USER_ROLE_ADMIN] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/pages/read/2207/access?lang=2');
            
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, "Page found."));
        }
    }

    /**
     *  test /api/v1/pages/read/2207/access?lang=2 Not Authorized
     *
     * @return void
     */
    public function testRouteAPIPageReadNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR_DFM;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/pages/read/2207/access?lang=2');

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/pages/versions/2207?lang=2 Authorized
     *
     * @return void
     */
    public function testRouteAPIVersionsAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach ([USER_ROLE_EDITOR_SMART, \USER_ROLE_ADMIN] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/pages/versions/2207?lang=2');
            
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, "List of pages found."));
        }
    }

    /**
     *  test /api/v1/pages/versions/2207 Required param
     *
     * @return void
     */
    public function testRouteAPIVersionsRequiredParam()
    {
        $user     = new User(['id' => 1111]);
        $user->role = \USER_ROLE_ADMIN;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/pages/versions/2207?s');

        $response->assertStatus(400);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.require.lang')));
    }

    /**
     *  test /api/v1/pages/versions/2207?lang=2 Not Authorized
     *
     * @return void
     */
    public function testRouteAPIVersionsNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR_DFM;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/pages/versions/2207?lang=2');

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /preview/{page_id} Authorized
     *
     * @return void
     */
    public function testRouteCMSPreviewAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $page = Page::find(2207); // Page from smart
        foreach ([USER_ROLE_EDITOR_SMART, \USER_ROLE_ADMIN] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/preview/' . $page->id);

            $response->assertStatus(200);
            $response->assertViewIs($page->template);
            $response->assertViewHas(['CONTENTS', 'PAGE']);
        }
    }

    /**
     *  test /preview/{page_id} Not Authorized
     *
     * @return void
     */
    public function testRouteCMSPreviewNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $page = Page::find(2207); // Page from smart
        $user->role = USER_ROLE_EDITOR_DFM;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/preview/' . $page->id);

        $response->assertStatus(401);
        $response->assertViewIs('backend.be_unauthorized_access');
        $response->assertViewHas(['error' => trans('ws_general_controller.webservice.unauthorized')]);
    }

    /**
     *  test /api/v1/pages/isavailable/2207?lang=2 Authorized
     *
     * @return void
     */
    public function testRouteAPIPageIsAvailableAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user     = User::where('active', 1)->first();
        foreach ([\USER_ROLE_ADMIN] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/pages/isavailable/2207?lang=2');
            
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'You can edit this page'));
        }
    }

    /**
     *  test /api/v1/pages/isavailable/2207?lang=2 Not Authorized
     *
     * @return void
     */
    public function testRouteAPIPageIsAvailableNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR_DFM;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/pages/isavailable/2207?lang=2');

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/pages/notes/2207?lang=2 Authorized
     *
     * @return void
     */
    public function testRouteAPIPageNotesAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user     = User::where('active', 1)->first();
        foreach ([\USER_ROLE_EDITOR_SMART, \USER_ROLE_ADMIN] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/pages/notes/2207?lang=2');
            
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'List of notes found.'));
        }
    }

    /**
     *  test /api/v1/pages/notes/2207?lang=2 Not Authorized
     *
     * @return void
     */
    public function testRouteAPIPageNotesNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR_DFM;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/pages/notes/2207?lang=2');

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

}

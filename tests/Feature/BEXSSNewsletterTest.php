<?php

namespace Tests\Feature;

use App\Models\Group;
use App\Models\User;
use App\Models\Page;
use App\Models\Newsletter;

use App\Http\Controllers\RestController;

use Tests\TestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Support\Facades\Auth;

class BEXSSNewsletterTest extends TestCase
{
    use DatabaseTransactions;
    
    /**
     *  test /api/v1/nl_contents/create No XSS content
     *
     * @return void
     */
    public function testRouteAPINewsletterContentCreateNoXSSContent()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';        
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = \USER_ROLE_NEWSLETTER_APPROVER;
        $userResponsible->save();

        $user->role = \USER_ROLE_NEWSLETTER_EDITOR;

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_DRAFT;
        $nl->save();

        // Load contents for update
        $plainHTML = "<p>Test Author</p>";
        $newNlContent = [
            'newsletter_id' => $nl->id,
            'lang' => \LANG_EN,
            'position' => "title",
            'content' => $plainHTML
        ];

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('POST', '/api/v1/nl_contents/create', $newNlContent);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, "Content was added.", [$newNlContent]));
    }

    /**
     *  test /api/v1/nl_contents/create With XSS content
     *
     * @return void
     */
    public function testRouteAPINewsletterContentCreateWithXSSContent()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = \USER_ROLE_NEWSLETTER_APPROVER;
        $userResponsible->save();

        $user->role = \USER_ROLE_NEWSLETTER_EDITOR;

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_DRAFT;
        $nl->save();

        // Load contents for update
        $plainHTML = "<p>Test Author</p>";
        $newNlContent = [
            'newsletter_id' => $nl->id,
            'lang' => \LANG_EN,
            'position' => "title",
            'content' => "<p onclick='alert('...ESCAPE UNTRUSTED DATA BEFORE PUTTING HERE...');'>Test Author</p>"
        ];

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('POST', '/api/v1/nl_contents/create', $newNlContent);

        $response->assertStatus(200);
        $newNlContent['content'] = $plainHTML;
        $response->assertJson(RestController::generateJsonResponse(false, "Content was added.", [$newNlContent]));
    }

    /**
     *  test /api/v1/nl_contents/create-many No XSS content
     *
     * @return void
     */
    public function testRouteAPINewsletterContentCreateManyNoXSSContent()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = \USER_ROLE_NEWSLETTER_APPROVER;
        $userResponsible->save();

        $user->role = \USER_ROLE_NEWSLETTER_EDITOR;

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_DRAFT;
        $nl->save();

        // Load contents for update
        $plainHTML = "<p>Test Author</p>";
        $newNlContent = [ 
            0 => [
                'newsletter_id' => $nl->id,
                'lang' => \LANG_EN,
                'position' => "title",
                'content' => $plainHTML
            ]
        ];

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('POST', '/api/v1/nl_contents/create-many', $newNlContent);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, "Content was added.", $newNlContent));
    }

    /**
     *  test /api/v1/nl_contents/create-many With XSS content
     *
     * @return void
     */
    public function testRouteAPINewsletterContentCreateManyWithXSSContent()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = \USER_ROLE_NEWSLETTER_APPROVER;
        $userResponsible->save();

        $user->role = \USER_ROLE_NEWSLETTER_EDITOR;

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_DRAFT;
        $nl->save();

        // Load contents for update
        $plainHTML = "<p>Test Author</p>";
        $newNlContent = [ 
            0 => [
                'newsletter_id' => $nl->id,
                'lang' => \LANG_EN,
                'position' => "title",
                'content' => "<p onclick='alert('...ESCAPE UNTRUSTED DATA BEFORE PUTTING HERE...');'>Test Author</p>"
            ]
        ];

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('POST', '/api/v1/nl_contents/create-many', $newNlContent);

        $response->assertStatus(200);
        $newNlContent[0]['content'] = $plainHTML;
        $response->assertJson(RestController::generateJsonResponse(false, "Content was added.", $newNlContent));
    }

    /**
     *  test /api/v1/nl_contents/update No XSS content
     *
     * @return void
     */
    public function testRouteAPINewsletterContentUpdateNoXSSContent()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = \USER_ROLE_NEWSLETTER_APPROVER;
        $userResponsible->save();

        $user->role = \USER_ROLE_NEWSLETTER_EDITOR;

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_DRAFT;
        $nl->save();

        // Load contents for update
        $nlContent = $nl->contents()->first();

        $plainHTML = "<p>Test Author</p>";
        $postNewsletterData = ['id' => $nlContent->id, 'content' => $plainHTML];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/nl_contents/update', $postNewsletterData);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, "Content was updated."));
        $responceNlContent = $response->baseResponse->original['response'][0]['content'];
        $this->assertEquals($responceNlContent, $plainHTML);
    }

    /**
     *  test /api/v1/nl_contents/update With XSS content
     *
     * @return void
     */
    public function testRouteAPINewsletterContentUpdateWithXSSContent()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';        
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = \USER_ROLE_NEWSLETTER_APPROVER;
        $userResponsible->save();

        $user->role = \USER_ROLE_NEWSLETTER_EDITOR;

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_DRAFT;
        $nl->save();

        // Load contents for update
        $nlContent = $nl->contents()->first();

        $plainHTML = "<p>Test Author</p>";
        $postNewsletterData = ['id' => $nlContent->id, 'content' => $plainHTML."<script>alert('...ESCAPE UNTRUSTED DATA BEFORE PUTTING HERE...')</script>"];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/nl_contents/update', $postNewsletterData);

        $response->assertStatus(200);
        $postNewsletterData["content"] = $plainHTML;
        $response->assertJson(RestController::generateJsonResponse(false, "Content was updated.", [$postNewsletterData]));
    }

    /**
     *  test /api/v1/nl_contents/update-many No XSS content
     *
     * @return void
     */
    public function testRouteAPINewsletterContentUpdateManyNoXSSContent()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';        
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = \USER_ROLE_NEWSLETTER_APPROVER;
        $userResponsible->save();

        $user->role = \USER_ROLE_NEWSLETTER_EDITOR;

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_DRAFT;
        $nl->save();

        // Load contents for update
        $nlContent = $nl->contents()->first();

        $plainHTML = "<p>Test Author</p>";
        $postNewsletterData = [ 0 => ['id' => $nlContent->id, 'content' => $plainHTML]];

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/nl_contents/update-many', $postNewsletterData);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, "Content was updated.", $postNewsletterData));
    }

    /**
     *  test /api/v1/nl_contents/update-many With XSS content
     *
     * @return void
     */
    public function testRouteAPINewsletterContentUpdateManyWithXSSContent()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';        
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = \USER_ROLE_NEWSLETTER_APPROVER;
        $userResponsible->save();

        $user->role = \USER_ROLE_NEWSLETTER_EDITOR;

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_DRAFT;
        $nl->save();

        // Load contents for update
        $nlContent = $nl->contents()->first();

        $plainHTML = "<p>Test Author</p>";
        $postNewsletterData = [ 0 => ['id' => $nlContent->id, 'content' => $plainHTML."<script>alert('...ESCAPE UNTRUSTED DATA BEFORE PUTTING HERE...')</script>"]];

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/nl_contents/update-many', $postNewsletterData);

        $response->assertStatus(200);
        $postNewsletterData[0]["content"] = $plainHTML;        
        $response->assertJson(RestController::generateJsonResponse(false, "Content was updated.", $postNewsletterData));
    }

}

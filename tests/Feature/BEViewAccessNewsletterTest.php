<?php

namespace Tests\Feature;

use App\Models\Group;
use App\Models\User;
use App\Models\Page;
use App\Models\Newsletter;

use App\Http\Controllers\RestController;
use App\Http\Middleware\BeAccessFaqs;
use App\Http\Middleware\BeAccessApprover;
use App\Http\Middleware\BeAccessNl;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BEViewAccessNewsletterTest extends TestCase
{
    use DatabaseTransactions;
    private static $notAuthorizedUsersRole = [USER_ROLE_EDITOR, USER_ROLE_EDITOR_SMART,
        USER_ROLE_EDITOR_MB, USER_ROLE_EDITOR_DFS, USER_ROLE_EDITOR_DFM,
        USER_ROLE_EDITOR_DTF, USER_ROLE_EDITOR_FF, USER_ROLE_EDITOR_DP, USER_ROLE_EDITOR_TSS, USER_ROLE_EDITOR_BKK,
        USER_ROLE_EDITOR_DB, USER_ROLE_EDITOR_EB, USER_ROLE_EDITOR_SETRA, USER_ROLE_EDITOR_OP, USER_ROLE_EDITOR_BS, USER_ROLE_EDITOR_FUSO,
        USER_ROLE_EDITOR_DT];

    /*
     * CMS Tests
     */

    /**
     *  test /cms/newsletter Authorized
     *
     * @return void
     */
    public function testRouteCMSNewsletterAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/newsletter');

            $response->assertStatus(200);
            $response->assertViewIs('backend.be_newsletter');
            $response->assertViewHas(['ACTIVE' => 'newsletter', 'LANG']);
        }
    }

    /**
     *  test /cms/newsletter Not Authorized
     *
     * @return void
     */
    public function testRouteCMSNewsletterNotAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (self::$notAuthorizedUsersRole as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/newsletter');

            $response->assertStatus(401);
            $response->assertViewIs('backend.be_unauthorized_access');
            $response->assertViewHas(['error' => trans('ws_general_controller.webservice.unauthorized')]);
        }
    }

    /**
     *  test /cms/newsletter/newsletterid Authorized
     *
     * @return void
     */
    public function testRouteCMSNewsletterByIDAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $newsletter = Newsletter::where(function ($query) {
                $query->whereNull('end_edit')
                      ->orWhere('end_edit', '<', strtotime(EDIT_TIME_BUFFER_INTERVAL));
            })->first();
        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/newsletter/'.$newsletter->id);

            $response->assertStatus(200);
            $response->assertViewIs('backend.newsletter_basic');
            $response->assertViewHas(['ACTIVE' => 'newsletter', 'LANG','FILEMANAGER_MODE' => 'downloads', 'CONTENTS',
                'CONTENTIDS', 'SETTINGS', 'NEWSLETTER', 'TIME_LEFT', 'CHANGE_WORKFLOW_RESPONSIBLE', 'WORKFLOW_ADMINS']);
        }
    }

    /**
     *  test /cms/newsletter/newsletterid Not Authorized
     *
     * @return void
     */
    public function testRouteCMSNewsletterByIDNotAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (self::$notAuthorizedUsersRole  as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/newsletter/123456789');

            $response->assertStatus(401);
            $response->assertViewIs('backend.be_unauthorized_access');
            $response->assertViewHas(['error' => trans('ws_general_controller.webservice.unauthorized')]);
        }
    }

    /**
     *  test /cms/newsletter_users Authorized
     *
     * @return void
     */
    public function testRouteCMSNewsletterUsersAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/newsletter_users');

            $response->assertStatus(200);
            $response->assertViewIs('backend.be_newsletter_user');
            $response->assertViewHas(['ACTIVE' => 'newsletter_users', 'LANG']);
        }
    }

    /**
     *  test /cms/newsletter_users Not Authorized
     *
     * @return void
     */
    public function testRouteCMSNewsletterUsersNotAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
         foreach (self::$notAuthorizedUsersRole as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/newsletter_users');

            $response->assertStatus(401);
            $response->assertViewIs('backend.be_unauthorized_access');
            $response->assertViewHas(['error' => trans('ws_general_controller.webservice.unauthorized')]);
         }
    }

    /**
     *  test /cms/newsletter_users_test Authorized
     *
     * @return void
     */
    public function testRouteCMSNewsletterUsersTestAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/newsletter_users_test');

            $response->assertStatus(200);
            $response->assertViewIs('backend.be_newsletter_user');
            $response->assertViewHas(['ACTIVE' => 'newsletter_users_test', 'LANG']);
        }
    }

    /**
     *  test /cms/newsletter_users_test Not Authorized
     *
     * @return void
     */
    public function testRouteCMSNewsletterUsersTestNotAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
         foreach (self::$notAuthorizedUsersRole as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/newsletter_users_test');

            $response->assertStatus(401);
            $response->assertViewIs('backend.be_unauthorized_access');
            $response->assertViewHas(['error' => trans('ws_general_controller.webservice.unauthorized')]);
         }
    }

    /*
     * API Tests
     */
    /**
     *  test /api/v1/newsletter/responsible Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterResponsibleAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_REVIEW;
        $nl->save();
        $user = User::find($nl->user_responsible);
        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('GET', '/api/v1/newsletter/responsible?lang=1');

            $response->assertStatus(200);

            $response->assertJson(RestController::generateJsonResponse(false, 'List of newsletter found.', []), true);

            $this->assertEquals($response->baseResponse->original['response'][0]['id'], $nl->id);
        }
    }

    /**
     *  test /api/v1/pages/responsible Authorized Include Newsletter
     *
     * @return void
     */
    public function testRouteAPINewsletterResponsibleFromPagesAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_REVIEW;
        $nl->save();
        $user = User::find($nl->user_responsible);
        foreach ([USER_ROLE_ADMIN] as $role) {
            $user->role = $role;

            $responsePage = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('GET', '/api/v1/pages/responsible?lang=1');

            $responsePage->assertStatus(200);

            $responsePage->assertJson(RestController::generateJsonResponse(false, 'List of pages found.', []), true);

            $arrNewsletter = array_filter($responsePage->baseResponse->original['response'], function($item) use ($nl) {
                return isset($item['editing_state']) && $item['id'] == $nl->id;
            });
            $this->assertEquals(count($arrNewsletter), 1);
        }
    }

    /**
     *  test /api/v1/newsletter/responsible Authorized Empty List
     *
     * @return void
     */
    public function testRouteAPINewsletterResponsibleEmptyListAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $page = Page::find(2207); // Page from smart
        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('GET', '/api/v1/newsletter/responsible?lang=1');

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'List of newsletter found.', []));
        }
    }

    /**
     *  test /api/v1/newsletter/update Not Authorized Editor from other brand
     *
     * @return void
     */
    public function testRouteAPINewsletterUpdateEditorOtherBrandNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_EDITOR_DFM;

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_DRAFT;
        $nl->save();

        $postNewsletterData = ['id' => $nl->id, 'editing_state' => \STATE_REVIEW];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/newsletter/update', $postNewsletterData);

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/newsletter/update Authorized Change of State Draft to Review
     *
     * @return void
     */
    public function testRouteAPINewsletterUpdateStateDraftReviewAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = \USER_ROLE_NEWSLETTER_APPROVER;
        $userResponsible->save();

        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;
            // Clean user and session
            Auth::logout();
            $this->flushSession();
            // Start transaction!
            DB::beginTransaction();
            $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
            $nl->editing_state = \STATE_DRAFT;
            $nl->save();

            $postNewsletterData = ['id' => $nl->id, 'editing_state' => \STATE_REVIEW, 'user_responsible' => $userResponsible->id];
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('PUT', '/api/v1/newsletter/update', $postNewsletterData);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, "Newsletter was updated."));
            // Rollback and then return errors
            DB::rollback();
        }
    }

    /**
     *  test /api/v1/newsletter/update Authorized Change of State Review to Draft
     *
     * @return void
     */
    public function testRouteAPINewsletterUpdateStateReviewDraftAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = \USER_ROLE_NEWSLETTER_APPROVER;

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = \USER_ROLE_NEWSLETTER_EDITOR;
        $userResponsible->save();

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_REVIEW;
        $nl->save();

        $postNewsletterData = ['id' => $nl->id, 'editing_state' => \STATE_DRAFT, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/newsletter/update', $postNewsletterData);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'Newsletter was updated.'));
    }

    /**
     *  test /api/v1/newsletter/update Not Authorized Change of State Review to Draft
     *
     * @return void
     */
    public function testRouteAPINewsletterUpdateStateReviewDraftNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = \USER_ROLE_NEWSLETTER_EDITOR;

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = \USER_ROLE_NEWSLETTER_APPROVER; // Approver
        $userResponsible->save();

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_REVIEW;
        $nl->save();

        $postNewsletterData = ['id' => $nl->id, 'editing_state' => \STATE_DRAFT, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/newsletter/update', $postNewsletterData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('backend_newsletter.workflow.editor.change_state')));
    }

    /**
     *  test /api/v1/newsletter/update Not Authorized Change of State Approved to Review
     *
     * @return void
     */
    public function testRouteAPINewsletterUpdateStateApprovedReviewNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = \USER_ROLE_NEWSLETTER_EDITOR;

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = \USER_ROLE_NEWSLETTER_APPROVER; // Approver
        $userResponsible->save();

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_APPROVED;
        $nl->save();

        $postNewsletterData = ['id' => $nl->id, 'editing_state' => \STATE_REVIEW, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/newsletter/update', $postNewsletterData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('backend_newsletter.workflow.editor.change_state')));
    }

    /**
     *  test /api/v1/newsletter/changeresponsible Authorized Change of Responsible to Approver
     *
     * @return void
     */
    public function testRouteAPINewsletterUpdateResponsibleAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = \USER_ROLE_NEWSLETTER_EDITOR;

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = \USER_ROLE_NEWSLETTER_APPROVER; // Approver
        $userResponsible->save();

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_DRAFT;
        $nl->save();

        $postNewsletterData = ['id' => $nl->id, 'editing_state' => \STATE_REVIEW, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/newsletter/update', $postNewsletterData);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'Newsletter was updated.'));
    }

    /**
     *  test /api/v1/newsletter/update Not Authorized Change of Responsible to Editor of SMART
     *
     * @return void
     */
    public function testRouteAPINesletterUpdateResponsibleNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = \USER_ROLE_NEWSLETTER_EDITOR;

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = USER_ROLE_EDITOR_SMART;
        $userResponsible->save();

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_DRAFT;
        $nl->save();

        $postNewsletterData = ['id' => $nl->id, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/newsletter/update', $postNewsletterData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('backend_newsletter.workflow.attached.approver')));
    }

    /**
     *  test /api/v1/newsletter/update Not Authorized Change of Responsible to Editor of SMART
     *
     * @return void
     */
    public function testRouteAPINesletterUpdateResponsibleDraftReviewNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = \USER_ROLE_NEWSLETTER_EDITOR;

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = USER_ROLE_EDITOR_SMART;
        $userResponsible->save();

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_DRAFT;
        $nl->save();

        $postNewsletterData = ['id' => $nl->id, 'editing_state' => \STATE_REVIEW, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/newsletter/update', $postNewsletterData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('backend_newsletter.workflow.attached.approver')));
    }

    /**
     *  test /api/v1/newsletter/changeresponsible Authorized Change of Responsible to Approver in Review
     *
     * @return void
     */
    public function testRouteAPINewsletterChangeResponsibleReviewAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_NEWSLETTER_EDITOR;

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_REVIEW;
        $nl->save();

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = USER_ROLE_NEWSLETTER_APPROVER; // Approver for Newsletter
        $userResponsible->save();

        $postNewsletterData = ['newsletter_id' => $nl->id, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/newsletter/changeresponsible', $postNewsletterData);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'Newsletter was updated.'));
    }

    /**
     *  test /api/v1/newsletter/changeresponsible Authorized Change of Responsible to Editor in Draft
     *
     * @return void
     */
    public function testRouteAPINewsletterChangeResponsibleDraftAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_NEWSLETTER_EDITOR;

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_DRAFT;
        $nl->save();

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = \USER_ROLE_NEWSLETTER_EDITOR; // Approver for Newsletter
        $userResponsible->save();

        $postNewsletterData = ['newsletter_id' => $nl->id, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/newsletter/changeresponsible', $postNewsletterData);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'Newsletter was updated.'));
    }

    /**
     *  test /api/v1/newsletter/changeresponsible Not Authorized Change of Responsible to Editor of other brand in Draft
     *
     * @return void
     */
    public function testRouteAPINewsletterChangeResponsibleDraftNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_NEWSLETTER_EDITOR;

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_DRAFT;
        $nl->save();

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = \USER_ROLE_EDITOR_BS; // Editor of other brand
        $userResponsible->save();

        $postNewsletterData = ['newsletter_id' => $nl->id, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/newsletter/changeresponsible', $postNewsletterData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('backend_newsletter.workflow.attached.approver')));
    }

    /**
     *  test /api/v1/newsletter/changeresponsible Not Authorized Change of Responsible from Editor of brand
     *
     * @return void
     */
    public function testRouteAPINewsletterChangeResponsibleEditorBrandNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_EDITOR_DTF;

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_REVIEW;
        $nl->save();

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = USER_ROLE_NEWSLETTER_APPROVER; // Approver for Newsletter
        $userResponsible->save();

        $postNewsletterData = ['newsletter_id' => $nl->id, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/newsletter/changeresponsible', $postNewsletterData);

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/newsletter/changeresponsible Not Authorized Change of Responsible to empty
     *
     * @return void
     */
    public function testRouteAPINewsletterChangeResponsibleEmptyNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_NEWSLETTER_EDITOR;

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_REVIEW;
        $nl->save();

        $postNewsletterData = ['newsletter_id' => $nl->id, 'user_responsible' => 0];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/newsletter/changeresponsible', $postNewsletterData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('backend_newsletter.attached.approver')));
    }

    /**
     *  test /api/v1/newsletter/changeresponsible Not Authorized Change of Responsible to Editor of Newsletter
     *
     * @return void
     */
    public function testRouteAPINewsletterChangeResponsibleEditorNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_NEWSLETTER_EDITOR;

        $nl = Newsletter::where('user_responsible', '<>', 0)->first(); // any Newsletter
        $nl->editing_state = \STATE_APPROVED;
        $nl->save();

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = USER_ROLE_NEWSLETTER_EDITOR; // Approver for Newsletter
        $userResponsible->save();

        $postNewsletterData = ['newsletter_id' => $nl->id, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/newsletter/changeresponsible', $postNewsletterData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('backend_newsletter.attached.approver')));
    }

    /**
     *  test /api/v1/newsletter/read/{id?} Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterReadAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $newsletter = Newsletter::where(function ($query) {
                $query->whereNull('end_edit')
                      ->orWhere('end_edit', '<', strtotime(EDIT_TIME_BUFFER_INTERVAL));
            })->first();
        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/api/v1/newsletter/read/'.$newsletter->id);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'Newsletter found.', $newsletter));
        }
    }

    /**
     *  test test /api/v1/newsletter/read/{id?} Not Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterReadNotAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (self::$notAuthorizedUsersRole  as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/api/v1/newsletter/read/123456789');

            $response->assertStatus(401);
            $response->assertViewIs('backend.be_unauthorized_access');
            $response->assertViewHas(['error' => trans('ws_general_controller.webservice.unauthorized')]);
        }
    }

    /**
     *  test /api/v1/newsletter/all Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterAllAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET','/api/v1/newsletter/all', ['lang'=>2]);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'List of newsletter found.'));
        }
    }

    /**
     *  test test /api/v1/newsletter/all Not Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterAllNotAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (self::$notAuthorizedUsersRole  as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/api/v1/newsletter/all');

            $response->assertStatus(401);
            $response->assertViewIs('backend.be_unauthorized_access');
            $response->assertViewHas(['error' => trans('ws_general_controller.webservice.unauthorized')]);
        }
    }

    /**
     *  test /api/v1/newsletter/search Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterSearchAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;

        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET','/api/v1/newsletter/search', ['lang'=>2]);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'List of newsletters found.'));
        }
    }

    /**
     *  test test /api/v1/newsletter/search Not Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterSearchNotAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (self::$notAuthorizedUsersRole  as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET','/api/v1/newsletter/search', ['lang'=>2]);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
     *  test /api/v1/newsletter/package/{id?} Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterPackageAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $newsletter = Newsletter::where(function ($query) {
                $query->whereNull('end_edit')
                      ->orWhere('end_edit', '<', strtotime(EDIT_TIME_BUFFER_INTERVAL));
            })->where('editing_state','<>', STATE_APPROVED)
            ->where('active', 1)
           ->first();
        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                    ->get('/api/v1/newsletter/package/'.$newsletter->id);


            $response->assertStatus(400);
            $response->assertJson(RestController::generateJsonResponse(true, "Newsletter is not in correct state to be sent"));
        }
    }

    /**
     *  test test /api/v1/newsletter/package/{id?} Not Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterPackageNotAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (self::$notAuthorizedUsersRole  as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET','/api/v1/newsletter/package/123456789');

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
     *  test /api/v1/newsletter/send Newsletter doesn't exists Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterSendNewsletterNotExistsAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST','/api/v1/newsletter/send', ['id'=> 123456789987456321]);

            $response->assertStatus(400);
            $response->assertJson(RestController::generateJsonResponse(true, "Newsletter with this ID doesn't exists"));
        }
    }

    /**
     *  test test /api/v1/newsletter/send Not Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterSendNotAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (self::$notAuthorizedUsersRole  as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST','/api/v1/newsletter/send', []);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
     *  test /api/v1/newsletter/sendtest Newsletter doesn't exists Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterSendTestNewsletterNotExistsAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST','/api/v1/newsletter/sendtest',
                    ['id'=> 123456789987456321, 'test_list'=>true, 'emails'=> 'test_email@test.com']);

            $response->assertStatus(400);
            $response->assertJson(RestController::generateJsonResponse(true, "Newsletter with this ID doesn't exists"));
        }
    }

    /**
     *  test test /api/v1/newsletter/sendtest Not Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterSendTestNotAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (self::$notAuthorizedUsersRole  as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST','/api/v1/newsletter/sendtest', []);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
     *  test /api/v1/newsletter/copy Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterCopyNewsletterNotFoundAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;
            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST','/api/v1/newsletter/copy', ['id'=>123456789987654321]);

            $response->assertStatus(404);
            $response->assertJson(RestController::generateJsonResponse(true, 'Newsletter not found.'));
            DB::rollback();
        }
    }

    /**
     *  test test /api/v1/newsletter/copy Not Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterCopyNotAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (self::$notAuthorizedUsersRole  as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST','/api/v1/newsletter/copy', []);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

   /**
    *  test /api/v1/newsletter/create Authorized
    *
    * @return void
    */
   public function testRouteAPINewsletterCreateAuthorized()
   {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $newsletter = ['slug' => 'testslug', 'status' => 'Draft', 'editing_state' => 'Draft', 'active' => 0];
        // OR
        // $user = User::where('active', 1)->first();
        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;
            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST','/api/v1/newsletter/create', $newsletter);
            $response->assertStatus(200);

            $response->assertJson(RestController::generateJsonResponse(false, 'Newsletter was added.', $newsletter));
            DB::rollback();
        }
   }

    /**
     *  test test /api/v1/newsletter/create Not Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterCreateNotAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (self::$notAuthorizedUsersRole  as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST','/api/v1/newsletter/create', []);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
     *  test /api/v1/newsletter/delete Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterDeleteAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $newsletter = Newsletter::where(function ($query) {
                $query->whereNull('end_edit')
                      ->orWhere('end_edit', '<', strtotime(EDIT_TIME_BUFFER_INTERVAL));
            })->first();
        unset($newsletter->updated_at);
        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;
            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('DELETE','/api/v1/newsletter/delete/'.$newsletter->id);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'Newsletter was deleted.', $newsletter));
            DB::rollback();
        }
    }

    /**
     *  test test /api/v1/newsletter/delete Not Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterDeleteNotAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (self::$notAuthorizedUsersRole  as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('DELETE','/api/v1/newsletter/delete');

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
     *  test /api/v1/newsletter/appendtime Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterAppendtimeAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $newsletter = Newsletter::where(function ($query) {
                $query->whereNull('end_edit')
                      ->orWhere('end_edit', '<', strtotime(EDIT_TIME_BUFFER_INTERVAL));
            })->first();
        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;
            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST','/api/v1/newsletter/appendtime/',
                    ['newsletterId' => $newsletter->id]);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'You can edit this newsletter', ['status'=> true]));
            DB::rollback();
        }
    }

    /**
     *  test test /api/v1/newsletter/unlock Not Authorized
     *
     * @return void,
     */
    public function testRouteAPINewsletterAppendtimeNotAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (self::$notAuthorizedUsersRole  as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST','/api/v1/newsletter/appendtime/',
                    ['newsletterId' => 123456789987456123]);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
     *  test /api/v1/newsletter/unlock Authorized
     *
     * @return void
     */
    public function testRouteAPINewsletterUnlockAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $newsletter = Newsletter::where(function ($query) {
                $query->whereNull('end_edit')
                      ->orWhere('end_edit', '<', strtotime(EDIT_TIME_BUFFER_INTERVAL));
            })->first();
        $newsletter->end_edit = null;
        $newsletter->user_edit_id = null;
        unset($newsletter->updated_at);
        foreach (BeAccessNl::getRoles() as $role) {
            $user->role = $role;
            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST','/api/v1/newsletter/unlock/',
                    ['newsletterId' => $newsletter->id]);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'Newsletter is unlocked', $newsletter));
            DB::rollback();
        }
    }

    /**
     *  test test /api/v1/newsletter/unlock Not Authorized
     *
     * @return void,
     */
    public function testRouteAPINewsletterUnlockNotAuthorized()
    {
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach (self::$notAuthorizedUsersRole  as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST','/api/v1/newsletter/unlock/',
                    ['newsletterId' => 123456789987456123]);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }
}
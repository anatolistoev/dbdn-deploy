<?php

namespace Tests\Feature;

use App\Models\Group;
use App\Models\User;
use App\Models\Page;
use App\Models\Subscription;

use App\Http\Controllers\RestController;
use App\Http\Middleware\BeAccessFaqs;
use App\Http\Middleware\BeAccessCms;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BEViewAccessPageEditTest extends TestCase
{
    use DatabaseTransactions;

    /**
     *  test /api/v1/pages/delete/{id} Authorized
     *
     * @return void
     */
    public function testRouteAPIPagesDeleteAuthorized()
    {
        $user = new User(['id' => 1111]);
        $srcPage = Page::find(2207);
        foreach ([USER_ROLE_EDITOR_SMART, \USER_ROLE_ADMIN] as $role) {
            $user->role = $role;
            // Clean user and session
            Auth::logout();
            $this->flushSession();
            // Start transaction!
            DB::beginTransaction();

            // // Copy the page
            // $copyPage = $srcPage->replicate();
            // $copyPage->slug = $srcPage->slug . '_copy';
            // // Copy u_id to id for new pages
            // // $data = DB::select("SHOW TABLE STATUS LIKE 'page'");
            // // $copyPage->id = $data[0]->Auto_increment;
            // $versions = Page::select('version')->where('id', '=', $srcPage->id)->orderBy('version', 'desc')->take(1)->get();
            // $copyPage->version = (int)$versions[0]->version + 1;
            // $copyPage->parent_id = $srcPage->parent_id;
            // $copyPage->save();

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('DELETE', '/api/v1/pages/delete/'. $srcPage->u_id);
            
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, "Page was deleted."));
            // Rollback and then return errors
            DB::rollback();
        }
    }

    /**
     *  test /api/v1/pages/delete/{id} Not Authorized
     *
     * @return void
     */
    public function testRouteAPIPagesDeleteNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR_DFM;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('DELETE', '/api/v1/pages/delete/2207');

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/pages/copy Authorized
     *
     * @return void
     */
    public function testRouteAPIPagesCopyAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $srcPage = Page::find(2207); // Page from smart
        foreach ([USER_ROLE_EDITOR_SMART, \USER_ROLE_ADMIN] as $role) {
            $user->role = $role;
            // Clean user and session
            Auth::logout();
            $this->flushSession();
            // Start transaction!
            DB::beginTransaction();
            $postPageData = ['u_id' => $srcPage->u_id, 'parent_id' => $srcPage->parent_id, 'position' => 1, 'action' => 'copy'];
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('POST', '/api/v1/pages/copy', $postPageData);
            
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, "Page was added."));

            $copyPage = Page::find($response->original['response']['id']);

            $fieldsToCheck = ['visits'];
            $srcPageData = array_intersect_key($srcPage->getAttributes(), array_flip($fieldsToCheck));
            $resultPageData = array_intersect_key($copyPage->getAttributes(), array_flip($fieldsToCheck));
            $this->assertNotEquals($srcPageData, $resultPageData);

            // Rollback and then return errors
            DB::rollback();
        }
    }

    /**
     *  test /api/v1/pages/copy Not Authorized
     *
     * @return void
     */
    public function testRouteAPIPagesCopyNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $srcPage = Page::find(2207); // Page from smart
        $user->role = USER_ROLE_EDITOR_DFM;

        $postPageData = ['u_id' => $srcPage->u_id, 'parent_id' => $srcPage->parent_id, 'position' => 1, 'action' => 'copy'];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/pages/copy', $postPageData);

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/pages/update Authorized
     *
     * @return void
     */
    public function testRouteAPIPagesUpdateAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        // Setup page
        $srcPage = Page::alive()
            ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
            ->where('hierarchy.parent_id', SMART)
            ->where('hierarchy.level', 3)
            ->first();
        $srcPage->active = 0;
        $srcPage->save();
        $srcPage->workflow->editing_state = STATE_DRAFT;
        $srcPage->workflow->save();

        // Setup user
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        foreach ([USER_ROLE_EDITOR_SMART, \USER_ROLE_ADMIN] as $role) {
            $user->role = $role;
            // Clean user and session
            Auth::logout();
            $this->flushSession();
            // Start transaction!
            DB::beginTransaction();
            $postPageData = ['u_id' => $srcPage->u_id, 'position' => 1];
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('PUT', '/api/v1/pages/update', $postPageData);
            
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, "Page was updated."));
            // Rollback and then return errors
            DB::rollback();
        }
    }

    /**
     *  test /api/v1/pages/update Not Authorized
     *
     * @return void
     */
    public function testRouteAPIPagesUpdateNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $srcPage = Page::find(2207); // Page from smart
        $user->role = USER_ROLE_EDITOR_DFM;

        $postPageData = ['u_id' => $srcPage->u_id, 'position' => 1];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/pages/update', $postPageData);

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/pages/update Not Authorized state Approved
     *
     * @return void
     */
    public function testRouteAPIPagesUpdateNotAuthorizedStateApproved()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        // Setup page
        $srcPage = Page::find(2207); // Page from smart
        $srcPage->workflow->editing_state = STATE_APPROVED;
        $srcPage->workflow->save();

        // Setup user
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = \USER_ROLE_EDITOR_SMART;

        $postPageData = ['u_id' => $srcPage->u_id, 'position' => 1, 'slug' => 'smart_our_colours Modified 2' ];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/pages/update', $postPageData);
        
        $response->assertStatus(403);

        $responsible_name = "";
        if ($srcPage->workflow->user_responsible > 0) {
            $user = User::find($srcPage->workflow->user_responsible);
            if (!is_null($user)) {
                $responsible_name = trim($user->first_name) . ' ' . trim($user->last_name);
            }
        }            
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.page.state_error', ['name' => $responsible_name])));
    }

    /**
     *  test /api/v1/pages/update Possition Authorized state Approved
     *
     * @return void
     */
    public function testRouteAPIPagesUpdatePositionAuthorizedStateApproved()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        // Setup page
        $srcPage = Page::alive()
            ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
            ->where('hierarchy.parent_id', SMART)
            ->where('hierarchy.level', 3)
            ->first();
        $srcPage->workflow->editing_state = STATE_APPROVED;
        $srcPage->workflow->save();

        // Setup user
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = \USER_ROLE_EDITOR;

        $postPageData = ['u_id' => $srcPage->u_id, 'position' => 22];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/pages/update', $postPageData);
        
        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, "Page was updated."));
    }

    /**
     *  test /api/v1/pages/update Activate Authorized state Approved
     *
     * @return void
     */
    public function testRouteAPIPagesUpdateActivateAuthorizedStateApproved()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        // Setup page
        $srcPage = Page::alive()
            ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
            ->where('hierarchy.parent_id', SMART)
            ->where('hierarchy.level', 3)
            ->first();
        $srcPage->active = 0;
        $srcPage->save();
        $srcPage->workflow->editing_state = STATE_APPROVED;
        $srcPage->workflow->save();

        // Setup user
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = \USER_ROLE_EDITOR;

        $postPageData = ['u_id' => $srcPage->u_id, 'active' => 1];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/pages/update', $postPageData);
        
        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, "Page was updated."));
    }

    /**
     *  test /api/v1/pages/update Authorized but readonly fields
     *
     * @return void
     */
    public function testRouteAPIPagesUpdateAuthorizedReadonlyAttributes()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        // Setup page
        $srcPage = Page::alive()
            ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
            ->where('hierarchy.parent_id', SMART)
            ->where('hierarchy.level', 3)
            ->first();
        $srcPage->active = 0;
        $srcPage->save();
        $srcPage->workflow->editing_state = STATE_DRAFT;
        $srcPage->workflow->save();

        // Setup user
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = \USER_ROLE_EDITOR_SMART;
        
        // Modify some params
        $postPageData = ['u_id' => $srcPage->u_id,
            'id' => $srcPage->id + 100,
            'visits' => $srcPage->visits + 100,
            'deletable' => (($srcPage->deletable != false)? 0: 1),
            'deleted' => (($srcPage->deleted != false)? 0: 1)
        ];
        
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/pages/update', $postPageData);
        
        $response->assertStatus(200);

        $srcPageData = array_intersect_key($srcPage->getAttributes(), $postPageData);
        $resultPageData = array_intersect_key($response->original['response'], $postPageData);
        $this->assertEquals($srcPageData, $resultPageData);

        $destPage = Page::find($srcPage->u_id); // Reload the page from DB
        $savedPageData = array_intersect_key($destPage->getAttributes(), $postPageData);
        $this->assertEquals($srcPageData, $savedPageData);
    }

    /**
     *  test /api/v1/pages/update Authorized but unknown fields
     *
     * @return void
     */
    public function testRouteAPIPagesUpdateAuthorizedUnknownAttributes()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        // Setup page
        $srcPage = Page::alive()
            ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
            ->where('hierarchy.parent_id', SMART)
            ->where('hierarchy.level', 3)
            ->first();
        $srcPage->active = 0;
        $srcPage->save();
        $srcPage->workflow->editing_state = STATE_DRAFT;
        $srcPage->workflow->save();

        // Setup user
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = \USER_ROLE_EDITOR_SMART;

        $postPageData = ['u_id' => $srcPage->u_id, 'visits' => 'Hello!', "syss" => 1 ];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/pages/update', $postPageData);
        
        $response->assertStatus(500);

        $response->assertJson(RestController::generateJsonResponse(true, "Unknown column."));
    }

    /**
     *  test /api/v1/pages/create Authorized
     *
     * @return void
     */
    public function testRouteAPIPagesCreateAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $srcPage = Page::find(2207); // Page from smart
        foreach ([USER_ROLE_EDITOR_SMART, \USER_ROLE_ADMIN] as $role) {
            $user->role = $role;
            // Clean user and session
            Auth::logout();
            $this->flushSession();
            // Start transaction!
            DB::beginTransaction();
            $copyPage = $srcPage->replicate();
            $copyPage->slug = $srcPage->slug . '_copy';
            $copyPage->parent_id = $srcPage->id;

            $postPageData = $copyPage->toArray();
            unset($postPageData['id']);
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('POST', '/api/v1/pages/create', $postPageData);
            
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, "Page was added."));
            // Rollback and then return errors
            DB::rollback();
        }
    }

    /**
     *  test /api/v1/pages/create Not Authorized  EDITOR_DFM in Smart
     *
     * @return void
     */
    public function testRouteAPIPagesCreateNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $srcPage = Page::find(2207); // Page from smart
        $user->role = USER_ROLE_EDITOR_DFM;

        $postPageData = ['slug' => $srcPage->slug, 'parent_id' => $srcPage->parent_id, 'langs' => $srcPage->langs, 
            'position' => 1, 'template' => $srcPage->template];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/pages/create', $postPageData);

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/pages/create Not Authorized EDITOR_SMART in Daimler
     *
     * @return void
     */
    public function testRouteAPIPagesCreateNotAuthorizedSmartEditorInDimler()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_EDITOR_SMART;

        $parentPage = Page::where('id', DAIMLER)->where('active', 1)->first(); // Root page from DAIMLER
        $srcPage = Page::find(2207); // Page from smart

        $postPageData = ['slug' => $srcPage->slug, 'parent_id' => $parentPage->parent_id, 'langs' => $srcPage->langs, 
            'position' => 1, 'template' => $srcPage->template];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/pages/create', $postPageData);

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/pages/sendalert/{page_id} Authorized
     *
     * @return void
     */
    public function testRouteAPIPagesSendAlertAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $page = Page::find(2207); // Page from smart
        $page->active = 1;
        $page->save();
        $subscriptions = Subscription::where('page_id', $page->id)->where('active', 1)->with('user')->get();
        $emaiDriver = app()->make('swift.transport')->driver();

        foreach ([USER_ROLE_EDITOR_SMART, \USER_ROLE_ADMIN] as $role) {
            $user->role = $role;
            // Clean user and session
            Auth::logout();
            $this->flushSession();
            $emaiDriver->flush();

            $updateAlert = ['comments_en' => 'Comment EN', 'comments_de' => 'Comment DE'];
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                ->json('PUT', '/api/v1/pages/sendalert/' . $page->id, $updateAlert);
                
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, "Update alert was sent."));
            $emailMessages = $emaiDriver->messages();
            $this->assertCount($subscriptions->count(), $emailMessages);
            $emails = [];
            foreach ($emailMessages as $emailMessage) {
                $emails = array_merge($emails, array_keys($emailMessage->getTo()));
            }
            foreach ($subscriptions as $subscription) {
                $user = $subscription->user()->first();
                $this->assertContains($user->email, $emails);
            }
        }
    }

    /**
     *  test /api/v1/pages/sendalert/{page_id} Not Authorized
     *
     * @return void
     */
    public function testRouteAPIPagesSendAlertNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $page = Page::find(2207); // Page from smart
        $page->active = 1;
        $page->save();
        $user->role = USER_ROLE_EDITOR_DFM;

        $updateAlert = ['comments_en' => 'Comment EN', 'comments_de' => 'Comment DE'];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/pages/sendalert/' . $page->id, $updateAlert);

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/pages/versioncopy Authorized
     *
     * @return void
     */
    public function testRouteAPIPagesVersionCopyAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user    = User::where('active', 1)->first();

        $srcPage = Page::find(2207); // Page from smart

        foreach ([USER_ROLE_EDITOR_SMART, \USER_ROLE_ADMIN] as $role) {
            $user->role = $role;
            // Clean user and session
            Auth::logout();
            $this->flushSession();
            // Start transaction!
            DB::beginTransaction();
            $postPageData = ['id' => $srcPage->u_id, 'lang' => 2];
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('POST', '/api/v1/pages/versioncopy', $postPageData);
            
            $response->assertStatus(200);
            $copyPageAttributes = $srcPage->getAttributes();
            unset($copyPageAttributes['u_id']); // New u_id
            unset($copyPageAttributes['version']); // New Version
            $copyPageAttributes['active'] = 0;
            $copyPageAttributes['end_edit'] = null;
            $copyPageAttributes['user_edit_id'] = null;
            $copyPageAttributes['created_by'] = $user->id;
            $copyPageAttributes['is_visible'] = 0;
            $copyPageAttributes['publish_date'] = DATE_TIME_ZERO;
            $copyPageAttributes['unpublish_date'] = DATE_TIME_ZERO;
            unset($copyPageAttributes['deleted_at']); // Hidden!
    
            $response->assertJson($copyPageAttributes);
            // Rollback and then return errors
            DB::rollback();
        }
    }

    /**
     *  test /api/v1/pages/versioncopy Not Authorized EDITOR_DFM in Smart
     *
     * @return void
     */
    public function testRouteAPIPagesVersionCopyNotAuthorizedEditorDTFInSmart()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $srcPage = Page::find(2207); // Page from smart
        $user->role = USER_ROLE_EDITOR_DFM;

        $postPageData = ['id' => $srcPage->u_id, 'lang' => 2];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('POST', '/api/v1/pages/versioncopy', $postPageData);

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/pages/versioncopy Not Authorized EDITOR_SMART in Daimler
     *
     * @return void
     */
    public function testRouteAPIPagesVersionCopyNotAuthorizedEditorSmartInDaimler()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $srcPage = Page::where('parent_id', DAIMLER)->where('active', 1)->first(); // Sub page in root of DAIMLER

        $user->role = \USER_ROLE_EDITOR_SMART;

        $postPageData = ['id' => $srcPage->u_id, 'lang' => 2];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('POST', '/api/v1/pages/versioncopy', $postPageData);

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/pages/search empty result for Not Authorized EDITOR in Smart
     *
     * @return void
     */
    public function testRouteAPIPagesSearchEmptyResultNotAuthorizedEditorInSmart()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $srcPage = Page::join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->join('content', 'content.page_u_id', '=', 'page.u_id')
                ->where('page.active', 1)
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->where('content.section','title')
                ->select('page.*', 'content.body', 'content.lang')
                ->first(); // Page from smart
        foreach ([USER_ROLE_EDITOR_DFM, USER_ROLE_EDITOR_MB, USER_ROLE_EDITOR_DFS, USER_ROLE_EDITOR_DTF] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('GET', '/api/v1/pages/search', ['filter' => $srcPage->body, 'lang'=> $srcPage->lang ]);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'List of pages found.', []));
        }
    }

        /**
     *  test /api/v1/pages/search get result for Authorized Editor in Smart
     *
     * @return void
     */
    public function testRouteAPIPagesSearchAuthorizedEditorInSmartGetResult()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $postPageData = Page::join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->join('content', 'content.page_u_id', '=', 'page.u_id')
                ->where('page.active', 1)
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->where('content.section','title')
                ->select('page.*', 'content.body', 'content.lang')
                ->first(); // Page from smart
        $body = $postPageData->body ;
        $lang = $postPageData->lang;
        unset($postPageData->lang);
        unset($postPageData->body);
        foreach ([USER_ROLE_EDITOR_SMART, USER_ROLE_ADMIN, USER_ROLE_EDITOR] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('GET', '/api/v1/pages/search', ['filter' => $body, 'lang'=> $lang ]);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'List of pages found.', [$postPageData->toArray()]));
        }
    }

    /**
     *  test /api/v1/pages/search Not Authorized Member
     *
     * @return void
     */
    public function testRouteAPIPagesSearchNotAuthorizedMember()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_MEMBER;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('GET', '/api/v1/pages/search', []);

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/pages/deleted/{lang} Authorized Member
     *
     * @return void
     */
    public function testRouteAPIPagesGetDeletedAuthorizedMember(){
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;

        foreach ([USER_ROLE_EDITOR_SMART, USER_ROLE_EDITOR_DTF, USER_ROLE_ADMIN, USER_ROLE_EDITOR] as $role) {
            $user->role = $role;
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('GET', '/api/v1/pages/deleted/2');

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'List of pages found.'));
        }
    }

       /**
     *  test /api/v1/pages/deleted/{lang} Not Authorized Member
     *
     * @return void
     */
    public function testRouteAPIPagesGetDeletedNotAuthorizedMember(){
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_MEMBER;
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
        ->json('GET', '/api/v1/pages/deleted/2');

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/pages/appendtime Not Authorizer USER_ROLE_EDITOR_DFM, USER_ROLE_EDITOR_MB, USER_ROLE_EDITOR_DFS, USER_ROLE_EDITOR_DTF in Smart
     *
     * @return void
     */
    public function testRouteAPIPagesAppendtimeNotAuthorizedEditorInSmart()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $srcPage = Page::alive()
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->select('page.*')
                ->first(); // Page from smart
        foreach ([USER_ROLE_EDITOR_DFM, USER_ROLE_EDITOR_MB, USER_ROLE_EDITOR_DFS, USER_ROLE_EDITOR_DTF] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('POST', '/api/v1/pages/appendtime', ['pageId' => $srcPage->id]);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
     *  test /api/v1/pages/appendtime Authorizer EDITOR in Smart
     *
     * @return void
     */
    public function testRouteAPIPagesAppendtimeAuthorizedEditorInSmart()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $srcPage = Page::alive()
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->where(function ($query) {
                    $query->whereNull('page.end_edit')
                            ->orWhere('page.end_edit', '<', strtotime(EDIT_TIME_BUFFER_INTERVAL));
                 })
                ->select('page.*')
                ->first(); // Page from smart
        foreach ([USER_ROLE_EDITOR_SMART, USER_ROLE_ADMIN, USER_ROLE_EDITOR] as $role) {
            $user->role = $role;

            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('POST', '/api/v1/pages/appendtime', ['pageId' => $srcPage->id]);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'You can edit this page', ["status"=> true]));
            DB::rollback();
        }
    }

    /**
     *  test /api/v1/pages/appendtime Not Authorized Member
     *
     * @return void
     */
    public function testRouteAPIPagesAppendtimeNotAuthorizedMember(){
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_MEMBER;
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
        ->json('POST', '/api/v1/pages/appendtime');

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

        /**
     *  test /api/v1/pages/unlock Not Authorizer USER_ROLE_EDITOR_DFM, USER_ROLE_EDITOR_MB, USER_ROLE_EDITOR_DFS, USER_ROLE_EDITOR_DTF in Smart
     *
     * @return void
     */
    public function testRouteAPIPagesUnlockNotAuthorizedEditorInSmart()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $srcPage = Page::alive()
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->select('page.*')
                ->first(); // Page from smart
        foreach ([USER_ROLE_EDITOR_DFM, USER_ROLE_EDITOR_MB, USER_ROLE_EDITOR_DFS, USER_ROLE_EDITOR_DTF] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('POST', '/api/v1/pages/unlock', ['pageId' => $srcPage->id]);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
     *  test /api/v1/pages/unlock Authorizer Editor in Smart
     *
     * @return void
     */
    public function testRouteAPIPagesUnlockAuthorisedEditorInSmart()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $srcPage = Page::alive()
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->where(function ($query) {
                    $query->whereNull('page.end_edit')
                            ->orWhere('page.end_edit', '<', strtotime(EDIT_TIME_BUFFER_INTERVAL));
                 })
                ->select('page.*')
                ->first(); // Page from smart
        unset($srcPage->updated_at);
        $srcPage->end_edit = null;
        $srcPage->user_edit_id = null;
        foreach ([USER_ROLE_EDITOR_SMART, USER_ROLE_ADMIN, USER_ROLE_EDITOR] as $role) {
            $user->role = $role;

            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('POST', '/api/v1/pages/unlock', ['pageId' => $srcPage->u_id]);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'Page is unlocked', $srcPage));
            DB::rollback();
        }
    }

    /**
     *  test /api/v1/pages/unlock Not Authorized Member
     *
     * @return void
     */
    public function testRouteAPIPagesUnlockNotAuthorizedMember(){
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_MEMBER;
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
        ->json('POST', '/api/v1/pages/unlock');

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

     /**
     *  test /api/v1/pages/restore Not Authorizer USER_ROLE_EDITOR_DFM, USER_ROLE_EDITOR_MB, USER_ROLE_EDITOR_DFS, USER_ROLE_EDITOR_DTF in Smart
     *
     * @return void
     */
    public function testRouteAPIPagesRestoreNotAuthorizedEditorInSmart()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $srcPage = Page::onlyTrashed()
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->select('page.*')
                ->first(); // Page from smart
        foreach ([USER_ROLE_EDITOR_DFM, USER_ROLE_EDITOR_MB, USER_ROLE_EDITOR_DFS, USER_ROLE_EDITOR_DTF] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('PUT', '/api/v1/pages/restore', ['id' => $srcPage->id]);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, 'You don\'t have access to restore this page!'));
        }
    }

    /**
     *  test /api/v1/pages/restore Authorizer Editor in Smart
     *
     * @return void
     */
    public function testRouteAPIPagesRestoreAuthorizedEditorInSmart()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $srcPage = Page::onlyTrashed()
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->select('page.*')
                ->first(); // Page from smart
        unset($srcPage->updated_at);
        unset($srcPage->position);
        unset($srcPage->parent_id);
        $srcPage->active = 0;
        $srcPage->deleted_at = null;

        foreach ([USER_ROLE_EDITOR_SMART, USER_ROLE_ADMIN, USER_ROLE_EDITOR] as $role) {
            $user->role = $role;

            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('PUT', '/api/v1/pages/restore', ['id' => $srcPage->u_id]);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'Restore page.', $srcPage));
            DB::rollback();
        }
    }

    /**
     *  test /api/v1/pages/restore Not Authorized Member
     *
     * @return void
     */
    public function testRouteAPIPagesRestoreNotAuthorizedMember(){
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_MEMBER;
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
        ->json('PUT', '/api/v1/pages/restore');

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/pages/permanentdelete/{ID} Not Authorizer USER_ROLE_EDITOR_DFM, USER_ROLE_EDITOR_MB, USER_ROLE_EDITOR_DFS, USER_ROLE_EDITOR_DTF
     *
     * @return void
     */
    public function testRouteAPIPagesPermanentdeleteNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;

        foreach ([USER_ROLE_EDITOR_SMART, USER_ROLE_EDITOR_DFM, USER_ROLE_EDITOR_MB, USER_ROLE_EDITOR_DFS, USER_ROLE_EDITOR_DTF] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('DELETE', '/api/v1/pages/permanentdelete/123456789987654321');

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
     *  test /api/v1/pages/permanentdelete/{ID Authorizer Editor for Delete
     *
     * @return void
     */
    public function testRouteAPIPagesPermanentdeleteAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $srcPage = DB::table('page')
                ->whereNotNull('page.deleted_at')
                ->where('page.is_visible', '=', 1)
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->select('page.*')
                ->first(); // Page from smart
        unset($srcPage->updated_at);

        foreach ([USER_ROLE_ADMIN, USER_ROLE_EDITOR] as $role) {
            $user->role = $role;

            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('DELETE', '/api/v1/pages/permanentdelete/'.$srcPage->u_id);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'Page was permanently deleted.'));
            DB::rollback();
        }
    }

}

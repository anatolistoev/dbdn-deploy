<?php

namespace Tests\Feature;

use App\Models\Group;
use App\Models\User;
use App\Models\Page;
use App\Models\Subscription;

use App\Http\Controllers\RestController;
use App\Http\Middleware\BeAccessFaqs;
use App\Http\Middleware\BeAccessApprover;
use App\Http\Middleware\BeAccessCms;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BEViewAccessWorkflowTest extends TestCase
{
    use DatabaseTransactions;

    /**
     *  test /cms/ Authorized
     *
     * @return void
     */
    public function testRouteCMSWelcomeAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach (BeAccessCms::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/');

            $response->assertStatus(200);
            $response->assertViewIs('backend.be_welcome');
            $response->assertViewHas(['ACTIVE' => 'workflow', 'GROUPS' => Group::all(), 'LANG']);
        }
    }

    /**
     *  test /api/v1/workflow/update Not Authorized Editor from other brand
     *
     * @return void
     */
    public function testRouteAPIWorkflowUpdateNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $page = Page::find(2207); // Page from smart
        $user->role = USER_ROLE_EDITOR_DFM;

        $page->workflow->editing_state = STATE_REVIEW;
        $page->workflow->save();
        $postWorkflowData = ['id' => $page->workflow->id, 'editing_state' => $page->workflow->editing_state, 'user_responsible' => $page->workflow->user_responsible];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/workflow/update', $postWorkflowData);

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/workflow/update Authorized Change of State Draft to Review
     *
     * @return void
     */
    public function testRouteAPIWorkflowUpdateStateDraftReviewAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $page = Page::find(2207); // Page from smart
        foreach ([USER_ROLE_EDITOR_SMART, \USER_ROLE_ADMIN] as $role) {
            $user->role = $role;
            // Clean user and session
            Auth::logout();
            $this->flushSession();
            // Start transaction!
            DB::beginTransaction();
            $page->workflow->editing_state = \STATE_DRAFT;
            $page->workflow->save();
            $postWorkflowData = ['id' => $page->workflow->id, 'editing_state' => \STATE_REVIEW, 'user_responsible' => $page->workflow->user_responsible];
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
                ->json('PUT', '/api/v1/workflow/update', $postWorkflowData);
            
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, "Workflow was updated."));
            // Rollback and then return errors
            DB::rollback();
        }
    }

    /**
     *  test /api/v1/workflow/update Authorized Change of State Review to Draft
     *
     * @return void
     */
    public function testRouteAPIWorkflowUpdateStateReviewDraftAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $page = Page::find(2207); // Page from smart
        $user->role = USER_ROLE_EDITOR; // Approver

        $page->workflow->editing_state = \STATE_REVIEW;
        $page->workflow->save();
        $postWorkflowData = ['id' => $page->workflow->id, 'editing_state' => \STATE_DRAFT, 'user_responsible' => $page->workflow->user_responsible];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/workflow/update', $postWorkflowData);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'Workflow was updated.'));
    }

    /**
     *  test /api/v1/workflow/update Not Authorized Change of State Review to Draft
     *
     * @return void
     */
    public function testRouteAPIWorkflowUpdateStateReviewDraftNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $page = Page::find(2207); // Page from smart
        $user->role = USER_ROLE_EDITOR_SMART;

        $page->workflow->editing_state = \STATE_REVIEW;
        $page->workflow->save();
        $postWorkflowData = ['id' => $page->workflow->id, 'editing_state' => \STATE_DRAFT, 'user_responsible' => $page->workflow->user_responsible];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/workflow/update', $postWorkflowData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('backend_pages.workflow.editor.change_state')));
    }

    /**
     *  test /api/v1/workflow/update Not Authorized Change of State Draft to Apprved
     *
     * @return void
     */
    public function testRouteAPIWorkflowUpdateStateDraftApprovedNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_EDITOR_SMART;

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->where('active', 1)->first();
        $userResponsible->role = USER_ROLE_EDITOR; // Approver
        $userResponsible->save();

        // Setup page and workflow
        $page = Page::find(2207); // Page from smart
        $page->workflow->editing_state = \STATE_DRAFT;
        $page->workflow->save();

        $postWorkflowData = ['id' => $page->workflow->id, 'editing_state' => \STATE_APPROVED, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/workflow/update', $postWorkflowData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('backend_pages.workflow.editor.change_state')));
    }

    /**
     *  test /api/v1/workflow/update Not Authorized Change of State Review to Apprved
     *
     * @return void
     */
    public function testRouteAPIWorkflowUpdateStateReviewApprovedNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_EDITOR_SMART;

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->where('active', 1)->first();
        $userResponsible->role = USER_ROLE_EDITOR; // Approver
        $userResponsible->save();

        // Setup page and workflow
        $page = Page::find(2207); // Page from smart
        $page->workflow->editing_state = \STATE_REVIEW;
        $page->workflow->save();

        $postWorkflowData = ['id' => $page->workflow->id, 'editing_state' => \STATE_APPROVED, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/workflow/update', $postWorkflowData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('backend_pages.workflow.editor.change_state')));
    }

    /**
     *  test /api/v1/workflow/update Not Authorized Change of State Approved to Review
     *
     * @return void
     */
    public function testRouteAPIWorkflowUpdateStateApprovedReviewNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $page = Page::find(2207); // Page from smart
        $user->role = USER_ROLE_EDITOR_SMART;

        $page->workflow->editing_state = \STATE_APPROVED;
        $page->workflow->save();
        $postWorkflowData = ['id' => $page->workflow->id, 'editing_state' => \STATE_REVIEW, 'user_responsible' => $page->workflow->user_responsible];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/workflow/update', $postWorkflowData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('backend_pages.workflow.editor.change_state')));
    }

    /**
     *  test /api/v1/workflow/update Authorized Change of Responsible to Approver
     *
     * @return void
     */
    public function testRouteAPIWorkflowUpdateResponsibleAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_EDITOR;
        $page = Page::find(2207); // Page from smart

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = USER_ROLE_EDITOR; // Approver
        $userResponsible->save();

        $page->workflow->editing_state = STATE_REVIEW;
        $page->workflow->save();

        $postWorkflowData = ['id' => $page->workflow->id, 'editing_state' => $page->workflow->editing_state, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/workflow/update', $postWorkflowData);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'Workflow was updated.'));
    }

    /**
     *  test /api/v1/workflow/update Authorized Change of Responsible to Unassaign
     *
     * @return void
     */
    public function testRouteAPIWorkflowUpdateResponsibleUnassignAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_EDITOR;
        $page = Page::find(2207); // Page from smart

        $page->workflow->editing_state = \STATE_REVIEW;
        $page->workflow->save();

        $postWorkflowData = ['id' => $page->workflow->id, 'editing_state' => \STATE_DRAFT, 'user_responsible' => 0];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/workflow/update', $postWorkflowData);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'Workflow was updated.'));
    }

    /**
     *  test /api/v1/workflow/update Not Authorized Change of Responsible to Approver
     *
     * @return void
     */
    public function testRouteAPIWorkflowUpdateResponsibleApproverNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_EDITOR_SMART;
        $page = Page::find(2207); // Page from smart

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = USER_ROLE_EDITOR;
        $userResponsible->save();

        $page->workflow->editing_state = STATE_REVIEW;
        $page->workflow->save();

        $postWorkflowData = ['id' => $page->workflow->id, 'editing_state' => $page->workflow->editing_state, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/workflow/update', $postWorkflowData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('backend_pages.workflow.editor.change_state')));
    }

    /**
     *  test /api/v1/workflow/update Not Authorized Change of Responsible to Editor of SMART
     *
     * @return void
     */
    public function testRouteAPIWorkflowUpdateResponsibleNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_EDITOR_SMART;
        $page = Page::find(2207); // Page from smart

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = USER_ROLE_EDITOR_SMART;
        $userResponsible->save();

        $page->workflow->editing_state = STATE_REVIEW;
        $page->workflow->save();

        $postWorkflowData = ['id' => $page->workflow->id, 'editing_state' => $page->workflow->editing_state, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/workflow/update', $postWorkflowData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('backend_pages.workflow.editor.change_state')));
    }

    /**
     *  test /api/v1/workflow/update Not Authorized Change of Responsible to Editor of other brand
     *
     * @return void
     */
    public function testRouteAPIWorkflowUpdateResponsibleOtherBrandNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = \USER_ROLE_EDITOR_SMART;
        $page = Page::find(2207); // Page from smart

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = \USER_ROLE_EDITOR_DTF;
        $userResponsible->save();

        $page->workflow->editing_state = \STATE_DRAFT;
        $page->workflow->save();
        
        $postWorkflowData = ['id' => $page->workflow->id, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/workflow/update', $postWorkflowData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('backend_pages.workflow.attached.approver')));
    }

    /**
     *  test /api/v1/workflow/changeresponsible Authorized Change of Responsible to Approver
     *
     * @return void
     */
    public function testRouteAPIWorkflowChangeResponsibleAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_EDITOR_SMART;
        $page = Page::find(2207); // Page from smart

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = USER_ROLE_EDITOR; // Approver
        $userResponsible->save();

        $page->workflow->editing_state = STATE_REVIEW;
        $page->workflow->save();
        $postWorkflowData = ['page_id' => $page->id, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/workflow/changeresponsible', $postWorkflowData);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'Workflow was updated.'));
    }

    /**
     *  test /api/v1/workflow/changeresponsible Not Authorized Change of Responsible from Editor of other brand
     *
     * @return void
     */
    public function testRouteAPIWorkflowChangeResponsibleEditorOtherBrandNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_EDITOR_DTF;
        $page = Page::find(2207); // Page from smart

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = USER_ROLE_EDITOR; // Approver
        $userResponsible->save();

        $page->workflow->editing_state = STATE_REVIEW;
        $page->workflow->save();

        $postWorkflowData = ['page_id' => $page->id, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/workflow/changeresponsible', $postWorkflowData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/workflow/changeresponsible Not Authorized Change of Responsible to empty
     *
     * @return void
     */
    public function testRouteAPIWorkflowChangeResponsibleEmptyNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_EDITOR_SMART;
        $page = Page::find(2207); // Page from smart

        $page->workflow->editing_state = STATE_REVIEW;
        $page->workflow->save();

        $postWorkflowData = ['page_id' => $page->id, 'user_responsible' => 0];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/workflow/changeresponsible', $postWorkflowData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('backend_pages.workflow.attached.approver')));
    }

    /**
     *  test /api/v1/workflow/changeresponsible Not Authorized Change of Responsible to Editor of SMART in Review
     *
     * @return void
     */
    public function testRouteAPIWorkflowChangeResponsibleReviewEditorSmartNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_EDITOR_SMART;
        $page = Page::find(2207); // Page from smart

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = USER_ROLE_EDITOR_SMART;
        $userResponsible->save();

        $page->workflow->editing_state = STATE_REVIEW;
        $page->workflow->save();
        $postWorkflowData = ['page_id' => $page->id, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/workflow/changeresponsible', $postWorkflowData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('backend_pages.workflow.attached.approver')));
    }

    /**
     *  test /api/v1/workflow/changeresponsible Not Authorized Change of Responsible to Editor of other brand in Draft
     *
     * @return void
     */
    public function testRouteAPIWorkflowChangeResponsibleDraftEditorOtherBrandNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $user->role = USER_ROLE_EDITOR_SMART;
        $page = Page::find(2207); // Page from smart

        $userResponsible = User::where('role', USER_ROLE_MEMBER)->first();
        $userResponsible->role = \USER_ROLE_EDITOR_DTF; // Editor that has no access to this page!
        $userResponsible->save();

        $page->workflow->editing_state = \STATE_DRAFT;
        $page->workflow->save();

        $postWorkflowData = ['page_id' => $page->id, 'user_responsible' => $userResponsible->id];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->withServerVariables(['REMOTE_ADDR' => '127.0.0.1'])
            ->json('PUT', '/api/v1/workflow/changeresponsible', $postWorkflowData);

        $response->assertStatus(403);
        $response->assertJson(RestController::generateJsonResponse(true, trans('backend_pages.workflow.attached.approver')));
    }
}
<?php

namespace Tests\Feature;

use App\Models\FileItem;
use App\Models\User;
use App\Models\Page;
use App\Models\Download;
use App\Http\Controllers\RestController;
use App\Http\Controllers\FileController;
use App\Http\Middleware\BeAccessApprover;
use App\Http\Middleware\BeAccessCms;
use App\Http\Middleware\BeAccessNlOrCms;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;

class BEViewAccessFilesTest extends TestCase
{
    use DatabaseTransactions;

    public static function createUploadedFile(string $pdfFullPath, $originalName = null): UploadedFile 
    {
        $temp_file = tempnam(sys_get_temp_dir(), 'TestUploadedFile');
        if (!copy($pdfFullPath, $temp_file)) {
            throw new \Exception("Cannot create Test UploadedFile from: " . $pdfFullPath);
        }
        if (is_null($originalName)) { 
            $originalName = basename($pdfFullPath);
        }
        // __construct($path, $originalName, $mimeType = null, $size = null, $error = null, $test = false)
        return new UploadedFile($temp_file, $originalName, null, null, null, true);
    }

    /**
     * WEB Routes
     */

    /**
     *  test /cms/downloads Authorized
     *
     * @return void
     */
    public function testRouteCMSDownloadsAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach (BeAccessCms::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/downloads');

            $response->assertStatus(200);
            $response->assertViewIs('backend.be_downloads');
            $response->assertViewHas(['ACTIVE' => 'downloads', 'FOLDER_STATUS', 'LANG', 'FILEMANAGER_MODE'   => 'downloads',]);
        }
    }

    /**
     *  test /cms/downloads Not Authorized
     *
     * @return void
     */
    public function testRouteCMSDownloadsNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach ([USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR, USER_ROLE_MEMBER] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/downloads');

            if ($user->role == USER_ROLE_MEMBER) {
                $response->assertStatus(200);
                $response->assertViewIs('backend.be_login');
            } else {
                $response->assertStatus(401);
                $response->assertViewIs('backend.be_unauthorized_access');
                $response->assertViewHas(['error' => trans('ws_general_controller.webservice.unauthorized')]);
            }
        }
    }
    /**
     *  test /cms/images Authorized
     *
     * @return void
     */
    public function testRouteCMSImagesAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach (BeAccessCms::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/images');

            $response->assertStatus(200);
            $response->assertViewIs('backend.be_images');
            $response->assertViewHas(['ACTIVE' => 'images', 'FOLDER_STATUS', 'LANG', 'FILEMANAGER_MODE'   => 'images',]);
        }
    }

    /**
     *  test /cms/images Not Authorized
     *
     * @return void
     */
    public function testRouteCMSImagesNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_MEMBER;

         $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/images');
         $response->assertStatus(200);
         $response->assertViewIs('backend.be_login');
    }

    /**
     *  test /cms/filemanager Authorized
     *
     * @return void
     */
    public function testRouteCMSFilemanagerAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach (BeAccessCms::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/filemanager');

            $response->assertStatus(200);
            $response->assertViewIs('backend.be_filemanager');
            $response->assertViewHas(['FOLDER_STATUS' => NULL, 'LANG', 'FILEMANAGER_MODE'   => 'images',]);
        }
    }

    /**
     *  test /cms/filemanager Not Authorized
     *
     * @return void
     */
    public function testRouteCMSFilemanagerNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_MEMBER;

         $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/filemanager');
         $response->assertStatus(200);
         $response->assertViewIs('backend.be_login');
    }

    /**
     * API Routes
     */

    /**
     *  test api/v1/files/read/ Authorized
     *
     * @return void
     */
    public function testRouteAPIFilesReadAuthorized(){
        $user     = new User(['id' => 1111]);
        $file = FileItem::where('type', '=', 'downloads')->first();
        $fileResult = $file->toArray();
        $fileResult['fileInfo'] = $file->info()->getResults()->toArray();
        foreach (BeAccessApprover::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET','api/v1/files/read/'. $file->id);
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, trans('ws_file_controller.file.found'), $fileResult));
        }

    }

    /**
     *  test /api/v1/files/read/{id?} Not Authorized for Newsletter Editor and Approver
     *
     * @return void
     */
    public function testRouteAPIFilesReadNewsletterNotAuthorized()
    {
        $user = new User(['id' => 1111]);
        $file = FileItem::where('type', '=', 'downloads')->first();
        foreach ([USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                ->json('GET', '/api/v1/files/read/' . $file->id );

            $response->assertStatus(404);
            $response->assertJson(RestController::generateJsonResponse(true, 'You don\'t have access to read this file!'));
        }
    }

    /**
     *  test api/v1/files/discription/{id?} Authorized
     *
     * @return void
     */
    public function testRouteAPIFilesDiscriptionAuthorized(){
        $user     = new User(['id' => 1111]);
        $file = FileItem::where('type', '=', 'downloads')->first();
        $fileItem = FileItem::join('fileinfo', 'file.id', '=', 'fileinfo.file_id')
            ->where('file.id', $file->id)
            ->where('fileinfo.lang', 1)
            ->select('file.filename', 'fileinfo.description')
            ->first();
        foreach (BeAccessApprover::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET','api/v1/files/discription/'. $file->id);
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, trans('ws_file_controller.file.found'), $fileItem));
        }

    }

    /**
     *  test api/v1/files/discription/{id?} Not AuthorizedNot Authorized for Newsletter Editor and Approver
     *
     * @return void
     */
    public function testRouteAPIFilesDiscriptionNewsletterNotAuthorized(){
        $user = new User(['id' => 1111]);
        $file = FileItem::where('type', '=', 'downloads')->first();
        foreach ([USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                ->json('GET', '/api/v1/files/discription/' . $file->id );

            $response->assertStatus(404);
            $response->assertJson(RestController::generateJsonResponse(true, 'You don\'t have access to read this file!'));
        }
    }

     /**
     *  test /api/v1/files/all/downloads Authorized
     *
     * @return void
     */
     public function testRouteAPIFilesAllAuthorized(){
         $user = new User(['id' => 1111]);
        foreach (BeAccessApprover::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET','api/v1/files/all/downloads');
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'List of files found.'));
        }
     }
     /**
     *  test /api/v1/files/all/downloads Not Authorized for Newsletter Editor and Approver
     *
     * @return void
     */
    public function testRouteAPIFilesAllNewsletterNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach ([USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                ->json('GET', '/api/v1/files/all/downloads');

            $response->assertStatus(404);
            $response->assertJson(RestController::generateJsonResponse(true, 'You don\'t have access to this folder!'));
        }
    }

    /**
     *  test /api/v1/files/subfolders?path=%2F&mode=downloads&time=664&isDeleted=false&isFullTree=false Authorized
     *
     * @return void
     */
    public function testRouteAPIFilesSubfoldersAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach (BeAccessApprover::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                ->json('GET', '/api/v1/files/subfolders?path=%2F&mode=downloads');

            $response->assertStatus(200);
            $response->assertJson([ 'files' => [], 'folder_info' => [] ]);
        }
    }

    /**
     *  test /api/v1/files/subfolders?path=%2F&mode=downloads Not Authorized for Newsletter Editor and Approver
     *
     * @return void
     */
    public function testRouteAPIFilesSubfoldersNewsletterNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach ([USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                ->json('GET', '/api/v1/files/subfolders?path=%2F&mode=downloads');

            $response->assertStatus(404);
            $response->assertJson(RestController::generateJsonResponse(true, 'You don\'t have access to this folder!'));
        }
    }

    /**
     *  test /api/v1/files/isavailable/{file_id} File Isavailable Authorized
     *
     * @return void
     */
    public function testRouteAPIFilesIsAvailableAuthorized() {
        $user     = new User(['id' => 1111]);
        $file = FileItem::where('type', '=', 'downloads')->first();
        foreach (BeAccessApprover::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET','api/v1/files/isavailable/'. $file->id);
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'You can edit this file'));
        }

    }

    /**
     *  test /api/v1/files/isavailable/{file_id} File Isavailable Not Authorized for Member
     *
     * @return void
     */
    public function testRouteAPIFilesIsAvailableMemberNotAuthorized() {
        $user     = new User(['id' => 1111]);
        $file = FileItem::where('type', '=', 'downloads')->first();
        $user->role = USER_ROLE_MEMBER;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET','api/v1/files/isavailable/'. $file->id);
        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/files/createfile File Create Authorized
     *
     * @return void
     */
    public function testRouteAPIFilesCreateAuthorized() {
        $user = new User(['id' => 1111]);
        foreach (BeAccessApprover::getRoles() as $role) {
            $user->role = $role;

            DB::beginTransaction();
            $new_file = ['mode' => 'downloads', 'file_title' => 'test_create_file_downloads_newsletter',
                'currentpath' => '/',
                'file' => self::createUploadedFile(app_path('../tests/test_files/test_download_file.pdf')),];
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                ->json('POST', '/api/v1/files/createfile', $new_file);

            $response->assertStatus(201);
            $response->assertJson(RestController::generateJsonResponse(false, "File was added."));
            DB::rollback();
        }
    }

    /**
     *  test /api/v1/files/createfile File Create Not Authorized for Newsletter Editor and Approver
     *
     * @return void
     */
    public function testRouteAPIFilesCreateNewsletterNotAuthorized(){
        $user = new User(['id' => 1111]);
        foreach ([USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR] as $role) {
            $user->role = $role;

            $new_file = ['mode' => 'downloads', 'file_title' => 'test_create_file_downloads_newsletter',
                'currentpath' => '/',
                'file' => self::createUploadedFile(app_path('../tests/test_files/test_download_file.pdf'))];
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                ->json('POST', '/api/v1/files/createfile', $new_file);

            $response->assertStatus(400);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_file_controller.filemanager.wrongMode')));
        }
    }

     /**
     *  test /api/v1/files/pdfupload/ File pdfupload Authorized
     *
     * @return void
     */

     public function testRouteAPIFilesPDFUploadAuthorized() {
        $page = Page::alive()->where('parent_id', 1)->where('is_visible', 1)->first();
        $user = new User(['id' => 1111]);
        foreach (BeAccessCms::getRoles() as $role) {
            $user->role = $role;

            $new_file = ['u_id' => $page->u_id,
                'file' => self::createUploadedFile(app_path('../tests/test_files/test_download_file.pdf')),];

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                    ->withHeader('HTTP_X-Requested-With' , 'XMLHttpRequest')
                    ->json('POST', '/api/v1/files/pdfupload', $new_file);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(
                false,
                "PDF File was saved to: " . trans('template.currLocale') . '/' . $page->slug . '.pdf')
            );
        }
     }
     /**
     *  test /api/v1/files/pdfupload/ File pdfupload Member Not Authorized
     *
     * @return void
     */

     public function testRouteAPIFilesPDFUploadMemberNotAuthorized() {
        $page = Page::alive()->where('parent_id', 1)->where('is_visible', 1)->where('slug', 'daimler')->first();
        $user = new User(['id' => 1111]);
        $user->role = USER_ROLE_MEMBER;

        $new_file = ['u_id' => $page->u_id,
                'file' => self::createUploadedFile(app_path('../tests/test_files/test_download_file.pdf')),];
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                ->json('POST', '/api/v1/files/pdfupload', $new_file);

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
     }

     /**
     *  test /api/v1/files/updatefile/{$id} File Update Authorized
     *
     * @return void
     */
     public function testRouteAPIFilesUpdateAuthorized() {
        $user = new User(['id' => 1111]);
        $file = FileItem::where('type', '=', 'downloads')->where('protected' , 0)->first();
       foreach (BeAccessApprover::getRoles() as $role) {
            $user->role = $role;

            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                ->json('POST', '/api/v1/files/updatefile/' . $file->id ,
                        ['file_title'=> $file->filename,'file_thumb_title' => $file->thumbnail, 'file_zoom_title' =>  $file->zoompic ,
                            'description' => [1 => '', 2 => '']]);

            $response->assertStatus(201);
            $response->assertJson(RestController::generateJsonResponse(false, 'File was updated.'));
            DB::rollback();
        }
     }

    /**
     *  test /api/v1/files/updatefile/{$id} File Update Not Authorized for Newsletter Editor and Approver
     *
     * @return void
     */
    public function testRouteAPIFilesUpdateNewsletterNotAuthorized(){
        $user = new User(['id' => 1111]);
        $file = FileItem::where('type', '=', 'downloads')->first();
        foreach ([USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR] as $role) {
            $user->role = $role;

            $file_data = ['title' => [\LANG_EN => '', \LANG_DE => '']];
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                ->json('POST', '/api/v1/files/updatefile/' . $file->id, $file_data );
            
            $response->assertStatus(400);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_file_controller.filemanager.wrongMode')));
        }
    }
     /**
     *  test /api/v1/files/makedir/{filemanager_mode} Create dir Authorized
     *
     * @return void
     */
     public function testRouteAPIFilesCreateDirAuthorized() {
         $user = new User(['id' => 1111]);
        foreach (BeAccessApprover::getRoles() as $role) {
             $user->role = $role;

             $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                 ->json('POST', '/api/v1/files/makedir/downloads',
                         ['dirname'=> 'Daimler','path' => '/']);

             $response->assertStatus(400);
             $response->assertJson(RestController::generateJsonResponse(true,
                trans('ws_file_controller.dir.exist'),
                ['dirname' => 'Daimler']));
         }
     }
     /**
     *  test /api/v1/files/makedir/{filemanager_mode} Create dir Not Authorized for Newsletter Editor and Approver
     *
     * @return void
     */
     public function testRouteAPIFilesCreateDirNewsletterNotAuthorized() {
         $user = new User(['id' => 1111]);
        foreach ([USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR] as $role) {
             $user->role = $role;

             $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                 ->json('POST', '/api/v1/files/makedir/downloads',
                         ['dirname'=> 'Daimler','path' => '/']);

             $response->assertStatus(404);
             $response->assertJson(RestController::generateJsonResponse(true, 'You don\'t have access to create this folder!'));
         }
    }
     /**
     *  test /api/v1/files/appendtime/ Appendtime Authorized
     *
     * @return void
     */
     public function testRouteAPIFilesAppendtimeAuthorized() {
        $user = new User(['id' => 1111]);
        $file = FileItem::where('type', '=', 'downloads')->first();
        foreach (BeAccessApprover::getRoles() as $role) {
             $user->role = $role;

             DB::beginTransaction();
             $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                 ->json('POST', '/api/v1/files/appendtime',
                         ['fileId'=> $file->id]);

             $response->assertStatus(200);
             $response->assertJson(RestController::generateJsonResponse(false, 'You can edit this file'));
             DB::rollback();

         }
     }

          /**
     *  test /api/v1/files/appendtime/ Appendtime Member Not Authorized
     *
     * @return void
     */
     public function testRouteAPIFilesAppendtimeMemberNotuthorized() {
        $user = new User(['id' => 1111]);
        $file = FileItem::where('type', '=', 'downloads')->first();
        $user->role = USER_ROLE_MEMBER;

        DB::beginTransaction();
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                 ->json('POST', '/api/v1/files/appendtime',
                         ['fileId'=> $file->id]);

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        DB::rollback();
     }

     /**
     *  test /api/v1/files/unlock/ Unlock Authorized
     *
     * @return void
     */
     public function testRouteAPIFilesUnlockAuthorized() {
        $user = new User(['id' => 1111]);
        $file = FileItem::where('type', '=', 'downloads')->first();
        foreach (BeAccessApprover::getRoles() as $role) {
             $user->role = $role;

             DB::beginTransaction();
             $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                 ->json('POST', '/api/v1/files/unlock',
                         ['fileId'=> $file->id]);

             $response->assertStatus(200);
             $response->assertJson(RestController::generateJsonResponse(false, 'File is unlocked', $file));
             DB::rollback();

         }
     }

     /**
     *  test /api/v1/files/unlock/ Unlock Member Not Authorized
     *
     * @return void
     */
     public function testRouteAPIFilesUnlockMemberNotuthorized() {
        $user = new User(['id' => 1111]);
        $file = FileItem::where('type', '=', 'downloads')->first();
        $user->role = USER_ROLE_MEMBER;

        DB::beginTransaction();
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                 ->json('POST', '/api/v1/files/unlock',
                         ['fileId'=> $file->id]);

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        DB::rollback();
     }

     /**
     *  test /api/v1/files/deletefile/ID Delete File Authorized
     *
     * @return void
     */
     public function testRouteAPIFilesDeletefileAuthorized() {
        $user = new User(['id' => 1111]);
        $file = FileItem::where('file.type', '=', 'downloads')
                ->join('download', 'download.file_id', '=', 'file.id')
                ->join('page', 'page.u_id', '=', 'page_u_id')
                ->where('page.deleted', '=', 0)
                ->select('file.*')->first();
        $pages = Download::where('file_id', '=', $file->id)->join('page', 'page.u_id', '=', 'page_u_id')->where(
                'page.deleted',
                '=',
                0
            )->select('page.*')->get();
        $count = $pages->count();
        $message = trans('ws_file_controller.file.inUse');
        if ($count > 0) {
            $pageVersion = -1;
            for ($i = 0; $i < $count; ++$i) {
                $page = $pages[$i];
                $pageVersion = $page->version;
                $message .= 'page slug:' . $page->slug . ', ID:' . $page->u_id . ', version:' . $page->version . ($i < $count - 1 ? '</br>' : '.');
            }
        }
        foreach (BeAccessApprover::getRoles() as $role) {
             $user->role = $role;
             $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                 ->json('DELETE', '/api/v1/files/deletefile/'.$file->id);

             $response->assertStatus(400);
             $response->assertJson(RestController::generateJsonResponse(true,$message,$file));
         }
     }

     /**
     *  test /api/v1/files/deletefile/ID Delete File Not Authorized for Newsletter Editor and Approver
     *
     * @return void
     */
     public function testRouteAPIFilesDeletefileNewsletterNotAuthorized() {
        $user = new User(['id' => 1111]);
        $file = FileItem::where('type', '=', 'downloads')->first();
        foreach ([USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR]  as $role) {
             $user->role = $role;
             $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                 ->json('DELETE', '/api/v1/files/deletefile/'.$file->id);

             $response->assertStatus(404);
             $response->assertJson(RestController::generateJsonResponse(true,'You don\'t have access to this folder!'));
         }
     }

      /**
     *  test /api/v1/files/deletedir/ Deletedir Authorized
     *
     * @return void
     */
     public function testRouteAPIFilesDeletedirAuthorized() {
        $user = new User(['id' => 1111]);
        foreach (BeAccessApprover::getRoles() as $role) {
             $user->role = $role;

             $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                 ->json('DELETE', '/api/v1/files/deletedir/downloads',
                         ['dirname'=> 'Daimler']);

             $response->assertStatus(404);
             $response->assertJson(RestController::generateJsonResponse(true,
                "You aren't allow to delete Brand/Company folder: Daimler !"));
         }
     }

     /**
     *  test /api/v1/files/deletedir/ Deletedir Not Authorized for Newsletter Editor and Approver
     *
     * @return void
     */
     public function testRouteAPIFilesDeletedirNewsletterNotAuthorized() {
        $user = new User(['id' => 1111]);
        foreach ([USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR]  as $role) {
             $user->role = $role;

             $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                 ->json('DELETE', '/api/v1/files/deletedir/downloads',
                         ['dirname'=> 'Daimler']);

             $response->assertStatus(404);
             $response->assertJson(RestController::generateJsonResponse(true,'You don\'t have access to delete this folder!'));
         }
     }

     /**
     *  test /api/v1/files/upload File Create Not Authorized for Newsletter Editor and Approver
     *
     * @return void
     */
    public function testRouteAPIFilesUploadNewsletterNotAuthorized(){
        $user = new User(['id' => 1111]);
        foreach ([USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR] as $role) {
            $user->role = $role;

            $new_file = ['mode' => 'downloads',
                'currentpath' => '/',
                'file' => self::createUploadedFile(app_path('../tests/test_files/test_download_file.pdf'))];
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                        ->withHeader('HTTP_X-Requested-With' , 'XMLHttpRequest')
                        ->json('POST', '/api/v1/files/upload', $new_file);

            $response->assertStatus(400);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_file_controller.filemanager.wrongMode')));
        }
    }

     /**
     *  test /api/v1/files/restore/{id?} File Create Authorized
     *
     * @return void
     */
    public function testRouteAPIFilesRestoreAuthorized() {
        $user = new User(['id' => 1111]);
        foreach (BeAccessApprover::getRoles() as $role) {
            $user->role = $role;
            $full_path = FileController::getFullPath('downloads', '/');
            DB::beginTransaction();
            $new_file = ['mode' => 'downloads', 'file_title' => 'test_create_file_downloads_newsletter',
                'currentpath' => '/',
                'file' => self::createUploadedFile(app_path('../tests/test_files/test_download_file.pdf'), 'test_document.pdf'),];
            $addResponse = $this->actingAs($user)->withSession(['isConfirmed' => true])
                ->json('POST', '/api/v1/files/createfile', $new_file);
            $addResponse->assertStatus(201);

            $addedFileArray=$addResponse->decodeResponseJson()['response'];
            $deleteResponse = $this->actingAs($user)->withSession(['isConfirmed' => true])
                 ->json('DELETE', '/api/v1/files/deletefile/'.$addedFileArray['id']);
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                 ->json('PUT', '/api/v1/files/restore/', ['id'=>$addedFileArray['id']]);
            DB::rollback();
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, trans('ws_file_controller.file.restored')));
            unlink($full_path.'/test_document.pdf');
        }
    }

    /**
     *  test /api/v1/files/restore File Create Not Authorized for Newsletter Editor and Approver
     *
     * @return void
     */
    public function testRouteAPIFilesRestoreNewsletterNotAuthorized(){
        $user = new User(['id' => 1111]);
        $admin = new User(['id' => 1111]);
        $admin->role = USER_ROLE_ADMIN;
        foreach ([USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR] as $role) {
            $user->role = $role;
            $full_path_delete = FileController::getFullPath('downloads', '/', false ,true);
            DB::beginTransaction();
            $new_file = ['mode' => 'downloads', 'file_title' => 'test_create_file_downloads_newsletter',
                'currentpath' => '/',
                'file' => self::createUploadedFile(app_path('../tests/test_files/test_download_file.pdf'), 'test_document.pdf'),];

            $addResponse = $this->actingAs($admin)->withSession(['isConfirmed' => true])
                ->json('POST', '/api/v1/files/createfile', $new_file);
            $addResponse->assertStatus(201);

            $addedFileArray=$addResponse->decodeResponseJson()['response'];
            $deleteResponse = $this->actingAs($admin)->withSession(['isConfirmed' => true])
                 ->json('DELETE', '/api/v1/files/deletefile/'.$addedFileArray['id']);

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                 ->json('PUT', '/api/v1/files/restore/', ['id'=>$addedFileArray['id']]);
            DB::rollback();
            unlink($full_path_delete.'/test_document.pdf');
            $response->assertStatus(404);
            $response->assertJson(RestController::generateJsonResponse(true, 'You don\'t have access to this folder!'));
        }
    }

     /**
     *  test /api/v1/files/permanentdelete/{id?} File Create Authorized
     *
     * @return void
     */
    public function testRouteAPIFilesPermanentDeleteAuthorized() {

        $user = new User(['id' => 1111]);
        foreach (BeAccessApprover::getRoles() as $role) {
            $user->role = $role;
            $full_path = FileController::getFullPath('downloads', '/');
            DB::beginTransaction();
            $new_file = ['mode' => 'downloads', 'file_title' => 'test_create_file_downloads_newsletter',
                'currentpath' => '/',
                'file' => self::createUploadedFile(app_path('../tests/test_files/test_download_file.pdf')),];

            $addResponse = $this->actingAs($user)->withSession(['isConfirmed' => true])
                ->json('POST', '/api/v1/files/createfile', $new_file);
            $addResponse->assertStatus(201);

            $addedFileArray=$addResponse->decodeResponseJson()['response'];
            $deleteResponse = $this->actingAs($user)->withSession(['isConfirmed' => true])
                 ->json('DELETE', '/api/v1/files/deletefile/'.$addedFileArray['id']);
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                 ->json('DELETE', '/api/v1/files/permanentdelete/'.$addedFileArray['id']);
            DB::rollback();
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, trans('ws_file_controller.file.deleted')));
        }
    }

    /**
     *  test /api/v1/files/permanentdelete/{id?} File Create Authorized
     *
     * @return void
     */
    public function testRouteAPIFilesPermanentDeleteNewsletterNotAuthorized() {
        $user = new User(['id' => 1111]);
        foreach ([USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR] as $role) {
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])
                 ->json('DELETE', '/api/v1/files/permanentdelete/22222222222222');
            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }
}

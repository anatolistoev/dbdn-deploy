<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Tests\Feature;

use App\Models\User;
use App\Models\Group;

use App\Http\Controllers\RestController;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;

class BEViewAccessUsersTest extends TestCase{

    use DatabaseTransactions;
    /**
	 * WEB Routes
	 */

    /**
     *  test /cms/users Authorized
     *
     * @return void
     */
    public function testRouteCMSUsersAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = \USER_ROLE_ADMIN;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/users');

        $response->assertStatus(200);
        $response->assertViewIs('backend.be_users');
        $groups = Group::where('id', '>', 1)->get();
        $response->assertViewHas(['ACTIVE' => 'users', 'GROUPS' => $groups, 'LANG']);
    }

    /**
     *  test /cms/users Not Authorized
     *
     * @return void
     */
    public function testRouteCMSUsersNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        // Role USER_ROLE_MEMBER is not in the list because it redirect to login!
        $roles = [USER_ROLE_EDITOR, USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR, USER_ROLE_EXPORTER];
        foreach ($roles as $role) {
            $user->role = $role;
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/users');

            $response->assertStatus(401);
            $response->assertViewIs('backend.be_unauthorized_access');
            $response->assertViewHas(['error' => trans('ws_general_controller.webservice.unauthorized')]);
        }
    }

    /**
	 * API Routes
	 */

    /**
     *  test /api/v1/user/all Authorized
     *
     * @return void
     */
    public function testRouteAPIUsersAllAuthorized()
    {
        $user = new User(['id' => 1111]);
        $user->role = USER_ROLE_ADMIN;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/user/all');

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'List of Users found'));
    }

    /**
     *  test /api/v1/user/all Not Authorized
     *
     * @return void
     */
    public function testRouteAPIUsersAllNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $roles = [USER_ROLE_MEMBER, USER_ROLE_EDITOR, USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR, USER_ROLE_EXPORTER];
        foreach ($roles as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/user/all');

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
     *
     *  test /api/v1/user/read/{id?} Authorized
     *
     * @return void
     */
    public function testRouteAPIUsersReadAuthorized()
    {
        $user = new User(['id' => 1111]);
        $user->role = USER_ROLE_ADMIN;
        $searchedUser = User::where('active', 1)->first();

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/user/read/'.$searchedUser->id);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'User found'));
    }

    /**
     *  test /api/v1/user/read/{id?} Not Authorized
     *
     * @return void
     */
    public function testRouteAPIUsersReadNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $roles = [USER_ROLE_MEMBER, USER_ROLE_EDITOR, USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR, USER_ROLE_EXPORTER];
        $searchedUser = User::where('active', 1)->first();
        foreach ($roles as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/user/read/'.$searchedUser->id);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }
    /**
     *
     *  test /api/v1/user/create Authorized
     *
     * @return void
     */
    public function testRouteAPIUsersCreateAuthorized()
    {
        $user = new User(['id' => 1111]);
        $user->role = USER_ROLE_ADMIN;
        $new_user = array(
            'username'=>"usr_grp_crt_usr_1",'password' => "P1!as123sword",'password_confirmation'=>"P1!as123sword",
            'first_name'=>"First Name",'last_name'=>"Last Name", 'email'=>"somemail@gmail.com","company" => "Daimler", 'position'=>"developer",
            'department'=>"IT",'address'=>"somewhere",'code'=>"ST",'city'=>"Stuttgart",'country'=>"Germany",
            'phone'=>"0889285",'fax'=>"32423434",'active'=>1,'last_login'=>"2014-01-14 16:46",
            'unlock_code'=>"123123",'role'=>1,'newsletter'=>2,'force_pwd_change'=>2, 'group' => []
        );

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/user/create',$new_user);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'User was added.'));

        // Read the new user
        $response2 = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/user/read/'.$response->original['response']['id']);

        $response2->assertStatus(200);
        $response2->assertJson(RestController::generateJsonResponse(false, 'User found'));
    }

    /**
     *  test /api/v1/user/create Not Authorized
     *
     * @return void
     */
    public function testRouteAPIUsersCreateNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $roles = [USER_ROLE_MEMBER, USER_ROLE_EDITOR, USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR, USER_ROLE_EXPORTER];
        $new_user = [];
        foreach ($roles as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/user/create', $new_user);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }
        /**
     *
     *  test /api/v1/user/update/ Authorized
     *
     * @return void
     */
    public function testRouteAPIUsersUpdateAuthorized()
    {
        $user = new User(['id' => 1111]);
        $user->role = USER_ROLE_ADMIN;
        $searchedUser = User::where('active', 1)->first();

        DB::beginTransaction();
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/user/update/',$searchedUser->toArray());

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'User was updated.'));
        DB::rollback();
    }

    /**
     *  test /api/v1/user/update/ Not Authorized
     *
     * @return void
     */
    public function testRouteAPIUsersUpdateNotAuthorized()
    {
        $user  = new User(['id' => 1111]);
        $roles = [USER_ROLE_MEMBER, USER_ROLE_EDITOR, USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR, USER_ROLE_EXPORTER];
        foreach ($roles as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/user/update/',[]);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
     *
     *  test /api/v1/user/delete/{id?} Authorized
     *
     * @return void
     */
    public function testRouteAPIUsersDeleteAuthorized()
    {
        $user = new User(['id' => 1111]);
        $user->role = USER_ROLE_ADMIN;
        $searchedUser = User::where('active', 1)->first();

        DB::beginTransaction();
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('DELETE', '/api/v1/user/delete/'.$searchedUser->id);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'User was deleted!'));
        DB::rollback();
    }

    /**
     *  test /api/v1/user/delete/{id?} Not Authorized
     *
     * @return void
     */
    public function testRouteAPIUsersDeleteNotAuthorized()
    {
        $user  = new User(['id' => 1111]);
        $roles = [USER_ROLE_MEMBER, USER_ROLE_EDITOR, USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR, USER_ROLE_EXPORTER];
        foreach ($roles as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('DELETE', '/api/v1/user/delete/444444444444444444');

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }
}

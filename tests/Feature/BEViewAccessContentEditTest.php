<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Content;
use App\Models\Page;

use App\Http\Controllers\RestController;
use App\Http\Middleware\BeAccessApprover;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;

class BEViewAccessContentEditTest extends TestCase{

   use DatabaseTransactions;

   private static $smartAuthorizedEditor = [USER_ROLE_ADMIN, USER_ROLE_EDITOR, USER_ROLE_EDITOR_SMART];
   private static $smartNotAuthorizedEditor = [USER_ROLE_EDITOR_MB, USER_ROLE_EDITOR_DFS, USER_ROLE_EDITOR_DFM,
        USER_ROLE_EDITOR_DTF, USER_ROLE_EDITOR_FF, USER_ROLE_EDITOR_DP, USER_ROLE_EDITOR_TSS, USER_ROLE_EDITOR_BKK,
        USER_ROLE_EDITOR_DB, USER_ROLE_EDITOR_EB, USER_ROLE_EDITOR_SETRA, USER_ROLE_EDITOR_OP, USER_ROLE_EDITOR_BS, USER_ROLE_EDITOR_FUSO,
        USER_ROLE_EDITOR_DT];


   /**
    *  test /api/v1/contents/create Authorized State Draft
    *
    * @return void
    */
    public function testRouteAPIContentsCreateAuthorizedStateDraft()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $smartPage = Page::join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->first();
        $smartPage->workflow->editing_state = \STATE_DRAFT;
        $smartPage->workflow->save();

        $postData = ["body"=> "<p>Test Author</p>",
                    "format" => 1,
                    "lang"=> 2,
                    "page_id" => $smartPage->id,
                    "page_u_id" => $smartPage->u_id,
                    "position" => 0,
                    "searchable" => 1,
                    "section" => 'author'];
        foreach (self::$smartAuthorizedEditor as $role) {
            $user->role = $role;

            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/contents/create', $postData);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, "Content was added.", [$postData]));
            DB::rollback();
        }
    }

   /**
    *  test /api/v1/contents/create Not Authorized State Draft
    *
    * @return void
    */
    public function testRouteAPIContentsCreateNotAuthorizedStateDraft()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $smartPage = Page::join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->first();
        $smartPage->workflow->editing_state = \STATE_DRAFT;
        $smartPage->workflow->save();

        $postData = ["body"=> "<p>Test Author</p>",
                    "format" => 1,
                    "lang"=> 2,
                    "page_id" => $smartPage->id,
                    "page_u_id" => $smartPage->u_id,
                    "position" => 0,
                    "searchable" => 1,
                    "section" => 'author'];
        foreach (self::$smartNotAuthorizedEditor as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/contents/create', $postData);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

   /**
    *  test /api/v1/contents/create Not Authorized State Approved
    *
    * @return void
    */
    public function testRouteAPIContentsCreateNotAuthorizedStateApproved()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $smartPage = Page::join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->first();
        $smartPage->workflow->editing_state = \STATE_APPROVED;
        $smartPage->workflow->save();

        $postData = ["body"=> "<p>Test Author</p>",
                    "format" => 1,
                    "lang"=> 2,
                    "page_id" => $smartPage->id,
                    "page_u_id" => $smartPage->u_id,
                    "position" => 0,
                    "searchable" => 1,
                    "section" => 'author'];
        foreach (self::$smartAuthorizedEditor as $role) {
            $user->role = $role;

            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/contents/create', $postData);

            $response->assertStatus(403);
            $responsible_name = "";
            if ($smartPage->workflow->user_responsible > 0) {
                $user = User::find($smartPage->workflow->user_responsible);
                if (!is_null($user)) {
                    $responsible_name = trim($user->first_name) . ' ' . trim($user->last_name);
                }
            }            
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.page.state_error', ['name' => $responsible_name])));
            DB::rollback();
        }
    }

   /**
    *  test /api/v1/contents/create Not Authorized State Review
    *
    * @return void
    */
    public function testRouteAPIContentsCreateNotAuthorizedStateReview()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        // Setup page
        $smartPage = Page::join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->first();
        $smartPage->workflow->editing_state = \STATE_REVIEW;
        $smartPage->workflow->save();

        $postData = ["body"=> "<p>Test Author</p>",
                    "format" => 1,
                    "lang"=> 2,
                    "page_id" => $smartPage->id,
                    "page_u_id" => $smartPage->u_id,
                    "position" => 0,
                    "searchable" => 1,
                    "section" => 'author'];
        
        foreach ([\USER_ROLE_EDITOR_SMART] as $role) {
            $user->role = $role;

            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/contents/create', $postData);

            $response->assertStatus(403);
            $responsible_name = "";
            if ($smartPage->workflow->user_responsible > 0) {
                $user = User::find($smartPage->workflow->user_responsible);
                if (!is_null($user)) {
                    $responsible_name = trim($user->first_name) . ' ' . trim($user->last_name);
                }
            }
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.page.state_error', ['name' => $responsible_name])));
            DB::rollback();
        }
    }

   /**
    *  test /api/v1/contents/related Authorized
    *
    * @return void
    */
    public function testRouteAPIContentsRelatedAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $smartPage = Page::join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->first();
        $postData = [ 0 =>['position'=> "0", 'related_id'=> "2655", "page_u_id" => $smartPage->u_id],
                      1 =>['position'=> "1", 'related_id'=> "995", "page_u_id" => $smartPage->u_id]];
        foreach (self::$smartAuthorizedEditor as $role) {
            $user->role = $role;

            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/contents/related', $postData);

            $response->assertStatus(200);
            $response->assertJson($postData);
            DB::rollback();
        }
    }

   /**
    *  test /api/v1/contents/related Not Authorized
    *
    * @return void
    */
    public function testRouteAPIContentsRelatedNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $smartPage = Page::join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->first();
        $postData = [ 0 =>['position'=> "0", 'related_id'=> "2655", "page_u_id" => $smartPage->u_id],
                      1 =>['position'=> "1", 'related_id'=> "995", "page_u_id" => $smartPage->u_id]];
        foreach (self::$smartNotAuthorizedEditor as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/contents/related', $postData);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
    *  test /api/v1/contents/reset-related Authorized
    *
    * @return void
   */
    public function testRouteAPIContentsResetRelatedAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $smartPage = Page::join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->first();
        $postData = ["lang"=> "2", "page_u_id" => $smartPage->u_id];
        foreach (self::$smartAuthorizedEditor as $role) {
            $user->role = $role;
            // Clean user and session
            Auth::logout();
            $this->flushSession();
            // Start transaction!
            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/contents/reset-related', $postData);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'Reset related pages.'));
            //TODO: Check the content!
            // ['content' => ?]
            // Rollback and then return errors
            DB::rollback();
        }
    }

   /**
    *  test /api/v1/contents/reset-related Not Authorized
    *
    * @return void
   */
    public function testRouteAPIContentsResetRelatedNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $smartPage = Page::join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->first();
        $postData = ["lang"=> "2", "page_u_id" => $smartPage->u_id];

        foreach (self::$smartNotAuthorizedEditor as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/contents/reset-related', $postData);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
    *  test /api/v1/contents/create-many Authorized
    *
    * @return void
   */
    public function testRouteAPIContentsCreateManyAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $smartPage = Page::join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->first();
        
        $smartPage->workflow->editing_state = \STATE_DRAFT;
        $smartPage->workflow->save();
                
        $postData = [0 => ["body"=> "<p>Test Author</p>",
                    "format" => 1,
                    "lang"=> 2,
                    "page_id" => $smartPage->id,
                    "page_u_id" => $smartPage->u_id,
                    "position" => 0,
                    "searchable" => 1,
                    "section" => 'author'],
                    1 => ["body"=> "<p>Test Author</p>",
                    "format" => 1,
                    "lang"=> 1,
                    "page_id" => $smartPage->id,
                    "page_u_id" => $smartPage->u_id,
                    "position" => 0,
                    "searchable" => 1,
                    "section" => 'author']
            ];
        foreach (self::$smartAuthorizedEditor as $role) {
            $user->role = $role;

            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/contents/create-many', $postData);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, "Content was added.", $postData));
            DB::rollback();
        }
    }

   /**
    *  test /api/v1/contents/create-many Not Authorized
    *
    * @return void
   */
    public function testRouteAPIContentsCreateManyNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $smartPage = Page::join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->first();
        $postData = [0 => ["body"=> "<p>Test Author</p>",
                    "format" => 1,
                    "lang"=> 2,
                    "page_id" => $smartPage->id,
                    "page_u_id" => $smartPage->u_id,
                    "position" => 0,
                    "searchable" => 1,
                    "section" => 'author'],
                    1 => ["body"=> "<p>Test Author</p>",
                    "format" => 1,
                    "lang"=> 1,
                    "page_id" => $smartPage->id,
                    "page_u_id" => $smartPage->u_id,
                    "position" => 0,
                    "searchable" => 1,
                    "section" => 'author']
            ];
        foreach (self::$smartNotAuthorizedEditor as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/contents/create-many', $postData);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
    *  test /api/v1/contents/update Authorized
    *
    * @return void
   */
    public function testRouteAPIContentsUpdateAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $pageContent = Content::select("content.*")
                ->from(DB::raw("(Select page_u_id from `content` where section = 'title' AND `deleted_at` IS NULL group by page_u_id having count(page_u_id) = 2) as ct"))
                ->join('page', 'ct.page_u_id', '=', 'page.u_id')
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->join('content', 'content.page_u_id', '=', 'page.u_id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->whereNull('content.deleted_at')
                ->where('content.section', '=', 'title')
                ->whereNull('page.deleted_at')
                ->orderBy('content.page_u_id')
                ->limit(1)
                ->first();

        $srcPage = $pageContent->page;
        $srcPage->workflow->editing_state = \STATE_DRAFT;
        $srcPage->workflow->save();

        $postData = ['body' => $pageContent->body, 'id' => $pageContent->id];

        foreach (self::$smartAuthorizedEditor as $role) {
            $user->role = $role;

            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/contents/update', $postData);

            $response->assertStatus(200);

            $response->assertJson(RestController::generateJsonResponse(false, "Content was updated.", [$postData]));
            DB::rollback();

        }
    }

   /**
    *  test /api/v1/contents/update Not Authorized
    *
    * @return void
    */
    public function testRouteAPIContentsUpdateNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $smartPage = Page::
                join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->join('content', 'content.page_u_id', '=', 'page.u_id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->whereNull('content.deleted_at')
                ->where('content.section', '=', 'title')
                ->select('content.*')
                ->first();

        $postData = ['body' => $smartPage->body, 'id' => $smartPage->id];

        foreach (self::$smartNotAuthorizedEditor as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/contents/update', $postData);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

   /**
    *  test /api/v1/contents/update-many Authorized State Draft
    *
    * @return void
    */
    public function testRouteAPIContentsUpdateManyAuthorizedStateDraft()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        // Setup page
        $pageContent = Content::select("content.*")
                ->from(DB::raw("(Select page_u_id from `content` where section = 'title' AND `deleted_at` IS NULL group by page_u_id having count(page_u_id) = 2) as ct"))
                ->join('page', 'ct.page_u_id', '=', 'page.u_id')
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->join('content', 'content.page_u_id', '=', 'page.u_id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->whereNull('content.deleted_at')
                ->where('content.section', '=', 'title')
                ->whereNull('page.deleted_at')
                ->orderBy('content.page_u_id')
                ->limit(2)
                ->get();
        
        $pageContent[0]->page->workflow->editing_state = STATE_DRAFT;
        $pageContent[0]->page->workflow->save();
        $pageContent[1]->page->workflow->editing_state = STATE_DRAFT;
        $pageContent[1]->page->workflow->save();

        $postData = [ 0 => ['body' => $pageContent[0]->body, 'id' => $pageContent[0]->id],
                      1 => ['body' => $pageContent[1]->body, 'id' => $pageContent[1]->id]];

        foreach (self::$smartAuthorizedEditor as $role) {
            $user->role = $role;

            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/contents/update-many', $postData);

            $response->assertStatus(200);

            $response->assertJson(RestController::generateJsonResponse(false, "Content was updated.", $postData));
            DB::rollback();

        }
    }

   /**
    *  test /api/v1/contents/update-many Not Authorized State Draft
    *
    * @return void
    */
    public function testRouteAPIContentsUpdateManyNotAuthorizedStateDraft()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $pageContent = Content::select("content.*")
                ->from(DB::raw("(Select page_u_id from `content` where section = 'title' AND `deleted_at` IS NULL group by page_u_id having count(page_u_id) = 2) as ct"))
                ->join('page', 'ct.page_u_id', '=', 'page.u_id')
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->join('content', 'content.page_u_id', '=', 'page.u_id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->whereNull('content.deleted_at')
                ->where('content.section', '=', 'title')
                ->whereNull('page.deleted_at')
                ->orderBy('content.page_u_id')
                ->limit(2)
                ->get();

        $pageContent[0]->page->workflow->editing_state = STATE_DRAFT;
        $pageContent[0]->page->workflow->save();
        $pageContent[1]->page->workflow->editing_state = STATE_DRAFT;
        $pageContent[1]->page->workflow->save();
        
        $postData = [ 0 => ['body' => $pageContent[0]->body, 'id' => $pageContent[0]->id],
                      1 => ['body' => $pageContent[1]->body, 'id' => $pageContent[1]->id]];

        foreach (self::$smartNotAuthorizedEditor as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/contents/update-many', $postData);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

   /**
    *  test /api/v1/contents/update-many Authorized State Review
    *
    * @return void
    */
    public function testRouteAPIContentsUpdateManyAuthorizedStateReview()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        // Setup page
        $pageContent = Content::select("content.*")
                ->from(DB::raw("(Select page_u_id from `content` where section = 'title' AND `deleted_at` IS NULL group by page_u_id having count(page_u_id) = 2) as ct"))
                ->join('page', 'ct.page_u_id', '=', 'page.u_id')
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->join('content', 'content.page_u_id', '=', 'page.u_id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->whereNull('content.deleted_at')
                ->where('content.section', '=', 'title')
                ->whereNull('page.deleted_at')
                ->orderBy('content.page_u_id')
                ->limit(2)
                ->get();
        
        $pageContent[0]->page->workflow->editing_state = \STATE_REVIEW;
        $pageContent[0]->page->workflow->save();
        $pageContent[1]->page->workflow->editing_state = \STATE_REVIEW;
        $pageContent[1]->page->workflow->save();

        $postData = [ 0 => ['body' => $pageContent[0]->body, 'id' => $pageContent[0]->id],
                      1 => ['body' => $pageContent[1]->body, 'id' => $pageContent[1]->id]];
        
        foreach (BeAccessApprover::getRoles() as $role) {
            $user->role = $role;

            DB::beginTransaction();
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/contents/update-many', $postData);

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, "Content was updated.", $postData));
            DB::rollback();
        }
    }

   /**
    *  test /api/v1/contents/update-many Not Authorized State Review
    *
    * @return void
    */
    public function testRouteAPIContentsUpdateManyNotAuthorizedStateReview()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $pageContent = Content::select("content.*")
                ->from(DB::raw("(Select page_u_id from `content` where section = 'title' AND `deleted_at` IS NULL group by page_u_id having count(page_u_id) = 2) as ct"))
                ->join('page', 'ct.page_u_id', '=', 'page.u_id')
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->join('content', 'content.page_u_id', '=', 'page.u_id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->whereNull('content.deleted_at')
                ->where('content.section', '=', 'title')
                ->whereNull('page.deleted_at')
                ->orderBy('content.page_u_id')
                ->limit(2)
                ->get();

        $pageContent[0]->page->workflow->editing_state = \STATE_REVIEW;
        $pageContent[0]->page->workflow->save();
        $pageContent[1]->page->workflow->editing_state = \STATE_REVIEW;
        $pageContent[1]->page->workflow->save();
        
        $postData = [ 0 => ['body' => $pageContent[0]->body, 'id' => $pageContent[0]->id],
                      1 => ['body' => $pageContent[1]->body, 'id' => $pageContent[1]->id]];

        $user->role = \USER_ROLE_EDITOR_SMART;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/contents/update-many', $postData);

        $response->assertStatus(403);
        $responsible_name = "";
        if ($pageContent[0]->page->workflow->user_responsible > 0) {
            $user = User::find($pageContent[0]->page->workflow->user_responsible);
            if (!is_null($user)) {
                $responsible_name = trim($user->first_name) . ' ' . trim($user->last_name);
            }
        }            
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.page.state_error', ['name' => $responsible_name])));
    }

   /**
    *  test /api/v1/contents/update-many Not Authorized State Approved
    *
    * @return void
    */
    public function testRouteAPIContentsUpdateManyNotAuthorizedStateApproved()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $pageContent = Content::select("content.*")
                ->from(DB::raw("(Select page_u_id from `content` where section = 'title' AND `deleted_at` IS NULL group by page_u_id having count(page_u_id) = 2) as ct"))
                ->join('page', 'ct.page_u_id', '=', 'page.u_id')
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->join('content', 'content.page_u_id', '=', 'page.u_id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->whereNull('content.deleted_at')
                ->where('content.section', '=', 'title')
                ->whereNull('page.deleted_at')
                ->orderBy('content.page_u_id')
                ->limit(2)
                ->get();

        $pageContent[0]->page->workflow->editing_state = \STATE_APPROVED;
        $pageContent[0]->page->workflow->save();
        $pageContent[1]->page->workflow->editing_state = \STATE_APPROVED;
        $pageContent[1]->page->workflow->save();
        
        $postData = [ 0 => ['body' => $pageContent[0]->body, 'id' => $pageContent[0]->id],
                      1 => ['body' => $pageContent[1]->body, 'id' => $pageContent[1]->id]];

        foreach (BeAccessApprover::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/contents/update-many', $postData);

            $response->assertStatus(403);
            $responsible_name = "";
            if ($pageContent[0]->page->workflow->user_responsible > 0) {
                $user = User::find($pageContent[0]->page->workflow->user_responsible);
                if (!is_null($user)) {
                    $responsible_name = trim($user->first_name) . ' ' . trim($user->last_name);
                }
            }
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.page.state_error', ['name' => $responsible_name])));
        }
    }

}
//0: {id: 35664, body: "Test Page"}
//1: {id: 35665, body: "Test Page"}
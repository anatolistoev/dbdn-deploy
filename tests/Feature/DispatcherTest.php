<?php
namespace Tests\Feature;

use App\Models\Page;
use App\Models\User;
use App\Models\Usergroup;
use Tests\TestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DispatcherTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp(){
        parent::setUp();
    }

    public function tearDown(){
        parent::tearDown();
    }

    /**
     * inspect the variables in Home view
     * @return void
     */
    protected function checkHomeViewVars($response)
    {
        $response->assertViewHas('PAGE');
        $response->assertViewHas('CONTENTS');
        $response->assertViewHas('SECTION_PID_INDEX');
        $response->assertViewHas('L2ID');
        $response->assertViewHas('ALT_LANG');
        $response->assertViewHas('HOMEBLOCKS');
    }

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testRoute()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    /**
     * test /home
     * @return void
     */
    public function testRouteHome()
    {
        $response = $this->get('/home');

        $response->assertStatus(200);
        $this->checkHomeViewVars($response);
    }
    /**
     * Test /{id of Home}
     * @return void
     */
    public function testRouteHomeByID()
    {
        $page = Page::where('slug', '=', 'home')
                    ->alive()
                    ->first();
        assert(!empty($page), 'Page Home should exists!');

        $response = $this->get('/' . $page->u_id);

        $response->assertStatus(200);
        $this->checkHomeViewVars($response);
    }

    /**
     * Test /print/{id of Home} of overview
     * @return void
     */
    public function testRoutePrintHomeByID()
    {
        $page = Page::where('slug', '=', 'home')
                    ->alive()
                    ->first();
        assert(!empty($page), 'Page Home should exists!');

        $response = $this->get('/print/' . $page->u_id);

        $response->assertStatus(404);
        $response->assertViewIs('errors.fatal');
        $response->assertViewHas('PAGE', function($page) {
            return $page->slug == 'fatal';
        });
    }
    /**
     * test by slug /daimler
     * @return void
     */
    public function testRouteDaimler()
    {
        $response = $this->get('/daimler');

        $response->assertStatus(200);
    }
    /**
     * test by page id /{page_u_id}
     * @return void
     */
    public function testRouteDaimlerByID()
    {
        Session::put('lang', \LANG_EN);
        $page = Page::where('slug', '=', 'daimler')
                    ->alive()
                    ->haveLang()
                    ->first();

        assert(!empty($page), 'Page Home should exists!');

        $response = $this->get('/' . $page->u_id);

        $response->assertStatus(200);
    }
    /**
     * test /nonexisting
     * @return void
     */
    public function testRouteNonexisting()
    {
        $response = $this->get('/nosuchroute');

        $response->assertStatus(404);
    }

    /**
     * test /2252 Autorization
     * @return void
     */
    public function testRouteAutorizationRequire()
    {
        // Find user that have access to protected page
        $userPage = Page::alive()
            ->join('pagegroup', 'page.id', '=', 'pagegroup.page_id')
            ->join('usergroup', 'usergroup.group_id', '=', 'pagegroup.group_id')
            ->where('page.is_visible', 1)
            ->whereNull('pagegroup.deleted_at')
            ->whereNull('usergroup.deleted_at')
            ->groupBy('page.u_id')
            ->select('usergroup.user_id', 'page.u_id')
            ->first();

        $response = $this->get('/' . $userPage->u_id);

        $response->assertStatus(302);
        $response->assertSessionHasErrors(['success' => trans('system.login_required.contents')], $format = null, $errorBag = 'default');
    }

    /**
     * test /2252 Autorization
     * @return void
     */
    public function testRouteAutorizationRequire2()
    {
        // Find user that have access to protected page
        $userPage = Page::alive()
            ->join('pagegroup', 'page.id', '=', 'pagegroup.page_id')
            ->join('usergroup', 'usergroup.group_id', '=', 'pagegroup.group_id')
            ->where('page.is_visible', 1)
            ->whereNull('pagegroup.deleted_at')
            ->whereNull('usergroup.deleted_at')
            ->groupBy('page.u_id')
            ->select('usergroup.user_id', 'page.u_id')
            ->first();

        $user     = new User();
        $response = $this->actingAs($user)->get('/' . $userPage->u_id);

        $response->assertStatus(200);
        $response->assertViewIs('no_access');
        $response->assertViewHas(['PAGE', 'OLD_email', 'OLD_text_area']);
    }

    /**
     * test /2252 Autorization
     * @return void
     */
    public function testRouteAutorizationSuccess()
    {
        // Find user that have access to protected page
        $groupFile = Page::alive()
            ->join('pagegroup', 'page.id', '=', 'pagegroup.page_id')
            ->where('page.is_visible', 1)
            ->whereNull('pagegroup.deleted_at')
            ->groupBy('page.u_id')
            ->select('page.u_id' , 'pagegroup.group_id')
            ->first();

        $user = new User(['username' => 'test_0001',
                'email' => "test@gmail.com",
                'password' => "!?TestUser123",
                'password_confirmation' => "!?TestUser123",
                'first_name' => "Test",
                'last_name' => "Test",
                'company' => "Daimler",
                'country' => "Germany",
                'role' => USER_ROLE_MEMBER]);
        $user->save();

        $usergroup_item = new Usergroup();
        $usergroup_item->user_id = $user->id;
        $usergroup_item->group_id = $groupFile->group_id;
        $usergroup_item->save();

        $response = $this->actingAs($user)->get('/' . $groupFile->u_id);

        $response->assertStatus(200);
    }
}

<?php

namespace Tests\Feature;

use App\Models\Download;
use App\Models\FileItem;
use App\Models\Page;
use App\Models\User;
use App\Models\Usergroup;
use App\Helpers\FileHelper;
use App\Http\Controllers\RestController;
use App\Http\Controllers\FileController;
use App\Http\Middleware\BeAccessApprover;
use App\Http\Middleware\BeAccessCms;
use App\Http\Middleware\BeAccessNlOrCms;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class FEAccessProtectedFilesTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp(){
        parent::setUp();
        ob_end_clean();
        ob_start();
    }

    public function tearDown(){
        parent::tearDown();
    }

    /**
     * WEB Routes
     */

    /**
     *  test /download file Authorized Member
     *
     * @return void
     */
    public function testRouteDownloadFileAuthorizedMember()
    {
        // Find file that accessable
        $accessableFile = Page::alive()
            ->join('download', 'page.u_id', '=', 'download.page_u_id')
            ->join('file', 'download.file_id', '=', 'file.id')
            ->where('page.is_visible', 1)
            ->whereNull('download.deleted_at')
            ->whereNull('file.deleted_at')
            ->where('file.protected', 0)
            ->groupBy('download.page_u_id')
            ->select('download.file_id')
            ->first();

        $user       = new User(['id' => 1111]);
        $user->role = USER_ROLE_MEMBER;

        $fileItem = FileItem::find($accessableFile->file_id);
        $sourceFullPath  = app_path('../tests/test_files/zip-icon-upload.zip');
        $destFullPath    = public_path($fileItem->getPath(FILEITEM_REAL_PATH));
        $dirPathFileItem = dirname($destFullPath);
        FileHelper::makedirs($dirPathFileItem, FILE_DEFAULT_ACCESS);
        copy($sourceFullPath, $destFullPath);

        try {
            $data = ['fid' => [$fileItem->id]];
            $response = $this->actingAs($user)->post('/download?lang=en', $data, []);

            $response->assertStatus(200);
        } finally {
            @unlink($destFullPath);
        }
    }

    /**
     *  test /download file Not published page Authorized Member
     *
     * @return void
     */
    public function testRouteDownloadFileNotPublishedPageAuthorizedMember()
    {
        // Find file that accessable
        $accessableFile = Page::alive()
            ->join('download', 'page.u_id', '=', 'download.page_u_id')
            ->join('file', 'download.file_id', '=', 'file.id')
            ->where('page.is_visible', 1)
            ->whereNull('download.deleted_at')
            ->whereNull('file.deleted_at')
            ->where('file.protected', 0)
            ->groupBy('download.page_u_id')
            ->select('download.file_id')
            ->first();

        $user       = new User(['id' => 1111]);
        $user->role = USER_ROLE_MEMBER;

        $fileItem = FileItem::find($accessableFile->file_id);
        // Make all pages unpublised
        $pagesForSelectedFile = Page::alive()
            ->join('download', 'page.u_id', '=', 'download.page_u_id')
            ->whereNull('download.deleted_at')
            ->where('download.file_id', $accessableFile->file_id)
            ->get();
        foreach ($pagesForSelectedFile as $page) {
            $page->unpublish_date = '2013-12-31 00:00:00';
            $page->save();
        }

        try {
            $data = ['fid' => [$fileItem->id]];
            $response = $this->actingAs($user)->post('/download?lang=en', $data, []);

            $response->assertStatus(403);
            $response->assertSeeText('parent.msg("' . trans('system.mydownloads.notfound') . '")');
        } finally {
            //@unlink($destFullPath);
        }
    }

    /**
     *  test /download Protected file Authorized Member
     *
     * @return void
     */
    public function testRouteDownloadFileProtectedAuthorizedMember()
    {
        // Find user that have access to protected file
        $groupFile = Page::alive()
            ->join('download', 'page.u_id', '=', 'download.page_u_id')
            ->join('pagegroup', 'page.id', '=', 'pagegroup.page_id')
            //->join('usergroup', 'usergroup.group_id', '=', 'pagegroup.group_id')
            ->join('file', 'download.file_id', '=', 'file.id')
            ->where('page.is_visible', 1)
            ->whereNull('download.deleted_at')
            ->whereNull('pagegroup.deleted_at')
            //->whereNull('usergroup.deleted_at')
            ->whereNull('file.deleted_at')
            ->where('file.protected', 1)
            ->groupBy('download.page_u_id')
            ->select('pagegroup.group_id', 'download.file_id')
            ->first();

        $user = new User(['username' => 'test_0001',
                'email' => "test@gmail.com",
                'password' => "!?TestUser123",
                'password_confirmation' => "!?TestUser123",
                'first_name' => "Test",
                'last_name' => "Test",
                'company' => "Daimler",
                'country' => "Germany",
                'role' => USER_ROLE_MEMBER]);
        $user->save();

        $usergroup_item = new Usergroup();
        $usergroup_item->user_id = $user->id;
        $usergroup_item->group_id = $groupFile->group_id;
        $usergroup_item->save();


        $fileItem = FileItem::find($groupFile->file_id);
        $sourceFullPath  = app_path('../tests/test_files/zip-icon-upload.zip');
        $destFullPath    = public_path($fileItem->getPath(FILEITEM_REAL_PATH));
        $dirPathFileItem = dirname($destFullPath);
        FileHelper::makedirs($dirPathFileItem, FILE_DEFAULT_ACCESS);
        copy($sourceFullPath, $destFullPath);

        try {
            $data = ['fid' => [$fileItem->id]];
            $response = $this->actingAs($user)->post('/download?lang=en', $data, []);

            $response->assertStatus(200);
        } finally {
            @unlink($destFullPath);
        }
    }

    /**
     *  test /download Protected file Not published page Authorized Member
     *
     * @return void
     */
    public function testRouteDownloadFileProtectedNotPublishedPageAuthorizedMember()
    {
        // Find group that have access to protected file
        $groupFile = Page::alive()
            ->join('download', 'page.u_id', '=', 'download.page_u_id')
            ->join('pagegroup', 'page.id', '=', 'pagegroup.page_id')
            ->join('file', 'download.file_id', '=', 'file.id')
            ->where('page.is_visible', 1)
            ->whereNull('download.deleted_at')
            ->whereNull('pagegroup.deleted_at')
            ->whereNull('file.deleted_at')
            ->where('file.protected', 1)
            ->groupBy('download.page_u_id')
            ->select('pagegroup.group_id', 'download.file_id')
            ->first();

        $user = new User(['username' => 'test_0001',
                'email' => "test@gmail.com",
                'password' => "!?TestUser123",
                'password_confirmation' => "!?TestUser123",
                'first_name' => "Test",
                'last_name' => "Test",
                'company' => "Daimler",
                'country' => "Germany",
                'role' => USER_ROLE_MEMBER]);
        $user->save();

        $usergroup_item = new Usergroup();
        $usergroup_item->user_id = $user->id;
        $usergroup_item->group_id = $groupFile->group_id;
        $usergroup_item->save();

        $fileItem = FileItem::find($groupFile->file_id);

        $pagesForSelectedFile = Page::alive()
            ->join('download', 'page.u_id', '=', 'download.page_u_id')
            ->join('pagegroup', 'page.id', '=', 'pagegroup.page_id')
            ->join('usergroup', 'usergroup.group_id', '=', 'pagegroup.group_id')
            ->whereNull('pagegroup.deleted_at')
            ->whereNull('usergroup.deleted_at')
            ->whereNull('download.deleted_at')
            ->where('usergroup.user_id', '=', $user->id)
            ->where('download.file_id', $groupFile->file_id)
            ->get();
        foreach ($pagesForSelectedFile as $page) {
            $page->unpublish_date = '2013-12-31 00:00:00';
            $page->save();
        }

        try {
            $data = ['fid' => [$fileItem->id]];
            $response = $this->actingAs($user)->post('/download?lang=en', $data, []);

            $response->assertStatus(403);
            $response->assertSeeText('parent.msg("' . trans('system.mydownloads.notfound') . '")');
        } finally {
            //@unlink($destFullPath);
        }
    }

    /**
     *  test /download file Not Authorized
     *
     * @return void
     */
    public function testRouteDownloadFileNotAuthorizedMember()
    {
        $user       = new User(['id' => 1111]);
        $user->role = USER_ROLE_MEMBER;

        $filename = 'test_download_file.pdf';
        $currentpath = '/Daimler/';
        $destinationPath = FileController::getFullPath(FileController::MODE_DOWNLOADS, $currentpath, true);
        $sourceFullPath  = app_path('../tests/test_files/test_download_file.pdf');
        $destFullPath    = $destinationPath . $filename;

        copy($sourceFullPath, $destFullPath);
        try {
            $fileItem = new FileItem();
            $fileItem->protected = 0;
            $fileItem->type = FileController::MODE_DOWNLOADS;
            $fileItem->filename = $currentpath . $filename;
            $fileItem->size = @filesize($destFullPath);
            $fileItem->extension = 'pdf';
            $fileItem->save();

            $data = ['fid' => [$fileItem->id]];
            $response = $this->actingAs($user)->post('/download?lang=en', $data, []);

            $response->assertStatus(403);
            $response->assertSeeText('parent.msg("' . trans('system.mydownloads.notfound') . '")');
        } finally {
            @unlink($destFullPath);
        }
    }

    /**
     *  test /download Protected file Not Authorized
     *
     * @return void
     */
    public function testRouteDownloadFileProtectedNotAuthorizedMember()
    {
        $user       = new User(['id' => 1111]);
        $user->role = USER_ROLE_MEMBER;

        $filename = 'test_download_file.pdf';
        $currentpath = '/Daimler/';
        $destinationPath = FileController::getFullPath(FileController::MODE_DOWNLOADS, $currentpath, true);
        $sourceFullPath  = app_path('../tests/test_files/test_download_file.pdf');
        $destFullPath    = $destinationPath . $filename;

        copy($sourceFullPath, $destFullPath);
        try {
            $fileItem = new FileItem();
            $fileItem->protected = 1;
            $fileItem->type = FileController::MODE_DOWNLOADS;
            $fileItem->filename = $currentpath . $filename;
            $fileItem->size = @filesize($destFullPath);
            $fileItem->extension = 'pdf';
            $fileItem->save();

            $data = ['fid' => [$fileItem->id]];
            $response = $this->actingAs($user)->post('/download?lang=en', $data, []);

            $response->assertStatus(403);
            $response->assertSeeText('parent.msg("' . trans('system.mydownloads.notfound') . '")');
        } finally {
            @unlink($destFullPath);
        }
    }

    /**
     *  test /download Protected file Authorized Editor
     *
     * @return void
     */
    public function testRouteDownloadFileProtectedAuthorizedEditor()
    {
        $user       = new User(['id' => 1111]);
        $user->role = \USER_ROLE_EDITOR_SMART;

        $filename = 'test_download_file.pdf';
        $currentpath = '/'. \App\Helpers\UserAccessHelper::getSpecialPage(SMART)->folder . '/';
        $destinationPath = FileController::getFullPath(FileController::MODE_DOWNLOADS, $currentpath, true);
        $sourceFullPath  = app_path('../tests/test_files/test_download_file.pdf');
        $destFullPath    = $destinationPath . $filename;
        FileHelper::makedirs($destinationPath, FILE_DEFAULT_ACCESS);

        copy($sourceFullPath, $destFullPath);

        try {
            $fileItem = new FileItem();
            $fileItem->protected = 1;
            $fileItem->type = FileController::MODE_DOWNLOADS;
            $fileItem->filename = $currentpath . $filename;
            $fileItem->size = @filesize($destFullPath);
            $fileItem->extension = 'pdf';
            $fileItem->save();

            $data = ['fid' => [$fileItem->id]];
            $response = $this->actingAs($user)->post('/download?lang=en', $data, []);

            $response->assertStatus(200);
            // TODO: Improve the test for file content!
            // $response->assertHeader("Content-Type", "application/pdf; charset=binary");
            // $response->assertHeader("Content-Length", @filesize($destFullPath));
            // $response->assertHeader("Content-Disposition", 'attachment; filename="' . basename($destFullPath) . '"');

        } finally {
            @unlink($destFullPath);
        }
    }

    /**
     *  test /download file Not Authorized Editor
     *
     * @return void
     */
    public function testRouteDownloadFileNotAuthorizedEditor()
    {
        $user       = new User(['id' => 1111]);
        $user->role = \USER_ROLE_EDITOR_SMART;

        $filename = 'test_download_file.pdf';
        $currentpath = '/Daimler/';
        $destinationPath = FileController::getFullPath(FileController::MODE_DOWNLOADS, $currentpath, true);
        $sourceFullPath  = app_path('../tests/test_files/test_download_file.pdf');
        $destFullPath    = $destinationPath . $filename;

        copy($sourceFullPath, $destFullPath);

        try {
            $fileItem = new FileItem();
            $fileItem->protected = 0;
            $fileItem->type = FileController::MODE_DOWNLOADS;
            $fileItem->filename = $currentpath . $filename;
            $fileItem->size = @filesize($destFullPath);
            $fileItem->extension = 'pdf';
            $fileItem->save();

            $data = ['fid' => [$fileItem->id]];
            $response = $this->actingAs($user)->post('/download?lang=en', $data, []);

            $response->assertStatus(403);
            $response->assertSeeText('parent.msg("' . trans('system.mydownloads.notfound') . '")');
        } finally {
            @unlink($destFullPath);
        }
    }

    /**
     *  test /download Protected file Not Authorized Editor
     *
     * @return void
     */
    public function testRouteDownloadFileProtectedNotAuthorizedEditor()
    {
        $user       = new User(['id' => 1111]);
        $user->role = \USER_ROLE_EDITOR_SMART;

        $filename = 'test_download_file.pdf';
        $currentpath = '/Daimler/';
        $destinationPath = FileController::getFullPath(FileController::MODE_DOWNLOADS, $currentpath, true);
        $sourceFullPath  = app_path('../tests/test_files/test_download_file.pdf');
        $destFullPath    = $destinationPath . $filename;

        copy($sourceFullPath, $destFullPath);

        try {
            $fileItem = new FileItem();
            $fileItem->protected = 1;
            $fileItem->type = FileController::MODE_DOWNLOADS;
            $fileItem->filename = $currentpath . $filename;
            $fileItem->size = @filesize($destFullPath);
            $fileItem->extension = 'pdf';
            $fileItem->save();

            $data = ['fid' => [$fileItem->id]];
            $response = $this->actingAs($user)->post('/download?lang=en', $data, []);

            $response->assertStatus(403);
            $response->assertSeeText('parent.msg("' . trans('system.mydownloads.notfound') . '")');
        } finally {
            @unlink($destFullPath);
        }
    }

    /**
     *  test /download file Authorized Group Member
     *
     * @return void
     */
    public function testRouteDownloadFileAuthorizedMemberByGroup()
    {
        // Find user that have access to protected file
        $groupFile = Page::alive()
            ->join('download', 'page.u_id', '=', 'download.page_u_id')
            ->join('pagegroup', 'page.id', '=', 'pagegroup.page_id')
            ->join('file', 'download.file_id', '=', 'file.id')
            ->where('page.is_visible', 1)
            ->whereNull('download.deleted_at')
            ->whereNull('pagegroup.deleted_at')
            ->whereNull('file.deleted_at')
            ->where('file.protected',1)
            ->where('pagegroup.group_id', '<>' , 1)
            ->groupBy('download.page_u_id')
            ->select('pagegroup.group_id', 'download.file_id')
            ->first();

        DB::beginTransaction();
        $user = new User(['username' => 'test_0001',
                'email' => "test@gmail.com",
                'password' => "TestPa55!?",
                'password_confirmation' => "TestPa55!?",
                'first_name' => "Test",
                'last_name' => "Test",
                'company' => "Daimler",
                'country' => "Germany",
                'role' => USER_ROLE_EDITOR_FUSO]);
        $user->save();

        $usergroup_item = new Usergroup();
        $usergroup_item->user_id = $user->id;
        $usergroup_item->group_id = $groupFile->group_id;
        $usergroup_item->save();

        $fileItem = FileItem::find($groupFile->file_id);
        $sourceFullPath  = app_path('../tests/test_files/zip-icon-upload.zip');
        $destFullPath    = public_path($fileItem->getPath(FILEITEM_REAL_PATH));
        $dirPathFileItem = dirname($destFullPath);
        FileHelper::makedirs($dirPathFileItem, FILE_DEFAULT_ACCESS);
        copy($sourceFullPath, $destFullPath);

        try {
            $data = ['fid' => [$fileItem->id]];
            $response = $this->actingAs($user)->post('/download?lang=en', $data, []);

            $response->assertStatus(200);
        } finally {
            @unlink($destFullPath);
            DB::rollback();
        }
    }

    /**
     *  test /download file Anonimus
     *
     * @return void
     */
    public function testRouteDownloadFileNotAutorizedAnonimus()
    {
        $filename = 'test_download_file.pdf';
        $currentpath = '/Daimler/';
        $destinationPath = FileController::getFullPath(FileController::MODE_DOWNLOADS, $currentpath, true);
        $sourceFullPath  = app_path('../tests/test_files/test_download_file.pdf');
        $destFullPath    = $destinationPath . $filename;

        copy($sourceFullPath, $destFullPath);

        try {
            $fileItem = new FileItem();
            $fileItem->protected = 0;
            $fileItem->type = FileController::MODE_DOWNLOADS;
            $fileItem->filename = $currentpath . $filename;
            $fileItem->size = @filesize($destFullPath);
            $fileItem->extension = 'pdf';
            $fileItem->save();

            $data = ['fid' => [$fileItem->id]];
            $response = $this->post('/download?lang=en', $data, []);

            $response->assertStatus(403);
            $response->assertSeeText('parent.msg("' . trans('system.mydownloads.notfound') . '")');
        } finally {
            @unlink($destFullPath);
        }
    }

    /**
     *  test /download file Authorized Anonimus
     *
     * @return void
     */
    public function testRouteDownloadFileAutorizedAnonimus()
    {
        // Find not protected file with access for Anonimus
        $publicFile = Page::alive()
            ->join('download', 'page.u_id', '=', 'download.page_u_id')
            ->join('file', 'download.file_id', '=', 'file.id')
            ->where('page.is_visible', 1)
            ->whereNull('download.deleted_at')
            ->whereNull('file.deleted_at')
            ->where('file.protected', 0)
            ->groupBy('download.page_u_id')
            ->select('download.file_id')
            ->first();

        $fileItem = FileItem::find($publicFile->file_id);
        $sourceFullPath  = app_path('../tests/test_files/zip-icon-upload.zip');
        $destFullPath    = public_path($fileItem->getPath(FILEITEM_REAL_PATH));
        $dirPathFileItem = dirname($destFullPath);
        FileHelper::makedirs($dirPathFileItem, FILE_DEFAULT_ACCESS);
        copy($sourceFullPath, $destFullPath);

        try {
            $data = ['fid' => [$fileItem->id]];
            $response = $this->post('/download?lang=en', $data, []);

            $response->assertStatus(200);
        } finally {
            @unlink($destFullPath);
        }
    }

    /**
     *  test /files/{filemanager_mode}/{file} download image Not published page Authorized Member
     *
     * @return void
     */
    public function testRouteDownloadImageNotPublishedPageAuthorizedMember()
    {
        // Find public image that accessable
        $publicImage = Page::alive()
            ->join('gallery as g', 'page.u_id', '=', 'g.page_id')
            ->join('file', 'g.file_id', '=', 'file.id')
            ->where('page.is_visible', 1)
            ->whereNull('g.deleted_at')
            ->whereNull('file.deleted_at')
            ->where('file.protected', 0)
            ->groupBy('page.u_id')
            ->select('file.id as file_id')
            ->first();

        $user       = new User(['id' => 1111]);
        $user->role = USER_ROLE_MEMBER;

        $fileItem = FileItem::find($publicImage->file_id);
        $publicImageURL = $fileItem->getPath();
        // Set image if not exists localy
        $destFullPath    = public_path($fileItem->getPath(FILEITEM_REAL_PATH));
        if (!is_file($destFullPath)) {
            $sourceFullPath  = app_path('../tests/test_files/icon-image-upload.png');
            $dirPathFileItem = dirname($destFullPath);
            FileHelper::makedirs($dirPathFileItem, FILE_DEFAULT_ACCESS);
            copy($sourceFullPath, $destFullPath);
        }
        // Make image protected
        $fileItem->protected = 1;
        $fileItem->zoompic = "";
        $fileItem->thumbnail = "";
        $fileItem->save();

        $protectedFullPath = public_path($fileItem->getPath(FILEITEM_REAL_PATH));
        $dirPathFileItem = dirname($protectedFullPath);
        FileHelper::makedirs($dirPathFileItem, FILE_DEFAULT_ACCESS);

        FileHelper::moveProtectedFile(true, $fileItem, FileController::MODE_IMAGES);

        // Make all pages unpublised
        $pagesForSelectedFile = Page::alive()
            ->join('gallery as g', 'page.u_id', '=', 'g.page_id')
            ->whereNull('g.deleted_at')
            ->where('g.file_id', $fileItem->id)
            ->get();
        foreach ($pagesForSelectedFile as $page) {
            $page->unpublish_date = '2013-12-31 00:00:00';
            $page->save();
        }

        try {
            $response = $this->actingAs($user)->withSession(['foo' => 'bar'])->get($publicImageURL);

            $response->assertStatus(302);
            $response->assertRedirect('/file-not-found');

            // Generate protected url
            $potectedImageURL = $fileItem->getPath();
            // Keep session ID
            $cookies = [Session::getName() => Crypt::encrypt(Session::getId())];
            $response = $this->withSession(['foo' => 'bar'])->call("GET", $potectedImageURL, $parameters = [], $cookies);

            $response->assertStatus(200);
        } finally {
            FileHelper::moveProtectedFile(false, $fileItem, FileController::MODE_IMAGES);

            if (isset($sourceFullPath)) {
                @unlink($destFullPath);
            }
        }
    }

    /**
     *  test not visible thumb file /protected-file/10501/blurred_preview?policy=XXX Anonimus
     *
     * @return void
     */
    public function testRouteDownloadNotVisibleThumbOfProtectedFileNotAutorizedAnonimus()
    {
        $filename = 'test_download_file.pdf';
        $filename_thumb = 'test_download_file_thumb.png';
        $currentpath = '/Daimler/';
        $destinationPath = FileController::getFullPath(FileController::MODE_DOWNLOADS, $currentpath, true);
        $sourceFullPath  = app_path('../tests/test_files/test_download_file.pdf');
        $destFullPath    = $destinationPath . $filename;
        FileHelper::makedirs($destinationPath, FILE_DEFAULT_ACCESS);
        copy($sourceFullPath, $destFullPath);

        try {
            $fileItem = new FileItem();
            $fileItem->protected = 1;
            $fileItem->type = FileController::MODE_DOWNLOADS;
            $fileItem->filename = $currentpath . $filename;
            $fileItem->size = @filesize($destFullPath);
            $fileItem->extension = 'pdf';
            $fileItem->save();

            // Set thumb
            $destFullPathThumb = $destinationPath . FileController::FOLDER_NAME_THUMBNAIL . '/' . $filename_thumb;
            $sourceFullPath  = app_path('../tests/test_files/icon-image-upload.png');
            $dirPathThumb = dirname($destFullPathThumb);
            FileHelper::makedirs($dirPathThumb, FILE_DEFAULT_ACCESS);
            copy($sourceFullPath, $destFullPathThumb);

            // Make image protected
            $fileItem->zoompic = "";
            $fileItem->thumb_visible = false;
            $fileItem->thumbnail = $destFullPathThumb;
            $fileItem->save();

            // Generate protected url for the thumb
            $publicThumbURL = $fileItem->getCachedThumbPath('blurred_preview');

            $response = $this->withSession(['foo' => 'bar'])->get($publicThumbURL);

            $response->assertStatus(302);
            $response->assertRedirect('/file-not-found');

            // Keep session ID
            $cookies = [Session::getName() => Crypt::encrypt(Session::getId())];
            $publicThumbURL = $fileItem->getCachedThumbPath('blurred_preview');
            $response = $this->withSession(['foo' => 'bar'])->call("GET", $publicThumbURL, $parameters = [], $cookies);

            $response->assertStatus(302);
            $response->assertRedirect('/file-not-found');
        } finally {
            @unlink($destFullPath);
            @unlink($destFullPathThumb);
        }
    }

    /**
     *  test visible thumb file /protected-file/10501/blurred_preview?policy=XXX Anonimus
     *
     * @return void
     */
    public function testRouteDownloadVisibleThumbOfProtectedFileNotAutorizedAnonimus()
    {
        $filename = 'test_download_file.pdf';
        $filename_thumb = 'test_download_file_thumb.png';
        $currentpath = '/Daimler/';
        $destinationPath = FileController::getFullPath(FileController::MODE_DOWNLOADS, $currentpath, true);
        $sourceFullPath  = app_path('../tests/test_files/test_download_file.pdf');
        $destFullPath    = $destinationPath . $filename;
        FileHelper::makedirs($destinationPath, FILE_DEFAULT_ACCESS);
        copy($sourceFullPath, $destFullPath);

        try {
            $fileItem = new FileItem();
            $fileItem->protected = 1;
            $fileItem->type = FileController::MODE_DOWNLOADS;
            $fileItem->filename = $currentpath . $filename;
            $fileItem->size = @filesize($destFullPath);
            $fileItem->extension = 'pdf';
            $fileItem->save();

            // Set thumb
            $destFullPathThumb = $destinationPath . FileController::FOLDER_NAME_THUMBNAIL . '/' . $filename_thumb;
            $sourceFullPath  = app_path('../tests/test_files/icon-image-upload.png');
            $dirPathThumb = dirname($destFullPathThumb);
            FileHelper::makedirs($dirPathThumb, FILE_DEFAULT_ACCESS);
            copy($sourceFullPath, $destFullPathThumb);

            // Make image protected
            $fileItem->zoompic = "";
            $fileItem->thumb_visible = true;
            $fileItem->thumbnail = $destFullPathThumb;
            $fileItem->save();

            // Generate protected url for the thumb
            $publicThumbURL = $fileItem->getCachedThumbPath('blurred_preview');

            $response = $this->withSession(['foo' => 'bar'])->get($publicThumbURL);

            $response->assertStatus(302);
            $response->assertRedirect('/file-not-found');

            // Keep session ID
            $cookies = [Session::getName() => Crypt::encrypt(Session::getId())];
            $publicThumbURL = $fileItem->getCachedThumbPath('blurred_preview');
            $response = $this->withSession(['foo' => 'bar'])->call("GET", $publicThumbURL, $parameters = [], $cookies);

            $response->assertStatus(200);
        } finally {
            @unlink($destFullPath);
            @unlink($destFullPathThumb);
        }
    }

}
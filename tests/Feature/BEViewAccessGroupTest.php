<?php
namespace Tests\Feature;

use App\Models\User;
use App\Models\Group;
use App\Models\Usergroup;
use App\Http\Controllers\RestController;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;

class BEViewAccessGroupTest extends TestCase{

    /**
     * WEB Routes
     */

        /**
     *  test /cms/groups Authorized
     *
     * @return void
     */
    public function testRouteCMSGroupsAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_ADMIN;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/groups');

        $response->assertStatus(200);
        $response->assertViewIs('backend.be_groups');
        $response->assertViewHas(['ACTIVE' => 'groups']);
    }

    /**
     *  test /cms/groups Not Authorized
     *
     * @return void
     */
    public function testRouteCMSGroupsNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        // Role USER_ROLE_MEMBER is not in the list because it redirect to login!
        $roles = [USER_ROLE_EDITOR, USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR, USER_ROLE_EXPORTER];
        foreach ($roles as $role) {
            $user->role = $role;
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/groups');

            $response->assertStatus(401);
            $response->assertViewIs('backend.be_unauthorized_access');
            $response->assertViewHas(['error' => trans('ws_general_controller.webservice.unauthorized')]);
        }
    }

    /**
	/* API Routes
	/**

    /**
     *  test /api/v1/group/all Authorized
     *
     * @return void
     */
    public function testRouteAPIGroupAllAuthorized()
    {
        $user = new User(['id' => 1111]);
        $user->role = USER_ROLE_ADMIN;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/group/all');

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'List of Groups found'));
    }

     /**
     *  test /api/v1/group/all Not Authorized
     *
     * @return void
     */
    public function testRouteAPIGroupAllNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $roles = [USER_ROLE_MEMBER, USER_ROLE_EDITOR, USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR, USER_ROLE_EXPORTER];
        foreach ($roles as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/group/all');

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
     *
     *  test /api/v1/group/read/{id?} Authorized
     *
     * @return void
     */
    public function testRouteAPIGroupReadAuthorized()
    {
        $user = new User(['id' => 1111]);
        $user->role = USER_ROLE_ADMIN;
        $group = Group::where('active', 1)->first();

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/group/read/'.$group->id);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'Group found', $group));
    }

    /**
     *  test /api/v1/group/read/{id?} Not Authorized
     *
     * @return void
     */
    public function testRouteAPIGroupReadNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $roles = [USER_ROLE_MEMBER, USER_ROLE_EDITOR, USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR, USER_ROLE_EXPORTER];
        $group = Group::where('active', 1)->first();
        foreach ($roles as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/group/read/'.$group->id);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
     *
     *  test /api/v1/group/readbyuserid/{id?} Authorized
     *
     * @return void
     */
    public function testRouteAPIGroupReadByUserIdAuthorized()
    {
        $user = new User(['id' => 1111]);
        $user->role = USER_ROLE_ADMIN;
        $searchedUser = User::where('user.active', 1)
                ->join('usergroup', 'usergroup.user_id', '=', 'user.id')
                ->join('group', 'group.id', '=', 'usergroup.group_id')
                ->where('group.active', 1)
                ->whereNull('user.deleted_at')
                ->whereNull('usergroup.deleted_at')
                ->whereNull('group.deleted_at')
                ->select('user.*')->first();

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/group/readbyuserid/'.$searchedUser->id);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'Groups found'));
    }

    /**
     *  test /api/v1/group/readbyuserid/{id?} Not Authorized
     *
     * @return void
     */
    public function testRouteAPIGroupReadByUserIdNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $roles = [USER_ROLE_MEMBER, USER_ROLE_EDITOR, USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR, USER_ROLE_EXPORTER];
        foreach ($roles as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/group/readbyuserid/444444444444');

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
     *
     *  test /api/v1/group/create Authorized
     *
     * @return void
     */
    public function testRouteAPIGroupCreateAuthorized()
    {
        $user = new User(['id' => 1111]);
        $user->role = USER_ROLE_ADMIN;
        $group =  ['name' => 'test_group', 'visible' => 1, 'active' => 1];

        DB::beginTransaction();
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/group/create/', $group);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'Group was added.'));
        DB::rollback();
    }

    /**
     *  test /api/v1/group/create Not Authorized
     *
     * @return void
     */
    public function testRouteAPIGroupCreateNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $roles = [USER_ROLE_MEMBER, USER_ROLE_EDITOR, USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR, USER_ROLE_EXPORTER];
        foreach ($roles as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/group/create/', []);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }
    /**
     *
     *  test /api/v1/group/addmember Authorized
     *
     * @return void
     */
    public function testRouteAPIGroupAddMemberAuthorized()
    {
        $user = new User(['id' => 1111]);
        $user->role = USER_ROLE_ADMIN;
        $group = Group::where('id','<>', 1)->first();
        $searchedUser = User::leftJoin('usergroup', 'user.id', '=', 'usergroup.user_id')
                ->groupBy('user.id')
                ->havingRaw('COUNT(user.id) = 1')
                ->select('user.*')
                ->first();
        DB::beginTransaction();
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('Post', '/api/v1/group/addmember/',
                ['user_id' => $searchedUser->id,'group_id'=> $group->id]);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'User was added to group.'));
        DB::rollback();
    }

    /**
     *  test /api/v1/group/addmember Not Authorized
     *
     * @return void
     */
    public function testRouteAPIGroupAddMemberNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $roles = [USER_ROLE_MEMBER, USER_ROLE_EDITOR, USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR, USER_ROLE_EXPORTER];
        foreach ($roles as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('Post', '/api/v1/group/addmember/',
                ['user_id' => '','group_id'=> '']);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

     /**
     *
     *  test /api/v1/group/update Authorized
     *
     * @return void
     */
    public function testRouteAPIGroupUpdateAuthorized()
    {
        $user = new User(['id' => 1111]);
        $user->role = USER_ROLE_ADMIN;
        $group = Group::where('id','<>', 1)->first();

        DB::beginTransaction();
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/group/update/', $group->toArray());

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'Group was updated.', $group));
        DB::rollback();
    }

    /**
     *  test /api/v1/group/update Not Authorized
     *
     * @return void
     */
    public function testRouteAPIGroupUpdateNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $roles = [USER_ROLE_MEMBER, USER_ROLE_EDITOR, USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR, USER_ROLE_EXPORTER];
        foreach ($roles as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/group/update/', []);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
     *
     *  test /api/v1/group/delete/{id?} Authorized
     *
     * @return void
     */
    public function testRouteAPIGroupDeleteAuthorized()
    {
        $user = new User(['id' => 1111]);
        $user->role = USER_ROLE_ADMIN;
        $group = Group::first();

        DB::beginTransaction();
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('DELETE', '/api/v1/group/delete/'.$group->id);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'Group was deleted!'));
        DB::rollback();
    }

    /**
     *  test /api/v1/group/delete/{id?} Not Authorized
     *
     * @return void
     */
    public function testRouteAPIGroupDeleteNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $roles = [USER_ROLE_MEMBER, USER_ROLE_EDITOR, USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR, USER_ROLE_EXPORTER];
        foreach ($roles as $role) {
            $user->role = $role;

            $response = $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('DELETE', '/api/v1/group/delete/1000000000');

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }

    /**
     *
     *  test /api/v1/group/removemember/ Authorized
     *
     * @return void
     */
    public function testRouteAPIGroupRemoveMemberAuthorized()
    {
        $user = new User(['id' => 1111]);
        $user->role = USER_ROLE_ADMIN;
        $usergroup = Usergroup::where('group_id', '<>', 1)->first();

        DB::beginTransaction();
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('DELETE', '/api/v1/group/removemember/', $usergroup->toArray());

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, 'User was deleted from group!',$usergroup));
        DB::rollback();
    }
	 
	 /**
     *  test /api/v1/group/removemember/ Not Authorized
     *
     * @return void
     */
    public function testRouteAPIGroupRemoveMemberNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $roles = [USER_ROLE_MEMBER, USER_ROLE_EDITOR, USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR, USER_ROLE_EXPORTER];
        foreach ($roles as $role) {
            $user->role = $role;

            $response = $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('DELETE', '/api/v1/group/removemember', []);

            $response->assertStatus(401);
            $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
        }
    }
}

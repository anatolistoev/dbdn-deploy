<?php

namespace Tests\Feature;

use App\Models\Group;
use App\Models\User;
use App\Models\Faq;
use App\Http\Controllers\RestController;
use App\Http\Middleware\BeAccessFaqs;
use App\Http\Middleware\BeAccessApprover;
use App\Http\Middleware\BeAccessCms;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;

class BEViewAccessFaqsTest extends TestCase
{
    /**
     *  test /cms/faqs Authorized
     *
     * @return void
     */
    public function testRouteCMSFaqsAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach (BeAccessFaqs::getRoles() as $role) {
            $user->role = $role;
            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/faqs');

            $response->assertStatus(200);
            $response->assertViewIs('backend.be_faqs');
            $response->assertViewHas(['ACTIVE' => 'faqs', 'LANG']);
        }
    }

    /**
     *  test /api/v1/faq/allfaqs/2 Authorized
     *
     * @return void
     */
    public function testRouteAPIFaqsAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach (BeAccessFaqs::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/faq/allfaqs/2');

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'List of FAQs found.'));
        }
    }

    /**
     *  test /api/v1/faq/allfaqs Required param
     *
     * @return void
     */
    public function testRouteAPIFaqsRequiredParam()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_ADMIN;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET','/api/v1/faq/allfaqs');

        $response->assertStatus(400);
        $response->assertJson(RestController::generateJsonResponse(true, 'Parameter lang is required!'));
    }

    /**
     *  test /cms/faqs Not Authorized
     *
     * @return void
     */
    public function testRouteCMSFaqsNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = \USER_ROLE_EDITOR_FUSO;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/faqs');

        $response->assertStatus(401);
        $response->assertViewIs('backend.be_unauthorized_access');
        $response->assertViewHas(['error' => trans('ws_general_controller.webservice.unauthorized')]);
    }

    /**
     *  test /api/v1/faq/allfaqs/2 Not Authorized
     *
     * @return void
     */
    public function testRouteAPIFaqsNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR_FUSO;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET','/api/v1/faq/allfaqs/2');

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

     /**
     *  test /api/v1/faq/faqbyid/{id?} Authorized
     *
     * @return void
     */
    public function testRouteAPIFaqsByIdAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $faq_item = Faq::where('brand_id', '<>', SMART)->first();
        foreach (BeAccessFaqs::getRoles() as $role) {
            $user->role = $role;
            if($user->role != USER_ROLE_EDITOR_SMART){
                $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/faq/faqbyid/'.$faq_item->id);

                $response->assertStatus(200);
                $response->assertJson(RestController::generateJsonResponse(false, 'FAQ is found.', $faq_item));
            }
        }
    }

    /**
     *  test /api/v1/faq/faqbyid/{id?} SMART user Not Authorized
     *
     * @return void
     */
    public function testRouteAPIFaqsByIdSmartUserNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $faq_item = Faq::where('brand_id', '<>', SMART)->first();
        $user->role = USER_ROLE_EDITOR_SMART;
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/faq/faqbyid/'.$faq_item->id);

        $response->assertStatus(400);
        $response->assertJson(RestController::generateJsonResponse(true, "You don't have access to FAQ for this brand!"));
    }

        /**
     *  test /api/v1/faq/faqbyid/{id?} Not Authorized
     *
     * @return void
     */
    public function testRouteAPIFaqsByIdNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $faq_item = Faq::first();
        $user->role = USER_ROLE_EDITOR_FUSO;
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/faq/faqbyid/'.$faq_item->id);

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/faq/create Authorized
     *
     * @return void
     */
    public function testRouteAPIFaqsCreateAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $postData = ['question'=> 'Test faq question.', 'answer' => 'Test FAQ answer.', 'brand_id' => DAIMLER, 'category_id' => 1, 'lang' => 2];
        foreach (BeAccessFaqs::getRoles() as $role) {
            $user->role = $role;
            if($user->role != USER_ROLE_EDITOR_SMART){
                DB::beginTransaction();
                $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/faq/create/',$postData);

                $response->assertStatus(200);
                $response->assertJson(RestController::generateJsonResponse(false, "FAQ was added."));
                DB::rollback();
            }
        }
    }

    /**
     *  test /api/v1/faq/create Smart not Authorized
     *
     * @return void
     */
    public function testRouteAPIFaqsCreateSmartNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $postData = ['question'=> 'Test faq question.', 'answer' => 'Test FAQ answer.', 'brand_id' => DAIMLER, 'category_id' => 1, 'lang' => 2];
        $user->role = USER_ROLE_EDITOR_SMART;
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/faq/create/',$postData);

        $response->assertStatus(400);
        $response->assertJson(RestController::generateJsonResponse(true, "You don't have access to create FAQs for this brand!"));
    }

    /**
     *  test /api/v1/faq/create not Authorized
     *
     * @return void
     */
    public function testRouteAPIFaqsCreateNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR_FUSO;
        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/faq/create/',[]);

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/faq/update/ Authorized
     *
     * @return void
     */
    public function testRouteAPIFaqsUpdateAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $faq_item = Faq::where('brand_id', '<>', SMART)->first();
        foreach (BeAccessFaqs::getRoles() as $role) {
            $user->role = $role;
            if($user->role != USER_ROLE_EDITOR_SMART){
                DB::beginTransaction();
                $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/faq/update/', $faq_item->toArray());

                $response->assertStatus(200);
                $response->assertJson(RestController::generateJsonResponse(false, 'FAQ was updated.'));
                DB::rollback();
            }
        }
    }

    /**
     *  test /api/v1/faq/update/ Smart user not Authorized
     *
     * @return void
     */
    public function testRouteAPIFaqsUpdateSmartUserNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $faq_item = Faq::where('brand_id', '<>', SMART)->first();
        $user->role = USER_ROLE_EDITOR_SMART;
        DB::beginTransaction();

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/faq/update/', $faq_item->toArray());
        $response->assertStatus(400);
        $response->assertJson(RestController::generateJsonResponse(true, "You don't have access to edit FAQs for this brand!"));
        DB::rollback();
    }

    /**
     *  test /api/v1/faq/update/ Not Authorized
     *
     * @return void
     */
    public function testRouteAPIFaqsUpdateNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR_FUSO;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/faq/update/', []);
        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/faq/delete//$id Authorized
     *
     * @return void
     */
    public function testRouteAPIFaqsDeleteAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $faq_item = Faq::where('brand_id', '<>', SMART)->first();
        foreach (BeAccessFaqs::getRoles() as $role) {
            $user->role = $role;
            if($user->role != USER_ROLE_EDITOR_SMART){
                DB::beginTransaction();
                $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('DELETE', '/api/v1/faq/delete/'. $faq_item->id);

                $response->assertStatus(200);
                $response->assertJson(RestController::generateJsonResponse(false, 'FAQ was deleted!'));
                DB::rollback();
            }
        }
    }

    /**
     *  test /api/v1/faq/delete/ Smart user not Authorized
     *
     * @return void
     */
    public function testRouteAPIFaqsDeleteSmartUserNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $faq_item = Faq::where('brand_id', '<>', SMART)->first();
        $user->role = USER_ROLE_EDITOR_SMART;
        DB::beginTransaction();

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('DELETE', '/api/v1/faq/delete/'. $faq_item->id);
        $response->assertStatus(400);
        $response->assertJson(RestController::generateJsonResponse(true, "You don't have access to delete FAQs for this brand!"));
        DB::rollback();
    }

    /**
     *  test /api/v1/faq/delete/ Not Authorized
     *
     * @return void
     */
    public function testRouteAPIFaqsDeleteNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR_FUSO;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('DELETE', '/api/v1/faq/delete/'. 123456789);
        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }
}

<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;

class ConsoleTest extends TestCase
{
    /**
    * Test a console command CountPagesByTag.
    *
    * @return void
    */
    public function test_pagesByTag_count()
    {
        $updateCount = $this->artisan('pagesByTag:count');
        $this->assertGreaterThanOrEqual(0, $updateCount, 'Updated Tag counts');
        // If you need result of console output
        $resultAsText = Artisan::output();
//        $this->assertEquals('', $resultAsText);
    }

    /**
    * Test a console command CountPagesByTag.
    *
    * @return void
    */
    public function test_futurePublishPages_activate()
    {
        $activatedCount = $this->artisan('futurePublishPages:activate');
        $this->assertGreaterThanOrEqual(0, $activatedCount, 'Activated Pages counts');
        // If you need result of console output
        $resultAsText = Artisan::output();
//        $this->assertEquals('', $resultAsText);
    }
}

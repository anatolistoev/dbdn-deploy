<?php

namespace Tests\Feature;

use App\Models\Group;
use App\Models\User;
use App\Http\Controllers\RestController;
use App\Http\Middleware\BeAccessFaqs;
use App\Http\Middleware\BeAccessApprover;
use App\Http\Middleware\BeAccessCms;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BEViewAccessGlossaryTest extends TestCase
{
    /**
     *  test /cms/glossary Authorized
     *
     * @return void
     */
    public function testRouteCMSGlossaryAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach (BeAccessApprover::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/glossary');

            $response->assertStatus(200);
            $response->assertViewIs('backend.be_glossary');
            $response->assertViewHas(['ACTIVE' => 'glossary', 'LANG']);
        }
    }

    /**
     *  test /api/v1/glossary/glossary/3902?lang=2 Authorized
     *
     * @return void
     */
    public function testRouteAPIGlossaryAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach (BeAccessApprover::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/glossary/glossary/3902?lang=2');

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, "List of terms found."));
        }
    }

    /**
     *  test /cms/glossary Not Authorized
     *
     * @return void
     */
    public function testRouteCMSGlossaryNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR_SMART;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/glossary');

        $response->assertStatus(401);
        $response->assertViewIs('backend.be_unauthorized_access');
        $response->assertViewHas(['error' => trans('ws_general_controller.webservice.unauthorized')]);
    }

    /**
     *  test /api/v1/glossary/glossary/3902?lang=2 Not Authorized
     *
     * @return void
     */
    public function testRouteAPIGlossaryNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR_SMART;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET','/api/v1/glossary/glossary/3902?lang=2');

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

}

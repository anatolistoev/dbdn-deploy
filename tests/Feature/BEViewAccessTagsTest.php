<?php

namespace Tests\Feature;

use App\Models\Group;
use App\Models\User;
use App\Models\Tag;
use App\Http\Controllers\RestController;
use App\Http\Middleware\BeAccessFaqs;
use App\Http\Middleware\BeAccessApprover;
use App\Http\Middleware\BeAccessCms;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BEViewAccessTagsTest extends TestCase
{
    use DatabaseTransactions;

    /**
     *  test /cms/tags Authorized
     *
     * @return void
     */
    public function testRouteCMSTagsAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach (BeAccessApprover::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/tags');

            $response->assertStatus(200);
            $response->assertViewIs('backend.be_tags');
            $response->assertViewHas(['ACTIVE' => 'tags', 'LANG']);
        }
    }

    /**
     *  test /api/v1/tag/all/2 Authorized
     *
     * @return void
     */
    public function testRouteAPITagsAllAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach (BeAccessApprover::getRoles() as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/tag/all/2');
            
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, "List of Tags found"));
        }
    }

    /**
     *  test /api/v1/tag/delete/{id} Not Authorized
     *
     * @return void
     */
    public function testRouteAPITagsAllNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR_SMART;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/tag/all/2');

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/tag/delete/{id} Authorized
     *
     * @return void
     */
    public function testRouteAPITagsDeleteAuthorized()
    {
        $user     = new User(['id' => 1111]);
        foreach (BeAccessApprover::getRoles() as $role) {
            // Create Tag
            $tag = new Tag();
            $tag->fill(['lang' => 2, 'name' => 'TestDeleteTag']);
            $tag->save();

            // Delete Tag
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('DELETE', '/api/v1/tag/delete/' . $tag->id);
            
            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, "Tag was deleted!"));
        }
    }

    /**
     *  test /api/v1/tag/delete/{id} Not Authorized
     *
     * @return void
     */
    public function testRouteAPITagsDeleteNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR_SMART;

        // Create Tag
        $tag = new Tag();
        $tag->fill(['lang' => 2, 'name' => 'TestDeleteTag']);
        $tag->save();

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('DELETE', '/api/v1/tag/delete/' . $tag->id);

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

    /**
     *  test /api/v1/tag/all Required param
     *
     * @return void
     */
    public function testRouteAPITagsRequiredParam()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_ADMIN;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET','/api/v1/tag/all');

        $response->assertStatus(400);
        $response->assertJson(RestController::generateJsonResponse(true, 'Parameter lang is required!'));
    }

    /**
     *  test /cms/tags Not Authorized
     *
     * @return void
     */
    public function testRouteCMSTagsNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR_SMART;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/tags');

        $response->assertStatus(401);
        $response->assertViewIs('backend.be_unauthorized_access');
        $response->assertViewHas(['error' => trans('ws_general_controller.webservice.unauthorized')]);
    }

    /**
     *  test /api/v1/tag/all/2 Not Authorized
     *
     * @return void
     */
    public function testRouteAPITagsNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_NEWSLETTER_APPROVER;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET','/api/v1/tag/all/2');

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }

}

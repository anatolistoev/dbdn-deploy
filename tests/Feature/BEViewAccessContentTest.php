<?php

namespace Tests\Feature;

use App\Models\Group;
use App\Models\User;
use App\Models\Page;
use App\Models\Subscription;

use App\Http\Controllers\RestController;
use App\Http\Middleware\BeAccessFaqs;
use App\Http\Middleware\BeAccessApprover;
use App\Http\Middleware\BeAccessCms;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BEViewAccessContentTest extends TestCase
{
    use DatabaseTransactions;

    //TODO: !!! /cms/content/files/{file}

    /**
     *  test /cms/content/{pageid} Authorized
     *
     * @return void
     */
    public function testRouteCMSContentForPageAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = User::where('active', 1)->first();
        // Make page editable
        $page = Page::find(2207); // Page from smart
        $page->active = 0;
        $page->workflow->editing_state = \STATE_DRAFT;
        $page->workflow->user_responsible = $user->id;
        $page->save();
        $page->workflow->save();

        foreach ([\USER_ROLE_EDITOR_SMART, \USER_ROLE_ADMIN, \USER_ROLE_EDITOR] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/content/'. $page->id);

            $response->assertStatus(200);
            $response->assertViewIs('backend.content_basic');
            $response->assertViewHas(['ACTIVE' => 'content', 'GROUPS' => Group::all(), 'FILEMANAGER_MODE' => 'downloads', 'LANG']);
        }
    }

    /**
     *  test /cms/content/{pageid} Authorized but access approved page
     *
     * @return void
     */
    public function testRouteCMSContentForPageApprovedNotAuthorized()
    {
        $user     = new User(['id' => 1111]);
        $page = Page::find(2207); // Page from smart
        $page->active = 1;
        $page->workflow()->editing_state = \STATE_APPROVED;
        $page->save();

        foreach ([\USER_ROLE_EDITOR_SMART, \USER_ROLE_ADMIN, \USER_ROLE_EDITOR] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->get('/cms/content/' . $page->id);

            $response->assertStatus(404);
            $response->assertViewIs('backend.be_approved');
            $response->assertViewHas(['ACTIVE' => '']);
        }
    }

    /**
     *  test /cms/pages Authorized first step but without second step!
     *
     * @return void
     */
    public function testRouteCMSContentForPageAuthorizedWithoutConfirm()
    {
        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR;

        $response = $this->actingAs($user)->get('/cms/content/2207');

        $response->assertStatus(200);
        $response->assertViewIs('backend.be_confirm');
        $response->assertViewMissing('ACTIVE');
    }

    /**
     *  test /api/v1/contents/page-id/2207/2 Authorized
     *
     * @return void
     */
    public function testRouteAPIContentsForPageAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user     = User::where('active', 1)->first();
        foreach ([\USER_ROLE_EDITOR_SMART, \USER_ROLE_ADMIN] as $role) {
            $user->role = $role;

            $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/contents/page-id/2207/2');

            $response->assertStatus(200);
            $response->assertJson(RestController::generateJsonResponse(false, 'List of contents found.'));
        }
    }

    /**
     *  test /api/v1/contents/page-id/2207/2 Not Authorized
     *
     * @return void
     */
    public function testRouteAPIContentForPageNotAuthorized()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user     = new User(['id' => 1111]);
        $user->role = USER_ROLE_EDITOR_DFM;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('GET', '/api/v1/contents/page-id/2207/2');

        $response->assertStatus(401);
        $response->assertJson(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')));
    }
}

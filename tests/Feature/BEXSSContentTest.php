<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Content;
use App\Models\Page;

use App\Http\Controllers\RestController;

use Tests\TestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Support\Facades\DB;


class BEXSSContentTest extends TestCase
{
    use DatabaseTransactions;

   /**
    *  test /api/v1/contents/create No XSS content
    *
    * @return void
    */
    public function testRouteAPIContentsCreateNoXSSContent()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $smartPage = Page::join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->first();
        $smartPage->workflow->editing_state = \STATE_DRAFT;
        $smartPage->workflow->save();

        $postData = ["body"=> "<p>Test Author</p>",
                    "format" => 1,
                    "lang"=> 2,
                    "page_id" => $smartPage->id,
                    "page_u_id" => $smartPage->u_id,
                    "position" => 0,
                    "searchable" => 1,
                    "section" => 'author'];
        
        $user->role = USER_ROLE_EDITOR_SMART;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/contents/create', $postData);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, "Content was added.", [$postData]));
    }

   /**
    *  test /api/v1/contents/create with XSS content
    *
    * @return void
    */
    public function testRouteAPIContentsCreateWithXSSContent()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $smartPage = Page::join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->first();
        $smartPage->workflow->editing_state = \STATE_DRAFT;
        $smartPage->workflow->save();

        $plainHTML = "<p>Test Author</p>";
        //TODO: Add more tags and elemts to be stripped
        $postData = ["body"=> $plainHTML."<script>alert('...ESCAPE UNTRUSTED DATA BEFORE PUTTING HERE...')</script>",
                    "format" => 1,
                    "lang"=> 2,
                    "page_id" => $smartPage->id,
                    "page_u_id" => $smartPage->u_id,
                    "position" => 0,
                    "searchable" => 1,
                    "section" => 'author'];
        
        $user->role = USER_ROLE_EDITOR_SMART;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/contents/create', $postData);

        $response->assertStatus(200);
        $postData["body"] = $plainHTML;
        $response->assertJson(RestController::generateJsonResponse(false, "Content was added.", [$postData]));
    }

   /**
    *  test /api/v1/contents/create-many No XSS content
    *
    * @return void
    */
    public function testRouteAPIContentsCreateManyNoXSSContent()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $smartPage = Page::join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->first();
        $smartPage->workflow->editing_state = \STATE_DRAFT;
        $smartPage->workflow->save();

        $postData = [
            0 => ["body"=> "<p>Test Author</p>",
                "format" => 1,
                "lang"=> 2,
                "page_id" => $smartPage->id,
                "page_u_id" => $smartPage->u_id,
                "position" => 0,
                "searchable" => 1,
                "section" => 'author'],
            1 => ["body"=> "<p>Test Author</p>",
                "format" => 1,
                "lang"=> 1,
                "page_id" => $smartPage->id,
                "page_u_id" => $smartPage->u_id,
                "position" => 0,
                "searchable" => 1,
                "section" => 'author']
        ];
        
        $user->role = USER_ROLE_EDITOR_SMART;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/contents/create-many', $postData);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, "Content was added.", $postData));
    }

   /**
    *  test /api/v1/contents/create-many with XSS content
    *
    * @return void
    */
    public function testRouteAPIContentsCreateManyWithXSSContent()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $smartPage = Page::join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->first();
        $smartPage->workflow->editing_state = \STATE_DRAFT;
        $smartPage->workflow->save();

        $plainHTML = "<p>Test Author</p>";
        //TODO: Add more tags and elemts to be stripped
        $postData = [
            0 => ["body"=> $plainHTML."<script>alert('...ESCAPE UNTRUSTED DATA BEFORE PUTTING HERE...')</script>",
                "format" => 1,
                "lang"=> 2,
                "page_id" => $smartPage->id,
                "page_u_id" => $smartPage->u_id,
                "position" => 0,
                "searchable" => 1,
                "section" => 'author'],
            1 => ["body"=> "<p onclick='alert('...ESCAPE UNTRUSTED DATA BEFORE PUTTING HERE...');'>Test Author</p>",
                "format" => 1,
                "lang"=> 1,
                "page_id" => $smartPage->id,
                "page_u_id" => $smartPage->u_id,
                "position" => 0,
                "searchable" => 1,
                "section" => 'author']
        ];
                    
        $user->role = USER_ROLE_EDITOR_SMART;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('POST', '/api/v1/contents/create-many', $postData);

        $response->assertStatus(200);
        $postData[0]["body"] = $plainHTML;
        $postData[1]["body"] = $plainHTML;
        $response->assertJson(RestController::generateJsonResponse(false, "Content was added.", $postData));
    }


   /**
    *  test /api/v1/contents/update No XSS content
    *
    * @return void
    */
    public function testRouteAPIContentsUpdateNoXSSContent()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $pageContent = Content::select("content.*")
                ->from(DB::raw("(Select page_u_id from `content` where section = 'title' AND `deleted_at` IS NULL group by page_u_id having count(page_u_id) = 2) as ct"))
                ->join('page', 'ct.page_u_id', '=', 'page.u_id')
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->join('content', 'content.page_u_id', '=', 'page.u_id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->whereNull('content.deleted_at')
                ->where('content.section', '=', 'title')
                ->whereNull('page.deleted_at')
                ->orderBy('content.page_u_id')
                ->limit(1)
                ->first();

        $srcPage = $pageContent->page;
        $srcPage->workflow->editing_state = \STATE_DRAFT;
        $srcPage->workflow->save();

        $postData = ['body' => $pageContent->body . ' Update Content!', 'id' => $pageContent->id];

        $user->role = USER_ROLE_EDITOR_SMART;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/contents/update', $postData);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, "Content was updated.", [$postData]));
    }

   /**
    *  test /api/v1/contents/update With XSS content
    *
    * @return void
    */
    public function testRouteAPIContentsUpdateWithXSSContent()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        $pageContent = Content::select("content.*")
                ->from(DB::raw("(Select page_u_id from `content` where section = 'title' AND `deleted_at` IS NULL group by page_u_id having count(page_u_id) = 2) as ct"))
                ->join('page', 'ct.page_u_id', '=', 'page.u_id')
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->join('content', 'content.page_u_id', '=', 'page.u_id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->whereNull('content.deleted_at')
                ->where('content.section', '=', 'title')
                ->whereNull('page.deleted_at')
                ->orderBy('content.page_u_id')
                ->limit(1)
                ->first();

        $srcPage = $pageContent->page;
        $srcPage->workflow->editing_state = \STATE_DRAFT;
        $srcPage->workflow->save();

        $plainHTML = "<p>Test Author</p>";
        //TODO: Add more tags and elemts to be stripped
        $postData = ['body' => $plainHTML."<script>alert('...ESCAPE UNTRUSTED DATA BEFORE PUTTING HERE...')</script>", 'id' => $pageContent->id];

        $user->role = USER_ROLE_EDITOR_SMART;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/contents/update', $postData);

        $response->assertStatus(200);
        $postData["body"] = $plainHTML;
        $response->assertJson(RestController::generateJsonResponse(false, "Content was updated.", [$postData]));
    }

   /**
    *  test /api/v1/contents/update-many No XSS content
    *
    * @return void
    */
    public function testRouteAPIContentsUpdateManyNoXSSContent()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        // Setup page
        $pageContent = Content::select("content.*")
                ->from(DB::raw("(Select page_u_id from `content` where section = 'title' AND `deleted_at` IS NULL group by page_u_id having count(page_u_id) = 2) as ct"))
                ->join('page', 'ct.page_u_id', '=', 'page.u_id')
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->join('content', 'content.page_u_id', '=', 'page.u_id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->whereNull('content.deleted_at')
                ->where('content.section', '=', 'title')
                ->whereNull('page.deleted_at')
                ->orderBy('content.page_u_id')
                ->limit(2)
                ->get();
        
        $pageContent[0]->page->workflow->editing_state = STATE_DRAFT;
        $pageContent[0]->page->workflow->save();
        $pageContent[1]->page->workflow->editing_state = STATE_DRAFT;
        $pageContent[1]->page->workflow->save();

        $postData = [ 0 => ['body' => "Update content 1", 'id' => $pageContent[0]->id],
                      1 => ['body' => "Update content 2", 'id' => $pageContent[1]->id]];

        $user->role = USER_ROLE_EDITOR_SMART;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/contents/update-many', $postData);

        $response->assertStatus(200);
        $response->assertJson(RestController::generateJsonResponse(false, "Content was updated.", $postData));
    }

   /**
    *  test /api/v1/contents/update-many with XSS content
    *
    * @return void
    */
    public function testRouteAPIContentsUpdateManyWithXSSContent()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $user = new User(['username' => 'testPageUser']);
        $user->id = 1111;
        // Setup page
        $pageContent = Content::select("content.*")
                ->from(DB::raw("(Select page_u_id from `content` where section = 'title' AND `deleted_at` IS NULL group by page_u_id having count(page_u_id) = 2) as ct"))
                ->join('page', 'ct.page_u_id', '=', 'page.u_id')
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->join('content', 'content.page_u_id', '=', 'page.u_id')
                ->where('hierarchy.parent_id',SMART)
                ->where('hierarchy.level',3)
                ->whereNull('content.deleted_at')
                ->where('content.section', '=', 'title')
                ->whereNull('page.deleted_at')
                ->orderBy('content.page_u_id')
                ->limit(2)
                ->get();
        
        $pageContent[0]->page->workflow->editing_state = STATE_DRAFT;
        $pageContent[0]->page->workflow->save();
        $pageContent[1]->page->workflow->editing_state = STATE_DRAFT;
        $pageContent[1]->page->workflow->save();

        $plainHTML = "<p>Test Author</p>";
        //TODO: Add more tags and elemts to be stripped
        $postData = [ 0 => ['body' => $plainHTML."<script>alert('...ESCAPE UNTRUSTED DATA BEFORE PUTTING HERE...')</script>", 'id' => $pageContent[0]->id],
                      1 => ['body' => "<p onclick='alert('...ESCAPE UNTRUSTED DATA BEFORE PUTTING HERE...');'>Test Author</p>", 'id' => $pageContent[1]->id]];

        $user->role = USER_ROLE_EDITOR_SMART;

        $response = $this->actingAs($user)->withSession(['isConfirmed' => true])->json('PUT', '/api/v1/contents/update-many', $postData);

        $response->assertStatus(200);
        $postData[0]["body"] = $plainHTML;
        $postData[1]["body"] = $plainHTML;
        $response->assertJson(RestController::generateJsonResponse(false, "Content was updated.", $postData));
    }

}

<?php

namespace Tests\Feature;

use App\Models\Group;
use App\Models\User;
use App\Http\Controllers\RestController;
use App\Http\Middleware\BeAccessFaqs;
use App\Http\Middleware\BeAccessApprover;
use App\Http\Middleware\BeAccessCms;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BEViewAccessTest extends TestCase
{
    /**
     *  test /cms No Authorized
     *
     * @return void
     */
    public function testRouteCMSNoAuthorized()
    {
        $response = $this->get('/cms');

        $response->assertStatus(200);
        $response->assertViewIs('backend.be_login');
    }

}

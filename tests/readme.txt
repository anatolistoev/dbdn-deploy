To run the tests dev need:
1. Requirements:
FF 43+
IE 11
Chrome 40+

2. Setup app in debug and test mode:
Set in app.php for your environment (usually: local_dev)
	'debug' => true,
	'testing' => TRUE,

3. Run the app with command line:
> php.exe -S localhost:8000

4. Open the URL in required browser:
All test:
http://localhost:8000/tests/qunit/index.html

One module test:
http://localhost:8000/tests/qunit/index.html?module=Test%20REST%20Comments%20Web%20Services

You can select particular test from combobox on the right upper side.
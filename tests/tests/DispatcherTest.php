<?php

class DispatcherTest extends BrowserKitTestCase
{

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testRoute()
    {
        $crawler = $this->client->request('GET', '/');

        $this->assertTrue($this->client->getResponse()->isOk());
    }
    /**
     * inspect the variables in Home view
     * @return void
     */
    protected function checkHomeViewVars()
    {
        $this->assertViewHas('PAGE');
        $this->assertViewHas('CONTENTS');
        $this->assertViewHas('SECTION_PID_INDEX');
        $this->assertViewHas('L2ID');
        $this->assertViewHas('ACCORDIONS');
        $this->assertViewHas('HOMEBLOCKS');
    }
    /**
     * test /home
     * @return void
     */
    public function testRouteHome()
    {
        $crawler = $this->client->request('GET', '/home');

        $this->assertTrue($this->client->getResponse()->isOk());
        $this->checkHomeViewVars();
    }
    /**
     * Test /1
     * @return void
     */
    public function testRoute1()
    {
        $crawler = $this->client->request('GET', '/1');

        $this->assertTrue($this->client->getResponse()->isOk());
        $this->checkHomeViewVars();
    }
    /**
     * Test /print/1
     * @return void
     */
    public function testRoutePrint1()
    {
        $crawler = $this->client->request('GET', '/print/1');

        $this->assertTrue($this->client->getResponse()->isOk());
        $this->assertViewHas('PAGE');
        $this->assertViewHas('CONTENTS');
        $this->assertViewHas('SECTION_PID_INDEX'); #?
        $this->assertViewHas('L2ID');
        $this->assertViewHas('ACCORDIONS');
        $this->assertViewHas('HOMEBLOCKS');
    }
    /**
     * test /daimler
     * @return void
     */
    public function testRouteDaimler()
    {
        $crawler = $this->client->request('GET', '/daimler');

        $this->assertTrue($this->client->getResponse()->isOk());
    }
    /**
     * test /979
     * @return void
     */
    public function testRoute979()
    {
        $crawler = $this->client->request('GET', '/979');

        $this->assertTrue($this->client->getResponse()->isOk());
    }
    /**
     * test /nonexisting
     * @return void
     */
    public function testRouteNonexisting()
    {
        $crawler = $this->client->request('GET', '/nosuchroute');

        $this->assertFalse($this->client->getResponse()->isOk());
    }
}

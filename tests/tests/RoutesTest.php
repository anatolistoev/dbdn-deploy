<?php

class RoutesTest extends BrowserKitTestCase
{

    public function setUp()
    {
        parent::setUp();

        Session::start();

        //Route::enableFilters();
    }

    protected function tryRoute($path, $method = 'GET', $params = [])
    {
        //$response = $this->call($method, $uri, $parameters, $files, $server, $content);
        $crawler = $this->client->request($method, $path, $params);
        //$this->assertResponseOk();
        $this->assertTrue($this->client->getResponse()->isOk());
    }

    public function testRoot()
    {
        $this->tryRoute('/');
    }

    //Route::any('/login/{idslug}', 	'LoginController@doLogin')->where('idslug', '[A-Za-z0-9_/:\.\-\~]*');
    public function testRouteLogin()
    {
        //$this->tryRoute('/login/979');
        $params = ['username'=>'slowpog', 'password'=>'QQqq!!11', 'csrf_token' => csrf_token()];
        $response = $this->call('POST', 'login/979', $params);
        //$this->assertResponseOk();
        //$this->assertTrue($response->isOk());
        $this->assertRedirectedTo('/979');
        $this->assertSessionHas('myDownloadsCount');
        $this->assertSessionHas('watchlistCount');
    }
    //Route::any('/logout', 			'LoginController@logout');
    public function testRouteLogout()
    {
        $user = new User(['id'=>1, 'username' => 'slowpog']);
        $this->be($user);
        //$this->tryRoute('logout');
        $response = $this->call('GET', 'logout');

        //$this->assertResponseStatus(302);
        $this->assertRedirectedTo('home');
    }
    //Route::get('/captcha', 			'LoginController@generateCAPTCHA');
    //public function testRouteCaptcha()
    //{
    //	$user = new User(array('id'=>1, 'username' => 'slowpog'));
    //	$this->be($user);
    //	Session::set('captcha', 'asdfg');
    //	$response = $this->call('GET', '/captcha');
    //}
    //Route::get('/registration', 	'LoginController@registrationForm');
    public function testRouteRegistration()
    {
        $this->tryRoute('/registration');
    }
    //Route::post('/registration', 			array('before' => 'csrf', 'uses' => 'LoginController@register'));
    public function testRouteRegistrationPost()
    {
        //$user = new User(array('id'=>1, 'username' => 'slowpog'));
        //$this->be($user);
        //$this->tryRoute('/registration', 'POST', array());
        $response = $this->call('POST', '/registration', []);
        //$this->assertResponseStatus(200);
        $this->assertSessionHasErrors();
    }
    //Route::get('/user/activate/{code}', 	'LoginController@unlockUser')->where('code', '[A-Za-z0-9]{10}');
    public function testRouteActivate()
    {
        $crawler = $this->client->request('GET', '/user/activate/1234567890');
        $this->assertCount(1, $crawler->filter('h1:contains("Fehlgeschlagene Aktivierung")'));
    }
    //Route::get('/profile', 					array('before' => 'auth', 		'uses' => 'LoginController@profileForm'));
    public function testRouteProfile()
    {
        $user = new User(['id'=>1, 'username' => 'slowpog']);
        $this->be($user);
        $this->tryRoute('/profile');
    }
    //Route::post('/profile', 				array('before' => 'auth|csrf', 	'uses' => 'LoginController@updateProfile'));
    public function testRouteProfilePost()
    {
        $user = new User(['id'=>1, 'username' => 'slowpog']);
        $this->be($user);
        //$this->tryRoute('/profile', 'POST');
        $response = $this->call('POST', '/profile', []);
        //$this->assertResponseStatus(200);
        $this->assertSessionHasErrors();
    }
    //Route::get('/forgotten',		'LoginController@forgottenForm');
    public function testRouteForgotten()
    {
        $this->tryRoute('/forgotten');
    }
    //Route::post('/forgotten',		'LoginController@createReset');
    public function testRouteForgottenPost()
    {
        Session::set('captcha', 'asdfg');
        $response = $this->call('POST', '/forgotten', ['captcha'=>'asdfg', 'username'=>'slowpog']);
    }
    //Route::get('/reset/{token?}',  	'LoginController@resetForm');
    public function testRouteReset()
    {
        $this->tryRoute('/reset/3cc0f5e6a1ccb1f4ffbf68d658f349bb1f19f2fb');
    }
    //Route::post('/reset/{token?}', 	'LoginController@doReset');
    public function testRouteResetPost()
    {
        Session::set('captcha', 'asdfg');
        $crawler = $this->client->request('POST', '/reset/3cc0f5e6a1ccb1f4ffbf68d658f349bb1f19f2fb', ['username'=>'slowpog', 'captcha'=>'asdfg', 'password'=>'']);
        $this->assertSessionHas('error');
    }
    //Route::any('/{locale}/{idslug}', 'Dispatcher@changeLang')->where(array('locale'=>'[a-z]{2}', 'idslug'=>'[A-Za-z0-9_/\.\-\~]+'));
    public function testRouteLocale()
    {
        //$this->tryRoute('/en/979');
        $crawler = $this->client->request('GET', '/en/979');
        $this->assertRedirectedTo('/979');
        //$this->tryRoute('/de/979');
        $crawler = $this->client->request('GET', '/de/979');
        $this->assertRedirectedTo('/979');
    }
    //Route::delete('/mydownloads', 			array('before' => 'auth|csrf', 	'uses' => 'MyDownloadsPageController@removeDownloads'));
    //Route::put('/mydownloads/add/{id}', 	array('before' => 'auth|csrf', 	'uses' => 'MyDownloadsPageController@addDownload'))->where('id', '[0-9]+');
    //Route::post('/mydownloads', 			array('before' => 'auth|csrf', 	'uses' => 'MyDownloadsPageController@startDownload'));
    //Route::get('/mydownloads', 				array('before' => 'auth', 		'uses' => 'MyDownloadsPageController@listDownloads'));
    public function testRouteListAddDeleteMydownload()
    {
        //Session::start();
        $user = new User(['id'=>1, 'username' => 'slowpog']);
        $this->be($user);
        $this->tryRoute('/mydownloads', 'GET');
        //$this->tryRoute('/mydownloads/add/2015', 'PUT', array('csrf_token' => csrf_token()));
        $crawler = $this->client->request('POST', '/mydownloads/add/2015', ['_method'=>'PUT', 'fid'=>2015, 'csrf_token' => csrf_token()]);
        $this->assertRedirectedTo('/mydownloads');
        $this->assertSessionHas('myDownloadsCount');
        $crawler = $this->client->request('POST', '/mydownloads', ['_method'=>'DELETE', 'fid'=>2015, 'csrf_token' => csrf_token()]);
        $this->assertRedirectedTo('/mydownloads');
        $this->assertSessionHas('myDownloadsCount');
    }

    //Route::post('/download', 				array('before' => 'csrf', 	'uses' => 'MyDownloadsPageController@startDownload'));
    public function testRouteDownloadPost()
    {
        $crawler = $this->client->request('POST', '/download', ['fileid'=>2015, 'csrf_token'=>csrf_token()]);
        //$crawler->
        $this->assertResponseStatus(200);
    }
    //Route::post('/gallery',					array('before' => 'csrf', 'uses' => 'FileController@galleryData'));
    public function testRouteGallery()
    {
        $this->tryRoute('/gallery', 'POST', ['files'=>[9216,9472], 'csrf_token'=>csrf_token()]);
    }
    //Route::post('/print/gallery',			array('before' => 'csrf', 'uses' => 'FileController@galleryData'));
    public function testRoutePrintGallery()
    {
        $this->tryRoute('/print/gallery', 'POST', ['files'=>[1,2,3,4,9216,9472], 'csrf_token'=>csrf_token()]);
    }
    //Route::get('/watchlist', 				array('before' => 'auth', 		'uses' => 'WatchlistPageController@index'));
    //Route::put('/watchlist',	 			array('before' => 'auth|csrf', 	'uses' => 'WatchlistPageController@store'));
    //Route::delete('/watchlist', 			array('before' => 'auth|csrf', 	'uses' => 'WatchlistPageController@destroy'));
    public function testRouteWatchlist()
    {
        $user = new User(['id'=>1, 'username' => 'slowpog']);
        $this->be($user);
        $this->tryRoute('/Watchlist', 'GET');
        $crawler = $this->client->request('PUT', '/Watchlist', ['pageid'=>979, 'csrf_token' => csrf_token()]);
        $this->assertSessionHas('watchlistCount');
        $crawler = $this->client->request('POST', '/Watchlist', ['_method'=>'DELETE', 'pageid'=>979, 'csrf_token' => csrf_token()]);
        $this->assertRedirectedTo('/Watchlist');
    }
    //Route::controller('pages', 'PageController'); path: '/api/v1/pages/sendalert/{id}'; method: PageController@putSendAlert()
    public function testRouteSendAlerts()
    {
        $this->tryRoute('/api/v1/pages/sendalert/979', 'PUT');
    }
    //Route::put('/rating/add',	 			array('before' => 'auth|csrf', 	'uses' => 'RatingPageController@store'));
    public function testRouteAddRatingPut()
    {
        $user = new User(['id'=>1, 'username' => 'slowpog']);
        $this->be($user);
        $this->tryRoute('/rating/add', 'PUT', ['pageid'=>'979', 'vote'=>9]);
    }
    //Route::get('/dialog', 					array('before' => '', 			'uses' => 'DialogPageController@index'));
    public function testRouteDialog()
    {
        $this->tryRoute('/dialog', 'GET');
    }
    //Route::post('/dialog', 					array('before' => 'csrf', 		'uses' => 'DialogPageController@send'));
    public function testRouteDialogPOST()
    {
        Session::set('captcha', 'asdfg');
        $crawler = $this->client->request('POST', '/dialog', ['email'=>'slowpog@gmail.com', 'captcha'=>'asdfg', 'messageText'=>'test dialog message']);
        $this->assertCount(1, $crawler->filter('div#main:contains("Vielen Dank!")'));
    }
    //Route::get('/feedback/{idslug}', 		array('before' => '', 			'uses' => 'FeedbackPageController@index'))->where('idslug', '[A-Za-z0-9_\.\-\~]+');
    public function testRouteFeedback()
    {
        $this->tryRoute('/feedback/979', 'GET');
    }
    //Route::post('/feedback/{idslug}', 		array('before' => 'csrf', 		'uses' => 'FeedbackPageController@send'))->where('idslug', '[A-Za-z0-9_\.\-\~]+');
    public function testRouteFeedbackPost()
    {
        Session::set('captcha', 'asdfg');
        $crawler = $this->client->request('POST', '/feedback/979', ['email'=>'slowpog@gmail.com', 'captcha'=>'asdfg', 'comments'=>'test comments']);
        $this->assertCount(1, $crawler->filter('span:contains("Ihre Nachricht wurde erfolgreich versendet.")'));
    }
//
    //Route::post('/comments/{idslug}', 		array('before' => 'auth|csrf', 	'uses' => 'CommentsPageController@store'))->where('idslug', '[A-Za-z0-9_\.\-\~]+');
    public function testRouteCommentsPost()
    {
        $user = new User(['id'=>1, 'username' => 'slowpog']);
        $this->be($user);
        $crawler = $this->client->request('POST', '/comments/979', ['comment_agree'=>'0', 'headline'=>'test headline', 'comments'=>'test comments']);
        $this->assertSessionHas('errors');
    }
//
    //Route::get('/sitemap', 					array('before' => '', 			'uses' => 'SitemapPageController@sitemap'));
    public function testRouteSitemap()
    {
        $this->tryRoute('/sitemap');
    }
    //Route::get('/index/{letter?}', 			array('before' => '', 			'uses' => 'SitemapPageController@index'))->where('letter', '[A-Za-z]{1}|&[a-zA-Z]{2,4};');
    public function testRouteIndex()
    {
        $this->tryRoute('/index/a');
    }
    //Route::any('/search/{qparam?}', 		array('before' => '',			'uses' => 'SearchPageController@showResults'));
    public function testRouteSearch()
    {
        $crawler = $this->client->request('POST', '/search', ['search'=>'Anbieter']);
        $this->assertResponseStatus(200);
        $this->assertCount(3, $crawler->filter('a:contains("Anbieter")'));
    }
    //Route::any('/glossary/{letter?}', 			'GlossaryPageController@index')->where('letter', '[A-Za-z]{1}');
    public function testRouteGlossary()
    {
        $this->tryRoute('/glossary/a');
    }
    //Route::any('/glossary/{id?}', 				'GlossaryPageController@index')->where('id', '[0-9]+');
    public function testRouteGlossaryID()
    {
        $this->tryRoute('/glossary/144');
    }
    //Route::any('/print/glossary/{idletter?}', 	'GlossaryPageController@forPrint')->where('idletter', '[A-Za-z_]{1}|[0-9]+');
    public function testRouteGlossaryPrint()
    {
        $this->tryRoute('/print/glossary/a');
    }
//
    //Route::any('/preview/{id}', 	'Dispatcher@getPreview')->where('id', '[0-9]+');
    public function testPreview979()
    {
        $user = new User(['id'=>1, 'username' => 'slowpog', 'role'=>3]);
        $this->be($user);
        $crawler = $this->client->request('POST', '/preview/979', ['main'=>'lorem ipsum', 'banner'=>'banner test', 'right'=>'', 'farRight'=>'']);
        $this->assertResponseStatus(200);
        $this->assertCount(1, $crawler->filter('div#main:contains("lorem ipsum")'));
    }
    //Route::any('/print/{id}', 		'Dispatcher@getPrintById')->where('id', '[0-9]+');
    //Route::any('/print/{slug}', 	'Dispatcher@getPrintBySlug')->where('slug', '[A-Za-z0-9_\.\-\~]+');
    public function testPrint()
    {
        $crawler = $this->client->request('GET', '/print/979');
        $this->assertResponseStatus(200);
        $this->assertCount(1, $crawler->filter('h1:contains("Daimler")'));
        $crawler = $this->client->request('GET', '/print/daimler');
        $this->assertResponseStatus(200);
        $this->assertCount(1, $crawler->filter('h1:contains("Daimler")'));
    }
    // tested in DispatcherTest.php
    //Route::any('/home', 			'Dispatcher@getHomepage');
    //Route::any('/1',	 			'Dispatcher@getHomepage');

    //Route::any('/external', 	'Dispatcher@externalPage');
    public function testRouteExternal()
    {
        $this->tryRoute('/external');
        $crawler = $this->client->request('POST', '/external', ['url'=>'http://testingurl.com']);
        $this->assertResponseStatus(200);
        $this->assertCount(1, $crawler->filter('a:contains("testingurl.com")'));
    }
    // tested in DispatcherTest.php
    //Route::any('/{id}', 			'Dispatcher@getPageById')->where('id','[0-9]+');
    //Route::any('/{slug}', 			'Dispatcher@getPageBySlug')->where('slug', '[A-Za-z0-9_\.\-\~]+');
    //Route::any('/', 				'Dispatcher@index');
}

let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.options({
              processCssUrls: false
           })
    .sass('resources/assets/sass/style.scss', 'public/css/desktop.css')           
    .sass('resources/assets/sass/handheld-devices.scss', 'public/css/handheld-devices.css')
    .styles([
        'resources/assets/css/le.css',
        'resources/assets/css/right_navigation.css',
        'resources/assets/css/lytebox.css',
        'resources/assets/css/smoothness/jquery-ui-1.10.4.custom.css'
    ], 'public/css/all-common.css')

    // Includes styles only for desktop
    .styles([
        'public/css/desktop.css',
        'public/css/all-common.css'
    ], 'public/css/all-desktop.css')
    // Includes styles for Desktop, Tablet, Mobile
    .styles([
        'public/css/handheld-devices.css',
        'public/css/all-common.css'
    ], 'public/css/all-hhd.css')
    .styles([
        'public/css/desktop.css',
        'resources/assets/css/le.css',
        'resources/assets/css/print.css'
    ], 'public/css/all.print.css')
    .styles([
        'public/css/desktop.css',
        'resources/assets/css/le.css',
        'resources/assets/css/lytebox.css'
    ], 'public/css/all.explainer.css')
    .copy('resources/assets/css/styleSmart.css', 'public/css')
    .copy('resources/assets/css/tabletSmart.css', 'public/css')
    .copy('resources/assets/css/phoneSmart.css', 'public/css')
    .copy('resources/assets/css/styleSmartPrint.css', 'public/css')
    .copy('resources/assets/css/pdf.css', 'public/css')
    .copy('resources/assets/css/pdfSmart.css', 'public/css')
    .copy('resources/assets/js/joyride-2.1/joyride-2.1.css', 'public/css/joyride')
    .copy('resources/assets/js/chosen_v1.7.0/chosen.css', 'public/css/chosen')

    //Backend css
    .copy('resources/assets/css/backend', 'public/css')
    .copy('resources/assets/css/smoothness', 'public/css/smoothness')

    //scripts
    .scripts([
        'resources/assets/js/es-shim.min.js',
        'resources/assets/js/jquery-1.11.1.min.js',
        'resources/assets/js/onload.js',
        'resources/assets/js/slider.js',
        'resources/assets/js/jquery.zoom.min.js',
        'resources/assets/js/jquery_lazyload_1.9.7/jquery.lazyload.js',
        'resources/assets/js/lytebox.js',
        'resources/assets/js/jquery.scrollbar.js',
        'resources/assets/js/public.js',
        'resources/assets/js/jquery-ui.min.js',
        'resources/assets/js/datepicker-de.js',
        'resources/assets/js/jquery.collapse.js',
        'resources/assets/js/jquery.event.move.js',
        'resources/assets/js/jquery.event.swipe.js',
        'resources/assets/js/modernizr.js',
        'resources/assets/js/joyride-2.1/jquery.joyride-2.1.js',
        'resources/assets/js/guided_tour.js',
        'resources/assets/js/sly.min.js',
        'resources/assets/js/sly.js',
        'resources/assets/js/helpers.js',
    ], 'public/js/all.js')
    .scripts([
        'resources/assets/js/sly.min.js',
        'resources/assets/js/sly.js',
        'resources/assets/js/slider.js'
    ], 'public/js/all.slider.js')
    .scripts([
        'resources/assets/js/es-shim.min.js',
        'resources/assets/js/jquery-1.11.1.min.js',
        'resources/assets/js/onload.js',
        'resources/assets/js/jquery.scrollbar.js',
        'resources/assets/js/public.js',
        'resources/assets/js/jquery.zoom.min.js',
        'resources/assets/js/jquery.event.move.js',
        'resources/assets/js/jquery.event.swipe.js',
        'resources/assets/js/jquery-ui.min.js',
        'resources/assets/js/jquery.collapse.js',
        'resources/assets/js/modernizr.js',
        'resources/assets/js/helpers.js',
    ], 'public/js/all.fatal.js')
    .scripts([
        'resources/assets/js/jquery-1.11.1.min.js',
        'resources/assets/js/jquery-ui.min.js',
        'resources/assets/js/jquery.scrollbar.js',
        'resources/assets/js/le.js'
    ], 'public/js/all.explainer.js')
    .scripts([
        'resources/assets/js/jquery-1.11.1.min.js',
        'resources/assets/js/print.js',
    ], 'public/js/all.print.js')
    .copy('resources/assets/js/downloads_filter.js', 'public/js')
    .copy('resources/assets/js/header_load.js', 'public/js')
    .copy('resources/assets/js/faqs_list.js', 'public/js')
    .copy('resources/assets/js/selectivizr-min.js', 'public/js')
    .copy('resources/assets/js/classie.js', 'public/js')
    .copy('resources/assets/js/jquery-ui-1.10.2.custom.min.js', 'public/js')
    .copy('resources/assets/js/jquery.simplePagination.js', 'public/js')
    .copy('resources/assets/js/guided_tour.js', 'public/js')
    .copy('resources/assets/js/chosen_v1.7.0/chosen.jquery.js', 'public/js/chosen')
    .copy('resources/assets/js/chosen_v1.7.0/chosen.proto.js', 'public/js/chosen')

    //Backend js
    .copy('resources/assets/js/backend', 'public/js')
    .copy('resources/assets/js/jquery-1.11.1.min.js', 'public/js')
    .copy('resources/assets/js/jquery-ui.min.js', 'public/js');

    mix.version();

    if (mix.inProduction()) {
    }else{
        mix.sourceMaps();
    }
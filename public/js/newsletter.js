var oTable;
var filtered_table = false;
var move_item = 0;

//var relPath = getRelativePath();

$(document).ready(function() {

		oTable = $('#newsletters').dataTable({
			"bProcessing": true,
			"sAjaxSource": relPath + "api/v1/newsletter/all?lang=" + lang,
			"sAjaxDataProp": 'response',
			"fnServerData": fnServerData,
			"iDisplayLength": 45,
			"bFilter": true,
			"sPaginationType": 'ellipses',
			"bLengthChange": false,
			"iShowPages": 10,
			"bSort": false,
			"bDeferRender": true,
			"oLanguage": {
				"sEmptyTable": " "
			},
			"aoColumns": [
				{"sType": "page-title", "mData": "title", "sWidth": "440px", "sClass": "strong", "bSearchable": true},
				{"sType": "numeric", "mData": "id", "sWidth": "40px", "bSearchable": false, "sClass": "newsletter_id"},
                {"sType": "string", "mData": function(source) {
						if (source.active == 1) {
							var status = js_localize['pages.label.active'];
							return status+'<br />('+source.editing_state+')';
						} else {
							return js_localize['pages.label.inactive']+'<br />('+source.editing_state+')';
						}
					}, "sWidth": "80px", "bSearchable": false},
                {"sType": "string", "mData": function(source) {
						return source.status + ' ' + source.sent_emails + '/' + source.all_subscribers;
					},"sWidth": "110px", "bSearchable": true},
				{"sType": "string", "mData": "updated_at", "sWidth": "130px", "bSearchable": false},
			]
		});



	var oSettings = oTable.fnSettings();
	oSettings.sAlertTitle = js_localize['newsletter.list.title'];
	// Hide processing indicator for the first loading
	//oTable.oApi._fnProcessingDisplay(oSettings, false);

	$("#newsletters tbody").click(function(event) {
		if (!$(event.target).hasClass('dataTables_empty')) {
			closeRowEditor();
			$(oTable.fnSettings().aoData).each(function() {
				$(this.nTr).removeClass('row_selected');
			});

			$(event.target.parentNode).addClass('row_selected');
            var dataSelected = oTable.fnGetData(event.target.parentNode);
            if(dataSelected.active == 1 && dataSelected.editing_state == 'Approved'){
               $('.user_edit_wrapper .package, .user_edit_wrapper .send').removeClass('inactiveButton');
               if(dataSelected.status == "Sent")
                 $('.user_edit_wrapper .send').addClass('inactiveButton');
            }else{
                $('.user_edit_wrapper .package, .user_edit_wrapper .send').addClass('inactiveButton');
            }

        }
        $('.user_edit_wrapper').css('top', $('#newsletters_wrapper').position().top + $(event.target.parentNode).position().top + $(event.target.parentNode).height() + 1);
		$('.user_edit_wrapper').show();
	});

	$('body').click(function(event) {
		if ($(event.target).parents('tbody').length == 0 && !($(event.target).hasClass('paginate_button'))&& $(event.target).parents('.user_edit_wrapper').length == 0 && $('#popup_overlay').length == 0 && $("#ui-datepicker-div:hover").length == 0) {
			//alert($(event.target).parents('#ui-datepicker-div').length);
			closeRowEditor();
		}
	});

	bindRowEditButtons();
});

function bindRowEditButtons() {
	/* Add a click handler for the delete row */
	$('.user_actions .nav_box.remove').click(function() {
		var anSelected = fnGetSelected(oTable);

//			if($(anSelected).find('.publish_unpublish').html().indexOf(js_localize['newsletter.label.active']) != -1){
//				jAlert(js_localize['newsletter.remove.error'], js_localize['newsletter.remove.title'], "error");
//				return false;
//			}
			jConfirm($(this).attr('confirm'), js_localize['newsletter.remove.title'], "success", function(r) {
				closeRowEditor();
				if (r) {
					if (anSelected.length !== 0) {
						var oSettings = oTable.fnSettings();
                            oTable.oApi._fnProcessingDisplay(oSettings, true);
						jQuery.ajax({
							type: "DELETE",
							url: relPath + "api/v1/newsletter/delete/" + $(anSelected[0]).find('.newsletter_id').html(),
							success: function(data) {
								oTable.fnDeleteRow(anSelected[0]);
								jAlert(js_localize['newsletter.remove.text'], js_localize['newsletter.remove.title'], "success");
							},
							error: function(XMLHttpRequest, textStatus, errorThrown) {
								var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
								var message = JSON_ERROR.composeMessageFromJSON(data, true);
								jAlert(message, js_localize['newsletter.remove.title'], "error");
							},
							complete: function() {
                                    oTable.oApi._fnProcessingDisplay(oSettings, false);
                                }
						});
					}
				}
			});

	});

	/* Add a click handler for the delete row */
	$('.user_actions .nav_box.edit').click(function() {
        var anSelected = fnGetSelected(oTable);
        var newsletter_id = $(anSelected[0]).find('.newsletter_id').html();
        if (anSelected.length !== 0) {
            checkNewsletterAvailability(newsletter_id, function () {
                window.location.href = relPath + 'cms/newsletter/' + newsletter_id;
            });
        }
    });

	$('.user_actions .nav_box.properties').click(function () {
        var self = this;
        var anSelected = fnGetSelected(oTable);
        if (anSelected.length !== 0) {
            $('body').css('cursor', 'progress');
            var newsletter_id = $(anSelected[0]).find('.newsletter_id').html();
            checkNewsletterAvailability(newsletter_id, function (time_left) {
				editing_timer.start(
                    time_left,
                    function(){
                        $('.user_fields .nav_box.save').click();
                    },
                    function(){
                        var promise = cancelChanges();
                        promise.done(function(){
                            window.location.href = relPath + 'cms/newsletter';
                        });
                    },
                    newsletter_id,
                    'newsletter',
                    '',
                    EDIT_MAX_TIME_SECONDS
                );
                $('a').css('cursor', 'progress');
                $(this).addClass('active');
                clearPropertiesForm();
                populateFormNewsletter(newsletter_id, function (has_errors) {
                    if (!has_errors) {
                        openPropertiesForm();
                    } else {
                        $(self).removeClass('active');
                    }
                    $('body').css('cursor', 'default');
                    $('a').css('cursor', 'pointer');

					if(time_left > EDIT_TIME_EXTENSION_ALERT_SECONDS){
						jAlert(
                            js_localize['newsletter.edit.start']
                                .replace('{time_left}', editing_timer.formatTimeInMinutes(time_left))
                                .replace('{alert_minutes}', editing_timer.formatTimeInMinutes(EDIT_TIME_EXTENSION_ALERT_SECONDS)),
                            js_localize['newsletter.edit.title'],
                            'success',
                            change_workflow_responsible
                        );
                    }
                });

            });
        }
    });

	$('.user_actions .nav_box.add_newsletter').click(function() {
		var selected = fnGetSelected(oTable)[0];
		clearPropertiesForm();
		openPropertiesForm();
		$(this).addClass('active');

	});

	$('.user_actions .nav_box.copy').click(function(e) {
		var selected = fnGetSelected(oTable)[0];
		var move_info = {'id': $(selected).find('.newsletter_id').html(), action: 'copy'};
		url = relPath + "api/v1/newsletter/copy";
		type = "POST";
		jQuery.ajax({
			type: type,
			url: url,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(move_info),
			success: function(data) {
				var page_info = jQuery.extend(data.response, {'title': $(selected).find('.strong').html()});
				oTable.fnAddData(page_info);
				jAlert(js_localize['newsletter.edit.copy_success'], js_localize['newsletter.copy.title'], "success");
			},
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
				var message = JSON_ERROR.composeMessageFromJSON(data);
				if (data.response !== undefined) {
					$.each(data.response, function(key, value) {
						$('input[name="' + key + '"]').parent().addClass('error');
					});
				}
				$('body').css('cursor', 'default');
				//alert("Error while updating group!");
				jAlert(message, js_localize['newsletter.edit.title'], "error");
			},
		});

	});

	$('.user_fields .nav_box.cancel').click(function() {
		cancelChanges();
	});

	$('.user_fields .nav_box.save').click(function() {
		$('body').css('cursor', 'progress');
		var anSelected = fnGetSelected(oTable);
		var newsletterID = $('#newsletter_form input[name="id"]').val();
		if (newsletterID != "" && newsletterID > 0) {
			var promise = saveNewsletterProperties(updateTableRow, "PUT", anSelected);
            promise.done(function(){
                editing_timer.stopTimer(false);
                unlockItem(newsletterID, 'newsletter');
            });
		} else {
			saveNewsletterProperties(addNewTableRow, "POST", anSelected);
		}
	});

	$('.user_actions .nav_box.publish').click(function() {
		$('body').css('cursor', 'progress');
		var anSelected = fnGetSelected(oTable);
		var newsletterID = $(anSelected).find('.newsletter_id').html();
		var data = {'id': newsletterID, 'active': 1};
		activate_deactivate(data, anSelected);
	});

	$('.user_actions .nav_box.unpublish').click(function() {
		$('body').css('cursor', 'progress');
		var anSelected = fnGetSelected(oTable);
		var newsletterID = $(anSelected).find('.newsletter_id').html();
		var data = {'id': newsletterID, 'active': 0};
		activate_deactivate(data, anSelected);
	});

	$('.user_actions .nav_box.send').click(function() {
        var anSelected = fnGetSelected(oTable);
        if (anSelected.length !== 0) {
            var dataSelected = oTable.fnGetData(anSelected[0]);
            //if(dataSelected.active == 1 && dataSelected.editing_state == 'Approved'){
                var info = {'id': dataSelected.id};
                jConfirm(js_localize['newsletter.send.confirm'], js_localize['newsletter.send.title'],"success", function(r) {
                    if (r) {
                        jAlert(js_localize['newsletter.send.wait'], js_localize['newsletter.send.title'], 'success', null);
                        $('#popup_close').hide();
                        $('#popup_panel').hide();
                        jQuery.ajax({
                            type: "POST",
                            url: relPath + "api/v1/newsletter/send",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: JSON.stringify(info),
                            success: function(data) {
                                dataSelected.status = data.response.status;
                                $('.user_actions send').addClass('inactiveButton');
                                $('body').css('cursor', 'default');
                                var td = $('#newsletters tr:eq(' + (anSelected.index()+1) + ') td:eq(3)');
                                td.html(dataSelected.status);
                                if(dataSelected.status == 'Sent'){
                                     $('.user_edit_wrapper .send').addClass('inactiveButton');
                                }
                                var msg = js_localize['newsletter.send.success'];
                                if(data.message != "")
                                    msg = data.message;
                                jAlert(msg, js_localize['newsletter.send.title'], "success");
                            },
                            // script call was *not* successful
                            error: function(XMLHttpRequest, textStatus, errorThrown) {
                                var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                                var message = JSON_ERROR.composeMessageFromJSON(data);
                                $('body').css('cursor', 'default');
                                //alert("Error while updating group!");
                                jAlert(message, js_localize['newsletter.send.title'], "error");
                            },
                        });
            //}
                    }
                });
        }
    });

    $('.user_actions .nav_box.package').click(function() {
        var anSelected = fnGetSelected(oTable);
        if (anSelected.length !== 0) {
            var dataSelected = oTable.fnGetData(anSelected[0]),
                url = relPath + "api/v1/newsletter/package/"+dataSelected.id;

            if(dataSelected.active == 1 && dataSelected.editing_state == 'Approved'){

                window.open(url,'_self',false); // <OR> window.location = url;

                /*
                jQuery.ajax({
                    type: "GET",
                    url: relPath + "api/v1/newsletter/package/"+dataSelected.id,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {
                        $('body').css('cursor', 'default');
                        jAlert(js_localize['newsletter.send.success'], js_localize['newsletter.send.title'], "success");
                    },
                    // script call was *not* successful
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                        var message = JSON_ERROR.composeMessageFromJSON(data);
                        $('body').css('cursor', 'default');
                        //alert("Error while updating group!");
                        jAlert(message, js_localize['newsletter.send.title'], "error");
                    },
                });
                //*/
            }
        }
    });

    $('.user_actions .nav_box.test').click(function(){
        var anSelected = fnGetSelected(oTable);
        if (anSelected.length !== 0) {
            var newsletter_id = $(anSelected[0]).find('.newsletter_id').html();
            $('#test_email_wrapper input[name="id"]').val(newsletter_id);
            $('#test_email_wrapper textarea').val('');
            $('.textarea_overlay').show();
            $('#test_email_wrapper').show();
        }
    });

    $('#test_newsletter_form .cancel').click(function(){
        $('.textarea_overlay').hide();
        $('#test_email_wrapper').hide();
    });

    $('#test_newsletter_form .test').click(function(){
        $('body').css('cursor', 'progress');
		var isReceiversEmails = ($("#test_newsletter_form input[name='receivers']:checked").val() == 'true');
		// Validate input
        if(isReceiversEmails && $('#test_newsletter_form textarea').val() == ""){
            jAlert("There are no emails written", js_localize['newsletter.send.title'], "error");
            $('body').css('cursor', 'default');
            return;
        }
        var info = {
			'id': $("#test_newsletter_form input[name='id']").val(), 
			'emails': $("#test_newsletter_form textarea[name='test_emails']").val(), 
		};
		if (!isReceiversEmails) {
			info.test_list = true;
		}
        jConfirm(js_localize['newsletter.send.test.confirm'], js_localize['newsletter.send.title'],"success", function(r) {
            if (r) {
                jQuery.ajax({
                    type: "POST",
                    url: relPath + "api/v1/newsletter/sendtest",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(info),
                    success: function(data) {
                        if(data.message && data.message.length > 1)
                             jAlert(data.message, js_localize['newsletter.send.title'], "success");
                         else
                            jAlert(js_localize['newsletter.send.success'], js_localize['newsletter.send.title'], "success");
                        $('.textarea_overlay').hide();
                        $('#test_email_wrapper').hide();
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                        var message = JSON_ERROR.composeMessageFromJSON(data);
                        $('body').css('cursor', 'default');
                        jAlert(message, js_localize['newsletter.send.title'], "error");
                    },
                    complete: function(){
                        $('body').css('cursor', 'default');
                    }
                });
            }else{
                $('body').css('cursor', 'default');
            }
        });
    });


	function activate_deactivate(data, anSelected){
		jQuery.ajax({
			type: "PUT",
			url: relPath + "api/v1/newsletter/update",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(data),
			success: function(returnData) {
				if(data['active'] == 1){
					if(returnData.response['isNotActive'] == 1)
						$(anSelected).find('.publish_unpublish').html(
							js_localize['newsletter.label.active'] + '<span style="font-size: 18px;">*</span><br />('+returnData.response.workflow.editing_state+')');
					else
						$(anSelected).find('.publish_unpublish').html(js_localize['newsletter.label.active']+'<br />('+returnData.response.workflow.editing_state+')');

					jAlert(js_localize['newsletter.edit.activate_success'], js_localize['newsletter.edit.title'], "success");
				} else {
					$(anSelected).find('.publish_unpublish').html(js_localize['newsletter.label.inactive']+'<br />('+returnData.response.workflow.editing_state+')');
					jAlert(js_localize['newsletter.edit.deactivate_success'], js_localize['newsletter.edit.title'], "success");
				}
				active_inactive();
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var returnData = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
				var message = JSON_ERROR.composeMessageFromJSON(returnData, true);
				if (returnData.response !== undefined) {
					$.each(returnData.response, function(key, value) {
						$('input[name="' + key + '"]').parent().addClass('error');
					});
				}
				jAlert(message, js_localize['newsletter.edit.title'], "error");
			},
			complete: function() {
				$('body').css('cursor', 'default');
                   closeRowEditor();
                }
		});
	}
}

function checkNewsletterAvailability(newsletter_id, callback) {

    jQuery.ajax({
        type: "GET",
        url: relPath + "api/v1/newsletter/isavailable/" + newsletter_id,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (typeof (callback) === "function") {
                var time_left = 0;
                if(data.response.time_left){
                    time_left = data.response.time_left;
                }
                callback(time_left);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
            var message = JSON_ERROR.composeMessageFromJSON(data, true);
            jAlert(message, js_localize['newsletter.edit.title'], "error");
            $('body').css('cursor', 'default');
        }
    });
}

function bindClicks(selected, action) {
	$(document).bind('click', function(e) {
		if ($(selected).length > 0) {
			var elem = e.target;
			if ($(elem).prop('tagName') == "SPAN" || $(elem).prop('tagName') == "A") {
				elem = $(elem).parent();
			}
			if ($(elem).parent().prop("tagName") == "TR" && $(elem).prop('tagName') != "TH") {
				e.preventDefault();
				e.stopPropagation();
				var row = $(elem).parent();
				if (action == 'copy' || !$(elem).parent().hasClass('disabled')) {
					var move_info = {'id': move_item, action: action};
					var url = relPath + "api/v1/newsletter/update";
					var type = "PUT";
					if (action == 'copy') {
						url = relPath + "api/v1/newsletter/copy";
						type = "POST";
					}
					jQuery.ajax({
						type: type,
						url: url,
						contentType: "application/json; charset=utf-8",
						dataType: "json",
						data: JSON.stringify(move_info),
						success: function(data) {
							stopMove(selected);
							var page_info = jQuery.extend(data.response, {'title': $(selected).find('.strong').html()});
							oTable.fnAddData(page_info);
							jAlert(js_localize['newsletter.edit.copy_success'], js_localize['newsletter.copy.title'], "success");
						},
						// script call was *not* successful
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							stopMove(selected);
							var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
							var message = JSON_ERROR.composeMessageFromJSON(data);
							if (data.response !== undefined) {
								$.each(data.response, function(key, value) {
									$('input[name="' + key + '"]').parent().addClass('error');
								});
							}
							$('body').css('cursor', 'default');
							//alert("Error while updating group!");
							jAlert(message, js_localize['newsletter.edit.title'], "error");
						},
					});
				} else {
					jAlert(js_localize['newsletter.move.error_same'], js_localize['newsletter.move.title'], "error");
				}
			}
		}
	});
}

function closeRowEditor() {
	$('#publish_date').datepicker('hide');
	$('#unpublish_date').datepicker('hide');
	$('.user_table').css('height', '');
	$('.dataTables_paginate.paging_ellipses').show();
	$('#user_form .input_group').removeClass('error');
	$('#user_form .role_group').removeClass('error');
	$('.user_actions .nav_box').removeClass('active');
	$('.user_edit_wrapper .user_fields').hide();
	$('#user_form input:text').val('');
	$('#user_form input[name="id"]').val('0');
	$(oTable.fnSettings().aoData).each(function() {
		$(this.nTr).removeClass('row_selected');
	});
	$('.user_edit_wrapper').hide();
	$('.textarea_overlay').hide();
	$('body').css('cursor', 'default');
}

/* Get the rows which are currently selected */
function fnGetSelected(oTableLocal) {
	return oTableLocal.$('tr.row_selected');
}

function refreshTable(event) {
	event.stopPropagation();
	var search_input = $('.user_search').val() !== js_localize['newsletter.label.search'] ? $('.user_search').val() : "";
	var sort_input = $('.user_sort').val();
	if (search_input != "") {
		oTable.fnReloadAjax(relPath + "api/v1/newsletter/search?lang=" + lang + "&filter=" + encodeURIComponent(search_input), function() {
			if (sort_input == -1) {

				oTable.fnSortNeutral();
			} else {
				oTable.fnSort([[sort_input, 'asc']]);
			}
		});
		filtered_table = true;
	} else {
		if (filtered_table) {
			oTable.fnReloadAjax(relPath + "api/v1/newsletter/all?lang=" + lang + "&depth=2&parent_id=0", function() {
				console.log(sort_input);
				if (sort_input == -1) {
					oTable.fnSortNeutral();
				} else {
					oTable.fnSort([[sort_input, 'asc']]);
				}
			});
		} else {
			if (sort_input == -1) {
				oTable.fnSortNeutral();
			} else {
				oTable.fnSort([[sort_input, 'asc']]);
			}
		}
		filtered_table = false;
	}
	return false;
}

function hideTitle(el, lang) {
	if ($(el).prop('checked') == true) {
		$('input[name="title_' + lang + '"]').parent().show();
	} else {
		$('input[name="title_' + lang + '"]').parent().hide();
	}
}

function updateTableRow(data, anSelected) {
	$('#wrapper_form_newsletter').closeDialog();
	var page_info = jQuery.extend(data.response, {'title': $('input[name="title_' + lang_label + '"]').val()});
	oTable.fnUpdate(page_info, anSelected[0], undefined, false, false);
	closeRowEditor();
}

function addNewTableRow(data, anSelected) {
	$('#wrapper_form_newsletter').closeDialog();
	if ($('input[name="title_' + lang_label + '"]').val() != '') {
		var page_info = jQuery.extend(data.response, {'title': $('input[name="title_' + lang_label + '"]').val()});
		oTable.fnAddData(page_info);
	}
	closeRowEditor();
}

function openPropertiesForm() {
	$('#wrapper_form_newsletter').openDialog($('div.user_edit_wrapper .textarea_overlay'));

	var height = $('.user_edit_wrapper').position().top + $('.user_edit_wrapper').height() + $('.user_fields').height() + 10;
	if ($('.user_table').height() < height) {
		$('.user_table').css('height', height + 'px');
		$('.dataTables_paginate.paging_ellipses').hide();
	}
	$('.user_edit_wrapper .user_fields').show();
}

function cancelChanges() {
    var promise = null;
    var nlID = $('#user_form input[name="id"]').val();
    if (nlID != "" && nlID > 0) {
        promise = unlockItem(nlID, 'newsletter');
    } else {
        promise = $.Deferred().resolve().promise();
    }
    promise.always(function () {
        editing_timer.stopTimer(false);
        $('#wrapper_form_newsletter').closeDialog();
        $('.user_actions .nav_box').removeClass('active');
        $('.user_edit_wrapper .user_fields').hide();
        $('.user_table').css('height', '');
        $('.dataTables_paginate.paging_ellipses').show();
    });

    return promise;
}

var oTable;
//var relPath = getRelativePath();

$(document).ready(function() {

	oTable = $('#tags').dataTable({
		"bProcessing": false,
		"sAjaxSource": relPath+"api/v1/tag/all/"+lang,
		"sAjaxDataProp": 'response',
		"fnServerData": fnServerData,
		"iDisplayLength": 45,
		"bFilter": true,
		"sPaginationType": 'ellipses',
		"bLengthChange": false,
		"iShowPages": 10,
		"bSort": false,
		"bDeferRender": true,
		"oLanguage": {
			"sEmptyTable": " "},
		"aoColumns": [
			{"sType": "numeric", "mData": "id", "sWidth": "30px", "bSearchable": false, "sClass": "tag_id"},
			{"sType": "string", "mData": "name", "sWidth": "200px", "bSearchable": true},
			{"sType": "date-eu", "mData": "updated_at", "sWidth": "105px", "bSearchable": false}
		],
		"fnInitComplete": function(oSettings, json) {
//			if(username){
//				$('.user_search').val(username);
//				refreshTable();
//			}
		}
	});

	var oSettings = oTable.fnSettings();
	oSettings.sAlertTitle = js_localize['tags.list.title'];

	$("#tags tbody").click(function(event) {
			if (!$(event.target).hasClass('dataTables_empty')){
				closeProfile();
				$(oTable.fnSettings().aoData).each(function() {
					$(this.nTr).removeClass('row_selected');
				});
				$(event.target.parentNode).addClass('row_selected');

				if( !$('a.edit').is(":visible") ){
					$('a.edit').show();
					$('a.remove').show();
				}
			}else{
				$('a.edit').hide();
				$('a.remove').hide();
			}

			$('.user_edit_wrapper').css('top', $('#tags_wrapper').position().top + $(event.target.parentNode).position().top + $(event.target.parentNode).height() + 1);
			$('.user_edit_wrapper').show();
	});

	$('body').click(function(event) {
		if($(event.target).parents('tbody').length == 0 && $(event.target).parents('.user_edit_wrapper').length == 0 && $('#popup_overlay').length == 0){
			closeProfile();
		}
		if (event.target.parentNode) {
			if (!(event.target.parentNode.className == "admin_data" || event.target.parentNode.id == "admin_info" || event.target.parentNode.id == "admin_action")) {
				if ($('#admin_action').is(":visible"))
				{
					$("#admin_info").removeClass('active');
					$('#admin_action').hide();
				}
			}
		}
	});

	/* Add a click handler for the delete row */
	$('.user_actions .nav_box.remove').click(function() {
		jConfirm($(this).attr('confirm'), js_localize['tag.remove.title'],"success", function(r) {
			if (r) {
				var anSelected = fnGetSelected(oTable);
				closeProfile();
				if (anSelected.length !== 0) {
					jQuery.ajax({
						type: "DELETE",
						url: relPath+"api/v1/tag/delete/" + $(anSelected[0]).find('.tag_id').html(),
						success: function(data) {
							oTable.fnDeleteRow(anSelected[0]);
							jAlert(js_localize['tag.remove.success'].replace('{0}',data.response.name),js_localize['tag.remove.title'], "success");
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
							var message = JSON_ERROR.composeMessageFromJSON(data, true);
							jAlert(message, js_localize['tag.remove.title'], "error");
						},
					});
				}
			}
		});
	});

	$('.user_actions .nav_box.edit').click(function() {
		$('#wrapper_form_tags').openDialog($('div.user_edit_wrapper .textarea_overlay'));
		$('#tag_form .input_group').removeClass('error');
		$('body').css('cursor','progress');
		$('.user_actions .nav_box').removeClass('active');
		$(this).addClass('active');
		var anSelected = fnGetSelected(oTable);
		$('.user_edit_wrapper .user_fields').show();
		$('#tags_form input:checkbox:checked').removeAttr('checked');
		populateForm($(anSelected[0]).find('.tag_id').html());
		var height = $('.user_edit_wrapper').position().top + $('.user_edit_wrapper').height() + $('.user_fields').height() + 10;
		if($('.user_table').height() < height){
			$('.user_table').css('height', height + 'px');
			$('.dataTables_paginate.paging_ellipses').hide();
		}
	});

	$('.user_actions .nav_box.add').click(function() {
		$('#wrapper_form_tags').openDialog($('div.user_edit_wrapper .textarea_overlay'));
		$('#tag_form .input_group').removeClass('error');
		$('.user_actions .nav_box').removeClass('active');
		$(this).addClass('active');
		$('#tag_form input[name="id"]').val('0');
		$('#tag_form input:text').val('');
		$('#tag_form p.si_data').html('-');
		$('.user_edit_wrapper .user_fields').show();
		var height = $('.user_edit_wrapper').position().top + $('.user_edit_wrapper').height() +$('.user_fields').height() + 10;
		if($('.user_table').height() < height){
			$('.user_table').css('height', height + 'px');
			$('.dataTables_paginate.paging_ellipses').hide();
		}
	})


	$('.user_fields .nav_box.cancel').click(function() {
		$('#wrapper_form_tags').closeDialog();
		$('#tag_form .input_group').removeClass('error');
		$('.user_actions .nav_box').removeClass('active');
		$('.user_edit_wrapper .user_fields').hide();
		$('.user_table').css('height', '');
		$('.dataTables_paginate.paging_ellipses').show();
		closeProfile();
	});

	$('.user_fields .nav_box.save').click(function() {
		$('body').css('cursor','progress');
		var anSelected = fnGetSelected(oTable);

		if ($('#tag_form input[name="id"]').val() != "" && $('#tag_form input[name="id"]').val() > 0) {
			jQuery.ajax({
				type: "PUT",
				url: relPath+"api/v1/tag/update",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: parseForm(),
				// script call was *not* successful
				success: function(data) {
					$('#wrapper_form_tags').closeDialog();
					$('body').css('cursor','default');
					jAlert(data.message,js_localize['tag.edit.title'],"success")
					closeProfile();
					oTable.fnUpdate(data.response, anSelected[0]);
					$('.user_table').css('height', '');
					$('.dataTables_paginate.paging_ellipses').show();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
					var message = JSON_ERROR.composeMessageFromJSON(data, true);
					if(data.response !== undefined){
						$.each(data.response, function(key, value) {
							if($('input[name="' + key + '"]').length > 0 ){
								$('input[name="' + key + '"]').parent().addClass('error');
							}
						});
					}
					$('body').css('cursor','default');
					jAlert(message,js_localize['tag.edit.title'],"error");
				}
			});
		} else {
			jQuery.ajax({
				type: "POST",
				url: relPath+"api/v1/tag/create",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: parseForm(),
				success: function(data) {
					$('#wrapper_form_tags').closeDialog();
					$('body').css('cursor','default');
					jAlert(data.message,js_localize['tag.add.title'],"success")
					closeProfile();
					oTable.fnAddData(data.response);
					$('.user_table').css('height', '');
					$('.dataTables_paginate.paging_ellipses').show();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
					var message = JSON_ERROR.composeMessageFromJSON(data, true);
					if(data.response !== undefined){
						$.each(data.response, function(key, value) {
							if($('input[name="' + key + '"]').length > 0 ){
								$('input[name="' + key + '"]').parent().addClass('error');
							}
						});
					}
					$('body').css('cursor','default');
					jAlert(message,js_localize['tag.add.title'],"error");
				},
			});
		}
	});
    $('.nav_box.count_pages').click(function(e){
        $.alerts.cancelButton = js_localize['users.no'];
        $.alerts.okButton = js_localize['users.yes'];
        jConfirm(js_localize['tags.count.pages.msg'], js_localize['tags.count.pages.title'], 'success', function (r) {
            if (r) {
                $('body').css('cursor','progress');
                 $.alerts.okButton = js_localize['pages.jconfirm.ok'];
                jQuery.ajax({
                        type: "GET",
                        url: relPath + "system/count-pages-by-tag",
                        success: function (data) {
                            jAlert(js_localize['tags.count.pages.success'].replace('{0}', data), js_localize['tags.count.pages.title'], "success");
                            $('body').css('cursor','default');
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                            var message = JSON_ERROR.composeMessageFromJSON(data);
                            jAlert(message, js_localize['tags.count.pages.title'], "error");
                            $('body').css('cursor','default');
                        },
                    });
            }
        });
    });
});

function closeProfile() {
	$('#tag_form .input_group').removeClass('error');
	$('.user_actions .nav_box').removeClass('active');
	$('.user_edit_wrapper .user_fields').hide();
	$('#tag_form input:text').val('');
	$('#tag_form input:password').val('');
	$('#tag_form input[name="id"]').val('0');
	$('#tag_form input:checkbox:checked').removeAttr('checked');
	$('#tag_form input:radio:checked').removeAttr('checked');
	$('#tag_form textarea').val('');
	$(oTable.fnSettings().aoData).each(function() {
		$(this.nTr).removeClass('row_selected');
	});
	$('.user_edit_wrapper').hide();
}

/* Get the rows which are currently selected */
function fnGetSelected(oTableLocal) {
	return oTableLocal.$('tr.row_selected');
}
function refreshTable() {
	var search_input = $('.user_search').val() !== js_localize['pages.label.search_tag'] ? $('.user_search').val() : "";
	var sort_input = $('.user_sort').val() == -1 ? 1 : $('.user_sort').val();
	oTable.fnSort([[sort_input, 'asc']]);
	oTable.fnFilter(search_input);
	return false;
}

function populateForm(tagId) {
	jQuery.ajax({
		type: "GET",
		url: relPath+"api/v1/tag/read/" + tagId,
		success: function(data) {
			$.each(data.response, function(key, value) {
				if ($('input[name="' + key + '"]').attr('type') == 'checkbox' || $('input[name="' + key + '"]').attr('type') == 'radio') {
					$('input[name="' + key + '"][value="' + value + '"]').prop('checked', true)
				} else if($('p#'+key).length > 0){
					$('p#'+key).html(value);
				} else {
					$(':input[name="' + key + '"]').val(value);
				}
			});
			$('body').css('cursor','default');
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
			var message = JSON_ERROR.composeMessageFromJSON(data, true);
			jAlert(message, js_localize['tag.list.title'], "error");
		},
	});
	return false;
}

function parseForm() {
	var serialized = $('#tag_form').serializeArray();
	var s = '';
	var data = {};
	for (s in serialized) {
		if(serialized[s]['name'] != "group"){
			if(serialized[s]['name'] == "role" || serialized[s]['name'] == "active" || serialized[s]['name'] == "newsletter"){
				data[serialized[s]['name']] = parseInt(serialized[s]['value']);
			}else{
				data[serialized[s]['name']] = serialized[s]['value'];
			}
		}
	}
	return JSON.stringify(data);
}

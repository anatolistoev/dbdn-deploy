var callback_selection = null;
var hotspots = null;
var image_src = '';
var hst_count = 0;


$(document).ready(function () {
    initForm();

    bindHotspotClicks();
	
});

function initForm() {
    // Check if form is opened in TinyMCE
    if (top.tinymce && top.tinymce.activeEditor) {
        // Get the parameters passed into the window from the top frame
        var args = top.tinymce.activeEditor.windowManager.getParams();
        console.log(args);
        if (args.dialog_mode) {
            // Keep the call back to set the selection
            callback_selection = args.callback_selection;
			if(args.hotspots){
				hotspots = args.hotspots.points;

				image_src = args.image_src;
				$('#hotspot_img').attr('src','../'+image_src);
				
				if(hotspots){
					for(var i=0;i < hotspots.length;i++){
						addHotSpot();
						var last_hst_id = $('.hotspot_points .hotspot').last().attr('id');
						var hst_id = last_hst_id.substr(8);
						$('.hotspot_points .hotspot').last().find('#top_'+hst_id).val(hotspots[i].top);
						$('.hotspot_points .hotspot').last().find('#left_'+hst_id).val(hotspots[i].left);
						$('.hotspot_points .hotspot').last().find('#pos_'+hst_id).val(hotspots[i].pos);
						$('.hotspot_points .hotspot').last().find('.styledSelect').text($('.hotspot_points .hotspot').last().find('#pos_'+hst_id).next().next().find('li[rel="'+hotspots[i].pos+'"]').text());
					}
				}	

				$('#hotspot_description').val(args.hotspots.description);
			}
        }
		
		var css_content = relPath + "css/main.css";
		css_content += ", " + relPath + "css/backend_main_fixes.css";
		
		var tiny_image = tinymce.init({
			menubar: false,
			body_class: "image_body",
			content_css: css_content,
			skin: "dbdnskin",
			selector: "textarea#hotspot_description",
			toolbar: "bold",
			forced_root_block: 'p',
			valid_children: "+span[div|ul|p]",
			convert_urls: false,
			relative_urls: false,
			resize: false,
			setup: function(editor) {
			}
		});
    } else {
        // Downloads Tab
    }
}

function bindHotspotClicks(){
	bindCoordsClick();
	
	$('#add_hotspot').click(function(){
		addHotSpot();
	});
	
	$('a.nav_box.remove').click(function(){
		removeHotspot(this);
	});
	
	$('a.nav_box.save').click(function(){
		var hst_array = new Array();
		var points = new Array();
		$('.hotspot_points .hotspot').each(function(){
			var hst_id = $(this).attr('id').substr(8);
			var hst_json = {top: $(this).find('#top_'+hst_id).val() ,left:$(this).find('#left_'+hst_id).val(),pos:$(this).find('#pos_'+hst_id).val()};
			points.push(hst_json);			
		});
		var hst_json = {description : tinyMCE.get('hotspot_description').getContent(), points : points};
		callback_selection(JSON.stringify(hst_json));

        top.tinymce.activeEditor.windowManager.close();
	});
	
	$('a.nav_box.cancel').click(function(){
		top.tinymce.activeEditor.windowManager.close();
	})
}

function removeHotspot(el){
	jConfirm('Are you sure you want to remove this hotspot', 'Hotspot', "success", function(r) {
		if(r){
			$(el).parent().parent().remove();
		}
	})
}

function addHotSpot(){
	var hst_html = $('#hotspot_template').html();
	hst_html = hst_html.replace(/{hotspot_id}/g, (hst_count + 1));
	$('.hotspot_points').append(hst_html);
	hst_count += 1;
	bindCoordsClick();
	bindSelectClick();
	$('a.nav_box.remove').click(function(){
		removeHotspot(this);
	});
}

function bindSelectClick(){
	var $styledSelect = $('#hotspot_'+hst_count).find('div.styledSelect');
		var $list = $('#hotspot_'+hst_count).find('ul.options');
		var $listItems = $list.children('li');
		// Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
		$styledSelect.click(function (e) {
			e.stopPropagation();
			$('div.styledSelect.active').each(function () {
				$(this).removeClass('active').next('ul.options').hide();
			});
			$(this).toggleClass('active').next('ul.options').toggle();
		});

		// Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
		// Updates the select element to have the value of the equivalent option
		$listItems.click(function (e) {
			e.stopPropagation();
			$styledSelect.text($(this).text()).removeClass('active');
			$list.parent().find('select').val($(this).attr('rel'));
			$list.hide();
			//$(".user_sort").val($(this).attr('rel'));
			 //alert($this.val());// Uncomment this for demonstration! 
		});

		// Hides the unordered list when clicking outside of it
		$(document).click(function () {
			$styledSelect.removeClass('active');
			$list.hide();
		});
}

function bindCoordsClick(){
	$('.coords_btn').click(function(){
		$(document).unbind('click');
		$('body').css('cursor','crosshair');
		var btn = this;
		
		$(document).bind('click', function(e) {
			var elem = e.target;
			if($(elem).attr('id') == 'hotspot_img'){
				$(document).unbind('click');
				$('body').css('cursor','default');
				var offset = $('#hotspot_img').offset();
				var hst_id = $(btn).attr('id').substr(11);
				var top_percent = ((e.pageY - offset.top)/$('#hotspot_img').height()*100).toFixed(2);
				var left_percent = ((e.pageX - offset.left)/$('#hotspot_img').width()*100).toFixed(2);
				$(btn).parent().parent().find('#top_'+hst_id).val(top_percent+'%');
				$(btn).parent().parent().find('#left_'+hst_id).val(left_percent+'%');
			}
		})
		
		$(document).bind('keyup',function(e){
			if(e.keyCode === 27){
				$('body').css('cursor','default');
				$(document).unbind('click');
			}	
		});
	});
	
}

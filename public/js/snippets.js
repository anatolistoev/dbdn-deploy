var dbdn_snippets = [
    {section: "main,right", text:'Title', value: '<h1>Title text</h1><p>&nbsp;</p>'},
    {section: "main,right", text:'Title & subheading', value: '<h1>Title text <small>Sub-heading text</small></h1><p>&nbsp;</p>'},
    {section: "main,right", text:'Copy text', value: '<p>Copy text</p>'},
    {section: "main", text:'Notice', value: '<p class="notice">Notice</p><p>&nbsp;</p>'},
	{section: "main", text:'Subline', value: '<p class="subline">Subline</p><p>&nbsp;</p>'},
    {section: "main", text:'Pre-left', value: '<div class="pre-div-left"></div><div class="pre-left"><p>Pre tag left</p></div><p>&nbsp;</p>'},
    {section: "main", text:'Pre-right', value: '<div class="pre-div-right"></div><div class="pre-right"><p>Pre tag right</p></div><p>&nbsp;</p>'},
    {section: "main", text:'Quote', value: '<div class="quote"><p >Quote</p></div><p>&nbsp;</p>'},
//    {section: "main", text:'Teaser block', value : '<div class="overview_page_block"><a href="#">\n\
//		<img class="regular_image" border="0" src="'+relPath+'img/template/snippets/snippet_overview-teaser.png"></a>\n\
//	<b>Lorem ipsum dolor</b>\n\
//	<p>Sid amed</p>\n\
//	<a class="textLink" href="#">Lorem ipsum</a>\n\
//	<hr>\n\
//        </div>\n' },
	{section: "right", text: 'Message block', value: '<a href="#" class="newsblock">Lorem ipsum dolor sit amet, consectetuer adipiscing elit felis. </a>'},
	{section: "right", text: 'Message block (image)', value: '<div class="overview_teaser_block">\n\
		<a href="#"><img class="regular_image" src="' + relPath + 'img/template/120x80fff.gif" border="0"></a>\n\
		<a href="#" class="teaserLink">Link Lucent blue</a>\n\
	</div>'},
//    {section: "main, right", text:'Default link', value: '<a href="#" class="arrowlink">Default link</a>'},
//    {section: "main, right", text:'Contact link', value: '<a href="#" class="contactlink">Contact Link</a>'},
//    {section: "main, right", text:'Read more link', value: '<a href="#" class="readmore">Read more</a>'},
//    {section: "main, right", text:'External link', value: '<a href="#" class="externallink">External link</a>'},
//    {section: "main", text:'Go to top icon', value: '<a href="#top"><img class="regular_image" border="0" alt="Go to top" src="'+relPath+'img/template/icons/imo_top.gif" width="15" height="15"></a><hr>'},
	{section: "simple_image", image_type: 'simple', text: 'Banner image', value: '<img class="simple" src="{selection_src}" data-gallery="{idArray}">'},
	{section: "zoom_image images", image_type: 'zoom', text: 'Zoom image', value: '<img class="zoom" src="{selection_src}"  data-gallery="{idArray}">'},
	{section: "gallery_image", text: 'Insert gallery', value: '<img class="gallery" src="{selection_src}"  data-gallery="{idArray}">'},
	{section: "downloads", text: 'Insert file', value: '<img class="gallery" src="' + relPath + 'img/backend/loading.gif" data-gallery="{idArray}">'},
	{section: "main", text: 'Anchor navigation', value: '<img class="anchor_nav" src="' + relPath + 'img/backend/anchor.gif">'},
	{section: "explainer_image", text: 'Insert layout explainer', value: '<img class="explainer" src="{selection_src}"  data-gallery="{idArray}">'},
	{section: "inline_zoom_image images", image_type: 'inline_zoom', text: 'Inline zoom image', value: '<img class="inline_zoom" src="{selection_src}"   data-gallery="{idArray}">'},
	{section: "simple_no_zoom_image images", image_type: 'simple_no_zoom', text: 'Simple image', value: '<img class="simple_no_zoom" src="{selection_src}"  data-gallery="{idArray}">'},
	{section: "nl_main", text: 'Title', value: '<span class="webfont36" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:34px; line-height:40px; color:#000; padding:0 0px;">Title</span>'},
	{section: "nl_main", text: 'Subtitle', value: '<span class="webfont28" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:22px; line-height:26px; color:#000; padding:0 0px;">Subtitle</span>'},
	{section: "nl_main", text: 'Copy text', value: ' <span class="webfont14" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:12px; line-height:20px; color:#000; padding:0 0px;">Copy text</span>'},
	{section: "nl_main", text: 'Info Box', value: nl_info_section},
	{section: "nl_main", text: 'Header Section', value: header_section_snippet},
	{section: "nl_main", text: 'Content Section', value: content_section_snippet},
	{section: "nl_main", text: 'Two Cell Section', value: two_section_snippet},
	{section: "nl_simple_image", text: 'Big Image', value: nl_text_bottom_big_image},
	{section: "nl_text_right_image", text: 'Image Text Right', value: nl_text_right_image_snippet},
	{section: "nl_text_bottom_image", text: 'Image Text Bottom', value: nl_text_bottom_image_snippet},
    {section: "nl_main_footer", text: 'Footer text', value: nl_footer},
    {section: "nl_main_unsuscribe_link", text: 'Unsuscribe link', value: nl_unsuscribe_link},
	{section: "nl_header_d_image", text: 'Insert Header Desktop Image', value: '<img src="{selection_src}" style="vertical-align:top; width:660px; height:450px;" width="660" height="450" alt="{selection_desc}" />'},
	{section: "nl_header_m_image", text: 'Insert Header Desktop Image', value: '<img src="{selection_src}" width="0" height="0" alt="{selection_desc}" />'},
	{section: "nl_main", text: 'Title Box', value: nl_title_section},
	{section: "nl_main", text: 'Anchor', value: '<a class="nl_anchor" name=""></a>'},
];

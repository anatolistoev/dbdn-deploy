/**
 * core-js 2.5.1
 * https://github.com/zloirock/core-js
 * License: http://rock.mit-license.org
 * © 2017 Denis Pushkarev
 */
!function(t,n,r){"use strict";!function(t){function __webpack_require__(r){if(n[r])return n[r].exports;var e=n[r]={i:r,l:!1,exports:{}};return t[r].call(e.exports,e,e.exports,__webpack_require__),e.l=!0,e.exports}var n={};__webpack_require__.m=t,__webpack_require__.c=n,__webpack_require__.d=function(t,n,r){__webpack_require__.o(t,n)||Object.defineProperty(t,n,{configurable:!1,enumerable:!0,get:r})},__webpack_require__.n=function(t){var n=t&&t.__esModule?function getDefault(){return t["default"]}:function getModuleExports(){return t};return __webpack_require__.d(n,"a",n),n},__webpack_require__.o=function(t,n){return Object.prototype.hasOwnProperty.call(t,n)},__webpack_require__.p="",__webpack_require__(__webpack_require__.s=123)}([function(t,n,e){var i=e(2),o=e(28),u=e(12),c=e(13),f=e(18),a=function(t,n,e){var s,l,h,p,v=t&a.F,g=t&a.G,y=t&a.S,d=t&a.P,_=t&a.B,S=g?i:y?i[n]||(i[n]={}):(i[n]||{}).prototype,b=g?o:o[n]||(o[n]={}),m=b.prototype||(b.prototype={});g&&(e=n);for(s in e)h=((l=!v&&S&&S[s]!==r)?S:e)[s],p=_&&l?f(h,i):d&&"function"==typeof h?f(Function.call,h):h,S&&c(S,s,h,t&a.U),b[s]!=h&&u(b,s,p),d&&m[s]!=h&&(m[s]=h)};i.core=o,a.F=1,a.G=2,a.S=4,a.P=8,a.B=16,a.W=32,a.U=64,a.R=128,t.exports=a},function(t,n,r){var e=r(4);t.exports=function(t){if(!e(t))throw TypeError(t+" is not an object!");return t}},function(t,r){var e=t.exports="undefined"!=typeof window&&window.Math==Math?window:"undefined"!=typeof self&&self.Math==Math?self:Function("return this")();"number"==typeof n&&(n=e)},function(t,n){t.exports=function(t){try{return!!t()}catch(n){return!0}}},function(t,n){t.exports=function(t){return"object"==typeof t?null!==t:"function"==typeof t}},function(t,n,r){var e=r(49)("wks"),i=r(32),o=r(2).Symbol,u="function"==typeof o;(t.exports=function(t){return e[t]||(e[t]=u&&o[t]||(u?o:i)("Symbol."+t))}).store=e},function(t,n,r){t.exports=!r(3)(function(){return 7!=Object.defineProperty({},"a",{get:function(){return 7}}).a})},function(t,n,r){var e=r(1),i=r(89),o=r(21),u=Object.defineProperty;n.f=r(6)?Object.defineProperty:function defineProperty(t,n,r){if(e(t),n=o(n,!0),e(r),i)try{return u(t,n,r)}catch(c){}if("get"in r||"set"in r)throw TypeError("Accessors not supported!");return"value"in r&&(t[n]=r.value),t}},function(t,n,r){var e=r(23),i=Math.min;t.exports=function(t){return t>0?i(e(t),9007199254740991):0}},function(t,n,r){var e=r(22);t.exports=function(t){return Object(e(t))}},function(t,n){t.exports=function(t){if("function"!=typeof t)throw TypeError(t+" is not a function!");return t}},function(t,n){var r={}.hasOwnProperty;t.exports=function(t,n){return r.call(t,n)}},function(t,n,r){var e=r(7),i=r(31);t.exports=r(6)?function(t,n,r){return e.f(t,n,i(1,r))}:function(t,n,r){return t[n]=r,t}},function(t,n,r){var e=r(2),i=r(12),o=r(11),u=r(32)("src"),c=Function.toString,f=(""+c).split("toString");r(28).inspectSource=function(t){return c.call(t)},(t.exports=function(t,n,r,c){var a="function"==typeof r;a&&(o(r,"name")||i(r,"name",n)),t[n]!==r&&(a&&(o(r,u)||i(r,u,t[n]?""+t[n]:f.join(String(n)))),t===e?t[n]=r:c?t[n]?t[n]=r:i(t,n,r):(delete t[n],i(t,n,r)))})(Function.prototype,"toString",function toString(){return"function"==typeof this&&this[u]||c.call(this)})},function(t,n,r){var e=r(0),i=r(3),o=r(22),u=/"/g,c=function(t,n,r,e){var i=String(o(t)),c="<"+n;return""!==r&&(c+=" "+r+'="'+String(e).replace(u,"&quot;")+'"'),c+">"+i+"</"+n+">"};t.exports=function(t,n){var r={};r[t]=n(c),e(e.P+e.F*i(function(){var n=""[t]('"');return n!==n.toLowerCase()||n.split('"').length>3}),"String",r)}},function(t,n,r){var e=r(46),i=r(22);t.exports=function(t){return e(i(t))}},function(t,n,r){var e=r(47),i=r(31),o=r(15),u=r(21),c=r(11),f=r(89),a=Object.getOwnPropertyDescriptor;n.f=r(6)?a:function getOwnPropertyDescriptor(t,n){if(t=o(t),n=u(n,!0),f)try{return a(t,n)}catch(r){}if(c(t,n))return i(!e.f.call(t,n),t[n])}},function(t,n,r){var e=r(11),i=r(9),o=r(65)("IE_PROTO"),u=Object.prototype;t.exports=Object.getPrototypeOf||function(t){return t=i(t),e(t,o)?t[o]:"function"==typeof t.constructor&&t instanceof t.constructor?t.constructor.prototype:t instanceof Object?u:null}},function(t,n,e){var i=e(10);t.exports=function(t,n,e){if(i(t),n===r)return t;switch(e){case 1:return function(r){return t.call(n,r)};case 2:return function(r,e){return t.call(n,r,e)};case 3:return function(r,e,i){return t.call(n,r,e,i)}}return function(){return t.apply(n,arguments)}}},function(t,n){var r={}.toString;t.exports=function(t){return r.call(t).slice(8,-1)}},function(t,n,r){var e=r(3);t.exports=function(t,n){return!!t&&e(function(){n?t.call(null,function(){},1):t.call(null)})}},function(t,n,r){var e=r(4);t.exports=function(t,n){if(!e(t))return t;var r,i;if(n&&"function"==typeof(r=t.toString)&&!e(i=r.call(t)))return i;if("function"==typeof(r=t.valueOf)&&!e(i=r.call(t)))return i;if(!n&&"function"==typeof(r=t.toString)&&!e(i=r.call(t)))return i;throw TypeError("Can't convert object to primitive value")}},function(t,n){t.exports=function(t){if(t==r)throw TypeError("Can't call method on  "+t);return t}},function(t,n){var r=Math.ceil,e=Math.floor;t.exports=function(t){return isNaN(t=+t)?0:(t>0?e:r)(t)}},function(t,n,r){var e=r(0),i=r(28),o=r(3);t.exports=function(t,n){var r=(i.Object||{})[t]||Object[t],u={};u[t]=n(r),e(e.S+e.F*o(function(){r(1)}),"Object",u)}},function(t,n,e){var i=e(18),o=e(46),u=e(9),c=e(8),f=e(82);t.exports=function(t,n){var e=1==t,a=2==t,s=3==t,l=4==t,h=6==t,p=5==t||h,v=n||f;return function(n,f,g){for(var y,d,_=u(n),S=o(_),b=i(f,g,3),m=c(S.length),x=0,w=e?v(n,m):a?v(n,0):r;m>x;x++)if((p||x in S)&&(y=S[x],d=b(y,x,_),t))if(e)w[x]=d;else if(d)switch(t){case 3:return!0;case 5:return y;case 6:return x;case 2:w.push(y)}else if(l)return!1;return h?-1:s||l?l:w}}},function(t,n,e){if(e(6)){var i=e(33),o=e(2),u=e(3),c=e(0),f=e(59),a=e(88),s=e(18),l=e(39),h=e(31),p=e(12),v=e(41),g=e(23),y=e(8),d=e(116),_=e(35),S=e(21),b=e(11),m=e(48),x=e(4),w=e(9),E=e(79),O=e(36),P=e(17),M=e(37).f,I=e(81),F=e(32),A=e(5),k=e(25),N=e(50),j=e(57),R=e(84),T=e(44),L=e(54),D=e(38),W=e(83),C=e(105),U=e(7),G=e(16),B=U.f,V=G.f,z=o.RangeError,q=o.TypeError,K=o.Uint8Array,J=Array.prototype,Y=a.ArrayBuffer,H=a.DataView,X=k(0),Z=k(2),$=k(3),Q=k(4),tt=k(5),nt=k(6),rt=N(!0),et=N(!1),it=R.values,ot=R.keys,ut=R.entries,ct=J.lastIndexOf,ft=J.reduce,at=J.reduceRight,st=J.join,lt=J.sort,ht=J.slice,pt=J.toString,vt=J.toLocaleString,gt=A("iterator"),yt=A("toStringTag"),dt=F("typed_constructor"),_t=F("def_constructor"),St=f.CONSTR,bt=f.TYPED,mt=f.VIEW,xt=k(1,function(t,n){return Mt(j(t,t[_t]),n)}),wt=u(function(){return 1===new K(new Uint16Array([1]).buffer)[0]}),Et=!!K&&!!K.prototype.set&&u(function(){new K(1).set({})}),Ot=function(t,n){var r=g(t);if(r<0||r%n)throw z("Wrong offset!");return r},Pt=function(t){if(x(t)&&bt in t)return t;throw q(t+" is not a typed array!")},Mt=function(t,n){if(!(x(t)&&dt in t))throw q("It is not a typed array constructor!");return new t(n)},It=function(t,n){return Ft(j(t,t[_t]),n)},Ft=function(t,n){for(var r=0,e=n.length,i=Mt(t,e);e>r;)i[r]=n[r++];return i},At=function(t,n,r){B(t,n,{get:function(){return this._d[r]}})},kt=function from(t){var n,e,i,o,u,c,f=w(t),a=arguments.length,l=a>1?arguments[1]:r,h=l!==r,p=I(f);if(p!=r&&!E(p)){for(c=p.call(f),i=[],n=0;!(u=c.next()).done;n++)i.push(u.value);f=i}for(h&&a>2&&(l=s(l,arguments[2],2)),n=0,e=y(f.length),o=Mt(this,e);e>n;n++)o[n]=h?l(f[n],n):f[n];return o},Nt=function of(){for(var t=0,n=arguments.length,r=Mt(this,n);n>t;)r[t]=arguments[t++];return r},jt=!!K&&u(function(){vt.call(new K(1))}),Rt=function toLocaleString(){return vt.apply(jt?ht.call(Pt(this)):Pt(this),arguments)},Tt={copyWithin:function copyWithin(t,n){return C.call(Pt(this),t,n,arguments.length>2?arguments[2]:r)},every:function every(t){return Q(Pt(this),t,arguments.length>1?arguments[1]:r)},fill:function fill(t){return W.apply(Pt(this),arguments)},filter:function filter(t){return It(this,Z(Pt(this),t,arguments.length>1?arguments[1]:r))},find:function find(t){return tt(Pt(this),t,arguments.length>1?arguments[1]:r)},findIndex:function findIndex(t){return nt(Pt(this),t,arguments.length>1?arguments[1]:r)},forEach:function forEach(t){X(Pt(this),t,arguments.length>1?arguments[1]:r)},indexOf:function indexOf(t){return et(Pt(this),t,arguments.length>1?arguments[1]:r)},includes:function includes(t){return rt(Pt(this),t,arguments.length>1?arguments[1]:r)},join:function join(t){return st.apply(Pt(this),arguments)},lastIndexOf:function lastIndexOf(t){return ct.apply(Pt(this),arguments)},map:function map(t){return xt(Pt(this),t,arguments.length>1?arguments[1]:r)},reduce:function reduce(t){return ft.apply(Pt(this),arguments)},reduceRight:function reduceRight(t){return at.apply(Pt(this),arguments)},reverse:function reverse(){for(var t,n=this,r=Pt(n).length,e=Math.floor(r/2),i=0;i<e;)t=n[i],n[i++]=n[--r],n[r]=t;return n},some:function some(t){return $(Pt(this),t,arguments.length>1?arguments[1]:r)},sort:function sort(t){return lt.call(Pt(this),t)},subarray:function subarray(t,n){var e=Pt(this),i=e.length,o=_(t,i);return new(j(e,e[_t]))(e.buffer,e.byteOffset+o*e.BYTES_PER_ELEMENT,y((n===r?i:_(n,i))-o))}},Lt=function slice(t,n){return It(this,ht.call(Pt(this),t,n))},Dt=function set(t){Pt(this);var n=Ot(arguments[1],1),r=this.length,e=w(t),i=y(e.length),o=0;if(i+n>r)throw z("Wrong length!");for(;o<i;)this[n+o]=e[o++]},Wt={entries:function entries(){return ut.call(Pt(this))},keys:function keys(){return ot.call(Pt(this))},values:function values(){return it.call(Pt(this))}},Ct=function(t,n){return x(t)&&t[bt]&&"symbol"!=typeof n&&n in t&&String(+n)==String(n)},Ut=function getOwnPropertyDescriptor(t,n){return Ct(t,n=S(n,!0))?h(2,t[n]):V(t,n)},Gt=function defineProperty(t,n,r){return!(Ct(t,n=S(n,!0))&&x(r)&&b(r,"value"))||b(r,"get")||b(r,"set")||r.configurable||b(r,"writable")&&!r.writable||b(r,"enumerable")&&!r.enumerable?B(t,n,r):(t[n]=r.value,t)};St||(G.f=Ut,U.f=Gt),c(c.S+c.F*!St,"Object",{getOwnPropertyDescriptor:Ut,defineProperty:Gt}),u(function(){pt.call({})})&&(pt=vt=function toString(){return st.call(this)});var Bt=v({},Tt);v(Bt,Wt),p(Bt,gt,Wt.values),v(Bt,{slice:Lt,set:Dt,constructor:function(){},toString:pt,toLocaleString:Rt}),At(Bt,"buffer","b"),At(Bt,"byteOffset","o"),At(Bt,"byteLength","l"),At(Bt,"length","e"),B(Bt,yt,{get:function(){return this[bt]}}),t.exports=function(t,n,e,a){var s=t+((a=!!a)?"Clamped":"")+"Array",h="get"+t,v="set"+t,g=o[s],_=g||{},S=g&&P(g),b=!g||!f.ABV,w={},E=g&&g.prototype,I=function(t,r){var e=t._d;return e.v[h](r*n+e.o,wt)},F=function(t,r,e){var i=t._d;a&&(e=(e=Math.round(e))<0?0:e>255?255:255&e),i.v[v](r*n+i.o,e,wt)},A=function(t,n){B(t,n,{get:function(){return I(this,n)},set:function(t){return F(this,n,t)},enumerable:!0})};b?(g=e(function(t,e,i,o){l(t,g,s,"_d");var u,c,f,a,h=0,v=0;if(x(e)){if(!(e instanceof Y||"ArrayBuffer"==(a=m(e))||"SharedArrayBuffer"==a))return bt in e?Ft(g,e):kt.call(g,e);u=e,v=Ot(i,n);var _=e.byteLength;if(o===r){if(_%n)throw z("Wrong length!");if((c=_-v)<0)throw z("Wrong length!")}else if((c=y(o)*n)+v>_)throw z("Wrong length!");f=c/n}else f=d(e),u=new Y(c=f*n);for(p(t,"_d",{b:u,o:v,l:c,e:f,v:new H(u)});h<f;)A(t,h++)}),E=g.prototype=O(Bt),p(E,"constructor",g)):u(function(){g(1)})&&u(function(){new g(-1)})&&L(function(t){new g,new g(null),new g(1.5),new g(t)},!0)||(g=e(function(t,e,i,o){l(t,g,s);var u;return x(e)?e instanceof Y||"ArrayBuffer"==(u=m(e))||"SharedArrayBuffer"==u?o!==r?new _(e,Ot(i,n),o):i!==r?new _(e,Ot(i,n)):new _(e):bt in e?Ft(g,e):kt.call(g,e):new _(d(e))}),X(S!==Function.prototype?M(_).concat(M(S)):M(_),function(t){t in g||p(g,t,_[t])}),g.prototype=E,i||(E.constructor=g));var k=E[gt],N=!!k&&("values"==k.name||k.name==r),j=Wt.values;p(g,dt,!0),p(E,bt,s),p(E,mt,!0),p(E,_t,g),(a?new g(1)[yt]==s:yt in E)||B(E,yt,{get:function(){return s}}),w[s]=g,c(c.G+c.W+c.F*(g!=_),w),c(c.S,s,{BYTES_PER_ELEMENT:n}),c(c.S+c.F*u(function(){_.of.call(g,1)}),s,{from:kt,of:Nt}),"BYTES_PER_ELEMENT"in E||p(E,"BYTES_PER_ELEMENT",n),c(c.P,s,Tt),D(s),c(c.P+c.F*Et,s,{set:Dt}),c(c.P+c.F*!N,s,Wt),i||E.toString==pt||(E.toString=pt),c(c.P+c.F*u(function(){new g(1).slice()}),s,{slice:Lt}),c(c.P+c.F*(u(function(){return[1,2].toLocaleString()!=new g([1,2]).toLocaleString()})||!u(function(){E.toLocaleString.call([1,2])})),s,{toLocaleString:Rt}),T[s]=N?k:j,i||N||p(E,gt,j)}}else t.exports=function(){}},function(t,n,e){var i=e(110),o=e(0),u=e(49)("metadata"),c=u.store||(u.store=new(e(113))),f=function(t,n,e){var o=c.get(t);if(!o){if(!e)return r;c.set(t,o=new i)}var u=o.get(n);if(!u){if(!e)return r;o.set(n,u=new i)}return u};t.exports={store:c,map:f,has:function(t,n,e){var i=f(n,e,!1);return i!==r&&i.has(t)},get:function(t,n,e){var i=f(n,e,!1);return i===r?r:i.get(t)},set:function(t,n,r,e){f(r,e,!0).set(t,n)},keys:function(t,n){var r=f(t,n,!1),e=[];return r&&r.forEach(function(t,n){e.push(n)}),e},key:function(t){return t===r||"symbol"==typeof t?t:String(t)},exp:function(t){o(o.S,"Reflect",t)}}},function(n,r){var e=n.exports={version:"2.5.1"};"number"==typeof t&&(t=e)},function(t,n,r){var e=r(32)("meta"),i=r(4),o=r(11),u=r(7).f,c=0,f=Object.isExtensible||function(){return!0},a=!r(3)(function(){return f(Object.preventExtensions({}))}),s=function(t){u(t,e,{value:{i:"O"+ ++c,w:{}}})},l=t.exports={KEY:e,NEED:!1,fastKey:function(t,n){if(!i(t))return"symbol"==typeof t?t:("string"==typeof t?"S":"P")+t;if(!o(t,e)){if(!f(t))return"F";if(!n)return"E";s(t)}return t[e].i},getWeak:function(t,n){if(!o(t,e)){if(!f(t))return!0;if(!n)return!1;s(t)}return t[e].w},onFreeze:function(t){return a&&l.NEED&&f(t)&&!o(t,e)&&s(t),t}}},function(t,n,e){var i=e(5)("unscopables"),o=Array.prototype;o[i]==r&&e(12)(o,i,{}),t.exports=function(t){o[i][t]=!0}},function(t,n){t.exports=function(t,n){return{enumerable:!(1&t),configurable:!(2&t),writable:!(4&t),value:n}}},function(t,n){var e=0,i=Math.random();t.exports=function(t){return"Symbol(".concat(t===r?"":t,")_",(++e+i).toString(36))}},function(t,n){t.exports=!1},function(t,n,r){var e=r(91),i=r(66);t.exports=Object.keys||function keys(t){return e(t,i)}},function(t,n,r){var e=r(23),i=Math.max,o=Math.min;t.exports=function(t,n){return(t=e(t))<0?i(t+n,0):o(t,n)}},function(t,n,e){var i=e(1),o=e(92),u=e(66),c=e(65)("IE_PROTO"),f=function(){},a=function(){var t,n=e(63)("iframe"),r=u.length;for(n.style.display="none",e(67).appendChild(n),n.src="javascript:",(t=n.contentWindow.document).open(),t.write("<script>document.F=Object<\/script>"),t.close(),a=t.F;r--;)delete a.prototype[u[r]];return a()};t.exports=Object.create||function create(t,n){var e;return null!==t?(f.prototype=i(t),e=new f,f.prototype=null,e[c]=t):e=a(),n===r?e:o(e,n)}},function(t,n,r){var e=r(91),i=r(66).concat("length","prototype");n.f=Object.getOwnPropertyNames||function getOwnPropertyNames(t){return e(t,i)}},function(t,n,r){var e=r(2),i=r(7),o=r(6),u=r(5)("species");t.exports=function(t){var n=e[t];o&&n&&!n[u]&&i.f(n,u,{configurable:!0,get:function(){return this}})}},function(t,n){t.exports=function(t,n,e,i){if(!(t instanceof n)||i!==r&&i in t)throw TypeError(e+": incorrect invocation!");return t}},function(t,n,r){var e=r(18),i=r(103),o=r(79),u=r(1),c=r(8),f=r(81),a={},s={};(n=t.exports=function(t,n,r,l,h){var p,v,g,y,d=h?function(){return t}:f(t),_=e(r,l,n?2:1),S=0;if("function"!=typeof d)throw TypeError(t+" is not iterable!");if(o(d)){for(p=c(t.length);p>S;S++)if((y=n?_(u(v=t[S])[0],v[1]):_(t[S]))===a||y===s)return y}else for(g=d.call(t);!(v=g.next()).done;)if((y=i(g,_,v.value,n))===a||y===s)return y}).BREAK=a,n.RETURN=s},function(t,n,r){var e=r(13);t.exports=function(t,n,r){for(var i in n)e(t,i,n[i],r);return t}},function(t,n,r){var e=r(7).f,i=r(11),o=r(5)("toStringTag");t.exports=function(t,n,r){t&&!i(t=r?t:t.prototype,o)&&e(t,o,{configurable:!0,value:n})}},function(t,n,r){var e=r(0),i=r(22),o=r(3),u=r(70),c="["+u+"]",f=RegExp("^"+c+c+"*"),a=RegExp(c+c+"*$"),s=function(t,n,r){var i={},c=o(function(){return!!u[t]()||"​"!="​"[t]()}),f=i[t]=c?n(l):u[t];r&&(i[r]=f),e(e.P+e.F*c,"String",i)},l=s.trim=function(t,n){return t=String(i(t)),1&n&&(t=t.replace(f,"")),2&n&&(t=t.replace(a,"")),t};t.exports=s},function(t,n){t.exports={}},function(t,n,r){var e=r(4);t.exports=function(t,n){if(!e(t)||t._t!==n)throw TypeError("Incompatible receiver, "+n+" required!");return t}},function(t,n,r){var e=r(19);t.exports=Object("z").propertyIsEnumerable(0)?Object:function(t){return"String"==e(t)?t.split(""):Object(t)}},function(t,n){n.f={}.propertyIsEnumerable},function(t,n,e){var i=e(19),o=e(5)("toStringTag"),u="Arguments"==i(function(){return arguments}()),c=function(t,n){try{return t[n]}catch(r){}};t.exports=function(t){var n,e,f;return t===r?"Undefined":null===t?"Null":"string"==typeof(e=c(n=Object(t),o))?e:u?i(n):"Object"==(f=i(n))&&"function"==typeof n.callee?"Arguments":f}},function(t,n,r){var e=r(2),i=e["__core-js_shared__"]||(e["__core-js_shared__"]={});t.exports=function(t){return i[t]||(i[t]={})}},function(t,n,r){var e=r(15),i=r(8),o=r(35);t.exports=function(t){return function(n,r,u){var c,f=e(n),a=i(f.length),s=o(u,a);if(t&&r!=r){for(;a>s;)if((c=f[s++])!=c)return!0}else for(;a>s;s++)if((t||s in f)&&f[s]===r)return t||s||0;return!t&&-1}}},function(t,n){n.f=Object.getOwnPropertySymbols},function(t,n,r){var e=r(19);t.exports=Array.isArray||function isArray(t){return"Array"==e(t)}},function(t,n,e){var i=e(4),o=e(19),u=e(5)("match");t.exports=function(t){var n;return i(t)&&((n=t[u])!==r?!!n:"RegExp"==o(t))}},function(t,n,r){var e=r(5)("iterator"),i=!1;try{var o=[7][e]();o["return"]=function(){i=!0},Array.from(o,function(){throw 2})}catch(u){}t.exports=function(t,n){if(!n&&!i)return!1;var r=!1;try{var o=[7],c=o[e]();c.next=function(){return{done:r=!0}},o[e]=function(){return c},t(o)}catch(u){}return r}},function(t,n,r){var e=r(1);t.exports=function(){var t=e(this),n="";return t.global&&(n+="g"),t.ignoreCase&&(n+="i"),t.multiline&&(n+="m"),t.unicode&&(n+="u"),t.sticky&&(n+="y"),n}},function(t,n,r){var e=r(12),i=r(13),o=r(3),u=r(22),c=r(5);t.exports=function(t,n,r){var f=c(t),a=r(u,f,""[t]),s=a[0],l=a[1];o(function(){var n={};return n[f]=function(){return 7},7!=""[t](n)})&&(i(String.prototype,t,s),e(RegExp.prototype,f,2==n?function(t,n){return l.call(t,this,n)}:function(t){return l.call(t,this)}))}},function(t,n,e){var i=e(1),o=e(10),u=e(5)("species");t.exports=function(t,n){var e,c=i(t).constructor;return c===r||(e=i(c)[u])==r?n:o(e)}},function(t,n,e){var i=e(2),o=e(0),u=e(13),c=e(41),f=e(29),a=e(40),s=e(39),l=e(4),h=e(3),p=e(54),v=e(42),g=e(69);t.exports=function(t,n,e,y,d,_){var S=i[t],b=S,m=d?"set":"add",x=b&&b.prototype,w={},E=function(t){var n=x[t];u(x,t,"delete"==t?function(t){return!(_&&!l(t))&&n.call(this,0===t?0:t)}:"has"==t?function has(t){return!(_&&!l(t))&&n.call(this,0===t?0:t)}:"get"==t?function get(t){return _&&!l(t)?r:n.call(this,0===t?0:t)}:"add"==t?function add(t){return n.call(this,0===t?0:t),this}:function set(t,r){return n.call(this,0===t?0:t,r),this})};if("function"==typeof b&&(_||x.forEach&&!h(function(){(new b).entries().next()}))){var O=new b,P=O[m](_?{}:-0,1)!=O,M=h(function(){O.has(1)}),I=p(function(t){new b(t)}),F=!_&&h(function(){for(var t=new b,n=5;n--;)t[m](n,n);return!t.has(-0)});I||((b=n(function(n,e){s(n,b,t);var i=g(new S,n,b);return e!=r&&a(e,d,i[m],i),i})).prototype=x,x.constructor=b),(M||F)&&(E("delete"),E("has"),d&&E("get")),(F||P)&&E(m),_&&x.clear&&delete x.clear}else b=y.getConstructor(n,t,d,m),c(b.prototype,e),f.NEED=!0;return v(b,t),w[t]=b,o(o.G+o.W+o.F*(b!=S),w),_||y.setStrong(b,t,d),b}},function(t,n,r){for(var e,i=r(2),o=r(12),u=r(32),c=u("typed_array"),f=u("view"),a=!(!i.ArrayBuffer||!i.DataView),s=a,l=0,h="Int8Array,Uint8Array,Uint8ClampedArray,Int16Array,Uint16Array,Int32Array,Uint32Array,Float32Array,Float64Array".split(",");l<9;)(e=i[h[l++]])?(o(e.prototype,c,!0),o(e.prototype,f,!0)):s=!1;t.exports={ABV:a,CONSTR:s,TYPED:c,VIEW:f}},function(t,n,r){t.exports=r(33)||!r(3)(function(){var t=Math.random();__defineSetter__.call(null,t,function(){}),delete r(2)[t]})},function(t,n,r){var e=r(0);t.exports=function(t){e(e.S,t,{of:function of(){for(var t=arguments.length,n=Array(t);t--;)n[t]=arguments[t];return new this(n)}})}},function(t,n,e){var i=e(0),o=e(10),u=e(18),c=e(40);t.exports=function(t){i(i.S,t,{from:function from(t){var n,e,i,f,a=arguments[1];return o(this),(n=a!==r)&&o(a),t==r?new this:(e=[],n?(i=0,f=u(a,arguments[2],2),c(t,!1,function(t){e.push(f(t,i++))})):c(t,!1,e.push,e),new this(e))}})}},function(t,n,r){var e=r(4),i=r(2).document,o=e(i)&&e(i.createElement);t.exports=function(t){return o?i.createElement(t):{}}},function(t,n,r){var e=r(2),i=r(28),o=r(33),u=r(90),c=r(7).f;t.exports=function(t){var n=i.Symbol||(i.Symbol=o?{}:e.Symbol||{});"_"==t.charAt(0)||t in n||c(n,t,{value:u.f(t)})}},function(t,n,r){var e=r(49)("keys"),i=r(32);t.exports=function(t){return e[t]||(e[t]=i(t))}},function(t,n){t.exports="constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",")},function(t,n,r){var e=r(2).document;t.exports=e&&e.documentElement},function(t,n,e){var i=e(4),o=e(1),u=function(t,n){if(o(t),!i(n)&&null!==n)throw TypeError(n+": can't set as prototype!")};t.exports={set:Object.setPrototypeOf||("__proto__"in{}?function(t,n,r){try{(r=e(18)(Function.call,e(16).f(Object.prototype,"__proto__").set,2))(t,[]),n=!(t instanceof Array)}catch(i){n=!0}return function setPrototypeOf(t,e){return u(t,e),n?t.__proto__=e:r(t,e),t}}({},!1):r),check:u}},function(t,n,r){var e=r(4),i=r(68).set;t.exports=function(t,n,r){var o,u=n.constructor;return u!==r&&"function"==typeof u&&(o=u.prototype)!==r.prototype&&e(o)&&i&&i(t,o),t}},function(t,n){t.exports="\t\n\x0B\f\r   ᠎             　\u2028\u2029\ufeff"},function(t,n,r){var e=r(23),i=r(22);t.exports=function repeat(t){var n=String(i(this)),r="",o=e(t);if(o<0||o==Infinity)throw RangeError("Count can't be negative");for(;o>0;(o>>>=1)&&(n+=n))1&o&&(r+=n);return r}},function(t,n){t.exports=Math.sign||function sign(t){return 0==(t=+t)||t!=t?t:t<0?-1:1}},function(t,n){var r=Math.expm1;t.exports=!r||r(10)>22025.465794806718||r(10)<22025.465794806718||-2e-17!=r(-2e-17)?function expm1(t){return 0==(t=+t)?t:t>-1e-6&&t<1e-6?t+t*t/2:Math.exp(t)-1}:r},function(t,n,e){var i=e(23),o=e(22);t.exports=function(t){return function(n,e){var u,c,f=String(o(n)),a=i(e),s=f.length;return a<0||a>=s?t?"":r:(u=f.charCodeAt(a))<55296||u>56319||a+1===s||(c=f.charCodeAt(a+1))<56320||c>57343?t?f.charAt(a):u:t?f.slice(a,a+2):c-56320+(u-55296<<10)+65536}}},function(t,n,r){var e=r(53),i=r(22);t.exports=function(t,n,r){if(e(n))throw TypeError("String#"+r+" doesn't accept regex!");return String(i(t))}},function(t,n,r){var e=r(5)("match");t.exports=function(t){var n=/./;try{"/./"[t](n)}catch(r){try{return n[e]=!1,!"/./"[t](n)}catch(i){}}return!0}},function(t,n,e){var i=e(33),o=e(0),u=e(13),c=e(12),f=e(11),a=e(44),s=e(78),l=e(42),h=e(17),p=e(5)("iterator"),v=!([].keys&&"next"in[].keys()),g=function(){return this};t.exports=function(t,n,e,y,d,_,S){s(e,n,y);var b,m,x,w=function(t){if(!v&&t in M)return M[t];switch(t){case"keys":return function keys(){return new e(this,t)};case"values":return function values(){return new e(this,t)}}return function entries(){return new e(this,t)}},E=n+" Iterator",O="values"==d,P=!1,M=t.prototype,I=M[p]||M["@@iterator"]||d&&M[d],F=I||w(d),A=d?O?w("entries"):F:r,k="Array"==n?M.entries||I:I;if(k&&(x=h(k.call(new t)))!==Object.prototype&&x.next&&(l(x,E,!0),i||f(x,p)||c(x,p,g)),O&&I&&"values"!==I.name&&(P=!0,F=function values(){return I.call(this)}),i&&!S||!v&&!P&&M[p]||c(M,p,F),a[n]=F,a[E]=g,d)if(b={values:O?F:w("values"),keys:_?F:w("keys"),entries:A},S)for(m in b)m in M||u(M,m,b[m]);else o(o.P+o.F*(v||P),n,b);return b}},function(t,n,r){var e=r(36),i=r(31),o=r(42),u={};r(12)(u,r(5)("iterator"),function(){return this}),t.exports=function(t,n,r){t.prototype=e(u,{next:i(1,r)}),o(t,n+" Iterator")}},function(t,n,e){var i=e(44),o=e(5)("iterator"),u=Array.prototype;t.exports=function(t){return t!==r&&(i.Array===t||u[o]===t)}},function(t,n,r){var e=r(7),i=r(31);t.exports=function(t,n,r){n in t?e.f(t,n,i(0,r)):t[n]=r}},function(t,n,e){var i=e(48),o=e(5)("iterator"),u=e(44);t.exports=e(28).getIteratorMethod=function(t){if(t!=r)return t[o]||t["@@iterator"]||u[i(t)]}},function(t,n,r){var e=r(207);t.exports=function(t,n){return new(e(t))(n)}},function(t,n,e){var i=e(9),o=e(35),u=e(8);t.exports=function fill(t){for(var n=i(this),e=u(n.length),c=arguments.length,f=o(c>1?arguments[1]:r,e),a=c>2?arguments[2]:r,s=a===r?e:o(a,e);s>f;)n[f++]=t;return n}},function(t,n,e){var i=e(30),o=e(106),u=e(44),c=e(15);t.exports=e(77)(Array,"Array",function(t,n){this._t=c(t),this._i=0,this._k=n},function(){var t=this._t,n=this._k,e=this._i++;return!t||e>=t.length?(this._t=r,o(1)):"keys"==n?o(0,e):"values"==n?o(0,t[e]):o(0,[e,t[e]])},"values"),u.Arguments=u.Array,i("keys"),i("values"),i("entries")},function(t,n,r){var e,i,o,u=r(18),c=r(96),f=r(67),a=r(63),s=r(2),l=s.process,h=s.setImmediate,p=s.clearImmediate,v=s.MessageChannel,g=s.Dispatch,y=0,d={},_=function(){var t=+this;if(d.hasOwnProperty(t)){var n=d[t];delete d[t],n()}},S=function(t){_.call(t.data)};h&&p||(h=function setImmediate(t){for(var n=[],r=1;arguments.length>r;)n.push(arguments[r++]);return d[++y]=function(){c("function"==typeof t?t:Function(t),n)},e(y),y},p=function clearImmediate(t){delete d[t]},"process"==r(19)(l)?e=function(t){l.nextTick(u(_,t,1))}:g&&g.now?e=function(t){g.now(u(_,t,1))}:v?(o=(i=new v).port2,i.port1.onmessage=S,e=u(o.postMessage,o,1)):s.addEventListener&&"function"==typeof postMessage&&!s.importScripts?(e=function(t){s.postMessage(t+"","*")},s.addEventListener("message",S,!1)):e="onreadystatechange"in a("script")?function(t){f.appendChild(a("script")).onreadystatechange=function(){f.removeChild(this),_.call(t)}}:function(t){setTimeout(u(_,t,1),0)}),t.exports={set:h,clear:p}},function(t,n,e){var i=e(2),o=e(85).set,u=i.MutationObserver||i.WebKitMutationObserver,c=i.process,f=i.Promise,a="process"==e(19)(c);t.exports=function(){var t,n,e,s=function(){var i,o;for(a&&(i=c.domain)&&i.exit();t;){o=t.fn,t=t.next;try{o()}catch(u){throw t?e():n=r,u}}n=r,i&&i.enter()};if(a)e=function(){c.nextTick(s)};else if(u){var l=!0,h=document.createTextNode("");new u(s).observe(h,{characterData:!0}),e=function(){h.data=l=!l}}else if(f&&f.resolve){var p=f.resolve();e=function(){p.then(s)}}else e=function(){o.call(i,s)};return function(i){var o={fn:i,next:r};n&&(n.next=o),t||(t=o,e()),n=o}}},function(t,n,e){function PromiseCapability(t){var n,e;this.promise=new t(function(t,i){if(n!==r||e!==r)throw TypeError("Bad Promise constructor");n=t,e=i}),this.resolve=i(n),this.reject=i(e)}var i=e(10);t.exports.f=function(t){return new PromiseCapability(t)}},function(t,n,e){function packIEEE754(t,n,r){var e,i,o,u=Array(r),c=8*r-n-1,f=(1<<c)-1,a=f>>1,s=23===n?I(2,-24)-I(2,-77):0,l=0,h=t<0||0===t&&1/t<0?1:0;for((t=M(t))!=t||t===O?(i=t!=t?1:0,e=f):(e=F(A(t)/k),t*(o=I(2,-e))<1&&(e--,o*=2),(t+=e+a>=1?s/o:s*I(2,1-a))*o>=2&&(e++,o/=2),e+a>=f?(i=0,e=f):e+a>=1?(i=(t*o-1)*I(2,n),e+=a):(i=t*I(2,a-1)*I(2,n),e=0));n>=8;u[l++]=255&i,i/=256,n-=8);for(e=e<<n|i,c+=n;c>0;u[l++]=255&e,e/=256,c-=8);return u[--l]|=128*h,u}function unpackIEEE754(t,n,r){var e,i=8*r-n-1,o=(1<<i)-1,u=o>>1,c=i-7,f=r-1,a=t[f--],s=127&a;for(a>>=7;c>0;s=256*s+t[f],f--,c-=8);for(e=s&(1<<-c)-1,s>>=-c,c+=n;c>0;e=256*e+t[f],f--,c-=8);if(0===s)s=1-u;else{if(s===o)return e?NaN:a?-O:O;e+=I(2,n),s-=u}return(a?-1:1)*e*I(2,s-n)}function unpackI32(t){return t[3]<<24|t[2]<<16|t[1]<<8|t[0]}function packI8(t){return[255&t]}function packI16(t){return[255&t,t>>8&255]}function packI32(t){return[255&t,t>>8&255,t>>16&255,t>>24&255]}function packF64(t){return packIEEE754(t,52,8)}function packF32(t){return packIEEE754(t,23,4)}function addGetter(t,n,r){y(t[S],n,{get:function(){return this[r]}})}function get(t,n,r,e){var i=v(+r);if(i+n>t[j])throw E(b);var o=t[N]._b,u=i+t[R],c=o.slice(u,u+n);return e?c:c.reverse()}function set(t,n,r,e,i,o){var u=v(+r);if(u+n>t[j])throw E(b);for(var c=t[N]._b,f=u+t[R],a=e(+i),s=0;s<n;s++)c[f+s]=a[o?s:n-s-1]}var i=e(2),o=e(6),u=e(33),c=e(59),f=e(12),a=e(41),s=e(3),l=e(39),h=e(23),p=e(8),v=e(116),g=e(37).f,y=e(7).f,d=e(83),_=e(42),S="prototype",b="Wrong index!",m=i.ArrayBuffer,x=i.DataView,w=i.Math,E=i.RangeError,O=i.Infinity,P=m,M=w.abs,I=w.pow,F=w.floor,A=w.log,k=w.LN2,N=o?"_b":"buffer",j=o?"_l":"byteLength",R=o?"_o":"byteOffset";if(c.ABV){if(!s(function(){m(1)})||!s(function(){new m(-1)})||s(function(){return new m,new m(1.5),new m(NaN),"ArrayBuffer"!=m.name})){for(var T,L=(m=function ArrayBuffer(t){return l(this,m),new P(v(t))})[S]=P[S],D=g(P),W=0;D.length>W;)(T=D[W++])in m||f(m,T,P[T]);u||(L.constructor=m)}var C=new x(new m(2)),U=x[S].setInt8;C.setInt8(0,2147483648),C.setInt8(1,2147483649),!C.getInt8(0)&&C.getInt8(1)||a(x[S],{setInt8:function setInt8(t,n){U.call(this,t,n<<24>>24)},setUint8:function setUint8(t,n){U.call(this,t,n<<24>>24)}},!0)}else m=function ArrayBuffer(t){l(this,m,"ArrayBuffer");var n=v(t);this._b=d.call(Array(n),0),this[j]=n},x=function DataView(t,n,e){l(this,x,"DataView"),l(t,m,"DataView");var i=t[j],o=h(n);if(o<0||o>i)throw E("Wrong offset!");if(e=e===r?i-o:p(e),o+e>i)throw E("Wrong length!");this[N]=t,this[R]=o,this[j]=e},o&&(addGetter(m,"byteLength","_l"),addGetter(x,"buffer","_b"),addGetter(x,"byteLength","_l"),addGetter(x,"byteOffset","_o")),a(x[S],{getInt8:function getInt8(t){return get(this,1,t)[0]<<24>>24},getUint8:function getUint8(t){return get(this,1,t)[0]},getInt16:function getInt16(t){var n=get(this,2,t,arguments[1]);return(n[1]<<8|n[0])<<16>>16},getUint16:function getUint16(t){var n=get(this,2,t,arguments[1]);return n[1]<<8|n[0]},getInt32:function getInt32(t){return unpackI32(get(this,4,t,arguments[1]))},getUint32:function getUint32(t){return unpackI32(get(this,4,t,arguments[1]))>>>0},getFloat32:function getFloat32(t){return unpackIEEE754(get(this,4,t,arguments[1]),23,4)},getFloat64:function getFloat64(t){return unpackIEEE754(get(this,8,t,arguments[1]),52,8)},setInt8:function setInt8(t,n){set(this,1,t,packI8,n)},setUint8:function setUint8(t,n){set(this,1,t,packI8,n)},setInt16:function setInt16(t,n){set(this,2,t,packI16,n,arguments[2])},setUint16:function setUint16(t,n){set(this,2,t,packI16,n,arguments[2])},setInt32:function setInt32(t,n){set(this,4,t,packI32,n,arguments[2])},setUint32:function setUint32(t,n){set(this,4,t,packI32,n,arguments[2])},setFloat32:function setFloat32(t,n){set(this,4,t,packF32,n,arguments[2])},setFloat64:function setFloat64(t,n){set(this,8,t,packF64,n,arguments[2])}});_(m,"ArrayBuffer"),_(x,"DataView"),f(x[S],c.VIEW,!0),n.ArrayBuffer=m,n.DataView=x},function(t,n,r){t.exports=!r(6)&&!r(3)(function(){return 7!=Object.defineProperty(r(63)("div"),"a",{get:function(){return 7}}).a})},function(t,n,r){n.f=r(5)},function(t,n,r){var e=r(11),i=r(15),o=r(50)(!1),u=r(65)("IE_PROTO");t.exports=function(t,n){var r,c=i(t),f=0,a=[];for(r in c)r!=u&&e(c,r)&&a.push(r);for(;n.length>f;)e(c,r=n[f++])&&(~o(a,r)||a.push(r));return a}},function(t,n,r){var e=r(7),i=r(1),o=r(34);t.exports=r(6)?Object.defineProperties:function defineProperties(t,n){i(t);for(var r,u=o(n),c=u.length,f=0;c>f;)e.f(t,r=u[f++],n[r]);return t}},function(t,n,r){var e=r(15),i=r(37).f,o={}.toString,u="object"==typeof window&&window&&Object.getOwnPropertyNames?Object.getOwnPropertyNames(window):[],c=function(t){try{return i(t)}catch(n){return u.slice()}};t.exports.f=function getOwnPropertyNames(t){return u&&"[object Window]"==o.call(t)?c(t):i(e(t))}},function(t,n,r){var e=r(34),i=r(51),o=r(47),u=r(9),c=r(46),f=Object.assign;t.exports=!f||r(3)(function(){var t={},n={},r=Symbol(),e="abcdefghijklmnopqrst";return t[r]=7,e.split("").forEach(function(t){n[t]=t}),7!=f({},t)[r]||Object.keys(f({},n)).join("")!=e})?function assign(t,n){for(var r=u(t),f=arguments.length,a=1,s=i.f,l=o.f;f>a;)for(var h,p=c(arguments[a++]),v=s?e(p).concat(s(p)):e(p),g=v.length,y=0;g>y;)l.call(p,h=v[y++])&&(r[h]=p[h]);return r}:f},function(t,n,r){var e=r(10),i=r(4),o=r(96),u=[].slice,c={},f=function(t,n,r){if(!(n in c)){for(var e=[],i=0;i<n;i++)e[i]="a["+i+"]"
;c[n]=Function("F,a","return new F("+e.join(",")+")")}return c[n](t,r)};t.exports=Function.bind||function bind(t){var n=e(this),r=u.call(arguments,1),c=function(){var e=r.concat(u.call(arguments));return this instanceof c?f(n,e.length,e):o(n,e,t)};return i(n.prototype)&&(c.prototype=n.prototype),c}},function(t,n){t.exports=function(t,n,e){var i=e===r;switch(n.length){case 0:return i?t():t.call(e);case 1:return i?t(n[0]):t.call(e,n[0]);case 2:return i?t(n[0],n[1]):t.call(e,n[0],n[1]);case 3:return i?t(n[0],n[1],n[2]):t.call(e,n[0],n[1],n[2]);case 4:return i?t(n[0],n[1],n[2],n[3]):t.call(e,n[0],n[1],n[2],n[3])}return t.apply(e,n)}},function(t,n,r){var e=r(19);t.exports=function(t,n){if("number"!=typeof t&&"Number"!=e(t))throw TypeError(n);return+t}},function(t,n,r){var e=r(4),i=Math.floor;t.exports=function isInteger(t){return!e(t)&&isFinite(t)&&i(t)===t}},function(t,n,r){var e=r(2).parseFloat,i=r(43).trim;t.exports=1/e(r(70)+"-0")!=-Infinity?function parseFloat(t){var n=i(String(t),3),r=e(n);return 0===r&&"-"==n.charAt(0)?-0:r}:e},function(t,n,r){var e=r(2).parseInt,i=r(43).trim,o=r(70),u=/^[-+]?0[xX]/;t.exports=8!==e(o+"08")||22!==e(o+"0x16")?function parseInt(t,n){var r=i(String(t),3);return e(r,n>>>0||(u.test(r)?16:10))}:e},function(t,n){t.exports=Math.log1p||function log1p(t){return(t=+t)>-1e-8&&t<1e-8?t-t*t/2:Math.log(1+t)}},function(t,n,r){var e=r(72),i=Math.pow,o=i(2,-52),u=i(2,-23),c=i(2,127)*(2-u),f=i(2,-126),a=function(t){return t+1/o-1/o};t.exports=Math.fround||function fround(t){var n,r,i=Math.abs(t),s=e(t);return i<f?s*a(i/f/u)*f*u:(n=(1+u/o)*i,(r=n-(n-i))>c||r!=r?s*Infinity:s*r)}},function(t,n,e){var i=e(1);t.exports=function(t,n,e,o){try{return o?n(i(e)[0],e[1]):n(e)}catch(c){var u=t["return"];throw u!==r&&i(u.call(t)),c}}},function(t,n,r){var e=r(10),i=r(9),o=r(46),u=r(8);t.exports=function(t,n,r,c,f){e(n);var a=i(t),s=o(a),l=u(a.length),h=f?l-1:0,p=f?-1:1;if(r<2)for(;;){if(h in s){c=s[h],h+=p;break}if(h+=p,f?h<0:l<=h)throw TypeError("Reduce of empty array with no initial value")}for(;f?h>=0:l>h;h+=p)h in s&&(c=n(c,s[h],h,a));return c}},function(t,n,e){var i=e(9),o=e(35),u=e(8);t.exports=[].copyWithin||function copyWithin(t,n){var e=i(this),c=u(e.length),f=o(t,c),a=o(n,c),s=arguments.length>2?arguments[2]:r,l=Math.min((s===r?c:o(s,c))-a,c-f),h=1;for(a<f&&f<a+l&&(h=-1,a+=l-1,f+=l-1);l-- >0;)a in e?e[f]=e[a]:delete e[f],f+=h,a+=h;return e}},function(t,n){t.exports=function(t,n){return{value:n,done:!!t}}},function(t,n,r){r(6)&&"g"!=/./g.flags&&r(7).f(RegExp.prototype,"flags",{configurable:!0,get:r(55)})},function(t,n){t.exports=function(t){try{return{e:!1,v:t()}}catch(n){return{e:!0,v:n}}}},function(t,n,r){var e=r(1),i=r(4),o=r(87);t.exports=function(t,n){if(e(t),i(n)&&n.constructor===t)return n;var r=o.f(t);return(0,r.resolve)(n),r.promise}},function(t,n,e){var i=e(111),o=e(45);t.exports=e(58)("Map",function(t){return function Map(){return t(this,arguments.length>0?arguments[0]:r)}},{get:function get(t){var n=i.getEntry(o(this,"Map"),t);return n&&n.v},set:function set(t,n){return i.def(o(this,"Map"),0===t?0:t,n)}},i,!0)},function(t,n,e){var i=e(7).f,o=e(36),u=e(41),c=e(18),f=e(39),a=e(40),s=e(77),l=e(106),h=e(38),p=e(6),v=e(29).fastKey,g=e(45),y=p?"_s":"size",d=function(t,n){var r,e=v(n);if("F"!==e)return t._i[e];for(r=t._f;r;r=r.n)if(r.k==n)return r};t.exports={getConstructor:function(t,n,e,s){var l=t(function(t,i){f(t,l,n,"_i"),t._t=n,t._i=o(null),t._f=r,t._l=r,t[y]=0,i!=r&&a(i,e,t[s],t)});return u(l.prototype,{clear:function clear(){for(var t=g(this,n),e=t._i,i=t._f;i;i=i.n)i.r=!0,i.p&&(i.p=i.p.n=r),delete e[i.i];t._f=t._l=r,t[y]=0},"delete":function(t){var r=g(this,n),e=d(r,t);if(e){var i=e.n,o=e.p;delete r._i[e.i],e.r=!0,o&&(o.n=i),i&&(i.p=o),r._f==e&&(r._f=i),r._l==e&&(r._l=o),r[y]--}return!!e},forEach:function forEach(t){g(this,n);for(var e,i=c(t,arguments.length>1?arguments[1]:r,3);e=e?e.n:this._f;)for(i(e.v,e.k,this);e&&e.r;)e=e.p},has:function has(t){return!!d(g(this,n),t)}}),p&&i(l.prototype,"size",{get:function(){return g(this,n)[y]}}),l},def:function(t,n,e){var i,o,u=d(t,n);return u?u.v=e:(t._l=u={i:o=v(n,!0),k:n,v:e,p:i=t._l,n:r,r:!1},t._f||(t._f=u),i&&(i.n=u),t[y]++,"F"!==o&&(t._i[o]=u)),t},getEntry:d,setStrong:function(t,n,e){s(t,n,function(t,e){this._t=g(t,n),this._k=e,this._l=r},function(){for(var t=this,n=t._k,e=t._l;e&&e.r;)e=e.p;return t._t&&(t._l=e=e?e.n:t._t._f)?"keys"==n?l(0,e.k):"values"==n?l(0,e.v):l(0,[e.k,e.v]):(t._t=r,l(1))},e?"entries":"values",!e,!0),h(n)}}},function(t,n,e){var i=e(111),o=e(45);t.exports=e(58)("Set",function(t){return function Set(){return t(this,arguments.length>0?arguments[0]:r)}},{add:function add(t){return i.def(o(this,"Set"),t=0===t?0:t,t)}},i)},function(t,n,e){var i,o=e(25)(0),u=e(13),c=e(29),f=e(94),a=e(114),s=e(4),l=e(3),h=e(45),p=c.getWeak,v=Object.isExtensible,g=a.ufstore,y={},d=function(t){return function WeakMap(){return t(this,arguments.length>0?arguments[0]:r)}},_={get:function get(t){if(s(t)){var n=p(t);return!0===n?g(h(this,"WeakMap")).get(t):n?n[this._i]:r}},set:function set(t,n){return a.def(h(this,"WeakMap"),t,n)}},S=t.exports=e(58)("WeakMap",d,_,a,!0,!0);l(function(){return 7!=(new S).set((Object.freeze||Object)(y),7).get(y)})&&(f((i=a.getConstructor(d,"WeakMap")).prototype,_),c.NEED=!0,o(["delete","has","get","set"],function(t){var n=S.prototype,r=n[t];u(n,t,function(n,e){if(s(n)&&!v(n)){this._f||(this._f=new i);var o=this._f[t](n,e);return"set"==t?this:o}return r.call(this,n,e)})}))},function(t,n,e){var i=e(41),o=e(29).getWeak,u=e(1),c=e(4),f=e(39),a=e(40),s=e(25),l=e(11),h=e(45),p=s(5),v=s(6),g=0,y=function(t){return t._l||(t._l=new d)},d=function(){this.a=[]},_=function(t,n){return p(t.a,function(t){return t[0]===n})};d.prototype={get:function(t){var n=_(this,t);if(n)return n[1]},has:function(t){return!!_(this,t)},set:function(t,n){var r=_(this,t);r?r[1]=n:this.a.push([t,n])},"delete":function(t){var n=v(this.a,function(n){return n[0]===t});return~n&&this.a.splice(n,1),!!~n}},t.exports={getConstructor:function(t,n,e,u){var s=t(function(t,i){f(t,s,n,"_i"),t._t=n,t._i=g++,t._l=r,i!=r&&a(i,e,t[u],t)});return i(s.prototype,{"delete":function(t){if(!c(t))return!1;var r=o(t);return!0===r?y(h(this,n))["delete"](t):r&&l(r,this._i)&&delete r[this._i]},has:function has(t){if(!c(t))return!1;var r=o(t);return!0===r?y(h(this,n)).has(t):r&&l(r,this._i)}}),s},def:function(t,n,r){var e=o(u(n),!0);return!0===e?y(t).set(n,r):e[t._i]=r,t},ufstore:y}},function(t,n,r){var e=r(37),i=r(51),o=r(1),u=r(2).Reflect;t.exports=u&&u.ownKeys||function ownKeys(t){var n=e.f(o(t)),r=i.f;return r?n.concat(r(t)):n}},function(t,n,e){var i=e(23),o=e(8);t.exports=function(t){if(t===r)return 0;var n=i(t),e=o(n);if(n!==e)throw RangeError("Wrong length!");return e}},function(t,n,e){function flattenIntoArray(t,n,e,a,s,l,h,p){for(var v,g,y=s,d=0,_=!!h&&c(h,p,3);d<a;){if(d in e){if(v=_?_(e[d],d,n):e[d],g=!1,o(v)&&(g=(g=v[f])!==r?!!g:i(v)),g&&l>0)y=flattenIntoArray(t,n,v,u(v.length),y,l-1)-1;else{if(y>=9007199254740991)throw TypeError();t[y]=v}y++}d++}return y}var i=e(52),o=e(4),u=e(8),c=e(18),f=e(5)("isConcatSpreadable");t.exports=flattenIntoArray},function(t,n,e){var i=e(8),o=e(71),u=e(22);t.exports=function(t,n,e,c){var f=String(u(t)),a=f.length,s=e===r?" ":String(e),l=i(n);if(l<=a||""==s)return f;var h=l-a,p=o.call(s,Math.ceil(h/s.length));return p.length>h&&(p=p.slice(0,h)),c?p+f:f+p}},function(t,n,r){var e=r(34),i=r(15),o=r(47).f;t.exports=function(t){return function(n){for(var r,u=i(n),c=e(u),f=c.length,a=0,s=[];f>a;)o.call(u,r=c[a++])&&s.push(t?[r,u[r]]:u[r]);return s}}},function(t,n,r){var e=r(48),i=r(121);t.exports=function(t){return function toJSON(){if(e(this)!=t)throw TypeError(t+"#toJSON isn't generic");return i(this)}}},function(t,n,r){var e=r(40);t.exports=function(t,n){var r=[];return e(t,!1,r.push,r,n),r}},function(t,n){t.exports=Math.scale||function scale(t,n,r,e,i){return 0===arguments.length||t!=t||n!=n||r!=r||e!=e||i!=i?NaN:t===Infinity||t===-Infinity?t:(t-n)*(i-e)/(r-n)+e}},function(t,n,r){r(124),r(126),r(127),r(128),r(129),r(130),r(131),r(132),r(133),r(134),r(135),r(136),r(137),r(138),r(139),r(140),r(142),r(143),r(144),r(145),r(146),r(147),r(148),r(149),r(150),r(151),r(152),r(153),r(154),r(155),r(156),r(157),r(158),r(159),r(160),r(161),r(162),r(163),r(164),r(165),r(166),r(167),r(168),r(169),r(170),r(171),r(172),r(173),r(174),r(175),r(176),r(177),r(178),r(179),r(180),r(181),r(182),r(183),r(184),r(185),r(186),r(187),r(188),r(189),r(190),r(191),r(192),r(193),r(194),r(195),r(196),r(197),r(198),r(199),r(200),r(201),r(202),r(203),r(204),r(205),r(206),r(208),r(209),r(210),r(211),r(212),r(213),r(214),r(215),r(216),r(217),r(218),r(219),r(84),r(220),r(221),r(222),r(107),r(223),r(224),r(225),r(226),r(227),r(110),r(112),r(113),r(228),r(229),r(230),r(231),r(232),r(233),r(234),r(235),r(236),r(237),r(238),r(239),r(240),r(241),r(242),r(243),r(244),r(245),r(247),r(248),r(250),r(251),r(252),r(253),r(254),r(255),r(256),r(257),r(258),r(259),r(260),r(261),r(262),r(263),r(264),r(265),r(266),r(267),r(268),r(269),r(270),r(271),r(272),r(273),r(274),r(275),r(276),r(277),r(278),r(279),r(280),r(281),r(282),r(283),r(284),r(285),r(286),r(287),r(288),r(289),r(290),r(291),r(292),r(293),r(294),r(295),r(296),r(297),r(298),r(299),r(300),r(301),r(302),r(303),r(304),r(305),r(306),r(307),r(308),r(309),r(310),r(311),r(312),r(313),r(314),r(315),r(316),r(317),r(318),t.exports=r(319)},function(t,n,e){var i=e(2),o=e(11),u=e(6),c=e(0),f=e(13),a=e(29).KEY,s=e(3),l=e(49),h=e(42),p=e(32),v=e(5),g=e(90),y=e(64),d=e(125),_=e(52),S=e(1),b=e(15),m=e(21),x=e(31),w=e(36),E=e(93),O=e(16),P=e(7),M=e(34),I=O.f,F=P.f,A=E.f,k=i.Symbol,N=i.JSON,j=N&&N.stringify,R=v("_hidden"),T=v("toPrimitive"),L={}.propertyIsEnumerable,D=l("symbol-registry"),W=l("symbols"),C=l("op-symbols"),U=Object.prototype,G="function"==typeof k,B=i.QObject,V=!B||!B.prototype||!B.prototype.findChild,z=u&&s(function(){return 7!=w(F({},"a",{get:function(){return F(this,"a",{value:7}).a}})).a})?function(t,n,r){var e=I(U,n);e&&delete U[n],F(t,n,r),e&&t!==U&&F(U,n,e)}:F,q=function(t){var n=W[t]=w(k.prototype);return n._k=t,n},K=G&&"symbol"==typeof k.iterator?function(t){return"symbol"==typeof t}:function(t){return t instanceof k},J=function defineProperty(t,n,r){return t===U&&J(C,n,r),S(t),n=m(n,!0),S(r),o(W,n)?(r.enumerable?(o(t,R)&&t[R][n]&&(t[R][n]=!1),r=w(r,{enumerable:x(0,!1)})):(o(t,R)||F(t,R,x(1,{})),t[R][n]=!0),z(t,n,r)):F(t,n,r)},Y=function defineProperties(t,n){S(t);for(var r,e=d(n=b(n)),i=0,o=e.length;o>i;)J(t,r=e[i++],n[r]);return t},H=function propertyIsEnumerable(t){var n=L.call(this,t=m(t,!0));return!(this===U&&o(W,t)&&!o(C,t))&&(!(n||!o(this,t)||!o(W,t)||o(this,R)&&this[R][t])||n)},X=function getOwnPropertyDescriptor(t,n){if(t=b(t),n=m(n,!0),t!==U||!o(W,n)||o(C,n)){var r=I(t,n);return!r||!o(W,n)||o(t,R)&&t[R][n]||(r.enumerable=!0),r}},Z=function getOwnPropertyNames(t){for(var n,r=A(b(t)),e=[],i=0;r.length>i;)o(W,n=r[i++])||n==R||n==a||e.push(n);return e},$=function getOwnPropertySymbols(t){for(var n,r=t===U,e=A(r?C:b(t)),i=[],u=0;e.length>u;)!o(W,n=e[u++])||r&&!o(U,n)||i.push(W[n]);return i};G||(f((k=function Symbol(){if(this instanceof k)throw TypeError("Symbol is not a constructor!");var t=p(arguments.length>0?arguments[0]:r),n=function(r){this===U&&n.call(C,r),o(this,R)&&o(this[R],t)&&(this[R][t]=!1),z(this,t,x(1,r))};return u&&V&&z(U,t,{configurable:!0,set:n}),q(t)}).prototype,"toString",function toString(){return this._k}),O.f=X,P.f=J,e(37).f=E.f=Z,e(47).f=H,e(51).f=$,u&&!e(33)&&f(U,"propertyIsEnumerable",H,!0),g.f=function(t){return q(v(t))}),c(c.G+c.W+c.F*!G,{Symbol:k});for(var Q="hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables".split(","),tt=0;Q.length>tt;)v(Q[tt++]);for(var nt=M(v.store),rt=0;nt.length>rt;)y(nt[rt++]);c(c.S+c.F*!G,"Symbol",{"for":function(t){return o(D,t+="")?D[t]:D[t]=k(t)},keyFor:function keyFor(t){if(!K(t))throw TypeError(t+" is not a symbol!");for(var n in D)if(D[n]===t)return n},useSetter:function(){V=!0},useSimple:function(){V=!1}}),c(c.S+c.F*!G,"Object",{create:function create(t,n){return n===r?w(t):Y(w(t),n)},defineProperty:J,defineProperties:Y,getOwnPropertyDescriptor:X,getOwnPropertyNames:Z,getOwnPropertySymbols:$}),N&&c(c.S+c.F*(!G||s(function(){var t=k();return"[null]"!=j([t])||"{}"!=j({a:t})||"{}"!=j(Object(t))})),"JSON",{stringify:function stringify(t){if(t!==r&&!K(t)){for(var n,e,i=[t],o=1;arguments.length>o;)i.push(arguments[o++]);return"function"==typeof(n=i[1])&&(e=n),!e&&_(n)||(n=function(t,n){if(e&&(n=e.call(this,t,n)),!K(n))return n}),i[1]=n,j.apply(N,i)}}}),k.prototype[T]||e(12)(k.prototype,T,k.prototype.valueOf),h(k,"Symbol"),h(Math,"Math",!0),h(i.JSON,"JSON",!0)},function(t,n,r){var e=r(34),i=r(51),o=r(47);t.exports=function(t){var n=e(t),r=i.f;if(r)for(var u,c=r(t),f=o.f,a=0;c.length>a;)f.call(t,u=c[a++])&&n.push(u);return n}},function(t,n,r){var e=r(0);e(e.S+e.F*!r(6),"Object",{defineProperty:r(7).f})},function(t,n,r){var e=r(0);e(e.S+e.F*!r(6),"Object",{defineProperties:r(92)})},function(t,n,r){var e=r(15),i=r(16).f;r(24)("getOwnPropertyDescriptor",function(){return function getOwnPropertyDescriptor(t,n){return i(e(t),n)}})},function(t,n,r){var e=r(0);e(e.S,"Object",{create:r(36)})},function(t,n,r){var e=r(9),i=r(17);r(24)("getPrototypeOf",function(){return function getPrototypeOf(t){return i(e(t))}})},function(t,n,r){var e=r(9),i=r(34);r(24)("keys",function(){return function keys(t){return i(e(t))}})},function(t,n,r){r(24)("getOwnPropertyNames",function(){return r(93).f})},function(t,n,r){var e=r(4),i=r(29).onFreeze;r(24)("freeze",function(t){return function freeze(n){return t&&e(n)?t(i(n)):n}})},function(t,n,r){var e=r(4),i=r(29).onFreeze;r(24)("seal",function(t){return function seal(n){return t&&e(n)?t(i(n)):n}})},function(t,n,r){var e=r(4),i=r(29).onFreeze;r(24)("preventExtensions",function(t){return function preventExtensions(n){return t&&e(n)?t(i(n)):n}})},function(t,n,r){var e=r(4);r(24)("isFrozen",function(t){return function isFrozen(n){return!e(n)||!!t&&t(n)}})},function(t,n,r){var e=r(4);r(24)("isSealed",function(t){return function isSealed(n){return!e(n)||!!t&&t(n)}})},function(t,n,r){var e=r(4);r(24)("isExtensible",function(t){return function isExtensible(n){return!!e(n)&&(!t||t(n))}})},function(t,n,r){var e=r(0);e(e.S+e.F,"Object",{assign:r(94)})},function(t,n,r){var e=r(0);e(e.S,"Object",{is:r(141)})},function(t,n){t.exports=Object.is||function is(t,n){return t===n?0!==t||1/t==1/n:t!=t&&n!=n}},function(t,n,r){var e=r(0);e(e.S,"Object",{setPrototypeOf:r(68).set})},function(t,n,r){var e=r(48),i={};i[r(5)("toStringTag")]="z",i+""!="[object z]"&&r(13)(Object.prototype,"toString",function toString(){return"[object "+e(this)+"]"},!0)},function(t,n,r){var e=r(0);e(e.P,"Function",{bind:r(95)})},function(t,n,r){var e=r(7).f,i=Function.prototype,o=/^\s*function ([^ (]*)/;"name"in i||r(6)&&e(i,"name",{configurable:!0,get:function(){try{return(""+this).match(o)[1]}catch(t){return""}}})},function(t,n,r){var e=r(4),i=r(17),o=r(5)("hasInstance"),u=Function.prototype;o in u||r(7).f(u,o,{value:function(t){if("function"!=typeof this||!e(t))return!1;if(!e(this.prototype))return t instanceof this;for(;t=i(t);)if(this.prototype===t)return!0;return!1}})},function(t,n,r){var e=r(2),i=r(11),o=r(19),u=r(69),c=r(21),f=r(3),a=r(37).f,s=r(16).f,l=r(7).f,h=r(43).trim,p=e.Number,v=p,g=p.prototype,y="Number"==o(r(36)(g)),d="trim"in String.prototype,_=function(t){var n=c(t,!1);if("string"==typeof n&&n.length>2){var r,e,i,o=(n=d?n.trim():h(n,3)).charCodeAt(0);if(43===o||45===o){if(88===(r=n.charCodeAt(2))||120===r)return NaN}else if(48===o){switch(n.charCodeAt(1)){case 66:case 98:e=2,i=49;break;case 79:case 111:e=8,i=55;break;default:return+n}for(var u,f=n.slice(2),a=0,s=f.length;a<s;a++)if((u=f.charCodeAt(a))<48||u>i)return NaN;return parseInt(f,e)}}return+n};if(!p(" 0o1")||!p("0b1")||p("+0x1")){p=function Number(t){var n=arguments.length<1?0:t,r=this;return r instanceof p&&(y?f(function(){g.valueOf.call(r)}):"Number"!=o(r))?u(new v(_(n)),r,p):_(n)};for(var S,b=r(6)?a(v):"MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger".split(","),m=0;b.length>m;m++)i(v,S=b[m])&&!i(p,S)&&l(p,S,s(v,S));p.prototype=g,g.constructor=p,r(13)(e,"Number",p)}},function(t,n,r){var e=r(0),i=r(23),o=r(97),u=r(71),c=1..toFixed,f=Math.floor,a=[0,0,0,0,0,0],s="Number.toFixed: incorrect invocation!",l=function(t,n){for(var r=-1,e=n;++r<6;)e+=t*a[r],a[r]=e%1e7,e=f(e/1e7)},h=function(t){for(var n=6,r=0;--n>=0;)r+=a[n],a[n]=f(r/t),r=r%t*1e7},p=function(){for(var t=6,n="";--t>=0;)if(""!==n||0===t||0!==a[t]){var r=String(a[t]);n=""===n?r:n+u.call("0",7-r.length)+r}return n},v=function(t,n,r){return 0===n?r:n%2==1?v(t,n-1,r*t):v(t*t,n/2,r)},g=function(t){for(var n=0,r=t;r>=4096;)n+=12,r/=4096;for(;r>=2;)n+=1,r/=2;return n};e(e.P+e.F*(!!c&&("0.000"!==8e-5.toFixed(3)||"1"!==.9.toFixed(0)||"1.25"!==1.255.toFixed(2)||"1000000000000000128"!==(0xde0b6b3a7640080).toFixed(0))||!r(3)(function(){c.call({})})),"Number",{toFixed:function toFixed(t){var n,r,e,c,f=o(this,s),a=i(t),y="",d="0";if(a<0||a>20)throw RangeError(s);if(f!=f)return"NaN";if(f<=-1e21||f>=1e21)return String(f);if(f<0&&(y="-",f=-f),f>1e-21)if(n=g(f*v(2,69,1))-69,r=n<0?f*v(2,-n,1):f/v(2,n,1),r*=4503599627370496,(n=52-n)>0){for(l(0,r),e=a;e>=7;)l(1e7,0),e-=7;for(l(v(10,e,1),0),e=n-1;e>=23;)h(1<<23),e-=23;h(1<<e),l(1,1),h(2),d=p()}else l(0,r),l(1<<-n,0),d=p()+u.call("0",a);return d=a>0?y+((c=d.length)<=a?"0."+u.call("0",a-c)+d:d.slice(0,c-a)+"."+d.slice(c-a)):y+d}})},function(t,n,e){var i=e(0),o=e(3),u=e(97),c=1..toPrecision;i(i.P+i.F*(o(function(){return"1"!==c.call(1,r)})||!o(function(){c.call({})})),"Number",{toPrecision:function toPrecision(t){var n=u(this,"Number#toPrecision: incorrect invocation!");return t===r?c.call(n):c.call(n,t)}})},function(t,n,r){var e=r(0);e(e.S,"Number",{EPSILON:Math.pow(2,-52)})},function(t,n,r){var e=r(0),i=r(2).isFinite;e(e.S,"Number",{isFinite:function isFinite(t){return"number"==typeof t&&i(t)}})},function(t,n,r){var e=r(0);e(e.S,"Number",{isInteger:r(98)})},function(t,n,r){var e=r(0);e(e.S,"Number",{isNaN:function isNaN(t){return t!=t}})},function(t,n,r){var e=r(0),i=r(98),o=Math.abs;e(e.S,"Number",{isSafeInteger:function isSafeInteger(t){return i(t)&&o(t)<=9007199254740991}})},function(t,n,r){var e=r(0);e(e.S,"Number",{MAX_SAFE_INTEGER:9007199254740991})},function(t,n,r){var e=r(0);e(e.S,"Number",{MIN_SAFE_INTEGER:-9007199254740991})},function(t,n,r){var e=r(0),i=r(99);e(e.S+e.F*(Number.parseFloat!=i),"Number",{parseFloat:i})},function(t,n,r){var e=r(0),i=r(100);e(e.S+e.F*(Number.parseInt!=i),"Number",{parseInt:i})},function(t,n,r){var e=r(0),i=r(100);e(e.G+e.F*(parseInt!=i),{parseInt:i})},function(t,n,r){var e=r(0),i=r(99);e(e.G+e.F*(parseFloat!=i),{parseFloat:i})},function(t,n,r){var e=r(0),i=r(101),o=Math.sqrt,u=Math.acosh;e(e.S+e.F*!(u&&710==Math.floor(u(Number.MAX_VALUE))&&u(Infinity)==Infinity),"Math",{acosh:function acosh(t){return(t=+t)<1?NaN:t>94906265.62425156?Math.log(t)+Math.LN2:i(t-1+o(t-1)*o(t+1))}})},function(t,n,r){function asinh(t){return isFinite(t=+t)&&0!=t?t<0?-asinh(-t):Math.log(t+Math.sqrt(t*t+1)):t}var e=r(0),i=Math.asinh;e(e.S+e.F*!(i&&1/i(0)>0),"Math",{asinh:asinh})},function(t,n,r){var e=r(0),i=Math.atanh;e(e.S+e.F*!(i&&1/i(-0)<0),"Math",{atanh:function atanh(t){return 0==(t=+t)?t:Math.log((1+t)/(1-t))/2}})},function(t,n,r){var e=r(0),i=r(72);e(e.S,"Math",{cbrt:function cbrt(t){return i(t=+t)*Math.pow(Math.abs(t),1/3)}})},function(t,n,r){var e=r(0);e(e.S,"Math",{clz32:function clz32(t){return(t>>>=0)?31-Math.floor(Math.log(t+.5)*Math.LOG2E):32}})},function(t,n,r){var e=r(0),i=Math.exp;e(e.S,"Math",{cosh:function cosh(t){return(i(t=+t)+i(-t))/2}})},function(t,n,r){var e=r(0),i=r(73);e(e.S+e.F*(i!=Math.expm1),"Math",{expm1:i})},function(t,n,r){var e=r(0);e(e.S,"Math",{fround:r(102)})},function(t,n,r){var e=r(0),i=Math.abs;e(e.S,"Math",{hypot:function hypot(t,n){for(var r,e,o=0,u=0,c=arguments.length,f=0;u<c;)f<(r=i(arguments[u++]))?(o=o*(e=f/r)*e+1,f=r):o+=r>0?(e=r/f)*e:r;return f===Infinity?Infinity:f*Math.sqrt(o)}})},function(t,n,r){var e=r(0),i=Math.imul;e(e.S+e.F*r(3)(function(){return-5!=i(4294967295,5)||2!=i.length}),"Math",{imul:function imul(t,n){var r=+t,e=+n,i=65535&r,o=65535&e;return 0|i*o+((65535&r>>>16)*o+i*(65535&e>>>16)<<16>>>0)}})},function(t,n,r){var e=r(0);e(e.S,"Math",{log10:function log10(t){return Math.log(t)*Math.LOG10E}})},function(t,n,r){var e=r(0);e(e.S,"Math",{log1p:r(101)})},function(t,n,r){var e=r(0);e(e.S,"Math",{log2:function log2(t){return Math.log(t)/Math.LN2}})},function(t,n,r){var e=r(0);e(e.S,"Math",{sign:r(72)})},function(t,n,r){var e=r(0),i=r(73),o=Math.exp;e(e.S+e.F*r(3)(function(){return-2e-17!=!Math.sinh(-2e-17)}),"Math",{sinh:function sinh(t){return Math.abs(t=+t)<1?(i(t)-i(-t))/2:(o(t-1)-o(-t-1))*(Math.E/2)}})},function(t,n,r){var e=r(0),i=r(73),o=Math.exp;e(e.S,"Math",{tanh:function tanh(t){var n=i(t=+t),r=i(-t);return n==Infinity?1:r==Infinity?-1:(n-r)/(o(t)+o(-t))}})},function(t,n,r){var e=r(0);e(e.S,"Math",{trunc:function trunc(t){return(t>0?Math.floor:Math.ceil)(t)}})},function(t,n,r){var e=r(0),i=r(35),o=String.fromCharCode,u=String.fromCodePoint;e(e.S+e.F*(!!u&&1!=u.length),"String",{fromCodePoint:function fromCodePoint(t){for(var n,r=[],e=arguments.length,u=0;e>u;){if(n=+arguments[u++],i(n,1114111)!==n)throw RangeError(n+" is not a valid code point");r.push(n<65536?o(n):o(55296+((n-=65536)>>10),n%1024+56320))}return r.join("")}})},function(t,n,r){var e=r(0),i=r(15),o=r(8);e(e.S,"String",{raw:function raw(t){for(var n=i(t.raw),r=o(n.length),e=arguments.length,u=[],c=0;r>c;)u.push(String(n[c++])),c<e&&u.push(String(arguments[c]));return u.join("")}})},function(t,n,r){r(43)("trim",function(t){return function trim(){return t(this,3)}})},function(t,n,r){var e=r(0),i=r(74)(!1);e(e.P,"String",{codePointAt:function codePointAt(t){return i(this,t)}})},function(t,n,e){var i=e(0),o=e(8),u=e(75),c="".endsWith;i(i.P+i.F*e(76)("endsWith"),"String",{endsWith:function endsWith(t){var n=u(this,t,"endsWith"),e=arguments.length>1?arguments[1]:r,i=o(n.length),f=e===r?i:Math.min(o(e),i),a=String(t);return c?c.call(n,a,f):n.slice(f-a.length,f)===a}})},function(t,n,e){var i=e(0),o=e(75);i(i.P+i.F*e(76)("includes"),"String",{includes:function includes(t){return!!~o(this,t,"includes").indexOf(t,arguments.length>1?arguments[1]:r)}})},function(t,n,r){var e=r(0);e(e.P,"String",{repeat:r(71)})},function(t,n,e){var i=e(0),o=e(8),u=e(75),c="".startsWith;i(i.P+i.F*e(76)("startsWith"),"String",{startsWith:function startsWith(t){var n=u(this,t,"startsWith"),e=o(Math.min(arguments.length>1?arguments[1]:r,n.length)),i=String(t);return c?c.call(n,i,e):n.slice(e,e+i.length)===i}})},function(t,n,e){var i=e(74)(!0);e(77)(String,"String",function(t){this._t=String(t),this._i=0},function(){var t,n=this._t,e=this._i;return e>=n.length?{value:r,done:!0}:(t=i(n,e),this._i+=t.length,{value:t,done:!1})})},function(t,n,r){r(14)("anchor",function(t){return function anchor(n){return t(this,"a","name",n)}})},function(t,n,r){r(14)("big",function(t){return function big(){return t(this,"big","","")}})},function(t,n,r){r(14)("blink",function(t){return function blink(){return t(this,"blink","","")}})},function(t,n,r){r(14)("bold",function(t){return function bold(){return t(this,"b","","")}})},function(t,n,r){r(14)("fixed",function(t){return function fixed(){return t(this,"tt","","")}})},function(t,n,r){r(14)("fontcolor",function(t){return function fontcolor(n){return t(this,"font","color",n)}})},function(t,n,r){r(14)("fontsize",function(t){return function fontsize(n){return t(this,"font","size",n)}})},function(t,n,r){r(14)("italics",function(t){return function italics(){return t(this,"i","","")}})},function(t,n,r){r(14)("link",function(t){return function link(n){return t(this,"a","href",n)}})},function(t,n,r){r(14)("small",function(t){return function small(){return t(this,"small","","")}})},function(t,n,r){r(14)("strike",function(t){return function strike(){return t(this,"strike","","")}})},function(t,n,r){r(14)("sub",function(t){return function sub(){return t(this,"sub","","")}})},function(t,n,r){r(14)("sup",function(t){return function sup(){return t(this,"sup","","")}})},function(t,n,r){var e=r(0);e(e.S,"Array",{isArray:r(52)})},function(t,n,e){var i=e(18),o=e(0),u=e(9),c=e(103),f=e(79),a=e(8),s=e(80),l=e(81);o(o.S+o.F*!e(54)(function(t){Array.from(t)}),"Array",{from:function from(t){var n,e,o,h,p=u(t),v="function"==typeof this?this:Array,g=arguments.length,y=g>1?arguments[1]:r,d=y!==r,_=0,S=l(p);if(d&&(y=i(y,g>2?arguments[2]:r,2)),S==r||v==Array&&f(S))for(e=new v(n=a(p.length));n>_;_++)s(e,_,d?y(p[_],_):p[_]);else for(h=S.call(p),e=new v;!(o=h.next()).done;_++)s(e,_,d?c(h,y,[o.value,_],!0):o.value);return e.length=_,e}})},function(t,n,r){var e=r(0),i=r(80);e(e.S+e.F*r(3)(function(){function F(){}return!(Array.of.call(F)instanceof F)}),"Array",{of:function of(){for(var t=0,n=arguments.length,r=new("function"==typeof this?this:Array)(n);n>t;)i(r,t,arguments[t++]);return r.length=n,r}})},function(t,n,e){var i=e(0),o=e(15),u=[].join;i(i.P+i.F*(e(46)!=Object||!e(20)(u)),"Array",{join:function join(t){return u.call(o(this),t===r?",":t)}})},function(t,n,e){var i=e(0),o=e(67),u=e(19),c=e(35),f=e(8),a=[].slice;i(i.P+i.F*e(3)(function(){o&&a.call(o)}),"Array",{slice:function slice(t,n){var e=f(this.length),i=u(this);if(n=n===r?e:n,"Array"==i)return a.call(this,t,n);for(var o=c(t,e),s=c(n,e),l=f(s-o),h=Array(l),p=0;p<l;p++)h[p]="String"==i?this.charAt(o+p):this[o+p];return h}})},function(t,n,e){var i=e(0),o=e(10),u=e(9),c=e(3),f=[].sort,a=[1,2,3];i(i.P+i.F*(c(function(){a.sort(r)})||!c(function(){a.sort(null)})||!e(20)(f)),"Array",{sort:function sort(t){return t===r?f.call(u(this)):f.call(u(this),o(t))}})},function(t,n,r){var e=r(0),i=r(25)(0),o=r(20)([].forEach,!0);e(e.P+e.F*!o,"Array",{forEach:function forEach(t){return i(this,t,arguments[1])}})},function(t,n,e){var i=e(4),o=e(52),u=e(5)("species");t.exports=function(t){var n;return o(t)&&("function"!=typeof(n=t.constructor)||n!==Array&&!o(n.prototype)||(n=r),i(n)&&null===(n=n[u])&&(n=r)),n===r?Array:n}},function(t,n,r){var e=r(0),i=r(25)(1);e(e.P+e.F*!r(20)([].map,!0),"Array",{map:function map(t){return i(this,t,arguments[1])}})},function(t,n,r){var e=r(0),i=r(25)(2);e(e.P+e.F*!r(20)([].filter,!0),"Array",{filter:function filter(t){return i(this,t,arguments[1])}})},function(t,n,r){var e=r(0),i=r(25)(3);e(e.P+e.F*!r(20)([].some,!0),"Array",{some:function some(t){return i(this,t,arguments[1])}})},function(t,n,r){var e=r(0),i=r(25)(4);e(e.P+e.F*!r(20)([].every,!0),"Array",{every:function every(t){return i(this,t,arguments[1])}})},function(t,n,r){var e=r(0),i=r(104);e(e.P+e.F*!r(20)([].reduce,!0),"Array",{reduce:function reduce(t){return i(this,t,arguments.length,arguments[1],!1)}})},function(t,n,r){var e=r(0),i=r(104);e(e.P+e.F*!r(20)([].reduceRight,!0),"Array",{reduceRight:function reduceRight(t){return i(this,t,arguments.length,arguments[1],!0)}})},function(t,n,r){var e=r(0),i=r(50)(!1),o=[].indexOf,u=!!o&&1/[1].indexOf(1,-0)<0;e(e.P+e.F*(u||!r(20)(o)),"Array",{indexOf:function indexOf(t){return u?o.apply(this,arguments)||0:i(this,t,arguments[1])}})},function(t,n,r){var e=r(0),i=r(15),o=r(23),u=r(8),c=[].lastIndexOf,f=!!c&&1/[1].lastIndexOf(1,-0)<0;e(e.P+e.F*(f||!r(20)(c)),"Array",{lastIndexOf:function lastIndexOf(t){if(f)return c.apply(this,arguments)||0;var n=i(this),r=u(n.length),e=r-1;for(arguments.length>1&&(e=Math.min(e,o(arguments[1]))),e<0&&(e=r+e);e>=0;e--)if(e in n&&n[e]===t)return e||0;return-1}})},function(t,n,r){var e=r(0);e(e.P,"Array",{copyWithin:r(105)}),r(30)("copyWithin")},function(t,n,r){var e=r(0);e(e.P,"Array",{fill:r(83)}),r(30)("fill")},function(t,n,e){var i=e(0),o=e(25)(5),u=!0;"find"in[]&&Array(1).find(function(){u=!1}),i(i.P+i.F*u,"Array",{find:function find(t){return o(this,t,arguments.length>1?arguments[1]:r)}}),e(30)("find")},function(t,n,e){var i=e(0),o=e(25)(6),u="findIndex",c=!0;u in[]&&Array(1)[u](function(){c=!1}),i(i.P+i.F*c,"Array",{findIndex:function findIndex(t){return o(this,t,arguments.length>1?arguments[1]:r)}}),e(30)(u)},function(t,n,r){r(38)("Array")},function(t,n,e){var i=e(2),o=e(69),u=e(7).f,c=e(37).f,f=e(53),a=e(55),s=i.RegExp,l=s,h=s.prototype,p=/a/g,v=/a/g,g=new s(p)!==p;if(e(6)&&(!g||e(3)(function(){return v[e(5)("match")]=!1,s(p)!=p||s(v)==v||"/a/i"!=s(p,"i")}))){s=function RegExp(t,n){var e=this instanceof s,i=f(t),u=n===r;return!e&&i&&t.constructor===s&&u?t:o(g?new l(i&&!u?t.source:t,n):l((i=t instanceof s)?t.source:t,i&&u?a.call(t):n),e?this:h,s)};for(var y=c(l),d=0;y.length>d;)!function(t){t in s||u(s,t,{configurable:!0,get:function(){return l[t]},set:function(n){l[t]=n}})}(y[d++]);h.constructor=s,s.prototype=h,e(13)(i,"RegExp",s)}e(38)("RegExp")},function(t,n,e){e(107);var i=e(1),o=e(55),u=e(6),c=/./.toString,f=function(t){e(13)(RegExp.prototype,"toString",t,!0)};e(3)(function(){return"/a/b"!=c.call({source:"a",flags:"b"})})?f(function toString(){var t=i(this);return"/".concat(t.source,"/","flags"in t?t.flags:!u&&t instanceof RegExp?o.call(t):r)}):"toString"!=c.name&&f(function toString(){return c.call(this)})},function(t,n,e){e(56)("match",1,function(t,n,e){return[function match(e){var i=t(this),o=e==r?r:e[n];return o!==r?o.call(e,i):new RegExp(e)[n](String(i))},e]})},function(t,n,e){e(56)("replace",2,function(t,n,e){return[function replace(i,o){var u=t(this),c=i==r?r:i[n];return c!==r?c.call(i,u,o):e.call(String(u),i,o)},e]})},function(t,n,e){e(56)("search",1,function(t,n,e){return[function search(e){var i=t(this),o=e==r?r:e[n];return o!==r?o.call(e,i):new RegExp(e)[n](String(i))},e]})},function(t,n,e){e(56)("split",2,function(t,n,i){var o=e(53),u=i,c=[].push,f="length";if("c"=="abbc".split(/(b)*/)[1]||4!="test".split(/(?:)/,-1)[f]||2!="ab".split(/(?:ab)*/)[f]||4!=".".split(/(.?)(.?)/)[f]||".".split(/()()/)[f]>1||"".split(/.?/)[f]){var a=/()??/.exec("")[1]===r;i=function(t,n){var e=String(this);if(t===r&&0===n)return[];if(!o(t))return u.call(e,t,n);var i,s,l,h,p,v=[],g=(t.ignoreCase?"i":"")+(t.multiline?"m":"")+(t.unicode?"u":"")+(t.sticky?"y":""),y=0,d=n===r?4294967295:n>>>0,_=new RegExp(t.source,g+"g");for(a||(i=new RegExp("^"+_.source+"$(?!\\s)",g));(s=_.exec(e))&&!((l=s.index+s[0][f])>y&&(v.push(e.slice(y,s.index)),!a&&s[f]>1&&s[0].replace(i,function(){for(p=1;p<arguments[f]-2;p++)arguments[p]===r&&(s[p]=r)}),s[f]>1&&s.index<e[f]&&c.apply(v,s.slice(1)),h=s[0][f],y=l,v[f]>=d));)_.lastIndex===s.index&&_.lastIndex++;return y===e[f]?!h&&_.test("")||v.push(""):v.push(e.slice(y)),v[f]>d?v.slice(0,d):v}}else"0".split(r,0)[f]&&(i=function(t,n){return t===r&&0===n?[]:u.call(this,t,n)});return[function split(e,o){var u=t(this),c=e==r?r:e[n];return c!==r?c.call(e,u,o):i.call(String(u),e,o)},i]})},function(t,n,e){var i,o,u,c,f=e(33),a=e(2),s=e(18),l=e(48),h=e(0),p=e(4),v=e(10),g=e(39),y=e(40),d=e(57),_=e(85).set,S=e(86)(),b=e(87),m=e(108),x=e(109),w=a.TypeError,E=a.process,O=a.Promise,P="process"==l(E),M=function(){},I=o=b.f,F=!!function(){try{var t=O.resolve(1),n=(t.constructor={})[e(5)("species")]=function(t){t(M,M)};return(P||"function"==typeof PromiseRejectionEvent)&&t.then(M)instanceof n}catch(r){}}(),A=function(t){var n;return!(!p(t)||"function"!=typeof(n=t.then))&&n},k=function(t,n){if(!t._n){t._n=!0;var r=t._c;S(function(){for(var e=t._v,i=1==t._s,o=0;r.length>o;)!function(n){var r,o,u=i?n.ok:n.fail,c=n.resolve,f=n.reject,a=n.domain;try{u?(i||(2==t._h&&R(t),t._h=1),!0===u?r=e:(a&&a.enter(),r=u(e),a&&a.exit()),r===n.promise?f(w("Promise-chain cycle")):(o=A(r))?o.call(r,c,f):c(r)):f(e)}catch(s){f(s)}}(r[o++]);t._c=[],t._n=!1,n&&!t._h&&N(t)})}},N=function(t){_.call(a,function(){var n,e,i,o=t._v,u=j(t);if(u&&(n=m(function(){P?E.emit("unhandledRejection",o,t):(e=a.onunhandledrejection)?e({promise:t,reason:o}):(i=a.console)&&i.error&&i.error("Unhandled promise rejection",o)}),t._h=P||j(t)?2:1),t._a=r,
u&&n.e)throw n.v})},j=function(t){if(1==t._h)return!1;for(var n,r=t._a||t._c,e=0;r.length>e;)if((n=r[e++]).fail||!j(n.promise))return!1;return!0},R=function(t){_.call(a,function(){var n;P?E.emit("rejectionHandled",t):(n=a.onrejectionhandled)&&n({promise:t,reason:t._v})})},T=function(t){var n=this;n._d||(n._d=!0,(n=n._w||n)._v=t,n._s=2,n._a||(n._a=n._c.slice()),k(n,!0))},L=function(t){var n,r=this;if(!r._d){r._d=!0,r=r._w||r;try{if(r===t)throw w("Promise can't be resolved itself");(n=A(t))?S(function(){var e={_w:r,_d:!1};try{n.call(t,s(L,e,1),s(T,e,1))}catch(i){T.call(e,i)}}):(r._v=t,r._s=1,k(r,!1))}catch(e){T.call({_w:r,_d:!1},e)}}};F||(O=function Promise(t){g(this,O,"Promise","_h"),v(t),i.call(this);try{t(s(L,this,1),s(T,this,1))}catch(n){T.call(this,n)}},(i=function Promise(t){this._c=[],this._a=r,this._s=0,this._d=!1,this._v=r,this._h=0,this._n=!1}).prototype=e(41)(O.prototype,{then:function then(t,n){var e=I(d(this,O));return e.ok="function"!=typeof t||t,e.fail="function"==typeof n&&n,e.domain=P?E.domain:r,this._c.push(e),this._a&&this._a.push(e),this._s&&k(this,!1),e.promise},"catch":function(t){return this.then(r,t)}}),u=function(){var t=new i;this.promise=t,this.resolve=s(L,t,1),this.reject=s(T,t,1)},b.f=I=function(t){return t===O||t===c?new u(t):o(t)}),h(h.G+h.W+h.F*!F,{Promise:O}),e(42)(O,"Promise"),e(38)("Promise"),c=e(28).Promise,h(h.S+h.F*!F,"Promise",{reject:function reject(t){var n=I(this);return(0,n.reject)(t),n.promise}}),h(h.S+h.F*(f||!F),"Promise",{resolve:function resolve(t){return x(f&&this===c?O:this,t)}}),h(h.S+h.F*!(F&&e(54)(function(t){O.all(t)["catch"](M)})),"Promise",{all:function all(t){var n=this,e=I(n),i=e.resolve,o=e.reject,u=m(function(){var e=[],u=0,c=1;y(t,!1,function(t){var f=u++,a=!1;e.push(r),c++,n.resolve(t).then(function(t){a||(a=!0,e[f]=t,--c||i(e))},o)}),--c||i(e)});return u.e&&o(u.v),e.promise},race:function race(t){var n=this,r=I(n),e=r.reject,i=m(function(){y(t,!1,function(t){n.resolve(t).then(r.resolve,e)})});return i.e&&e(i.v),r.promise}})},function(t,n,e){var i=e(114),o=e(45);e(58)("WeakSet",function(t){return function WeakSet(){return t(this,arguments.length>0?arguments[0]:r)}},{add:function add(t){return i.def(o(this,"WeakSet"),t,!0)}},i,!1,!0)},function(t,n,r){var e=r(0),i=r(10),o=r(1),u=(r(2).Reflect||{}).apply,c=Function.apply;e(e.S+e.F*!r(3)(function(){u(function(){})}),"Reflect",{apply:function apply(t,n,r){var e=i(t),f=o(r);return u?u(e,n,f):c.call(e,n,f)}})},function(t,n,r){var e=r(0),i=r(36),o=r(10),u=r(1),c=r(4),f=r(3),a=r(95),s=(r(2).Reflect||{}).construct,l=f(function(){function F(){}return!(s(function(){},[],F)instanceof F)}),h=!f(function(){s(function(){})});e(e.S+e.F*(l||h),"Reflect",{construct:function construct(t,n){o(t),u(n);var r=arguments.length<3?t:o(arguments[2]);if(h&&!l)return s(t,n,r);if(t==r){switch(n.length){case 0:return new t;case 1:return new t(n[0]);case 2:return new t(n[0],n[1]);case 3:return new t(n[0],n[1],n[2]);case 4:return new t(n[0],n[1],n[2],n[3])}var e=[null];return e.push.apply(e,n),new(a.apply(t,e))}var f=r.prototype,p=i(c(f)?f:Object.prototype),v=Function.apply.call(t,p,n);return c(v)?v:p}})},function(t,n,r){var e=r(7),i=r(0),o=r(1),u=r(21);i(i.S+i.F*r(3)(function(){Reflect.defineProperty(e.f({},1,{value:1}),1,{value:2})}),"Reflect",{defineProperty:function defineProperty(t,n,r){o(t),n=u(n,!0),o(r);try{return e.f(t,n,r),!0}catch(i){return!1}}})},function(t,n,r){var e=r(0),i=r(16).f,o=r(1);e(e.S,"Reflect",{deleteProperty:function deleteProperty(t,n){var r=i(o(t),n);return!(r&&!r.configurable)&&delete t[n]}})},function(t,n,e){var i=e(0),o=e(1),u=function(t){this._t=o(t),this._i=0;var n,r=this._k=[];for(n in t)r.push(n)};e(78)(u,"Object",function(){var t,n=this,e=n._k;do{if(n._i>=e.length)return{value:r,done:!0}}while(!((t=e[n._i++])in n._t));return{value:t,done:!1}}),i(i.S,"Reflect",{enumerate:function enumerate(t){return new u(t)}})},function(t,n,e){function get(t,n){var e,c,s=arguments.length<3?t:arguments[2];return a(t)===s?t[n]:(e=i.f(t,n))?u(e,"value")?e.value:e.get!==r?e.get.call(s):r:f(c=o(t))?get(c,n,s):void 0}var i=e(16),o=e(17),u=e(11),c=e(0),f=e(4),a=e(1);c(c.S,"Reflect",{get:get})},function(t,n,r){var e=r(16),i=r(0),o=r(1);i(i.S,"Reflect",{getOwnPropertyDescriptor:function getOwnPropertyDescriptor(t,n){return e.f(o(t),n)}})},function(t,n,r){var e=r(0),i=r(17),o=r(1);e(e.S,"Reflect",{getPrototypeOf:function getPrototypeOf(t){return i(o(t))}})},function(t,n,r){var e=r(0);e(e.S,"Reflect",{has:function has(t,n){return n in t}})},function(t,n,r){var e=r(0),i=r(1),o=Object.isExtensible;e(e.S,"Reflect",{isExtensible:function isExtensible(t){return i(t),!o||o(t)}})},function(t,n,r){var e=r(0);e(e.S,"Reflect",{ownKeys:r(115)})},function(t,n,r){var e=r(0),i=r(1),o=Object.preventExtensions;e(e.S,"Reflect",{preventExtensions:function preventExtensions(t){i(t);try{return o&&o(t),!0}catch(n){return!1}}})},function(t,n,e){function set(t,n,e){var f,h,p=arguments.length<4?t:arguments[3],v=o.f(s(t),n);if(!v){if(l(h=u(t)))return set(h,n,e,p);v=a(0)}return c(v,"value")?!(!1===v.writable||!l(p))&&(f=o.f(p,n)||a(0),f.value=e,i.f(p,n,f),!0):v.set!==r&&(v.set.call(p,e),!0)}var i=e(7),o=e(16),u=e(17),c=e(11),f=e(0),a=e(31),s=e(1),l=e(4);f(f.S,"Reflect",{set:set})},function(t,n,r){var e=r(0),i=r(68);i&&e(e.S,"Reflect",{setPrototypeOf:function setPrototypeOf(t,n){i.check(t,n);try{return i.set(t,n),!0}catch(r){return!1}}})},function(t,n,r){var e=r(0);e(e.S,"Date",{now:function(){return(new Date).getTime()}})},function(t,n,r){var e=r(0),i=r(9),o=r(21);e(e.P+e.F*r(3)(function(){return null!==new Date(NaN).toJSON()||1!==Date.prototype.toJSON.call({toISOString:function(){return 1}})}),"Date",{toJSON:function toJSON(t){var n=i(this),r=o(n);return"number"!=typeof r||isFinite(r)?n.toISOString():null}})},function(t,n,r){var e=r(0),i=r(246);e(e.P+e.F*(Date.prototype.toISOString!==i),"Date",{toISOString:i})},function(t,n,r){var e=r(3),i=Date.prototype.getTime,o=Date.prototype.toISOString,u=function(t){return t>9?t:"0"+t};t.exports=e(function(){return"0385-07-25T07:06:39.999Z"!=o.call(new Date(-5e13-1))})||!e(function(){o.call(new Date(NaN))})?function toISOString(){if(!isFinite(i.call(this)))throw RangeError("Invalid time value");var t=this,n=t.getUTCFullYear(),r=t.getUTCMilliseconds(),e=n<0?"-":n>9999?"+":"";return e+("00000"+Math.abs(n)).slice(e?-6:-4)+"-"+u(t.getUTCMonth()+1)+"-"+u(t.getUTCDate())+"T"+u(t.getUTCHours())+":"+u(t.getUTCMinutes())+":"+u(t.getUTCSeconds())+"."+(r>99?r:"0"+u(r))+"Z"}:o},function(t,n,r){var e=Date.prototype,i=e.toString,o=e.getTime;new Date(NaN)+""!="Invalid Date"&&r(13)(e,"toString",function toString(){var t=o.call(this);return t===t?i.call(this):"Invalid Date"})},function(t,n,r){var e=r(5)("toPrimitive"),i=Date.prototype;e in i||r(12)(i,e,r(249))},function(t,n,r){var e=r(1),i=r(21);t.exports=function(t){if("string"!==t&&"number"!==t&&"default"!==t)throw TypeError("Incorrect hint");return i(e(this),"number"!=t)}},function(t,n,e){var i=e(0),o=e(59),u=e(88),c=e(1),f=e(35),a=e(8),s=e(4),l=e(2).ArrayBuffer,h=e(57),p=u.ArrayBuffer,v=u.DataView,g=o.ABV&&l.isView,y=p.prototype.slice,d=o.VIEW;i(i.G+i.W+i.F*(l!==p),{ArrayBuffer:p}),i(i.S+i.F*!o.CONSTR,"ArrayBuffer",{isView:function isView(t){return g&&g(t)||s(t)&&d in t}}),i(i.P+i.U+i.F*e(3)(function(){return!new p(2).slice(1,r).byteLength}),"ArrayBuffer",{slice:function slice(t,n){if(y!==r&&n===r)return y.call(c(this),t);for(var e=c(this).byteLength,i=f(t,e),o=f(n===r?e:n,e),u=new(h(this,p))(a(o-i)),s=new v(this),l=new v(u),g=0;i<o;)l.setUint8(g++,s.getUint8(i++));return u}}),e(38)("ArrayBuffer")},function(t,n,r){var e=r(0);e(e.G+e.W+e.F*!r(59).ABV,{DataView:r(88).DataView})},function(t,n,r){r(26)("Int8",1,function(t){return function Int8Array(n,r,e){return t(this,n,r,e)}})},function(t,n,r){r(26)("Uint8",1,function(t){return function Uint8Array(n,r,e){return t(this,n,r,e)}})},function(t,n,r){r(26)("Uint8",1,function(t){return function Uint8ClampedArray(n,r,e){return t(this,n,r,e)}},!0)},function(t,n,r){r(26)("Int16",2,function(t){return function Int16Array(n,r,e){return t(this,n,r,e)}})},function(t,n,r){r(26)("Uint16",2,function(t){return function Uint16Array(n,r,e){return t(this,n,r,e)}})},function(t,n,r){r(26)("Int32",4,function(t){return function Int32Array(n,r,e){return t(this,n,r,e)}})},function(t,n,r){r(26)("Uint32",4,function(t){return function Uint32Array(n,r,e){return t(this,n,r,e)}})},function(t,n,r){r(26)("Float32",4,function(t){return function Float32Array(n,r,e){return t(this,n,r,e)}})},function(t,n,r){r(26)("Float64",8,function(t){return function Float64Array(n,r,e){return t(this,n,r,e)}})},function(t,n,e){var i=e(0),o=e(50)(!0);i(i.P,"Array",{includes:function includes(t){return o(this,t,arguments.length>1?arguments[1]:r)}}),e(30)("includes")},function(t,n,r){var e=r(0),i=r(117),o=r(9),u=r(8),c=r(10),f=r(82);e(e.P,"Array",{flatMap:function flatMap(t){var n,r,e=o(this);return c(t),n=u(e.length),r=f(e,0),i(r,e,e,n,0,1,t,arguments[1]),r}}),r(30)("flatMap")},function(t,n,e){var i=e(0),o=e(117),u=e(9),c=e(8),f=e(23),a=e(82);i(i.P,"Array",{flatten:function flatten(){var t=arguments[0],n=u(this),e=c(n.length),i=a(n,0);return o(i,n,n,e,0,t===r?1:f(t)),i}}),e(30)("flatten")},function(t,n,r){var e=r(0),i=r(74)(!0);e(e.P,"String",{at:function at(t){return i(this,t)}})},function(t,n,e){var i=e(0),o=e(118);i(i.P,"String",{padStart:function padStart(t){return o(this,t,arguments.length>1?arguments[1]:r,!0)}})},function(t,n,e){var i=e(0),o=e(118);i(i.P,"String",{padEnd:function padEnd(t){return o(this,t,arguments.length>1?arguments[1]:r,!1)}})},function(t,n,r){r(43)("trimLeft",function(t){return function trimLeft(){return t(this,1)}},"trimStart")},function(t,n,r){r(43)("trimRight",function(t){return function trimRight(){return t(this,2)}},"trimEnd")},function(t,n,r){var e=r(0),i=r(22),o=r(8),u=r(53),c=r(55),f=RegExp.prototype,a=function(t,n){this._r=t,this._s=n};r(78)(a,"RegExp String",function next(){var t=this._r.exec(this._s);return{value:t,done:null===t}}),e(e.P,"String",{matchAll:function matchAll(t){if(i(this),!u(t))throw TypeError(t+" is not a regexp!");var n=String(this),r="flags"in f?String(t.flags):c.call(t),e=new RegExp(t.source,~r.indexOf("g")?r:"g"+r);return e.lastIndex=o(t.lastIndex),new a(e,n)}})},function(t,n,r){r(64)("asyncIterator")},function(t,n,r){r(64)("observable")},function(t,n,e){var i=e(0),o=e(115),u=e(15),c=e(16),f=e(80);i(i.S,"Object",{getOwnPropertyDescriptors:function getOwnPropertyDescriptors(t){for(var n,e,i=u(t),a=c.f,s=o(i),l={},h=0;s.length>h;)(e=a(i,n=s[h++]))!==r&&f(l,n,e);return l}})},function(t,n,r){var e=r(0),i=r(119)(!1);e(e.S,"Object",{values:function values(t){return i(t)}})},function(t,n,r){var e=r(0),i=r(119)(!0);e(e.S,"Object",{entries:function entries(t){return i(t)}})},function(t,n,r){var e=r(0),i=r(9),o=r(10),u=r(7);r(6)&&e(e.P+r(60),"Object",{__defineGetter__:function __defineGetter__(t,n){u.f(i(this),t,{get:o(n),enumerable:!0,configurable:!0})}})},function(t,n,r){var e=r(0),i=r(9),o=r(10),u=r(7);r(6)&&e(e.P+r(60),"Object",{__defineSetter__:function __defineSetter__(t,n){u.f(i(this),t,{set:o(n),enumerable:!0,configurable:!0})}})},function(t,n,r){var e=r(0),i=r(9),o=r(21),u=r(17),c=r(16).f;r(6)&&e(e.P+r(60),"Object",{__lookupGetter__:function __lookupGetter__(t){var n,r=i(this),e=o(t,!0);do{if(n=c(r,e))return n.get}while(r=u(r))}})},function(t,n,r){var e=r(0),i=r(9),o=r(21),u=r(17),c=r(16).f;r(6)&&e(e.P+r(60),"Object",{__lookupSetter__:function __lookupSetter__(t){var n,r=i(this),e=o(t,!0);do{if(n=c(r,e))return n.set}while(r=u(r))}})},function(t,n,r){var e=r(0);e(e.P+e.R,"Map",{toJSON:r(120)("Map")})},function(t,n,r){var e=r(0);e(e.P+e.R,"Set",{toJSON:r(120)("Set")})},function(t,n,r){r(61)("Map")},function(t,n,r){r(61)("Set")},function(t,n,r){r(61)("WeakMap")},function(t,n,r){r(61)("WeakSet")},function(t,n,r){r(62)("Map")},function(t,n,r){r(62)("Set")},function(t,n,r){r(62)("WeakMap")},function(t,n,r){r(62)("WeakSet")},function(t,n,r){var e=r(0);e(e.G,{global:r(2)})},function(t,n,r){var e=r(0);e(e.S,"System",{global:r(2)})},function(t,n,r){var e=r(0),i=r(19);e(e.S,"Error",{isError:function isError(t){return"Error"===i(t)}})},function(t,n,r){var e=r(0);e(e.S,"Math",{clamp:function clamp(t,n,r){return Math.min(r,Math.max(n,t))}})},function(t,n,r){var e=r(0);e(e.S,"Math",{DEG_PER_RAD:Math.PI/180})},function(t,n,r){var e=r(0),i=180/Math.PI;e(e.S,"Math",{degrees:function degrees(t){return t*i}})},function(t,n,r){var e=r(0),i=r(122),o=r(102);e(e.S,"Math",{fscale:function fscale(t,n,r,e,u){return o(i(t,n,r,e,u))}})},function(t,n,r){var e=r(0);e(e.S,"Math",{iaddh:function iaddh(t,n,r,e){var i=t>>>0,o=r>>>0;return(n>>>0)+(e>>>0)+((i&o|(i|o)&~(i+o>>>0))>>>31)|0}})},function(t,n,r){var e=r(0);e(e.S,"Math",{isubh:function isubh(t,n,r,e){var i=t>>>0,o=r>>>0;return(n>>>0)-(e>>>0)-((~i&o|~(i^o)&i-o>>>0)>>>31)|0}})},function(t,n,r){var e=r(0);e(e.S,"Math",{imulh:function imulh(t,n){var r=+t,e=+n,i=65535&r,o=65535&e,u=r>>16,c=e>>16,f=(u*o>>>0)+(i*o>>>16);return u*c+(f>>16)+((i*c>>>0)+(65535&f)>>16)}})},function(t,n,r){var e=r(0);e(e.S,"Math",{RAD_PER_DEG:180/Math.PI})},function(t,n,r){var e=r(0),i=Math.PI/180;e(e.S,"Math",{radians:function radians(t){return t*i}})},function(t,n,r){var e=r(0);e(e.S,"Math",{scale:r(122)})},function(t,n,r){var e=r(0);e(e.S,"Math",{umulh:function umulh(t,n){var r=+t,e=+n,i=65535&r,o=65535&e,u=r>>>16,c=e>>>16,f=(u*o>>>0)+(i*o>>>16);return u*c+(f>>>16)+((i*c>>>0)+(65535&f)>>>16)}})},function(t,n,r){var e=r(0);e(e.S,"Math",{signbit:function signbit(t){return(t=+t)!=t?t:0==t?1/t==Infinity:t>0}})},function(t,n,r){var e=r(0),i=r(28),o=r(2),u=r(57),c=r(109);e(e.P+e.R,"Promise",{"finally":function(t){var n=u(this,i.Promise||o.Promise),r="function"==typeof t;return this.then(r?function(r){return c(n,t()).then(function(){return r})}:t,r?function(r){return c(n,t()).then(function(){throw r})}:t)}})},function(t,n,r){var e=r(0),i=r(87),o=r(108);e(e.S,"Promise",{"try":function(t){var n=i.f(this),r=o(t);return(r.e?n.reject:n.resolve)(r.v),n.promise}})},function(t,n,r){var e=r(27),i=r(1),o=e.key,u=e.set;e.exp({defineMetadata:function defineMetadata(t,n,r,e){u(t,n,i(r),o(e))}})},function(t,n,e){var i=e(27),o=e(1),u=i.key,c=i.map,f=i.store;i.exp({deleteMetadata:function deleteMetadata(t,n){var e=arguments.length<3?r:u(arguments[2]),i=c(o(n),e,!1);if(i===r||!i["delete"](t))return!1;if(i.size)return!0;var a=f.get(n);return a["delete"](e),!!a.size||f["delete"](n)}})},function(t,n,e){var i=e(27),o=e(1),u=e(17),c=i.has,f=i.get,a=i.key,s=function(t,n,e){if(c(t,n,e))return f(t,n,e);var i=u(n);return null!==i?s(t,i,e):r};i.exp({getMetadata:function getMetadata(t,n){return s(t,o(n),arguments.length<3?r:a(arguments[2]))}})},function(t,n,e){var i=e(112),o=e(121),u=e(27),c=e(1),f=e(17),a=u.keys,s=u.key,l=function(t,n){var r=a(t,n),e=f(t);if(null===e)return r;var u=l(e,n);return u.length?r.length?o(new i(r.concat(u))):u:r};u.exp({getMetadataKeys:function getMetadataKeys(t){return l(c(t),arguments.length<2?r:s(arguments[1]))}})},function(t,n,e){var i=e(27),o=e(1),u=i.get,c=i.key;i.exp({getOwnMetadata:function getOwnMetadata(t,n){return u(t,o(n),arguments.length<3?r:c(arguments[2]))}})},function(t,n,e){var i=e(27),o=e(1),u=i.keys,c=i.key;i.exp({getOwnMetadataKeys:function getOwnMetadataKeys(t){return u(o(t),arguments.length<2?r:c(arguments[1]))}})},function(t,n,e){var i=e(27),o=e(1),u=e(17),c=i.has,f=i.key,a=function(t,n,r){if(c(t,n,r))return!0;var e=u(n);return null!==e&&a(t,e,r)};i.exp({hasMetadata:function hasMetadata(t,n){return a(t,o(n),arguments.length<3?r:f(arguments[2]))}})},function(t,n,e){var i=e(27),o=e(1),u=i.has,c=i.key;i.exp({hasOwnMetadata:function hasOwnMetadata(t,n){return u(t,o(n),arguments.length<3?r:c(arguments[2]))}})},function(t,n,e){var i=e(27),o=e(1),u=e(10),c=i.key,f=i.set;i.exp({metadata:function metadata(t,n){return function decorator(e,i){f(t,n,(i!==r?o:u)(e),c(i))}}})},function(t,n,r){var e=r(0),i=r(86)(),o=r(2).process,u="process"==r(19)(o);e(e.G,{asap:function asap(t){var n=u&&o.domain;i(n?n.bind(t):t)}})},function(t,n,e){var i=e(0),o=e(2),u=e(28),c=e(86)(),f=e(5)("observable"),a=e(10),s=e(1),l=e(39),h=e(41),p=e(12),v=e(40),g=v.RETURN,y=function(t){return null==t?r:a(t)},d=function(t){var n=t._c;n&&(t._c=r,n())},_=function(t){return t._o===r},S=function(t){_(t)||(t._o=r,d(t))},b=function(t,n){s(t),this._c=r,this._o=t,t=new m(this);try{var e=n(t),i=e;null!=e&&("function"==typeof e.unsubscribe?e=function(){i.unsubscribe()}:a(e),this._c=e)}catch(o){return void t.error(o)}_(this)&&d(this)};b.prototype=h({},{unsubscribe:function unsubscribe(){S(this)}});var m=function(t){this._s=t};m.prototype=h({},{next:function next(t){var n=this._s;if(!_(n)){var r=n._o;try{var e=y(r.next);if(e)return e.call(r,t)}catch(i){try{S(n)}finally{throw i}}}},error:function error(t){var n=this._s;if(_(n))throw t;var e=n._o;n._o=r;try{var i=y(e.error);if(!i)throw t;t=i.call(e,t)}catch(o){try{d(n)}finally{throw o}}return d(n),t},complete:function complete(t){var n=this._s;if(!_(n)){var e=n._o;n._o=r;try{var i=y(e.complete);t=i?i.call(e,t):r}catch(o){try{d(n)}finally{throw o}}return d(n),t}}});var x=function Observable(t){l(this,x,"Observable","_f")._f=a(t)};h(x.prototype,{subscribe:function subscribe(t){return new b(t,this._f)},forEach:function forEach(t){var n=this;return new(u.Promise||o.Promise)(function(r,e){a(t);var i=n.subscribe({next:function(n){try{return t(n)}catch(r){e(r),i.unsubscribe()}},error:e,complete:r})})}}),h(x,{from:function from(t){var n="function"==typeof this?this:x,r=y(s(t)[f]);if(r){var e=s(r.call(t));return e.constructor===n?e:new n(function(t){return e.subscribe(t)})}return new n(function(n){var r=!1;return c(function(){if(!r){try{if(v(t,!1,function(t){if(n.next(t),r)return g})===g)return}catch(e){if(r)throw e;return void n.error(e)}n.complete()}}),function(){r=!0}})},of:function of(){for(var t=0,n=arguments.length,r=Array(n);t<n;)r[t]=arguments[t++];return new("function"==typeof this?this:x)(function(t){var n=!1;return c(function(){if(!n){for(var e=0;e<r.length;++e)if(t.next(r[e]),n)return;t.complete()}}),function(){n=!0}})}}),p(x.prototype,f,function(){return this}),i(i.G,{Observable:x}),e(38)("Observable")},function(t,n,r){var e=r(0),i=r(85);e(e.G+e.B,{setImmediate:i.set,clearImmediate:i.clear})},function(t,n,r){for(var e=r(84),i=r(34),o=r(13),u=r(2),c=r(12),f=r(44),a=r(5),s=a("iterator"),l=a("toStringTag"),h=f.Array,p={CSSRuleList:!0,CSSStyleDeclaration:!1,CSSValueList:!1,ClientRectList:!1,DOMRectList:!1,DOMStringList:!1,DOMTokenList:!0,DataTransferItemList:!1,FileList:!1,HTMLAllCollection:!1,HTMLCollection:!1,HTMLFormElement:!1,HTMLSelectElement:!1,MediaList:!0,MimeTypeArray:!1,NamedNodeMap:!1,NodeList:!0,PaintRequestList:!1,Plugin:!1,PluginArray:!1,SVGLengthList:!1,SVGNumberList:!1,SVGPathSegList:!1,SVGPointList:!1,SVGStringList:!1,SVGTransformList:!1,SourceBufferList:!1,StyleSheetList:!0,TextTrackCueList:!1,TextTrackList:!1,TouchList:!1},v=i(p),g=0;g<v.length;g++){var y,d=v[g],_=p[d],S=u[d],b=S&&S.prototype;if(b&&(b[s]||c(b,s,h),b[l]||c(b,l,d),f[d]=h,_))for(y in e)b[y]||o(b,y,e[y],!0)}},function(t,n,r){var e=r(2),i=r(0),o=e.navigator,u=[].slice,c=!!o&&/MSIE .\./.test(o.userAgent),f=function(t){return function(n,r){var e=arguments.length>2,i=!!e&&u.call(arguments,2);return t(e?function(){("function"==typeof n?n:Function(n)).apply(this,i)}:n,r)}};i(i.G+i.B+i.F*c,{setTimeout:f(e.setTimeout),setInterval:f(e.setInterval)})}]),"undefined"!=typeof module&&module.exports?module.exports=t:"function"==typeof define&&define.amd?define(function(){return t}):n.core=t}(1,1);
//# sourceMappingURL=shim.min.js.map

/*! jQuery v1.11.1 | (c) 2005, 2014 jQuery Foundation, Inc. | jquery.org/license */
!function(a,b){"object"==typeof module&&"object"==typeof module.exports?module.exports=a.document?b(a,!0):function(a){if(!a.document)throw new Error("jQuery requires a window with a document");return b(a)}:b(a)}("undefined"!=typeof window?window:this,function(a,b){var c=[],d=c.slice,e=c.concat,f=c.push,g=c.indexOf,h={},i=h.toString,j=h.hasOwnProperty,k={},l="1.11.1",m=function(a,b){return new m.fn.init(a,b)},n=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,o=/^-ms-/,p=/-([\da-z])/gi,q=function(a,b){return b.toUpperCase()};m.fn=m.prototype={jquery:l,constructor:m,selector:"",length:0,toArray:function(){return d.call(this)},get:function(a){return null!=a?0>a?this[a+this.length]:this[a]:d.call(this)},pushStack:function(a){var b=m.merge(this.constructor(),a);return b.prevObject=this,b.context=this.context,b},each:function(a,b){return m.each(this,a,b)},map:function(a){return this.pushStack(m.map(this,function(b,c){return a.call(b,c,b)}))},slice:function(){return this.pushStack(d.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(a){var b=this.length,c=+a+(0>a?b:0);return this.pushStack(c>=0&&b>c?[this[c]]:[])},end:function(){return this.prevObject||this.constructor(null)},push:f,sort:c.sort,splice:c.splice},m.extend=m.fn.extend=function(){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||m.isFunction(g)||(g={}),h===i&&(g=this,h--);i>h;h++)if(null!=(e=arguments[h]))for(d in e)a=g[d],c=e[d],g!==c&&(j&&c&&(m.isPlainObject(c)||(b=m.isArray(c)))?(b?(b=!1,f=a&&m.isArray(a)?a:[]):f=a&&m.isPlainObject(a)?a:{},g[d]=m.extend(j,f,c)):void 0!==c&&(g[d]=c));return g},m.extend({expando:"jQuery"+(l+Math.random()).replace(/\D/g,""),isReady:!0,error:function(a){throw new Error(a)},noop:function(){},isFunction:function(a){return"function"===m.type(a)},isArray:Array.isArray||function(a){return"array"===m.type(a)},isWindow:function(a){return null!=a&&a==a.window},isNumeric:function(a){return!m.isArray(a)&&a-parseFloat(a)>=0},isEmptyObject:function(a){var b;for(b in a)return!1;return!0},isPlainObject:function(a){var b;if(!a||"object"!==m.type(a)||a.nodeType||m.isWindow(a))return!1;try{if(a.constructor&&!j.call(a,"constructor")&&!j.call(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}if(k.ownLast)for(b in a)return j.call(a,b);for(b in a);return void 0===b||j.call(a,b)},type:function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?h[i.call(a)]||"object":typeof a},globalEval:function(b){b&&m.trim(b)&&(a.execScript||function(b){a.eval.call(a,b)})(b)},camelCase:function(a){return a.replace(o,"ms-").replace(p,q)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()},each:function(a,b,c){var d,e=0,f=a.length,g=r(a);if(c){if(g){for(;f>e;e++)if(d=b.apply(a[e],c),d===!1)break}else for(e in a)if(d=b.apply(a[e],c),d===!1)break}else if(g){for(;f>e;e++)if(d=b.call(a[e],e,a[e]),d===!1)break}else for(e in a)if(d=b.call(a[e],e,a[e]),d===!1)break;return a},trim:function(a){return null==a?"":(a+"").replace(n,"")},makeArray:function(a,b){var c=b||[];return null!=a&&(r(Object(a))?m.merge(c,"string"==typeof a?[a]:a):f.call(c,a)),c},inArray:function(a,b,c){var d;if(b){if(g)return g.call(b,a,c);for(d=b.length,c=c?0>c?Math.max(0,d+c):c:0;d>c;c++)if(c in b&&b[c]===a)return c}return-1},merge:function(a,b){var c=+b.length,d=0,e=a.length;while(c>d)a[e++]=b[d++];if(c!==c)while(void 0!==b[d])a[e++]=b[d++];return a.length=e,a},grep:function(a,b,c){for(var d,e=[],f=0,g=a.length,h=!c;g>f;f++)d=!b(a[f],f),d!==h&&e.push(a[f]);return e},map:function(a,b,c){var d,f=0,g=a.length,h=r(a),i=[];if(h)for(;g>f;f++)d=b(a[f],f,c),null!=d&&i.push(d);else for(f in a)d=b(a[f],f,c),null!=d&&i.push(d);return e.apply([],i)},guid:1,proxy:function(a,b){var c,e,f;return"string"==typeof b&&(f=a[b],b=a,a=f),m.isFunction(a)?(c=d.call(arguments,2),e=function(){return a.apply(b||this,c.concat(d.call(arguments)))},e.guid=a.guid=a.guid||m.guid++,e):void 0},now:function(){return+new Date},support:k}),m.each("Boolean Number String Function Array Date RegExp Object Error".split(" "),function(a,b){h["[object "+b+"]"]=b.toLowerCase()});function r(a){var b=a.length,c=m.type(a);return"function"===c||m.isWindow(a)?!1:1===a.nodeType&&b?!0:"array"===c||0===b||"number"==typeof b&&b>0&&b-1 in a}var s=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u="sizzle"+-new Date,v=a.document,w=0,x=0,y=gb(),z=gb(),A=gb(),B=function(a,b){return a===b&&(l=!0),0},C="undefined",D=1<<31,E={}.hasOwnProperty,F=[],G=F.pop,H=F.push,I=F.push,J=F.slice,K=F.indexOf||function(a){for(var b=0,c=this.length;c>b;b++)if(this[b]===a)return b;return-1},L="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",M="[\\x20\\t\\r\\n\\f]",N="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",O=N.replace("w","w#"),P="\\["+M+"*("+N+")(?:"+M+"*([*^$|!~]?=)"+M+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+O+"))|)"+M+"*\\]",Q=":("+N+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+P+")*)|.*)\\)|)",R=new RegExp("^"+M+"+|((?:^|[^\\\\])(?:\\\\.)*)"+M+"+$","g"),S=new RegExp("^"+M+"*,"+M+"*"),T=new RegExp("^"+M+"*([>+~]|"+M+")"+M+"*"),U=new RegExp("="+M+"*([^\\]'\"]*?)"+M+"*\\]","g"),V=new RegExp(Q),W=new RegExp("^"+O+"$"),X={ID:new RegExp("^#("+N+")"),CLASS:new RegExp("^\\.("+N+")"),TAG:new RegExp("^("+N.replace("w","w*")+")"),ATTR:new RegExp("^"+P),PSEUDO:new RegExp("^"+Q),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+M+"*(even|odd|(([+-]|)(\\d*)n|)"+M+"*(?:([+-]|)"+M+"*(\\d+)|))"+M+"*\\)|)","i"),bool:new RegExp("^(?:"+L+")$","i"),needsContext:new RegExp("^"+M+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+M+"*((?:-\\d)?\\d*)"+M+"*\\)|)(?=[^-]|$)","i")},Y=/^(?:input|select|textarea|button)$/i,Z=/^h\d$/i,$=/^[^{]+\{\s*\[native \w/,_=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,ab=/[+~]/,bb=/'|\\/g,cb=new RegExp("\\\\([\\da-f]{1,6}"+M+"?|("+M+")|.)","ig"),db=function(a,b,c){var d="0x"+b-65536;return d!==d||c?b:0>d?String.fromCharCode(d+65536):String.fromCharCode(d>>10|55296,1023&d|56320)};try{I.apply(F=J.call(v.childNodes),v.childNodes),F[v.childNodes.length].nodeType}catch(eb){I={apply:F.length?function(a,b){H.apply(a,J.call(b))}:function(a,b){var c=a.length,d=0;while(a[c++]=b[d++]);a.length=c-1}}}function fb(a,b,d,e){var f,h,j,k,l,o,r,s,w,x;if((b?b.ownerDocument||b:v)!==n&&m(b),b=b||n,d=d||[],!a||"string"!=typeof a)return d;if(1!==(k=b.nodeType)&&9!==k)return[];if(p&&!e){if(f=_.exec(a))if(j=f[1]){if(9===k){if(h=b.getElementById(j),!h||!h.parentNode)return d;if(h.id===j)return d.push(h),d}else if(b.ownerDocument&&(h=b.ownerDocument.getElementById(j))&&t(b,h)&&h.id===j)return d.push(h),d}else{if(f[2])return I.apply(d,b.getElementsByTagName(a)),d;if((j=f[3])&&c.getElementsByClassName&&b.getElementsByClassName)return I.apply(d,b.getElementsByClassName(j)),d}if(c.qsa&&(!q||!q.test(a))){if(s=r=u,w=b,x=9===k&&a,1===k&&"object"!==b.nodeName.toLowerCase()){o=g(a),(r=b.getAttribute("id"))?s=r.replace(bb,"\\$&"):b.setAttribute("id",s),s="[id='"+s+"'] ",l=o.length;while(l--)o[l]=s+qb(o[l]);w=ab.test(a)&&ob(b.parentNode)||b,x=o.join(",")}if(x)try{return I.apply(d,w.querySelectorAll(x)),d}catch(y){}finally{r||b.removeAttribute("id")}}}return i(a.replace(R,"$1"),b,d,e)}function gb(){var a=[];function b(c,e){return a.push(c+" ")>d.cacheLength&&delete b[a.shift()],b[c+" "]=e}return b}function hb(a){return a[u]=!0,a}function ib(a){var b=n.createElement("div");try{return!!a(b)}catch(c){return!1}finally{b.parentNode&&b.parentNode.removeChild(b),b=null}}function jb(a,b){var c=a.split("|"),e=a.length;while(e--)d.attrHandle[c[e]]=b}function kb(a,b){var c=b&&a,d=c&&1===a.nodeType&&1===b.nodeType&&(~b.sourceIndex||D)-(~a.sourceIndex||D);if(d)return d;if(c)while(c=c.nextSibling)if(c===b)return-1;return a?1:-1}function lb(a){return function(b){var c=b.nodeName.toLowerCase();return"input"===c&&b.type===a}}function mb(a){return function(b){var c=b.nodeName.toLowerCase();return("input"===c||"button"===c)&&b.type===a}}function nb(a){return hb(function(b){return b=+b,hb(function(c,d){var e,f=a([],c.length,b),g=f.length;while(g--)c[e=f[g]]&&(c[e]=!(d[e]=c[e]))})})}function ob(a){return a&&typeof a.getElementsByTagName!==C&&a}c=fb.support={},f=fb.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;return b?"HTML"!==b.nodeName:!1},m=fb.setDocument=function(a){var b,e=a?a.ownerDocument||a:v,g=e.defaultView;return e!==n&&9===e.nodeType&&e.documentElement?(n=e,o=e.documentElement,p=!f(e),g&&g!==g.top&&(g.addEventListener?g.addEventListener("unload",function(){m()},!1):g.attachEvent&&g.attachEvent("onunload",function(){m()})),c.attributes=ib(function(a){return a.className="i",!a.getAttribute("className")}),c.getElementsByTagName=ib(function(a){return a.appendChild(e.createComment("")),!a.getElementsByTagName("*").length}),c.getElementsByClassName=$.test(e.getElementsByClassName)&&ib(function(a){return a.innerHTML="<div class='a'></div><div class='a i'></div>",a.firstChild.className="i",2===a.getElementsByClassName("i").length}),c.getById=ib(function(a){return o.appendChild(a).id=u,!e.getElementsByName||!e.getElementsByName(u).length}),c.getById?(d.find.ID=function(a,b){if(typeof b.getElementById!==C&&p){var c=b.getElementById(a);return c&&c.parentNode?[c]:[]}},d.filter.ID=function(a){var b=a.replace(cb,db);return function(a){return a.getAttribute("id")===b}}):(delete d.find.ID,d.filter.ID=function(a){var b=a.replace(cb,db);return function(a){var c=typeof a.getAttributeNode!==C&&a.getAttributeNode("id");return c&&c.value===b}}),d.find.TAG=c.getElementsByTagName?function(a,b){return typeof b.getElementsByTagName!==C?b.getElementsByTagName(a):void 0}:function(a,b){var c,d=[],e=0,f=b.getElementsByTagName(a);if("*"===a){while(c=f[e++])1===c.nodeType&&d.push(c);return d}return f},d.find.CLASS=c.getElementsByClassName&&function(a,b){return typeof b.getElementsByClassName!==C&&p?b.getElementsByClassName(a):void 0},r=[],q=[],(c.qsa=$.test(e.querySelectorAll))&&(ib(function(a){a.innerHTML="<select msallowclip=''><option selected=''></option></select>",a.querySelectorAll("[msallowclip^='']").length&&q.push("[*^$]="+M+"*(?:''|\"\")"),a.querySelectorAll("[selected]").length||q.push("\\["+M+"*(?:value|"+L+")"),a.querySelectorAll(":checked").length||q.push(":checked")}),ib(function(a){var b=e.createElement("input");b.setAttribute("type","hidden"),a.appendChild(b).setAttribute("name","D"),a.querySelectorAll("[name=d]").length&&q.push("name"+M+"*[*^$|!~]?="),a.querySelectorAll(":enabled").length||q.push(":enabled",":disabled"),a.querySelectorAll("*,:x"),q.push(",.*:")})),(c.matchesSelector=$.test(s=o.matches||o.webkitMatchesSelector||o.mozMatchesSelector||o.oMatchesSelector||o.msMatchesSelector))&&ib(function(a){c.disconnectedMatch=s.call(a,"div"),s.call(a,"[s!='']:x"),r.push("!=",Q)}),q=q.length&&new RegExp(q.join("|")),r=r.length&&new RegExp(r.join("|")),b=$.test(o.compareDocumentPosition),t=b||$.test(o.contains)?function(a,b){var c=9===a.nodeType?a.documentElement:a,d=b&&b.parentNode;return a===d||!(!d||1!==d.nodeType||!(c.contains?c.contains(d):a.compareDocumentPosition&&16&a.compareDocumentPosition(d)))}:function(a,b){if(b)while(b=b.parentNode)if(b===a)return!0;return!1},B=b?function(a,b){if(a===b)return l=!0,0;var d=!a.compareDocumentPosition-!b.compareDocumentPosition;return d?d:(d=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b):1,1&d||!c.sortDetached&&b.compareDocumentPosition(a)===d?a===e||a.ownerDocument===v&&t(v,a)?-1:b===e||b.ownerDocument===v&&t(v,b)?1:k?K.call(k,a)-K.call(k,b):0:4&d?-1:1)}:function(a,b){if(a===b)return l=!0,0;var c,d=0,f=a.parentNode,g=b.parentNode,h=[a],i=[b];if(!f||!g)return a===e?-1:b===e?1:f?-1:g?1:k?K.call(k,a)-K.call(k,b):0;if(f===g)return kb(a,b);c=a;while(c=c.parentNode)h.unshift(c);c=b;while(c=c.parentNode)i.unshift(c);while(h[d]===i[d])d++;return d?kb(h[d],i[d]):h[d]===v?-1:i[d]===v?1:0},e):n},fb.matches=function(a,b){return fb(a,null,null,b)},fb.matchesSelector=function(a,b){if((a.ownerDocument||a)!==n&&m(a),b=b.replace(U,"='$1']"),!(!c.matchesSelector||!p||r&&r.test(b)||q&&q.test(b)))try{var d=s.call(a,b);if(d||c.disconnectedMatch||a.document&&11!==a.document.nodeType)return d}catch(e){}return fb(b,n,null,[a]).length>0},fb.contains=function(a,b){return(a.ownerDocument||a)!==n&&m(a),t(a,b)},fb.attr=function(a,b){(a.ownerDocument||a)!==n&&m(a);var e=d.attrHandle[b.toLowerCase()],f=e&&E.call(d.attrHandle,b.toLowerCase())?e(a,b,!p):void 0;return void 0!==f?f:c.attributes||!p?a.getAttribute(b):(f=a.getAttributeNode(b))&&f.specified?f.value:null},fb.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)},fb.uniqueSort=function(a){var b,d=[],e=0,f=0;if(l=!c.detectDuplicates,k=!c.sortStable&&a.slice(0),a.sort(B),l){while(b=a[f++])b===a[f]&&(e=d.push(f));while(e--)a.splice(d[e],1)}return k=null,a},e=fb.getText=function(a){var b,c="",d=0,f=a.nodeType;if(f){if(1===f||9===f||11===f){if("string"==typeof a.textContent)return a.textContent;for(a=a.firstChild;a;a=a.nextSibling)c+=e(a)}else if(3===f||4===f)return a.nodeValue}else while(b=a[d++])c+=e(b);return c},d=fb.selectors={cacheLength:50,createPseudo:hb,match:X,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){return a[1]=a[1].replace(cb,db),a[3]=(a[3]||a[4]||a[5]||"").replace(cb,db),"~="===a[2]&&(a[3]=" "+a[3]+" "),a.slice(0,4)},CHILD:function(a){return a[1]=a[1].toLowerCase(),"nth"===a[1].slice(0,3)?(a[3]||fb.error(a[0]),a[4]=+(a[4]?a[5]+(a[6]||1):2*("even"===a[3]||"odd"===a[3])),a[5]=+(a[7]+a[8]||"odd"===a[3])):a[3]&&fb.error(a[0]),a},PSEUDO:function(a){var b,c=!a[6]&&a[2];return X.CHILD.test(a[0])?null:(a[3]?a[2]=a[4]||a[5]||"":c&&V.test(c)&&(b=g(c,!0))&&(b=c.indexOf(")",c.length-b)-c.length)&&(a[0]=a[0].slice(0,b),a[2]=c.slice(0,b)),a.slice(0,3))}},filter:{TAG:function(a){var b=a.replace(cb,db).toLowerCase();return"*"===a?function(){return!0}:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b}},CLASS:function(a){var b=y[a+" "];return b||(b=new RegExp("(^|"+M+")"+a+"("+M+"|$)"))&&y(a,function(a){return b.test("string"==typeof a.className&&a.className||typeof a.getAttribute!==C&&a.getAttribute("class")||"")})},ATTR:function(a,b,c){return function(d){var e=fb.attr(d,a);return null==e?"!="===b:b?(e+="","="===b?e===c:"!="===b?e!==c:"^="===b?c&&0===e.indexOf(c):"*="===b?c&&e.indexOf(c)>-1:"$="===b?c&&e.slice(-c.length)===c:"~="===b?(" "+e+" ").indexOf(c)>-1:"|="===b?e===c||e.slice(0,c.length+1)===c+"-":!1):!0}},CHILD:function(a,b,c,d,e){var f="nth"!==a.slice(0,3),g="last"!==a.slice(-4),h="of-type"===b;return 1===d&&0===e?function(a){return!!a.parentNode}:function(b,c,i){var j,k,l,m,n,o,p=f!==g?"nextSibling":"previousSibling",q=b.parentNode,r=h&&b.nodeName.toLowerCase(),s=!i&&!h;if(q){if(f){while(p){l=b;while(l=l[p])if(h?l.nodeName.toLowerCase()===r:1===l.nodeType)return!1;o=p="only"===a&&!o&&"nextSibling"}return!0}if(o=[g?q.firstChild:q.lastChild],g&&s){k=q[u]||(q[u]={}),j=k[a]||[],n=j[0]===w&&j[1],m=j[0]===w&&j[2],l=n&&q.childNodes[n];while(l=++n&&l&&l[p]||(m=n=0)||o.pop())if(1===l.nodeType&&++m&&l===b){k[a]=[w,n,m];break}}else if(s&&(j=(b[u]||(b[u]={}))[a])&&j[0]===w)m=j[1];else while(l=++n&&l&&l[p]||(m=n=0)||o.pop())if((h?l.nodeName.toLowerCase()===r:1===l.nodeType)&&++m&&(s&&((l[u]||(l[u]={}))[a]=[w,m]),l===b))break;return m-=e,m===d||m%d===0&&m/d>=0}}},PSEUDO:function(a,b){var c,e=d.pseudos[a]||d.setFilters[a.toLowerCase()]||fb.error("unsupported pseudo: "+a);return e[u]?e(b):e.length>1?(c=[a,a,"",b],d.setFilters.hasOwnProperty(a.toLowerCase())?hb(function(a,c){var d,f=e(a,b),g=f.length;while(g--)d=K.call(a,f[g]),a[d]=!(c[d]=f[g])}):function(a){return e(a,0,c)}):e}},pseudos:{not:hb(function(a){var b=[],c=[],d=h(a.replace(R,"$1"));return d[u]?hb(function(a,b,c,e){var f,g=d(a,null,e,[]),h=a.length;while(h--)(f=g[h])&&(a[h]=!(b[h]=f))}):function(a,e,f){return b[0]=a,d(b,null,f,c),!c.pop()}}),has:hb(function(a){return function(b){return fb(a,b).length>0}}),contains:hb(function(a){return function(b){return(b.textContent||b.innerText||e(b)).indexOf(a)>-1}}),lang:hb(function(a){return W.test(a||"")||fb.error("unsupported lang: "+a),a=a.replace(cb,db).toLowerCase(),function(b){var c;do if(c=p?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang"))return c=c.toLowerCase(),c===a||0===c.indexOf(a+"-");while((b=b.parentNode)&&1===b.nodeType);return!1}}),target:function(b){var c=a.location&&a.location.hash;return c&&c.slice(1)===b.id},root:function(a){return a===o},focus:function(a){return a===n.activeElement&&(!n.hasFocus||n.hasFocus())&&!!(a.type||a.href||~a.tabIndex)},enabled:function(a){return a.disabled===!1},disabled:function(a){return a.disabled===!0},checked:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&!!a.checked||"option"===b&&!!a.selected},selected:function(a){return a.parentNode&&a.parentNode.selectedIndex,a.selected===!0},empty:function(a){for(a=a.firstChild;a;a=a.nextSibling)if(a.nodeType<6)return!1;return!0},parent:function(a){return!d.pseudos.empty(a)},header:function(a){return Z.test(a.nodeName)},input:function(a){return Y.test(a.nodeName)},button:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&"button"===a.type||"button"===b},text:function(a){var b;return"input"===a.nodeName.toLowerCase()&&"text"===a.type&&(null==(b=a.getAttribute("type"))||"text"===b.toLowerCase())},first:nb(function(){return[0]}),last:nb(function(a,b){return[b-1]}),eq:nb(function(a,b,c){return[0>c?c+b:c]}),even:nb(function(a,b){for(var c=0;b>c;c+=2)a.push(c);return a}),odd:nb(function(a,b){for(var c=1;b>c;c+=2)a.push(c);return a}),lt:nb(function(a,b,c){for(var d=0>c?c+b:c;--d>=0;)a.push(d);return a}),gt:nb(function(a,b,c){for(var d=0>c?c+b:c;++d<b;)a.push(d);return a})}},d.pseudos.nth=d.pseudos.eq;for(b in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})d.pseudos[b]=lb(b);for(b in{submit:!0,reset:!0})d.pseudos[b]=mb(b);function pb(){}pb.prototype=d.filters=d.pseudos,d.setFilters=new pb,g=fb.tokenize=function(a,b){var c,e,f,g,h,i,j,k=z[a+" "];if(k)return b?0:k.slice(0);h=a,i=[],j=d.preFilter;while(h){(!c||(e=S.exec(h)))&&(e&&(h=h.slice(e[0].length)||h),i.push(f=[])),c=!1,(e=T.exec(h))&&(c=e.shift(),f.push({value:c,type:e[0].replace(R," ")}),h=h.slice(c.length));for(g in d.filter)!(e=X[g].exec(h))||j[g]&&!(e=j[g](e))||(c=e.shift(),f.push({value:c,type:g,matches:e}),h=h.slice(c.length));if(!c)break}return b?h.length:h?fb.error(a):z(a,i).slice(0)};function qb(a){for(var b=0,c=a.length,d="";c>b;b++)d+=a[b].value;return d}function rb(a,b,c){var d=b.dir,e=c&&"parentNode"===d,f=x++;return b.first?function(b,c,f){while(b=b[d])if(1===b.nodeType||e)return a(b,c,f)}:function(b,c,g){var h,i,j=[w,f];if(g){while(b=b[d])if((1===b.nodeType||e)&&a(b,c,g))return!0}else while(b=b[d])if(1===b.nodeType||e){if(i=b[u]||(b[u]={}),(h=i[d])&&h[0]===w&&h[1]===f)return j[2]=h[2];if(i[d]=j,j[2]=a(b,c,g))return!0}}}function sb(a){return a.length>1?function(b,c,d){var e=a.length;while(e--)if(!a[e](b,c,d))return!1;return!0}:a[0]}function tb(a,b,c){for(var d=0,e=b.length;e>d;d++)fb(a,b[d],c);return c}function ub(a,b,c,d,e){for(var f,g=[],h=0,i=a.length,j=null!=b;i>h;h++)(f=a[h])&&(!c||c(f,d,e))&&(g.push(f),j&&b.push(h));return g}function vb(a,b,c,d,e,f){return d&&!d[u]&&(d=vb(d)),e&&!e[u]&&(e=vb(e,f)),hb(function(f,g,h,i){var j,k,l,m=[],n=[],o=g.length,p=f||tb(b||"*",h.nodeType?[h]:h,[]),q=!a||!f&&b?p:ub(p,m,a,h,i),r=c?e||(f?a:o||d)?[]:g:q;if(c&&c(q,r,h,i),d){j=ub(r,n),d(j,[],h,i),k=j.length;while(k--)(l=j[k])&&(r[n[k]]=!(q[n[k]]=l))}if(f){if(e||a){if(e){j=[],k=r.length;while(k--)(l=r[k])&&j.push(q[k]=l);e(null,r=[],j,i)}k=r.length;while(k--)(l=r[k])&&(j=e?K.call(f,l):m[k])>-1&&(f[j]=!(g[j]=l))}}else r=ub(r===g?r.splice(o,r.length):r),e?e(null,g,r,i):I.apply(g,r)})}function wb(a){for(var b,c,e,f=a.length,g=d.relative[a[0].type],h=g||d.relative[" "],i=g?1:0,k=rb(function(a){return a===b},h,!0),l=rb(function(a){return K.call(b,a)>-1},h,!0),m=[function(a,c,d){return!g&&(d||c!==j)||((b=c).nodeType?k(a,c,d):l(a,c,d))}];f>i;i++)if(c=d.relative[a[i].type])m=[rb(sb(m),c)];else{if(c=d.filter[a[i].type].apply(null,a[i].matches),c[u]){for(e=++i;f>e;e++)if(d.relative[a[e].type])break;return vb(i>1&&sb(m),i>1&&qb(a.slice(0,i-1).concat({value:" "===a[i-2].type?"*":""})).replace(R,"$1"),c,e>i&&wb(a.slice(i,e)),f>e&&wb(a=a.slice(e)),f>e&&qb(a))}m.push(c)}return sb(m)}function xb(a,b){var c=b.length>0,e=a.length>0,f=function(f,g,h,i,k){var l,m,o,p=0,q="0",r=f&&[],s=[],t=j,u=f||e&&d.find.TAG("*",k),v=w+=null==t?1:Math.random()||.1,x=u.length;for(k&&(j=g!==n&&g);q!==x&&null!=(l=u[q]);q++){if(e&&l){m=0;while(o=a[m++])if(o(l,g,h)){i.push(l);break}k&&(w=v)}c&&((l=!o&&l)&&p--,f&&r.push(l))}if(p+=q,c&&q!==p){m=0;while(o=b[m++])o(r,s,g,h);if(f){if(p>0)while(q--)r[q]||s[q]||(s[q]=G.call(i));s=ub(s)}I.apply(i,s),k&&!f&&s.length>0&&p+b.length>1&&fb.uniqueSort(i)}return k&&(w=v,j=t),r};return c?hb(f):f}return h=fb.compile=function(a,b){var c,d=[],e=[],f=A[a+" "];if(!f){b||(b=g(a)),c=b.length;while(c--)f=wb(b[c]),f[u]?d.push(f):e.push(f);f=A(a,xb(e,d)),f.selector=a}return f},i=fb.select=function(a,b,e,f){var i,j,k,l,m,n="function"==typeof a&&a,o=!f&&g(a=n.selector||a);if(e=e||[],1===o.length){if(j=o[0]=o[0].slice(0),j.length>2&&"ID"===(k=j[0]).type&&c.getById&&9===b.nodeType&&p&&d.relative[j[1].type]){if(b=(d.find.ID(k.matches[0].replace(cb,db),b)||[])[0],!b)return e;n&&(b=b.parentNode),a=a.slice(j.shift().value.length)}i=X.needsContext.test(a)?0:j.length;while(i--){if(k=j[i],d.relative[l=k.type])break;if((m=d.find[l])&&(f=m(k.matches[0].replace(cb,db),ab.test(j[0].type)&&ob(b.parentNode)||b))){if(j.splice(i,1),a=f.length&&qb(j),!a)return I.apply(e,f),e;break}}}return(n||h(a,o))(f,b,!p,e,ab.test(a)&&ob(b.parentNode)||b),e},c.sortStable=u.split("").sort(B).join("")===u,c.detectDuplicates=!!l,m(),c.sortDetached=ib(function(a){return 1&a.compareDocumentPosition(n.createElement("div"))}),ib(function(a){return a.innerHTML="<a href='#'></a>","#"===a.firstChild.getAttribute("href")})||jb("type|href|height|width",function(a,b,c){return c?void 0:a.getAttribute(b,"type"===b.toLowerCase()?1:2)}),c.attributes&&ib(function(a){return a.innerHTML="<input/>",a.firstChild.setAttribute("value",""),""===a.firstChild.getAttribute("value")})||jb("value",function(a,b,c){return c||"input"!==a.nodeName.toLowerCase()?void 0:a.defaultValue}),ib(function(a){return null==a.getAttribute("disabled")})||jb(L,function(a,b,c){var d;return c?void 0:a[b]===!0?b.toLowerCase():(d=a.getAttributeNode(b))&&d.specified?d.value:null}),fb}(a);m.find=s,m.expr=s.selectors,m.expr[":"]=m.expr.pseudos,m.unique=s.uniqueSort,m.text=s.getText,m.isXMLDoc=s.isXML,m.contains=s.contains;var t=m.expr.match.needsContext,u=/^<(\w+)\s*\/?>(?:<\/\1>|)$/,v=/^.[^:#\[\.,]*$/;function w(a,b,c){if(m.isFunction(b))return m.grep(a,function(a,d){return!!b.call(a,d,a)!==c});if(b.nodeType)return m.grep(a,function(a){return a===b!==c});if("string"==typeof b){if(v.test(b))return m.filter(b,a,c);b=m.filter(b,a)}return m.grep(a,function(a){return m.inArray(a,b)>=0!==c})}m.filter=function(a,b,c){var d=b[0];return c&&(a=":not("+a+")"),1===b.length&&1===d.nodeType?m.find.matchesSelector(d,a)?[d]:[]:m.find.matches(a,m.grep(b,function(a){return 1===a.nodeType}))},m.fn.extend({find:function(a){var b,c=[],d=this,e=d.length;if("string"!=typeof a)return this.pushStack(m(a).filter(function(){for(b=0;e>b;b++)if(m.contains(d[b],this))return!0}));for(b=0;e>b;b++)m.find(a,d[b],c);return c=this.pushStack(e>1?m.unique(c):c),c.selector=this.selector?this.selector+" "+a:a,c},filter:function(a){return this.pushStack(w(this,a||[],!1))},not:function(a){return this.pushStack(w(this,a||[],!0))},is:function(a){return!!w(this,"string"==typeof a&&t.test(a)?m(a):a||[],!1).length}});var x,y=a.document,z=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,A=m.fn.init=function(a,b){var c,d;if(!a)return this;if("string"==typeof a){if(c="<"===a.charAt(0)&&">"===a.charAt(a.length-1)&&a.length>=3?[null,a,null]:z.exec(a),!c||!c[1]&&b)return!b||b.jquery?(b||x).find(a):this.constructor(b).find(a);if(c[1]){if(b=b instanceof m?b[0]:b,m.merge(this,m.parseHTML(c[1],b&&b.nodeType?b.ownerDocument||b:y,!0)),u.test(c[1])&&m.isPlainObject(b))for(c in b)m.isFunction(this[c])?this[c](b[c]):this.attr(c,b[c]);return this}if(d=y.getElementById(c[2]),d&&d.parentNode){if(d.id!==c[2])return x.find(a);this.length=1,this[0]=d}return this.context=y,this.selector=a,this}return a.nodeType?(this.context=this[0]=a,this.length=1,this):m.isFunction(a)?"undefined"!=typeof x.ready?x.ready(a):a(m):(void 0!==a.selector&&(this.selector=a.selector,this.context=a.context),m.makeArray(a,this))};A.prototype=m.fn,x=m(y);var B=/^(?:parents|prev(?:Until|All))/,C={children:!0,contents:!0,next:!0,prev:!0};m.extend({dir:function(a,b,c){var d=[],e=a[b];while(e&&9!==e.nodeType&&(void 0===c||1!==e.nodeType||!m(e).is(c)))1===e.nodeType&&d.push(e),e=e[b];return d},sibling:function(a,b){for(var c=[];a;a=a.nextSibling)1===a.nodeType&&a!==b&&c.push(a);return c}}),m.fn.extend({has:function(a){var b,c=m(a,this),d=c.length;return this.filter(function(){for(b=0;d>b;b++)if(m.contains(this,c[b]))return!0})},closest:function(a,b){for(var c,d=0,e=this.length,f=[],g=t.test(a)||"string"!=typeof a?m(a,b||this.context):0;e>d;d++)for(c=this[d];c&&c!==b;c=c.parentNode)if(c.nodeType<11&&(g?g.index(c)>-1:1===c.nodeType&&m.find.matchesSelector(c,a))){f.push(c);break}return this.pushStack(f.length>1?m.unique(f):f)},index:function(a){return a?"string"==typeof a?m.inArray(this[0],m(a)):m.inArray(a.jquery?a[0]:a,this):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(a,b){return this.pushStack(m.unique(m.merge(this.get(),m(a,b))))},addBack:function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))}});function D(a,b){do a=a[b];while(a&&1!==a.nodeType);return a}m.each({parent:function(a){var b=a.parentNode;return b&&11!==b.nodeType?b:null},parents:function(a){return m.dir(a,"parentNode")},parentsUntil:function(a,b,c){return m.dir(a,"parentNode",c)},next:function(a){return D(a,"nextSibling")},prev:function(a){return D(a,"previousSibling")},nextAll:function(a){return m.dir(a,"nextSibling")},prevAll:function(a){return m.dir(a,"previousSibling")},nextUntil:function(a,b,c){return m.dir(a,"nextSibling",c)},prevUntil:function(a,b,c){return m.dir(a,"previousSibling",c)},siblings:function(a){return m.sibling((a.parentNode||{}).firstChild,a)},children:function(a){return m.sibling(a.firstChild)},contents:function(a){return m.nodeName(a,"iframe")?a.contentDocument||a.contentWindow.document:m.merge([],a.childNodes)}},function(a,b){m.fn[a]=function(c,d){var e=m.map(this,b,c);return"Until"!==a.slice(-5)&&(d=c),d&&"string"==typeof d&&(e=m.filter(d,e)),this.length>1&&(C[a]||(e=m.unique(e)),B.test(a)&&(e=e.reverse())),this.pushStack(e)}});var E=/\S+/g,F={};function G(a){var b=F[a]={};return m.each(a.match(E)||[],function(a,c){b[c]=!0}),b}m.Callbacks=function(a){a="string"==typeof a?F[a]||G(a):m.extend({},a);var b,c,d,e,f,g,h=[],i=!a.once&&[],j=function(l){for(c=a.memory&&l,d=!0,f=g||0,g=0,e=h.length,b=!0;h&&e>f;f++)if(h[f].apply(l[0],l[1])===!1&&a.stopOnFalse){c=!1;break}b=!1,h&&(i?i.length&&j(i.shift()):c?h=[]:k.disable())},k={add:function(){if(h){var d=h.length;!function f(b){m.each(b,function(b,c){var d=m.type(c);"function"===d?a.unique&&k.has(c)||h.push(c):c&&c.length&&"string"!==d&&f(c)})}(arguments),b?e=h.length:c&&(g=d,j(c))}return this},remove:function(){return h&&m.each(arguments,function(a,c){var d;while((d=m.inArray(c,h,d))>-1)h.splice(d,1),b&&(e>=d&&e--,f>=d&&f--)}),this},has:function(a){return a?m.inArray(a,h)>-1:!(!h||!h.length)},empty:function(){return h=[],e=0,this},disable:function(){return h=i=c=void 0,this},disabled:function(){return!h},lock:function(){return i=void 0,c||k.disable(),this},locked:function(){return!i},fireWith:function(a,c){return!h||d&&!i||(c=c||[],c=[a,c.slice?c.slice():c],b?i.push(c):j(c)),this},fire:function(){return k.fireWith(this,arguments),this},fired:function(){return!!d}};return k},m.extend({Deferred:function(a){var b=[["resolve","done",m.Callbacks("once memory"),"resolved"],["reject","fail",m.Callbacks("once memory"),"rejected"],["notify","progress",m.Callbacks("memory")]],c="pending",d={state:function(){return c},always:function(){return e.done(arguments).fail(arguments),this},then:function(){var a=arguments;return m.Deferred(function(c){m.each(b,function(b,f){var g=m.isFunction(a[b])&&a[b];e[f[1]](function(){var a=g&&g.apply(this,arguments);a&&m.isFunction(a.promise)?a.promise().done(c.resolve).fail(c.reject).progress(c.notify):c[f[0]+"With"](this===d?c.promise():this,g?[a]:arguments)})}),a=null}).promise()},promise:function(a){return null!=a?m.extend(a,d):d}},e={};return d.pipe=d.then,m.each(b,function(a,f){var g=f[2],h=f[3];d[f[1]]=g.add,h&&g.add(function(){c=h},b[1^a][2].disable,b[2][2].lock),e[f[0]]=function(){return e[f[0]+"With"](this===e?d:this,arguments),this},e[f[0]+"With"]=g.fireWith}),d.promise(e),a&&a.call(e,e),e},when:function(a){var b=0,c=d.call(arguments),e=c.length,f=1!==e||a&&m.isFunction(a.promise)?e:0,g=1===f?a:m.Deferred(),h=function(a,b,c){return function(e){b[a]=this,c[a]=arguments.length>1?d.call(arguments):e,c===i?g.notifyWith(b,c):--f||g.resolveWith(b,c)}},i,j,k;if(e>1)for(i=new Array(e),j=new Array(e),k=new Array(e);e>b;b++)c[b]&&m.isFunction(c[b].promise)?c[b].promise().done(h(b,k,c)).fail(g.reject).progress(h(b,j,i)):--f;return f||g.resolveWith(k,c),g.promise()}});var H;m.fn.ready=function(a){return m.ready.promise().done(a),this},m.extend({isReady:!1,readyWait:1,holdReady:function(a){a?m.readyWait++:m.ready(!0)},ready:function(a){if(a===!0?!--m.readyWait:!m.isReady){if(!y.body)return setTimeout(m.ready);m.isReady=!0,a!==!0&&--m.readyWait>0||(H.resolveWith(y,[m]),m.fn.triggerHandler&&(m(y).triggerHandler("ready"),m(y).off("ready")))}}});function I(){y.addEventListener?(y.removeEventListener("DOMContentLoaded",J,!1),a.removeEventListener("load",J,!1)):(y.detachEvent("onreadystatechange",J),a.detachEvent("onload",J))}function J(){(y.addEventListener||"load"===event.type||"complete"===y.readyState)&&(I(),m.ready())}m.ready.promise=function(b){if(!H)if(H=m.Deferred(),"complete"===y.readyState)setTimeout(m.ready);else if(y.addEventListener)y.addEventListener("DOMContentLoaded",J,!1),a.addEventListener("load",J,!1);else{y.attachEvent("onreadystatechange",J),a.attachEvent("onload",J);var c=!1;try{c=null==a.frameElement&&y.documentElement}catch(d){}c&&c.doScroll&&!function e(){if(!m.isReady){try{c.doScroll("left")}catch(a){return setTimeout(e,50)}I(),m.ready()}}()}return H.promise(b)};var K="undefined",L;for(L in m(k))break;k.ownLast="0"!==L,k.inlineBlockNeedsLayout=!1,m(function(){var a,b,c,d;c=y.getElementsByTagName("body")[0],c&&c.style&&(b=y.createElement("div"),d=y.createElement("div"),d.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(d).appendChild(b),typeof b.style.zoom!==K&&(b.style.cssText="display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1",k.inlineBlockNeedsLayout=a=3===b.offsetWidth,a&&(c.style.zoom=1)),c.removeChild(d))}),function(){var a=y.createElement("div");if(null==k.deleteExpando){k.deleteExpando=!0;try{delete a.test}catch(b){k.deleteExpando=!1}}a=null}(),m.acceptData=function(a){var b=m.noData[(a.nodeName+" ").toLowerCase()],c=+a.nodeType||1;return 1!==c&&9!==c?!1:!b||b!==!0&&a.getAttribute("classid")===b};var M=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,N=/([A-Z])/g;function O(a,b,c){if(void 0===c&&1===a.nodeType){var d="data-"+b.replace(N,"-$1").toLowerCase();if(c=a.getAttribute(d),"string"==typeof c){try{c="true"===c?!0:"false"===c?!1:"null"===c?null:+c+""===c?+c:M.test(c)?m.parseJSON(c):c}catch(e){}m.data(a,b,c)}else c=void 0}return c}function P(a){var b;for(b in a)if(("data"!==b||!m.isEmptyObject(a[b]))&&"toJSON"!==b)return!1;return!0}function Q(a,b,d,e){if(m.acceptData(a)){var f,g,h=m.expando,i=a.nodeType,j=i?m.cache:a,k=i?a[h]:a[h]&&h;
if(k&&j[k]&&(e||j[k].data)||void 0!==d||"string"!=typeof b)return k||(k=i?a[h]=c.pop()||m.guid++:h),j[k]||(j[k]=i?{}:{toJSON:m.noop}),("object"==typeof b||"function"==typeof b)&&(e?j[k]=m.extend(j[k],b):j[k].data=m.extend(j[k].data,b)),g=j[k],e||(g.data||(g.data={}),g=g.data),void 0!==d&&(g[m.camelCase(b)]=d),"string"==typeof b?(f=g[b],null==f&&(f=g[m.camelCase(b)])):f=g,f}}function R(a,b,c){if(m.acceptData(a)){var d,e,f=a.nodeType,g=f?m.cache:a,h=f?a[m.expando]:m.expando;if(g[h]){if(b&&(d=c?g[h]:g[h].data)){m.isArray(b)?b=b.concat(m.map(b,m.camelCase)):b in d?b=[b]:(b=m.camelCase(b),b=b in d?[b]:b.split(" ")),e=b.length;while(e--)delete d[b[e]];if(c?!P(d):!m.isEmptyObject(d))return}(c||(delete g[h].data,P(g[h])))&&(f?m.cleanData([a],!0):k.deleteExpando||g!=g.window?delete g[h]:g[h]=null)}}}m.extend({cache:{},noData:{"applet ":!0,"embed ":!0,"object ":"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"},hasData:function(a){return a=a.nodeType?m.cache[a[m.expando]]:a[m.expando],!!a&&!P(a)},data:function(a,b,c){return Q(a,b,c)},removeData:function(a,b){return R(a,b)},_data:function(a,b,c){return Q(a,b,c,!0)},_removeData:function(a,b){return R(a,b,!0)}}),m.fn.extend({data:function(a,b){var c,d,e,f=this[0],g=f&&f.attributes;if(void 0===a){if(this.length&&(e=m.data(f),1===f.nodeType&&!m._data(f,"parsedAttrs"))){c=g.length;while(c--)g[c]&&(d=g[c].name,0===d.indexOf("data-")&&(d=m.camelCase(d.slice(5)),O(f,d,e[d])));m._data(f,"parsedAttrs",!0)}return e}return"object"==typeof a?this.each(function(){m.data(this,a)}):arguments.length>1?this.each(function(){m.data(this,a,b)}):f?O(f,a,m.data(f,a)):void 0},removeData:function(a){return this.each(function(){m.removeData(this,a)})}}),m.extend({queue:function(a,b,c){var d;return a?(b=(b||"fx")+"queue",d=m._data(a,b),c&&(!d||m.isArray(c)?d=m._data(a,b,m.makeArray(c)):d.push(c)),d||[]):void 0},dequeue:function(a,b){b=b||"fx";var c=m.queue(a,b),d=c.length,e=c.shift(),f=m._queueHooks(a,b),g=function(){m.dequeue(a,b)};"inprogress"===e&&(e=c.shift(),d--),e&&("fx"===b&&c.unshift("inprogress"),delete f.stop,e.call(a,g,f)),!d&&f&&f.empty.fire()},_queueHooks:function(a,b){var c=b+"queueHooks";return m._data(a,c)||m._data(a,c,{empty:m.Callbacks("once memory").add(function(){m._removeData(a,b+"queue"),m._removeData(a,c)})})}}),m.fn.extend({queue:function(a,b){var c=2;return"string"!=typeof a&&(b=a,a="fx",c--),arguments.length<c?m.queue(this[0],a):void 0===b?this:this.each(function(){var c=m.queue(this,a,b);m._queueHooks(this,a),"fx"===a&&"inprogress"!==c[0]&&m.dequeue(this,a)})},dequeue:function(a){return this.each(function(){m.dequeue(this,a)})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,b){var c,d=1,e=m.Deferred(),f=this,g=this.length,h=function(){--d||e.resolveWith(f,[f])};"string"!=typeof a&&(b=a,a=void 0),a=a||"fx";while(g--)c=m._data(f[g],a+"queueHooks"),c&&c.empty&&(d++,c.empty.add(h));return h(),e.promise(b)}});var S=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,T=["Top","Right","Bottom","Left"],U=function(a,b){return a=b||a,"none"===m.css(a,"display")||!m.contains(a.ownerDocument,a)},V=m.access=function(a,b,c,d,e,f,g){var h=0,i=a.length,j=null==c;if("object"===m.type(c)){e=!0;for(h in c)m.access(a,b,h,c[h],!0,f,g)}else if(void 0!==d&&(e=!0,m.isFunction(d)||(g=!0),j&&(g?(b.call(a,d),b=null):(j=b,b=function(a,b,c){return j.call(m(a),c)})),b))for(;i>h;h++)b(a[h],c,g?d:d.call(a[h],h,b(a[h],c)));return e?a:j?b.call(a):i?b(a[0],c):f},W=/^(?:checkbox|radio)$/i;!function(){var a=y.createElement("input"),b=y.createElement("div"),c=y.createDocumentFragment();if(b.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",k.leadingWhitespace=3===b.firstChild.nodeType,k.tbody=!b.getElementsByTagName("tbody").length,k.htmlSerialize=!!b.getElementsByTagName("link").length,k.html5Clone="<:nav></:nav>"!==y.createElement("nav").cloneNode(!0).outerHTML,a.type="checkbox",a.checked=!0,c.appendChild(a),k.appendChecked=a.checked,b.innerHTML="<textarea>x</textarea>",k.noCloneChecked=!!b.cloneNode(!0).lastChild.defaultValue,c.appendChild(b),b.innerHTML="<input type='radio' checked='checked' name='t'/>",k.checkClone=b.cloneNode(!0).cloneNode(!0).lastChild.checked,k.noCloneEvent=!0,b.attachEvent&&(b.attachEvent("onclick",function(){k.noCloneEvent=!1}),b.cloneNode(!0).click()),null==k.deleteExpando){k.deleteExpando=!0;try{delete b.test}catch(d){k.deleteExpando=!1}}}(),function(){var b,c,d=y.createElement("div");for(b in{submit:!0,change:!0,focusin:!0})c="on"+b,(k[b+"Bubbles"]=c in a)||(d.setAttribute(c,"t"),k[b+"Bubbles"]=d.attributes[c].expando===!1);d=null}();var X=/^(?:input|select|textarea)$/i,Y=/^key/,Z=/^(?:mouse|pointer|contextmenu)|click/,$=/^(?:focusinfocus|focusoutblur)$/,_=/^([^.]*)(?:\.(.+)|)$/;function ab(){return!0}function bb(){return!1}function cb(){try{return y.activeElement}catch(a){}}m.event={global:{},add:function(a,b,c,d,e){var f,g,h,i,j,k,l,n,o,p,q,r=m._data(a);if(r){c.handler&&(i=c,c=i.handler,e=i.selector),c.guid||(c.guid=m.guid++),(g=r.events)||(g=r.events={}),(k=r.handle)||(k=r.handle=function(a){return typeof m===K||a&&m.event.triggered===a.type?void 0:m.event.dispatch.apply(k.elem,arguments)},k.elem=a),b=(b||"").match(E)||[""],h=b.length;while(h--)f=_.exec(b[h])||[],o=q=f[1],p=(f[2]||"").split(".").sort(),o&&(j=m.event.special[o]||{},o=(e?j.delegateType:j.bindType)||o,j=m.event.special[o]||{},l=m.extend({type:o,origType:q,data:d,handler:c,guid:c.guid,selector:e,needsContext:e&&m.expr.match.needsContext.test(e),namespace:p.join(".")},i),(n=g[o])||(n=g[o]=[],n.delegateCount=0,j.setup&&j.setup.call(a,d,p,k)!==!1||(a.addEventListener?a.addEventListener(o,k,!1):a.attachEvent&&a.attachEvent("on"+o,k))),j.add&&(j.add.call(a,l),l.handler.guid||(l.handler.guid=c.guid)),e?n.splice(n.delegateCount++,0,l):n.push(l),m.event.global[o]=!0);a=null}},remove:function(a,b,c,d,e){var f,g,h,i,j,k,l,n,o,p,q,r=m.hasData(a)&&m._data(a);if(r&&(k=r.events)){b=(b||"").match(E)||[""],j=b.length;while(j--)if(h=_.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o){l=m.event.special[o]||{},o=(d?l.delegateType:l.bindType)||o,n=k[o]||[],h=h[2]&&new RegExp("(^|\\.)"+p.join("\\.(?:.*\\.|)")+"(\\.|$)"),i=f=n.length;while(f--)g=n[f],!e&&q!==g.origType||c&&c.guid!==g.guid||h&&!h.test(g.namespace)||d&&d!==g.selector&&("**"!==d||!g.selector)||(n.splice(f,1),g.selector&&n.delegateCount--,l.remove&&l.remove.call(a,g));i&&!n.length&&(l.teardown&&l.teardown.call(a,p,r.handle)!==!1||m.removeEvent(a,o,r.handle),delete k[o])}else for(o in k)m.event.remove(a,o+b[j],c,d,!0);m.isEmptyObject(k)&&(delete r.handle,m._removeData(a,"events"))}},trigger:function(b,c,d,e){var f,g,h,i,k,l,n,o=[d||y],p=j.call(b,"type")?b.type:b,q=j.call(b,"namespace")?b.namespace.split("."):[];if(h=l=d=d||y,3!==d.nodeType&&8!==d.nodeType&&!$.test(p+m.event.triggered)&&(p.indexOf(".")>=0&&(q=p.split("."),p=q.shift(),q.sort()),g=p.indexOf(":")<0&&"on"+p,b=b[m.expando]?b:new m.Event(p,"object"==typeof b&&b),b.isTrigger=e?2:3,b.namespace=q.join("."),b.namespace_re=b.namespace?new RegExp("(^|\\.)"+q.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,b.result=void 0,b.target||(b.target=d),c=null==c?[b]:m.makeArray(c,[b]),k=m.event.special[p]||{},e||!k.trigger||k.trigger.apply(d,c)!==!1)){if(!e&&!k.noBubble&&!m.isWindow(d)){for(i=k.delegateType||p,$.test(i+p)||(h=h.parentNode);h;h=h.parentNode)o.push(h),l=h;l===(d.ownerDocument||y)&&o.push(l.defaultView||l.parentWindow||a)}n=0;while((h=o[n++])&&!b.isPropagationStopped())b.type=n>1?i:k.bindType||p,f=(m._data(h,"events")||{})[b.type]&&m._data(h,"handle"),f&&f.apply(h,c),f=g&&h[g],f&&f.apply&&m.acceptData(h)&&(b.result=f.apply(h,c),b.result===!1&&b.preventDefault());if(b.type=p,!e&&!b.isDefaultPrevented()&&(!k._default||k._default.apply(o.pop(),c)===!1)&&m.acceptData(d)&&g&&d[p]&&!m.isWindow(d)){l=d[g],l&&(d[g]=null),m.event.triggered=p;try{d[p]()}catch(r){}m.event.triggered=void 0,l&&(d[g]=l)}return b.result}},dispatch:function(a){a=m.event.fix(a);var b,c,e,f,g,h=[],i=d.call(arguments),j=(m._data(this,"events")||{})[a.type]||[],k=m.event.special[a.type]||{};if(i[0]=a,a.delegateTarget=this,!k.preDispatch||k.preDispatch.call(this,a)!==!1){h=m.event.handlers.call(this,a,j),b=0;while((f=h[b++])&&!a.isPropagationStopped()){a.currentTarget=f.elem,g=0;while((e=f.handlers[g++])&&!a.isImmediatePropagationStopped())(!a.namespace_re||a.namespace_re.test(e.namespace))&&(a.handleObj=e,a.data=e.data,c=((m.event.special[e.origType]||{}).handle||e.handler).apply(f.elem,i),void 0!==c&&(a.result=c)===!1&&(a.preventDefault(),a.stopPropagation()))}return k.postDispatch&&k.postDispatch.call(this,a),a.result}},handlers:function(a,b){var c,d,e,f,g=[],h=b.delegateCount,i=a.target;if(h&&i.nodeType&&(!a.button||"click"!==a.type))for(;i!=this;i=i.parentNode||this)if(1===i.nodeType&&(i.disabled!==!0||"click"!==a.type)){for(e=[],f=0;h>f;f++)d=b[f],c=d.selector+" ",void 0===e[c]&&(e[c]=d.needsContext?m(c,this).index(i)>=0:m.find(c,this,null,[i]).length),e[c]&&e.push(d);e.length&&g.push({elem:i,handlers:e})}return h<b.length&&g.push({elem:this,handlers:b.slice(h)}),g},fix:function(a){if(a[m.expando])return a;var b,c,d,e=a.type,f=a,g=this.fixHooks[e];g||(this.fixHooks[e]=g=Z.test(e)?this.mouseHooks:Y.test(e)?this.keyHooks:{}),d=g.props?this.props.concat(g.props):this.props,a=new m.Event(f),b=d.length;while(b--)c=d[b],a[c]=f[c];return a.target||(a.target=f.srcElement||y),3===a.target.nodeType&&(a.target=a.target.parentNode),a.metaKey=!!a.metaKey,g.filter?g.filter(a,f):a},props:"altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){return null==a.which&&(a.which=null!=b.charCode?b.charCode:b.keyCode),a}},mouseHooks:{props:"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,b){var c,d,e,f=b.button,g=b.fromElement;return null==a.pageX&&null!=b.clientX&&(d=a.target.ownerDocument||y,e=d.documentElement,c=d.body,a.pageX=b.clientX+(e&&e.scrollLeft||c&&c.scrollLeft||0)-(e&&e.clientLeft||c&&c.clientLeft||0),a.pageY=b.clientY+(e&&e.scrollTop||c&&c.scrollTop||0)-(e&&e.clientTop||c&&c.clientTop||0)),!a.relatedTarget&&g&&(a.relatedTarget=g===a.target?b.toElement:g),a.which||void 0===f||(a.which=1&f?1:2&f?3:4&f?2:0),a}},special:{load:{noBubble:!0},focus:{trigger:function(){if(this!==cb()&&this.focus)try{return this.focus(),!1}catch(a){}},delegateType:"focusin"},blur:{trigger:function(){return this===cb()&&this.blur?(this.blur(),!1):void 0},delegateType:"focusout"},click:{trigger:function(){return m.nodeName(this,"input")&&"checkbox"===this.type&&this.click?(this.click(),!1):void 0},_default:function(a){return m.nodeName(a.target,"a")}},beforeunload:{postDispatch:function(a){void 0!==a.result&&a.originalEvent&&(a.originalEvent.returnValue=a.result)}}},simulate:function(a,b,c,d){var e=m.extend(new m.Event,c,{type:a,isSimulated:!0,originalEvent:{}});d?m.event.trigger(e,null,b):m.event.dispatch.call(b,e),e.isDefaultPrevented()&&c.preventDefault()}},m.removeEvent=y.removeEventListener?function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c,!1)}:function(a,b,c){var d="on"+b;a.detachEvent&&(typeof a[d]===K&&(a[d]=null),a.detachEvent(d,c))},m.Event=function(a,b){return this instanceof m.Event?(a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||void 0===a.defaultPrevented&&a.returnValue===!1?ab:bb):this.type=a,b&&m.extend(this,b),this.timeStamp=a&&a.timeStamp||m.now(),void(this[m.expando]=!0)):new m.Event(a,b)},m.Event.prototype={isDefaultPrevented:bb,isPropagationStopped:bb,isImmediatePropagationStopped:bb,preventDefault:function(){var a=this.originalEvent;this.isDefaultPrevented=ab,a&&(a.preventDefault?a.preventDefault():a.returnValue=!1)},stopPropagation:function(){var a=this.originalEvent;this.isPropagationStopped=ab,a&&(a.stopPropagation&&a.stopPropagation(),a.cancelBubble=!0)},stopImmediatePropagation:function(){var a=this.originalEvent;this.isImmediatePropagationStopped=ab,a&&a.stopImmediatePropagation&&a.stopImmediatePropagation(),this.stopPropagation()}},m.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(a,b){m.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c,d=this,e=a.relatedTarget,f=a.handleObj;return(!e||e!==d&&!m.contains(d,e))&&(a.type=f.origType,c=f.handler.apply(this,arguments),a.type=b),c}}}),k.submitBubbles||(m.event.special.submit={setup:function(){return m.nodeName(this,"form")?!1:void m.event.add(this,"click._submit keypress._submit",function(a){var b=a.target,c=m.nodeName(b,"input")||m.nodeName(b,"button")?b.form:void 0;c&&!m._data(c,"submitBubbles")&&(m.event.add(c,"submit._submit",function(a){a._submit_bubble=!0}),m._data(c,"submitBubbles",!0))})},postDispatch:function(a){a._submit_bubble&&(delete a._submit_bubble,this.parentNode&&!a.isTrigger&&m.event.simulate("submit",this.parentNode,a,!0))},teardown:function(){return m.nodeName(this,"form")?!1:void m.event.remove(this,"._submit")}}),k.changeBubbles||(m.event.special.change={setup:function(){return X.test(this.nodeName)?(("checkbox"===this.type||"radio"===this.type)&&(m.event.add(this,"propertychange._change",function(a){"checked"===a.originalEvent.propertyName&&(this._just_changed=!0)}),m.event.add(this,"click._change",function(a){this._just_changed&&!a.isTrigger&&(this._just_changed=!1),m.event.simulate("change",this,a,!0)})),!1):void m.event.add(this,"beforeactivate._change",function(a){var b=a.target;X.test(b.nodeName)&&!m._data(b,"changeBubbles")&&(m.event.add(b,"change._change",function(a){!this.parentNode||a.isSimulated||a.isTrigger||m.event.simulate("change",this.parentNode,a,!0)}),m._data(b,"changeBubbles",!0))})},handle:function(a){var b=a.target;return this!==b||a.isSimulated||a.isTrigger||"radio"!==b.type&&"checkbox"!==b.type?a.handleObj.handler.apply(this,arguments):void 0},teardown:function(){return m.event.remove(this,"._change"),!X.test(this.nodeName)}}),k.focusinBubbles||m.each({focus:"focusin",blur:"focusout"},function(a,b){var c=function(a){m.event.simulate(b,a.target,m.event.fix(a),!0)};m.event.special[b]={setup:function(){var d=this.ownerDocument||this,e=m._data(d,b);e||d.addEventListener(a,c,!0),m._data(d,b,(e||0)+1)},teardown:function(){var d=this.ownerDocument||this,e=m._data(d,b)-1;e?m._data(d,b,e):(d.removeEventListener(a,c,!0),m._removeData(d,b))}}}),m.fn.extend({on:function(a,b,c,d,e){var f,g;if("object"==typeof a){"string"!=typeof b&&(c=c||b,b=void 0);for(f in a)this.on(f,b,c,a[f],e);return this}if(null==c&&null==d?(d=b,c=b=void 0):null==d&&("string"==typeof b?(d=c,c=void 0):(d=c,c=b,b=void 0)),d===!1)d=bb;else if(!d)return this;return 1===e&&(g=d,d=function(a){return m().off(a),g.apply(this,arguments)},d.guid=g.guid||(g.guid=m.guid++)),this.each(function(){m.event.add(this,a,d,c,b)})},one:function(a,b,c,d){return this.on(a,b,c,d,1)},off:function(a,b,c){var d,e;if(a&&a.preventDefault&&a.handleObj)return d=a.handleObj,m(a.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler),this;if("object"==typeof a){for(e in a)this.off(e,b,a[e]);return this}return(b===!1||"function"==typeof b)&&(c=b,b=void 0),c===!1&&(c=bb),this.each(function(){m.event.remove(this,a,c,b)})},trigger:function(a,b){return this.each(function(){m.event.trigger(a,b,this)})},triggerHandler:function(a,b){var c=this[0];return c?m.event.trigger(a,b,c,!0):void 0}});function db(a){var b=eb.split("|"),c=a.createDocumentFragment();if(c.createElement)while(b.length)c.createElement(b.pop());return c}var eb="abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",fb=/ jQuery\d+="(?:null|\d+)"/g,gb=new RegExp("<(?:"+eb+")[\\s/>]","i"),hb=/^\s+/,ib=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,jb=/<([\w:]+)/,kb=/<tbody/i,lb=/<|&#?\w+;/,mb=/<(?:script|style|link)/i,nb=/checked\s*(?:[^=]|=\s*.checked.)/i,ob=/^$|\/(?:java|ecma)script/i,pb=/^true\/(.*)/,qb=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,rb={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],area:[1,"<map>","</map>"],param:[1,"<object>","</object>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:k.htmlSerialize?[0,"",""]:[1,"X<div>","</div>"]},sb=db(y),tb=sb.appendChild(y.createElement("div"));rb.optgroup=rb.option,rb.tbody=rb.tfoot=rb.colgroup=rb.caption=rb.thead,rb.th=rb.td;function ub(a,b){var c,d,e=0,f=typeof a.getElementsByTagName!==K?a.getElementsByTagName(b||"*"):typeof a.querySelectorAll!==K?a.querySelectorAll(b||"*"):void 0;if(!f)for(f=[],c=a.childNodes||a;null!=(d=c[e]);e++)!b||m.nodeName(d,b)?f.push(d):m.merge(f,ub(d,b));return void 0===b||b&&m.nodeName(a,b)?m.merge([a],f):f}function vb(a){W.test(a.type)&&(a.defaultChecked=a.checked)}function wb(a,b){return m.nodeName(a,"table")&&m.nodeName(11!==b.nodeType?b:b.firstChild,"tr")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a}function xb(a){return a.type=(null!==m.find.attr(a,"type"))+"/"+a.type,a}function yb(a){var b=pb.exec(a.type);return b?a.type=b[1]:a.removeAttribute("type"),a}function zb(a,b){for(var c,d=0;null!=(c=a[d]);d++)m._data(c,"globalEval",!b||m._data(b[d],"globalEval"))}function Ab(a,b){if(1===b.nodeType&&m.hasData(a)){var c,d,e,f=m._data(a),g=m._data(b,f),h=f.events;if(h){delete g.handle,g.events={};for(c in h)for(d=0,e=h[c].length;e>d;d++)m.event.add(b,c,h[c][d])}g.data&&(g.data=m.extend({},g.data))}}function Bb(a,b){var c,d,e;if(1===b.nodeType){if(c=b.nodeName.toLowerCase(),!k.noCloneEvent&&b[m.expando]){e=m._data(b);for(d in e.events)m.removeEvent(b,d,e.handle);b.removeAttribute(m.expando)}"script"===c&&b.text!==a.text?(xb(b).text=a.text,yb(b)):"object"===c?(b.parentNode&&(b.outerHTML=a.outerHTML),k.html5Clone&&a.innerHTML&&!m.trim(b.innerHTML)&&(b.innerHTML=a.innerHTML)):"input"===c&&W.test(a.type)?(b.defaultChecked=b.checked=a.checked,b.value!==a.value&&(b.value=a.value)):"option"===c?b.defaultSelected=b.selected=a.defaultSelected:("input"===c||"textarea"===c)&&(b.defaultValue=a.defaultValue)}}m.extend({clone:function(a,b,c){var d,e,f,g,h,i=m.contains(a.ownerDocument,a);if(k.html5Clone||m.isXMLDoc(a)||!gb.test("<"+a.nodeName+">")?f=a.cloneNode(!0):(tb.innerHTML=a.outerHTML,tb.removeChild(f=tb.firstChild)),!(k.noCloneEvent&&k.noCloneChecked||1!==a.nodeType&&11!==a.nodeType||m.isXMLDoc(a)))for(d=ub(f),h=ub(a),g=0;null!=(e=h[g]);++g)d[g]&&Bb(e,d[g]);if(b)if(c)for(h=h||ub(a),d=d||ub(f),g=0;null!=(e=h[g]);g++)Ab(e,d[g]);else Ab(a,f);return d=ub(f,"script"),d.length>0&&zb(d,!i&&ub(a,"script")),d=h=e=null,f},buildFragment:function(a,b,c,d){for(var e,f,g,h,i,j,l,n=a.length,o=db(b),p=[],q=0;n>q;q++)if(f=a[q],f||0===f)if("object"===m.type(f))m.merge(p,f.nodeType?[f]:f);else if(lb.test(f)){h=h||o.appendChild(b.createElement("div")),i=(jb.exec(f)||["",""])[1].toLowerCase(),l=rb[i]||rb._default,h.innerHTML=l[1]+f.replace(ib,"<$1></$2>")+l[2],e=l[0];while(e--)h=h.lastChild;if(!k.leadingWhitespace&&hb.test(f)&&p.push(b.createTextNode(hb.exec(f)[0])),!k.tbody){f="table"!==i||kb.test(f)?"<table>"!==l[1]||kb.test(f)?0:h:h.firstChild,e=f&&f.childNodes.length;while(e--)m.nodeName(j=f.childNodes[e],"tbody")&&!j.childNodes.length&&f.removeChild(j)}m.merge(p,h.childNodes),h.textContent="";while(h.firstChild)h.removeChild(h.firstChild);h=o.lastChild}else p.push(b.createTextNode(f));h&&o.removeChild(h),k.appendChecked||m.grep(ub(p,"input"),vb),q=0;while(f=p[q++])if((!d||-1===m.inArray(f,d))&&(g=m.contains(f.ownerDocument,f),h=ub(o.appendChild(f),"script"),g&&zb(h),c)){e=0;while(f=h[e++])ob.test(f.type||"")&&c.push(f)}return h=null,o},cleanData:function(a,b){for(var d,e,f,g,h=0,i=m.expando,j=m.cache,l=k.deleteExpando,n=m.event.special;null!=(d=a[h]);h++)if((b||m.acceptData(d))&&(f=d[i],g=f&&j[f])){if(g.events)for(e in g.events)n[e]?m.event.remove(d,e):m.removeEvent(d,e,g.handle);j[f]&&(delete j[f],l?delete d[i]:typeof d.removeAttribute!==K?d.removeAttribute(i):d[i]=null,c.push(f))}}}),m.fn.extend({text:function(a){return V(this,function(a){return void 0===a?m.text(this):this.empty().append((this[0]&&this[0].ownerDocument||y).createTextNode(a))},null,a,arguments.length)},append:function(){return this.domManip(arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=wb(this,a);b.appendChild(a)}})},prepend:function(){return this.domManip(arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=wb(this,a);b.insertBefore(a,b.firstChild)}})},before:function(){return this.domManip(arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)})},after:function(){return this.domManip(arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)})},remove:function(a,b){for(var c,d=a?m.filter(a,this):this,e=0;null!=(c=d[e]);e++)b||1!==c.nodeType||m.cleanData(ub(c)),c.parentNode&&(b&&m.contains(c.ownerDocument,c)&&zb(ub(c,"script")),c.parentNode.removeChild(c));return this},empty:function(){for(var a,b=0;null!=(a=this[b]);b++){1===a.nodeType&&m.cleanData(ub(a,!1));while(a.firstChild)a.removeChild(a.firstChild);a.options&&m.nodeName(a,"select")&&(a.options.length=0)}return this},clone:function(a,b){return a=null==a?!1:a,b=null==b?a:b,this.map(function(){return m.clone(this,a,b)})},html:function(a){return V(this,function(a){var b=this[0]||{},c=0,d=this.length;if(void 0===a)return 1===b.nodeType?b.innerHTML.replace(fb,""):void 0;if(!("string"!=typeof a||mb.test(a)||!k.htmlSerialize&&gb.test(a)||!k.leadingWhitespace&&hb.test(a)||rb[(jb.exec(a)||["",""])[1].toLowerCase()])){a=a.replace(ib,"<$1></$2>");try{for(;d>c;c++)b=this[c]||{},1===b.nodeType&&(m.cleanData(ub(b,!1)),b.innerHTML=a);b=0}catch(e){}}b&&this.empty().append(a)},null,a,arguments.length)},replaceWith:function(){var a=arguments[0];return this.domManip(arguments,function(b){a=this.parentNode,m.cleanData(ub(this)),a&&a.replaceChild(b,this)}),a&&(a.length||a.nodeType)?this:this.remove()},detach:function(a){return this.remove(a,!0)},domManip:function(a,b){a=e.apply([],a);var c,d,f,g,h,i,j=0,l=this.length,n=this,o=l-1,p=a[0],q=m.isFunction(p);if(q||l>1&&"string"==typeof p&&!k.checkClone&&nb.test(p))return this.each(function(c){var d=n.eq(c);q&&(a[0]=p.call(this,c,d.html())),d.domManip(a,b)});if(l&&(i=m.buildFragment(a,this[0].ownerDocument,!1,this),c=i.firstChild,1===i.childNodes.length&&(i=c),c)){for(g=m.map(ub(i,"script"),xb),f=g.length;l>j;j++)d=i,j!==o&&(d=m.clone(d,!0,!0),f&&m.merge(g,ub(d,"script"))),b.call(this[j],d,j);if(f)for(h=g[g.length-1].ownerDocument,m.map(g,yb),j=0;f>j;j++)d=g[j],ob.test(d.type||"")&&!m._data(d,"globalEval")&&m.contains(h,d)&&(d.src?m._evalUrl&&m._evalUrl(d.src):m.globalEval((d.text||d.textContent||d.innerHTML||"").replace(qb,"")));i=c=null}return this}}),m.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){m.fn[a]=function(a){for(var c,d=0,e=[],g=m(a),h=g.length-1;h>=d;d++)c=d===h?this:this.clone(!0),m(g[d])[b](c),f.apply(e,c.get());return this.pushStack(e)}});var Cb,Db={};function Eb(b,c){var d,e=m(c.createElement(b)).appendTo(c.body),f=a.getDefaultComputedStyle&&(d=a.getDefaultComputedStyle(e[0]))?d.display:m.css(e[0],"display");return e.detach(),f}function Fb(a){var b=y,c=Db[a];return c||(c=Eb(a,b),"none"!==c&&c||(Cb=(Cb||m("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement),b=(Cb[0].contentWindow||Cb[0].contentDocument).document,b.write(),b.close(),c=Eb(a,b),Cb.detach()),Db[a]=c),c}!function(){var a;k.shrinkWrapBlocks=function(){if(null!=a)return a;a=!1;var b,c,d;return c=y.getElementsByTagName("body")[0],c&&c.style?(b=y.createElement("div"),d=y.createElement("div"),d.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(d).appendChild(b),typeof b.style.zoom!==K&&(b.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1",b.appendChild(y.createElement("div")).style.width="5px",a=3!==b.offsetWidth),c.removeChild(d),a):void 0}}();var Gb=/^margin/,Hb=new RegExp("^("+S+")(?!px)[a-z%]+$","i"),Ib,Jb,Kb=/^(top|right|bottom|left)$/;a.getComputedStyle?(Ib=function(a){return a.ownerDocument.defaultView.getComputedStyle(a,null)},Jb=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Ib(a),g=c?c.getPropertyValue(b)||c[b]:void 0,c&&(""!==g||m.contains(a.ownerDocument,a)||(g=m.style(a,b)),Hb.test(g)&&Gb.test(b)&&(d=h.width,e=h.minWidth,f=h.maxWidth,h.minWidth=h.maxWidth=h.width=g,g=c.width,h.width=d,h.minWidth=e,h.maxWidth=f)),void 0===g?g:g+""}):y.documentElement.currentStyle&&(Ib=function(a){return a.currentStyle},Jb=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Ib(a),g=c?c[b]:void 0,null==g&&h&&h[b]&&(g=h[b]),Hb.test(g)&&!Kb.test(b)&&(d=h.left,e=a.runtimeStyle,f=e&&e.left,f&&(e.left=a.currentStyle.left),h.left="fontSize"===b?"1em":g,g=h.pixelLeft+"px",h.left=d,f&&(e.left=f)),void 0===g?g:g+""||"auto"});function Lb(a,b){return{get:function(){var c=a();if(null!=c)return c?void delete this.get:(this.get=b).apply(this,arguments)}}}!function(){var b,c,d,e,f,g,h;if(b=y.createElement("div"),b.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",d=b.getElementsByTagName("a")[0],c=d&&d.style){c.cssText="float:left;opacity:.5",k.opacity="0.5"===c.opacity,k.cssFloat=!!c.cssFloat,b.style.backgroundClip="content-box",b.cloneNode(!0).style.backgroundClip="",k.clearCloneStyle="content-box"===b.style.backgroundClip,k.boxSizing=""===c.boxSizing||""===c.MozBoxSizing||""===c.WebkitBoxSizing,m.extend(k,{reliableHiddenOffsets:function(){return null==g&&i(),g},boxSizingReliable:function(){return null==f&&i(),f},pixelPosition:function(){return null==e&&i(),e},reliableMarginRight:function(){return null==h&&i(),h}});function i(){var b,c,d,i;c=y.getElementsByTagName("body")[0],c&&c.style&&(b=y.createElement("div"),d=y.createElement("div"),d.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(d).appendChild(b),b.style.cssText="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute",e=f=!1,h=!0,a.getComputedStyle&&(e="1%"!==(a.getComputedStyle(b,null)||{}).top,f="4px"===(a.getComputedStyle(b,null)||{width:"4px"}).width,i=b.appendChild(y.createElement("div")),i.style.cssText=b.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0",i.style.marginRight=i.style.width="0",b.style.width="1px",h=!parseFloat((a.getComputedStyle(i,null)||{}).marginRight)),b.innerHTML="<table><tr><td></td><td>t</td></tr></table>",i=b.getElementsByTagName("td"),i[0].style.cssText="margin:0;border:0;padding:0;display:none",g=0===i[0].offsetHeight,g&&(i[0].style.display="",i[1].style.display="none",g=0===i[0].offsetHeight),c.removeChild(d))}}}(),m.swap=function(a,b,c,d){var e,f,g={};for(f in b)g[f]=a.style[f],a.style[f]=b[f];e=c.apply(a,d||[]);for(f in b)a.style[f]=g[f];return e};var Mb=/alpha\([^)]*\)/i,Nb=/opacity\s*=\s*([^)]*)/,Ob=/^(none|table(?!-c[ea]).+)/,Pb=new RegExp("^("+S+")(.*)$","i"),Qb=new RegExp("^([+-])=("+S+")","i"),Rb={position:"absolute",visibility:"hidden",display:"block"},Sb={letterSpacing:"0",fontWeight:"400"},Tb=["Webkit","O","Moz","ms"];function Ub(a,b){if(b in a)return b;var c=b.charAt(0).toUpperCase()+b.slice(1),d=b,e=Tb.length;while(e--)if(b=Tb[e]+c,b in a)return b;return d}function Vb(a,b){for(var c,d,e,f=[],g=0,h=a.length;h>g;g++)d=a[g],d.style&&(f[g]=m._data(d,"olddisplay"),c=d.style.display,b?(f[g]||"none"!==c||(d.style.display=""),""===d.style.display&&U(d)&&(f[g]=m._data(d,"olddisplay",Fb(d.nodeName)))):(e=U(d),(c&&"none"!==c||!e)&&m._data(d,"olddisplay",e?c:m.css(d,"display"))));for(g=0;h>g;g++)d=a[g],d.style&&(b&&"none"!==d.style.display&&""!==d.style.display||(d.style.display=b?f[g]||"":"none"));return a}function Wb(a,b,c){var d=Pb.exec(b);return d?Math.max(0,d[1]-(c||0))+(d[2]||"px"):b}function Xb(a,b,c,d,e){for(var f=c===(d?"border":"content")?4:"width"===b?1:0,g=0;4>f;f+=2)"margin"===c&&(g+=m.css(a,c+T[f],!0,e)),d?("content"===c&&(g-=m.css(a,"padding"+T[f],!0,e)),"margin"!==c&&(g-=m.css(a,"border"+T[f]+"Width",!0,e))):(g+=m.css(a,"padding"+T[f],!0,e),"padding"!==c&&(g+=m.css(a,"border"+T[f]+"Width",!0,e)));return g}function Yb(a,b,c){var d=!0,e="width"===b?a.offsetWidth:a.offsetHeight,f=Ib(a),g=k.boxSizing&&"border-box"===m.css(a,"boxSizing",!1,f);if(0>=e||null==e){if(e=Jb(a,b,f),(0>e||null==e)&&(e=a.style[b]),Hb.test(e))return e;d=g&&(k.boxSizingReliable()||e===a.style[b]),e=parseFloat(e)||0}return e+Xb(a,b,c||(g?"border":"content"),d,f)+"px"}m.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=Jb(a,"opacity");return""===c?"1":c}}}},cssNumber:{columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":k.cssFloat?"cssFloat":"styleFloat"},style:function(a,b,c,d){if(a&&3!==a.nodeType&&8!==a.nodeType&&a.style){var e,f,g,h=m.camelCase(b),i=a.style;if(b=m.cssProps[h]||(m.cssProps[h]=Ub(i,h)),g=m.cssHooks[b]||m.cssHooks[h],void 0===c)return g&&"get"in g&&void 0!==(e=g.get(a,!1,d))?e:i[b];if(f=typeof c,"string"===f&&(e=Qb.exec(c))&&(c=(e[1]+1)*e[2]+parseFloat(m.css(a,b)),f="number"),null!=c&&c===c&&("number"!==f||m.cssNumber[h]||(c+="px"),k.clearCloneStyle||""!==c||0!==b.indexOf("background")||(i[b]="inherit"),!(g&&"set"in g&&void 0===(c=g.set(a,c,d)))))try{i[b]=c}catch(j){}}},css:function(a,b,c,d){var e,f,g,h=m.camelCase(b);return b=m.cssProps[h]||(m.cssProps[h]=Ub(a.style,h)),g=m.cssHooks[b]||m.cssHooks[h],g&&"get"in g&&(f=g.get(a,!0,c)),void 0===f&&(f=Jb(a,b,d)),"normal"===f&&b in Sb&&(f=Sb[b]),""===c||c?(e=parseFloat(f),c===!0||m.isNumeric(e)?e||0:f):f}}),m.each(["height","width"],function(a,b){m.cssHooks[b]={get:function(a,c,d){return c?Ob.test(m.css(a,"display"))&&0===a.offsetWidth?m.swap(a,Rb,function(){return Yb(a,b,d)}):Yb(a,b,d):void 0},set:function(a,c,d){var e=d&&Ib(a);return Wb(a,c,d?Xb(a,b,d,k.boxSizing&&"border-box"===m.css(a,"boxSizing",!1,e),e):0)}}}),k.opacity||(m.cssHooks.opacity={get:function(a,b){return Nb.test((b&&a.currentStyle?a.currentStyle.filter:a.style.filter)||"")?.01*parseFloat(RegExp.$1)+"":b?"1":""},set:function(a,b){var c=a.style,d=a.currentStyle,e=m.isNumeric(b)?"alpha(opacity="+100*b+")":"",f=d&&d.filter||c.filter||"";c.zoom=1,(b>=1||""===b)&&""===m.trim(f.replace(Mb,""))&&c.removeAttribute&&(c.removeAttribute("filter"),""===b||d&&!d.filter)||(c.filter=Mb.test(f)?f.replace(Mb,e):f+" "+e)}}),m.cssHooks.marginRight=Lb(k.reliableMarginRight,function(a,b){return b?m.swap(a,{display:"inline-block"},Jb,[a,"marginRight"]):void 0}),m.each({margin:"",padding:"",border:"Width"},function(a,b){m.cssHooks[a+b]={expand:function(c){for(var d=0,e={},f="string"==typeof c?c.split(" "):[c];4>d;d++)e[a+T[d]+b]=f[d]||f[d-2]||f[0];return e}},Gb.test(a)||(m.cssHooks[a+b].set=Wb)}),m.fn.extend({css:function(a,b){return V(this,function(a,b,c){var d,e,f={},g=0;if(m.isArray(b)){for(d=Ib(a),e=b.length;e>g;g++)f[b[g]]=m.css(a,b[g],!1,d);return f}return void 0!==c?m.style(a,b,c):m.css(a,b)},a,b,arguments.length>1)},show:function(){return Vb(this,!0)},hide:function(){return Vb(this)},toggle:function(a){return"boolean"==typeof a?a?this.show():this.hide():this.each(function(){U(this)?m(this).show():m(this).hide()})}});function Zb(a,b,c,d,e){return new Zb.prototype.init(a,b,c,d,e)}m.Tween=Zb,Zb.prototype={constructor:Zb,init:function(a,b,c,d,e,f){this.elem=a,this.prop=c,this.easing=e||"swing",this.options=b,this.start=this.now=this.cur(),this.end=d,this.unit=f||(m.cssNumber[c]?"":"px")
},cur:function(){var a=Zb.propHooks[this.prop];return a&&a.get?a.get(this):Zb.propHooks._default.get(this)},run:function(a){var b,c=Zb.propHooks[this.prop];return this.pos=b=this.options.duration?m.easing[this.easing](a,this.options.duration*a,0,1,this.options.duration):a,this.now=(this.end-this.start)*b+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),c&&c.set?c.set(this):Zb.propHooks._default.set(this),this}},Zb.prototype.init.prototype=Zb.prototype,Zb.propHooks={_default:{get:function(a){var b;return null==a.elem[a.prop]||a.elem.style&&null!=a.elem.style[a.prop]?(b=m.css(a.elem,a.prop,""),b&&"auto"!==b?b:0):a.elem[a.prop]},set:function(a){m.fx.step[a.prop]?m.fx.step[a.prop](a):a.elem.style&&(null!=a.elem.style[m.cssProps[a.prop]]||m.cssHooks[a.prop])?m.style(a.elem,a.prop,a.now+a.unit):a.elem[a.prop]=a.now}}},Zb.propHooks.scrollTop=Zb.propHooks.scrollLeft={set:function(a){a.elem.nodeType&&a.elem.parentNode&&(a.elem[a.prop]=a.now)}},m.easing={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2}},m.fx=Zb.prototype.init,m.fx.step={};var $b,_b,ac=/^(?:toggle|show|hide)$/,bc=new RegExp("^(?:([+-])=|)("+S+")([a-z%]*)$","i"),cc=/queueHooks$/,dc=[ic],ec={"*":[function(a,b){var c=this.createTween(a,b),d=c.cur(),e=bc.exec(b),f=e&&e[3]||(m.cssNumber[a]?"":"px"),g=(m.cssNumber[a]||"px"!==f&&+d)&&bc.exec(m.css(c.elem,a)),h=1,i=20;if(g&&g[3]!==f){f=f||g[3],e=e||[],g=+d||1;do h=h||".5",g/=h,m.style(c.elem,a,g+f);while(h!==(h=c.cur()/d)&&1!==h&&--i)}return e&&(g=c.start=+g||+d||0,c.unit=f,c.end=e[1]?g+(e[1]+1)*e[2]:+e[2]),c}]};function fc(){return setTimeout(function(){$b=void 0}),$b=m.now()}function gc(a,b){var c,d={height:a},e=0;for(b=b?1:0;4>e;e+=2-b)c=T[e],d["margin"+c]=d["padding"+c]=a;return b&&(d.opacity=d.width=a),d}function hc(a,b,c){for(var d,e=(ec[b]||[]).concat(ec["*"]),f=0,g=e.length;g>f;f++)if(d=e[f].call(c,b,a))return d}function ic(a,b,c){var d,e,f,g,h,i,j,l,n=this,o={},p=a.style,q=a.nodeType&&U(a),r=m._data(a,"fxshow");c.queue||(h=m._queueHooks(a,"fx"),null==h.unqueued&&(h.unqueued=0,i=h.empty.fire,h.empty.fire=function(){h.unqueued||i()}),h.unqueued++,n.always(function(){n.always(function(){h.unqueued--,m.queue(a,"fx").length||h.empty.fire()})})),1===a.nodeType&&("height"in b||"width"in b)&&(c.overflow=[p.overflow,p.overflowX,p.overflowY],j=m.css(a,"display"),l="none"===j?m._data(a,"olddisplay")||Fb(a.nodeName):j,"inline"===l&&"none"===m.css(a,"float")&&(k.inlineBlockNeedsLayout&&"inline"!==Fb(a.nodeName)?p.zoom=1:p.display="inline-block")),c.overflow&&(p.overflow="hidden",k.shrinkWrapBlocks()||n.always(function(){p.overflow=c.overflow[0],p.overflowX=c.overflow[1],p.overflowY=c.overflow[2]}));for(d in b)if(e=b[d],ac.exec(e)){if(delete b[d],f=f||"toggle"===e,e===(q?"hide":"show")){if("show"!==e||!r||void 0===r[d])continue;q=!0}o[d]=r&&r[d]||m.style(a,d)}else j=void 0;if(m.isEmptyObject(o))"inline"===("none"===j?Fb(a.nodeName):j)&&(p.display=j);else{r?"hidden"in r&&(q=r.hidden):r=m._data(a,"fxshow",{}),f&&(r.hidden=!q),q?m(a).show():n.done(function(){m(a).hide()}),n.done(function(){var b;m._removeData(a,"fxshow");for(b in o)m.style(a,b,o[b])});for(d in o)g=hc(q?r[d]:0,d,n),d in r||(r[d]=g.start,q&&(g.end=g.start,g.start="width"===d||"height"===d?1:0))}}function jc(a,b){var c,d,e,f,g;for(c in a)if(d=m.camelCase(c),e=b[d],f=a[c],m.isArray(f)&&(e=f[1],f=a[c]=f[0]),c!==d&&(a[d]=f,delete a[c]),g=m.cssHooks[d],g&&"expand"in g){f=g.expand(f),delete a[d];for(c in f)c in a||(a[c]=f[c],b[c]=e)}else b[d]=e}function kc(a,b,c){var d,e,f=0,g=dc.length,h=m.Deferred().always(function(){delete i.elem}),i=function(){if(e)return!1;for(var b=$b||fc(),c=Math.max(0,j.startTime+j.duration-b),d=c/j.duration||0,f=1-d,g=0,i=j.tweens.length;i>g;g++)j.tweens[g].run(f);return h.notifyWith(a,[j,f,c]),1>f&&i?c:(h.resolveWith(a,[j]),!1)},j=h.promise({elem:a,props:m.extend({},b),opts:m.extend(!0,{specialEasing:{}},c),originalProperties:b,originalOptions:c,startTime:$b||fc(),duration:c.duration,tweens:[],createTween:function(b,c){var d=m.Tween(a,j.opts,b,c,j.opts.specialEasing[b]||j.opts.easing);return j.tweens.push(d),d},stop:function(b){var c=0,d=b?j.tweens.length:0;if(e)return this;for(e=!0;d>c;c++)j.tweens[c].run(1);return b?h.resolveWith(a,[j,b]):h.rejectWith(a,[j,b]),this}}),k=j.props;for(jc(k,j.opts.specialEasing);g>f;f++)if(d=dc[f].call(j,a,k,j.opts))return d;return m.map(k,hc,j),m.isFunction(j.opts.start)&&j.opts.start.call(a,j),m.fx.timer(m.extend(i,{elem:a,anim:j,queue:j.opts.queue})),j.progress(j.opts.progress).done(j.opts.done,j.opts.complete).fail(j.opts.fail).always(j.opts.always)}m.Animation=m.extend(kc,{tweener:function(a,b){m.isFunction(a)?(b=a,a=["*"]):a=a.split(" ");for(var c,d=0,e=a.length;e>d;d++)c=a[d],ec[c]=ec[c]||[],ec[c].unshift(b)},prefilter:function(a,b){b?dc.unshift(a):dc.push(a)}}),m.speed=function(a,b,c){var d=a&&"object"==typeof a?m.extend({},a):{complete:c||!c&&b||m.isFunction(a)&&a,duration:a,easing:c&&b||b&&!m.isFunction(b)&&b};return d.duration=m.fx.off?0:"number"==typeof d.duration?d.duration:d.duration in m.fx.speeds?m.fx.speeds[d.duration]:m.fx.speeds._default,(null==d.queue||d.queue===!0)&&(d.queue="fx"),d.old=d.complete,d.complete=function(){m.isFunction(d.old)&&d.old.call(this),d.queue&&m.dequeue(this,d.queue)},d},m.fn.extend({fadeTo:function(a,b,c,d){return this.filter(U).css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){var e=m.isEmptyObject(a),f=m.speed(b,c,d),g=function(){var b=kc(this,m.extend({},a),f);(e||m._data(this,"finish"))&&b.stop(!0)};return g.finish=g,e||f.queue===!1?this.each(g):this.queue(f.queue,g)},stop:function(a,b,c){var d=function(a){var b=a.stop;delete a.stop,b(c)};return"string"!=typeof a&&(c=b,b=a,a=void 0),b&&a!==!1&&this.queue(a||"fx",[]),this.each(function(){var b=!0,e=null!=a&&a+"queueHooks",f=m.timers,g=m._data(this);if(e)g[e]&&g[e].stop&&d(g[e]);else for(e in g)g[e]&&g[e].stop&&cc.test(e)&&d(g[e]);for(e=f.length;e--;)f[e].elem!==this||null!=a&&f[e].queue!==a||(f[e].anim.stop(c),b=!1,f.splice(e,1));(b||!c)&&m.dequeue(this,a)})},finish:function(a){return a!==!1&&(a=a||"fx"),this.each(function(){var b,c=m._data(this),d=c[a+"queue"],e=c[a+"queueHooks"],f=m.timers,g=d?d.length:0;for(c.finish=!0,m.queue(this,a,[]),e&&e.stop&&e.stop.call(this,!0),b=f.length;b--;)f[b].elem===this&&f[b].queue===a&&(f[b].anim.stop(!0),f.splice(b,1));for(b=0;g>b;b++)d[b]&&d[b].finish&&d[b].finish.call(this);delete c.finish})}}),m.each(["toggle","show","hide"],function(a,b){var c=m.fn[b];m.fn[b]=function(a,d,e){return null==a||"boolean"==typeof a?c.apply(this,arguments):this.animate(gc(b,!0),a,d,e)}}),m.each({slideDown:gc("show"),slideUp:gc("hide"),slideToggle:gc("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){m.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),m.timers=[],m.fx.tick=function(){var a,b=m.timers,c=0;for($b=m.now();c<b.length;c++)a=b[c],a()||b[c]!==a||b.splice(c--,1);b.length||m.fx.stop(),$b=void 0},m.fx.timer=function(a){m.timers.push(a),a()?m.fx.start():m.timers.pop()},m.fx.interval=13,m.fx.start=function(){_b||(_b=setInterval(m.fx.tick,m.fx.interval))},m.fx.stop=function(){clearInterval(_b),_b=null},m.fx.speeds={slow:600,fast:200,_default:400},m.fn.delay=function(a,b){return a=m.fx?m.fx.speeds[a]||a:a,b=b||"fx",this.queue(b,function(b,c){var d=setTimeout(b,a);c.stop=function(){clearTimeout(d)}})},function(){var a,b,c,d,e;b=y.createElement("div"),b.setAttribute("className","t"),b.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",d=b.getElementsByTagName("a")[0],c=y.createElement("select"),e=c.appendChild(y.createElement("option")),a=b.getElementsByTagName("input")[0],d.style.cssText="top:1px",k.getSetAttribute="t"!==b.className,k.style=/top/.test(d.getAttribute("style")),k.hrefNormalized="/a"===d.getAttribute("href"),k.checkOn=!!a.value,k.optSelected=e.selected,k.enctype=!!y.createElement("form").enctype,c.disabled=!0,k.optDisabled=!e.disabled,a=y.createElement("input"),a.setAttribute("value",""),k.input=""===a.getAttribute("value"),a.value="t",a.setAttribute("type","radio"),k.radioValue="t"===a.value}();var lc=/\r/g;m.fn.extend({val:function(a){var b,c,d,e=this[0];{if(arguments.length)return d=m.isFunction(a),this.each(function(c){var e;1===this.nodeType&&(e=d?a.call(this,c,m(this).val()):a,null==e?e="":"number"==typeof e?e+="":m.isArray(e)&&(e=m.map(e,function(a){return null==a?"":a+""})),b=m.valHooks[this.type]||m.valHooks[this.nodeName.toLowerCase()],b&&"set"in b&&void 0!==b.set(this,e,"value")||(this.value=e))});if(e)return b=m.valHooks[e.type]||m.valHooks[e.nodeName.toLowerCase()],b&&"get"in b&&void 0!==(c=b.get(e,"value"))?c:(c=e.value,"string"==typeof c?c.replace(lc,""):null==c?"":c)}}}),m.extend({valHooks:{option:{get:function(a){var b=m.find.attr(a,"value");return null!=b?b:m.trim(m.text(a))}},select:{get:function(a){for(var b,c,d=a.options,e=a.selectedIndex,f="select-one"===a.type||0>e,g=f?null:[],h=f?e+1:d.length,i=0>e?h:f?e:0;h>i;i++)if(c=d[i],!(!c.selected&&i!==e||(k.optDisabled?c.disabled:null!==c.getAttribute("disabled"))||c.parentNode.disabled&&m.nodeName(c.parentNode,"optgroup"))){if(b=m(c).val(),f)return b;g.push(b)}return g},set:function(a,b){var c,d,e=a.options,f=m.makeArray(b),g=e.length;while(g--)if(d=e[g],m.inArray(m.valHooks.option.get(d),f)>=0)try{d.selected=c=!0}catch(h){d.scrollHeight}else d.selected=!1;return c||(a.selectedIndex=-1),e}}}}),m.each(["radio","checkbox"],function(){m.valHooks[this]={set:function(a,b){return m.isArray(b)?a.checked=m.inArray(m(a).val(),b)>=0:void 0}},k.checkOn||(m.valHooks[this].get=function(a){return null===a.getAttribute("value")?"on":a.value})});var mc,nc,oc=m.expr.attrHandle,pc=/^(?:checked|selected)$/i,qc=k.getSetAttribute,rc=k.input;m.fn.extend({attr:function(a,b){return V(this,m.attr,a,b,arguments.length>1)},removeAttr:function(a){return this.each(function(){m.removeAttr(this,a)})}}),m.extend({attr:function(a,b,c){var d,e,f=a.nodeType;if(a&&3!==f&&8!==f&&2!==f)return typeof a.getAttribute===K?m.prop(a,b,c):(1===f&&m.isXMLDoc(a)||(b=b.toLowerCase(),d=m.attrHooks[b]||(m.expr.match.bool.test(b)?nc:mc)),void 0===c?d&&"get"in d&&null!==(e=d.get(a,b))?e:(e=m.find.attr(a,b),null==e?void 0:e):null!==c?d&&"set"in d&&void 0!==(e=d.set(a,c,b))?e:(a.setAttribute(b,c+""),c):void m.removeAttr(a,b))},removeAttr:function(a,b){var c,d,e=0,f=b&&b.match(E);if(f&&1===a.nodeType)while(c=f[e++])d=m.propFix[c]||c,m.expr.match.bool.test(c)?rc&&qc||!pc.test(c)?a[d]=!1:a[m.camelCase("default-"+c)]=a[d]=!1:m.attr(a,c,""),a.removeAttribute(qc?c:d)},attrHooks:{type:{set:function(a,b){if(!k.radioValue&&"radio"===b&&m.nodeName(a,"input")){var c=a.value;return a.setAttribute("type",b),c&&(a.value=c),b}}}}}),nc={set:function(a,b,c){return b===!1?m.removeAttr(a,c):rc&&qc||!pc.test(c)?a.setAttribute(!qc&&m.propFix[c]||c,c):a[m.camelCase("default-"+c)]=a[c]=!0,c}},m.each(m.expr.match.bool.source.match(/\w+/g),function(a,b){var c=oc[b]||m.find.attr;oc[b]=rc&&qc||!pc.test(b)?function(a,b,d){var e,f;return d||(f=oc[b],oc[b]=e,e=null!=c(a,b,d)?b.toLowerCase():null,oc[b]=f),e}:function(a,b,c){return c?void 0:a[m.camelCase("default-"+b)]?b.toLowerCase():null}}),rc&&qc||(m.attrHooks.value={set:function(a,b,c){return m.nodeName(a,"input")?void(a.defaultValue=b):mc&&mc.set(a,b,c)}}),qc||(mc={set:function(a,b,c){var d=a.getAttributeNode(c);return d||a.setAttributeNode(d=a.ownerDocument.createAttribute(c)),d.value=b+="","value"===c||b===a.getAttribute(c)?b:void 0}},oc.id=oc.name=oc.coords=function(a,b,c){var d;return c?void 0:(d=a.getAttributeNode(b))&&""!==d.value?d.value:null},m.valHooks.button={get:function(a,b){var c=a.getAttributeNode(b);return c&&c.specified?c.value:void 0},set:mc.set},m.attrHooks.contenteditable={set:function(a,b,c){mc.set(a,""===b?!1:b,c)}},m.each(["width","height"],function(a,b){m.attrHooks[b]={set:function(a,c){return""===c?(a.setAttribute(b,"auto"),c):void 0}}})),k.style||(m.attrHooks.style={get:function(a){return a.style.cssText||void 0},set:function(a,b){return a.style.cssText=b+""}});var sc=/^(?:input|select|textarea|button|object)$/i,tc=/^(?:a|area)$/i;m.fn.extend({prop:function(a,b){return V(this,m.prop,a,b,arguments.length>1)},removeProp:function(a){return a=m.propFix[a]||a,this.each(function(){try{this[a]=void 0,delete this[a]}catch(b){}})}}),m.extend({propFix:{"for":"htmlFor","class":"className"},prop:function(a,b,c){var d,e,f,g=a.nodeType;if(a&&3!==g&&8!==g&&2!==g)return f=1!==g||!m.isXMLDoc(a),f&&(b=m.propFix[b]||b,e=m.propHooks[b]),void 0!==c?e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:a[b]=c:e&&"get"in e&&null!==(d=e.get(a,b))?d:a[b]},propHooks:{tabIndex:{get:function(a){var b=m.find.attr(a,"tabindex");return b?parseInt(b,10):sc.test(a.nodeName)||tc.test(a.nodeName)&&a.href?0:-1}}}}),k.hrefNormalized||m.each(["href","src"],function(a,b){m.propHooks[b]={get:function(a){return a.getAttribute(b,4)}}}),k.optSelected||(m.propHooks.selected={get:function(a){var b=a.parentNode;return b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex),null}}),m.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){m.propFix[this.toLowerCase()]=this}),k.enctype||(m.propFix.enctype="encoding");var uc=/[\t\r\n\f]/g;m.fn.extend({addClass:function(a){var b,c,d,e,f,g,h=0,i=this.length,j="string"==typeof a&&a;if(m.isFunction(a))return this.each(function(b){m(this).addClass(a.call(this,b,this.className))});if(j)for(b=(a||"").match(E)||[];i>h;h++)if(c=this[h],d=1===c.nodeType&&(c.className?(" "+c.className+" ").replace(uc," "):" ")){f=0;while(e=b[f++])d.indexOf(" "+e+" ")<0&&(d+=e+" ");g=m.trim(d),c.className!==g&&(c.className=g)}return this},removeClass:function(a){var b,c,d,e,f,g,h=0,i=this.length,j=0===arguments.length||"string"==typeof a&&a;if(m.isFunction(a))return this.each(function(b){m(this).removeClass(a.call(this,b,this.className))});if(j)for(b=(a||"").match(E)||[];i>h;h++)if(c=this[h],d=1===c.nodeType&&(c.className?(" "+c.className+" ").replace(uc," "):"")){f=0;while(e=b[f++])while(d.indexOf(" "+e+" ")>=0)d=d.replace(" "+e+" "," ");g=a?m.trim(d):"",c.className!==g&&(c.className=g)}return this},toggleClass:function(a,b){var c=typeof a;return"boolean"==typeof b&&"string"===c?b?this.addClass(a):this.removeClass(a):this.each(m.isFunction(a)?function(c){m(this).toggleClass(a.call(this,c,this.className,b),b)}:function(){if("string"===c){var b,d=0,e=m(this),f=a.match(E)||[];while(b=f[d++])e.hasClass(b)?e.removeClass(b):e.addClass(b)}else(c===K||"boolean"===c)&&(this.className&&m._data(this,"__className__",this.className),this.className=this.className||a===!1?"":m._data(this,"__className__")||"")})},hasClass:function(a){for(var b=" "+a+" ",c=0,d=this.length;d>c;c++)if(1===this[c].nodeType&&(" "+this[c].className+" ").replace(uc," ").indexOf(b)>=0)return!0;return!1}}),m.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){m.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)}}),m.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)},bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return 1===arguments.length?this.off(a,"**"):this.off(b,a||"**",c)}});var vc=m.now(),wc=/\?/,xc=/(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;m.parseJSON=function(b){if(a.JSON&&a.JSON.parse)return a.JSON.parse(b+"");var c,d=null,e=m.trim(b+"");return e&&!m.trim(e.replace(xc,function(a,b,e,f){return c&&b&&(d=0),0===d?a:(c=e||b,d+=!f-!e,"")}))?Function("return "+e)():m.error("Invalid JSON: "+b)},m.parseXML=function(b){var c,d;if(!b||"string"!=typeof b)return null;try{a.DOMParser?(d=new DOMParser,c=d.parseFromString(b,"text/xml")):(c=new ActiveXObject("Microsoft.XMLDOM"),c.async="false",c.loadXML(b))}catch(e){c=void 0}return c&&c.documentElement&&!c.getElementsByTagName("parsererror").length||m.error("Invalid XML: "+b),c};var yc,zc,Ac=/#.*$/,Bc=/([?&])_=[^&]*/,Cc=/^(.*?):[ \t]*([^\r\n]*)\r?$/gm,Dc=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,Ec=/^(?:GET|HEAD)$/,Fc=/^\/\//,Gc=/^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,Hc={},Ic={},Jc="*/".concat("*");try{zc=location.href}catch(Kc){zc=y.createElement("a"),zc.href="",zc=zc.href}yc=Gc.exec(zc.toLowerCase())||[];function Lc(a){return function(b,c){"string"!=typeof b&&(c=b,b="*");var d,e=0,f=b.toLowerCase().match(E)||[];if(m.isFunction(c))while(d=f[e++])"+"===d.charAt(0)?(d=d.slice(1)||"*",(a[d]=a[d]||[]).unshift(c)):(a[d]=a[d]||[]).push(c)}}function Mc(a,b,c,d){var e={},f=a===Ic;function g(h){var i;return e[h]=!0,m.each(a[h]||[],function(a,h){var j=h(b,c,d);return"string"!=typeof j||f||e[j]?f?!(i=j):void 0:(b.dataTypes.unshift(j),g(j),!1)}),i}return g(b.dataTypes[0])||!e["*"]&&g("*")}function Nc(a,b){var c,d,e=m.ajaxSettings.flatOptions||{};for(d in b)void 0!==b[d]&&((e[d]?a:c||(c={}))[d]=b[d]);return c&&m.extend(!0,a,c),a}function Oc(a,b,c){var d,e,f,g,h=a.contents,i=a.dataTypes;while("*"===i[0])i.shift(),void 0===e&&(e=a.mimeType||b.getResponseHeader("Content-Type"));if(e)for(g in h)if(h[g]&&h[g].test(e)){i.unshift(g);break}if(i[0]in c)f=i[0];else{for(g in c){if(!i[0]||a.converters[g+" "+i[0]]){f=g;break}d||(d=g)}f=f||d}return f?(f!==i[0]&&i.unshift(f),c[f]):void 0}function Pc(a,b,c,d){var e,f,g,h,i,j={},k=a.dataTypes.slice();if(k[1])for(g in a.converters)j[g.toLowerCase()]=a.converters[g];f=k.shift();while(f)if(a.responseFields[f]&&(c[a.responseFields[f]]=b),!i&&d&&a.dataFilter&&(b=a.dataFilter(b,a.dataType)),i=f,f=k.shift())if("*"===f)f=i;else if("*"!==i&&i!==f){if(g=j[i+" "+f]||j["* "+f],!g)for(e in j)if(h=e.split(" "),h[1]===f&&(g=j[i+" "+h[0]]||j["* "+h[0]])){g===!0?g=j[e]:j[e]!==!0&&(f=h[0],k.unshift(h[1]));break}if(g!==!0)if(g&&a["throws"])b=g(b);else try{b=g(b)}catch(l){return{state:"parsererror",error:g?l:"No conversion from "+i+" to "+f}}}return{state:"success",data:b}}m.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:zc,type:"GET",isLocal:Dc.test(yc[1]),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":Jc,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/xml/,html:/html/,json:/json/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":m.parseJSON,"text xml":m.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(a,b){return b?Nc(Nc(a,m.ajaxSettings),b):Nc(m.ajaxSettings,a)},ajaxPrefilter:Lc(Hc),ajaxTransport:Lc(Ic),ajax:function(a,b){"object"==typeof a&&(b=a,a=void 0),b=b||{};var c,d,e,f,g,h,i,j,k=m.ajaxSetup({},b),l=k.context||k,n=k.context&&(l.nodeType||l.jquery)?m(l):m.event,o=m.Deferred(),p=m.Callbacks("once memory"),q=k.statusCode||{},r={},s={},t=0,u="canceled",v={readyState:0,getResponseHeader:function(a){var b;if(2===t){if(!j){j={};while(b=Cc.exec(f))j[b[1].toLowerCase()]=b[2]}b=j[a.toLowerCase()]}return null==b?null:b},getAllResponseHeaders:function(){return 2===t?f:null},setRequestHeader:function(a,b){var c=a.toLowerCase();return t||(a=s[c]=s[c]||a,r[a]=b),this},overrideMimeType:function(a){return t||(k.mimeType=a),this},statusCode:function(a){var b;if(a)if(2>t)for(b in a)q[b]=[q[b],a[b]];else v.always(a[v.status]);return this},abort:function(a){var b=a||u;return i&&i.abort(b),x(0,b),this}};if(o.promise(v).complete=p.add,v.success=v.done,v.error=v.fail,k.url=((a||k.url||zc)+"").replace(Ac,"").replace(Fc,yc[1]+"//"),k.type=b.method||b.type||k.method||k.type,k.dataTypes=m.trim(k.dataType||"*").toLowerCase().match(E)||[""],null==k.crossDomain&&(c=Gc.exec(k.url.toLowerCase()),k.crossDomain=!(!c||c[1]===yc[1]&&c[2]===yc[2]&&(c[3]||("http:"===c[1]?"80":"443"))===(yc[3]||("http:"===yc[1]?"80":"443")))),k.data&&k.processData&&"string"!=typeof k.data&&(k.data=m.param(k.data,k.traditional)),Mc(Hc,k,b,v),2===t)return v;h=k.global,h&&0===m.active++&&m.event.trigger("ajaxStart"),k.type=k.type.toUpperCase(),k.hasContent=!Ec.test(k.type),e=k.url,k.hasContent||(k.data&&(e=k.url+=(wc.test(e)?"&":"?")+k.data,delete k.data),k.cache===!1&&(k.url=Bc.test(e)?e.replace(Bc,"$1_="+vc++):e+(wc.test(e)?"&":"?")+"_="+vc++)),k.ifModified&&(m.lastModified[e]&&v.setRequestHeader("If-Modified-Since",m.lastModified[e]),m.etag[e]&&v.setRequestHeader("If-None-Match",m.etag[e])),(k.data&&k.hasContent&&k.contentType!==!1||b.contentType)&&v.setRequestHeader("Content-Type",k.contentType),v.setRequestHeader("Accept",k.dataTypes[0]&&k.accepts[k.dataTypes[0]]?k.accepts[k.dataTypes[0]]+("*"!==k.dataTypes[0]?", "+Jc+"; q=0.01":""):k.accepts["*"]);for(d in k.headers)v.setRequestHeader(d,k.headers[d]);if(k.beforeSend&&(k.beforeSend.call(l,v,k)===!1||2===t))return v.abort();u="abort";for(d in{success:1,error:1,complete:1})v[d](k[d]);if(i=Mc(Ic,k,b,v)){v.readyState=1,h&&n.trigger("ajaxSend",[v,k]),k.async&&k.timeout>0&&(g=setTimeout(function(){v.abort("timeout")},k.timeout));try{t=1,i.send(r,x)}catch(w){if(!(2>t))throw w;x(-1,w)}}else x(-1,"No Transport");function x(a,b,c,d){var j,r,s,u,w,x=b;2!==t&&(t=2,g&&clearTimeout(g),i=void 0,f=d||"",v.readyState=a>0?4:0,j=a>=200&&300>a||304===a,c&&(u=Oc(k,v,c)),u=Pc(k,u,v,j),j?(k.ifModified&&(w=v.getResponseHeader("Last-Modified"),w&&(m.lastModified[e]=w),w=v.getResponseHeader("etag"),w&&(m.etag[e]=w)),204===a||"HEAD"===k.type?x="nocontent":304===a?x="notmodified":(x=u.state,r=u.data,s=u.error,j=!s)):(s=x,(a||!x)&&(x="error",0>a&&(a=0))),v.status=a,v.statusText=(b||x)+"",j?o.resolveWith(l,[r,x,v]):o.rejectWith(l,[v,x,s]),v.statusCode(q),q=void 0,h&&n.trigger(j?"ajaxSuccess":"ajaxError",[v,k,j?r:s]),p.fireWith(l,[v,x]),h&&(n.trigger("ajaxComplete",[v,k]),--m.active||m.event.trigger("ajaxStop")))}return v},getJSON:function(a,b,c){return m.get(a,b,c,"json")},getScript:function(a,b){return m.get(a,void 0,b,"script")}}),m.each(["get","post"],function(a,b){m[b]=function(a,c,d,e){return m.isFunction(c)&&(e=e||d,d=c,c=void 0),m.ajax({url:a,type:b,dataType:e,data:c,success:d})}}),m.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(a,b){m.fn[b]=function(a){return this.on(b,a)}}),m._evalUrl=function(a){return m.ajax({url:a,type:"GET",dataType:"script",async:!1,global:!1,"throws":!0})},m.fn.extend({wrapAll:function(a){if(m.isFunction(a))return this.each(function(b){m(this).wrapAll(a.call(this,b))});if(this[0]){var b=m(a,this[0].ownerDocument).eq(0).clone(!0);this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstChild&&1===a.firstChild.nodeType)a=a.firstChild;return a}).append(this)}return this},wrapInner:function(a){return this.each(m.isFunction(a)?function(b){m(this).wrapInner(a.call(this,b))}:function(){var b=m(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=m.isFunction(a);return this.each(function(c){m(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(){return this.parent().each(function(){m.nodeName(this,"body")||m(this).replaceWith(this.childNodes)}).end()}}),m.expr.filters.hidden=function(a){return a.offsetWidth<=0&&a.offsetHeight<=0||!k.reliableHiddenOffsets()&&"none"===(a.style&&a.style.display||m.css(a,"display"))},m.expr.filters.visible=function(a){return!m.expr.filters.hidden(a)};var Qc=/%20/g,Rc=/\[\]$/,Sc=/\r?\n/g,Tc=/^(?:submit|button|image|reset|file)$/i,Uc=/^(?:input|select|textarea|keygen)/i;function Vc(a,b,c,d){var e;if(m.isArray(b))m.each(b,function(b,e){c||Rc.test(a)?d(a,e):Vc(a+"["+("object"==typeof e?b:"")+"]",e,c,d)});else if(c||"object"!==m.type(b))d(a,b);else for(e in b)Vc(a+"["+e+"]",b[e],c,d)}m.param=function(a,b){var c,d=[],e=function(a,b){b=m.isFunction(b)?b():null==b?"":b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)};if(void 0===b&&(b=m.ajaxSettings&&m.ajaxSettings.traditional),m.isArray(a)||a.jquery&&!m.isPlainObject(a))m.each(a,function(){e(this.name,this.value)});else for(c in a)Vc(c,a[c],b,e);return d.join("&").replace(Qc,"+")},m.fn.extend({serialize:function(){return m.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var a=m.prop(this,"elements");return a?m.makeArray(a):this}).filter(function(){var a=this.type;return this.name&&!m(this).is(":disabled")&&Uc.test(this.nodeName)&&!Tc.test(a)&&(this.checked||!W.test(a))}).map(function(a,b){var c=m(this).val();return null==c?null:m.isArray(c)?m.map(c,function(a){return{name:b.name,value:a.replace(Sc,"\r\n")}}):{name:b.name,value:c.replace(Sc,"\r\n")}}).get()}}),m.ajaxSettings.xhr=void 0!==a.ActiveXObject?function(){return!this.isLocal&&/^(get|post|head|put|delete|options)$/i.test(this.type)&&Zc()||$c()}:Zc;var Wc=0,Xc={},Yc=m.ajaxSettings.xhr();a.ActiveXObject&&m(a).on("unload",function(){for(var a in Xc)Xc[a](void 0,!0)}),k.cors=!!Yc&&"withCredentials"in Yc,Yc=k.ajax=!!Yc,Yc&&m.ajaxTransport(function(a){if(!a.crossDomain||k.cors){var b;return{send:function(c,d){var e,f=a.xhr(),g=++Wc;if(f.open(a.type,a.url,a.async,a.username,a.password),a.xhrFields)for(e in a.xhrFields)f[e]=a.xhrFields[e];a.mimeType&&f.overrideMimeType&&f.overrideMimeType(a.mimeType),a.crossDomain||c["X-Requested-With"]||(c["X-Requested-With"]="XMLHttpRequest");for(e in c)void 0!==c[e]&&f.setRequestHeader(e,c[e]+"");f.send(a.hasContent&&a.data||null),b=function(c,e){var h,i,j;if(b&&(e||4===f.readyState))if(delete Xc[g],b=void 0,f.onreadystatechange=m.noop,e)4!==f.readyState&&f.abort();else{j={},h=f.status,"string"==typeof f.responseText&&(j.text=f.responseText);try{i=f.statusText}catch(k){i=""}h||!a.isLocal||a.crossDomain?1223===h&&(h=204):h=j.text?200:404}j&&d(h,i,j,f.getAllResponseHeaders())},a.async?4===f.readyState?setTimeout(b):f.onreadystatechange=Xc[g]=b:b()},abort:function(){b&&b(void 0,!0)}}}});function Zc(){try{return new a.XMLHttpRequest}catch(b){}}function $c(){try{return new a.ActiveXObject("Microsoft.XMLHTTP")}catch(b){}}m.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/(?:java|ecma)script/},converters:{"text script":function(a){return m.globalEval(a),a}}}),m.ajaxPrefilter("script",function(a){void 0===a.cache&&(a.cache=!1),a.crossDomain&&(a.type="GET",a.global=!1)}),m.ajaxTransport("script",function(a){if(a.crossDomain){var b,c=y.head||m("head")[0]||y.documentElement;return{send:function(d,e){b=y.createElement("script"),b.async=!0,a.scriptCharset&&(b.charset=a.scriptCharset),b.src=a.url,b.onload=b.onreadystatechange=function(a,c){(c||!b.readyState||/loaded|complete/.test(b.readyState))&&(b.onload=b.onreadystatechange=null,b.parentNode&&b.parentNode.removeChild(b),b=null,c||e(200,"success"))},c.insertBefore(b,c.firstChild)},abort:function(){b&&b.onload(void 0,!0)}}}});var _c=[],ad=/(=)\?(?=&|$)|\?\?/;m.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var a=_c.pop()||m.expando+"_"+vc++;return this[a]=!0,a}}),m.ajaxPrefilter("json jsonp",function(b,c,d){var e,f,g,h=b.jsonp!==!1&&(ad.test(b.url)?"url":"string"==typeof b.data&&!(b.contentType||"").indexOf("application/x-www-form-urlencoded")&&ad.test(b.data)&&"data");return h||"jsonp"===b.dataTypes[0]?(e=b.jsonpCallback=m.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,h?b[h]=b[h].replace(ad,"$1"+e):b.jsonp!==!1&&(b.url+=(wc.test(b.url)?"&":"?")+b.jsonp+"="+e),b.converters["script json"]=function(){return g||m.error(e+" was not called"),g[0]},b.dataTypes[0]="json",f=a[e],a[e]=function(){g=arguments},d.always(function(){a[e]=f,b[e]&&(b.jsonpCallback=c.jsonpCallback,_c.push(e)),g&&m.isFunction(f)&&f(g[0]),g=f=void 0}),"script"):void 0}),m.parseHTML=function(a,b,c){if(!a||"string"!=typeof a)return null;"boolean"==typeof b&&(c=b,b=!1),b=b||y;var d=u.exec(a),e=!c&&[];return d?[b.createElement(d[1])]:(d=m.buildFragment([a],b,e),e&&e.length&&m(e).remove(),m.merge([],d.childNodes))};var bd=m.fn.load;m.fn.load=function(a,b,c){if("string"!=typeof a&&bd)return bd.apply(this,arguments);var d,e,f,g=this,h=a.indexOf(" ");return h>=0&&(d=m.trim(a.slice(h,a.length)),a=a.slice(0,h)),m.isFunction(b)?(c=b,b=void 0):b&&"object"==typeof b&&(f="POST"),g.length>0&&m.ajax({url:a,type:f,dataType:"html",data:b}).done(function(a){e=arguments,g.html(d?m("<div>").append(m.parseHTML(a)).find(d):a)}).complete(c&&function(a,b){g.each(c,e||[a.responseText,b,a])}),this},m.expr.filters.animated=function(a){return m.grep(m.timers,function(b){return a===b.elem}).length};var cd=a.document.documentElement;function dd(a){return m.isWindow(a)?a:9===a.nodeType?a.defaultView||a.parentWindow:!1}m.offset={setOffset:function(a,b,c){var d,e,f,g,h,i,j,k=m.css(a,"position"),l=m(a),n={};"static"===k&&(a.style.position="relative"),h=l.offset(),f=m.css(a,"top"),i=m.css(a,"left"),j=("absolute"===k||"fixed"===k)&&m.inArray("auto",[f,i])>-1,j?(d=l.position(),g=d.top,e=d.left):(g=parseFloat(f)||0,e=parseFloat(i)||0),m.isFunction(b)&&(b=b.call(a,c,h)),null!=b.top&&(n.top=b.top-h.top+g),null!=b.left&&(n.left=b.left-h.left+e),"using"in b?b.using.call(a,n):l.css(n)}},m.fn.extend({offset:function(a){if(arguments.length)return void 0===a?this:this.each(function(b){m.offset.setOffset(this,a,b)});var b,c,d={top:0,left:0},e=this[0],f=e&&e.ownerDocument;if(f)return b=f.documentElement,m.contains(b,e)?(typeof e.getBoundingClientRect!==K&&(d=e.getBoundingClientRect()),c=dd(f),{top:d.top+(c.pageYOffset||b.scrollTop)-(b.clientTop||0),left:d.left+(c.pageXOffset||b.scrollLeft)-(b.clientLeft||0)}):d},position:function(){if(this[0]){var a,b,c={top:0,left:0},d=this[0];return"fixed"===m.css(d,"position")?b=d.getBoundingClientRect():(a=this.offsetParent(),b=this.offset(),m.nodeName(a[0],"html")||(c=a.offset()),c.top+=m.css(a[0],"borderTopWidth",!0),c.left+=m.css(a[0],"borderLeftWidth",!0)),{top:b.top-c.top-m.css(d,"marginTop",!0),left:b.left-c.left-m.css(d,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var a=this.offsetParent||cd;while(a&&!m.nodeName(a,"html")&&"static"===m.css(a,"position"))a=a.offsetParent;return a||cd})}}),m.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(a,b){var c=/Y/.test(b);m.fn[a]=function(d){return V(this,function(a,d,e){var f=dd(a);return void 0===e?f?b in f?f[b]:f.document.documentElement[d]:a[d]:void(f?f.scrollTo(c?m(f).scrollLeft():e,c?e:m(f).scrollTop()):a[d]=e)},a,d,arguments.length,null)}}),m.each(["top","left"],function(a,b){m.cssHooks[b]=Lb(k.pixelPosition,function(a,c){return c?(c=Jb(a,b),Hb.test(c)?m(a).position()[b]+"px":c):void 0})}),m.each({Height:"height",Width:"width"},function(a,b){m.each({padding:"inner"+a,content:b,"":"outer"+a},function(c,d){m.fn[d]=function(d,e){var f=arguments.length&&(c||"boolean"!=typeof d),g=c||(d===!0||e===!0?"margin":"border");return V(this,function(b,c,d){var e;return m.isWindow(b)?b.document.documentElement["client"+a]:9===b.nodeType?(e=b.documentElement,Math.max(b.body["scroll"+a],e["scroll"+a],b.body["offset"+a],e["offset"+a],e["client"+a])):void 0===d?m.css(b,c,g):m.style(b,c,d,g)},b,f?d:void 0,f,null)}})}),m.fn.size=function(){return this.length},m.fn.andSelf=m.fn.addBack,"function"==typeof define&&define.amd&&define("jquery",[],function(){return m});var ed=a.jQuery,fd=a.$;return m.noConflict=function(b){return a.$===m&&(a.$=fd),b&&a.jQuery===m&&(a.jQuery=ed),m},typeof b===K&&(a.jQuery=a.$=m),m});

(function () {
    var lastTime = 0;
    var vendors = ['webkit', 'moz'];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame =
          window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function (callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function () { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function (id) {
            clearTimeout(id);
        };
}());
(function () {
    function decimalAdjust(type, value, exp) {
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;

        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }

        if (value < 0) {
            return -decimalAdjust(type, -value, exp);
        }

        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));

        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }

    if (!Math.round10) {
        Math.round10 = function (value, exp) {
            return decimalAdjust('round', value, exp);
        };
    }
    if (!Math.floor10) {
        Math.floor10 = function (value, exp) {
            return decimalAdjust('floor', value, exp);
        };
    }
    if (!Math.ceil10) {
        Math.ceil10 = function (value, exp) {
            return decimalAdjust('ceil', value, exp);
        };
    }
})();
(function (namespace) {
    namespace.storage = namespace.storage || {};
    var me = namespace.storage;

    var globalMemoryStore = {};

    me.InMemoryStorage = function InMemoryStorage(memorySegmentIdentifier) {
        memorySegmentIdentifier = memorySegmentIdentifier || '__default__';

        var instance = this;
        var instanceMemoryStore = {};

        function init() {
            if (!(instance instanceof InMemoryStorage)) {
                return new InMemoryStorage(memorySegmentIdentifier);
            }

            if (globalMemoryStore.hasOwnProperty(memorySegmentIdentifier)) {
                return globalMemoryStore[memorySegmentIdentifier];
            }

            globalMemoryStore[memorySegmentIdentifier] = instance;

            Object.defineProperty(instance, 'length', {
                enumerable: true,
                configurable: true,
                get: function () {
                    return Object.keys(instanceMemoryStore).length;
                }
            });

            return instance;
        }

        instance.key = function (keyIndex) {
            var instanceMemoryStoreKeysCollection = Object.keys(instanceMemoryStore);

            if (instanceMemoryStoreKeysCollection.hasOwnProperty(keyIndex)) {
                return instanceMemoryStoreKeysCollection[keyIndex];
            }

            return null;
        };

        instance.getItem = function (key) {
            if (!instanceMemoryStore.hasOwnProperty(key)) {
                return null;
            }

            return instanceMemoryStore[key];
        };

        instance.setItem = function (key, value) {
            instanceMemoryStore[key] = value;
        };

        instance.removeItem = function (key) {
            delete instanceMemoryStore[key];
        };

        instance.clear = function () {
            instanceMemoryStore = {};
        };

        return init();
    };
}(window.DBDN = window.DBDN || {}));
(function () {
    String.prototype.prependCSSSelector = function (toBePrependedToSelector) {
        return this.split(',').map(function (selectorItem) {
            return toBePrependedToSelector + ' ' + selectorItem;
        })
        .join(',');
    };

    String.prototype.combineOrCSSSelector = function (selectorToBeOrCombinedWith) {
        return this + ',' + selectorToBeOrCombinedWith;
    };
}());
(function () {
    var instance = this;
    this.orientation = {portrait: 'portrait', landscape: 'landscape', square: 'square'};

    function checkViewport(minWidth, maxWidth) {
        minWidth = minWidth || null;
        maxWidth = maxWidth || null;

        var changeDeviceWidth = false;
        var $viewportElement = $("meta[name='viewport']");

        if ($viewportElement.length > 0
            && !$viewportElement.attr('content').match(/width=device-width/i)) {
            changeDeviceWidth = true;
        }

        if (changeDeviceWidth) {
            var oldAttrValue = $viewportElement.attr('content');
            // Otherwise the matchMedia will return always true!
            $viewportElement.attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0');
        }

        var matchMediaResult = null;

        if (minWidth !== null && maxWidth !== null) {
            matchMediaResult = matchMedia('(min-width:' + minWidth + ')').matches
                        && matchMedia('(max-width:' + maxWidth + ')').matches;
        } else if (minWidth !== null) {
            matchMediaResult = matchMedia('(min-width:' + minWidth + ')').matches;
        } else if (maxWidth !== null) {
            matchMediaResult = matchMedia('(max-width:' + maxWidth + ')').matches;
        }

        if (changeDeviceWidth) {
            $viewportElement.attr('content', oldAttrValue);
        }

        return matchMediaResult;
    }

    this.isTabletViewport = function (ignoreRequestDesktopState) {
        ignoreRequestDesktopState = ignoreRequestDesktopState || false;

        if (DBDN.utils.isForcedDesktopMode() && !ignoreRequestDesktopState) {
            return false;
        }

        return checkViewport('768px', '1023px');
    };

    this.isMobileViewport = function (ignoreRequestDesktopState) {
        ignoreRequestDesktopState = ignoreRequestDesktopState || false;

        if (DBDN.utils.isForcedDesktopMode() && !ignoreRequestDesktopState) {
            return false;
        }

        return checkViewport(null, '767px');
    };

    this.isDesktopViewport = function (ignoreRequestDesktopState) {
        ignoreRequestDesktopState = ignoreRequestDesktopState || false;

        return !instance.isTabletViewport(ignoreRequestDesktopState) && !instance.isMobileViewport(ignoreRequestDesktopState);
    };

    this.getScreenOrientation = function () {
        if ($(window).width() > $(window).height()) {
            return this.orientation.landscape;
        }

        if ($(window).width() == $(window).height()) {
            return this.orientation.square;
        }

        return this.orientation.portrait;
    };

}());
(function (namespace) {
    namespace.events = namespace.events || {};
    var me = namespace.events;

    var trackedEvents = {};

    // BaseEvent
    function BaseEvent(eventName, instanceEventNamespace, eventTargetObject) {
        var ticking = false;

        var eventName = eventName || 'base';
        var eventRootNamespace = BaseEvent.eventRootNamespace;
        var instanceEventNamespace = instanceEventNamespace || new Date().getTime();
        var eventTargetObject = eventTargetObject instanceof HTMLElement || eventTargetObject instanceof jQuery ? eventTargetObject : window;

        function doWork(ev, callback) {
            if (typeof callback === 'function') {
                callback(ev);
            }

            ticking = false;
        }

        this.tick = function (ev, callback) {
            if (!ticking) {
                requestAnimationFrame(function () {
                    doWork(ev, callback);
                });
            }

            ticking = true;
        }

        this.getEventNameWithNamespace = function (includeInstanceNamespace) {
            includeInstanceNamespace = includeInstanceNamespace || true;

            if (includeInstanceNamespace) {
                return this.getEventName() + '.' + this.getEventRootNamespace() + '.' + this.getInstanceEventNamespace();
            } else {
                return this.getEventName() + '.' + this.getEventRootNamespace();
            }
        };

        this.getEventNameWithInstanceNamespace = function () {
            return this.getEventName() + '.' + this.getInstanceEventNamespace();
        };

        this.getEventName = function () {
            return eventName;
        };

        this.getInstanceEventNamespace = function () {
            return instanceEventNamespace;
        };

        this.getEventRootNamespace = function () {
            return eventRootNamespace;
        };

        this.stopEvent = function () {
            $(eventTargetObject).off(this.getEventNameWithNamespace());
        };

        this.trackEvent = function () {
            trackedEvents[this.getEventNameWithNamespace()] = this;
        };
    };
    BaseEvent.eventRootNamespace = 'DBDN_Events';
    BaseEvent.prototype.removeTrackedEvents = function (namespace){
        if (typeof namespace === 'undefined') {
            throw new Error('Please provide namespace value.');
        }

        for (property in trackedEvents) {
            if (trackedEvents.hasOwnProperty(property)
                && property.indexOf(namespace) !== -1) {
                trackedEvents[property].stopEvent();
            }
        }
    };

    // Resize
    me.WindowResizeEvent = function WindowResizeEvent(timeoutInMs, onResizeCallback, instanceEventNamespace) {
        BaseEvent.call(this, WindowResizeEvent.eventName, instanceEventNamespace);
        var instance = this;

        onResizeCallback = onResizeCallback || timeoutInMs;
        var debounceDurationInMs = typeof timeoutInMs === 'function' ? 200 : (timeoutInMs | 0) || 200;
        var debounceTimeoutHandle = null;

        if (typeof onResizeCallback !== 'function') {
            throw new Error('onResizeCallback not a function!');
        }

        this.startEvent = function () {
            $(window).on(this.getEventNameWithNamespace(), function (ev) {
                clearTimeout(debounceTimeoutHandle);

                debounceTimeoutHandle = setTimeout(function () {
                    instance.tick(ev, onResizeCallback);
                }, debounceDurationInMs);
            });
        };

        this.changeDebounceTimeout = function (newTimeoutInMs) {
            debounceDurationInMs = (newTimeoutInMs | 0) || debounceDurationInMs;
        };

        this.startEvent();
    };
    me.WindowResizeEvent.eventName = 'resize';
    extend(BaseEvent, me.WindowResizeEvent);
    me.WindowResizeEvent.prototype.clearAllEvents = function (namespace) {
        namespace = namespace ? '.' + namespace : '';

        $(window).off(me.WindowResizeEvent.eventName + '.' + BaseEvent.eventRootNamespace + namespace);
    };

    // Orientation change
    me.OrientationChangeEvent = function OrientationChangeEvent(onOrientationChangeCallback, instanceEventNamespace) {
        BaseEvent.call(this, OrientationChangeEvent.eventName, instanceEventNamespace);
        var instance = this;

        var orientation = { portrait: 'portrait', landscape: 'landscape', square: 'square' }
        var previousOrientation = null;
        var resizeInstance = null;

        function init() {
            previousOrientation = getScreenOrientation();

            instance.startEvent();
        }

        function getScreenOrientation() {
            if ($(window).width() > $(window).height()) {
                return orientation.landscape;
            }

            if ($(window).width() == $(window).height()) {
                return orientation.square;
            }

            return orientation.portrait;
        }

        function baseEventCallback() {
            var currentOrientation = getScreenOrientation();

            if (currentOrientation !== previousOrientation
                && typeof onOrientationChangeCallback === 'function') {
                previousOrientation = currentOrientation;

                var event = $.Event(instance.getEventName());
                event.orientation = currentOrientation;

                onOrientationChangeCallback(event);
            }
        }

        this.startEvent = function () {
            if (!DBDN.utils.isCSSPropertySupported('-webkit-overflow-scrolling')) {
                resizeInstance = new me.WindowResizeEvent(100, function () {
                    baseEventCallback();
                }, instance.getEventNameWithInstanceNamespace());

            } else {
                // Needed because of the Mobile Safary buggy behaviour!
                $(window).on('orientationchange', function () {
                    baseEventCallback();
                });
            }
        };

        this.stopEvent = function () {
            resizeInstance.stopEvent();
        };

        init();
    };
    me.OrientationChangeEvent.eventName = 'orientationChange';
    extend(BaseEvent, me.OrientationChangeEvent);
    me.OrientationChangeEvent.prototype.clearAllEvents = function () {
        me.WindowResizeEvent.prototype.clearAllEvents.call(null, me.OrientationChangeEvent.eventName);
    };

    // Scroll
    me.ScrollEvent = function ScrollEvent(element, timeoutInMs, onScrollCallback, instanceEventNamespace) {
        BaseEvent.call(this, ScrollEvent.eventName, instanceEventNamespace, element);
        var instance = this;

        onScrollCallback = onScrollCallback || timeoutInMs || element;
        var targetElement = element instanceof HTMLElement || element instanceof jQuery ? element : window;
        var debounceDurationInMs = typeof timeoutInMs === 'number' ? timeoutInMs : 200;
        var debounceTimeoutHandle = null;

        if (typeof element === 'number') {
            debounceDurationInMs = element;
        }

        if (typeof onScrollCallback !== 'function') {
            throw new Error('onScrollCallback not a function!');
        }

        this.startEvent = function () {
            $(targetElement).on(this.getEventNameWithNamespace(), function (ev) {
                clearTimeout(debounceTimeoutHandle);

                debounceTimeoutHandle = setTimeout(function () {
                    instance.tick(ev, onScrollCallback);
                }, debounceDurationInMs);
            });
        };

        this.changeDebounceTimeout = function (newTimeoutInMs) {
            debounceDurationInMs = typeof newTimeoutInMs === 'number' ? newTimeoutInMs : debounceDurationInMs;
        };

        this.startEvent();
        this.trackEvent();
    };
    me.ScrollEvent.eventName = 'scroll';
    extend(BaseEvent, me.ScrollEvent);
    me.ScrollEvent.prototype.clearAllEvents = function (namespace) {
        namespace = namespace ? '.' + namespace : '';

        BaseEvent.prototype.removeTrackedEvents.call(null, me.ScrollEvent.eventName + '.' + BaseEvent.eventRootNamespace + namespace);
    };

    // Common
    function extend(parentObject, childObject) {
        childObject.prototype = new parentObject();
        childObject.prototype.constructor = childObject;
    }
}(window.DBDN = window.DBDN || {}));
(function () {
    $(document).on('tabletDesktopViewportChange.DBDN_General tabletTabletViewportChange.DBDN_General desktopDesktopViewportChange.DBDN_General', function (ev) {
        DBDN.scaleWebsite.scale();
    });
}());
(function (namespace) {
    namespace.utils = namespace.utils || {};
    var me = namespace.utils;

    me.isScaledViewport = function () {
        return DBDN.scaleWebsite.isViewportScaled();
    };

    me.isDesktopScaledViewport = function () {
        return DBDN.scaleWebsite.isViewportScaled() && isDesktopViewport();
    };

    me.isCSSPropertySupported = function (propertyName) {
        if (propertyName.startsWith('-')) {
            propertyName = propertyName.substr(1);
        }

        var propertyNameTokens = propertyName.split('-');

        if (propertyNameTokens.length > 1) {
            Array.prototype.forEach.call(propertyNameTokens, function (item, index, collection) {
                if (index > 0) {
                    collection[index] = item.substr(0, 1).toUpperCase() + item.substr(1);
                }
            });

            propertyName = propertyNameTokens.join('');
        }

        var inMemoryElement = document.createElement('darthvader');

        if (typeof inMemoryElement.style[propertyName] !== 'undefined') {
            return true;
        }

        return false;
    };

    me.poll = function poll(conditionFn, callback, stopPollingConditionFn, interval, timeout) {
        interval = interval || 50;
        timeout = new Date().getTime() + (timeout || 2000);

        var pollingStatus = false;
        var rafIdentifier = null;

        (function pollStatus() {
            if (stopPollingConditionFn !== null && stopPollingConditionFn()) {
                cancelAnimationFrame(rafIdentifier);

                return;
            }

            if (pollingStatus) {
                return;
            }

            if (conditionFn()) {
                pollingStatus = true;

                rafIdentifier = requestAnimationFrame(doWork);
            } else if (new Date().getTime() < timeout) {
                setTimeout(function () {
                    pollStatus();
                }, interval)
            }
        }());

        function doWork() {
            callback();

            pollingStatus = false;
        }
    };

    me.getSessionLanguage = function () {
        var fallbackLang = 'de';

        if (typeof lang_label !== 'undefined') {
            return lang_label;
        }

        var lang = document.querySelector('html').getAttribute('lang');
        if (lang) {
            return lang;
        }

        return fallbackLang;
    };

    me.getBoundingClientRectOfHiddenElement = function ($element) {
        if (typeof document.body['getBoundingClientRect'] === 'undefined') {
            return false;
        }

        var isElementVisible = $element.is(':visible');

        if (!isElementVisible) {
            $element.css('visibility', 'hidden');
            $element.show();
        }

        var boundingRectangle = $element[0].getBoundingClientRect();

        if (!isElementVisible) {
            $element.hide();
            $element.css('visibility', '');
        }

        return boundingRectangle;
    };

    me.toggleTabletDesktopEvent = function (desktopCallback, tabletCallback, htmlViewportMode) {
        if (htmlViewportMode.previous === DBDN.enum.HtmlViewportMode.TABLET
            && htmlViewportMode.active === DBDN.enum.HtmlViewportMode.DESKTOP) {
            if (typeof desktopCallback === 'function') {
                desktopCallback();
            }
        }

        if (htmlViewportMode.previous === DBDN.enum.HtmlViewportMode.DESKTOP
            && htmlViewportMode.active === DBDN.enum.HtmlViewportMode.TABLET) {
            if (typeof tabletCallback === 'function') {
                tabletCallback();
            }
        }
    };

    me.toggleTabletDesktopHtml = function ($container,
        $desktopHtml,
        $tabletHtml,
        htmlViewportMode,
        insertCallback) {
        if (htmlViewportMode.previous === DBDN.enum.HtmlViewportMode.TABLET
            && htmlViewportMode.active === DBDN.enum.HtmlViewportMode.DESKTOP) {
            $container.hide();

            if (typeof insertCallback !== 'function') {
                $container
                    .append($desktopHtml);
            } else {
                /* toBeInserted, toBeDeleted */
                insertCallback($desktopHtml, $tabletHtml);
            }

            $tabletHtml
                .not($tabletHtml)
                .add($tabletHtml.detach());

            $container.show();
        }

        if (htmlViewportMode.previous === DBDN.enum.HtmlViewportMode.DESKTOP
            && htmlViewportMode.active === DBDN.enum.HtmlViewportMode.TABLET) {
            $container.hide();

            if (typeof insertCallback !== 'function') {
                $container
                    .append($tabletHtml);
            } else {
                insertCallback($tabletHtml, $desktopHtml);
            }

            $desktopHtml
                .not($desktopHtml)
                .add($desktopHtml.detach());

            $container.show();
        }
    };

    me.getActiveHtmlViewportMode = function () {
        if (isTabletViewport()) {
            return DBDN.enum.HtmlViewportMode.TABLET
        }

        if (isMobileViewport()) {
            return DBDN.enum.HtmlViewportMode.MOBILE
        }

        if (!isTabletViewport() && !isMobileViewport()) {
            return DBDN.enum.HtmlViewportMode.DESKTOP
        }
    };

    me.createXMLHTTPObject = function() {
        var XMLHttpFactories = [
            function () {return new XMLHttpRequest();},
            function () {return new ActiveXObject("Msxml2.XMLHTTP");},
            function () {return new ActiveXObject("Msxml3.XMLHTTP");},
            function () {return new ActiveXObject("Microsoft.XMLHTTP");}
        ];
        var xmlhttp = false;
        for (var i=0;i<XMLHttpFactories.length;i++) {
            try {
                xmlhttp = XMLHttpFactories[i]();
            }
            catch (e) {
                continue;
            }
            break;
        }
        return xmlhttp;
    };

    me.downloadFileAndDisplayNotificationWhileBeingDownloaded = function (element, notificationMsg, isItForm){
        isItForm = isItForm || false;

        $("#alertsContents .alert_text").html(notificationMsg);
        $("#alertsBG").unbind('click');
        $("#alertsBG").show();
        $('#alerts .close-button, #alerts .ok_button').hide();
        $("#alerts").addClass('center');
        $('#alerts').show();

        var url = isItForm ? $(element).prop('action') : $(element).attr('href');
        var xhr = DBDN.utils.createXMLHTTPObject();
        var requestMethod = isItForm ? $(element).prop('method') : 'GET';

        xhr.open(requestMethod, url, true);
        xhr.responseType = 'blob';
        xhr.onload = function (e) {
            if (this.status >= 200 && this.status < 300) {
                var filename = "";
                var disposition = this.getResponseHeader('Content-Disposition');
                if (disposition && disposition.indexOf('attachment') !== -1) {
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1])
                        filename = matches[1].replace(/['"]/g, '');
                }

                var type = xhr.getResponseHeader('Content-Type');
                var blob = e.target.response;

                if (typeof window.navigator.msSaveBlob !== 'undefined') {
                    // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                    window.navigator.msSaveBlob(blob, filename);
                } else {
                    var URL = window.URL || window.webkitURL;
                    var downloadUrl = URL.createObjectURL(blob);

                    if (filename) {
                        // use HTML5 a[download] attribute to specify filename
                        var a = document.createElement("a");
                        // safari doesn't support this yet
                        if (typeof a.download === 'undefined') {
                            window.location = downloadUrl;
                        } else {
                            a.href = downloadUrl;
                            a.download = filename;
                            document.body.appendChild(a);
                            a.click();
                        }
                    } else {
                        window.location = downloadUrl;
                    }

                    setTimeout(function () {
                        URL.revokeObjectURL(downloadUrl);

                        if (typeof a !== 'undefined') {
                            try {
                                document.body.removeChild(a);
                            } catch (e) {

                            }
                        }
                    }, 100); // cleanup
                }
                $("#alertsBG").on("click", function(){
                    $("#alerts, #alertsBG").fadeOut(300,function(){$("#alerts").removeClass('red');$("#alerts").removeClass('center');});
                });
                $("#alertsBG").hide();
                $('#alerts').hide();
                $("#alerts").removeClass('center');
                $('#alerts .close-button, #alerts .ok_button').show();
            } else {
                $("#alerts").addClass('red');
                $('#alerts .close-button, #alerts .ok_button').show();
                $('#alerts .alert_text').html(this.statusText);
            }
        };

        if (isItForm) {
            xhr.send(new FormData($(element)[0]));
        } else {
            xhr.send();
        }
};

    me.validateProfileForm = function (selector, notRequaredFields){
        notRequaredFields = notRequaredFields ? notRequaredFields : [];
        if($(selector + ' #iagree').length > 0){
            var submit_button = $(selector + ' .more_info .submit_button');
            if(!$(selector + ' #iagree').is(":checked")){
                submit_button.attr('disabled', 'disabled');
            }
            $(selector + ' #iagree').click(function(){
                if($(this).is(":checked")){
                    submit_button.removeAttr("disabled");
                }else{
                   submit_button.attr('disabled', 'disabled');
                }
            });
        }
        $(selector).submit(function(){
            var requaredFields = $('span.important').closest('.input').find('.input__field');
            var error = false;
            var errorMsg = '';
            requaredFields.each(function(index, element){
                if($(element).val() === ''){
                    var fieldName = $(element).attr('name');
                    var notRequared = notRequaredFields.find(
                                function(str) {
                                    return str == fieldName;
                                }
                            );
                    if(!notRequared){
                        fieldName = js_localize['field.'+fieldName];
                        errorMsg += js_localize['field.required'].replace('{0}', fieldName);
                        errorMsg += '<br>';
                        $(element).addClass('error');
                        error = true;
                    }

                }else{
                    $(element).removeClass('error');
                }
            });
            if(error){
                msg(errorMsg,  true);
                return false;
            }
        });
    };

}(window.DBDN = window.DBDN || {}));
(function (namespace) {
    namespace.enum = namespace.enum || {};
    var me = namespace.enum;

    me.HtmlViewportMode = Object.freeze({
        TABLET: 'tablet',
        MOBILE: 'mobile',
        DESKTOP: 'desktop',
        SCALED: 'scaled-viewport'
    });
}(window.DBDN = window.DBDN || {}));
// Raise special event after orientation change occurs
// if the viewport width falls within certain predefined ranges.
(function () {
    var previousHtmlViewportMode = DBDN.utils.getActiveHtmlViewportMode();
    var wasPreviousHtmlViewportModeScaled = DBDN.utils.isScaledViewport();

    new DBDN.events.OrientationChangeEvent(function () {
        var event = null;

        if (isDesktopViewport()
            && previousHtmlViewportMode === DBDN.enum.HtmlViewportMode.TABLET) {
            event = getTabletDesktopViewportChangeEvent();
        }

        if (isTabletViewport()
            && previousHtmlViewportMode === DBDN.enum.HtmlViewportMode.DESKTOP) {
            event = getTabletDesktopViewportChangeEvent();
        }

        if (isTabletViewport()
            && previousHtmlViewportMode === DBDN.enum.HtmlViewportMode.TABLET) {
            event = getTabletTabletViewportChangeEvent();
        }

        if (isMobileViewport()
            && previousHtmlViewportMode === DBDN.enum.HtmlViewportMode.MOBILE) {
            event = getMobileMobileViewportChangeEvent();
        }

        if (isDesktopViewport()
            && previousHtmlViewportMode === DBDN.enum.HtmlViewportMode.DESKTOP) {
            event = getDesktopDesktopViewportChangeEvent();
        }

        if (event !== null) {
            event.htmlViewportMode.previous = previousHtmlViewportMode;
            event.htmlViewportMode.active = DBDN.utils.getActiveHtmlViewportMode();
            event.htmlViewportMode.wasPreviousScaled = wasPreviousHtmlViewportModeScaled;
            event.htmlViewportMode.isActiveScaled = DBDN.utils.isScaledViewport();
        }

        updatePreviousHtmlViewportMode();

        raiseEvent(event);
    });

    function raiseEvent(event) {
        if (event === null) {
            return;
        }

        $(document).trigger(event);
    }

    function updatePreviousHtmlViewportMode() {
        previousHtmlViewportMode = DBDN.utils.getActiveHtmlViewportMode();
    }

    function getEvent(eventName) {
        var event = $.Event(eventName);
        event.htmlViewportMode = {};

        return event;
    }

    function getTabletDesktopViewportChangeEvent() {
        return getEvent('tabletDesktopViewportChange.DBDN_General');
    }

    function getTabletTabletViewportChangeEvent() {
        return getEvent('tabletTabletViewportChange.DBDN_General');
    }

    function getMobileMobileViewportChangeEvent() {
        return getEvent('mobileMobileViewportChange.DBDN_General');
    }

    function getDesktopDesktopViewportChangeEvent() {
        return getEvent('desktopDesktopViewportChange.DBDN_General');
    }
}());
(function () {
    jQuery.fn.getUUID = function () {
        var uuidKey = 'selfUuid';

        return this.data(uuidKey) || null;
    };

    jQuery.fn.generateUUID = function () {
        var uuidKey = 'selfUuid';

        if (this.getUUID() !== null) {
            return this;
        }

        var timestamp = new Date().getTime();

        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (char) {
            var randomNumber = (timestamp + Math.random() * 16) % 16 | 0;
            timestamp = Math.floor(timestamp / 16);

            return (char == 'x' ? randomNumber : (randomNumber & 0x3 | 0x8)).toString(16);
        });

        this.data(uuidKey, uuid);

        return this;
    };

    jQuery.fn.hideForDesktop = function (hide) {
        var className = 'desktop-hidden';

        if (typeof hide !== 'undefined' && !hide) {
            return this.removeClass(className);
        }

        return this.addClass(className);
    };

    jQuery.fn.hideForTablet = function (hide) {
        var className = 'tablet-hidden';

        if (typeof hide !== 'undefined' && !hide) {
            return this.removeClass(className);
        }

        return this.addClass(className);
    };

    jQuery.fn.hideBeforeTransition = function () {
        var className = 'transition-hide';

        return this.addClass(className);
    };

    jQuery.fn.showAfterTransition = function () {
        var className = 'transition-hide';

        return this.removeClass(className);
    };

    jQuery.fn.visibleToTabletOnly = function (conditionFn, isTransitionEnd) {
        var className = 'tablet-only';
        isTransitionEnd = isTransitionEnd || true;

        if (typeof htmlViewportMode === 'undefined') {
            this.addClass(className);
        }

        if (typeof conditionFn === 'function'
            && conditionFn()) {
            this.addClass(className);
        }

        return this.showAfterTransition();
    };
}());
/*\
|*|  :: cookies.js ::
|*|
|*|  A complete cookies reader/writer framework with full unicode support.
|*|
|*|  Revision #1 - September 4, 2014
|*|
|*|  https://developer.mozilla.org/en-US/docs/Web/API/document.cookie
|*|  https://developer.mozilla.org/User:fusionchess
|*|
|*|  This framework is released under the GNU Public License, version 3 or later.
|*|  http://www.gnu.org/licenses/gpl-3.0-standalone.html
|*|
|*|  Syntaxes:
|*|
|*|  * docCookies.setItem(name, value[, end[, path[, domain[, secure]]]])
|*|  * docCookies.getItem(name)
|*|  * docCookies.removeItem(name[, path[, domain]])
|*|  * docCookies.hasItem(name)
|*|  * docCookies.keys()
\*/
var docCookies = {
  getItem: function (sKey) {
    if (!sKey) { return null; }
    return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
  },
  setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
    if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
    var sExpires = "";
    if (vEnd) {
      switch (vEnd.constructor) {
        case Number:
          sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
          break;
        case String:
          sExpires = "; expires=" + vEnd;
          break;
        case Date:
          sExpires = "; expires=" + vEnd.toUTCString();
          break;
      }
    }
    document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
    return true;
  },
  removeItem: function (sKey, sPath, sDomain) {
    if (!this.hasItem(sKey)) { return false; }
    document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
    return true;
  },
  hasItem: function (sKey) {
    if (!sKey) { return false; }
    return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
  },
  keys: function () {
    var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
    for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
    return aKeys;
  }
};
/* END COOKIES */

/* Fix for litebox; issue #10853 */
if ("undefined" !== typeof KeyboardEvent && false === KeyboardEvent.hasOwnProperty('DOM_VK_ESCAPE')) {
    KeyboardEvent.prototype.DOM_VK_ESCAPE = 27;
}

/*******************************************************************************
 * JSON Error Handling
 *******************************************************************************/
/**
 * @returns {JSON}
 */
var JSON_ERROR = {
};

JSON_ERROR.errorResponse2JSON = function(XMLHttpRequest) {
	var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
	var data = JSON.parse(fixedResponse);
	return data;
};

JSON_ERROR.composeMessageFromJSON = function(jsonData, processRespnce) {
	var message = jsonData.message;
	if (processRespnce && jsonData.response !== undefined) {
		for (var key in jsonData.response) {
			if (key === 'length' || !jsonData.response.hasOwnProperty(key)) continue;
			var value = jsonData.response[key];
			if (value instanceof Array) {
				message+= ' ' + value.join('; ');
			} else {
				message+= ' ' + value;
			}
		}
	}
	return message;
};

var imageProcess = {
    imageData : [],
    imagePostData : [],
    galleryData:[],
    galleryPostData:[],
    isImageProccessStart : false,
    isGalleryProccessStart : false,
    isImageProcessingComleted: false,
    start:function(){
        this.isImageProccessStart = false;
        this.isGalleryProccessStart = false;
        this.prepareImageData();
        this.prepareGalleryData();
    },
    prepareImageData :function(){
        $('img.explainer').each(function(index){
            var me = $(this);
            var wrapper = me.closest('.image_wrapper');
            imageProcess.imageData.push({'wrapper': wrapper, 'me': $(this)});
            imageProcess.imagePostData.push(me.attr('data-gallery').split(','));
        });
        this.getData();

        function updateValuesOnDemand() {
            $('img.zoom, img.inline_zoom, img.simple_no_zoom').each(function (index) {
                var me = $(this);
                var wrapper = me.closest('.image_wrapper');
                imageProcess.generateImage(wrapper, me);
            });
        }
        updateValuesOnDemand();

        $(document).on('tabletDesktopViewportChange.DBDN_General tabletTabletViewportChange.DBDN_General', function () {
            updateValuesOnDemand();
        });
        if (!DBDN.utils.isForcedDesktopMode() && isDesktopViewport() && !TABLET) {
            new DBDN.events.WindowResizeEvent(50, function () {
                updateValuesOnDemand();
            });
        }
    },
    prepareGalleryData:function(){
        $('img.gallery').each(function () {
            var me = $(this);
            // More distance craziness!
            me.parent().addClass('single-view-carousel');
            me.wrap('<div class="gallery-main-wrapper"><div class="galCropper"></div></div>');
            if(me.hasClass('first_image')){
                me.parent().addClass('first_image');
            }
            me.parent().append('<div class="galMover" style="display:none;"></div>');
            var wrapper = me.siblings('.galMover');
            imageProcess.galleryData.push({'wrapper': wrapper, 'me': $(this)});
            imageProcess.galleryPostData.push(me.attr('data-gallery').split(','));
        });
        this.getData(true);
    },
    getData: function(isGallery){
        isGallery = typeof isGallery === 'undefined' ? false: true;
        var postData = isGallery === false ? this.imagePostData : this.galleryPostData;
        var galleryCroppersCollection = [];
        if (postData.length > 0) {
            if(isGallery){
                this.isGalleryProccessStart = true;
            }else{
                this.isImageProccessStart = true;
            }
            var data = {
                'files': postData,
                '_token': $('form#searchForm2 input[name=_token]').val()
            };
            $.ajax({
                    type : 'POST',
                    url : paths.gallery,
                    data : data,
                    success : function(json){
                        var data = json.response;
                        for (var i = 0; i < data.length; i++) {
                            if(isGallery){
                                var element = imageProcess.galleryData[i];
                                element.me.attr({'unselectable': 'on',
                                    'border': '0'});
                                var hasClassLazyloadThumb = element.me.hasClass('lazyload-thumb') ? true : false;
                                element.me.removeClass();
                                if (hasClassLazyloadThumb) {
                                    element.me.addClass('lazyload-thumb');
                                }
                                imageProcess.generateGallery(element.wrapper, data[i], element.me, i);
                                element.wrapper.siblings().hide();
                                element.wrapper.show();
                                var $closestGalCropper = element.wrapper.closest('.galCropper');
                                galleryCroppersCollection.push($closestGalCropper);
                                imageProcess.setupCroppers($closestGalCropper);
                                imageProcess.isGalleryProccessStart = false;
                            }else{
                                var element = imageProcess.imageData[i];
                                imageProcess.generateExplainer(element.wrapper, data[i], element.me);
                                imageProcess.isImageProccessStart = false;
                            }
                        }
                        if (isGallery) {
                            $(document).on('tabletDesktopViewportChange.DBDN_General tabletTabletViewportChange.DBDN_General mobileMobileViewportChange.DBDN_General', function () {
                                $.each(galleryCroppersCollection, function () {
                                    imageProcess.setupCroppers(this, false);
                                });
                            });

                            $(document).trigger('singleImageGalleryCarouselLoaded.DBDN_General');
                        }
                        if(!imageProcess.isImageProccessStart && !imageProcess.isGalleryProccessStart){
                            $(document).trigger('imagesLoaded');
                            imageProcess.isImageProcessingComleted = true;
                        }
                    }
            });
        }else if(isGallery && !imageProcess.isImageProccessStart && !imageProcess.isGalleryProccessStart){
            $(document).trigger('imagesLoaded');
            imageProcess.isImageProcessingComleted = true;
        }
    },
    generateExplainer: function (container, data, img) {
        var imgAtag = img.closest('a');
        var explainerTitle = imgAtag.attr('id');
        explainerTitle = explainerTitle.substring(0,explainerTitle.lastIndexOf("_"));
        var newHTML = '';
        var images = data.images;
        var lowresSrc, lowresSrc2x, image;
        var files_str = '';
        $.each(img.attr('data-gallery').split(','), function (index, value) {
            files_str += '&files[]=' + value;
        });
        img.removeAttr('data-gallery');
        for (var z = 0; z < images.length; z++) {
            image = images[z];
            lowresSrc = image['lowres'];
            //lowresSrc2x = image['lowres2x'];
            if (!lowresSrc) {
                lowresSrc = image['path'];
                //lowresSrc2x = image['path'];
            }
            if (z == 0) {
                imgAtag.attr('href', relPath + 'explainer?_token=' + $('form#searchForm2 input[name=_token]').val() + files_str);
                imgAtag.attr('data-info', image['descr'].replace(/"/g, '&quot;'));
            }else{
                newHTML = '<div class="image_container" style="position:absolute;' +
                    '"><a class="explainer_layover" rev="scrolling:no;" rel="lyteframe" href="' + relPath + 'explainer?_token=' + $('form#searchForm2 input[name=_token]').val() + files_str + '" id="' + explainerTitle + '_' + z + '" unselectable="on" data-info="' + image['descr'].replace(/"/g, '&quot;') + '">\
                    </a></div>';
                container.find('.image_text').before(newHTML);
                var el = $(container).find('.explainer_gallery .explainer_layover:last');
                //el.append('<img src="' + lowresSrc + '" srcset="' + lowresSrc + ', ' + lowresSrc2x + ' 2x" border="0" unselectable="on" />');
                el.append('<img src="' + lowresSrc + '" border="0" unselectable="on" />');
            }
        }
        initLytebox();
    },
    _positioningImageText:function($container, img) {
        var parent = $container.find('.image_item');
        var text = parent.find('.image_text');
        var left = parent.find('a').outerWidth();
        if ($('body').hasClass('smart')) {
            if (left !== null) {
                text.css('width', left + 'px');
            }
        } else {
            if (isTabletViewport()) {
                text.css('width', ($container.outerWidth() - img.outerWidth()) + 'px');
            } else {
                text.css('width', '');
            }
            if (left !== null) {
                if (parent.hasClass("right_image")) {
                    left = parent.outerWidth() - (text.outerWidth() + left);
                }
                text.css('left', left + 'px');
            } else {
                console.debug('Wrong left: ' + left);
            }
        }
    },
    generateImage:function(container, img) {
        var $container = $(container);
        var title = $container.find('a').attr('data-title');
        if(title && title.length > 0){
            var cleartitle = title.replace(/<\s*br\s*\/?\s*>|<\s*\/p\s*>/gi, "\n").replace(/&nbsp;/gi, ' ').replace(/<\s*h1\s*>.*<\s*\/\s*h1\s*>|<[^>]+>/gi, '');
            cleartitle = $('<textarea />').html(cleartitle).text();
            $container.find('a').attr('title', cleartitle);
        }
        if (img.height() > img.width() && !isTabletViewport()) {
            $container.css('width', '740px');
        } else {
            $container.css('width', '');
        }
        imageProcess._positioningImageText($container, img);

        initLytebox();
    },
    generateGallery:function(container, data, img, index) {
        img.removeAttr('data-gallery');
        var arrowTop = Math.ceil(img.height()/2) + 7;
        var counterTop = MOBILE ? img.height() + 10 : img.height() - 94;
        var imgWidth = $(window).width() - 40;
        var now = new Date();
        var galleryTitle = 'lytebox[gallery_'+now.getTime()+'_'+ index +']';
        var newHTML = '';
        var images = data.images;
        var lowresSrc, lowresSrc2x, image;
        var imgToAppend = img;
        /*
            Quick and rudimentary fix

            On devices with DPR > 1x the img argument remains a reference to
            the default (1x) image, and as a consequence, the newly loaded higher
            quality image (Nx) overlays the gallery component and interferes with
            its navigation.

            The fix is the 8 lines below this comment/note.
        */
        var imgToCompareWith = container.prev('img');
        if (imgToAppend[0] != imgToCompareWith[0]) {
            imgToAppend = imgToCompareWith.removeClass();

            // If the 1x image wasn't removed by the setResolutionPictures method for any
            // reason, try to remove it again!
            img.remove();
        }
        //container.html(newHTML);
        for(var z=0; z < images.length; z++){
            image=images[z];
            lowresSrc = image['lowres'];
            lowresSrc2x = image['lowres2x'];
            if (!lowresSrc) {
                lowresSrc = image['path'];
                lowresSrc2x = lowresSrc;
            }

                var info = '<div>'+image['descr']+'</div>';
                var img_header = info;
                var descr = image['descr'].replace(/"/g, '&quot;');
                newHTML='<div class="galItem '+(z == 0 ? 'shown':"")+'" unselectable="on">\
                    <div class="image_container">\
                     <a class="gallery_layover" href="'+image['path']+'" rel="'+galleryTitle+'" data-title="'+ descr +'" unselectable="on" >\
                    </a></div>'+
                    ((image['descr'] != '' && typeof(image['descr']) != 'undefined') ?'<div class="imgText961">'+ img_header+'</div>':"");

                newHTML+='</div>';
                container.append(newHTML);
                var el = $(container).find('.galItem:last a.gallery_layover');
                if(image['descr'].length > 0){
                    var title = image['descr'].replace(/<\s*br\s*\/?\s*>|<\s*\/p\s*>/gi, "\n").replace(/&nbsp;/gi, ' ').replace(/<\s*h1\s*>.*<\s*\/\s*h1\s*>|<[^>]+>/gi, '');
                    title = $('<textarea />').html(title).text();
                    $(el).attr('title', title);
                }

                if (z > 0) {
                    //el.append('<img src="' + lowresSrc + '" srcset="' + lowresSrc + ', ' + lowresSrc2x + ' 2x" border="0" unselectable="on" />');
                    imgToAppend = $('<img src="' + lowresSrc + '" border="0" unselectable="on" class="ready-for-enhanced-resolution" data-check="04" />');
                }
                if(isMobileViewport()){
                   var ratio = parseInt(imgToAppend.attr('width')) / parseInt(imgToAppend.attr('height'));
                   var height = (imgWidth / ratio).toFixed(2);
                    imgToAppend.css({"width":imgWidth + "px", 'height': height + 'px'});
                }
                el.append(imgToAppend);
        }
        if (images.length > 1) {
            newHTML = '<div class="carouselNav">\
                            <a href="javascript:;" class="carouselLeft" style="top:'+ arrowTop +'px"></a>\
                            <div class="carouselCounter" style="top:'+ counterTop +'px"><span>1</span> / ' + images.length + '</div>\
                            <a href="javascript:;" class="carouselRight" style="top:'+ arrowTop +'px"></a>\
                        </div>';
        }
        container.closest('.galCropper').after(newHTML);
        container.closest('.single-view-carousel').css({ 'position': 'relative', 'line-height': '0px' });
       initLytebox();
    },
    setupCroppers: function ($croppers, bindEvents) {
        if (typeof bindEvents === 'undefined') {
            bindEvents = true;
        }
        var maxWidth = 0;
        var $me = $croppers.find('.galItem:first a img').first();
        var $dad = $me.closest('.image_container');
        var $grand = $dad.closest('.galCropper');
        var $firstImgDescr = $croppers.find('.galItem:first .imgText961');
        var $carouselNav = $croppers.next('.carouselNav');
        var $carouselCounter = $carouselNav.find('.carouselCounter')

        var counterMobileTop = $me.height() + 25;
        if (MOBILE && $firstImgDescr.outerHeight(true) > 50) {
            var counterMobileTop = $me.height() + 40;
        }
        var counterTop = MOBILE ? counterMobileTop : $me.height() - 94;
        $carouselCounter.css('top', counterTop + 'px');

        if ($dad.index() == 0) {
            var cropperHeight = $dad.closest('.galItem').outerHeight(true);
            if (MOBILE && cropperHeight < counterTop + $carouselCounter.outerHeight(true)) {
                cropperHeight = counterTop + $carouselCounter.outerHeight(true);
            }
            $grand.height(cropperHeight);
            $grand.width($dad.closest('.galItem').outerWidth(true));
            //$grand
            //  .width($me.width())
            //  .height($dad.outerHeight(true));
        }
        //$me.parent().width($me.width());
        //$dad.width($me.width());

        var $mover = $dad.closest('.galMover');
        var curW = 0;
        $mover.find('.galItem > .image_container').each(function () {
            //$(this).css('left', curW+'px');
            //curW += $(this).outerWidth();
            curW += Math.round10($(this).get(0).getBoundingClientRect().width, -2);
        });

        var moverLeftPosition = 0;

        if ($mover.length > 0) {
            moverLeftPosition = $mover.position().left;
        }

        var previousItemWidth = $mover.data('previousWidth');
        var moverLeftPositionFactor = 1;

        if (typeof previousItemWidth !== 'undefined') {
            moverLeftPositionFactor = $dad.width() / (previousItemWidth | 0);
        }

        $mover.width(Math.ceil10(curW, 0));

        if (Math.abs(moverLeftPosition) > 0) {
            $mover.css('left', moverLeftPosition * moverLeftPositionFactor + 'px');
        }

        $mover.data('previousWidth', $dad.width());

        var $carouselRight = $carouselNav.find('.carouselRight');
        var $carouselLeft = $carouselNav.find('.carouselLeft');
        if (bindEvents) {
            $carouselRight.on('click', this.showNextGalleryItem);
            $carouselLeft.on('click', this.showPrevGalleryItem);

            $croppers.on("swipeleft", this.showNextGalleryItem);
            $croppers.on("swiperight", this.showPrevGalleryItem);
        }

        // position the carousel navigation
        var arrowTop = Math.ceil($me.height()/2) + 7;
        $carouselRight.css('top', arrowTop + 'px');
        $carouselLeft.css('top', arrowTop + 'px');

        $croppers.trigger('afterGallerySetupCroppers.DBDN_General');
    },

    showPrevGalleryItem : function(){
        var $btn = $(this);
        if ($btn.hasClass('galCropper')) {
            $btn = $btn.next('.carouselNav').find('.carouselLeft')
        }
        var $cropper = $btn.closest('.carouselNav').prev('.galCropper');
        var $mover = $cropper.find('.galMover');
        $mover.stop(true, true);
        // This is needed if you want to animate images with various sizes
        $mover.closest('.galCropper').stop(true, true);
        var cLeft = parseInt( $mover.css('left'), 10);
        var $item = $cropper.find('.galItem.shown');
        var $carouselCounter = $cropper.next().find('.carouselCounter');
        var minMobileHeight = parseInt($carouselCounter.css('top')) + $carouselCounter.outerHeight(true);


        if ($item.prev().length) {
            $mover.animate({'left': cLeft + $item.prev().width()}, 400);
            var newHeight = $item.prev().height();
            if (MOBILE) {
                newHeight = newHeight < minMobileHeight ? minMobileHeight : newHeight;
            }
            // A comment would be nice here, explaining what is the purpose of animating
            // the cropper element.
            // UPDATE (15.1.2018): This is used to smooth the transition when the container is resized due
            // to images with different sizes.
            // UPDATE (16.1.2018): As addition to the above, the container size is also affected by the text size.
            //$mover.parent().animate({height:newHeight, width:$item.prev().width()},400);
            // For the time being assume that only the height of the container will be resized.
            $mover.closest('.galCropper').animate({ height: newHeight }, 400);
            $item.removeClass('shown');
            $item.prev().addClass('shown');
            $btn.closest('.carouselNav').find('.carouselCounter').html('<span>' + parseInt($item.closest('.galMover').find('.galItem').index($item.prev()) + 1) + '</span> / ' + $item.closest('.galMover').find('.galItem').length);
        }else{
            var $last = $item.siblings().last();
            $item.removeClass('shown');
            $last.addClass('shown');
            var childWidth = $item.outerWidth(true);
            //var childWidth = 0;
            $item.siblings().each(function() {
                childWidth += $(this).outerWidth( true );
            });
            childWidth -= $last.outerWidth(); // correction
            $mover.animate({'left': 0-childWidth}, 400);
            var newHeight = $last.height();
            if (MOBILE) {
                newHeight = newHeight < minMobileHeight ? minMobileHeight : newHeight;
            }
            // See the comment in the IF block.
            $mover.closest('.galCropper').animate({ height: newHeight }, 400);
            $btn.closest('.carouselNav').find('.carouselCounter').html('<span>' + $item.closest('.galMover').find('.galItem').length + '</span> / ' + $item.closest('.galMover').find('.galItem').length);
        }
    },

    showNextGalleryItem : function(){
        var $btn = $(this);
        if ($btn.hasClass('galCropper')) {
            $btn = $btn.next('.carouselNav').find('.carouselRight')
        }
        var $cropper = $btn.closest('.carouselNav').prev('.galCropper');
        var $mover = $cropper.find('.galMover');
        $mover.stop(true, true);
        // See the comment in the showPrevGalleryItem method
        $mover.closest('.galCropper').stop(true, true);
        var cLeft = parseInt($mover.css('left'), 10);
        var $item = $cropper.find('.galItem.shown');
        var $carouselCounter = $cropper.next().find('.carouselCounter');
        var minMobileHeight = parseInt($carouselCounter.css('top')) + $carouselCounter.outerHeight(true);

        if ($item.next().length) {
            $item.removeClass('shown');
            $item.next().addClass('shown');
            $mover.animate({'left': cLeft - $item.width()}, 400);
            var newHeight = $item.next().height();
            if (MOBILE) {
                newHeight = newHeight < minMobileHeight ? minMobileHeight : newHeight;
            }
            // See the comment in the showPrevGalleryItem method
            $mover.closest('.galCropper').animate({ height: newHeight }, 400)
            $btn.closest('.carouselNav').find('.carouselCounter').html('<span>' + parseInt($item.closest('.galMover').find('.galItem').index($item.next()) + 1) + '</span> / ' + $item.closest('.galMover').find('.galItem').length);
        }else{
            $mover.animate({'left': 0}, 400);
            var $first = $item.siblings().first();
            $item.removeClass('shown');
            $first.addClass('shown');
            var newHeight = $first.height();
            if (MOBILE) {
                newHeight = newHeight < minMobileHeight ? minMobileHeight : newHeight;
            }
            // See the comment in the showPrevGalleryItem method
            $mover.closest('.galCropper').animate({ height: newHeight }, 400)
            $btn.closest('.carouselNav').find('.carouselCounter').html('<span>1</span> / ' + $item.closest('.galMover').find('.galItem').length);
        }
    },

    setResolutionPictures: function (section, force) {
        if (window.matchMedia) {
            if (typeof (section) === 'undefined') {
                section = 'body';
            }
            if (typeof (force) === 'undefined') {
                force = false;
            }

            var factor = 1;
            if( window.matchMedia("(min-resolution: 3dppx)").matches ||
                window.matchMedia("(-webkit-min-device-pixel-ratio: 3)").matches ||
                window.matchMedia("(min--moz-device-pixel-ratio: 3)").matches ||
                window.matchMedia("(-o-min-device-pixel-ratio: 3/1)").matches ||
                window.matchMedia("(min-device-pixel-ratio: 3) ").matches ||
                window.matchMedia("(min-resolution: 288dpi)").matches){
                factor = 3;
            }else if( window.matchMedia("(min-resolution: 2dppx)").matches ||
                      window.matchMedia("(-webkit-min-device-pixel-ratio: 2)").matches ||
                      window.matchMedia("(min--moz-device-pixel-ratio: 2)").matches ||
                      window.matchMedia("(-o-min-device-pixel-ratio: 2/1)").matches ||
                      window.matchMedia("(min-device-pixel-ratio: 2)").matches ||
                      window.matchMedia("( min-resolution: 192dpi)").matches){
                factor = 2;
            }

            if (factor != 1) {
                var image_selector = '.ready-for-enhanced-resolution';
                if (force) {
                    image_selector = '.content-img';
                }
                var imgs = $(section).find(image_selector);
                imgs.each(function (index) {
                    var $cur_img = $(this);
                    $cur_img.removeClass('ready-for-enhanced-resolution');

                    //TODO: Recalculate image qiuality on rotation
                    var cur_img_factor = factor;
                    if (MOBILE && cur_img_factor > 1) {
                        var curent_orientation = getScreenOrientation();
                        var aspectRatio = $cur_img.width() / $cur_img.height();

                        if (curent_orientation === orientation.portrait && aspectRatio < 1) {
                            ++cur_img_factor;
                        } else if (curent_orientation === orientation.landscape && aspectRatio >= 1) {
                            ++cur_img_factor;
                        }
                    }

                    if ($cur_img.data('loadedDPIFactor') == cur_img_factor || $cur_img.attr('src') == '') {
                        return true;
                    }

                    if (cur_img_factor === 1) {
                        // Skeep this image for hi resolution.
                        return true;
                    }

                    var img = $('<img>');
                    //var svg = $(this).next();

                    if ($cur_img.attr('style') != '') {
                        img.attr('style', $cur_img.attr('style') + ';display:none;');
                    }else{
                        img.attr('style', 'display:none;');
                    }
                    if ($cur_img.hasClass('content-img')) {
                        img.attr('class', $cur_img.attr('class'));
                    } else {
                        img.attr('class', $cur_img.attr('class') + ' content-img');
                    }

                    if ($cur_img.attr('id') != "") {
                        img.attr('id', $cur_img.attr('id'));
                    }
                    if ($cur_img.attr('data-hotspot') != "") {
                        img.attr('data-hotspot', $cur_img.attr('data-hotspot'));
                    }
                    img.attr('border', '0');
                    img.attr('unselectable', 'on');
                    img.load(function () {
                        var $loaded_img = $(this);
                        if(window.location.search.indexOf('debugDPIImages') > -1){
                            $loaded_img.parent().addClass('debug_images_x' + cur_img_factor);
                        }
                        $loaded_img.prev().remove();
                        $loaded_img.show();
//                        if(svg.is('#toBeBlured')){
//                            var svgImg =  svg.find('image');
//                            $(svgImg).attr('xlink:href', $loaded_img.attr('src'));
//                            $(svgImg).attr('width', $loaded_img.width());
//                            $(svgImg).attr('height', $loaded_img.height());
//                        }
                    });

                    var img_src = $cur_img.attr('data-original');
                    if (!img_src) {
                        img_src = $cur_img.attr('src');
                    }

                    img.attr('data-original', img_src);

                    var img_src_arr = img_src.split('/');
                    if(img_src_arr.indexOf('__cache') != -1){
                        var cache_index = img_src_arr.indexOf('__cache') + 1;
                        img_src_arr[cache_index] = img_src_arr[cache_index] + '~' + cur_img_factor + 'x';
                    }else if(img_src_arr.indexOf('protected-file') != -1){
                        var cache_index = img_src_arr.indexOf('protected-file') + 2;
                        if(img_src_arr[cache_index] != 'thumb' && img_src_arr[cache_index] != 'doc'){
                            var split_query = img_src_arr[cache_index].split('?');
                            img_src_arr[cache_index] = split_query[0] + '~' + cur_img_factor + 'x?' + split_query[1];
                        }
                    }
                    img.attr('src', img_src_arr.join('/'));
                    img.data('loadedDPIFactor', cur_img_factor);
                    $cur_img.after(img);
                });
            }
        }
    },

    lazyLoadWithCallBack: function (images){
        if(!images.length){
            if (!imageProcess.isImageProcessingComleted) {
                imageProcess.start();
            }else{
                $(document).trigger('imagesLoaded');
            }
            return false;
        }

        var fi = images.first();
        var isFirst = fi.closest('#fullsize_banner').length > 0? true: false;
        var settings = {
            effect: "show",
            effectTime: 0,
            event: "scroll",
            threshold: -120,
            load: function() {
                var $img = $(this);
                if ($img.hasClass('lazyload-thumb')) {
                    $img.removeClass('lazyload-thumb');
                    //Fix border of the image.
                    $img.closest('a.image_layover').width('auto');

                    $img.addClass('ready-for-enhanced-resolution');
                    // trigger the loading of the scaled (x2, x3) image
                    $(document).trigger('imagesLoaded');
                    if(isFirst) {
                        $('#coverBackground').show();
                    }
                    var $parentCropper = $img.closest(".galCropper");
                    if ($parentCropper.length > 0) {
                        imageProcess.setupCroppers($parentCropper, false);
                    }
                }
            },
            error: function(e) {
                var $img = $(this);
                console.log('Error: Image load: ' + $img.attr('src') + ' error ' + e);
            }
        };

        // Handle an edge case when the device orientation changes from tablet to desktop
        // and sets the original width and height.
        if(isDesktopViewport() && TABLET && fi.closest('#content_main').length > 0){
            fi.attr('width', fi.data('original_width'));
            fi.attr('height', fi.data('original_height'));
        }

        fi.lazyload(settings);

        // Init lazyLoad for next image
        images = images.slice(1);
        this.lazyLoadWithCallBack(images);
    },

    updateLazyload: function () {
        setTimeout(function () { $(window).trigger('scroll'); }, 200);
    }

};

var vertical_navigation = {
    marker_offsets : new Array(),
    marker_select_disable: false,
    marker_selectd: false,
    hasDocumentReadyBeenCalled: false,
    hasWindowLoadedBeenCalled: false,

    windowLoad: function(initialCall){

        this.markerSelectInit(initialCall);
        if (initialCall) {
            this.hasWindowLoadedBeenCalled = true;

            if(document.location.href.lastIndexOf('#') > -1){
                var anch = document.location.href.substr(document.location.href.lastIndexOf('#') + 1);
                if (anch.length !== 0) {
                    this.marker_selectd = anch;
                }
            }
        }
        if (this.marker_selectd) {
            var anchor = $('#cd-vertical-nav ul.dot_items li.dots').find('a#'+this.marker_selectd);
            if (anchor.length > 0) {
                anchor.click();
//                $('header, #basic_nav_closed').sticky(false);
            }
        }
    },

    documentReady: function(){

        // disable anchor nav on mobile
        //if(MOBILE)
        //    return;

        var that = this;

        $('#cd-vertical-nav #add-icon a,#cd-vertical-nav #remove-icon a').click(function(){
            $('#watchlistForm').submit();
            return false;
        });
        $('#cd-vertical-nav #rate-icon a').click(function(){
            if($(this).parent().hasClass('unactive')){
                $("#notification .alert_text").html(js_localize['page.rated'])
                generateNotification();
            }else{
                $('#rateForm').submit();
            }
            return false;
        });

        if(isDesktopViewport()){
            this.createAnchorNav();
            /*** Hide right navigation ***/
            addPassiveEventListener("scroll", function() {
                that.checkOffset();
            });

            this.hasDocumentReadyBeenCalled = true;
            }
            //this.windowLoad(true);
        },

    markerMap : function(){
        var that = this;

        this.marker_offsets = new Array();

        $('#cd-vertical-nav .dot_items a').each(function() {
            var target = $('section #' + $(this).attr('id') + '.block_title');
            var new_offset = Math.floor(target.offset().top) - 50 ;//- $('header.no-sticky').height(); //header no-sticky height + header sticky height
            that.marker_offsets.push(new_offset);
        });
    },

    markerSelectInit : function(initialCall){
        var that = this;

        that.markerMap();
        if (initialCall) {
            $(window).bind('myScrollEvent', function() {
                that.markerSelect();
            });

            // Use custom Event handling instead of $(window).scroll(function(){
            addPassiveEventListener("scroll", function() {
                $(window).trigger('myScrollEvent');
            });
        }
    },

    markerSelect : function(){
        if (this.marker_select_disable) {
            // Skip the selection!
            return;
        }

        var current_anchor_index = 0,
            scrollTop = $(window).scrollTop(),
            sticky_nav = $("header.sticky"),
            top_nav = $("div#topNotifications");

        if (!MOBILE && top_nav.length && top_nav.is(":visible")) {
            scrollTop += top_nav.outerHeight();
        }

        if(sticky_nav.length && sticky_nav.is(":visible")) {
            scrollTop += sticky_nav.position().top + sticky_nav.outerHeight();
        }

        while (current_anchor_index < this.marker_offsets.length && scrollTop >= this.marker_offsets[current_anchor_index]) {
            current_anchor_index++;
        }

        if (current_anchor_index === 0 && scrollTop < this.marker_offsets[current_anchor_index]) {
            current_anchor_index = -1;
        } else {
            current_anchor_index -= 1;
        }
        var active_anchor = $('#cd-vertical-nav .dot_items a.is-selected');

        if (current_anchor_index >=0 && current_anchor_index < this.marker_offsets.length) {

            var active_anchor_index = active_anchor.parent().index();

            if (current_anchor_index !== active_anchor_index) {
                active_anchor.removeClass('is-selected');
                var activeLink = $('#cd-vertical-nav .dot_items li').eq(current_anchor_index).find('a');
                activeLink.addClass('is-selected');
                if('replaceHash' in window)
                    window.replaceHash(activeLink.attr('id'));
            }
        } else {
            active_anchor.removeClass('is-selected');
            console.log("Out of range: " + current_anchor_index);
        }
    },

    createAnchorNav: function (){

        var that = this;

        if($('#cd-vertical-nav').length > 0){
            if($('img.anchor_nav').length > 0 || $('.block_title').length > 0 ) {
                $('#cd-vertical-nav ul.dot_items').show();
            }
            var link = $('#topLinks #language a').attr('href');
            if(link != undefined){
                link = link.substr(0, link.length - 2)+lang_label;
            }else{
                link = window.location.href+ '?lang=' + lang_label;
            }

            if(!($('.block_title').length < 2 && $('.downloads_overview').length > 0)){
                $('.block_title').each(function(index) {
                    var _this = $(this),
                        id = _this.attr('id').replace(/\-\d+$/,'').trim(),
						// Remove Counter [X]
                        txt = _this.html().trim().replace(/\[\d+\]$/,'');
                        // Replace Double dagger and dot
                        txt = txt.replace(/[\u2021\.]/g,'').trim();

                    _this.attr('id',id);

                    var html = '<li class="dots">\
                                <a href="'+link+'#'+id+'" id="'+id+'">\
                                    <span class="cd-dot"></span>\
                                    <span class="cd-label">'+txt+'</span>\
                                </a>\
                            </li>';

                    if ($('#cd-vertical-nav ul.dot_items li.dots').length > 0) {
                        $('#cd-vertical-nav ul.dot_items li.dots').last().after(html);
                    } else {
                        $('#cd-vertical-nav ul.dot_items').prepend(html);
                    }
                });
            }

            $('img.anchor_nav').each(function(index) {

                var _this = $(this),
                    id_attr = uriEncode(_this.data('text'));

                _this.addClass('block_title').attr('id',id_attr);

                var html = '<li class="dots">\
                            <a href="'+link+'#'+id_attr+'" id="'+id_attr+'">\
                                <span class="cd-dot"></span>\
                                <span class="cd-label">'+_this.data('text')+'</span>\
                            </a>\
                        </li>';

                if($('#cd-vertical-nav ul.dot_items li.dots').length > 0) {
                    $('#cd-vertical-nav ul.dot_items li.dots').last().after(html);
                } else {
                    $('#cd-vertical-nav ul.dot_items').prepend(html);
                }
            });

            $('li.dots a').click(function(){
                //$(window).unbind('myScrollEvent');
                that.marker_select_disable = true;
                $('li.dots a').removeClass('is-selected');
                $(this).addClass('is-selected');

                $('section #' + $(this).attr('id')).scrollTo(function () {
//                    $(window).bind('myScrollEvent', function(){that.markerSelect();});
                    // TODO: Clear scroll events
                    // Skip the events
                    setTimeout(function() {that.marker_select_disable = false;}, 60);
                });
                return false;
            });
        }
    },

    checkOffset: function() {
        var vn = $('#cd-vertical-nav');

        if(vn.length > 0){
            if(vn.offset().top + vn.height() >= $('.footer_content').offset().top )
                $('#cd-vertical-nav, .r-links, .related-links-right').css('visibility', 'hidden');
            else
                vn.css('visibility', 'visible');
        }
    }
};

$(window).load(function(){

    var initNav = true;

//    $(document).on('imagesLoaded', function(){
        // disable anchor nav on mobile and tablet
        var scroolToAnchor = isDesktopViewport()? true : false;
        if(scroolToAnchor){
            vertical_navigation.windowLoad(initNav);
        }
        initNav = false;
//    });

    /*Hack e-tracker for braking escape on IE*/
    var old_et_escape = ('function' === typeof window.et_escape) ? window.et_escape : null;
    window.et_escape = function(){
        if(old_et_escape && 1 === arguments.length && 'function' === typeof window.escape)
            return old_et_escape(arguments[0]);
        return 27;
    };

});

jQuery(document).ready(function($){

    $(document).on('imagesLoaded', function () {
        imageProcess.setResolutionPictures();
    });

    $(document).on('mobileMobileViewportChange.DBDN_General', function () {
        imageProcess.setResolutionPictures('body', true);
    });

    $('a#save-icon_button').click(function (e) {
        e.preventDefault();
        DBDN.utils.downloadFileAndDisplayNotificationWhileBeingDownloaded(this,js_localize['pdf.download']);

        return false;
    });

    $("#alerts .ok_button, #alerts .close_button, #alertsBG").on("click", function(){
        $("#alerts, #alertsBG").fadeOut(300,function(){$("#alerts").removeClass('red'); $("#alerts").removeClass('center');});
    });

    if(MOBILE) {
        // $("section.basic_nav_opened.filters.best_practice > div.basic_nav").on("click", ".menu_label",function(){
        $("div.basic_nav").on("click", ".menu_label", switchMobileFilterMenu);
    }

    var imgs = $('img.lazyload-thumb');
    imageProcess.lazyLoadWithCallBack(imgs);
    imageProcess.updateLazyload();
    setTimeout(imageProcess.updateLazyload, 3000); // Tontchev complains that the header image sometimes doesn't get loaded

    $('.slider-item, .front_slider').bind('wheel', imageProcess.updateLazyload);
    $(document).on('touchend', '.slider_container, .grid-3cols > .inner1-wrapper', imageProcess.updateLazyload);
    $('.next.btn_navigation, .prev.btn_navigation, .next.slider_hover, .prev.slider_hover').click(imageProcess.updateLazyload);

    (function () {
        var vcp = docCookies.hasItem('viewed_cookie_policy') ? docCookies.getItem('viewed_cookie_policy') : '';

        if (!vcp || (-1 == vcp.split(',').indexOf(lang_label))) {
            var yearFromNow = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
            var loc_lang_label = (typeof lang_label !== "undefined") ? lang_label : '';
            var newVcp = vcp ? vcp + ',' + loc_lang_label : loc_lang_label;
            var secure = -1 === window.location.hostname.indexOf('192');
            docCookies.setItem('viewed_cookie_policy', newVcp, yearFromNow, '/', null, secure);
            $("#cookie_notification").show();//fadeIn(300);

            $("#cookie_notification .ok_button, #cookie_notification .close_button").on("click", function(){
                $("#cookie_notification").fadeOut(300, calcMessTops);
            });
        }
    })();


    generateNotification();


    $('#topLinks #privacy').click(function(){
        $("#provider_privacy, #alertsBG").fadeIn(300);
        $("#provider_privacy .ok_button, #provider_privacy .close_button, #alertsBG").on("click", function() {
            $("#provider_privacy, #alertsBG").fadeOut(300, function() {
                $("#provider_privacy").removeClass('red');
            });
        });
        return false;
    });

    $('.middle_page_text a, .row a').each(function(){
        if($(this).attr('href') != '' && typeof($(this).attr('href')) != 'undefined'){
            if(isExternal($(this).attr('href'))){
                $(this).addClass('externallink');
            }
        }
    });

    $("a.externallink").click(function(e) {
        e.preventDefault();
        var url = encodeURIComponent($(this).attr('href'));
        location.href = relPath + 'external?url=' + url
    });

    $("#searchForm_faq input#search,#searchForm input#search,input#search2").each(function () {
        var $this = $(this);
        var $parent = $this.parent();
        var classPerInstance = '';
        var classPerInstanceSuffix = '_ac';

        if ($parent.length > 0 && $parent[0].id) {
            classPerInstance = $parent[0].id + classPerInstanceSuffix;
        }

        if ($parent.length < 1 || ($parent.length > 0 && !$parent[0].id)
                && this.id) {
            classPerInstance = this.id + classPerInstanceSuffix;
        }

        $this.autocomplete({
            minLength: 2,
            position: { my: "left top", at: "left bottom", collision: "none" },
            source: function (request, response) {
                var term = request.term.toLowerCase(),
                        element = this.element,
                        cache = this.element.data('autocompleteCache') || {},
                        foundInCache = false;

                var type = 'page';
                var urlParams = '';
                if ('searchForm_faq' === this.element.parent().attr('id')) {
                    type = 'faq';
                    var actionParts = this.element.parent().attr('action').split('/');
                    urlParams = '/' + actionParts[actionParts.length - 1];
                }

                $.each(cache, function (key, data) {
                    if (term.indexOf(key) === 0 && data.length > 0) {
                        var result = jQuery.grep(data, function (n) {
                            return (n.value.toLowerCase().indexOf(term) >= 0);
                        });
                        response(result);
                        foundInCache = true;
                        return;
                    }
                });
                if (foundInCache)
                    return;
                request._token = $(element).parent().find('input[name="_token"]').val();
                $.ajax({
                    url: relPath + 'tag/autocomplete/' + type + urlParams,
                    type: 'POST',
                    dataType: "json",
                    data: request,
                    success: function (data) {
                        var data_arr = new Array();
                        for (var key = 0; key < data.response.length; key++) {
                            tag_value = $('<div/>').html(data.response[key].value).text();
                            if ('faq' == type) {
                                tag_value = tag_value.split('[')[0].trim();
                            }
                            data_arr[key] = {value: tag_value, tag_id: $('<div/>').html(data.response[key].id).text()};
                        }
                        cache[term] = data_arr;
                        element.data('autocompleteCache', cache);
                        response(data_arr);
                    }
                });
            },
            select: function (event, ui) {
                $(this).val(ui.item.value);
                $(this).parent().submit();
                return false;
            }
        })
        .autocomplete('instance')
        .menu
        .element.addClass(classPerInstance);
    });

    $("input#search2").autocomplete('option', 'position',
        { my: "left top", at: "left-20 bottom", collision: "none" });

    /*
    if($(".back-button").length > 0){
        $('.basic_nav_closed').css('margin-top', '67px');
    }else{
        $('.basic_nav_closed').css('margin-top', '16px');
        $('.basic_nav_opened').css('margin-top','-53px');
    }
    */

    $('.scrollbar-rail').scrollbar({
        "autoUpdate": true
    });
//    if($('#fullsize_banner').lenght > 0){
//        var img = $('#fullsize_banner').find('img');
//    }
    if($('.coverBackground').length > 0){
        $(window).scroll(function(){
            calculateOpacity($('#fullsize_banner .simple, #fullsize_banner .background, #fullsize_banner .text'));
        });
    }

    // disable anchor nav on mobile and tablet
    var scroolToAnchor = isDesktopViewport() || isTabletViewport()? true : false;
    if(scroolToAnchor){
        vertical_navigation.documentReady();
    }

    $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
        DBDN.utils.toggleTabletDesktopEvent(function () {
            if (!vertical_navigation.hasDocumentReadyBeenCalled) {
                vertical_navigation.documentReady();

                if (!vertical_navigation.hasWindowLoadedBeenCalled) {
                    vertical_navigation.windowLoad(true);
                }
            } else {
                vertical_navigation.markerMap();
            }
        }, function () {
        }, ev.htmlViewportMode);
    });

    //validate register form!
    if($('#profileForm #iagree').length > 0){
        DBDN.utils.validateProfileForm('#profileForm');
    // validate profile form and remove password field from requarements
    }else{
        DBDN.utils.validateProfileForm('#profileForm', ['password', 'password_confirmation'] );
    }
});

//Force the load of icons for the footer
(function () {
    $.each(['help', 'glossary', 'faq', 'sitemap'], function () {
        var img = new Image();
        img.src = relPath + 'img/template/svg/' + this + '-hover.svg';
    });
    if(isMobileViewport()){
        $.each(['plus_bold_b', 'plus_bold_w', 'minus_bold_b', 'minus_bold_w'], function () {
            var img = new Image();
            img.src = relPath + 'img/template/svg/' + this + '.svg';
        });
    }
}());


///Polyfill for find
// https://tc39.github.io/ecma262/#sec-array.prototype.find
if (!Array.prototype.find) {
  Object.defineProperty(Array.prototype, 'find', {
    value: function(predicate) {
     // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If IsCallable(predicate) is false, throw a TypeError exception.
      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }

      // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
      var thisArg = arguments[1];

      // 5. Let k be 0.
      var k = 0;

      // 6. Repeat, while k < len
      while (k < len) {
        // a. Let Pk be ! ToString(k).
        // b. Let kValue be ? Get(O, Pk).
        // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
        // d. If testResult is true, return kValue.
        var kValue = o[k];
        if (predicate.call(thisArg, kValue, k, o)) {
          return kValue;
        }
        // e. Increase k by 1.
        k++;
      }

      // 7. Return undefined.
      return undefined;
    }
  });
}
//===========

function isExternal(url) {
    var internal_hosts = [location.host,"www.daimler.com","intra.corpintra.net"];
    var internal_protocols = ["http:","https:", "mailto:"];
    var match = url.match(/^([^:\/?#]+:)?(?:\/\/([^\/?#]*))?([^?#]+)?(\?[^#]*)?(#.*)?/);

    if (typeof match[1] === "string" && match[1].length > 0 && internal_protocols.indexOf(match[1].toLowerCase()) < 0) return true;
    if (typeof match[2] === "string" && match[2].length > 0 && internal_hosts.indexOf(match[2].replace(new RegExp(":("+{"http:":80,"https:":443}[location.protocol]+")?$"), "")) < 0) return true;
    return false;
}

function uriEncode(str) {
    str = str.toLowerCase();

     // convert spaces to '-'
    str = str = str.replace(/ /g, "-");
    // Make lowercase

    str = str.replace(/\u00df/g, "ss");
    str = str.replace(/\u00e4/g, "ae");
    str = str.replace(/\u00f6/g, "oe");
    str = str.replace(/\u00FC/g, "ue");


   // Remove characters that are not alphanumeric or a '-'
   str = str.replace(/[^a-z0-9-]/g, "-");
   // Combine multiple dashes (i.e., '---') into one dash '-'.
   str = str.replace(/[-]+/g, "-");
   str = str.replace(/^-/g, "");
   str = str.replace(/-$/g, "");
   return str;

}

function msg(strHTML, isAlert) {
   isAlert = isAlert ? isAlert : false;
   if(isAlert){
       $("#notification").addClass('red');
    }
    $("#notification .alert_text").html(strHTML);
//    $("#alertsContents .alert_text").html(strHTML);
//    $("#alerts, #alertsBG").fadeIn(300);
//    $("#alerts .ok_button, #alerts .close_button, #alertsBG").on("click", function(){
//        $("#alerts, #alertsBG").fadeOut(300,function(){$("#alerts").removeClass('red');});
//    });
    generateNotification();
}

function generateNotification() {
    var $ac = $($("#notification .alert_text")[0]);
    if ($ac.length > 0 && $ac.text().replace(/\s/g, '').length > 10) {
        $("#notification").fadeIn(300, function () {
            if(document.location.href.lastIndexOf('#') == -1){
                $('body').scrollTo();
            }
        });
        $("#notification .close_button").on("click", function () {
            $("#notification").fadeOut(300, function () {
                calcMessTops();
                $("#notification").removeClass('red');
            });
        });
    }
}


function switchMobileFilterMenu() {

    var t = $(this), a = (window === this) ? true : t.hasClass('active');

    $("div.basic_nav > .menu_label").removeClass('active');
    $("body").toggleClass('filter-dialog-active');

    if(a) {
        menu.overlay(0);
    } else {
        menu.overlay(1);
        t.addClass('active');
    }
}

$(document).ready(function() {
    ///*** Home carousel ***//
    if($('#hbA').length > 0){
//        var res = '';
//        var num = $('#hbACont a').length;
//       $('#hbACont .banner_image_wrapper img:last-of-type').each(function(index, img){
//           res += '<a class="hBlockPage" href="javascript:;" onclick="showHBA(this);"><img src="'+img.src.replace('brand_banner', 'menu_carousel')+'"></a>';
//       });
    //	for (var i = 0; i < num; i++) {
    //		res += '<a class="hBlockPage" href="javascript:;" onclick="scrollHBA(this);"></a>';
    //	}
//        $('#hbA .hBlockPages').html(res);
       // $('#hbA .hBlockPages a:first-of-type').addClass('hBlockPageActive');
//        var smallImage = new Image();
//        smallImage.onload = function(){
//            $('#hbA .hBlockPages').show();
//            // autoShow();
//        }
//
//        var lupeUrl = $('#hbA .hBlockPages img:last-of-type')[0].src;
//        var reg = /htt[^"\)']+/g;
//        lupeUrl = reg.exec(lupeUrl); // cut the url of the image
//        smallImage.src = lupeUrl[0];

    //	$('#hbA .hBlockPages').css('margin-left','-'+($('#hbA .hBlockPages').width() - 10)/2+'px')


    //	if(num > 1){
    //		$('.hBlockLeft,.hBlockRight').show();
    //		$('.hBlockLeft').click(function() {
    //			moveHBA('left');
    //			return false;
    //		})
    //		$('.hBlockRight').click(function() {
    //			moveHBA('right');
    //			return false;
    //		})
    //	}
        //generateHovers();
    }
    if($('#hbA').length > 0){
        $(window).scroll(function(){
            calculateOpacity($('.banner_text_wrapper, .banner_image_wrapper'))
        });
    }
});

// code for home slider
function generateHovers(){
    var i, hbACont = $("#hbACont a"), hBlockPage = $("a.hBlockPage");

    if(!hBlockPage.length || !hbACont.length)
        return;

    if(hBlockPage.length != hbACont.length)
    {
        console.error("Banners do not match");
        return;
    }

    $("<div>")
        .attr({"id":"sliderPopup","class":"popup"})
        .appendTo($(hBlockPage[0]).parent());

    $(hbACont).each(function(idx, el){
        var t = $(el).find('.banner_text_wrapper h1').html(), h = $(hBlockPage[idx]);

        if(t)
        {
            h.attr("popup-title", t);

            h.hover(
                function(event)
                {
                     var position = $('.hBlockPage').eq(idx).position();
                    $("#sliderPopup")
                        .html($(this).attr("popup-title"))
                        .append($("<div>").attr("class","popup-ticker"))
                        .css({'visibility':'visible', 'overflow':'visible', "left": (position.left - 30) + "px", "top": (position.top - $("#sliderPopup").outerHeight() - 9)+ "px"});
                },
                function(event)
                {
                    $("#sliderPopup").css({visibility:"hidden"}).empty();
                }
            );
        }
    });
}

//function scrollHBA(callingA) {
//	var $caller = $(callingA);
//	var pos = $caller.index();
//	$('#hbACont')
//			.stop(true, false)
//			.animate({left: pos * -1180}, 400, 'swing');
//	$caller.siblings().removeClass('hBlockPageActive');
//	$caller.addClass('hBlockPageActive');
//}

function moveHBA(direction) {
	var pos = $('.hBlockPages .hBlockPageActive').index();
	if (direction == 'right') {
		pos++;
	} else {
		pos--;
	}

	if (pos < 0 || pos >= $('.hBlockPages .hBlockPage').length)
		return false;
	$('#hbACont')
			.stop(true, false)
			.animate({left: pos * -1180}, 400, 'swing');
	$('.hBlockPages .hBlockPage').removeClass('hBlockPageActive');
	$('.hBlockPages .hBlockPage').eq(pos).addClass('hBlockPageActive');
}

//function autoScroll() {
//
//	var $hbACont = $('#hbACont');
//	var lft = parseInt($hbACont.css('left'), 10);
//	$('#hbA a.hBlockPage').removeClass('hBlockPageActive');
//	$('#hbA a.hBlockPage').eq(lft / -1180).addClass('hBlockPageActive');
//	if (lft > (-1180 * ($hbACont.children('a').length - 1))) { // is container.left > the total (negative) width of its children
//		$hbACont
//				.delay(10000)
//				//.delay(1000)
//				.animate({left: lft - 1180}, 700, 'swing',
//						function() {
//							autoScroll();
//						});
//	} else {
//		$hbACont
//				.delay(10000)
//				.animate({left: 0}, 900, 'swing',
//						function() {
//							autoScroll();
//						});
//	}
//}
function showHBA(callingA) {
	var $caller = $(callingA);
    if(!$caller.hasClass('hBlockPageActive')){
        var index = $caller.index();
        var prevIndex = $('.hBlockPageActive').index();
        var prevSelected = $('.hBlockPageActive');
        $(prevSelected).removeClass('hBlockPageActive');
            $caller.addClass('hBlockPageActive');
        $('#hbACont a').eq(prevIndex).fadeOut();
        $('#hbACont a').eq(index).fadeIn();
    }
}
function autoShow() {
    setInterval(function(){
        var currIndex = $('.hBlockPageActive').index();
        var currSelected = $('.hBlockPageActive');
        var nextIndex = (currIndex + 1) < $('.hBlockPage').length ? (currIndex + 1): 0;
        currSelected.removeClass('hBlockPageActive');
        $('.hBlockPage').eq(nextIndex).addClass('hBlockPageActive');
        $('#hbACont a').eq(currIndex).fadeOut();
        $('#hbACont a').eq(nextIndex).fadeIn();
    }, 10000);
}
//function calculateOpacity(){
//    var scrollTop = $(window).scrollTop(), opacityElement = $('.banner_text_wrapper, .banner_image_wrapper');
////    if(scrollTop < 72){
////        if(opacityElement.css('opacity') != '1'){
////            opacityElement.css('opacity', '1');
////        }
////        return;
////    }
//    if(scrollTop > 750){
//        if(opacityElement.css('opacity') != '0'){
//            opacityElement.css('opacity', '0');
//        }
//
//        return;
//    }
//    var opacity = (11 - (scrollTop / 68)) / 10;
//    opacityElement.css('opacity', opacity);
////    if(scrollTop >= 72 && scrollTop < 144){
////        opacityElement.css('opacity', '0.9');
////    }else if(scrollTop < 216){
////        opacityElement.css('opacity', '0.8');
////    }else if(scrollTop < 288){
////        opacityElement.css('opacity', '0.7');
////    } else if(scrollTop < 360){
////        opacityElement.css('opacity', '0.6');
////    }else if(scrollTop < 432){
////        opacityElement.css('opacity', '0.5');
////    }else if(scrollTop < 504){
////        opacityElement.css('opacity', '0.4');
////    }else if(scrollTop < 576){
////        opacityElement.css('opacity', '0.3');
////    }else if(scrollTop < 648){
////        opacityElement.css('opacity', '0.2');
////    }else if(scrollTop < 720){
////        opacityElement.css('opacity', '0.1');
////    }else if(scrollTop < 800){
////        opacityElement.css('opacity', '0');
////    }
//}

/*!
	Zoom 1.7.14
	license: MIT
	http://www.jacklmoore.com/zoom
*/
(function($){var defaults={url:false,callback:false,target:false,duration:120,on:"mouseover",touch:true,onZoomIn:false,onZoomOut:false,magnify:1};$.zoom=function(target,source,img,magnify){var targetHeight,targetWidth,sourceHeight,sourceWidth,xRatio,yRatio,offset,$target=$(target),position=$target.css("position"),$source=$(source);$target.css("position",/(absolute|fixed)/.test(position)?position:"relative");$target.css("overflow","hidden");img.style.width=img.style.height="";$(img).addClass("zoomImg").css({position:"absolute",top:0,left:0,opacity:0,width:img.width*magnify,height:img.height*magnify,border:"none",maxWidth:"none",maxHeight:"none"}).appendTo(target);return{init:function(){targetWidth=$target.outerWidth();targetHeight=$target.outerHeight();if(source===$target[0]){sourceWidth=targetWidth;sourceHeight=targetHeight}else{sourceWidth=$source.outerWidth();sourceHeight=$source.outerHeight()}xRatio=(img.width-targetWidth)/sourceWidth;yRatio=(img.height-targetHeight)/sourceHeight;offset=$source.offset()},move:function(e){var left=e.pageX-offset.left,top=e.pageY-offset.top;top=Math.max(Math.min(top,sourceHeight),0);left=Math.max(Math.min(left,sourceWidth),0);img.style.left=left*-xRatio+"px";img.style.top=top*-yRatio+"px"}}};$.fn.zoom=function(options){return this.each(function(){var settings=$.extend({},defaults,options||{}),target=settings.target||this,source=this,$source=$(source),$target=$(target),img=document.createElement("img"),$img=$(img),mousemove="mousemove.zoom",clicked=false,touched=false,$urlElement;if(!settings.url){$urlElement=$source.find("img");if($urlElement[0]){settings.url=$urlElement.data("src")||$urlElement.attr("src")}if(!settings.url){return}}(function(){var position=$target.css("position");var overflow=$target.css("overflow");$source.one("zoom.destroy",function(){$source.off(".zoom");$target.css("position",position);$target.css("overflow",overflow);$img.remove()})})();img.onload=function(){var zoom=$.zoom(target,source,img,settings.magnify);function start(e){zoom.init();zoom.move(e);$img.stop().fadeTo($.support.opacity?settings.duration:0,1,$.isFunction(settings.onZoomIn)?settings.onZoomIn.call(img):false)}function stop(){$img.stop().fadeTo(settings.duration,0,$.isFunction(settings.onZoomOut)?settings.onZoomOut.call(img):false)}if(settings.on==="grab"){$source.on("mousedown.zoom",function(e){if(e.which===1){$(document).one("mouseup.zoom",function(){stop();$(document).off(mousemove,zoom.move)});start(e);$(document).on(mousemove,zoom.move);e.preventDefault()}})}else if(settings.on==="click"){$source.on("click.zoom",function(e){if(clicked){return}else{clicked=true;start(e);$(document).on(mousemove,zoom.move);$(document).one("click.zoom",function(){stop();clicked=false;$(document).off(mousemove,zoom.move)});return false}})}else if(settings.on==="toggle"){$source.on("click.zoom",function(e){if(clicked){stop()}else{start(e)}clicked=!clicked})}else if(settings.on==="mouseover"){zoom.init();$source.on("mouseenter.zoom",start).on("mouseleave.zoom",stop).on(mousemove,zoom.move)}if(settings.touch){$source.on("touchstart.zoom",function(e){e.preventDefault();if(touched){touched=false;stop()}else{touched=true;start(e.originalEvent.touches[0]||e.originalEvent.changedTouches[0])}}).on("touchmove.zoom",function(e){e.preventDefault();zoom.move(e.originalEvent.touches[0]||e.originalEvent.changedTouches[0])})}if($.isFunction(settings.callback)){settings.callback.call(img)}};img.src=settings.url})};$.fn.zoom.defaults=defaults})(window.jQuery);
/*!
 * Lazy Load - jQuery plugin for lazy loading images
 *
 * Copyright (c) 2007-2015 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *   http://www.appelsiini.net/projects/lazyload
 *
 * Version:  1.9.7
 *
 */

(function($, window, document, undefined) {
    var $window = $(window);

    $.fn.lazyload = function(options) {
        var elements = this;
        var $container;
        var settings = {
            threshold       : 0,
            failure_limit   : 0,
            event           : "scroll",
            effect          : "show",
            container       : window,
            data_attribute  : "original",
            skip_invisible  : false,
            appear          : null,
            load            : null,
            error           : null,
            placeholder     : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
        };

        function update() {
            var counter = 0;

            elements.each(function() {
                var $this = $(this);
                if (settings.skip_invisible && !$this.is(":visible")) {
                    return;
                }
                if ($.abovethetop(this, settings) ||
                    $.leftofbegin(this, settings)) {
                        /* Nothing. */
                } else if (!$.belowthefold(this, settings) &&
                    !$.rightoffold(this, settings)) {
                        $this.trigger("appear");
                        /* if we found an image we'll load, reset the counter */
                        counter = 0;
                } else {
                    if (++counter > settings.failure_limit) {
                        return false;
                    }
                }
            });

        }

        if(options) {
            /* Maintain BC for a couple of versions. */
            if (undefined !== options.failurelimit) {
                options.failure_limit = options.failurelimit;
                delete options.failurelimit;
            }
            if (undefined !== options.effectspeed) {
                options.effect_speed = options.effectspeed;
                delete options.effectspeed;
            }

            $.extend(settings, options);
        }

        /* Cache container as jQuery as object. */
        $container = (settings.container === undefined ||
                      settings.container === window) ? $window : $(settings.container);

        /* Fire one scroll event per scroll. Not one scroll event per image. */
        if (0 === settings.event.indexOf("scroll")) {
            $container.bind(settings.event, function() {
                return update();
            });
        }

        this.each(function() {
            var self = this;
            var $self = $(self);

            self.loaded = false;

            /* If no src attribute given use data:uri. */
            if ($self.attr("src") === undefined || $self.attr("src") === false) {
                if ($self.is("img")) {
                    $self.attr("src", settings.placeholder);
                }
            }

            /* When appear is triggered load original image. */
            $self.one("appear", function() {
                if (!this.loaded) {
                    if (settings.appear) {
                        var elements_left = elements.length;
                        settings.appear.call(self, elements_left, settings);
                    }
                    $("<img />")
                        .bind("load", function() {

                            var original = $self.attr("data-" + settings.data_attribute);
                            $self.hide();
                            if ($self.is("img")) {
                                $self.attr("src", original);
                            } else {
                                $self.css("background-image", "url('" + original + "')");
                            }
                            $self[settings.effect](settings.effect_speed);

                            self.loaded = true;

                            /* Remove image from array so it is not looped next time. */
                            var temp = $.grep(elements, function(element) {
                                return !element.loaded;
                            });
                            elements = $(temp);

                            if (settings.load) {
                                var elements_left = elements.length;
                                settings.load.call(self, elements_left, settings);
                                }
                        }).error(function () {
                            self.error = true;
                            var temp = $.grep(elements, function (element) {
                                return !element.loaded && !element.error;
                            });
                            var elements_left = temp.length;
                            settings.error.call(self, elements_left, settings);
                        })
                        .attr("src", $self.attr("data-" + settings.data_attribute));
                }
            });

            /* When wanted event is triggered load original image */
            /* by triggering appear.                              */
            if (0 !== settings.event.indexOf("scroll")) {
                $self.bind(settings.event, function() {
                    if (!self.loaded) {
                        $self.trigger("appear");
                    }
                });
            }
        });

        /* Check if something appears when window is resized. */
        $window.bind("resize", function() {
            update();
        });

        /* With IOS5 force loading images when navigating with back button. */
        /* Non optimal workaround. */
        if ((/(?:iphone|ipod|ipad).*os 5/gi).test(navigator.appVersion)) {
            $window.bind("pageshow", function(event) {
                if (event.originalEvent && event.originalEvent.persisted) {
                    elements.each(function() {
                        $(this).trigger("appear");
                    });
                }
            });
        }

        /* Force initial check if images should appear. */
        $(document).ready(function() {
            update();
        });

        return this;
    };

    /* Convenience methods in jQuery namespace.           */
    /* Use as  $.belowthefold(element, {threshold : 100, container : window}) */

    $.belowthefold = function(element, settings) {
        var fold;

        if (settings.container === undefined || settings.container === window) {
            fold = (window.innerHeight ? window.innerHeight : $window.height()) + $window.scrollTop();
        } else {
            fold = $(settings.container).offset().top + $(settings.container).height();
        }
        return fold <= $(element).parent().offset().top - settings.threshold;
    };

    $.rightoffold = function(element, settings) {
        var fold;

        if (settings.container === undefined || settings.container === window) {
            fold = $window.width() + $window.scrollLeft();
        } else {
            fold = $(settings.container).offset().left + $(settings.container).width();
        }

        return fold <= $(element).parent().offset().left - settings.threshold;
    };

    $.abovethetop = function(element, settings) {
        var fold;

        if (settings.container === undefined || settings.container === window) {
            fold = $window.scrollTop();
        } else {
            fold = $(settings.container).offset().top;
        }

        return fold >= $(element).parent().offset().top + settings.threshold  + $(element).height();
    };

    $.leftofbegin = function(element, settings) {
        var fold;

        if (settings.container === undefined || settings.container === window) {
            fold = $window.scrollLeft();
        } else {
            fold = $(settings.container).offset().left;
        }

        return fold >= $(element).parent().offset().left + settings.threshold + $(element).width();
    };

    $.inviewport = function(element, settings) {
         return !$.rightoffold(element, settings) && !$.leftofbegin(element, settings) &&
                !$.belowthefold(element, settings) && !$.abovethetop(element, settings);
     };

    /* Custom selectors for your convenience.   */
    /* Use as $("img:below-the-fold").something() or */
    /* $("img").filter(":below-the-fold").something() which is faster */

    $.extend($.expr[":"], {
        "below-the-fold" : function(a) { return $.belowthefold(a, {threshold : 0}); },
        "above-the-top"  : function(a) { return !$.belowthefold(a, {threshold : 0}); },
        "right-of-screen": function(a) { return $.rightoffold(a, {threshold : 0}); },
        "left-of-screen" : function(a) { return !$.rightoffold(a, {threshold : 0}); },
        "in-viewport"    : function(a) { return $.inviewport(a, {threshold : 0}); },
        /* Maintain BC for couple of versions. */
        "above-the-fold" : function(a) { return !$.belowthefold(a, {threshold : 0}); },
        "right-of-fold"  : function(a) { return $.rightoffold(a, {threshold : 0}); },
        "left-of-fold"   : function(a) { return !$.rightoffold(a, {threshold : 0}); }
    });

})(jQuery, window, document);

//***********************************************************************************************************************************/
//	LyteBox v3.22
//
//	 Author: Markus F. Hay
//  Website: http://www.dolem.com/lytebox
//	   Date: October 2, 2007
//	License: Creative Commons Attribution 3.0 License (http://creativecommons.org/licenses/by/3.0/)
// Browsers: Tested successfully on WinXP with the following browsers (using no DOCTYPE and Strict/Transitional/Loose DOCTYPES):
//				* Firefox: 2.0.0.7, 1.5.0.12
//				* Internet Explorer: 7.0, 6.0 SP2, 5.5 SP2
//				* Opera: 9.23
//
// Releases: For up-to-date and complete release information, visit http://www.dolem.com/forum/showthread.php?tid=62
//				* v3.22 (10/02/07)
//				* v3.21 (09/30/07)
//				* v3.20 (07/12/07)
//				* v3.10 (05/28/07)
//				* v3.00 (05/15/07)
//				* v2.02 (11/13/06)
//
//   Credit: LyteBox was originally derived from the Lightbox class (v2.02) that was written by Lokesh Dhakar. For more
//			 information please visit http://huddletogether.com/projects/lightbox2/
//***********************************************************************************************************************************/
Array.prototype.removeDuplicates = function () {
    for (var i = 1; i < this.length; i++) {
        if (this[i][0] == this[i - 1][0]) {
            this.splice(i, 1);
        }
    }
}
Array.prototype.empty = function () {
    for (var i = 0; i <= this.length; i++) {
        this.shift();
    }
}
String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, '');
}

function LyteBox() {
    /*** Start Global Configuration ***/
    this.theme = 'white';	// themes: grey (default), red, green, blue, gold
    this.hideFlash = true;		// controls whether or not Flash objects should be hidden
    this.outerBorder = true;		// controls whether to show the outer grey (or theme) border
    this.resizeSpeed = 8;		// controls the speed of the image resizing (1=slowest and 10=fastest)
    this.maxOpacity = 80;		// higher opacity = darker overlay, lower opacity = lighter overlay
    this.navType = 1;		// 1 = "Prev/Next" buttons on top left and left (default), 2 = "<< prev | next >>" links next to image number
    this.autoResize = true;		// controls whether or not images should be resized if larger than the browser window dimensions
    this.doAnimations = true;		// controls whether or not "animate" Lytebox, i.e. resize transition between images, fade in/out effects, etc.

    this.borderSize = 60;		// if you adjust the padding in the CSS, you will need to update this variable -- otherwise, leave this alone...
    /*** End Global Configuration ***/

    /*** Configure Slideshow Options ***/
    this.slideInterval = 4000;		// Change value (milliseconds) to increase/decrease the time between "slides" (10000 = 10 seconds)
    this.showNavigation = true;		// true to display Next/Prev buttons/text during slideshow, false to hide
    this.showClose = true;		// true to display the Close button, false to hide
    this.showDetails = true;		// true to display image details (caption, count), false to hide
    this.showPlayPause = true;		// true to display pause/play buttons next to close button, false to hide
    this.autoEnd = true;		// true to automatically close Lytebox after the last image is reached, false to keep open
    this.pauseOnNextClick = false;	// true to pause the slideshow when the "Next" button is clicked
    this.pauseOnPrevClick = true;		// true to pause the slideshow when the "Prev" button is clicked
    /*** End Slideshow Configuration ***/

    if (this.resizeSpeed > 10) {
        this.resizeSpeed = 10;
    }
    if (this.resizeSpeed < 1) {
        resizeSpeed = 1;
    }
    this.resizeDuration = (11 - this.resizeSpeed) * 0.15;
    this.resizeWTimerArray = new Array();
    this.resizeWTimerCount = 0;
    this.resizeHTimerArray = new Array();
    this.resizeHTimerCount = 0;
    this.showContentTimerArray = new Array();
    this.showContentTimerCount = 0;
    this.overlayTimerArray = new Array();
    this.overlayTimerCount = 0;
    this.imageTimerArray = new Array();
    this.imageTimerCount = 0;
    this.timerIDArray = new Array();
    this.timerIDCount = 0;
    this.slideshowIDArray = new Array();
    this.slideshowIDCount = 0;
    this.AllTimerCollection = [
        this.resizeWTimerArray,
        this.resizeHTimerArray,
        this.showContentTimerArray,
        this.overlayTimerArray,
        this.imageTimerArray,
        this.timerIDArray,
        this.slideshowIDArray];
    this.imageArray = new Array();
    this.activeImage = null;
    this.slideArray = new Array();
    this.activeSlide = null;
    this.frameArray = new Array();
    this.activeFrame = null;
    this.checkFrame();
    this.isSlideshow = false;
    this.isLyteframe = false;
    this.initZoom = false;
    this.bodyScroll = null;
    this.boundEventCollection = {};
    //this.ie = (document.all && !window.opera) ? true : false;
    /*@cc_on
     /*@if (@_jscript)
     this.ie = (document.all && !window.opera) ? checkVersion() : false;
     /*@else @*/
    this.ie = false;
    /*@end
     @*/
    this.ie7 = (this.ie && window.XMLHttpRequest);
    this.initialize();
}

function checkVersion() {
    if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
        var ieversion = new Number(RegExp.$1)
        if (ieversion >= 8)
            return false;
        else if (ieversion < 8)
            return true;
    }
    return false;
}

LyteBox.prototype.initialize = function () {
    this.updateLyteboxItems();
    var objBody = this.doc.getElementsByTagName("body").item(0);
    if (this.doc.getElementById('lbOverlay')) {
        objBody.removeChild(this.doc.getElementById("lbOverlay"));
        objBody.removeChild(this.doc.getElementById("lbMain"));
    }
    var objOverlay = this.doc.createElement("div");
    objOverlay.setAttribute('id', 'lbOverlay');
    objOverlay.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    if ((this.ie && !this.ie7) || (this.ie7 && this.doc.compatMode == 'BackCompat')) {
        objOverlay.style.position = 'absolute';
    }
    objOverlay.style.display = 'none';
    objBody.appendChild(objOverlay);
    var objLytebox = this.doc.createElement("div");
    objLytebox.setAttribute('id', 'lbMain');
    objLytebox.style.display = 'none';
    objBody.appendChild(objLytebox);
    var lbWrapper = this.doc.createElement("div");
    lbWrapper.setAttribute('id', 'lbWrapper');
    objLytebox.appendChild(lbWrapper);
    var objOuterContainer = this.doc.createElement("div");
    objOuterContainer.setAttribute('id', 'lbOuterContainer');
    objOuterContainer.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    lbWrapper.appendChild(objOuterContainer);
    var objIframeContainer = this.doc.createElement("div");
    objIframeContainer.setAttribute('id', 'lbIframeContainer');
    objIframeContainer.style.display = 'none';
    objLytebox.appendChild(objIframeContainer);
    var objIframe = this.doc.createElement("iframe");
    objIframe.setAttribute('id', 'lbIframe');
    objIframe.setAttribute('name', 'lbIframe');
    objIframe.style.display = 'none';
    objIframeContainer.appendChild(objIframe);
    var objImageContainer = this.doc.createElement("div");
    objImageContainer.setAttribute('id', 'lbImageContainer');
    objOuterContainer.appendChild(objImageContainer);
    var objImageWrapper = this.doc.createElement("div");
    objImageWrapper.setAttribute('id', 'lbImageWrapper');
    objImageContainer.appendChild(objImageWrapper);
    var objLyteboxImage = this.doc.createElement("img");
    objLyteboxImage.setAttribute('id', 'lbImage');
    objImageWrapper.appendChild(objLyteboxImage);
    var objLoadingWrapper = this.doc.createElement("div");
    objLoadingWrapper.setAttribute('id', 'lbLoadingWrapper');
    var objLoading = this.doc.createElement("div");
    objLoading.setAttribute('class', 'basic_lbLoading');
    objLoading.setAttribute('id', 'lbLoading');
    objLoadingWrapper.appendChild(objLoading);
    objLytebox.appendChild(objLoadingWrapper);
    for (var i = 1; i < 4; i++) {
        var innderDiv = this.doc.createElement("div");
        innderDiv.setAttribute('class', ('cssload-rect' + i));
        objLoading.appendChild(innderDiv);
    }
    var objDetailsContainer = this.doc.createElement("div");
    objDetailsContainer.setAttribute('id', 'lbDetailsContainer');
    objDetailsContainer.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    lbWrapper.appendChild(objDetailsContainer);
    var objDetailsData = this.doc.createElement("div");
    objDetailsData.setAttribute('id', 'lbDetailsData');
    objDetailsData.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    objDetailsContainer.appendChild(objDetailsData);
    var objDetails = this.doc.createElement("div");
    objDetails.setAttribute('id', 'lbDetails');
    objDetailsData.appendChild(objDetails);
    var objCaption = this.doc.createElement("span");
    objCaption.setAttribute('id', 'lbCaption');
    objDetails.appendChild(objCaption);
    var objHoverNav = this.doc.createElement("div");
    objHoverNav.setAttribute('id', 'lbHoverNav');
    objImageContainer.appendChild(objHoverNav);
    var objBottomNav = this.doc.createElement("div");
    objBottomNav.setAttribute('id', 'lbBottomNav');
    objLytebox.appendChild(objBottomNav);
    var objPrev = this.doc.createElement("a");
    objPrev.setAttribute('id', 'lbPrev');
    objPrev.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    objPrev.setAttribute('href', '#');
    objHoverNav.appendChild(objPrev);
    var objNumberDisplay = this.doc.createElement("span");
    objNumberDisplay.setAttribute('id', 'lbNumberDisplay');
    //objDetails.appendChild(objNumberDisplay);
    objHoverNav.appendChild(objNumberDisplay);
    var objNext = this.doc.createElement("a");
    objNext.setAttribute('id', 'lbNext');
    objNext.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    objNext.setAttribute('href', '#');
    objHoverNav.appendChild(objNext);

    var objNavDisplay = this.doc.createElement("span");
    objNavDisplay.setAttribute('id', 'lbNavDisplay');
    objNavDisplay.style.display = 'none';
    objDetails.appendChild(objNavDisplay);

    var objCopySpan = this.doc.createElement("span");
    objCopySpan.setAttribute('id', 'copyspan');
    //objCopySpan.innerHTML = lcopy();
    //objDetailsContainer.appendChild(objCopySpan);
    objDetails.appendChild(objCopySpan);
    var objCountSpan = this.doc.createElement("div");
    objCountSpan.setAttribute('id', 'imageCounter');
    objDetails.appendChild(objCountSpan);

    var objInnerSpan = this.doc.createElement("span");
    objInnerSpan.setAttribute('id', 'innercopy');
    objInnerSpan.innerHTML = lcopy();
    objCopySpan.appendChild(objInnerSpan);

    var objClose = this.doc.createElement("a");
    objClose.setAttribute('id', 'lbClose');
    objClose.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    objClose.setAttribute('href', '#');
    objBottomNav.appendChild(objClose);
    var objTextEsc = this.doc.createElement("span");
    objTextEsc.innerHTML = 'esc';
    objTextEsc.setAttribute('id', 'lbEsc');
    objBottomNav.appendChild(objTextEsc);
    var objPause = this.doc.createElement("a");
    objPause.setAttribute('id', 'lbPause');
    objPause.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    objPause.setAttribute('href', '#');
    objPause.style.display = 'none';
    objBottomNav.appendChild(objPause);
    var objPlay = this.doc.createElement("a");
    objPlay.setAttribute('id', 'lbPlay');
    objPlay.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    objPlay.setAttribute('href', '#');
    objPlay.style.display = 'none';
    objBottomNav.appendChild(objPlay);
};
LyteBox.prototype.updateLyteboxItems = function () {
    var anchors = (this.isFrame) ? window.parent.frames[window.name].document.getElementsByTagName('a') : document.getElementsByTagName('a');
    for (var i = 0; i < anchors.length; i++) {
        var anchor = anchors[i];
        var relAttribute = String(anchor.getAttribute('rel'));
        if (anchor.getAttribute('href')) {
            if (relAttribute.toLowerCase().match('lytebox')) {
                this.addInlineEvent(anchor, 'onclick', function () {
                    myLytebox.start(this, false, false);
                    return false;
                });
            } else if (relAttribute.toLowerCase().match('lyteshow')) {
                this.addInlineEvent(anchor, 'onclick', function () {
                    myLytebox.start(this, true, false);
                    return false;
                });
            } else if (relAttribute.toLowerCase().match('lyteframe')) {
                this.addInlineEvent(anchor, 'onclick', function () {
                    myLytebox.start(this, false, true);
                    return false;
                });
            }
        }
    }
};
LyteBox.prototype.start = function (imageLink, doSlide, doFrame) {
    if (this.ie && !this.ie7) {
        this.toggleSelects('hide');
    }
    if (this.hideFlash) {
        this.toggleFlash('hide');
    }
    this.isLyteframe = (doFrame ? true : false);
    var pageSize = this.getPageSize();
    var objOverlay = this.doc.getElementById('lbOverlay');
    var objBody = this.doc.getElementsByTagName("body").item(0);
    this.bodyScroll = $(window).scrollTop();
    $('html').css('overflow', "hidden");
    //objBody.style.overflow="hidden";
    objOverlay.style.height = pageSize[1] + "px";
    objOverlay.style.display = '';
    this.appear('lbOverlay', (this.doAnimations ? 0 : this.maxOpacity));
    var anchors = (this.isFrame) ? window.parent.frames[window.name].document.getElementsByTagName('a') : document.getElementsByTagName('a');
    if (this.isLyteframe) {
        this.frameArray = [];
        this.frameNum = 0;
        if ((imageLink.getAttribute('rel') == 'lyteframe')) {
            var rev = imageLink.getAttribute('rev');
            this.frameArray.push(new Array(imageLink.getAttribute('href'), imageLink.getAttribute('title'), (rev == null || rev == '' ? 'width: 400px; height: 400px; scrolling: auto;' : rev)));
        } else {
            if (imageLink.getAttribute('rel').indexOf('lyteframe') != -1) {
                for (var i = 0; i < anchors.length; i++) {
                    var anchor = anchors[i];
                    if (anchor.getAttribute('href') && (anchor.getAttribute('rel') == imageLink.getAttribute('rel'))) {
                        var rev = anchor.getAttribute('rev');
                        this.frameArray.push(new Array(anchor.getAttribute('href'), anchor.getAttribute('data-title'), (rev == null || rev == '' ? 'width: 400px; height: 400px; scrolling: auto;' : rev)));
                    }
                }
                this.frameArray.removeDuplicates();
                while (this.frameArray[this.frameNum][0] != imageLink.getAttribute('href')) {
                    this.frameNum++;
                }
            }
        }
    } else {
        this.imageArray = [];
        this.imageNum = 0;
        this.slideArray = [];
        this.slideNum = 0;
        if ((imageLink.getAttribute('rel') == 'lytebox')) {
            this.imageArray.push(new Array(imageLink.getAttribute('href'), imageLink.getAttribute('data-title'), imageLink));
        } else {
            if (imageLink.getAttribute('rel').indexOf('lytebox') != -1) {
                for (var i = 0; i < anchors.length; i++) {
                    var anchor = anchors[i];
                    if (anchor.getAttribute('href') && (anchor.getAttribute('rel') == imageLink.getAttribute('rel'))) {
                        this.imageArray.push(new Array(anchor.getAttribute('href'), anchor.getAttribute('data-title'), anchor));
                    }
                }
                this.imageArray.removeDuplicates();
                while (this.imageArray[this.imageNum][0] != imageLink.getAttribute('href')) {
                    this.imageNum++;
                }
            }
            if (imageLink.getAttribute('rel').indexOf('lyteshow') != -1) {
                for (var i = 0; i < anchors.length; i++) {
                    var anchor = anchors[i];
                    if (anchor.getAttribute('href') && (anchor.getAttribute('rel') == imageLink.getAttribute('rel'))) {
                        this.slideArray.push(new Array(anchor.getAttribute('href'), anchor.getAttribute('data-title')));
                    }
                }
                this.slideArray.removeDuplicates();
                while (this.slideArray[this.slideNum][0] != imageLink.getAttribute('href')) {
                    this.slideNum++;
                }
            }
        }
    }
    var object = this.doc.getElementById('lbMain');
    var img = $(imageLink).find('img').first();

    var imageCont = this.doc.getElementById('lbImageContainer');
    var imgc_style = imageCont.currentStyle || window.getComputedStyle(imageCont);

    var detc = this.doc.getElementById('lbDetailsContainer');
    var detc_style = detc.currentStyle || window.getComputedStyle(detc);
    var imageTitle = (this.isSlideshow ? this.slideArray[this.slideNum][1] : ((this.imageArray[this.imageNum]) ? this.imageArray[this.imageNum][1] : ''));

    if (imageTitle == '' && !$(imageLink).find('img.inline_zoom').length > 0 && (typeof ($(imageLink).find('img').attr('data-hotspot')) == 'undefined' || $(imageLink).find('img').attr('data-hotspot').length <= 0)) {
        var x = pageSize[2] - (parseInt(parseInt($('#lbMain').width(),10)*parseFloat($('#lbDetailsContainer').width(), 10)/100) + parseInt($('#lbImageContainer').css('margin-left')) + parseInt($('#lbImageContainer').css('margin-right')) + 1 + 60) ;
    } else {
        var x = pageSize[2] - (parseInt(parseInt($('#lbMain').width(),10)*parseFloat($('#lbDetailsContainer').width(), 10)/100)+ parseInt($('#lbImageContainer').css('margin-left')) + parseInt($('#lbImageContainer').css('margin-right')) + 1);
    }
    var y = pageSize[3] - parseInt($('#lbImageContainer').css('margin-top')) - parseInt($('#lbImageContainer').css('margin-bottom'));


    var ratioHeight = 840;
    var ratioWidth = 1190;
    if ($(img).hasClass('first_image')) {
        ratioHeight = 793;
    }

    if (pageSize[2] <= 1280) {
        this.borderSize = 49;
        //$(object).css('left', '21px');
        ratioWidth = 740;
        ratioHeight = 520;
        if ($(img).hasClass('first_image')) {
            ratioHeight = 493;
        }
    }

    if (ratioWidth > x) {
        ratioHeight = Math.round(ratioHeight * (x / ratioWidth));
        ratioWidth = x;
        if (ratioHeight > y) {
            ratioWidth = Math.round(ratioWidth * (y / ratioHeight));
            ratioHeight = y;
        }
    } else if (ratioHeight > y) {
        ratioWidth = Math.round(ratioWidth * (y / ratioHeight));
        ratioHeight = y;
        if (ratioWidth > x) {
            ratioHeight = Math.round(ratioHeight * (x / ratioWidth));
            ratioWidth = x;
        }
    }

    var ratio = $(img).width() / $(img).height();
    var height = 0;
    if (ratio > ratioWidth / ratioHeight) {
        height = Math.ceil(ratioWidth / ratio);
    } else {
        height = ratioHeight;
    }
    $('#lbImageContainer').height(height);
    //object.style.height = (height + 2 * (this.borderSize)) + 'px';
    //object.style.top = (this.getPageScroll() + (pageSize[3] - (height + 2 * (this.borderSize))) / 2) + "px";
    var lbOuterContainer = this.doc.getElementById('lbOuterContainer');
    //console.log(lbOuterContainer.offsetHeight());
//    lbOuterContainer.style.lineHeight = lbOuterContainer.offsetHeight;
    object.style.display = '';
//    if(this.isLyteframe){
//        lbOuterContainer.style.width = '100%';
//    }
    if (!this.outerBorder) {
        lbOuterContainer.style.border = 'none';
        lbOuterContainer.style.border = 'none';
    } else {
        lbOuterContainer.style.borderBottom = '';
        lbOuterContainer.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    }
    this.addInlineEvent(this.doc.getElementById('lbOverlay'), 'onclick', function () {
        myLytebox.end();
        return false;
    });
    this.addInlineEvent(this.doc.getElementById('lbMain'), 'onclick', function (e) {
        var e = e;
        if (!e) {
            //if (window.parent.frames[window.name] && (parent.document.getElementsByTagName('frameset').length <= 0)) {
            //	e = window.parent.window.event;
            //} else {
            e = window.event;
            //}
        }
        //var id = (e.target ? e.target.id : e.srcElement.id);
        //if (id == 'lbMain') { myLytebox.end(); return false; }
    });
    this.addInlineEvent(this.doc.getElementById('lbClose'), 'onclick', function () {
        myLytebox.end();
        return false;
    });
    this.addInlineEvent(this.doc.getElementById('lbPause'), 'onclick', function () {
        myLytebox.togglePlayPause("lbPause", "lbPlay");
        return false;
    });
    this.addInlineEvent(this.doc.getElementById('lbPlay'), 'onclick', function () {
        myLytebox.togglePlayPause("lbPlay", "lbPause");
        return false;
    });
    this.isSlideshow = doSlide;
    this.isPaused = (this.slideNum != 0 ? true : false);
    if (this.isSlideshow && this.showPlayPause && this.isPaused) {
        this.doc.getElementById('lbPlay').style.display = '';
        this.doc.getElementById('lbPause').style.display = 'none';
    }

    $('#lbImageWrapper .pois').remove();
    this.initZoom = false;
    if ($(imageLink).find('img.inline_zoom').length > 0) {
        this.initZoom = true;
    }

    if (this.isLyteframe) {
        this.changeContent(this.frameNum);
    } else {
        if (this.isSlideshow) {
            this.changeContent(this.slideNum);
        } else {
            this.changeContent(this.imageNum);
        }
    }
        this.doc.getElementById('lbMain').style.lineHeight =  this.doc.getElementById('lbMain').offsetHeight + 'px';
};
LyteBox.prototype.changeContent = function (imageNum) {
    if (this.isSlideshow) {
        for (var i = 0; i < this.slideshowIDCount; i++) {
            window.clearTimeout(this.slideshowIDArray[i]);
        }
    }
    this.activeImage = this.activeSlide = this.activeFrame = imageNum;
    if (!this.outerBorder) {
        this.doc.getElementById('lbOuterContainer').style.border = 'none';
        this.doc.getElementById('lbDetailsContainer').style.border = 'none';
    } else {
        this.doc.getElementById('lbOuterContainer').style.borderBottom = '';
        this.doc.getElementById('lbOuterContainer').setAttribute((this.ie ? 'className' : 'class'), this.theme);
    }
    this.doc.getElementById('lbLoadingWrapper').style.display = '';
    this.doc.getElementById('lbImage').style.display = 'none';
    this.doc.getElementById('lbIframe').style.display = 'none';
    this.doc.getElementById('lbPrev').style.display = 'none';
    this.doc.getElementById('lbNext').style.display = 'none';
    this.doc.getElementById('lbIframeContainer').style.display = 'none';
    this.doc.getElementById('lbDetailsContainer').style.visibility = 'hidden';
    //this.doc.getElementById('lbDetailsContainer').style.display = 'none';
    this.doc.getElementById('lbNumberDisplay').style.display = 'none';
    if (this.navType == 2 || this.isLyteframe) {
        object = this.doc.getElementById('lbNavDisplay');
        //object.innerHTML = '<b id="lbSpacer" class="' + this.theme + '"></b><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td width="30%" align="left" style="padding-left:20px;"><span id="lbPrev2_Off" style="display: none;" class="' + this.theme + '">&nbsp;</span><a href="#" id="lbPrev2" class="' + this.theme + '" style="display: none;">'+lprev()+'</a></td><td style="text-align:center;">' + eval(this.activeSlide + 1) + " "+lof()+" " + this.imageArray.length+'</td><td width="30%" align="right" style="padding-right:20px;"><span id="lbNext2_Off" style="display: none;" class="' + this.theme + '">&nbsp;</span><a href="#" id="lbNext2" class="' + this.theme + '" style="display: none;">'+lnext()+'</a></td></tr></table>';
        object.innerHTML = '<b id="lbSpacer" class="' + this.theme + '"></b><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td width="30%" align="left"><span id="lbPrev2_Off" style="display: none;" class="' + this.theme + '">&nbsp;</span><a href="#" id="lbPrev2" class="' + this.theme + '" style="display: none;">' + lprev() + '</a></td><td style="text-align:center;">' + eval(this.activeSlide + 1) + " " + lof() + " " + this.imageArray.length + '</td><td width="30%" align="right"><span id="lbNext2_Off" style="display: none;" class="' + this.theme + '">&nbsp;</span><a href="#" id="lbNext2" class="' + this.theme + '" style="display: none;">' + lnext() + '</a></td></tr></table>';
        //<tr><td colspan="3"><span id="copyspan">'+lcopy()+'</span></td></tr><span id="lbNumberDisplay"></span>
        object.style.display = 'none';
    }

    if (this.isLyteframe) {
        var iframe = myLytebox.doc.getElementById('lbIframe');
        var styles = this.frameArray[this.activeFrame][2];
        var aStyles = styles.split(';');
        for (var i = 0; i < aStyles.length; i++) {
            if (aStyles[i].indexOf('width:') >= 0) {
                var w = aStyles[i].replace('width:', '');
                iframe.width = w.trim();
            } else if (aStyles[i].indexOf('height:') >= 0) {
                var h = aStyles[i].replace('height:', '');
                iframe.height = h.trim();
            } else if (aStyles[i].indexOf('scrolling:') >= 0) {
                var s = aStyles[i].replace('scrolling:', '');
                iframe.scrolling = s.trim();
            } else if (aStyles[i].indexOf('border:') >= 0) {
                // Not implemented yet, as there are cross-platform issues with setting the border (from a GUI standpoint)
                //var b = aStyles[i].replace('border:', '');
                //iframe.style.border = b.trim();
            }
        }
        this.resizeContainer(parseInt(iframe.width), parseInt(iframe.height));
    } else {
        imgPreloader = new Image();
        var contWidth = 1190;
        var contHeight = 840;
        var pagesize = myLytebox.getPageSize();
        // Get image source
        var origImgUrl = (myLytebox.isSlideshow ? myLytebox.slideArray[myLytebox.activeSlide][0] : myLytebox.imageArray[myLytebox.activeImage][0]);
        var imgSrc = origImgUrl;
        //var imgSrcSet = null;
        var contentImageWrraper = (myLytebox.isSlideshow ? myLytebox.slideArray[myLytebox.activeSlide][2] : myLytebox.imageArray[myLytebox.activeImage][2]);
        if ($(contentImageWrraper).find("img")) {
            imgSrc = $(contentImageWrraper).find("img").attr('src');
            //imgSrcSet = $(contentImageWrraper).find("img").attr('srcset');
//            if (pagesize[2] > 1280 && imgSrc) {
            if (imgSrc) {
                // Src: files/images/__cache/content/Daimler/applications/05_presentations/PPT_Abb_00.png.jpg
                //      protected-file/10474/content?policy=$2y$08$FRYG80ONjB2oS.eTP1Ay4eJmWZzlynBtGma9XwW3KcF8TVeS/EKVO
                if (imgSrc.indexOf('protected-file/') >= 0) {
                    strSearch = '/content';
                    strReplace = '/zoom';
                } else {
                    strSearch = '/__cache/content';
                    strReplace = '/__cache/zoom';
                }
                imgSrc = imgSrc.split(strSearch).join(strReplace);
                //imgSrcSet = imgSrcSet.split(strSearch).join(strReplace);
            }
        }

        imgPreloader.onload = function () {
            var imageTitle = (myLytebox.isSlideshow ? myLytebox.slideArray[myLytebox.activeSlide][1] : myLytebox.imageArray[myLytebox.activeImage][1]);
            var imageLink = (myLytebox.isSlideshow ? myLytebox.slideArray[myLytebox.activeSlide][2] : myLytebox.imageArray[myLytebox.activeImage][2]);

            var imageWidth = imgPreloader.width;
            var imageHeight = imgPreloader.height;
            if (myLytebox.autoResize) {
                var imageCont = myLytebox.doc.getElementById('lbImageContainer');
                //imageCont.style.marginLeft = myLytebox.borderSize + 'px';
                var imgc_style = imageCont.currentStyle || window.getComputedStyle(imageCont);

                var detc = myLytebox.doc.getElementById('lbDetailsContainer');
                var detc_style = detc.currentStyle || window.getComputedStyle(detc);

                var x = ($('#lbMain').width() - 120) * 69.56/100;
//                if (imageTitle == '' && !myLytebox.initZoom && (typeof ($(imageLink).find('img').attr('data-hotspot')) == 'undefined' || $(imageLink).find('img').attr('data-hotspot').length <= 0)) {
//                    //var x = parseInt($('#lbOuterContainer').width(), 10) - (1 + parseInt($('#lbImageContainer').css('margin-left')) + parseInt($('#lbImageContainer').css('margin-right')));
//                } else {
//                    var x = ($('#lbMain').width() * 69.56/100);
//                    //var x = parseInt($('#lbOuterContainer').width(), 10) - 1 - parseInt($('#lbImageContainer').css('margin-left')) - parseInt($('#lbImageContainer').css('margin-right'));
//                }
               //var y = parseInt($('#lbOuterContainer').height(), 10) - parseInt($('#lbImageContainer').css('margin-top')) - parseInt($('#lbImageContainer').css('margin-bottom'));
               var y = $('#lbMain').height() - 120;
               if ($(myLytebox.imageArray[myLytebox.activeImage][2]).closest('.galCropper').hasClass('first_image') || $(myLytebox.imageArray[myLytebox.activeImage][2]).closest('.image_wrapper').hasClass('first_image')) {
                    contHeight = 793;
                }

                if (pagesize[2] <= 1280) {
                    myLytebox.borderSize = 49;
                    contWidth = 740;
                    contHeight = 520;
                    if ($(myLytebox.imageArray[myLytebox.activeImage][2]).closest('.galCropper').hasClass('first_image') || $(myLytebox.imageArray[myLytebox.activeImage][2]).closest('.image_wrapper').hasClass('first_image')) {
                        contHeight = 493;
                    }
                }
                if (contWidth > x) {
                    contHeight = Math.round(contHeight * (x / contWidth));
                    contWidth = x;
                    if (contHeight > y) {
                        contWidth = Math.round(contWidth * (y / contHeight));
                        contHeight = y;
                    }
                } else if (contHeight > y) {
                    contWidth = Math.round(contWidth * (y / contHeight));
                    contHeight = y;
                    if (contWidth > x) {
                        contHeight = Math.round(contHeight * (x / contWidth));
                        contWidth = x;
                    }
                }

                if (imageWidth < contWidth && imageHeight < contHeight) {
                } else if (imageWidth / imageHeight > contWidth / contHeight) {
                    oldImageWidth = imageWidth;
                    imageWidth = contWidth;
                    imageHeight = parseInt((imageHeight / oldImageWidth) * imageWidth);
                } else {
                    oldImageHeight = imageHeight;
                    imageHeight = contHeight;
                    imageWidth = parseInt((imageWidth / oldImageHeight) * imageHeight);
                }
            }

            if (contHeight > imageHeight) {
                contHeight = imageHeight;
            }

            if (imageTitle == '' && !myLytebox.initZoom && (typeof ($(imageLink).find('img').attr('data-hotspot')) == 'undefined' || $(imageLink).find('img').attr('data-hotspot').length <= 0)) {
//                $('#lbDetailsContainer').css('width', '0px');
                contWidth = imageWidth;
                //	imageCont.style.marginLeft = (myLytebox.borderSize + parseInt(detc_style.width,10)/ 2 - 1)+'px';
            } else {
//                $('#lbDetailsContainer').css('width', '');
                //	imageCont.style.marginLeft = myLytebox.borderSize+'px';
            }

            imageCont.style.width = imageWidth + 'px';
//            if (myLytebox.imageArray.length > 1) {
//                $('#lbMain').css('top', (myLytebox.getPageScroll() + (pagesize[3] - (contHeight + 2 * myLytebox.borderSize)) / 2) + "px");
//            }
            imageCont.style.height = imageHeight + 'px';
            imageCont.style.lineHeight = imageHeight + 'px';

            //$('#lbMain').height(contHeight + 2 * (myLytebox.borderSize));
            //

            var lbImage = myLytebox.doc.getElementById('lbImage');
            lbImage.src = imgSrc;
//            if(imgSrcSet){
//                lbImage.srcset = imgSrcSet;
//            }

            $('#lbOuterContainer').height(imageHeight);
            $('#lbDetailsContainer').height(imageHeight);
            $('#lbDetailsContainer').css('line-height', imageHeight + 'px');
            $('#lbOuterContainer').width(imageWidth);
            lbImage.width = imageWidth;
            lbImage.height = imageHeight;
                        myLytebox.resizeContainer(contWidth, contHeight);
            if (myLytebox.initZoom) {
                $('#lbImageWrapper').trigger('zoom.destroy');
                $('#lbLoadingWrapper').css('z-index', 100).show();
                $('#lbImageWrapper').zoom({url: origImgUrl, callback: function () {
                    $('#lbLoadingWrapper').css('z-index', 11).hide();
                    }});
            } else {
                $('#lbImageWrapper').trigger('zoom.destroy');
            }

        };
        imgPreloader.src = imgSrc;
    }
};
LyteBox.prototype.resizeContainer = function (imgWidth, imgHeight) {
    this.wCur = this.doc.getElementById('lbOuterContainer').offsetWidth;
    this.hCur = this.doc.getElementById('lbOuterContainer').offsetHeight;
    //this.doc.getElementById('lbOuterContainer').style.lineHeight = this.hCur + 'px';
    this.xScale = ((imgWidth + (this.borderSize * 2)) / this.wCur) * 100;
    this.yScale = ((imgHeight + (this.borderSize * 2)) / this.hCur) * 100;

    var imageTitle = (this.isSlideshow ? this.slideArray[this.activeSlide][1] : ((this.imageArray[this.activeImage]) ? this.imageArray[this.activeImage][1] : ''));
    var imageLink = (this.isSlideshow ? this.slideArray[this.activeSlide][2] : ((this.imageArray[this.activeImage]) ? this.imageArray[this.activeImage][2] : ''));
    if (imageTitle == '' && !this.initZoom) {
        var wDiff = (this.wCur - this.borderSize * 2) - imgWidth - 60;
    } else {
        var wDiff = (this.wCur - this.borderSize * 2) - imgWidth;
    }
    var hDiff = (this.hCur - this.borderSize * 2) - imgHeight;
//    if (!(hDiff == 0)) {
//        this.hDone = false;
//        this.maxHeight = imgHeight + this.borderSize * 2;
//        this.resizeH('lbOuterContainer', this.hCur, imgHeight + this.borderSize * 2, this.getPixelRate(this.hCur, imgHeight));
//    } else {
        this.hDone = true;
//    }
//    if (!(wDiff == 0)) {
//        this.wDone = false;
//
//        if (imageTitle == '' && !this.initZoom && (typeof ($(imageLink).find('img').attr('data-hotspot')) == 'undefined' || $(imageLink).find('img').attr('data-hotspot').length <= 0)) {
//            this.maxWidth = imgWidth + this.borderSize * 2 + 60;
//            this.resizeW('lbOuterContainer', this.wCur, imgWidth + this.borderSize * 2 + 60, this.getPixelRate(this.wCur, imgWidth + 60));
//        } else {
//            this.maxWidth = imgWidth + this.borderSize * 2;
//            this.resizeW('lbOuterContainer', this.wCur, imgWidth + this.borderSize * 2, this.getPixelRate(this.wCur, imgWidth));
//        }
//    } else {
        this.wDone = true;
//    }
    if ((hDiff == 0) && (wDiff == 0)) {
        if (this.ie) {
            this.pause(250);
        } else {
            this.pause(100);
        }
    }
    //this.doc.getElementById('lbPrev').style.height = imgHeight + "px";
    //this.doc.getElementById('lbNext').style.height = imgHeight + "px";
    //this.doc.getElementById('lbDetailsContainer').style.width = (imgWidth + (this.borderSize * 2) + (this.ie && this.doc.compatMode == "BackCompat" && this.outerBorder ? 2 : 0)) + "px";
    this.showContent();
};
LyteBox.prototype.showContent = function () {
    if (this.wDone && this.hDone) {
        for (var i = 0; i < this.showContentTimerCount; i++) {
            window.clearTimeout(this.showContentTimerArray[i]);
        }
        if (this.outerBorder) {
            this.doc.getElementById('lbOuterContainer').style.borderBottom = 'none';
        }
        if (!myLytebox.initZoom && !this.isLyteframe) {
            this.doc.getElementById('lbLoadingWrapper').style.display = 'none';
        }
        if (this.isLyteframe) {
            this.doc.getElementById('lbIframe').style.display = '';
            this.appear('lbIframe', (this.doAnimations ? 0 : 100));
        } else {
            this.doc.getElementById('lbImage').style.display = '';
            this.appear('lbImage', (this.doAnimations ? 0 : 100));
            this.preloadNeighborImages();
        }
        if (this.isSlideshow) {
            if (this.activeSlide == (this.slideArray.length - 1)) {
                if (this.autoEnd) {
                    this.slideshowIDArray[this.slideshowIDCount++] = setTimeout("myLytebox.end('slideshow')", this.slideInterval);
                }
            } else {
                if (!this.isPaused) {
                    this.slideshowIDArray[this.slideshowIDCount++] = setTimeout("myLytebox.changeContent(" + (this.activeSlide + 1) + ")", this.slideInterval);
                }
            }
            this.doc.getElementById('lbHoverNav').style.display = (this.showNavigation && this.navType == 1 ? '' : 'none');
            this.doc.getElementById('lbClose').style.display = (this.showClose ? '' : 'none');
            this.doc.getElementById('lbDetails').style.display = (this.showDetails ? '' : 'none');
            this.doc.getElementById('lbPause').style.display = (this.showPlayPause && !this.isPaused ? '' : 'none');
            this.doc.getElementById('lbPlay').style.display = (this.showPlayPause && !this.isPaused ? 'none' : '');
            this.doc.getElementById('lbNavDisplay').style.display = (this.showNavigation && this.navType == 2 ? '' : 'none');
        } else {
            this.doc.getElementById('lbHoverNav').style.display = (this.navType == 1 && !this.isLyteframe ? '' : 'none');
            if ((this.navType == 2 && !this.isLyteframe && this.imageArray.length > 1) || (this.frameArray.length > 1 && this.isLyteframe)) {
                this.doc.getElementById('lbNavDisplay').style.display = '';
            } else {
                this.doc.getElementById('lbNavDisplay').style.display = 'none';
            }
            this.doc.getElementById('lbClose').style.display = '';
            this.doc.getElementById('lbDetails').style.display = '';
            this.doc.getElementById('lbPause').style.display = 'none';
            this.doc.getElementById('lbPlay').style.display = 'none';

            if (!this.isLyteframe) {
                var imageLink = myLytebox.imageArray[myLytebox.activeImage][2];

                if (typeof ($(imageLink).find('img').attr('data-hotspot')) != 'undefined' && $(imageLink).find('img').attr('data-hotspot').length > 0) {
                    var hst_json = JSON.parse($(imageLink).find('img').attr('data-hotspot'));
                    var html = '<div id="LE-pois-0" class="pois">';
                    for (var i = 0; i < hst_json.points.length; i++) {
                        html += '<div class="poi ' + hst_json.points[i].pos + '" style="left: ' + hst_json.points[i].left + '; top: ' + hst_json.points[i].top + ';">' + (i + 1) + '</div>';
                    }
                    html += '</div>';
                    $('#lbImageWrapper').append(html);

                    var caption_html = '<h1>' + js_localize['hotspot.default_text'] + '</h1>' + hst_json.description;

                    $('#lbCaption').html(caption_html);
//                    if ($('#lbCaption').outerHeight() + (2 * this.borderSize) > $('#lbMain').height()) {
//                        //$('#lbMain').height($('#lbCaption').outerHeight() + (2*this.borderSize));
//                    }

                    myLytebox.imageArray[myLytebox.activeImage][1] = caption_html;
                }
                 $('#lbDetailsContainer').removeClass('zero-width');
                if((!this.initZoom  && this.imageArray[this.activeImage][1] == '' && this.imageArray.length == 1) || this.isLyteframe){
                    $('#lbOuterContainer').addClass('full-width');
                    $('#lbDetailsContainer').addClass('zero-width');
                }else if(this.imageArray.length > 1){
                    $('#lbOuterContainer').addClass('max-width');
                }
            }
        }
        this.doc.getElementById('lbWrapper').style.display = (this.isLyteframe ? 'none' : '');
        this.doc.getElementById('lbBottomNav').style.display = (this.isLyteframe ? 'none' : '');
        this.doc.getElementById('lbIframeContainer').style.display = (this.isLyteframe ? '' : 'none');
        if(this.isLyteframe){
            this.doc.getElementById('lbOuterContainer').style.width = '100%';
            this.doc.getElementById('lbOuterContainer').style.height ='100%';
        }
        try {
            if (this.isLyteframe) {
                var iframeEl = this.doc.getElementById('lbIframe');
                iframeEl.onload = function () {
                    $('#lbLoadingWrapper').hide();
                    console.log('iframe loaded!');
                };

                iframeEl.src = this.frameArray[this.activeFrame][0];

            }
        } catch (e) {
        }
    } else {

        this.showContentTimerArray[this.showContentTimerCount++] = setTimeout("myLytebox.showContent()", 200);
    }
    //$('#lbImageWrapper').trigger('showContent');
};
LyteBox.prototype.updateDetails = function () {
    var object = this.doc.getElementById('lbCaption');
    var sTitle = (this.isSlideshow ? this.slideArray[this.activeSlide][1] : (this.isLyteframe ? this.frameArray[this.activeFrame][1] : this.imageArray[this.activeImage][1]));
    object.style.display = '';
    var nbps = "<p>" + Array(90).join("&nbsp;") + "</p>";
    object.innerHTML = (sTitle == null ? '' : sTitle) + nbps;
    $(object).removeClass('bottom');
     $('#lbDetailsContainer').removeClass('bottom');

    if(this.imageArray.length > 1){
        $(object).addClass('bottom');
        $('#lbDetailsContainer').addClass('bottom');
    }
    if (this.initZoom) {
        $(object).addClass('inline_zoom');
        object.innerHTML = object.innerHTML + '<p id="inline_zoom_addition">' + js_localize['inline_zoom.zoom_text'] + '</p>';
        //$('#inline_zoom_addition').css('left', ($('#lbOuterContainer').width() + parseInt($('#lbDetailsData').css('padding-left'), 10)) + 'px');
    } else {
        $(object).removeClass('inline_zoom');
    }

    if ($(object).outerHeight() + (2 * this.borderSize) > $('#lbMain').height()) {
        //$('#lbMain').height($(object).outerHeight() + (2*this.borderSize));
    }
    //if(object.innerHTML.length > 0){
    object.style.display = 'block';
    this.doc.getElementById('lbNavDisplay').style.borderTopWidth = '1px';
    //}else{
    //     object.style.display =  'none';
    //    this.doc.getElementById('lbNavDisplay').style.borderTopWidth = '0px';
    // }


    this.updateNav();
    var imageCounter =  this.doc.getElementById('imageCounter');
    imageCounter.style.display = 'none';
    this.doc.getElementById('lbDetailsContainer').style.visibility = '';
    this.doc.getElementById('lbDetailsContainer').style.display = '';
    object = this.doc.getElementById('lbNumberDisplay');
    if (this.isSlideshow && this.slideArray.length > 1) {
        object.style.display = '';
        var nav_html = '';
        for (var i = 0; i < this.slideArray.length; i++) {
            nav_html += '<span class="slider_block ' + (i == this.activeSlide ? 'active' : '') + '" onClick="myLytebox.changeContent(' + i + ');"><span>';
        }
        object.innerHTML = "" + eval(this.activeSlide + 1) + " " + lof() + " " + this.slideArray.length;
        this.doc.getElementById('lbNavDisplay').style.display = (this.navType == 2 && this.showNavigation ? '' : 'none');
    } else if (this.imageArray.length > 1 && !this.isLyteframe) {
        object.style.display = 'none';
        imageCounter.style.display = '';
        //if(typeof(lof) == "undefined"){
        var nav_html = '<span class="curentCount">' + (this.activeImage + 1) +'</span>' + '<span class="count"> / '+this.imageArray.length+ '</span>';
//        for (var i = 0; i < this.imageArray.length; i++) {
//            nav_html += '<span class="slider_block ' + (i == this.activeImage ? 'active' : '') + '" onClick="myLytebox.changeContent(' + i + ');"></span>';
//        }
        imageCounter.innerHTML = nav_html;
//        object.style.marginLeft = '-' + (object.offsetWidth - 20) / 2 + 'px';
        this.doc.getElementById('lbNavDisplay').style.display = (this.navType == 2 ? '' : 'none');
        //}
    } else if (this.frameArray.length > 1 && this.isLyteframe) {
        object.style.display = '';
        object.innerHTML = "Page " + eval(this.activeFrame + 1) + " " + lof() + " " + this.frameArray.length;
        this.doc.getElementById('lbNavDisplay').style.display = '';
    } else {
        this.doc.getElementById('lbNavDisplay').style.display = 'none';
    }
    //alert(object.innerHTML.length);
    // show / hide the details container
    //if(object.innerHTML.length > 1){ // && this.doc.getElementById('lbNavDisplay').style.display != 'none'){
    //if(object.innerHTML.length > 1 || this.doc.getElementById('lbCaption').style.display != 'none'){
    if (this.doc.getElementById('lbNavDisplay').style.display != 'none' || this.doc.getElementById('lbCaption').style.display != 'none') {
        if (!this.isLyteframe) {
            this.appear('lbDetailsContainer', (this.doAnimations ? 0 : 100));
        } else {
            this.doc.getElementById('lbDetailsContainer').style.display = 'none';
        }
    } else {
        this.doc.getElementById('lbDetailsContainer').style.display = 'none';
    }
};
LyteBox.prototype.updateNav = function () {
    if (this.isSlideshow) {
        if (this.activeSlide != 0) {
            var object = (this.navType == 2 ? this.doc.getElementById('lbPrev2') : this.doc.getElementById('lbPrev'));
            object.style.display = '';
            this.addInlineEvent(object, 'onclick', function () {
                if (myLytebox.pauseOnPrevClick) {
                    myLytebox.togglePlayPause("lbPause", "lbPlay");
                }
                myLytebox.changeContent(myLytebox.activeSlide - 1);
                return false;
            });
        } else {
            if (this.navType == 2) {
                this.doc.getElementById('lbPrev2_Off').style.display = '';
            }
        }
        if (this.activeSlide != (this.slideArray.length - 1)) {
            var object = (this.navType == 2 ? this.doc.getElementById('lbNext2') : this.doc.getElementById('lbNext'));
            object.style.display = '';
            this.addInlineEvent(object, 'onclick', function () {
                if (myLytebox.pauseOnNextClick) {
                    myLytebox.togglePlayPause("lbPause", "lbPlay");
                }
                myLytebox.changeContent(myLytebox.activeSlide + 1);
                return false;
            });
        } else {
            if (this.navType == 2) {
                this.doc.getElementById('lbNext2_Off').style.display = '';
            }
        }
    } else if (this.isLyteframe) {
        if (this.activeFrame != 0) {
            var object = this.doc.getElementById('lbPrev2');
            object.style.display = '';
            this.addInlineEvent(object, 'onclick', function () {
                myLytebox.changeContent(myLytebox.activeFrame - 1);
                return false;
            });
        } else {
            this.doc.getElementById('lbPrev2_Off').style.display = '';
        }
        if (this.activeFrame != (this.frameArray.length - 1)) {
            var object = this.doc.getElementById('lbNext2');
            object.style.display = '';
            this.addInlineEvent(object, 'onclick', function () {
                myLytebox.changeContent(myLytebox.activeFrame + 1);
                return false;
            });
        } else {
            this.doc.getElementById('lbNext2_Off').style.display = '';
        }
    } else {
        if (this.activeImage != 0) {
            var object = (this.navType == 2 ? this.doc.getElementById('lbPrev2') : this.doc.getElementById('lbPrev'));
            object.style.display = '';
            this.addInlineEvent(object, 'onclick', function () {
                myLytebox.changeContent(myLytebox.activeImage - 1);
                return false;
            });
        } else {
            if (this.navType == 2) {
                this.doc.getElementById('lbPrev2_Off').style.display = '';
            }
        }
        if (this.activeImage != (this.imageArray.length - 1)) {
            var object = (this.navType == 2 ? this.doc.getElementById('lbNext2') : this.doc.getElementById('lbNext'));
            object.style.display = '';
            this.addInlineEvent(object, 'onclick', function () {
                myLytebox.changeContent(myLytebox.activeImage + 1);
                return false;
            });
        } else {
            if (this.navType == 2) {
                this.doc.getElementById('lbNext2_Off').style.display = '';
            }
        }
    }
    this.enableKeyboardNav();
};
LyteBox.prototype.enableKeyboardNav = function () {
    document.onkeydown = this.keyboardAction;
};
LyteBox.prototype.disableKeyboardNav = function () {
    document.onkeydown = '';
};
LyteBox.prototype.keyboardAction = function (e) {
    var keycode = key = escape = null;
    keycode = (e == null) ? event.keyCode : e.which;
    key = String.fromCharCode(keycode).toLowerCase();
    escape = (e == null) ? 27 : e.DOM_VK_ESCAPE;
    if ((key == 'x') || (key == 'c') || (keycode == escape)) {
        myLytebox.end();
    } else if ((key == 'p') || (keycode == 37)) {
        if (myLytebox.isSlideshow) {
            if (myLytebox.activeSlide != 0) {
                myLytebox.disableKeyboardNav();
                myLytebox.changeContent(myLytebox.activeSlide - 1);
            }
        } else if (myLytebox.isLyteframe) {
            if (myLytebox.activeFrame != 0) {
                myLytebox.disableKeyboardNav();
                myLytebox.changeContent(myLytebox.activeFrame - 1);
            }
        } else {
            if (myLytebox.activeImage != 0) {
                myLytebox.disableKeyboardNav();
                myLytebox.changeContent(myLytebox.activeImage - 1);
            }
        }
    } else if ((key == 'n') || (keycode == 39)) {
        if (myLytebox.isSlideshow) {
            if (myLytebox.activeSlide != (myLytebox.slideArray.length - 1)) {
                myLytebox.disableKeyboardNav();
                myLytebox.changeContent(myLytebox.activeSlide + 1);
            }
        } else if (myLytebox.isLyteframe) {
            if (myLytebox.activeFrame != (myLytebox.frameArray.length - 1)) {
                myLytebox.disableKeyboardNav();
                myLytebox.changeContent(myLytebox.activeFrame + 1);
            }
        } else {
            if (myLytebox.activeImage != (myLytebox.imageArray.length - 1)) {
                myLytebox.disableKeyboardNav();
                myLytebox.changeContent(myLytebox.activeImage + 1);
            }
        }
    }
};
LyteBox.prototype.preloadNeighborImages = function () {
    if (this.isSlideshow) {
        if ((this.slideArray.length - 1) > this.activeSlide) {
            preloadNextImage = new Image();
            preloadNextImage.src = this.slideArray[this.activeSlide + 1][0];
        }
        if (this.activeSlide > 0) {
            preloadPrevImage = new Image();
            preloadPrevImage.src = this.slideArray[this.activeSlide - 1][0];
        }
    } else {
        if ((this.imageArray.length - 1) > this.activeImage) {
            preloadNextImage = new Image();
            preloadNextImage.src = this.imageArray[this.activeImage + 1][0];
        }
        if (this.activeImage > 0) {
            preloadPrevImage = new Image();
            preloadPrevImage.src = this.imageArray[this.activeImage - 1][0];
        }
    }
};
LyteBox.prototype.togglePlayPause = function (hideID, showID) {
    if (this.isSlideshow && hideID == "lbPause") {
        for (var i = 0; i < this.slideshowIDCount; i++) {
            window.clearTimeout(this.slideshowIDArray[i]);
        }
    }
    this.doc.getElementById(hideID).style.display = 'none';
    this.doc.getElementById(showID).style.display = '';
    if (hideID == "lbPlay") {
        this.isPaused = false;
        if (this.activeSlide == (this.slideArray.length - 1)) {
            this.end();
        } else {
            this.changeContent(this.activeSlide + 1);
        }
    } else {
        this.isPaused = true;
    }
};
LyteBox.prototype.end = function (caller) {
    $('html').css('overflow', "visible");
    if (this.bodyScroll) {
        $(window).scrollTop(this.bodyScroll);
        this.bodyScroll = null;
    }
    var closeClick = (caller == 'slideshow' ? false : true);
    if (this.isSlideshow && this.isPaused && !closeClick) {
        return;
    }
    this.disableKeyboardNav();
    this.doc.getElementById('lbMain').style.display = 'none';
    this.fade('lbOverlay', (this.doAnimations ? this.maxOpacity : 0));
    this.toggleSelects('visible');
    if (this.hideFlash) {
        this.toggleFlash('visible');
    }
    if (this.isSlideshow) {
        for (var i = 0; i < this.slideshowIDCount; i++) {
            window.clearTimeout(this.slideshowIDArray[i]);
        }
    }
    if (this.isLyteframe) {
        $('#lbIframe').attr('src', '');
        this.initialize();
    }

    $('#lbImageWrapper').trigger('zoom.destroy');
};
LyteBox.prototype.checkFrame = function () {
    //if (window.parent.frames[window.name] && (parent.document.getElementsByTagName('frameset').length <= 0)) {
    //	this.isFrame = true;
    //	this.lytebox = "window.parent." + window.name + ".myLytebox";
    //	this.doc = parent.document;
    //} else {
    this.isFrame = false;
    this.lytebox = "myLytebox";
    this.doc = document;
    //}
};
LyteBox.prototype.getPixelRate = function (cur, img) {
    var diff = (img > cur) ? img - cur : cur - img;
    if (diff >= 0 && diff <= 100) {
        return 10;
    }
    if (diff > 100 && diff <= 200) {
        return 15;
    }
    if (diff > 200 && diff <= 300) {
        return 20;
    }
    if (diff > 300 && diff <= 400) {
        return 25;
    }
    if (diff > 400 && diff <= 500) {
        return 30;
    }
    if (diff > 500 && diff <= 600) {
        return 35;
    }
    if (diff > 600 && diff <= 700) {
        return 40;
    }
    if (diff > 700) {
        return 45;
    }
};
LyteBox.prototype.appear = function (id, opacity) {
//    if (opacity > 0 && (id == 'lbDetailsContainer' || id == 'lbIframe')) {
//        var main = this.doc.getElementById('lbMain');
//        var pagesize = myLytebox.getPageSize();
////        main.style.left = Math.floor((((pagesize[2] - main.offsetWidth) / 2) + $('html').scrollLeft())) + "px";
////        main.style.top = (this.getPageScroll() + (pagesize[3] - main.offsetHeight) / 2) + "px";
//    }
    var object = this.doc.getElementById(id).style;
    object.opacity = (opacity / 100);
    object.MozOpacity = (opacity / 100);
    object.KhtmlOpacity = (opacity / 100);
    object.filter = "alpha(opacity=" + (opacity + 10) + ")";
    if (opacity == 100 && (id == 'lbImage' || id == 'lbIframe')) {
        try {
            object.removeAttribute("filter");
        } catch (e) {
        }	/* Fix added for IE Alpha Opacity Filter bug. */
        this.updateDetails();
    } else if (opacity >= this.maxOpacity && id == 'lbOverlay') {
        for (var i = 0; i < this.overlayTimerCount; i++) {
            window.clearTimeout(this.overlayTimerArray[i]);
        }
        return;
    } else if (opacity >= 100 && id == 'lbDetailsContainer') {
        try {
            object.removeAttribute("filter");
        } catch (e) {
        }	/* Fix added for IE Alpha Opacity Filter bug. */
        for (var i = 0; i < this.imageTimerCount; i++) {
            window.clearTimeout(this.imageTimerArray[i]);
        }
        this.doc.getElementById('lbOverlay').style.height = this.getPageSize()[1] + "px";
    } else {
        if (id == 'lbOverlay') {
            this.overlayTimerArray[this.overlayTimerCount++] = setTimeout("myLytebox.appear('" + id + "', " + (opacity + 20) + ")", 1);
        } else {
            this.imageTimerArray[this.imageTimerCount++] = setTimeout("myLytebox.appear('" + id + "', " + (opacity + 10) + ")", 1);
        }
    }
};
LyteBox.prototype.fade = function (id, opacity) {
    var object = this.doc.getElementById(id).style;
    object.opacity = (opacity / 100);
    object.MozOpacity = (opacity / 100);
    object.KhtmlOpacity = (opacity / 100);
    object.filter = "alpha(opacity=" + opacity + ")";
    if (opacity <= 0) {
        try {
            object.display = 'none';
        } catch (err) {
        }
    } else if (id == 'lbOverlay') {
        this.overlayTimerArray[this.overlayTimerCount++] = setTimeout("myLytebox.fade('" + id + "', " + (opacity - 20) + ")", 1);
    } else {
        this.timerIDArray[this.timerIDCount++] = setTimeout("myLytebox.fade('" + id + "', " + (opacity - 10) + ")", 1);
    }
};
LyteBox.prototype.resizeW = function (id, curW, maxW, pixelrate, speed) {
    if (this.maxWidth == maxW) {
        if (!this.hDone) {
            this.resizeWTimerArray[this.resizeWTimerCount++] = setTimeout("myLytebox.resizeW('" + id + "', " + curW + ", " + maxW + ", " + pixelrate + ")", 100);
            return;
        }
        var object = this.doc.getElementById(id);
        var timer = speed ? speed : (this.resizeDuration / 2);
        var newW = (this.doAnimations ? curW : maxW);
        object.style.width = (newW) + "px";
//        if (id == 'lbOuterContainer') {
//            var main = this.doc.getElementById('lbMain');
//            var pagesize = myLytebox.getPageSize();
//            main.style.left = (((pagesize[2] - main.offsetWidth) / 2) + $('html').scrollLeft()) + "px";
//        }
        if (newW < maxW) {
            newW += (newW + pixelrate >= maxW) ? (maxW - newW) : pixelrate;
        } else if (newW > maxW) {
            newW -= (newW - pixelrate <= maxW) ? (newW - maxW) : pixelrate;
        }
        this.resizeWTimerArray[this.resizeWTimerCount++] = setTimeout("myLytebox.resizeW('" + id + "', " + newW + ", " + maxW + ", " + pixelrate + ", " + (timer + 0.02) + ")", timer + 0.02);
        if (parseInt(object.style.width) == maxW) {
            this.wDone = true;
            for (var i = 0; i < this.resizeWTimerCount; i++) {
                window.clearTimeout(this.resizeWTimerArray[i]);
            }
        }
    }
};
LyteBox.prototype.resizeH = function (id, curH, maxH, pixelrate, speed) {
    if (this.maxHeight == maxH) {
        var timer = speed ? speed : (this.resizeDuration / 2);
        var object = this.doc.getElementById(id);
        var newH = (this.doAnimations ? curH : maxH);
        object.style.height = (newH) + "px";
        if (newH < maxH) {
            newH += (newH + pixelrate >= maxH) ? (maxH - newH) : pixelrate;
        } else if (newH > maxH) {
            newH -= (newH - pixelrate <= maxH) ? (newH - maxH) : pixelrate;
        }
        this.resizeHTimerArray[this.resizeHTimerCount++] = setTimeout("myLytebox.resizeH('" + id + "', " + newH + ", " + maxH + ", " + pixelrate + ", " + (timer + .02) + ")", timer + .02);
        if (parseInt(object.style.height) == maxH) {
            this.hDone = true;
            for (var i = 0; i < this.resizeHTimerCount; i++) {
                window.clearTimeout(this.resizeHTimerArray[i]);
            }
        }
    }
};
LyteBox.prototype.getPageScroll = function () {
    if (self.pageYOffset) {
        return this.isFrame ? parent.pageYOffset : self.pageYOffset;
    } else if (this.doc.documentElement && this.doc.documentElement.scrollTop) {
        return this.doc.documentElement.scrollTop;
    } else if (document.body) {
        return this.doc.body.scrollTop;
    }
};
LyteBox.prototype.getPageSize = function () {
    var xScroll, yScroll, windowWidth, windowHeight;
    if (window.innerHeight && window.scrollMaxY) {
        xScroll = this.doc.scrollWidth;
        yScroll = (this.isFrame ? parent.innerHeight : self.innerHeight) + (this.isFrame ? parent.scrollMaxY : self.scrollMaxY);
    } else if (this.doc.body.scrollHeight > this.doc.body.offsetHeight) {
        xScroll = this.doc.body.scrollWidth;
        yScroll = this.doc.body.scrollHeight;
    } else {
        xScroll = this.doc.getElementsByTagName("html").item(0).offsetWidth;
        yScroll = this.doc.getElementsByTagName("html").item(0).offsetHeight;
        xScroll = (xScroll < this.doc.body.offsetWidth) ? this.doc.body.offsetWidth : xScroll;
        yScroll = (yScroll < this.doc.body.offsetHeight) ? this.doc.body.offsetHeight : yScroll;
    }
    if (self.innerHeight) {
        windowWidth = (this.isFrame) ? parent.innerWidth : $('html').innerWidth();
        windowHeight = (this.isFrame) ? parent.innerHeight : self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) {
        windowWidth = this.doc.documentElement.clientWidth;
        windowHeight = this.doc.documentElement.clientHeight;
    } else if (document.body) {
        windowWidth = this.doc.getElementsByTagName("html").item(0).clientWidth;
        windowHeight = this.doc.getElementsByTagName("html").item(0).clientHeight;
        windowWidth = (windowWidth == 0) ? this.doc.body.clientWidth : windowWidth;
        windowHeight = (windowHeight == 0) ? this.doc.body.clientHeight : windowHeight;
    }
    var pageHeight = (yScroll < windowHeight) ? windowHeight : yScroll;
    var pageWidth = (xScroll < windowWidth) ? windowWidth : xScroll;
    return new Array(pageWidth, pageHeight, windowWidth, windowHeight);
};
LyteBox.prototype.toggleFlash = function (state) {
    var objects = this.doc.getElementsByTagName("object");
    for (var i = 0; i < objects.length; i++) {
        objects[i].style.visibility = (state == "hide") ? 'hidden' : 'visible';
    }
    var embeds = this.doc.getElementsByTagName("embed");
    for (var i = 0; i < embeds.length; i++) {
        embeds[i].style.visibility = (state == "hide") ? 'hidden' : 'visible';
    }
    if (this.isFrame) {
        for (var i = 0; i < parent.frames.length; i++) {
            try {
                objects = parent.frames[i].window.document.getElementsByTagName("object");
                for (var j = 0; j < objects.length; j++) {
                    objects[j].style.visibility = (state == "hide") ? 'hidden' : 'visible';
                }
            } catch (e) {
            }
            try {
                embeds = parent.frames[i].window.document.getElementsByTagName("embed");
                for (var j = 0; j < embeds.length; j++) {
                    embeds[j].style.visibility = (state == "hide") ? 'hidden' : 'visible';
                }
            } catch (e) {
            }
        }
    }
};
LyteBox.prototype.toggleSelects = function (state) {
    var selects = this.doc.getElementsByTagName("select");
    for (var i = 0; i < selects.length; i++) {
        selects[i].style.visibility = (state == "hide") ? 'hidden' : 'visible';
    }
    if (this.isFrame) {
        for (var i = 0; i < parent.frames.length; i++) {
            try {
                selects = parent.frames[i].window.document.getElementsByTagName("select");
                for (var j = 0; j < selects.length; j++) {
                    selects[j].style.visibility = (state == "hide") ? 'hidden' : 'visible';
                }
            } catch (e) {
            }
        }
    }
};
LyteBox.prototype.pause = function (numberMillis) {
    var now = new Date();
    var exitTime = now.getTime() + numberMillis;
    while (true) {
        now = new Date();
        if (now.getTime() > exitTime) {
            return;
        }
    }
};
LyteBox.prototype.addInlineEvent = function (element, eventName, handler) {
    if (!element) {
        return;
    }

    element[eventName] = handler;

    var elementUUID = $(element)
        .generateUUID()
        .getUUID();

    this.boundEventCollection[elementUUID] = { element: element, eventName: eventName };
};
LyteBox.prototype.resizeHandler = function () {
    if (typeof (myLytebox) !== 'undefined') {
        if (myLytebox.activeFrame !== null || myLytebox.activeImage !== null || myLytebox.activeSlide !== null) {
            if (myLytebox.activeFrame !== null) {
                myLytebox.changeContent(myLytebox.activeFrame);
            } else if (myLytebox.activeImage !== null) {
                myLytebox.changeContent(myLytebox.activeImage);
            } else if (myLytebox.activeFrame !== null) {
                activeSlide.changeContent(myLytebox.activeFrame);
            }
        }
    }
};

// Let's try to get rid of this horrific mess once and for all!
LyteBox.prototype.destroy = function () {
    this.doAnimations = false;
    this.end();

    for (prop in this.boundEventCollection) {
        if (this.boundEventCollection.hasOwnProperty(prop)) {
            var eventItem = this.boundEventCollection[prop];

            eventItem.element[eventItem.eventName] = null;
        }
    }

    $('#lbImageWrapper').trigger('zoom.destroy');
    this.disableKeyboardNav();
    window.removeEventListener('load', initLytebox, false);
    window.removeEventListener('resize', LyteBox.prototype.resizeHandler, false);

    for (var i = 0; i < this.AllTimerCollection.length; i++) {
        for (var k = 0; k < this.AllTimerCollection[i].length; k++) {
            clearTimeout(this.AllTimerCollection[i][k]);
        }
    }

    window.myLytebox = null;
    delete window.myLytebox;
};

if (window.addEventListener) {
    window.addEventListener("load", initLytebox, false);
} else if (window.attachEvent) {
    window.attachEvent("onload", initLytebox);
} else {
    window.onload = function () {
        initLytebox();
    }
}

function initLytebox() {
    if (typeof (myLytebox) !== 'undefined'
        && myLytebox !== null) {
        myLytebox.updateLyteboxItems();
    } else {
        myLytebox = new LyteBox();

        if (window.addEventListener) {
            window.addEventListener('resize', LyteBox.prototype.resizeHandler, false);
        } else if (window.attachEvent) {
            window.attachEvent('onresize', LyteBox.prototype.resizeHandler);
        }
    }
}

/**
 * jQuery CSS Customizable Scrollbar
 *
 * Copyright 2015, Yuriy Khabarov
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * If you found bug, please contact me via email <13real008@gmail.com>
 *
 * @author Yuriy Khabarov aka Gromo
 * @version 0.2.8
 * @url https://github.com/gromo/jquery.scrollbar/
 *
 */
;
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else {
        factory(root.jQuery);
    }
}(this, function ($) {
    'use strict';

    // init flags & variables
    var debug = false;

    var browser = {
        data: {
            index: 0,
            name: 'scrollbar'
        },
        macosx: /mac/i.test(navigator.platform),
        mobile: /android|webos|iphone|ipad|ipod|blackberry/i.test(navigator.userAgent),
        overlay: null,
        scroll: null,
        scrolls: [],
        webkit: /webkit/i.test(navigator.userAgent) && !/edge\/\d+/i.test(navigator.userAgent)
    };

    browser.scrolls.add = function (instance) {
        this.remove(instance).push(instance);
    };
    browser.scrolls.remove = function (instance) {
        while ($.inArray(instance, this) >= 0) {
            this.splice($.inArray(instance, this), 1);
        }
        return this;
    };

    var defaults = {
        "autoScrollSize": true,     // automatically calculate scrollsize
        "autoUpdate": true,         // update scrollbar if content/container size changed
        "debug": false,             // debug mode
        "disableBodyScroll": false, // disable body scroll if mouse over container
        "duration": 200,            // scroll animate duration in ms
        "ignoreMobile": false,      // ignore mobile devices
        "ignoreOverlay": false,     // ignore browsers with overlay scrollbars (mobile, MacOS)
        "scrollStep": 30,           // scroll step for scrollbar arrows
        "showArrows": false,        // add class to show arrows
        "stepScrolling": true,      // when scrolling to scrollbar mousedown position

        "scrollx": null,            // horizontal scroll element
        "scrolly": null,            // vertical scroll element

        "onDestroy": null,          // callback function on destroy,
        "onInit": null,             // callback function on first initialization
        "onScroll": null,           // callback function on content scrolling
        "onUpdate": null            // callback function on init/resize (before scrollbar size calculation)
    };


    var BaseScrollbar = function (container) {

        if (!browser.scroll) {
            browser.overlay = isScrollOverlaysContent();
            browser.scroll = getBrowserScrollSize();
            updateScrollbars();

            $(window).resize(function () {
                var forceUpdate = false;
                if (browser.scroll && (browser.scroll.height || browser.scroll.width)) {
                    var scroll = getBrowserScrollSize();
                    if (scroll.height !== browser.scroll.height || scroll.width !== browser.scroll.width) {
                        browser.scroll = scroll;
                        forceUpdate = true; // handle page zoom
                    }
                }
                updateScrollbars(forceUpdate);
            });
        }

        this.container = container;
        this.namespace = '.scrollbar_' + browser.data.index++;
        this.options = $.extend({}, defaults, window.jQueryScrollbarOptions || {});
        this.scrollTo = null;
        this.scrollx = {};
        this.scrolly = {};

        container.data(browser.data.name, this);
        browser.scrolls.add(this);
    };

    BaseScrollbar.prototype = {

        destroy: function () {

            if (!this.wrapper) {
                return;
            }

            this.container.removeData(browser.data.name);
            browser.scrolls.remove(this);

            // init variables
            var scrollLeft = this.container.scrollLeft();
            var scrollTop = this.container.scrollTop();

            this.container.insertBefore(this.wrapper).css({
                "height": "",
                "margin": "",
                "max-height": ""
            })
                .removeClass('scroll-content scroll-scrollx_visible scroll-scrolly_visible')
                .off(this.namespace)
                .scrollLeft(scrollLeft)
                .scrollTop(scrollTop);

            this.scrollx.scroll.removeClass('scroll-scrollx_visible').find('div').andSelf().off(this.namespace);
            this.scrolly.scroll.removeClass('scroll-scrolly_visible').find('div').andSelf().off(this.namespace);

            this.wrapper.remove();

            $(document).add('body').off(this.namespace);

            if ($.isFunction(this.options.onDestroy)){
                this.options.onDestroy.apply(this, [this.container]);
            }
        },
        init: function (options) {

            // init variables
            var S = this,
                c = this.container,
                cw = this.containerWrapper || c,
                namespace = this.namespace,
                o = $.extend(this.options, options || {}),
                s = {x: this.scrollx, y: this.scrolly},
                w = this.wrapper;

            var initScroll = {
                "scrollLeft": c.scrollLeft(),
                "scrollTop": c.scrollTop()
            };

            // do not init if in ignorable browser
            if ((browser.mobile && o.ignoreMobile)
                || (browser.overlay && o.ignoreOverlay)
                || (browser.macosx && !browser.webkit) // still required to ignore nonWebKit browsers on Mac
                ) {
                return false;
            }

            // init scroll container
            if (!w) {
                this.wrapper = w = $('<div>').addClass('scroll-wrapper').addClass(c.attr('class'))
                    .css('position', c.css('position') == 'absolute' ? 'absolute' : 'relative')
                    .insertBefore(c).append(c);

                if (c.is('textarea')) {
                    this.containerWrapper = cw = $('<div>').insertBefore(c).append(c);
                    w.addClass('scroll-textarea');
                }

                cw.addClass('scroll-content').css({
                    "height": "auto",
                    "margin-bottom": browser.scroll.height * -1 + 'px',
                    "margin-right": browser.scroll.width * -1 + 'px',
                    "max-height": ""
                });

                c.on('scroll' + namespace, function (event) {
                    if ($.isFunction(o.onScroll)) {
                        o.onScroll.call(S, {
                            "maxScroll": s.y.maxScrollOffset,
                            "scroll": c.scrollTop(),
                            "size": s.y.size,
                            "visible": s.y.visible
                        }, {
                            "maxScroll": s.x.maxScrollOffset,
                            "scroll": c.scrollLeft(),
                            "size": s.x.size,
                            "visible": s.x.visible
                        });
                    }
                    s.x.isVisible && s.x.scroll.bar.css('left', c.scrollLeft() * s.x.kx + 'px');
                    s.y.isVisible && s.y.scroll.bar.css('top', c.scrollTop() * s.y.kx + 'px');
                });

                /* prevent native scrollbars to be visible on #anchor click */
                w.on('scroll' + namespace, function () {
                    w.scrollTop(0).scrollLeft(0);
                });

                if (o.disableBodyScroll) {
                    var handleMouseScroll = function (event) {
                        isVerticalScroll(event) ?
                            s.y.isVisible && s.y.mousewheel(event) :
                            s.x.isVisible && s.x.mousewheel(event);
                    };
                    w.on('MozMousePixelScroll' + namespace, handleMouseScroll);
                    w.on('mousewheel' + namespace, handleMouseScroll);

                    if (browser.mobile) {
                        w.on('touchstart' + namespace, function (event) {
                            var touch = event.originalEvent.touches && event.originalEvent.touches[0] || event;
                            var originalTouch = {
                                "pageX": touch.pageX,
                                "pageY": touch.pageY
                            };
                            var originalScroll = {
                                "left": c.scrollLeft(),
                                "top": c.scrollTop()
                            };
                            $(document).on('touchmove' + namespace, function (event) {
                                var touch = event.originalEvent.targetTouches && event.originalEvent.targetTouches[0] || event;
                                c.scrollLeft(originalScroll.left + originalTouch.pageX - touch.pageX);
                                c.scrollTop(originalScroll.top + originalTouch.pageY - touch.pageY);
                                event.preventDefault();
                            });
                            $(document).on('touchend' + namespace, function () {
                                $(document).off(namespace);
                            });
                        });
                    }
                }
                if ($.isFunction(o.onInit)){
                    o.onInit.apply(this, [c]);
                }
            } else {
                cw.css({
                    "height": "auto",
                    "margin-bottom": browser.scroll.height * -1 + 'px',
                    "margin-right": browser.scroll.width * -1 + 'px',
                    "max-height": ""
                });
            }

            // init scrollbars & recalculate sizes
            $.each(s, function (d, scrollx) {

                var scrollCallback = null;
                var scrollForward = 1;
                var scrollOffset = (d === 'x') ? 'scrollLeft' : 'scrollTop';
                var scrollStep = o.scrollStep;
                var scrollTo = function () {
                    var currentOffset = c[scrollOffset]();
                    c[scrollOffset](currentOffset + scrollStep);
                    if (scrollForward == 1 && (currentOffset + scrollStep) >= scrollToValue)
                        currentOffset = c[scrollOffset]();
                    if (scrollForward == -1 && (currentOffset + scrollStep) <= scrollToValue)
                        currentOffset = c[scrollOffset]();
                    if (c[scrollOffset]() == currentOffset && scrollCallback) {
                        scrollCallback();
                    }
                }
                var scrollToValue = 0;

                if (!scrollx.scroll) {

                    scrollx.scroll = S._getScroll(o['scroll' + d]).addClass('scroll-' + d);

                    if(o.showArrows){
                        scrollx.scroll.addClass('scroll-element_arrows_visible');
                    }

                    scrollx.mousewheel = function (event) {

                        if (!scrollx.isVisible || (d === 'x' && isVerticalScroll(event))) {
                            return true;
                        }
                        if (d === 'y' && !isVerticalScroll(event)) {
                            s.x.mousewheel(event);
                            return true;
                        }

                        var delta = event.originalEvent.wheelDelta * -1 || event.originalEvent.detail;
                        var maxScrollValue = scrollx.size - scrollx.visible - scrollx.offset;

                        if ((delta > 0 && scrollToValue < maxScrollValue) || (delta < 0 && scrollToValue > 0)) {
                            scrollToValue = scrollToValue + delta;
                            if (scrollToValue < 0)
                                scrollToValue = 0;
                            if (scrollToValue > maxScrollValue)
                                scrollToValue = maxScrollValue;

                            S.scrollTo = S.scrollTo || {};
                            S.scrollTo[scrollOffset] = scrollToValue;
                            setTimeout(function () {
                                if (S.scrollTo) {
                                    c.stop().animate(S.scrollTo, 240, 'linear', function () {
                                        scrollToValue = c[scrollOffset]();
                                    });
                                    S.scrollTo = null;
                                }
                            }, 1);
                        }

                        event.preventDefault();
                        return false;
                    };

                    scrollx.scroll
                        .on('MozMousePixelScroll' + namespace, scrollx.mousewheel)
                        .on('mousewheel' + namespace, scrollx.mousewheel)
                        .on('mouseenter' + namespace, function () {
                            scrollToValue = c[scrollOffset]();
                        });

                    // handle arrows & scroll inner mousedown event
                    scrollx.scroll.find('.scroll-arrow, .scroll-element_track')
                        .on('mousedown' + namespace, function (event) {

                            if (event.which != 1) // lmb
                                return true;

                            scrollForward = 1;

                            var data = {
                                "eventOffset": event[(d === 'x') ? 'pageX' : 'pageY'],
                                "maxScrollValue": scrollx.size - scrollx.visible - scrollx.offset,
                                "scrollbarOffset": scrollx.scroll.bar.offset()[(d === 'x') ? 'left' : 'top'],
                                "scrollbarSize": scrollx.scroll.bar[(d === 'x') ? 'outerWidth' : 'outerHeight']()
                            };
                            var timeout = 0, timer = 0;

                            if ($(this).hasClass('scroll-arrow')) {
                                scrollForward = $(this).hasClass("scroll-arrow_more") ? 1 : -1;
                                scrollStep = o.scrollStep * scrollForward;
                                scrollToValue = scrollForward > 0 ? data.maxScrollValue : 0;
                            } else {
                                scrollForward = (data.eventOffset > (data.scrollbarOffset + data.scrollbarSize) ? 1
                                    : (data.eventOffset < data.scrollbarOffset ? -1 : 0));
                                scrollStep = Math.round(scrollx.visible * 0.75) * scrollForward;
                                scrollToValue = (data.eventOffset - data.scrollbarOffset -
                                    (o.stepScrolling ? (scrollForward == 1 ? data.scrollbarSize : 0)
                                        : Math.round(data.scrollbarSize / 2)));
                                scrollToValue = c[scrollOffset]() + (scrollToValue / scrollx.kx);
                            }

                            S.scrollTo = S.scrollTo || {};
                            S.scrollTo[scrollOffset] = o.stepScrolling ? c[scrollOffset]() + scrollStep : scrollToValue;

                            if (o.stepScrolling) {
                                scrollCallback = function () {
                                    scrollToValue = c[scrollOffset]();
                                    clearInterval(timer);
                                    clearTimeout(timeout);
                                    timeout = 0;
                                    timer = 0;
                                };
                                timeout = setTimeout(function () {
                                    timer = setInterval(scrollTo, 40);
                                }, o.duration + 100);
                            }

                            setTimeout(function () {
                                if (S.scrollTo) {
                                    c.animate(S.scrollTo, o.duration);
                                    S.scrollTo = null;
                                }
                            }, 1);

                            return S._handleMouseDown(scrollCallback, event);
                        });

                    // handle scrollbar drag'n'drop
                    scrollx.scroll.bar.on('mousedown' + namespace, function (event) {

                        if (event.which != 1) // lmb
                            return true;

                        var eventPosition = event[(d === 'x') ? 'pageX' : 'pageY'];
                        var initOffset = c[scrollOffset]();

                        scrollx.scroll.addClass('scroll-draggable');

                        $(document).on('mousemove' + namespace, function (event) {
                            var diff = parseInt((event[(d === 'x') ? 'pageX' : 'pageY'] - eventPosition) / scrollx.kx, 10);
                            c[scrollOffset](initOffset + diff);
                        });

                        return S._handleMouseDown(function () {
                            scrollx.scroll.removeClass('scroll-draggable');
                            scrollToValue = c[scrollOffset]();
                        }, event);
                    });
                }
            });

            // remove classes & reset applied styles
            $.each(s, function (d, scrollx) {
                var scrollClass = 'scroll-scroll' + d + '_visible';
                var scrolly = (d == "x") ? s.y : s.x;

                scrollx.scroll.removeClass(scrollClass);
                scrolly.scroll.removeClass(scrollClass);
                cw.removeClass(scrollClass);
            });

            // calculate init sizes
            $.each(s, function (d, scrollx) {
                $.extend(scrollx, (d == "x") ? {
                    "offset": parseInt(c.css('left'), 10) || 0,
                    "size": c.prop('scrollWidth'),
                    "visible": w.width()
                } : {
                    "offset": parseInt(c.css('top'), 10) || 0,
                    "size": c.prop('scrollHeight'),
                    "visible": w.height()
                });
            });

            // update scrollbar visibility/dimensions
            this._updateScroll('x', this.scrollx);
            this._updateScroll('y', this.scrolly);

            if ($.isFunction(o.onUpdate)){
                o.onUpdate.apply(this, [c]);
            }

            // calculate scroll size
            $.each(s, function (d, scrollx) {

                var cssOffset = (d === 'x') ? 'left' : 'top';
                var cssFullSize = (d === 'x') ? 'outerWidth' : 'outerHeight';
                var cssSize = (d === 'x') ? 'width' : 'height';
                var offset = parseInt(c.css(cssOffset), 10) || 0;

                var AreaSize = scrollx.size;
                var AreaVisible = scrollx.visible + offset;

                var scrollSize = scrollx.scroll.size[cssFullSize]() + (parseInt(scrollx.scroll.size.css(cssOffset), 10) || 0);

                if (o.autoScrollSize) {
                    scrollx.scrollbarSize = parseInt(scrollSize * AreaVisible / AreaSize, 10);
                    scrollx.scroll.bar.css(cssSize, scrollx.scrollbarSize + 'px');
                }

                scrollx.scrollbarSize = scrollx.scroll.bar[cssFullSize]();
                scrollx.kx = ((scrollSize - scrollx.scrollbarSize) / (AreaSize - AreaVisible)) || 1;
                scrollx.maxScrollOffset = AreaSize - AreaVisible;
            });

            c.scrollLeft(initScroll.scrollLeft).scrollTop(initScroll.scrollTop).trigger('scroll');
        },

        /**
         * Get scrollx/scrolly object
         *
         * @param {Mixed} scroll
         * @returns {jQuery} scroll object
         */
        _getScroll: function (scroll) {
            var types = {
                advanced: [
                    '<div class="scroll-element">',
                    '<div class="scroll-element_corner"></div>',
                    '<div class="scroll-arrow scroll-arrow_less"></div>',
                    '<div class="scroll-arrow scroll-arrow_more"></div>',
                    '<div class="scroll-element_outer">',
                    '<div class="scroll-element_size"></div>', // required! used for scrollbar size calculation !
                    '<div class="scroll-element_inner-wrapper">',
                    '<div class="scroll-element_inner scroll-element_track">', // used for handling scrollbar click
                    '<div class="scroll-element_inner-bottom"></div>',
                    '</div>',
                    '</div>',
                    '<div class="scroll-bar">', // required
                    '<div class="scroll-bar_body">',
                    '<div class="scroll-bar_body-inner"></div>',
                    '</div>',
                    '<div class="scroll-bar_bottom"></div>',
                    '<div class="scroll-bar_center"></div>',
                    '</div>',
                    '</div>',
                    '</div>'
                ].join(''),
                simple: [
                    '<div class="scroll-element">',
                    '<div class="scroll-element_outer">',
                    '<div class="scroll-element_size"></div>', // required! used for scrollbar size calculation !
                    '<div class="scroll-element_track"></div>', // used for handling scrollbar click
                    '<div class="scroll-bar"></div>', // required
                    '</div>',
                    '</div>'
                ].join('')
            };
            if (types[scroll]) {
                scroll = types[scroll];
            }
            if (!scroll) {
                scroll = types['simple'];
            }
            if (typeof (scroll) == 'string') {
                scroll = $(scroll).appendTo(this.wrapper);
            } else {
                scroll = $(scroll);
            }
            $.extend(scroll, {
                bar: scroll.find('.scroll-bar'),
                size: scroll.find('.scroll-element_size'),
                track: scroll.find('.scroll-element_track')
            });
            return scroll;
        },

        _handleMouseDown: function(callback, event) {

            var namespace = this.namespace;

            $(document).on('blur' + namespace, function () {
                $(document).add('body').off(namespace);
                callback && callback();
            });
            $(document).on('dragstart' + namespace, function (event) {
                event.preventDefault();
                return false;
            });
            $(document).on('mouseup' + namespace, function () {
                $(document).add('body').off(namespace);
                callback && callback();
            });
            $('body').on('selectstart' + namespace, function (event) {
                event.preventDefault();
                return false;
            });

            event && event.preventDefault();
            return false;
        },

        _updateScroll: function (d, scrollx) {

            var container = this.container,
                containerWrapper = this.containerWrapper || container,
                scrollClass = 'scroll-scroll' + d + '_visible',
                scrolly = (d === 'x') ? this.scrolly : this.scrollx,
                offset = parseInt(this.container.css((d === 'x') ? 'left' : 'top'), 10) || 0,
                wrapper = this.wrapper;

            var AreaSize = scrollx.size;
            var AreaVisible = scrollx.visible + offset;

            scrollx.isVisible = (AreaSize - AreaVisible) > 1; // bug in IE9/11 with 1px diff
            if (scrollx.isVisible) {
                scrollx.scroll.addClass(scrollClass);
                scrolly.scroll.addClass(scrollClass);
                containerWrapper.addClass(scrollClass);
            } else {
                scrollx.scroll.removeClass(scrollClass);
                scrolly.scroll.removeClass(scrollClass);
                containerWrapper.removeClass(scrollClass);
            }

            if (d === 'y') {
                if(container.is('textarea') || AreaSize < AreaVisible){
                    containerWrapper.css({
                        "height": (AreaVisible + browser.scroll.height) + 'px',
                        "max-height": "none"
                    });
                } else {
                    containerWrapper.css({
                        //"height": "auto", // do not reset height value: issue with height:100%!
                        "max-height": (AreaVisible + browser.scroll.height) + 'px'
                    });
                }
            }

            if (scrollx.size != container.prop('scrollWidth')
                || scrolly.size != container.prop('scrollHeight')
                || scrollx.visible != wrapper.width()
                || scrolly.visible != wrapper.height()
                || scrollx.offset != (parseInt(container.css('left'), 10) || 0)
                || scrolly.offset != (parseInt(container.css('top'), 10) || 0)
                ) {
                $.extend(this.scrollx, {
                    "offset": parseInt(container.css('left'), 10) || 0,
                    "size": container.prop('scrollWidth'),
                    "visible": wrapper.width()
                });
                $.extend(this.scrolly, {
                    "offset": parseInt(container.css('top'), 10) || 0,
                    "size": this.container.prop('scrollHeight'),
                    "visible": wrapper.height()
                });
                this._updateScroll(d === 'x' ? 'y' : 'x', scrolly);
            }
        }
    };

    var CustomScrollbar = BaseScrollbar;

    /*
     * Extend jQuery as plugin
     *
     * @param {Mixed} command to execute
     * @param {Mixed} arguments as Array
     * @return {jQuery}
     */
    $.fn.scrollbar = function (command, args) {
        if (typeof command !== 'string') {
            args = command;
            command = 'init';
        }
        if (typeof args === 'undefined') {
            args = [];
        }
        if (!$.isArray(args)) {
            args = [args];
        }
        this.not('body, .scroll-wrapper').each(function () {
            var element = $(this),
                instance = element.data(browser.data.name);
            if (instance || command === 'init') {
                if (!instance) {
                    instance = new CustomScrollbar(element);
                }
                if (instance[command]) {
                    instance[command].apply(instance, args);
                }
            }
        });
        return this;
    };

    /**
     * Connect default options to global object
     */
    $.fn.scrollbar.options = defaults;


    /**
     * Check if scroll content/container size is changed
     */

    var updateScrollbars = (function () {
        var timer = 0,
            timerCounter = 0;

        return function (force) {
            var i, container, options, scroll, wrapper, scrollx, scrolly;
            for (i = 0; i < browser.scrolls.length; i++) {
                scroll = browser.scrolls[i];
                container = scroll.container;
                options = scroll.options;
                wrapper = scroll.wrapper;
                scrollx = scroll.scrollx;
                scrolly = scroll.scrolly;
                if (force || (options.autoUpdate && wrapper && wrapper.is(':visible') &&
                    (container.prop('scrollWidth') != scrollx.size || container.prop('scrollHeight') != scrolly.size || wrapper.width() != scrollx.visible || wrapper.height() != scrolly.visible))) {
                    scroll.init();

                    if (options.debug) {
                        window.console && console.log({
                            scrollHeight: container.prop('scrollHeight') + ':' + scroll.scrolly.size,
                            scrollWidth: container.prop('scrollWidth') + ':' + scroll.scrollx.size,
                            visibleHeight: wrapper.height() + ':' + scroll.scrolly.visible,
                            visibleWidth: wrapper.width() + ':' + scroll.scrollx.visible
                        }, true);
                        timerCounter++;
                    }
                }
            }
            if (debug && timerCounter > 10) {
                window.console && console.log('Scroll updates exceed 10');
                updateScrollbars = function () {};
            } else {
                clearTimeout(timer);
                timer = setTimeout(updateScrollbars, 50);
            }
        };
    })();

    /* ADDITIONAL FUNCTIONS */
    /**
     * Get native browser scrollbar size (height/width)
     *
     * @param {Boolean} actual size or CSS size, default - CSS size
     * @returns {Object} with height, width
     */
    function getBrowserScrollSize(actualSize) {

        if (browser.webkit && !actualSize) {
            return {
                "height": 0,
                "width": 0
            };
        }

        if (!browser.data.outer) {
            var css = {
                "border": "none",
                "box-sizing": "content-box",
                "height": "200px",
                "margin": "0",
                "padding": "0",
                "width": "200px"
            };
            browser.data.inner = $("<div>").css($.extend({}, css));
            browser.data.outer = $("<div>").css($.extend({
                "left": "-1000px",
                "overflow": "scroll",
                "position": "absolute",
                "top": "-1000px"
            }, css)).append(browser.data.inner).appendTo("body");
        }

        browser.data.outer.scrollLeft(1000).scrollTop(1000);

        return {
            "height": Math.ceil((browser.data.outer.offset().top - browser.data.inner.offset().top) || 0),
            "width": Math.ceil((browser.data.outer.offset().left - browser.data.inner.offset().left) || 0)
        };
    }

    /**
     * Check if native browser scrollbars overlay content
     *
     * @returns {Boolean}
     */
    function isScrollOverlaysContent() {
        var scrollSize = getBrowserScrollSize(true);
        return !(scrollSize.height || scrollSize.width);
    }

    function isVerticalScroll(event) {
        var e = event.originalEvent;
        if (e.axis && e.axis === e.HORIZONTAL_AXIS)
            return false;
        if (e.wheelDeltaX)
            return false;
        return true;
    }


    /**
     * Extend AngularJS as UI directive
     * and expose a provider for override default config
     *
     */
    if (window.angular) {
        (function (angular) {
            angular.module('jQueryScrollbar', [])
                .provider('jQueryScrollbar', function () {
                    var defaultOptions = defaults;
                    return {
                        setOptions: function (options) {
                            angular.extend(defaultOptions, options);
                        },
                        $get: function () {
                            return {
                                options: angular.copy(defaultOptions)
                            };
                        }
                    };
                })
                .directive('jqueryScrollbar', function (jQueryScrollbar, $parse) {
                    return {
                        "restrict": "AC",
                        "link": function (scope, element, attrs) {
                            var model = $parse(attrs.jqueryScrollbar),
                                options = model(scope);
                            element.scrollbar(options || jQueryScrollbar.options)
                                .on('$destroy', function () {
                                    element.scrollbar('destroy');
                                });
                        }
                    };
                });
        })(window.angular);
    }
}));
var animationTime= 400;

$(document).ready(function () {
/*** Hide popups ***/
    $('body').click(function(e){
        var popupParent = $(e.target).parents('.popup').length > 0;
        if ($(e.target).hasClass('popup-close')
            || popupParent
            || $(e.target).hasClass('popup')
            || (popupParent && $(e.target).parents('a'))
            || $(e.target).closest('.tablet-popup, .tablet-popup-entry').length > 0) {
        }else{
            closeShowedPopup();
        }
    });

/*** Tooltip hover***/
if(!MOBILE && !TABLET)
{
    $('.middle_page_text a.tooltip').hover(function(){
        var span = $(this).children();
        var height = span.height();
        span.css({display:'block', height:0});
        span.animate({height:height},animationTime/4,function(){
             span.css({overflow:'visible'});
        })}, function(){
            var span = $(this).children();
            span.animate({height:0},animationTime/4, function(){
                 span.css({display:'none', height:'auto', 'overflow': 'hidden'});
            });
        });

    if($('.middle_page_text .tooltip').length > 0) {
        $('.middle_page_text .tooltip').sort(function(a,b){
            return $(a).find('span > b').html() > $(b).find('span > b').html();
        }).each(function(index){
                var html = '<li><a href="'+relPath+'glossary/'+$(this).attr('glossary_id')+'">'+$(this).find('span > b').html()+'</a></li>';
                $('#glossary-formular ul').append(html);
        })

        $("li#glossary > a").click(function(event){
            openPopup(event, this);
        });
    }
}

/*** Scroll Down ***/
    $(".goDown").click(function(event){
        event.preventDefault();
        $('#searchDiv').scrollTo();
        return false;
    });
    $(".linkDown").click(function(event){
        event.preventDefault();
        var elHeight = $(this).closest('#banner, #fullsize_banner').offset().top +  $(this).closest('#banner, #fullsize_banner').outerHeight();
        $("html, body")
            .animate({scrollTop: Math.ceil(elHeight) + "px"}, animationTime * 2, "swing")
        return false;
    });

/*** Scroll Up ***/
    $("a.goUp").click(function(event){
        event.preventDefault();
        $('body').scrollTo();
        return false;
    });

    $('#download-icon, #links-icon').click(function(){
        $('#related_wrapper').scrollTo();
        return false;
    });

/*** Flyout Menu ***/
    $(".brandSelect, #closeFlyoutMenu").click(function(event){
        event.preventDefault();
        event.stopPropagation();

        $('div#hamburger').removeClass('active'); // JIC close hamburger menu


        var flyout_menu = $('.flyout_menu.popup');

        // Dusty: for legacy-compatible code... doesn't look good.
        var menuIsVisible = $(".menu_click").length ?
                $(".menu_click").hasClass("hidden") :
                'block' === flyout_menu.css('display');

        if ( menuIsVisible ) {
            $(".menu_click").removeClass("hidden");
            $('.brandSelect').removeClass('active');

            if (MOBILE) {
                flyout_menu.hide();
                menu.overlay(0);
            } else {
                flyout_menu.animate({height:0}, animationTime, function(){
                    flyout_menu.css({display:'none', height:'390px'});
                    menu.overlay(0);
                });
            }

        } else {
            closeShowedPopup();
            menu.overlay(1);
            $(".menu_click").addClass("hidden");
            $('.brandSelect').addClass('active');

            if (MOBILE) {
                flyout_menu.show();
                window.scrollTo(0, 0);
            } else {
                if(isTabletViewport()){
                    $('html').attr('style', 'overflow:hidden!important;');
                }
                var height = $('.flyout_menu').height();
                flyout_menu.css({display:'block', height:0});
                flyout_menu.animate({height:height},animationTime);
            }
        }
    });


/*** Popup Footer Tools ***/
//#tools ul.footer_tools li a,
    $("li#help > a,div#profile").click(function(event){
        if ((isTabletViewport() || isMobileViewport()) && $(this).parent('#help').length > 0) {
            return true;
        }
        openPopup(event, this);
    });

    $(".form_item a.popup-close, footer a.popup-close, #topLinks a.popup-close").click(function(event){
        event.preventDefault();
        event.stopPropagation();
        var popup;
        popup = $(this).parent().parent().find('.popup');
        if (popup.length > 0) {

            popup.css('overflow','hidden');
            $scroll = popup.find(".related-links-right,.related-downloads-right");
            $scroll.css({visibility:"hidden"});
            popup.animate({height:0},animationTime, function(){
                popup.parent().parent().find('.active:not(.hamburger)').removeClass('active');
                popup.css({visibility:'hidden', height:'auto'});

            });
        }
    });

    if(!MOBILE && $('li#faq > .popup').length > 0){
        $('li#faq > a').click(function(event){
            openPopup(event, $('li#faq > a'));
        });
    }

    $('#help-formular #messageText').keyup(function(){
        var len = $(this).val().length;
        $('#messageText_counter').html(len);
        if(len > 1000){
            $('#counter_wrapper').addClass('error');
        }else if($('#counter_wrapper').hasClass('error')){
            $('#counter_wrapper').removeClass('error');
        }
    });
     // issue warning } - javascript


/*** Left Navigation ***/
    $(".menuitems li").click(function(event){
        event.preventDefault();
        var goToItem = $(this).find("a").attr('href');
        if($("#"+goToItem).length > 0){
            $('html, body').animate({ scrollTop: $("#"+goToItem).offset().top-70 }, 1000);
        }
    });

    // Is this still used anywhere? I think not.
	//$(".level3_nav li span").click(function() {
	//	if($(this).parent().hasClass('active')){
	//		$(this).parent().find(".level4_nav").hide();
	//		$(this).parent().removeClass('active');
	//	}else{
	//		$(this).parent().parent().find(".level4_nav").hide();
	//		$(this).parent().parent().find('li').removeClass('active');
	//		$(this).parent().find(".level4_nav").show();
	//		$(this).parent().addClass('active');
	//	}
	//});

    $('.filter_filters .filter').click(function(){
        if($(this).hasClass('closed')){
            $(this).removeClass('closed');
            $(this).addClass('opened');
        }else{
            $(this).removeClass('opened');
            $(this).addClass('closed');
        }
        $(this).find('.filter_content').toggle();
    });






    $('.basic_nav_closed wrap div, #rubrics li a').click(menu.show);

    if (isTabletViewport()) {
        $('div.filter-panel-title').addClass('tablet-popup-entry');
    }

    $('div.filter-panel-title').click(function(event){
        var el = $(this).parent();

        if(el.hasClass('active')) {
            el.removeClass('active');
            $(this).removeClass('active');

            if(MOBILE) {
                $('#remove_filters').css('display','none');
            }
            menu.overlay(false);
        } else {
            if (isTabletViewport()) {
                closeShowedPopup('.filter-panel-title');
            }

            el.addClass('active');
            $(this).addClass('active');

            if(MOBILE) {
                $("div.basic_nav > .menu_label").removeClass('active');
                $('#remove_filters').css('display','block');
            }
            menu.overlay(true);
        }
    });



    $(document).on('click', 'span#menu_close, #menu_overlay, section.basic_nav_opened', function(event) {
        var target = $(event.target),
            tId = target.attr('id');

        if ( 'menu_close' == tId || target.hasClass('basic_nav_opened') ) {
            stopEvent(event);
            if (isTabletViewport()) {
                return;
            }
            menu.hide();
        }
        else if ('menu_overlay' == tId) {
            if ( $(".basic_nav_opened").inViewport() ) {
                stopEvent(event);
                menu.hide();
            } else if($(".flyout_search.popup").inViewport()) {
                flyoutSearch.hide(event);
            }
        } else {
            // console.warn(tId);
        }

    });

    $('.load-more').click(function(){
        loadBPPages(this);
    });

    // Grid and List view
//    $('#view-controls a').click(function(event){
    $('body').on('click', '#view-controls a', function(event) {
        event.preventDefault();
        event.stopPropagation();

        var t = $(this), bw = t.closest('div.blocks_wrap');

        if(!bw.length)
            bw = t.closest('section.downloads_overview');

        $('#view-controls a').removeClass('active');
        t.addClass('active');

        if(t.hasClass('view-list')) {
            bw.removeClass('bwgrid').addClass('bwlist');
            // $('div.blocks_wrap > section.section_content').removeClass('grid-view');
            $('.grid-page').addClass('list-view');
            if($('.downloads_overview').length) {
                sessionStorage.setItem('list-view', true);
            }
        } else {
            bw.removeClass('bwlist').addClass('bwgrid');
            // $('div.blocks_wrap > section.section_content').addClass('grid-view');
            $('.grid-page').removeClass('list-view');
            if($('.downloads_overview').length) {
                sessionStorage.removeItem('list-view');
            }
        }

        if (filter && filter.count() == 0) {
            vertical_navigation.markerMap();
        }
        imageProcess.updateLazyload();
        //return false;
    });

    $('#load-more_wrapper .black-button').click(function(){
        $('#related_wrapper .hided').toggleClass('showed');
        if($('#related_wrapper .showed').length > 0){
            $(this).text(js_localize['bp.show_less']);
            $('#related_wrapper ul li:nth-child(6)').css('border-bottom-width', '1px');
        }else{
            $('#related_wrapper').scrollTo();
            $(this).text(js_localize['bp.load_more']);
            $('#related_wrapper ul li:nth-child(6)').css('border-bottom-width', 0);
        }
    });

    if ( $('.banner_text_wrapper p').length > 0 && $('body').hasClass('smart') ) {
        $('.banner_text_wrapper p').last().append('<span class="right_arrow"></span>');
    }

    $('.reset_captcha').click(function(){
        var button = this;
        $.ajax({
            url : relPath + "reset_captcha",
            type : 'GET',
            success: function (data) {
                var brand = $('body').hasClass('smart') ? 'smart' : 'daimler';
                $($(button).parent().find('img')).attr("src", relPath + "captcha?brand=" + brand + "&timestamp=" + new Date().getTime());
            }
        });
         return false;
    });
}); // $(document).ready()

function loadBPPages(button){
    var page = $(button).attr('page');
    $.ajax({
        url : relPath + "load_best_practice",
        type : 'POST',
        data : {pageid :$(button).attr('page_id'), page : page,_token : $('#searchForm2 input[name="_token"]').val()},
        success : function(data){
            var response = data.response;
            $('.best-practice.grid-page').append(response.html);
            $(button).attr('page',parseInt(page)+1);
            if(response.islast){
                $('a.load-more').hide();
            }else{
                $('a.load-more').show();
            }
        }
    });
}

$(function loadMoreTen () {
    var size_li = $("#load-more-tags li").size();
    var  maxVisibleTagCount = 10;
    $('#load-more-tags li:lt('+maxVisibleTagCount+')').show();
    if(size_li <= maxVisibleTagCount){
        $('#load-more-ten').hide();
    }else{
        $('#load-more-ten').show();
         $('#load-more-ten').click(function () {
        maxVisibleTagCount = (maxVisibleTagCount+10 <= size_li) ? maxVisibleTagCount+10 : size_li;
        $('#load-more-tags li:lt('+maxVisibleTagCount+')').show();
        if(size_li <= maxVisibleTagCount){
            $('#load-more-ten').hide();
        }

    });
   }
});

function closeShowedPopup(exclude) {
    $.fx.off = true;
    $('.popup').each(function() {
        var el = $(this);
        if(el.is(':visible') && el.css("visibility") == "visible") {
            if(exclude && el.is(exclude)) {
                // skip
                console.log('skipping: ' + el);
            } else {
                el.find('.popup-close').trigger('click');
            }
        }
    });

    exclude = exclude || '';

    if (isTabletViewport()) {
        $('.tablet-popup-entry').filter('.active').not(exclude).trigger('click');
    }

    $.fx.off = false;
}

function openPopup(event, el){
    var parent = $(el).parent();
    var popup  = parent.find('.popup');
    //$(el).parent().css('overflow', 'visible');

    if(popup.length > 0){
        event.preventDefault();
        event.stopPropagation();
        if(popup.css('visibility') == "visible"){
            popup.find('.popup-close').trigger('click');
        }else{
            if(!(isMobileViewport()
                && el.id == 'profile')) {
                menu.hide();
                closeShowedPopup();
            }
            $(el).addClass('active');


            if($('#links-icon').length){
                maxHeight();
            }
            var height = popup.outerHeight();
            if(parent.hasClass('right-menu-item')) {
                if(parent.position().top < height) {
                    popup.css({top:0 , bottom:'auto'});
                }
            }
            $scroll = popup.find(".related-links-right,.related-downloads-right");
console.log('here - open', height, $scroll);
            $scroll.css({visibility:"hidden"});
            popup.css({visibility:'visible', height:0});
            //popup.fadeToggle();
            popup.animate({height:height},animationTime, function(){
                popup.css({'overflow':'visible'});
            });
        }
    }
}

function maxHeight(){
    var top = $(window).height();
    if ($('footer.sticky').offset()) {
        top = $('footer.sticky').offset().top;
    }
    var height = top - $('#links-icon').offset().top - 103;
    $('.scrollbar-rail').css('max-height', height +"px");
    $('.related-links-right .scrollbar-rail').scrollbar({
        "autoUpdate": true
    });
    $('.related-downloads-right .scrollbar-rail').scrollbar({
        "autoUpdate": true
    });
}

function moveNav(height){
    $("html, body").animate({
        scrollTop: height + "px"
    }, 1700,"swing");
    return false;
}

function calculateOpacity($object){
    var scrollTop = $(window).scrollTop(), opacityElement = $object;

    if(scrollTop > 750){
        if(opacityElement.css('opacity') != '0'){
            opacityElement.css('opacity', '0');
        }

        return;
    }
    var opacity = (11 - (scrollTop / 68)) / 10;
    opacityElement.css('opacity', opacity);
}

//
// Download Files functions
//
function startDownload(id, ev) {
    // Create download IFRAME if not exists
    var $iframe = $('#downloadIFrame');
    if (0 === $iframe.length) {
        $iframe = $('<iframe id="downloadIFrame" name="downloadIFrame" style="display:none;" />');
        $iframe.appendTo('body');
    }

    var $form = $('#downloadsForm');
    if (id !== undefined) {
        if (0 === $form.length) {
            $form = $('<form id="downloadsForm"/>');
            $form.attr('method', 'POST');
            $form.appendTo('body');
        }
        var checkbox = $($form.find('#file_' + id));
        if (0 === checkbox.length) {
            $form.append('<input type="checkbox" \n\
                id="file_' + id + '" class="chb checkbox1 " value="' + id + '" name="auid[]">');
            checkbox = $form.find('#file_' + id);
        }
        checkbox.prop("checked", true);
        $form.attr('action', 'download');

        // Token
        var tokenElement = $form.find('input[name="_token"]');
        if (0 === tokenElement.length) {
            $form.append('<input name="_token" type="hidden" value="' + $('meta[name="csrf-token"]').attr('content') + '">');
        }
    }
    $form.find('input[name="_method"]').val('post');
    $form.attr('target', 'downloadIFrame');
    if (isDesktopViewport()) {
        $form.submit();
    } else {
        DBDN.utils.downloadFileAndDisplayNotificationWhileBeingDownloaded($form, js_localize['file.download'], true);
    }

    if (undefined !== id) {
        checkbox.prop("checked", false);
    }

    return false;
}

function addToCart(id, ev) {
    ev.preventDefault();
    ev.stopPropagation();
    $('body').css('cursor', 'progress');
    var $form = $('#downloadsForm');
    $form.find('input[name="_method"]').val('put');
    var linkTag = $(ev.target);
    var textTag = linkTag.find('span');
    // Download Page
    if ('A' === linkTag.prop('tagName') && linkTag.hasClass('cart')) {
        textTag = linkTag;
    } else {
        // Most ... and Dawnload List
        var grid = linkTag.parents('.download_hover');
        if (grid.length > 0) {
            var div = grid.find(".cart_wrapper");
        } else {
            var div = linkTag.parents('.list-hover').find(".cart_wrapper");
        }
        textTag = div.find('span');
    }

    $.ajax({
        method: "PUT",
        url: 'Download_Cart/add/' + id,
        data: $form.serialize()
    })
            .done(function (data) {
                $('.topItem#downloads .counter').html(data.response.myDownloadsCount);
                ev.target.onclick = function () {
                    return false;
                };
                linkTag.addClass('not-active');
                textTag.html(data.message);
                $('body').css('cursor', 'default');
            })
            .fail(function (XMLHttpRequest) {
                var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                var message = JSON_ERROR.composeMessageFromJSON(data, true);
                msg(message, true);
                $('body').css('cursor', 'default');
            });

    return false;
}

if (!DBDN.utils.isForcedDesktopMode()) {
    // Tablet only: Add the active viewport as class to the page class list
    $(function () {
        var $htmlElement = $('html').addClass(DBDN.utils.getActiveHtmlViewportMode());

        if (DBDN.utils.isScaledViewport()) {
            $htmlElement.addClass(DBDN.enum.HtmlViewportMode.SCALED);
        }

        if (isTabletViewport() || isDesktopViewport()) {
            $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
                $htmlElement.removeClass(ev.htmlViewportMode.previous);
                $htmlElement.addClass(ev.htmlViewportMode.active);

                if (ev.htmlViewportMode.isActiveScaled) {
                    $htmlElement.addClass(DBDN.enum.HtmlViewportMode.SCALED);
                } else {
                    $htmlElement.removeClass(DBDN.enum.HtmlViewportMode.SCALED);
                }
            });
        }
    });

    // Tablet only: Fix the crazy markup in the section "oftheday"
    $(function () {
        if (isTabletViewport() || isDesktopViewport()) {
            var $ofTheDaySection = $('#oftheday');
            var newOfTheDayElementCollection = [];

            $ofTheDaySection.find('div:nth-of-type(2) > .container').each(function (index) {
                var $this = $(this);

                var $newOfTheDayElement = $('<\div class="obj container">').visibleToTabletOnly();

                var $mainHead = $this.children().clone().addClass('mainHead');
                var $subHead = $this.parent().next().children().eq(index).children().clone().addClass('subHead');
                var $text = $this.parent().next().next().children().eq(index).children().clone().addClass('text');
                var $linkButton = $this.parent().next().next().next().children().eq(index).children().clone().addClass('linkButton');

                $newOfTheDayElement.append([$mainHead, $subHead, $text, $linkButton]);
                newOfTheDayElementCollection.push($newOfTheDayElement);
            });

            $ofTheDaySection
                .hide()
                .append(newOfTheDayElementCollection)
                .show();
        }
    });

    // Tablet only: Split the footer metalinks in two lists
    $(function () {
        if (isTabletViewport() || isDesktopViewport()) {
            var $originalSingleList = $('#footer_wrap ul.metalinks');
            var $originalSingleListParent = $originalSingleList.parent();
            var $originalSingleListItems = $originalSingleList.children('li');
            var numberOfListItemsPerList = Math.round($originalSingleListItems.length / 2);
            var $firstTabletList = $('<ul class="metalinks"></ul>').visibleToTabletOnly();
            var $secondTabletList = $firstTabletList.clone();

            $originalSingleListItems
                .slice(0, numberOfListItemsPerList)
                .clone()
                .last()
                .addClass('last')
                .end()
                .appendTo($firstTabletList);

            $originalSingleListItems
                .slice(numberOfListItemsPerList)
                .clone()
                .appendTo($secondTabletList);

            var $tabletLists = $firstTabletList.add($secondTabletList);

            if (isTabletViewport()) {
                $originalSingleList = $originalSingleList
                    .parent()
                    .hide()
                    .end()
                    .after($tabletLists)
                    .detach();

                $originalSingleListParent.show();
            }

            $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
                DBDN.utils.toggleTabletDesktopHtml($originalSingleListParent,
                    $originalSingleList,
                    $tabletLists,
                    ev.htmlViewportMode,
                    function ($toBeInserted, $toBeDeleted) {
                        $toBeDeleted
                            .last()
                            .after($toBeInserted);
                    });
            });
        }
    });

    // Tablet only: Position elements
    $(function () {
        function positionElementsOnce() {
            // Design manuals content page (basic) - pages navigation buttons
            $('.nav_buttons')
                .clone()
                .hideBeforeTransition()
                .insertAfter('.middle_page_text #content_main')
                .visibleToTabletOnly();
            // Best practice content page - contact info box
            $(document).on('stats_loaded.DBDN_Tablet', function () {
                $('.middle_page_text #contact_info')
                    .clone()
                    .hideBeforeTransition()
                    .insertAfter('.middle_page_text #content_main')
                    .visibleToTabletOnly();
            });
        }

        if (isTabletViewport() || isDesktopViewport()) {
            positionElementsOnce();
        }
    });

    // Tablet/Desktop: Close some components when switching between desktop/tablet.
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var isThisFunctionalitySuspended = false;
        var $brandSelectMenu = $('header .brandSelect');

        function closeSearchPopup() {
            flyoutSearch.hide();
            menu.overlay(0);
        }

        function closeBrandSelectMenu() {
            if ($brandSelectMenu.hasClass('active')) {
                $brandSelectMenu.trigger('click');
            }
        }

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            if (isThisFunctionalitySuspended) {
                return;
            }

            $.fx.off = true;
            closeSearchPopup();
            closeBrandSelectMenu();
            $.fx.off = false;
        });

        $(document).on('suspendComponentClosingOnTabletDesktop.DBDN_General', function () {
            isThisFunctionalitySuspended = true;
        });

        $(document).on('unsuspendComponentClosingOnTabletDesktop.DBDN_General', function () {
            isThisFunctionalitySuspended = false;
        });
    });

    // Tablet only: Design Manuals content page (basic) - stats on top and bottom of the page
    // Downloads single pages - stats top of page
    $(function () {
        $(document).on('pageActions_showUnauthAlertText.DBDN_Tablet', function (ev, alertMessage) {
            showMsg(alertMessage);
        });

        function showMsg(text) {
            var $notificationElement = $('body #wrapper #notification');
            var eventSelector = '.ok_button, .no_button';
            $notificationElement.addClass('page-action');

            $notificationElement
                .off('click', eventSelector)
                .on('click', eventSelector, function (ev) {
                    var $this = $(this);

                    if ($this.hasClass('ok_button')) {
                        var pattern = relPath;
                        var regex = new RegExp(pattern, 'i');

                        location.href = relPath + 'login/' + location.href.replace(regex, '');
                    } else {
                        $notificationElement.hide();
                    }
                });

            msg(text);
        }

        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var collectionOfElements = [];
        var $verticalNavigation = $('#cd-vertical-nav > .form_item');

        if ($verticalNavigation.children('li#add-icon').length > 0) {
            var $addToWatchListElement = $('<\span class="add-watchlist page-action" data-require-auth="1" data-target="#add-icon a" data-msg="watchlist">');
            collectionOfElements.push($addToWatchListElement[0]);
        } else if ($verticalNavigation.children('li#remove-icon').length > 0) {
            var $removeFromWatchListElement = $('<\span class="remove-watchlist page-action" data-target="#remove-icon a">');
            collectionOfElements.push($removeFromWatchListElement[0]);
        }

        if ($verticalNavigation.children('li#rate-icon').length > 0) {
            var $ratePageElement = $('<\span class="rate-page page-action" data-require-auth="1" data-target="#rate-icon a" data-msg="rate">');
            collectionOfElements.push($ratePageElement[0]);
        }

        if ($verticalNavigation.children('li#print-icon').length > 0) {
            var $printPageElement = $('<\span class="print-page page-action" data-target="#print-icon a">');
            collectionOfElements.push($printPageElement[0]);
        }

        if ($verticalNavigation.children('li#save-icon').length > 0) {
            var $saveAsPDF = $('<\span class="save-pdf page-action" data-target="#save-icon a">');
            collectionOfElements.push($saveAsPDF[0]);
        }

        var $statsContainerTop = $('div#best_practice_stats div.stats, div#downloads_stats div.stats');
        if ($statsContainerTop.length > 0) {
            $()
                .pushStack(collectionOfElements.reverse())
                .hideBeforeTransition()
                .appendTo($statsContainerTop)
                .visibleToTabletOnly();

            var $clonedStatsContainerTop = new jQuery;

            if ($statsContainerTop.closest('div#downloads_stats').length < 1) {
                $clonedStatsContainerTop = $statsContainerTop
                    .closest('div#best_practice_stats')
                    .clone()
                    .hideBeforeTransition();

                $clonedStatsContainerTop[0].id = $clonedStatsContainerTop[0].id + '_bottom';

                $clonedStatsContainerTop
                    .insertAfter('.middle_page_text #content_main')
                    .visibleToTabletOnly();
            }

            $statsContainerTop
                .add($clonedStatsContainerTop)
                .find('div.stats')
                .addBack('div.stats')
                .on('click', '.page-action', function () {

                    var $this = $(this);
                    var targetSelector = $this.data('target');

                    if (!DBDN.utils.isSessionAuthenticated()
                        && typeof $this.data('requireAuth') !== 'undefined') {
                        var specificMsg = $this.data('msg');
                        showMsg(js_localize['page.actions.unauth_alert_text.' + specificMsg]);
                        $(window).scrollTop(0);

                        return;
                    }

                    if (targetSelector) {
                        $verticalNavigation.find(targetSelector).click();
                    }
                });
        }

        $(document).trigger('stats_loaded.DBDN_Tablet');
    });

    // Tablet only: Handle front_slider like carousels (bottom of some pages)
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var $sliderWrapper = $('.slider_wrapper');

        if ($sliderWrapper.length < 1) {
            return;
        }

        function setCarouselHeight() {
            $sliderWrapper.each(function () {
                var $singleWrapper = $(this);

                var maxItemHeight = isTabletViewport() ? $singleWrapper.children('.slider_title').height() + $singleWrapper.children('.slider_container').height() : 'auto';

                $singleWrapper.height(maxItemHeight);
            });
        }

        setCarouselHeight();

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            setCarouselHeight();
        });
    });

    // Tablet only: Handle destruction and initialization of carousels
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var eventSelector = '.front_slider:not(.carousel_basic), .mslide';
        var slyInstanceCollection = {};

        if ($(eventSelector).length < 1) {
            return;
        }

        $(document).on('slyLoaded', eventSelector, function (ev) {
            var $this = $(this);

            $this.generateUUID();
            if (isTabletViewport()) {
                ev.customData.sliderInstance.destroy();
            }

            slyInstanceCollection[$this.getUUID()] = ev.customData.sliderInstance;
        });

        function destroyCarousels() {
            for (slyInstance in slyInstanceCollection) {
                if (slyInstanceCollection.hasOwnProperty(slyInstance)) {
                    slyInstanceCollection[slyInstance].destroy();
                }
            }
        }

        function initializeCarousels() {
            for (slyInstance in slyInstanceCollection) {
                if (slyInstanceCollection.hasOwnProperty(slyInstance)) {
                    slyInstanceCollection[slyInstance].init();
                }
            }
        }

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            DBDN.utils.toggleTabletDesktopEvent(function () {
                if ($('#view-controls > a.view-grid.active').length < 1) {
                    initializeCarousels();
                }
            }, function () {
                destroyCarousels();
            }, ev.htmlViewportMode);
        });
    });

    // Tablet only: Design Manuals content page (basic) - Carousel at page bottom
    // And other pages where this type of carousel is used
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var $grid3cols = $('.grid-3cols:not(.manuals)').has('.page');

        if (!$grid3cols.length > 0) {
            return;
        }

        $grid3cols.wrapInner('<\div class="inner1-wrapper">');

        function setCarouselHeight() {
            $grid3cols.each(function () {
                var $carouselMainWrapper = $(this);
                var height = isTabletViewport() ? $carouselMainWrapper.find('.inner1-wrapper').height() : '';
                $carouselMainWrapper.css("height", height);
            });
        }

        setCarouselHeight();

        // The 'most*' components should be composed of 3 items when on desktop and 4 on tablet.
        // - This is needed because of the unreliable user-agent backend detection logic.
        if ($grid3cols.closest('.mostsomethingblocks').length > 0) {
            $grid3cols.each(function () {
                var $teasers = $(this).find('.page');
                if ($teasers.length >= 4) {
                    $teasers.last().visibleToTabletOnly();
                }
            });
        }

        $(document).on('tabletDesktopViewportChange.DBDN_General filteredAjaxDataLoaded.DBDN_General', function (ev) {
            setCarouselHeight();
        });
    });

    // Tablet only: Header - Hamburger menu
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var $header = $('header');
        var $rubricsWrapper = $header.find('#rubrics_wrapper').addClass('tablet-popup');
        var $activeMenu = $rubricsWrapper.find('ul#rubrics > li.active a');
        var childMenuItemsSelector = '.basic_nav_opened .content.tablet-only.basic-menu';

        if ($rubricsWrapper.length < 1) {
            return;
        }

        var $hamburgerButton = $('<\div class="hamburger">').addClass('tablet-popup-entry');

        var $linesContainer = $hamburgerButton
        .clone()
        .removeClass()
        .addClass('lines-container');

        $hamburgerButton
            .hideBeforeTransition()
            .append($linesContainer)
            .visibleToTabletOnly();

        $header.find('#navContainer > a.search_button').after($hamburgerButton);

        var scrollOldPosition = 0;
        var togglingHamburgerInProgress = false;
        $header.on('click', 'div.hamburger', function (ev) {
            if (togglingHamburgerInProgress) {
                return;
            }

            togglingHamburgerInProgress = true;
            var $window = $(window);

            if ($rubricsWrapper.is(':visible')) {
                hideBurgerMenu(function () {
                    $window.scrollTop(scrollOldPosition);

                    togglingHamburgerInProgress = false;
                });
            } else {
                scrollOldPosition = $window.scrollTop();

                if ($header.hasClass('sticky')) {
                    $window.scrollTop(0);
                }

                showBurgerMenu(function () {
                    $activeMenu.trigger('click');
                    closeShowedPopup('.basic_nav, .hamburger');

                    togglingHamburgerInProgress = false;
                });
            }
        });

        // Handle the active child menu items
        $(document).on('afterAttachingMenuChildItems.DBDN_General', function () {
            var activeUriRelativeToDocumentRoot = location.href.replace(relPath.substr(0, relPath.length - 1), '');

            var activeChildMenuItemSelector = childMenuItemsSelector + ' a[href="' + activeUriRelativeToDocumentRoot + '"]';
            activeChildMenuItemSelector += ', ' + childMenuItemsSelector + ' a[href="' + location.href + '"]';

            var $activeChildMenuItem = $(activeChildMenuItemSelector);

            clearPreviousActiveMenuChildPage();

            // Case-insensitive search until CSS4 has broader support
            if ($activeChildMenuItem.length < 1) {
                $activeChildMenuItem = $(childMenuItemsSelector + ' a[href]').filter(function () {
                    return this.href.toLowerCase() == activeUriRelativeToDocumentRoot.toLowerCase() || this.href.toLowerCase() == location.href.toLowerCase();
                });
            }

            if ($activeChildMenuItem.length > 0) {
                $activeChildMenuItem.addClass('active');

                if (!$activeChildMenuItem.hasClass('parent')) {
                    var parentClass = $activeChildMenuItem.data('parent');

                    if (typeof parentClass !== 'undefined') {
                        parentClass = '.' + parentClass;

                        $(childMenuItemsSelector).find(parentClass).addClass('active');
                    }
                }
            }
        });

        function showBurgerMenu(callback) {
            $rubricsWrapper.show();
            $hamburgerButton.addClass('active');

            menu.overlay(1, null, function () {
                if (typeof callback === 'function') {
                    callback();
                }
            });
        }

        function hideBurgerMenu(callback, hideRubricsWrapper, hideDuration) {
            if (typeof hideRubricsWrapper === 'undefined') {
                hideRubricsWrapper = true;
            }

            menu.hide(function () {
                if (hideRubricsWrapper) {
                    $rubricsWrapper.css('display', '');
                }

                $hamburgerButton.removeClass('active');

                if (typeof callback === 'function') {
                    callback();
                }
            }, hideDuration);
        }

        function isBurgerActive() {
            return $hamburgerButton.filter('.active').length > 0;
        }

        function isDesktopMenuActive() {
            return $('section.basic_nav_opened:visible').length > 0;
        }

        function clearPreviousActiveMenuChildPage() {
            $(childMenuItemsSelector + ' li a').removeClass('active');
        }

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            DBDN.utils.toggleTabletDesktopEvent(function () {
                if (isBurgerActive()) {
                    hideBurgerMenu(function () {
                        $rubricsWrapper.css('display', '');
                    }, false, 0);
                }
            }, function () {
                if (isDesktopMenuActive()) {
                    menu.hide(null, 0);
                }
            }, ev.htmlViewportMode);
        });
    });

    // Tablet only: Prefetch page menus
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var prefetchedMenusKey = 'prefetchedMenus_' + DBDN.utils.getSessionLanguage();
        var $rubricsWrapper = $('header #rubrics_wrapper');

        var fetchMenuContentReal = null;
        var menuShowReal = null;
        var menuHideReal = null;
        var menuItemRealClickHandler = null;
        var fetchMenuWaitingList = {};
        var storageEngine = null;

        // The use of the InMemoryStorage is primarily meant as a workaround
        // for a bug in Safari < 11 when using web store in private mode.
        try {
            storageEngine = sessionStorage;

            var tempKey = '__' + (new Date()).getTime();

            storageEngine.setItem(tempKey, true);
            storageEngine.removeItem(tempKey);
        } catch (e) {
            storageEngine = new DBDN.storage.InMemoryStorage('mainTopMenu');
        }

        // CTRL-F5 to reload the page and clear the prefetched menus
        $(document).keydown(function (ev) {
            if (ev.keyCode == 116 && ev.ctrlKey) {
                clearPrefetchedMenus();
            }
        });

        if (isTabletViewport()) {
            setupTabletEnvironment();
        }

        function setupDesktopEnvironment() {
            menu.fetchMenuContent = fetchMenuContentReal;
            menu.show = menuShowReal;
            menu.hide = menuHideReal;

            if (typeof menuItemRealClickHandler === 'function') {
                $('#rubrics li a')
                    .off('click')
                    .on('click', menuItemRealClickHandler);
            }
        }

        function setupTabletEnvironment() {
            fetchMenuContentReal = menu.fetchMenuContent;
            menuShowReal = menu.show;
            menuHideReal = menu.hide;

            var menuMainMenuItems = $('#rubrics li a');
            var menuMainMenuItemsEvents = menuMainMenuItems.length > 0 ? $._data($('#rubrics li a').eq(0)[0], 'events') || null : null;

            if (menuMainMenuItemsEvents !== null
                && menuMainMenuItemsEvents.hasOwnProperty('click')
                && Array.isArray(menuMainMenuItemsEvents.click)) {
                menuItemRealClickHandler = menuMainMenuItemsEvents.click[0].handler;
            }

            menu.fetchMenuContent = function (menuId, callback) {
                var prefetchedMenu = getPrefetchedMenu(menuId);

                if (prefetchedMenu) {
                    if (typeof callback === 'function') {
                        callback(prefetchedMenu);
                    }

                    return;
                }

                fetchMenuContentReal.call(this, menuId, function () {
                    callback.apply(null, arguments);

                    if (isItInFetchMenuWaitingList(menuId)) {
                        var waitingCallback = getFromFetchMenuWaitingList(menuId);

                        waitingCallback();

                        clearFetchMenuWaitingList();
                    }
                });
            };

            $rubricsWrapper
                .find('ul#rubrics > li')
                .each(function () {
                    var menuItem = this;

                    if (!isMenuAlreadyPrefetched(menuItem)) {
                        setTimeout(function () {
                            menu.fetchMenuContent(menuItem.id, function (data) {
                                addMenuToPrefetchedMenuCollection(menuItem.id, data);
                            });
                        }, 200);
                    }
                });

            // Show preloader while waiting for menu to be prefetched
            // and prevent fetching of menu items for which fetch requests were already
            // fired.
            menu.show = function (ev) {
                var showContext = this;
                var showArguments = arguments;
                var $this = $(showContext);

                Array.prototype.unshift.call(showArguments, ev);

                if (this instanceof HTMLAnchorElement) {
                    var menuId = $this.parent()[0].id;

                    if (isMenuAlreadyPrefetched(menuId)) {
                        menuShowReal.apply(showContext, showArguments);

                        return;
                    }

                    if (!isItInFetchMenuWaitingList(menuId)) {
                        showFetchWaitingPreloader();

                        addToFetchMenuWaitingList(menuId, function () {
                            Array.prototype.push.call(showArguments, function () {
                                hideFetchWaitingPreloader();
                            });

                            menuShowReal.apply(showContext, showArguments);
                        });

                        return false;
                    }
                }
            };

            menu.hide = function (callBack, hideDuration) {
                hideFetchWaitingPreloader();

                menuHideReal.call(this, callBack, hideDuration);
            };

            $('#rubrics li a')
                .off('click')
                .on('click', menu.show);
        }

        function isMenuAlreadyPrefetched(menuId) {
            var prefetchedMenu = getPrefetchedMenu(menuId);

            if (!prefetchedMenu) {
                return false;
            }

            return true;
        }

        function clearPrefetchedMenus() {
            storageEngine.removeItem(prefetchedMenusKey)
        }

        function getPrefetchedMenu(menuId) {
            var prefetchedMenuCollection = getPrefetchedMenuCollection();

            if (prefetchedMenuCollection.hasOwnProperty(menuId)) {
                return prefetchedMenuCollection[menuId];
            }

            return null;
        }

        function getPrefetchedMenuCollection() {
            return JSON.parse(storageEngine.getItem(prefetchedMenusKey) || "{}");
        }

        function addMenuToPrefetchedMenuCollection(menuId, menuContent) {
            var prefetchedMenuCollection = getPrefetchedMenuCollection();

            if (!prefetchedMenuCollection.hasOwnProperty(menuId)) {
                prefetchedMenuCollection[menuId] = menuContent;
            }

            storageEngine.setItem(prefetchedMenusKey, JSON.stringify(prefetchedMenuCollection))
        }

        function addToFetchMenuWaitingList(menuId, callback) {
            fetchMenuWaitingList[menuId] = callback;
        }

        function clearFetchMenuWaitingList() {
            fetchMenuWaitingList = {};
        }

        function isItInFetchMenuWaitingList(menuId) {
            return fetchMenuWaitingList.hasOwnProperty(menuId);
        }

        function getFromFetchMenuWaitingList(menuId) {
            return fetchMenuWaitingList[menuId];
        }

        function showFetchWaitingPreloader() {
            $('#basic_preloader').show();
        }

        function hideFetchWaitingPreloader() {
            $('#basic_preloader').hide();
        }

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            DBDN.utils.toggleTabletDesktopEvent(function () {
                setupDesktopEnvironment();
            }, function () {
                setupTabletEnvironment();
            }, ev.htmlViewportMode);
        });
    });

    // Tablet only: top level overview page - list/view toggle switch, headers, etc.
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var desktopContentSelectorCollection = ['#downloadsForm',
            '.best-practice > .blocks_wrap',
            ':not(.best-practice) > .blocks_wrap',
            '#myDownloadsForm',
            '#watchlistForm'];

        var $desktopContent = null;

        function init() {
            updateDesktopContent();

            if (isTabletViewport()) {
                setupTabletEnvironment();
            }
        }

        function updateDesktopContent() {
            $(desktopContentSelectorCollection.join(','))
                .each(function () {
                    var $this = $(this);
                    var $children = $this.children();

                    if ($children.length > 0) {
                        $desktopContent = $children.clone(true, true);
                        $desktopContent.$parentObject = $this;

                        return false;
                    }
                });
        }

        function updateTabletContent() {
            setupTabletEnvironment();
        }

        function setupDesktopEnvironment() {
            if ($desktopContent === null) {
                return;
            }

            $desktopContent
                .$parentObject
                .hide()
                .empty()
                .append($desktopContent)
                .show();
        }

        function setupTabletEnvironment() {
            // Downloads page
            $('#downloadsForm').hide();

            $('#downloadsForm h1.block_title').each(function () {
                $(this).next('a').addBack().wrapAll('<\div class="second-row-info cf">');
            });
            $('#downloadsForm #view-controls').each(function () {
                $(this).next('h1.hct,h1.single').addBack().wrapAll('<\div class="first-row-info cf">');
            });

            $('#downloadsForm .grid-4cols > div[style*="clear:both"]').remove();
            $('#downloadsForm').show();

            // Bestpractice page
            $('.best-practice > .blocks_wrap').hide();

            $('.best-practice > .blocks_wrap h1.block_title').each(function () {
                $(this).next('a').addBack().wrapAll('<\div class="second-row-info cf">');
            });
            $('.best-practice > .blocks_wrap #view-controls').each(function () {
                $(this).next('h1.hct, h1').addBack().wrapAll('<\div class="first-row-info cf">');
            });

            $('.best-practice > .blocks_wrap .grid-4cols > div[class="cf"]').remove();

            $('.best-practice > .blocks_wrap').show();

            // Design manuals & Corporate Identity
            $(':not(.best-practice) > .blocks_wrap').hide();

            $(':not(.best-practice) > .blocks_wrap h2:first-child').each(function () {
                $(this).next('a').addBack().wrapAll('<\div class="second-row-info cf">');
            });
            $(':not(.best-practice) > .blocks_wrap #view-controls').each(function () {
                $(this).next('h1.hct, h1').addBack().wrapAll('<\div class="first-row-info cf">');
            });

            $(':not(.best-practice) > .blocks_wrap .grid-4cols > div[class="cf"]').remove();

            $(':not(.best-practice) > .blocks_wrap').show();

            // Download cart & Watchlist
            $('#myDownloadsForm, #watchlistForm').hide();

            $('#myDownloadsForm .grid-4cols > div[class="cf"], #watchlistForm .grid-3cols > div[class="cf"]').remove();

            $('#myDownloadsForm, #watchlistForm').show();
        }
        init();

        $(document).on('filteredAjaxDataLoaded.DBDN_General', function () {
            updateDesktopContent();
            updateTabletContent();
        });

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            DBDN.utils.toggleTabletDesktopEvent(function () {
                setupDesktopEnvironment();
            }, function () {
                updateDesktopContent();
                setupTabletEnvironment();
            }, ev.htmlViewportMode);
        });
    });

    // Tablet only: Glossary page - remove margin from the last letter on each row
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var clearedCollection = [];

        function clearMarginStyleFromPreviousLastLetter() {
            if (clearedCollection.length > 0) {
                clearedCollection.pop().css('margin-right', '');
            }
        }

        function removeMarginFromLastLetter() {
            if (!isTabletViewport()) {
                return;
            }

            clearMarginStyleFromPreviousLastLetter();

            var letterPerRow = Math.floor(($('.glossary-navigation').width() - 50) / 58.8) + 1;
            var $lastElement = $('.glossary-navigation a:nth-of-type(' + letterPerRow + 'n) div');
            $lastElement.css('margin-right', 0);

            clearedCollection.push($lastElement);
        }

        removeMarginFromLastLetter();

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            DBDN.utils.toggleTabletDesktopEvent(function () {
                clearMarginStyleFromPreviousLastLetter();
            }, function () {
                removeMarginFromLastLetter();
            }, ev.htmlViewportMode);
        });
    });

    // Tablet only: Filters
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var $basicNavElement = $('.basic_nav');
        var $menuLabels = $basicNavElement.children('.menu_label').addClass('tablet-popup-entry');
        var filterCompactSelectionReal = null;
        var me = {};

        if ($menuLabels.length < 1) {
            return;
        }

        $basicNavElement = $menuLabels.parent();
        var $filterPanel = $basicNavElement.children('.filter-panel');
        var $filterPanelTitle = $filterPanel.find('.filter-panel-title');
        var $filterPanelSelected = $filterPanel.children('#filter-panel-selected');
        var $window = $(window);
        var basicNavElementIDName = 'tabletFilters';
        var basicNavElementIDSelector = '#' + basicNavElementIDName;
        var sortByFiltersContainerSelector = '.content.second > .level3_nav';
        var collectionOfSortBySelectorsExcludedFromHiddenCheckboxLogic = ['#title_filter'];
        var sortByFiltersWithHiddenCheckboxSelector = basicNavElementIDSelector.prependCSSSelector('.tablet')
            + ' ' + sortByFiltersContainerSelector + ' > li:not(' + collectionOfSortBySelectorsExcludedFromHiddenCheckboxLogic.join(',') + ') .filter_title';
        var sortByFiltersSelector = basicNavElementIDSelector.prependCSSSelector('.tablet')
            + ' ' + sortByFiltersContainerSelector + ' > li .filter_title';
        var hideSortByCheckboxClassName = 'hide-sortby-checkbox';
        var sortByFiltersInputSelector = basicNavElementIDSelector.prependCSSSelector('.tablet')
            + ' ' + sortByFiltersContainerSelector + ' .level4_nav input';
        var selectedSortByFilterClassName = 'selected';
        //var $filterPanelForm = $filterPanel.children('form');

        $filterPanelSelected.addClass('tablet-popup');
        $basicNavElement.children('.menu_label, .content').addClass('tablet-popup');

        $basicNavElement.addClass('filter cf');
        $basicNavElement[0].id = basicNavElementIDName;

        setupTabletEnvironment();
        setupGeneralEnvironment();

        $(document).on('click', '.tablet .basic_nav > .menu_label', function (ev) {
            var $this = $(this);

            toggleFilterFlyout($this);
            closeShowedPopup('.basic_nav > .menu_label');
        });

        $(document).on('click',
            basicNavElementIDSelector.prependCSSSelector('.tablet'),
            function () {
                if ($(this).parent().hasClass('sticky')) {
                    $window.scrollTop(0);
                }
            });

        $(document).on('filteredAjaxDataLoaded.DBDN_General', function () {
            hideFilterFlyout();
        });

        $(document).on('hideFilters.DBDN_Tablet', function () {
            hideFilterFlyout();
        });

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            DBDN.utils.toggleTabletDesktopEvent(function () {
                hideFilterFlyout();
                hideActiveOptionsFlyout();
                setupDesktopEnvironment();
            }, function () {
                $("span.filter_close").trigger('click');
                hideActiveOptionsFlyout();
                setupTabletEnvironment();
                filter.compactSelection();
            }, ev.htmlViewportMode);
        });

        function setupGeneralEnvironment() {
            if (isDesktopViewport()) {
                $basicNavElement.hideForTablet();
            }

            hideSortByFilterCheckboxes();
        }

        function setupDesktopEnvironment() {
            unbindTabletEvents();
            me.reverseChangesMadeByTabletProxyFunctions();

            $basicNavElement.hideForTablet();

            $basicNavElement
                .find('#remove_filters')
                .appendTo($basicNavElement);

            $filterPanel
                .insertBefore($basicNavElement.children('#remove_filters'));

            $basicNavElement.hideForDesktop(false);
        }

        function setupTabletEnvironment() {
            if (!isTabletViewport()) {
                return;
            }

            setupTabletProxyFunctions();

            $basicNavElement.hideForDesktop();

            $basicNavElement
                .children('.unchecked-all')
                .hideBeforeTransition()
                .insertAfter($filterPanelTitle)
                .showAfterTransition();

            $filterPanel
                .hideBeforeTransition()
                .prependTo($basicNavElement)
                .showAfterTransition();

            synchronizeSortByFilterState();
            bindTabletEvents();

            $basicNavElement.hideForTablet(false);
        }

        function setupTabletProxyFunctions() {
            filterCompactSelectionReal = filter.compactSelection;
            var emptyActiveOptionsContent = js_localize['filter.active_options.empty'];
            var $nonEmptyActiveOptionsContent = $filterPanelTitle
                .contents()
                .clone();
            var emptyClassName = 'empty-filter';
            var jqueryShow = jQuery.fn.show;

            filter.compactSelection = function () {
                if (filter.getCountOfSelectedFilterOptions() < 1) {
                    $filterPanel.addClass(emptyClassName);
                    $filterPanelTitle
                        .empty()
                        .text(emptyActiveOptionsContent);
                } else {
                    $filterPanel.removeClass(emptyClassName);
                    $filterPanelTitle
                        .empty()
                        .append($nonEmptyActiveOptionsContent);
                }

                filterCompactSelectionReal.apply(this, arguments);
            };

            jQuery.fn.show = function () {
                if (this.hasClass(emptyClassName)) {
                    this[0].style.setProperty('display', 'block', 'important');

                    return this;
                }

                return jqueryShow.apply(this, arguments);
            };

            me.reverseChangesMadeByTabletProxyFunctions = function () {
                filter.compactSelection = filterCompactSelectionReal;

                $filterPanel.removeClass(emptyClassName);
                $filterPanelTitle
                    .empty()
                    .append($nonEmptyActiveOptionsContent);

                jQuery.fn.show = jqueryShow;
            }
        }

        function unbindTabletEvents() {
            $(document).off('change', sortByFiltersInputSelector);
            $(document).off('click', sortByFiltersWithHiddenCheckboxSelector);
        }

        function bindTabletEvents() {
            $(document).on('change', sortByFiltersInputSelector, function (ev) {
                var $this = $(this);

                clearSortByFilterStateClass();
                setSortByFilterState($this);
            });

            $(document).on('click', sortByFiltersWithHiddenCheckboxSelector, function () {
                var $this = $(this);
                $this.next('li')
                    .children('input[type=checkbox]')
                    .click()
            });
        }

        function hideActiveOptionsFlyout() {
            $filterPanelSelected.removeClass('active');
            menu.overlay(0);
        }

        function hideFilterFlyout(leaveOverlayActive) {
            leaveOverlayActive = leaveOverlayActive || false;
            $menuLabels.removeClass('active');

            if (!leaveOverlayActive) {
                menu.overlay(0);
            }
        }

        function toggleFilterFlyout($filterObj) {
            if ($filterObj.hasClass('active')) {
                $filterObj.removeClass('active');
                menu.overlay(0);
            } else {
                menu.overlay(1);
                $menuLabels.removeClass('active');
                $filterObj.addClass('active');
            }
        }

        function hideSortByFilterCheckboxes() {
            $(sortByFiltersWithHiddenCheckboxSelector).next('li').addClass(hideSortByCheckboxClassName);
        }

        function setSortByFilterState($input) {
            var $filterTitleElement = $input.parent().prev('.filter_title');

            if ($input.prop('checked')) {
                $filterTitleElement.addClass(selectedSortByFilterClassName);
            }
        }

        function clearSortByFilterStateClass() {
            var $sortByFilters = $(sortByFiltersSelector);

            $sortByFilters.removeClass(selectedSortByFilterClassName);
        }

        function synchronizeSortByFilterState() {
            clearSortByFilterStateClass();

            $(sortByFiltersInputSelector).each(function () {
                setSortByFilterState($(this));
            });
        }
    });

    // Tablet only: Make embedded videos size fluid
    $(function () {
        //    if (!(isTabletViewport() || isDesktopViewport())) {
        //        return;
        //    }

        var $allEmbeddedVideos = $('iframe[src*="youtube"]');
        var videoWrapperSelector = 'video-fluid-container';

        function wrapVideoElement() {
            var wrap_selector = 'div.' + videoWrapperSelector;
            var wrap_element = '<\div class="' + videoWrapperSelector + '">';
            $allEmbeddedVideos.each(function () {
                var parent = $(this).parent(wrap_selector);
                if (0 === parent.length) {
                    $(this).wrap(wrap_element)
                    .show()
                    .closest('p').addClass('embedded-video');
                } else {
                    // If already wrapped
                    $(this).show();
                }
            });
        }
        wrapVideoElement();

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            wrapVideoElement();

            //        DBDN.utils.toggleTabletDesktopEvent(function () {
            //            $allEmbeddedVideos.unwrap(videoWrapperSelector);
            //        }, function () {
            //            wrapVideoElement();
            //        }, ev.htmlViewportMode);
        });
    });

    // Tablet only: New zoom-in behaviour of the responsive popup images
    (function () {
        if (isZoomInAllowedToRun()) {
            preventLyteBoxInitialization();
        }

        $(function () {
            var allPopupImagesSelector = 'a[rel*="lytebox"], a[rel*="lyteshow"], a[rel*="lyteframe"]';
            var closeButtonSelector = '.zoomInImgContainer a.close_button';
            var globalHtmlClass = 'zoomed-img-modal';
            var $outerContainer = $('<\div class="zoomInImgContainer">').hide();
            var $innerContainer = $('<\div class="innerImgContainer">');
            var $img = $('<\img class="zoomIn">');
            var $closeButton = $('<div class="buttons_wrapper"><\a class="close_button"></div>');
            var $htmlElement = $('html');
            var $window = $(window);
            var totalMovedDistanceX = 0;
            var totalMovedDistanceY = 0;
            var maxMoveDistanceX = 0;
            var maxMoveDistanceY = 0;
            var scrollOldPosition = 0;
            var outerContainerHeightCorrectionFactor = 0.9375;
            var outerContainerHeightOnceCorrected = false;

            $innerContainer
                .append($img)
                .appendTo($outerContainer);

            $outerContainer.append($closeButton);
            $outerContainer.appendTo(document.body);

            $img.on('load', function () {
                showImgModal();
            });

            allPopupImagesSelector = allPopupImagesSelector
                .prependCSSSelector('.tablet')
                .combineOrCSSSelector(allPopupImagesSelector.prependCSSSelector('.mobile'))
                .combineOrCSSSelector(allPopupImagesSelector.prependCSSSelector('.scaled-viewport.desktop'));
            $(document).on('click', allPopupImagesSelector, function (ev) {
                ev.preventDefault();

                var newImgSource = this.getAttribute('href');

                $img[0].src = '';
                if (newImgSource) {
                    togglePreloader();
                    menu.overlay(1);

                    $img.one('error', function () {
                        this.src = '';
                        this.src = $(this).data('errorSource');
                    });

                    $img.data('errorSource', $(this).children('img')[0].src);
                    $img[0].src = newImgSource;
                }
            });

            $(document).on('click', closeButtonSelector, function () {
                hideImgModal();
            });

            $img.on('movestart', function () {
            })
            .on('move', function (ev) {
                totalMovedDistanceX += ev.deltaX;
                totalMovedDistanceY += ev.deltaY;

                if (totalMovedDistanceX > 0) {
                    totalMovedDistanceX = 0;
                }

                if (totalMovedDistanceY > 0) {
                    totalMovedDistanceY = 0;
                }

                if (Math.abs(totalMovedDistanceX) >= maxMoveDistanceX) {
                    totalMovedDistanceX = -maxMoveDistanceX;
                }

                if (Math.abs(totalMovedDistanceY) >= maxMoveDistanceY) {
                    totalMovedDistanceY = -maxMoveDistanceY;
                }

                moveImage(totalMovedDistanceX, totalMovedDistanceY);
            });

            $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
                DBDN.utils.toggleTabletDesktopEvent(function () {
                    if (!DBDN.utils.isDesktopScaledViewport()) {
                        hideImgModal();
                        lyteBoxInitialize();
                    }
                }, function () {
                    destroyLyteBoxInstance();
                    // Prevent an attempt to initialize at later stage in the execution flow.
                    preventLyteBoxInitialization();
                }, ev.htmlViewportMode);
            });

            $(document).on('tabletTabletViewportChange.DBDN_General mobileMobileViewportChange.DBDN_General tabletDesktopViewportChange.DBDN_General desktopDesktopViewportChange.DBDN_General', function () {
                if (!isZoomInAllowedToRun()) {
                    return;
                }

                if (!$outerContainer.is(':visible')) {
                    return;
                }

                correctContainerHeightOnBuggyBrowsers();
                updateMaxMoveDistance();

                if (Math.abs(totalMovedDistanceX) >= maxMoveDistanceX) {
                    totalMovedDistanceX = -maxMoveDistanceX;
                }

                if (Math.abs(totalMovedDistanceY) >= maxMoveDistanceY) {
                    totalMovedDistanceY = -maxMoveDistanceY;
                }

                moveImage(totalMovedDistanceX, totalMovedDistanceY);
            });

            function clearContainerCorrections() {
                $outerContainer.css('max-height', '');
                $innerContainer.css('max-height', '');
                outerContainerHeightOnceCorrected = false;
            }

            function correctContainerHeightOnBuggyBrowsers() {
                var outerContainerBoundingRectangle = DBDN.utils.getBoundingClientRectOfHiddenElement($outerContainer);

                if (typeof outerContainerBoundingRectangle.height === 'undefined' || !outerContainerBoundingRectangle) {
                    return;
                }

                // The min method is necessary because some browsers behave differently in portrait and
                // landscape orientations. Amazed, huh?
                var windowHeightMinusTopAndBottomBrowserBars = Math.min($(window).height(), window.innerHeight);

                if (outerContainerBoundingRectangle.height > windowHeightMinusTopAndBottomBrowserBars || outerContainerHeightOnceCorrected) {
                    var maxHeight = windowHeightMinusTopAndBottomBrowserBars * outerContainerHeightCorrectionFactor;

                    $outerContainer.css('max-height', maxHeight + 'px');
                    $innerContainer.css('max-height', maxHeight + 'px');

                    outerContainerHeightOnceCorrected = true;
                }
            }

            function goBackToOldScrollPosition() {
                $window.scrollTop(scrollOldPosition);
            }

            function updateOldScrollValue() {
                scrollOldPosition = $window.scrollTop();
            }

            function hideImgModal() {
                menu.overlay(0);
                $outerContainer.hide();
                $htmlElement.removeClass(globalHtmlClass);
                clearContainerCorrections();
                goBackToOldScrollPosition();
                restoreInterferingFunctionalities();
            }

            function showImgModal() {
                updateOldScrollValue();
                resetTotalMovedDistance();
                resetImagePositionToInitialCoordinates();
                correctContainerHeightOnBuggyBrowsers();
                togglePreloader();
                $htmlElement.addClass(globalHtmlClass);
                $outerContainer.show();
                updateMaxMoveDistance();
                suspendInterferingFunctionalities();
            }

            function togglePreloader() {
                var $preloader = $('#basic_preloader');

                if ($preloader.is(':visible')) {
                    $preloader.hide();
                } else {
                    $preloader.show();
                }
            }

            function resetTotalMovedDistance() {
                totalMovedDistanceX = 0;
                totalMovedDistanceY = 0;
            }

            function resetImagePositionToInitialCoordinates() {
                moveImage(0, 0);
            }

            function moveImage(x, y) {
                $img.css('transform', 'translate3d(' + x + 'px, ' + y + 'px, 0px)');
            }

            function updateMaxMoveDistance() {
                maxMoveDistanceX = $img.outerWidth() - $outerContainer.width();
                maxMoveDistanceY = $img.outerHeight() - $outerContainer.height();
            }
        });

        function isZoomInAllowedToRun() {
            if (isTabletViewport()
                || isMobileViewport()
                || DBDN.utils.isDesktopScaledViewport()) {
                return true;
            }

            return false;
        }

        function suspendInterferingFunctionalities() {
            $(document).trigger('suspendComponentClosingOnTabletDesktop.DBDN_General');
        }

        function restoreInterferingFunctionalities() {
            $(document).trigger('unsuspendComponentClosingOnTabletDesktop.DBDN_General');
        }

        function preventLyteBoxInitialization() {
            var myLytebox = {};
            window.myLytebox = myLytebox;

            myLytebox.updateLyteboxItems = function () {
                // simply do nothing
            };
        }

        function lyteBoxInitialize() {
            if (typeof window.myLytebox !== 'undefined'
                && window.myLytebox instanceof LyteBox) {
                return;
            }

            window.myLytebox = null;
            delete window.myLytebox;

            initLytebox();
        }

        function destroyLyteBoxInstance() {
            if (typeof window.myLytebox !== 'undefined'
                && window.myLytebox instanceof LyteBox) {
                window.myLytebox.destroy();
            }
        }
    }());

    // Tablet/Mobile only: Add custom horizontal scrolls to specific components
    $(function () {
        var listOfSelectorsToAddScrollBarTo = [{
            mainParentContainerSelector: '.grid-3cols:not(.manuals)',
            attachToSelector: '.grid-3cols:not(.manuals)',
            viewportSelector: '.grid-3cols:not(.manuals)',
            scrollableContentSelector: '.inner1-wrapper',
            scrollBarClassList: 'most-something',
            scrollBarInlineStyles: { 'width': '-webkit-calc(100% - 20px)', 'width': 'calc(100% - 20px)' },
            scrollBarHandleType: 'fixed',
            applyCondition: function () {
                if (isTabletViewport()) {
                    return true;
                }

                return false;
            },
            position: { 'offset_top': 20 }
        },
        {
            mainParentContainerSelector: 'table',
            attachToSelector: '#content_main table',
            viewportSelector: 'table',
            scrollableContentSelector: 'tbody',
            applyCondition: function () {
                if (isMobileViewport()) {
                    return true;
                }

                return false;
            },
            hideBrowserScroll: true,
            position: { 'offset_top': 10 }
        },
        {
            mainParentContainerSelector: '.slider_wrapper',
            attachToSelector: '.slider_wrapper',
            viewportSelector: '.slider_wrapper',
            scrollableContentSelector: '.slider_container',
            scrollBarClassList: 'most-something',
            scrollBarInlineStyles: { 'width': '-webkit-calc(100% - 20px)', 'width': 'calc(100% - 20px)' },
            scrollBarHandleType: 'fixed',
            applyCondition: function () {
                if (isTabletViewport()) {
                    return true;
                }

                return false;
            },
            position: { 'offset_top': 20 }
        },
        {
            mainParentContainerSelector: '.gallery-main-wrapper',
            attachToSelector: '.gallery-main-wrapper',
            viewportSelector: '.galCropper',
            scrollableContentSelector: '.galMover',
            initializeOnEvent: 'singleImageGalleryCarouselLoaded.DBDN_General',
            initializeOnEventTargetElement: document,
            updateScrollDimensionPropertiesOnEvent: 'afterGallerySetupCroppers.DBDN_General',
            observeValueCallback: function () {
                if (arguments[0][0].target.style.left !== galleryMoverOldLeftPosition) {
                    galleryMoverOldLeftPosition = arguments[0][0].target.style.left;

                    return Math.abs(parseFloat(arguments[0][0].target.style.left));
                }
            },
            observeOptions: {
                attributes: true,
                attributeFilter: ['style']
            },
            applyCondition: function () {
                if (isMobileViewport()) {
                    return true;
                }

                return false;
            },
            position: { 'offset_top': 10 }
        }];
        var $body = $(document.body);
        var galleryMoverOldLeftPosition = null;
        var appName = 'custom_scrollbar';
        var eventWaitingKey = appName + '_' + 'eventWaiting';

        function ScrollBar(targetElement, userTargetOptions) {
            if (typeof ScrollBar.getPublicInstance(targetElement) !== 'undefined') {
                return ScrollBar.getPublicInstance(targetElement);
            }

            var targetOptions = {
                scrollSourceType: 'browserScroll',
                scrollBarClassList: '',
                behaviour: 'wrap',
                scrollBarInlineStyles: {},
                scrollBarHandleType: 'dynamic',
                applyCondition: '',
                hideBrowserScroll: false, // works only with 'wrap' behaviour
                updateScrollBarHandlePositionManually: false,
                initializeOnEvent: null,
                initializeOnEventTargetElement: null,
                observeValueCallback: null,
                observeOptions: {},
                pollForChangesAfterExternalUpdateOfScrollDimensionProperties: false,
                updateScrollDimensionPropertiesOnEvent: null,
                updateScrollDimensionPropertiesOnEventTargetElement: null
            };
            var $scrollBarElement = null;
            var $scrollBarHandleElement = null;
            var $scrollableContentElement = null;
            var $mainParentContainerElement = null;
            var $viewportContainerElement = null;
            var $targetElementToAttachScrollTo = $(targetElement);
            var scrollEventObj = null;
            var resizeEventObj = null;
            var lastScrollXPos = 0;
            var scrollXFactor = 0;
            var $scrollBarMainWrapper = $('<\div class="custom-scrollbar-wrapper">');
            var instanceInitialized = false;
            var instanceDestroyed = false;
            var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
            var mutationObserverInstance = null;
            var publicInstance = {};

            $.extend(targetOptions, userTargetOptions);

            function init() {
                setRefToMainParentContainerElement();
                setRefToViewportContainerElement();
                setRefToScrollableContentElement();

                if (!isScrollNeeded()) {
                    return false;
                }

                setRefToScrollBarElement();
                setRefToScrollBarHandle();

                setupScrollBarBehaviour();
                setupScrollBarInlineStyles();

                updateScrollDimensionProperties();

                if (!targetOptions.updateScrollBarHandlePositionManually) {
                    bindEvents();
                    bindObservers();
                }

                $scrollBarElement.addClass(targetOptions.scrollBarClassList);

                $scrollBarElement.show();

                hideBrowserScroll();

                checkTheScrollableComponentForSizeChanges();

                storePublicInstance();

                instanceInitialized = true;
            }

            function setupScrollBarInlineStyles() {
                $scrollBarElement.css(targetOptions.scrollBarInlineStyles);
            }

            function setupScrollBarBehaviour() {
                switch (targetOptions.behaviour) {
                    case 'attach':
                        $body.append($scrollBarElement);
                        break;
                    case 'wrap':
                        $targetElementToAttachScrollTo.wrap($scrollBarMainWrapper);
                        $targetElementToAttachScrollTo.parent().append($scrollBarElement);
                        break;
                    default:
                }
            }

            function bindObservers() {
                if (targetOptions.observeValueCallback === null) {
                    return;
                }

                if (mutationObserverInstance === null) {
                    mutationObserverInstance = new MutationObserver(function (mutations) {
                        var newPositionValue = targetOptions.observeValueCallback(mutations);

                        updateScrollHandlePosition(newPositionValue);
                    });
                }

                mutationObserverInstance.observe($scrollableContentElement[0], targetOptions.observeOptions);
            }

            function bindEvents() {
                if (targetOptions.observeValueCallback === null) {
                    scrollEventObj = new DBDN.events.ScrollEvent($scrollableContentElement, 0, function (ev) {
                        var currentScrollXPos = $scrollableContentElement.scrollLeft();

                        updateScrollHandlePosition(currentScrollXPos);
                    });
                }

                if (targetOptions.updateScrollDimensionPropertiesOnEvent !== null) {
                    var target = getUpdateScrollDimensionPropertiesOnEventTarget();

                    $(target).on(targetOptions.updateScrollDimensionPropertiesOnEvent, function () {
                        updateScrollDimensionProperties();
                    });
                }
            }

            function isScrollNeeded() {
                if (typeof targetOptions.applyCondition === 'function'
                    && !targetOptions.applyCondition()) {
                    return false;
                }

                if ($scrollableContentElement[0].scrollWidth > $viewportContainerElement.width()) {
                    return true;
                }

                return false;
            }

            function setRefToScrollBarHandle() {
                $scrollBarHandleElement = $scrollBarElement.find('.handle');
            }

            function setRefToScrollBarElement() {
                $scrollBarElement = getNewScrollBarElement().hide();
            }

            function setRefToViewportContainerElement() {
                $viewportContainerElement = $targetElementToAttachScrollTo.closest(targetOptions.viewportSelector);

                if ($viewportContainerElement.length < 1) {
                    $viewportContainerElement = $targetElementToAttachScrollTo.find(targetOptions.viewportSelector);
                }
            }

            function setRefToMainParentContainerElement() {
                $mainParentContainerElement = $targetElementToAttachScrollTo.closest(targetOptions.mainParentContainerSelector);

                if ($mainParentContainerElement.length < 1) {
                    throw new Error('Cannot update the ref. to the main parent container.');
                }
            }

            function setRefToScrollableContentElement() {
                $scrollableContentElement = $mainParentContainerElement.find(targetOptions.scrollableContentSelector);
            }

            function updateScrollDimensionProperties() {
                updateScrollBarPosition();
                updateScrollHandleSize();

                scrollXFactor = ($scrollBarElement.width() - $scrollBarHandleElement.width()) / ($scrollableContentElement[0].scrollWidth - $viewportContainerElement.width());
            }

            function updateScrollHandleSize() {
                if (targetOptions.scrollBarHandleType == 'fixed') {
                    return;
                }

                var newWidthSize = ($viewportContainerElement.width() / $scrollableContentElement[0].scrollWidth) * $scrollBarElement.width();

                $scrollBarHandleElement.css('width', newWidthSize + 'px');
            }

            function updateScrollHandlePosition(newXPosition) {
                if (lastScrollXPos !== newXPosition) {
                    $scrollBarHandleElement.css('transform', 'translate3d(' + (newXPosition * scrollXFactor) + 'px, 0px, 0px)');

                    lastScrollXPos = newXPosition;
                }
            }

            function updateScrollBarPosition() {
                var targetDocsOffset = $targetElementToAttachScrollTo.offset();
                var targetHeight = $targetElementToAttachScrollTo.outerHeight();
                var newScrollVerticalPosition = targetDocsOffset.top + targetHeight + targetOptions.position.offset_top;

                if (targetOptions.behaviour == 'wrap') {
                    //newScrollVerticalPosition = $targetElementToAttachScrollTo.height() + targetOptions.position.offset_top;
                    $scrollBarElement.css('bottom', 'calc(0% - ' + targetOptions.position.offset_top + 'px)');
                } else {
                    $scrollBarElement.css('top', newScrollVerticalPosition + 'px');
                }
            }

            function checkTheScrollableComponentForSizeChanges() {
                var initialScrollWidth = $scrollableContentElement[0].scrollWidth;

                DBDN.utils.poll(function () {
                    var currentScrollWidth = $scrollableContentElement[0].scrollWidth;

                    if (initialScrollWidth < currentScrollWidth || initialScrollWidth > currentScrollWidth) {
                        return true;
                    }

                    return false;
                }, function () {
                    initialScrollWidth = $scrollableContentElement[0].scrollWidth;

                    updateScrollDimensionProperties();
                }, function () {
                    return instanceDestroyed;
                }, 300, 10000);
            }

            function getUpdateScrollDimensionPropertiesOnEventTarget() {
                if (targetOptions.updateScrollDimensionPropertiesOnEventTargetElement !== null) {
                    return targetOptions.updateScrollDimensionPropertiesOnEventTargetElement;
                }

                return $targetElementToAttachScrollTo;
            }

            function getNewScrollBarElement() {
                return $(getScrollBarTemplate());
            }

            function getScrollBarTemplate() {
                var html = '<div class="custom-scrollbar"><div class="handle"></div></div>';

                return html;
            }

            function hideBrowserScroll(show) {
                if (targetOptions.hideBrowserScroll
                    && targetOptions.behaviour == 'wrap') {
                    var scrollableFixedHeight = $scrollableContentElement.outerHeight();

                    $viewportContainerElement
                        .addClass('default-scroll-hidden');

                    $viewportContainerElement[0].style.setProperty('height', scrollableFixedHeight + 'px', 'important');

                    $scrollableContentElement
                        .addClass('scrollable');
                }
            }

            function showBrowserScroll() {
                if (targetOptions.hideBrowserScroll
                    && targetOptions.behaviour == 'wrap') {
                    $viewportContainerElement
                        .css({ 'height': '' })
                        .removeClass('default-scroll-hidden');

                    $scrollableContentElement
                        .removeClass('scrollable');
                }
            }

            function setWaitingOnEventFlag(waiting) {
                if (typeof waiting === 'undefined') {
                    waiting = false;
                }

                $.data(targetElement, eventWaitingKey, waiting);
            }

            function storePublicInstance() {
                $.data(targetElement, appName, publicInstance);
            }

            function removePublicInstance() {
                $.removeData(targetElement, appName);
            }

            function destroy(partially) {
                if (mutationObserverInstance !== null) {
                    mutationObserverInstance.disconnect();
                }

                if (mutationObserverInstance === null) {
                    scrollEventObj.stopEvent();
                }

                if (partially !== true) {
                    removePublicInstance();
                }

                if (targetOptions.updateScrollDimensionPropertiesOnEvent !== null) {
                    $(getUpdateScrollDimensionPropertiesOnEventTarget()).off(targetOptions.updateScrollDimensionPropertiesOnEvent);
                }

                $scrollBarElement.remove();
				if($targetElementToAttachScrollTo.parent().is($scrollBarMainWrapper[0].className)){
                    $targetElementToAttachScrollTo.unwrap($scrollBarMainWrapper[0].className);
                }
                showBrowserScroll();
                instanceDestroyed = true;
            }

            publicInstance.hide = function () {
                destroy(true);
            };

            publicInstance.reinit = function () {
                init();
            };

            publicInstance.destroy = function () {
                destroy();
            };

            publicInstance.updateScrollDimensionProperties = function () {
                updateScrollDimensionProperties();

                if (targetOptions.pollForChangesAfterExternalUpdateOfScrollDimensionProperties) {
                    checkTheScrollableComponentForSizeChanges();
                }
            };

            publicInstance.updateScrollBarHandlePosition = function (newXPosition) {
                if (targetOptions.updateScrollBarHandlePositionManually) {
                    updateScrollHandlePosition(newXPosition);
                }
            };

            publicInstance.isScrollNeeded = function () {
                return isScrollNeeded();
            };

            publicInstance.isInstanceInitialized = function () {
                return instanceInitialized;
            };

            publicInstance.isInstanceDestroyed = function () {
                return instanceDestroyed;
            };

            if (targetOptions.initializeOnEvent !== null) {
                setWaitingOnEventFlag(true);

                $(targetOptions.initializeOnEventTargetElement).one(targetOptions.initializeOnEvent, function () {
                    init();
                    setWaitingOnEventFlag(false);
                });
            } else {
                init();
            }

            return publicInstance;
        }

        ScrollBar.alreadyWaitingToBeInitializedOnEvent = function (targetElement) {
            var flag = $.data(targetElement, eventWaitingKey);

            if (typeof flag !== 'undefined' && flag) {
                return true;
            }

            return false;
        };

        ScrollBar.getPublicInstance = function (targetElement) {
            return $.data(targetElement, appName);
        };

        function createOrUpdateScrollBarInstances() {
            $.each(listOfSelectorsToAddScrollBarTo, function () {
                var options = this;

                $(options.attachToSelector).each(function () {
                    var existingInstance = ScrollBar.getPublicInstance(this);

                    if (typeof existingInstance !== 'undefined') {
                        if (existingInstance.isScrollNeeded() && !existingInstance.isInstanceDestroyed()) {
                            existingInstance.updateScrollDimensionProperties();
                        }

                        if (existingInstance.isScrollNeeded() && existingInstance.isInstanceDestroyed()) {
                            existingInstance.reinit();
                        }

                        if (!existingInstance.isScrollNeeded()) {
                            existingInstance.hide();
                        }

                        return true;
                    }

                    if (!ScrollBar.alreadyWaitingToBeInitializedOnEvent(this)) {
                        new ScrollBar(this, options);
                    }
                });
            });
        }

        if (!isDesktopViewport()) {
            createOrUpdateScrollBarInstances();
        }

        $(document).on('tabletDesktopViewportChange.DBDN_General tabletTabletViewportChange.DBDN_General mobileMobileViewportChange.DBDN_General filteredAjaxDataLoaded.DBDN_General', function (ev) {
            createOrUpdateScrollBarInstances();
        });
    });

    // Tablet/Desktop only: Change header banner with device specific image
    $(function () {
        if (isTabletViewport() || isDesktopViewport()) {
            $(document).on('tabletDesktopViewportChange.DBDN_General', function () {
                var $bannerVisibleImage = $('.banner_load_wrapper:visible img.simple');
                imageProcess.lazyLoadWithCallBack($bannerVisibleImage);
                imageProcess.updateLazyload();
            });
        }
    });

    // Mobile only: ADD and LIKE
    $(function () {
        if (!isMobileViewport()) {
            return;
        }
        $(document).on('click', 'nav#cd-vertical-nav li#add-icon a, nav#cd-vertical-nav li#rate-icon a, nav#cd-vertical-nav li#remove-icon a', function (event) {
            event.preventDefault();

            if (!DBDN.utils.isSessionAuthenticated()) {
                var what = $(this).closest('li').attr('id').split('-')[0];
                $(document).trigger('pageActions_showUnauthAlertText.DBDN_Tablet', js_localize['page.actions.unauth_' + what + '_alert_text']);
                $(window).scrollTop(0);
            } else {
                var $parent = $(this).parent();
                if ($parent[0].id == 'rate-icon' && $parent.hasClass('unactive')) {
                    $("#notification .alert_text").html(js_localize['page.rated'])
                    generateNotification();
                } else {
                    $parent.submit();
                }
            }
        });

        if (MOBILE) {
            $.each(['div#downloads:not(.topItem)', 'div.comment-form', 'div#contact_info', 'div#content_main'], function (index, selector) {
                var el = $(selector);
                if (1 === el.length) {
                    el.after(
                        $('<section />').attr({ id: 'clonedItems', style: 'position:relative;' }).append(
                                $('div#best_practice_stats').clone(),
                                $('div#downloads_stats').clone(),
                                $('nav#cd-vertical-nav').clone()
                            )
                    );
                    return false;
                }
            });

            $('div#hamburger').on('click', 'div.switch', function () {
                var p = $(this).parent();
                if (p.hasClass('active')) {
                    $('div#searchDiv > div.basic_nav').css('z-index', '');
                    menu.overlay(false);
                    p.removeClass('active');
                } else {
                    $('div#searchDiv > div.basic_nav').css('z-index', 4);
                    menu.overlay(true);
                    p.addClass('active');
                }
                window.scrollTo(0, 0);
            });

            $('h1.block_title + a').each(function () {
                var t = $(this), h = t.prev().height();
                t.css('margin-top', 20 + h / 2 + 'px');
                // console.log(h, 40 + h/2 + 'px');
            });

            // fix for #16454
            var glr = $('div.glossary-result');
            if (1 === glr.length)
                window.scrollTo(0, glr.offset().top);

            // fix for #16458
            $('div.basic_nav div.content.second ul.level4_nav').each(function () {
                var t = $(this), lis = t.find('li');

                if (2 !== lis.length)
                    return;

                var tit = $(lis[0]), fld = $(lis[1]), inp = fld.find('input');

                if (1 !== inp.length)
                    return;

                fld.hide();

                if (inp[0].checked)
                    tit.addClass('checked');


                tit.click(function () {
                    $(this).next().find('input').trigger('click');
                });


            });

            $('div.basic_nav div.content.second input').change(function () {
                $('div.basic_nav div.content.second ul.level4_nav li.filter_title').removeClass('checked');
                $(this).parent('li').prev().toggleClass('checked', this.checked);
            });
            // end of fix for #16458


            // fix for #16380
            $(document).on('click', 'div.filter-panel.no-empty', function (e) {
                if ('FORM' == e.target.tagName) {
                    $('div.filter-panel-title').trigger('click');
                }
            });


        } // if( MOBILE)


    });
}

/*! jQuery UI - v1.11.0 - 2014-06-26
* http://jqueryui.com
* Includes: core.js, widget.js, mouse.js, position.js, accordion.js, autocomplete.js, button.js, datepicker.js, dialog.js, draggable.js, droppable.js, effect.js, effect-blind.js, effect-bounce.js, effect-clip.js, effect-drop.js, effect-explode.js, effect-fade.js, effect-fold.js, effect-highlight.js, effect-puff.js, effect-pulsate.js, effect-scale.js, effect-shake.js, effect-size.js, effect-slide.js, effect-transfer.js, menu.js, progressbar.js, resizable.js, selectable.js, selectmenu.js, slider.js, sortable.js, spinner.js, tabs.js, tooltip.js
* Copyright 2014 jQuery Foundation and other contributors; Licensed MIT */

(function(e){"function"==typeof define&&define.amd?define(["jquery"],e):e(jQuery)})(function(e){function t(t,s){var n,a,o,r=t.nodeName.toLowerCase();return"area"===r?(n=t.parentNode,a=n.name,t.href&&a&&"map"===n.nodeName.toLowerCase()?(o=e("img[usemap=#"+a+"]")[0],!!o&&i(o)):!1):(/input|select|textarea|button|object/.test(r)?!t.disabled:"a"===r?t.href||s:s)&&i(t)}function i(t){return e.expr.filters.visible(t)&&!e(t).parents().addBack().filter(function(){return"hidden"===e.css(this,"visibility")}).length}function s(e){for(var t,i;e.length&&e[0]!==document;){if(t=e.css("position"),("absolute"===t||"relative"===t||"fixed"===t)&&(i=parseInt(e.css("zIndex"),10),!isNaN(i)&&0!==i))return i;e=e.parent()}return 0}function n(){this._curInst=null,this._keyEvent=!1,this._disabledInputs=[],this._datepickerShowing=!1,this._inDialog=!1,this._mainDivId="ui-datepicker-div",this._inlineClass="ui-datepicker-inline",this._appendClass="ui-datepicker-append",this._triggerClass="ui-datepicker-trigger",this._dialogClass="ui-datepicker-dialog",this._disableClass="ui-datepicker-disabled",this._unselectableClass="ui-datepicker-unselectable",this._currentClass="ui-datepicker-current-day",this._dayOverClass="ui-datepicker-days-cell-over",this.regional=[],this.regional[""]={closeText:"Done",prevText:"Prev",nextText:"Next",currentText:"Today",monthNames:["January","February","March","April","May","June","July","August","September","October","November","December"],monthNamesShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],dayNames:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],dayNamesShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],dayNamesMin:["Su","Mo","Tu","We","Th","Fr","Sa"],weekHeader:"Wk",dateFormat:"mm/dd/yy",firstDay:0,isRTL:!1,showMonthAfterYear:!1,yearSuffix:""},this._defaults={showOn:"focus",showAnim:"fadeIn",showOptions:{},defaultDate:null,appendText:"",buttonText:"...",buttonImage:"",buttonImageOnly:!1,hideIfNoPrevNext:!1,navigationAsDateFormat:!1,gotoCurrent:!1,changeMonth:!1,changeYear:!1,yearRange:"c-10:c+10",showOtherMonths:!1,selectOtherMonths:!1,showWeek:!1,calculateWeek:this.iso8601Week,shortYearCutoff:"+10",minDate:null,maxDate:null,duration:"fast",beforeShowDay:null,beforeShow:null,onSelect:null,onChangeMonthYear:null,onClose:null,numberOfMonths:1,showCurrentAtPos:0,stepMonths:1,stepBigMonths:12,altField:"",altFormat:"",constrainInput:!0,showButtonPanel:!1,autoSize:!1,disabled:!1},e.extend(this._defaults,this.regional[""]),this.regional.en=e.extend(!0,{},this.regional[""]),this.regional["en-US"]=e.extend(!0,{},this.regional.en),this.dpDiv=a(e("<div id='"+this._mainDivId+"' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"))}function a(t){var i="button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";return t.delegate(i,"mouseout",function(){e(this).removeClass("ui-state-hover"),-1!==this.className.indexOf("ui-datepicker-prev")&&e(this).removeClass("ui-datepicker-prev-hover"),-1!==this.className.indexOf("ui-datepicker-next")&&e(this).removeClass("ui-datepicker-next-hover")}).delegate(i,"mouseover",function(){e.datepicker._isDisabledDatepicker(g.inline?t.parent()[0]:g.input[0])||(e(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"),e(this).addClass("ui-state-hover"),-1!==this.className.indexOf("ui-datepicker-prev")&&e(this).addClass("ui-datepicker-prev-hover"),-1!==this.className.indexOf("ui-datepicker-next")&&e(this).addClass("ui-datepicker-next-hover"))})}function o(t,i){e.extend(t,i);for(var s in i)null==i[s]&&(t[s]=i[s]);return t}function r(e){return function(){var t=this.element.val();e.apply(this,arguments),this._refresh(),t!==this.element.val()&&this._trigger("change")}}e.ui=e.ui||{},e.extend(e.ui,{version:"1.11.0",keyCode:{BACKSPACE:8,COMMA:188,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SPACE:32,TAB:9,UP:38}}),e.fn.extend({scrollParent:function(){var t=this.css("position"),i="absolute"===t,s=this.parents().filter(function(){var t=e(this);return i&&"static"===t.css("position")?!1:/(auto|scroll)/.test(t.css("overflow")+t.css("overflow-y")+t.css("overflow-x"))}).eq(0);return"fixed"!==t&&s.length?s:e(this[0].ownerDocument||document)},uniqueId:function(){var e=0;return function(){return this.each(function(){this.id||(this.id="ui-id-"+ ++e)})}}(),removeUniqueId:function(){return this.each(function(){/^ui-id-\d+$/.test(this.id)&&e(this).removeAttr("id")})}}),e.extend(e.expr[":"],{data:e.expr.createPseudo?e.expr.createPseudo(function(t){return function(i){return!!e.data(i,t)}}):function(t,i,s){return!!e.data(t,s[3])},focusable:function(i){return t(i,!isNaN(e.attr(i,"tabindex")))},tabbable:function(i){var s=e.attr(i,"tabindex"),n=isNaN(s);return(n||s>=0)&&t(i,!n)}}),e("<a>").outerWidth(1).jquery||e.each(["Width","Height"],function(t,i){function s(t,i,s,a){return e.each(n,function(){i-=parseFloat(e.css(t,"padding"+this))||0,s&&(i-=parseFloat(e.css(t,"border"+this+"Width"))||0),a&&(i-=parseFloat(e.css(t,"margin"+this))||0)}),i}var n="Width"===i?["Left","Right"]:["Top","Bottom"],a=i.toLowerCase(),o={innerWidth:e.fn.innerWidth,innerHeight:e.fn.innerHeight,outerWidth:e.fn.outerWidth,outerHeight:e.fn.outerHeight};e.fn["inner"+i]=function(t){return void 0===t?o["inner"+i].call(this):this.each(function(){e(this).css(a,s(this,t)+"px")})},e.fn["outer"+i]=function(t,n){return"number"!=typeof t?o["outer"+i].call(this,t):this.each(function(){e(this).css(a,s(this,t,!0,n)+"px")})}}),e.fn.addBack||(e.fn.addBack=function(e){return this.add(null==e?this.prevObject:this.prevObject.filter(e))}),e("<a>").data("a-b","a").removeData("a-b").data("a-b")&&(e.fn.removeData=function(t){return function(i){return arguments.length?t.call(this,e.camelCase(i)):t.call(this)}}(e.fn.removeData)),e.ui.ie=!!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()),e.fn.extend({focus:function(t){return function(i,s){return"number"==typeof i?this.each(function(){var t=this;setTimeout(function(){e(t).focus(),s&&s.call(t)},i)}):t.apply(this,arguments)}}(e.fn.focus),disableSelection:function(){var e="onselectstart"in document.createElement("div")?"selectstart":"mousedown";return function(){return this.bind(e+".ui-disableSelection",function(e){e.preventDefault()})}}(),enableSelection:function(){return this.unbind(".ui-disableSelection")},zIndex:function(t){if(void 0!==t)return this.css("zIndex",t);if(this.length)for(var i,s,n=e(this[0]);n.length&&n[0]!==document;){if(i=n.css("position"),("absolute"===i||"relative"===i||"fixed"===i)&&(s=parseInt(n.css("zIndex"),10),!isNaN(s)&&0!==s))return s;n=n.parent()}return 0}}),e.ui.plugin={add:function(t,i,s){var n,a=e.ui[t].prototype;for(n in s)a.plugins[n]=a.plugins[n]||[],a.plugins[n].push([i,s[n]])},call:function(e,t,i,s){var n,a=e.plugins[t];if(a&&(s||e.element[0].parentNode&&11!==e.element[0].parentNode.nodeType))for(n=0;a.length>n;n++)e.options[a[n][0]]&&a[n][1].apply(e.element,i)}};var h=0,l=Array.prototype.slice;e.cleanData=function(t){return function(i){for(var s,n=0;null!=(s=i[n]);n++)try{e(s).triggerHandler("remove")}catch(a){}t(i)}}(e.cleanData),e.widget=function(t,i,s){var n,a,o,r,h={},l=t.split(".")[0];return t=t.split(".")[1],n=l+"-"+t,s||(s=i,i=e.Widget),e.expr[":"][n.toLowerCase()]=function(t){return!!e.data(t,n)},e[l]=e[l]||{},a=e[l][t],o=e[l][t]=function(e,t){return this._createWidget?(arguments.length&&this._createWidget(e,t),void 0):new o(e,t)},e.extend(o,a,{version:s.version,_proto:e.extend({},s),_childConstructors:[]}),r=new i,r.options=e.widget.extend({},r.options),e.each(s,function(t,s){return e.isFunction(s)?(h[t]=function(){var e=function(){return i.prototype[t].apply(this,arguments)},n=function(e){return i.prototype[t].apply(this,e)};return function(){var t,i=this._super,a=this._superApply;return this._super=e,this._superApply=n,t=s.apply(this,arguments),this._super=i,this._superApply=a,t}}(),void 0):(h[t]=s,void 0)}),o.prototype=e.widget.extend(r,{widgetEventPrefix:a?r.widgetEventPrefix||t:t},h,{constructor:o,namespace:l,widgetName:t,widgetFullName:n}),a?(e.each(a._childConstructors,function(t,i){var s=i.prototype;e.widget(s.namespace+"."+s.widgetName,o,i._proto)}),delete a._childConstructors):i._childConstructors.push(o),e.widget.bridge(t,o),o},e.widget.extend=function(t){for(var i,s,n=l.call(arguments,1),a=0,o=n.length;o>a;a++)for(i in n[a])s=n[a][i],n[a].hasOwnProperty(i)&&void 0!==s&&(t[i]=e.isPlainObject(s)?e.isPlainObject(t[i])?e.widget.extend({},t[i],s):e.widget.extend({},s):s);return t},e.widget.bridge=function(t,i){var s=i.prototype.widgetFullName||t;e.fn[t]=function(n){var a="string"==typeof n,o=l.call(arguments,1),r=this;return n=!a&&o.length?e.widget.extend.apply(null,[n].concat(o)):n,a?this.each(function(){var i,a=e.data(this,s);return"instance"===n?(r=a,!1):a?e.isFunction(a[n])&&"_"!==n.charAt(0)?(i=a[n].apply(a,o),i!==a&&void 0!==i?(r=i&&i.jquery?r.pushStack(i.get()):i,!1):void 0):e.error("no such method '"+n+"' for "+t+" widget instance"):e.error("cannot call methods on "+t+" prior to initialization; "+"attempted to call method '"+n+"'")}):this.each(function(){var t=e.data(this,s);t?(t.option(n||{}),t._init&&t._init()):e.data(this,s,new i(n,this))}),r}},e.Widget=function(){},e.Widget._childConstructors=[],e.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",defaultElement:"<div>",options:{disabled:!1,create:null},_createWidget:function(t,i){i=e(i||this.defaultElement||this)[0],this.element=e(i),this.uuid=h++,this.eventNamespace="."+this.widgetName+this.uuid,this.options=e.widget.extend({},this.options,this._getCreateOptions(),t),this.bindings=e(),this.hoverable=e(),this.focusable=e(),i!==this&&(e.data(i,this.widgetFullName,this),this._on(!0,this.element,{remove:function(e){e.target===i&&this.destroy()}}),this.document=e(i.style?i.ownerDocument:i.document||i),this.window=e(this.document[0].defaultView||this.document[0].parentWindow)),this._create(),this._trigger("create",null,this._getCreateEventData()),this._init()},_getCreateOptions:e.noop,_getCreateEventData:e.noop,_create:e.noop,_init:e.noop,destroy:function(){this._destroy(),this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData(e.camelCase(this.widgetFullName)),this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName+"-disabled "+"ui-state-disabled"),this.bindings.unbind(this.eventNamespace),this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus")},_destroy:e.noop,widget:function(){return this.element},option:function(t,i){var s,n,a,o=t;if(0===arguments.length)return e.widget.extend({},this.options);if("string"==typeof t)if(o={},s=t.split("."),t=s.shift(),s.length){for(n=o[t]=e.widget.extend({},this.options[t]),a=0;s.length-1>a;a++)n[s[a]]=n[s[a]]||{},n=n[s[a]];if(t=s.pop(),1===arguments.length)return void 0===n[t]?null:n[t];n[t]=i}else{if(1===arguments.length)return void 0===this.options[t]?null:this.options[t];o[t]=i}return this._setOptions(o),this},_setOptions:function(e){var t;for(t in e)this._setOption(t,e[t]);return this},_setOption:function(e,t){return this.options[e]=t,"disabled"===e&&(this.widget().toggleClass(this.widgetFullName+"-disabled",!!t),t&&(this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus"))),this},enable:function(){return this._setOptions({disabled:!1})},disable:function(){return this._setOptions({disabled:!0})},_on:function(t,i,s){var n,a=this;"boolean"!=typeof t&&(s=i,i=t,t=!1),s?(i=n=e(i),this.bindings=this.bindings.add(i)):(s=i,i=this.element,n=this.widget()),e.each(s,function(s,o){function r(){return t||a.options.disabled!==!0&&!e(this).hasClass("ui-state-disabled")?("string"==typeof o?a[o]:o).apply(a,arguments):void 0}"string"!=typeof o&&(r.guid=o.guid=o.guid||r.guid||e.guid++);var h=s.match(/^([\w:-]*)\s*(.*)$/),l=h[1]+a.eventNamespace,u=h[2];u?n.delegate(u,l,r):i.bind(l,r)})},_off:function(e,t){t=(t||"").split(" ").join(this.eventNamespace+" ")+this.eventNamespace,e.unbind(t).undelegate(t)},_delay:function(e,t){function i(){return("string"==typeof e?s[e]:e).apply(s,arguments)}var s=this;return setTimeout(i,t||0)},_hoverable:function(t){this.hoverable=this.hoverable.add(t),this._on(t,{mouseenter:function(t){e(t.currentTarget).addClass("ui-state-hover")},mouseleave:function(t){e(t.currentTarget).removeClass("ui-state-hover")}})},_focusable:function(t){this.focusable=this.focusable.add(t),this._on(t,{focusin:function(t){e(t.currentTarget).addClass("ui-state-focus")},focusout:function(t){e(t.currentTarget).removeClass("ui-state-focus")}})},_trigger:function(t,i,s){var n,a,o=this.options[t];if(s=s||{},i=e.Event(i),i.type=(t===this.widgetEventPrefix?t:this.widgetEventPrefix+t).toLowerCase(),i.target=this.element[0],a=i.originalEvent)for(n in a)n in i||(i[n]=a[n]);return this.element.trigger(i,s),!(e.isFunction(o)&&o.apply(this.element[0],[i].concat(s))===!1||i.isDefaultPrevented())}},e.each({show:"fadeIn",hide:"fadeOut"},function(t,i){e.Widget.prototype["_"+t]=function(s,n,a){"string"==typeof n&&(n={effect:n});var o,r=n?n===!0||"number"==typeof n?i:n.effect||i:t;n=n||{},"number"==typeof n&&(n={duration:n}),o=!e.isEmptyObject(n),n.complete=a,n.delay&&s.delay(n.delay),o&&e.effects&&e.effects.effect[r]?s[t](n):r!==t&&s[r]?s[r](n.duration,n.easing,a):s.queue(function(i){e(this)[t](),a&&a.call(s[0]),i()})}}),e.widget;var u=!1;e(document).mouseup(function(){u=!1}),e.widget("ui.mouse",{version:"1.11.0",options:{cancel:"input,textarea,button,select,option",distance:1,delay:0},_mouseInit:function(){var t=this;this.element.bind("mousedown."+this.widgetName,function(e){return t._mouseDown(e)}).bind("click."+this.widgetName,function(i){return!0===e.data(i.target,t.widgetName+".preventClickEvent")?(e.removeData(i.target,t.widgetName+".preventClickEvent"),i.stopImmediatePropagation(),!1):void 0}),this.started=!1},_mouseDestroy:function(){this.element.unbind("."+this.widgetName),this._mouseMoveDelegate&&this.document.unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate)},_mouseDown:function(t){if(!u){this._mouseStarted&&this._mouseUp(t),this._mouseDownEvent=t;var i=this,s=1===t.which,n="string"==typeof this.options.cancel&&t.target.nodeName?e(t.target).closest(this.options.cancel).length:!1;return s&&!n&&this._mouseCapture(t)?(this.mouseDelayMet=!this.options.delay,this.mouseDelayMet||(this._mouseDelayTimer=setTimeout(function(){i.mouseDelayMet=!0},this.options.delay)),this._mouseDistanceMet(t)&&this._mouseDelayMet(t)&&(this._mouseStarted=this._mouseStart(t)!==!1,!this._mouseStarted)?(t.preventDefault(),!0):(!0===e.data(t.target,this.widgetName+".preventClickEvent")&&e.removeData(t.target,this.widgetName+".preventClickEvent"),this._mouseMoveDelegate=function(e){return i._mouseMove(e)},this._mouseUpDelegate=function(e){return i._mouseUp(e)},this.document.bind("mousemove."+this.widgetName,this._mouseMoveDelegate).bind("mouseup."+this.widgetName,this._mouseUpDelegate),t.preventDefault(),u=!0,!0)):!0}},_mouseMove:function(t){return e.ui.ie&&(!document.documentMode||9>document.documentMode)&&!t.button?this._mouseUp(t):t.which?this._mouseStarted?(this._mouseDrag(t),t.preventDefault()):(this._mouseDistanceMet(t)&&this._mouseDelayMet(t)&&(this._mouseStarted=this._mouseStart(this._mouseDownEvent,t)!==!1,this._mouseStarted?this._mouseDrag(t):this._mouseUp(t)),!this._mouseStarted):this._mouseUp(t)},_mouseUp:function(t){return this.document.unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate),this._mouseStarted&&(this._mouseStarted=!1,t.target===this._mouseDownEvent.target&&e.data(t.target,this.widgetName+".preventClickEvent",!0),this._mouseStop(t)),u=!1,!1},_mouseDistanceMet:function(e){return Math.max(Math.abs(this._mouseDownEvent.pageX-e.pageX),Math.abs(this._mouseDownEvent.pageY-e.pageY))>=this.options.distance},_mouseDelayMet:function(){return this.mouseDelayMet},_mouseStart:function(){},_mouseDrag:function(){},_mouseStop:function(){},_mouseCapture:function(){return!0}}),function(){function t(e,t,i){return[parseFloat(e[0])*(p.test(e[0])?t/100:1),parseFloat(e[1])*(p.test(e[1])?i/100:1)]}function i(t,i){return parseInt(e.css(t,i),10)||0}function s(t){var i=t[0];return 9===i.nodeType?{width:t.width(),height:t.height(),offset:{top:0,left:0}}:e.isWindow(i)?{width:t.width(),height:t.height(),offset:{top:t.scrollTop(),left:t.scrollLeft()}}:i.preventDefault?{width:0,height:0,offset:{top:i.pageY,left:i.pageX}}:{width:t.outerWidth(),height:t.outerHeight(),offset:t.offset()}}e.ui=e.ui||{};var n,a,o=Math.max,r=Math.abs,h=Math.round,l=/left|center|right/,u=/top|center|bottom/,d=/[\+\-]\d+(\.[\d]+)?%?/,c=/^\w+/,p=/%$/,f=e.fn.position;e.position={scrollbarWidth:function(){if(void 0!==n)return n;var t,i,s=e("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),a=s.children()[0];return e("body").append(s),t=a.offsetWidth,s.css("overflow","scroll"),i=a.offsetWidth,t===i&&(i=s[0].clientWidth),s.remove(),n=t-i},getScrollInfo:function(t){var i=t.isWindow||t.isDocument?"":t.element.css("overflow-x"),s=t.isWindow||t.isDocument?"":t.element.css("overflow-y"),n="scroll"===i||"auto"===i&&t.width<t.element[0].scrollWidth,a="scroll"===s||"auto"===s&&t.height<t.element[0].scrollHeight;return{width:a?e.position.scrollbarWidth():0,height:n?e.position.scrollbarWidth():0}},getWithinInfo:function(t){var i=e(t||window),s=e.isWindow(i[0]),n=!!i[0]&&9===i[0].nodeType;return{element:i,isWindow:s,isDocument:n,offset:i.offset()||{left:0,top:0},scrollLeft:i.scrollLeft(),scrollTop:i.scrollTop(),width:s?i.width():i.outerWidth(),height:s?i.height():i.outerHeight()}}},e.fn.position=function(n){if(!n||!n.of)return f.apply(this,arguments);n=e.extend({},n);var p,m,g,v,y,b,_=e(n.of),x=e.position.getWithinInfo(n.within),w=e.position.getScrollInfo(x),k=(n.collision||"flip").split(" "),T={};return b=s(_),_[0].preventDefault&&(n.at="left top"),m=b.width,g=b.height,v=b.offset,y=e.extend({},v),e.each(["my","at"],function(){var e,t,i=(n[this]||"").split(" ");1===i.length&&(i=l.test(i[0])?i.concat(["center"]):u.test(i[0])?["center"].concat(i):["center","center"]),i[0]=l.test(i[0])?i[0]:"center",i[1]=u.test(i[1])?i[1]:"center",e=d.exec(i[0]),t=d.exec(i[1]),T[this]=[e?e[0]:0,t?t[0]:0],n[this]=[c.exec(i[0])[0],c.exec(i[1])[0]]}),1===k.length&&(k[1]=k[0]),"right"===n.at[0]?y.left+=m:"center"===n.at[0]&&(y.left+=m/2),"bottom"===n.at[1]?y.top+=g:"center"===n.at[1]&&(y.top+=g/2),p=t(T.at,m,g),y.left+=p[0],y.top+=p[1],this.each(function(){var s,l,u=e(this),d=u.outerWidth(),c=u.outerHeight(),f=i(this,"marginLeft"),b=i(this,"marginTop"),D=d+f+i(this,"marginRight")+w.width,S=c+b+i(this,"marginBottom")+w.height,M=e.extend({},y),N=t(T.my,u.outerWidth(),u.outerHeight());"right"===n.my[0]?M.left-=d:"center"===n.my[0]&&(M.left-=d/2),"bottom"===n.my[1]?M.top-=c:"center"===n.my[1]&&(M.top-=c/2),M.left+=N[0],M.top+=N[1],a||(M.left=h(M.left),M.top=h(M.top)),s={marginLeft:f,marginTop:b},e.each(["left","top"],function(t,i){e.ui.position[k[t]]&&e.ui.position[k[t]][i](M,{targetWidth:m,targetHeight:g,elemWidth:d,elemHeight:c,collisionPosition:s,collisionWidth:D,collisionHeight:S,offset:[p[0]+N[0],p[1]+N[1]],my:n.my,at:n.at,within:x,elem:u})}),n.using&&(l=function(e){var t=v.left-M.left,i=t+m-d,s=v.top-M.top,a=s+g-c,h={target:{element:_,left:v.left,top:v.top,width:m,height:g},element:{element:u,left:M.left,top:M.top,width:d,height:c},horizontal:0>i?"left":t>0?"right":"center",vertical:0>a?"top":s>0?"bottom":"middle"};d>m&&m>r(t+i)&&(h.horizontal="center"),c>g&&g>r(s+a)&&(h.vertical="middle"),h.important=o(r(t),r(i))>o(r(s),r(a))?"horizontal":"vertical",n.using.call(this,e,h)}),u.offset(e.extend(M,{using:l}))})},e.ui.position={fit:{left:function(e,t){var i,s=t.within,n=s.isWindow?s.scrollLeft:s.offset.left,a=s.width,r=e.left-t.collisionPosition.marginLeft,h=n-r,l=r+t.collisionWidth-a-n;t.collisionWidth>a?h>0&&0>=l?(i=e.left+h+t.collisionWidth-a-n,e.left+=h-i):e.left=l>0&&0>=h?n:h>l?n+a-t.collisionWidth:n:h>0?e.left+=h:l>0?e.left-=l:e.left=o(e.left-r,e.left)},top:function(e,t){var i,s=t.within,n=s.isWindow?s.scrollTop:s.offset.top,a=t.within.height,r=e.top-t.collisionPosition.marginTop,h=n-r,l=r+t.collisionHeight-a-n;t.collisionHeight>a?h>0&&0>=l?(i=e.top+h+t.collisionHeight-a-n,e.top+=h-i):e.top=l>0&&0>=h?n:h>l?n+a-t.collisionHeight:n:h>0?e.top+=h:l>0?e.top-=l:e.top=o(e.top-r,e.top)}},flip:{left:function(e,t){var i,s,n=t.within,a=n.offset.left+n.scrollLeft,o=n.width,h=n.isWindow?n.scrollLeft:n.offset.left,l=e.left-t.collisionPosition.marginLeft,u=l-h,d=l+t.collisionWidth-o-h,c="left"===t.my[0]?-t.elemWidth:"right"===t.my[0]?t.elemWidth:0,p="left"===t.at[0]?t.targetWidth:"right"===t.at[0]?-t.targetWidth:0,f=-2*t.offset[0];0>u?(i=e.left+c+p+f+t.collisionWidth-o-a,(0>i||r(u)>i)&&(e.left+=c+p+f)):d>0&&(s=e.left-t.collisionPosition.marginLeft+c+p+f-h,(s>0||d>r(s))&&(e.left+=c+p+f))},top:function(e,t){var i,s,n=t.within,a=n.offset.top+n.scrollTop,o=n.height,h=n.isWindow?n.scrollTop:n.offset.top,l=e.top-t.collisionPosition.marginTop,u=l-h,d=l+t.collisionHeight-o-h,c="top"===t.my[1],p=c?-t.elemHeight:"bottom"===t.my[1]?t.elemHeight:0,f="top"===t.at[1]?t.targetHeight:"bottom"===t.at[1]?-t.targetHeight:0,m=-2*t.offset[1];0>u?(s=e.top+p+f+m+t.collisionHeight-o-a,e.top+p+f+m>u&&(0>s||r(u)>s)&&(e.top+=p+f+m)):d>0&&(i=e.top-t.collisionPosition.marginTop+p+f+m-h,e.top+p+f+m>d&&(i>0||d>r(i))&&(e.top+=p+f+m))}},flipfit:{left:function(){e.ui.position.flip.left.apply(this,arguments),e.ui.position.fit.left.apply(this,arguments)},top:function(){e.ui.position.flip.top.apply(this,arguments),e.ui.position.fit.top.apply(this,arguments)}}},function(){var t,i,s,n,o,r=document.getElementsByTagName("body")[0],h=document.createElement("div");t=document.createElement(r?"div":"body"),s={visibility:"hidden",width:0,height:0,border:0,margin:0,background:"none"},r&&e.extend(s,{position:"absolute",left:"-1000px",top:"-1000px"});for(o in s)t.style[o]=s[o];t.appendChild(h),i=r||document.documentElement,i.insertBefore(t,i.firstChild),h.style.cssText="position: absolute; left: 10.7432222px;",n=e(h).offset().left,a=n>10&&11>n,t.innerHTML="",i.removeChild(t)}()}(),e.ui.position,e.widget("ui.accordion",{version:"1.11.0",options:{active:0,animate:{},collapsible:!1,event:"click",header:"> li > :first-child,> :not(li):even",heightStyle:"auto",icons:{activeHeader:"ui-icon-triangle-1-s",header:"ui-icon-triangle-1-e"},activate:null,beforeActivate:null},hideProps:{borderTopWidth:"hide",borderBottomWidth:"hide",paddingTop:"hide",paddingBottom:"hide",height:"hide"},showProps:{borderTopWidth:"show",borderBottomWidth:"show",paddingTop:"show",paddingBottom:"show",height:"show"},_create:function(){var t=this.options;this.prevShow=this.prevHide=e(),this.element.addClass("ui-accordion ui-widget ui-helper-reset").attr("role","tablist"),t.collapsible||t.active!==!1&&null!=t.active||(t.active=0),this._processPanels(),0>t.active&&(t.active+=this.headers.length),this._refresh()},_getCreateEventData:function(){return{header:this.active,panel:this.active.length?this.active.next():e()}},_createIcons:function(){var t=this.options.icons;t&&(e("<span>").addClass("ui-accordion-header-icon ui-icon "+t.header).prependTo(this.headers),this.active.children(".ui-accordion-header-icon").removeClass(t.header).addClass(t.activeHeader),this.headers.addClass("ui-accordion-icons"))},_destroyIcons:function(){this.headers.removeClass("ui-accordion-icons").children(".ui-accordion-header-icon").remove()},_destroy:function(){var e;this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role"),this.headers.removeClass("ui-accordion-header ui-accordion-header-active ui-state-default ui-corner-all ui-state-active ui-state-disabled ui-corner-top").removeAttr("role").removeAttr("aria-expanded").removeAttr("aria-selected").removeAttr("aria-controls").removeAttr("tabIndex").removeUniqueId(),this._destroyIcons(),e=this.headers.next().removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active ui-state-disabled").css("display","").removeAttr("role").removeAttr("aria-hidden").removeAttr("aria-labelledby").removeUniqueId(),"content"!==this.options.heightStyle&&e.css("height","")},_setOption:function(e,t){return"active"===e?(this._activate(t),void 0):("event"===e&&(this.options.event&&this._off(this.headers,this.options.event),this._setupEvents(t)),this._super(e,t),"collapsible"!==e||t||this.options.active!==!1||this._activate(0),"icons"===e&&(this._destroyIcons(),t&&this._createIcons()),"disabled"===e&&(this.element.toggleClass("ui-state-disabled",!!t).attr("aria-disabled",t),this.headers.add(this.headers.next()).toggleClass("ui-state-disabled",!!t)),void 0)},_keydown:function(t){if(!t.altKey&&!t.ctrlKey){var i=e.ui.keyCode,s=this.headers.length,n=this.headers.index(t.target),a=!1;switch(t.keyCode){case i.RIGHT:case i.DOWN:a=this.headers[(n+1)%s];break;case i.LEFT:case i.UP:a=this.headers[(n-1+s)%s];break;case i.SPACE:case i.ENTER:this._eventHandler(t);break;case i.HOME:a=this.headers[0];break;case i.END:a=this.headers[s-1]}a&&(e(t.target).attr("tabIndex",-1),e(a).attr("tabIndex",0),a.focus(),t.preventDefault())}},_panelKeyDown:function(t){t.keyCode===e.ui.keyCode.UP&&t.ctrlKey&&e(t.currentTarget).prev().focus()},refresh:function(){var t=this.options;this._processPanels(),t.active===!1&&t.collapsible===!0||!this.headers.length?(t.active=!1,this.active=e()):t.active===!1?this._activate(0):this.active.length&&!e.contains(this.element[0],this.active[0])?this.headers.length===this.headers.find(".ui-state-disabled").length?(t.active=!1,this.active=e()):this._activate(Math.max(0,t.active-1)):t.active=this.headers.index(this.active),this._destroyIcons(),this._refresh()},_processPanels:function(){this.headers=this.element.find(this.options.header).addClass("ui-accordion-header ui-state-default ui-corner-all"),this.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").filter(":not(.ui-accordion-content-active)").hide()},_refresh:function(){var t,i=this.options,s=i.heightStyle,n=this.element.parent();this.active=this._findActive(i.active).addClass("ui-accordion-header-active ui-state-active ui-corner-top").removeClass("ui-corner-all"),this.active.next().addClass("ui-accordion-content-active").show(),this.headers.attr("role","tab").each(function(){var t=e(this),i=t.uniqueId().attr("id"),s=t.next(),n=s.uniqueId().attr("id");t.attr("aria-controls",n),s.attr("aria-labelledby",i)}).next().attr("role","tabpanel"),this.headers.not(this.active).attr({"aria-selected":"false","aria-expanded":"false",tabIndex:-1}).next().attr({"aria-hidden":"true"}).hide(),this.active.length?this.active.attr({"aria-selected":"true","aria-expanded":"true",tabIndex:0}).next().attr({"aria-hidden":"false"}):this.headers.eq(0).attr("tabIndex",0),this._createIcons(),this._setupEvents(i.event),"fill"===s?(t=n.height(),this.element.siblings(":visible").each(function(){var i=e(this),s=i.css("position");"absolute"!==s&&"fixed"!==s&&(t-=i.outerHeight(!0))}),this.headers.each(function(){t-=e(this).outerHeight(!0)}),this.headers.next().each(function(){e(this).height(Math.max(0,t-e(this).innerHeight()+e(this).height()))}).css("overflow","auto")):"auto"===s&&(t=0,this.headers.next().each(function(){t=Math.max(t,e(this).css("height","").height())}).height(t))},_activate:function(t){var i=this._findActive(t)[0];i!==this.active[0]&&(i=i||this.active[0],this._eventHandler({target:i,currentTarget:i,preventDefault:e.noop}))},_findActive:function(t){return"number"==typeof t?this.headers.eq(t):e()},_setupEvents:function(t){var i={keydown:"_keydown"};t&&e.each(t.split(" "),function(e,t){i[t]="_eventHandler"}),this._off(this.headers.add(this.headers.next())),this._on(this.headers,i),this._on(this.headers.next(),{keydown:"_panelKeyDown"}),this._hoverable(this.headers),this._focusable(this.headers)},_eventHandler:function(t){var i=this.options,s=this.active,n=e(t.currentTarget),a=n[0]===s[0],o=a&&i.collapsible,r=o?e():n.next(),h=s.next(),l={oldHeader:s,oldPanel:h,newHeader:o?e():n,newPanel:r};t.preventDefault(),a&&!i.collapsible||this._trigger("beforeActivate",t,l)===!1||(i.active=o?!1:this.headers.index(n),this.active=a?e():n,this._toggle(l),s.removeClass("ui-accordion-header-active ui-state-active"),i.icons&&s.children(".ui-accordion-header-icon").removeClass(i.icons.activeHeader).addClass(i.icons.header),a||(n.removeClass("ui-corner-all").addClass("ui-accordion-header-active ui-state-active ui-corner-top"),i.icons&&n.children(".ui-accordion-header-icon").removeClass(i.icons.header).addClass(i.icons.activeHeader),n.next().addClass("ui-accordion-content-active")))},_toggle:function(t){var i=t.newPanel,s=this.prevShow.length?this.prevShow:t.oldPanel;this.prevShow.add(this.prevHide).stop(!0,!0),this.prevShow=i,this.prevHide=s,this.options.animate?this._animate(i,s,t):(s.hide(),i.show(),this._toggleComplete(t)),s.attr({"aria-hidden":"true"}),s.prev().attr("aria-selected","false"),i.length&&s.length?s.prev().attr({tabIndex:-1,"aria-expanded":"false"}):i.length&&this.headers.filter(function(){return 0===e(this).attr("tabIndex")}).attr("tabIndex",-1),i.attr("aria-hidden","false").prev().attr({"aria-selected":"true",tabIndex:0,"aria-expanded":"true"})},_animate:function(e,t,i){var s,n,a,o=this,r=0,h=e.length&&(!t.length||e.index()<t.index()),l=this.options.animate||{},u=h&&l.down||l,d=function(){o._toggleComplete(i)};return"number"==typeof u&&(a=u),"string"==typeof u&&(n=u),n=n||u.easing||l.easing,a=a||u.duration||l.duration,t.length?e.length?(s=e.show().outerHeight(),t.animate(this.hideProps,{duration:a,easing:n,step:function(e,t){t.now=Math.round(e)}}),e.hide().animate(this.showProps,{duration:a,easing:n,complete:d,step:function(e,i){i.now=Math.round(e),"height"!==i.prop?r+=i.now:"content"!==o.options.heightStyle&&(i.now=Math.round(s-t.outerHeight()-r),r=0)}}),void 0):t.animate(this.hideProps,a,n,d):e.animate(this.showProps,a,n,d)},_toggleComplete:function(e){var t=e.oldPanel;t.removeClass("ui-accordion-content-active").prev().removeClass("ui-corner-top").addClass("ui-corner-all"),t.length&&(t.parent()[0].className=t.parent()[0].className),this._trigger("activate",null,e)}}),e.widget("ui.menu",{version:"1.11.0",defaultElement:"<ul>",delay:300,options:{icons:{submenu:"ui-icon-carat-1-e"},items:"> *",menus:"ul",position:{my:"left-1 top",at:"right top"},role:"menu",blur:null,focus:null,select:null},_create:function(){this.activeMenu=this.element,this.mouseHandled=!1,this.element.uniqueId().addClass("ui-menu ui-widget ui-widget-content").toggleClass("ui-menu-icons",!!this.element.find(".ui-icon").length).attr({role:this.options.role,tabIndex:0}),this.options.disabled&&this.element.addClass("ui-state-disabled").attr("aria-disabled","true"),this._on({"mousedown .ui-menu-item":function(e){e.preventDefault()},"click .ui-menu-item":function(t){var i=e(t.target);!this.mouseHandled&&i.not(".ui-state-disabled").length&&(this.select(t),t.isPropagationStopped()||(this.mouseHandled=!0),i.has(".ui-menu").length?this.expand(t):!this.element.is(":focus")&&e(this.document[0].activeElement).closest(".ui-menu").length&&(this.element.trigger("focus",[!0]),this.active&&1===this.active.parents(".ui-menu").length&&clearTimeout(this.timer)))},"mouseenter .ui-menu-item":function(t){var i=e(t.currentTarget);i.siblings(".ui-state-active").removeClass("ui-state-active"),this.focus(t,i)},mouseleave:"collapseAll","mouseleave .ui-menu":"collapseAll",focus:function(e,t){var i=this.active||this.element.find(this.options.items).eq(0);t||this.focus(e,i)},blur:function(t){this._delay(function(){e.contains(this.element[0],this.document[0].activeElement)||this.collapseAll(t)})},keydown:"_keydown"}),this.refresh(),this._on(this.document,{click:function(e){this._closeOnDocumentClick(e)&&this.collapseAll(e),this.mouseHandled=!1
}})},_destroy:function(){this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeClass("ui-menu ui-widget ui-widget-content ui-menu-icons ui-front").removeAttr("role").removeAttr("tabIndex").removeAttr("aria-labelledby").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-disabled").removeUniqueId().show(),this.element.find(".ui-menu-item").removeClass("ui-menu-item").removeAttr("role").removeAttr("aria-disabled").removeUniqueId().removeClass("ui-state-hover").removeAttr("tabIndex").removeAttr("role").removeAttr("aria-haspopup").children().each(function(){var t=e(this);t.data("ui-menu-submenu-carat")&&t.remove()}),this.element.find(".ui-menu-divider").removeClass("ui-menu-divider ui-widget-content")},_keydown:function(t){function i(e){return e.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")}var s,n,a,o,r,h=!0;switch(t.keyCode){case e.ui.keyCode.PAGE_UP:this.previousPage(t);break;case e.ui.keyCode.PAGE_DOWN:this.nextPage(t);break;case e.ui.keyCode.HOME:this._move("first","first",t);break;case e.ui.keyCode.END:this._move("last","last",t);break;case e.ui.keyCode.UP:this.previous(t);break;case e.ui.keyCode.DOWN:this.next(t);break;case e.ui.keyCode.LEFT:this.collapse(t);break;case e.ui.keyCode.RIGHT:this.active&&!this.active.is(".ui-state-disabled")&&this.expand(t);break;case e.ui.keyCode.ENTER:case e.ui.keyCode.SPACE:this._activate(t);break;case e.ui.keyCode.ESCAPE:this.collapse(t);break;default:h=!1,n=this.previousFilter||"",a=String.fromCharCode(t.keyCode),o=!1,clearTimeout(this.filterTimer),a===n?o=!0:a=n+a,r=RegExp("^"+i(a),"i"),s=this.activeMenu.find(this.options.items).filter(function(){return r.test(e(this).text())}),s=o&&-1!==s.index(this.active.next())?this.active.nextAll(".ui-menu-item"):s,s.length||(a=String.fromCharCode(t.keyCode),r=RegExp("^"+i(a),"i"),s=this.activeMenu.find(this.options.items).filter(function(){return r.test(e(this).text())})),s.length?(this.focus(t,s),s.length>1?(this.previousFilter=a,this.filterTimer=this._delay(function(){delete this.previousFilter},1e3)):delete this.previousFilter):delete this.previousFilter}h&&t.preventDefault()},_activate:function(e){this.active.is(".ui-state-disabled")||(this.active.is("[aria-haspopup='true']")?this.expand(e):this.select(e))},refresh:function(){var t,i,s=this,n=this.options.icons.submenu,a=this.element.find(this.options.menus);this.element.toggleClass("ui-menu-icons",!!this.element.find(".ui-icon").length),a.filter(":not(.ui-menu)").addClass("ui-menu ui-widget ui-widget-content ui-front").hide().attr({role:this.options.role,"aria-hidden":"true","aria-expanded":"false"}).each(function(){var t=e(this),i=t.parent(),s=e("<span>").addClass("ui-menu-icon ui-icon "+n).data("ui-menu-submenu-carat",!0);i.attr("aria-haspopup","true").prepend(s),t.attr("aria-labelledby",i.attr("id"))}),t=a.add(this.element),i=t.find(this.options.items),i.not(".ui-menu-item").each(function(){var t=e(this);s._isDivider(t)&&t.addClass("ui-widget-content ui-menu-divider")}),i.not(".ui-menu-item, .ui-menu-divider").addClass("ui-menu-item").uniqueId().attr({tabIndex:-1,role:this._itemRole()}),i.filter(".ui-state-disabled").attr("aria-disabled","true"),this.active&&!e.contains(this.element[0],this.active[0])&&this.blur()},_itemRole:function(){return{menu:"menuitem",listbox:"option"}[this.options.role]},_setOption:function(e,t){"icons"===e&&this.element.find(".ui-menu-icon").removeClass(this.options.icons.submenu).addClass(t.submenu),"disabled"===e&&this.element.toggleClass("ui-state-disabled",!!t).attr("aria-disabled",t),this._super(e,t)},focus:function(e,t){var i,s;this.blur(e,e&&"focus"===e.type),this._scrollIntoView(t),this.active=t.first(),s=this.active.addClass("ui-state-focus").removeClass("ui-state-active"),this.options.role&&this.element.attr("aria-activedescendant",s.attr("id")),this.active.parent().closest(".ui-menu-item").addClass("ui-state-active"),e&&"keydown"===e.type?this._close():this.timer=this._delay(function(){this._close()},this.delay),i=t.children(".ui-menu"),i.length&&e&&/^mouse/.test(e.type)&&this._startOpening(i),this.activeMenu=t.parent(),this._trigger("focus",e,{item:t})},_scrollIntoView:function(t){var i,s,n,a,o,r;this._hasScroll()&&(i=parseFloat(e.css(this.activeMenu[0],"borderTopWidth"))||0,s=parseFloat(e.css(this.activeMenu[0],"paddingTop"))||0,n=t.offset().top-this.activeMenu.offset().top-i-s,a=this.activeMenu.scrollTop(),o=this.activeMenu.height(),r=t.outerHeight(),0>n?this.activeMenu.scrollTop(a+n):n+r>o&&this.activeMenu.scrollTop(a+n-o+r))},blur:function(e,t){t||clearTimeout(this.timer),this.active&&(this.active.removeClass("ui-state-focus"),this.active=null,this._trigger("blur",e,{item:this.active}))},_startOpening:function(e){clearTimeout(this.timer),"true"===e.attr("aria-hidden")&&(this.timer=this._delay(function(){this._close(),this._open(e)},this.delay))},_open:function(t){var i=e.extend({of:this.active},this.options.position);clearTimeout(this.timer),this.element.find(".ui-menu").not(t.parents(".ui-menu")).hide().attr("aria-hidden","true"),t.show().removeAttr("aria-hidden").attr("aria-expanded","true").position(i)},collapseAll:function(t,i){clearTimeout(this.timer),this.timer=this._delay(function(){var s=i?this.element:e(t&&t.target).closest(this.element.find(".ui-menu"));s.length||(s=this.element),this._close(s),this.blur(t),this.activeMenu=s},this.delay)},_close:function(e){e||(e=this.active?this.active.parent():this.element),e.find(".ui-menu").hide().attr("aria-hidden","true").attr("aria-expanded","false").end().find(".ui-state-active").not(".ui-state-focus").removeClass("ui-state-active")},_closeOnDocumentClick:function(t){return!e(t.target).closest(".ui-menu").length},_isDivider:function(e){return!/[^\-\u2014\u2013\s]/.test(e.text())},collapse:function(e){var t=this.active&&this.active.parent().closest(".ui-menu-item",this.element);t&&t.length&&(this._close(),this.focus(e,t))},expand:function(e){var t=this.active&&this.active.children(".ui-menu ").find(this.options.items).first();t&&t.length&&(this._open(t.parent()),this._delay(function(){this.focus(e,t)}))},next:function(e){this._move("next","first",e)},previous:function(e){this._move("prev","last",e)},isFirstItem:function(){return this.active&&!this.active.prevAll(".ui-menu-item").length},isLastItem:function(){return this.active&&!this.active.nextAll(".ui-menu-item").length},_move:function(e,t,i){var s;this.active&&(s="first"===e||"last"===e?this.active["first"===e?"prevAll":"nextAll"](".ui-menu-item").eq(-1):this.active[e+"All"](".ui-menu-item").eq(0)),s&&s.length&&this.active||(s=this.activeMenu.find(this.options.items)[t]()),this.focus(i,s)},nextPage:function(t){var i,s,n;return this.active?(this.isLastItem()||(this._hasScroll()?(s=this.active.offset().top,n=this.element.height(),this.active.nextAll(".ui-menu-item").each(function(){return i=e(this),0>i.offset().top-s-n}),this.focus(t,i)):this.focus(t,this.activeMenu.find(this.options.items)[this.active?"last":"first"]())),void 0):(this.next(t),void 0)},previousPage:function(t){var i,s,n;return this.active?(this.isFirstItem()||(this._hasScroll()?(s=this.active.offset().top,n=this.element.height(),this.active.prevAll(".ui-menu-item").each(function(){return i=e(this),i.offset().top-s+n>0}),this.focus(t,i)):this.focus(t,this.activeMenu.find(this.options.items).first())),void 0):(this.next(t),void 0)},_hasScroll:function(){return this.element.outerHeight()<this.element.prop("scrollHeight")},select:function(t){this.active=this.active||e(t.target).closest(".ui-menu-item");var i={item:this.active};this.active.has(".ui-menu").length||this.collapseAll(t,!0),this._trigger("select",t,i)}}),e.widget("ui.autocomplete",{version:"1.11.0",defaultElement:"<input>",options:{appendTo:null,autoFocus:!1,delay:300,minLength:1,position:{my:"left top",at:"left bottom",collision:"none"},source:null,change:null,close:null,focus:null,open:null,response:null,search:null,select:null},requestIndex:0,pending:0,_create:function(){var t,i,s,n=this.element[0].nodeName.toLowerCase(),a="textarea"===n,o="input"===n;this.isMultiLine=a?!0:o?!1:this.element.prop("isContentEditable"),this.valueMethod=this.element[a||o?"val":"text"],this.isNewMenu=!0,this.element.addClass("ui-autocomplete-input").attr("autocomplete","off"),this._on(this.element,{keydown:function(n){if(this.element.prop("readOnly"))return t=!0,s=!0,i=!0,void 0;t=!1,s=!1,i=!1;var a=e.ui.keyCode;switch(n.keyCode){case a.PAGE_UP:t=!0,this._move("previousPage",n);break;case a.PAGE_DOWN:t=!0,this._move("nextPage",n);break;case a.UP:t=!0,this._keyEvent("previous",n);break;case a.DOWN:t=!0,this._keyEvent("next",n);break;case a.ENTER:this.menu.active&&(t=!0,n.preventDefault(),this.menu.select(n));break;case a.TAB:this.menu.active&&this.menu.select(n);break;case a.ESCAPE:this.menu.element.is(":visible")&&(this._value(this.term),this.close(n),n.preventDefault());break;default:i=!0,this._searchTimeout(n)}},keypress:function(s){if(t)return t=!1,(!this.isMultiLine||this.menu.element.is(":visible"))&&s.preventDefault(),void 0;if(!i){var n=e.ui.keyCode;switch(s.keyCode){case n.PAGE_UP:this._move("previousPage",s);break;case n.PAGE_DOWN:this._move("nextPage",s);break;case n.UP:this._keyEvent("previous",s);break;case n.DOWN:this._keyEvent("next",s)}}},input:function(e){return s?(s=!1,e.preventDefault(),void 0):(this._searchTimeout(e),void 0)},focus:function(){this.selectedItem=null,this.previous=this._value()},blur:function(e){return this.cancelBlur?(delete this.cancelBlur,void 0):(clearTimeout(this.searching),this.close(e),this._change(e),void 0)}}),this._initSource(),this.menu=e("<ul>").addClass("ui-autocomplete ui-front").appendTo(this._appendTo()).menu({role:null}).hide().menu("instance"),this._on(this.menu.element,{mousedown:function(t){t.preventDefault(),this.cancelBlur=!0,this._delay(function(){delete this.cancelBlur});var i=this.menu.element[0];e(t.target).closest(".ui-menu-item").length||this._delay(function(){var t=this;this.document.one("mousedown",function(s){s.target===t.element[0]||s.target===i||e.contains(i,s.target)||t.close()})})},menufocus:function(t,i){var s,n;return this.isNewMenu&&(this.isNewMenu=!1,t.originalEvent&&/^mouse/.test(t.originalEvent.type))?(this.menu.blur(),this.document.one("mousemove",function(){e(t.target).trigger(t.originalEvent)}),void 0):(n=i.item.data("ui-autocomplete-item"),!1!==this._trigger("focus",t,{item:n})&&t.originalEvent&&/^key/.test(t.originalEvent.type)&&this._value(n.value),s=i.item.attr("aria-label")||n.value,s&&jQuery.trim(s).length&&(this.liveRegion.children().hide(),e("<div>").text(s).appendTo(this.liveRegion)),void 0)},menuselect:function(e,t){var i=t.item.data("ui-autocomplete-item"),s=this.previous;this.element[0]!==this.document[0].activeElement&&(this.element.focus(),this.previous=s,this._delay(function(){this.previous=s,this.selectedItem=i})),!1!==this._trigger("select",e,{item:i})&&this._value(i.value),this.term=this._value(),this.close(e),this.selectedItem=i}}),this.liveRegion=e("<span>",{role:"status","aria-live":"assertive","aria-relevant":"additions"}).addClass("ui-helper-hidden-accessible").appendTo(this.document[0].body),this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete")}})},_destroy:function(){clearTimeout(this.searching),this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete"),this.menu.element.remove(),this.liveRegion.remove()},_setOption:function(e,t){this._super(e,t),"source"===e&&this._initSource(),"appendTo"===e&&this.menu.element.appendTo(this._appendTo()),"disabled"===e&&t&&this.xhr&&this.xhr.abort()},_appendTo:function(){var t=this.options.appendTo;return t&&(t=t.jquery||t.nodeType?e(t):this.document.find(t).eq(0)),t&&t[0]||(t=this.element.closest(".ui-front")),t.length||(t=this.document[0].body),t},_initSource:function(){var t,i,s=this;e.isArray(this.options.source)?(t=this.options.source,this.source=function(i,s){s(e.ui.autocomplete.filter(t,i.term))}):"string"==typeof this.options.source?(i=this.options.source,this.source=function(t,n){s.xhr&&s.xhr.abort(),s.xhr=e.ajax({url:i,data:t,dataType:"json",success:function(e){n(e)},error:function(){n([])}})}):this.source=this.options.source},_searchTimeout:function(e){clearTimeout(this.searching),this.searching=this._delay(function(){var t=this.term===this._value(),i=this.menu.element.is(":visible"),s=e.altKey||e.ctrlKey||e.metaKey||e.shiftKey;(!t||t&&!i&&!s)&&(this.selectedItem=null,this.search(null,e))},this.options.delay)},search:function(e,t){return e=null!=e?e:this._value(),this.term=this._value(),e.length<this.options.minLength?this.close(t):this._trigger("search",t)!==!1?this._search(e):void 0},_search:function(e){this.pending++,this.element.addClass("ui-autocomplete-loading"),this.cancelSearch=!1,this.source({term:e},this._response())},_response:function(){var t=++this.requestIndex;return e.proxy(function(e){t===this.requestIndex&&this.__response(e),this.pending--,this.pending||this.element.removeClass("ui-autocomplete-loading")},this)},__response:function(e){e&&(e=this._normalize(e)),this._trigger("response",null,{content:e}),!this.options.disabled&&e&&e.length&&!this.cancelSearch?(this._suggest(e),this._trigger("open")):this._close()},close:function(e){this.cancelSearch=!0,this._close(e)},_close:function(e){this.menu.element.is(":visible")&&(this.menu.element.hide(),this.menu.blur(),this.isNewMenu=!0,this._trigger("close",e))},_change:function(e){this.previous!==this._value()&&this._trigger("change",e,{item:this.selectedItem})},_normalize:function(t){return t.length&&t[0].label&&t[0].value?t:e.map(t,function(t){return"string"==typeof t?{label:t,value:t}:e.extend({},t,{label:t.label||t.value,value:t.value||t.label})})},_suggest:function(t){var i=this.menu.element.empty();this._renderMenu(i,t),this.isNewMenu=!0,this.menu.refresh(),i.show(),this._resizeMenu(),i.position(e.extend({of:this.element},this.options.position)),this.options.autoFocus&&this.menu.next()},_resizeMenu:function(){var e=this.menu.element;e.outerWidth(Math.max(e.width("").outerWidth()+1,this.element.outerWidth()))},_renderMenu:function(t,i){var s=this;e.each(i,function(e,i){s._renderItemData(t,i)})},_renderItemData:function(e,t){return this._renderItem(e,t).data("ui-autocomplete-item",t)},_renderItem:function(t,i){return e("<li>").text(i.label).appendTo(t)},_move:function(e,t){return this.menu.element.is(":visible")?this.menu.isFirstItem()&&/^previous/.test(e)||this.menu.isLastItem()&&/^next/.test(e)?(this.isMultiLine||this._value(this.term),this.menu.blur(),void 0):(this.menu[e](t),void 0):(this.search(null,t),void 0)},widget:function(){return this.menu.element},_value:function(){return this.valueMethod.apply(this.element,arguments)},_keyEvent:function(e,t){(!this.isMultiLine||this.menu.element.is(":visible"))&&(this._move(e,t),t.preventDefault())}}),e.extend(e.ui.autocomplete,{escapeRegex:function(e){return e.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")},filter:function(t,i){var s=RegExp(e.ui.autocomplete.escapeRegex(i),"i");return e.grep(t,function(e){return s.test(e.label||e.value||e)})}}),e.widget("ui.autocomplete",e.ui.autocomplete,{options:{messages:{noResults:"No search results.",results:function(e){return e+(e>1?" results are":" result is")+" available, use up and down arrow keys to navigate."}}},__response:function(t){var i;this._superApply(arguments),this.options.disabled||this.cancelSearch||(i=t&&t.length?this.options.messages.results(t.length):this.options.messages.noResults,this.liveRegion.children().hide(),e("<div>").text(i).appendTo(this.liveRegion))}}),e.ui.autocomplete;var d,c="ui-button ui-widget ui-state-default ui-corner-all",p="ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only",f=function(){var t=e(this);setTimeout(function(){t.find(":ui-button").button("refresh")},1)},m=function(t){var i=t.name,s=t.form,n=e([]);return i&&(i=i.replace(/'/g,"\\'"),n=s?e(s).find("[name='"+i+"'][type=radio]"):e("[name='"+i+"'][type=radio]",t.ownerDocument).filter(function(){return!this.form})),n};e.widget("ui.button",{version:"1.11.0",defaultElement:"<button>",options:{disabled:null,text:!0,label:null,icons:{primary:null,secondary:null}},_create:function(){this.element.closest("form").unbind("reset"+this.eventNamespace).bind("reset"+this.eventNamespace,f),"boolean"!=typeof this.options.disabled?this.options.disabled=!!this.element.prop("disabled"):this.element.prop("disabled",this.options.disabled),this._determineButtonType(),this.hasTitle=!!this.buttonElement.attr("title");var t=this,i=this.options,s="checkbox"===this.type||"radio"===this.type,n=s?"":"ui-state-active";null===i.label&&(i.label="input"===this.type?this.buttonElement.val():this.buttonElement.html()),this._hoverable(this.buttonElement),this.buttonElement.addClass(c).attr("role","button").bind("mouseenter"+this.eventNamespace,function(){i.disabled||this===d&&e(this).addClass("ui-state-active")}).bind("mouseleave"+this.eventNamespace,function(){i.disabled||e(this).removeClass(n)}).bind("click"+this.eventNamespace,function(e){i.disabled&&(e.preventDefault(),e.stopImmediatePropagation())}),this._on({focus:function(){this.buttonElement.addClass("ui-state-focus")},blur:function(){this.buttonElement.removeClass("ui-state-focus")}}),s&&this.element.bind("change"+this.eventNamespace,function(){t.refresh()}),"checkbox"===this.type?this.buttonElement.bind("click"+this.eventNamespace,function(){return i.disabled?!1:void 0}):"radio"===this.type?this.buttonElement.bind("click"+this.eventNamespace,function(){if(i.disabled)return!1;e(this).addClass("ui-state-active"),t.buttonElement.attr("aria-pressed","true");var s=t.element[0];m(s).not(s).map(function(){return e(this).button("widget")[0]}).removeClass("ui-state-active").attr("aria-pressed","false")}):(this.buttonElement.bind("mousedown"+this.eventNamespace,function(){return i.disabled?!1:(e(this).addClass("ui-state-active"),d=this,t.document.one("mouseup",function(){d=null}),void 0)}).bind("mouseup"+this.eventNamespace,function(){return i.disabled?!1:(e(this).removeClass("ui-state-active"),void 0)}).bind("keydown"+this.eventNamespace,function(t){return i.disabled?!1:((t.keyCode===e.ui.keyCode.SPACE||t.keyCode===e.ui.keyCode.ENTER)&&e(this).addClass("ui-state-active"),void 0)}).bind("keyup"+this.eventNamespace+" blur"+this.eventNamespace,function(){e(this).removeClass("ui-state-active")}),this.buttonElement.is("a")&&this.buttonElement.keyup(function(t){t.keyCode===e.ui.keyCode.SPACE&&e(this).click()})),this._setOption("disabled",i.disabled),this._resetButton()},_determineButtonType:function(){var e,t,i;this.type=this.element.is("[type=checkbox]")?"checkbox":this.element.is("[type=radio]")?"radio":this.element.is("input")?"input":"button","checkbox"===this.type||"radio"===this.type?(e=this.element.parents().last(),t="label[for='"+this.element.attr("id")+"']",this.buttonElement=e.find(t),this.buttonElement.length||(e=e.length?e.siblings():this.element.siblings(),this.buttonElement=e.filter(t),this.buttonElement.length||(this.buttonElement=e.find(t))),this.element.addClass("ui-helper-hidden-accessible"),i=this.element.is(":checked"),i&&this.buttonElement.addClass("ui-state-active"),this.buttonElement.prop("aria-pressed",i)):this.buttonElement=this.element},widget:function(){return this.buttonElement},_destroy:function(){this.element.removeClass("ui-helper-hidden-accessible"),this.buttonElement.removeClass(c+" ui-state-active "+p).removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html()),this.hasTitle||this.buttonElement.removeAttr("title")},_setOption:function(e,t){return this._super(e,t),"disabled"===e?(this.widget().toggleClass("ui-state-disabled",!!t),this.element.prop("disabled",!!t),t&&("checkbox"===this.type||"radio"===this.type?this.buttonElement.removeClass("ui-state-focus"):this.buttonElement.removeClass("ui-state-focus ui-state-active")),void 0):(this._resetButton(),void 0)},refresh:function(){var t=this.element.is("input, button")?this.element.is(":disabled"):this.element.hasClass("ui-button-disabled");t!==this.options.disabled&&this._setOption("disabled",t),"radio"===this.type?m(this.element[0]).each(function(){e(this).is(":checked")?e(this).button("widget").addClass("ui-state-active").attr("aria-pressed","true"):e(this).button("widget").removeClass("ui-state-active").attr("aria-pressed","false")}):"checkbox"===this.type&&(this.element.is(":checked")?this.buttonElement.addClass("ui-state-active").attr("aria-pressed","true"):this.buttonElement.removeClass("ui-state-active").attr("aria-pressed","false"))},_resetButton:function(){if("input"===this.type)return this.options.label&&this.element.val(this.options.label),void 0;var t=this.buttonElement.removeClass(p),i=e("<span></span>",this.document[0]).addClass("ui-button-text").html(this.options.label).appendTo(t.empty()).text(),s=this.options.icons,n=s.primary&&s.secondary,a=[];s.primary||s.secondary?(this.options.text&&a.push("ui-button-text-icon"+(n?"s":s.primary?"-primary":"-secondary")),s.primary&&t.prepend("<span class='ui-button-icon-primary ui-icon "+s.primary+"'></span>"),s.secondary&&t.append("<span class='ui-button-icon-secondary ui-icon "+s.secondary+"'></span>"),this.options.text||(a.push(n?"ui-button-icons-only":"ui-button-icon-only"),this.hasTitle||t.attr("title",e.trim(i)))):a.push("ui-button-text-only"),t.addClass(a.join(" "))}}),e.widget("ui.buttonset",{version:"1.11.0",options:{items:"button, input[type=button], input[type=submit], input[type=reset], input[type=checkbox], input[type=radio], a, :data(ui-button)"},_create:function(){this.element.addClass("ui-buttonset")},_init:function(){this.refresh()},_setOption:function(e,t){"disabled"===e&&this.buttons.button("option",e,t),this._super(e,t)},refresh:function(){var t="rtl"===this.element.css("direction"),i=this.element.find(this.options.items),s=i.filter(":ui-button");i.not(":ui-button").button(),s.button("refresh"),this.buttons=i.map(function(){return e(this).button("widget")[0]}).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass(t?"ui-corner-right":"ui-corner-left").end().filter(":last").addClass(t?"ui-corner-left":"ui-corner-right").end().end()},_destroy:function(){this.element.removeClass("ui-buttonset"),this.buttons.map(function(){return e(this).button("widget")[0]}).removeClass("ui-corner-left ui-corner-right").end().button("destroy")}}),e.ui.button,e.extend(e.ui,{datepicker:{version:"1.11.0"}});var g;e.extend(n.prototype,{markerClassName:"hasDatepicker",maxRows:4,_widgetDatepicker:function(){return this.dpDiv},setDefaults:function(e){return o(this._defaults,e||{}),this},_attachDatepicker:function(t,i){var s,n,a;s=t.nodeName.toLowerCase(),n="div"===s||"span"===s,t.id||(this.uuid+=1,t.id="dp"+this.uuid),a=this._newInst(e(t),n),a.settings=e.extend({},i||{}),"input"===s?this._connectDatepicker(t,a):n&&this._inlineDatepicker(t,a)},_newInst:function(t,i){var s=t[0].id.replace(/([^A-Za-z0-9_\-])/g,"\\\\$1");return{id:s,input:t,selectedDay:0,selectedMonth:0,selectedYear:0,drawMonth:0,drawYear:0,inline:i,dpDiv:i?a(e("<div class='"+this._inlineClass+" ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")):this.dpDiv}},_connectDatepicker:function(t,i){var s=e(t);i.append=e([]),i.trigger=e([]),s.hasClass(this.markerClassName)||(this._attachments(s,i),s.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp),this._autoSize(i),e.data(t,"datepicker",i),i.settings.disabled&&this._disableDatepicker(t))},_attachments:function(t,i){var s,n,a,o=this._get(i,"appendText"),r=this._get(i,"isRTL");i.append&&i.append.remove(),o&&(i.append=e("<span class='"+this._appendClass+"'>"+o+"</span>"),t[r?"before":"after"](i.append)),t.unbind("focus",this._showDatepicker),i.trigger&&i.trigger.remove(),s=this._get(i,"showOn"),("focus"===s||"both"===s)&&t.focus(this._showDatepicker),("button"===s||"both"===s)&&(n=this._get(i,"buttonText"),a=this._get(i,"buttonImage"),i.trigger=e(this._get(i,"buttonImageOnly")?e("<img/>").addClass(this._triggerClass).attr({src:a,alt:n,title:n}):e("<button type='button'></button>").addClass(this._triggerClass).html(a?e("<img/>").attr({src:a,alt:n,title:n}):n)),t[r?"before":"after"](i.trigger),i.trigger.click(function(){return e.datepicker._datepickerShowing&&e.datepicker._lastInput===t[0]?e.datepicker._hideDatepicker():e.datepicker._datepickerShowing&&e.datepicker._lastInput!==t[0]?(e.datepicker._hideDatepicker(),e.datepicker._showDatepicker(t[0])):e.datepicker._showDatepicker(t[0]),!1}))},_autoSize:function(e){if(this._get(e,"autoSize")&&!e.inline){var t,i,s,n,a=new Date(2009,11,20),o=this._get(e,"dateFormat");o.match(/[DM]/)&&(t=function(e){for(i=0,s=0,n=0;e.length>n;n++)e[n].length>i&&(i=e[n].length,s=n);return s},a.setMonth(t(this._get(e,o.match(/MM/)?"monthNames":"monthNamesShort"))),a.setDate(t(this._get(e,o.match(/DD/)?"dayNames":"dayNamesShort"))+20-a.getDay())),e.input.attr("size",this._formatDate(e,a).length)}},_inlineDatepicker:function(t,i){var s=e(t);s.hasClass(this.markerClassName)||(s.addClass(this.markerClassName).append(i.dpDiv),e.data(t,"datepicker",i),this._setDate(i,this._getDefaultDate(i),!0),this._updateDatepicker(i),this._updateAlternate(i),i.settings.disabled&&this._disableDatepicker(t),i.dpDiv.css("display","block"))},_dialogDatepicker:function(t,i,s,n,a){var r,h,l,u,d,c=this._dialogInst;return c||(this.uuid+=1,r="dp"+this.uuid,this._dialogInput=e("<input type='text' id='"+r+"' style='position: absolute; top: -100px; width: 0px;'/>"),this._dialogInput.keydown(this._doKeyDown),e("body").append(this._dialogInput),c=this._dialogInst=this._newInst(this._dialogInput,!1),c.settings={},e.data(this._dialogInput[0],"datepicker",c)),o(c.settings,n||{}),i=i&&i.constructor===Date?this._formatDate(c,i):i,this._dialogInput.val(i),this._pos=a?a.length?a:[a.pageX,a.pageY]:null,this._pos||(h=document.documentElement.clientWidth,l=document.documentElement.clientHeight,u=document.documentElement.scrollLeft||document.body.scrollLeft,d=document.documentElement.scrollTop||document.body.scrollTop,this._pos=[h/2-100+u,l/2-150+d]),this._dialogInput.css("left",this._pos[0]+20+"px").css("top",this._pos[1]+"px"),c.settings.onSelect=s,this._inDialog=!0,this.dpDiv.addClass(this._dialogClass),this._showDatepicker(this._dialogInput[0]),e.blockUI&&e.blockUI(this.dpDiv),e.data(this._dialogInput[0],"datepicker",c),this},_destroyDatepicker:function(t){var i,s=e(t),n=e.data(t,"datepicker");s.hasClass(this.markerClassName)&&(i=t.nodeName.toLowerCase(),e.removeData(t,"datepicker"),"input"===i?(n.append.remove(),n.trigger.remove(),s.removeClass(this.markerClassName).unbind("focus",this._showDatepicker).unbind("keydown",this._doKeyDown).unbind("keypress",this._doKeyPress).unbind("keyup",this._doKeyUp)):("div"===i||"span"===i)&&s.removeClass(this.markerClassName).empty())},_enableDatepicker:function(t){var i,s,n=e(t),a=e.data(t,"datepicker");n.hasClass(this.markerClassName)&&(i=t.nodeName.toLowerCase(),"input"===i?(t.disabled=!1,a.trigger.filter("button").each(function(){this.disabled=!1}).end().filter("img").css({opacity:"1.0",cursor:""})):("div"===i||"span"===i)&&(s=n.children("."+this._inlineClass),s.children().removeClass("ui-state-disabled"),s.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",!1)),this._disabledInputs=e.map(this._disabledInputs,function(e){return e===t?null:e}))},_disableDatepicker:function(t){var i,s,n=e(t),a=e.data(t,"datepicker");n.hasClass(this.markerClassName)&&(i=t.nodeName.toLowerCase(),"input"===i?(t.disabled=!0,a.trigger.filter("button").each(function(){this.disabled=!0}).end().filter("img").css({opacity:"0.5",cursor:"default"})):("div"===i||"span"===i)&&(s=n.children("."+this._inlineClass),s.children().addClass("ui-state-disabled"),s.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",!0)),this._disabledInputs=e.map(this._disabledInputs,function(e){return e===t?null:e}),this._disabledInputs[this._disabledInputs.length]=t)},_isDisabledDatepicker:function(e){if(!e)return!1;for(var t=0;this._disabledInputs.length>t;t++)if(this._disabledInputs[t]===e)return!0;return!1},_getInst:function(t){try{return e.data(t,"datepicker")}catch(i){throw"Missing instance data for this datepicker"}},_optionDatepicker:function(t,i,s){var n,a,r,h,l=this._getInst(t);return 2===arguments.length&&"string"==typeof i?"defaults"===i?e.extend({},e.datepicker._defaults):l?"all"===i?e.extend({},l.settings):this._get(l,i):null:(n=i||{},"string"==typeof i&&(n={},n[i]=s),l&&(this._curInst===l&&this._hideDatepicker(),a=this._getDateDatepicker(t,!0),r=this._getMinMaxDate(l,"min"),h=this._getMinMaxDate(l,"max"),o(l.settings,n),null!==r&&void 0!==n.dateFormat&&void 0===n.minDate&&(l.settings.minDate=this._formatDate(l,r)),null!==h&&void 0!==n.dateFormat&&void 0===n.maxDate&&(l.settings.maxDate=this._formatDate(l,h)),"disabled"in n&&(n.disabled?this._disableDatepicker(t):this._enableDatepicker(t)),this._attachments(e(t),l),this._autoSize(l),this._setDate(l,a),this._updateAlternate(l),this._updateDatepicker(l)),void 0)},_changeDatepicker:function(e,t,i){this._optionDatepicker(e,t,i)},_refreshDatepicker:function(e){var t=this._getInst(e);t&&this._updateDatepicker(t)},_setDateDatepicker:function(e,t){var i=this._getInst(e);i&&(this._setDate(i,t),this._updateDatepicker(i),this._updateAlternate(i))},_getDateDatepicker:function(e,t){var i=this._getInst(e);return i&&!i.inline&&this._setDateFromField(i,t),i?this._getDate(i):null},_doKeyDown:function(t){var i,s,n,a=e.datepicker._getInst(t.target),o=!0,r=a.dpDiv.is(".ui-datepicker-rtl");if(a._keyEvent=!0,e.datepicker._datepickerShowing)switch(t.keyCode){case 9:e.datepicker._hideDatepicker(),o=!1;break;case 13:return n=e("td."+e.datepicker._dayOverClass+":not(."+e.datepicker._currentClass+")",a.dpDiv),n[0]&&e.datepicker._selectDay(t.target,a.selectedMonth,a.selectedYear,n[0]),i=e.datepicker._get(a,"onSelect"),i?(s=e.datepicker._formatDate(a),i.apply(a.input?a.input[0]:null,[s,a])):e.datepicker._hideDatepicker(),!1;case 27:e.datepicker._hideDatepicker();break;case 33:e.datepicker._adjustDate(t.target,t.ctrlKey?-e.datepicker._get(a,"stepBigMonths"):-e.datepicker._get(a,"stepMonths"),"M");break;case 34:e.datepicker._adjustDate(t.target,t.ctrlKey?+e.datepicker._get(a,"stepBigMonths"):+e.datepicker._get(a,"stepMonths"),"M");break;case 35:(t.ctrlKey||t.metaKey)&&e.datepicker._clearDate(t.target),o=t.ctrlKey||t.metaKey;break;case 36:(t.ctrlKey||t.metaKey)&&e.datepicker._gotoToday(t.target),o=t.ctrlKey||t.metaKey;break;case 37:(t.ctrlKey||t.metaKey)&&e.datepicker._adjustDate(t.target,r?1:-1,"D"),o=t.ctrlKey||t.metaKey,t.originalEvent.altKey&&e.datepicker._adjustDate(t.target,t.ctrlKey?-e.datepicker._get(a,"stepBigMonths"):-e.datepicker._get(a,"stepMonths"),"M");break;case 38:(t.ctrlKey||t.metaKey)&&e.datepicker._adjustDate(t.target,-7,"D"),o=t.ctrlKey||t.metaKey;break;case 39:(t.ctrlKey||t.metaKey)&&e.datepicker._adjustDate(t.target,r?-1:1,"D"),o=t.ctrlKey||t.metaKey,t.originalEvent.altKey&&e.datepicker._adjustDate(t.target,t.ctrlKey?+e.datepicker._get(a,"stepBigMonths"):+e.datepicker._get(a,"stepMonths"),"M");break;case 40:(t.ctrlKey||t.metaKey)&&e.datepicker._adjustDate(t.target,7,"D"),o=t.ctrlKey||t.metaKey;break;default:o=!1}else 36===t.keyCode&&t.ctrlKey?e.datepicker._showDatepicker(this):o=!1;o&&(t.preventDefault(),t.stopPropagation())},_doKeyPress:function(t){var i,s,n=e.datepicker._getInst(t.target);return e.datepicker._get(n,"constrainInput")?(i=e.datepicker._possibleChars(e.datepicker._get(n,"dateFormat")),s=String.fromCharCode(null==t.charCode?t.keyCode:t.charCode),t.ctrlKey||t.metaKey||" ">s||!i||i.indexOf(s)>-1):void 0},_doKeyUp:function(t){var i,s=e.datepicker._getInst(t.target);if(s.input.val()!==s.lastVal)try{i=e.datepicker.parseDate(e.datepicker._get(s,"dateFormat"),s.input?s.input.val():null,e.datepicker._getFormatConfig(s)),i&&(e.datepicker._setDateFromField(s),e.datepicker._updateAlternate(s),e.datepicker._updateDatepicker(s))
}catch(n){}return!0},_showDatepicker:function(t){if(t=t.target||t,"input"!==t.nodeName.toLowerCase()&&(t=e("input",t.parentNode)[0]),!e.datepicker._isDisabledDatepicker(t)&&e.datepicker._lastInput!==t){var i,n,a,r,h,l,u;i=e.datepicker._getInst(t),e.datepicker._curInst&&e.datepicker._curInst!==i&&(e.datepicker._curInst.dpDiv.stop(!0,!0),i&&e.datepicker._datepickerShowing&&e.datepicker._hideDatepicker(e.datepicker._curInst.input[0])),n=e.datepicker._get(i,"beforeShow"),a=n?n.apply(t,[t,i]):{},a!==!1&&(o(i.settings,a),i.lastVal=null,e.datepicker._lastInput=t,e.datepicker._setDateFromField(i),e.datepicker._inDialog&&(t.value=""),e.datepicker._pos||(e.datepicker._pos=e.datepicker._findPos(t),e.datepicker._pos[1]+=t.offsetHeight),r=!1,e(t).parents().each(function(){return r|="fixed"===e(this).css("position"),!r}),h={left:e.datepicker._pos[0],top:e.datepicker._pos[1]},e.datepicker._pos=null,i.dpDiv.empty(),i.dpDiv.css({position:"absolute",display:"block",top:"-1000px"}),e.datepicker._updateDatepicker(i),h=e.datepicker._checkOffset(i,h,r),i.dpDiv.css({position:e.datepicker._inDialog&&e.blockUI?"static":r?"fixed":"absolute",display:"none",left:h.left+"px",top:h.top+"px"}),i.inline||(l=e.datepicker._get(i,"showAnim"),u=e.datepicker._get(i,"duration"),i.dpDiv.css("z-index",s(e(t))+1),e.datepicker._datepickerShowing=!0,e.effects&&e.effects.effect[l]?i.dpDiv.show(l,e.datepicker._get(i,"showOptions"),u):i.dpDiv[l||"show"](l?u:null),e.datepicker._shouldFocusInput(i)&&i.input.focus(),e.datepicker._curInst=i))}},_updateDatepicker:function(t){this.maxRows=4,g=t,t.dpDiv.empty().append(this._generateHTML(t)),this._attachHandlers(t),t.dpDiv.find("."+this._dayOverClass+" a");var i,s=this._getNumberOfMonths(t),n=s[1],a=17;t.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""),n>1&&t.dpDiv.addClass("ui-datepicker-multi-"+n).css("width",a*n+"em"),t.dpDiv[(1!==s[0]||1!==s[1]?"add":"remove")+"Class"]("ui-datepicker-multi"),t.dpDiv[(this._get(t,"isRTL")?"add":"remove")+"Class"]("ui-datepicker-rtl"),t===e.datepicker._curInst&&e.datepicker._datepickerShowing&&e.datepicker._shouldFocusInput(t)&&t.input.focus(),t.yearshtml&&(i=t.yearshtml,setTimeout(function(){i===t.yearshtml&&t.yearshtml&&t.dpDiv.find("select.ui-datepicker-year:first").replaceWith(t.yearshtml),i=t.yearshtml=null},0))},_shouldFocusInput:function(e){return e.input&&e.input.is(":visible")&&!e.input.is(":disabled")&&!e.input.is(":focus")},_checkOffset:function(t,i,s){var n=t.dpDiv.outerWidth(),a=t.dpDiv.outerHeight(),o=t.input?t.input.outerWidth():0,r=t.input?t.input.outerHeight():0,h=document.documentElement.clientWidth+(s?0:e(document).scrollLeft()),l=document.documentElement.clientHeight+(s?0:e(document).scrollTop());return i.left-=this._get(t,"isRTL")?n-o:0,i.left-=s&&i.left===t.input.offset().left?e(document).scrollLeft():0,i.top-=s&&i.top===t.input.offset().top+r?e(document).scrollTop():0,i.left-=Math.min(i.left,i.left+n>h&&h>n?Math.abs(i.left+n-h):0),i.top-=Math.min(i.top,i.top+a>l&&l>a?Math.abs(a+r):0),i},_findPos:function(t){for(var i,s=this._getInst(t),n=this._get(s,"isRTL");t&&("hidden"===t.type||1!==t.nodeType||e.expr.filters.hidden(t));)t=t[n?"previousSibling":"nextSibling"];return i=e(t).offset(),[i.left,i.top]},_hideDatepicker:function(t){var i,s,n,a,o=this._curInst;!o||t&&o!==e.data(t,"datepicker")||this._datepickerShowing&&(i=this._get(o,"showAnim"),s=this._get(o,"duration"),n=function(){e.datepicker._tidyDialog(o)},e.effects&&(e.effects.effect[i]||e.effects[i])?o.dpDiv.hide(i,e.datepicker._get(o,"showOptions"),s,n):o.dpDiv["slideDown"===i?"slideUp":"fadeIn"===i?"fadeOut":"hide"](i?s:null,n),i||n(),this._datepickerShowing=!1,a=this._get(o,"onClose"),a&&a.apply(o.input?o.input[0]:null,[o.input?o.input.val():"",o]),this._lastInput=null,this._inDialog&&(this._dialogInput.css({position:"absolute",left:"0",top:"-100px"}),e.blockUI&&(e.unblockUI(),e("body").append(this.dpDiv))),this._inDialog=!1)},_tidyDialog:function(e){e.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar")},_checkExternalClick:function(t){if(e.datepicker._curInst){var i=e(t.target),s=e.datepicker._getInst(i[0]);(i[0].id!==e.datepicker._mainDivId&&0===i.parents("#"+e.datepicker._mainDivId).length&&!i.hasClass(e.datepicker.markerClassName)&&!i.closest("."+e.datepicker._triggerClass).length&&e.datepicker._datepickerShowing&&(!e.datepicker._inDialog||!e.blockUI)||i.hasClass(e.datepicker.markerClassName)&&e.datepicker._curInst!==s)&&e.datepicker._hideDatepicker()}},_adjustDate:function(t,i,s){var n=e(t),a=this._getInst(n[0]);this._isDisabledDatepicker(n[0])||(this._adjustInstDate(a,i+("M"===s?this._get(a,"showCurrentAtPos"):0),s),this._updateDatepicker(a))},_gotoToday:function(t){var i,s=e(t),n=this._getInst(s[0]);this._get(n,"gotoCurrent")&&n.currentDay?(n.selectedDay=n.currentDay,n.drawMonth=n.selectedMonth=n.currentMonth,n.drawYear=n.selectedYear=n.currentYear):(i=new Date,n.selectedDay=i.getDate(),n.drawMonth=n.selectedMonth=i.getMonth(),n.drawYear=n.selectedYear=i.getFullYear()),this._notifyChange(n),this._adjustDate(s)},_selectMonthYear:function(t,i,s){var n=e(t),a=this._getInst(n[0]);a["selected"+("M"===s?"Month":"Year")]=a["draw"+("M"===s?"Month":"Year")]=parseInt(i.options[i.selectedIndex].value,10),this._notifyChange(a),this._adjustDate(n)},_selectDay:function(t,i,s,n){var a,o=e(t);e(n).hasClass(this._unselectableClass)||this._isDisabledDatepicker(o[0])||(a=this._getInst(o[0]),a.selectedDay=a.currentDay=e("a",n).html(),a.selectedMonth=a.currentMonth=i,a.selectedYear=a.currentYear=s,this._selectDate(t,this._formatDate(a,a.currentDay,a.currentMonth,a.currentYear)))},_clearDate:function(t){var i=e(t);this._selectDate(i,"")},_selectDate:function(t,i){var s,n=e(t),a=this._getInst(n[0]);i=null!=i?i:this._formatDate(a),a.input&&a.input.val(i),this._updateAlternate(a),s=this._get(a,"onSelect"),s?s.apply(a.input?a.input[0]:null,[i,a]):a.input&&a.input.trigger("change"),a.inline?this._updateDatepicker(a):(this._hideDatepicker(),this._lastInput=a.input[0],"object"!=typeof a.input[0]&&a.input.focus(),this._lastInput=null)},_updateAlternate:function(t){var i,s,n,a=this._get(t,"altField");a&&(i=this._get(t,"altFormat")||this._get(t,"dateFormat"),s=this._getDate(t),n=this.formatDate(i,s,this._getFormatConfig(t)),e(a).each(function(){e(this).val(n)}))},noWeekends:function(e){var t=e.getDay();return[t>0&&6>t,""]},iso8601Week:function(e){var t,i=new Date(e.getTime());return i.setDate(i.getDate()+4-(i.getDay()||7)),t=i.getTime(),i.setMonth(0),i.setDate(1),Math.floor(Math.round((t-i)/864e5)/7)+1},parseDate:function(t,i,s){if(null==t||null==i)throw"Invalid arguments";if(i="object"==typeof i?""+i:i+"",""===i)return null;var n,a,o,r,h=0,l=(s?s.shortYearCutoff:null)||this._defaults.shortYearCutoff,u="string"!=typeof l?l:(new Date).getFullYear()%100+parseInt(l,10),d=(s?s.dayNamesShort:null)||this._defaults.dayNamesShort,c=(s?s.dayNames:null)||this._defaults.dayNames,p=(s?s.monthNamesShort:null)||this._defaults.monthNamesShort,f=(s?s.monthNames:null)||this._defaults.monthNames,m=-1,g=-1,v=-1,y=-1,b=!1,_=function(e){var i=t.length>n+1&&t.charAt(n+1)===e;return i&&n++,i},x=function(e){var t=_(e),s="@"===e?14:"!"===e?20:"y"===e&&t?4:"o"===e?3:2,n=RegExp("^\\d{1,"+s+"}"),a=i.substring(h).match(n);if(!a)throw"Missing number at position "+h;return h+=a[0].length,parseInt(a[0],10)},w=function(t,s,n){var a=-1,o=e.map(_(t)?n:s,function(e,t){return[[t,e]]}).sort(function(e,t){return-(e[1].length-t[1].length)});if(e.each(o,function(e,t){var s=t[1];return i.substr(h,s.length).toLowerCase()===s.toLowerCase()?(a=t[0],h+=s.length,!1):void 0}),-1!==a)return a+1;throw"Unknown name at position "+h},k=function(){if(i.charAt(h)!==t.charAt(n))throw"Unexpected literal at position "+h;h++};for(n=0;t.length>n;n++)if(b)"'"!==t.charAt(n)||_("'")?k():b=!1;else switch(t.charAt(n)){case"d":v=x("d");break;case"D":w("D",d,c);break;case"o":y=x("o");break;case"m":g=x("m");break;case"M":g=w("M",p,f);break;case"y":m=x("y");break;case"@":r=new Date(x("@")),m=r.getFullYear(),g=r.getMonth()+1,v=r.getDate();break;case"!":r=new Date((x("!")-this._ticksTo1970)/1e4),m=r.getFullYear(),g=r.getMonth()+1,v=r.getDate();break;case"'":_("'")?k():b=!0;break;default:k()}if(i.length>h&&(o=i.substr(h),!/^\s+/.test(o)))throw"Extra/unparsed characters found in date: "+o;if(-1===m?m=(new Date).getFullYear():100>m&&(m+=(new Date).getFullYear()-(new Date).getFullYear()%100+(u>=m?0:-100)),y>-1)for(g=1,v=y;;){if(a=this._getDaysInMonth(m,g-1),a>=v)break;g++,v-=a}if(r=this._daylightSavingAdjust(new Date(m,g-1,v)),r.getFullYear()!==m||r.getMonth()+1!==g||r.getDate()!==v)throw"Invalid date";return r},ATOM:"yy-mm-dd",COOKIE:"D, dd M yy",ISO_8601:"yy-mm-dd",RFC_822:"D, d M y",RFC_850:"DD, dd-M-y",RFC_1036:"D, d M y",RFC_1123:"D, d M yy",RFC_2822:"D, d M yy",RSS:"D, d M y",TICKS:"!",TIMESTAMP:"@",W3C:"yy-mm-dd",_ticksTo1970:1e7*60*60*24*(718685+Math.floor(492.5)-Math.floor(19.7)+Math.floor(4.925)),formatDate:function(e,t,i){if(!t)return"";var s,n=(i?i.dayNamesShort:null)||this._defaults.dayNamesShort,a=(i?i.dayNames:null)||this._defaults.dayNames,o=(i?i.monthNamesShort:null)||this._defaults.monthNamesShort,r=(i?i.monthNames:null)||this._defaults.monthNames,h=function(t){var i=e.length>s+1&&e.charAt(s+1)===t;return i&&s++,i},l=function(e,t,i){var s=""+t;if(h(e))for(;i>s.length;)s="0"+s;return s},u=function(e,t,i,s){return h(e)?s[t]:i[t]},d="",c=!1;if(t)for(s=0;e.length>s;s++)if(c)"'"!==e.charAt(s)||h("'")?d+=e.charAt(s):c=!1;else switch(e.charAt(s)){case"d":d+=l("d",t.getDate(),2);break;case"D":d+=u("D",t.getDay(),n,a);break;case"o":d+=l("o",Math.round((new Date(t.getFullYear(),t.getMonth(),t.getDate()).getTime()-new Date(t.getFullYear(),0,0).getTime())/864e5),3);break;case"m":d+=l("m",t.getMonth()+1,2);break;case"M":d+=u("M",t.getMonth(),o,r);break;case"y":d+=h("y")?t.getFullYear():(10>t.getYear()%100?"0":"")+t.getYear()%100;break;case"@":d+=t.getTime();break;case"!":d+=1e4*t.getTime()+this._ticksTo1970;break;case"'":h("'")?d+="'":c=!0;break;default:d+=e.charAt(s)}return d},_possibleChars:function(e){var t,i="",s=!1,n=function(i){var s=e.length>t+1&&e.charAt(t+1)===i;return s&&t++,s};for(t=0;e.length>t;t++)if(s)"'"!==e.charAt(t)||n("'")?i+=e.charAt(t):s=!1;else switch(e.charAt(t)){case"d":case"m":case"y":case"@":i+="0123456789";break;case"D":case"M":return null;case"'":n("'")?i+="'":s=!0;break;default:i+=e.charAt(t)}return i},_get:function(e,t){return void 0!==e.settings[t]?e.settings[t]:this._defaults[t]},_setDateFromField:function(e,t){if(e.input.val()!==e.lastVal){var i=this._get(e,"dateFormat"),s=e.lastVal=e.input?e.input.val():null,n=this._getDefaultDate(e),a=n,o=this._getFormatConfig(e);try{a=this.parseDate(i,s,o)||n}catch(r){s=t?"":s}e.selectedDay=a.getDate(),e.drawMonth=e.selectedMonth=a.getMonth(),e.drawYear=e.selectedYear=a.getFullYear(),e.currentDay=s?a.getDate():0,e.currentMonth=s?a.getMonth():0,e.currentYear=s?a.getFullYear():0,this._adjustInstDate(e)}},_getDefaultDate:function(e){return this._restrictMinMax(e,this._determineDate(e,this._get(e,"defaultDate"),new Date))},_determineDate:function(t,i,s){var n=function(e){var t=new Date;return t.setDate(t.getDate()+e),t},a=function(i){try{return e.datepicker.parseDate(e.datepicker._get(t,"dateFormat"),i,e.datepicker._getFormatConfig(t))}catch(s){}for(var n=(i.toLowerCase().match(/^c/)?e.datepicker._getDate(t):null)||new Date,a=n.getFullYear(),o=n.getMonth(),r=n.getDate(),h=/([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g,l=h.exec(i);l;){switch(l[2]||"d"){case"d":case"D":r+=parseInt(l[1],10);break;case"w":case"W":r+=7*parseInt(l[1],10);break;case"m":case"M":o+=parseInt(l[1],10),r=Math.min(r,e.datepicker._getDaysInMonth(a,o));break;case"y":case"Y":a+=parseInt(l[1],10),r=Math.min(r,e.datepicker._getDaysInMonth(a,o))}l=h.exec(i)}return new Date(a,o,r)},o=null==i||""===i?s:"string"==typeof i?a(i):"number"==typeof i?isNaN(i)?s:n(i):new Date(i.getTime());return o=o&&"Invalid Date"==""+o?s:o,o&&(o.setHours(0),o.setMinutes(0),o.setSeconds(0),o.setMilliseconds(0)),this._daylightSavingAdjust(o)},_daylightSavingAdjust:function(e){return e?(e.setHours(e.getHours()>12?e.getHours()+2:0),e):null},_setDate:function(e,t,i){var s=!t,n=e.selectedMonth,a=e.selectedYear,o=this._restrictMinMax(e,this._determineDate(e,t,new Date));e.selectedDay=e.currentDay=o.getDate(),e.drawMonth=e.selectedMonth=e.currentMonth=o.getMonth(),e.drawYear=e.selectedYear=e.currentYear=o.getFullYear(),n===e.selectedMonth&&a===e.selectedYear||i||this._notifyChange(e),this._adjustInstDate(e),e.input&&e.input.val(s?"":this._formatDate(e))},_getDate:function(e){var t=!e.currentYear||e.input&&""===e.input.val()?null:this._daylightSavingAdjust(new Date(e.currentYear,e.currentMonth,e.currentDay));return t},_attachHandlers:function(t){var i=this._get(t,"stepMonths"),s="#"+t.id.replace(/\\\\/g,"\\");t.dpDiv.find("[data-handler]").map(function(){var t={prev:function(){e.datepicker._adjustDate(s,-i,"M")},next:function(){e.datepicker._adjustDate(s,+i,"M")},hide:function(){e.datepicker._hideDatepicker()},today:function(){e.datepicker._gotoToday(s)},selectDay:function(){return e.datepicker._selectDay(s,+this.getAttribute("data-month"),+this.getAttribute("data-year"),this),!1},selectMonth:function(){return e.datepicker._selectMonthYear(s,this,"M"),!1},selectYear:function(){return e.datepicker._selectMonthYear(s,this,"Y"),!1}};e(this).bind(this.getAttribute("data-event"),t[this.getAttribute("data-handler")])})},_generateHTML:function(e){var t,i,s,n,a,o,r,h,l,u,d,c,p,f,m,g,v,y,b,_,x,w,k,T,D,S,M,N,C,A,I,P,z,H,F,E,j,O,W,L=new Date,R=this._daylightSavingAdjust(new Date(L.getFullYear(),L.getMonth(),L.getDate())),Y=this._get(e,"isRTL"),B=this._get(e,"showButtonPanel"),J=this._get(e,"hideIfNoPrevNext"),q=this._get(e,"navigationAsDateFormat"),K=this._getNumberOfMonths(e),V=this._get(e,"showCurrentAtPos"),U=this._get(e,"stepMonths"),Q=1!==K[0]||1!==K[1],G=this._daylightSavingAdjust(e.currentDay?new Date(e.currentYear,e.currentMonth,e.currentDay):new Date(9999,9,9)),X=this._getMinMaxDate(e,"min"),$=this._getMinMaxDate(e,"max"),Z=e.drawMonth-V,et=e.drawYear;if(0>Z&&(Z+=12,et--),$)for(t=this._daylightSavingAdjust(new Date($.getFullYear(),$.getMonth()-K[0]*K[1]+1,$.getDate())),t=X&&X>t?X:t;this._daylightSavingAdjust(new Date(et,Z,1))>t;)Z--,0>Z&&(Z=11,et--);for(e.drawMonth=Z,e.drawYear=et,i=this._get(e,"prevText"),i=q?this.formatDate(i,this._daylightSavingAdjust(new Date(et,Z-U,1)),this._getFormatConfig(e)):i,s=this._canAdjustMonth(e,-1,et,Z)?"<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='"+i+"'><span class='ui-icon ui-icon-circle-triangle-"+(Y?"e":"w")+"'>"+i+"</span></a>":J?"":"<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='"+i+"'><span class='ui-icon ui-icon-circle-triangle-"+(Y?"e":"w")+"'>"+i+"</span></a>",n=this._get(e,"nextText"),n=q?this.formatDate(n,this._daylightSavingAdjust(new Date(et,Z+U,1)),this._getFormatConfig(e)):n,a=this._canAdjustMonth(e,1,et,Z)?"<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='"+n+"'><span class='ui-icon ui-icon-circle-triangle-"+(Y?"w":"e")+"'>"+n+"</span></a>":J?"":"<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='"+n+"'><span class='ui-icon ui-icon-circle-triangle-"+(Y?"w":"e")+"'>"+n+"</span></a>",o=this._get(e,"currentText"),r=this._get(e,"gotoCurrent")&&e.currentDay?G:R,o=q?this.formatDate(o,r,this._getFormatConfig(e)):o,h=e.inline?"":"<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>"+this._get(e,"closeText")+"</button>",l=B?"<div class='ui-datepicker-buttonpane ui-widget-content'>"+(Y?h:"")+(this._isInRange(e,r)?"<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>"+o+"</button>":"")+(Y?"":h)+"</div>":"",u=parseInt(this._get(e,"firstDay"),10),u=isNaN(u)?0:u,d=this._get(e,"showWeek"),c=this._get(e,"dayNames"),p=this._get(e,"dayNamesMin"),f=this._get(e,"monthNames"),m=this._get(e,"monthNamesShort"),g=this._get(e,"beforeShowDay"),v=this._get(e,"showOtherMonths"),y=this._get(e,"selectOtherMonths"),b=this._getDefaultDate(e),_="",w=0;K[0]>w;w++){for(k="",this.maxRows=4,T=0;K[1]>T;T++){if(D=this._daylightSavingAdjust(new Date(et,Z,e.selectedDay)),S=" ui-corner-all",M="",Q){if(M+="<div class='ui-datepicker-group",K[1]>1)switch(T){case 0:M+=" ui-datepicker-group-first",S=" ui-corner-"+(Y?"right":"left");break;case K[1]-1:M+=" ui-datepicker-group-last",S=" ui-corner-"+(Y?"left":"right");break;default:M+=" ui-datepicker-group-middle",S=""}M+="'>"}for(M+="<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix"+S+"'>"+(/all|left/.test(S)&&0===w?Y?a:s:"")+(/all|right/.test(S)&&0===w?Y?s:a:"")+this._generateMonthYearHeader(e,Z,et,X,$,w>0||T>0,f,m)+"</div><table class='ui-datepicker-calendar'><thead>"+"<tr>",N=d?"<th class='ui-datepicker-week-col'>"+this._get(e,"weekHeader")+"</th>":"",x=0;7>x;x++)C=(x+u)%7,N+="<th scope='col'"+((x+u+6)%7>=5?" class='ui-datepicker-week-end'":"")+">"+"<span title='"+c[C]+"'>"+p[C]+"</span></th>";for(M+=N+"</tr></thead><tbody>",A=this._getDaysInMonth(et,Z),et===e.selectedYear&&Z===e.selectedMonth&&(e.selectedDay=Math.min(e.selectedDay,A)),I=(this._getFirstDayOfMonth(et,Z)-u+7)%7,P=Math.ceil((I+A)/7),z=Q?this.maxRows>P?this.maxRows:P:P,this.maxRows=z,H=this._daylightSavingAdjust(new Date(et,Z,1-I)),F=0;z>F;F++){for(M+="<tr>",E=d?"<td class='ui-datepicker-week-col'>"+this._get(e,"calculateWeek")(H)+"</td>":"",x=0;7>x;x++)j=g?g.apply(e.input?e.input[0]:null,[H]):[!0,""],O=H.getMonth()!==Z,W=O&&!y||!j[0]||X&&X>H||$&&H>$,E+="<td class='"+((x+u+6)%7>=5?" ui-datepicker-week-end":"")+(O?" ui-datepicker-other-month":"")+(H.getTime()===D.getTime()&&Z===e.selectedMonth&&e._keyEvent||b.getTime()===H.getTime()&&b.getTime()===D.getTime()?" "+this._dayOverClass:"")+(W?" "+this._unselectableClass+" ui-state-disabled":"")+(O&&!v?"":" "+j[1]+(H.getTime()===G.getTime()?" "+this._currentClass:"")+(H.getTime()===R.getTime()?" ui-datepicker-today":""))+"'"+(O&&!v||!j[2]?"":" title='"+j[2].replace(/'/g,"&#39;")+"'")+(W?"":" data-handler='selectDay' data-event='click' data-month='"+H.getMonth()+"' data-year='"+H.getFullYear()+"'")+">"+(O&&!v?"&#xa0;":W?"<span class='ui-state-default'>"+H.getDate()+"</span>":"<a class='ui-state-default"+(H.getTime()===R.getTime()?" ui-state-highlight":"")+(H.getTime()===G.getTime()?" ui-state-active":"")+(O?" ui-priority-secondary":"")+"' href='#'>"+H.getDate()+"</a>")+"</td>",H.setDate(H.getDate()+1),H=this._daylightSavingAdjust(H);M+=E+"</tr>"}Z++,Z>11&&(Z=0,et++),M+="</tbody></table>"+(Q?"</div>"+(K[0]>0&&T===K[1]-1?"<div class='ui-datepicker-row-break'></div>":""):""),k+=M}_+=k}return _+=l,e._keyEvent=!1,_},_generateMonthYearHeader:function(e,t,i,s,n,a,o,r){var h,l,u,d,c,p,f,m,g=this._get(e,"changeMonth"),v=this._get(e,"changeYear"),y=this._get(e,"showMonthAfterYear"),b="<div class='ui-datepicker-title'>",_="";if(a||!g)_+="<span class='ui-datepicker-month'>"+o[t]+"</span>";else{for(h=s&&s.getFullYear()===i,l=n&&n.getFullYear()===i,_+="<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>",u=0;12>u;u++)(!h||u>=s.getMonth())&&(!l||n.getMonth()>=u)&&(_+="<option value='"+u+"'"+(u===t?" selected='selected'":"")+">"+r[u]+"</option>");_+="</select>"}if(y||(b+=_+(!a&&g&&v?"":"&#xa0;")),!e.yearshtml)if(e.yearshtml="",a||!v)b+="<span class='ui-datepicker-year'>"+i+"</span>";else{for(d=this._get(e,"yearRange").split(":"),c=(new Date).getFullYear(),p=function(e){var t=e.match(/c[+\-].*/)?i+parseInt(e.substring(1),10):e.match(/[+\-].*/)?c+parseInt(e,10):parseInt(e,10);return isNaN(t)?c:t},f=p(d[0]),m=Math.max(f,p(d[1]||"")),f=s?Math.max(f,s.getFullYear()):f,m=n?Math.min(m,n.getFullYear()):m,e.yearshtml+="<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>";m>=f;f++)e.yearshtml+="<option value='"+f+"'"+(f===i?" selected='selected'":"")+">"+f+"</option>";e.yearshtml+="</select>",b+=e.yearshtml,e.yearshtml=null}return b+=this._get(e,"yearSuffix"),y&&(b+=(!a&&g&&v?"":"&#xa0;")+_),b+="</div>"},_adjustInstDate:function(e,t,i){var s=e.drawYear+("Y"===i?t:0),n=e.drawMonth+("M"===i?t:0),a=Math.min(e.selectedDay,this._getDaysInMonth(s,n))+("D"===i?t:0),o=this._restrictMinMax(e,this._daylightSavingAdjust(new Date(s,n,a)));e.selectedDay=o.getDate(),e.drawMonth=e.selectedMonth=o.getMonth(),e.drawYear=e.selectedYear=o.getFullYear(),("M"===i||"Y"===i)&&this._notifyChange(e)},_restrictMinMax:function(e,t){var i=this._getMinMaxDate(e,"min"),s=this._getMinMaxDate(e,"max"),n=i&&i>t?i:t;return s&&n>s?s:n},_notifyChange:function(e){var t=this._get(e,"onChangeMonthYear");t&&t.apply(e.input?e.input[0]:null,[e.selectedYear,e.selectedMonth+1,e])},_getNumberOfMonths:function(e){var t=this._get(e,"numberOfMonths");return null==t?[1,1]:"number"==typeof t?[1,t]:t},_getMinMaxDate:function(e,t){return this._determineDate(e,this._get(e,t+"Date"),null)},_getDaysInMonth:function(e,t){return 32-this._daylightSavingAdjust(new Date(e,t,32)).getDate()},_getFirstDayOfMonth:function(e,t){return new Date(e,t,1).getDay()},_canAdjustMonth:function(e,t,i,s){var n=this._getNumberOfMonths(e),a=this._daylightSavingAdjust(new Date(i,s+(0>t?t:n[0]*n[1]),1));return 0>t&&a.setDate(this._getDaysInMonth(a.getFullYear(),a.getMonth())),this._isInRange(e,a)},_isInRange:function(e,t){var i,s,n=this._getMinMaxDate(e,"min"),a=this._getMinMaxDate(e,"max"),o=null,r=null,h=this._get(e,"yearRange");return h&&(i=h.split(":"),s=(new Date).getFullYear(),o=parseInt(i[0],10),r=parseInt(i[1],10),i[0].match(/[+\-].*/)&&(o+=s),i[1].match(/[+\-].*/)&&(r+=s)),(!n||t.getTime()>=n.getTime())&&(!a||t.getTime()<=a.getTime())&&(!o||t.getFullYear()>=o)&&(!r||r>=t.getFullYear())},_getFormatConfig:function(e){var t=this._get(e,"shortYearCutoff");return t="string"!=typeof t?t:(new Date).getFullYear()%100+parseInt(t,10),{shortYearCutoff:t,dayNamesShort:this._get(e,"dayNamesShort"),dayNames:this._get(e,"dayNames"),monthNamesShort:this._get(e,"monthNamesShort"),monthNames:this._get(e,"monthNames")}},_formatDate:function(e,t,i,s){t||(e.currentDay=e.selectedDay,e.currentMonth=e.selectedMonth,e.currentYear=e.selectedYear);var n=t?"object"==typeof t?t:this._daylightSavingAdjust(new Date(s,i,t)):this._daylightSavingAdjust(new Date(e.currentYear,e.currentMonth,e.currentDay));return this.formatDate(this._get(e,"dateFormat"),n,this._getFormatConfig(e))}}),e.fn.datepicker=function(t){if(!this.length)return this;e.datepicker.initialized||(e(document).mousedown(e.datepicker._checkExternalClick),e.datepicker.initialized=!0),0===e("#"+e.datepicker._mainDivId).length&&e("body").append(e.datepicker.dpDiv);var i=Array.prototype.slice.call(arguments,1);return"string"!=typeof t||"isDisabled"!==t&&"getDate"!==t&&"widget"!==t?"option"===t&&2===arguments.length&&"string"==typeof arguments[1]?e.datepicker["_"+t+"Datepicker"].apply(e.datepicker,[this[0]].concat(i)):this.each(function(){"string"==typeof t?e.datepicker["_"+t+"Datepicker"].apply(e.datepicker,[this].concat(i)):e.datepicker._attachDatepicker(this,t)}):e.datepicker["_"+t+"Datepicker"].apply(e.datepicker,[this[0]].concat(i))},e.datepicker=new n,e.datepicker.initialized=!1,e.datepicker.uuid=(new Date).getTime(),e.datepicker.version="1.11.0",e.datepicker,e.widget("ui.draggable",e.ui.mouse,{version:"1.11.0",widgetEventPrefix:"drag",options:{addClasses:!0,appendTo:"parent",axis:!1,connectToSortable:!1,containment:!1,cursor:"auto",cursorAt:!1,grid:!1,handle:!1,helper:"original",iframeFix:!1,opacity:!1,refreshPositions:!1,revert:!1,revertDuration:500,scope:"default",scroll:!0,scrollSensitivity:20,scrollSpeed:20,snap:!1,snapMode:"both",snapTolerance:20,stack:!1,zIndex:!1,drag:null,start:null,stop:null},_create:function(){"original"!==this.options.helper||/^(?:r|a|f)/.test(this.element.css("position"))||(this.element[0].style.position="relative"),this.options.addClasses&&this.element.addClass("ui-draggable"),this.options.disabled&&this.element.addClass("ui-draggable-disabled"),this._setHandleClassName(),this._mouseInit()},_setOption:function(e,t){this._super(e,t),"handle"===e&&this._setHandleClassName()},_destroy:function(){return(this.helper||this.element).is(".ui-draggable-dragging")?(this.destroyOnClear=!0,void 0):(this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled"),this._removeHandleClassName(),this._mouseDestroy(),void 0)},_mouseCapture:function(t){var i=this.document[0],s=this.options;try{i.activeElement&&"body"!==i.activeElement.nodeName.toLowerCase()&&e(i.activeElement).blur()}catch(n){}return this.helper||s.disabled||e(t.target).closest(".ui-resizable-handle").length>0?!1:(this.handle=this._getHandle(t),this.handle?(e(s.iframeFix===!0?"iframe":s.iframeFix).each(function(){e("<div class='ui-draggable-iframeFix' style='background: #fff;'></div>").css({width:this.offsetWidth+"px",height:this.offsetHeight+"px",position:"absolute",opacity:"0.001",zIndex:1e3}).css(e(this).offset()).appendTo("body")}),!0):!1)},_mouseStart:function(t){var i=this.options;return this.helper=this._createHelper(t),this.helper.addClass("ui-draggable-dragging"),this._cacheHelperProportions(),e.ui.ddmanager&&(e.ui.ddmanager.current=this),this._cacheMargins(),this.cssPosition=this.helper.css("position"),this.scrollParent=this.helper.scrollParent(),this.offsetParent=this.helper.offsetParent(),this.offsetParentCssPosition=this.offsetParent.css("position"),this.offset=this.positionAbs=this.element.offset(),this.offset={top:this.offset.top-this.margins.top,left:this.offset.left-this.margins.left},this.offset.scroll=!1,e.extend(this.offset,{click:{left:t.pageX-this.offset.left,top:t.pageY-this.offset.top},parent:this._getParentOffset(),relative:this._getRelativeOffset()}),this.originalPosition=this.position=this._generatePosition(t,!1),this.originalPageX=t.pageX,this.originalPageY=t.pageY,i.cursorAt&&this._adjustOffsetFromHelper(i.cursorAt),this._setContainment(),this._trigger("start",t)===!1?(this._clear(),!1):(this._cacheHelperProportions(),e.ui.ddmanager&&!i.dropBehaviour&&e.ui.ddmanager.prepareOffsets(this,t),this._mouseDrag(t,!0),e.ui.ddmanager&&e.ui.ddmanager.dragStart(this,t),!0)},_mouseDrag:function(t,i){if("fixed"===this.offsetParentCssPosition&&(this.offset.parent=this._getParentOffset()),this.position=this._generatePosition(t,!0),this.positionAbs=this._convertPositionTo("absolute"),!i){var s=this._uiHash();if(this._trigger("drag",t,s)===!1)return this._mouseUp({}),!1;this.position=s.position}return this.helper[0].style.left=this.position.left+"px",this.helper[0].style.top=this.position.top+"px",e.ui.ddmanager&&e.ui.ddmanager.drag(this,t),!1},_mouseStop:function(t){var i=this,s=!1;return e.ui.ddmanager&&!this.options.dropBehaviour&&(s=e.ui.ddmanager.drop(this,t)),this.dropped&&(s=this.dropped,this.dropped=!1),"invalid"===this.options.revert&&!s||"valid"===this.options.revert&&s||this.options.revert===!0||e.isFunction(this.options.revert)&&this.options.revert.call(this.element,s)?e(this.helper).animate(this.originalPosition,parseInt(this.options.revertDuration,10),function(){i._trigger("stop",t)!==!1&&i._clear()}):this._trigger("stop",t)!==!1&&this._clear(),!1},_mouseUp:function(t){return e("div.ui-draggable-iframeFix").each(function(){this.parentNode.removeChild(this)}),e.ui.ddmanager&&e.ui.ddmanager.dragStop(this,t),this.element.focus(),e.ui.mouse.prototype._mouseUp.call(this,t)},cancel:function(){return this.helper.is(".ui-draggable-dragging")?this._mouseUp({}):this._clear(),this},_getHandle:function(t){return this.options.handle?!!e(t.target).closest(this.element.find(this.options.handle)).length:!0},_setHandleClassName:function(){this._removeHandleClassName(),e(this.options.handle||this.element).addClass("ui-draggable-handle")},_removeHandleClassName:function(){this.element.find(".ui-draggable-handle").addBack().removeClass("ui-draggable-handle")},_createHelper:function(t){var i=this.options,s=e.isFunction(i.helper)?e(i.helper.apply(this.element[0],[t])):"clone"===i.helper?this.element.clone().removeAttr("id"):this.element;return s.parents("body").length||s.appendTo("parent"===i.appendTo?this.element[0].parentNode:i.appendTo),s[0]===this.element[0]||/(fixed|absolute)/.test(s.css("position"))||s.css("position","absolute"),s},_adjustOffsetFromHelper:function(t){"string"==typeof t&&(t=t.split(" ")),e.isArray(t)&&(t={left:+t[0],top:+t[1]||0}),"left"in t&&(this.offset.click.left=t.left+this.margins.left),"right"in t&&(this.offset.click.left=this.helperProportions.width-t.right+this.margins.left),"top"in t&&(this.offset.click.top=t.top+this.margins.top),"bottom"in t&&(this.offset.click.top=this.helperProportions.height-t.bottom+this.margins.top)},_isRootNode:function(e){return/(html|body)/i.test(e.tagName)||e===this.document[0]},_getParentOffset:function(){var t=this.offsetParent.offset(),i=this.document[0];return"absolute"===this.cssPosition&&this.scrollParent[0]!==i&&e.contains(this.scrollParent[0],this.offsetParent[0])&&(t.left+=this.scrollParent.scrollLeft(),t.top+=this.scrollParent.scrollTop()),this._isRootNode(this.offsetParent[0])&&(t={top:0,left:0}),{top:t.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:t.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}},_getRelativeOffset:function(){if("relative"!==this.cssPosition)return{top:0,left:0};var e=this.element.position(),t=this._isRootNode(this.scrollParent[0]);return{top:e.top-(parseInt(this.helper.css("top"),10)||0)+(t?0:this.scrollParent.scrollTop()),left:e.left-(parseInt(this.helper.css("left"),10)||0)+(t?0:this.scrollParent.scrollLeft())}},_cacheMargins:function(){this.margins={left:parseInt(this.element.css("marginLeft"),10)||0,top:parseInt(this.element.css("marginTop"),10)||0,right:parseInt(this.element.css("marginRight"),10)||0,bottom:parseInt(this.element.css("marginBottom"),10)||0}},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}},_setContainment:function(){var t,i,s,n=this.options,a=this.document[0];return this.relative_container=null,n.containment?"window"===n.containment?(this.containment=[e(window).scrollLeft()-this.offset.relative.left-this.offset.parent.left,e(window).scrollTop()-this.offset.relative.top-this.offset.parent.top,e(window).scrollLeft()+e(window).width()-this.helperProportions.width-this.margins.left,e(window).scrollTop()+(e(window).height()||a.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top],void 0):"document"===n.containment?(this.containment=[0,0,e(a).width()-this.helperProportions.width-this.margins.left,(e(a).height()||a.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top],void 0):n.containment.constructor===Array?(this.containment=n.containment,void 0):("parent"===n.containment&&(n.containment=this.helper[0].parentNode),i=e(n.containment),s=i[0],s&&(t="hidden"!==i.css("overflow"),this.containment=[(parseInt(i.css("borderLeftWidth"),10)||0)+(parseInt(i.css("paddingLeft"),10)||0),(parseInt(i.css("borderTopWidth"),10)||0)+(parseInt(i.css("paddingTop"),10)||0),(t?Math.max(s.scrollWidth,s.offsetWidth):s.offsetWidth)-(parseInt(i.css("borderRightWidth"),10)||0)-(parseInt(i.css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left-this.margins.right,(t?Math.max(s.scrollHeight,s.offsetHeight):s.offsetHeight)-(parseInt(i.css("borderBottomWidth"),10)||0)-(parseInt(i.css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top-this.margins.bottom],this.relative_container=i),void 0):(this.containment=null,void 0)},_convertPositionTo:function(e,t){t||(t=this.position);var i="absolute"===e?1:-1,s=this._isRootNode(this.scrollParent[0]);return{top:t.top+this.offset.relative.top*i+this.offset.parent.top*i-("fixed"===this.cssPosition?-this.offset.scroll.top:s?0:this.offset.scroll.top)*i,left:t.left+this.offset.relative.left*i+this.offset.parent.left*i-("fixed"===this.cssPosition?-this.offset.scroll.left:s?0:this.offset.scroll.left)*i}
},_generatePosition:function(e,t){var i,s,n,a,o=this.options,r=this._isRootNode(this.scrollParent[0]),h=e.pageX,l=e.pageY;return r&&this.offset.scroll||(this.offset.scroll={top:this.scrollParent.scrollTop(),left:this.scrollParent.scrollLeft()}),t&&(this.containment&&(this.relative_container?(s=this.relative_container.offset(),i=[this.containment[0]+s.left,this.containment[1]+s.top,this.containment[2]+s.left,this.containment[3]+s.top]):i=this.containment,e.pageX-this.offset.click.left<i[0]&&(h=i[0]+this.offset.click.left),e.pageY-this.offset.click.top<i[1]&&(l=i[1]+this.offset.click.top),e.pageX-this.offset.click.left>i[2]&&(h=i[2]+this.offset.click.left),e.pageY-this.offset.click.top>i[3]&&(l=i[3]+this.offset.click.top)),o.grid&&(n=o.grid[1]?this.originalPageY+Math.round((l-this.originalPageY)/o.grid[1])*o.grid[1]:this.originalPageY,l=i?n-this.offset.click.top>=i[1]||n-this.offset.click.top>i[3]?n:n-this.offset.click.top>=i[1]?n-o.grid[1]:n+o.grid[1]:n,a=o.grid[0]?this.originalPageX+Math.round((h-this.originalPageX)/o.grid[0])*o.grid[0]:this.originalPageX,h=i?a-this.offset.click.left>=i[0]||a-this.offset.click.left>i[2]?a:a-this.offset.click.left>=i[0]?a-o.grid[0]:a+o.grid[0]:a),"y"===o.axis&&(h=this.originalPageX),"x"===o.axis&&(l=this.originalPageY)),{top:l-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+("fixed"===this.cssPosition?-this.offset.scroll.top:r?0:this.offset.scroll.top),left:h-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+("fixed"===this.cssPosition?-this.offset.scroll.left:r?0:this.offset.scroll.left)}},_clear:function(){this.helper.removeClass("ui-draggable-dragging"),this.helper[0]===this.element[0]||this.cancelHelperRemoval||this.helper.remove(),this.helper=null,this.cancelHelperRemoval=!1,this.destroyOnClear&&this.destroy()},_trigger:function(t,i,s){return s=s||this._uiHash(),e.ui.plugin.call(this,t,[i,s,this],!0),"drag"===t&&(this.positionAbs=this._convertPositionTo("absolute")),e.Widget.prototype._trigger.call(this,t,i,s)},plugins:{},_uiHash:function(){return{helper:this.helper,position:this.position,originalPosition:this.originalPosition,offset:this.positionAbs}}}),e.ui.plugin.add("draggable","connectToSortable",{start:function(t,i,s){var n=s.options,a=e.extend({},i,{item:s.element});s.sortables=[],e(n.connectToSortable).each(function(){var i=e(this).sortable("instance");i&&!i.options.disabled&&(s.sortables.push({instance:i,shouldRevert:i.options.revert}),i.refreshPositions(),i._trigger("activate",t,a))})},stop:function(t,i,s){var n=e.extend({},i,{item:s.element});e.each(s.sortables,function(){this.instance.isOver?(this.instance.isOver=0,s.cancelHelperRemoval=!0,this.instance.cancelHelperRemoval=!1,this.shouldRevert&&(this.instance.options.revert=this.shouldRevert),this.instance._mouseStop(t),this.instance.options.helper=this.instance.options._helper,"original"===s.options.helper&&this.instance.currentItem.css({top:"auto",left:"auto"})):(this.instance.cancelHelperRemoval=!1,this.instance._trigger("deactivate",t,n))})},drag:function(t,i,s){var n=this;e.each(s.sortables,function(){var a=!1,o=this;this.instance.positionAbs=s.positionAbs,this.instance.helperProportions=s.helperProportions,this.instance.offset.click=s.offset.click,this.instance._intersectsWith(this.instance.containerCache)&&(a=!0,e.each(s.sortables,function(){return this.instance.positionAbs=s.positionAbs,this.instance.helperProportions=s.helperProportions,this.instance.offset.click=s.offset.click,this!==o&&this.instance._intersectsWith(this.instance.containerCache)&&e.contains(o.instance.element[0],this.instance.element[0])&&(a=!1),a})),a?(this.instance.isOver||(this.instance.isOver=1,this.instance.currentItem=e(n).clone().removeAttr("id").appendTo(this.instance.element).data("ui-sortable-item",!0),this.instance.options._helper=this.instance.options.helper,this.instance.options.helper=function(){return i.helper[0]},t.target=this.instance.currentItem[0],this.instance._mouseCapture(t,!0),this.instance._mouseStart(t,!0,!0),this.instance.offset.click.top=s.offset.click.top,this.instance.offset.click.left=s.offset.click.left,this.instance.offset.parent.left-=s.offset.parent.left-this.instance.offset.parent.left,this.instance.offset.parent.top-=s.offset.parent.top-this.instance.offset.parent.top,s._trigger("toSortable",t),s.dropped=this.instance.element,s.currentItem=s.element,this.instance.fromOutside=s),this.instance.currentItem&&this.instance._mouseDrag(t)):this.instance.isOver&&(this.instance.isOver=0,this.instance.cancelHelperRemoval=!0,this.instance.options.revert=!1,this.instance._trigger("out",t,this.instance._uiHash(this.instance)),this.instance._mouseStop(t,!0),this.instance.options.helper=this.instance.options._helper,this.instance.currentItem.remove(),this.instance.placeholder&&this.instance.placeholder.remove(),s._trigger("fromSortable",t),s.dropped=!1)})}}),e.ui.plugin.add("draggable","cursor",{start:function(t,i,s){var n=e("body"),a=s.options;n.css("cursor")&&(a._cursor=n.css("cursor")),n.css("cursor",a.cursor)},stop:function(t,i,s){var n=s.options;n._cursor&&e("body").css("cursor",n._cursor)}}),e.ui.plugin.add("draggable","opacity",{start:function(t,i,s){var n=e(i.helper),a=s.options;n.css("opacity")&&(a._opacity=n.css("opacity")),n.css("opacity",a.opacity)},stop:function(t,i,s){var n=s.options;n._opacity&&e(i.helper).css("opacity",n._opacity)}}),e.ui.plugin.add("draggable","scroll",{start:function(e,t,i){i.scrollParent[0]!==i.document[0]&&"HTML"!==i.scrollParent[0].tagName&&(i.overflowOffset=i.scrollParent.offset())},drag:function(t,i,s){var n=s.options,a=!1,o=s.document[0];s.scrollParent[0]!==o&&"HTML"!==s.scrollParent[0].tagName?(n.axis&&"x"===n.axis||(s.overflowOffset.top+s.scrollParent[0].offsetHeight-t.pageY<n.scrollSensitivity?s.scrollParent[0].scrollTop=a=s.scrollParent[0].scrollTop+n.scrollSpeed:t.pageY-s.overflowOffset.top<n.scrollSensitivity&&(s.scrollParent[0].scrollTop=a=s.scrollParent[0].scrollTop-n.scrollSpeed)),n.axis&&"y"===n.axis||(s.overflowOffset.left+s.scrollParent[0].offsetWidth-t.pageX<n.scrollSensitivity?s.scrollParent[0].scrollLeft=a=s.scrollParent[0].scrollLeft+n.scrollSpeed:t.pageX-s.overflowOffset.left<n.scrollSensitivity&&(s.scrollParent[0].scrollLeft=a=s.scrollParent[0].scrollLeft-n.scrollSpeed))):(n.axis&&"x"===n.axis||(t.pageY-e(o).scrollTop()<n.scrollSensitivity?a=e(o).scrollTop(e(o).scrollTop()-n.scrollSpeed):e(window).height()-(t.pageY-e(o).scrollTop())<n.scrollSensitivity&&(a=e(o).scrollTop(e(o).scrollTop()+n.scrollSpeed))),n.axis&&"y"===n.axis||(t.pageX-e(o).scrollLeft()<n.scrollSensitivity?a=e(o).scrollLeft(e(o).scrollLeft()-n.scrollSpeed):e(window).width()-(t.pageX-e(o).scrollLeft())<n.scrollSensitivity&&(a=e(o).scrollLeft(e(o).scrollLeft()+n.scrollSpeed)))),a!==!1&&e.ui.ddmanager&&!n.dropBehaviour&&e.ui.ddmanager.prepareOffsets(s,t)}}),e.ui.plugin.add("draggable","snap",{start:function(t,i,s){var n=s.options;s.snapElements=[],e(n.snap.constructor!==String?n.snap.items||":data(ui-draggable)":n.snap).each(function(){var t=e(this),i=t.offset();this!==s.element[0]&&s.snapElements.push({item:this,width:t.outerWidth(),height:t.outerHeight(),top:i.top,left:i.left})})},drag:function(t,i,s){var n,a,o,r,h,l,u,d,c,p,f=s.options,m=f.snapTolerance,g=i.offset.left,v=g+s.helperProportions.width,y=i.offset.top,b=y+s.helperProportions.height;for(c=s.snapElements.length-1;c>=0;c--)h=s.snapElements[c].left,l=h+s.snapElements[c].width,u=s.snapElements[c].top,d=u+s.snapElements[c].height,h-m>v||g>l+m||u-m>b||y>d+m||!e.contains(s.snapElements[c].item.ownerDocument,s.snapElements[c].item)?(s.snapElements[c].snapping&&s.options.snap.release&&s.options.snap.release.call(s.element,t,e.extend(s._uiHash(),{snapItem:s.snapElements[c].item})),s.snapElements[c].snapping=!1):("inner"!==f.snapMode&&(n=m>=Math.abs(u-b),a=m>=Math.abs(d-y),o=m>=Math.abs(h-v),r=m>=Math.abs(l-g),n&&(i.position.top=s._convertPositionTo("relative",{top:u-s.helperProportions.height,left:0}).top-s.margins.top),a&&(i.position.top=s._convertPositionTo("relative",{top:d,left:0}).top-s.margins.top),o&&(i.position.left=s._convertPositionTo("relative",{top:0,left:h-s.helperProportions.width}).left-s.margins.left),r&&(i.position.left=s._convertPositionTo("relative",{top:0,left:l}).left-s.margins.left)),p=n||a||o||r,"outer"!==f.snapMode&&(n=m>=Math.abs(u-y),a=m>=Math.abs(d-b),o=m>=Math.abs(h-g),r=m>=Math.abs(l-v),n&&(i.position.top=s._convertPositionTo("relative",{top:u,left:0}).top-s.margins.top),a&&(i.position.top=s._convertPositionTo("relative",{top:d-s.helperProportions.height,left:0}).top-s.margins.top),o&&(i.position.left=s._convertPositionTo("relative",{top:0,left:h}).left-s.margins.left),r&&(i.position.left=s._convertPositionTo("relative",{top:0,left:l-s.helperProportions.width}).left-s.margins.left)),!s.snapElements[c].snapping&&(n||a||o||r||p)&&s.options.snap.snap&&s.options.snap.snap.call(s.element,t,e.extend(s._uiHash(),{snapItem:s.snapElements[c].item})),s.snapElements[c].snapping=n||a||o||r||p)}}),e.ui.plugin.add("draggable","stack",{start:function(t,i,s){var n,a=s.options,o=e.makeArray(e(a.stack)).sort(function(t,i){return(parseInt(e(t).css("zIndex"),10)||0)-(parseInt(e(i).css("zIndex"),10)||0)});o.length&&(n=parseInt(e(o[0]).css("zIndex"),10)||0,e(o).each(function(t){e(this).css("zIndex",n+t)}),this.css("zIndex",n+o.length))}}),e.ui.plugin.add("draggable","zIndex",{start:function(t,i,s){var n=e(i.helper),a=s.options;n.css("zIndex")&&(a._zIndex=n.css("zIndex")),n.css("zIndex",a.zIndex)},stop:function(t,i,s){var n=s.options;n._zIndex&&e(i.helper).css("zIndex",n._zIndex)}}),e.ui.draggable,e.widget("ui.resizable",e.ui.mouse,{version:"1.11.0",widgetEventPrefix:"resize",options:{alsoResize:!1,animate:!1,animateDuration:"slow",animateEasing:"swing",aspectRatio:!1,autoHide:!1,containment:!1,ghost:!1,grid:!1,handles:"e,s,se",helper:!1,maxHeight:null,maxWidth:null,minHeight:10,minWidth:10,zIndex:90,resize:null,start:null,stop:null},_num:function(e){return parseInt(e,10)||0},_isNumber:function(e){return!isNaN(parseInt(e,10))},_hasScroll:function(t,i){if("hidden"===e(t).css("overflow"))return!1;var s=i&&"left"===i?"scrollLeft":"scrollTop",n=!1;return t[s]>0?!0:(t[s]=1,n=t[s]>0,t[s]=0,n)},_create:function(){var t,i,s,n,a,o=this,r=this.options;if(this.element.addClass("ui-resizable"),e.extend(this,{_aspectRatio:!!r.aspectRatio,aspectRatio:r.aspectRatio,originalElement:this.element,_proportionallyResizeElements:[],_helper:r.helper||r.ghost||r.animate?r.helper||"ui-resizable-helper":null}),this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i)&&(this.element.wrap(e("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({position:this.element.css("position"),width:this.element.outerWidth(),height:this.element.outerHeight(),top:this.element.css("top"),left:this.element.css("left")})),this.element=this.element.parent().data("ui-resizable",this.element.resizable("instance")),this.elementIsWrapper=!0,this.element.css({marginLeft:this.originalElement.css("marginLeft"),marginTop:this.originalElement.css("marginTop"),marginRight:this.originalElement.css("marginRight"),marginBottom:this.originalElement.css("marginBottom")}),this.originalElement.css({marginLeft:0,marginTop:0,marginRight:0,marginBottom:0}),this.originalResizeStyle=this.originalElement.css("resize"),this.originalElement.css("resize","none"),this._proportionallyResizeElements.push(this.originalElement.css({position:"static",zoom:1,display:"block"})),this.originalElement.css({margin:this.originalElement.css("margin")}),this._proportionallyResize()),this.handles=r.handles||(e(".ui-resizable-handle",this.element).length?{n:".ui-resizable-n",e:".ui-resizable-e",s:".ui-resizable-s",w:".ui-resizable-w",se:".ui-resizable-se",sw:".ui-resizable-sw",ne:".ui-resizable-ne",nw:".ui-resizable-nw"}:"e,s,se"),this.handles.constructor===String)for("all"===this.handles&&(this.handles="n,e,s,w,se,sw,ne,nw"),t=this.handles.split(","),this.handles={},i=0;t.length>i;i++)s=e.trim(t[i]),a="ui-resizable-"+s,n=e("<div class='ui-resizable-handle "+a+"'></div>"),n.css({zIndex:r.zIndex}),"se"===s&&n.addClass("ui-icon ui-icon-gripsmall-diagonal-se"),this.handles[s]=".ui-resizable-"+s,this.element.append(n);this._renderAxis=function(t){var i,s,n,a;t=t||this.element;for(i in this.handles)this.handles[i].constructor===String&&(this.handles[i]=this.element.children(this.handles[i]).first().show()),this.elementIsWrapper&&this.originalElement[0].nodeName.match(/textarea|input|select|button/i)&&(s=e(this.handles[i],this.element),a=/sw|ne|nw|se|n|s/.test(i)?s.outerHeight():s.outerWidth(),n=["padding",/ne|nw|n/.test(i)?"Top":/se|sw|s/.test(i)?"Bottom":/^e$/.test(i)?"Right":"Left"].join(""),t.css(n,a),this._proportionallyResize()),e(this.handles[i]).length},this._renderAxis(this.element),this._handles=e(".ui-resizable-handle",this.element).disableSelection(),this._handles.mouseover(function(){o.resizing||(this.className&&(n=this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i)),o.axis=n&&n[1]?n[1]:"se")}),r.autoHide&&(this._handles.hide(),e(this.element).addClass("ui-resizable-autohide").mouseenter(function(){r.disabled||(e(this).removeClass("ui-resizable-autohide"),o._handles.show())}).mouseleave(function(){r.disabled||o.resizing||(e(this).addClass("ui-resizable-autohide"),o._handles.hide())})),this._mouseInit()},_destroy:function(){this._mouseDestroy();var t,i=function(t){e(t).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").removeData("ui-resizable").unbind(".resizable").find(".ui-resizable-handle").remove()};return this.elementIsWrapper&&(i(this.element),t=this.element,this.originalElement.css({position:t.css("position"),width:t.outerWidth(),height:t.outerHeight(),top:t.css("top"),left:t.css("left")}).insertAfter(t),t.remove()),this.originalElement.css("resize",this.originalResizeStyle),i(this.originalElement),this},_mouseCapture:function(t){var i,s,n=!1;for(i in this.handles)s=e(this.handles[i])[0],(s===t.target||e.contains(s,t.target))&&(n=!0);return!this.options.disabled&&n},_mouseStart:function(t){var i,s,n,a=this.options,o=this.element;return this.resizing=!0,this._renderProxy(),i=this._num(this.helper.css("left")),s=this._num(this.helper.css("top")),a.containment&&(i+=e(a.containment).scrollLeft()||0,s+=e(a.containment).scrollTop()||0),this.offset=this.helper.offset(),this.position={left:i,top:s},this.size=this._helper?{width:this.helper.width(),height:this.helper.height()}:{width:o.width(),height:o.height()},this.originalSize=this._helper?{width:o.outerWidth(),height:o.outerHeight()}:{width:o.width(),height:o.height()},this.originalPosition={left:i,top:s},this.sizeDiff={width:o.outerWidth()-o.width(),height:o.outerHeight()-o.height()},this.originalMousePosition={left:t.pageX,top:t.pageY},this.aspectRatio="number"==typeof a.aspectRatio?a.aspectRatio:this.originalSize.width/this.originalSize.height||1,n=e(".ui-resizable-"+this.axis).css("cursor"),e("body").css("cursor","auto"===n?this.axis+"-resize":n),o.addClass("ui-resizable-resizing"),this._propagate("start",t),!0},_mouseDrag:function(t){var i,s=this.helper,n={},a=this.originalMousePosition,o=this.axis,r=t.pageX-a.left||0,h=t.pageY-a.top||0,l=this._change[o];return this.prevPosition={top:this.position.top,left:this.position.left},this.prevSize={width:this.size.width,height:this.size.height},l?(i=l.apply(this,[t,r,h]),this._updateVirtualBoundaries(t.shiftKey),(this._aspectRatio||t.shiftKey)&&(i=this._updateRatio(i,t)),i=this._respectSize(i,t),this._updateCache(i),this._propagate("resize",t),this.position.top!==this.prevPosition.top&&(n.top=this.position.top+"px"),this.position.left!==this.prevPosition.left&&(n.left=this.position.left+"px"),this.size.width!==this.prevSize.width&&(n.width=this.size.width+"px"),this.size.height!==this.prevSize.height&&(n.height=this.size.height+"px"),s.css(n),!this._helper&&this._proportionallyResizeElements.length&&this._proportionallyResize(),e.isEmptyObject(n)||this._trigger("resize",t,this.ui()),!1):!1},_mouseStop:function(t){this.resizing=!1;var i,s,n,a,o,r,h,l=this.options,u=this;return this._helper&&(i=this._proportionallyResizeElements,s=i.length&&/textarea/i.test(i[0].nodeName),n=s&&this._hasScroll(i[0],"left")?0:u.sizeDiff.height,a=s?0:u.sizeDiff.width,o={width:u.helper.width()-a,height:u.helper.height()-n},r=parseInt(u.element.css("left"),10)+(u.position.left-u.originalPosition.left)||null,h=parseInt(u.element.css("top"),10)+(u.position.top-u.originalPosition.top)||null,l.animate||this.element.css(e.extend(o,{top:h,left:r})),u.helper.height(u.size.height),u.helper.width(u.size.width),this._helper&&!l.animate&&this._proportionallyResize()),e("body").css("cursor","auto"),this.element.removeClass("ui-resizable-resizing"),this._propagate("stop",t),this._helper&&this.helper.remove(),!1},_updateVirtualBoundaries:function(e){var t,i,s,n,a,o=this.options;a={minWidth:this._isNumber(o.minWidth)?o.minWidth:0,maxWidth:this._isNumber(o.maxWidth)?o.maxWidth:1/0,minHeight:this._isNumber(o.minHeight)?o.minHeight:0,maxHeight:this._isNumber(o.maxHeight)?o.maxHeight:1/0},(this._aspectRatio||e)&&(t=a.minHeight*this.aspectRatio,s=a.minWidth/this.aspectRatio,i=a.maxHeight*this.aspectRatio,n=a.maxWidth/this.aspectRatio,t>a.minWidth&&(a.minWidth=t),s>a.minHeight&&(a.minHeight=s),a.maxWidth>i&&(a.maxWidth=i),a.maxHeight>n&&(a.maxHeight=n)),this._vBoundaries=a},_updateCache:function(e){this.offset=this.helper.offset(),this._isNumber(e.left)&&(this.position.left=e.left),this._isNumber(e.top)&&(this.position.top=e.top),this._isNumber(e.height)&&(this.size.height=e.height),this._isNumber(e.width)&&(this.size.width=e.width)},_updateRatio:function(e){var t=this.position,i=this.size,s=this.axis;return this._isNumber(e.height)?e.width=e.height*this.aspectRatio:this._isNumber(e.width)&&(e.height=e.width/this.aspectRatio),"sw"===s&&(e.left=t.left+(i.width-e.width),e.top=null),"nw"===s&&(e.top=t.top+(i.height-e.height),e.left=t.left+(i.width-e.width)),e},_respectSize:function(e){var t=this._vBoundaries,i=this.axis,s=this._isNumber(e.width)&&t.maxWidth&&t.maxWidth<e.width,n=this._isNumber(e.height)&&t.maxHeight&&t.maxHeight<e.height,a=this._isNumber(e.width)&&t.minWidth&&t.minWidth>e.width,o=this._isNumber(e.height)&&t.minHeight&&t.minHeight>e.height,r=this.originalPosition.left+this.originalSize.width,h=this.position.top+this.size.height,l=/sw|nw|w/.test(i),u=/nw|ne|n/.test(i);return a&&(e.width=t.minWidth),o&&(e.height=t.minHeight),s&&(e.width=t.maxWidth),n&&(e.height=t.maxHeight),a&&l&&(e.left=r-t.minWidth),s&&l&&(e.left=r-t.maxWidth),o&&u&&(e.top=h-t.minHeight),n&&u&&(e.top=h-t.maxHeight),e.width||e.height||e.left||!e.top?e.width||e.height||e.top||!e.left||(e.left=null):e.top=null,e},_proportionallyResize:function(){if(this._proportionallyResizeElements.length){var e,t,i,s,n,a=this.helper||this.element;for(e=0;this._proportionallyResizeElements.length>e;e++){if(n=this._proportionallyResizeElements[e],!this.borderDif)for(this.borderDif=[],i=[n.css("borderTopWidth"),n.css("borderRightWidth"),n.css("borderBottomWidth"),n.css("borderLeftWidth")],s=[n.css("paddingTop"),n.css("paddingRight"),n.css("paddingBottom"),n.css("paddingLeft")],t=0;i.length>t;t++)this.borderDif[t]=(parseInt(i[t],10)||0)+(parseInt(s[t],10)||0);n.css({height:a.height()-this.borderDif[0]-this.borderDif[2]||0,width:a.width()-this.borderDif[1]-this.borderDif[3]||0})}}},_renderProxy:function(){var t=this.element,i=this.options;this.elementOffset=t.offset(),this._helper?(this.helper=this.helper||e("<div style='overflow:hidden;'></div>"),this.helper.addClass(this._helper).css({width:this.element.outerWidth()-1,height:this.element.outerHeight()-1,position:"absolute",left:this.elementOffset.left+"px",top:this.elementOffset.top+"px",zIndex:++i.zIndex}),this.helper.appendTo("body").disableSelection()):this.helper=this.element},_change:{e:function(e,t){return{width:this.originalSize.width+t}},w:function(e,t){var i=this.originalSize,s=this.originalPosition;return{left:s.left+t,width:i.width-t}},n:function(e,t,i){var s=this.originalSize,n=this.originalPosition;return{top:n.top+i,height:s.height-i}},s:function(e,t,i){return{height:this.originalSize.height+i}},se:function(t,i,s){return e.extend(this._change.s.apply(this,arguments),this._change.e.apply(this,[t,i,s]))},sw:function(t,i,s){return e.extend(this._change.s.apply(this,arguments),this._change.w.apply(this,[t,i,s]))},ne:function(t,i,s){return e.extend(this._change.n.apply(this,arguments),this._change.e.apply(this,[t,i,s]))},nw:function(t,i,s){return e.extend(this._change.n.apply(this,arguments),this._change.w.apply(this,[t,i,s]))}},_propagate:function(t,i){e.ui.plugin.call(this,t,[i,this.ui()]),"resize"!==t&&this._trigger(t,i,this.ui())},plugins:{},ui:function(){return{originalElement:this.originalElement,element:this.element,helper:this.helper,position:this.position,size:this.size,originalSize:this.originalSize,originalPosition:this.originalPosition,prevSize:this.prevSize,prevPosition:this.prevPosition}}}),e.ui.plugin.add("resizable","animate",{stop:function(t){var i=e(this).resizable("instance"),s=i.options,n=i._proportionallyResizeElements,a=n.length&&/textarea/i.test(n[0].nodeName),o=a&&i._hasScroll(n[0],"left")?0:i.sizeDiff.height,r=a?0:i.sizeDiff.width,h={width:i.size.width-r,height:i.size.height-o},l=parseInt(i.element.css("left"),10)+(i.position.left-i.originalPosition.left)||null,u=parseInt(i.element.css("top"),10)+(i.position.top-i.originalPosition.top)||null;i.element.animate(e.extend(h,u&&l?{top:u,left:l}:{}),{duration:s.animateDuration,easing:s.animateEasing,step:function(){var s={width:parseInt(i.element.css("width"),10),height:parseInt(i.element.css("height"),10),top:parseInt(i.element.css("top"),10),left:parseInt(i.element.css("left"),10)};n&&n.length&&e(n[0]).css({width:s.width,height:s.height}),i._updateCache(s),i._propagate("resize",t)}})}}),e.ui.plugin.add("resizable","containment",{start:function(){var t,i,s,n,a,o,r,h=e(this).resizable("instance"),l=h.options,u=h.element,d=l.containment,c=d instanceof e?d.get(0):/parent/.test(d)?u.parent().get(0):d;c&&(h.containerElement=e(c),/document/.test(d)||d===document?(h.containerOffset={left:0,top:0},h.containerPosition={left:0,top:0},h.parentData={element:e(document),left:0,top:0,width:e(document).width(),height:e(document).height()||document.body.parentNode.scrollHeight}):(t=e(c),i=[],e(["Top","Right","Left","Bottom"]).each(function(e,s){i[e]=h._num(t.css("padding"+s))}),h.containerOffset=t.offset(),h.containerPosition=t.position(),h.containerSize={height:t.innerHeight()-i[3],width:t.innerWidth()-i[1]},s=h.containerOffset,n=h.containerSize.height,a=h.containerSize.width,o=h._hasScroll(c,"left")?c.scrollWidth:a,r=h._hasScroll(c)?c.scrollHeight:n,h.parentData={element:c,left:s.left,top:s.top,width:o,height:r}))},resize:function(t,i){var s,n,a,o,r=e(this).resizable("instance"),h=r.options,l=r.containerOffset,u=r.position,d=r._aspectRatio||t.shiftKey,c={top:0,left:0},p=r.containerElement,f=!0;p[0]!==document&&/static/.test(p.css("position"))&&(c=l),u.left<(r._helper?l.left:0)&&(r.size.width=r.size.width+(r._helper?r.position.left-l.left:r.position.left-c.left),d&&(r.size.height=r.size.width/r.aspectRatio,f=!1),r.position.left=h.helper?l.left:0),u.top<(r._helper?l.top:0)&&(r.size.height=r.size.height+(r._helper?r.position.top-l.top:r.position.top),d&&(r.size.width=r.size.height*r.aspectRatio,f=!1),r.position.top=r._helper?l.top:0),r.offset.left=r.parentData.left+r.position.left,r.offset.top=r.parentData.top+r.position.top,s=Math.abs((r._helper?r.offset.left-c.left:r.offset.left-l.left)+r.sizeDiff.width),n=Math.abs((r._helper?r.offset.top-c.top:r.offset.top-l.top)+r.sizeDiff.height),a=r.containerElement.get(0)===r.element.parent().get(0),o=/relative|absolute/.test(r.containerElement.css("position")),a&&o&&(s-=Math.abs(r.parentData.left)),s+r.size.width>=r.parentData.width&&(r.size.width=r.parentData.width-s,d&&(r.size.height=r.size.width/r.aspectRatio,f=!1)),n+r.size.height>=r.parentData.height&&(r.size.height=r.parentData.height-n,d&&(r.size.width=r.size.height*r.aspectRatio,f=!1)),f||(r.position.left=i.prevPosition.left,r.position.top=i.prevPosition.top,r.size.width=i.prevSize.width,r.size.height=i.prevSize.height)},stop:function(){var t=e(this).resizable("instance"),i=t.options,s=t.containerOffset,n=t.containerPosition,a=t.containerElement,o=e(t.helper),r=o.offset(),h=o.outerWidth()-t.sizeDiff.width,l=o.outerHeight()-t.sizeDiff.height;t._helper&&!i.animate&&/relative/.test(a.css("position"))&&e(this).css({left:r.left-n.left-s.left,width:h,height:l}),t._helper&&!i.animate&&/static/.test(a.css("position"))&&e(this).css({left:r.left-n.left-s.left,width:h,height:l})}}),e.ui.plugin.add("resizable","alsoResize",{start:function(){var t=e(this).resizable("instance"),i=t.options,s=function(t){e(t).each(function(){var t=e(this);t.data("ui-resizable-alsoresize",{width:parseInt(t.width(),10),height:parseInt(t.height(),10),left:parseInt(t.css("left"),10),top:parseInt(t.css("top"),10)})})};"object"!=typeof i.alsoResize||i.alsoResize.parentNode?s(i.alsoResize):i.alsoResize.length?(i.alsoResize=i.alsoResize[0],s(i.alsoResize)):e.each(i.alsoResize,function(e){s(e)})},resize:function(t,i){var s=e(this).resizable("instance"),n=s.options,a=s.originalSize,o=s.originalPosition,r={height:s.size.height-a.height||0,width:s.size.width-a.width||0,top:s.position.top-o.top||0,left:s.position.left-o.left||0},h=function(t,s){e(t).each(function(){var t=e(this),n=e(this).data("ui-resizable-alsoresize"),a={},o=s&&s.length?s:t.parents(i.originalElement[0]).length?["width","height"]:["width","height","top","left"];e.each(o,function(e,t){var i=(n[t]||0)+(r[t]||0);i&&i>=0&&(a[t]=i||null)}),t.css(a)})};"object"!=typeof n.alsoResize||n.alsoResize.nodeType?h(n.alsoResize):e.each(n.alsoResize,function(e,t){h(e,t)})},stop:function(){e(this).removeData("resizable-alsoresize")}}),e.ui.plugin.add("resizable","ghost",{start:function(){var t=e(this).resizable("instance"),i=t.options,s=t.size;t.ghost=t.originalElement.clone(),t.ghost.css({opacity:.25,display:"block",position:"relative",height:s.height,width:s.width,margin:0,left:0,top:0}).addClass("ui-resizable-ghost").addClass("string"==typeof i.ghost?i.ghost:""),t.ghost.appendTo(t.helper)},resize:function(){var t=e(this).resizable("instance");t.ghost&&t.ghost.css({position:"relative",height:t.size.height,width:t.size.width})},stop:function(){var t=e(this).resizable("instance");t.ghost&&t.helper&&t.helper.get(0).removeChild(t.ghost.get(0))}}),e.ui.plugin.add("resizable","grid",{resize:function(){var t=e(this).resizable("instance"),i=t.options,s=t.size,n=t.originalSize,a=t.originalPosition,o=t.axis,r="number"==typeof i.grid?[i.grid,i.grid]:i.grid,h=r[0]||1,l=r[1]||1,u=Math.round((s.width-n.width)/h)*h,d=Math.round((s.height-n.height)/l)*l,c=n.width+u,p=n.height+d,f=i.maxWidth&&c>i.maxWidth,m=i.maxHeight&&p>i.maxHeight,g=i.minWidth&&i.minWidth>c,v=i.minHeight&&i.minHeight>p;i.grid=r,g&&(c+=h),v&&(p+=l),f&&(c-=h),m&&(p-=l),/^(se|s|e)$/.test(o)?(t.size.width=c,t.size.height=p):/^(ne)$/.test(o)?(t.size.width=c,t.size.height=p,t.position.top=a.top-d):/^(sw)$/.test(o)?(t.size.width=c,t.size.height=p,t.position.left=a.left-u):(p-l>0?(t.size.height=p,t.position.top=a.top-d):(t.size.height=l,t.position.top=a.top+n.height-l),c-h>0?(t.size.width=c,t.position.left=a.left-u):(t.size.width=h,t.position.left=a.left+n.width-h))}}),e.ui.resizable,e.widget("ui.dialog",{version:"1.11.0",options:{appendTo:"body",autoOpen:!0,buttons:[],closeOnEscape:!0,closeText:"Close",dialogClass:"",draggable:!0,hide:null,height:"auto",maxHeight:null,maxWidth:null,minHeight:150,minWidth:150,modal:!1,position:{my:"center",at:"center",of:window,collision:"fit",using:function(t){var i=e(this).css(t).offset().top;0>i&&e(this).css("top",t.top-i)}},resizable:!0,show:null,title:null,width:300,beforeClose:null,close:null,drag:null,dragStart:null,dragStop:null,focus:null,open:null,resize:null,resizeStart:null,resizeStop:null},sizeRelatedOptions:{buttons:!0,height:!0,maxHeight:!0,maxWidth:!0,minHeight:!0,minWidth:!0,width:!0},resizableRelatedOptions:{maxHeight:!0,maxWidth:!0,minHeight:!0,minWidth:!0},_create:function(){this.originalCss={display:this.element[0].style.display,width:this.element[0].style.width,minHeight:this.element[0].style.minHeight,maxHeight:this.element[0].style.maxHeight,height:this.element[0].style.height},this.originalPosition={parent:this.element.parent(),index:this.element.parent().children().index(this.element)},this.originalTitle=this.element.attr("title"),this.options.title=this.options.title||this.originalTitle,this._createWrapper(),this.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(this.uiDialog),this._createTitlebar(),this._createButtonPane(),this.options.draggable&&e.fn.draggable&&this._makeDraggable(),this.options.resizable&&e.fn.resizable&&this._makeResizable(),this._isOpen=!1,this._trackFocus()},_init:function(){this.options.autoOpen&&this.open()},_appendTo:function(){var t=this.options.appendTo;return t&&(t.jquery||t.nodeType)?e(t):this.document.find(t||"body").eq(0)},_destroy:function(){var e,t=this.originalPosition;this._destroyOverlay(),this.element.removeUniqueId().removeClass("ui-dialog-content ui-widget-content").css(this.originalCss).detach(),this.uiDialog.stop(!0,!0).remove(),this.originalTitle&&this.element.attr("title",this.originalTitle),e=t.parent.children().eq(t.index),e.length&&e[0]!==this.element[0]?e.before(this.element):t.parent.append(this.element)},widget:function(){return this.uiDialog},disable:e.noop,enable:e.noop,close:function(t){var i,s=this;if(this._isOpen&&this._trigger("beforeClose",t)!==!1){if(this._isOpen=!1,this._focusedElement=null,this._destroyOverlay(),this._untrackInstance(),!this.opener.filter(":focusable").focus().length)try{i=this.document[0].activeElement,i&&"body"!==i.nodeName.toLowerCase()&&e(i).blur()}catch(n){}this._hide(this.uiDialog,this.options.hide,function(){s._trigger("close",t)})}},isOpen:function(){return this._isOpen},moveToTop:function(){this._moveToTop()},_moveToTop:function(t,i){var s=!1,n=this.uiDialog.siblings(".ui-front:visible").map(function(){return+e(this).css("z-index")}).get(),a=Math.max.apply(null,n);return a>=+this.uiDialog.css("z-index")&&(this.uiDialog.css("z-index",a+1),s=!0),s&&!i&&this._trigger("focus",t),s},open:function(){var t=this;return this._isOpen?(this._moveToTop()&&this._focusTabbable(),void 0):(this._isOpen=!0,this.opener=e(this.document[0].activeElement),this._size(),this._position(),this._createOverlay(),this._moveToTop(null,!0),this._show(this.uiDialog,this.options.show,function(){t._focusTabbable(),t._trigger("focus")}),this._trigger("open"),void 0)},_focusTabbable:function(){var e=this._focusedElement;e||(e=this.element.find("[autofocus]")),e.length||(e=this.element.find(":tabbable")),e.length||(e=this.uiDialogButtonPane.find(":tabbable")),e.length||(e=this.uiDialogTitlebarClose.filter(":tabbable")),e.length||(e=this.uiDialog),e.eq(0).focus()},_keepFocus:function(t){function i(){var t=this.document[0].activeElement,i=this.uiDialog[0]===t||e.contains(this.uiDialog[0],t);i||this._focusTabbable()}t.preventDefault(),i.call(this),this._delay(i)},_createWrapper:function(){this.uiDialog=e("<div>").addClass("ui-dialog ui-widget ui-widget-content ui-corner-all ui-front "+this.options.dialogClass).hide().attr({tabIndex:-1,role:"dialog"}).appendTo(this._appendTo()),this._on(this.uiDialog,{keydown:function(t){if(this.options.closeOnEscape&&!t.isDefaultPrevented()&&t.keyCode&&t.keyCode===e.ui.keyCode.ESCAPE)return t.preventDefault(),this.close(t),void 0;if(t.keyCode===e.ui.keyCode.TAB&&!t.isDefaultPrevented()){var i=this.uiDialog.find(":tabbable"),s=i.filter(":first"),n=i.filter(":last");t.target!==n[0]&&t.target!==this.uiDialog[0]||t.shiftKey?t.target!==s[0]&&t.target!==this.uiDialog[0]||!t.shiftKey||(this._delay(function(){n.focus()}),t.preventDefault()):(this._delay(function(){s.focus()}),t.preventDefault())}},mousedown:function(e){this._moveToTop(e)&&this._focusTabbable()
}}),this.element.find("[aria-describedby]").length||this.uiDialog.attr({"aria-describedby":this.element.uniqueId().attr("id")})},_createTitlebar:function(){var t;this.uiDialogTitlebar=e("<div>").addClass("ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix").prependTo(this.uiDialog),this._on(this.uiDialogTitlebar,{mousedown:function(t){e(t.target).closest(".ui-dialog-titlebar-close")||this.uiDialog.focus()}}),this.uiDialogTitlebarClose=e("<button type='button'></button>").button({label:this.options.closeText,icons:{primary:"ui-icon-closethick"},text:!1}).addClass("ui-dialog-titlebar-close").appendTo(this.uiDialogTitlebar),this._on(this.uiDialogTitlebarClose,{click:function(e){e.preventDefault(),this.close(e)}}),t=e("<span>").uniqueId().addClass("ui-dialog-title").prependTo(this.uiDialogTitlebar),this._title(t),this.uiDialog.attr({"aria-labelledby":t.attr("id")})},_title:function(e){this.options.title||e.html("&#160;"),e.text(this.options.title)},_createButtonPane:function(){this.uiDialogButtonPane=e("<div>").addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"),this.uiButtonSet=e("<div>").addClass("ui-dialog-buttonset").appendTo(this.uiDialogButtonPane),this._createButtons()},_createButtons:function(){var t=this,i=this.options.buttons;return this.uiDialogButtonPane.remove(),this.uiButtonSet.empty(),e.isEmptyObject(i)||e.isArray(i)&&!i.length?(this.uiDialog.removeClass("ui-dialog-buttons"),void 0):(e.each(i,function(i,s){var n,a;s=e.isFunction(s)?{click:s,text:i}:s,s=e.extend({type:"button"},s),n=s.click,s.click=function(){n.apply(t.element[0],arguments)},a={icons:s.icons,text:s.showText},delete s.icons,delete s.showText,e("<button></button>",s).button(a).appendTo(t.uiButtonSet)}),this.uiDialog.addClass("ui-dialog-buttons"),this.uiDialogButtonPane.appendTo(this.uiDialog),void 0)},_makeDraggable:function(){function t(e){return{position:e.position,offset:e.offset}}var i=this,s=this.options;this.uiDialog.draggable({cancel:".ui-dialog-content, .ui-dialog-titlebar-close",handle:".ui-dialog-titlebar",containment:"document",start:function(s,n){e(this).addClass("ui-dialog-dragging"),i._blockFrames(),i._trigger("dragStart",s,t(n))},drag:function(e,s){i._trigger("drag",e,t(s))},stop:function(n,a){var o=a.offset.left-i.document.scrollLeft(),r=a.offset.top-i.document.scrollTop();s.position={my:"left top",at:"left"+(o>=0?"+":"")+o+" "+"top"+(r>=0?"+":"")+r,of:i.window},e(this).removeClass("ui-dialog-dragging"),i._unblockFrames(),i._trigger("dragStop",n,t(a))}})},_makeResizable:function(){function t(e){return{originalPosition:e.originalPosition,originalSize:e.originalSize,position:e.position,size:e.size}}var i=this,s=this.options,n=s.resizable,a=this.uiDialog.css("position"),o="string"==typeof n?n:"n,e,s,w,se,sw,ne,nw";this.uiDialog.resizable({cancel:".ui-dialog-content",containment:"document",alsoResize:this.element,maxWidth:s.maxWidth,maxHeight:s.maxHeight,minWidth:s.minWidth,minHeight:this._minHeight(),handles:o,start:function(s,n){e(this).addClass("ui-dialog-resizing"),i._blockFrames(),i._trigger("resizeStart",s,t(n))},resize:function(e,s){i._trigger("resize",e,t(s))},stop:function(n,a){var o=i.uiDialog.offset(),r=o.left-i.document.scrollLeft(),h=o.top-i.document.scrollTop();s.height=i.uiDialog.height(),s.width=i.uiDialog.width(),s.position={my:"left top",at:"left"+(r>=0?"+":"")+r+" "+"top"+(h>=0?"+":"")+h,of:i.window},e(this).removeClass("ui-dialog-resizing"),i._unblockFrames(),i._trigger("resizeStop",n,t(a))}}).css("position",a)},_trackFocus:function(){this._on(this.widget(),{focusin:function(t){this._untrackInstance(),this._trackingInstances().unshift(this),this._focusedElement=e(t.target)}})},_untrackInstance:function(){var t=this._trackingInstances(),i=e.inArray(this,t);-1!==i&&t.splice(i,1)},_trackingInstances:function(){var e=this.document.data("ui-dialog-instances");return e||(e=[],this.document.data("ui-dialog-instances",e)),e},_minHeight:function(){var e=this.options;return"auto"===e.height?e.minHeight:Math.min(e.minHeight,e.height)},_position:function(){var e=this.uiDialog.is(":visible");e||this.uiDialog.show(),this.uiDialog.position(this.options.position),e||this.uiDialog.hide()},_setOptions:function(t){var i=this,s=!1,n={};e.each(t,function(e,t){i._setOption(e,t),e in i.sizeRelatedOptions&&(s=!0),e in i.resizableRelatedOptions&&(n[e]=t)}),s&&(this._size(),this._position()),this.uiDialog.is(":data(ui-resizable)")&&this.uiDialog.resizable("option",n)},_setOption:function(e,t){var i,s,n=this.uiDialog;"dialogClass"===e&&n.removeClass(this.options.dialogClass).addClass(t),"disabled"!==e&&(this._super(e,t),"appendTo"===e&&this.uiDialog.appendTo(this._appendTo()),"buttons"===e&&this._createButtons(),"closeText"===e&&this.uiDialogTitlebarClose.button({label:""+t}),"draggable"===e&&(i=n.is(":data(ui-draggable)"),i&&!t&&n.draggable("destroy"),!i&&t&&this._makeDraggable()),"position"===e&&this._position(),"resizable"===e&&(s=n.is(":data(ui-resizable)"),s&&!t&&n.resizable("destroy"),s&&"string"==typeof t&&n.resizable("option","handles",t),s||t===!1||this._makeResizable()),"title"===e&&this._title(this.uiDialogTitlebar.find(".ui-dialog-title")))},_size:function(){var e,t,i,s=this.options;this.element.show().css({width:"auto",minHeight:0,maxHeight:"none",height:0}),s.minWidth>s.width&&(s.width=s.minWidth),e=this.uiDialog.css({height:"auto",width:s.width}).outerHeight(),t=Math.max(0,s.minHeight-e),i="number"==typeof s.maxHeight?Math.max(0,s.maxHeight-e):"none","auto"===s.height?this.element.css({minHeight:t,maxHeight:i,height:"auto"}):this.element.height(Math.max(0,s.height-e)),this.uiDialog.is(":data(ui-resizable)")&&this.uiDialog.resizable("option","minHeight",this._minHeight())},_blockFrames:function(){this.iframeBlocks=this.document.find("iframe").map(function(){var t=e(this);return e("<div>").css({position:"absolute",width:t.outerWidth(),height:t.outerHeight()}).appendTo(t.parent()).offset(t.offset())[0]})},_unblockFrames:function(){this.iframeBlocks&&(this.iframeBlocks.remove(),delete this.iframeBlocks)},_allowInteraction:function(t){return e(t.target).closest(".ui-dialog").length?!0:!!e(t.target).closest(".ui-datepicker").length},_createOverlay:function(){if(this.options.modal){var t=!0;this._delay(function(){t=!1}),this.document.data("ui-dialog-overlays")||this._on(this.document,{focusin:function(e){t||this._allowInteraction(e)||(e.preventDefault(),this._trackingInstances()[0]._focusTabbable())}}),this.overlay=e("<div>").addClass("ui-widget-overlay ui-front").appendTo(this._appendTo()),this._on(this.overlay,{mousedown:"_keepFocus"}),this.document.data("ui-dialog-overlays",(this.document.data("ui-dialog-overlays")||0)+1)}},_destroyOverlay:function(){if(this.options.modal&&this.overlay){var e=this.document.data("ui-dialog-overlays")-1;e?this.document.data("ui-dialog-overlays",e):this.document.unbind("focusin").removeData("ui-dialog-overlays"),this.overlay.remove(),this.overlay=null}}}),e.widget("ui.droppable",{version:"1.11.0",widgetEventPrefix:"drop",options:{accept:"*",activeClass:!1,addClasses:!0,greedy:!1,hoverClass:!1,scope:"default",tolerance:"intersect",activate:null,deactivate:null,drop:null,out:null,over:null},_create:function(){var t,i=this.options,s=i.accept;this.isover=!1,this.isout=!0,this.accept=e.isFunction(s)?s:function(e){return e.is(s)},this.proportions=function(){return arguments.length?(t=arguments[0],void 0):t?t:t={width:this.element[0].offsetWidth,height:this.element[0].offsetHeight}},this._addToManager(i.scope),i.addClasses&&this.element.addClass("ui-droppable")},_addToManager:function(t){e.ui.ddmanager.droppables[t]=e.ui.ddmanager.droppables[t]||[],e.ui.ddmanager.droppables[t].push(this)},_splice:function(e){for(var t=0;e.length>t;t++)e[t]===this&&e.splice(t,1)},_destroy:function(){var t=e.ui.ddmanager.droppables[this.options.scope];this._splice(t),this.element.removeClass("ui-droppable ui-droppable-disabled")},_setOption:function(t,i){if("accept"===t)this.accept=e.isFunction(i)?i:function(e){return e.is(i)};else if("scope"===t){var s=e.ui.ddmanager.droppables[this.options.scope];this._splice(s),this._addToManager(i)}this._super(t,i)},_activate:function(t){var i=e.ui.ddmanager.current;this.options.activeClass&&this.element.addClass(this.options.activeClass),i&&this._trigger("activate",t,this.ui(i))},_deactivate:function(t){var i=e.ui.ddmanager.current;this.options.activeClass&&this.element.removeClass(this.options.activeClass),i&&this._trigger("deactivate",t,this.ui(i))},_over:function(t){var i=e.ui.ddmanager.current;i&&(i.currentItem||i.element)[0]!==this.element[0]&&this.accept.call(this.element[0],i.currentItem||i.element)&&(this.options.hoverClass&&this.element.addClass(this.options.hoverClass),this._trigger("over",t,this.ui(i)))},_out:function(t){var i=e.ui.ddmanager.current;i&&(i.currentItem||i.element)[0]!==this.element[0]&&this.accept.call(this.element[0],i.currentItem||i.element)&&(this.options.hoverClass&&this.element.removeClass(this.options.hoverClass),this._trigger("out",t,this.ui(i)))},_drop:function(t,i){var s=i||e.ui.ddmanager.current,n=!1;return s&&(s.currentItem||s.element)[0]!==this.element[0]?(this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function(){var t=e(this).droppable("instance");return t.options.greedy&&!t.options.disabled&&t.options.scope===s.options.scope&&t.accept.call(t.element[0],s.currentItem||s.element)&&e.ui.intersect(s,e.extend(t,{offset:t.element.offset()}),t.options.tolerance)?(n=!0,!1):void 0}),n?!1:this.accept.call(this.element[0],s.currentItem||s.element)?(this.options.activeClass&&this.element.removeClass(this.options.activeClass),this.options.hoverClass&&this.element.removeClass(this.options.hoverClass),this._trigger("drop",t,this.ui(s)),this.element):!1):!1},ui:function(e){return{draggable:e.currentItem||e.element,helper:e.helper,position:e.position,offset:e.positionAbs}}}),e.ui.intersect=function(){function e(e,t,i){return e>=t&&t+i>e}return function(t,i,s){if(!i.offset)return!1;var n,a,o=(t.positionAbs||t.position.absolute).left,r=(t.positionAbs||t.position.absolute).top,h=o+t.helperProportions.width,l=r+t.helperProportions.height,u=i.offset.left,d=i.offset.top,c=u+i.proportions().width,p=d+i.proportions().height;switch(s){case"fit":return o>=u&&c>=h&&r>=d&&p>=l;case"intersect":return o+t.helperProportions.width/2>u&&c>h-t.helperProportions.width/2&&r+t.helperProportions.height/2>d&&p>l-t.helperProportions.height/2;case"pointer":return n=(t.positionAbs||t.position.absolute).left+(t.clickOffset||t.offset.click).left,a=(t.positionAbs||t.position.absolute).top+(t.clickOffset||t.offset.click).top,e(a,d,i.proportions().height)&&e(n,u,i.proportions().width);case"touch":return(r>=d&&p>=r||l>=d&&p>=l||d>r&&l>p)&&(o>=u&&c>=o||h>=u&&c>=h||u>o&&h>c);default:return!1}}}(),e.ui.ddmanager={current:null,droppables:{"default":[]},prepareOffsets:function(t,i){var s,n,a=e.ui.ddmanager.droppables[t.options.scope]||[],o=i?i.type:null,r=(t.currentItem||t.element).find(":data(ui-droppable)").addBack();e:for(s=0;a.length>s;s++)if(!(a[s].options.disabled||t&&!a[s].accept.call(a[s].element[0],t.currentItem||t.element))){for(n=0;r.length>n;n++)if(r[n]===a[s].element[0]){a[s].proportions().height=0;continue e}a[s].visible="none"!==a[s].element.css("display"),a[s].visible&&("mousedown"===o&&a[s]._activate.call(a[s],i),a[s].offset=a[s].element.offset(),a[s].proportions({width:a[s].element[0].offsetWidth,height:a[s].element[0].offsetHeight}))}},drop:function(t,i){var s=!1;return e.each((e.ui.ddmanager.droppables[t.options.scope]||[]).slice(),function(){this.options&&(!this.options.disabled&&this.visible&&e.ui.intersect(t,this,this.options.tolerance)&&(s=this._drop.call(this,i)||s),!this.options.disabled&&this.visible&&this.accept.call(this.element[0],t.currentItem||t.element)&&(this.isout=!0,this.isover=!1,this._deactivate.call(this,i)))}),s},dragStart:function(t,i){t.element.parentsUntil("body").bind("scroll.droppable",function(){t.options.refreshPositions||e.ui.ddmanager.prepareOffsets(t,i)})},drag:function(t,i){t.options.refreshPositions&&e.ui.ddmanager.prepareOffsets(t,i),e.each(e.ui.ddmanager.droppables[t.options.scope]||[],function(){if(!this.options.disabled&&!this.greedyChild&&this.visible){var s,n,a,o=e.ui.intersect(t,this,this.options.tolerance),r=!o&&this.isover?"isout":o&&!this.isover?"isover":null;r&&(this.options.greedy&&(n=this.options.scope,a=this.element.parents(":data(ui-droppable)").filter(function(){return e(this).droppable("instance").options.scope===n}),a.length&&(s=e(a[0]).droppable("instance"),s.greedyChild="isover"===r)),s&&"isover"===r&&(s.isover=!1,s.isout=!0,s._out.call(s,i)),this[r]=!0,this["isout"===r?"isover":"isout"]=!1,this["isover"===r?"_over":"_out"].call(this,i),s&&"isout"===r&&(s.isout=!1,s.isover=!0,s._over.call(s,i)))}})},dragStop:function(t,i){t.element.parentsUntil("body").unbind("scroll.droppable"),t.options.refreshPositions||e.ui.ddmanager.prepareOffsets(t,i)}},e.ui.droppable;var v="ui-effects-";e.effects={effect:{}},function(e,t){function i(e,t,i){var s=d[t.type]||{};return null==e?i||!t.def?null:t.def:(e=s.floor?~~e:parseFloat(e),isNaN(e)?t.def:s.mod?(e+s.mod)%s.mod:0>e?0:e>s.max?s.max:e)}function s(i){var s=l(),n=s._rgba=[];return i=i.toLowerCase(),f(h,function(e,a){var o,r=a.re.exec(i),h=r&&a.parse(r),l=a.space||"rgba";return h?(o=s[l](h),s[u[l].cache]=o[u[l].cache],n=s._rgba=o._rgba,!1):t}),n.length?("0,0,0,0"===n.join()&&e.extend(n,a.transparent),s):a[i]}function n(e,t,i){return i=(i+1)%1,1>6*i?e+6*(t-e)*i:1>2*i?t:2>3*i?e+6*(t-e)*(2/3-i):e}var a,o="backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor",r=/^([\-+])=\s*(\d+\.?\d*)/,h=[{re:/rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,parse:function(e){return[e[1],e[2],e[3],e[4]]}},{re:/rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,parse:function(e){return[2.55*e[1],2.55*e[2],2.55*e[3],e[4]]}},{re:/#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,parse:function(e){return[parseInt(e[1],16),parseInt(e[2],16),parseInt(e[3],16)]}},{re:/#([a-f0-9])([a-f0-9])([a-f0-9])/,parse:function(e){return[parseInt(e[1]+e[1],16),parseInt(e[2]+e[2],16),parseInt(e[3]+e[3],16)]}},{re:/hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,space:"hsla",parse:function(e){return[e[1],e[2]/100,e[3]/100,e[4]]}}],l=e.Color=function(t,i,s,n){return new e.Color.fn.parse(t,i,s,n)},u={rgba:{props:{red:{idx:0,type:"byte"},green:{idx:1,type:"byte"},blue:{idx:2,type:"byte"}}},hsla:{props:{hue:{idx:0,type:"degrees"},saturation:{idx:1,type:"percent"},lightness:{idx:2,type:"percent"}}}},d={"byte":{floor:!0,max:255},percent:{max:1},degrees:{mod:360,floor:!0}},c=l.support={},p=e("<p>")[0],f=e.each;p.style.cssText="background-color:rgba(1,1,1,.5)",c.rgba=p.style.backgroundColor.indexOf("rgba")>-1,f(u,function(e,t){t.cache="_"+e,t.props.alpha={idx:3,type:"percent",def:1}}),l.fn=e.extend(l.prototype,{parse:function(n,o,r,h){if(n===t)return this._rgba=[null,null,null,null],this;(n.jquery||n.nodeType)&&(n=e(n).css(o),o=t);var d=this,c=e.type(n),p=this._rgba=[];return o!==t&&(n=[n,o,r,h],c="array"),"string"===c?this.parse(s(n)||a._default):"array"===c?(f(u.rgba.props,function(e,t){p[t.idx]=i(n[t.idx],t)}),this):"object"===c?(n instanceof l?f(u,function(e,t){n[t.cache]&&(d[t.cache]=n[t.cache].slice())}):f(u,function(t,s){var a=s.cache;f(s.props,function(e,t){if(!d[a]&&s.to){if("alpha"===e||null==n[e])return;d[a]=s.to(d._rgba)}d[a][t.idx]=i(n[e],t,!0)}),d[a]&&0>e.inArray(null,d[a].slice(0,3))&&(d[a][3]=1,s.from&&(d._rgba=s.from(d[a])))}),this):t},is:function(e){var i=l(e),s=!0,n=this;return f(u,function(e,a){var o,r=i[a.cache];return r&&(o=n[a.cache]||a.to&&a.to(n._rgba)||[],f(a.props,function(e,i){return null!=r[i.idx]?s=r[i.idx]===o[i.idx]:t})),s}),s},_space:function(){var e=[],t=this;return f(u,function(i,s){t[s.cache]&&e.push(i)}),e.pop()},transition:function(e,t){var s=l(e),n=s._space(),a=u[n],o=0===this.alpha()?l("transparent"):this,r=o[a.cache]||a.to(o._rgba),h=r.slice();return s=s[a.cache],f(a.props,function(e,n){var a=n.idx,o=r[a],l=s[a],u=d[n.type]||{};null!==l&&(null===o?h[a]=l:(u.mod&&(l-o>u.mod/2?o+=u.mod:o-l>u.mod/2&&(o-=u.mod)),h[a]=i((l-o)*t+o,n)))}),this[n](h)},blend:function(t){if(1===this._rgba[3])return this;var i=this._rgba.slice(),s=i.pop(),n=l(t)._rgba;return l(e.map(i,function(e,t){return(1-s)*n[t]+s*e}))},toRgbaString:function(){var t="rgba(",i=e.map(this._rgba,function(e,t){return null==e?t>2?1:0:e});return 1===i[3]&&(i.pop(),t="rgb("),t+i.join()+")"},toHslaString:function(){var t="hsla(",i=e.map(this.hsla(),function(e,t){return null==e&&(e=t>2?1:0),t&&3>t&&(e=Math.round(100*e)+"%"),e});return 1===i[3]&&(i.pop(),t="hsl("),t+i.join()+")"},toHexString:function(t){var i=this._rgba.slice(),s=i.pop();return t&&i.push(~~(255*s)),"#"+e.map(i,function(e){return e=(e||0).toString(16),1===e.length?"0"+e:e}).join("")},toString:function(){return 0===this._rgba[3]?"transparent":this.toRgbaString()}}),l.fn.parse.prototype=l.fn,u.hsla.to=function(e){if(null==e[0]||null==e[1]||null==e[2])return[null,null,null,e[3]];var t,i,s=e[0]/255,n=e[1]/255,a=e[2]/255,o=e[3],r=Math.max(s,n,a),h=Math.min(s,n,a),l=r-h,u=r+h,d=.5*u;return t=h===r?0:s===r?60*(n-a)/l+360:n===r?60*(a-s)/l+120:60*(s-n)/l+240,i=0===l?0:.5>=d?l/u:l/(2-u),[Math.round(t)%360,i,d,null==o?1:o]},u.hsla.from=function(e){if(null==e[0]||null==e[1]||null==e[2])return[null,null,null,e[3]];var t=e[0]/360,i=e[1],s=e[2],a=e[3],o=.5>=s?s*(1+i):s+i-s*i,r=2*s-o;return[Math.round(255*n(r,o,t+1/3)),Math.round(255*n(r,o,t)),Math.round(255*n(r,o,t-1/3)),a]},f(u,function(s,n){var a=n.props,o=n.cache,h=n.to,u=n.from;l.fn[s]=function(s){if(h&&!this[o]&&(this[o]=h(this._rgba)),s===t)return this[o].slice();var n,r=e.type(s),d="array"===r||"object"===r?s:arguments,c=this[o].slice();return f(a,function(e,t){var s=d["object"===r?e:t.idx];null==s&&(s=c[t.idx]),c[t.idx]=i(s,t)}),u?(n=l(u(c)),n[o]=c,n):l(c)},f(a,function(t,i){l.fn[t]||(l.fn[t]=function(n){var a,o=e.type(n),h="alpha"===t?this._hsla?"hsla":"rgba":s,l=this[h](),u=l[i.idx];return"undefined"===o?u:("function"===o&&(n=n.call(this,u),o=e.type(n)),null==n&&i.empty?this:("string"===o&&(a=r.exec(n),a&&(n=u+parseFloat(a[2])*("+"===a[1]?1:-1))),l[i.idx]=n,this[h](l)))})})}),l.hook=function(t){var i=t.split(" ");f(i,function(t,i){e.cssHooks[i]={set:function(t,n){var a,o,r="";if("transparent"!==n&&("string"!==e.type(n)||(a=s(n)))){if(n=l(a||n),!c.rgba&&1!==n._rgba[3]){for(o="backgroundColor"===i?t.parentNode:t;(""===r||"transparent"===r)&&o&&o.style;)try{r=e.css(o,"backgroundColor"),o=o.parentNode}catch(h){}n=n.blend(r&&"transparent"!==r?r:"_default")}n=n.toRgbaString()}try{t.style[i]=n}catch(h){}}},e.fx.step[i]=function(t){t.colorInit||(t.start=l(t.elem,i),t.end=l(t.end),t.colorInit=!0),e.cssHooks[i].set(t.elem,t.start.transition(t.end,t.pos))}})},l.hook(o),e.cssHooks.borderColor={expand:function(e){var t={};return f(["Top","Right","Bottom","Left"],function(i,s){t["border"+s+"Color"]=e}),t}},a=e.Color.names={aqua:"#00ffff",black:"#000000",blue:"#0000ff",fuchsia:"#ff00ff",gray:"#808080",green:"#008000",lime:"#00ff00",maroon:"#800000",navy:"#000080",olive:"#808000",purple:"#800080",red:"#ff0000",silver:"#c0c0c0",teal:"#008080",white:"#ffffff",yellow:"#ffff00",transparent:[null,null,null,0],_default:"#ffffff"}}(jQuery),function(){function t(t){var i,s,n=t.ownerDocument.defaultView?t.ownerDocument.defaultView.getComputedStyle(t,null):t.currentStyle,a={};if(n&&n.length&&n[0]&&n[n[0]])for(s=n.length;s--;)i=n[s],"string"==typeof n[i]&&(a[e.camelCase(i)]=n[i]);else for(i in n)"string"==typeof n[i]&&(a[i]=n[i]);return a}function i(t,i){var s,a,o={};for(s in i)a=i[s],t[s]!==a&&(n[s]||(e.fx.step[s]||!isNaN(parseFloat(a)))&&(o[s]=a));return o}var s=["add","remove","toggle"],n={border:1,borderBottom:1,borderColor:1,borderLeft:1,borderRight:1,borderTop:1,borderWidth:1,margin:1,padding:1};e.each(["borderLeftStyle","borderRightStyle","borderBottomStyle","borderTopStyle"],function(t,i){e.fx.step[i]=function(e){("none"!==e.end&&!e.setAttr||1===e.pos&&!e.setAttr)&&(jQuery.style(e.elem,i,e.end),e.setAttr=!0)}}),e.fn.addBack||(e.fn.addBack=function(e){return this.add(null==e?this.prevObject:this.prevObject.filter(e))}),e.effects.animateClass=function(n,a,o,r){var h=e.speed(a,o,r);return this.queue(function(){var a,o=e(this),r=o.attr("class")||"",l=h.children?o.find("*").addBack():o;l=l.map(function(){var i=e(this);return{el:i,start:t(this)}}),a=function(){e.each(s,function(e,t){n[t]&&o[t+"Class"](n[t])})},a(),l=l.map(function(){return this.end=t(this.el[0]),this.diff=i(this.start,this.end),this}),o.attr("class",r),l=l.map(function(){var t=this,i=e.Deferred(),s=e.extend({},h,{queue:!1,complete:function(){i.resolve(t)}});return this.el.animate(this.diff,s),i.promise()}),e.when.apply(e,l.get()).done(function(){a(),e.each(arguments,function(){var t=this.el;e.each(this.diff,function(e){t.css(e,"")})}),h.complete.call(o[0])})})},e.fn.extend({addClass:function(t){return function(i,s,n,a){return s?e.effects.animateClass.call(this,{add:i},s,n,a):t.apply(this,arguments)}}(e.fn.addClass),removeClass:function(t){return function(i,s,n,a){return arguments.length>1?e.effects.animateClass.call(this,{remove:i},s,n,a):t.apply(this,arguments)}}(e.fn.removeClass),toggleClass:function(t){return function(i,s,n,a,o){return"boolean"==typeof s||void 0===s?n?e.effects.animateClass.call(this,s?{add:i}:{remove:i},n,a,o):t.apply(this,arguments):e.effects.animateClass.call(this,{toggle:i},s,n,a)}}(e.fn.toggleClass),switchClass:function(t,i,s,n,a){return e.effects.animateClass.call(this,{add:i,remove:t},s,n,a)}})}(),function(){function t(t,i,s,n){return e.isPlainObject(t)&&(i=t,t=t.effect),t={effect:t},null==i&&(i={}),e.isFunction(i)&&(n=i,s=null,i={}),("number"==typeof i||e.fx.speeds[i])&&(n=s,s=i,i={}),e.isFunction(s)&&(n=s,s=null),i&&e.extend(t,i),s=s||i.duration,t.duration=e.fx.off?0:"number"==typeof s?s:s in e.fx.speeds?e.fx.speeds[s]:e.fx.speeds._default,t.complete=n||i.complete,t}function i(t){return!t||"number"==typeof t||e.fx.speeds[t]?!0:"string"!=typeof t||e.effects.effect[t]?e.isFunction(t)?!0:"object"!=typeof t||t.effect?!1:!0:!0}e.extend(e.effects,{version:"1.11.0",save:function(e,t){for(var i=0;t.length>i;i++)null!==t[i]&&e.data(v+t[i],e[0].style[t[i]])},restore:function(e,t){var i,s;for(s=0;t.length>s;s++)null!==t[s]&&(i=e.data(v+t[s]),void 0===i&&(i=""),e.css(t[s],i))},setMode:function(e,t){return"toggle"===t&&(t=e.is(":hidden")?"show":"hide"),t},getBaseline:function(e,t){var i,s;switch(e[0]){case"top":i=0;break;case"middle":i=.5;break;case"bottom":i=1;break;default:i=e[0]/t.height}switch(e[1]){case"left":s=0;break;case"center":s=.5;break;case"right":s=1;break;default:s=e[1]/t.width}return{x:s,y:i}},createWrapper:function(t){if(t.parent().is(".ui-effects-wrapper"))return t.parent();var i={width:t.outerWidth(!0),height:t.outerHeight(!0),"float":t.css("float")},s=e("<div></div>").addClass("ui-effects-wrapper").css({fontSize:"100%",background:"transparent",border:"none",margin:0,padding:0}),n={width:t.width(),height:t.height()},a=document.activeElement;try{a.id}catch(o){a=document.body}return t.wrap(s),(t[0]===a||e.contains(t[0],a))&&e(a).focus(),s=t.parent(),"static"===t.css("position")?(s.css({position:"relative"}),t.css({position:"relative"})):(e.extend(i,{position:t.css("position"),zIndex:t.css("z-index")}),e.each(["top","left","bottom","right"],function(e,s){i[s]=t.css(s),isNaN(parseInt(i[s],10))&&(i[s]="auto")}),t.css({position:"relative",top:0,left:0,right:"auto",bottom:"auto"})),t.css(n),s.css(i).show()},removeWrapper:function(t){var i=document.activeElement;return t.parent().is(".ui-effects-wrapper")&&(t.parent().replaceWith(t),(t[0]===i||e.contains(t[0],i))&&e(i).focus()),t},setTransition:function(t,i,s,n){return n=n||{},e.each(i,function(e,i){var a=t.cssUnit(i);a[0]>0&&(n[i]=a[0]*s+a[1])}),n}}),e.fn.extend({effect:function(){function i(t){function i(){e.isFunction(a)&&a.call(n[0]),e.isFunction(t)&&t()}var n=e(this),a=s.complete,r=s.mode;(n.is(":hidden")?"hide"===r:"show"===r)?(n[r](),i()):o.call(n[0],s,i)}var s=t.apply(this,arguments),n=s.mode,a=s.queue,o=e.effects.effect[s.effect];return e.fx.off||!o?n?this[n](s.duration,s.complete):this.each(function(){s.complete&&s.complete.call(this)}):a===!1?this.each(i):this.queue(a||"fx",i)},show:function(e){return function(s){if(i(s))return e.apply(this,arguments);var n=t.apply(this,arguments);return n.mode="show",this.effect.call(this,n)}}(e.fn.show),hide:function(e){return function(s){if(i(s))return e.apply(this,arguments);var n=t.apply(this,arguments);return n.mode="hide",this.effect.call(this,n)}}(e.fn.hide),toggle:function(e){return function(s){if(i(s)||"boolean"==typeof s)return e.apply(this,arguments);var n=t.apply(this,arguments);return n.mode="toggle",this.effect.call(this,n)}}(e.fn.toggle),cssUnit:function(t){var i=this.css(t),s=[];return e.each(["em","px","%","pt"],function(e,t){i.indexOf(t)>0&&(s=[parseFloat(i),t])}),s}})}(),function(){var t={};e.each(["Quad","Cubic","Quart","Quint","Expo"],function(e,i){t[i]=function(t){return Math.pow(t,e+2)}}),e.extend(t,{Sine:function(e){return 1-Math.cos(e*Math.PI/2)},Circ:function(e){return 1-Math.sqrt(1-e*e)},Elastic:function(e){return 0===e||1===e?e:-Math.pow(2,8*(e-1))*Math.sin((80*(e-1)-7.5)*Math.PI/15)},Back:function(e){return e*e*(3*e-2)},Bounce:function(e){for(var t,i=4;((t=Math.pow(2,--i))-1)/11>e;);return 1/Math.pow(4,3-i)-7.5625*Math.pow((3*t-2)/22-e,2)}}),e.each(t,function(t,i){e.easing["easeIn"+t]=i,e.easing["easeOut"+t]=function(e){return 1-i(1-e)},e.easing["easeInOut"+t]=function(e){return.5>e?i(2*e)/2:1-i(-2*e+2)/2}})}(),e.effects,e.effects.effect.blind=function(t,i){var s,n,a,o=e(this),r=/up|down|vertical/,h=/up|left|vertical|horizontal/,l=["position","top","bottom","left","right","height","width"],u=e.effects.setMode(o,t.mode||"hide"),d=t.direction||"up",c=r.test(d),p=c?"height":"width",f=c?"top":"left",m=h.test(d),g={},v="show"===u;o.parent().is(".ui-effects-wrapper")?e.effects.save(o.parent(),l):e.effects.save(o,l),o.show(),s=e.effects.createWrapper(o).css({overflow:"hidden"}),n=s[p](),a=parseFloat(s.css(f))||0,g[p]=v?n:0,m||(o.css(c?"bottom":"right",0).css(c?"top":"left","auto").css({position:"absolute"}),g[f]=v?a:n+a),v&&(s.css(p,0),m||s.css(f,a+n)),s.animate(g,{duration:t.duration,easing:t.easing,queue:!1,complete:function(){"hide"===u&&o.hide(),e.effects.restore(o,l),e.effects.removeWrapper(o),i()}})},e.effects.effect.bounce=function(t,i){var s,n,a,o=e(this),r=["position","top","bottom","left","right","height","width"],h=e.effects.setMode(o,t.mode||"effect"),l="hide"===h,u="show"===h,d=t.direction||"up",c=t.distance,p=t.times||5,f=2*p+(u||l?1:0),m=t.duration/f,g=t.easing,v="up"===d||"down"===d?"top":"left",y="up"===d||"left"===d,b=o.queue(),_=b.length;for((u||l)&&r.push("opacity"),e.effects.save(o,r),o.show(),e.effects.createWrapper(o),c||(c=o["top"===v?"outerHeight":"outerWidth"]()/3),u&&(a={opacity:1},a[v]=0,o.css("opacity",0).css(v,y?2*-c:2*c).animate(a,m,g)),l&&(c/=Math.pow(2,p-1)),a={},a[v]=0,s=0;p>s;s++)n={},n[v]=(y?"-=":"+=")+c,o.animate(n,m,g).animate(a,m,g),c=l?2*c:c/2;l&&(n={opacity:0},n[v]=(y?"-=":"+=")+c,o.animate(n,m,g)),o.queue(function(){l&&o.hide(),e.effects.restore(o,r),e.effects.removeWrapper(o),i()}),_>1&&b.splice.apply(b,[1,0].concat(b.splice(_,f+1))),o.dequeue()},e.effects.effect.clip=function(t,i){var s,n,a,o=e(this),r=["position","top","bottom","left","right","height","width"],h=e.effects.setMode(o,t.mode||"hide"),l="show"===h,u=t.direction||"vertical",d="vertical"===u,c=d?"height":"width",p=d?"top":"left",f={};e.effects.save(o,r),o.show(),s=e.effects.createWrapper(o).css({overflow:"hidden"}),n="IMG"===o[0].tagName?s:o,a=n[c](),l&&(n.css(c,0),n.css(p,a/2)),f[c]=l?a:0,f[p]=l?0:a/2,n.animate(f,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){l||o.hide(),e.effects.restore(o,r),e.effects.removeWrapper(o),i()}})},e.effects.effect.drop=function(t,i){var s,n=e(this),a=["position","top","bottom","left","right","opacity","height","width"],o=e.effects.setMode(n,t.mode||"hide"),r="show"===o,h=t.direction||"left",l="up"===h||"down"===h?"top":"left",u="up"===h||"left"===h?"pos":"neg",d={opacity:r?1:0};e.effects.save(n,a),n.show(),e.effects.createWrapper(n),s=t.distance||n["top"===l?"outerHeight":"outerWidth"](!0)/2,r&&n.css("opacity",0).css(l,"pos"===u?-s:s),d[l]=(r?"pos"===u?"+=":"-=":"pos"===u?"-=":"+=")+s,n.animate(d,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){"hide"===o&&n.hide(),e.effects.restore(n,a),e.effects.removeWrapper(n),i()}})},e.effects.effect.explode=function(t,i){function s(){b.push(this),b.length===d*c&&n()}function n(){p.css({visibility:"visible"}),e(b).remove(),m||p.hide(),i()}var a,o,r,h,l,u,d=t.pieces?Math.round(Math.sqrt(t.pieces)):3,c=d,p=e(this),f=e.effects.setMode(p,t.mode||"hide"),m="show"===f,g=p.show().css("visibility","hidden").offset(),v=Math.ceil(p.outerWidth()/c),y=Math.ceil(p.outerHeight()/d),b=[];for(a=0;d>a;a++)for(h=g.top+a*y,u=a-(d-1)/2,o=0;c>o;o++)r=g.left+o*v,l=o-(c-1)/2,p.clone().appendTo("body").wrap("<div></div>").css({position:"absolute",visibility:"visible",left:-o*v,top:-a*y}).parent().addClass("ui-effects-explode").css({position:"absolute",overflow:"hidden",width:v,height:y,left:r+(m?l*v:0),top:h+(m?u*y:0),opacity:m?0:1}).animate({left:r+(m?0:l*v),top:h+(m?0:u*y),opacity:m?1:0},t.duration||500,t.easing,s)},e.effects.effect.fade=function(t,i){var s=e(this),n=e.effects.setMode(s,t.mode||"toggle");s.animate({opacity:n},{queue:!1,duration:t.duration,easing:t.easing,complete:i})},e.effects.effect.fold=function(t,i){var s,n,a=e(this),o=["position","top","bottom","left","right","height","width"],r=e.effects.setMode(a,t.mode||"hide"),h="show"===r,l="hide"===r,u=t.size||15,d=/([0-9]+)%/.exec(u),c=!!t.horizFirst,p=h!==c,f=p?["width","height"]:["height","width"],m=t.duration/2,g={},v={};e.effects.save(a,o),a.show(),s=e.effects.createWrapper(a).css({overflow:"hidden"}),n=p?[s.width(),s.height()]:[s.height(),s.width()],d&&(u=parseInt(d[1],10)/100*n[l?0:1]),h&&s.css(c?{height:0,width:u}:{height:u,width:0}),g[f[0]]=h?n[0]:u,v[f[1]]=h?n[1]:0,s.animate(g,m,t.easing).animate(v,m,t.easing,function(){l&&a.hide(),e.effects.restore(a,o),e.effects.removeWrapper(a),i()})},e.effects.effect.highlight=function(t,i){var s=e(this),n=["backgroundImage","backgroundColor","opacity"],a=e.effects.setMode(s,t.mode||"show"),o={backgroundColor:s.css("backgroundColor")};"hide"===a&&(o.opacity=0),e.effects.save(s,n),s.show().css({backgroundImage:"none",backgroundColor:t.color||"#ffff99"}).animate(o,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){"hide"===a&&s.hide(),e.effects.restore(s,n),i()}})},e.effects.effect.size=function(t,i){var s,n,a,o=e(this),r=["position","top","bottom","left","right","width","height","overflow","opacity"],h=["position","top","bottom","left","right","overflow","opacity"],l=["width","height","overflow"],u=["fontSize"],d=["borderTopWidth","borderBottomWidth","paddingTop","paddingBottom"],c=["borderLeftWidth","borderRightWidth","paddingLeft","paddingRight"],p=e.effects.setMode(o,t.mode||"effect"),f=t.restore||"effect"!==p,m=t.scale||"both",g=t.origin||["middle","center"],v=o.css("position"),y=f?r:h,b={height:0,width:0,outerHeight:0,outerWidth:0};"show"===p&&o.show(),s={height:o.height(),width:o.width(),outerHeight:o.outerHeight(),outerWidth:o.outerWidth()},"toggle"===t.mode&&"show"===p?(o.from=t.to||b,o.to=t.from||s):(o.from=t.from||("show"===p?b:s),o.to=t.to||("hide"===p?b:s)),a={from:{y:o.from.height/s.height,x:o.from.width/s.width},to:{y:o.to.height/s.height,x:o.to.width/s.width}},("box"===m||"both"===m)&&(a.from.y!==a.to.y&&(y=y.concat(d),o.from=e.effects.setTransition(o,d,a.from.y,o.from),o.to=e.effects.setTransition(o,d,a.to.y,o.to)),a.from.x!==a.to.x&&(y=y.concat(c),o.from=e.effects.setTransition(o,c,a.from.x,o.from),o.to=e.effects.setTransition(o,c,a.to.x,o.to))),("content"===m||"both"===m)&&a.from.y!==a.to.y&&(y=y.concat(u).concat(l),o.from=e.effects.setTransition(o,u,a.from.y,o.from),o.to=e.effects.setTransition(o,u,a.to.y,o.to)),e.effects.save(o,y),o.show(),e.effects.createWrapper(o),o.css("overflow","hidden").css(o.from),g&&(n=e.effects.getBaseline(g,s),o.from.top=(s.outerHeight-o.outerHeight())*n.y,o.from.left=(s.outerWidth-o.outerWidth())*n.x,o.to.top=(s.outerHeight-o.to.outerHeight)*n.y,o.to.left=(s.outerWidth-o.to.outerWidth)*n.x),o.css(o.from),("content"===m||"both"===m)&&(d=d.concat(["marginTop","marginBottom"]).concat(u),c=c.concat(["marginLeft","marginRight"]),l=r.concat(d).concat(c),o.find("*[width]").each(function(){var i=e(this),s={height:i.height(),width:i.width(),outerHeight:i.outerHeight(),outerWidth:i.outerWidth()};
f&&e.effects.save(i,l),i.from={height:s.height*a.from.y,width:s.width*a.from.x,outerHeight:s.outerHeight*a.from.y,outerWidth:s.outerWidth*a.from.x},i.to={height:s.height*a.to.y,width:s.width*a.to.x,outerHeight:s.height*a.to.y,outerWidth:s.width*a.to.x},a.from.y!==a.to.y&&(i.from=e.effects.setTransition(i,d,a.from.y,i.from),i.to=e.effects.setTransition(i,d,a.to.y,i.to)),a.from.x!==a.to.x&&(i.from=e.effects.setTransition(i,c,a.from.x,i.from),i.to=e.effects.setTransition(i,c,a.to.x,i.to)),i.css(i.from),i.animate(i.to,t.duration,t.easing,function(){f&&e.effects.restore(i,l)})})),o.animate(o.to,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){0===o.to.opacity&&o.css("opacity",o.from.opacity),"hide"===p&&o.hide(),e.effects.restore(o,y),f||("static"===v?o.css({position:"relative",top:o.to.top,left:o.to.left}):e.each(["top","left"],function(e,t){o.css(t,function(t,i){var s=parseInt(i,10),n=e?o.to.left:o.to.top;return"auto"===i?n+"px":s+n+"px"})})),e.effects.removeWrapper(o),i()}})},e.effects.effect.scale=function(t,i){var s=e(this),n=e.extend(!0,{},t),a=e.effects.setMode(s,t.mode||"effect"),o=parseInt(t.percent,10)||(0===parseInt(t.percent,10)?0:"hide"===a?0:100),r=t.direction||"both",h=t.origin,l={height:s.height(),width:s.width(),outerHeight:s.outerHeight(),outerWidth:s.outerWidth()},u={y:"horizontal"!==r?o/100:1,x:"vertical"!==r?o/100:1};n.effect="size",n.queue=!1,n.complete=i,"effect"!==a&&(n.origin=h||["middle","center"],n.restore=!0),n.from=t.from||("show"===a?{height:0,width:0,outerHeight:0,outerWidth:0}:l),n.to={height:l.height*u.y,width:l.width*u.x,outerHeight:l.outerHeight*u.y,outerWidth:l.outerWidth*u.x},n.fade&&("show"===a&&(n.from.opacity=0,n.to.opacity=1),"hide"===a&&(n.from.opacity=1,n.to.opacity=0)),s.effect(n)},e.effects.effect.puff=function(t,i){var s=e(this),n=e.effects.setMode(s,t.mode||"hide"),a="hide"===n,o=parseInt(t.percent,10)||150,r=o/100,h={height:s.height(),width:s.width(),outerHeight:s.outerHeight(),outerWidth:s.outerWidth()};e.extend(t,{effect:"scale",queue:!1,fade:!0,mode:n,complete:i,percent:a?o:100,from:a?h:{height:h.height*r,width:h.width*r,outerHeight:h.outerHeight*r,outerWidth:h.outerWidth*r}}),s.effect(t)},e.effects.effect.pulsate=function(t,i){var s,n=e(this),a=e.effects.setMode(n,t.mode||"show"),o="show"===a,r="hide"===a,h=o||"hide"===a,l=2*(t.times||5)+(h?1:0),u=t.duration/l,d=0,c=n.queue(),p=c.length;for((o||!n.is(":visible"))&&(n.css("opacity",0).show(),d=1),s=1;l>s;s++)n.animate({opacity:d},u,t.easing),d=1-d;n.animate({opacity:d},u,t.easing),n.queue(function(){r&&n.hide(),i()}),p>1&&c.splice.apply(c,[1,0].concat(c.splice(p,l+1))),n.dequeue()},e.effects.effect.shake=function(t,i){var s,n=e(this),a=["position","top","bottom","left","right","height","width"],o=e.effects.setMode(n,t.mode||"effect"),r=t.direction||"left",h=t.distance||20,l=t.times||3,u=2*l+1,d=Math.round(t.duration/u),c="up"===r||"down"===r?"top":"left",p="up"===r||"left"===r,f={},m={},g={},v=n.queue(),y=v.length;for(e.effects.save(n,a),n.show(),e.effects.createWrapper(n),f[c]=(p?"-=":"+=")+h,m[c]=(p?"+=":"-=")+2*h,g[c]=(p?"-=":"+=")+2*h,n.animate(f,d,t.easing),s=1;l>s;s++)n.animate(m,d,t.easing).animate(g,d,t.easing);n.animate(m,d,t.easing).animate(f,d/2,t.easing).queue(function(){"hide"===o&&n.hide(),e.effects.restore(n,a),e.effects.removeWrapper(n),i()}),y>1&&v.splice.apply(v,[1,0].concat(v.splice(y,u+1))),n.dequeue()},e.effects.effect.slide=function(t,i){var s,n=e(this),a=["position","top","bottom","left","right","width","height"],o=e.effects.setMode(n,t.mode||"show"),r="show"===o,h=t.direction||"left",l="up"===h||"down"===h?"top":"left",u="up"===h||"left"===h,d={};e.effects.save(n,a),n.show(),s=t.distance||n["top"===l?"outerHeight":"outerWidth"](!0),e.effects.createWrapper(n).css({overflow:"hidden"}),r&&n.css(l,u?isNaN(s)?"-"+s:-s:s),d[l]=(r?u?"+=":"-=":u?"-=":"+=")+s,n.animate(d,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){"hide"===o&&n.hide(),e.effects.restore(n,a),e.effects.removeWrapper(n),i()}})},e.effects.effect.transfer=function(t,i){var s=e(this),n=e(t.to),a="fixed"===n.css("position"),o=e("body"),r=a?o.scrollTop():0,h=a?o.scrollLeft():0,l=n.offset(),u={top:l.top-r,left:l.left-h,height:n.innerHeight(),width:n.innerWidth()},d=s.offset(),c=e("<div class='ui-effects-transfer'></div>").appendTo(document.body).addClass(t.className).css({top:d.top-r,left:d.left-h,height:s.innerHeight(),width:s.innerWidth(),position:a?"fixed":"absolute"}).animate(u,t.duration,t.easing,function(){c.remove(),i()})},e.widget("ui.progressbar",{version:"1.11.0",options:{max:100,value:0,change:null,complete:null},min:0,_create:function(){this.oldValue=this.options.value=this._constrainedValue(),this.element.addClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").attr({role:"progressbar","aria-valuemin":this.min}),this.valueDiv=e("<div class='ui-progressbar-value ui-widget-header ui-corner-left'></div>").appendTo(this.element),this._refreshValue()},_destroy:function(){this.element.removeClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"),this.valueDiv.remove()},value:function(e){return void 0===e?this.options.value:(this.options.value=this._constrainedValue(e),this._refreshValue(),void 0)},_constrainedValue:function(e){return void 0===e&&(e=this.options.value),this.indeterminate=e===!1,"number"!=typeof e&&(e=0),this.indeterminate?!1:Math.min(this.options.max,Math.max(this.min,e))},_setOptions:function(e){var t=e.value;delete e.value,this._super(e),this.options.value=this._constrainedValue(t),this._refreshValue()},_setOption:function(e,t){"max"===e&&(t=Math.max(this.min,t)),"disabled"===e&&this.element.toggleClass("ui-state-disabled",!!t).attr("aria-disabled",t),this._super(e,t)},_percentage:function(){return this.indeterminate?100:100*(this.options.value-this.min)/(this.options.max-this.min)},_refreshValue:function(){var t=this.options.value,i=this._percentage();this.valueDiv.toggle(this.indeterminate||t>this.min).toggleClass("ui-corner-right",t===this.options.max).width(i.toFixed(0)+"%"),this.element.toggleClass("ui-progressbar-indeterminate",this.indeterminate),this.indeterminate?(this.element.removeAttr("aria-valuenow"),this.overlayDiv||(this.overlayDiv=e("<div class='ui-progressbar-overlay'></div>").appendTo(this.valueDiv))):(this.element.attr({"aria-valuemax":this.options.max,"aria-valuenow":t}),this.overlayDiv&&(this.overlayDiv.remove(),this.overlayDiv=null)),this.oldValue!==t&&(this.oldValue=t,this._trigger("change")),t===this.options.max&&this._trigger("complete")}}),e.widget("ui.selectable",e.ui.mouse,{version:"1.11.0",options:{appendTo:"body",autoRefresh:!0,distance:0,filter:"*",tolerance:"touch",selected:null,selecting:null,start:null,stop:null,unselected:null,unselecting:null},_create:function(){var t,i=this;this.element.addClass("ui-selectable"),this.dragged=!1,this.refresh=function(){t=e(i.options.filter,i.element[0]),t.addClass("ui-selectee"),t.each(function(){var t=e(this),i=t.offset();e.data(this,"selectable-item",{element:this,$element:t,left:i.left,top:i.top,right:i.left+t.outerWidth(),bottom:i.top+t.outerHeight(),startselected:!1,selected:t.hasClass("ui-selected"),selecting:t.hasClass("ui-selecting"),unselecting:t.hasClass("ui-unselecting")})})},this.refresh(),this.selectees=t.addClass("ui-selectee"),this._mouseInit(),this.helper=e("<div class='ui-selectable-helper'></div>")},_destroy:function(){this.selectees.removeClass("ui-selectee").removeData("selectable-item"),this.element.removeClass("ui-selectable ui-selectable-disabled"),this._mouseDestroy()},_mouseStart:function(t){var i=this,s=this.options;this.opos=[t.pageX,t.pageY],this.options.disabled||(this.selectees=e(s.filter,this.element[0]),this._trigger("start",t),e(s.appendTo).append(this.helper),this.helper.css({left:t.pageX,top:t.pageY,width:0,height:0}),s.autoRefresh&&this.refresh(),this.selectees.filter(".ui-selected").each(function(){var s=e.data(this,"selectable-item");s.startselected=!0,t.metaKey||t.ctrlKey||(s.$element.removeClass("ui-selected"),s.selected=!1,s.$element.addClass("ui-unselecting"),s.unselecting=!0,i._trigger("unselecting",t,{unselecting:s.element}))}),e(t.target).parents().addBack().each(function(){var s,n=e.data(this,"selectable-item");return n?(s=!t.metaKey&&!t.ctrlKey||!n.$element.hasClass("ui-selected"),n.$element.removeClass(s?"ui-unselecting":"ui-selected").addClass(s?"ui-selecting":"ui-unselecting"),n.unselecting=!s,n.selecting=s,n.selected=s,s?i._trigger("selecting",t,{selecting:n.element}):i._trigger("unselecting",t,{unselecting:n.element}),!1):void 0}))},_mouseDrag:function(t){if(this.dragged=!0,!this.options.disabled){var i,s=this,n=this.options,a=this.opos[0],o=this.opos[1],r=t.pageX,h=t.pageY;return a>r&&(i=r,r=a,a=i),o>h&&(i=h,h=o,o=i),this.helper.css({left:a,top:o,width:r-a,height:h-o}),this.selectees.each(function(){var i=e.data(this,"selectable-item"),l=!1;i&&i.element!==s.element[0]&&("touch"===n.tolerance?l=!(i.left>r||a>i.right||i.top>h||o>i.bottom):"fit"===n.tolerance&&(l=i.left>a&&r>i.right&&i.top>o&&h>i.bottom),l?(i.selected&&(i.$element.removeClass("ui-selected"),i.selected=!1),i.unselecting&&(i.$element.removeClass("ui-unselecting"),i.unselecting=!1),i.selecting||(i.$element.addClass("ui-selecting"),i.selecting=!0,s._trigger("selecting",t,{selecting:i.element}))):(i.selecting&&((t.metaKey||t.ctrlKey)&&i.startselected?(i.$element.removeClass("ui-selecting"),i.selecting=!1,i.$element.addClass("ui-selected"),i.selected=!0):(i.$element.removeClass("ui-selecting"),i.selecting=!1,i.startselected&&(i.$element.addClass("ui-unselecting"),i.unselecting=!0),s._trigger("unselecting",t,{unselecting:i.element}))),i.selected&&(t.metaKey||t.ctrlKey||i.startselected||(i.$element.removeClass("ui-selected"),i.selected=!1,i.$element.addClass("ui-unselecting"),i.unselecting=!0,s._trigger("unselecting",t,{unselecting:i.element})))))}),!1}},_mouseStop:function(t){var i=this;return this.dragged=!1,e(".ui-unselecting",this.element[0]).each(function(){var s=e.data(this,"selectable-item");s.$element.removeClass("ui-unselecting"),s.unselecting=!1,s.startselected=!1,i._trigger("unselected",t,{unselected:s.element})}),e(".ui-selecting",this.element[0]).each(function(){var s=e.data(this,"selectable-item");s.$element.removeClass("ui-selecting").addClass("ui-selected"),s.selecting=!1,s.selected=!0,s.startselected=!0,i._trigger("selected",t,{selected:s.element})}),this._trigger("stop",t),this.helper.remove(),!1}}),e.widget("ui.selectmenu",{version:"1.11.0",defaultElement:"<select>",options:{appendTo:null,disabled:null,icons:{button:"ui-icon-triangle-1-s"},position:{my:"left top",at:"left bottom",collision:"none"},width:null,change:null,close:null,focus:null,open:null,select:null},_create:function(){var e=this.element.uniqueId().attr("id");this.ids={element:e,button:e+"-button",menu:e+"-menu"},this._drawButton(),this._drawMenu(),this.options.disabled&&this.disable()},_drawButton:function(){var t=this,i=this.element.attr("tabindex");this.label=e("label[for='"+this.ids.element+"']").attr("for",this.ids.button),this._on(this.label,{click:function(e){this.button.focus(),e.preventDefault()}}),this.element.hide(),this.button=e("<span>",{"class":"ui-selectmenu-button ui-widget ui-state-default ui-corner-all",tabindex:i||this.options.disabled?-1:0,id:this.ids.button,role:"combobox","aria-expanded":"false","aria-autocomplete":"list","aria-owns":this.ids.menu,"aria-haspopup":"true"}).insertAfter(this.element),e("<span>",{"class":"ui-icon "+this.options.icons.button}).prependTo(this.button),this.buttonText=e("<span>",{"class":"ui-selectmenu-text"}).appendTo(this.button),this._setText(this.buttonText,this.element.find("option:selected").text()),this._setOption("width",this.options.width),this._on(this.button,this._buttonEvents),this.button.one("focusin",function(){t.menuItems||t._refreshMenu()}),this._hoverable(this.button),this._focusable(this.button)},_drawMenu:function(){var t=this;this.menu=e("<ul>",{"aria-hidden":"true","aria-labelledby":this.ids.button,id:this.ids.menu}),this.menuWrap=e("<div>",{"class":"ui-selectmenu-menu ui-front"}).append(this.menu).appendTo(this._appendTo()),this.menuInstance=this.menu.menu({role:"listbox",select:function(e,i){e.preventDefault(),t._select(i.item.data("ui-selectmenu-item"),e)},focus:function(e,i){var s=i.item.data("ui-selectmenu-item");null!=t.focusIndex&&s.index!==t.focusIndex&&(t._trigger("focus",e,{item:s}),t.isOpen||t._select(s,e)),t.focusIndex=s.index,t.button.attr("aria-activedescendant",t.menuItems.eq(s.index).attr("id"))}}).menu("instance"),this.menu.addClass("ui-corner-bottom").removeClass("ui-corner-all"),this.menuInstance._off(this.menu,"mouseleave"),this.menuInstance._closeOnDocumentClick=function(){return!1},this.menuInstance._isDivider=function(){return!1}},refresh:function(){this._refreshMenu(),this._setText(this.buttonText,this._getSelectedItem().text()),this._setOption("width",this.options.width)},_refreshMenu:function(){this.menu.empty();var e,t=this.element.find("option");t.length&&(this._parseOptions(t),this._renderMenu(this.menu,this.items),this.menuInstance.refresh(),this.menuItems=this.menu.find("li").not(".ui-selectmenu-optgroup"),e=this._getSelectedItem(),this.menuInstance.focus(null,e),this._setAria(e.data("ui-selectmenu-item")),this._setOption("disabled",this.element.prop("disabled")))},open:function(e){this.options.disabled||(this.menuItems?(this.menu.find(".ui-state-focus").removeClass("ui-state-focus"),this.menuInstance.focus(null,this._getSelectedItem())):this._refreshMenu(),this.isOpen=!0,this._toggleAttr(),this._resizeMenu(),this._position(),this._on(this.document,this._documentClick),this._trigger("open",e))},_position:function(){this.menuWrap.position(e.extend({of:this.button},this.options.position))},close:function(e){this.isOpen&&(this.isOpen=!1,this._toggleAttr(),this._off(this.document),this._trigger("close",e))},widget:function(){return this.button},menuWidget:function(){return this.menu},_renderMenu:function(t,i){var s=this,n="";e.each(i,function(i,a){a.optgroup!==n&&(e("<li>",{"class":"ui-selectmenu-optgroup ui-menu-divider"+(a.element.parent("optgroup").prop("disabled")?" ui-state-disabled":""),text:a.optgroup}).appendTo(t),n=a.optgroup),s._renderItemData(t,a)})},_renderItemData:function(e,t){return this._renderItem(e,t).data("ui-selectmenu-item",t)},_renderItem:function(t,i){var s=e("<li>");return i.disabled&&s.addClass("ui-state-disabled"),this._setText(s,i.label),s.appendTo(t)},_setText:function(e,t){t?e.text(t):e.html("&#160;")},_move:function(e,t){var i,s,n=".ui-menu-item";this.isOpen?i=this.menuItems.eq(this.focusIndex):(i=this.menuItems.eq(this.element[0].selectedIndex),n+=":not(.ui-state-disabled)"),s="first"===e||"last"===e?i["first"===e?"prevAll":"nextAll"](n).eq(-1):i[e+"All"](n).eq(0),s.length&&this.menuInstance.focus(t,s)},_getSelectedItem:function(){return this.menuItems.eq(this.element[0].selectedIndex)},_toggle:function(e){this[this.isOpen?"close":"open"](e)},_documentClick:{mousedown:function(t){this.isOpen&&(e(t.target).closest(".ui-selectmenu-menu, #"+this.ids.button).length||this.close(t))}},_buttonEvents:{click:"_toggle",keydown:function(t){var i=!0;switch(t.keyCode){case e.ui.keyCode.TAB:case e.ui.keyCode.ESCAPE:this.close(t),i=!1;break;case e.ui.keyCode.ENTER:this.isOpen&&this._selectFocusedItem(t);break;case e.ui.keyCode.UP:t.altKey?this._toggle(t):this._move("prev",t);break;case e.ui.keyCode.DOWN:t.altKey?this._toggle(t):this._move("next",t);break;case e.ui.keyCode.SPACE:this.isOpen?this._selectFocusedItem(t):this._toggle(t);break;case e.ui.keyCode.LEFT:this._move("prev",t);break;case e.ui.keyCode.RIGHT:this._move("next",t);break;case e.ui.keyCode.HOME:case e.ui.keyCode.PAGE_UP:this._move("first",t);break;case e.ui.keyCode.END:case e.ui.keyCode.PAGE_DOWN:this._move("last",t);break;default:this.menu.trigger(t),i=!1}i&&t.preventDefault()}},_selectFocusedItem:function(e){var t=this.menuItems.eq(this.focusIndex);t.hasClass("ui-state-disabled")||this._select(t.data("ui-selectmenu-item"),e)},_select:function(e,t){var i=this.element[0].selectedIndex;this.element[0].selectedIndex=e.index,this._setText(this.buttonText,e.label),this._setAria(e),this._trigger("select",t,{item:e}),e.index!==i&&this._trigger("change",t,{item:e}),this.close(t)},_setAria:function(e){var t=this.menuItems.eq(e.index).attr("id");this.button.attr({"aria-labelledby":t,"aria-activedescendant":t}),this.menu.attr("aria-activedescendant",t)},_setOption:function(e,t){"icons"===e&&this.button.find("span.ui-icon").removeClass(this.options.icons.button).addClass(t.button),this._super(e,t),"appendTo"===e&&this.menuWrap.appendTo(this._appendTo()),"disabled"===e&&(this.menuInstance.option("disabled",t),this.button.toggleClass("ui-state-disabled",t).attr("aria-disabled",t),this.element.prop("disabled",t),t?(this.button.attr("tabindex",-1),this.close()):this.button.attr("tabindex",0)),"width"===e&&(t||(t=this.element.outerWidth()),this.button.outerWidth(t))},_appendTo:function(){var t=this.options.appendTo;return t&&(t=t.jquery||t.nodeType?e(t):this.document.find(t).eq(0)),t&&t[0]||(t=this.element.closest(".ui-front")),t.length||(t=this.document[0].body),t},_toggleAttr:function(){this.button.toggleClass("ui-corner-top",this.isOpen).toggleClass("ui-corner-all",!this.isOpen).attr("aria-expanded",this.isOpen),this.menuWrap.toggleClass("ui-selectmenu-open",this.isOpen),this.menu.attr("aria-hidden",!this.isOpen)},_resizeMenu:function(){this.menu.outerWidth(Math.max(this.button.outerWidth(),this.menu.width("").outerWidth()+1))},_getCreateOptions:function(){return{disabled:this.element.prop("disabled")}},_parseOptions:function(t){var i=[];t.each(function(t,s){var n=e(s),a=n.parent("optgroup");i.push({element:n,index:t,value:n.attr("value"),label:n.text(),optgroup:a.attr("label")||"",disabled:a.prop("disabled")||n.prop("disabled")})}),this.items=i},_destroy:function(){this.menuWrap.remove(),this.button.remove(),this.element.show(),this.element.removeUniqueId(),this.label.attr("for",this.ids.element)}}),e.widget("ui.slider",e.ui.mouse,{version:"1.11.0",widgetEventPrefix:"slide",options:{animate:!1,distance:0,max:100,min:0,orientation:"horizontal",range:!1,step:1,value:0,values:null,change:null,slide:null,start:null,stop:null},numPages:5,_create:function(){this._keySliding=!1,this._mouseSliding=!1,this._animateOff=!0,this._handleIndex=null,this._detectOrientation(),this._mouseInit(),this.element.addClass("ui-slider ui-slider-"+this.orientation+" ui-widget"+" ui-widget-content"+" ui-corner-all"),this._refresh(),this._setOption("disabled",this.options.disabled),this._animateOff=!1},_refresh:function(){this._createRange(),this._createHandles(),this._setupEvents(),this._refreshValue()},_createHandles:function(){var t,i,s=this.options,n=this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"),a="<span class='ui-slider-handle ui-state-default ui-corner-all' tabindex='0'></span>",o=[];for(i=s.values&&s.values.length||1,n.length>i&&(n.slice(i).remove(),n=n.slice(0,i)),t=n.length;i>t;t++)o.push(a);this.handles=n.add(e(o.join("")).appendTo(this.element)),this.handle=this.handles.eq(0),this.handles.each(function(t){e(this).data("ui-slider-handle-index",t)})},_createRange:function(){var t=this.options,i="";t.range?(t.range===!0&&(t.values?t.values.length&&2!==t.values.length?t.values=[t.values[0],t.values[0]]:e.isArray(t.values)&&(t.values=t.values.slice(0)):t.values=[this._valueMin(),this._valueMin()]),this.range&&this.range.length?this.range.removeClass("ui-slider-range-min ui-slider-range-max").css({left:"",bottom:""}):(this.range=e("<div></div>").appendTo(this.element),i="ui-slider-range ui-widget-header ui-corner-all"),this.range.addClass(i+("min"===t.range||"max"===t.range?" ui-slider-range-"+t.range:""))):(this.range&&this.range.remove(),this.range=null)},_setupEvents:function(){this._off(this.handles),this._on(this.handles,this._handleEvents),this._hoverable(this.handles),this._focusable(this.handles)},_destroy:function(){this.handles.remove(),this.range&&this.range.remove(),this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-widget ui-widget-content ui-corner-all"),this._mouseDestroy()},_mouseCapture:function(t){var i,s,n,a,o,r,h,l,u=this,d=this.options;return d.disabled?!1:(this.elementSize={width:this.element.outerWidth(),height:this.element.outerHeight()},this.elementOffset=this.element.offset(),i={x:t.pageX,y:t.pageY},s=this._normValueFromMouse(i),n=this._valueMax()-this._valueMin()+1,this.handles.each(function(t){var i=Math.abs(s-u.values(t));(n>i||n===i&&(t===u._lastChangedValue||u.values(t)===d.min))&&(n=i,a=e(this),o=t)}),r=this._start(t,o),r===!1?!1:(this._mouseSliding=!0,this._handleIndex=o,a.addClass("ui-state-active").focus(),h=a.offset(),l=!e(t.target).parents().addBack().is(".ui-slider-handle"),this._clickOffset=l?{left:0,top:0}:{left:t.pageX-h.left-a.width()/2,top:t.pageY-h.top-a.height()/2-(parseInt(a.css("borderTopWidth"),10)||0)-(parseInt(a.css("borderBottomWidth"),10)||0)+(parseInt(a.css("marginTop"),10)||0)},this.handles.hasClass("ui-state-hover")||this._slide(t,o,s),this._animateOff=!0,!0))},_mouseStart:function(){return!0},_mouseDrag:function(e){var t={x:e.pageX,y:e.pageY},i=this._normValueFromMouse(t);return this._slide(e,this._handleIndex,i),!1},_mouseStop:function(e){return this.handles.removeClass("ui-state-active"),this._mouseSliding=!1,this._stop(e,this._handleIndex),this._change(e,this._handleIndex),this._handleIndex=null,this._clickOffset=null,this._animateOff=!1,!1},_detectOrientation:function(){this.orientation="vertical"===this.options.orientation?"vertical":"horizontal"},_normValueFromMouse:function(e){var t,i,s,n,a;return"horizontal"===this.orientation?(t=this.elementSize.width,i=e.x-this.elementOffset.left-(this._clickOffset?this._clickOffset.left:0)):(t=this.elementSize.height,i=e.y-this.elementOffset.top-(this._clickOffset?this._clickOffset.top:0)),s=i/t,s>1&&(s=1),0>s&&(s=0),"vertical"===this.orientation&&(s=1-s),n=this._valueMax()-this._valueMin(),a=this._valueMin()+s*n,this._trimAlignValue(a)},_start:function(e,t){var i={handle:this.handles[t],value:this.value()};return this.options.values&&this.options.values.length&&(i.value=this.values(t),i.values=this.values()),this._trigger("start",e,i)},_slide:function(e,t,i){var s,n,a;this.options.values&&this.options.values.length?(s=this.values(t?0:1),2===this.options.values.length&&this.options.range===!0&&(0===t&&i>s||1===t&&s>i)&&(i=s),i!==this.values(t)&&(n=this.values(),n[t]=i,a=this._trigger("slide",e,{handle:this.handles[t],value:i,values:n}),s=this.values(t?0:1),a!==!1&&this.values(t,i))):i!==this.value()&&(a=this._trigger("slide",e,{handle:this.handles[t],value:i}),a!==!1&&this.value(i))},_stop:function(e,t){var i={handle:this.handles[t],value:this.value()};this.options.values&&this.options.values.length&&(i.value=this.values(t),i.values=this.values()),this._trigger("stop",e,i)},_change:function(e,t){if(!this._keySliding&&!this._mouseSliding){var i={handle:this.handles[t],value:this.value()};this.options.values&&this.options.values.length&&(i.value=this.values(t),i.values=this.values()),this._lastChangedValue=t,this._trigger("change",e,i)}},value:function(e){return arguments.length?(this.options.value=this._trimAlignValue(e),this._refreshValue(),this._change(null,0),void 0):this._value()},values:function(t,i){var s,n,a;if(arguments.length>1)return this.options.values[t]=this._trimAlignValue(i),this._refreshValue(),this._change(null,t),void 0;if(!arguments.length)return this._values();if(!e.isArray(arguments[0]))return this.options.values&&this.options.values.length?this._values(t):this.value();for(s=this.options.values,n=arguments[0],a=0;s.length>a;a+=1)s[a]=this._trimAlignValue(n[a]),this._change(null,a);this._refreshValue()},_setOption:function(t,i){var s,n=0;switch("range"===t&&this.options.range===!0&&("min"===i?(this.options.value=this._values(0),this.options.values=null):"max"===i&&(this.options.value=this._values(this.options.values.length-1),this.options.values=null)),e.isArray(this.options.values)&&(n=this.options.values.length),"disabled"===t&&this.element.toggleClass("ui-state-disabled",!!i),this._super(t,i),t){case"orientation":this._detectOrientation(),this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-"+this.orientation),this._refreshValue();break;case"value":this._animateOff=!0,this._refreshValue(),this._change(null,0),this._animateOff=!1;break;case"values":for(this._animateOff=!0,this._refreshValue(),s=0;n>s;s+=1)this._change(null,s);this._animateOff=!1;break;case"min":case"max":this._animateOff=!0,this._refreshValue(),this._animateOff=!1;break;case"range":this._animateOff=!0,this._refresh(),this._animateOff=!1}},_value:function(){var e=this.options.value;return e=this._trimAlignValue(e)},_values:function(e){var t,i,s;if(arguments.length)return t=this.options.values[e],t=this._trimAlignValue(t);if(this.options.values&&this.options.values.length){for(i=this.options.values.slice(),s=0;i.length>s;s+=1)i[s]=this._trimAlignValue(i[s]);return i}return[]},_trimAlignValue:function(e){if(this._valueMin()>=e)return this._valueMin();if(e>=this._valueMax())return this._valueMax();var t=this.options.step>0?this.options.step:1,i=(e-this._valueMin())%t,s=e-i;return 2*Math.abs(i)>=t&&(s+=i>0?t:-t),parseFloat(s.toFixed(5))},_valueMin:function(){return this.options.min},_valueMax:function(){return this.options.max},_refreshValue:function(){var t,i,s,n,a,o=this.options.range,r=this.options,h=this,l=this._animateOff?!1:r.animate,u={};this.options.values&&this.options.values.length?this.handles.each(function(s){i=100*((h.values(s)-h._valueMin())/(h._valueMax()-h._valueMin())),u["horizontal"===h.orientation?"left":"bottom"]=i+"%",e(this).stop(1,1)[l?"animate":"css"](u,r.animate),h.options.range===!0&&("horizontal"===h.orientation?(0===s&&h.range.stop(1,1)[l?"animate":"css"]({left:i+"%"},r.animate),1===s&&h.range[l?"animate":"css"]({width:i-t+"%"},{queue:!1,duration:r.animate})):(0===s&&h.range.stop(1,1)[l?"animate":"css"]({bottom:i+"%"},r.animate),1===s&&h.range[l?"animate":"css"]({height:i-t+"%"},{queue:!1,duration:r.animate}))),t=i}):(s=this.value(),n=this._valueMin(),a=this._valueMax(),i=a!==n?100*((s-n)/(a-n)):0,u["horizontal"===this.orientation?"left":"bottom"]=i+"%",this.handle.stop(1,1)[l?"animate":"css"](u,r.animate),"min"===o&&"horizontal"===this.orientation&&this.range.stop(1,1)[l?"animate":"css"]({width:i+"%"},r.animate),"max"===o&&"horizontal"===this.orientation&&this.range[l?"animate":"css"]({width:100-i+"%"},{queue:!1,duration:r.animate}),"min"===o&&"vertical"===this.orientation&&this.range.stop(1,1)[l?"animate":"css"]({height:i+"%"},r.animate),"max"===o&&"vertical"===this.orientation&&this.range[l?"animate":"css"]({height:100-i+"%"},{queue:!1,duration:r.animate}))},_handleEvents:{keydown:function(t){var i,s,n,a,o=e(t.target).data("ui-slider-handle-index");switch(t.keyCode){case e.ui.keyCode.HOME:case e.ui.keyCode.END:case e.ui.keyCode.PAGE_UP:case e.ui.keyCode.PAGE_DOWN:case e.ui.keyCode.UP:case e.ui.keyCode.RIGHT:case e.ui.keyCode.DOWN:case e.ui.keyCode.LEFT:if(t.preventDefault(),!this._keySliding&&(this._keySliding=!0,e(t.target).addClass("ui-state-active"),i=this._start(t,o),i===!1))return}switch(a=this.options.step,s=n=this.options.values&&this.options.values.length?this.values(o):this.value(),t.keyCode){case e.ui.keyCode.HOME:n=this._valueMin();break;case e.ui.keyCode.END:n=this._valueMax();break;case e.ui.keyCode.PAGE_UP:n=this._trimAlignValue(s+(this._valueMax()-this._valueMin())/this.numPages);break;case e.ui.keyCode.PAGE_DOWN:n=this._trimAlignValue(s-(this._valueMax()-this._valueMin())/this.numPages);break;case e.ui.keyCode.UP:case e.ui.keyCode.RIGHT:if(s===this._valueMax())return;n=this._trimAlignValue(s+a);break;case e.ui.keyCode.DOWN:case e.ui.keyCode.LEFT:if(s===this._valueMin())return;n=this._trimAlignValue(s-a)}this._slide(t,o,n)},keyup:function(t){var i=e(t.target).data("ui-slider-handle-index");this._keySliding&&(this._keySliding=!1,this._stop(t,i),this._change(t,i),e(t.target).removeClass("ui-state-active"))}}}),e.widget("ui.sortable",e.ui.mouse,{version:"1.11.0",widgetEventPrefix:"sort",ready:!1,options:{appendTo:"parent",axis:!1,connectWith:!1,containment:!1,cursor:"auto",cursorAt:!1,dropOnEmpty:!0,forcePlaceholderSize:!1,forceHelperSize:!1,grid:!1,handle:!1,helper:"original",items:"> *",opacity:!1,placeholder:!1,revert:!1,scroll:!0,scrollSensitivity:20,scrollSpeed:20,scope:"default",tolerance:"intersect",zIndex:1e3,activate:null,beforeStop:null,change:null,deactivate:null,out:null,over:null,receive:null,remove:null,sort:null,start:null,stop:null,update:null},_isOverAxis:function(e,t,i){return e>=t&&t+i>e},_isFloating:function(e){return/left|right/.test(e.css("float"))||/inline|table-cell/.test(e.css("display"))},_create:function(){var e=this.options;this.containerCache={},this.element.addClass("ui-sortable"),this.refresh(),this.floating=this.items.length?"x"===e.axis||this._isFloating(this.items[0].item):!1,this.offset=this.element.offset(),this._mouseInit(),this._setHandleClassName(),this.ready=!0},_setOption:function(e,t){this._super(e,t),"handle"===e&&this._setHandleClassName()},_setHandleClassName:function(){this.element.find(".ui-sortable-handle").removeClass("ui-sortable-handle"),e.each(this.items,function(){(this.instance.options.handle?this.item.find(this.instance.options.handle):this.item).addClass("ui-sortable-handle")})},_destroy:function(){this.element.removeClass("ui-sortable ui-sortable-disabled").find(".ui-sortable-handle").removeClass("ui-sortable-handle"),this._mouseDestroy();for(var e=this.items.length-1;e>=0;e--)this.items[e].item.removeData(this.widgetName+"-item");return this},_mouseCapture:function(t,i){var s=null,n=!1,a=this;return this.reverting?!1:this.options.disabled||"static"===this.options.type?!1:(this._refreshItems(t),e(t.target).parents().each(function(){return e.data(this,a.widgetName+"-item")===a?(s=e(this),!1):void 0}),e.data(t.target,a.widgetName+"-item")===a&&(s=e(t.target)),s?!this.options.handle||i||(e(this.options.handle,s).find("*").addBack().each(function(){this===t.target&&(n=!0)}),n)?(this.currentItem=s,this._removeCurrentsFromItems(),!0):!1:!1)},_mouseStart:function(t,i,s){var n,a,o=this.options;if(this.currentContainer=this,this.refreshPositions(),this.helper=this._createHelper(t),this._cacheHelperProportions(),this._cacheMargins(),this.scrollParent=this.helper.scrollParent(),this.offset=this.currentItem.offset(),this.offset={top:this.offset.top-this.margins.top,left:this.offset.left-this.margins.left},e.extend(this.offset,{click:{left:t.pageX-this.offset.left,top:t.pageY-this.offset.top},parent:this._getParentOffset(),relative:this._getRelativeOffset()}),this.helper.css("position","absolute"),this.cssPosition=this.helper.css("position"),this.originalPosition=this._generatePosition(t),this.originalPageX=t.pageX,this.originalPageY=t.pageY,o.cursorAt&&this._adjustOffsetFromHelper(o.cursorAt),this.domPosition={prev:this.currentItem.prev()[0],parent:this.currentItem.parent()[0]},this.helper[0]!==this.currentItem[0]&&this.currentItem.hide(),this._createPlaceholder(),o.containment&&this._setContainment(),o.cursor&&"auto"!==o.cursor&&(a=this.document.find("body"),this.storedCursor=a.css("cursor"),a.css("cursor",o.cursor),this.storedStylesheet=e("<style>*{ cursor: "+o.cursor+" !important; }</style>").appendTo(a)),o.opacity&&(this.helper.css("opacity")&&(this._storedOpacity=this.helper.css("opacity")),this.helper.css("opacity",o.opacity)),o.zIndex&&(this.helper.css("zIndex")&&(this._storedZIndex=this.helper.css("zIndex")),this.helper.css("zIndex",o.zIndex)),this.scrollParent[0]!==document&&"HTML"!==this.scrollParent[0].tagName&&(this.overflowOffset=this.scrollParent.offset()),this._trigger("start",t,this._uiHash()),this._preserveHelperProportions||this._cacheHelperProportions(),!s)for(n=this.containers.length-1;n>=0;n--)this.containers[n]._trigger("activate",t,this._uiHash(this));
return e.ui.ddmanager&&(e.ui.ddmanager.current=this),e.ui.ddmanager&&!o.dropBehaviour&&e.ui.ddmanager.prepareOffsets(this,t),this.dragging=!0,this.helper.addClass("ui-sortable-helper"),this._mouseDrag(t),!0},_mouseDrag:function(t){var i,s,n,a,o=this.options,r=!1;for(this.position=this._generatePosition(t),this.positionAbs=this._convertPositionTo("absolute"),this.lastPositionAbs||(this.lastPositionAbs=this.positionAbs),this.options.scroll&&(this.scrollParent[0]!==document&&"HTML"!==this.scrollParent[0].tagName?(this.overflowOffset.top+this.scrollParent[0].offsetHeight-t.pageY<o.scrollSensitivity?this.scrollParent[0].scrollTop=r=this.scrollParent[0].scrollTop+o.scrollSpeed:t.pageY-this.overflowOffset.top<o.scrollSensitivity&&(this.scrollParent[0].scrollTop=r=this.scrollParent[0].scrollTop-o.scrollSpeed),this.overflowOffset.left+this.scrollParent[0].offsetWidth-t.pageX<o.scrollSensitivity?this.scrollParent[0].scrollLeft=r=this.scrollParent[0].scrollLeft+o.scrollSpeed:t.pageX-this.overflowOffset.left<o.scrollSensitivity&&(this.scrollParent[0].scrollLeft=r=this.scrollParent[0].scrollLeft-o.scrollSpeed)):(t.pageY-e(document).scrollTop()<o.scrollSensitivity?r=e(document).scrollTop(e(document).scrollTop()-o.scrollSpeed):e(window).height()-(t.pageY-e(document).scrollTop())<o.scrollSensitivity&&(r=e(document).scrollTop(e(document).scrollTop()+o.scrollSpeed)),t.pageX-e(document).scrollLeft()<o.scrollSensitivity?r=e(document).scrollLeft(e(document).scrollLeft()-o.scrollSpeed):e(window).width()-(t.pageX-e(document).scrollLeft())<o.scrollSensitivity&&(r=e(document).scrollLeft(e(document).scrollLeft()+o.scrollSpeed))),r!==!1&&e.ui.ddmanager&&!o.dropBehaviour&&e.ui.ddmanager.prepareOffsets(this,t)),this.positionAbs=this._convertPositionTo("absolute"),this.options.axis&&"y"===this.options.axis||(this.helper[0].style.left=this.position.left+"px"),this.options.axis&&"x"===this.options.axis||(this.helper[0].style.top=this.position.top+"px"),i=this.items.length-1;i>=0;i--)if(s=this.items[i],n=s.item[0],a=this._intersectsWithPointer(s),a&&s.instance===this.currentContainer&&n!==this.currentItem[0]&&this.placeholder[1===a?"next":"prev"]()[0]!==n&&!e.contains(this.placeholder[0],n)&&("semi-dynamic"===this.options.type?!e.contains(this.element[0],n):!0)){if(this.direction=1===a?"down":"up","pointer"!==this.options.tolerance&&!this._intersectsWithSides(s))break;this._rearrange(t,s),this._trigger("change",t,this._uiHash());break}return this._contactContainers(t),e.ui.ddmanager&&e.ui.ddmanager.drag(this,t),this._trigger("sort",t,this._uiHash()),this.lastPositionAbs=this.positionAbs,!1},_mouseStop:function(t,i){if(t){if(e.ui.ddmanager&&!this.options.dropBehaviour&&e.ui.ddmanager.drop(this,t),this.options.revert){var s=this,n=this.placeholder.offset(),a=this.options.axis,o={};a&&"x"!==a||(o.left=n.left-this.offset.parent.left-this.margins.left+(this.offsetParent[0]===document.body?0:this.offsetParent[0].scrollLeft)),a&&"y"!==a||(o.top=n.top-this.offset.parent.top-this.margins.top+(this.offsetParent[0]===document.body?0:this.offsetParent[0].scrollTop)),this.reverting=!0,e(this.helper).animate(o,parseInt(this.options.revert,10)||500,function(){s._clear(t)})}else this._clear(t,i);return!1}},cancel:function(){if(this.dragging){this._mouseUp({target:null}),"original"===this.options.helper?this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper"):this.currentItem.show();for(var t=this.containers.length-1;t>=0;t--)this.containers[t]._trigger("deactivate",null,this._uiHash(this)),this.containers[t].containerCache.over&&(this.containers[t]._trigger("out",null,this._uiHash(this)),this.containers[t].containerCache.over=0)}return this.placeholder&&(this.placeholder[0].parentNode&&this.placeholder[0].parentNode.removeChild(this.placeholder[0]),"original"!==this.options.helper&&this.helper&&this.helper[0].parentNode&&this.helper.remove(),e.extend(this,{helper:null,dragging:!1,reverting:!1,_noFinalSort:null}),this.domPosition.prev?e(this.domPosition.prev).after(this.currentItem):e(this.domPosition.parent).prepend(this.currentItem)),this},serialize:function(t){var i=this._getItemsAsjQuery(t&&t.connected),s=[];return t=t||{},e(i).each(function(){var i=(e(t.item||this).attr(t.attribute||"id")||"").match(t.expression||/(.+)[\-=_](.+)/);i&&s.push((t.key||i[1]+"[]")+"="+(t.key&&t.expression?i[1]:i[2]))}),!s.length&&t.key&&s.push(t.key+"="),s.join("&")},toArray:function(t){var i=this._getItemsAsjQuery(t&&t.connected),s=[];return t=t||{},i.each(function(){s.push(e(t.item||this).attr(t.attribute||"id")||"")}),s},_intersectsWith:function(e){var t=this.positionAbs.left,i=t+this.helperProportions.width,s=this.positionAbs.top,n=s+this.helperProportions.height,a=e.left,o=a+e.width,r=e.top,h=r+e.height,l=this.offset.click.top,u=this.offset.click.left,d="x"===this.options.axis||s+l>r&&h>s+l,c="y"===this.options.axis||t+u>a&&o>t+u,p=d&&c;return"pointer"===this.options.tolerance||this.options.forcePointerForContainers||"pointer"!==this.options.tolerance&&this.helperProportions[this.floating?"width":"height"]>e[this.floating?"width":"height"]?p:t+this.helperProportions.width/2>a&&o>i-this.helperProportions.width/2&&s+this.helperProportions.height/2>r&&h>n-this.helperProportions.height/2},_intersectsWithPointer:function(e){var t="x"===this.options.axis||this._isOverAxis(this.positionAbs.top+this.offset.click.top,e.top,e.height),i="y"===this.options.axis||this._isOverAxis(this.positionAbs.left+this.offset.click.left,e.left,e.width),s=t&&i,n=this._getDragVerticalDirection(),a=this._getDragHorizontalDirection();return s?this.floating?a&&"right"===a||"down"===n?2:1:n&&("down"===n?2:1):!1},_intersectsWithSides:function(e){var t=this._isOverAxis(this.positionAbs.top+this.offset.click.top,e.top+e.height/2,e.height),i=this._isOverAxis(this.positionAbs.left+this.offset.click.left,e.left+e.width/2,e.width),s=this._getDragVerticalDirection(),n=this._getDragHorizontalDirection();return this.floating&&n?"right"===n&&i||"left"===n&&!i:s&&("down"===s&&t||"up"===s&&!t)},_getDragVerticalDirection:function(){var e=this.positionAbs.top-this.lastPositionAbs.top;return 0!==e&&(e>0?"down":"up")},_getDragHorizontalDirection:function(){var e=this.positionAbs.left-this.lastPositionAbs.left;return 0!==e&&(e>0?"right":"left")},refresh:function(e){return this._refreshItems(e),this._setHandleClassName(),this.refreshPositions(),this},_connectWith:function(){var e=this.options;return e.connectWith.constructor===String?[e.connectWith]:e.connectWith},_getItemsAsjQuery:function(t){function i(){r.push(this)}var s,n,a,o,r=[],h=[],l=this._connectWith();if(l&&t)for(s=l.length-1;s>=0;s--)for(a=e(l[s]),n=a.length-1;n>=0;n--)o=e.data(a[n],this.widgetFullName),o&&o!==this&&!o.options.disabled&&h.push([e.isFunction(o.options.items)?o.options.items.call(o.element):e(o.options.items,o.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),o]);for(h.push([e.isFunction(this.options.items)?this.options.items.call(this.element,null,{options:this.options,item:this.currentItem}):e(this.options.items,this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),this]),s=h.length-1;s>=0;s--)h[s][0].each(i);return e(r)},_removeCurrentsFromItems:function(){var t=this.currentItem.find(":data("+this.widgetName+"-item)");this.items=e.grep(this.items,function(e){for(var i=0;t.length>i;i++)if(t[i]===e.item[0])return!1;return!0})},_refreshItems:function(t){this.items=[],this.containers=[this];var i,s,n,a,o,r,h,l,u=this.items,d=[[e.isFunction(this.options.items)?this.options.items.call(this.element[0],t,{item:this.currentItem}):e(this.options.items,this.element),this]],c=this._connectWith();if(c&&this.ready)for(i=c.length-1;i>=0;i--)for(n=e(c[i]),s=n.length-1;s>=0;s--)a=e.data(n[s],this.widgetFullName),a&&a!==this&&!a.options.disabled&&(d.push([e.isFunction(a.options.items)?a.options.items.call(a.element[0],t,{item:this.currentItem}):e(a.options.items,a.element),a]),this.containers.push(a));for(i=d.length-1;i>=0;i--)for(o=d[i][1],r=d[i][0],s=0,l=r.length;l>s;s++)h=e(r[s]),h.data(this.widgetName+"-item",o),u.push({item:h,instance:o,width:0,height:0,left:0,top:0})},refreshPositions:function(t){this.offsetParent&&this.helper&&(this.offset.parent=this._getParentOffset());var i,s,n,a;for(i=this.items.length-1;i>=0;i--)s=this.items[i],s.instance!==this.currentContainer&&this.currentContainer&&s.item[0]!==this.currentItem[0]||(n=this.options.toleranceElement?e(this.options.toleranceElement,s.item):s.item,t||(s.width=n.outerWidth(),s.height=n.outerHeight()),a=n.offset(),s.left=a.left,s.top=a.top);if(this.options.custom&&this.options.custom.refreshContainers)this.options.custom.refreshContainers.call(this);else for(i=this.containers.length-1;i>=0;i--)a=this.containers[i].element.offset(),this.containers[i].containerCache.left=a.left,this.containers[i].containerCache.top=a.top,this.containers[i].containerCache.width=this.containers[i].element.outerWidth(),this.containers[i].containerCache.height=this.containers[i].element.outerHeight();return this},_createPlaceholder:function(t){t=t||this;var i,s=t.options;s.placeholder&&s.placeholder.constructor!==String||(i=s.placeholder,s.placeholder={element:function(){var s=t.currentItem[0].nodeName.toLowerCase(),n=e("<"+s+">",t.document[0]).addClass(i||t.currentItem[0].className+" ui-sortable-placeholder").removeClass("ui-sortable-helper");return"tr"===s?t.currentItem.children().each(function(){e("<td>&#160;</td>",t.document[0]).attr("colspan",e(this).attr("colspan")||1).appendTo(n)}):"img"===s&&n.attr("src",t.currentItem.attr("src")),i||n.css("visibility","hidden"),n},update:function(e,n){(!i||s.forcePlaceholderSize)&&(n.height()||n.height(t.currentItem.innerHeight()-parseInt(t.currentItem.css("paddingTop")||0,10)-parseInt(t.currentItem.css("paddingBottom")||0,10)),n.width()||n.width(t.currentItem.innerWidth()-parseInt(t.currentItem.css("paddingLeft")||0,10)-parseInt(t.currentItem.css("paddingRight")||0,10)))}}),t.placeholder=e(s.placeholder.element.call(t.element,t.currentItem)),t.currentItem.after(t.placeholder),s.placeholder.update(t,t.placeholder)},_contactContainers:function(t){var i,s,n,a,o,r,h,l,u,d,c=null,p=null;for(i=this.containers.length-1;i>=0;i--)if(!e.contains(this.currentItem[0],this.containers[i].element[0]))if(this._intersectsWith(this.containers[i].containerCache)){if(c&&e.contains(this.containers[i].element[0],c.element[0]))continue;c=this.containers[i],p=i}else this.containers[i].containerCache.over&&(this.containers[i]._trigger("out",t,this._uiHash(this)),this.containers[i].containerCache.over=0);if(c)if(1===this.containers.length)this.containers[p].containerCache.over||(this.containers[p]._trigger("over",t,this._uiHash(this)),this.containers[p].containerCache.over=1);else{for(n=1e4,a=null,u=c.floating||this._isFloating(this.currentItem),o=u?"left":"top",r=u?"width":"height",d=u?"clientX":"clientY",s=this.items.length-1;s>=0;s--)e.contains(this.containers[p].element[0],this.items[s].item[0])&&this.items[s].item[0]!==this.currentItem[0]&&(h=this.items[s].item.offset()[o],l=!1,t[d]-h>this.items[s][r]/2&&(l=!0),n>Math.abs(t[d]-h)&&(n=Math.abs(t[d]-h),a=this.items[s],this.direction=l?"up":"down"));if(!a&&!this.options.dropOnEmpty)return;if(this.currentContainer===this.containers[p])return;a?this._rearrange(t,a,null,!0):this._rearrange(t,null,this.containers[p].element,!0),this._trigger("change",t,this._uiHash()),this.containers[p]._trigger("change",t,this._uiHash(this)),this.currentContainer=this.containers[p],this.options.placeholder.update(this.currentContainer,this.placeholder),this.containers[p]._trigger("over",t,this._uiHash(this)),this.containers[p].containerCache.over=1}},_createHelper:function(t){var i=this.options,s=e.isFunction(i.helper)?e(i.helper.apply(this.element[0],[t,this.currentItem])):"clone"===i.helper?this.currentItem.clone():this.currentItem;return s.parents("body").length||e("parent"!==i.appendTo?i.appendTo:this.currentItem[0].parentNode)[0].appendChild(s[0]),s[0]===this.currentItem[0]&&(this._storedCSS={width:this.currentItem[0].style.width,height:this.currentItem[0].style.height,position:this.currentItem.css("position"),top:this.currentItem.css("top"),left:this.currentItem.css("left")}),(!s[0].style.width||i.forceHelperSize)&&s.width(this.currentItem.width()),(!s[0].style.height||i.forceHelperSize)&&s.height(this.currentItem.height()),s},_adjustOffsetFromHelper:function(t){"string"==typeof t&&(t=t.split(" ")),e.isArray(t)&&(t={left:+t[0],top:+t[1]||0}),"left"in t&&(this.offset.click.left=t.left+this.margins.left),"right"in t&&(this.offset.click.left=this.helperProportions.width-t.right+this.margins.left),"top"in t&&(this.offset.click.top=t.top+this.margins.top),"bottom"in t&&(this.offset.click.top=this.helperProportions.height-t.bottom+this.margins.top)},_getParentOffset:function(){this.offsetParent=this.helper.offsetParent();var t=this.offsetParent.offset();return"absolute"===this.cssPosition&&this.scrollParent[0]!==document&&e.contains(this.scrollParent[0],this.offsetParent[0])&&(t.left+=this.scrollParent.scrollLeft(),t.top+=this.scrollParent.scrollTop()),(this.offsetParent[0]===document.body||this.offsetParent[0].tagName&&"html"===this.offsetParent[0].tagName.toLowerCase()&&e.ui.ie)&&(t={top:0,left:0}),{top:t.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:t.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}},_getRelativeOffset:function(){if("relative"===this.cssPosition){var e=this.currentItem.position();return{top:e.top-(parseInt(this.helper.css("top"),10)||0)+this.scrollParent.scrollTop(),left:e.left-(parseInt(this.helper.css("left"),10)||0)+this.scrollParent.scrollLeft()}}return{top:0,left:0}},_cacheMargins:function(){this.margins={left:parseInt(this.currentItem.css("marginLeft"),10)||0,top:parseInt(this.currentItem.css("marginTop"),10)||0}},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}},_setContainment:function(){var t,i,s,n=this.options;"parent"===n.containment&&(n.containment=this.helper[0].parentNode),("document"===n.containment||"window"===n.containment)&&(this.containment=[0-this.offset.relative.left-this.offset.parent.left,0-this.offset.relative.top-this.offset.parent.top,e("document"===n.containment?document:window).width()-this.helperProportions.width-this.margins.left,(e("document"===n.containment?document:window).height()||document.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top]),/^(document|window|parent)$/.test(n.containment)||(t=e(n.containment)[0],i=e(n.containment).offset(),s="hidden"!==e(t).css("overflow"),this.containment=[i.left+(parseInt(e(t).css("borderLeftWidth"),10)||0)+(parseInt(e(t).css("paddingLeft"),10)||0)-this.margins.left,i.top+(parseInt(e(t).css("borderTopWidth"),10)||0)+(parseInt(e(t).css("paddingTop"),10)||0)-this.margins.top,i.left+(s?Math.max(t.scrollWidth,t.offsetWidth):t.offsetWidth)-(parseInt(e(t).css("borderLeftWidth"),10)||0)-(parseInt(e(t).css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left,i.top+(s?Math.max(t.scrollHeight,t.offsetHeight):t.offsetHeight)-(parseInt(e(t).css("borderTopWidth"),10)||0)-(parseInt(e(t).css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top])},_convertPositionTo:function(t,i){i||(i=this.position);var s="absolute"===t?1:-1,n="absolute"!==this.cssPosition||this.scrollParent[0]!==document&&e.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent,a=/(html|body)/i.test(n[0].tagName);return{top:i.top+this.offset.relative.top*s+this.offset.parent.top*s-("fixed"===this.cssPosition?-this.scrollParent.scrollTop():a?0:n.scrollTop())*s,left:i.left+this.offset.relative.left*s+this.offset.parent.left*s-("fixed"===this.cssPosition?-this.scrollParent.scrollLeft():a?0:n.scrollLeft())*s}},_generatePosition:function(t){var i,s,n=this.options,a=t.pageX,o=t.pageY,r="absolute"!==this.cssPosition||this.scrollParent[0]!==document&&e.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent,h=/(html|body)/i.test(r[0].tagName);return"relative"!==this.cssPosition||this.scrollParent[0]!==document&&this.scrollParent[0]!==this.offsetParent[0]||(this.offset.relative=this._getRelativeOffset()),this.originalPosition&&(this.containment&&(t.pageX-this.offset.click.left<this.containment[0]&&(a=this.containment[0]+this.offset.click.left),t.pageY-this.offset.click.top<this.containment[1]&&(o=this.containment[1]+this.offset.click.top),t.pageX-this.offset.click.left>this.containment[2]&&(a=this.containment[2]+this.offset.click.left),t.pageY-this.offset.click.top>this.containment[3]&&(o=this.containment[3]+this.offset.click.top)),n.grid&&(i=this.originalPageY+Math.round((o-this.originalPageY)/n.grid[1])*n.grid[1],o=this.containment?i-this.offset.click.top>=this.containment[1]&&i-this.offset.click.top<=this.containment[3]?i:i-this.offset.click.top>=this.containment[1]?i-n.grid[1]:i+n.grid[1]:i,s=this.originalPageX+Math.round((a-this.originalPageX)/n.grid[0])*n.grid[0],a=this.containment?s-this.offset.click.left>=this.containment[0]&&s-this.offset.click.left<=this.containment[2]?s:s-this.offset.click.left>=this.containment[0]?s-n.grid[0]:s+n.grid[0]:s)),{top:o-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+("fixed"===this.cssPosition?-this.scrollParent.scrollTop():h?0:r.scrollTop()),left:a-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+("fixed"===this.cssPosition?-this.scrollParent.scrollLeft():h?0:r.scrollLeft())}},_rearrange:function(e,t,i,s){i?i[0].appendChild(this.placeholder[0]):t.item[0].parentNode.insertBefore(this.placeholder[0],"down"===this.direction?t.item[0]:t.item[0].nextSibling),this.counter=this.counter?++this.counter:1;var n=this.counter;this._delay(function(){n===this.counter&&this.refreshPositions(!s)})},_clear:function(e,t){function i(e,t,i){return function(s){i._trigger(e,s,t._uiHash(t))}}this.reverting=!1;var s,n=[];if(!this._noFinalSort&&this.currentItem.parent().length&&this.placeholder.before(this.currentItem),this._noFinalSort=null,this.helper[0]===this.currentItem[0]){for(s in this._storedCSS)("auto"===this._storedCSS[s]||"static"===this._storedCSS[s])&&(this._storedCSS[s]="");this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper")}else this.currentItem.show();for(this.fromOutside&&!t&&n.push(function(e){this._trigger("receive",e,this._uiHash(this.fromOutside))}),!this.fromOutside&&this.domPosition.prev===this.currentItem.prev().not(".ui-sortable-helper")[0]&&this.domPosition.parent===this.currentItem.parent()[0]||t||n.push(function(e){this._trigger("update",e,this._uiHash())}),this!==this.currentContainer&&(t||(n.push(function(e){this._trigger("remove",e,this._uiHash())}),n.push(function(e){return function(t){e._trigger("receive",t,this._uiHash(this))}}.call(this,this.currentContainer)),n.push(function(e){return function(t){e._trigger("update",t,this._uiHash(this))}}.call(this,this.currentContainer)))),s=this.containers.length-1;s>=0;s--)t||n.push(i("deactivate",this,this.containers[s])),this.containers[s].containerCache.over&&(n.push(i("out",this,this.containers[s])),this.containers[s].containerCache.over=0);if(this.storedCursor&&(this.document.find("body").css("cursor",this.storedCursor),this.storedStylesheet.remove()),this._storedOpacity&&this.helper.css("opacity",this._storedOpacity),this._storedZIndex&&this.helper.css("zIndex","auto"===this._storedZIndex?"":this._storedZIndex),this.dragging=!1,this.cancelHelperRemoval){if(!t){for(this._trigger("beforeStop",e,this._uiHash()),s=0;n.length>s;s++)n[s].call(this,e);this._trigger("stop",e,this._uiHash())}return this.fromOutside=!1,!1}if(t||this._trigger("beforeStop",e,this._uiHash()),this.placeholder[0].parentNode.removeChild(this.placeholder[0]),this.helper[0]!==this.currentItem[0]&&this.helper.remove(),this.helper=null,!t){for(s=0;n.length>s;s++)n[s].call(this,e);this._trigger("stop",e,this._uiHash())}return this.fromOutside=!1,!0},_trigger:function(){e.Widget.prototype._trigger.apply(this,arguments)===!1&&this.cancel()},_uiHash:function(t){var i=t||this;return{helper:i.helper,placeholder:i.placeholder||e([]),position:i.position,originalPosition:i.originalPosition,offset:i.positionAbs,item:i.currentItem,sender:t?t.element:null}}}),e.widget("ui.spinner",{version:"1.11.0",defaultElement:"<input>",widgetEventPrefix:"spin",options:{culture:null,icons:{down:"ui-icon-triangle-1-s",up:"ui-icon-triangle-1-n"},incremental:!0,max:null,min:null,numberFormat:null,page:10,step:1,change:null,spin:null,start:null,stop:null},_create:function(){this._setOption("max",this.options.max),this._setOption("min",this.options.min),this._setOption("step",this.options.step),""!==this.value()&&this._value(this.element.val(),!0),this._draw(),this._on(this._events),this._refresh(),this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete")}})},_getCreateOptions:function(){var t={},i=this.element;return e.each(["min","max","step"],function(e,s){var n=i.attr(s);void 0!==n&&n.length&&(t[s]=n)}),t},_events:{keydown:function(e){this._start(e)&&this._keydown(e)&&e.preventDefault()},keyup:"_stop",focus:function(){this.previous=this.element.val()},blur:function(e){return this.cancelBlur?(delete this.cancelBlur,void 0):(this._stop(),this._refresh(),this.previous!==this.element.val()&&this._trigger("change",e),void 0)},mousewheel:function(e,t){if(t){if(!this.spinning&&!this._start(e))return!1;this._spin((t>0?1:-1)*this.options.step,e),clearTimeout(this.mousewheelTimer),this.mousewheelTimer=this._delay(function(){this.spinning&&this._stop(e)},100),e.preventDefault()}},"mousedown .ui-spinner-button":function(t){function i(){var e=this.element[0]===this.document[0].activeElement;e||(this.element.focus(),this.previous=s,this._delay(function(){this.previous=s}))}var s;s=this.element[0]===this.document[0].activeElement?this.previous:this.element.val(),t.preventDefault(),i.call(this),this.cancelBlur=!0,this._delay(function(){delete this.cancelBlur,i.call(this)}),this._start(t)!==!1&&this._repeat(null,e(t.currentTarget).hasClass("ui-spinner-up")?1:-1,t)},"mouseup .ui-spinner-button":"_stop","mouseenter .ui-spinner-button":function(t){return e(t.currentTarget).hasClass("ui-state-active")?this._start(t)===!1?!1:(this._repeat(null,e(t.currentTarget).hasClass("ui-spinner-up")?1:-1,t),void 0):void 0},"mouseleave .ui-spinner-button":"_stop"},_draw:function(){var e=this.uiSpinner=this.element.addClass("ui-spinner-input").attr("autocomplete","off").wrap(this._uiSpinnerHtml()).parent().append(this._buttonHtml());this.element.attr("role","spinbutton"),this.buttons=e.find(".ui-spinner-button").attr("tabIndex",-1).button().removeClass("ui-corner-all"),this.buttons.height()>Math.ceil(.5*e.height())&&e.height()>0&&e.height(e.height()),this.options.disabled&&this.disable()},_keydown:function(t){var i=this.options,s=e.ui.keyCode;switch(t.keyCode){case s.UP:return this._repeat(null,1,t),!0;case s.DOWN:return this._repeat(null,-1,t),!0;case s.PAGE_UP:return this._repeat(null,i.page,t),!0;case s.PAGE_DOWN:return this._repeat(null,-i.page,t),!0}return!1},_uiSpinnerHtml:function(){return"<span class='ui-spinner ui-widget ui-widget-content ui-corner-all'></span>"},_buttonHtml:function(){return"<a class='ui-spinner-button ui-spinner-up ui-corner-tr'><span class='ui-icon "+this.options.icons.up+"'>&#9650;</span>"+"</a>"+"<a class='ui-spinner-button ui-spinner-down ui-corner-br'>"+"<span class='ui-icon "+this.options.icons.down+"'>&#9660;</span>"+"</a>"},_start:function(e){return this.spinning||this._trigger("start",e)!==!1?(this.counter||(this.counter=1),this.spinning=!0,!0):!1},_repeat:function(e,t,i){e=e||500,clearTimeout(this.timer),this.timer=this._delay(function(){this._repeat(40,t,i)},e),this._spin(t*this.options.step,i)},_spin:function(e,t){var i=this.value()||0;this.counter||(this.counter=1),i=this._adjustValue(i+e*this._increment(this.counter)),this.spinning&&this._trigger("spin",t,{value:i})===!1||(this._value(i),this.counter++)},_increment:function(t){var i=this.options.incremental;return i?e.isFunction(i)?i(t):Math.floor(t*t*t/5e4-t*t/500+17*t/200+1):1},_precision:function(){var e=this._precisionOf(this.options.step);return null!==this.options.min&&(e=Math.max(e,this._precisionOf(this.options.min))),e},_precisionOf:function(e){var t=""+e,i=t.indexOf(".");return-1===i?0:t.length-i-1},_adjustValue:function(e){var t,i,s=this.options;return t=null!==s.min?s.min:0,i=e-t,i=Math.round(i/s.step)*s.step,e=t+i,e=parseFloat(e.toFixed(this._precision())),null!==s.max&&e>s.max?s.max:null!==s.min&&s.min>e?s.min:e},_stop:function(e){this.spinning&&(clearTimeout(this.timer),clearTimeout(this.mousewheelTimer),this.counter=0,this.spinning=!1,this._trigger("stop",e))},_setOption:function(e,t){if("culture"===e||"numberFormat"===e){var i=this._parse(this.element.val());return this.options[e]=t,this.element.val(this._format(i)),void 0}("max"===e||"min"===e||"step"===e)&&"string"==typeof t&&(t=this._parse(t)),"icons"===e&&(this.buttons.first().find(".ui-icon").removeClass(this.options.icons.up).addClass(t.up),this.buttons.last().find(".ui-icon").removeClass(this.options.icons.down).addClass(t.down)),this._super(e,t),"disabled"===e&&(this.widget().toggleClass("ui-state-disabled",!!t),this.element.prop("disabled",!!t),this.buttons.button(t?"disable":"enable"))},_setOptions:r(function(e){this._super(e)}),_parse:function(e){return"string"==typeof e&&""!==e&&(e=window.Globalize&&this.options.numberFormat?Globalize.parseFloat(e,10,this.options.culture):+e),""===e||isNaN(e)?null:e},_format:function(e){return""===e?"":window.Globalize&&this.options.numberFormat?Globalize.format(e,this.options.numberFormat,this.options.culture):e},_refresh:function(){this.element.attr({"aria-valuemin":this.options.min,"aria-valuemax":this.options.max,"aria-valuenow":this._parse(this.element.val())})},isValid:function(){var e=this.value();return null===e?!1:e===this._adjustValue(e)},_value:function(e,t){var i;""!==e&&(i=this._parse(e),null!==i&&(t||(i=this._adjustValue(i)),e=this._format(i))),this.element.val(e),this._refresh()},_destroy:function(){this.element.removeClass("ui-spinner-input").prop("disabled",!1).removeAttr("autocomplete").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"),this.uiSpinner.replaceWith(this.element)},stepUp:r(function(e){this._stepUp(e)}),_stepUp:function(e){this._start()&&(this._spin((e||1)*this.options.step),this._stop())},stepDown:r(function(e){this._stepDown(e)}),_stepDown:function(e){this._start()&&(this._spin((e||1)*-this.options.step),this._stop())},pageUp:r(function(e){this._stepUp((e||1)*this.options.page)}),pageDown:r(function(e){this._stepDown((e||1)*this.options.page)}),value:function(e){return arguments.length?(r(this._value).call(this,e),void 0):this._parse(this.element.val())},widget:function(){return this.uiSpinner}}),e.widget("ui.tabs",{version:"1.11.0",delay:300,options:{active:null,collapsible:!1,event:"click",heightStyle:"content",hide:null,show:null,activate:null,beforeActivate:null,beforeLoad:null,load:null},_isLocal:function(){var e=/#.*$/;return function(t){var i,s;t=t.cloneNode(!1),i=t.href.replace(e,""),s=location.href.replace(e,"");try{i=decodeURIComponent(i)}catch(n){}try{s=decodeURIComponent(s)}catch(n){}return t.hash.length>1&&i===s}}(),_create:function(){var t=this,i=this.options;this.running=!1,this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all").toggleClass("ui-tabs-collapsible",i.collapsible).delegate(".ui-tabs-nav > li","mousedown"+this.eventNamespace,function(t){e(this).is(".ui-state-disabled")&&t.preventDefault()}).delegate(".ui-tabs-anchor","focus"+this.eventNamespace,function(){e(this).closest("li").is(".ui-state-disabled")&&this.blur()}),this._processTabs(),i.active=this._initialActive(),e.isArray(i.disabled)&&(i.disabled=e.unique(i.disabled.concat(e.map(this.tabs.filter(".ui-state-disabled"),function(e){return t.tabs.index(e)}))).sort()),this.active=this.options.active!==!1&&this.anchors.length?this._findActive(i.active):e(),this._refresh(),this.active.length&&this.load(i.active)},_initialActive:function(){var t=this.options.active,i=this.options.collapsible,s=location.hash.substring(1);return null===t&&(s&&this.tabs.each(function(i,n){return e(n).attr("aria-controls")===s?(t=i,!1):void 0}),null===t&&(t=this.tabs.index(this.tabs.filter(".ui-tabs-active"))),(null===t||-1===t)&&(t=this.tabs.length?0:!1)),t!==!1&&(t=this.tabs.index(this.tabs.eq(t)),-1===t&&(t=i?!1:0)),!i&&t===!1&&this.anchors.length&&(t=0),t},_getCreateEventData:function(){return{tab:this.active,panel:this.active.length?this._getPanelForTab(this.active):e()}},_tabKeydown:function(t){var i=e(this.document[0].activeElement).closest("li"),s=this.tabs.index(i),n=!0;if(!this._handlePageNav(t)){switch(t.keyCode){case e.ui.keyCode.RIGHT:case e.ui.keyCode.DOWN:s++;break;case e.ui.keyCode.UP:case e.ui.keyCode.LEFT:n=!1,s--;break;case e.ui.keyCode.END:s=this.anchors.length-1;break;case e.ui.keyCode.HOME:s=0;break;case e.ui.keyCode.SPACE:return t.preventDefault(),clearTimeout(this.activating),this._activate(s),void 0;case e.ui.keyCode.ENTER:return t.preventDefault(),clearTimeout(this.activating),this._activate(s===this.options.active?!1:s),void 0;default:return}t.preventDefault(),clearTimeout(this.activating),s=this._focusNextTab(s,n),t.ctrlKey||(i.attr("aria-selected","false"),this.tabs.eq(s).attr("aria-selected","true"),this.activating=this._delay(function(){this.option("active",s)},this.delay))}},_panelKeydown:function(t){this._handlePageNav(t)||t.ctrlKey&&t.keyCode===e.ui.keyCode.UP&&(t.preventDefault(),this.active.focus())},_handlePageNav:function(t){return t.altKey&&t.keyCode===e.ui.keyCode.PAGE_UP?(this._activate(this._focusNextTab(this.options.active-1,!1)),!0):t.altKey&&t.keyCode===e.ui.keyCode.PAGE_DOWN?(this._activate(this._focusNextTab(this.options.active+1,!0)),!0):void 0},_findNextTab:function(t,i){function s(){return t>n&&(t=0),0>t&&(t=n),t}for(var n=this.tabs.length-1;-1!==e.inArray(s(),this.options.disabled);)t=i?t+1:t-1;return t},_focusNextTab:function(e,t){return e=this._findNextTab(e,t),this.tabs.eq(e).focus(),e},_setOption:function(e,t){return"active"===e?(this._activate(t),void 0):"disabled"===e?(this._setupDisabled(t),void 0):(this._super(e,t),"collapsible"===e&&(this.element.toggleClass("ui-tabs-collapsible",t),t||this.options.active!==!1||this._activate(0)),"event"===e&&this._setupEvents(t),"heightStyle"===e&&this._setupHeightStyle(t),void 0)},_sanitizeSelector:function(e){return e?e.replace(/[!"$%&'()*+,.\/:;<=>?@\[\]\^`{|}~]/g,"\\$&"):""},refresh:function(){var t=this.options,i=this.tablist.children(":has(a[href])");t.disabled=e.map(i.filter(".ui-state-disabled"),function(e){return i.index(e)}),this._processTabs(),t.active!==!1&&this.anchors.length?this.active.length&&!e.contains(this.tablist[0],this.active[0])?this.tabs.length===t.disabled.length?(t.active=!1,this.active=e()):this._activate(this._findNextTab(Math.max(0,t.active-1),!1)):t.active=this.tabs.index(this.active):(t.active=!1,this.active=e()),this._refresh()},_refresh:function(){this._setupDisabled(this.options.disabled),this._setupEvents(this.options.event),this._setupHeightStyle(this.options.heightStyle),this.tabs.not(this.active).attr({"aria-selected":"false","aria-expanded":"false",tabIndex:-1}),this.panels.not(this._getPanelForTab(this.active)).hide().attr({"aria-hidden":"true"}),this.active.length?(this.active.addClass("ui-tabs-active ui-state-active").attr({"aria-selected":"true","aria-expanded":"true",tabIndex:0}),this._getPanelForTab(this.active).show().attr({"aria-hidden":"false"})):this.tabs.eq(0).attr("tabIndex",0)},_processTabs:function(){var t=this;this.tablist=this._getList().addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").attr("role","tablist"),this.tabs=this.tablist.find("> li:has(a[href])").addClass("ui-state-default ui-corner-top").attr({role:"tab",tabIndex:-1}),this.anchors=this.tabs.map(function(){return e("a",this)[0]
}).addClass("ui-tabs-anchor").attr({role:"presentation",tabIndex:-1}),this.panels=e(),this.anchors.each(function(i,s){var n,a,o,r=e(s).uniqueId().attr("id"),h=e(s).closest("li"),l=h.attr("aria-controls");t._isLocal(s)?(n=s.hash,o=n.substring(1),a=t.element.find(t._sanitizeSelector(n))):(o=h.attr("aria-controls")||e({}).uniqueId()[0].id,n="#"+o,a=t.element.find(n),a.length||(a=t._createPanel(o),a.insertAfter(t.panels[i-1]||t.tablist)),a.attr("aria-live","polite")),a.length&&(t.panels=t.panels.add(a)),l&&h.data("ui-tabs-aria-controls",l),h.attr({"aria-controls":o,"aria-labelledby":r}),a.attr("aria-labelledby",r)}),this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").attr("role","tabpanel")},_getList:function(){return this.tablist||this.element.find("ol,ul").eq(0)},_createPanel:function(t){return e("<div>").attr("id",t).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").data("ui-tabs-destroy",!0)},_setupDisabled:function(t){e.isArray(t)&&(t.length?t.length===this.anchors.length&&(t=!0):t=!1);for(var i,s=0;i=this.tabs[s];s++)t===!0||-1!==e.inArray(s,t)?e(i).addClass("ui-state-disabled").attr("aria-disabled","true"):e(i).removeClass("ui-state-disabled").removeAttr("aria-disabled");this.options.disabled=t},_setupEvents:function(t){var i={};t&&e.each(t.split(" "),function(e,t){i[t]="_eventHandler"}),this._off(this.anchors.add(this.tabs).add(this.panels)),this._on(!0,this.anchors,{click:function(e){e.preventDefault()}}),this._on(this.anchors,i),this._on(this.tabs,{keydown:"_tabKeydown"}),this._on(this.panels,{keydown:"_panelKeydown"}),this._focusable(this.tabs),this._hoverable(this.tabs)},_setupHeightStyle:function(t){var i,s=this.element.parent();"fill"===t?(i=s.height(),i-=this.element.outerHeight()-this.element.height(),this.element.siblings(":visible").each(function(){var t=e(this),s=t.css("position");"absolute"!==s&&"fixed"!==s&&(i-=t.outerHeight(!0))}),this.element.children().not(this.panels).each(function(){i-=e(this).outerHeight(!0)}),this.panels.each(function(){e(this).height(Math.max(0,i-e(this).innerHeight()+e(this).height()))}).css("overflow","auto")):"auto"===t&&(i=0,this.panels.each(function(){i=Math.max(i,e(this).height("").height())}).height(i))},_eventHandler:function(t){var i=this.options,s=this.active,n=e(t.currentTarget),a=n.closest("li"),o=a[0]===s[0],r=o&&i.collapsible,h=r?e():this._getPanelForTab(a),l=s.length?this._getPanelForTab(s):e(),u={oldTab:s,oldPanel:l,newTab:r?e():a,newPanel:h};t.preventDefault(),a.hasClass("ui-state-disabled")||a.hasClass("ui-tabs-loading")||this.running||o&&!i.collapsible||this._trigger("beforeActivate",t,u)===!1||(i.active=r?!1:this.tabs.index(a),this.active=o?e():a,this.xhr&&this.xhr.abort(),l.length||h.length||e.error("jQuery UI Tabs: Mismatching fragment identifier."),h.length&&this.load(this.tabs.index(a),t),this._toggle(t,u))},_toggle:function(t,i){function s(){a.running=!1,a._trigger("activate",t,i)}function n(){i.newTab.closest("li").addClass("ui-tabs-active ui-state-active"),o.length&&a.options.show?a._show(o,a.options.show,s):(o.show(),s())}var a=this,o=i.newPanel,r=i.oldPanel;this.running=!0,r.length&&this.options.hide?this._hide(r,this.options.hide,function(){i.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"),n()}):(i.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"),r.hide(),n()),r.attr("aria-hidden","true"),i.oldTab.attr({"aria-selected":"false","aria-expanded":"false"}),o.length&&r.length?i.oldTab.attr("tabIndex",-1):o.length&&this.tabs.filter(function(){return 0===e(this).attr("tabIndex")}).attr("tabIndex",-1),o.attr("aria-hidden","false"),i.newTab.attr({"aria-selected":"true","aria-expanded":"true",tabIndex:0})},_activate:function(t){var i,s=this._findActive(t);s[0]!==this.active[0]&&(s.length||(s=this.active),i=s.find(".ui-tabs-anchor")[0],this._eventHandler({target:i,currentTarget:i,preventDefault:e.noop}))},_findActive:function(t){return t===!1?e():this.tabs.eq(t)},_getIndex:function(e){return"string"==typeof e&&(e=this.anchors.index(this.anchors.filter("[href$='"+e+"']"))),e},_destroy:function(){this.xhr&&this.xhr.abort(),this.element.removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible"),this.tablist.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").removeAttr("role"),this.anchors.removeClass("ui-tabs-anchor").removeAttr("role").removeAttr("tabIndex").removeUniqueId(),this.tabs.add(this.panels).each(function(){e.data(this,"ui-tabs-destroy")?e(this).remove():e(this).removeClass("ui-state-default ui-state-active ui-state-disabled ui-corner-top ui-corner-bottom ui-widget-content ui-tabs-active ui-tabs-panel").removeAttr("tabIndex").removeAttr("aria-live").removeAttr("aria-busy").removeAttr("aria-selected").removeAttr("aria-labelledby").removeAttr("aria-hidden").removeAttr("aria-expanded").removeAttr("role")}),this.tabs.each(function(){var t=e(this),i=t.data("ui-tabs-aria-controls");i?t.attr("aria-controls",i).removeData("ui-tabs-aria-controls"):t.removeAttr("aria-controls")}),this.panels.show(),"content"!==this.options.heightStyle&&this.panels.css("height","")},enable:function(t){var i=this.options.disabled;i!==!1&&(void 0===t?i=!1:(t=this._getIndex(t),i=e.isArray(i)?e.map(i,function(e){return e!==t?e:null}):e.map(this.tabs,function(e,i){return i!==t?i:null})),this._setupDisabled(i))},disable:function(t){var i=this.options.disabled;if(i!==!0){if(void 0===t)i=!0;else{if(t=this._getIndex(t),-1!==e.inArray(t,i))return;i=e.isArray(i)?e.merge([t],i).sort():[t]}this._setupDisabled(i)}},load:function(t,i){t=this._getIndex(t);var s=this,n=this.tabs.eq(t),a=n.find(".ui-tabs-anchor"),o=this._getPanelForTab(n),r={tab:n,panel:o};this._isLocal(a[0])||(this.xhr=e.ajax(this._ajaxSettings(a,i,r)),this.xhr&&"canceled"!==this.xhr.statusText&&(n.addClass("ui-tabs-loading"),o.attr("aria-busy","true"),this.xhr.success(function(e){setTimeout(function(){o.html(e),s._trigger("load",i,r)},1)}).complete(function(e,t){setTimeout(function(){"abort"===t&&s.panels.stop(!1,!0),n.removeClass("ui-tabs-loading"),o.removeAttr("aria-busy"),e===s.xhr&&delete s.xhr},1)})))},_ajaxSettings:function(t,i,s){var n=this;return{url:t.attr("href"),beforeSend:function(t,a){return n._trigger("beforeLoad",i,e.extend({jqXHR:t,ajaxSettings:a},s))}}},_getPanelForTab:function(t){var i=e(t).attr("aria-controls");return this.element.find(this._sanitizeSelector("#"+i))}}),e.widget("ui.tooltip",{version:"1.11.0",options:{content:function(){var t=e(this).attr("title")||"";return e("<a>").text(t).html()},hide:!0,items:"[title]:not([disabled])",position:{my:"left top+15",at:"left bottom",collision:"flipfit flip"},show:!0,tooltipClass:null,track:!1,close:null,open:null},_addDescribedBy:function(t,i){var s=(t.attr("aria-describedby")||"").split(/\s+/);s.push(i),t.data("ui-tooltip-id",i).attr("aria-describedby",e.trim(s.join(" ")))},_removeDescribedBy:function(t){var i=t.data("ui-tooltip-id"),s=(t.attr("aria-describedby")||"").split(/\s+/),n=e.inArray(i,s);-1!==n&&s.splice(n,1),t.removeData("ui-tooltip-id"),s=e.trim(s.join(" ")),s?t.attr("aria-describedby",s):t.removeAttr("aria-describedby")},_create:function(){this._on({mouseover:"open",focusin:"open"}),this.tooltips={},this.parents={},this.options.disabled&&this._disable(),this.liveRegion=e("<div>").attr({role:"log","aria-live":"assertive","aria-relevant":"additions"}).addClass("ui-helper-hidden-accessible").appendTo(this.document[0].body)},_setOption:function(t,i){var s=this;return"disabled"===t?(this[i?"_disable":"_enable"](),this.options[t]=i,void 0):(this._super(t,i),"content"===t&&e.each(this.tooltips,function(e,t){s._updateContent(t)}),void 0)},_disable:function(){var t=this;e.each(this.tooltips,function(i,s){var n=e.Event("blur");n.target=n.currentTarget=s[0],t.close(n,!0)}),this.element.find(this.options.items).addBack().each(function(){var t=e(this);t.is("[title]")&&t.data("ui-tooltip-title",t.attr("title")).removeAttr("title")})},_enable:function(){this.element.find(this.options.items).addBack().each(function(){var t=e(this);t.data("ui-tooltip-title")&&t.attr("title",t.data("ui-tooltip-title"))})},open:function(t){var i=this,s=e(t?t.target:this.element).closest(this.options.items);s.length&&!s.data("ui-tooltip-id")&&(s.attr("title")&&s.data("ui-tooltip-title",s.attr("title")),s.data("ui-tooltip-open",!0),t&&"mouseover"===t.type&&s.parents().each(function(){var t,s=e(this);s.data("ui-tooltip-open")&&(t=e.Event("blur"),t.target=t.currentTarget=this,i.close(t,!0)),s.attr("title")&&(s.uniqueId(),i.parents[this.id]={element:this,title:s.attr("title")},s.attr("title",""))}),this._updateContent(s,t))},_updateContent:function(e,t){var i,s=this.options.content,n=this,a=t?t.type:null;return"string"==typeof s?this._open(t,e,s):(i=s.call(e[0],function(i){e.data("ui-tooltip-open")&&n._delay(function(){t&&(t.type=a),this._open(t,e,i)})}),i&&this._open(t,e,i),void 0)},_open:function(t,i,s){function n(e){l.of=e,a.is(":hidden")||a.position(l)}var a,o,r,h,l=e.extend({},this.options.position);if(s){if(a=this._find(i),a.length)return a.find(".ui-tooltip-content").html(s),void 0;i.is("[title]")&&(t&&"mouseover"===t.type?i.attr("title",""):i.removeAttr("title")),a=this._tooltip(i),this._addDescribedBy(i,a.attr("id")),a.find(".ui-tooltip-content").html(s),this.liveRegion.children().hide(),s.clone?(h=s.clone(),h.removeAttr("id").find("[id]").removeAttr("id")):h=s,e("<div>").html(h).appendTo(this.liveRegion),this.options.track&&t&&/^mouse/.test(t.type)?(this._on(this.document,{mousemove:n}),n(t)):a.position(e.extend({of:i},this.options.position)),a.hide(),this._show(a,this.options.show),this.options.show&&this.options.show.delay&&(r=this.delayedShow=setInterval(function(){a.is(":visible")&&(n(l.of),clearInterval(r))},e.fx.interval)),this._trigger("open",t,{tooltip:a}),o={keyup:function(t){if(t.keyCode===e.ui.keyCode.ESCAPE){var s=e.Event(t);s.currentTarget=i[0],this.close(s,!0)}}},i[0]!==this.element[0]&&(o.remove=function(){this._removeTooltip(a)}),t&&"mouseover"!==t.type||(o.mouseleave="close"),t&&"focusin"!==t.type||(o.focusout="close"),this._on(!0,i,o)}},close:function(t){var i=this,s=e(t?t.currentTarget:this.element),n=this._find(s);this.closing||(clearInterval(this.delayedShow),s.data("ui-tooltip-title")&&!s.attr("title")&&s.attr("title",s.data("ui-tooltip-title")),this._removeDescribedBy(s),n.stop(!0),this._hide(n,this.options.hide,function(){i._removeTooltip(e(this))}),s.removeData("ui-tooltip-open"),this._off(s,"mouseleave focusout keyup"),s[0]!==this.element[0]&&this._off(s,"remove"),this._off(this.document,"mousemove"),t&&"mouseleave"===t.type&&e.each(this.parents,function(t,s){e(s.element).attr("title",s.title),delete i.parents[t]}),this.closing=!0,this._trigger("close",t,{tooltip:n}),this.closing=!1)},_tooltip:function(t){var i=e("<div>").attr("role","tooltip").addClass("ui-tooltip ui-widget ui-corner-all ui-widget-content "+(this.options.tooltipClass||"")),s=i.uniqueId().attr("id");return e("<div>").addClass("ui-tooltip-content").appendTo(i),i.appendTo(this.document[0].body),this.tooltips[s]=t,i},_find:function(t){var i=t.data("ui-tooltip-id");return i?e("#"+i):e()},_removeTooltip:function(e){e.remove(),delete this.tooltips[e.attr("id")]},_destroy:function(){var t=this;e.each(this.tooltips,function(i,s){var n=e.Event("blur");n.target=n.currentTarget=s[0],t.close(n,!0),e("#"+i).remove(),s.data("ui-tooltip-title")&&(s.attr("title")||s.attr("title",s.data("ui-tooltip-title")),s.removeData("ui-tooltip-title"))}),this.liveRegion.remove()}})});
/* German initialisation for the jQuery UI date picker plugin. */
/* Written by Milian Wolff (mail@milianw.de). */
( function( factory ) {
	if ( typeof define === "function" && define.amd ) {

		// AMD. Register as an anonymous module.
		define( [ "../widgets/datepicker" ], factory );
	} else {

		// Browser globals
		factory( jQuery.datepicker );
	}
}( function( datepicker ) {

datepicker.regional.de = {
	closeText: js_localize['calendar.closeText'],
	prevText: js_localize['calendar.prevText'],
	nextText: js_localize['calendar.nextText'],
	currentText: js_localize['calendar.currentText'],
	monthNames: [ js_localize['calendar.monthNames.1'],js_localize['calendar.monthNames.2'],js_localize['calendar.monthNames.3'],js_localize['calendar.monthNames.4'],
		js_localize['calendar.monthNames.5'],js_localize['calendar.monthNames.6'],js_localize['calendar.monthNames.7'],js_localize['calendar.monthNames.8'],
		js_localize['calendar.monthNames.9'],js_localize['calendar.monthNames.10'],js_localize['calendar.monthNames.11'],js_localize['calendar.monthNames.12'] ],
	monthNamesShort: [ js_localize['calendar.monthNamesShort.1'],js_localize['calendar.monthNamesShort.2'],js_localize['calendar.monthNamesShort.3'],js_localize['calendar.monthNamesShort.4'],
		js_localize['calendar.monthNamesShort.5'],js_localize['calendar.monthNamesShort.6'],js_localize['calendar.monthNamesShort.7'],js_localize['calendar.monthNamesShort.8'],
		js_localize['calendar.monthNamesShort.9'],js_localize['calendar.monthNamesShort.10'],js_localize['calendar.monthNamesShort.11'],js_localize['calendar.monthNamesShort.12'] ],
	dayNames: [ js_localize['calendar.dayNames.1'],js_localize['calendar.dayNames.2'],js_localize['calendar.dayNames.3'],js_localize['calendar.dayNames.4'],
		js_localize['calendar.dayNames.5'],js_localize['calendar.dayNames.6'],js_localize['calendar.dayNames.7'] ],
	dayNamesShort: [ js_localize['calendar.dayNamesShort.1'],js_localize['calendar.dayNamesShort.2'],js_localize['calendar.dayNamesShort.3'],js_localize['calendar.dayNamesShort.4'],
		js_localize['calendar.dayNamesShort.5'],js_localize['calendar.dayNamesShort.6'],js_localize['calendar.dayNamesShort.7']],
	dayNamesMin: [ js_localize['calendar.dayNamesMin.1'],js_localize['calendar.dayNamesMin.2'],js_localize['calendar.dayNamesMin.3'],js_localize['calendar.dayNamesMin.4'],
		js_localize['calendar.dayNamesMin.5'],js_localize['calendar.dayNamesMin.6'],js_localize['calendar.dayNamesMin.7'] ],
	weekHeader: "KW",
	dateFormat: "dd.mm.yy",
	firstDay: 1,
	isRTL: false,
	showMonthAfterYear: false,
	yearSuffix: "" };
datepicker.setDefaults( datepicker.regional.de );

return datepicker.regional.de;

} ) );
/*
 * Collapse plugin for jQuery
 * --
 * source: http://github.com/danielstocks/jQuery-Collapse/
 * site: http://webcloud.se/jQuery-Collapse
 *
 * @author Daniel Stocks (http://webcloud.se)
 * Copyright 2013, Daniel Stocks
 * Released under the MIT, BSD, and GPL Licenses.
 */

(function($) {

  // Constructor
  function Collapse (el, options) {
    options = options || {};
    var _this = this,
      query = options.query || "> :even";

    $.extend(_this, {
      $el: el,
      options : options,
      sections: [],
      isAccordion : options.accordion || false,
      db : options.persist ? jQueryCollapseStorage(el.get(0).id) : false
    });

    // Figure out what sections are open if storage is used
    _this.states = _this.db ? _this.db.read() : [];

    // For every pair of elements in given
    // element, create a section
    _this.$el.find(query).each(function() {
      new jQueryCollapseSection($(this), _this);
    });

    // Capute ALL the clicks!
    (function(scope) {
      _this.$el.on("click", "[data-collapse-summary] " + (scope.options.clickQuery || ""),
        $.proxy(_this.handleClick, scope));

      _this.$el.bind("toggle close open",
        $.proxy(_this.handleEvent, scope));

    }(_this));
  }

  Collapse.prototype = {
    handleClick: function(e, state) {
      e.preventDefault();
      var state = state || "toggle"
      var sections = this.sections,
        l = sections.length;
      while(l--) {
        if($.contains(sections[l].$summary[0], e.target)) {
          sections[l][state]();
          break;
        }
      }
    },
    handleEvent: function(e) {
      if(e.target == this.$el.get(0)) return this[e.type]();
      this.handleClick(e, e.type);
    },
    open: function(eq) {
      if(isFinite(eq)) return this.sections[eq].open();
      $.each(this.sections, function(i, section) {
        section.open();
      })
    },
    close: function(eq) {
      if(isFinite(eq)) return this.sections[eq].close();
      $.each(this.sections, function(i, section) {
        section.close();
      })
    },
    toggle: function(eq) {
      if(isFinite(eq)) return this.sections[eq].toggle();
      $.each(this.sections, function(i, section) {
        section.toggle();
      })
    }
  };

  // Section constructor
  function Section($el, parent) {

    if(!parent.options.clickQuery) $el.wrapInner('<a href="#"/>');

    $.extend(this, {
      isOpen : false,
      $summary : $el.attr("data-collapse-summary",""),
      $details : $el.next(),
      options: parent.options,
      parent: parent
    });
    parent.sections.push(this);

    // Check current state of section
    var state = parent.states[this._index()];

    if(state === 0) {
      this.close(true)
    }
    else if(this.$summary.is(".open") || state === 1) {
      this.open(true);
    } else {
      this.close(true)
    }
  }

  Section.prototype = {
    toggle : function() {
      this.isOpen ? this.close() : this.open();
    },
    close: function(bypass) {
      this._changeState("close", bypass);
    },
    open: function(bypass) {
      var _this = this;
      if(_this.options.accordion && !bypass) {
        $.each(_this.parent.sections, function(i, section) {
          section.close()
        });
      }
      _this._changeState("open", bypass);
    },
    _index: function() {
      return $.inArray(this, this.parent.sections);
    },
    _changeState: function(state, bypass) {

      var _this = this;
      _this.isOpen = state == "open";
      if($.isFunction(_this.options[state]) && !bypass) {
        _this.options[state].apply(_this.$details);
      } else {
        _this.$details[_this.isOpen ? "show" : "hide"]();
      }

      _this.$summary.toggleClass("open", state != "close")
      _this.$details.attr("aria-hidden", state == "close");
      _this.$summary.attr("aria-expanded", state == "open");
      _this.$summary.trigger(state == "open" ? "opened" : "closed", _this);
      if(_this.parent.db) {
        _this.parent.db.write(_this._index(), _this.isOpen);
      }
    }
  };

  // Expose in jQuery API
  $.fn.extend({
    collapse: function(options, scan) {
      var nodes = (scan) ? $("body").find("[data-collapse]") : $(this);
      return nodes.each(function() {
        var settings = (scan) ? {} : options,
          values = $(this).attr("data-collapse") || "";
        $.each(values.split(" "), function(i,v) {
          if(v) settings[v] = true;
        });
        new Collapse($(this), settings);
      });
    }
  });

  //jQuery DOM Ready
  $(function() {
    $.fn.collapse(false, true);
  });

  // Expose constructor to
  // global namespace
  jQueryCollapse = Collapse;
  jQueryCollapseSection = Section;

})(window.jQuery);

// DOM.event.move
//
// 2.0.0
//
// Stephen Band
//
// Triggers 'movestart', 'move' and 'moveend' events after
// mousemoves following a mousedown cross a distance threshold,
// similar to the native 'dragstart', 'drag' and 'dragend' events.
// Move events are throttled to animation frames. Move event objects
// have the properties:
//
// pageX:
// pageY:     Page coordinates of pointer.
// startX:
// startY:    Page coordinates of pointer at movestart.
// distX:
// distY:     Distance the pointer has moved since movestart.
// deltaX:
// deltaY:    Distance the finger has moved since last event.
// velocityX:
// velocityY: Average velocity over last few events.


(function(fn) {
	if (typeof define === 'function' && define.amd) {
        define([], fn);
    } else if ((typeof module !== "undefined" && module !== null) && module.exports) {
        module.exports = fn;
	} else {
		fn();
	}
})(function(){
	var assign = Object.assign || window.jQuery && jQuery.extend;

	// Number of pixels a pressed pointer travels before movestart
	// event is fired.
	var threshold = 8;

	// Shim for requestAnimationFrame, falling back to timer. See:
	// see http://paulirish.com/2011/requestanimationframe-for-smart-animating/
	var requestFrame = (function(){
		return (
			window.requestAnimationFrame ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame ||
			window.oRequestAnimationFrame ||
			window.msRequestAnimationFrame ||
			function(fn, element){
				return window.setTimeout(function(){
					fn();
				}, 25);
			}
		);
	})();

	var ignoreTags = {
			textarea: true,
			input: true,
			select: true,
			button: true
		};

	var mouseevents = {
		move:   'mousemove',
		cancel: 'mouseup dragstart',
		end:    'mouseup'
	};

	var touchevents = {
		move:   'touchmove',
		cancel: 'touchend',
		end:    'touchend'
	};

	var rspaces = /\s+/;


	// DOM Events

	var eventOptions = { bubbles: true, cancelable: true };

	var eventsSymbol = Symbol('events');

	function createEvent(type) {
		return new CustomEvent(type, eventOptions);
	}

	function getEvents(node) {
		return node[eventsSymbol] || (node[eventsSymbol] = {});
	}

	function on(node, types, fn, data, selector) {
		types = types.split(rspaces);

		var events = getEvents(node);
		var i = types.length;
		var handlers, type;

		function handler(e) { fn(e, data); }

		while (i--) {
			type = types[i];
			handlers = events[type] || (events[type] = []);
			handlers.push([fn, handler]);
			node.addEventListener(type, handler);
		}
	}

	function off(node, types, fn, selector) {
		types = types.split(rspaces);

		var events = getEvents(node);
		var i = types.length;
		var type, handlers, k;

		if (!events) { return; }

		while (i--) {
			type = types[i];
			handlers = events[type];
			if (!handlers) { continue; }
			k = handlers.length;
			while (k--) {
				if (handlers[k][0] === fn) {
					node.removeEventListener(type, handlers[k][1]);
					handlers.splice(k, 1);
				}
			}
		}
	}

	function trigger(node, type, properties) {
		// Don't cache events. It prevents you from triggering an event of a
		// given type from inside the handler of another event of that type.
		var event = createEvent(type);
		if (properties) { assign(event, properties); }
		node.dispatchEvent(event);
	}


	// Constructors
	
	function Timer(fn){
		var callback = fn,
		    active = false,
		    running = false;
		
		function trigger(time) {
			if (active){
				callback();
				requestFrame(trigger);
				running = true;
				active = false;
			}
			else {
				running = false;
			}
		}
		
		this.kick = function(fn) {
			active = true;
			if (!running) { trigger(); }
		};
		
		this.end = function(fn) {
			var cb = callback;
			
			if (!fn) { return; }
			
			// If the timer is not running, simply call the end callback.
			if (!running) {
				fn();
			}
			// If the timer is running, and has been kicked lately, then
			// queue up the current callback and the end callback, otherwise
			// just the end callback.
			else {
				callback = active ?
					function(){ cb(); fn(); } : 
					fn ;
				
				active = true;
			}
		};
	}


	// Functions

	function noop() {}
	
	function preventDefault(e) {
		e.preventDefault();
	}

	function isIgnoreTag(e) {
		return !!ignoreTags[e.target.tagName.toLowerCase()];
	}

	function isPrimaryButton(e) {
		// Ignore mousedowns on any button other than the left (or primary)
		// mouse button, or when a modifier key is pressed.
		return (e.which === 1 && !e.ctrlKey && !e.altKey);
	}

	function identifiedTouch(touchList, id) {
		var i, l;

		if (touchList.identifiedTouch) {
			return touchList.identifiedTouch(id);
		}
		
		// touchList.identifiedTouch() does not exist in
		// webkit yet… we must do the search ourselves...
		
		i = -1;
		l = touchList.length;
		
		while (++i < l) {
			if (touchList[i].identifier === id) {
				return touchList[i];
			}
		}
	}

	function changedTouch(e, data) {
		var touch = identifiedTouch(e.changedTouches, data.identifier);

		// This isn't the touch you're looking for.
		if (!touch) { return; }

		// Chrome Android (at least) includes touches that have not
		// changed in e.changedTouches. That's a bit annoying. Check
		// that this touch has changed.
		if (touch.pageX === data.pageX && touch.pageY === data.pageY) { return; }

		return touch;
	}


	// Handlers that decide when the first movestart is triggered
	
	function mousedown(e){
		// Ignore non-primary buttons
		if (!isPrimaryButton(e)) { return; }

		// Ignore form and interactive elements
		if (isIgnoreTag(e)) { return; }

		on(document, mouseevents.move, mousemove, e);
		on(document, mouseevents.cancel, mouseend, e);
	}

	function mousemove(e, data){
		checkThreshold(e, data, e, removeMouse);
	}

	function mouseend(e, data) {
		removeMouse();
	}

	function removeMouse() {
		off(document, mouseevents.move, mousemove);
		off(document, mouseevents.cancel, mouseend);
	}

	function touchstart(e) {
		// Don't get in the way of interaction with form elements
		if (ignoreTags[e.target.tagName.toLowerCase()]) { return; }

		var touch = e.changedTouches[0];

		// iOS live updates the touch objects whereas Android gives us copies.
		// That means we can't trust the touchstart object to stay the same,
		// so we must copy the data. This object acts as a template for
		// movestart, move and moveend event objects.
		var data = {
			target:     touch.target,
			pageX:      touch.pageX,
			pageY:      touch.pageY,
			identifier: touch.identifier,

			// The only way to make handlers individually unbindable is by
			// making them unique.
			touchmove:  function(e, data) { touchmove(e, data); },
			touchend:   function(e, data) { touchend(e, data); }
		};

		on(document, touchevents.move, data.touchmove, data);
		on(document, touchevents.cancel, data.touchend, data);
	}

	function touchmove(e, data) {
		var touch = changedTouch(e, data);
		if (!touch) { return; }
		checkThreshold(e, data, touch, removeTouch);
	}

	function touchend(e, data) {
		var touch = identifiedTouch(e.changedTouches, data.identifier);
		if (!touch) { return; }
		removeTouch(data);
	}

	function removeTouch(data) {
		off(document, touchevents.move, data.touchmove);
		off(document, touchevents.cancel, data.touchend);
	}

	function checkThreshold(e, data, touch, fn) {
		var distX = touch.pageX - data.pageX;
		var distY = touch.pageY - data.pageY;

		// Do nothing if the threshold has not been crossed.
		if ((distX * distX) + (distY * distY) < (threshold * threshold)) { return; }

		triggerStart(e, data, touch, distX, distY, fn);
	}

	function triggerStart(e, data, touch, distX, distY, fn) {
		var touches = e.targetTouches;
		var time = e.timeStamp - data.timeStamp;

		// Create a movestart object with some special properties that
		// are passed only to the movestart handlers.
		var template = {
			altKey:     e.altKey,
			ctrlKey:    e.ctrlKey,
			shiftKey:   e.shiftKey,
			startX:     data.pageX,
			startY:     data.pageY,
			distX:      distX,
			distY:      distY,
			deltaX:     distX,
			deltaY:     distY,
			pageX:      touch.pageX,
			pageY:      touch.pageY,
			velocityX:  distX / time,
			velocityY:  distY / time,
			identifier: data.identifier,
			targetTouches: touches,
			finger: touches ? touches.length : 1,
			enableMove: function() {
				this.moveEnabled = true;
				this.enableMove = noop;
				e.preventDefault();
			}
		};

		// Trigger the movestart event.
		trigger(data.target, 'movestart', template);

		// Unbind handlers that tracked the touch or mouse up till now.
		fn(data);
	}


	// Handlers that control what happens following a movestart

	function activeMousemove(e, data) {
		var timer  = data.timer;

		data.touch = e;
		data.timeStamp = e.timeStamp;
		timer.kick();
	}

	function activeMouseend(e, data) {
		var target = data.target;
		var event  = data.event;
		var timer  = data.timer;

		removeActiveMouse();

		endEvent(target, event, timer, function() {
			// Unbind the click suppressor, waiting until after mouseup
			// has been handled.
			setTimeout(function(){
				off(target, 'click', preventDefault);
			}, 0);
		});
	}

	function removeActiveMouse() {
		off(document, mouseevents.move, activeMousemove);
		off(document, mouseevents.end, activeMouseend);
	}

	function activeTouchmove(e, data) {
		var event = data.event;
		var timer = data.timer;
		var touch = changedTouch(e, event);

		if (!touch) { return; }

		// Stop the interface from gesturing
		e.preventDefault();

		event.targetTouches = e.targetTouches;
		data.touch = touch;
		data.timeStamp = e.timeStamp;

		timer.kick();
	}

	function activeTouchend(e, data) {
		var target = data.target;
		var event  = data.event;
		var timer  = data.timer;
		var touch  = identifiedTouch(e.changedTouches, event.identifier);

		// This isn't the touch you're looking for.
		if (!touch) { return; }

		removeActiveTouch(data);
		endEvent(target, event, timer);
	}

	function removeActiveTouch(data) {
		off(document, touchevents.move, data.activeTouchmove);
		off(document, touchevents.end, data.activeTouchend);
	}


	// Logic for triggering move and moveend events

	function updateEvent(event, touch, timeStamp) {
		var time = timeStamp - event.timeStamp;

		event.distX =  touch.pageX - event.startX;
		event.distY =  touch.pageY - event.startY;
		event.deltaX = touch.pageX - event.pageX;
		event.deltaY = touch.pageY - event.pageY;
		
		// Average the velocity of the last few events using a decay
		// curve to even out spurious jumps in values.
		event.velocityX = 0.3 * event.velocityX + 0.7 * event.deltaX / time;
		event.velocityY = 0.3 * event.velocityY + 0.7 * event.deltaY / time;
		event.pageX =  touch.pageX;
		event.pageY =  touch.pageY;
	}

	function endEvent(target, event, timer, fn) {
		timer.end(function(){
			trigger(target, 'moveend', event);
			return fn && fn();
		});
	}


	// Set up the DOM

	function movestart(e) {
		if (e.defaultPrevented) { return; }
		if (!e.moveEnabled) { return; }

		var event = {
			startX:        e.startX,
			startY:        e.startY,
			pageX:         e.pageX,
			pageY:         e.pageY,
			distX:         e.distX,
			distY:         e.distY,
			deltaX:        e.deltaX,
			deltaY:        e.deltaY,
			velocityX:     e.velocityX,
			velocityY:     e.velocityY,
			identifier:    e.identifier,
			targetTouches: e.targetTouches,
			finger:        e.finger
		};

		var data = {
			target:    e.target,
			event:     event,
			timer:     new Timer(update),
			touch:     undefined,
			timeStamp: e.timeStamp
		};

		function update(time) {
			updateEvent(event, data.touch, data.timeStamp);
			trigger(data.target, 'move', event);
		}

		if (e.identifier === undefined) {
			// We're dealing with a mouse event.
			// Stop clicks from propagating during a move
			on(e.target, 'click', preventDefault);
			on(document, mouseevents.move, activeMousemove, data);
			on(document, mouseevents.end, activeMouseend, data);
		}
		else {
			// In order to unbind correct handlers they have to be unique
			data.activeTouchmove = function(e, data) { activeTouchmove(e, data); };
			data.activeTouchend = function(e, data) { activeTouchend(e, data); };

			// We're dealing with a touch.
			on(document, touchevents.move, data.activeTouchmove, data);
			on(document, touchevents.end, data.activeTouchend, data);
		}
	}

	on(document, 'mousedown', mousedown);
	on(document, 'touchstart', touchstart);
	on(document, 'movestart', movestart);


	// jQuery special events
	//
	// jQuery event objects are copies of DOM event objects. They need
	// a little help copying the move properties across.

	if (!window.jQuery) { return; }

	var properties = ("startX startY pageX pageY distX distY deltaX deltaY velocityX velocityY").split(' ');

	function enableMove1(e) { e.enableMove(); }
	function enableMove2(e) { e.enableMove(); }
	function enableMove3(e) { e.enableMove(); }

	function add(handleObj) {
		var handler = handleObj.handler;

		handleObj.handler = function(e) {
			// Copy move properties across from originalEvent
			var i = properties.length;
			var property;

			while(i--) {
				property = properties[i];
				e[property] = e.originalEvent[property];
			}

			handler.apply(this, arguments);
		};
	}

	jQuery.event.special.movestart = {
		setup: function() {
			// Movestart must be enabled to allow other move events
			on(this, 'movestart', enableMove1);

			// Do listen to DOM events
			return false;
		},

		teardown: function() {
			off(this, 'movestart', enableMove1);
			return false;
		},

		add: add
	};

	jQuery.event.special.move = {
		setup: function() {
			on(this, 'movestart', enableMove2);
			return false;
		},

		teardown: function() {
			off(this, 'movestart', enableMove2);
			return false;
		},

		add: add
	};

	jQuery.event.special.moveend = {
		setup: function() {
			on(this, 'movestart', enableMove3);
			return false;
		},

		teardown: function() {
			off(this, 'movestart', enableMove3);
			return false;
		},

		add: add
	};
});

// jQuery.event.swipe
// 0.5
// Stephen Band

// Dependencies
// jQuery.event.move 1.2

// One of swipeleft, swiperight, swipeup or swipedown is triggered on
// moveend, when the move has covered a threshold ratio of the dimension
// of the target node, or has gone really fast. Threshold and velocity
// sensitivity changed with:
//
// jQuery.event.special.swipe.settings.threshold
// jQuery.event.special.swipe.settings.sensitivity

(function (module) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as an anonymous module.
		define(['jquery'], module);
	} else {
		// Browser globals
		module(jQuery);
	}
})(function(jQuery, undefined){
	var add = jQuery.event.add,
	   
	    remove = jQuery.event.remove,

	    // Just sugar, so we can have arguments in the same order as
	    // add and remove.
	    trigger = function(node, type, data) {
	    	jQuery.event.trigger(type, data, node);
	    },

	    settings = {
	    	// Ratio of distance over target finger must travel to be
	    	// considered a swipe.
	    	threshold: 0.4,
	    	// Faster fingers can travel shorter distances to be considered
	    	// swipes. 'sensitivity' controls how much. Bigger is shorter.
	    	sensitivity: 6
	    };

	function moveend(e) {
		var w, h, event;

		w = e.target.offsetWidth;
		h = e.target.offsetHeight;

		// Copy over some useful properties from the move event
		event = {
			distX: e.distX,
			distY: e.distY,
			velocityX: e.velocityX,
			velocityY: e.velocityY,
			finger: e.finger
		};

		// Find out which of the four directions was swiped
		if (e.distX > e.distY) {
			if (e.distX > -e.distY) {
				if (e.distX/w > settings.threshold || e.velocityX * e.distX/w * settings.sensitivity > 1) {
					event.type = 'swiperight';
					trigger(e.currentTarget, event);
				}
			}
			else {
				if (-e.distY/h > settings.threshold || e.velocityY * e.distY/w * settings.sensitivity > 1) {
					event.type = 'swipeup';
					trigger(e.currentTarget, event);
				}
			}
		}
		else {
			if (e.distX > -e.distY) {
				if (e.distY/h > settings.threshold || e.velocityY * e.distY/w * settings.sensitivity > 1) {
					event.type = 'swipedown';
					trigger(e.currentTarget, event);
				}
			}
			else {
				if (-e.distX/w > settings.threshold || e.velocityX * e.distX/w * settings.sensitivity > 1) {
					event.type = 'swipeleft';
					trigger(e.currentTarget, event);
				}
			}
		}
	}

	function getData(node) {
		var data = jQuery.data(node, 'event_swipe');
		
		if (!data) {
			data = { count: 0 };
			jQuery.data(node, 'event_swipe', data);
		}
		
		return data;
	}

	jQuery.event.special.swipe =
	jQuery.event.special.swipeleft =
	jQuery.event.special.swiperight =
	jQuery.event.special.swipeup =
	jQuery.event.special.swipedown = {
		setup: function( data, namespaces, eventHandle ) {
			var data = getData(this);

			// If another swipe event is already setup, don't setup again.
			if (data.count++ > 0) { return; }

			add(this, 'moveend', moveend);

			return true;
		},

		teardown: function() {
			var data = getData(this);

			// If another swipe event is still setup, don't teardown.
			if (--data.count > 0) { return; }

			remove(this, 'moveend', moveend);

			return true;
		},

		settings: settings
	};
});
/*!
 * Modernizr v2.7.1
 * www.modernizr.com
 *
 * Copyright (c) Faruk Ates, Paul Irish, Alex Sexton
 * Available under the BSD and MIT licenses: www.modernizr.com/license/
 */

/*
 * Modernizr tests which native CSS3 and HTML5 features are available in
 * the current UA and makes the results available to you in two ways:
 * as properties on a global Modernizr object, and as classes on the
 * <html> element. This information allows you to progressively enhance
 * your pages with a granular level of control over the experience.
 *
 * Modernizr has an optional (not included) conditional resource loader
 * called Modernizr.load(), based on Yepnope.js (yepnopejs.com).
 * To get a build that includes Modernizr.load(), as well as choosing
 * which tests to include, go to www.modernizr.com/download/
 *
 * Authors        Faruk Ates, Paul Irish, Alex Sexton
 * Contributors   Ryan Seddon, Ben Alman
 */

window.Modernizr = (function( window, document, undefined ) {

    var version = '2.7.1',

    Modernizr = {},

    /*>>cssclasses*/
    // option for enabling the HTML classes to be added
    enableClasses = true,
    /*>>cssclasses*/

    docElement = document.documentElement,

    /**
     * Create our "modernizr" element that we do most feature tests on.
     */
    mod = 'modernizr',
    modElem = document.createElement(mod),
    mStyle = modElem.style,

    /**
     * Create the input element for various Web Forms feature tests.
     */
    inputElem /*>>inputelem*/ = document.createElement('input') /*>>inputelem*/ ,

    /*>>smile*/
    smile = ':)',
    /*>>smile*/

    toString = {}.toString,

    // TODO :: make the prefixes more granular
    /*>>prefixes*/
    // List of property values to set for css tests. See ticket #21
    prefixes = ' -webkit- -moz- -o- -ms- '.split(' '),
    /*>>prefixes*/

    /*>>domprefixes*/
    // Following spec is to expose vendor-specific style properties as:
    //   elem.style.WebkitBorderRadius
    // and the following would be incorrect:
    //   elem.style.webkitBorderRadius

    // Webkit ghosts their properties in lowercase but Opera & Moz do not.
    // Microsoft uses a lowercase `ms` instead of the correct `Ms` in IE8+
    //   erik.eae.net/archives/2008/03/10/21.48.10/

    // More here: github.com/Modernizr/Modernizr/issues/issue/21
    omPrefixes = 'Webkit Moz O ms',

    cssomPrefixes = omPrefixes.split(' '),

    domPrefixes = omPrefixes.toLowerCase().split(' '),
    /*>>domprefixes*/

    /*>>ns*/
    ns = {'svg': 'http://www.w3.org/2000/svg'},
    /*>>ns*/

    tests = {},
    inputs = {},
    attrs = {},

    classes = [],

    slice = classes.slice,

    featureName, // used in testing loop


    /*>>teststyles*/
    // Inject element with style element and some CSS rules
    injectElementWithStyles = function( rule, callback, nodes, testnames ) {

      var style, ret, node, docOverflow,
          div = document.createElement('div'),
          // After page load injecting a fake body doesn't work so check if body exists
          body = document.body,
          // IE6 and 7 won't return offsetWidth or offsetHeight unless it's in the body element, so we fake it.
          fakeBody = body || document.createElement('body');

      if ( parseInt(nodes, 10) ) {
          // In order not to give false positives we create a node for each test
          // This also allows the method to scale for unspecified uses
          while ( nodes-- ) {
              node = document.createElement('div');
              node.id = testnames ? testnames[nodes] : mod + (nodes + 1);
              div.appendChild(node);
          }
      }

      // <style> elements in IE6-9 are considered 'NoScope' elements and therefore will be removed
      // when injected with innerHTML. To get around this you need to prepend the 'NoScope' element
      // with a 'scoped' element, in our case the soft-hyphen entity as it won't mess with our measurements.
      // msdn.microsoft.com/en-us/library/ms533897%28VS.85%29.aspx
      // Documents served as xml will throw if using &shy; so use xml friendly encoded version. See issue #277
      style = ['&#173;','<style id="s', mod, '">', rule, '</style>'].join('');
      div.id = mod;
      // IE6 will false positive on some tests due to the style element inside the test div somehow interfering offsetHeight, so insert it into body or fakebody.
      // Opera will act all quirky when injecting elements in documentElement when page is served as xml, needs fakebody too. #270
      (body ? div : fakeBody).innerHTML += style;
      fakeBody.appendChild(div);
      if ( !body ) {
          //avoid crashing IE8, if background image is used
          fakeBody.style.background = '';
          //Safari 5.13/5.1.4 OSX stops loading if ::-webkit-scrollbar is used and scrollbars are visible
          fakeBody.style.overflow = 'hidden';
          docOverflow = docElement.style.overflow;
          docElement.style.overflow = 'hidden';
          docElement.appendChild(fakeBody);
      }

      ret = callback(div, rule);
      // If this is done after page load we don't want to remove the body so check if body exists
      if ( !body ) {
          fakeBody.parentNode.removeChild(fakeBody);
          docElement.style.overflow = docOverflow;
      } else {
          div.parentNode.removeChild(div);
      }

      return !!ret;

    },
    /*>>teststyles*/

    /*>>mq*/
    // adapted from matchMedia polyfill
    // by Scott Jehl and Paul Irish
    // gist.github.com/786768
    testMediaQuery = function( mq ) {

      var matchMedia = window.matchMedia || window.msMatchMedia;
      if ( matchMedia ) {
        return matchMedia(mq).matches;
      }

      var bool;

      injectElementWithStyles('@media ' + mq + ' { #' + mod + ' { position: absolute; } }', function( node ) {
        bool = (window.getComputedStyle ?
                  getComputedStyle(node, null) :
                  node.currentStyle)['position'] == 'absolute';
      });

      return bool;

     },
     /*>>mq*/


    /*>>hasevent*/
    //
    // isEventSupported determines if a given element supports the given event
    // kangax.github.com/iseventsupported/
    //
    // The following results are known incorrects:
    //   Modernizr.hasEvent("webkitTransitionEnd", elem) // false negative
    //   Modernizr.hasEvent("textInput") // in Webkit. github.com/Modernizr/Modernizr/issues/333
    //   ...
    isEventSupported = (function() {

      var TAGNAMES = {
        'select': 'input', 'change': 'input',
        'submit': 'form', 'reset': 'form',
        'error': 'img', 'load': 'img', 'abort': 'img'
      };

      function isEventSupported( eventName, element ) {

        element = element || document.createElement(TAGNAMES[eventName] || 'div');
        eventName = 'on' + eventName;

        // When using `setAttribute`, IE skips "unload", WebKit skips "unload" and "resize", whereas `in` "catches" those
        var isSupported = eventName in element;

        if ( !isSupported ) {
          // If it has no `setAttribute` (i.e. doesn't implement Node interface), try generic element
          if ( !element.setAttribute ) {
            element = document.createElement('div');
          }
          if ( element.setAttribute && element.removeAttribute ) {
            element.setAttribute(eventName, '');
            isSupported = is(element[eventName], 'function');

            // If property was created, "remove it" (by setting value to `undefined`)
            if ( !is(element[eventName], 'undefined') ) {
              element[eventName] = undefined;
            }
            element.removeAttribute(eventName);
          }
        }

        element = null;
        return isSupported;
      }
      return isEventSupported;
    })(),
    /*>>hasevent*/

    // TODO :: Add flag for hasownprop ? didn't last time

    // hasOwnProperty shim by kangax needed for Safari 2.0 support
    _hasOwnProperty = ({}).hasOwnProperty, hasOwnProp;

    if ( !is(_hasOwnProperty, 'undefined') && !is(_hasOwnProperty.call, 'undefined') ) {
      hasOwnProp = function (object, property) {
        return _hasOwnProperty.call(object, property);
      };
    }
    else {
      hasOwnProp = function (object, property) { /* yes, this can give false positives/negatives, but most of the time we don't care about those */
        return ((property in object) && is(object.constructor.prototype[property], 'undefined'));
      };
    }

    // Adapted from ES5-shim https://github.com/kriskowal/es5-shim/blob/master/es5-shim.js
    // es5.github.com/#x15.3.4.5

    if (!Function.prototype.bind) {
      Function.prototype.bind = function bind(that) {

        var target = this;

        if (typeof target != "function") {
            throw new TypeError();
        }

        var args = slice.call(arguments, 1),
            bound = function () {

            if (this instanceof bound) {

              var F = function(){};
              F.prototype = target.prototype;
              var self = new F();

              var result = target.apply(
                  self,
                  args.concat(slice.call(arguments))
              );
              if (Object(result) === result) {
                  return result;
              }
              return self;

            } else {

              return target.apply(
                  that,
                  args.concat(slice.call(arguments))
              );

            }

        };

        return bound;
      };
    }

    /**
     * setCss applies given styles to the Modernizr DOM node.
     */
    function setCss( str ) {
        mStyle.cssText = str;
    }

    /**
     * setCssAll extrapolates all vendor-specific css strings.
     */
    function setCssAll( str1, str2 ) {
        return setCss(prefixes.join(str1 + ';') + ( str2 || '' ));
    }

    /**
     * is returns a boolean for if typeof obj is exactly type.
     */
    function is( obj, type ) {
        return typeof obj === type;
    }

    /**
     * contains returns a boolean for if substr is found within str.
     */
    function contains( str, substr ) {
        return !!~('' + str).indexOf(substr);
    }

    /*>>testprop*/

    // testProps is a generic CSS / DOM property test.

    // In testing support for a given CSS property, it's legit to test:
    //    `elem.style[styleName] !== undefined`
    // If the property is supported it will return an empty string,
    // if unsupported it will return undefined.

    // We'll take advantage of this quick test and skip setting a style
    // on our modernizr element, but instead just testing undefined vs
    // empty string.

    // Because the testing of the CSS property names (with "-", as
    // opposed to the camelCase DOM properties) is non-portable and
    // non-standard but works in WebKit and IE (but not Gecko or Opera),
    // we explicitly reject properties with dashes so that authors
    // developing in WebKit or IE first don't end up with
    // browser-specific content by accident.

    function testProps( props, prefixed ) {
        for ( var i in props ) {
            var prop = props[i];
            if ( !contains(prop, "-") && mStyle[prop] !== undefined ) {
                return prefixed == 'pfx' ? prop : true;
            }
        }
        return false;
    }
    /*>>testprop*/

    // TODO :: add testDOMProps
    /**
     * testDOMProps is a generic DOM property test; if a browser supports
     *   a certain property, it won't return undefined for it.
     */
    function testDOMProps( props, obj, elem ) {
        for ( var i in props ) {
            var item = obj[props[i]];
            if ( item !== undefined) {

                // return the property name as a string
                if (elem === false) return props[i];

                // let's bind a function
                if (is(item, 'function')){
                  // default to autobind unless override
                  return item.bind(elem || obj);
                }

                // return the unbound function or obj or value
                return item;
            }
        }
        return false;
    }

    /*>>testallprops*/
    /**
     * testPropsAll tests a list of DOM properties we want to check against.
     *   We specify literally ALL possible (known and/or likely) properties on
     *   the element including the non-vendor prefixed one, for forward-
     *   compatibility.
     */
    function testPropsAll( prop, prefixed, elem ) {

        var ucProp  = prop.charAt(0).toUpperCase() + prop.slice(1),
            props   = (prop + ' ' + cssomPrefixes.join(ucProp + ' ') + ucProp).split(' ');

        // did they call .prefixed('boxSizing') or are we just testing a prop?
        if(is(prefixed, "string") || is(prefixed, "undefined")) {
          return testProps(props, prefixed);

        // otherwise, they called .prefixed('requestAnimationFrame', window[, elem])
        } else {
          props = (prop + ' ' + (domPrefixes).join(ucProp + ' ') + ucProp).split(' ');
          return testDOMProps(props, prefixed, elem);
        }
    }
    /*>>testallprops*/


    /**
     * Tests
     * -----
     */

    // The *new* flexbox
    // dev.w3.org/csswg/css3-flexbox

    tests['flexbox'] = function() {
      return testPropsAll('flexWrap');
    };

    // The *old* flexbox
    // www.w3.org/TR/2009/WD-css3-flexbox-20090723/

    tests['flexboxlegacy'] = function() {
        return testPropsAll('boxDirection');
    };

    // On the S60 and BB Storm, getContext exists, but always returns undefined
    // so we actually have to call getContext() to verify
    // github.com/Modernizr/Modernizr/issues/issue/97/

    tests['canvas'] = function() {
        var elem = document.createElement('canvas');
        return !!(elem.getContext && elem.getContext('2d'));
    };

    tests['canvastext'] = function() {
        return !!(Modernizr['canvas'] && is(document.createElement('canvas').getContext('2d').fillText, 'function'));
    };

    // webk.it/70117 is tracking a legit WebGL feature detect proposal

    // We do a soft detect which may false positive in order to avoid
    // an expensive context creation: bugzil.la/732441

    tests['webgl'] = function() {
        return !!window.WebGLRenderingContext;
    };

    /*
     * The Modernizr.touch test only indicates if the browser supports
     *    touch events, which does not necessarily reflect a touchscreen
     *    device, as evidenced by tablets running Windows 7 or, alas,
     *    the Palm Pre / WebOS (touch) phones.
     *
     * Additionally, Chrome (desktop) used to lie about its support on this,
     *    but that has since been rectified: crbug.com/36415
     *
     * We also test for Firefox 4 Multitouch Support.
     *
     * For more info, see: modernizr.github.com/Modernizr/touch.html
     */

    tests['touch'] = function() {
        var bool;

        if(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
          bool = true;
        } else {
          injectElementWithStyles(['@media (',prefixes.join('touch-enabled),('),mod,')','{#modernizr{top:9px;position:absolute}}'].join(''), function( node ) {
            bool = node.offsetTop === 9;
          });
        }

        return bool;
    };


    // geolocation is often considered a trivial feature detect...
    // Turns out, it's quite tricky to get right:
    //
    // Using !!navigator.geolocation does two things we don't want. It:
    //   1. Leaks memory in IE9: github.com/Modernizr/Modernizr/issues/513
    //   2. Disables page caching in WebKit: webk.it/43956
    //
    // Meanwhile, in Firefox < 8, an about:config setting could expose
    // a false positive that would throw an exception: bugzil.la/688158

    tests['geolocation'] = function() {
        return 'geolocation' in navigator;
    };


    tests['postmessage'] = function() {
      return !!window.postMessage;
    };


    // Chrome incognito mode used to throw an exception when using openDatabase
    // It doesn't anymore.
    tests['websqldatabase'] = function() {
      return !!window.openDatabase;
    };

    // Vendors had inconsistent prefixing with the experimental Indexed DB:
    // - Webkit's implementation is accessible through webkitIndexedDB
    // - Firefox shipped moz_indexedDB before FF4b9, but since then has been mozIndexedDB
    // For speed, we don't test the legacy (and beta-only) indexedDB
    tests['indexedDB'] = function() {
      return !!testPropsAll("indexedDB", window);
    };

    // documentMode logic from YUI to filter out IE8 Compat Mode
    //   which false positives.
    tests['hashchange'] = function() {
      return isEventSupported('hashchange', window) && (document.documentMode === undefined || document.documentMode > 7);
    };

    // Per 1.6:
    // This used to be Modernizr.historymanagement but the longer
    // name has been deprecated in favor of a shorter and property-matching one.
    // The old API is still available in 1.6, but as of 2.0 will throw a warning,
    // and in the first release thereafter disappear entirely.
    tests['history'] = function() {
      return !!(window.history && history.pushState);
    };

    tests['draganddrop'] = function() {
        var div = document.createElement('div');
        return ('draggable' in div) || ('ondragstart' in div && 'ondrop' in div);
    };

    // FF3.6 was EOL'ed on 4/24/12, but the ESR version of FF10
    // will be supported until FF19 (2/12/13), at which time, ESR becomes FF17.
    // FF10 still uses prefixes, so check for it until then.
    // for more ESR info, see: mozilla.org/en-US/firefox/organizations/faq/
    tests['websockets'] = function() {
        return 'WebSocket' in window || 'MozWebSocket' in window;
    };


    // css-tricks.com/rgba-browser-support/
    tests['rgba'] = function() {
        // Set an rgba() color and check the returned value

        setCss('background-color:rgba(150,255,150,.5)');

        return contains(mStyle.backgroundColor, 'rgba');
    };

    tests['hsla'] = function() {
        // Same as rgba(), in fact, browsers re-map hsla() to rgba() internally,
        //   except IE9 who retains it as hsla

        setCss('background-color:hsla(120,40%,100%,.5)');

        return contains(mStyle.backgroundColor, 'rgba') || contains(mStyle.backgroundColor, 'hsla');
    };

    tests['multiplebgs'] = function() {
        // Setting multiple images AND a color on the background shorthand property
        //  and then querying the style.background property value for the number of
        //  occurrences of "url(" is a reliable method for detecting ACTUAL support for this!

        setCss('background:url(https://),url(https://),red url(https://)');

        // If the UA supports multiple backgrounds, there should be three occurrences
        //   of the string "url(" in the return value for elemStyle.background

        return (/(url\s*\(.*?){3}/).test(mStyle.background);
    };



    // this will false positive in Opera Mini
    //   github.com/Modernizr/Modernizr/issues/396

    tests['backgroundsize'] = function() {
        return testPropsAll('backgroundSize');
    };

    tests['borderimage'] = function() {
        return testPropsAll('borderImage');
    };


    // Super comprehensive table about all the unique implementations of
    // border-radius: muddledramblings.com/table-of-css3-border-radius-compliance

    tests['borderradius'] = function() {
        return testPropsAll('borderRadius');
    };

    // WebOS unfortunately false positives on this test.
    tests['boxshadow'] = function() {
        return testPropsAll('boxShadow');
    };

    // FF3.0 will false positive on this test
    tests['textshadow'] = function() {
        return document.createElement('div').style.textShadow === '';
    };


    tests['opacity'] = function() {
        // Browsers that actually have CSS Opacity implemented have done so
        //  according to spec, which means their return values are within the
        //  range of [0.0,1.0] - including the leading zero.

        setCssAll('opacity:.55');

        // The non-literal . in this regex is intentional:
        //   German Chrome returns this value as 0,55
        // github.com/Modernizr/Modernizr/issues/#issue/59/comment/516632
        return (/^0.55$/).test(mStyle.opacity);
    };


    // Note, Android < 4 will pass this test, but can only animate
    //   a single property at a time
    //   daneden.me/2011/12/putting-up-with-androids-bullshit/
    tests['cssanimations'] = function() {
        return testPropsAll('animationName');
    };


    tests['csscolumns'] = function() {
        return testPropsAll('columnCount');
    };


    tests['cssgradients'] = function() {
        /**
         * For CSS Gradients syntax, please see:
         * webkit.org/blog/175/introducing-css-gradients/
         * developer.mozilla.org/en/CSS/-moz-linear-gradient
         * developer.mozilla.org/en/CSS/-moz-radial-gradient
         * dev.w3.org/csswg/css3-images/#gradients-
         */

        var str1 = 'background-image:',
            str2 = 'gradient(linear,left top,right bottom,from(#9f9),to(white));',
            str3 = 'linear-gradient(left top,#9f9, white);';

        setCss(
             // legacy webkit syntax (FIXME: remove when syntax not in use anymore)
              (str1 + '-webkit- '.split(' ').join(str2 + str1) +
             // standard syntax             // trailing 'background-image:'
              prefixes.join(str3 + str1)).slice(0, -str1.length)
        );

        return contains(mStyle.backgroundImage, 'gradient');
    };


    tests['cssreflections'] = function() {
        return testPropsAll('boxReflect');
    };


    tests['csstransforms'] = function() {
        return !!testPropsAll('transform');
    };


    tests['csstransforms3d'] = function() {

        var ret = !!testPropsAll('perspective');

        // Webkit's 3D transforms are passed off to the browser's own graphics renderer.
        //   It works fine in Safari on Leopard and Snow Leopard, but not in Chrome in
        //   some conditions. As a result, Webkit typically recognizes the syntax but
        //   will sometimes throw a false positive, thus we must do a more thorough check:
        if ( ret && 'webkitPerspective' in docElement.style ) {

          // Webkit allows this media query to succeed only if the feature is enabled.
          // `@media (transform-3d),(-webkit-transform-3d){ ... }`
          injectElementWithStyles('@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}', function( node, rule ) {
            ret = node.offsetLeft === 9 && node.offsetHeight === 3;
          });
        }
        return ret;
    };


    tests['csstransitions'] = function() {
        return testPropsAll('transition');
    };


    /*>>fontface*/
    // @font-face detection routine by Diego Perini
    // javascript.nwbox.com/CSSSupport/

    // false positives:
    //   WebOS github.com/Modernizr/Modernizr/issues/342
    //   WP7   github.com/Modernizr/Modernizr/issues/538
    tests['fontface'] = function() {
        var bool;

        injectElementWithStyles('@font-face {font-family:"font";src:url("https://")}', function( node, rule ) {
          var style = document.getElementById('smodernizr'),
              sheet = style.sheet || style.styleSheet,
              cssText = sheet ? (sheet.cssRules && sheet.cssRules[0] ? sheet.cssRules[0].cssText : sheet.cssText || '') : '';

          bool = /src/i.test(cssText) && cssText.indexOf(rule.split(' ')[0]) === 0;
        });

        return bool;
    };
    /*>>fontface*/

    // CSS generated content detection
    tests['generatedcontent'] = function() {
        var bool;

        injectElementWithStyles(['#',mod,'{font:0/0 a}#',mod,':after{content:"',smile,'";visibility:hidden;font:3px/1 a}'].join(''), function( node ) {
          bool = node.offsetHeight >= 3;
        });

        return bool;
    };



    // These tests evaluate support of the video/audio elements, as well as
    // testing what types of content they support.
    //
    // We're using the Boolean constructor here, so that we can extend the value
    // e.g.  Modernizr.video     // true
    //       Modernizr.video.ogg // 'probably'
    //
    // Codec values from : github.com/NielsLeenheer/html5test/blob/9106a8/index.html#L845
    //                     thx to NielsLeenheer and zcorpan

    // Note: in some older browsers, "no" was a return value instead of empty string.
    //   It was live in FF3.5.0 and 3.5.1, but fixed in 3.5.2
    //   It was also live in Safari 4.0.0 - 4.0.4, but fixed in 4.0.5

    tests['video'] = function() {
        var elem = document.createElement('video'),
            bool = false;

        // IE9 Running on Windows Server SKU can cause an exception to be thrown, bug #224
        try {
            if ( bool = !!elem.canPlayType ) {
                bool      = new Boolean(bool);
                bool.ogg  = elem.canPlayType('video/ogg; codecs="theora"')      .replace(/^no$/,'');

                // Without QuickTime, this value will be `undefined`. github.com/Modernizr/Modernizr/issues/546
                bool.h264 = elem.canPlayType('video/mp4; codecs="avc1.42E01E"') .replace(/^no$/,'');

                bool.webm = elem.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,'');
            }

        } catch(e) { }

        return bool;
    };

    tests['audio'] = function() {
        var elem = document.createElement('audio'),
            bool = false;

        try {
            if ( bool = !!elem.canPlayType ) {
                bool      = new Boolean(bool);
                bool.ogg  = elem.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/,'');
                bool.mp3  = elem.canPlayType('audio/mpeg;')               .replace(/^no$/,'');

                // Mimetypes accepted:
                //   developer.mozilla.org/En/Media_formats_supported_by_the_audio_and_video_elements
                //   bit.ly/iphoneoscodecs
                bool.wav  = elem.canPlayType('audio/wav; codecs="1"')     .replace(/^no$/,'');
                bool.m4a  = ( elem.canPlayType('audio/x-m4a;')            ||
                              elem.canPlayType('audio/aac;'))             .replace(/^no$/,'');
            }
        } catch(e) { }

        return bool;
    };


    // In FF4, if disabled, window.localStorage should === null.

    // Normally, we could not test that directly and need to do a
    //   `('localStorage' in window) && ` test first because otherwise Firefox will
    //   throw bugzil.la/365772 if cookies are disabled

    // Also in iOS5 Private Browsing mode, attempting to use localStorage.setItem
    // will throw the exception:
    //   QUOTA_EXCEEDED_ERRROR DOM Exception 22.
    // Peculiarly, getItem and removeItem calls do not throw.

    // Because we are forced to try/catch this, we'll go aggressive.

    // Just FWIW: IE8 Compat mode supports these features completely:
    //   www.quirksmode.org/dom/html5.html
    // But IE8 doesn't support either with local files

    tests['localstorage'] = function() {
        try {
            localStorage.setItem(mod, mod);
            localStorage.removeItem(mod);
            return true;
        } catch(e) {
            return false;
        }
    };

    tests['sessionstorage'] = function() {
        try {
            sessionStorage.setItem(mod, mod);
            sessionStorage.removeItem(mod);
            return true;
        } catch(e) {
            return false;
        }
    };


    tests['webworkers'] = function() {
        return !!window.Worker;
    };


    tests['applicationcache'] = function() {
        return !!window.applicationCache;
    };


    // Thanks to Erik Dahlstrom
    tests['svg'] = function() {
        return !!document.createElementNS && !!document.createElementNS(ns.svg, 'svg').createSVGRect;
    };

    // specifically for SVG inline in HTML, not within XHTML
    // test page: paulirish.com/demo/inline-svg
    tests['inlinesvg'] = function() {
      var div = document.createElement('div');
      div.innerHTML = '<svg/>';
      return (div.firstChild && div.firstChild.namespaceURI) == ns.svg;
    };

    // SVG SMIL animation
    tests['smil'] = function() {
        return !!document.createElementNS && /SVGAnimate/.test(toString.call(document.createElementNS(ns.svg, 'animate')));
    };

    // This test is only for clip paths in SVG proper, not clip paths on HTML content
    // demo: srufaculty.sru.edu/david.dailey/svg/newstuff/clipPath4.svg

    // However read the comments to dig into applying SVG clippaths to HTML content here:
    //   github.com/Modernizr/Modernizr/issues/213#issuecomment-1149491
    tests['svgclippaths'] = function() {
        return !!document.createElementNS && /SVGClipPath/.test(toString.call(document.createElementNS(ns.svg, 'clipPath')));
    };

    /*>>webforms*/
    // input features and input types go directly onto the ret object, bypassing the tests loop.
    // Hold this guy to execute in a moment.
    function webforms() {
        /*>>input*/
        // Run through HTML5's new input attributes to see if the UA understands any.
        // We're using f which is the <input> element created early on
        // Mike Taylr has created a comprehensive resource for testing these attributes
        //   when applied to all input types:
        //   miketaylr.com/code/input-type-attr.html
        // spec: www.whatwg.org/specs/web-apps/current-work/multipage/the-input-element.html#input-type-attr-summary

        // Only input placeholder is tested while textarea's placeholder is not.
        // Currently Safari 4 and Opera 11 have support only for the input placeholder
        // Both tests are available in feature-detects/forms-placeholder.js
        Modernizr['input'] = (function( props ) {
            for ( var i = 0, len = props.length; i < len; i++ ) {
                attrs[ props[i] ] = !!(props[i] in inputElem);
            }
            if (attrs.list){
              // safari false positive's on datalist: webk.it/74252
              // see also github.com/Modernizr/Modernizr/issues/146
              attrs.list = !!(document.createElement('datalist') && window.HTMLDataListElement);
            }
            return attrs;
        })('autocomplete autofocus list placeholder max min multiple pattern required step'.split(' '));
        /*>>input*/

        /*>>inputtypes*/
        // Run through HTML5's new input types to see if the UA understands any.
        //   This is put behind the tests runloop because it doesn't return a
        //   true/false like all the other tests; instead, it returns an object
        //   containing each input type with its corresponding true/false value

        // Big thanks to @miketaylr for the html5 forms expertise. miketaylr.com/
        Modernizr['inputtypes'] = (function(props) {

            for ( var i = 0, bool, inputElemType, defaultView, len = props.length; i < len; i++ ) {

                inputElem.setAttribute('type', inputElemType = props[i]);
                bool = inputElem.type !== 'text';

                // We first check to see if the type we give it sticks..
                // If the type does, we feed it a textual value, which shouldn't be valid.
                // If the value doesn't stick, we know there's input sanitization which infers a custom UI
                if ( bool ) {

                    inputElem.value         = smile;
                    inputElem.style.cssText = 'position:absolute;visibility:hidden;';

                    if ( /^range$/.test(inputElemType) && inputElem.style.WebkitAppearance !== undefined ) {

                      docElement.appendChild(inputElem);
                      defaultView = document.defaultView;

                      // Safari 2-4 allows the smiley as a value, despite making a slider
                      bool =  defaultView.getComputedStyle &&
                              defaultView.getComputedStyle(inputElem, null).WebkitAppearance !== 'textfield' &&
                              // Mobile android web browser has false positive, so must
                              // check the height to see if the widget is actually there.
                              (inputElem.offsetHeight !== 0);

                      docElement.removeChild(inputElem);

                    } else if ( /^(search|tel)$/.test(inputElemType) ){
                      // Spec doesn't define any special parsing or detectable UI
                      //   behaviors so we pass these through as true

                      // Interestingly, opera fails the earlier test, so it doesn't
                      //  even make it here.

                    } else if ( /^(url|email)$/.test(inputElemType) ) {
                      // Real url and email support comes with prebaked validation.
                      bool = inputElem.checkValidity && inputElem.checkValidity() === false;

                    } else {
                      // If the upgraded input compontent rejects the :) text, we got a winner
                      bool = inputElem.value != smile;
                    }
                }

                inputs[ props[i] ] = !!bool;
            }
            return inputs;
        })('search tel url email datetime date month week time datetime-local number range color'.split(' '));
        /*>>inputtypes*/
    }
    /*>>webforms*/


    // End of test definitions
    // -----------------------



    // Run through all tests and detect their support in the current UA.
    // todo: hypothetically we could be doing an array of tests and use a basic loop here.
    for ( var feature in tests ) {
        if ( hasOwnProp(tests, feature) ) {
            // run the test, throw the return value into the Modernizr,
            //   then based on that boolean, define an appropriate className
            //   and push it into an array of classes we'll join later.
            featureName  = feature.toLowerCase();
            Modernizr[featureName] = tests[feature]();

            classes.push((Modernizr[featureName] ? '' : 'no-') + featureName);
        }
    }

    /*>>webforms*/
    // input tests need to run.
    Modernizr.input || webforms();
    /*>>webforms*/


    /**
     * addTest allows the user to define their own feature tests
     * the result will be added onto the Modernizr object,
     * as well as an appropriate className set on the html element
     *
     * @param feature - String naming the feature
     * @param test - Function returning true if feature is supported, false if not
     */
     Modernizr.addTest = function ( feature, test ) {
       if ( typeof feature == 'object' ) {
         for ( var key in feature ) {
           if ( hasOwnProp( feature, key ) ) {
             Modernizr.addTest( key, feature[ key ] );
           }
         }
       } else {

         feature = feature.toLowerCase();

         if ( Modernizr[feature] !== undefined ) {
           // we're going to quit if you're trying to overwrite an existing test
           // if we were to allow it, we'd do this:
           //   var re = new RegExp("\\b(no-)?" + feature + "\\b");
           //   docElement.className = docElement.className.replace( re, '' );
           // but, no rly, stuff 'em.
           return Modernizr;
         }

         test = typeof test == 'function' ? test() : test;

         if (typeof enableClasses !== "undefined" && enableClasses) {
           docElement.className += ' ' + (test ? '' : 'no-') + feature;
         }
         Modernizr[feature] = test;

       }

       return Modernizr; // allow chaining.
     };


    // Reset modElem.cssText to nothing to reduce memory footprint.
    setCss('');
    modElem = inputElem = null;

    /*>>shiv*/
    /**
     * @preserve HTML5 Shiv prev3.7.1 | @afarkas @jdalton @jon_neal @rem | MIT/GPL2 Licensed
     */
    ;(function(window, document) {
        /*jshint evil:true */
        /** version */
        var version = '3.7.0';

        /** Preset options */
        var options = window.html5 || {};

        /** Used to skip problem elements */
        var reSkip = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i;

        /** Not all elements can be cloned in IE **/
        var saveClones = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i;

        /** Detect whether the browser supports default html5 styles */
        var supportsHtml5Styles;

        /** Name of the expando, to work with multiple documents or to re-shiv one document */
        var expando = '_html5shiv';

        /** The id for the the documents expando */
        var expanID = 0;

        /** Cached data for each document */
        var expandoData = {};

        /** Detect whether the browser supports unknown elements */
        var supportsUnknownElements;

        (function() {
          try {
            var a = document.createElement('a');
            a.innerHTML = '<xyz></xyz>';
            //if the hidden property is implemented we can assume, that the browser supports basic HTML5 Styles
            supportsHtml5Styles = ('hidden' in a);

            supportsUnknownElements = a.childNodes.length == 1 || (function() {
              // assign a false positive if unable to shiv
              (document.createElement)('a');
              var frag = document.createDocumentFragment();
              return (
                typeof frag.cloneNode == 'undefined' ||
                typeof frag.createDocumentFragment == 'undefined' ||
                typeof frag.createElement == 'undefined'
              );
            }());
          } catch(e) {
            // assign a false positive if detection fails => unable to shiv
            supportsHtml5Styles = true;
            supportsUnknownElements = true;
          }

        }());

        /*--------------------------------------------------------------------------*/

        /**
         * Creates a style sheet with the given CSS text and adds it to the document.
         * @private
         * @param {Document} ownerDocument The document.
         * @param {String} cssText The CSS text.
         * @returns {StyleSheet} The style element.
         */
        function addStyleSheet(ownerDocument, cssText) {
          var p = ownerDocument.createElement('p'),
          parent = ownerDocument.getElementsByTagName('head')[0] || ownerDocument.documentElement;

          p.innerHTML = 'x<style>' + cssText + '</style>';
          return parent.insertBefore(p.lastChild, parent.firstChild);
        }

        /**
         * Returns the value of `html5.elements` as an array.
         * @private
         * @returns {Array} An array of shived element node names.
         */
        function getElements() {
          var elements = html5.elements;
          return typeof elements == 'string' ? elements.split(' ') : elements;
        }

        /**
         * Returns the data associated to the given document
         * @private
         * @param {Document} ownerDocument The document.
         * @returns {Object} An object of data.
         */
        function getExpandoData(ownerDocument) {
          var data = expandoData[ownerDocument[expando]];
          if (!data) {
            data = {};
            expanID++;
            ownerDocument[expando] = expanID;
            expandoData[expanID] = data;
          }
          return data;
        }

        /**
         * returns a shived element for the given nodeName and document
         * @memberOf html5
         * @param {String} nodeName name of the element
         * @param {Document} ownerDocument The context document.
         * @returns {Object} The shived element.
         */
        function createElement(nodeName, ownerDocument, data){
          if (!ownerDocument) {
            ownerDocument = document;
          }
          if(supportsUnknownElements){
            return ownerDocument.createElement(nodeName);
          }
          if (!data) {
            data = getExpandoData(ownerDocument);
          }
          var node;

          if (data.cache[nodeName]) {
            node = data.cache[nodeName].cloneNode();
          } else if (saveClones.test(nodeName)) {
            node = (data.cache[nodeName] = data.createElem(nodeName)).cloneNode();
          } else {
            node = data.createElem(nodeName);
          }

          // Avoid adding some elements to fragments in IE < 9 because
          // * Attributes like `name` or `type` cannot be set/changed once an element
          //   is inserted into a document/fragment
          // * Link elements with `src` attributes that are inaccessible, as with
          //   a 403 response, will cause the tab/window to crash
          // * Script elements appended to fragments will execute when their `src`
          //   or `text` property is set
          return node.canHaveChildren && !reSkip.test(nodeName) && !node.tagUrn ? data.frag.appendChild(node) : node;
        }

        /**
         * returns a shived DocumentFragment for the given document
         * @memberOf html5
         * @param {Document} ownerDocument The context document.
         * @returns {Object} The shived DocumentFragment.
         */
        function createDocumentFragment(ownerDocument, data){
          if (!ownerDocument) {
            ownerDocument = document;
          }
          if(supportsUnknownElements){
            return ownerDocument.createDocumentFragment();
          }
          data = data || getExpandoData(ownerDocument);
          var clone = data.frag.cloneNode(),
          i = 0,
          elems = getElements(),
          l = elems.length;
          for(;i<l;i++){
            clone.createElement(elems[i]);
          }
          return clone;
        }

        /**
         * Shivs the `createElement` and `createDocumentFragment` methods of the document.
         * @private
         * @param {Document|DocumentFragment} ownerDocument The document.
         * @param {Object} data of the document.
         */
        function shivMethods(ownerDocument, data) {
          if (!data.cache) {
            data.cache = {};
            data.createElem = ownerDocument.createElement;
            data.createFrag = ownerDocument.createDocumentFragment;
            data.frag = data.createFrag();
          }


          ownerDocument.createElement = function(nodeName) {
            //abort shiv
            if (!html5.shivMethods) {
              return data.createElem(nodeName);
            }
            return createElement(nodeName, ownerDocument, data);
          };

          ownerDocument.createDocumentFragment = Function('h,f', 'return function(){' +
                                                          'var n=f.cloneNode(),c=n.createElement;' +
                                                          'h.shivMethods&&(' +
                                                          // unroll the `createElement` calls
                                                          getElements().join().replace(/[\w\-]+/g, function(nodeName) {
            data.createElem(nodeName);
            data.frag.createElement(nodeName);
            return 'c("' + nodeName + '")';
          }) +
            ');return n}'
                                                         )(html5, data.frag);
        }

        /*--------------------------------------------------------------------------*/

        /**
         * Shivs the given document.
         * @memberOf html5
         * @param {Document} ownerDocument The document to shiv.
         * @returns {Document} The shived document.
         */
        function shivDocument(ownerDocument) {
          if (!ownerDocument) {
            ownerDocument = document;
          }
          var data = getExpandoData(ownerDocument);

          if (html5.shivCSS && !supportsHtml5Styles && !data.hasCSS) {
            data.hasCSS = !!addStyleSheet(ownerDocument,
                                          // corrects block display not defined in IE6/7/8/9
                                          'article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}' +
                                            // adds styling not present in IE6/7/8/9
                                            'mark{background:#FF0;color:#000}' +
                                            // hides non-rendered elements
                                            'template{display:none}'
                                         );
          }
          if (!supportsUnknownElements) {
            shivMethods(ownerDocument, data);
          }
          return ownerDocument;
        }

        /*--------------------------------------------------------------------------*/

        /**
         * The `html5` object is exposed so that more elements can be shived and
         * existing shiving can be detected on iframes.
         * @type Object
         * @example
         *
         * // options can be changed before the script is included
         * html5 = { 'elements': 'mark section', 'shivCSS': false, 'shivMethods': false };
         */
        var html5 = {

          /**
           * An array or space separated string of node names of the elements to shiv.
           * @memberOf html5
           * @type Array|String
           */
          'elements': options.elements || 'abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video',

          /**
           * current version of html5shiv
           */
          'version': version,

          /**
           * A flag to indicate that the HTML5 style sheet should be inserted.
           * @memberOf html5
           * @type Boolean
           */
          'shivCSS': (options.shivCSS !== false),

          /**
           * Is equal to true if a browser supports creating unknown/HTML5 elements
           * @memberOf html5
           * @type boolean
           */
          'supportsUnknownElements': supportsUnknownElements,

          /**
           * A flag to indicate that the document's `createElement` and `createDocumentFragment`
           * methods should be overwritten.
           * @memberOf html5
           * @type Boolean
           */
          'shivMethods': (options.shivMethods !== false),

          /**
           * A string to describe the type of `html5` object ("default" or "default print").
           * @memberOf html5
           * @type String
           */
          'type': 'default',

          // shivs the document according to the specified `html5` object options
          'shivDocument': shivDocument,

          //creates a shived element
          createElement: createElement,

          //creates a shived documentFragment
          createDocumentFragment: createDocumentFragment
        };

        /*--------------------------------------------------------------------------*/

        // expose html5
        window.html5 = html5;

        // shiv the document
        shivDocument(document);

    }(this, document));
    /*>>shiv*/

    // Assign private properties to the return object with prefix
    Modernizr._version      = version;

    // expose these for the plugin API. Look in the source for how to join() them against your input
    /*>>prefixes*/
    Modernizr._prefixes     = prefixes;
    /*>>prefixes*/
    /*>>domprefixes*/
    Modernizr._domPrefixes  = domPrefixes;
    Modernizr._cssomPrefixes  = cssomPrefixes;
    /*>>domprefixes*/

    /*>>mq*/
    // Modernizr.mq tests a given media query, live against the current state of the window
    // A few important notes:
    //   * If a browser does not support media queries at all (eg. oldIE) the mq() will always return false
    //   * A max-width or orientation query will be evaluated against the current state, which may change later.
    //   * You must specify values. Eg. If you are testing support for the min-width media query use:
    //       Modernizr.mq('(min-width:0)')
    // usage:
    // Modernizr.mq('only screen and (max-width:768)')
    Modernizr.mq            = testMediaQuery;
    /*>>mq*/

    /*>>hasevent*/
    // Modernizr.hasEvent() detects support for a given event, with an optional element to test on
    // Modernizr.hasEvent('gesturestart', elem)
    Modernizr.hasEvent      = isEventSupported;
    /*>>hasevent*/

    /*>>testprop*/
    // Modernizr.testProp() investigates whether a given style property is recognized
    // Note that the property names must be provided in the camelCase variant.
    // Modernizr.testProp('pointerEvents')
    Modernizr.testProp      = function(prop){
        return testProps([prop]);
    };
    /*>>testprop*/

    /*>>testallprops*/
    // Modernizr.testAllProps() investigates whether a given style property,
    //   or any of its vendor-prefixed variants, is recognized
    // Note that the property names must be provided in the camelCase variant.
    // Modernizr.testAllProps('boxSizing')
    Modernizr.testAllProps  = testPropsAll;
    /*>>testallprops*/


    /*>>teststyles*/
    // Modernizr.testStyles() allows you to add custom styles to the document and test an element afterwards
    // Modernizr.testStyles('#modernizr { position:absolute }', function(elem, rule){ ... })
    Modernizr.testStyles    = injectElementWithStyles;
    /*>>teststyles*/


    /*>>prefixed*/
    // Modernizr.prefixed() returns the prefixed or nonprefixed property name variant of your input
    // Modernizr.prefixed('boxSizing') // 'MozBoxSizing'

    // Properties must be passed as dom-style camelcase, rather than `box-sizing` hypentated style.
    // Return values will also be the camelCase variant, if you need to translate that to hypenated style use:
    //
    //     str.replace(/([A-Z])/g, function(str,m1){ return '-' + m1.toLowerCase(); }).replace(/^ms-/,'-ms-');

    // If you're trying to ascertain which transition end event to bind to, you might do something like...
    //
    //     var transEndEventNames = {
    //       'WebkitTransition' : 'webkitTransitionEnd',
    //       'MozTransition'    : 'transitionend',
    //       'OTransition'      : 'oTransitionEnd',
    //       'msTransition'     : 'MSTransitionEnd',
    //       'transition'       : 'transitionend'
    //     },
    //     transEndEventName = transEndEventNames[ Modernizr.prefixed('transition') ];

    Modernizr.prefixed      = function(prop, obj, elem){
      if(!obj) {
        return testPropsAll(prop, 'pfx');
      } else {
        // Testing DOM property e.g. Modernizr.prefixed('requestAnimationFrame', window) // 'mozRequestAnimationFrame'
        return testPropsAll(prop, obj, elem);
      }
    };
    /*>>prefixed*/


    /*>>cssclasses*/
    // Remove "no-js" class from <html> element, if it exists:
    docElement.className = docElement.className.replace(/(^|\s)no-js(\s|$)/, '$1$2') +

                            // Add the new classes to the <html> element.
                            (enableClasses ? ' js ' + classes.join(' ') : '');
    /*>>cssclasses*/

    return Modernizr;

})(this, this.document);

  /*
 * jQuery Foundation Joyride Plugin 2.1
 * http://foundation.zurb.com
 * Copyright 2013, ZURB
 * Free to use under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
*/

/*jslint unparam: true, browser: true, indent: 2 */

;(function ($, window, undefined) {
  'use strict';

  var defaults = {
      'version'              : '2.1',
      'tipLocation'          : 'bottom',  // 'top' or 'bottom' in relation to parent
      'nubPosition'          : 'auto',    // override on a per tooltip bases
      'scroll'               : true,      // whether to scroll to tips
      'scrollSpeed'          : 300,       // Page scrolling speed in milliseconds
      'timer'                : 0,         // 0 = no timer , all other numbers = timer in milliseconds
      'autoStart'            : false,     // true or false - false tour starts when restart called
      'startTimerOnClick'    : true,      // true or false - true requires clicking the first button start the timer
      'startOffset'          : 0,         // the index of the tooltip you want to start on (index of the li)
      'nextButton'           : true,      // true or false to control whether a next button is used
      'tipAnimation'         : 'fade',    // 'pop' or 'fade' in each tip
      'pauseAfter'           : [],        // array of indexes where to pause the tour after
      'tipAnimationFadeSpeed': 300,       // when tipAnimation = 'fade' this is speed in milliseconds for the transition
      'cookieMonster'        : false,     // true or false to control whether cookies are used
      'cookieName'           : 'joyride', // Name the cookie you'll use
      'cookieDomain'         : false,     // Will this cookie be attached to a domain, ie. '.notableapp.com'
      'cookiePath'           : false,     // Set to '/' if you want the cookie for the whole website
      'localStorage'         : false,     // true or false to control whether localstorage is used
      'localStorageKey'      : 'joyride', // Keyname in localstorage
      'tipContainer'         : 'body',    // Where will the tip be attached
      'modal'                : false,     // Whether to cover page with modal during the tour
      'expose'               : false,     // Whether to expose the elements at each step in the tour (requires modal:true)
      'postExposeCallback'   : $.noop,    // A method to call after an element has been exposed
      'preRideCallback'      : $.noop,    // A method to call before the tour starts (passed index, tip, and cloned exposed element)
      'postRideCallback'     : $.noop,    // A method to call once the tour closes (canceled or complete)
      'preStepCallback'      : $.noop,    // A method to call before each step
      'postStepCallback'     : $.noop,    // A method to call after each step
      'postCalculateCallback'     : $.noop,    // A method to call after each step
      'template' : { // HTML segments for tip layout
        'link'    : '<a href="#close" class="joyride-close-tip"></a>',
        'timer'   : '<div class="joyride-timer-indicator-wrap"><span class="joyride-timer-indicator"></span></div>',
        'tip'     : '<div class="joyride-tip-guide"><span class="joyride-nub"></span></div>',
        'wrapper' : '<div class="joyride-content-wrapper" role="dialog"></div>',
        'button'  : '<a href="#" class="joyride-next-tip"></a>',
        'modal'   : '<div class="joyride-modal-bg"></div>',
        'expose'  : '<div class="joyride-expose-wrapper"></div>',
        'exposeCover': '<div class="joyride-expose-cover"></div>'
      }
    },

    Modernizr = Modernizr || false,

    settings = {},

    methods = {

      init : function (opts) {
        return this.each(function () {

          if ($.isEmptyObject(settings)) {
            settings = $.extend(true, defaults, opts);

            // non configurable settings
            settings.document = window.document;
            settings.$document = $(settings.document);
            settings.$window = $(window);
            settings.$content_el = $(this);
            settings.$body = $(settings.tipContainer);
            settings.body_offset = $(settings.tipContainer).position();
            settings.$tip_content = $('> li', settings.$content_el);
            settings.paused = false;
            settings.attempts = 0;

            settings.tipLocationPatterns = {
              top: ['bottom'],
              bottom: [], // bottom should not need to be repositioned
              left: ['right', 'top', 'bottom'],
              right: ['left', 'top', 'bottom']
            };

            // are we using jQuery 1.7+
            methods.jquery_check();

            // can we create cookies?
            if (!$.isFunction($.cookie)) {
              settings.cookieMonster = false;
            }

            // generate the tips and insert into dom.
            if ( (!settings.cookieMonster || !$.cookie(settings.cookieName) ) &&
              (!settings.localStorage || !methods.support_localstorage() || !localStorage.getItem(settings.localStorageKey) ) ) {

              settings.$tip_content.each(function (index) {
                methods.create({$li : $(this), index : index});
              });

              // show first tip
              if(settings.autoStart)
              {
                if (!settings.startTimerOnClick && settings.timer > 0) {
                  methods.show('init');
                  methods.startTimer();
                } else {
                  methods.show('init');
                }
              }

            }

            settings.$document.on('click.joyride', '.joyride-next-tip, .joyride-modal-bg', function (e) {
              e.preventDefault();

              if (settings.$li.next().length < 1) {
                methods.end();
              } else if (settings.timer > 0) {
                clearTimeout(settings.automate);
                methods.hide();
                methods.show();
                methods.startTimer();
              } else {
                methods.hide();
                methods.show();
              }

            });

            settings.$document.on('click.joyride', '.joyride-close-tip', function (e) {
              e.preventDefault();
              methods.end(true /* isAborted */);
            });

            settings.$window.bind('resize.joyride', function (e) {
              if(settings.$li){
              if(settings.exposed && settings.exposed.length>0){
                var $els = $(settings.exposed);
                $els.each(function(){
                  var $this = $(this);
                  methods.un_expose($this);
                  methods.expose($this);
                });
              }
              if (methods.is_phone()) {
                methods.pos_phone();
              } else {
                methods.pos_default();
              }
              }
            });
          } else {
            methods.restart();
          }

        });
      },

      // call this method when you want to resume the tour
      resume : function () {
        methods.set_li();
        methods.show();
      },

      nextTip: function(){
            if (settings.$li.next().length < 1) {
            methods.end();
            } else if (settings.timer > 0) {
            clearTimeout(settings.automate);
            methods.hide();
            methods.show();
            methods.startTimer();
            } else {
            methods.hide();
            methods.show();
            }
      },

      tip_template : function (opts) {
        var $blank, content, $wrapper;

        opts.tip_class = opts.tip_class || '';

        $blank = $(settings.template.tip).addClass(opts.tip_class);
        content = $.trim($(opts.li).html()) +
          methods.button_text(opts.button_text) +
          settings.template.link +
          methods.timer_instance(opts.index);

        $wrapper = $(settings.template.wrapper);
        if (opts.li.attr('data-aria-labelledby')) {
          $wrapper.attr('aria-labelledby', opts.li.attr('data-aria-labelledby'))
        }
        if (opts.li.attr('data-aria-describedby')) {
          $wrapper.attr('aria-describedby', opts.li.attr('data-aria-describedby'))
        }
        $blank.append($wrapper);
        $blank.first().attr('data-index', opts.index);
        $('.joyride-content-wrapper', $blank).append(content);

        return $blank[0];
      },

      timer_instance : function (index) {
        var txt;

        if ((index === 0 && settings.startTimerOnClick && settings.timer > 0) || settings.timer === 0) {
          txt = '';
        } else {
          txt = methods.outerHTML($(settings.template.timer)[0]);
        }
        return txt;
      },

      button_text : function (txt) {
        if (settings.nextButton) {
          txt = $.trim(txt) || 'Next';
          txt = methods.outerHTML($(settings.template.button).append(txt)[0]);
        } else {
          txt = '';
        }
        return txt;
      },

      create : function (opts) {
        // backwards compatibility with data-text attribute
        var buttonText = opts.$li.attr('data-button') || opts.$li.attr('data-text'),
          tipClass = opts.$li.attr('class'),
          $tip_content = $(methods.tip_template({
            tip_class : tipClass,
            index : opts.index,
            button_text : buttonText,
            li : opts.$li
          }));

        $(settings.tipContainer).append($tip_content);
      },

      show : function (init) {
        var opts = {}, ii, opts_arr = [], opts_len = 0, p,
            $timer = null;

        // are we paused?
        if (settings.$li === undefined || ($.inArray(settings.$li.index(), settings.pauseAfter) === -1)) {

          // don't go to the next li if the tour was paused
          if (settings.paused) {
            settings.paused = false;
          } else {
            methods.set_li(init);
          }

          settings.attempts = 0;

          if (settings.$li.length && settings.$target.length > 0) {
            if(init){ //run when we first start
                settings.preRideCallback(settings.$li.index(), settings.$next_tip );
                if(settings.modal){
                    methods.show_modal();
                }
            }
            settings.preStepCallback(settings.$li.index(), settings.$next_tip );

            // parse options
            opts_arr = (settings.$li.data('options') || ':').split(';');
            opts_len = opts_arr.length;
            for (ii = opts_len - 1; ii >= 0; ii--) {
              p = opts_arr[ii].split(':');

              if (p.length === 2) {
                opts[$.trim(p[0])] = $.trim(p[1]);
              }
            }
            settings.tipSettings = $.extend({}, settings, opts);
            settings.tipSettings.tipLocationPattern = settings.tipLocationPatterns[settings.tipSettings.tipLocation];

            if(settings.modal && settings.expose){
              methods.expose();
            }

            // scroll if not modal
            if (!/body/i.test(settings.$target.selector) && settings.scroll) {
              methods.scroll_to();
            }

            if (methods.is_phone()) {
              methods.pos_phone(true);
            } else {
              methods.pos_default(true);
            }

            $timer = $('.joyride-timer-indicator', settings.$next_tip);

            if (/pop/i.test(settings.tipAnimation)) {

              $timer.outerWidth(0);

              if (settings.timer > 0) {

                settings.$next_tip.show();
                $timer.animate({
                  width: $('.joyride-timer-indicator-wrap', settings.$next_tip).outerWidth()
                }, settings.timer);

              } else {

                settings.$next_tip.show();

              }


            } else if (/fade/i.test(settings.tipAnimation)) {

              $timer.outerWidth(0);

              if (settings.timer > 0) {

                settings.$next_tip.fadeIn(settings.tipAnimationFadeSpeed);

                settings.$next_tip.show();
                $timer.animate({
                  width: $('.joyride-timer-indicator-wrap', settings.$next_tip).outerWidth()
                }, settings.timer);

              } else {

                settings.$next_tip.fadeIn(settings.tipAnimationFadeSpeed);

              }
            }

            settings.$current_tip = settings.$next_tip;
            // Focus next button for keyboard users.
            $('.joyride-next-tip', settings.$current_tip).focus();
            methods.tabbable(settings.$current_tip);
          // skip non-existent targets
          } else if (settings.$li && settings.$target.length < 1) {

            methods.show();

          } else {

            methods.end();

          }
        } else {

          settings.paused = true;

        }

      },

      // detect phones with media queries if supported.
      is_phone : function () {
        if (Modernizr) {
          return Modernizr.mq('only screen and (max-width: 767px)');
        }

        return (settings.$window.width() < 767) ? true : false;
      },

      support_localstorage : function () {
        if (Modernizr) {
          return Modernizr.localstorage;
        } else {
          return !!window.localStorage;
        }
      },

      hide : function () {
        if(settings.modal && settings.expose){
          methods.un_expose();
        }
        if(!settings.modal){
        $('.joyride-modal-bg').hide();
        }
        settings.$current_tip.hide();
        settings.postStepCallback(settings.$li.index(), settings.$current_tip);
      },

      set_li : function (init) {
        if (init) {
          settings.$li = settings.$tip_content.eq(settings.startOffset);
          methods.set_next_tip();
          settings.$current_tip = settings.$next_tip;
        } else {
          settings.$li = settings.$li.next();
          methods.set_next_tip();
        }

        methods.set_target();
      },

      set_next_tip : function () {
        settings.$next_tip = $('.joyride-tip-guide[data-index=' + settings.$li.index() + ']');
      },

      set_target : function () {
        var cl = settings.$li.attr('data-class'),
            id = settings.$li.attr('data-id'),
            $sel = function () {
              if (id) {
                return $(settings.document.getElementById(id));
              } else if (cl) {
                return $('.' + cl).filter(":visible").first();
              } else {
                return $('body');
              }
            };

        settings.$target = $sel();
      },

      scroll_to : function () {
        var window_half, tipOffset;

        window_half = settings.$window.height() / 2;
        tipOffset = Math.ceil(settings.$target.offset().top - window_half + settings.$next_tip.outerHeight());

        $("html, body").stop().animate({
          scrollTop: tipOffset
        }, settings.scrollSpeed);
      },

      paused : function () {
        if (($.inArray((settings.$li.index() + 1), settings.pauseAfter) === -1)) {
          return true;
        }

        return false;
      },

      destroy : function () {
        if(!$.isEmptyObject(settings)){
        settings.$document.off('.joyride');
        }

        $(window).off('.joyride');
        $('.joyride-close-tip, .joyride-next-tip, .joyride-modal-bg').off('.joyride');
        $('.joyride-tip-guide, .joyride-modal-bg').remove();
        clearTimeout(settings.automate);
        settings = {};
      },

      restart : function () {
        if(!settings.autoStart)
        {
          if (!settings.startTimerOnClick && settings.timer > 0) {
            methods.show('init');
            methods.startTimer();
          } else {
            methods.show('init');
          }
          settings.autoStart = true;
        }
        else
        {
        methods.hide();
        settings.$li = undefined;
        methods.show('init');
        }
      },

      pos_default : function (init) {
        var half_fold = Math.ceil(settings.$window.height() / 2),
            tip_position = settings.$next_tip.offset(),
            $nub = $('.joyride-nub', settings.$next_tip),
            nub_width = Math.ceil($nub.outerWidth() / 2),
            nub_height = Math.ceil($nub.outerHeight() / 2),
            toggle = init || false;

        // tip must not be "display: none" to calculate position
        if (toggle) {
          settings.$next_tip.css('visibility', 'hidden');
          settings.$next_tip.show();
        }

        if (!/body/i.test(settings.$target.selector)) {
            var
              topAdjustment = settings.tipSettings.tipAdjustmentY ? parseInt(settings.tipSettings.tipAdjustmentY) : 0,
              leftAdjustment = settings.tipSettings.tipAdjustmentX ? parseInt(settings.tipSettings.tipAdjustmentX) : 0;
              var position = "";
            if (methods.bottom()) {
              settings.$next_tip.css({
                top: (settings.$target.offset().top + nub_height + settings.$target.outerHeight() + topAdjustment),
                left: settings.$target.offset().left + leftAdjustment});

              if (/right/i.test(settings.tipSettings.nubPosition)) {
                settings.$next_tip.css('left', settings.$target.offset().left - settings.$next_tip.outerWidth() + settings.$target.outerWidth());
              }

              methods.nub_position($nub, settings.tipSettings.nubPosition, 'top');
              position = 'bottom';
            } else if (methods.top()) {

              settings.$next_tip.css({
                top: (settings.$target.offset().top - settings.$next_tip.outerHeight() - nub_height + topAdjustment),
                left: settings.$target.offset().left + leftAdjustment});

              methods.nub_position($nub, settings.tipSettings.nubPosition, 'bottom');
              position = 'top';
            } else if (methods.right()) {

              settings.$next_tip.css({
                top: settings.$target.offset().top + topAdjustment,
                left: (settings.$target.outerWidth() + settings.$target.offset().left + nub_width) + leftAdjustment});

              methods.nub_position($nub, settings.tipSettings.nubPosition, 'left');
              position = 'right';
            } else if (methods.left()) {

              settings.$next_tip.css({
                top: settings.$target.offset().top + topAdjustment,
                left: (settings.$target.offset().left - settings.$next_tip.outerWidth() - nub_width) + leftAdjustment});

              methods.nub_position($nub, settings.tipSettings.nubPosition, 'right');
              position = 'left';
            }

            if (!methods.visible(methods.corners(settings.$next_tip)) && settings.attempts < settings.tipSettings.tipLocationPattern.length) {

              $nub.removeClass('bottom')
                .removeClass('top')
                .removeClass('right')
                .removeClass('left');

              settings.tipSettings.tipLocation = settings.tipSettings.tipLocationPattern[settings.attempts];

              settings.attempts++;

              methods.pos_default(true);

            }
            settings.postCalculateCallback(settings.$li, settings.$next_tip, position);
        } else if (settings.$li.length) {

          methods.pos_modal($nub);

        }

        if (toggle) {
          settings.$next_tip.hide();
          settings.$next_tip.css('visibility', 'visible');
        }

      },

      pos_phone : function (init) {
        var tip_height = settings.$next_tip.outerHeight(),
            tip_offset = settings.$next_tip.offset(),
            target_height = settings.$target.outerHeight(),
            $nub = $('.joyride-nub', settings.$next_tip),
            nub_height = Math.ceil($nub.outerHeight() / 2),
            toggle = init || false;

        $nub.removeClass('bottom')
          .removeClass('top')
          .removeClass('right')
          .removeClass('left');

        if (toggle) {
          settings.$next_tip.css('visibility', 'hidden');
          settings.$next_tip.show();
        }

        if (!/body/i.test(settings.$target.selector)) {

          if (methods.top()) {

              settings.$next_tip.offset({top: settings.$target.offset().top - tip_height - nub_height});
              $nub.addClass('bottom');

          } else {

            settings.$next_tip.offset({top: settings.$target.offset().top + target_height + nub_height});
            $nub.addClass('top');

          }

        } else if (settings.$li.length) {

          methods.pos_modal($nub);

        }

        if (toggle) {
          settings.$next_tip.hide();
          settings.$next_tip.css('visibility', 'visible');
        }
      },

      pos_modal : function ($nub) {
        methods.center();
        $nub.hide();

        methods.show_modal();

      },

      show_modal : function() {
        if ($('.joyride-modal-bg').length < 1) {
            $('body').append(settings.template.modal).show();
        }

        if (/pop/i.test(settings.tipAnimation)) {
          $('.joyride-modal-bg').show();
        } else {
          $('.joyride-modal-bg').fadeIn(settings.tipAnimationFadeSpeed);
        }
      },

      expose: function(){
        var expose,
          exposeCover,
          el,
          origCSS,
          randId = 'expose-'+Math.floor(Math.random()*10000);
        if (arguments.length>0 && arguments[0] instanceof $){
          el = arguments[0];
        } else if(settings.$target && !/body/i.test(settings.$target.selector)){
          el = settings.$target;
        }  else {
          return false;
        }
        if(el.length < 1){
          if(window.console){
            console.error('element not valid', el);
          }
          return false;
        }
        expose = $(settings.template.expose);
        settings.$body.append(expose);
        expose.css({
          top: el.offset().top,
          left: el.offset().left,
          width: el.outerWidth(true),
          height: el.outerHeight(true)
        });
        exposeCover = $(settings.template.exposeCover);
        origCSS = {
                  zIndex: el.css('z-index'),
                  position: el.css('position')
                  };
        el.css('z-index',expose.css('z-index')*1+1);
        if(origCSS.position == 'static'){
          el.css('position','relative');
        }
        el.data('expose-css',origCSS);
        exposeCover.css({
          top: el.offset().top,
          left: el.offset().left,
          width: el.outerWidth(true),
          height: el.outerHeight(true)
        });
        settings.$body.append(exposeCover);
        expose.addClass(randId);
        exposeCover.addClass(randId);
        if(settings.tipSettings['exposeClass']){
          expose.addClass(settings.tipSettings['exposeClass']);
          exposeCover.addClass(settings.tipSettings['exposeClass']);
        }
        el.data('expose', randId);
        settings.postExposeCallback(settings.$li.index(), settings.$next_tip, el);
        methods.add_exposed(el);
      },

      un_expose: function(){
        var exposeId,
          el,
          expose ,
          origCSS,
          clearAll = false;
        if (arguments.length>0 && arguments[0] instanceof $){
          el = arguments[0];
        } else if(settings.$target && !/body/i.test(settings.$target.selector)){
          el = settings.$target;
        }  else {
          return false;
        }
        if(el.length < 1){
          if(window.console){
            console.error('element not valid', el);
          }
          return false;
        }
        exposeId = el.data('expose');
        expose = $('.'+exposeId);
        if(arguments.length>1){
          clearAll = arguments[1];
        }
        if(clearAll === true){
          $('.joyride-expose-wrapper,.joyride-expose-cover').remove();
        } else {
          expose.remove();
        }
        origCSS = el.data('expose-css');
        if(origCSS.zIndex == 'auto'){
          el.css('z-index', '');
        } else {
          el.css('z-index',origCSS.zIndex);
        }
        if(origCSS.position != el.css('position')){
          if(origCSS.position == 'static'){// this is default, no need to set it.
            el.css('position', '');
          } else {
            el.css('position',origCSS.position);
          }
        }
        el.removeData('expose');
        el.removeData('expose-z-index');
        methods.remove_exposed(el);
      },

      add_exposed: function(el){
        settings.exposed = settings.exposed || [];
        if(el instanceof $){
          settings.exposed.push(el[0]);
        } else if(typeof el == 'string'){
          settings.exposed.push(el);
        }
      },

      remove_exposed: function(el){
        var search;
        if(el instanceof $){
          search = el[0]
        } else if (typeof el == 'string'){
          search = el;
        }
        settings.exposed = settings.exposed || [];
        for(var i=0; i<settings.exposed.length; i++){
          if(settings.exposed[i] == search){
            settings.exposed.splice(i,1);
            return;
          }
        }
      },

      center : function () {
        var $w = settings.$window;

        settings.$next_tip.css({
          top : ((($w.height() - settings.$next_tip.outerHeight()) / 2) + $w.scrollTop()),
          left : ((($w.width() - settings.$next_tip.outerWidth()) / 2) + $w.scrollLeft())
        });

        return true;
      },

      bottom : function () {
        return /bottom/i.test(settings.tipSettings.tipLocation);
      },

      top : function () {
        return /top/i.test(settings.tipSettings.tipLocation);
      },

      right : function () {
        return /right/i.test(settings.tipSettings.tipLocation);
      },

      left : function () {
        return /left/i.test(settings.tipSettings.tipLocation);
      },

      corners : function (el) {
        var w = settings.$window,
            window_half = w.height() / 2,
            tipOffset = Math.ceil(settings.$target.offset().top - window_half + settings.$next_tip.outerHeight()),//using this to calculate since scroll may not have finished yet.
            right = w.width() + w.scrollLeft(),
            offsetBottom =  w.height() + tipOffset,
            bottom = w.height() + w.scrollTop(),
            top = w.scrollTop();

            if(tipOffset < top){
              if (tipOffset <0 ){
                top = 0;
              } else {
                top = tipOffset;
              }
            }

            if(offsetBottom > bottom){
              bottom = offsetBottom;
            }

        return [
          el.offset().top < top,
          right < el.offset().left + el.outerWidth(),
          bottom < el.offset().top + el.outerHeight(),
          w.scrollLeft() > el.offset().left
        ];
      },

      visible : function (hidden_corners) {
        var i = hidden_corners.length;

        while (i--) {
          if (hidden_corners[i]) return false;
        }

        return true;
      },

      nub_position : function (nub, pos, def) {
        if (pos === 'auto') {
          nub.addClass(def);
        } else {
          nub.addClass(pos);
        }
      },

      startTimer : function () {
        if (settings.$li.length) {
          settings.automate = setTimeout(function () {
            methods.hide();
            methods.show();
            methods.startTimer();
          }, settings.timer);
        } else {
          clearTimeout(settings.automate);
        }
      },

      end : function (isAborted) {
        isAborted = isAborted || false;

        // Unbind resize events.
        if (isAborted) {
          settings.$window.unbind('resize.joyride');
        }

        if (settings.cookieMonster) {
          $.cookie(settings.cookieName, 'ridden', { expires: 365, domain: settings.cookieDomain, path: settings.cookiePath });
        }

        if (settings.localStorage) {
          localStorage.setItem(settings.localStorageKey, true);
        }

        if (settings.timer > 0) {
          clearTimeout(settings.automate);
        }
        if(settings.modal && settings.expose){
          methods.un_expose();
        }
        if (settings.$current_tip) {
          settings.$current_tip.hide();
        }
        if (settings.$li) {
          settings.postStepCallback(settings.$li.index(), settings.$current_tip, isAborted);
          settings.postRideCallback(settings.$li.index(), settings.$current_tip, isAborted);
        }
        $('.joyride-modal-bg').hide();
      },

      jquery_check : function () {
        // define on() and off() for older jQuery
        if (!$.isFunction($.fn.on)) {

          $.fn.on = function (types, sel, fn) {

            return this.delegate(sel, types, fn);

          };

          $.fn.off = function (types, sel, fn) {

            return this.undelegate(sel, types, fn);

          };

          return false;
        }

        return true;
      },

      outerHTML : function (el) {
        // support FireFox < 11
        return el.outerHTML || new XMLSerializer().serializeToString(el);
      },

      version : function () {
        return settings.version;
      },

      tabbable : function (el) {
        $(el).on('keydown', function( event ) {
          if (!event.isDefaultPrevented() && event.keyCode &&
              // Escape key.
              event.keyCode === 27 ) {
            event.preventDefault();
            methods.end(true /* isAborted */);
            return;
          }

          // Prevent tabbing out of tour items.
          if ( event.keyCode !== 9 ) {
            return;
          }
          var tabbables = $(el).find(":tabbable"),
            first = tabbables.filter(":first"),
            last  = tabbables.filter(":last");
          if ( event.target === last[0] && !event.shiftKey ) {
            first.focus( 1 );
            event.preventDefault();
          } else if ( event.target === first[0] && event.shiftKey ) {
            last.focus( 1 );
            event.preventDefault();
          }
        });
      }

    };

  $.fn.joyride = function (method) {
    if (methods[method]) {
      return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method) {
      return methods.init.apply(this, arguments);
    } else {
      $.error('Method ' +  method + ' does not exist on jQuery.joyride');
    }
  };

}(jQuery, this));

var guided_tour ={
    specialPages : {},
    cookie : 'guided_tour_cookie',
    start_cookie : 'guided_tour_cookie_started',
    page: '',
    isStarted: false,
    elements : [],
    start : function(page){
        var that = this;
        if(typeof generateTourElements === "function" && !docCookies.hasItem(this.cookie) 
                && docCookies.hasItem('viewed_cookie_policy')
                && ((page != 'home' && docCookies.hasItem(this.start_cookie)) || page == 'home')
          ) {
            this.elements = generateTourElements();
            this.page = page;
            switch(page) {
                case 'home':
                    this.elements[2].id = 'hbA';
                    this.elements[2].pos = 'right';
                    this.elements[2].scroll = 260;
                    this.elements[3].id = 'searchDiv';
                    this.elements[3].pos = 'top';
                    this.elements[3].scroll = 140;
                    this.elements[4].id = 'heading-title';
                    this.elements[4].pos = 'right';
                    this.elements[4].top = 20;
                    this.elements[4].scroll = 740;
                    this.elements[5].id = 'profile';
                    this.elements[5].scroll = 0;
                    this.elements[5].left = 30;
                    this.elements[6].id = 'brandSelect';
                    this.elements[6].scroll = 0;
                    this.generateTourHtml(this.elements, 0);
                    break;
                case 'brand':
                    this.elements[0].id = 'rubrics';
                    this.elements[0].scroll = 500;
                    this.elements[1].id = 'heading-title';
                    this.elements[1].pos = 'right';
                    this.elements[1].top = 10;
                    this.elements[1].scroll = 715;
                    this.elements[2].id = 'help';
                    this.elements[2].pos = 'top';
                    this.elements[2].scroll = 500;
                    this.generateTourHtml(this.elements, 1);
                    break;
                case 'identity':
                    this.elements[0].id = 'rubrics';
                    this.generateTourHtml(this.elements, 2);
                    break;
                case 'manuals':
                    this.elements[0].id = 'L2ID2094';
                    this.elements[0].scroll = 200;
                    this.generateTourHtml(this.elements, 3);
                    break;
                case 'content':
                    $('.inline_zoom').first().attr('id', 'first_inline_zoom');
                    $('.tooltip').first().attr('id', 'first_tooltip');
                    this.elements[0].id = 'basic_nav_closed';
                    this.elements[0].pos = 'right';
                    this.elements[0].scroll = 0;
                    this.elements[0].top = 20;
                    this.elements[1].id = 'dot_items';
                    this.elements[1].pos = 'left';
                    this.elements[2].id = 'print-icon';
                    this.elements[2].scroll = 200;
                    this.elements[2].pos = 'left';
                    this.elements[2].top = 20;
                    this.elements[3].id = 'links-icon';
                    this.elements[3].pos = 'left';
                    this.elements[3].top = 20;
                    this.elements[3].scroll = 200;
                    this.elements[4].id = 'first_inline_zoom';
                    this.elements[4].pos = 'left';
                    this.elements[4].scroll = 630;
                    this.elements[5].id = 'first_tooltip';
                    this.elements[5].pos = 'left';
                    this.elements[5].top = 30;
                    this.elements[5].scroll = 500;
                    this.elements[6].id = 'nav_button_right';
                    this.elements[6].pos = 'left';
                    this.generateTourHtml(this.elements, 4);
                    break;
                case 'practice':
                    this.elements[0].id = 'L2ID971';
                    //this.elements[0].pos = 'left';
                    this.elements[0].left = 312;
                    this.generateTourHtml(this.elements, 5);
                    break;
                case 'bp_content':
                    this.elements[0].id = 'basic_nav_closed';
                    this.elements[0].pos = 'right';
                    this.elements[0].scroll = 0;
                    this.elements[0].top = 20;   
                    this.elements[1].id = 'comment-form-header';
                    this.elements[1].pos = 'left';
                    this.elements[1].top = 15;
                    this.elements[1].scroll = 6200;
                    this.generateTourHtml(this.elements, 6);
                    break;
                case 'download':
                    this.elements[0].id = 'L2ID1384';
                    this.elements[0].left = 324;
                    this.generateTourHtml(this.elements, 7);
                    break;
                case 'd_content':
                    this.elements[0].id = 'basic_nav_closed';
                    this.elements[0].pos = 'right';
                    this.elements[0].scroll = 0;
                    this.elements[0].top = 20;
                    this.elements[1].id = 'downloadCol2';
                    this.elements[1].pos = 'right';
                    this.elements[1].scroll = 300;
                    this.elements[2].id = 'download_actions';
                    this.elements[2].scroll = 770;         
                    this.generateTourHtml(this.elements, 8);
                    break;
                default:

            }
            $('#joyRideTipContent').joyride({
              autoStart : true,
                scroll : false, 
                postStepCallback: function(index, currentTip){ // A method to call after each step
                    var scroll = this.$li.attr('data-after_scroll');
                    if($.isNumeric(scroll)){
                        $(window).scrollTop( scroll );
                        $(window).trigger('scroll');
                    }                
                },
                preStepCallback: function(index, next){  // A method to call before each step
                    var scroll = this.$li.attr('data-scroll');
                    if($.isNumeric(scroll)){
                        $(window).scrollTop( scroll );
                        console.log('Set Scroll: ' + scroll);
                        $(window).trigger('scroll');
                        console.log('After Trigger Scroll: ' + $(window).scrollTop());
                    }
                    if(that.page ==  'd_content' && index == that.elements.length - 1) {
                        $('div[data-index="' + index + '" ]').find('a:contains("Next")').css({"display":"none"});
                    }
                },
                postRideCallback: function(index, current, isAborted){ // A method to call once the tour closes (canceled or complete)
                    if(isAborted){
                        var weekFromNow = new Date();
                        weekFromNow.setDate( weekFromNow.getDate() + 7 );
                        docCookies.setItem(that.cookie, 1, weekFromNow, '/', null, true);
                        if(docCookies.hasItem(that.start_cookie)){
                            docCookies.removeItem(that.start_cookie, '/', null);
                        }
                        that.isStarted = false;
                    }else{
                        switch(that.page) {
                            case 'home':
                                $('#brandMenu a')[0].click();
                                break;
                            case 'brand':
                                $('#L2ID1717 a')[0].click();                            
                                break;
                            case 'identity':                                                        
                                $('#L2ID2094 a')[0].click();                            
                                break;
                            case 'manuals':
                                window.location = '/Daimler_Corporate_Logotype';
                                //$('.manuals .page:first-of-type a')[0].click();                            
                                break;
                            case 'content':                                                        
                                $('#L2ID971 a')[0].click();                            
                                break;
                            case 'practice':
                                window.location = '/Daimler_2016_Annual_Meeting';
                                //$('#oneperframe1 .page:first-of-type a')[0].click();                           
                                break;
                            case 'bp_content':                                                        
                                $('#L2ID1384 a')[0].click();                           
                                break;
                            case 'download':
                                window.location = '/Daimler_Brochures_InDesign_DINA4_Templates';
                                //$('.downloads .page:first-of-type a')[0].click();                       
                                break;
                            case 'd_content':
                                that.isStarted = false;
                                var weekFromNow = new Date();
                                weekFromNow.setDate( weekFromNow.getDate() + 7 );
                                docCookies.setItem(that.cookie, 1, weekFromNow, '/', null, true);
                                if(docCookies.hasItem(that.start_cookie)){
                                    docCookies.removeItem(that.start_cookie, '/', null);
                                }
                                break;
                            default:
                        }
                    }
                },
                preRideCallback: function(){ // A method to call before the tour starts (passed index, tip, and cloned exposed element)
                    var hourFromNow = new Date();
                    hourFromNow.setHours(hourFromNow.getHours() + 1);
                    docCookies.setItem(that.start_cookie, 1, hourFromNow, '/', null, true);
                    that.isStarted = true;
                },
                postCalculateCallback: function(li_element, currentTip, position){
                     var top = this.$li.attr('data-top');
                     if($.isNumeric(top) && (position == 'left' || position == 'right')){
                         var elTop = currentTip.css('top');
                         elTop = parseInt(elTop.substring(0, elTop.indexOf("px"))) - parseInt(top);
                         currentTip.css({
                             top: elTop + 'px'
                         })
                    }
                    var left = this.$li.attr('data-left');
                    if($.isNumeric(left) && position == 'bottom'){
                         var elLeft = currentTip.css('left');
                         elLeft = parseInt(elLeft.substring(0, elLeft.indexOf("px"))) - parseInt(left);
                         currentTip.css({
                             left: elLeft + 'px'
                         })
                         if(that.page == 'practice' || that.page ==  'download'){
                             currentTip.find('span.joyride-nub').css({'left': 'auto', 'right':'25px'});
                         }else{
                            currentTip.find('span.joyride-nub').css('left', '25px');
                            }
                    }
                }
                
                
            });
        }
    },
    generateTourHtml: function(elements, start){
        var html = '<ol id="joyRideTipContent">';
        var len = elements.length;
        for(var i= 0; i< len; i++){
            if(start == 0 && (i == 0 || i == 1)){
                if(i == 0){
                     html +=  '<li data-text="'+js_localize['guided_tour.button_start']+'" data-scroll="'+elements[i].scroll +'">'; 
                }else{
                     html +=  '<li data-text="'+js_localize['guided_tour.button_next']+'">'; 
                }
            }else if(start == 8 && i == len - 1){
                html +=  '<li>'; 
            }else{
                html +=  '<li data-left="'+elements[i].left+'" data-top= "'+elements[i].top+'" data-after_scroll = "'+elements[i].after_scroll +'" data-scroll="'+elements[i].scroll +'" data-id="'+ elements[i].id + '" data-text="'+js_localize['guided_tour.button_next']+'" data-options="tipLocation:'+elements[i].pos+'">';
            }        
            html += '<div class="bubble-container">';
            html += '<h2>'+ elements[i].header + '</h2>';
            html +=  '<div class="text">'+ elements[i].text + '</div>';
//            if(!(start == 0 && i == 0)){
//                html += '<div class="tour_pages">'+(start + i) +'/25</div>'
//            }
            if(start == 8 && i == len - 1){
                 html += '<a class="tour_link" href="/">'+js_localize['guided_tour.link_home']+'</a>';
            }
            html += '</div></li>';
        }
        html += '</ol>';
        $('body').append(html);
    }
};
/*!
 * sly 1.6.1 - 8th Aug 2015
 * https://github.com/darsain/sly
 *
 * Licensed under the MIT license.
 * http://opensource.org/licenses/MIT
 */

;(function ($, w, undefined) {
	'use strict';

	var pluginName = 'sly';
	var className  = 'Sly';
	var namespace  = pluginName;

	// Local WindowAnimationTiming interface
	var cAF = w.cancelAnimationFrame || w.cancelRequestAnimationFrame;
	var rAF = w.requestAnimationFrame;

	// Support indicators
	var transform, gpuAcceleration;

	// Other global values
	var $doc = $(document);
	var dragInitEvents = 'touchstart.' + namespace + ' mousedown.' + namespace;
	var dragMouseEvents = 'mousemove.' + namespace + ' mouseup.' + namespace;
	var dragTouchEvents = 'touchmove.' + namespace + ' touchend.' + namespace;
	var wheelEvent = (document.implementation.hasFeature('Event.wheel', '3.0') ? 'wheel.' : 'mousewheel.') + namespace;
	var clickEvent = 'click.' + namespace;
	var mouseDownEvent = 'mousedown.' + namespace;
	var interactiveElements = ['INPUT', 'SELECT', 'BUTTON', 'TEXTAREA'];
	var tmpArray = [];
	var time;

	// Math shorthands
	var abs = Math.abs;
	var sqrt = Math.sqrt;
	var pow = Math.pow;
	var round = Math.round;
	var max = Math.max;
	var min = Math.min;

	// Keep track of last fired global wheel event
	var lastGlobalWheel = 0;
	$doc.on(wheelEvent, function (event) {
		var sly = event.originalEvent[namespace];
		var time = +new Date();
		// Update last global wheel time, but only when event didn't originate
		// in Sly frame, or the origin was less than scrollHijack time ago
		if (!sly || sly.options.scrollHijack < time - lastGlobalWheel) lastGlobalWheel = time;
	});

	/**
	 * Sly.
	 *
	 * @class
	 *
	 * @param {Element} frame       DOM element of sly container.
	 * @param {Object}  options     Object with options.
	 * @param {Object}  callbackMap Callbacks map.
	 */
	function Sly(frame, options, callbackMap) {
		if (!(this instanceof Sly)) return new Sly(frame, options, callbackMap);

		// Extend options
		var o = $.extend({}, Sly.defaults, options);

		// Private variables
		var self = this;
		var parallax = isNumber(frame);

		// Frame
		var $frame = $(frame);
		var $slidee = o.slidee ? $(o.slidee).eq(0) : $frame.children().eq(0);
		var frameSize = 0;
		var slideeSize = 0;
		var pos = {
			start: 0,
			center: 0,
			end: 0,
			cur: 0,
			dest: 0
		};

		// Scrollbar
		var $sb = $(o.scrollBar).eq(0);
		var $handle = $sb.children().eq(0);
		var sbSize = 0;
		var handleSize = 0;
		var hPos = {
			start: 0,
			end: 0,
			cur: 0
		};

		// Pagesbar
		var $pb = $(o.pagesBar);
		var $pages = 0;
		var pages = [];

		// Items
		var $items = 0;
		var items = [];
		var rel = {
			firstItem: 0,
			lastItem: 0,
			centerItem: 0,
			activeItem: null,
			activePage: 0
		};

		// Styles
		var frameStyles = new StyleRestorer($frame[0]);
		var slideeStyles = new StyleRestorer($slidee[0]);
		var sbStyles = new StyleRestorer($sb[0]);
		var handleStyles = new StyleRestorer($handle[0]);

		// Navigation type booleans
		var basicNav = o.itemNav === 'basic';
		var forceCenteredNav = o.itemNav === 'forceCentered';
		var centeredNav = o.itemNav === 'centered' || forceCenteredNav;
		var itemNav = !parallax && (basicNav || centeredNav || forceCenteredNav);

		// Miscellaneous
		var $scrollSource = o.scrollSource ? $(o.scrollSource) : $frame;
		var $dragSource = o.dragSource ? $(o.dragSource) : $frame;
		var $forwardButton = $(o.forward);
		var $backwardButton = $(o.backward);
		var $prevButton = $(o.prev);
		var $nextButton = $(o.next);
		var $prevPageButton = $(o.prevPage);
		var $nextPageButton = $(o.nextPage);
		var callbacks = {};
		var last = {};
		var animation = {};
		var move = {};
		var dragging = {
			released: 1
		};
		var scrolling = {
			last: 0,
			delta: 0,
			resetTime: 200
		};
		var renderID = 0;
		var historyID = 0;
		var cycleID = 0;
		var continuousID = 0;
		var i, l;

		// Normalizing frame
		if (!parallax) {
			frame = $frame[0];
		}

		// Expose properties
		self.initialized = 0;
		self.frame = frame;
		self.slidee = $slidee[0];
		self.pos = pos;
		self.rel = rel;
		self.items = items;
		self.pages = pages;
		self.isPaused = 0;
		self.options = o;
		self.dragging = dragging;

		/**
		 * Loading function.
		 *
		 * Populate arrays, set sizes, bind events, ...
		 *
		 * @param {Boolean} [isInit] Whether load is called from within self.init().
		 * @return {Void}
		 */
		function load(isInit) {
			// Local variables
			var lastItemsCount = 0;
			var lastPagesCount = pages.length;

			// Save old position
			pos.old = $.extend({}, pos);

			// Reset global variables
			frameSize = parallax ? 0 : $frame[o.horizontal ? 'width' : 'height']();
			sbSize = $sb[o.horizontal ? 'width' : 'height']();
			slideeSize = parallax ? frame : $slidee[o.horizontal ? 'outerWidth' : 'outerHeight']();
			pages.length = 0;

			// Set position limits & relatives
			pos.start = 0;
			pos.end = max(slideeSize - frameSize, 0);

			// Sizes & offsets for item based navigations
			if (itemNav) {
				// Save the number of current items
				lastItemsCount = items.length;

				// Reset itemNav related variables
				$items = $slidee.children(o.itemSelector);
				items.length = 0;

				// Needed variables
				var paddingStart = getPx($slidee, o.horizontal ? 'paddingLeft' : 'paddingTop');
				var paddingEnd = getPx($slidee, o.horizontal ? 'paddingRight' : 'paddingBottom');
				var borderBox = $($items).css('boxSizing') === 'border-box';
				var areFloated = $items.css('float') !== 'none';
				var ignoredMargin = 0;
				var lastItemIndex = $items.length - 1;
				var lastItem;

				// Reset slideeSize
				slideeSize = 0;

				// Iterate through items
				$items.each(function (i, element) {
					// Item
					var $item = $(element);
					var rect = element.getBoundingClientRect();
					var itemSize = round(o.horizontal ? rect.width || rect.right - rect.left : rect.height || rect.bottom - rect.top);
					var itemMarginStart = getPx($item, o.horizontal ? 'marginLeft' : 'marginTop');
					var itemMarginEnd = getPx($item, o.horizontal ? 'marginRight' : 'marginBottom');
					var itemSizeFull = itemSize + itemMarginStart + itemMarginEnd;
					var singleSpaced = !itemMarginStart || !itemMarginEnd;
					var item = {};
					item.el = element;
					item.size = singleSpaced ? itemSize : itemSizeFull;
					item.half = item.size / 2;
					item.start = slideeSize + (singleSpaced ? itemMarginStart : 0);
					item.center = item.start - round(frameSize / 2 - item.size / 2);
					item.end = item.start - frameSize + item.size;

					// Account for slidee padding
					if (!i) {
						slideeSize += paddingStart;
					}

					// Increment slidee size for size of the active element
					slideeSize += itemSizeFull;

					// Try to account for vertical margin collapsing in vertical mode
					// It's not bulletproof, but should work in 99% of cases
					if (!o.horizontal && !areFloated) {
						// Subtract smaller margin, but only when top margin is not 0, and this is not the first element
						if (itemMarginEnd && itemMarginStart && i > 0) {
							slideeSize -= min(itemMarginStart, itemMarginEnd);
						}
					}

					// Things to be done on last item
					if (i === lastItemIndex) {
						item.end += paddingEnd;
						slideeSize += paddingEnd;
						ignoredMargin = singleSpaced ? itemMarginEnd : 0;
					}

					// Add item object to items array
					items.push(item);
					lastItem = item;
				});

				// Resize SLIDEE to fit all items
				$slidee[0].style[o.horizontal ? 'width' : 'height'] = (borderBox ? slideeSize: slideeSize - paddingStart - paddingEnd) + 'px';

				// Adjust internal SLIDEE size for last margin
				slideeSize -= ignoredMargin;

				// Set limits
				if (items.length) {
					pos.start =  items[0][forceCenteredNav ? 'center' : 'start'];
					pos.end = forceCenteredNav ? lastItem.center : frameSize < slideeSize ? lastItem.end : pos.start;
				} else {
					pos.start = pos.end = 0;
				}
			}

			// Calculate SLIDEE center position
			pos.center = round(pos.end / 2 + pos.start / 2);

			// Update relative positions
			updateRelatives();

			// Scrollbar
			if ($handle.length && sbSize > 0) {
				// Stretch scrollbar handle to represent the visible area
				if (o.dynamicHandle) {
					handleSize = pos.start === pos.end ? sbSize : round(sbSize * frameSize / slideeSize);
					handleSize = within(handleSize, o.minHandleSize, sbSize);
					$handle[0].style[o.horizontal ? 'width' : 'height'] = handleSize + 'px';
				} else {
					handleSize = $handle[o.horizontal ? 'outerWidth' : 'outerHeight']();
				}

				hPos.end = sbSize - handleSize;

				if (!renderID) {
					syncScrollbar();
				}
			}

			// Pages
			if (!parallax && frameSize > 0) {
				var tempPagePos = pos.start;
				var pagesHtml = '';

				// Populate pages array
				if (itemNav) {
					$.each(items, function (i, item) {
						if (forceCenteredNav) {
							pages.push(item.center);
						} else if (item.start + item.size > tempPagePos && tempPagePos <= pos.end) {
							tempPagePos = item.start;
							pages.push(tempPagePos);
							tempPagePos += frameSize;
							if (tempPagePos > pos.end && tempPagePos < pos.end + frameSize) {
								pages.push(pos.end);
							}
						}
					});
				} else {
					while (tempPagePos - frameSize < pos.end) {
						pages.push(tempPagePos);
						tempPagePos += frameSize;
					}
				}

				// Pages bar
				if ($pb[0] && lastPagesCount !== pages.length) {
					for (var i = 0; i < pages.length; i++) {
						pagesHtml += o.pageBuilder.call(self, i);
					}
					$pages = $pb.html(pagesHtml).children();
					$pages.eq(rel.activePage).addClass(o.activeClass);
				}
			}

			// Extend relative variables object with some useful info
			rel.slideeSize = slideeSize;
			rel.frameSize = frameSize;
			rel.sbSize = sbSize;
			rel.handleSize = handleSize;

			// Activate requested position
			if (itemNav) {
				if (isInit && o.startAt != null) {
					activate(o.startAt);
					self[centeredNav ? 'toCenter' : 'toStart'](o.startAt);
				}
				// Fix possible overflowing
				var activeItem = items[rel.activeItem];
				slideTo(centeredNav && activeItem ? activeItem.center : within(pos.dest, pos.start, pos.end));
			} else {
				if (isInit) {
					if (o.startAt != null) slideTo(o.startAt, 1);
				} else {
					// Fix possible overflowing
					slideTo(within(pos.dest, pos.start, pos.end));
				}
			}

			// Trigger load event
			trigger('load');
		}
		self.reload = function () { load(); };

		/**
		 * Animate to a position.
		 *
		 * @param {Int}  newPos    New position.
		 * @param {Bool} immediate Reposition immediately without an animation.
		 * @param {Bool} dontAlign Do not align items, use the raw position passed in first argument.
		 *
		 * @return {Void}
		 */
		function slideTo(newPos, immediate, dontAlign) {
			// Align items
			if (itemNav && dragging.released && !dontAlign) {
				var tempRel = getRelatives(newPos);
				var isNotBordering = newPos > pos.start && newPos < pos.end;

				if (centeredNav) {
					if (isNotBordering) {
						newPos = items[tempRel.centerItem].center;
					}
					if (forceCenteredNav && o.activateMiddle) {
						activate(tempRel.centerItem);
					}
				} else if (isNotBordering) {
					newPos = items[tempRel.firstItem].start;
				}
			}

			// Handle overflowing position limits
			if (dragging.init && dragging.slidee && o.elasticBounds) {
				if (newPos > pos.end) {
					newPos = pos.end + (newPos - pos.end) / 6;
				} else if (newPos < pos.start) {
					newPos = pos.start + (newPos - pos.start) / 6;
				}
			} else {
				newPos = within(newPos, pos.start, pos.end);
			}

			// Update the animation object
			animation.start = +new Date();
			animation.time = 0;
			animation.from = pos.cur;
			animation.to = newPos;
			animation.delta = newPos - pos.cur;
			animation.tweesing = dragging.tweese || dragging.init && !dragging.slidee;
			animation.immediate = !animation.tweesing && (immediate || dragging.init && dragging.slidee || !o.speed);

			// Reset dragging tweesing request
			dragging.tweese = 0;

			// Start animation rendering
			if (newPos !== pos.dest) {
				pos.dest = newPos;
				trigger('change');
				if (!renderID) {
					render();
				}
			}

			// Reset next cycle timeout
			resetCycle();

			// Synchronize states
			updateRelatives();
			updateButtonsState();
			syncPagesbar();
		}

		/**
		 * Render animation frame.
		 *
		 * @return {Void}
		 */
		function render() {
			if (!self.initialized) {
				return;
			}

			// If first render call, wait for next animationFrame
			if (!renderID) {
				renderID = rAF(render);
				if (dragging.released) {
					trigger('moveStart');
				}
				return;
			}

			// If immediate repositioning is requested, don't animate.
			if (animation.immediate) {
				pos.cur = animation.to;
			}
			// Use tweesing for animations without known end point
			else if (animation.tweesing) {
				animation.tweeseDelta = animation.to - pos.cur;
				// Fuck Zeno's paradox
				if (abs(animation.tweeseDelta) < 0.1) {
					pos.cur = animation.to;
				} else {
					pos.cur += animation.tweeseDelta * (dragging.released ? o.swingSpeed : o.syncSpeed);
				}
			}
			// Use tweening for basic animations with known end point
			else {
				animation.time = min(+new Date() - animation.start, o.speed);
				pos.cur = animation.from + animation.delta * $.easing[o.easing](animation.time/o.speed, animation.time, 0, 1, o.speed);
			}

			// If there is nothing more to render break the rendering loop, otherwise request new animation frame.
			if (animation.to === pos.cur) {
				pos.cur = animation.to;
				dragging.tweese = renderID = 0;
			} else {
				renderID = rAF(render);
			}

			trigger('move');

			// Update SLIDEE position
			if (!parallax) {
				if (transform) {
					$slidee[0].style[transform] = gpuAcceleration + (o.horizontal ? 'translateX' : 'translateY') + '(' + (-pos.cur) + 'px)';
				} else {
					$slidee[0].style[o.horizontal ? 'left' : 'top'] = -round(pos.cur) + 'px';
				}
			}

			// When animation reached the end, and dragging is not active, trigger moveEnd
			if (!renderID && dragging.released) {
				trigger('moveEnd');
			}

			syncScrollbar();
		}

		/**
		 * Synchronizes scrollbar with the SLIDEE.
		 *
		 * @return {Void}
		 */
		function syncScrollbar() {
			if ($handle.length) {
				hPos.cur = pos.start === pos.end ? 0 : (((dragging.init && !dragging.slidee) ? pos.dest : pos.cur) - pos.start) / (pos.end - pos.start) * hPos.end;
				hPos.cur = within(round(hPos.cur), hPos.start, hPos.end);
				if (last.hPos !== hPos.cur) {
					last.hPos = hPos.cur;
					if (transform) {
						$handle[0].style[transform] = gpuAcceleration + (o.horizontal ? 'translateX' : 'translateY') + '(' + hPos.cur + 'px)';
					} else {
						$handle[0].style[o.horizontal ? 'left' : 'top'] = hPos.cur + 'px';
					}
				}
			}
		}

		/**
		 * Synchronizes pagesbar with SLIDEE.
		 *
		 * @return {Void}
		 */
		function syncPagesbar() {
			if ($pages[0] && last.page !== rel.activePage) {
				last.page = rel.activePage;
				$pages.removeClass(o.activeClass).eq(rel.activePage).addClass(o.activeClass);
				trigger('activePage', last.page);
			}
		}

		/**
		 * Returns the position object.
		 *
		 * @param {Mixed} item
		 *
		 * @return {Object}
		 */
		self.getPos = function (item) {
			if (itemNav) {
				var index = getIndex(item);
				return index !== -1 ? items[index] : false;
			} else {
				var $item = $slidee.find(item).eq(0);

				if ($item[0]) {
					var offset = o.horizontal ? $item.offset().left - $slidee.offset().left : $item.offset().top - $slidee.offset().top;
					var size = $item[o.horizontal ? 'outerWidth' : 'outerHeight']();

					return {
						start: offset,
						center: offset - frameSize / 2 + size / 2,
						end: offset - frameSize + size,
						size: size
					};
				} else {
					return false;
				}
			}
		};

		/**
		 * Continuous move in a specified direction.
		 *
		 * @param  {Bool} forward True for forward movement, otherwise it'll go backwards.
		 * @param  {Int}  speed   Movement speed in pixels per frame. Overrides options.moveBy value.
		 *
		 * @return {Void}
		 */
		self.moveBy = function (speed) {
			move.speed = speed;
			// If already initiated, or there is nowhere to move, abort
			if (dragging.init || !move.speed || pos.cur === (move.speed > 0 ? pos.end : pos.start)) {
				return;
			}
			// Initiate move object
			move.lastTime = +new Date();
			move.startPos = pos.cur;
			// Set dragging as initiated
			continuousInit('button');
			dragging.init = 1;
			// Start movement
			trigger('moveStart');
			cAF(continuousID);
			moveLoop();
		};

		/**
		 * Continuous movement loop.
		 *
		 * @return {Void}
		 */
		function moveLoop() {
			// If there is nowhere to move anymore, stop
			if (!move.speed || pos.cur === (move.speed > 0 ? pos.end : pos.start)) {
				self.stop();
			}
			// Request new move loop if it hasn't been stopped
			continuousID = dragging.init ? rAF(moveLoop) : 0;
			// Update move object
			move.now = +new Date();
			move.pos = pos.cur + (move.now - move.lastTime) / 1000 * move.speed;
			// Slide
			slideTo(dragging.init ? move.pos : round(move.pos));
			// Normally, this is triggered in render(), but if there
			// is nothing to render, we have to do it manually here.
			if (!dragging.init && pos.cur === pos.dest) {
				trigger('moveEnd');
			}
			// Update times for future iteration
			move.lastTime = move.now;
		}

		/**
		 * Stops continuous movement.
		 *
		 * @return {Void}
		 */
		self.stop = function () {
			if (dragging.source === 'button') {
				dragging.init = 0;
				dragging.released = 1;
			}
		};

		/**
		 * Activate previous item.
		 *
		 * @return {Void}
		 */
		self.prev = function () {
			self.activate(rel.activeItem == null ? 0 : rel.activeItem - 1);
		};

		/**
		 * Activate next item.
		 *
		 * @return {Void}
		 */
		self.next = function () {
			self.activate(rel.activeItem == null ? 0 : rel.activeItem + 1);
		};

		/**
		 * Activate previous page.
		 *
		 * @return {Void}
		 */
		self.prevPage = function () {
			self.activatePage(rel.activePage - 1);
		};

		/**
		 * Activate next page.
		 *
		 * @return {Void}
		 */
		self.nextPage = function () {
			self.activatePage(rel.activePage + 1);
		};

		/**
		 * Slide SLIDEE by amount of pixels.
		 *
		 * @param {Int}  delta     Pixels/Items. Positive means forward, negative means backward.
		 * @param {Bool} immediate Reposition immediately without an animation.
		 *
		 * @return {Void}
		 */
		self.slideBy = function (delta, immediate) {
			if (!delta) {
				return;
			}
			if (itemNav) {
				self[centeredNav ? 'toCenter' : 'toStart'](
					within((centeredNav ? rel.centerItem : rel.firstItem) + o.scrollBy * delta, 0, items.length)
				);
			} else {
				slideTo(pos.dest + delta, immediate);
			}
		};

		/**
		 * Animate SLIDEE to a specific position.
		 *
		 * @param {Int}  pos       New position.
		 * @param {Bool} immediate Reposition immediately without an animation.
		 *
		 * @return {Void}
		 */
		self.slideTo = function (pos, immediate) {
			slideTo(pos, immediate);
		};

		/**
		 * Core method for handling `toLocation` methods.
		 *
		 * @param  {String} location
		 * @param  {Mixed}  item
		 * @param  {Bool}   immediate
		 *
		 * @return {Void}
		 */
		function to(location, item, immediate) {
			// Optional arguments logic
			if (type(item) === 'boolean') {
				immediate = item;
				item = undefined;
			}

			if (item === undefined) {
				slideTo(pos[location], immediate);
			} else {
				// You can't align items to sides of the frame
				// when centered navigation type is enabled
				if (centeredNav && location !== 'center') {
					return;
				}

				var itemPos = self.getPos(item);
				if (itemPos) {
					slideTo(itemPos[location], immediate, !centeredNav);
				}
			}
		}

		/**
		 * Animate element or the whole SLIDEE to the start of the frame.
		 *
		 * @param {Mixed} item      Item DOM element, or index starting at 0. Omitting will animate SLIDEE.
		 * @param {Bool}  immediate Reposition immediately without an animation.
		 *
		 * @return {Void}
		 */
		self.toStart = function (item, immediate) {
			to('start', item, immediate);
		};

		/**
		 * Animate element or the whole SLIDEE to the end of the frame.
		 *
		 * @param {Mixed} item      Item DOM element, or index starting at 0. Omitting will animate SLIDEE.
		 * @param {Bool}  immediate Reposition immediately without an animation.
		 *
		 * @return {Void}
		 */
		self.toEnd = function (item, immediate) {
			to('end', item, immediate);
		};

		/**
		 * Animate element or the whole SLIDEE to the center of the frame.
		 *
		 * @param {Mixed} item      Item DOM element, or index starting at 0. Omitting will animate SLIDEE.
		 * @param {Bool}  immediate Reposition immediately without an animation.
		 *
		 * @return {Void}
		 */
		self.toCenter = function (item, immediate) {
			to('center', item, immediate);
		};

		/**
		 * Get the index of an item in SLIDEE.
		 *
		 * @param {Mixed} item     Item DOM element.
		 *
		 * @return {Int}  Item index, or -1 if not found.
		 */
		function getIndex(item) {
			return item != null ?
					isNumber(item) ?
						item >= 0 && item < items.length ? item : -1 :
						$items.index(item) :
					-1;
		}
		// Expose getIndex without lowering the compressibility of it,
		// as it is used quite often throughout Sly.
		self.getIndex = getIndex;

		/**
		 * Get index of an item in SLIDEE based on a variety of input types.
		 *
		 * @param  {Mixed} item DOM element, positive or negative integer.
		 *
		 * @return {Int}   Item index, or -1 if not found.
		 */
		function getRelativeIndex(item) {
			return getIndex(isNumber(item) && item < 0 ? item + items.length : item);
		}

		/**
		 * Activates an item.
		 *
		 * @param  {Mixed} item Item DOM element, or index starting at 0.
		 *
		 * @return {Mixed} Activated item index or false on fail.
		 */
		function activate(item, force) {
			var index = getIndex(item);

			if (!itemNav || index < 0) {
				return false;
			}

			// Update classes, last active index, and trigger active event only when there
			// has been a change. Otherwise just return the current active index.
			if (last.active !== index || force) {
				// Update classes
				$items.eq(rel.activeItem).removeClass(o.activeClass);
				$items.eq(index).addClass(o.activeClass);

				last.active = rel.activeItem = index;

				updateButtonsState();
				trigger('active', index);
			}

			return index;
		}

		/**
		 * Activates an item and helps with further navigation when o.smart is enabled.
		 *
		 * @param {Mixed} item      Item DOM element, or index starting at 0.
		 * @param {Bool}  immediate Whether to reposition immediately in smart navigation.
		 *
		 * @return {Void}
		 */
		self.activate = function (item, immediate) {
			var index = activate(item);

			// Smart navigation
			if (o.smart && index !== false) {
				// When centeredNav is enabled, center the element.
				// Otherwise, determine where to position the element based on its current position.
				// If the element is currently on the far end side of the frame, assume that user is
				// moving forward and animate it to the start of the visible frame, and vice versa.
				if (centeredNav) {
					self.toCenter(index, immediate);
				} else if (index >= rel.lastItem) {
					self.toStart(index, immediate);
				} else if (index <= rel.firstItem) {
					self.toEnd(index, immediate);
				} else {
					resetCycle();
				}
			}
		};

		/**
		 * Activates a page.
		 *
		 * @param {Int}  index     Page index, starting from 0.
		 * @param {Bool} immediate Whether to reposition immediately without animation.
		 *
		 * @return {Void}
		 */
		self.activatePage = function (index, immediate) {
			if (isNumber(index)) {
				slideTo(pages[within(index, 0, pages.length - 1)], immediate);
			}
		};

		/**
		 * Return relative positions of items based on their visibility within FRAME.
		 *
		 * @param {Int} slideePos Position of SLIDEE.
		 *
		 * @return {Void}
		 */
		function getRelatives(slideePos) {
			slideePos = within(isNumber(slideePos) ? slideePos : pos.dest, pos.start, pos.end);

			var relatives = {};
			var centerOffset = forceCenteredNav ? 0 : frameSize / 2;

			// Determine active page
			if (!parallax) {
				for (var p = 0, pl = pages.length; p < pl; p++) {
					if (slideePos >= pos.end || p === pages.length - 1) {
						relatives.activePage = pages.length - 1;
						break;
					}

					if (slideePos <= pages[p] + centerOffset) {
						relatives.activePage = p;
						break;
					}
				}
			}

			// Relative item indexes
			if (itemNav) {
				var first = false;
				var last = false;
				var center = false;

				// From start
				for (var i = 0, il = items.length; i < il; i++) {
					// First item
					if (first === false && slideePos <= items[i].start + items[i].half) {
						first = i;
					}

					// Center item
					if (center === false && slideePos <= items[i].center + items[i].half) {
						center = i;
					}

					// Last item
					if (i === il - 1 || slideePos <= items[i].end + items[i].half) {
						last = i;
						break;
					}
				}

				// Safe assignment, just to be sure the false won't be returned
				relatives.firstItem = isNumber(first) ? first : 0;
				relatives.centerItem = isNumber(center) ? center : relatives.firstItem;
				relatives.lastItem = isNumber(last) ? last : relatives.centerItem;
			}

			return relatives;
		}

		/**
		 * Update object with relative positions.
		 *
		 * @param {Int} newPos
		 *
		 * @return {Void}
		 */
		function updateRelatives(newPos) {
			$.extend(rel, getRelatives(newPos));
		}

		/**
		 * Disable navigation buttons when needed.
		 *
		 * Adds disabledClass, and when the button is <button> or <input>, activates :disabled state.
		 *
		 * @return {Void}
		 */
		function updateButtonsState() {
			var isStart = pos.dest <= pos.start;
			var isEnd = pos.dest >= pos.end;
			var slideePosState = (isStart ? 1 : 0) | (isEnd ? 2 : 0);

			// Update paging buttons only if there has been a change in SLIDEE position
			if (last.slideePosState !== slideePosState) {
				last.slideePosState = slideePosState;

				if ($prevPageButton.is('button,input')) {
					$prevPageButton.prop('disabled', isStart);
				}

				if ($nextPageButton.is('button,input')) {
					$nextPageButton.prop('disabled', isEnd);
				}

				$prevPageButton.add($backwardButton)[isStart ? 'addClass' : 'removeClass'](o.disabledClass);
				$nextPageButton.add($forwardButton)[isEnd ? 'addClass' : 'removeClass'](o.disabledClass);
			}

			// Forward & Backward buttons need a separate state caching because we cannot "property disable"
			// them while they are being used, as disabled buttons stop emitting mouse events.
			if (last.fwdbwdState !== slideePosState && dragging.released) {
				last.fwdbwdState = slideePosState;

				if ($backwardButton.is('button,input')) {
					$backwardButton.prop('disabled', isStart);
				}

				if ($forwardButton.is('button,input')) {
					$forwardButton.prop('disabled', isEnd);
				}
			}

			// Item navigation
			if (itemNav && rel.activeItem != null) {
				var isFirst = rel.activeItem === 0;
				var isLast = rel.activeItem >= items.length - 1;
				var itemsButtonState = (isFirst ? 1 : 0) | (isLast ? 2 : 0);

				if (last.itemsButtonState !== itemsButtonState) {
					last.itemsButtonState = itemsButtonState;

					if ($prevButton.is('button,input')) {
						$prevButton.prop('disabled', isFirst);
					}

					if ($nextButton.is('button,input')) {
						$nextButton.prop('disabled', isLast);
					}

					$prevButton[isFirst ? 'addClass' : 'removeClass'](o.disabledClass);
					$nextButton[isLast ? 'addClass' : 'removeClass'](o.disabledClass);
				}
			}
		}

		/**
		 * Resume cycling.
		 *
		 * @param {Int} priority Resume pause with priority lower or equal than this. Used internally for pauseOnHover.
		 *
		 * @return {Void}
		 */
		self.resume = function (priority) {
			if (!o.cycleBy || !o.cycleInterval || o.cycleBy === 'items' && (!items[0] || rel.activeItem == null) || priority < self.isPaused) {
				return;
			}

			self.isPaused = 0;

			if (cycleID) {
				cycleID = clearTimeout(cycleID);
			} else {
				trigger('resume');
			}

			cycleID = setTimeout(function () {
				trigger('cycle');
				switch (o.cycleBy) {
					case 'items':
						self.activate(rel.activeItem >= items.length - 1 ? 0 : rel.activeItem + 1);
						break;

					case 'pages':
						self.activatePage(rel.activePage >= pages.length - 1 ? 0 : rel.activePage + 1);
						break;
				}
			}, o.cycleInterval);
		};

		/**
		 * Pause cycling.
		 *
		 * @param {Int} priority Pause priority. 100 is default. Used internally for pauseOnHover.
		 *
		 * @return {Void}
		 */
		self.pause = function (priority) {
			if (priority < self.isPaused) {
				return;
			}

			self.isPaused = priority || 100;

			if (cycleID) {
				cycleID = clearTimeout(cycleID);
				trigger('pause');
			}
		};

		/**
		 * Toggle cycling.
		 *
		 * @return {Void}
		 */
		self.toggle = function () {
			self[cycleID ? 'pause' : 'resume']();
		};

		/**
		 * Updates a signle or multiple option values.
		 *
		 * @param {Mixed} name  Name of the option that should be updated, or object that will extend the options.
		 * @param {Mixed} value New option value.
		 *
		 * @return {Void}
		 */
		self.set = function (name, value) {
			if ($.isPlainObject(name)) {
				$.extend(o, name);
			} else if (o.hasOwnProperty(name)) {
				o[name] = value;
			}
		};

		/**
		 * Add one or multiple items to the SLIDEE end, or a specified position index.
		 *
		 * @param {Mixed} element Node element, or HTML string.
		 * @param {Int}   index   Index of a new item position. By default item is appended at the end.
		 *
		 * @return {Void}
		 */
		self.add = function (element, index) {
			var $element = $(element);

			if (itemNav) {
				// Insert the element(s)
				if (index == null || !items[0] || index >= items.length) {
					$element.appendTo($slidee);
				} else if (items.length) {
					$element.insertBefore(items[index].el);
				}

				// Adjust the activeItem index
				if (rel.activeItem != null && index <= rel.activeItem) {
					last.active = rel.activeItem += $element.length;
				}
			} else {
				$slidee.append($element);
			}

			// Reload
			load();
		};

		/**
		 * Remove an item from SLIDEE.
		 *
		 * @param {Mixed} element Item index, or DOM element.
		 * @param {Int}   index   Index of a new item position. By default item is appended at the end.
		 *
		 * @return {Void}
		 */
		self.remove = function (element) {
			if (itemNav) {
				var index = getRelativeIndex(element);

				if (index > -1) {
					// Remove the element
					$items.eq(index).remove();

					// If the current item is being removed, activate new one after reload
					var reactivate = index === rel.activeItem;

					// Adjust the activeItem index
					if (rel.activeItem != null && index < rel.activeItem) {
						last.active = --rel.activeItem;
					}

					// Reload
					load();

					// Activate new item at the removed position
					if (reactivate) {
						last.active = null;
						self.activate(rel.activeItem);
					}
				}
			} else {
				$(element).remove();
				load();
			}
		};

		/**
		 * Helps re-arranging items.
		 *
		 * @param  {Mixed} item     Item DOM element, or index starting at 0. Use negative numbers to select items from the end.
		 * @param  {Mixed} position Item insertion anchor. Accepts same input types as item argument.
		 * @param  {Bool}  after    Insert after instead of before the anchor.
		 *
		 * @return {Void}
		 */
		function moveItem(item, position, after) {
			item = getRelativeIndex(item);
			position = getRelativeIndex(position);

			// Move only if there is an actual change requested
			if (item > -1 && position > -1 && item !== position && (!after || position !== item - 1) && (after || position !== item + 1)) {
				$items.eq(item)[after ? 'insertAfter' : 'insertBefore'](items[position].el);

				var shiftStart = item < position ? item : (after ? position : position - 1);
				var shiftEnd = item > position ? item : (after ? position + 1 : position);
				var shiftsUp = item > position;

				// Update activeItem index
				if (rel.activeItem != null) {
					if (item === rel.activeItem) {
						last.active = rel.activeItem = after ? (shiftsUp ? position + 1 : position) : (shiftsUp ? position : position - 1);
					} else if (rel.activeItem > shiftStart && rel.activeItem < shiftEnd) {
						last.active = rel.activeItem += shiftsUp ? 1 : -1;
					}
				}

				// Reload
				load();
			}
		}

		/**
		 * Move item after the target anchor.
		 *
		 * @param  {Mixed} item     Item to be moved. Can be DOM element or item index.
		 * @param  {Mixed} position Target position anchor. Can be DOM element or item index.
		 *
		 * @return {Void}
		 */
		self.moveAfter = function (item, position) {
			moveItem(item, position, 1);
		};

		/**
		 * Move item before the target anchor.
		 *
		 * @param  {Mixed} item     Item to be moved. Can be DOM element or item index.
		 * @param  {Mixed} position Target position anchor. Can be DOM element or item index.
		 *
		 * @return {Void}
		 */
		self.moveBefore = function (item, position) {
			moveItem(item, position);
		};

		/**
		 * Registers callbacks.
		 *
		 * @param  {Mixed} name  Event name, or callbacks map.
		 * @param  {Mixed} fn    Callback, or an array of callback functions.
		 *
		 * @return {Void}
		 */
		self.on = function (name, fn) {
			// Callbacks map
			if (type(name) === 'object') {
				for (var key in name) {
					if (name.hasOwnProperty(key)) {
						self.on(key, name[key]);
					}
				}
			// Callback
			} else if (type(fn) === 'function') {
				var names = name.split(' ');
				for (var n = 0, nl = names.length; n < nl; n++) {
					callbacks[names[n]] = callbacks[names[n]] || [];
					if (callbackIndex(names[n], fn) === -1) {
						callbacks[names[n]].push(fn);
					}
				}
			// Callbacks array
			} else if (type(fn) === 'array') {
				for (var f = 0, fl = fn.length; f < fl; f++) {
					self.on(name, fn[f]);
				}
			}
		};

		/**
		 * Registers callbacks to be executed only once.
		 *
		 * @param  {Mixed} name  Event name, or callbacks map.
		 * @param  {Mixed} fn    Callback, or an array of callback functions.
		 *
		 * @return {Void}
		 */
		self.one = function (name, fn) {
			function proxy() {
				fn.apply(self, arguments);
				self.off(name, proxy);
			}
			self.on(name, proxy);
		};

		/**
		 * Remove one or all callbacks.
		 *
		 * @param  {String} name Event name.
		 * @param  {Mixed}  fn   Callback, or an array of callback functions. Omit to remove all callbacks.
		 *
		 * @return {Void}
		 */
		self.off = function (name, fn) {
			if (fn instanceof Array) {
				for (var f = 0, fl = fn.length; f < fl; f++) {
					self.off(name, fn[f]);
				}
			} else {
				var names = name.split(' ');
				for (var n = 0, nl = names.length; n < nl; n++) {
					callbacks[names[n]] = callbacks[names[n]] || [];
					if (fn == null) {
						callbacks[names[n]].length = 0;
					} else {
						var index = callbackIndex(names[n], fn);
						if (index !== -1) {
							callbacks[names[n]].splice(index, 1);
						}
					}
				}
			}
		};

		/**
		 * Returns callback array index.
		 *
		 * @param  {String}   name Event name.
		 * @param  {Function} fn   Function
		 *
		 * @return {Int} Callback array index, or -1 if isn't registered.
		 */
		function callbackIndex(name, fn) {
			for (var i = 0, l = callbacks[name].length; i < l; i++) {
				if (callbacks[name][i] === fn) {
					return i;
				}
			}
			return -1;
		}

		/**
		 * Reset next cycle timeout.
		 *
		 * @return {Void}
		 */
		function resetCycle() {
			if (dragging.released && !self.isPaused) {
				self.resume();
			}
		}

		/**
		 * Calculate SLIDEE representation of handle position.
		 *
		 * @param  {Int} handlePos
		 *
		 * @return {Int}
		 */
		function handleToSlidee(handlePos) {
			return round(within(handlePos, hPos.start, hPos.end) / hPos.end * (pos.end - pos.start)) + pos.start;
		}

		/**
		 * Keeps track of a dragging delta history.
		 *
		 * @return {Void}
		 */
		function draggingHistoryTick() {
			// Looking at this, I know what you're thinking :) But as we need only 4 history states, doing it this way
			// as opposed to a proper loop is ~25 bytes smaller (when minified with GCC), a lot faster, and doesn't
			// generate garbage. The loop version would create 2 new variables on every tick. Unexaptable!
			dragging.history[0] = dragging.history[1];
			dragging.history[1] = dragging.history[2];
			dragging.history[2] = dragging.history[3];
			dragging.history[3] = dragging.delta;
		}

		/**
		 * Initialize continuous movement.
		 *
		 * @return {Void}
		 */
		function continuousInit(source) {
			dragging.released = 0;
			dragging.source = source;
			dragging.slidee = source === 'slidee';
		}

		/**
		 * Dragging initiator.
		 *
		 * @param  {Event} event
		 *
		 * @return {Void}
		 */
		function dragInit(event) {
			var isTouch = event.type === 'touchstart';
			var source = event.data.source;
			var isSlidee = source === 'slidee';

			// Ignore when already in progress, or interactive element in non-touch navivagion
			if (dragging.init || !isTouch && isInteractive(event.target)) {
				return;
			}

			// Handle dragging conditions
			if (source === 'handle' && (!o.dragHandle || hPos.start === hPos.end)) {
				return;
			}

			// SLIDEE dragging conditions
			if (isSlidee && !(isTouch ? o.touchDragging : o.mouseDragging && event.which < 2)) {
				return;
			}

			if (!isTouch) {
				// prevents native image dragging in Firefox
				stopDefault(event);
			}

			// Reset dragging object
			continuousInit(source);

			// Properties used in dragHandler
			dragging.init = 0;
			dragging.$source = $(event.target);
			dragging.touch = isTouch;
			dragging.pointer = isTouch ? event.originalEvent.touches[0] : event;
			dragging.initX = dragging.pointer.pageX;
			dragging.initY = dragging.pointer.pageY;
			dragging.initPos = isSlidee ? pos.cur : hPos.cur;
			dragging.start = +new Date();
			dragging.time = 0;
			dragging.path = 0;
			dragging.delta = 0;
			dragging.locked = 0;
			dragging.history = [0, 0, 0, 0];
			dragging.pathToLock = isSlidee ? isTouch ? 30 : 10 : 0;

			// Bind dragging events
			$doc.on(isTouch ? dragTouchEvents : dragMouseEvents, dragHandler);

			// Pause ongoing cycle
			self.pause(1);

			// Add dragging class
			(isSlidee ? $slidee : $handle).addClass(o.draggedClass);

			// Trigger moveStart event
			trigger('moveStart');

			// Keep track of a dragging path history. This is later used in the
			// dragging release swing calculation when dragging SLIDEE.
			if (isSlidee) {
				historyID = setInterval(draggingHistoryTick, 10);
			}
		}

		/**
		 * Handler for dragging scrollbar handle or SLIDEE.
		 *
		 * @param  {Event} event
		 *
		 * @return {Void}
		 */
		function dragHandler(event) {
			dragging.released = event.type === 'mouseup' || event.type === 'touchend';
			dragging.pointer = dragging.touch ? event.originalEvent[dragging.released ? 'changedTouches' : 'touches'][0] : event;
			dragging.pathX = dragging.pointer.pageX - dragging.initX;
			dragging.pathY = dragging.pointer.pageY - dragging.initY;
			dragging.path = sqrt(pow(dragging.pathX, 2) + pow(dragging.pathY, 2));
			dragging.delta = o.horizontal ? dragging.pathX : dragging.pathY;

			if (!dragging.released && dragging.path < 1) return;

			// We haven't decided whether this is a drag or not...
			if (!dragging.init) {
				// If the drag path was very short, maybe it's not a drag?
				if (dragging.path < o.dragThreshold) {
					// If the pointer was released, the path will not become longer and it's
					// definitely not a drag. If not released yet, decide on next iteration
					return dragging.released ? dragEnd() : undefined;
				}
				else {
					// If dragging path is sufficiently long we can confidently start a drag
					// if drag is in different direction than scroll, ignore it
					if (o.horizontal ? abs(dragging.pathX) > abs(dragging.pathY) : abs(dragging.pathX) < abs(dragging.pathY)) {
						dragging.init = 1;
					} else {
						return dragEnd();
					}
				}
			}

			stopDefault(event);

			// Disable click on a source element, as it is unwelcome when dragging
			if (!dragging.locked && dragging.path > dragging.pathToLock && dragging.slidee) {
				dragging.locked = 1;
				dragging.$source.on(clickEvent, disableOneEvent);
			}

			// Cancel dragging on release
			if (dragging.released) {
				dragEnd();

				// Adjust path with a swing on mouse release
				if (o.releaseSwing && dragging.slidee) {
					dragging.swing = (dragging.delta - dragging.history[0]) / 40 * 300;
					dragging.delta += dragging.swing;
					dragging.tweese = abs(dragging.swing) > 10;
				}
			}

			slideTo(dragging.slidee ? round(dragging.initPos - dragging.delta) : handleToSlidee(dragging.initPos + dragging.delta));
		}

		/**
		 * Stops dragging and cleans up after it.
		 *
		 * @return {Void}
		 */
		function dragEnd() {
			clearInterval(historyID);
			dragging.released = true;
			$doc.off(dragging.touch ? dragTouchEvents : dragMouseEvents, dragHandler);
			(dragging.slidee ? $slidee : $handle).removeClass(o.draggedClass);

			// Make sure that disableOneEvent is not active in next tick.
			setTimeout(function () {
				dragging.$source.off(clickEvent, disableOneEvent);
			});

			// Normally, this is triggered in render(), but if there
			// is nothing to render, we have to do it manually here.
			if (pos.cur === pos.dest && dragging.init) {
				trigger('moveEnd');
			}

			// Resume ongoing cycle
			self.resume(1);

			dragging.init = 0;
		}

		/**
		 * Check whether element is interactive.
		 *
		 * @return {Boolean}
		 */
		function isInteractive(element) {
			return ~$.inArray(element.nodeName, interactiveElements) || $(element).is(o.interactive);
		}

		/**
		 * Continuous movement cleanup on mouseup.
		 *
		 * @return {Void}
		 */
		function movementReleaseHandler() {
			self.stop();
			$doc.off('mouseup', movementReleaseHandler);
		}

		/**
		 * Buttons navigation handler.
		 *
		 * @param  {Event} event
		 *
		 * @return {Void}
		 */
		function buttonsHandler(event) {
			/*jshint validthis:true */
			stopDefault(event);
			switch (this) {
				case $forwardButton[0]:
				case $backwardButton[0]:
					self.moveBy($forwardButton.is(this) ? o.moveBy : -o.moveBy);
					$doc.on('mouseup', movementReleaseHandler);
					break;

				case $prevButton[0]:
					self.prev();
					break;

				case $nextButton[0]:
					self.next();
					break;

				case $prevPageButton[0]:
					self.prevPage();
					break;

				case $nextPageButton[0]:
					self.nextPage();
					break;
			}
		}

		/**
		 * Mouse wheel delta normalization.
		 *
		 * @param  {Event} event
		 *
		 * @return {Int}
		 */
		function normalizeWheelDelta(event) {
			// wheelDelta needed only for IE8-
			scrolling.curDelta = ((o.horizontal ? event.deltaY || event.deltaX : event.deltaY) || -event.wheelDelta);
			scrolling.curDelta /= event.deltaMode === 1 ? 3 : 100;
			if (!itemNav) {
				return scrolling.curDelta;
			}
			time = +new Date();
			if (scrolling.last < time - scrolling.resetTime) {
				scrolling.delta = 0;
			}
			scrolling.last = time;
			scrolling.delta += scrolling.curDelta;
			if (abs(scrolling.delta) < 1) {
				scrolling.finalDelta = 0;
			} else {
				scrolling.finalDelta = round(scrolling.delta / 1);
				scrolling.delta %= 1;
			}
			return scrolling.finalDelta;
		}

		/**
		 * Mouse scrolling handler.
		 *
		 * @param  {Event} event
		 *
		 * @return {Void}
		 */
		function scrollHandler(event) {
			// Mark event as originating in a Sly instance
			event.originalEvent[namespace] = self;
			// Don't hijack global scrolling
			var time = +new Date();
			if (lastGlobalWheel + o.scrollHijack > time && $scrollSource[0] !== document && $scrollSource[0] !== window) {
				lastGlobalWheel = time;
				return;
			}
			// Ignore if there is no scrolling to be done
			if (!o.scrollBy || pos.start === pos.end) {
				return;
			}
			var delta = normalizeWheelDelta(event.originalEvent);
			// Trap scrolling only when necessary and/or requested
			if (o.scrollTrap || delta > 0 && pos.dest < pos.end || delta < 0 && pos.dest > pos.start) {
				stopDefault(event, 1);
			}
			self.slideBy(o.scrollBy * delta);
		}

		/**
		 * Scrollbar click handler.
		 *
		 * @param  {Event} event
		 *
		 * @return {Void}
		 */
		function scrollbarHandler(event) {
			// Only clicks on scroll bar. Ignore the handle.
			if (o.clickBar && event.target === $sb[0]) {
				stopDefault(event);
				// Calculate new handle position and sync SLIDEE to it
				slideTo(handleToSlidee((o.horizontal ? event.pageX - $sb.offset().left : event.pageY - $sb.offset().top) - handleSize / 2));
			}
		}

		/**
		 * Keyboard input handler.
		 *
		 * @param  {Event} event
		 *
		 * @return {Void}
		 */
		function keyboardHandler(event) {
			if (!o.keyboardNavBy) {
				return;
			}

			switch (event.which) {
				// Left or Up
				case o.horizontal ? 37 : 38:
					stopDefault(event);
					self[o.keyboardNavBy === 'pages' ? 'prevPage' : 'prev']();
					break;

				// Right or Down
				case o.horizontal ? 39 : 40:
					stopDefault(event);
					self[o.keyboardNavBy === 'pages' ? 'nextPage' : 'next']();
					break;
			}
		}

		/**
		 * Click on item activation handler.
		 *
		 * @param  {Event} event
		 *
		 * @return {Void}
		 */
		function activateHandler(event) {
			/*jshint validthis:true */

			// Ignore clicks on interactive elements.
			if (isInteractive(this)) {
				event.originalEvent[namespace + 'ignore'] = true;
				return;
			}

			// Ignore events that:
			// - are not originating from direct SLIDEE children
			// - originated from interactive elements
			if (this.parentNode !== $slidee[0] || event.originalEvent[namespace + 'ignore']) return;

			self.activate(this);
		}

		/**
		 * Click on page button handler.
		 *
		 * @param {Event} event
		 *
		 * @return {Void}
		 */
		function activatePageHandler() {
			/*jshint validthis:true */
			// Accept only events from direct pages bar children.
			if (this.parentNode === $pb[0]) {
				self.activatePage($pages.index(this));
			}
		}

		/**
		 * Pause on hover handler.
		 *
		 * @param  {Event} event
		 *
		 * @return {Void}
		 */
		function pauseOnHoverHandler(event) {
			if (o.pauseOnHover) {
				self[event.type === 'mouseenter' ? 'pause' : 'resume'](2);
			}
		}

		/**
		 * Trigger callbacks for event.
		 *
		 * @param  {String} name Event name.
		 * @param  {Mixed}  argX Arguments passed to callbacks.
		 *
		 * @return {Void}
		 */
		function trigger(name, arg1) {
			if (callbacks[name]) {
				l = callbacks[name].length;
				// Callbacks will be stored and executed from a temporary array to not
				// break the execution queue when one of the callbacks unbinds itself.
				tmpArray.length = 0;
				for (i = 0; i < l; i++) {
					tmpArray.push(callbacks[name][i]);
				}
				// Execute the callbacks
				for (i = 0; i < l; i++) {
					tmpArray[i].call(self, name, arg1);
				}
			}
		}

		/**
		 * Destroys instance and everything it created.
		 *
		 * @return {Void}
		 */
		self.destroy = function () {
			// Remove the reference to itself
			Sly.removeInstance(frame);

			// Unbind all events
			$scrollSource
				.add($handle)
				.add($sb)
				.add($pb)
				.add($forwardButton)
				.add($backwardButton)
				.add($prevButton)
				.add($nextButton)
				.add($prevPageButton)
				.add($nextPageButton)
				.off('.' + namespace);

			// Unbinding specifically as to not nuke out other instances
			$doc.off('keydown', keyboardHandler);

			// Remove classes
			$prevButton
				.add($nextButton)
				.add($prevPageButton)
				.add($nextPageButton)
				.removeClass(o.disabledClass);

			if ($items && rel.activeItem != null) {
				$items.eq(rel.activeItem).removeClass(o.activeClass);
			}

			// Remove page items
			$pb.empty();

			if (!parallax) {
				// Unbind events from frame
				$frame.off('.' + namespace);
				// Restore original styles
				frameStyles.restore();
				slideeStyles.restore();
				sbStyles.restore();
				handleStyles.restore();
				// Remove the instance from element data storage
				$.removeData(frame, namespace);
			}

			// Clean up collections
			items.length = pages.length = 0;
			last = {};

			// Reset initialized status and return the instance
			self.initialized = 0;
			return self;
		};

		/**
		 * Initialize.
		 *
		 * @return {Object}
		 */
		self.init = function () {
			if (self.initialized) {
				return;
			}

			// Disallow multiple instances on the same element
			if (Sly.getInstance(frame)) throw new Error('There is already a Sly instance on this element');

			// Store the reference to itself
			Sly.storeInstance(frame, self);

			// Register callbacks map
			self.on(callbackMap);

			// Save styles
			var holderProps = ['overflow', 'position'];
			var movableProps = ['position', 'webkitTransform', 'msTransform', 'transform', 'left', 'top', 'width', 'height'];
			frameStyles.save.apply(frameStyles, holderProps);
			sbStyles.save.apply(sbStyles, holderProps);
			slideeStyles.save.apply(slideeStyles, movableProps);
			handleStyles.save.apply(handleStyles, movableProps);

			// Set required styles
			var $movables = $handle;
			if (!parallax) {
				$movables = $movables.add($slidee);
				$frame.css('overflow', 'hidden');
				if (!transform && $frame.css('position') === 'static') {
					$frame.css('position', 'relative');
				}
			}
			if (transform) {
				if (gpuAcceleration) {
					$movables.css(transform, gpuAcceleration);
				}
			} else {
				if ($sb.css('position') === 'static') {
					$sb.css('position', 'relative');
				}
				$movables.css({ position: 'absolute' });
			}

			// Navigation buttons
			if (o.forward) {
				$forwardButton.on(mouseDownEvent, buttonsHandler);
			}
			if (o.backward) {
				$backwardButton.on(mouseDownEvent, buttonsHandler);
			}
			if (o.prev) {
				$prevButton.on(clickEvent, buttonsHandler);
			}
			if (o.next) {
				$nextButton.on(clickEvent, buttonsHandler);
			}
			if (o.prevPage) {
				$prevPageButton.on(clickEvent, buttonsHandler);
			}
			if (o.nextPage) {
				$nextPageButton.on(clickEvent, buttonsHandler);
			}

			// Scrolling navigation
			$scrollSource.on(wheelEvent, scrollHandler);

			// Clicking on scrollbar navigation
			if ($sb[0]) {
				$sb.on(clickEvent, scrollbarHandler);
			}

			// Click on items navigation
			if (itemNav && o.activateOn) {
				$frame.on(o.activateOn + '.' + namespace, '*', activateHandler);
			}

			// Pages navigation
			if ($pb[0] && o.activatePageOn) {
				$pb.on(o.activatePageOn + '.' + namespace, '*', activatePageHandler);
			}

			// Dragging navigation
			$dragSource.on(dragInitEvents, { source: 'slidee' }, dragInit);

			// Scrollbar dragging navigation
			if ($handle) {
				$handle.on(dragInitEvents, { source: 'handle' }, dragInit);
			}

			// Keyboard navigation
			$doc.on('keydown', keyboardHandler);

			if (!parallax) {
				// Pause on hover
				$frame.on('mouseenter.' + namespace + ' mouseleave.' + namespace, pauseOnHoverHandler);
				// Reset native FRAME element scroll
				$frame.on('scroll.' + namespace, resetScroll);
			}

			// Mark instance as initialized
			self.initialized = 1;

			// Load
			load(true);

			// Initiate automatic cycling
			if (o.cycleBy && !parallax) {
				self[o.startPaused ? 'pause' : 'resume']();
			}

			// Return instance
			return self;
		};
	}

	Sly.getInstance = function (element) {
		return $.data(element, namespace);
	};

	Sly.storeInstance = function (element, sly) {
		return $.data(element, namespace, sly);
	};

	Sly.removeInstance = function (element) {
		return $.removeData(element, namespace);
	};

	/**
	 * Return type of the value.
	 *
	 * @param  {Mixed} value
	 *
	 * @return {String}
	 */
	function type(value) {
		if (value == null) {
			return String(value);
		}

		if (typeof value === 'object' || typeof value === 'function') {
			return Object.prototype.toString.call(value).match(/\s([a-z]+)/i)[1].toLowerCase() || 'object';
		}

		return typeof value;
	}

	/**
	 * Event preventDefault & stopPropagation helper.
	 *
	 * @param {Event} event     Event object.
	 * @param {Bool}  noBubbles Cancel event bubbling.
	 *
	 * @return {Void}
	 */
	function stopDefault(event, noBubbles) {
		event.preventDefault();
		if (noBubbles) {
			event.stopPropagation();
		}
	}

	/**
	 * Disables an event it was triggered on and unbinds itself.
	 *
	 * @param  {Event} event
	 *
	 * @return {Void}
	 */
	function disableOneEvent(event) {
		/*jshint validthis:true */
		stopDefault(event, 1);
		$(this).off(event.type, disableOneEvent);
	}

	/**
	 * Resets native element scroll values to 0.
	 *
	 * @return {Void}
	 */
	function resetScroll() {
		/*jshint validthis:true */
		this.scrollLeft = 0;
		this.scrollTop = 0;
	}

	/**
	 * Check if variable is a number.
	 *
	 * @param {Mixed} value
	 *
	 * @return {Boolean}
	 */
	function isNumber(value) {
		return !isNaN(parseFloat(value)) && isFinite(value);
	}

	/**
	 * Parse style to pixels.
	 *
	 * @param {Object}   $item    jQuery object with element.
	 * @param {Property} property CSS property to get the pixels from.
	 *
	 * @return {Int}
	 */
	function getPx($item, property) {
		return 0 | round(String($item.css(property)).replace(/[^\-0-9.]/g, ''));
	}

	/**
	 * Make sure that number is within the limits.
	 *
	 * @param {Number} number
	 * @param {Number} min
	 * @param {Number} max
	 *
	 * @return {Number}
	 */
	function within(number, min, max) {
		return number < min ? min : number > max ? max : number;
	}

	/**
	 * Saves element styles for later restoration.
	 *
	 * Example:
	 *   var styles = new StyleRestorer(frame);
	 *   styles.save('position');
	 *   element.style.position = 'absolute';
	 *   styles.restore(); // restores to state before the assignment above
	 *
	 * @param {Element} element
	 */
	function StyleRestorer(element) {
		var self = {};
		self.style = {};
		self.save = function () {
			if (!element || !element.nodeType) return;
			for (var i = 0; i < arguments.length; i++) {
				self.style[arguments[i]] = element.style[arguments[i]];
			}
			return self;
		};
		self.restore = function () {
			if (!element || !element.nodeType) return;
			for (var prop in self.style) {
				if (self.style.hasOwnProperty(prop)) element.style[prop] = self.style[prop];
			}
			return self;
		};
		return self;
	}

	// Local WindowAnimationTiming interface polyfill
	(function (w) {
		rAF = w.requestAnimationFrame
			|| w.webkitRequestAnimationFrame
			|| fallback;

		/**
		* Fallback implementation.
		*/
		var prev = new Date().getTime();
		function fallback(fn) {
			var curr = new Date().getTime();
			var ms = Math.max(0, 16 - (curr - prev));
			var req = setTimeout(fn, ms);
			prev = curr;
			return req;
		}

		/**
		* Cancel.
		*/
		var cancel = w.cancelAnimationFrame
			|| w.webkitCancelAnimationFrame
			|| w.clearTimeout;

		cAF = function(id){
			cancel.call(w, id);
		};
	}(window));

	// Feature detects
	(function () {
		var prefixes = ['', 'Webkit', 'Moz', 'ms', 'O'];
		var el = document.createElement('div');

		function testProp(prop) {
			for (var p = 0, pl = prefixes.length; p < pl; p++) {
				var prefixedProp = prefixes[p] ? prefixes[p] + prop.charAt(0).toUpperCase() + prop.slice(1) : prop;
				if (el.style[prefixedProp] != null) {
					return prefixedProp;
				}
			}
		}

		// Global support indicators
		transform = testProp('transform');
		gpuAcceleration = testProp('perspective') ? 'translateZ(0) ' : '';
	}());

	// Expose class globally
	w[className] = Sly;

	// jQuery proxy
	$.fn[pluginName] = function (options, callbackMap) {
		var method, methodArgs;

		// Attributes logic
		if (!$.isPlainObject(options)) {
			if (type(options) === 'string' || options === false) {
				method = options === false ? 'destroy' : options;
				methodArgs = Array.prototype.slice.call(arguments, 1);
			}
			options = {};
		}

		// Apply to all elements
		return this.each(function (i, element) {
			// Call with prevention against multiple instantiations
			var plugin = Sly.getInstance(element);

			if (!plugin && !method) {
				// Create a new object if it doesn't exist yet
				plugin = new Sly(element, options, callbackMap).init();
			} else if (plugin && method) {
				// Call method
				if (plugin[method]) {
					plugin[method].apply(plugin, methodArgs);
				}
			}
		});
	};

	// Default options
	Sly.defaults = {
		slidee:     null,  // Selector, DOM element, or jQuery object with DOM element representing SLIDEE.
		horizontal: false, // Switch to horizontal mode.

		// Item based navigation
		itemNav:        null,  // Item navigation type. Can be: 'basic', 'centered', 'forceCentered'.
		itemSelector:   null,  // Select only items that match this selector.
		smart:          false, // Repositions the activated item to help with further navigation.
		activateOn:     null,  // Activate an item on this event. Can be: 'click', 'mouseenter', ...
		activateMiddle: false, // Always activate the item in the middle of the FRAME. forceCentered only.

		// Scrolling
		scrollSource: null,  // Element for catching the mouse wheel scrolling. Default is FRAME.
		scrollBy:     0,     // Pixels or items to move per one mouse scroll. 0 to disable scrolling.
		scrollHijack: 300,   // Milliseconds since last wheel event after which it is acceptable to hijack global scroll.
		scrollTrap:   false, // Don't bubble scrolling when hitting scrolling limits.

		// Dragging
		dragSource:    null,  // Selector or DOM element for catching dragging events. Default is FRAME.
		mouseDragging: false, // Enable navigation by dragging the SLIDEE with mouse cursor.
		touchDragging: false, // Enable navigation by dragging the SLIDEE with touch events.
		releaseSwing:  false, // Ease out on dragging swing release.
		swingSpeed:    0.2,   // Swing synchronization speed, where: 1 = instant, 0 = infinite.
		elasticBounds: false, // Stretch SLIDEE position limits when dragging past FRAME boundaries.
		dragThreshold: 3,     // Distance in pixels before Sly recognizes dragging.
		interactive:   null,  // Selector for special interactive elements.

		// Scrollbar
		scrollBar:     null,  // Selector or DOM element for scrollbar container.
		dragHandle:    false, // Whether the scrollbar handle should be draggable.
		dynamicHandle: false, // Scrollbar handle represents the ratio between hidden and visible content.
		minHandleSize: 50,    // Minimal height or width (depends on sly direction) of a handle in pixels.
		clickBar:      false, // Enable navigation by clicking on scrollbar.
		syncSpeed:     0.5,   // Handle => SLIDEE synchronization speed, where: 1 = instant, 0 = infinite.

		// Pagesbar
		pagesBar:       null, // Selector or DOM element for pages bar container.
		activatePageOn: null, // Event used to activate page. Can be: click, mouseenter, ...
		pageBuilder:          // Page item generator.
			function (index) {
				return '<li>' + (index + 1) + '</li>';
			},

		// Navigation buttons
		forward:  null, // Selector or DOM element for "forward movement" button.
		backward: null, // Selector or DOM element for "backward movement" button.
		prev:     null, // Selector or DOM element for "previous item" button.
		next:     null, // Selector or DOM element for "next item" button.
		prevPage: null, // Selector or DOM element for "previous page" button.
		nextPage: null, // Selector or DOM element for "next page" button.

		// Automated cycling
		cycleBy:       null,  // Enable automatic cycling by 'items' or 'pages'.
		cycleInterval: 5000,  // Delay between cycles in milliseconds.
		pauseOnHover:  false, // Pause cycling when mouse hovers over the FRAME.
		startPaused:   false, // Whether to start in paused sate.

		// Mixed options
		moveBy:        300,     // Speed in pixels per second used by forward and backward buttons.
		speed:         0,       // Animations speed in milliseconds. 0 to disable animations.
		easing:        'swing', // Easing for duration based (tweening) animations.
		startAt:       null,    // Starting offset in pixels or items.
		keyboardNavBy: null,    // Enable keyboard navigation by 'items' or 'pages'.

		// Classes
		draggedClass:  'dragged', // Class for dragged elements (like SLIDEE or scrollbar handle).
		activeClass:   'active',  // Class for active items and pages.
		disabledClass: 'disabled' // Class for disabled navigation elements.
	};
}(jQuery, window));

// Intercept Sly load event in order to hide the pagination
// when there is only single page. #13960
(function () {
    var slyReal = Sly;

    window.Sly = function Sly(frame, options, callbackMap) {
        callbackMap = callbackMap || {};

        var callbackMapLoadReal = null;

        if (typeof callbackMap === 'object'
            && callbackMap.hasOwnProperty('load')) {
            callbackMapLoadReal = callbackMap.load;
        }

        callbackMap.load = function (eventName) {
            var loadContext = this;

            if (this.pages.length < 2) {
                this.options.pagesBar.hide();
            }

            if (callbackMapLoadReal instanceof Function) {
                callbackMapLoadReal.call(loadContext, eventName);
            }

            // Providing global load event for all Sly sliders for better handling
            // of edge cases.
            var loadedGlobalEvent = $.Event('slyLoaded');
            loadedGlobalEvent.customData = { sliderInstance: loadContext };

            $(frame).trigger(loadedGlobalEvent);
        };

        return new slyReal(frame, options, callbackMap);
    };

    $.event.special.slyLoaded = {
        add: function (handleObj) {
            var selector = this;

            if (typeof handleObj.selector !== 'undefined') {
                selector = handleObj.selector;
            }

            $(selector).each(function () {
                var slyInstance = $.data(this, 'sly');

                if (slyInstance instanceof slyReal) {
                    var event = $.Event('slyLoaded');
                    event.customData = { sliderInstance: slyInstance };

                    handleObj.handler.call(this, event);
                }
            });
        }
    };
}());

var SliderInit, sliderFrames = [];
jQuery(function ($) {
    'use strict';
// -------------------------------------------------------------
// Basic Navigation
// -------------------------------------------------------------
    (function () {
        var $frame = $('.carousel_basic');
        var $slidee = $frame.children('ul').eq(0);
        var $wrap = $frame.parent();
// Call Sly on frame
        var sly = $frame.sly({
                horizontal: 1,
                itemNav: 'forceCentered',
                smart: 1,
                activateMiddle: 1,
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1,
                startAt: 0,
                scrollBy: 1,
                pagesBar: $wrap.find('.pages'),
                activatePageOn: 'click',
                speed: 300,
                elasticBounds: 1,
                easing: 'easeOutExpo',
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,
                // Buttons
                prev: $wrap.find('.prev'),
                next: $wrap.find('.next'),
                activeClass: 'active'
        },
        {
            load: function(e){
                generateCarouselIfo($(this.items[0].el.children[0]));
            }
        }
    );

        $slidee.find('.slider_element').on('click',function(){
            generateCarouselIfo($(this));
       });
       function generateCarouselIfo($element){
           $('.slider_element.active').removeClass('active');
            $element.addClass('active');
            $('#carousel_text').html($element.attr('data-description')+ '<span class="link_more"></span>');
            $('#carousel_title').html($element.attr('data-title'));
            $('.carousel_href').attr('href', $element.attr('data-href'));
            var img = $element.find('img');
            if(img.length == 0){
                $('#carousel_img').css('display', 'none');
                $('#carousel_img').after($element.find('a').html());
            }else{
                var svg = $('#carousel_img_wrapper').find('svg');
                if(svg.length > 0){
                    svg.remove();
                }
                $('#carousel_img').css('display', 'inline');
				var factor = 1;
                //TODO: Extract for common use.
				if(window.matchMedia){
					if( window.matchMedia("(min-resolution: 3dppx)").matches ||
						window.matchMedia("(-webkit-min-device-pixel-ratio: 3)").matches ||
						window.matchMedia("(min--moz-device-pixel-ratio: 3)").matches ||
						window.matchMedia("(-o-min-device-pixel-ratio: 3/1)").matches ||
						window.matchMedia("(min-device-pixel-ratio: 3) ").matches ||
						window.matchMedia("(min-resolution: 288dpi)").matches){
						factor = 3;
					}else if( window.matchMedia("(min-resolution: 2dppx)").matches ||
							  window.matchMedia("(-webkit-min-device-pixel-ratio: 2)").matches ||
							  window.matchMedia("(min--moz-device-pixel-ratio: 2)").matches ||
							  window.matchMedia("(-o-min-device-pixel-ratio: 2/1)").matches ||
							  window.matchMedia("(min-device-pixel-ratio: 2)").matches ||
							  window.matchMedia("( min-resolution: 192dpi)").matches){
						factor = 2;
					}
				}

				if((factor === 2 || factor === 3)){
						var carouselPreloader_x1 = new Image();
						carouselPreloader_x1.onload = function() {
							if(img.attr('src').indexOf('small_carousel~'+factor+'x') > -1){
								var carouselPreloader = new Image();
								carouselPreloader.onload = function() {
                                    var srcImgCurrent = $(this).attr('src')
                                    var srcCarousel = $('#carousel_img').attr('src');
                                    //TODO: Extend for Protected files.
									if(srcCarousel.substr(srcCarousel.indexOf('carousel') + 8, srcCarousel.length) == srcImgCurrent.substr(srcImgCurrent.indexOf('carousel~'+factor+'x')+11, srcImgCurrent.length)){
										$('#carousel_img').attr('src',$(this).attr('src'));
									}
								};
                                //TODO: Extend for Protected files.
								carouselPreloader.src = $('#carousel_img').attr('src').replace('__cache/carousel', '__cache/carousel~'+factor+'x');;
							}
						};
                        //TODO: Extend for Protected files.
						var image_src = img.attr('src').replace(new RegExp('__cache/small_carousel(~'+factor+'x)?/', 'g'), '__cache/carousel/')
						carouselPreloader_x1.src = image_src;
						$('#carousel_img').attr('src', image_src);

						$('#carousel_img').siblings().each(function(){
							$(this).remove();
						});
				}else{
					$('#carousel_img').attr('src', img.attr('src').replace('__cache/small_carousel', '__cache/carousel'));
				}
                //$('#carousel_img').attr('srcset', img.attr('srcset').replace(new RegExp('__cache/small_carousel', 'g'), '__cache/carousel'));
            }
       }
    }());
// -------------------------------------------------------------
// One Item Per Frame
// -------------------------------------------------------------



    (function () {

        var mslide = $('.mslide');

        if (mslide.length > 0) {
            SliderInit = createSlider();

            mslide.each(function () {
                sliderFrames.push(new SliderInit($(this)));
            });

        }

        $('a.slider-grid').click( function() {

            var top = this.getBoundingClientRect().top;

            mslide.each(function() {
                var _this = $(this);
                _this.parent().find('.btn_navigation').css('visibility', 'hidden');
                _this.removeClass('frame');
                _this.addClass('overview');
                _this.parent().find('.pages').hide();
            });

            if ( mslide.length > 0 ) {
                $.each(sliderFrames, function( idx, val ) {
                    val.destroy();
                });
            }

            setTimeout(function() {
                $('a.view-slider').removeClass('active');
                $('a.slider-grid').addClass('active');
                window.scrollTo(0, arguments[0].offset().top - arguments[1]);
            }, 100, $(this), top);

            return false;
        });

        $('a.view-slider').click( function() {

            var top = this.getBoundingClientRect().top;

            mslide.each(function() {
                var _this = $(this);
                _this.parent().find('.btn_navigation').css('visibility', 'visible');
                _this.removeClass('overview');
                _this.addClass('frame');
                _this.parent().find('.pages').show();
            });


            if ( mslide.length > 0 ) {
                $.each(sliderFrames, function( idx, val ) {
                    val.init();
                });
            }

            setTimeout(function() {
                $('a.slider-grid').removeClass('active');
                $('a.view-slider').addClass('active');
                window.scrollTo(0, arguments[0].offset().top - arguments[1]);
            }, 10, $(this), top);

            return false;
        });
    }());

// -------------------------------------------------------------
// Home Sliders
// -------------------------------------------------------------
    (function () {
        $('.front_slider:not(.carousel_basic)').each(function(index, slider){
            var $frame = $(slider);
            var $wrap = $frame.parent();
    // Call Sly on frame

        var sly = new Sly($frame,{
                horizontal: 1,
                itemNav: 'forceCentered',
                smart: 1,
                activateMiddle: 1,
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1,
                startAt: 0,
                scrollBy: 1,
                pagesBar: $wrap.find('.pages'),
                activatePageOn: 'click',
                speed: 300,
                elasticBounds: 1,
                easing: 'easeOutExpo',
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,
                // Buttons
                prev: $wrap.find('.prev'),
                next: $wrap.find('.next'),
                activeClass: 'active'
        });
        sly.init();
        });
    }());
});
	function createSlider(){
		return function (module) {
            var currentSlider = $(module);
            var currentWrap = currentSlider.parent();
            var options = {
                horizontal: 1,
                itemNav: 'forceCentered',
                smart: 1,
                activateMiddle: 1,
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1,
                startAt: 0,
                scrollBy: 1,
                pagesBar: currentWrap.find('.pages'),
                activatePageOn: 'click',
                speed: 300,
                elasticBounds: 1,
                easing: 'easeOutExpo',
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,
                // Buttons
                prev: currentWrap.find('.prev'),
                next: currentWrap.find('.next'),
                activeClass: 'active'
            };
            var slider =  new Sly(currentSlider, options);
            // if($('#oneperframe1').hasClass('frame')){
            //     slider.init();
            // }

            if($('.mslide').hasClass('frame')){
                slider.init();
            }
            //resizeContainerImages('div.overview div.page div.image img',580,387);
            return slider;
        };
	}

// Test via a getter in the options object to see if the passive property is accessed
var supportsPassive = false;
try {
    var opts = Object.defineProperty({}, 'passive', {
        get: function() {
            supportsPassive = true;
        }
    });
    window.addEventListener("test", null, opts);
} catch (e) {}


(function(namespace) {
    namespace._passiveEL = {};

    namespace.addPassiveEventListener = function(event, fn, prepend) {

        if(!namespace._passiveEL[event])
        {
            namespace._passiveEL[event] = [];
            namespace._passiveEL[event+"_timeout"] = false;

            namespace.addEventListener(event, function()
            {
                if(true) // set to true to disable timeout
                {
                    namespace._passiveEL[event].forEach(function(fn){fn()});
                }
                else
                {
                    if(namespace._passiveEL[event+"_timeout"])
                        clearTimeout(namespace._passiveEL[event+"_timeout"]);

                    namespace._passiveEL[event+"_timeout"] = setTimeout(function(){
                        namespace._passiveEL[event+"_timeout"] = false;
                        namespace._passiveEL[event].forEach(function(fn){fn()});
                    }, 50);
                }

            }, supportsPassive ? { passive: true } : false);
        }

        if(prepend)
            namespace._passiveEL[event].unshift(fn);
        else
            namespace._passiveEL[event].push(fn);

    }
})(window);





(function(namespace) {
    if ('replaceState' in history) {
        namespace.replaceHash = function(newhash) {
            if(!newhash) return;
            if ((''+newhash).charAt(0) !== '#')
                newhash = '#' + newhash;
            history.replaceState('', '', newhash);
        }
    } else {
        var hash = location.hash;
        namespace.replaceHash = function(newhash) {
            if(!newhash) return;
            if (location.hash !== hash)
                history.back();
            location.hash = newhash;
        };
    }

    if('pushState' in history) {
        namespace.setHash = function(newhash) {
            if(!newhash) return;
            history.pushState(null, null, '#'+newhash);
        }
    } else {
        namespace.setHash = function(newhash) {
            if(!newhash) return;
            location.hash = newhash;
        }
    }

})(window);



(function($) {
    $.fn.sticky = function (sticky) {
        $(this).each(function() {
            var el = $(this),
                tagName = el.prop('tagName'),
                no_sticky_class = 1 + $.inArray(tagName, ['HEADER','FOOTER']);

            if(sticky && !el.hasClass('sticky'))
            {
                if ('HEADER' == tagName)
                {
                    if ($('header .hamburger.active').length > 0) {
                        return;
                    }

                    $('<div>')
                        .attr('id','header_paceholder')
                        .css('height', el.outerHeight()+'px')
                        .prependTo('body');
                }

                if (isTabletViewport()
                    && el.filter('section.filters').length > 0
                    && $('#tabletFilters > .menu_label.active, #tabletFilters #filter-panel-selected.active').length > 0) {
                    return;
                }

                el.addClass('sticky');
                if(no_sticky_class)
                    el.removeClass('no-sticky');

            }
            else if(!sticky && el.hasClass('sticky'))
            {
                el.removeClass('sticky');
                if(no_sticky_class)
                    el.addClass('no-sticky');
                if('HEADER' == tagName)
                    $('#header_paceholder').remove();
            }
        });

        return this;
    };
}(jQuery));


(function($) {
     $.fn.scrollTo = function(callBack) {

        var el = $(this);
        if(!el.length)
            return false;

        var dst = $(document).scrollTop(),
            offset = el.offset().top,    // - 50 - $('header.no-sticky').height();
            direction = dst > offset ? 'up' : 'dn', // up => true, down => false,
            filtersHeight = $('.basic_nav_opened.filters').height() || 0,
            headerHeight = $('header').height(),
            headerSticky = headerHeight < 100,   // 50 - sticky, 200 - no-sticky
            scrollTime;

        offset -= 10; // little air above target element

//        if(headerSticky)
//            offset -= 50; // Dusty: jQuery wrong calculates header.sticky height into offset
        if('up' == direction) {
            offset -= 70;
        } else {
            offset -= 20;
        }

        // Correctio for the filters
        if(filtersHeight)
        {
            offset -= filtersHeight;
        }

        if('replaceHash' in window && el.attr('id'))
            window.replaceHash(el.attr('id'));

        scrollTime = Math.abs(offset - dst);

        if (scrollTime > animationTime * 2)
            scrollTime = animationTime * 2;

        if (TABLET) {
            scrollTime = 0;
        }

        $("html, body")
            .animate({scrollTop: Math.ceil(offset) + "px"}, scrollTime, "swing")
            .promise()
            .then(function() {
                if("function" === typeof callBack)
                    callBack();
            });

        return this;
    };
}(jQuery));


(function($) {
     $.fn.inViewport = function(partial, partiallyVisibleOnX) {
        if(!this.length)
            return false;

        partiallyVisibleOnX = partiallyVisibleOnX || false;
        var rec = this[0].getBoundingClientRect(),
            $w = $(window), vpWidth = $w.width(), vpHeight = $w.height(),
            tViz = rec.top >= 0 && Math.round10(rec.top, 0) < vpHeight,
            bViz = rec.bottom > 0 && Math.round10(rec.bottom , 0) <= vpHeight,
            lViz = rec.left >= 0 && Math.round10(rec.left, 0) < vpWidth,
            rViz = rec.right > 0 && Math.round10(rec.right, 0) <= vpWidth,
            vVisible   = partial ? tViz || bViz : tViz && bViz,
            hVisible = partial ? lViz || rViz : lViz && rViz;

        return (vVisible && hVisible) || (partiallyVisibleOnX && hVisible);
        /*
        var rect = this[0].getBoundingClientRect();
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && // or $(window).height()
            rect.right <= (window.innerWidth || document.documentElement.clientWidth) // or $(window).width()
        )
        */

    }
}(jQuery));



(function($) {
    $.fn.splitList = function(rpc, skip) {

        var el = $(this),
            tag = el.prop("tagName"),
            lis,
            cols,
            li,
            width = 0,
            owidth = 0,
            i,
            idx,
            newpos,
            liarr = [];

        /*
        newcol = floor( index / rpc)
        colpos = index % rpc
        newpos = newcol + cols*colpos
        */

        if(!el.length)
            return false;

        if(!skip)
            skip = 0;

        if("OL" != tag && "UL" != tag)
            console.warn("Not list tag");

        else if(!el.is(":visible"))
            console.warn("Only visible elements");

        else
        {
            el.children("li.puttyLI").remove();
            lis = el.children("li");

            if(lis.length - skip >= rpc)
            {

                el.removeAttr("style").css("white-space","normal");
                lis.removeAttr("style").css({"white-space":"nowrap","display":"inline-block"});

                cols   = Math.ceil( (lis.length - skip)/rpc );

                for(i = 0; i < lis.length; i++)
                {
                    li = $(lis[i]);
                    width  = Math.max(width, li.width());
                    owidth = Math.max(owidth, li.outerWidth());
                }

                while (cols*(rpc-1) >= lis.length - skip)
                    --rpc;


                for(i = 0; i < lis.length; i++)
                {
                    li = $(lis[i]);
                    if(!li.attr("orgpos"))
                        li.attr("orgpos",i);

                    idx = parseInt(li.attr("orgpos"));

                    li.css("width", idx < skip ? cols*owidth : width + 'px');

                    newpos = idx < skip ? idx : Math.floor((idx-skip)/rpc) + (cols)*((idx-skip)%rpc) + skip;

                    liarr[newpos] = li.detach();
                }

                for(i = 0; i < cols*rpc + skip; i++)
                {
                    if(liarr[i])
                        el.append(liarr[i]);
                    else
                        el.append($("<li>")
                            .attr("class","puttyLI")
                            .css({"width":width+'px',"height":"20px","display":"inline-block","visibility":"hidden"})
                            .html("&nbsp;"));
                }
            }
        }

        return this;
    };
}(jQuery));


function isEvent(event)
{
    return (event && event.target) ? true : false;
}


function stopEvent(event)
{
    if (isEvent(event)) {
        event.preventDefault();
        event.stopPropagation();
    }
    return event;
}


function imgTest(p) {
    if(!p) p = 1000;
    $('img').each(function(){
        var t = $(this), vW = t.width(), vH = t.height();
        if(0 === vW || 0 === vH ) return true;
        var vR = Math.round((vW/vH)*p)/p, i = new Image();
        i.src = t.attr('src');
        i.onload = function() {
            var oW = i.width, oH = i.height, oR = Math.round((oW/oH)*p)/p, src = i.src;

            if( (oW == vW && oH == vH) || oR == vR ) return;
            console.log('Vis:', vW+'x'+vH,'('+vR+')','Org:',oW+'x'+oH,'('+oR+')',src);
        }
    });
}


/*
function parcali() {
    return  $('*').filter(function() {
        var that = $(this), pos = that.css("position");
        return ('fixed' === pos || "absolute" === pos ) && that.inViewport();
    });
}
*/

var Menu = function() {

    this.menues = {};

    var _this = this;

    $(document).ready(function(){
        $('<div>').attr('id','menu_overlay').prependTo('body');
    });

    this.bodyWidthDiff = 0;

    this.show = function (event) {

        var el = event ? $(event.target) : false,
            fromNav = el ? el.closest('#rubrics').length : false,
            ref = fromNav ? el.closest('li').attr('id') : false,
            basic_nav_opened = $('.basic_nav_opened:not(.filters)'),
            showArguments = arguments;

        if (fromNav) {

            if ( !basic_nav_opened.length || (basic_nav_opened.inViewport(true) && basic_nav_opened.attr('ref') === ref) ) {
                // do nothing; link from menu; just let browser navigate to link
                return this;
            } else {
                stopEvent(event);
            }
        }

        // Prevent scrolling on macOS tablet devices
        if (TABLET
            && isDesktopViewport()
            && typeof document.body.style['webkitOverflowScrolling'] !== 'undefined') {
            $('html').css('position', 'fixed');
        }

        basic_nav_opened.attr('ref', ref).css('z-index',10);
        _this.overlay(1);

        if (ref) {
            $('#rubrics li').removeClass('activeTmp');
            el.closest('li').addClass('activeTmp');

            if (!_this.menues[ref]) {
                _this.fetchMenuContent(ref, function (data) {
                    data = data.trim();
                    if (el && ('' == data || '<span id="menu_close" class="popup-close"></span>' == data) ) { // quick and durty fix (task #14611)
                        _this.hide(function () {
                            window.location.href = el.attr('href');
                        });
                    } else {
                        _this.menues[ref] = data;
                        _this._show(ref, showArguments);
                    }
                });
            } else {
                _this._show(ref, showArguments);
            }
        } else {
            _this._show(false, showArguments);
        }

        return _this;
    },

    this.fetchMenuContent = function (menuId, callback) {
        $.get('menu/' + menuId, function (data) {
            if (typeof callback === 'function') {
                callback(data);
            }
        });
    },

    this._show = function (ref, showArguments) {
        var basic_nav_closed = $('.basic_nav_closed'),
            basic_nav_opened = $('.basic_nav_opened:not(.filters)'),
            header = $('header'),
            hTop = header.offset().top + header.height() - $(window).scrollTop();

        if (isTabletViewport()) {
            var $rubricsWrapper = header.find('#rubrics_wrapper');
            hTop = header.offset().top + header.height() + $rubricsWrapper.outerHeight();
        }

        if (ref && _this.menues[ref]) {
            basic_nav_opened.find('div.basic_nav.basic_menu').html(_this.menues[ref]);
        }

        $(document).trigger('afterAttachingMenuChildItems.DBDN_General');

        // Load images for the current resolution
        imageProcess.setResolutionPictures('#menu_carousel_wrapper');
        imageProcess.lazyLoadWithCallBack($('#menu_carousel_wrapper img.lazyload-thumb'));
        imageProcess.updateLazyload();
        $('#menu_slider').bind('wheel', imageProcess.updateLazyload);
        $('.next.slider_hover, .prev.slider_hover').click(imageProcess.updateLazyload);

		/*
        if($('#fullsize_banner').length || !header.inViewport(true))
        {
            window.scrollTo(0, hTop - 50);
            window.lastScrollTop = $(window).scrollTop() + 1;
            stickyMess();
        }
        */

        if(basic_nav_closed.length)
            basic_nav_closed.hide();

        //flyoutSearch.hide();
        if (!isTabletViewport()) {
            closeShowedPopup('.basic_nav');
        } else {
            closeShowedPopup('.basic_nav, .hamburger');
        }

        basic_nav_opened.css('visibility', 'hidden').show(0);

        $('.menu_carousel').addClass('hidden');

        if ($('.menu_carousel').inViewport() || DBDN.utils.isForcedDesktopMode()) {
           $('.menu_carousel').removeClass('hidden').show(0, createMenuSlider);
        }

        basic_nav_opened.hide(0).css('visibility', 'visible');

        var slideDuration = animationTime;

        if (isTabletViewport()) {
            slideDuration = 0;
        }

        basic_nav_opened
                .css("top", hTop + "px")
                .slideDown(slideDuration / 2)
                .promise()
                .then(function() {
                    _this.overlay(1);
                    // stupid callback - maybe never needed ?!
                    for (var i = 0; i < showArguments.length; i++) {
                        if("function" === typeof showArguments[i])
                            showArguments[i]();
                    }
                });

        return _this;
    },

    this.hide = function (callBack, hideDuration) {
        hideDuration = (typeof hideDuration !== 'undefined' && (hideDuration === 0 || hideDuration > 0)) ? hideDuration : animationTime;

        if (hideDuration < 1) {
            $('.basic_nav_opened:not(.filters)').hide();
            hideProcessComplete();
        } else {
            $('.basic_nav_opened:not(.filters)').slideUp(hideDuration, hideProcessComplete);
        }

        function hideProcessComplete() {
            $('html').css('position', '');

            $(this).css("top","").attr('ref', '');
            $('#rubrics li').removeClass('activeTmp');
            $('#rubrics li.active').addClass('activeTmp');

            var basic_nav_closed = $('.basic_nav_closed');

            if(basic_nav_closed) {
                basic_nav_closed
                    .show()
                    .promise()
                    .then(function() {
                        if("function" === typeof callBack)
                            callBack();
                    });
            } else {
                if("function" === typeof callBack)
                    callBack();
            }
        }

        return _this.overlay(0);
    },

    this.overlay = function (on, el, callback) {

        var menu_overlay = $('#menu_overlay'),
            activated = menu_overlay.is(':visible');

        menu_overlay.css('z-index', el && on ? $(el).css('z-index') - 1 : '');

        if (on && !activated)
        {
            var bw = $('body').width();

            $('html').css('overflow','hidden');

            var bwdiff = $('body').width() - bw;

            $('body').css('width', bw + 'px');

            $('header.sticky, footer.sticky').css('right', bwdiff + 'px');
            $('header.sticky').css('width', $('header.sticky').width() - bwdiff + 'px');
            $('footer.sticky').css('width', $('footer.sticky').width() - bwdiff + 'px');
            $('#cd-vertical-nav.sticky').css('right', bwdiff + parseInt($('#cd-vertical-nav').css('right')) + 'px' )
            $('.basic_nav_opened').css('width', $('.basic_nav_opened').width() + 'px');
            checkFiltersCount();
            menu_overlay.css('border-right-width', bwdiff + 'px').fadeIn(animationTime / 4, function () {
                if (typeof callback === 'function') {
                    callback();
                }
            });
        }
        else if((on && activated) || (!on && !activated)) {
            if (typeof callback === 'function') {
                callback();
            }
        }
        else if(!on && activated)
            menu_overlay.fadeOut(animationTime / 4, function(){
                $('html').css('overflow','');
                $('body').css('width', '');
                $('header, footer, #cd-vertical-nav').css({'right':'','width':''});
                $('.basic_nav_opened').css('width', '');
                checkFiltersCount();

                if (typeof callback === 'function') {
                    callback();
                }
            });
        return _this;
    },


    this.resolve = function(loc) {
        var el = $(loc);
        if(el.length)
        {
            if (isTabletViewport()) {
                el.scrollTo();
                closeShowedPopup();
            } else {
                menu.hide(function () { el.scrollTo() });
            }

            return false;
        }
        if (isTabletViewport()) {
            closeShowedPopup();
        } else {
            menu.hide();
        }

        return true;
    }

    function createMenuSlider() {
        var menu_slider = $('#menu_slider');
        if (menu_slider.length > 0 && menu_slider.data().sly === undefined) {
            var wrap = menu_slider.parent();
            new Sly(menu_slider, {
                horizontal: 1,
                itemNav: 'forceCentered',
                smart: 1,
                activateMiddle: 1,
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1,
                startAt: 0,
                scrollBy: 1,
                pagesBar: wrap.find('.pages'),
                activatePageOn: 'click',
                speed: 300,
                elasticBounds: 1,
                easing: 'easeOutExpo',
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,
                // Buttons
                prev: wrap.find('.prev'),
                next: wrap.find('.next'),
                activeClass: 'active'
            }).init();
        }
    }
}

var menu = new Menu();




/*** Flyout Search ***/

var FlyoutSearch = function(toggleSelector) {

    var _this = this;

    $(document).ready(function() {
        _this.el = $(".flyout_search");
        _this.sb = $("a.search_button");
        if(toggleSelector) {
            $(toggleSelector).click(_this.toggle);
            _this.el.find('.popup-close').click(_this.hide);
        }
    });


    this.show = function (event) {
        stopEvent(event);

        closeShowedPopup();
        //menu.hide();
        menu.overlay(true, _this.el);

        _this.sb.addClass('active');
        var height = _this.el.height();
        _this.el.css({display:'block', height:0}).animate({height:height}, animationTime, function(){
            _this.el.find("input[type=text]").focus();
        });
    }

    this.hide = function (event) {
        if(isEvent(event)) {
            stopEvent(event);
            _this.el.animate({height:0},animationTime, function(){
                _this.el.css({display:'none', height:'auto'});
                menu.overlay(false);
            });
        } else {
            _this.el.css({display:'none', height:'auto'});
        }
        _this.sb.removeClass("active");
    }

    this.toggle = function (event) {
        if(_this.sb.hasClass("active")) {
            _this.hide(event);
        } else {
            _this.show(event);
        }
    }

}

var flyoutSearch = new FlyoutSearch('a.search_button');



var Filter = function() {

    var _this = this;

    $(document).ready(function(){

        var fl = $('.filter_label');

        if(!fl.length)
            return;

        if($("#best_practice_stats").length)
            $("#best_practice_stats").addClass('filters');
        if($("#downloads_stats").length)
            $("#downloads_stats").addClass('filters');

        $("span.filter_close").click(function(){
            if(MOBILE)
            {
                switchMobileFilterMenu();
            } else if(isTabletViewport()) {
                closeShowedPopup();

                return false;
            }
            else
            {
                $(this).closest("li.cf.active").removeClass("active");
            }

            menu.overlay(0);
        });


        fl.each(function(){

            var el = $(this),
                t = el.attr("title");

            if(t)
            {
                el.removeAttr("title").attr("popup-title", t);
                el.hover(_this.popupShow, _this.popupHide);
            }

            el.click(_this.expand);
        });

    });


    this.expand = function()
    {
        /*
        var sf = $("section.basic_nav_opened.filters");
        if(sf.length && !sf.hasClass('sticky'))
            window.scrollTo(0, sf.offset().top);
        */

        var el = $(this),
            p  = el.parent(),
            i  = p.find('input'),
            ul = p.find('ul'),
            split, rpc, skip;
            if(p.hasClass('active'))
            {
                p.removeClass('active');
            }
            else
            {
                p.closest('.basic_nav').find('li.active').removeClass('active');
                if(1 == i.length)
                    i[0].click();
                else
                {
                    p.closest('section.search-section').scrollTo();
                    menu.overlay(1);
                    p.addClass('active');
                    split = ul.attr('splitlist');
                    if(split)
                    {
                        split = split.split(',');
                        rpc = parseInt(split[0]);
                        skip = split[1] ? parseInt(split[1]) : 0;
                        ul.splitList(rpc, skip);
                    }
                }
            }

    };


    this.popupShow = function()
    {
        _this.popupHide();

        if($(this).closest("li").hasClass("active"))
            return;

        var popup = $("<div>")
                        .attr({"id":"filterPopup","class":"popup"})
                        .text($(this).attr("popup-title"))
                        .append($("<div>").attr("class","popup-ticker"))
                        .appendTo($(this).parent());

        var height = popup.outerHeight();

        $("section.basic_nav_opened.best_practice").css("z-index","200");
        //popup.css({visibility:'visible', height:0}).animate({height:height},animationTime / 4);
        popup.css({visibility:'visible'});
    };


    this.popupHide = function()
    {
        $("section.basic_nav_opened.best_practice").css("z-index","");
        $("#filterPopup").remove();
    };


    this.hide = function(callBack)
    {
        if("function" === typeof callBack)
            callBack();
    };

    this.change = function(event)
    {
        addSelection(event.target,event);
        sendBrowserRequest();
    };

    this.count = function()
    {
        return $('#filter-panel-selected .filter-tags').not('#filter-example').length;
    };

    this.getCountOfSelectedFilterOptions = function () {
        return this.count();
    };

    this.drop = function(el)
    {
        $(el).closest(".filter-tags").remove();

        if(0 == _this.count())
            _this.clear();
        else
            if(MOBILE){
                sendMobileBrowserRequest();
            }else{
               sendBrowserRequest();
            }

    };

    this.clear = function()
    {
        //window.location.reload();
        $('#filter-panel-selected .filter-tags').not('#filter-example').remove();

        $('.level3_nav input').each(function(){
           if($(this).attr('type') == 'text')
                $(this).val('');
            else
                $(this).prop('checked',false);
        });
        $('.level3_nav .active span').click();

        if(MOBILE) {
            $('#remove_filters').css('display','none');
            $("#filter-panel-selected").removeClass("dropdown active");
            sendMobileBrowserRequest();
            menu.overlay(false);
        } else if (isTabletViewport()) {
            $('div.filter-panel-title').removeClass('active');
            sendBrowserRequest();
        } else {
            $('.filter-panel').hide();
            sendBrowserRequest();
        }
    };


    this.compactSelection = function()
    {
        var f = $("#filter-panel-selected"),
            l = f.find("label").length - 1,
            section = $("#searchDiv").closest("section");

        $("#active_filter_count").html(l);

        if(l > 1)
        {
            f.addClass("dropdown");
            if(!MOBILE) {
                $("#remove_filters").css('display', 'block');
            }
        }
        else
        {
            f.removeClass("dropdown active");
            $('#remove_filters').css('display','none');
        }

        if(l > 0)
        {
            if(MOBILE){
                $(".basic_nav .filter-panel").addClass('no-empty');
                $("section.section_content.best-practice").css("margin-top", "189px");
                $(".downloads_overview").addClass("filtered");
                
            }
            $(".filter_hide").fadeOut();
            if(! section.hasClass("search-section"))
                section.fadeOut();
        }
        else
        {
            if(MOBILE){
                $(".basic_nav .filter-panel").removeClass('no-empty');
                $("section.section_content.best-practice").css("margin-top", "");
                $(".downloads_overview").removeClass("filtered");
            }
            $(".filter_hide").fadeIn();
            if(! section.hasClass("search-section"))
                section.fadeIn();
        }
    };

};


var filter = new Filter();







function stickyMess()
{
    var $window = $(window), scrollTop = $window.scrollTop(),
            scrollTolerance = 4;


        if (scrollTop > window.lastScrollTop + scrollTolerance)  // scroll Down
        {
            $('header, #basic_nav_closed').sticky(false);

            $('section.filters').sticky(scrollTop > 160 + stickyAddH).removeClass('up');

            $('body').removeClass('show-sticky-filters');
        }
        else if (scrollTop < window.lastScrollTop - scrollTolerance)  // scroll Up
        {
            var isSticky = scrollTop > 140 && !$('#hamburger').hasClass('active') && !(MOBILE && $('.flyout_menu').is(":visible"));
            $('header').sticky(isSticky);
            if (isSticky) {
                $('body').addClass('show-sticky-filters');
            } else {
                $('body').removeClass('show-sticky-filters');
            }
            $('#basic_nav_closed').sticky(scrollTop > 150 + stickyAddH);
            $('section.filters').sticky(scrollTop > 150 + stickyAddH).addClass('up');
        }
        else // no scroll
        {
            //
        }

        $('#cd-vertical-nav, .back-button').sticky(scrollTop > 150 + stickyAddH);

        var $footer = $('footer');

        var isSticky = ($(document).height() - scrollTop - $(window).height()) > 120 && !scrollTop == 0;

        $footer.sticky(isSticky);

        if (isSticky) {
            var currentScrollXOffset = $window.scrollLeft() * -1;

            $footer.css('transform', 'translate3d(' + currentScrollXOffset + 'px, 0px, 0px)');
        }

        window.lastScrollTop = scrollTop;

        /*
        if (scrollTop > 150)
        {
            $('.back-button').sticky(true);
            $('.level3_nav').css('max-height', '');
        }
        else if(scrollTop > 0)
        {
            if(scrollTop > 30){
                $('.back-button').sticky(false);
                $('.back-button').css('top','');
            }

            var con  = $('body').hasClass('smart') ? 441 : 433;

            var height = (con - scrollTop + $('footer.sticky').outerHeight()) + 'px';

            $('.sticky .level3_nav').css('max-height', 'calc(100vh - '+height+')');
        }
        else
        {
            $('.back-button').sticky(false);
            $('.back-button').css('top','');
            $('.sticky .level3_nav').css('max-height', '');
        }
        */
        if($('.user_tools .profile-formular').css('visibility') == "visible")
            $('.user_tools .profile-formular').find('.popup-close').trigger('click');
}



function calcMessTops()
{

    stickyAddH = 0 + $("#fullsize_banner").outerHeight();// + $("#notification:visible").outerHeight();

    var basic_nav_closed = $("#basic_nav_closed"),
        filters = $("section.filters"),
        cd_vertical_nav = $("#cd-vertical-nav");

    if(basic_nav_closed.length)
        basic_nav_closed
            .css("top", "")
            .css("top", Math.round(basic_nav_closed.offset().top + stickyAddH) + "px");

    if(filters.length)
        filters
            .css("top", "")
            .css("top", Math.round(10 + filters.offset().top + stickyAddH) + "px");

    if(cd_vertical_nav.length)
        cd_vertical_nav
            .css("top", "")
            .css("top", Math.round(cd_vertical_nav.offset().top + stickyAddH) + "px");
}


function placeholderFix(el) {
    var el = $(this);
    if('' != el.val()) {
        el.removeClass('placeholder-shown');
    } else {
        el.addClass('placeholder-shown');
    }
}

function addFilterToStickyOnMobile () {
    if(MOBILE) {
        setSmartLabels();
        $('section.filters div.basic_nav').click ( function (e) {
            if ( $(window).scrollTop() > 190 && ($(e.target).hasClass('menu_label') || $(e.target).closest('.filter-panel').length)) {
                $('html, body').animate( {scrollTop:0} );

            }
        });
    }
}

// sticky mess
window.lastScrollTop = 0;
window.stickyAddH = 0;

$(document).ready(function() {

    // fix for unsupported in IE pseudo class :placeholder-shown
    $('input[placeholder]').each(placeholderFix);
    $(document).on('keydown keypress keyup paste', 'input[placeholder]', placeholderFix);

    addFilterToStickyOnMobile();

    calcMessTops();

    stickyMess();

    addPassiveEventListener("scroll", stickyMess); //$(window).scroll(stickyMess);

    $(document).on('tabletDesktopViewportChange.DBDN_General', function () {
        if (isDesktopViewport()) {
            calcMessTops();
        }
    });
});

function setEqualTitle(titlesList) {
    $(titlesList).css('height', 'auto');
    var titleMaxHeight = 0;
    var currentHeight = 0;
    if($(titlesList).length) {
        $(titlesList).each(function() {
            currentHeight = $(this).innerHeight();
            if(currentHeight > titleMaxHeight) {
                titleMaxHeight = currentHeight;
            }
        });
        $(titlesList).css({
            'height': titleMaxHeight
        });
    }
}

function checkForLabelValue() {
    $('.filter-panel .filter-tags label').each(function() {
        if(!$(this).text()) {
            $(this).children('.filter_value').css({
                'display': 'block',
                'max-width': '50%',
                'float': 'left'
            });
        }
    });
}

function setSmartLabels() {
    checkForLabelValue()
    var language = $('#language a').text();
    if(language == 'Deutsch' && $('body').hasClass('smart')) {
        $('.filter-panel .filter-tags label').each(function() {
            if($(this).first().text().indexOf('A-Z') != -1 || $(this).first().text().indexOf('Z-A') != -1) {
                $(this).css('text-transform', 'none');
                //$(this).children().css('text-transform', 'none');
            } else {
                $(this).css('text-transform', 'lowercase');
            }
        });
    } else if (language == 'English' && $('body').hasClass('smart')) {
        $('.filter-panel .filter-tags label').css({
            'text-transform': 'none'
        });
    }
}

function setTitlesCapitalization(titles) {
    var language = $('#language a').text();
    if(language == 'Deutsch') {
        if($(titles).length) {
            $(titles).each(function() {
                //$(this).css('text-transform', 'lowercase');
                //TODO: Check for pseudo elements
                //$(this).text().toLowerCase();
                //$(this).text()[0].toUpperCase();
            });
        }
    } else if (language == 'English') {
        $(titles).css({
            'text-transform': 'none'
        });
    }
}

function checkFiltersCount() {
    if(MOBILE) {
        if(parseInt($('#active_filter_count').text()) <= 1) {
            $('.filter_remove').css('margin-top', '7px');
        } else {
            $('.filter_remove').css('margin-top', '0px');
        }
    }
}

function groupEqualTitlesSections() {
    if($(window).width() >= 767) {
        setEqualTitle($('.slider-item .title'));
        setEqualTitle($('.inner1-wrapper .title'));
        setEqualTitle($(".grid-page .title"));
        setEqualTitle($('.frontblock_col .title'));
        //setEqualTitle($('.title-chb label span'));
        setEqualTitle($('.download_elements .title-chb'));
    }
}

var BrowserDetect = {
    init: function () {
        this.browser = this.searchString(this.dataBrowser) || "Other";
        this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
    },
    searchString: function (data) {
        for (var i = 0; i < data.length; i++) {
            var dataString = data[i].string;
            this.versionSearchString = data[i].subString;

            if (dataString.indexOf(data[i].subString) !== -1) {
                return data[i].identity;
            }
        }
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index === -1) {
            return;
        }

        var rv = dataString.indexOf("rv:");
        if (this.versionSearchString === "Trident" && rv !== -1) {
            return parseFloat(dataString.substring(rv + 3));
        } else {
            return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
        }
    },

    dataBrowser: [
        {string: navigator.userAgent, subString: "Edge", identity: "MS Edge"},
        {string: navigator.userAgent, subString: "MSIE", identity: "Explorer"},
        {string: navigator.userAgent, subString: "Trident", identity: "Explorer"},
        {string: navigator.userAgent, subString: "Firefox", identity: "Firefox"},
        {string: navigator.userAgent, subString: "Opera", identity: "Opera"},  
        {string: navigator.userAgent, subString: "OPR", identity: "Opera"},  

        {string: navigator.userAgent, subString: "Chrome", identity: "Chrome"}, 
        {string: navigator.userAgent, subString: "Safari", identity: "Safari"}       
    ]
};


function showMoreButtonHandle() {
    if(!$('#downloads_related .hided').length) {
        $('#load-more_wrapper').hide();
    }
}


$(document).on('ready', function() {

    showMoreButtonHandle();

    BrowserDetect.init();
    $('html').addClass(BrowserDetect.browser);
    $('html').addClass(navigator.platform);
    //alert("You are using <b>" + BrowserDetect.browser + "</b> with version <b>" + BrowserDetect.version + "</b>");
    setSmartLabels();
    //setTitlesCapitalization($('.hct'));

    groupEqualTitlesSections();

    // Setting related downloads on exposed page template
    var downl = $('#related_downlowds');
    var pag = $('#related_pages');

    if(downl && pag && $(window).width() > 767 && $('.exposed')) {
        if (downl.innerHeight() > pag.innerHeight()) {
            pag.css({ 'height': downl.innerHeight() });
        } else {
            downl.css({ 'height': pag.innerHeight() });
        }
    }
});



//alert('MY_CONST: ' + CONFIG.get('MY_CONST'));  // 1
var CONFIG = (function() {
     var private = {
         'DATE_TIME_ZERO': '0000-00-00 00:00'
     };

     return {
        get: function(name) { return private[name]; }
    };
})();

function getRelativePath(){
    var pathArray = window.location.pathname.split( '/' );
    var found = 0;
    var relPath = "";
    var view = "cms";
    for ( i = 0; i < pathArray.length; i++ ) {
      if(found == 1){
          relPath += "../";
      }
      if(pathArray[i] == view){
          found = 1;
      }

    }

    return relPath;
}

$(function(event) {
	if($('#admin_action').length != 0){
		if($('.admin_data').hasClass('editMode'))
		{
			$('.admin_data').removeClass('editMode');
		}
		$("#admin_info").click(function(){
			if($('#admin_action').is(":visible"))
			{
				$("#admin_info").removeClass('active');
				$('#admin_action').hide();
			}else{
				$("#admin_info").addClass('active');
				$('#admin_action').show();
			}
		});
	}else{
		$('.admin_data').addClass('editMode');
	}
});

/*******************************************************************************
 * JSON Error Handling
 *******************************************************************************/
/**
 * @returns {JSON}
 */
var JSON_ERROR = {
}

JSON_ERROR.errorResponse2JSON = function(XMLHttpRequest) {
	var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
	var data = JSON.parse(fixedResponse);
	return data;
}

JSON_ERROR.composeMessageFromJSON = function(jsonData, processRespnce) {
	var message = jsonData.message;
	if (processRespnce && jsonData.response !== undefined) {
		for (var key in jsonData.response) {
			if (key === 'length' || !jsonData.response.hasOwnProperty(key)) continue;
			var value = jsonData.response[key];
			if (value instanceof Array) {
				message+= ' ' + value.join('; ');
			} else {
				message+= ' ' + value;
			}
		}
	}
	return message;
}

/*******************************************************************************
 * JQUERY Extensions
 *******************************************************************************/
/*
 * Open modal dialog
 */
var INDEX_OFFSET = 1000;

if(jQuery) (function($){
	$.extend($.fn, {
		openDialog: function(overlay) {
			// Get first element
			var dialog = this.get(0);
			// Ceate Overlay
			if (overlay) {
				this.data('dialogOverlay', overlay);
				overlay.show();
			}
			// Lock taborder to modal dialogue!
			var lstInputs = $(':not(div, p, span, script, link, option)').filter(':not(#' + dialog.id + ' *)');
			if (lstInputs) {
				$.each(lstInputs, function(key, value){
					var tabIndex = value.tabIndex;
					value.tabIndex = tabIndex - INDEX_OFFSET;
				});
			}
			// Show the modal dialog
	        this.show();
		},
		closeDialog: function() {
			// Get first element
			var dialog = this.get(0);
			//  Hide Overlay
			var overlay = this.data('dialogOverlay');
			if (overlay) {
				overlay.hide();
			}
			// UnLock taborder for other inputs!
			var lstInputs = $(':not(div, p, span, script, link, option)').filter(':not(#' + dialog.id + ' *)');
			if (lstInputs) {
				$.each(lstInputs, function(key, value){
					var tabIndex = value.tabIndex;
					value.tabIndex = tabIndex + INDEX_OFFSET;
				});
			}
			// Show the modal dialog
	        this.hide();
		},
		hasScrollBar: function() {
			return this.get(0).scrollHeight >  this.get(0).clientHeight;
		}
	});
})(jQuery);


/*******************************************************************************
 * DataTable Extensions
 *******************************************************************************/
function handleAjaxError( xhr, textStatus, error ) {
	var oSettings = oTable.fnSettings();
	var fnCallback = oSettings._fnReloadAjaxCompleate;
	if ( typeof fnCallback == 'function' && fnCallback !== null )
	{
		fnCallback( oSettings );
	}

	if ( textStatus === 'timeout' ) {
		jAlert(js_localize['general.error.timeout'], oSettings.sAlertTitle, "error");
	}
	else {
		var data = JSON_ERROR.errorResponse2JSON(xhr);
		var message = JSON_ERROR.composeMessageFromJSON(data, true);
		jAlert(message, oSettings.sAlertTitle, "error");
	}
	oTable.oApi._fnProcessingDisplay( oSettings, false );
}

function fnServerData( sUrl, aoData, fnCallback, oSettings ) {
	oSettings.jqXHR = $.ajax( {
		"url":  sUrl,
		"data": aoData,
		"success": function (json) {
			if ( json.sError ) {
				oSettings.oApi._fnLog( oSettings, 0, json.sError );
			}

			$(oSettings.oInstance).trigger('xhr', [oSettings, json]);
			fnCallback( json );
		},
		"dataType": "json",
		"cache": false,
		"type": oSettings.sServerMethod,
		"error": handleAjaxError
	} );
};

/*******************************************************************************
 * Other Extensions
 *******************************************************************************/

$(function(){
	// Iterate over each select element
$('select').each(function () {

    // Cache the number of options
    var $this = $(this),
        numberOfOptions = $(this).children('option').length;

    // Hides the select element
    $this.addClass('s-hidden');

    // Wrap the select element in a div
    $this.wrap('<div class="select"></div>');

    // Insert a styled div to sit over the top of the hidden select element
    $this.after('<div class="styledSelect" page_prop="'+$this.attr('name')+'"></div>');

    // Cache the styled div
    var $styledSelect = $this.next('div.styledSelect');

    // Show the first select option in the styled div
    $styledSelect.text($this.children('option:selected').eq(0).text());

    // Insert an unordered list after the styled div and also cache the list
    var $list = $('<ul />', {
        'class': 'options'
    }).insertAfter($styledSelect);

    // Insert a list item into the unordered list for each select option
    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    // Cache the list items
    var $listItems = $list.children('li');

    // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
    $styledSelect.click(function (e) {
        e.stopPropagation();
        $('div.styledSelect.active').each(function () {
            $(this).removeClass('active').next('ul.options').hide();
        });
        $(this).toggleClass('active').next('ul.options').toggle();
    });

    // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
    // Updates the select element to have the value of the equivalent option
    $listItems.click(function (e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
		$(".user_sort").val($(this).attr('rel'));
        $(this).parents('div.select').find('select').val($(this).attr('rel'));
        /* alert($this.val()); Uncomment this for demonstration! */
    });

    // Hides the unordered list when clicking outside of it
    $(document).click(function () {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});
});

function strip_tags(str){
	str = str.replace(/<[^>]*>/gi, '');
	return str;
}

function getRoleTextByID(role_id) {
	switch (role_id) {
		case 0:
			return js_localize['users.role.member'];
		case 1:
			return js_localize['users.role.approver'];
		case 2:
			return js_localize['users.role.administrator'];
		case 3:
			return js_localize['users.role.exporter'];
		case 4:
			return js_localize['users.role.editor_smart'];
		case 5:
			return js_localize['users.role.editor_mb'];
		case 6:
			return js_localize['users.role.editor_dfs'];
		case 7:
			return js_localize['users.role.editor_dfm'];
		case 8:
			return js_localize['users.role.editor_dtf'];
		case 9:
			return js_localize['users.role.editor_ff'];
		case 10:
			return js_localize['users.role.editor_dp'];
		case 11:
			return js_localize['users.role.editor_tss'];
		case 12:
			return js_localize['users.role.editor_bkk'];
		case 13:
			return js_localize['users.role.newsletter_approver'];
		case 14:
			return js_localize['users.role.newsletter_editor'];

        // New Brands and Roles for 2018
        case 15:
            return js_localize['users.role.editor_db'];
        case 16:
            return js_localize['users.role.editor_eb'];
        case 17:
            return js_localize['users.role.editor_setra'];
        case 18:
            return js_localize['users.role.editor_op'];
        case 19:
            return js_localize['users.role.editor_bs'];
        case 20:
            return js_localize['users.role.editor_fuso'];
        case 21:
            return js_localize['users.role.editor_dt'];
        case 22:
            return js_localize['users.role.editor_dmo'];
    }
}
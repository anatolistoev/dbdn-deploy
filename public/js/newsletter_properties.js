var content_save_error = false;
var langArray = {en: 1, de: 2};
var nlTitle = {'content_id': {}, 'text': {}};

var change_workflow_responsible = function(){};

function clearPropertiesForm() {
	nlTitle.content_id = {};
	$('#newsletter_form .input_group').removeClass('error');
	$('#newsletter_form .role_group').removeClass('error');
	$('#newsletter_form input:checkbox:checked').removeAttr('checked');
	$('#newsletter_form input:radio:checked').removeAttr('checked');
	$('#newsletter_form input:radio[value="0"]').prop('checked', true);
	$('#newsletter_form input#type_file').prop('checked', true);
	$('#newsletter_form input[name="id"]').val('0');
	$('#newsletter_form input:text').val('');
	$('#newsletter_form textarea').val('');
	$('#newsletter_form input[name="title_de"]').val('').parent().hide();
	$('#newsletter_form input[name="title_en"]').val('').parent().hide();
	$('#newsletter_form input[name="active"]').removeAttr('disabled');
    $('#template_select ul li:first-of-type').click();

}

function closePropertiesForm() {
	clearPropertiesForm();
	$('#properties_wrapper').hide();
	$('#properties_wrapper .user_fields').hide();
	$('.left_navigation .nav_box.properties').removeClass('active');
	$('body').css('cursor', 'default');
}

function saveNewsletterProperties(callbackFunc, type, selectedRow) {
    var promise = $.Deferred().reject();
	if (validatePropertiesFields()) {
		var url = relPath + "api/v1/newsletter/create";
		if (type == "PUT") {
			url = relPath + "api/v1/newsletter/update";
		}
		promise = jQuery.ajax({
			type: type,
			url: url,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: parseFormProp(),
			success: function(data) {
				if($("input[name = 'isPageRestricted']").length>0){
					$("input[name = 'isPageRestricted']").val(data.response.restricted);
				}
				saveContent((type == "PUT") ? true : false, data.response.id, function(has_content_error) {
					if (!has_content_error) {
						if (callbackFunc instanceof Function) {
							callbackFunc(data, selectedRow);
						}
						$('body').css('cursor', 'default');
						jAlert(data.message, js_localize['newsletter.edit.title'], "success");
					} else {
						$('body').css('cursor', 'default');
						jAlert(js_localize['newsletter.edit.error'], js_localize['newsletter.edit.title'], "error");
					}
				});
			},
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
				var message = JSON_ERROR.composeMessageFromJSON(data, true);
				if (data.response !== undefined) {
					$.each(data.response, function(key, value) {
						$('input[name="' + key + '"]').parent().addClass('error');
					});
				}
				$('body').css('cursor', 'default');
				jAlert(message, js_localize['newsletter.edit.title'], "error");
			}
		});
	}
    return promise;
}

function saveContent(isUpdate, newsletter_id, callbackFunc) {
	content_save_error = false;
	var createContentArray = [];
	var updateContentArray = [];
	for (var lang in langArray){
		if ($('input#language_' + lang).prop('checked') == true) {
			var title = $('input[name="title_' + lang + '"]').val();
			if (isUpdate && nlTitle.content_id[lang] > 0) {
				var content = {id: nlTitle.content_id[lang], content: title};
				updateContentArray.push(content);
			} else {
				// Create Title
				var content = {newsletter_id: newsletter_id, lang: langArray[lang], position: 'title', content: title};
				createContentArray.push(content);
			}
		}
	}

	// Create Title
	if (createContentArray.length > 0) {
		jQuery.ajax({
			type: "POST",
			url: relPath + "api/v1/nl_contents/create-many",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(createContentArray),
			success: function(data) {
				if (callbackFunc instanceof Function) {
					callbackFunc(false);
				}
			},
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				//TODO: Get Language for the error
				$('input[name="title_de"]').parent().addClass('error');
				$('input[name="title_en"]').parent().addClass('error');
				if (callbackFunc instanceof Function) {
					callbackFunc(true);
				}
			}
		});
	}
	// Update Title
	if (isUpdate && updateContentArray.length > 0) {
		jQuery.ajax({
			type: "PUT",
			url: relPath + "api/v1/nl_contents/update-many",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(updateContentArray),
			success: function(data) {
				if (callbackFunc instanceof Function) {
					callbackFunc(false);
				}
			},
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				//TODO: Get Language
				$('input[name="title_de"]').parent().addClass('error');
				$('input[name="title_en"]').parent().addClass('error');
				if (callbackFunc instanceof Function) {
					callbackFunc(true);
				}
			}
		});
	}
}

function populateFormNewsletter(newsletterId, callbackFunc) {
//	jQuery.when(
		jQuery.ajax({
				type: "GET",
				url: relPath + "api/v1/newsletter/read/" + newsletterId,
				success: function(data) {
					if(data.response.user_responsible != EDITING_USER && (data.response.editing_state != 'Draft' || data.response.user_responsible != 0)){
						change_workflow_responsible = function(){
							jConfirm(js_localize['newsletter.edit.workflow.responsible'],js_localize['newsletter.edit.title'], 'success',function(r){
								if(r){
									changeNewsletterResponsible(data.response.id,EDITING_USER);
								}else{
									var anSelected = fnGetSelected(oTable);
									var promise = unlockItem($(anSelected).find('.newsletter_id').html(),'newsletter');
									promise.always(function(){
										editing_timer.stopTimer(false);
										$('#wrapper_form_newsletter').closeDialog();
										$('.user_actions .nav_box').removeClass('active');
										$('.user_edit_wrapper .user_fields').hide();
										$('.user_table').css('height', '');
										$('.dataTables_paginate.paging_ellipses').show();
									});
								}
							});
						}
					}else{
                        change_workflow_responsible = function(){};
                    }
					$.each(data.response, function(key, value) {
						if (key == 'langs') {
							//TODO: Optimize it
							var binary = value.toString(2);
							if (value <= 1) {
								binary = "0" + binary;
							}
							var binary_array = binary.split('');
							console.log(binary_array);
							$('#newsletter_form input[name="' + key + '"]').each(function(i) {
								if (binary_array[i] == 1) {
									if (i == 0) {
										$('#newsletter_form input[name="title_de"]').parent().show();
									} else {
										$('#newsletter_form input[name="title_en"]').parent().show();
									}
									$(this).prop('checked', true);
								} else {
									if (i == 0) {
										$('#newsletter_form input[name="title_de"]').parent().hide();
									} else {
										$('#newsletter_form input[name="title_en"]').parent().hide();
									}
									$(this).prop('checked', false);
								}
							});

                        } else if ($('#newsletter_form input[name="' + key + '"]').attr('type') == 'radio') {
							$('#newsletter_form input[name="' + key + '"][value="' + value + '"]').prop('checked', true);
							if(key == 'active' && data.response.editing_state != 'Approved'){
                                $('#newsletter_form input[name="active"]').attr('disabled','disabled');
							}
                        }else if(key == 'titles'){
                            nlTitle.content_id = {};
                            var titles = data.response.titles;
                            $.each(titles, function(key, value) {
                                if (value.lang == 2) {
                                    $('input[name="' + value.position + '_de"]').val(value.content);
                                    nlTitle.text['2'] = value.content;
                                    nlTitle.content_id['de'] = value.id;
                                }else if (value.lang == 1) {
                                    $('input[name="' + value.position + '_en"]').val(value.content);
                                    nlTitle.text['1'] = value.content;
                                    nlTitle.content_id['en'] = value.id;
                                }
                            });
						} else if ($('p#' + key).length > 0) {
							$('p#' + key).html(value);
						}  else {
							$('#newsletter_form input[name="' + key + '"]').val(value);
						}
					});
                    callbackFunc(false);
				}, // success
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
					var message = JSON_ERROR.composeMessageFromJSON(data, true);
					jAlert(message, js_localize['newsletter.properties.title'], "error");
                    callbackFunc(true);
				}
			});
//	jQuery.ajax({
//		type: "GET",
//		url: relPath + "api/v1/nl_contents/newsletter-id/" + newsletterId + "?section=title",
//		success: function(data) {
//			//Reset title_content_id
//			nlTitle.content_id = {};
//			$.each(data.response, function(key, value) {
//				if (value.lang == 2) {
//					$('input[name="' + value.position + '_de"]').val(value.content);
//					nlTitle.text['2'] = value.content;
//					nlTitle.content_id['de'] = value.id;
//				}
//				if (value.lang == 1) {
//					$('input[name="' + value.position + '_en"]').val(value.content);
//					nlTitle.text['1'] = value.content;
//					nlTitle.content_id['en'] = value.id;
//				}
//			});
//		}, // success
//		error: function(XMLHttpRequest, textStatus, errorThrown) {
//			var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
//			var message = JSON_ERROR.composeMessageFromJSON(data, true);
//			jAlert(message, js_localize['newsletter.properties.title'], "error");
//		}
//	})
//			).then(function() {
//		callbackFunc(true);
//	}).fail(function(data) {
//		callbackFunc(false);
//	});
	return false;
}

function parseFormProp() {
	var serialized = $('#newsletter_form').serializeArray();
	var s = '';
	var langs_binary = 0;
	var data = {};
	var accessArray = [];
    var keywords_arr = [];
	for (s in serialized) {
		if (serialized[s]['name'] == "access") {
			var access = {group_id: serialized[s]['value']};
			accessArray.push(access);
		} else if(serialized[s]['name'] == "keywords_arr"){
            keywords_arr.push(JSON.parse(serialized[s]['value']));
        }else if (serialized[s]['name'] != "title_en" && serialized[s]['name'] != "title_de") {
			if (serialized[s]['type'] == "active" || serialized[s]['name'] == "searchable" || serialized[s]['name'] == "in_navigation") {
				data[serialized[s]['name']] = parseInt(serialized[s]['value']);
			} else if (serialized[s]['name'] == "langs") {
				langs_binary = langs_binary + parseInt(serialized[s]['value']);
			} else {
				data[serialized[s]['name']] = serialized[s]['value'];
			}
		}
	}
	data['langs'] = langs_binary;

	return JSON.stringify(data);
}

function hideTitle(el, lang) {
	if ($(el).prop('checked') == true) {
		$('input[name="title_' + lang + '"]').parent().show();
	} else {
		$('input[name="title_' + lang + '"]').parent().hide();
	}
}

function validatePropertiesFields() {
	if (($('input[name="title_en"]').val() == "" && $('input#language_en').prop('checked') == true)
			|| ($('input[name="title_de"]').val() == "" && $('input#language_de').prop('checked') == true)
            ||($('input[name="title_en"]').val() == "" && $('input[name="title_de"]').val() == "") ||  $('input[name="slug"]').val() == "") {
        var msg = js_localize['newsletter.add.empty_loc'];
		if ($('input[name="title_en"]').val() == "")
			$('input[name="title_en"]').parent().addClass('error');
		else
			$('input[name="title_en"]').parent().removeClass('error');
		if ($('input[name="title_de"]').val() == "")
			$('input[name="title_de"]').parent().addClass('error');
		else
			$('input[name="title_de"]').parent().removeClass('error');
        if($('input[name="slug"]').val() == ""){
            $('input[name="slug"]').parent().addClass('error');
            msg = js_localize['newsletter.add.empty_slug']
        }
		$('body').css('cursor', 'default');
		jAlert(msg, js_localize['newsletter.add.title'], "error");

		return false;
	} else {
		return true;
	}
}

$(document).ready(function() {
	$('div.user_fields fieldset .nullify_input').click(function() {
		$(this).parent().children('input').val(CONFIG.get('DATE_TIME_ZERO'));
		if ($('#publish_date').val() == CONFIG.get('DATE_TIME_ZERO')) {
			curentTime.setHours(curentTime.getHours()+ 1);
			$('#unpublish_date').datepicker('option', 'minDate', curentTime);
			$('#unpublish_date').datetimepicker('option', 'minDateTime', curentTime);
		}
		if($(this).parent().parent().hasClass('unpublish_date'))
			$(this).parent().children('input').val(CONFIG.get('DATE_TIME_ZERO'));
	});

});

// jQuery File Tree Plugin
//
// Version 1.01
//
// Cory S.N. LaViska
// A Beautiful Site (http://abeautifulsite.net/)
// 24 March 2008
//
// Visit http://abeautifulsite.net/notebook.php?article=58 for more information
//
// Usage: $('.fileTreeDemo').fileTree( options, callback )
//
// Options:  root           - root folder to display; default = /
//           script         - location of the serverside AJAX file to use; default = jqueryFileTree.php
//           folderEvent    - event to trigger expand/collapse; default = click
//           expandSpeed    - default = 500 (ms); use -1 for no animation
//           collapseSpeed  - default = 500 (ms); use -1 for no animation
//           expandEasing   - easing function to use on expand (optional)
//           collapseEasing - easing function to use on collapse (optional)
//           multiFolder    - whether or not to limit the browser to one subfolder at a time
//           loadMessage    - Message to display while initial tree loads (can be HTML)
//
// History:
// 1.03 (patched by simo for Filemanager) - add an expandedFolder option to open desired folder when init
// 1.02 (patched by Filemanager) - add a datafunc option to get data through a function instead of a script
// 1.01 - updated to work with foreign characters in directory/file names (12 April 2008)
// 1.00 - released (24 March 2008)
//
// TERMS OF USE
// 
// jQuery File Tree is licensed under a Creative Commons License and is copyrighted (C)2008 by Cory S.N. LaViska.
// For details, visit http://creativecommons.org/licenses/by/3.0/us/
//
if(jQuery) (function($){
	
	$.extend($.fn, {
		fileTree: function(o, h) {
			// Defaults
			if( !o ) var o = {};
			if( o.root == undefined ) o.root = '/';
			if( o.datafunc == undefined ) o.datafunc = null;
			if( o.script == undefined ) o.script = 'jqueryFileTree.php';
			if( o.folderEvent == undefined ) o.folderEvent = 'click';
			if( o.expandSpeed == undefined ) o.expandSpeed= 500;
			if( o.collapseSpeed == undefined ) o.collapseSpeed= 500;
			if( o.expandEasing == undefined ) o.expandEasing = null;
			if( o.collapseEasing == undefined ) o.collapseEasing = null;
			if( o.multiFolder == undefined ) o.multiFolder = true;
			if( o.loadMessage == undefined ) o.loadMessage = 'Loading...';
			if( o.folderCallback == undefined ) o.folderCallback = null;
			if( o.after == undefined ) o.after = null;
			if( o.expandedFolder == undefined ) o.expandedFolder = '';
			
			o.showTree = function(c, t) {
				function showData(data) {
					$(c).find('.start').html('');
					$(c).removeClass('wait').append(data);
					$(c).find('UL:hidden').slideDown({ duration: o.expandSpeed, easing: o.expandEasing });
					o.bindTree(c);
					if (o.expandedFolder != null) {
						$(c).find(".directory.collapsed").each(function (i,f) {
						   if ((o.expandedFolder).match($(f).children().attr('data-path'))) {
								o.showTree($(f), $(f).children().attr('data-path').match(/.*\//));
								$(f).removeClass('collapsed').addClass('expanded');
							};

						});
					}

					o.after(data);
				}
				$(c).addClass('wait');
				$(".jqueryFileTree.start").remove();
				if (o.datafunc) o.datafunc(t, showData);
				else $.post(o.script, { dir: t }, showData);
			};
			o.bindTree = function(t) {
				$(t).find('LI A').bind(o.folderEvent, function() {
					if( $(this).parent().hasClass('directory') ) {
						if( $(this).parent().hasClass('collapsed') ) {
							// Expand
							if( !o.multiFolder ) {
								$(this).parent().parent().find('UL').slideUp({ duration: o.collapseSpeed, easing: o.collapseEasing });
								$(this).parent().parent().find('LI.directory').removeClass('expanded').addClass('collapsed');
							}
							$(this).parent().find('UL').remove(); // cleanup
							o.showTree( $(this).parent(), $(this).attr('data-path').match( /.*\// ) );
							$(this).parent().removeClass('collapsed').addClass('expanded');
						} else {
							// Collapse
							$(this).parent().find('UL').slideUp({ duration: o.collapseSpeed, easing: o.collapseEasing });
							$(this).parent().removeClass('expanded').addClass('collapsed');
						}

						o.folderCallback($(this).attr('data-path'));

					} else {
						h($(this).attr('data-path'));
					}
					return false;
				});
				// Prevent A from triggering the # on non-click events
				if( o.folderEvent.toLowerCase != 'click' ) $(t).find('LI A').bind('click', function() { return false; });
			};
			
			$(this).data("fileTree", o );
			
			$(this).each( function() {
				// Loading message
				$(this).html('<ul class="jqueryFileTree start"><li class="wait">' + o.loadMessage + '<li></ul>');
				// Get the initial file list
				o.showTree( $(this), o.root );
			});
		},
		// Refresh the tree
		refreshTree: function(path) {
			var o = $(this).data("fileTree");
			// Defaults
			if( path == undefined ) {
				// Get the initial file list
				path = o.root;
			}			
			var loc = $(this).find("[data-path='" + path + "']").parent();
			loc.find('ul').remove();
			o.showTree(loc, path );
		},
		changeSettings: function(newSettings){
			var o = $(this).data("fileTree");
			if(newSettings!= undefined){
				if( newSettings.root != undefined ) o.root = newSettings.root;
				if( newSettings.datafunc != undefined ) o.datafunc = newSettings.datafunc;
				if( newSettings.script != undefined ) o.script = newSettings.script;
				if( newSettings.folderEvent != undefined ) o.folderEvent = newSettings.folderEvent;
				if( newSettings.expandSpeed != undefined ) o.expandSpeed= newSettings.expandSpeed;
				if( newSettings.collapseSpeed != undefined ) o.collapseSpeed= newSettings.collapseSpeed;
				if( newSettings.expandEasing != undefined ) o.expandEasing = newSettings.expandEasing;
				if( newSettings.collapseEasing != undefined ) o.collapseEasing = newSettings.collapseEasing;
				if( newSettings.multiFolder != undefined ) o.multiFolder = newSettings.multiFolder;
				if( newSettings.loadMessage != undefined ) o.loadMessage = newSettings.loadMessage;
				if( newSettings.folderCallback != undefined ) o.folderCallback = newSettings.folderCallback;
				if( newSettings.after != undefined ) o.after = newSettings.afte;
				if( newSettings.expandedFolder != undefined ) o.expandedFolder = newSettings.expandedFolder;
			}
			$(this).data("fileTree", o );
			var loc = $(this).find("[data-path='" + path + "']").parent();
			loc.find('ul').remove();
			o.showTree(loc, o.root );
		}
	});
})(jQuery);

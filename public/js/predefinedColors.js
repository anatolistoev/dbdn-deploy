var predefinedColors = [
                        "000000", "Black",
                        "FF0000", "Signal Red",
                        "E6E6E6", "Light Grey 100%",
                        "444444", "Light Grey +80K",
                        "707070", "Light Grey +60K", 
                        "9E9E9E", "Light Grey +40K",
                        "C8C8C8", "Light Grey +20K",
                        "00677F", "Petrol 100%",
                        "004355", "Petrol +40K",
                        "00566A", "Petrol +20K",
                        "007A93", "Petrol 80%",
                        "5097AB", "Petrol 60%",
                        "79AEBF", "Petrol 40%",
                        "A6CAD8", "Petrol 20%",
                        "71180C", "Deep Red 100%",
                        "440E07", "Deep Red 40K",
                        "5A130A", "Deep Red 20K",
                ];

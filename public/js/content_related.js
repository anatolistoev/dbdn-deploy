//var relPath = getRelativePath("content");
$(document).ready(function() {
	$('#header a,#header input[type="submit"],#header input[type="image"], #footer a, #path a').click(function(e) {
		e.preventDefault();
	});

	$('#right div.genericRS.viewable').hover(function() {
		if (!$(this).hasClass('unviewable')) {
			$(this).find('.modify_overlay').show();
			$(this).find('.modify_wrapper').show();
			$(this).removeClass('viewable');
		}
	}, function() {
		if (!$(this).hasClass('unviewable')) {
			$(this).find('.modify_overlay').hide();
			$(this).find('.modify_wrapper').hide();
			$(this).addClass('viewable');
		}
	});

	$('#right div.genericRS .modify_wrapper a.nav_box.edit').click(function() {
//		$(this).parent().parent().find('div.related_buttons').show();
		$(this).parent().parent().find('div.modify_buttons').show();
		$(this).parent().parent().find('.modify_overlay').hide();
		$(this).parent().parent().find('.modify_wrapper').hide();
		$(this).parent().parent().addClass('unviewable');

	});

	$('#right div.genericRS a.modify_links.back_to_view').click(function() {
		$(this).parent().parent().find('div.related_buttons').hide();
		$(this).parent().hide();
		$(this).parent().parent().removeClass('unviewable');
		$(this).parent().parent().find('div.active').removeClass("active");
		return false;
	});

	$('#right div.genericRS .related_records').click(function(event) {
		if (event.target.tagName == "SPAN") {
			var related_btn = $(event.target).parent().find('.related_buttons');
			if (related_btn.is(":visible")) {
				related_btn.hide();
				$(event.target).parent().removeClass("active");
			}
			else {
				related_btn.show();
				$(event.target).parent().addClass("active");
			}
		}
	});

	$('#right div.genericRS a.modify_links.add.download').click(function() {
		return fnContentRelatedEdit('downloads');
	});

	$('#right div.genericRS a.modify_links.add.other').click(function() {
		return fnContentRelatedEdit('other');
	});

	bindContentButtons();
});

function fnContentRelatedEdit(oTemplateTypeVar) {
	$('#overlay').show();
	var userHeight = $(window).height() - 60;
	$('#tree_wrapper').css({position: 'fixed', maxHeight: userHeight + 'px'});
	$('#tree_wrapper').show();

	if (typeof (oTable_rel) == 'undefined') {
		$.fn.dataTableExt.oStdClasses.sPaging = 'dataTables_paginate paging_ellipses related_';
		oTable_rel = $('#pages_rel').dataTable({
			"bProcessing": false,
			"sAjaxSource": relPath + "api/v1/pages/all?lang=" + lang + "&depth=1&parent_id=1&related=1",
			"sAjaxDataProp": 'response',
			"fnServerData": fnServerData,
			"iDisplayLength": 12,
			"bFilter": true,
			"sPaginationType": 'ellipses',
			"bLengthChange": false,
			"iShowPages": 10,
			"bSort": false,
			"aoColumns": [
				{"sType": "page-title", "mData": function(source) {
						var path = '';
						var sign = '+';
						if (source.id == 1) {
							sign = "-";
						}
						if (typeof (source.path) != "undefined") {
							path = '<span style="display: block;font-size: 11px;line-height: 12px;">' + source.path + '</span>';
						}
						path += '<a onClick="return rowExpand(this,event)" href="" class="' + (source.isChildless ? "childless" : "") + '">' + (source.isChildless ? "" : sign) + '</a> <span class="title">' + source.title + '</span>';
						return path;
					}, "sWidth": "410px", "sClass": "strong", "bSearchable": true},
				{"sType": "numeric", "mData": "id", "sWidth": "40px", "bSearchable": false, "sClass": "page_id"},
				{"sType": "numeric", "mData": function(source) {
						if (source.active == 1) {
							return js_localize['pages.label.active'];
						} else {
							return js_localize['pages.label.inactive'];
						}
					}, "sWidth": "60px", "bSearchable": false},
				{"sType": "string", "mData": "template", "sWidth": "80px", "bSearchable": false, "sClass": "template"},
			],
			"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$(nRow).attr('id', 'page_' + aData.id);
				$(nRow).attr('pos', aData.position);
				$(nRow).attr('level', aData.level);
				$(nRow).addClass('level_' + aData.level);
				$(nRow).attr('parent', aData.parent_id);

			},
			"fnInitComplete": function(oSettings, json) {

			}
		});

		var oSettings = oTable_rel.fnSettings();
		oSettings.sAlertTitle = js_localize['pages.list.title'];
	}
	// Add handler for Click on table
//	$('#pages_rel tbody').unbind("click");
//	$("#pages_rel tbody").click(function(event) {
//		var related_id, title, template;
//
//		if ($(event.target.parentNode).prop("tagName") != "TD") {
//			related_id = $(event.target.parentNode).find('.page_id').html();
//			title = $(event.target.parentNode).find('td.strong span.title').html();
//			template = $(event.target.parentNode).find('.template').html();
//		} else {
//			related_id = $(event.target.parentNode).parent().find('.page_id').html();
//			title = $(event.target.parentNode).find('span.title').html();
//			template = $(event.target.parentNode).parent().find('.template').html();
//		}
//
//		if (oTemplateTypeVar == 'downloads') {
//			if (template.toLowerCase() != 'downloads') {
//				jAlert(js_localize['content.add_related.wrong_page_error.message'], js_localize['content.add_related.wrong_page_error.title'], "error");
//				return false;
//			}
//		}
//		else {
//			if (template.toLowerCase() == 'downloads') {
//				jAlert(js_localize['content.add_related.wrong_page_error.message'], js_localize['content.add_related.wrong_page_error.title'], "error");
//				return false;
//			}
//		}
//
//		var data = {page_id: $('div#content_edit_page_id').html(), related_id: related_id, position: ($('div#related div.related_record,div#downloads div.related_record').length + 1)};
//		jQuery.ajax({
//			type: "POST",
//			url: relPath + "api/v1/pages/addrelated",
//			contentType: "application/json; charset=utf-8",
//			dataType: "json",
//			data: JSON.stringify(data),
//			success: function(data) {
//				if (template == "downloads") {
//					$("#downloads .related_records .related_record_template").first().clone().attr('class', 'related_record cf').attr('pos', data.response.position).attr('related', data.response.related_id).appendTo('#downloads .related_records').show();
//					$("#downloads .related_records .related_record[related='" + data.response.related_id + "'] span").attr('id', 'related_' + data.response.related_id).html('> ' + title);
//				} else {
//					$("#related .related_records .related_record_template").first().clone().attr('class', 'related_record cf').attr('pos', data.response.position).attr('related', data.response.related_id).appendTo('#related .related_records').show();
//					$("#related .related_records .related_record[related='" + data.response.related_id + "'] span").attr('id', 'related_' + data.response.related_id).html('> ' + title);
//				}
//				unbindContentButtons();
//				bindContentButtons();
//			},
//			error: function(XMLHttpRequest, textStatus, errorThrown) {
//				var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
//				var message = JSON_ERROR.composeMessageFromJSON(data, true);
//				jAlert(message, js_localize['content.add_related.title'], "error");
//			}
//		});
//		event.stopPropagation();
//	});

	$("#popup_close,#overlay").click(function(event) {
		$('#overlay').hide();
		$('#tree_wrapper').hide();
	});
	return false;
}

/* Get the rows which are currently selected */
function fnGetSelected(oTableLocal) {
	return oTableLocal.$('tr.row_selected');
}

function rowExpand(el, event) {
	event.stopPropagation();
	if ($(el).parent().parent().hasClass('expanded')) {
		$(el).parent().parent().removeClass('expanded');
		oTable_rel.fnDeleteDataAndDisplay($(el).parent().parent().attr('id'));
		$(el).html('+');
	} else if (!$(el).hasClass('childless')) {
		var page_id = $(el).parent().parent().find('.page_id').html();
		jQuery.ajax({
			type: "GET",
			url: relPath + "api/v1/pages/all?lang="+ lang + "&depth=1&parent_id=" + page_id,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function(data) {
				var anSelected = fnGetSelected(oTable_rel);
				for (var i = 0; i < data.response.length; i++) {
					data.response[i].level = parseInt($(el).parent().parent().attr('level')) + 1;
					oTable_rel.fnAddDataAndDisplay(data.response[i], true);
				}
				$(el).parent().parent().addClass('expanded');
				$(el).html('-');
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
				var message = JSON_ERROR.composeMessageFromJSON(data, true);
				jAlert(message, js_localize['pages.list.title'], "error");
			},
		});
	}
	return false;
}
//function bindContentButtons() {
//	$('#right div.genericRS a.modify_links.move_up').click(function() {
//		var selected = this;
//		if ($(this).parent().parent().attr('pos') == 1 || $(this).parent().parent().prev().hasClass('related_record_template')) {
//			jAlert(js_localize['content.edit_related.move_first_error'], js_localize['content.edit_related.title'], "error");
//		} else if (!$(selected).parent().parent().attr('data-isMoving')) {
//			$(selected).parent().parent().attr('data-isMoving', 'true');
//			var data = {page_id: $('div#content_edit_page_id').html(), related_id: $(this).parent().parent().attr('related'), position: $(this).parent().parent().prev('div.related_record').attr('pos')};
//			var el = this;
//			jQuery.ajax({
//				url: relPath + "api/v1/pages/editrelated",
//				type: "PUT",
//				contentType: "application/json; charset=utf-8",
//				dataType: "json",
//				data: JSON.stringify(data),
//				success: function(data) {
//					var tmp = $(el).parent().parent().attr('pos');
//					$(el).parent().parent().attr('pos', $(el).parent().parent().prev().attr('pos'));
//					$(el).parent().parent().prev().attr('pos', tmp);
//					$(el).parent().parent().prev().insertAfter($(el).parent().parent());
//					$(selected).parent().parent().removeAttr('data-isMoving');
//					//jAlert("Related page was moved!", "Edit Related", "success");
//				},
//				error: function(XMLHttpRequest, textStatus, errorThrown) {
//					var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
//					var message = JSON_ERROR.composeMessageFromJSON(data, true);
//					jAlert(message, js_localize['content.edit_related.title'], "error");
//					$(selected).parent().parent().removeAttr('data-isMoving');
//				},
//			})
//		}
//		return false;
//	});
//
//	$('#right div.genericRS a.modify_links.move_down').click(function() {
//		if ($(this).parent().parent().is(':last-child')) {
//			jAlert(js_localize['content.edit_related.move_last_error'], js_localize['content.edit_related.title'], "error");
//		} else {
//			var data = {page_id: $('div#content_edit_page_id').html(), related_id: $(this).parent().parent().attr('related'), position: $(this).parent().parent().next('div.related_record').attr('pos')};
//			var el = this;
//			jQuery.ajax({
//				url: relPath + "api/v1/pages/editrelated",
//				type: "PUT",
//				contentType: "application/json; charset=utf-8",
//				dataType: "json",
//				data: JSON.stringify(data),
//				success: function(data) {
//					var tmp = $(el).parent().parent().attr('pos');
//					$(el).parent().parent().attr('pos', $(el).parent().parent().next().attr('pos'));
//					$(el).parent().parent().next().attr('pos', tmp);
//					$(el).parent().parent().insertAfter($(el).parent().parent().next());
//					//jAlert("Related page was moved!", "Edit Related", "success");
//				},
//				error: function(XMLHttpRequest, textStatus, errorThrown) {
//					jAlert(js_localize['content.edit_related.error'], js_localize['content.edit_related.title'], "error");
//				},
//			})
//		}
//		return false;
//	});
//	$('#right div.genericRS a.modify_links.delete').click(function() {
//		var el = this;
//		jConfirm(js_localize['content.edit_related.remove_confirm'], js_localize['content.edit_related.remove_title'], "success", function(r) {
//			if (r) {
//				var data = {page_id: $('div#content_edit_page_id').html(), related_id: $(el).parent().parent().attr('related')};
//				jQuery.ajax({
//					type: "DELETE",
//					url: relPath + "api/v1/pages/removerelated",
//					contentType: "application/json; charset=utf-8",
//					dataType: "json",
//					data: JSON.stringify(data),
//					success: function(data) {
//						$('div#related div.related_record,div#downloads div.related_record').each(function() {
//							if ($(this).attr('pos') > $(el).parent().parent().attr('pos')) {
//								$(this).attr('pos', parseInt($(this).attr('pos')) - 1);
//							}
//						});
//						$(el).parent().parent().remove();
//						jAlert(js_localize['content.edit_related.remove_success'], js_localize['content.edit_related.remove_title'], "success");
//					},
//					error: function(XMLHttpRequest, textStatus, errorThrown) {
//						var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
//						var message = JSON_ERROR.composeMessageFromJSON(data, true);
//						jAlert(message, js_localize['content.edit_related.remove_title'], "error");
//					},
//				});
//
//			}
//		});
//		return false;
//	});
//}

function unbindContentButtons() {
	$('#right div.genericRS a.modify_links.move_up').unbind("click");
	$('#right div.genericRS a.modify_links.move_down').unbind("click");
	$('#right div.genericRS a.modify_links.delete').unbind("click");
}

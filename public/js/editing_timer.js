var editing_timer = {
    // Configs
    time_left_abs_error_seconds: 60,
    defaultTickTime : 10000,
    buffer_seconds: 30,
	mode : "", // image or download
	ws_type : "", // file or page
    item_id: "",  // page or file ID
    endYesCallback: "",
    endNoCallback: "",

    // Private members
    interval: '',
    time: 0,
    defaultTime: 0,
    tickTime : 10000,
    isAlerted:false,
    expire_datetime:'',

    // Public methods
    start: function (initial_time_left, endYesCallback, endNoCallback, itemId, type, mode, defaultTime) {

        if (typeof (endYesCallback) === "function") {
            this.endYesCallback = endYesCallback;
        }
        if (typeof (endNoCallback) === "function") {
            this.endNoCallback = endNoCallback;
        }
        if(itemId){
            this.item_id = itemId;
        }
        if(defaultTime){
            this.defaultTime = defaultTime;
        }
        if(this.interval){
            this.stopTimer(false);
        }
		if(type == 'file'){
			this.ws_type = 'files';
			data = {fileId: this.item_id};
			this.mode = '_'+mode;
		}else if(type == 'newsletter'){
			this.ws_type = 'newsletter';
			data = {newsletterId: this.item_id};
			this.mode = "";
		}else{
			this.ws_type = 'pages';
			data = {pageId: this.item_id};
			this.mode = "";
		}
        var that = this;
        this.isAlerted = false;
        this.time = initial_time_left;
        this.expire_datetime = new Date();
        this.expire_datetime.setSeconds(this.expire_datetime.getSeconds() + initial_time_left);
        if (!this.checkTimer(initial_time_left)) {
            that.tickTime = Math.min(that.defaultTickTime, (initial_time_left - EDIT_COMPLEATED_ALERT_SECONDS) * 1000 );
            this.interval = setInterval(function () {
                that.time-= that.tickTime/1000;
                var now = new Date();
                var time_left = Math.round((that.expire_datetime - now)/ 1000);
                if( Math.abs(time_left-that.time) < that.time_left_abs_error_seconds){
                    console.log('This time: time_left: '+time_left+ " that.time: "+that.time);
                    that.checkTimer(time_left);
                }else{
                    console.log('Other: time_left: '+time_left+ " that.time: "+that.time);
                    that.checkTimer(that.time);
                }
            }, that.tickTime);
        }
    },
    restartTimer: function (time, type, mode) {
        if (this.interval != "") {
            clearInterval(this.interval);
            this.interval = "";
        }
        this.start(time, "", "",0,type, mode);
    },
    stopTimer: function (hasTime) {
        var that = this;
        if (this.interval != "") {
            clearInterval(this.interval);
            this.interval = "";
        }
        if (hasTime) {
			$.alerts.noButton = js_localize['pages.edit.logout.lock'];
			$.alerts.yesButton = js_localize['pages.edit.keep_login.lock'];
            jConfirmTimeOut(js_localize[this.ws_type+this.mode+'.edit.extend_finish_time'], js_localize[this.ws_type+this.mode+'.edit.title'], "success", function (r) {
                $.alerts.noButton = js_localize['pages.edit.logout.default'];
				$.alerts.yesButton = js_localize['pages.edit.keep_login.default'];
				if (r) {
                    that.endYesCallback();
                } else {
                    that.endNoCallback();
                }
            }, EDIT_COMPLEATED_ALERT_SECONDS);
        }
    },
    // Return true if time for edit is passed
    checkTimer:function(time){
		var that = this;
        if (time <= EDIT_TIME_EXTENSION_ALERT_SECONDS && time >=(EDIT_TIME_EXTENSION_ALERT_SECONDS - that.buffer_seconds) && !that.isAlerted) {
            $.alerts.cancelButton = js_localize['pages.edit.extend.no'];
            $.alerts.okButton = js_localize['pages.edit.extend.yes'];
            jConfirm(
                js_localize[that.ws_type + that.mode + '.edit.extend']
                    .replace('{time_left}', that.formatTimeInMinutes(EDIT_TIME_EXTENSION_ALERT_SECONDS))
                    .replace('{time_total}', that.formatTimeInMinutes(EDIT_MAX_TIME_SECONDS)),
                js_localize[that.ws_type + that.mode + '.edit.title'],
                "success",
                function (r) {
                    $.alerts.cancelButton = js_localize['pages.jconfirm.no'];
                    $.alerts.okButton = js_localize['pages.jconfirm.ok'];
                    if (r) {
                        jQuery.ajax({
                            type: "POST",
                            url: relPath + "api/v1/" + that.ws_type + "/appendtime",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: JSON.stringify(data),
                            success: function (data) {
                                jAlert(
                                    js_localize[that.ws_type + that.mode + '.edit.extend_success']
                                        .replace('{time_total}', that.formatTimeInMinutes(EDIT_MAX_TIME_SECONDS)),
                                    js_localize[that.ws_type + that.mode + '.edit.title'],
                                    "success"
                                );
                                var time_left = that.defaultTime;
                                if (data.response.time_left) {
                                    time_left = data.response.time_left;
                                }
                                that.restartTimer(time_left, that.ws_type, that.mode.substr(1));
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                                var message = JSON_ERROR.composeMessageFromJSON(data, true);
                                jAlert(message, js_localize[that.ws_type + that.mode + '.edit.title'], "error");
                            }
                        });
                    } else {
                        jAlert(js_localize[that.ws_type + that.mode + '.edit.extend_decline'], js_localize[that.ws_type + that.mode + '.edit.title'], "error");
                    }
                }
            );
            that.isAlerted = true;

        } else if (time <= 0 || !time) {
            that.stopTimer(false);
            that.endNoCallback();
            return true;
        } else if (time <= EDIT_COMPLEATED_ALERT_SECONDS) {
            that.stopTimer(true);
            return true;
        }

        return false;
    },

    // Format time in Minutes
    formatTimeInMinutes: function(timeInSeconds) {
        return Math.ceil(timeInSeconds / 60);
    }
}

/*******************************************************************************
 * Function for change status of page during edit
 *******************************************************************************/

function unlockItem(item_id, type){
   var ws_type,data;
   if(type == 'page'){
	   ws_type = 'pages';
	   data = JSON.stringify({pageId:item_id});
   }else if(type == 'newsletter'){
	   ws_type = 'newsletter';
	   data = JSON.stringify({newsletterId:item_id});
   }else{
	   ws_type = 'files';
	   data = JSON.stringify({fileId:item_id});
   }

   return jQuery.ajax({
			type: "POST",
			url: relPath + "api/v1/"+ws_type+"/unlock",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: data,
			success: function(data) {

			},
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
				var message = JSON_ERROR.composeMessageFromJSON(data, true);
				jAlert(message, js_localize[ws_type+'.edit.title'], "error");
			}
		});
}

function changePageResponsible(pId,userId){
	$.ajax({
		url : relPath + "api/v1/workflow/changeresponsible",
		type: 'PUT',
		data : JSON.stringify({page_id: pId, user_responsible : userId}),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success : function(data){
			$('#workflow_wrapper select[name="user_responsible"]').val(data.response.user_responsible);
			$('#workflow_wrapper select[name="user_responsible"]').next('.styledSelect').text($('#workflow_wrapper select[name="user_responsible"]').next().next().find('li[rel="'+data.response.user_responsible+'"]').text());
			jAlert(js_localize['page.edit.responsible.success'], js_localize['pages.edit.title'], "success");
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
			var message = JSON_ERROR.composeMessageFromJSON(data, true);
			jAlert(message, js_localize['page.edit.title'], "error", function(){window.location.href = relPath + 'cms/pages';});
		}
	});
}

function changeNewsletterResponsible(nId,userId){
	$.ajax({
		url : relPath + "api/v1/newsletter/changeresponsible",
		type: 'PUT',
		data : JSON.stringify({newsletter_id: nId, user_responsible : userId}),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success : function(data){
			$('#workflow_wrapper select[name="user_responsible"]').val(data.response.user_responsible);
			$('#workflow_wrapper select[name="user_responsible"]').next('.styledSelect').text($('#workflow_wrapper select[name="user_responsible"]').next().next().find('li[rel="'+data.response.user_responsible+'"]').text());
			jAlert(js_localize['page.edit.responsible.success'], js_localize['newsletter.edit.title'], "success");
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
			var message = JSON_ERROR.composeMessageFromJSON(data, true);
			jAlert(message, js_localize['newsletter.edit.title'], "error", function(){window.location.href = relPath + 'cms/newsletter';});
		}
	});
}
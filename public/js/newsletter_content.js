//var relPath = getRelativePath();
var content_text = "";

$(document).ready(function() {
    bindSectionClicks('header_d');
    bindSectionClicks('header_m');
    bindSectionClicks('content');
    bindSectionClicks('footer_i');
	bindSectionClicks('footer_e');
    bindSectionClicks('footer_e_bl');

	bindPagePropertiesClicks();

	$('#page_save').addClass('save_inactive');
    $('#page_save').click(function() {
            savePage(this);
			return false;
    });

	$('.nav_box.copy').click(function(){
		copyFooters();
		return false;
	})

    $('.top_navigation a,#change_lang, .admin_info a').click(function(e) {
        if ($(this).attr('id') != "page_save") {
            el = this;
            if ($('#content_content_text').hasClass('changed') || $('#content_header_d_text').hasClass('changed') || $('#content_header_m_text').hasClass('changed') || $('#content_footer_i_text').hasClass('changed') || $('#content_footer_e_text').hasClass('changed') || $('#content_footer_e_bl_text').hasClass('changed')) {
                e.preventDefault();
                var cancel_def =  $.alerts.cancelButton;
                $.alerts.cancelButton =  '&nbsp;No&nbsp;';
                jConfirm(js_localize['content.save.confirm'], js_localize['content.save.title'], "error", function(r) {
                    if (r) {
                        if (saveImgs()) {
//
                            var headerDPromise, headerMPromise, contentPromise, footerIPromise, footerEPromise, footerEBlPromise;
                            headerDPromise = headerMPromise = contentPromise =  footerIPromise = footerEPromise = footerEBlPromise = $.Deferred();
                            if($('#content_header_d_text').hasClass('changed')){
                               headerDPromise = saveSection('header_d');
                            }else{
                                 headerDPromise.resolve();
                            }
                            if($('#content_header_m_text').hasClass('changed')){
                                headerMPromise = saveSection('header_m');
                            }else{
                                headerMPromise.resolve();
                            }
                            if($('#content_content_text').hasClass('changed')){
                                contentPromise = saveSection('content');
                            }else{
                                contentPromise.resolve();
                            }
							if($('#content_footer_i_text').hasClass('changed')){
                                footerIPromise = saveSection('footer_i');
                            }else{
                                footerIPromise.resolve();
                            }
                            if($('#content_footer_e_text').hasClass('changed')){
                                footerEPromise = saveSection('footer_e');
                            }else{
                                footerEPromise.resolve();
                            }
                            if($('#content_footer_e_bl_text').hasClass('changed')){
                                footerEBlPromise = saveSection('footer_e_bl', 3);
                            }else{
                                footerEBlPromise.resolve();
                            }

                            var promise = $.when(headerDPromise, headerMPromise, contentPromise, footerIPromise, footerEPromise,footerEBlPromise).always(function() {
                                if($(el).attr('id') != "change_lang"){
                                    var statusPromise = unlockItem($('div#content_edit_newsletter_id').html(),'newsletter');
                                    statusPromise.always(function() {
                                         window.location.href = $(el).attr('href');
                                    });
                                }else{
                                    window.location.href = $(el).attr('href');
                                }

                             });
                        }else{
                            jAlert(js_localize['content.images.user_error'], js_localize['content.images.title'], "error");
                        }
                    } else {
                        if($(el).attr('id') != "change_lang" && $(el).attr('id') != "page_workflow"){
							var statusPromise = unlockItem($('div#content_edit_newsletter_id').html(),'newsletter');
							statusPromise.always(function() {
								window.location.href = $(el).attr('href');
							});
						}else if($(el).attr('id') == "change_lang"){
							window.location.href = $(el).attr('href');
						}
                    }
                });
				$.alerts.cancelButton =  cancel_def;
            }else{
                if($(el).attr('id') != "change_lang" && $(el).attr('id') != "page_workflow"){
                    var statusPromise = unlockItem($('div#content_edit_newsletter_id').html(),'newsletter');
                    statusPromise.always(function() {
                        window.location.href = $(el).attr('href');
                    });
                }else if($(el).attr('id') == "change_lang"){
					window.location.href = $(el).attr('href');
				}
            }
        }
        return false;
    });
});

function savePage(element) {
    var result = $.Deferred().reject();
    if ($('#page_save').hasClass('not_saved')) {
        if (saveImgs()) {
            var headerDPromise, headerMPromise, contentPromise, footerIPromise, footerEPromise, footerEBlPromise;
            headerDPromise = headerMPromise = contentPromise = footerIPromise = footerEPromise = footerEBlPromise = $.Deferred();
			if ($('#content_header_d_text').hasClass('changed')) {
				headerDPromise = saveSection('header_d');
			} else {
				headerDPromise.resolve();
			}
			if ($('#content_header_m_text').hasClass('changed')) {
				headerMPromise = saveSection('header_m');
			} else {
				headerMPromise.resolve();
			}
			if ($('#content_content_text').hasClass('changed')) {
				contentPromise = saveSection('content');
			} else {
				contentPromise.resolve();
			}
			if ($('#content_footer_i_text').hasClass('changed')) {
				footerIPromise = saveSection('footer_i');
			} else {
				footerIPromise.resolve();
			}
			if ($('#content_footer_e_text').hasClass('changed')) {
				footerEPromise = saveSection('footer_e');
			} else {
				footerEPromise.resolve();
			}
			if ($('#content_footer_e_bl_text').hasClass('changed')) {
				footerEBlPromise = saveSection('footer_e_bl', 3);
			} else {
				footerEBlPromise.resolve();
			}
            $("#page_save").removeClass('not_saved');
            result = $.when(headerDPromise, headerMPromise, contentPromise, footerIPromise, footerEPromise,footerEBlPromise);
        } else {
            jAlert(js_localize['content.images.user_error'], js_localize['content.images.title'], "error");
        }
    } else {
        // Disable Save Button there is no chages
        $(element).addClass('save_inactive');
    }
    return result;
}

function bindSectionClicks(section) {
    //var image_attr = '';
    //$('#content_' + section + '_text').find('img.single_image').each(function() {
    //    image_attr += $(this).data('gallery') + ',';
    //});
    //$('#content_' + section + '_text').attr('gallery', image_attr.slice(0, -1));

    $('div.content_' + section).hover(function() {
        if (!$(this).hasClass('unviewable')) {
            $(this).find('.modify_overlay').show();
            $(this).find('.modify_wrapper').show();
            $(this).removeClass('viewable');
        }
    }, function() {
        if (!$(this).hasClass('unviewable')) {
            $(this).find('.modify_overlay').hide();
            $(this).find('.modify_wrapper').hide();
            $(this).addClass('viewable');
        }
    });

    $('div.content_' + section + ' .modify_wrapper a.nav_box.edit').click(function() {
        if (!$(this).hasClass('clicked')) {
            content_text = $('#content_' + section + '_text').html();
            tinyMCE.get(section + '_body').show();
            tinyMCE.get(section + '_body').setContent(content_text);
			if(section !== 'header_d' && section !== 'header_m')
				tinyMCE.get(section + '_body').getBody().setAttribute('contenteditable', false);
            $(this).parent().parent().addClass('unviewable');
            $('div.content_' + section + ' .modify_wrapper a.nav_box.back_to_view').show();
            $('.textarea_overlay').show();
            $(this).addClass('clicked');
        }
    });

    $('div.content_' + section + ' .modify_wrapper a.nav_box.back_to_view').click(function() {
		//alert( tinyMCE.get(section + '_body').getContent() );
        $(this).hide();
        $('.textarea_overlay').hide();
        tinyMCE.get(section + '_body').hide();
		// Workaround to hide TinyMCE menus
		$('.mce-menu').hide();
		var content_text_new = tinyMCE.get(section + '_body').getContent();
		content_text_new = $('#content_' + section + '_text').html(content_text_new).html();
        $(this).parent().parent().removeClass('unviewable');
        $('div.content_' + section + ' .modify_wrapper a.nav_box.edit').removeClass('clicked');
        $(this).parent().parent().find('.modify_overlay').hide();
        $(this).parent().parent().find('.modify_wrapper').hide();
        if(content_text.trim() !=  content_text_new.trim()){
            $(this).parent().parent().find('#content_' + section + '_text').addClass('changed');
//            $(this).parent().parent().find('#content_' + section + '_text').attr('gallery', '');
//            var image_attr = '';
//            $('#content_' + section + '_text').find('img.simple,img.zoom,img.inline_zoom,img.gallery').each(function() {
//                image_attr += $(this).data('gallery') + ',';
//            });
//            $('#content_' + section + '_text').attr('gallery', image_attr.slice(0, -1));

            $(this).parent().parent().addClass('not_saved');
            $("#page_save").addClass('not_saved');
        }

//		$('#content_content_text').find('table').each( function() {
//			max_width = 740; //$(this).parent().parent().width();
//			if($(this).width() > max_width)
//				$(this).width(max_width);
//		});

        content_text = "";

		$('body').focus();

        return false;
    });
}

function bindPagePropertiesClicks() {
	$('.left_navigation .nav_box.properties').click(function() {
		clearPropertiesForm();
		$('body').css('cursor', 'progress');
		$('a').css('cursor', 'progress');
		populateFormNewsletter($('div#content_edit_newsletter_id').html(), function (has_errors) {
			$('#wrapper_form_newsletter').openDialog($('div#properties_wrapper .textarea_overlay'));
			$('#properties_wrapper').show();
			$('#properties_wrapper .user_fields').show();
			$('body').css('cursor', 'default');
			$('a').css('cursor', 'pointer');
		});

		$(this).addClass('active');
	});

	$('#properties_wrapper .user_fields .nav_box.cancel').click(function() {
		$('#wrapper_form_newsletter').closeDialog();
		$('#properties_wrapper').hide();
		$('#user_form .input_group').removeClass('error');
		$('#user_form .role_group').removeClass('error');
		$('.left_navigation .nav_box.properties').removeClass('active');
		$('#properties_wrapper .user_fields').hide();
		return false;
	});

	$('#properties_wrapper .user_fields .nav_box.save').click(function() {
		$('body').css('cursor', 'progress');
		saveNewsletterProperties(closePropertiesForm, "PUT");
		return false;
	});

	$('#page_workflow').click(function(){
		$('#wrapper_form_workflow').openDialog($('div#workflow_wrapper .textarea_overlay'));
		$('#workflow_wrapper').show();
		$('#workflow_wrapper .workflow_fields').show();
		$('body').css('cursor', 'default');
		$('a').css('cursor', 'pointer');
		$(this).addClass('active');
		return false;
	});

	$('#workflow_wrapper .user_fields .nav_box.cancel').click(function() {
		$('#wrapper_form_workflow').closeDialog();
		$('#workflow_wrapper').hide();
		$('#user_form .input_group').removeClass('error');
		$('#user_form .role_group').removeClass('error');
		$('.top_navigation #page_workflow').removeClass('active');
		$('#workflow_wrapper .user_fields').hide();
		return false;
	});

	$('#workflow_wrapper .user_fields .nav_box.save').click(function() {
		$('body').css('cursor', 'progress');
		saveNewsletterWorkflow();
		return false;
	});
}

function saveNewsletterWorkflow() {

		var url = relPath + "api/v1/newsletter/update";
		var promise = jQuery.ajax({
			type: "PUT",
			url: url,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: parseFormWorkflow(),
			success: function(data) {

				$('#wrapper_form_workflow').closeDialog();
				$('#workflow_wrapper').hide();
				$('#workflow_form .input_group').removeClass('error');
				$('#workflow_form .role_group').removeClass('error');
				$('.top_navigation #page_workflow').removeClass('active');
				$('#workflow_wrapper .user_fields').hide();
				$('#workflow_wrapper textarea[name="note"]').val('');
				$('body').css('cursor', 'default');
			},
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
				var message = JSON_ERROR.composeMessageFromJSON(data, true);
				if (data.response !== undefined) {
					$.each(data.response, function(key, value) {
						$('input[name="' + key + '"]').parent().addClass('error');
					});
				}
				$('body').css('cursor', 'default');
				jAlert(message, js_localize['newsletter.edit.title'], "error");
			}
		});
}

function parseFormWorkflow() {
	var serialized = $('#wrapper_form_workflow #workflow_form').serializeArray();
	var s = '';
	var data = {};
	for (s in serialized) {
		data[serialized[s]['name']] = serialized[s]['value'];
	}

	return JSON.stringify(data);
}

function saveSection(section, current_lang=lang) {
    var promise = $.Deferred();
    var sectionId = parseInt($('.content_' + section).attr('content_id'));
	var url = '';
	var httpMethod = 'POST';
	var tinyMCE_Content = $('#content_' + section + '_text').html();
    if(tinyMCE_Content == undefined){
        promise.resolve();
    }else if (sectionId > 0) {
        var content = {id: sectionId, content: tinyMCE_Content};
		url = relPath + "api/v1/nl_contents/update";
		httpMethod = 'PUT';
    } else if (tinyMCE_Content.trim() != "") {
        var content = {newsletter_id: $('div#content_edit_newsletter_id').html(), lang: current_lang,position: section, content: tinyMCE_Content};
		url = relPath + "api/v1/nl_contents/create";
    }else if (tinyMCE_Content.trim() == "" && $('.content_' + section).hasClass('not_saved')) {
        $('.content_' + section).removeClass('not_saved');
		$('#content_' + section + '_text').removeClass('changed');
		if (typeof (callback) === 'function') {
			callback();
		}
    }

	if (url.length > 0) {
		promise = jQuery.ajax({
			type: httpMethod,
			url: url,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(content),
			success: function(data) {
				$('.content_' + section).removeClass('not_saved');
				$('#content_' + section + '_text').removeClass('changed');
				if (typeof (callback) === 'function') {
					callback();
				}
				if(url == relPath + "api/v1/nl_contents/create"){
					$('.content_' + section).attr('content_id',data.response[0].id);
				}
			},
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
				var message = JSON_ERROR.composeMessageFromJSON(data, true);
				jAlert(message, "Save " + section, "error");
			}
		});
	}
     return promise;
}

function saveImgs() {
//    var file_data = "";
//    if($('#content_main_text').attr('gallery') != "" && typeof($('#content_main_text').attr('gallery')) != "undefined") file_data += $('#content_main_text').attr('gallery')+',';
//    if($('#content_right_text').attr('gallery') != "" && typeof($('#content_right_text').attr('gallery')) != "undefined") file_data +=  $('#content_right_text').attr('gallery')+',';
//    if($('#content_banner_text').attr('gallery') != "" && typeof($('#content_banner_text').attr('gallery')) != "undefined") file_data += $('#content_banner_text').attr('gallery')+',';
//	if($('#content_author_text').attr('gallery') != "" && typeof($('#content_author_text').attr('gallery')) != "undefined") file_data += $('#content_author_text').attr('gallery')+',';
//    file_data = file_data.slice(0,-1);
//    var file_data_array = file_data.split(',');
//    if (checkDuplicates(file_data_array) > 0) {
//        return false;
//    } else {
//        var file_info = {};
//        file_info = {files: file_data_array};
//        jQuery.ajax({
//            type: "PUT",
//            url: relPath + "api/v1/gallery/update/" + $('div#content_edit_page_id').html(),
//            contentType: "application/json; charset=utf-8",
//            dataType: "json",
//            data: JSON.stringify(file_info),
//            success: function() {
//            },
//			// script call was *not* successful
//            error: function(XMLHttpRequest, textStatus, errorThrown) {
//				var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
//				var message = JSON_ERROR.composeMessageFromJSON(data, true);
//				if(textStatus == 'Unauthorized'){
//					jAlert(message, js_localize['command.error'], "error");
//				}
//				jAlert(message, js_localize['content.images.title'], "error");
//			}
//        });
//        return true;
//    }
	  return true;
}

function checkDuplicates(arr) {
    arr.sort();
    var last = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (arr[i] == last)
            return i;
        last = arr[i];
    }
}

function openPreview(ev, pageId) {
    ev.stopPropagation();
    ev.preventDefault();
    ev.stopImmediatePropagation();
	var url = relPath + 'preview/' + pageId;
	//.replace(/"/g, "&quot;");
	var main_content = $('#content_main_text').html().replace(/"/g, "'");
	var right_content = $('#content_right_text').html().replace(/"/g, "'");
    var banner_content = "";
    if($('#content_banner_text').length > 0)
	banner_content = $('#content_banner_text').html().replace(/"/g, "'");
	var farRight_content = $('#content_farRight_text').html().replace(/"/g, "'");
	var author_content = $('#content_author_text').html().replace(/"/g, "'");
	var form = $('<form action="' + url + '" method="post" target="_blank">' +
			'<input type="text" name="main" value="' + main_content + '" />' +
			'<input type="text" name="right" value="' + right_content + '" />' +
			'<input type="text" name="banner" value="' + banner_content + '" />' +
			'<input type="text" name="farRight" value="' + farRight_content + '" />' +
			'<input type="text" name="author" value="' + author_content + '" />' +
			'</form>');
	// $('input[name="main"]', form).attr('value', $('#content_main_text').html());
	//$('input[name="right"]', form).attr('value', $('#content_right_text').html());
	$('body').append(form);

	$(form).submit();
	$(form).remove();
}

function copyFooters(){
	jConfirm(js_localize['newsletter.copy_footer.confirm'], js_localize['newsletter.copy_footer.title'], "success", function(r) {
		if (r) {
			jQuery.ajax({
				type: 'GET',
				url: relPath + "api/v1/nl_contents/copy-footer/"+lang+ "/" + $('div#content_edit_newsletter_id').html(),
				contentType: "application/json; charset=utf-8",
				success: function(data) {
					if(data.response.footer_e.length > 0){
						$('div.content_footer_e .modify_wrapper a.nav_box.back_to_view').hide();
						if($('div.content_footer_e').hasClass('unviewable')){
							$('.textarea_overlay').hide();
							tinyMCE.get('footer_e_body').hide();
							$('.mce-menu').hide();
							$('div.content_footer_e').removeClass('unviewable');
							$('div.content_footer_e .modify_wrapper a.nav_box.edit').removeClass('clicked');
							$('div.content_footer_e').find('.modify_overlay').hide();
							$('div.content_footer_e').find('.modify_wrapper').hide();
						}
						$('#content_footer_e_text').html(data.response.footer_e);
						$('.content_footer_e').addClass('not_saved');
						$('#content_footer_e_text').addClass('changed');
						$("#page_save").addClass('not_saved');
					}
//                    if(data.response.footer_e_bl.length > 0){
//						$('div.content_footer_e_bl .modify_wrapper a.nav_box.back_to_view').hide();
//						if($('div.content_footer_e_bl').hasClass('unviewable')){
//							$('.textarea_overlay').hide();
//							tinyMCE.get('footer_e_bl_body').hide();
//							$('.mce-menu').hide();
//							$('div.content_footer_e_bl').removeClass('unviewable');
//							$('div.content_footer_e_bl .modify_wrapper a.nav_box.edit').removeClass('clicked');
//							$('div.content_footer_e_bl').find('.modify_overlay').hide();
//							$('div.content_footer_e_bl').find('.modify_wrapper').hide();
//						}
//						$('#content_footer_e_bl_text').html(data.response.footer_e);
//						$('.content_footer_e_bl').addClass('not_saved');
//						$('#content_footer_e_bl_text').addClass('changed');
//						$("#page_save").addClass('not_saved');
//					}

					if(data.response.footer_i.length > 0){
						$('div.content_footer_i .modify_wrapper a.nav_box.back_to_view').hide();
						if($('div.content_footer_i').hasClass('unviewable')){
							$('.textarea_overlay').hide();
							tinyMCE.get('footer_i_body').hide();
							$('.mce-menu').hide();
							$('div.content_footer_i').removeClass('unviewable');
							$('div.content_footer_i .modify_wrapper a.nav_box.edit').removeClass('clicked');
							$('div.content_footer_i').find('.modify_overlay').hide();
							$('div.content_footer_i').find('.modify_wrapper').hide();
						}
						$('#content_footer_i_text').html(data.response.footer_i);
						$('.content_footer_i').addClass('not_saved');
						$('#content_footer_i_text').addClass('changed');
						$("#page_save").addClass('not_saved');
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
					var message = JSON_ERROR.composeMessageFromJSON(data, true);
					jAlert(message, js_localize['newsletter.copy_footer.title'], "error");
				}
			})
		}
	});
}

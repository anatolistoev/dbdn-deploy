var oTable;
var callback_selection = null;
var image_type = "";
var select_multiple = false;
var initial_selection = "";
var selectionArray = [];
var isPageRestricted = null;

var fileRoot = "/";
var editorRoot = "";
var isDeleted = false;
var skipEmptyCheck = false;
var fileuploadData = null;
var fileuploadDataChunk = null;
var uploadProgress = {startedUploads: 0, totalSize: 0, uploadedSize: 0};
var folderInfoMap = [];

$(document).ready(function () {
    initForm();

    // Creates file tree.
    createFileTree();

    initTable();

    bindFolderClicks();

    bindFormUploadClicks();

    bindRowEditClicks();

    $("#pages tbody").click(function (event) {
        if (event.target.localName != "label") {
            if ($(event.target).hasClass("dataTables_empty")) {
                $('.nav_box.white.edit').hide();
                $('.nav_box.white.remove').hide();
                $('.nav_box.white.restore').hide();
            } else {
                if (isDeleted) {
                    $('.nav_box.white.edit').hide();
                    $('.nav_box.white.restore').show();
                    if (!hasAccessForDelete) {
                        $('.nav_box.white.remove').hide();
                    } else {
                        $('.nav_box.white.remove').show();
                    }
                } else {
                    $('.nav_box.white.edit').show();
                    $('.nav_box.white.remove').show();
                    $('.nav_box.white.restore').hide();
                }
            }
            if (top.tinymce && top.tinymce.activeEditor) {
                var args = top.tinymce.activeEditor.windowManager.getParams();
                if (args && args.dialog_mode) {
                    $('.nav_box.white.edit').hide();
                }
            }
            closeRowEdit();

            if (!select_multiple && (event.target.type == "checkbox" || event.target.childNodes[0].type == "checkbox")) {
                $("#pages tbody").find(":checked").each(function () {
                    $(this).prop("checked", false);
                });
            }

            if (event.target.type != "checkbox" && event.target.childNodes[0].type != "checkbox") {
                if ($(event.target.parentNode).prop("tagName") != "TD") {
                    var parentElement = $(event.target.parentNode);
                    if (parentElement.prop("tagName") == "SPAN") {
                        parentElement = $(event.target.parentNode).parent().parent();
                    }
                    parentElement.addClass('row_selected');
                    $('.user_edit_wrapper').css('top', parentElement.position().top + parentElement.height() + $(".dataTables_scrollBody").scrollTop() + 1);
                } else {
                    $(event.target.parentNode).parent().addClass('row_selected');
                    $('.user_edit_wrapper').css('top', $(event.target.parentNode).position().top + $(event.target.parentNode.parentNode).height() + $(".dataTables_scrollBody").scrollTop() + 1);
                }
//                if ($('.dataTables_scrollBody').hasScrollBar()) {
//                    $('.user_edit_wrapper').css('width', '100%');
//                } else {
//                    $('.user_edit_wrapper').css('width', '100%');
//                }
                $('.user_edit_wrapper').show();

            } else if (event.target.type == "checkbox" && !select_multiple) {
                $(event.target).prop("checked", true);

            } else if (event.target.childNodes[0] && event.target.childNodes[0].type == "checkbox") {
                if ($(event.target.childNodes[0]).is(":checked")) {
                    $(event.target.childNodes[0]).prop("checked", false);
                } else {
                    $(event.target.childNodes[0]).prop("checked", true);
                }
            }
            if (select_multiple && (image_type == 'gallery' || image_type == 'explainer') && $(event.target).closest('tr').find("input[type='checkbox']").prop("checked")) {
                $(".up_down_wrapper").show();
            } else if ($(".up_down_wrapper").is(":visible")) {
                $(".up_down_wrapper").hide();
            }
        }
    });

    $('body').click(function (event) {
        if ($(event.target).parents('tbody').length == 0 && $(event.target).parents('.user_edit_wrapper').length == 0 && $('#popup_overlay').length == 0 && !$(event.target).hasClass('textarea_overlay') && $(event.target).parents('#wrapper_form_upload').length == 0) {
            closeRowEdit();
        }
        if (event.target.parentNode) {
            if (!(event.target.parentNode.className == "admin_data" || event.target.parentNode.id == "admin_info" || event.target.parentNode.id == "admin_action")) {
                if ($('#admin_action').is(":visible"))
                {
                    $("#admin_info").removeClass('active');
                    $('#admin_action').hide();
                }
            }
        }
    });

    $('.left_navigation .recycle, .top_navigation .nav_box, .logout').click(function (e) {
        var el = this;
        if (uploadProgress.startedUploads > 0) {
            jConfirm(js_localize['file.upload.tab_change'], js_localize['file.upload.title'], "success", function (r) {
                if (r) {
                    if ($(el).hasClass('recycle')) {
                        $(".cancel_submit").click();
                        clearUploadForm();
                        showRecycleBin(el);
                    } else {
                        window.onbeforeunload = null;
                        window.location.href = $(el).attr('href');
                    }
                }
            });
        } else {
            if ($(el).hasClass('recycle')) {
                $(".cancel_submit").click();
                clearUploadForm();
                showRecycleBin(el);
            } else {
                window.onbeforeunload = null;
                window.location.href = $(el).attr('href');
            }
        }
        return false;
    });

    $('.nav_box.clear_cache').click(function (e) {
        var mode = $('#filemanger_mode').data('value');
        var msg = mode == 'images' ? js_localize['file.cache.clear.images'] : js_localize['file.cache.clear.files'];
        $.alerts.cancelButton = js_localize['users.no'];
        $.alerts.okButton = js_localize['users.yes'];
        jConfirm(msg, js_localize['file.cache.title'], 'success', function (r) {
            if (r) {
                jQuery.ajax({
                    type: "GET",
                    url: relPath + "system/clear/cache/" + mode,
                    success: function (data) {
                        jAlert(js_localize['file.cache.success'], js_localize['file.cache.title'], "success");
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                        var message = JSON_ERROR.composeMessageFromJSON(data);
                        jAlert(message, js_localize['file.cache.error_title'], "error");
                    },
                });
            }
        });
    });

    $('#upload').fileupload({
        autoUpload: false,
//        add: function (e, data) {
//            data.submit();
//        },
        maxChunkSize: 15000000, // 10 MB
//        multipart:false,
        type: 'POST',
        url: relPath + 'api/v1/files/upload',
        singleFileUploads: true,
        sequentialUploads: true,
        add: function (e, data) {
            var options = $(this).fileupload('option');
            var index;
            if (!e.delegatedEvent.target) {
                index = $(e.originalEvent.delegatedEvent.target).attr('name');
            } else {
                index = $(e.delegatedEvent.target).attr('name');
            }
            if (data.files[0].size > options.maxChunkSize) {
                fileuploadDataChunk = data.files;
            } else {
                if (!fileuploadData) {
                    fileuploadData = {};
                    //data.submit();
                }
                fileuploadData[index + '_title'] = data.files[0];
            }
        }
    });

    window.onbeforeunload = function (e) {
        e = e || window.event;
        var message = '';
        // For IE and Firefox prior to version 4
        if (uploadProgress.startedUploads > 0 && e.target.activeElement.nodeName == 'BODY') {
            message = 'You have started uploads and if you leave you will lost them!';
            if (e) {
                e.returnValue = message;
            }
            // For Safari
            return message;
        }
    };

});

function initForm() {
    // Check if form is opened in TinyMCE
    if (top.tinymce && top.tinymce.activeEditor) {
        // Get the parameters passed into the window from the top frame
        var args = top.tinymce.activeEditor.windowManager.getParams();
        console.log(args);
        if (args.dialog_mode) {
            // Keep the call back to set the selection
            callback_selection = args.callback_selection;
            select_multiple = args.select_multiple;
            image_type = args.image_type;
            if (args.initial_selection)
                initial_selection = args.initial_selection;
            if (args.isPageRestricted)
                isPageRestricted = args.isPageRestricted;
            // TODO: Show Buttons Insert and Cancel
            $('#filemanager_btn_upload').hide();
            $('#filemanager_btn_right').show();
            // TODO: Setup form for Single or Multi selection
            $(".wrapper").css({'padding-left': '0', 'width': '960px'});
        }
    } else {
        // Downloads Tab
    }
}

function bindFolderClicks() {
    $('#folder_control #create_folder').click(function () {
        jPrompt(js_localize['folder.add.label'], js_localize['folder.add.placeholder'], js_localize['folder.add.title'], 'success', function (r) {
            if (r) {
                var mode = $('#filemanger_mode').data('value');
                var anSelectedFolder = $("#upload #currentpath").val();
                if (anSelectedFolder.length !== 0) {
                    jQuery.ajax({
                        type: "POST",
                        url: relPath + "api/v1/files/makedir/" + mode + "?path=" + encodeURIComponent(anSelectedFolder) + "&dirname=" + encodeURIComponent(r),
                        success: function (data) {
                            //TODO: Update folder tree
                            closeRowEdit();
                            var full_path = anSelectedFolder + data.response.dirname;
                            jAlert(js_localize['folder.add.success'].replace('{0}', full_path), js_localize['folder.add.title'], "success");
                            $('#filetree').refreshTree(anSelectedFolder);
                        }, // success
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                            var message = JSON_ERROR.composeMessageFromJSON(data);
                            jAlert(message, js_localize['folder.add.error_title'], "error");
                        },
                    });
                }
            }
        });
    });

    /* Add a click handler for the delete row */
    $('#folder_control #remove_folder').click(function () {
        var anSelectedFolder = $("#upload #currentpath").val();
        if (anSelectedFolder.length !== 0) {
            var strMessage = $(this).attr('confirm');
            strMessage = strMessage.replace('{0}', anSelectedFolder);
            jConfirm(strMessage, js_localize['folder.remove.title'], "success", function (r) {
                if (r) {
                    var mode = $('#filemanger_mode').data('value');
                    var anSelectedFolder = $("#upload #currentpath").val();
                    if (anSelectedFolder.length !== 0) {
                        var urlAction = relPath + "api/v1/files/deletedir/" + mode + "?dirname=" + encodeURIComponent(anSelectedFolder) + '&isDeleted=' + isDeleted;
                        if (skipEmptyCheck) {
                            urlAction += '&skipEmptyCheck=' + skipEmptyCheck;
                        }
                        $('#pages_processing').css('visibility', 'visible');
                        jQuery.ajax({
                            type: "DELETE",
                            url: urlAction,
                            success: function (data) {
                                $('#pages_processing').css('visibility', 'hidden');
                                closeRowEdit();
                                //alert("Group " + data.response.name + " was delete successfully!");
                                jAlert(js_localize['folder.remove.text'], js_localize['folder.remove.title'], "success");
                                // Update folder tree
                                var parrentIndex = anSelectedFolder.lastIndexOf('/', anSelectedFolder.length - 2);
                                var parrent = anSelectedFolder.substring(0, parrentIndex + 1);
                                $('#filetree').refreshTree(parrent);
                            }, // success
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                $('#pages_processing').css('visibility', 'hidden');
                                var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                                var message = JSON_ERROR.composeMessageFromJSON(data);
                                jAlert(message, js_localize['folder.remove.title'], "error");
                            }
                        });
                    }
                }
            });
        } else {
            jAlert(js_localize['folder.remove.non_select'], 'Delete folder', "success");
        }
    });
}

function bindFormUploadClicks() {
    $('#cancel_files_upload').click(function (e) {
        if (uploadProgress.startedUploads > 0) {
            jConfirm(js_localize['file.upload.cancel'], js_localize['file.upload.title'], "success", function (r) {
                if (r) {
                    $(".cancel_submit").click();
                    clearUploadForm();
                }
            });
        } else {
            clearUploadForm();
        }
    });
    $('#user_table_btns #filemanager_btn_upload .nav_box.upload').click(function (e) {
        //e.stopPropagation();
        // Setup Form for Images or Dwonloads
        if (!$('#progress_wrapper').is(":visible")) {
            clearUploadForm();
            $('#progress_wrapper').show();
        }
        setupForm();
        $('#fileId').val('');
        // Show form
        $('#wrapper_form_upload').openDialog($('div.form_right .textarea_overlay'));

        var mode = $('#filemanger_mode').data('value');
        if (mode == 'images') {
            $('#wrapper_form_upload').css('top', '335px');
        } else if (mode == 'downloads') {
            $('#wrapper_form_upload').css('top', '275px');
        }
        $('#file_input').prop('disabled', false);
        $('#check_visible').parent().hide();
        $('#check_visible').removeAttr('checked');
    });

    /* Pop-up for upload files */
    $('#file_input').on('change', function () {
        $("#label_file_input").val(this.value);
    });
    $('#file_zoom').on('change', function () {
        $("#label_file_zoom").val(this.value);
    });
    $('#file_thumb').on('change', function () {
        $("#label_file_thumb").val(this.value);
    });

    $('#cancel_upload').on('click', function () {
        cancelChanges();
    });
    $('#check_protected').on('change', function () {
        if ($(this).is(':checked')) {
            $('#check_visible').parent().show();
        } else {
            $('#check_visible').parent().hide();
            $('#check_visible').removeAttr('checked');
        }

    });
}

function cancelChanges() {
    var promise = null;
    var fileID = $('#wrapper_form_upload #fileId').val();
    if (fileID != "" && fileID > 0) {
        promise = unlockItem(fileID, 'file');
    } else {
        promise = $.Deferred().resolve().promise();
    }

    promise.always(function () {
        editing_timer.stopTimer(false);
        $('.user_edit_wrapper').hide();
        $('#upload')[0].reset();
        $('#wrapper_form_upload').closeDialog();
        $('.user_table').css('height', '');
        $('#upload .error').removeClass('error');
        if (uploadProgress.startedUploads <= 0) {
            clearUploadForm();
        }
    });

    return promise;
}

function bindRowEditClicks() {
    /* Add a click handler for the delete row */
    $('.user_actions .nav_box.remove').click(function () {
        jConfirm($(this).attr('confirm'), js_localize['file.remove.title'], "success", function (r) {
            if (r) {
                var selectedFiles = fnGetSelected(oTable);
                if (selectedFiles.length > 0) {
                    closeRowEdit();

                    deleteFile(selectedFiles[0], isDeleted);
                }
            }
        });
    });

    /* Add a click handler for the edit row */
    $('.user_actions .nav_box.edit').click(function (e) {
        if ($("#filemanger_mode").data().value == "downloads") {
            $('#edit_mode').show();
        }
        e.stopPropagation();
        // Setup Form for Images or Dwonloads
        $('body').css('cursor', 'progress');
        $(this).addClass('active');
        setupForm();
        var anSelected = fnGetSelected(oTable);
        var file_id = $(anSelected[0]).find('.file_id').html();
        checkFileAvailability(file_id, function (time_left) {
            editing_timer.start(time_left, function () {
                $('#wrapper_form_upload #save_upload').click();
            }, function () {
                var promise = cancelChanges();
                promise.done(function () {
                    if (top.tinymce && top.tinymce.activeEditor) {
                        window.location.reload();
                    } else {
                        window.location.href = relPath + 'cms/' + $('#filemanger_mode').data('value');
                    }
                });

            }, file_id, 'file', $('#filemanger_mode').data('value'), EDIT_MAX_TIME_SECONDS);
            populateForm(file_id);
            $('#file_input').prop('disabled', true);
            $('#requaredInputFile').hide();
            if (time_left > EDIT_TIME_EXTENSION_ALERT_SECONDS) {
                jAlert(js_localize['files_' + $('#filemanger_mode').data('value') + '.edit.start']
                        .replace('{time_left}', editing_timer.formatTimeInMinutes(time_left))
                        .replace('{alert_minutes}', editing_timer.formatTimeInMinutes(EDIT_TIME_EXTENSION_ALERT_SECONDS)),
                        js_localize['files_' + $('#filemanger_mode').data('value') + '.edit.title'],
                        'success')
            }
        });
    });

    /* Add a click handler for recycle restore */
    $('.user_actions .nav_box.restore').click(function () {
        $('body').css('cursor', 'progress');
        jConfirm($(this).attr('confirm'), js_localize['file.restore.title'], "success", function (r) {
            if (r) {
                var anSelected = fnGetSelected(oTable);
                closeRowEdit();
                data = {'id': $(anSelected[0]).find('.file_id').html()};
                jQuery.ajax({
                    type: "PUT",
                    url: relPath + "api/v1/files/restore",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(data),
                    success: function (data) {
                        jAlert(js_localize['file.restore.success'], js_localize['file.restore.title'], "success");
                        //oTable.fnDeleteDataAndDisplay($(anSelected[0]).attr('id'));
                        oTable.fnDeleteRow(anSelected[0]);
                    },
                    // script call was *not* successful
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                        var message = JSON_ERROR.composeMessageFromJSON(data);
                        if (data.response !== undefined) {
                            $.each(data.response, function (key, value) {
                                $('input[name="' + key + '"]').parent().addClass('error');
                            });
                        }
                        $('body').css('cursor', 'default');
                        jAlert(message, js_localize['file.restore.title'], "error");
                    }
                });
            }
        });
    });

    /* Add a click handler for the move up row */
    $('.user_actions .nav_box.move_up').click(function (e) {
        var selected = fnGetSelected(oTable)[0];
        oTable.fnSwapRow(selected, 'up');
        closeRowEdit();
    });
    /* Add a click handler for the move down row */
    $('.user_actions .nav_box.move_down').click(function (e) {
        var selected = fnGetSelected(oTable)[0];
        oTable.fnSwapRow(selected, 'down');
        closeRowEdit();
    });

    /* Buttons Insert and cancel */
    $('#user_table_btnInsert').on('click', function () {
        var idArray = getChecked();
        if (top.tinymce && top.tinymce.activeEditor) {
            var args = top.tinymce.activeEditor.windowManager.getParams();
            if (args && args.dialog_mode) {
                localStorage.setItem($('#filemanger_mode').data('value'), $('#currentpath').val());
            }
        }
        var anSelectedFolder = $("#upload #currentpath").val();
        var srcArray = [];
        for (i = 0; i < idArray.length; i++) {
            if ($('tr#page_' + idArray[i] + ' td.strong span').length > 1 && isPageRestricted == '0') {
                jAlert(js_localize['file.protected.error'], js_localize['content.edit_download.title'], "error");
                return false;
            }
            var tmpSrc = $('tr#page_' + idArray[i] + ' td.strong span').html();
            // For files from current folder extend the path
            if (selectionArray.indexOf(idArray[i]) === -1) {
                tmpSrc = anSelectedFolder + tmpSrc;
            }
            srcArray.push(tmpSrc);
        }
        var isNewsletter = $('#isNewsletter').data('value');
        if (isNewsletter) {
            jQuery.ajax({
                type: "GET",
                url: relPath + "api/v1/files/discription/" + idArray[0] + '?lang=' + lang,
                success: function (data) {
                    var cleartitle = '';
                    if (data.response && data.response.description && data.response.description != '') {
                        cleartitle = data.response.description.replace(/&nbsp;/gi, ' ').replace(/<\s*h1\s*>.*<\s*\/\s*h1\s*>|<[^>]+>/gi, '');
                    }
                    callback_selection(idArray, srcArray, cleartitle);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                    var message = JSON_ERROR.composeMessageFromJSON(data, true);
                    jAlert(message, js_localize['file.load.title'], "error");
                    callback_selection(idArray, srcArray, "");
                },
                complete: function () {
                    top.tinymce.activeEditor.windowManager.close();
                }
            });
        } else {
            callback_selection(idArray, srcArray, "");
            top.tinymce.activeEditor.windowManager.close();
        }

    });

    $('#user_table_btnCancel').on('click', function () {
        if (top.tinymce && top.tinymce.activeEditor) {
            var args = top.tinymce.activeEditor.windowManager.getParams();
            if (args && args.dialog_mode) {
                localStorage.setItem($('#filemanger_mode').data('value'), $('#currentpath').val());
            }
        }
        refreshTable();
        top.tinymce.activeEditor.windowManager.close();
    });
}

function closeRowEdit() {
    $(oTable.fnSettings().aoData).each(function () {
        $(this.nTr).removeClass('row_selected');
    });
    if ($('.user_actions .nav_box').hasClass('active')) {
        $('.user_actions .nav_box').removeClass('active');
    }
    if ($('#edit_mode').length && $('#edit_mode').is(':visible')) {
        $('#edit_mode').hide();
    }
    $('.user_edit_wrapper').hide();
    $('body').css('cursor', 'default');
}

function checkFileAvailability(file_id, callback) {

    jQuery.ajax({
        type: "GET",
        url: relPath + "api/v1/files/isavailable/" + file_id,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (typeof (callback) === "function") {
                var time_left = 0;
                if (data.response.time_left) {
                    time_left = data.response.time_left;
                }
                callback(time_left);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
            var message = JSON_ERROR.composeMessageFromJSON(data, true);
            jAlert(message, js_localize['files_' + $('#filemanger_mode').data('value') + '.edit.title'], "error");
            $('body').css('cursor', 'default');
        }
    });
}

function initTable() {
    var visible = false;
    if (top.tinymce && top.tinymce.activeEditor) {
        var args = top.tinymce.activeEditor.windowManager.getParams();
        if (args && args.dialog_mode) {
            visible = true;
        }
    }
    $.fn.dataTableExt.oStdClasses.sPaging = 'dataTables_paginate paging_ellipses download_';
    oTable = $('#pages').dataTable({
        "sAjaxDataProp": 'response',
        "fnServerData": fnServerData,
        "iDisplayLength": 20,
        "bFilter": true,
        "bLengthChange": false,
        "sPaginationType": 'ellipses',
        "iPageInterval": 30,
        "sScrollY": '570px',
        "bSort": false,
        'bAutoWidth': false,
        "bDeferRender": true,
        "bProcessing": true,
        "oLanguage": {
            "sEmptyTable": " "},
        "aoColumns": [
            {"sType": "checkbox", "mData": function (source) {
                    var checked = "";
                    if (selectionArray.indexOf(source.id.toString()) !== -1) {
                        checked = "checked check_pos='" + selectionArray.indexOf(source.id.toString()) + "'";
                    }
                    return '<input ' + checked + ' type="checkbox" class="filemanagerCheckbox" value="' + source.id + '" id="' + source.id + '" /><label for="' + source.id + '"></label>';
                }, "sWidth": "14px", "bSearchable": false, "bVisible": visible},
            {"sType": "page-title", "mData": function (source) {
                    var protected = '';
                    if (source.protected) {
                        protected = '<span style="font-size: 14px;">*</span>'
                    }
                    return '<span style="display: block;font-size: 11px;line-height: 12px;">' + protected + source.filename + '</span>';
                }, "sWidth": "280px", "sClass": "strong", "bSearchable": true},
            {"sType": "numeric", "mData": "id", "sWidth": "40px", "bSearchable": false, "sClass": "file_id"},
            {"sType": "string", "mData": "extension", "sWidth": "36px", "bSearchable": false},
            {"sType": "numeric", "mData": function (source) {
                    return '<span style="display: block;font-size: 11px;line-height: 12px;">' + bytesToSize(source.size) +'</span>';
                }, "sWidth": "73px", "bSearchable": false},
            {"sType": "string", "mData": "updated_at", "sWidth": "115px", "bSearchable": false}
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).attr('id', 'page_' + aData.id);
            $(nRow).attr('pos', aData.position);
            $(nRow).attr('level', aData.level);
            $(nRow).addClass('level_' + aData.level);
            $(nRow).attr('parent', aData.parent_id);
        },
        "fnInitComplete": function (oSettings, json) {
            $('#pages').addClass('initialized');
        }
    });
    $('.user_edit_wrapper').appendTo('.dataTables_scrollBody');
    /* Atach event when have scrool to hide edit wrapper */
//	$('.dataTables_scrollBody').scroll(function(){
//		if($('.user_edit_wrapper').is(":visible")){
//			closeRowEdit();
//		}
//	});
}

/* Get the rows which are currently selected */
function fnGetSelected(oTableLocal) {
    return oTableLocal.$('tr.row_selected');
}

function getChecked() {
    var idArray = [];
    $("input:checked", oTable.fnGetNodes()).each(function () {
        idArray.push($(this).val());
    });
    return idArray;
}

function refreshTable() {
    $('body').css('cursor', 'progress');
    var mode = $('#filemanger_mode').data('value');
    var anSelectedFolder = $("#upload #currentpath").val();
    var search_input = $('.user_search').val() !== js_localize['pages.label.search'] ? $('.user_search').val() : "";
    var sort_input = $('.user_sort').val() == -1 ? 'filename' : $('.user_sort').val();
    if ($('#pages').hasClass('initialized') && select_multiple == true && $("input", oTable.fnGetNodes()).length > 0) {
        selectionArray = getChecked();
        initial_selection = selectionArray.join(',');
    } else {
        if (initial_selection) {
            selectionArray = initial_selection.split(',');
        } else {
            selectionArray = [];
        }
    }
    oTable.fnSettings()._fnReloadAjaxCompleate = function () {
        $('body').css('cursor', 'default');
    };
    oTable.fnReloadAjax(relPath + "api/v1/files/all/" + mode + "?filter=" + search_input + "&path=" + anSelectedFolder + "&init_selection=" + initial_selection + "&sort-by=" + sort_input + "&isDeleted=" + isDeleted,
            function () {
                $('body').css('cursor', 'default');
            });
    return false;
}

function hideTitle(el, lang) {
    if ($(el).prop('checked') == true) {
        $('input[name="title_' + lang + '"]').parent().show();
    } else {
        $('input[name="title_' + lang + '"]').parent().hide();
    }
}

function bytesToSize(bytes, lblZero) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0)
        return (lblZero) ? lblZero : 'n/a';
    var i = parseInt(Math.floor(Math.log(Math.abs(bytes)) / Math.log(1024)));
    if (i === 0) {
        if (Math.abs(bytes) === 1)
            return bytes + ' Byte';
        else
            return bytes + ' ' + sizes[i];
    } else {
        return (bytes / Math.pow(1024, i)).toFixed(2) + ' ' + sizes[i];
    }
}

function populateForm(fileId) {
    jQuery.ajax({
        type: "GET",
        url: relPath + "api/v1/files/read/" + fileId,
        success: function (data) {
            var dataEN = $.grep(data.response.fileInfo, function (e) {
                return e.lang == 1;
            })[0];
            var dataDE = $.grep(data.response.fileInfo, function (e) {
                return e.lang == 2;
            })[0];
            var dataFile = data.response;
            if (dataEN) {
                $("#titleEN").val(dataEN.title);
                tinyMCE.get('textarea_descriptionEN').setContent(dataEN.description);
                $('#fileInfoIdEN').val(dataEN.id);
            }
            if (dataDE) {
                $("#titleDE").val(dataDE.title);
                tinyMCE.get('textarea_descriptionDE').setContent(dataDE.description);
                $('#fileInfoIdDE').val(dataDE.id);
            }
            if (dataFile) {
                $('#fileId').val(dataFile.id);
                $('#label_file_input').val(dataFile.filename);
                $('#label_file_zoom').val(dataFile.zoompic);
                $('#label_file_thumb').val(dataFile.thumbnail);
            }
            if ($('#filemanger_mode').data('value') == 'downloads') {
                if (dataFile.visible || dataFile.protected) {
                    $('#check_visible').parent().show();
                } else {
                    $('#check_visible').parent().hide();
                }
                $.each(dataFile, function (key, value) {
                    if ($('select[name="' + key + '"]').length > 0) {
                        if (!value) {
                            value = 0;
                        }
                        $('select[name="' + key + '"]').parent().find('ul li[rel="' + value + '"]').click();
                    } else if ($(':input[name="' + key + '"]').length > 0) {
                        if ($(':input[name="' + key + '"]').attr('type') == "checkbox" && value) {
                            $(':input[name="' + key + '"]').prop('checked', true);
                        } else {
                            $(':input[name="' + key + '"]').val(value);
                        }
                    }
                });
            }
            if (dataFile.protected) {
                $('#check_protected').prop('checked', true);
            }
            // Show form
            var position = $('.user_edit_wrapper').position();
            $('#wrapper_form_upload').css('top', position.top + 81 + 'px');
            $('#wrapper_form_upload').openDialog($('div.form_right .textarea_overlay'));

            var height = $('#wrapper_form_upload').position().top + $('#wrapper_form_upload').height() + 10 + $('.form_right').position().top;
            if ($('.user_table').height() < height) {
                $('.user_table').css('height', height + 'px');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
            var message = JSON_ERROR.composeMessageFromJSON(data, true);
            jAlert(message, js_localize['file.load.title'], "error");
        },
        complete: function () {
            $('body').css('cursor', 'default');
        }
    });
    return false;
}

/* Preparation for upload */
function setupForm() {
    var mode = $('#filemanger_mode').data('value');
    $('div#upload .error').removeClass('error');
    if (mode == 'images') {
        $('#wrapper_titleEN').hide();
        $('#wrapper_titleDE').hide();
        $('#label_for_file_input').html("Image<span id='requaredInputFile' class='fieldset_requared'>*</span>");
        $('#file_input').attr('accept', 'image/jpeg,image/png');
        //$('#div_file_thumb').css('visibility', 'hidden');
    } else if (mode == 'downloads') {
        $('#wrapper_titleEN').show();
        $('#wrapper_titleDE').show();
        $('#label_for_file_input').html("File<span id='requaredInputFile' class='fieldset_requared'>*</span>");
        $('#file_input').attr('accept', '');
        //$('#div_file_thumb').css('visibility', 'visible');
    }
    createTitleTinyMce();
}

function parseForm() {
    var fileID = $('#fileId').val();
    var mode = $('#filemanger_mode').data('value');
    if ((!fileID || fileID.length == 0) && (mode == 'images')) {
        var fileName = $('#file_input').val();
        $('#titleEN').val(fileName);
        $('#titleDE').val(fileName);
    }
    $('#descriptionEN').val(tinyMCE.get('textarea_descriptionEN').getContent());
    $('#descriptionDE').val(tinyMCE.get('textarea_descriptionDE').getContent());
}

/* AJAX Actions */
function uploadFile() {
    var fileId = $('#wrapper_form_upload #fileId').val();
    if (fileId != "" && fileId > 0) {
        var promise = uploadFilePromise();
        promise.done(function () {
            editing_timer.stopTimer(false);
            unlockItem(fileId, 'file');
        });
    } else {
        uploadFilePromise();
    }
    return false;
}

function uploadFilePromise() {
    var deferred = $.Deferred();
    var file = {};
    parseForm();
    var isNewFile = true;
    var mode = $('#filemanger_mode').data('value');
    var fileID = $('#fileId').val();
    var strURL = relPath + "api/v1/files/createfile?mode=" + mode;
    if (fileID && fileID.length > 0) {
        isNewFile = false;
        strURL = relPath + "api/v1/files/updatefile/" + fileID + "?mode=" + mode;
    }
//    var strURL = relPath + 'api/v1/files/upload';

    var progressbar = $('<div/>', {class: 'progress_wrapper cf'});
    var formDiv = $('<div/>', {class: 'form'});
    progressbar.append(formDiv);
    progressbar.appendTo('#progress');
    if (isNewFile) {
        progressbar.append('<div class="ball_wrapper"></div>');
        progressbar.append('<div class="bar_wrapper"><div class="bar_border"><div class="bar"></div></div></div>');
        progressbar.append('<span class="cancel_submit">X</span>');
        progressbar.append('<div class="info_wrapper"><div class="file_name"></div><div class="file_size"></div></div>');
        formDiv.fileupload({
            autoUpload: false,
            type: 'POST',
            url: relPath + 'api/v1/files/upload?mode=' + $('#filemanger_mode').data('value'),
            singleFileUploads: true,
            maxChunkSize: 15000000,
            progress: function (e, data) {
                if (data.formData[data.formData.length - 1].size) {
                    data.total = data.formData[data.formData.length - 1].size;
                }
                if (data.maxChunkSize) {
                    uploadProgress.uploadedSize += data.loaded - data.formData[data.formData.length - 1].chunkLoaded;
                    data.formData[data.formData.length - 1].chunkLoaded = data.loaded;
                    data.loaded = 0;
                } else {
                    uploadProgress.uploadedSize += data.loaded - data.formData[data.formData.length - 1].loaded;
                    data.formData[data.formData.length - 1].loaded = data.loaded;
                }
                if (data.formData[data.formData.length - 1].size) {
                    data.loaded = data.loaded + data.formData[data.formData.length - 1].chunkLoaded;
                }
                var progress = parseInt(data.loaded / data.total * 100, 10);
                progressbar.find('.bar').css('width', progress + '%');
                progressbar.find('.ball_wrapper').html(progress + '%');
                progressbar.find('.file_size').html(parseFloat(data.loaded / 1024 / 1024).toFixed(2) + ' MB/' + parseFloat(data.total / 1024 / 1024).toFixed(2) + ' MB');
                var allProgress = parseInt(uploadProgress.uploadedSize / uploadProgress.totalSize * 100, 10);
                $('#main_progress .ball_wrapper').html(allProgress + '%');
                $('#main_progress .file_size').html(parseFloat(uploadProgress.uploadedSize / 1024 / 1024).toFixed(2) + ' MB/' + parseFloat(uploadProgress.totalSize / 1024 / 1024).toFixed(2) + ' MB');
                $('#main_progress .bar').css('width', allProgress + '%');
            },
        });
    } else {
        formDiv.fileupload({
            autoUpload: false,
            type: 'POST',
            singleFileUploads: true,
            progress: function () {}
        });
    }
//    var options = $('#upload').fileupload('option');
    var filesArray = [];
    var total = 0;
    for (var key in fileuploadData) {
        filesArray.push(fileuploadData[key]);
        $("input[name='" + key + "']").val(filesArray.length - 1);
        if (key == 'file_title') {
            progressbar.find('.file_name').html(fileuploadData[key].name);
        }
        total += fileuploadData[key].size;
    }
    var formData = $('#upload').serializeArray();
    if (isNewFile) {
        formData.push({name: 'upload', loaded: 0});
        uploadProgress.totalSize += total;
    }
    if (fileuploadDataChunk) {
        total += fileuploadDataChunk[0].size;
        uploadProgress.totalSize += fileuploadDataChunk[0].size;
        formData[formData.length - 1].size = total;
        formData[formData.length - 1].chunkLoaded = 0;
        var locFileuploadData = filesArray;
        uploadProgress.startedUploads += 1;
        var element = formData[formData.length - 1];
        formData[formData.length - 1] = {name: "file", value: fileuploadDataChunk[0].name};
        formData.push(element);
        var jqXHR = formDiv.fileupload('send', {files: fileuploadDataChunk, formData: formData});
        progressbar.find('.file_name').html(fileuploadDataChunk[0].name);
        progressbar.find('span.cancel_submit').click(function (e) {
            var that = this;
            jConfirm(js_localize['file.upload.cancel'], js_localize['file.upload.title'], "success", function (r) {
                if (r) {
                    $(that).unbind();
                    jqXHR.abort();
                }
            });
        });
        jqXHR.success(function (result, textStatus, jqXHR) {
            var result = JSON.parse(result);
            var index = -1;
            for (var i = 0; i < formData.length; i++) {
                if (formData[i].name.toLowerCase() === "file_title") {
                    formData[i].value = result.files[0].path;
                }
                if (formData[i].name.toLowerCase() === "file" && formData[i].value == result.files[0].name) {
                    index = i;
                }
            }
            if (index >= 0) {
                formData.splice(index, 1);
            }
            uploadProgress.startedUploads -= 1;
//            var options = formDiv.fileupload('option');
            sendFileUploadForm(progressbar, strURL, locFileuploadData, formDiv, formData, deferred, isNewFile);
        }).error(function (data, textStatus, errorThrown) {
            if (data.responseText) {
                var data = JSON_ERROR.errorResponse2JSON(data);
                var message = JSON_ERROR.composeMessageFromJSON(data, true);
                if (data.response !== undefined) {
                    $.each(data.response, function (key, value) {
                        if ($('input[name="' + key + '"]').length > 0) {
                            $('input[name="' + key + '"]').parent().addClass('error');
                        } else if ($('select[name="' + key + '"]').length > 0) {
                            $('select[name="' + key + '"]').parent().addClass('error');
                        }
                    });
                }
            } else if (textStatus == 'abort') {
                message = js_localize['file.save.abort'];
            } else {
                message = errorThrown;
            }
            jAlert(message, js_localize['file.save.title'], "error");
            deferred.resolve();
            uploadProgress.startedUploads -= 1;
            progressbar.addClass('error');
            progressbar.find('span.cancel_submit').css('display', 'none').unbind();
        });
        ;
    } else {
        sendFileUploadForm(progressbar, strURL, filesArray, formDiv, formData, deferred, isNewFile);
    }
    $('.user_edit_wrapper').hide();
    $('#wrapper_form_upload').closeDialog();
    $('#upload')[0].reset();
    $('.user_table').css('height', '');
    fileuploadData = null;
    fileuploadDataChunk = null;
//    jQuery('#upload').fileupload('send', jQuery('#upload').data());
//    jQuery('#upload').ajaxForm({
//        url: strURL + mode,
//        data: file,
//        beforeSubmit: function (a, f, o) {
//            o.dataType = "json";
//        },
//        // script call was successful
//        // data contains the JSON values returned by the Perl script
//        success: function (data) {
//            $('.user_edit_wrapper').hide();
//            $('#wrapper_form_upload').closeDialog();
//            $('#upload')[0].reset();
//            $('.user_table').css('height', '');
//            refreshTable();
//            jAlert(js_localize['file.save.text'], js_localize['file.save.title'], "success");
//            file.id = data.response.id;
//            if (data.error) { // script returned error
//                if (typeof (console) !== "undefined" && console.log !== undefined)
//                {
//                    console.log("error: " + data);
//                }
//            }
//
//            $('div#upload .error').removeClass('error');
//             $(".bar").css('width', '0px');
//			deferred.resolve();
//        },
//        // script call was *not* successful
//        error: function (XMLHttpRequest, textStatus, errorThrown) {
//            var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
//            var message = JSON_ERROR.composeMessageFromJSON(data, true);
//            if (data.response !== undefined) {
//                $.each(data.response, function (key, value) {
//                    if ($('input[name="' + key + '"]').length > 0) {
//                        $('input[name="' + key + '"]').parent().parent().addClass('error');
//                    }
//                });
//            }
//            jAlert(message, js_localize['file.save.title'], "error");
//			deferred.resolve();
//        }, // error
//        uploadProgress: function(event, position, total, percentComplete) {
//            var percentVal = percentComplete + '%';
//            $(".bar").css('width', percentVal);
//        },
//    });
    return deferred.promise();
}

function sendFileUploadForm(progressbar, strURL, data, formDiv, formData, deferred, isNewFile) {
    var context;
    formDiv.fileupload('option', {
        url: strURL,
        maxChunkSize: false,
        formData: formData,
        add: function (e, data) {
            context = data;
        }
    });

    var jqXHR;
    if (data.length == 0) {
        formDiv.fileupload('add', {
            files: ['']
        });
        if (isNewFile)
            uploadProgress.startedUploads += 1;
        jqXHR = context.submit();
    } else {
        if (isNewFile)
            uploadProgress.startedUploads += 1;
        jqXHR = formDiv.fileupload('send', {files: data});
    }
//    var jqXHR = data.submit();
    progressbar.find('span.cancel_submit').click(function (e) {
        var that = this;
        jConfirm(js_localize['file.upload.cancel'], js_localize['file.upload.title'], "success", function (r) {
            if (r) {
                $(that).unbind();
                jqXHR.abort();
            }
        });
    });
    jqXHR.success(function (data, textStatus, jqXHR) {
        refreshTable();
        jAlert(js_localize['file.save.text'], js_localize['file.save.title'], "success");
        if (data.error) { // script returned error
            if (typeof (console) !== "undefined" && console.log !== undefined)
            {
                console.log("error: " + data.result);
            }
        }

        $('div#upload .error').removeClass('error');
//            progressbar.remove();
        deferred.resolve();
        if (isNewFile)
            uploadProgress.startedUploads -= 1;
        progressbar.find('span.cancel_submit').css('display', 'none').unbind();
    }).error(function (data, textStatus, errorThrown) {
        if (data.responseText) {
            var data = JSON_ERROR.errorResponse2JSON(data);
            var message = JSON_ERROR.composeMessageFromJSON(data, true);
//                if (data.response !== undefined) {
//                    $.each(data.response, function (key, value) {
//                        if ($('input[name="' + key + '"]').length > 0) {
//                            $('input[name="' + key + '"]').parent().addClass('error');
//                        }
//                    });
//                }
        } else if (textStatus == 'abort') {
            message = js_localize['file.save.abort'];
        } else {
            message = errorThrown;
        }
        jAlert(message, js_localize['file.save.title'], "error");
        deferred.resolve();
        if (isNewFile)
            uploadProgress.startedUploads -= 1;
        progressbar.addClass('error');
        progressbar.find('span.cancel_submit').css('display', 'none').unbind();
    });

}
// Delete file
function deleteFile(selectedFile, isPermanentDeleted, skipRelationCheck = false) {
    var action = isPermanentDeleted ? 'permanentdelete/' : 'deletefile/';
    if (selectedFile) {
        var urlAction = relPath + "api/v1/files/" + action + $(selectedFile).find('.file_id').html();
        urlAction += '?skipRelationCheck=' + skipRelationCheck;
        jQuery.ajax({
            type: "DELETE",
            url: urlAction,
            success: function (data) {
                oTable.fnDeleteRow(selectedFile);
                $('#wrapper_form_upload').closeDialog();
                $('#upload')[0].reset();
                jAlert(js_localize['file.remove.text'], js_localize['file.remove.title'], "success");
            }, // success
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                var message = JSON_ERROR.composeMessageFromJSON(data);
                //Check for no free space in recycle bin (507 - Insufficient Storage)
                if (XMLHttpRequest.status == 507) {
                    jConfirm(js_localize['file.remove.confirmation'], js_localize['file.remove.title'], "error", function (r) {
                        if (r) {
                            deleteFile(selectedFile, isPermanentDeleted);
                        }
                    });
                } else if (XMLHttpRequest.status == 423) {
                    // file is locked. It is used in page
                    $.alerts.cancelButton = js_localize['users.no'];
                    $.alerts.okButton = js_localize['users.yes'];
                    jConfirm(message + '<br/><br/>' + js_localize['file.remove.version_zero.confirm'], js_localize['file.remove.title'], "error",
                            function (r) {
                                if (r) {
                                    //TODO: Permanent delete?
                                    deleteFile(selectedFile, isPermanentDeleted, true);
                                }
                            }
                    );
                    $.alerts.cancelButton = js_localize['pages.jconfirm.no'];
                    $.alerts.okButton = js_localize['pages.jconfirm.ok'];
                } else {
                    jAlert(message, js_localize['file.remove.title'], "error");
                }
            },
        });
}
}


/*
 * Create FileTree and bind elements
 * called during initialization and also when adding a file
 * directly in root folder (via addNode)
 */
var fullexpandedFolder = null;

var createFileTree = function () {
    // Creates file tree.
    $('#filetree').fileTree({
        root: fileRoot,
        datafunc: populateFileTree,
        multiFolder: false,
        folderCallback: function (path) {
            setUploader(path);/*populateFileTree(path, function () {});*/
        },
        expandedFolder: fullexpandedFolder,
        after: function (data) {
        }
    }, function (file) {
        //No files in the tree
    });

};

// Display Folder info
function showFolderInfo(path) {
    if (path in folderInfoMap) {
        var folderInfo = folderInfoMap[path];
        var size_error_bytes = folderInfo.db_folder_space_bytes - folderInfo.fs_folder_space_bytes;
        var lblPath = path;
        if (!(path === '/')) {
            lblPath = "{path} ({error}/{size})".replace('{path}', path).replace('{error}', bytesToSize(size_error_bytes, '-'))
                    .replace('{size}', bytesToSize(folderInfo.db_folder_space_bytes, '-'));
        }
        $('#currentfolder span#folder_selected').text(lblPath);

        if (isDeleted && folderInfo.total_free_space) {
            // Total space of recycle is visible for Admin and Aprover only
            if (hasAccessForDelete) {
                var lblTotal = js_localize['recycle_bin.total.text'].replace('{0}', bytesToSize(folderInfo.total_free_space_bytes)
                        ).replace('{1}', Math.round(folderInfo.total_free_space));
                $("div#total_aviable_space").text(lblTotal).css('visibility', 'visible');
            }

            var lblBrand = js_localize['recycle_bin.brand.text'].replace('{0}', bytesToSize(folderInfo.brand_free_space_bytes)
                    ).replace('{1}', Math.round(folderInfo.brand_free_space));
            $("div#aviable_space").text(lblBrand).css('visibility', 'visible');
        }
    }
}

// Retrieve data (file/folder listing) for jqueryFileTree and pass the data back
// to the callback function in jqueryFileTree
var populateFileTree = function (path, callback) {
    // Dirty fix
    if (!path)
        path = '';
    var d = new Date(); // to prevent IE cache issues
    var isFullTree = false;
    var mode = $('#filemanger_mode').data('value');
    if (top.tinymce && top.tinymce.activeEditor) {
        var args = top.tinymce.activeEditor.windowManager.getParams();
        var storedPath = localStorage.getItem(mode);
        if (args && args.dialog_mode && storedPath) {
            localStorage.removeItem(mode);
            path = storedPath;
            isFullTree = true;
        }
    }
    var url = relPath + 'api/v1/files/subfolders?path=' + encodeURIComponent(path) + '&mode=' + mode + '&time=' + d.getMilliseconds() + '&isDeleted=' + isDeleted + '&isFullTree=' + isFullTree;
    //if ($.urlParam('type')) url += '&type=' + $.urlParam('type');
    jQuery.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            var result = '';
            if (data) {
                if (data.folder_info) {
                    folderInfoMap[path] = data.folder_info;
                    showFolderInfo(path);
                }
                if (data.files) {
                    if (path == fileRoot || isFullTree) {
                        setUploader(path);
                        if ($('#filetree').find('.rootFileTree').length == 0) {
                            result += '<ul class="jqueryFileTree rootFileTree">';
                            result += '<li class="directory expanded"><a href="#" class="" data-path="' + fileRoot + '">' + mode + fileRoot.slice(0, -1) + "</a>";
                        }
                    }
                    result += "<ul class=\"jqueryFileTree\" style=\"display: none;\">";
                    // Sort Alphabetical
                    var key_sorted = Object.keys(data.files).sort(function (a, b)
                    {
                        var A = a.toLowerCase();
                        var B = b.toLowerCase();
                        if (A < B) {
                            return -1;
                        } else if (A > B) {
                            return  1;
                        } else {
                            return 0;
                        }
                    });
                    for (var i = 0; i < key_sorted.length; i++) {
                        var cap_classes = "";
                        var folderItem = data.files[key_sorted[i]];
                        result += "<li class=\"directory " + (folderItem.Childs ? "expanded" : "collapsed")
                                + "\"><a href=\"#\" class=\"" + cap_classes + "\" data-path=\"" + folderItem['Path'] + "\">"
                                + folderItem['Filename'] + "</a>";
                        if (folderItem.Childs) {
                            result += generateChild(folderItem.Childs);
                        }
                        result += "</li>";
                    }
                    result += "</ul>";
                    if (path == fileRoot || isFullTree) {
                        result += "</li></ul>";
                    }
                }
            } else {
                result += '<h1>' + lg.could_not_retrieve_folder + '</h1>';
            }
            callback(result);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
            var message = JSON_ERROR.composeMessageFromJSON(data, true);
            jAlert(message, js_localize['fileTree.load.title'], "error");
        }
    });
};

// Sets the folder status, upload, and new folder functions
// to the path specified. Called on initial page load and
// whenever a new directory is selected.
var setUploader = function (path) {
    $('#currentpath').val(path);
    showFolderInfo(path);

    closeRowEdit();
    if (oTable) {
        refreshTable();
    }
};

function generateChild(childs) {
    var result = "<ul class=\"jqueryFileTree\" style=\"display: none;\">";
    var key_sorted = Object.keys(childs).sort(function (a, b) {
        var A = a.toLowerCase();
        var B = b.toLowerCase();
        if (A < B) {
            return -1;
        } else if (A > B) {
            return  1;
        } else {
            return 0;
        }
    });
    for (var i = 0; i < key_sorted.length; i++) {
        var cap_classes = "";
        result += "<li class=\"directory " + (childs[key_sorted[i]]['Childs'] ? "expanded" : "collapsed")
                + "\"><a href=\"#\" class=\"" + cap_classes + "\" data-path=\"" + childs[key_sorted[i]]['Path'] + "\">"
                + childs[key_sorted[i]]['Filename'] + "</a>";
        if (childs[key_sorted[i]]['Childs']) {
            result += generateChild(childs[key_sorted[i]]['Childs']);
        }
        result += "</li>";
    }
    result += "</ul>";
    return result;
}

function clearUploadForm() {
    uploadProgress = {startedUploads: 0, totalSize: 0, uploadedSize: 0};
    $('#progress_wrapper').hide();
    $('#main_progress .ball_wrapper').html('0%');
    $('#main_progress .file_size').html('0 MB/0 MB');
    $('#main_progress .bar').css('width', '0%');
    $('#progress .progress_wrapper').remove();
}

function showRecycleBin(element) {
    $(element).addClass('active');
    $('.nav_box.white.remove').attr('confirm', js_localize['recycle_bin.remove.confirmation']);
    $('.user_table .free_space').css('visibility', 'initial');
    isDeleted = true;
    $("#create_folder").css('visibility', 'hidden');
    $('#filetree').refreshTree();
    $('#user_table_btns #filemanager_btn_upload').css('visibility', 'hidden');
}

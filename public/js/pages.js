var oTable;
var filtered_table = false;
var move_item = 0;
var has_new_version = false;

//var relPath = getRelativePath();

$(document).ready(function() {
	if (typeof page_table_init !== 'undefined') {
		//defined in welcome screen for example (welcome.js)
		oTable = $('#pages').dataTable(page_table_init);
	}else{
		oTable = $('#pages').dataTable({
			"bProcessing": true,
			"sAjaxSource": relPath + "api/v1/pages/all?lang=" + lang + "&depth=2&parent_id=0",
			"sAjaxDataProp": 'response',
			"fnServerData": fnServerData,
			"iDisplayLength": 10,
			"bFilter": true,
			"sPaginationType": 'ellipses',
			"bLengthChange": false,
			"iShowPages": 20,
			"bSort": true,
			"aaSorting": [],
			"oLanguage": {
				"sEmptyTable": " "},
			"aoColumns": [
				{"sType": "page-title", "mData": function(source) {
						var path = '';
						var sign = '+';
						if (source.id == 1) {
							sign = "-";
						}
						if (typeof (source.path) != "undefined") {
							path = '<span style="display: block;font-size: 11px;line-height: 12px;">' + source.path + '</span>';
						}

						path += '<a onClick="return rowExpand(this,event)" href="" class="' + (source.isChildless ? "childless" : "") + '">' + (source.isChildless ? "" : sign) + '</a> <span class="title">' + source.title + '</span>';

						return path;
					}, "sWidth": "470px", "sClass": "strong", "bSearchable": true},
				{"sType": "numeric", "mData": "id", "sWidth": "40px", "bSearchable": false, "sClass": "page_id"},
//				{"sType": "string", "mData": "type", "sWidth": "60px", "bSearchable": false},
				{"sType": "string", "mData": function(source) {
						if (source.active == 1) {
							var status = js_localize['pages.label.active'];
							if(source.isNotActive){
								status += '<span style="font-size: 18px;">*</span>';
							}
							return status+'<br />('+source.editing_state+')';
							//return status;
						} else {
							//return js_localize['pages.label.inactive']+',<br />'+source.editing_state;
							return js_localize['pages.label.inactive']+'<br />('+source.editing_state+')';
						}
					}, "sWidth": "70px", "bSearchable": false, "sClass": "publish_unpublish"},
				{"sType": "string", "mData": function(source) {
						if (source.authorization == 1) {
							return js_localize['pages.label.auth'];
						} else {
							return js_localize['pages.label.non_auth'];
						}
					}, "sWidth": "110px", "bSearchable": false},
				{"sType": "string", "mData": "updated_at", "sWidth": "130px", "bSearchable": false},
                {"sType": "number", "mData": "u_id", "bSearchable": false, "sClass": "u_id","bVisible": false},

			],
			"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$(nRow).attr('id', 'page_' + aData.id);
				$(nRow).attr('pos', aData.position);
				$(nRow).attr('view_pos', aData.view_pos);
				$(nRow).attr('level', aData.level);
				$(nRow).addClass('level_' + aData.level);
				$(nRow).attr('parent', aData.parent_id);
				if (aData.id == 1) {
					$(nRow).addClass('expanded');
				}
				if (move_item == aData.u_id) {
					$(nRow).addClass('disabled');
				}
			},
		});
	}


	var oSettings = oTable.fnSettings();
	oSettings.sAlertTitle = js_localize['pages.list.title'];
	// Hide processing indicator for the first loading
	oTable.oApi._fnProcessingDisplay(oSettings, false);

	$("#pages tbody").click(function(event) {
		if (!$(event.target).hasClass('dataTables_empty') && !$('.page_move_div').is(":visible") && $(event.target).closest('.welcome').length == 0) {
            var $tr = $(event.target.parentNode)[0];
			closeRowEditor();
			$(oTable.fnSettings().aoData).each(function() {
				$(this.nTr).removeClass('row_selected');
			});
			if ($(event.target.parentNode).prop("tagName") != "TD") {
				$(event.target.parentNode).addClass('row_selected');
				$('.user_edit_wrapper').css('top', $('#pages_wrapper').position().top + $(event.target.parentNode).position().top + $(event.target.parentNode).height() + 1);
			} else {
				$(event.target.parentNode).parent().addClass('row_selected');
                $tr = $(event.target.parentNode).parent()[0];
				$('.user_edit_wrapper').css('top', $('#pages_wrapper').position().top + $(event.target.parentNode).parent().position().top + $(event.target.parentNode).parent().height() + 1);
			}
             var data = oTable.fnGetData($tr);
             $('.user_edit_wrapper>.user_actions .view_draft, .user_edit_wrapper>.user_actions .properties').addClass('inactiveButton');
             if(data.draft != null || $(this).closest('.welcome_table').length){
                 $('.user_edit_wrapper>.user_actions .view_draft').removeClass('inactiveButton');
             }
             if(data.workflow.editing_state != "Approved"){
                  $('.user_edit_wrapper>.user_actions .properties').removeClass('inactiveButton');
             }
             $('.access_overlay').show();
             if(data.version == 0 && data.is_visible == 1 && data.workflow.editing_state != "Approved"){
                 $('.access_overlay').hide();
             }
			$('.user_edit_wrapper').show();
            if($('#revision_edit_wrapper').is(":visible")){
                closeRevisionsRowEditor();
                $('#revision_edit_wrapper').hide();
            }
			active_inactive();
			//alert($('#users_wrapper').position().top)
		}
	});
    $("#pages_revision tbody").click(function (event) {
        if (!$(event.target).hasClass('dataTables_empty')) {
            closeRevisionsRowEditor();
            var $tr = $(event.target.parentNode);
            if ($(event.target.parentNode).prop("tagName") != "TD") {
                $(event.target.parentNode).addClass('row_selected');
            } else {
                tr = (event.target.parentNode).parent();
                $(event.target.parentNode).parent().addClass('row_selected');
            }
            var data = $('#pages_revision').dataTable().fnGetData($tr[0]);
            $("#revision_edit_wrapper .user_actions .edit").removeClass('inactiveButton');
            $("#revision_edit_wrapper .user_actions .properties").removeClass('inactiveButton');
            $("#revision_edit_wrapper .user_actions .update_alert").addClass('inactiveButton');
            $("#revision_edit_wrapper .user_actions .unpublish").addClass('inactiveButton');
            $("#revision_edit_wrapper .user_actions .upload_pdf").addClass('inactiveButton');
            $("#revision_edit_wrapper .user_actions .get_pdf").addClass('inactiveButton');
            if(data.active){
                $("#revision_edit_wrapper .user_actions .update_alert").removeClass('inactiveButton');
                $("#revision_edit_wrapper .user_actions .unpublish").removeClass('inactiveButton');
                if(data.overview == 0 && data.template!='downloads') {
                    $("#revision_edit_wrapper .user_actions .upload_pdf").removeClass('inactiveButton');
                    $("#revision_edit_wrapper .user_actions .get_pdf").removeClass('inactiveButton');
                }
            }
            if (data.workflow.editing_state == "Approved") {
                $("#revision_edit_wrapper .user_actions .edit").addClass('inactiveButton');
                $("#revision_edit_wrapper .user_actions .properties").addClass('inactiveButton');
            }
            if(data.is_visible){
                $('#revision_edit_wrapper .user_actions .nav_box.remove').attr('confirm',js_localize['pages.remove.zero']);
            }else{
                $('#revision_edit_wrapper .user_actions .nav_box.remove').attr('confirm',js_localize['pages.remove.confirm']);
            }
            $("#revision_edit_wrapper .user_actions .publish").addClass('inactiveButton');
            if (data.workflow.editing_state == "Approved" && data.active == 0) {
                var allData = $('#pages_revision').dataTable().fnGetData();
                var canActivate = true;
                allData.forEach(function (value, index) {
                    if (value.u_id != data.u_id) {
                        if (value.workflow.editing_state == "Approved" && value.version > data.version) {
                            canActivate = false;
                            return false;
                        }
                    }
                });
                if (canActivate)
                    $("#revision_edit_wrapper .user_actions .publish").removeClass('inactiveButton');
            }
            $('.access_overlay').show();
             if(data.version == 0 && data.is_visible == 1 && data.workflow.editing_state != "Approved"){
                 $('.access_overlay').hide();
             }
            $('.user_actions').css('top', $(event.target.parentNode).position().top + $(event.target.parentNode).height() + 1);
            $('.user_actions').show();
        }
    });
	$('body').click(function(event) {
		if ($(event.target).parents('tbody').length == 0 && !($(event.target).hasClass('paginate_button') && $('#revision_edit_wrapper').is(":visible"))&& $(event.target).parents('.user_edit_wrapper').length == 0 && $('#popup_overlay').length == 0 && $("#ui-datepicker-div:hover").length == 0) {
			//alert($(event.target).parents('#ui-datepicker-div').length);
			closeRowEditor();
		}
        if(!$('#pdf_upload_wrapper').is(":visible") && $(event.target).parents('#revision_edit_wrapper').length > 0 && !$(event.target).hasClass('user_actions') && $(event.target).parents('#pages_revision').length == 0 && !$(event.target).hasClass('nav_box')&&$(event.target).parents('#wrapper_form_update_alert').length == 0){
            closeRevisionsRowEditor();
        }
		if (event.target.parentNode) {
			if (!(event.target.parentNode.className == "admin_data" || event.target.parentNode.id == "admin_info" || event.target.parentNode.id == "admin_action")) {
				if ($('#admin_action').is(":visible"))
				{
					$("#admin_info").removeClass('active');
					$('#admin_action').hide();
				}
			}
		}
	});

	bindRowEditButtons();
});
function deactivate_revision_row(selectedRow, isActive) {
    var table = $('#pages_revision').dataTable();
    var allData = table.fnGetData();
    allData.forEach(function (value, index) {
        if (value.u_id != selectedRow.u_id && (value.active == 1 || value.is_visible == 1)) {
            value['active'] = 0;
            value['is_visible'] = 0;
            var i = table.fnSettings().aiDisplay[index];
            var td = $('#pages_revision tr:eq(' + (i + 1) + ') td:eq(3)');
            var text = td.html().replace(js_localize['pages.label.active'], js_localize['pages.label.inactive']);
            td.parent().removeClass('active_row');
            td.html(text);
        } else if (value.u_id == selectedRow.u_id) {
            if (isActive)
                value['active'] = 1;
            else
                value['active'] = 0;
            value['is_visible'] = 1;
        }
    });
    $('#pages_revision').dataTable().fnDraw();
}
function active_inactive(refresh){
	$('.user_edit_wrapper>.user_actions .nav_box.white.publish').removeClass("inactiveButton");
	$('.user_edit_wrapper>.user_actions .nav_box.white.unpublish').removeClass("inactiveButton");
	var anSelected = fnGetSelected(oTable);
	var pub_unpub = $(anSelected).find('.publish_unpublish').html();
	if(pub_unpub.indexOf(js_localize['pages.label.active']) != -1 || pub_unpub.indexOf(js_localize['pages.label.active'] + "<") != -1){
		$('.user_edit_wrapper>.user_actions .nav_box.white.publish').addClass("inactiveButton");
		$('.user_edit_wrapper>.user_actions .nav_box.white.publish').css("pointer-events", "none");
		$('.user_edit_wrapper>.user_actions .nav_box.white.unpublish').css("pointer-events", "auto");
	}else{
		$('.user_edit_wrapper>.user_actions .nav_box.white.unpublish').addClass("inactiveButton");
		$('.user_edit_wrapper>.user_actions .nav_box.white.unpublish').css("pointer-events", "none");
		$('.user_edit_wrapper>.user_actions .nav_box.white.publish').css("pointer-events", "auto");
        if(refresh != undefined){
            var selectedData = oTable.fnGetData(anSelected[0]);
            selectedData.active = 0;
            var $parent =  $("#page_" + selectedData.parent_id +" a");
            if(selectedData.version != 0 && $parent.lenght > 0){
                $parent.click();
                $parent.click();
            }
        }
	}
};

function bindRowEditButtons() {
	/* Add a click handler for the delete row */
    $('.user_actions .update_alert').click(function(event){
        $('#wrapper_form_update_alert').openDialog($('div#update_alert_wrapper .textarea_overlay'));
        $('#update_alert_wrapper').css('top', ($(this).parent().position().top - 1) + 'px');
        $('#update_alert_wrapper').show();
        $('body').css('cursor', 'default');
        $('a').css('cursor', 'pointer');
        $(this).addClass('active');
        return false;
	});
    $('#update_alert_wrapper .user_fields .nav_box.cancel').click(function() {
		$('#wrapper_form_update_alert').closeDialog();
        $('#update_alert_form textarea').val('');
		$('#update_alert_wrapper').hide();
		$('#user_form .input_group').removeClass('error');
		$('#user_form .role_group').removeClass('error');
		$('.left_navigation #update_alert').removeClass('active');
		$('#update_alert_wrapper .user_fields').hide();
        return false;
	});
    $('.user_actions .nav_box.remove').click(function() {
        var that = this;
        var isRevision = false;
        var oSettings;
        var dataSelected;
        if($(that).closest('#revision_edit_wrapper').length > 0){
            isRevision = true;
            var anSelected = fnGetSelected($('#pages_revision').dataTable());
            if (anSelected.length !== 0) {
                dataSelected = $('#pages_revision').dataTable().fnGetData(anSelected[0]);
            }
        }else{
            var anSelected = fnGetSelected(oTable);
            if (anSelected.length !== 0) {
                dataSelected = oTable.fnGetData(anSelected[0]);
            }
        }
		if (dataSelected.id != 1) {
			if(dataSelected.active){
				jAlert(js_localize['pages.remove.error'], js_localize['pages.remove.title'], "error");
				return false;
			}
            var msg = $(that).attr('confirm');
			jConfirm(msg, js_localize['pages.remove.title'], "success", function(r) {
                if(isRevision){
                   closeRevisionsRowEditor();
                }else{
                    closeRowEditor();
                }
				if (r) {
					if (anSelected.length !== 0) {
                        if(isRevision){
                            oSettings = $('#pages_revision').dataTable().fnSettings();
                            $('#pages_revision').dataTable().oApi._fnProcessingDisplay(oSettings, true);
                        }else{
                            oSettings = oTable.fnSettings();
                            oTable.oApi._fnProcessingDisplay(oSettings, true);
                        }
						jQuery.ajax({
							type: "DELETE",
							url: relPath + "api/v1/pages/delete/" + dataSelected.u_id,
							success: function(data) {
                                var removedMsg = js_localize['pages.remove.text'];
                                if(!isRevision){
								if ($(anSelected[0]).prev().attr('level') < $(anSelected[0]).attr('level') && ($(anSelected[0]).next().length == 0 || $(anSelected[0]).next().attr('level') < $(anSelected[0]).attr('level'))) {
									$(anSelected[0]).prev().find('a').addClass('childless').html('');
								}
								var searchedRow = oSettings.aoData.filter(function(el) {
									return el['nTr']['id'] == $(anSelected[0]).attr('id');
								});
								var index = searchedRow[0]['nTr']['_DT_RowIndex'] + 1;
								var level = parseInt($(anSelected[0]).attr('level'));
								if (oSettings.aoData.length > index) {
									for (index; index < oSettings.aoData.length; index++) {
										if (level == oSettings.aoData[index]['_aData']['level']) {
											oSettings.aoData[index]._aData.position = parseInt(oSettings.aoData[index]._aData.position) - 1;
											oSettings.aoData[index]._aData.view_pos = parseInt(oSettings.aoData[index]._aData.view_pos) - 1;
										} else if (level > oSettings.aoData[index]['_aData']['level']) {
											break;
										}
									}
								}
								oTable.fnDeleteDataAndDisplay($(anSelected[0]).attr('id'));
								oTable.fnDeleteRow(anSelected[0]);
                            }else{
                                removedMsg = js_localize['pages.remove.text.version'];
                                $('#pages_revision').dataTable().fnDeleteRow(anSelected[0]);
                            }
								jAlert(removedMsg, js_localize['pages.remove.title'], "success");
							},
							error: function(XMLHttpRequest, textStatus, errorThrown) {
								var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
								var message = JSON_ERROR.composeMessageFromJSON(data, true);
								jAlert(message, js_localize['pages.remove.title'], "error");
							},
							complete: function() {
                                if(isRevision){
                                    if(dataSelected.is_visible == 1){
                                        closeRevisionsRowEditor();
                                        closeVersions();
                                    }else{
                                        $('#pages_revision').dataTable().oApi._fnProcessingDisplay(oSettings, false);
                                    }
                                }else{
                                    oTable.oApi._fnProcessingDisplay(oSettings, false);
                                }
							}
						});
					}
				}
			});
		} else {
			jAlert(js_localize['pages.remove.root'], js_localize['pages.remove.title'], "error");
		}
	});

	/* Add a click handler for the delete row */
	$('.user_actions .nav_box.edit').click(function() {
        if ($('#pages_revision').length) {
            var anSelected = fnGetSelected($('#pages_revision').dataTable());
            if (anSelected.length !== 0) {
                var data = $('#pages_revision').dataTable().fnGetData(anSelected[0]);
                if (data.workflow.editing_state != "Approved") {
                    checkPageAvailability(data.u_id, function() {
                        window.location.href = relPath + 'cms/content/' + data.u_id;
                    });
                }
            }
        }
    });
    $('.user_actions .nav_box.view_draft').click(function () {
        var anSelected = fnGetSelected(oTable);
        if (anSelected.length !== 0) {
            var data = oTable.fnGetData(anSelected[0]);
            if($(this).closest('.welcome_table').length) data.draft = data.u_id
            checkPageAvailability(data.draft, function () {
                window.location.href = relPath + 'cms/content/' + data.draft;
            });
        }
    });

    $('.user_actions .nav_box.preview').click(function () {
        var anSelected = fnGetSelected($('#pages_revision').dataTable());
        if (anSelected.length !== 0) {
            var page_id = $('#pages_revision').dataTable().fnGetData(anSelected[0]).u_id;
            var href = relPath + 'preview/' + page_id;
            var link = $('<a href="' + href + '" />');
            link.attr('target', '_blank');
            window.open(link.attr('href'));
        }
    });

    $('.user_actions .nav_box.revisions').click(function () {
        has_new_version = false;
        var self = this;
        var anSelected = fnGetSelected(oTable);
        if (anSelected.length !== 0) {
            $('body').css('cursor', 'progress');
            $('a').css('cursor', 'progress');
            var page_id = oTable.fnGetData(anSelected[0]).id;
            $('a').css('cursor', 'progress');
            $(this).addClass('active');
            if(!$.fn.dataTable.fnIsDataTable($('#pages_revision')[0])){
                $('#pages_revision').dataTable({
                    "bProcessing": true,
                    "sAjaxSource": relPath + "api/v1/pages/versions/" + page_id+"?lang=" + lang,
                    "sAjaxDataProp": 'response',
                    "fnServerData": fnServerData,
                    "iDisplayLength": 10,
                    "bFilter": true,
                    "sPaginationType": 'ellipses',
                    "bLengthChange": false,
                    "iShowPages": 20,
                    "bSort": true,
                    "aaSorting": [[1, 'desc']],
                    "oLanguage": {
                        "sEmptyTable": " "},
                    "aoColumns": [
                        {"sType": "string", "mData": "title", "bSearchable": false, "bSortable": false,"sWidth": "420px"},
                        {"sType": "numeric", "mData": "version", "bSearchable": false, "bSortable": false,"sWidth": "10px"},
                        {"sType": "numeric", "mData": "u_id",  "bSearchable": false, "sClass": "page_u_id","bSortable": false,"sWidth": "40px"},
                        {"sType": "string", "mData": function(source) {
                            if (source.active == 1) {
                                var status = js_localize['pages.label.active'];
                                if(source.isNotActive){
                                    status += '<span style="font-size: 18px;">*</span>';
                                }
                                return status+'<br />('+source.workflow.editing_state+')';
                                //return status;
                            } else {
                                //return js_localize['pages.label.inactive']+',<br />'+source.editing_state;
                                return js_localize['pages.label.inactive']+'<br />('+source.workflow.editing_state+')';
                            }
                        }, "bSearchable": false,"bSortable": false, "sClass": "publish_unpublish","sWidth": "70px"},
                        {"sType": "string", "mData": function(source) {
                            if (source.authorization == 1) {
                                return js_localize['pages.label.auth'];
                            } else {
                                return js_localize['pages.label.non_auth'];
                            }
                        }, "bSearchable": false,"bSortable": false,"sWidth": "110px"},
                        {"sType": "string", "mData": "updated_at", "bSearchable": false, "bSortable": false,"sWidth": "130px"},
                    ],
                    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        if (aData.active == 1) {
                            $(nRow).addClass('active_row');
                        }
                    },
                });
            }else{
                 $('#pages_revision').dataTable().fnReloadAjax(relPath + "api/v1/pages/versions/" + page_id+"?lang=" + lang);
            }
            $('#revision_edit_wrapper').openDialog($('div#revision_edit_wrapper .textarea_overlay'));
            $('body').css('cursor', 'default');
            $('a').css('cursor', 'pointer');
            $("#revision_page_id").val(page_id);
            if($('#revision_edit_wrapper .user_actions').is(':visible')){
                closeRevisionsRowEditor();
            }
            $('body').scrollTop($('#revision_edit_wrapper').offset().top);
        }
    });

    $('.user_actions .nav_box.revert').click(function(){
         var anSelected = fnGetSelected($('#pages_revision').dataTable());
        jConfirm(js_localize['pages.version.revert'], js_localize['pages.version.title'],"success", function(r) {
			if (r) {
                if (anSelected.length !== 0) {
                    var page_id = $('#pages_revision').dataTable().fnGetData(anSelected[0]).u_id;
                    jQuery.ajax({
                        type: "POST",
                        url: relPath + "api/v1/pages/versioncopy",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({'id' : page_id, 'lang': lang}),
                        success: function(data) {
                            $('#pages_revision').dataTable().fnAddData(data);
                            has_new_version = true;
                            closeRevisionsRowEditor();
                        },
                        // script call was *not* successful
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            //TODO: Get Language
                            closeRevisionsRowEditor();
                        }
                    });
                }
            }
        });
    });

	$('.user_actions .nav_box.properties').click(function () {
        var self = this;
         if($(this).closest('#revision_edit_wrapper').length > 0){
             var table = $('#pages_revision').dataTable();
             var anSelected = fnGetSelected(table);
         }else{
             var anSelected = fnGetSelected(oTable);
             var table = oTable;
         }
        if (anSelected.length !== 0) {
            var element = table.fnGetData(anSelected[0]);
            $('body').css('cursor', 'progress');
            var page_id = element.u_id;
            checkPageAvailability(page_id, function (time_left) {
				editing_timer.start(
                    time_left,
                    function(){
                        $('.user_fields .nav_box.save').click();
                    },
                    function(){
                        var promise = cancelChanges();
                        promise.done(function(){
                            window.location.href = relPath + 'cms/pages';
                        });
                    },
                    page_id,
                    'page',
                    '',
                    EDIT_MAX_TIME_SECONDS
                );
                $('a').css('cursor', 'progress');
                $(this).addClass('active');
                clearPropertiesFrom();
                populateFormPage(page_id, element, function (data) {
                    if (data) {
                        openPropertiesForm();
                    } else {
                        $(self).removeClass('active');
                    }
                    $('body').css('cursor', 'default');
                    $('a').css('cursor', 'pointer');

					if(time_left > EDIT_TIME_EXTENSION_ALERT_SECONDS){
						jAlert(
                            js_localize['pages.edit.start']
                                .replace('{time_left}', editing_timer.formatTimeInMinutes(time_left))
                                .replace('{alert_minutes}', editing_timer.formatTimeInMinutes(EDIT_TIME_EXTENSION_ALERT_SECONDS)),
                            js_localize['pages.edit.title'],
                            'success',
                            change_workflow_responsible
                        );
                    }
                });

            });
        }
    });

	$('.user_actions .nav_box.add_before').click(function() {
		var selected = fnGetSelected(oTable)[0];
		if ($(selected).find('.page_id').html() != "1") {
			$('#user_form input[name="position"]').val($(selected).attr('pos'));
			$('#user_form input[name="parent_id"]').val($(selected).attr('parent'));
			clearPropertiesFrom();
            $('.access_overlay').hide();
			openPropertiesForm();
			$(this).addClass('active');
			$('#user_form input[name="active"]').attr('disabled','disabled');
		} else {
			jAlert(js_localize['pages.add.root_before'], js_localize['pages.add.title'], "error");
		}
	});

	$('.user_actions .nav_box.add_after').click(function() {
		var selected = fnGetSelected(oTable)[0];
		if ($(selected).find('.page_id').html() != "1") {
			$('#user_form input[name="position"]').val(parseInt($(selected).attr('pos')) + 1);
			$('#user_form input[name="parent_id"]').val($(selected).attr('parent'));
			clearPropertiesFrom();
            $('.access_overlay').hide();
			openPropertiesForm();
			$(this).addClass('active');
			$('#user_form input[name="active"]').attr('disabled','disabled');
		} else {
			jAlert(js_localize['pages.add.root_after'], js_localize['pages.add.title'], "error");
		}
	});

	$('.user_actions .nav_box.add_child').click(function() {
		$('#user_form select').each(function() {
			$(this).val($(this).find("option:first").val());
		});
		$('#user_form input[name="id"]').val('0');
        $('#user_form input[name="u_id"]').val('0');
		var selected = fnGetSelected(oTable)[0];
		$('#user_form input[name="position"]').val(1);
		$('#user_form input[name="parent_id"]').val($(selected).find('.page_id').html());
		clearPropertiesFrom();
        $('.access_overlay').hide();
		openPropertiesForm();
		$(this).addClass('active');
		$('#user_form input[name="active"]').attr('disabled','disabled');
	});

	$('.user_actions .nav_box.move_cursor').click(function(e) {
		startMoving("move");
		e.stopPropagation();
    });

	$('.user_actions .nav_box.copy').click(function(e) {
		startMoving("copy");
		e.stopPropagation();
	});

	$('.user_actions .nav_box.copy_all').click(function(e) {
		startMoving("copyAll");
		e.stopPropagation();
	});

	$('.user_actions .nav_box.move_up').click(function() {
		var selected = fnGetSelected(oTable)[0];
        var data = oTable.fnGetData(selected);
		if (data.id != 1) {
			var result = hasElement(selected, 'up');
			if (!result.hasElement) {
				jAlert(js_localize['pages.edit.move_up_error'], js_localize['pages.edit.title'], "error");
			} else {
				saveNewPosition(result, data, 'up');
			}
		} else {
			jAlert(js_localize['pages.move.root_up'], js_localize['pages.move.title'], "error");
		}
	});

	$('.user_actions .nav_box.move_down').click(function() {
		var selected = fnGetSelected(oTable)[0];
        var data = oTable.fnGetData(selected);
		if (data.id != 1) {
			var result = hasElement(selected, 'down');
			if (!result.hasElement) {
				jAlert(js_localize['pages.edit.move_down_error'], js_localize['pages.edit.title'], "error");
			} else {
				saveNewPosition(result, data, 'down');
			}
		} else {
			jAlert(js_localize['pages.move.root_down'], js_localize['pages.move.title'], "error");
		}
	});

	$('.user_fields .nav_box.cancel').click(function() {
		cancelChanges();
	});
    $('#pdf_upload_form').fileupload({
        autoUpload: false,
        type: 'POST',
        singleFileUploads: true,
     });

    $("#pdf_upload_wrapper .user_fields .nav_box.save").click(function(ev){
        ev.preventDefault();
        ev.stopPropagation();
        ev.stopImmediatePropagation();
        var anSelected = fnGetSelected($('#pages_revision').dataTable());
        var filesList = $('#file_input').prop('files');
        if (anSelected.length !== 0 && filesList.length > 0) {
            var dataSelected = $('#pages_revision').dataTable().fnGetData(anSelected[0]);
            var page_id = dataSelected.u_id;
            var urlPath = relPath + 'api/v1/files/pdfupload?u_id='+ page_id;

            $('#pdf_upload_form').fileupload('enable');

            $('#pdf_upload_form').fileupload('option',{
                url: urlPath
            });

            $('#pdf_upload_form').fileupload('send', {files: filesList})
                    .done(function (result, textStatus, jqXHR) {
                        if(result.message)
                            jAlert(result.message, js_localize['file.save.title'], "success");
                       $("#pdf_upload_wrapper .cancel").trigger('click');
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.responseText) {
                            var data = JSON_ERROR.errorResponse2JSON(jqXHR);
                            var message = JSON_ERROR.composeMessageFromJSON(data, true);

                        }else{
                            var message = errorThrown;
                        }
                         jAlert(message, js_localize['file.save.title'], "error");
                    });
    }
           return false;
    });

	$('.user_fields .nav_box.save').click(function() {
        if($('#revision_edit_wrapper').is(":visible")){
            $('body').css('cursor', 'progress');
            var anSelected = fnGetSelected($('#pages_revision').dataTable());
			var element = $('#pages_revision').dataTable().fnGetData(anSelected[0]);
			//Workaraound: Sometimes element is array. We will get first element
			if (element instanceof Array) {
				element = element[0];
			}
            var promise = savePageProperties(updateRevisionRow, "PUT", anSelected);
            promise.done(function(){
                editing_timer.stopTimer(false);
                unlockItem(element.u_id, 'page');
                $('body').scrollTop($('#revision_edit_wrapper').offset().top);
                $('.user_table').css('height', '');
                $('.dataTables_paginate.paging_ellipses').show();
                $("#revision_edit_wrapper .textarea_overlay").show();
            });
        }else{
            $('body').css('cursor', 'progress');
            var anSelected = fnGetSelected(oTable);
            var pageID = $('#user_form input[name="u_id"]').val();
            if (pageID != "" && pageID > 0) {
                var promise = savePageProperties(updateTableRow, "PUT", anSelected);
                promise.done(function(){
                    editing_timer.stopTimer(false);
                    unlockItem(pageID, 'page');
                    $('#wrapper_form_page').closeDialog();
                    $('.user_actions .nav_box').removeClass('active');
                    $('.user_edit_wrapper .user_fields').hide();
                    $('.user_table').css('height', '');
                    $('.dataTables_paginate.paging_ellipses').show();
                });
            } else {
                var promise = savePageProperties(addNewTableRow, "POST", anSelected);
                promise.done(function(){
                    $('#wrapper_form_page').closeDialog();
                    $('.user_actions .nav_box').removeClass('active');
                    $('.user_edit_wrapper .user_fields').hide();
                    $('.user_table').css('height', '');
                    $('.dataTables_paginate.paging_ellipses').show();
                });
            }
            active_inactive();
        }

	});

	$('.user_actions .nav_box.publish').click(function () {
        var self = this;
        jConfirm(js_localize['pages.version.activate'], js_localize['pages.version.title'], "success", function (r) {
            if (r) {
                $('body').css('cursor', 'progress');
                if ($(self).closest('#revision_edit_wrapper').length > 0) {
                    var anSelected = fnGetSelected($('#pages_revision').dataTable());
                    if (anSelected.length !== 0) {
                        var element = $('#pages_revision').dataTable().fnGetData(anSelected[0]);
                        has_new_version = true;
                    }
                } else {
                    var anSelected = fnGetSelected(oTable);
                    if (anSelected.length !== 0) {
                        var element = oTable.fnGetData(anSelected[0]);
                    }
                }

                var data = {'u_id': element.u_id, 'active': 1};
                activate_deactivate(data, anSelected);
            }
        });
    });

	$('.user_actions .nav_box.unpublish').click(function() {
		$('body').css('cursor', 'progress');
        var self = this;
        if ($(self).closest('#revision_edit_wrapper').length > 0) {
            var anSelected = fnGetSelected($('#pages_revision').dataTable());
            if (anSelected.length !== 0) {
                var data = $('#pages_revision').dataTable().fnGetData(anSelected[0]);
                has_new_version = true;
            }
        } else {
            var anSelected = fnGetSelected(oTable);
            if (anSelected.length !== 0) {
                var data = oTable.fnGetData(anSelected[0]);
            }
        }
        var data = {'u_id': data.u_id, 'active': 0};
        activate_deactivate(data, anSelected);
	});

	function activate_deactivate(data, anSelected){
		jQuery.ajax({
			type: "PUT",
			url: relPath + "api/v1/pages/update",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(data),
			success: function(returnData) {
				if(data['active'] == 1){
					if(returnData.response['isNotActive'] == 1)
						$(anSelected).find('.publish_unpublish').html(
							js_localize['pages.label.active'] + '<span style="font-size: 18px;">*</span><br />('+returnData.response.workflow.editing_state+')');
					else
						$(anSelected).find('.publish_unpublish').html(js_localize['pages.label.active']+'<br />('+returnData.response.workflow.editing_state+')');
                    if(returnData.message && returnData.message.length > 100){
                        jAlert(returnData.message, js_localize['pages.edit.title'], "success");
                    }else{
                        jAlert(js_localize['pages.edit.activate_success'], js_localize['pages.edit.title'], "success");
                    }
				} else {
					$(anSelected).find('.publish_unpublish').html(js_localize['pages.label.inactive']+'<br />('+returnData.response.workflow.editing_state+')');
					jAlert(js_localize['pages.edit.deactivate_success'], js_localize['pages.edit.title'], "success");
				}
                if($(anSelected).closest('#revision_edit_wrapper').length == 0){
                    active_inactive(true);
                }else{
                    deactivate_revision_row(data, data['active'] == 1);
                    closeRevisionsRowEditor();
                }
//				oTable.fnReloadAjax( null, function(){
//					if(data['active'] === 1)
//						jAlert(js_localize['pages.edit.activate_success'], js_localize['pages.edit.title'], "success");
//					else
//						jAlert(js_localize['pages.edit.deactivate_success'], js_localize['pages.edit.title'], "success");
//				});
////				updateTableRow(returnData, anSelected);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var returnData = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
				var message = JSON_ERROR.composeMessageFromJSON(returnData, true);
				if (returnData.response !== undefined) {
					$.each(returnData.response, function(key, value) {
						$('input[name="' + key + '"]').parent().addClass('error');
					});
				}
				jAlert(message, js_localize['pages.edit.title'], "error");
			},
			complete: function() {
				$('body').css('cursor', 'default');
                if($(anSelected).closest('#revision_edit_wrapper').length == 0){
                   closeRowEditor();
                }
			}
		});
	}

    $('.revision_table .cancel').click(function () {
        var promise = null;
        var pageID = $('#revision_edit_wrapper input#revision_page_id').val();
//        if (pageID != "" && pageID > 0) {
//            promise = unlockItem(pageID, 'page');
//        } else {
//            promise = $.Deferred().resolve().promise();
//        }
        promise = $.Deferred().resolve().promise();
        promise.always(function () {
            //editing_timer.stopTimer(false);
            closeVersions();
        });

        return promise;
    });

    $('.left_navigation .clear_cache').click(function(){
        jConfirm(js_localize['pages.clear.message'], js_localize['pages.clear.title'], "success", function (r) {
            if (r) {
                jQuery.ajax({
                    type: "POST",
                    url: relPath + "api/v1/pages/clearcache",
                    success: function (data) {
                        jAlert(js_localize['pages.clear.success'], js_localize['pages.clear.title'], "success");
                    }, // success
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                        var message = JSON_ERROR.composeMessageFromJSON(data);
                        jAlert(message, js_localize['pages.clear.title'], "error");
                    }
                });
            }
        });
    });

     $('.user_actions .get_pdf').click(function(ev){
         var anSelected = fnGetSelected($('#pages_revision').dataTable());
            if (anSelected.length !== 0) {
                var data = $('#pages_revision').dataTable().fnGetData(anSelected[0]);
                getPDF(data.u_id,false);
            }

     });

     $('.user_actions .upload_pdf').click(function(ev){
        $('#pdf_upload_wrapper').css('top', ($(this).parent().position().top - 1) + 'px');
        $('#pdf_upload_wrapper').show();
        $('#pdf_upload_wrapper .textarea_overlay').show();
        $('#wrapper_form_pdf_upload').show();
        $('body').css('cursor', 'default');
        $('a').css('cursor', 'pointer');
        $(this).addClass('active');
        return false;
     });

     $('#file_input').on('change', function () {
        $("#label_file_input").val(this.value);
    });
     $('#file_input').on('click', function () {
        $('#pdf_upload_form').fileupload('disable');
    });
    $("#pdf_upload_wrapper .cancel").click(function(){
        $('#pdf_upload_wrapper').hide();
        $('#pdf_upload_wrapper .textarea_overlay').hide();
        $('#label_file_input').val('');
        $('#file_input').val('');
        return false;
    });
}

function closeVersions(){
    $('#revision_edit_wrapper').closeDialog();
    $('.user_actions .nav_box').removeClass('active');
    var allData = $('#pages_revision').dataTable().fnGetData();
    var activeElement = null;
    allData.forEach(function (value, index) {
        if (value.is_visible == 1) {
            activeElement = value;
        }
    });
    var anSelected = fnGetSelected(oTable);
    var selectedData = oTable.fnGetData(anSelected[0]);
    if (activeElement == null || selectedData.u_id != activeElement.u_id || has_new_version || selectedData.authorization != activeElement.authorization) {
        var $parent =  $("#page_" + selectedData.parent_id + " a");
        if($parent.length > 0){
            $parent.click();
            $parent.click();
        }else{
            closeRowEditor();
            oTable.fnReloadAjax(null,null,true);

        }
    }
}
function checkPageAvailability(page_id, callback) {

    jQuery.ajax({
        type: "GET",
        url: relPath + "api/v1/pages/isavailable/" + page_id,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (typeof (callback) === "function") {
                var time_left = 0;
                if(data.response.time_left){
                    time_left = data.response.time_left;
                }
                callback(time_left);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
            var message = JSON_ERROR.composeMessageFromJSON(data, true);
            jAlert(message, js_localize['pages.edit.title'], "error");
            $('body').css('cursor', 'default');
        }
    });
}

function startMoving(action) {
	var selected = fnGetSelected(oTable)[0];
    var data = oTable.fnGetData(selected);
	if (data.id != 1) {
		var $selected = $(selected);
		$selected.addClass('disabled');
		if ($selected.hasClass('expanded')) {
			$selected.find('.strong a').click();
		}
		closeRowEditor();
		move_item = data.u_id;
		var ImgUrl = 'url("../img/backend/page_move_div_icon.png") no-repeat scroll 0 0 white';
		if (action == 'copy') {
			ImgUrl = 'url("../img/backend/copy_move_div_icon.png") no-repeat scroll 0 0 white';
		} else if (action == 'copyAll') {
			ImgUrl = 'url("../img/backend/copy_all_move_div_icon.png") no-repeat scroll 0 0 white';
		}
		$('.page_move_div').css('background', ImgUrl);
		$('.page_move_div').show();
		$('.page_move_div').html($selected.find('.strong span.title').html());
		$(document).bind('mousemove', function(e) {
			$('.page_move_div').css({
				cursor: 'pointer',
				left: e.pageX + 15,
				top: e.pageY - 39
			});
			$('html,body').css('cursor', 'pointer');
		});

		bindClicks(selected, action);
	} else {
		if (action == 'move') {
			jAlert(js_localize['pages.move.root_move'], js_localize['pages.move.title'], "error");
		} else {
			jAlert(js_localize['pages.copy.root_copy'], js_localize['pages.copy.title'], "error");
		}
	}
}

function bindClicks(selected, action) {
	$(document).bind('click', function(e) {
		if ($(selected).length > 0) {
			var elem = e.target;
			if ($(elem).prop('tagName') == "SPAN" || $(elem).prop('tagName') == "A") {
				elem = $(elem).parent();
			}
			if ($(elem).parent().prop("tagName") == "TR" && $(elem).prop('tagName') != "TH") {
				e.preventDefault();
				e.stopPropagation();
				var row = $(elem).parent();
				if (action == 'copy' || action == 'copyAll' || !row.hasClass('disabled')) {
					// Check if operation is in progress
					if (!!selected.started) {
						return;
					}
					selected.started = true;
				
					oSettings = oTable.fnSettings();
					oTable.oApi._fnProcessingDisplay(oSettings, true);
					var move_info = {'u_id': move_item, 'parent_id': row.find('.page_id').html(), position: 1, action: action};
					var url = relPath + "api/v1/pages/update";
					var type = "PUT";
					if (action == 'copy' || action == 'copyAll') {
						url = relPath + "api/v1/pages/copy";
						type = "POST";
					}
					jQuery.ajax({
						type: type,
						url: url,
						contentType: "application/json; charset=utf-8",
						dataType: "json",
						data: JSON.stringify(move_info),
						success: function(data) {
							stopMove(selected);
							var $selected = $(selected);
							if ($selected.prev().attr('level') < $selected.attr('level') && ($selected.next().length == 0 || $selected.next().attr('level') < $selected.attr('level'))) {
								$selected.prev().find('a').addClass('childless').html('');
							}
							if ($(row).find('a').hasClass('childless'))
							{
								$(row).find('a').removeClass('childless').html('-');
							}
							if (!$(row).hasClass('expanded')) {
								if (action == 'move') {
									if ($selected.length > 0)
										oTable.fnDeleteRow(selected);
								}
								$(row).find('.strong a').click();
							} else {
								var page_info = jQuery.extend(data.response, {
									'title': $selected.find('.strong span.title').html(),
									'path': $(row).find('.strong span').html() + $(row).find('.strong .title').html() + "/",
									'view_pos': data.response.position,
									'authorization': $('#user_form input[name="access"]:checked').length > 0 ? 1 : 0
								});
								page_info.level = parseInt($(row).attr('level')) + 1;
								if ($selected.find('a').hasClass('childless')) {
									page_info.childless = true;
								}
								if (action == 'move') {
									if ($selected.length > 0)
										oTable.fnDeleteRow(selected);
								}
								oTable.fnAddDataAndDisplay(page_info);
								if (page_info.childless) {
									$('#page_' + page_info.id).find('a').addClass('childless').html('');
								}
							}
							if (action == 'copy') {
								jAlert(js_localize['pages.edit.copy_success'], js_localize['pages.copy.title'], "success");
							} else if(action == 'copyAll'){
								jAlert(js_localize['pages.edit.copy_multiple_success'], js_localize['pages.copy.multiple.title'], "success");
							}else {
								jAlert(js_localize['pages.edit.move_success'], js_localize['pages.move.title'], "success");
							}

						},
						// script call was *not* successful
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							stopMove(selected);
							var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
							var message = JSON_ERROR.composeMessageFromJSON(data);
							if (data.response !== undefined) {
								$.each(data.response, function(key, value) {
									$('input[name="' + key + '"]').parent().addClass('error');
								});
							}
							$('body').css('cursor', 'default');
							//alert("Error while updating group!");
							jAlert(message, js_localize['pages.edit.title'], "error");
						},
						complete: function(data) {
							oTable.oApi._fnProcessingDisplay(oSettings, false);
						}
					});
				} else {
					jAlert(js_localize['pages.move.error_same'], js_localize['pages.move.title'], "error");
				}
			} else if ($(elem).find('.user_table').length != 0) {
				stopMove(selected);
			}
		}
	});
}

function stopMove(selected) {
	selected.started = false;
	move_item = 0;
	$('.page_move_div').hide();
	$("#pages tbody tr").unbind('click');
	$("#pages tbody tr.disabled").unbind('click');
	$(document).unbind('mousemove');
	$(document).unbind('click');
	$(selected).removeClass('disabled');
	$('.user_table').unbind('click');
	$('html,body').css('cursor', 'default');
}

function closeRowEditor() {
	$('#publish_date').datepicker('hide');
	$('#unpublish_date').datepicker('hide');
	$('.user_table').css('height', '');
	$('.dataTables_paginate.paging_ellipses').show();
	$('#user_form .input_group').removeClass('error');
	$('#user_form .role_group').removeClass('error');
	$('.user_actions .nav_box').removeClass('active');
	$('.user_edit_wrapper .user_fields').hide();
	$('#user_form input:text').val('');
	$('#user_form input[name="id"]').val('0');
    $('#user_form input[name="u_id"]').val('0');
	$(oTable.fnSettings().aoData).each(function() {
		$(this.nTr).removeClass('row_selected');
	});
	$('.user_edit_wrapper').hide();
	$('.textarea_overlay').hide();
	$('body').css('cursor', 'default');
}

/* Get the rows which are currently selected */
function fnGetSelected(oTableLocal) {
	return oTableLocal.$('tr.row_selected');
}

function refreshTable() {
	var search_input = $('.user_search').val() !== js_localize['pages.label.search'] ? $('.user_search').val() : "";
	var sort_input = $('.user_sort').val();
	if (search_input != "") {
		oTable.fnReloadAjax(relPath + "api/v1/pages/search?lang=" + lang + "&filter=" + encodeURIComponent(search_input), function() {
			if (sort_input == -1) {

				oTable.fnSortNeutral();
			} else {
				oTable.fnSort([[sort_input, 'asc']]);
			}
//			if (move_item > 0) {
//				bindClicks($("tr#page_" + move_item));
//			}
		});
		filtered_table = true;
	} else {
		if (filtered_table) {
			oTable.fnReloadAjax(relPath + "api/v1/pages/all?lang=" + lang + "&depth=2&parent_id=0", function() {
				console.log(sort_input);
				if (sort_input == -1) {
					oTable.fnSortNeutral();
				} else {
					oTable.fnSort([[sort_input, 'asc']]);
				}
//				if (move_item > 0) {
//					bindClicks($("tr#page_" + move_item));
//				}
			});
		} else {
			if (sort_input == -1) {
				oTable.fnSortNeutral();
			} else {
				oTable.fnSort([[sort_input, 'asc']]);
			}
//			if (move_item > 0) {
//				bindClicks($("tr#page_" + move_item));
//			}
		}
		filtered_table = false;
	}
	return false;
}

function rowExpand(el, event) {
	if (typeof (event) != "undefined")
		event.stopPropagation();
	if ($('.user_edit_wrapper').is(":visible")) {
		closeRowEditor();
	}
	if ($(el).parent().parent().hasClass('expanded')) {
		if (!$(el).parent().parent().find('a').hasClass('childless')) {
			$(el).parent().parent().removeClass('expanded');
			oTable.fnDeleteDataAndDisplay($(el).parent().parent().attr('id'));
			$(el).html('+');
			if ($(el).parent().parent().hasClass('expanded')) {
				$(el).parent().parent().removeClass('expanded');
			}
			var anSelected = fnGetSelected(oTable);
			if ($('.user_edit_wrapper').is(':visible')) {
				if (anSelected[0])
					$('.user_edit_wrapper').css('top', $('#pages_wrapper').position().top + $(anSelected[0]).position().top + $(anSelected[0]).height() + 1);
				else
					$('.user_edit_wrapper').hide();
				$('.user_table').css('height', '');
				$('.dataTables_paginate.paging_ellipses').show();
			}
		}
	} else {
		var page_id = $(el).parent().parent().find('.page_id').html();
		jQuery.ajax({
			type: "GET",
			url: relPath + "api/v1/pages/all?lang=" + lang + "&depth=1&parent_id=" + page_id,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function(data) {
				var anSelected = fnGetSelected(oTable);
				for (var i = 0; i < data.response.length; i++) {
					data.response[i].level = parseInt($(el).parent().parent().attr('level')) + 1;
					//data.response[i].position = i;
					oTable.fnAddDataAndDisplay(data.response[i], true);
				}
				if ($('.user_edit_wrapper').is(':visible')) {
					$('.user_edit_wrapper').css('top', $('#pages_wrapper').position().top + $(anSelected[0]).position().top + $(anSelected[0]).height() + 1);
				}
				if (data.response.length == 0) {
					$(el).parent().parent().find('a').addClass('childless');
					$(el).html('');
				} else {
					$(el).html('-');
				}
				$(el).parent().parent().addClass('expanded');
//				if (move_item > 0) {
//					bindClicks($("tr#page_" + move_item));
//				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
				var message = JSON_ERROR.composeMessageFromJSON(data, true);
				jAlert(message, js_localize['pages.list.title'], "error");
			}
		});
	}
	return false;
}

function hideTitle(el, lang) {
	if ($(el).prop('checked') == true) {
		$('input[name="title_' + lang + '"]').parent().show();
	} else {
		$('input[name="title_' + lang + '"]').parent().hide();
	}
}

function hasElement(selected, direction) {
	var oSettings = oTable.fnSettings();
	var selectedId = $(selected).attr('id');
	var searchedRow = oSettings.aoData.filter(function(el) {
		return el['nTr']['id'] == selectedId;
	});
	var elementIndex = searchedRow[0]['nTr']['_DT_RowIndex'];
	var hasElement = false;
	var targetElementIndex;
	var target_pos;
	if (direction == 'up') {
		if ($(selected).attr('view_pos') == "1") {
			hasElement = false;
		} else {
			for (var i = elementIndex - 1; i > 0; i--) {
				if (oSettings.aoData[i]['_aData']['parent_id'] == $(selected).attr('parent')) {
					hasElement = true;
					targetElementIndex = oSettings.aoData[i]['nTr']['_DT_RowIndex'];
					target_pos = oSettings.aoData[i]['_aData']['position'];
					i = 0;
				} else if (oSettings.aoData[i]['_aData']['level'] < $(selected).attr('level')) {
					hasElement = false;
					i = 0;
				}
			}
		}
	} else {
		for (var i = elementIndex + 1; i < oSettings.aoData.length; i++) {
			if (oSettings.aoData[i]['_aData']['parent_id'] == $(selected).attr('parent')) {
				hasElement = true;
				targetElementIndex = oSettings.aoData[i]['nTr']['_DT_RowIndex'];
				target_pos = oSettings.aoData[i]['_aData']['position'];
				i = oSettings.aoData.length;
			} else if (oSettings.aoData[i]['_aData']['level'] < $(selected).attr('level')) {
				hasElement = false;
				i = oSettings.aoData.length;
			}
		}
	}

	return {'elementIndex': elementIndex, 'hasElement': hasElement, 'targetElementIndex': targetElementIndex, 'target_pos': target_pos};
}

function updateTableRow(data, anSelected) {
	$('#wrapper_form_page').closeDialog();
	var page_info = jQuery.extend(data.response, {'title': $('input[name="title_' + lang_label + '"]').val(),
		'path': $(anSelected[0]).find('.strong span').html(),
		'authorization': $('#user_form input[name="access"]:checked').length > 0 ? 1 : 0,
		'level': $(anSelected[0]).attr('level'),
		'view_pos': parseInt($(anSelected[0]).attr('view_pos'), 10),
		'childless': $(anSelected).find('a').hasClass('childless')});
	oTable.fnUpdate(page_info, anSelected[0], undefined, false, false);
	if (page_info.parent_id == $(anSelected).find('.page_id').html() && page_info.childless) {
		$(anSelected).find('a').removeClass('childless').html('-');
	} else if (page_info.childless) {
		$(anSelected).find('a').addClass('childless').html('');
	}
	//update case
	if (page_info.id == $(anSelected).find('.page_id').html() && $(anSelected).hasClass('expanded')) {
		if (!page_info.childless && pageTitle.text[lang] != $('input[name="title_' + lang_label + '"]').val()) {
			$(anSelected).removeClass('expanded');
			rowExpand($(anSelected).find('.strong a'));
		}else if($(anSelected).find('a').html() == "+"){
			$(anSelected).find('a').html('-');
		}
	}
	closeRowEditor();
}

function addNewTableRow(data, anSelected) {
	$('#wrapper_form_page').closeDialog();
	if ($('input[name="title_' + lang_label + '"]').val() != '') {
		if ($(anSelected).find('a').hasClass('childless') && $(anSelected[0]).find('.page_id').html() == data.response.parent_id) {
			//we add first child
			$(anSelected).find('a').removeClass('childless').html('-');
		}
		if ($(anSelected[0]).find('.page_id').html() == data.response.parent_id && !$(anSelected[0]).hasClass('expanded')) {
			//we add new child
			$(anSelected[0]).find('.strong a').click();
		} else {
			var page_info = jQuery.extend(data.response, {'title': $('input[name="title_' + lang_label + '"]').val(),
				'path': $(anSelected[0]).find('.strong span').html(),
				'authorization': $('#user_form input[name="access"]:checked').length > 0 ? 1 : 0});
			page_info.position = page_info.position;
			//we have added node as child
			if ($(anSelected[0]).find('.page_id').html() == data.response.parent_id) {
				page_info.level = parseInt($(anSelected[0]).attr('level')) + 1;
				page_info.view_pos = 1;
			} else {
				page_info.level = $(anSelected[0]).attr('level');
				var view_pos = 1;
				var dataPosition = parseInt(data.response.position);
				var oSettings = oTable.fnSettings();
				var childs = oSettings.aoData.filter(function(el) {
					return el['_aData']['parent_id'] == page_info.parent_id;
				});
				childs.sort(function(a, b) {
					return a._aData.position - b._aData.position
				});
				for (var i = 0; i < childs.length; i++) {
					if (childs[i]._aData.position < dataPosition) {
						view_pos = childs[i]._aData.view_pos + 1;
					}
				}
				page_info.view_pos = view_pos;
			}
			oTable.fnAddDataAndDisplay(page_info);
			$('#page_' + page_info.id).find('a').addClass('childless').html('');
		}
	}
	closeRowEditor();
}

function saveNewPosition(result, selected, direction) {
	$('body').css('cursor', 'progress');
	var data = {u_id: selected.u_id, position: result.target_pos};
	jQuery.ajax({
		type: "PUT",
		url: relPath + "api/v1/pages/update",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: JSON.stringify(data),
		success: function(data) {
			oTable.fnSwapData(result.elementIndex, result.targetElementIndex, direction);
			jAlert(js_localize['pages.edit.move_success'], js_localize['pages.edit.title'], "success");
		},
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
			var message = JSON_ERROR.composeMessageFromJSON(data);
			if (data.response !== undefined) {
				$.each(data.response, function(key, value) {
					$('input[name="' + key + '"]').parent().addClass('error');
				});
			}
			$('body').css('cursor', 'default');
			//alert("Error while updating group!");
			jAlert(message, js_localize['pages.edit.title'], "error");
		},
		complete: function() {
			$('body').css('cursor', 'default');
			closeRowEditor();
		}
	});
}

function openPropertiesForm() {
	$('#wrapper_form_page').openDialog($('div.user_edit_wrapper .textarea_overlay'));
	var height = $('.user_edit_wrapper').position().top + $('.user_edit_wrapper').height() + $('.user_fields').height() + 10;
	if ($('.user_table').height() < height) {
		$('.user_table').css('height', height + 'px');
		$('.dataTables_paginate.paging_ellipses').hide();
	}
	$('.user_edit_wrapper .user_fields').show();
}

function cancelChanges() {
    var promise = null;
    var pageID = $('#user_form input[name="u_id"]').val();
    if (pageID != "" && pageID > 0) {
        promise = unlockItem(pageID, 'page');
    } else {
        promise = $.Deferred().resolve().promise();
    }
    promise.always(function () {
        editing_timer.stopTimer(false);
        $('#wrapper_form_page').closeDialog();
        $('.user_actions .nav_box').removeClass('active');
        $('.user_edit_wrapper .user_fields').hide();
        $('.user_table').css('height', '');
        $('.dataTables_paginate.paging_ellipses').show();
        if($("#revision_edit_wrapper").is(":visible"))
            $("#revision_edit_wrapper .textarea_overlay").show();
        active_inactive();
    });

    return promise;
}

function closeRevisionsRowEditor(){
    $('.dataTables_paginate.paging_ellipses').show();
	$($('#pages_revision').dataTable().fnSettings().aoData).each(function() {
		$(this.nTr).removeClass('row_selected');
	});
	$('#revision_edit_wrapper .user_actions').hide();
	$('body').css('cursor', 'default');
}

function updateRevisionRow(data, anSelected){
        var page  = data.response;
    page.title = $('input[name="title_' + (lang == 1? 'en':'de') + '"]').val();
    if(page.authorization == 1 && anSelected.find('td:eq(4)').html() == js_localize['pages.label.non_auth']){
        anSelected.parent().find('tr td:nth-of-type(5)').html(js_localize['pages.label.auth']);
    }else if(page.authorization == 0 && anSelected.find('td:eq(4)').html() == js_localize['pages.label.auth']){
        anSelected.parent().find('tr td:nth-of-type(5)').html(js_localize['pages.label.non_auth']);
    }
    $('#wrapper_form_page').closeDialog();
    $('#wrapper_form_page').closeDialog();
    $("#revision_edit_wrapper .textarea_overlay").show();
    $('#pages_revision').dataTable().fnUpdate(page, anSelected[0], undefined, false, false);
}

function sendMailNotification(event) {
    event.stopPropagation();
    event.preventDefault();
    var anSelected = fnGetSelected($('#pages_revision').dataTable());
    if (anSelected.length !== 0) {
        var dataSelected = $('#pages_revision').dataTable().fnGetData(anSelected[0]);
        var page_id = dataSelected.u_id;
        var data = {comments_en: $('#upd_al_en').val(), comments_de: $('#upd_al_de').val()};
        var notificationCounts = 0
	jQuery.ajax({
		type: 'PUT',
		url: relPath + 'api/v1/pages/sendalert/' + page_id,
		dataType: "json",
		data: JSON.stringify(data),
		success: function(data) {
			if (data.response !== undefined) {
				notificationCounts = data.response.count;
			}
			jAlert(js_localize['pages.alerts.success.text'].replace('{0}', notificationCounts), js_localize['pages.alerts.success.title'], "success");
            $('#update_alert_wrapper .user_fields .nav_box.cancel').click();
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
			var message = JSON_ERROR.composeMessageFromJSON(data, true);
			jAlert(message, js_localize['mail.send.title'], "error");
		}
	});
    }
	return false;
}


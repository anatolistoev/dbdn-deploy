var oTable;
//var relPath = getRelativePath();

$(document).ready(function() {

	oTable = $('#pages').dataTable({
		"bProcessing": false,
		"sAjaxSource": relPath+"api/v1/pages/deleted/"+lang,
		"sAjaxDataProp": 'response',
		"fnServerData": fnServerData,
		"iDisplayLength": 20,
		"bFilter": true,
		"sPaginationType": 'ellipses',
		"bLengthChange": false,
		"iShowPages": 10,
		"bSort": false,
		"bProcessing": true,
		"oLanguage": {
			"sEmptyTable": " "},
		"aoColumns": [
			{"sType": "page-title", "mData": function(source) {
					var path = '';
					if (typeof (source.path) != "undefined") {
						path = '<span style="display: block;font-size: 11px;line-height: 12px;">' + source.path + '</span>';
					}
					return path + '<a style="display:none;" href="">+</a><span class="title">' + source.title + '</span>';
				}, "sWidth": "470px", "sClass": "strong", "bSearchable": true},
			{"sType": "numeric", "mData": "id", "sWidth": "40px", "bSearchable": false, "sClass": "page_id"},
			{"sType": "string", "mData": "type", "sWidth": "70px", "bSearchable": false},
			{"sType": "numeric", "mData": function(source) {
					if (source.active == 1) {
						return js_localize['pages.label.active'];
					} else {
						return js_localize['pages.label.inactive'];
					}
				}, "sWidth": "60px", "bSearchable": false},
			{"sType": "numeric", "mData": function(source) {
					if (source.authorization == 1) {
						return js_localize['pages.label.auth'];
					} else {
						return js_localize['pages.label.non_auth'];
					}
				}, "sWidth": "110px", "bSearchable": false},
			{"sType": "string", "mData": "updated_at", "sWidth": "130px", "bSearchable": false},
		],
		"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			$(nRow).attr('id', 'page_' + aData.id);
			$(nRow).attr('pos', aData.position);
			$(nRow).attr('parent', aData.parent_id);
		}
	});

	var oSettings = oTable.fnSettings();
	oSettings.sAlertTitle = js_localize['recycle_bin.list.title'];

	$("#pages tbody").click(function(event) {
		if (!$(event.target).hasClass('dataTables_empty')){
			closeProfile();
			$(oTable.fnSettings().aoData).each(function() {
				$(this.nTr).removeClass('row_selected');
			});
			if ($(event.target.parentNode).prop("tagName") != "TD") {
				$(event.target.parentNode).addClass('row_selected');
				$('.user_edit_wrapper').css('top', $('#pages_wrapper').position().top + $(event.target.parentNode).position().top + $(event.target.parentNode).height() + 1);
			} else {
				$(event.target.parentNode).parent().addClass('row_selected');
				$('.user_edit_wrapper').css('top', $('#pages_wrapper').position().top + $(event.target.parentNode).parent().position().top + $(event.target.parentNode).parent().height() + 1);
			}
			$('.user_edit_wrapper').show();
		}
	});
	
   	$('body').click(function(event) {
		if($(event.target).parents('tbody').length == 0 && $(event.target).parents('.user_edit_wrapper').length == 0 && $('#popup_overlay').length == 0){
			closeProfile();
		}
		if (event.target.parentNode) {
			if (!(event.target.parentNode.className == "admin_data" || event.target.parentNode.id == "admin_info" || event.target.parentNode.id == "admin_action")) {
				if ($('#admin_action').is(":visible"))
				{
					$("#admin_info").removeClass('active');
					$('#admin_action').hide();
				}
			}
		}
	});

	$('.user_actions .nav_box.restore').click(function() {
		$('body').css('cursor', 'progress');
		var anSelected = fnGetSelected(oTable);
		closeProfile();
        var selectedData = oTable.fnGetData(anSelected[0]);
		var data = {'id': selectedData.u_id};
		jQuery.ajax({
			type: "PUT",
			url: relPath+"api/v1/pages/restore",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(data),		
			success: function(data) {			
				jAlert(js_localize['pages.restore.success'], js_localize['pages.restore.title'], "success");
				//oTable.fnDeleteDataAndDisplay($(anSelected[0]).attr('id'));
				oTable.fnDeleteRow(anSelected[0]);
                closeProfile();
//				oTable.fnReloadAjax( null, function(){
//					closeProfile();
//				});
			},
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
				var message = JSON_ERROR.composeMessageFromJSON(data);
				if(data.response !== undefined){
					$.each(data.response, function(key, value) {
						$('input[name="' + key + '"]').parent().addClass('error');
					});
				}
				$('body').css('cursor', 'default');
				jAlert(message, js_localize['pages.restore.title'], "error");
			}
		});
	});
    	$('.user_actions .nav_box.remove').click(function() {
		jConfirm($(this).attr('confirm'), js_localize['pages.remove.title'],"success", function(r) {
			if (r) {
				var anSelected = fnGetSelected(oTable);
                var selectedData = oTable.fnGetData(anSelected[0]);
				closeProfile();
				if (anSelected.length !== 0) {
					jQuery.ajax({
						type: "DELETE",
						url: relPath+"api/v1/pages/permanentdelete/" + selectedData.u_id,
						success: function(data) {
                            if(data.response.length > 1){
                                for (var i = 0;  i < data.response.length; i++) {
                                    oTable.fnDeleteRow($('#pages #page_' + data.response[i].id)[0],null, false);                                   
                                }
                                oTable.fnDraw();
                            }else{
                                oTable.fnDeleteRow(anSelected[0]);
                            }
							jAlert(js_localize['pages.remove.text'].replace('{0}',data.response.subject),js_localize['pages.remove.title'],"success");
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
							var message = JSON_ERROR.composeMessageFromJSON(data, true);
							jAlert(message, js_localize['pages.remove.title'], "error");
						},
					});
				}
			}
		});	
	});

});

function closeProfile() {
	$('.user_actions .nav_box').removeClass('active');
	$('.user_edit_wrapper .user_fields').hide();
	$(oTable.fnSettings().aoData).each(function() {
		$(this.nTr).removeClass('row_selected');
	});
	$('.user_edit_wrapper').hide();
	$('body').css('cursor', 'default');
}

function refreshTable() {
	var search_input = $('.user_search').val() !== js_localize['pages.label.search'] ? $('.user_search').val() : "";
	var sort_input = $('.user_sort').val() == -1 ? 1 : $('.user_sort').val();
	oTable.fnSort([[sort_input, 'asc']]);
	oTable.fnFilter(search_input);
	return false;
}

/* Get the rows which are currently selected */
function fnGetSelected(oTableLocal) {
	return oTableLocal.$('tr.row_selected');
}

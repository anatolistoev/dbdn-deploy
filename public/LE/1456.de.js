{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1456/L7/banner_DE.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '750px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
					//background: '#e8e9ea url(img/LE/sceneBG/7-8_teaserpic_newsletter_960X150_DE.jpg) no-repeat'
				}
			},
			css: {
				position: 'absolute',
				height: '730px',
				width: '480px',
				background: 'url(LE/1456/L7/0.png) 0 50px no-repeat'
				//background: 'url(img/LE/LE7/09.png) 0 50px no-repeat'
			},
			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',
			leftExpandButton: false,
			tools: {
				1: {
					title: 'Gestaltungsfl&#228;che und Rahmen',
					info: 'Die Gestaltungsfl&#228;che ist gr&#246;&#223;er als der Satzspiegel und begrenzt die maximale Ausdehnung der Bilder und Farbfl&#228;chen. Wird die Gestaltungsfl&#228;che mit einem Bildmotiv ausgefu&#776;llt, entsteht so automatisch ein wei&#223;er Rahmen als Begrenzung.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1456/L7/1.png)'
					}
				},
				2: {
					title: 'Satzspiegel',
					info: 'Schrift wird nur innerhalb des Satzspiegels gesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1456/L7/2.png)'
					}
				},
				3: false,
				4: false,
				5: false,
				6: false
			},
			layers: [
				{
					title: 'Titelkopf',
					info: 'Der Name des Newsletters steht immer auf Wei&#223;. Er besteht aus zwei Begriffen und wird je nach L&#228;nge des\
Namens aus Schriftgr&#246;&#223;en zwischen 40 Pt. und 54 Pt. i.d.R. in einer Zeile gesetzt. Der Name wird aus einer\
Kombination der Corporate S Extrabold mit der Corporate Regular dargestellt. Je nach Betonung wird der\
wichtigere Teil des Namens in Versalbuchstaben aus der Corporate S Extrabold und die Erg&#228;nzung aus der\
Corporate S Regular wiedergegeben. Der versal geschriebenen Teils des Namens kann zu Beginn oder am Ende\
des Namens stehen. Bei Bedarf kann eine zus&#228;tzliche Zeile aus der Corporate S Bold in 16 Pt. unterhalb des\
Namens in Titanium gesetzt werden. Der Name des Newsletters sollte aus dem Daimler-Standardfarbpaar\
Premium Blue/Lucent Blue gesetzt werden. Alle anderen Farbpaare der Daimler-Farbpalette sind aber auch m&#246;glich. \
Am oberen Rand des Titelkopfes befinden sich die Angaben zum Absender des Newsletters, die Ausgabe-Nummer und das Erscheinungsdatum. \
Diese Angaben werden aus der Corporate S Regular in 9 Pt. gesetzt. Ein grauer Balken in 100% Titanium grenzt den Titelkopf vom Inhalt ab.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L7/3.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '60px',
						top: '55px'
					}
				},{
					title: 'Bilder',
					info: 'Unterhalb des Titelkopfes k&#246;nnen Bilder und Text innerhalb der vorgegebenen Spalten frei gestaltet werden. Zwischen Bildern und Farbfl&#228;chen ist immer ein Abstand von 1 mm einzuhalten.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L7/4.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '375px',
						top: '140px'
					}
				},{
					title: 'Editorial',
					info: 'Um das Editorial gesondert hervorzuheben, kann dieses in Form einer Infobox in Titanium, Platinum oder einer Fl&#228;chenfarbe gestaltet werden.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L7/5.png)'
					},
					contents: '',
					//alwaysVisible: true,
					arrow: {
						'class':'down',
						left: '375px',
						top: '355px'
					}
				},{
					title: '&#220;berschriften und Einleitungen',
					info: '&#220;berschriften und Einleitungen k&#246;nnen mit den Farbpaaren gestaltet werden. Um eine durchg&#228;ngige und harmonische Gestaltung zu erzeugen, sollte innerhalb eines Newsletters immer nur ein Farbpaar eingesetzt werden.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L7/6.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '140px',
						top: '425px'
					}
				},{
					title: 'Flie&#223;text',
					info: 'Flie&#223;texte sind in Schwarz in der Corporate S Regular zu setzen. Einfu&#776;hrungstexte und Zwischenu&#776;berschriften werden in einer Fl&#228;chenfarbe in der Corporate S Bold gesetzt. Hervorhebungen sind in der entsprechenden Akzentfarbe m&#246;glich.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L7/7.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '180px',
						top: '425px'
					}
				}
			] // layers 1
		},
		{ // app 2 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1456/L8/banner_DE.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '750px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '730px',
				width: '480px',
				background: 'url(LE/1456/L8/0.png) 0 50px no-repeat'
			},
			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',
			leftExpandButton: false,
			tools: {
				1: {
					title: 'Gestaltungsfl&#228;che und Rahmen',
					info: 'Die Gestaltungsfl&#228;che ist gr&#246;&#223;er als der Satzspiegel und begrenzt die maximale Ausdehnung der Bilder und Farbfl&#228;chen. Wird die Gestaltungsfl&#228;che mit einem Bildmotiv ausgefu&#776;llt, entsteht so automatisch ein wei&#223;er Rahmen als Begrenzung.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1456/L8/1.png)'
					}
				},
				2: {
					title: 'Satzspiegel',
					info: 'Schrift wird nur innerhalb des Satzspiegels gesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1456/L8/2.png)'
					}
				},
				3: false,
				4: false,
				5: false,
				6: false
			},
			layers: [
				{
					title: 'Kolumnentitel',
					info: 'Der Kolumnentitel auf den Folgeseiten beinhaltet den Newsletter-Titel, eventuell Absender und/oder Gu&#776;ltigkeitsbereich sowie Ausgabe und Erscheinungsdatum.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L8/3.png)'
					},
					contents: '',
					arrow: {
						'class':'left',
						left: '114px',
						top: '65px'
					}
				},{
					title: 'Bild',
					info: 'Unterhalb des Kolumnentitels k&#246;nnen Bilder innerhalb der vorgegebenen Spalten individuell eingesetzt werden.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L8/4.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '75px',
						top: '250px'
					}
				},{
					title: 'Bildunterschrift',
					info: 'Bildunterschriften werden in der Regel neben oder unter dem Bild platziert. Der Text wird aus der Corporate S Bold in 7,5 Pt. mit einem Zeilenabstand von 3 mm gesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L8/5.png)'
					},
					contents: '',
					//alwaysVisible: true,
					arrow: {
						'class':'left',
						left: '230px',
						top: '449px'
					}
				},{
					title: '&#220;berschrift',
					info: '&#220;berschriften und Einleitungen k&#246;nnen mit den Farbpaaren gestaltet werden. Um eine durchg&#228;ngige und harmonische Gestaltung zu erzeugen, sollte innerhalb eines Newsletters immer nur ein Farbpaar eingesetzt werden.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L8/6.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '205px',
						top: '143px'
					}
				},{
					title: 'Flie&#223;texte',
					info: 'Auf wei&#223;en und hellen Fl&#228;chen steht der Flie&#223;text immer in Schwarz aus der Corporate S Regular. Headlines werden immer linksbu&#776;ndig angeordnet, der Flie&#223;text wird als linksbu&#776;ndiger Flattersatz gesetzt. Die Schriftgr&#246;&#223;e fu&#776;r Flie&#223;texte ist 9 Pt. und fu&#776;r &#220;berschriften stehen die Gr&#246;&#223;en 24 Pt. bzw. 18 Pt. zur Verfu&#776;gung.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L8/7.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '370px',
						top: '255px'
					}
				},{
					title: 'Trennlinie',
					info: 'Befinden sich mehrere Themen auf einer Seite des Newsletters, so k&#246;nnen diese durch eine 1 Pt. starke schwarze Linie voneinander getrennt werden.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L8/8.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '370px',
						top: '450px'
					}
				}
			] // layers 2
		} // app 2
	] // apps
}
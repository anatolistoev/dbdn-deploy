{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1022/L13/banner_EN.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '430px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '410px',
				width: '480px',
				background: 'url(LE/1022/L13/0.png) 0 50px no-repeat'
			},
			expandText:'Expand',
			collapseText:'',
			leftExpandButton: true,
			tools: {
				1: false,
				2: false,
				3: false,
				4: false,
				5: false,
				6: false
			},
			layers: [
				{
					title: 'Corporate Logotype',
					info: 'The corporate logotype is justified left, upper margin, on the text slides. Its size and position is defined and preset in the available templates. It is separated by the slide content using a line.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '360px',
						'backgroundImage': 'url(LE/1022/L13/1.png)'
					},
					contents: '',
					arrow: {
						'class':'left',
						left: '112px',
						top: '57px'
					}
				},{
					title: 'Headlines',
					info: 'The headline is set in Corporate S Bold 28 points, in Lucent Blue and is limited to two lines.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '360px',
						'backgroundImage': 'url(LE/1022/L13/2.png)'
					},
					contents: '',
					arrow: {
						'class':'left',
						left: '369px',
						top: '112px'
					}
				},{
					title: 'Texts',
					info: 'All texts within the design area are set in Corporate S Regular; the standard font is 20 points.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '360px',
						'backgroundImage': 'url(LE/1022/L13/3.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '240px',
						top: '215px'
					}
				},{
					title: 'Footer',
					info: 'Beneath the design area a footer has the sender information, the date and the slide number, for instance. It is set in Corporate S Regular, 9 points.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '360px',
						'backgroundImage': 'url(LE/1022/L13/4.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '70px',
						top: '367px'
					}
				},{
					title: 'Optional Marking Row',
					info: 'To indicate the section or mark the chart, an additional right-aligned line can be optionally added above the dividing line.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '360px',
						'backgroundImage': 'url(LE/1022/L13/5.png)'
					},
					contents: '',
					arrow: {
						'class':'right',
						left: '328px',
						top: '57px'
					}
				}
			] // layers 1
		}
	] // apps
}
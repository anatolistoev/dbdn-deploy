{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1456/L7/banner_EN.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '750px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '730px',
				width: '480px',
				background: 'url(LE/1456/L7/0.png) 0 50px no-repeat'
			},
			expandText:'Expand',
			collapseText:'Collapse',
			leftExpandButton: false,
			tools: {
				1: {
					title: 'Design Area and Frame',
					info: 'The design area is larger than the text block and establishes the maximum borders for visuals and colored spaces. If the design area is populated with a visual, a white frame is created automatically as a border.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1456/L7/1.png)'
					}
				},
				2: {
					title: 'Type Area',
					info: 'Text is set only within the type area.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1456/L7/2.png)'
					}
				},
				3: false,
				4: false,
				5: false,
				6: false
			},
			layers: [
				{
					title: 'Title',
					info: 'The name of the newsletter is always printed on white. It consists of two terms and, depending on the length of\
the name, is generally set in a single line using a type size between 40 pt. and 54 pt. The name is a combination\
of Corporate S Extrabold and Corporate Regular. Depending on the stress, the more important part of the name\
appears in all capital letters in Corporate S Extrabold and the supplementary part is set in Corporate S Regular.\
The part of the name in all caps may be at the start or the end of the name. If required, an additional line in\
Corporate S Bold, 16 pt., can be placed below the name in Titanium. The name of the newsletter should be set in\
the standard Daimler color pair of Premium Blue/Lucent Blue. However, all other color pairs from the Daimler\
color range may also be used. The upper edge of the header shows the publisher information, the edition\
number and the date of publication. This information is set in Corporate S Regular, 9 pt. A gray bar in 100%\
Titanium separates the header from the content.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L7/3.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '60px',
						top: '55px'
					}
				},{
					title: 'Visuals',
					info: 'Visuals and text can be placed freely within the assigned columns beneath the title. A space of 1 mm must always be maintained between visuals and colored spaces.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L7/4.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '375px',
						top: '140px'
					}
				},{
					title: 'Editorial',
					info: 'To give the editorial prominence, it can be in an info box in Titanium, Platinum or a space color.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L7/5.png)'
					},
					contents: '',
					//alwaysVisible: true,
					arrow: {
						'class':'down',
						left: '375px',
						top: '355px'
					}
				},{
					title: 'Headlines and Intros',
					info: 'Headlines and intros can be designed with the color pairs. To ensure a consistent and harmonious appearance, only one pair of colors should be used within a newsletter.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L7/6.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '140px',
						top: '425px'
					}
				},{
					title: 'Body Copy',
					info: 'Body copy must be set in black, Corporate S Regular. Introductions and subheadlines are set in a space color in Corporate S Bold. Highlighting can be done in the appropriate highlight color.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L7/7.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '180px',
						top: '425px'
					}
				}
			] // layers 1
		},
		{ // app 2 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1456/L8/banner_EN.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '750px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '730px',
				width: '480px',
				background: 'url(LE/1456/L8/0.png) 0 50px no-repeat'
			},
			expandText:'Expand',
			collapseText:'Collapse',
			leftExpandButton: false,
			tools: {
				1: {
					title: 'Design Area and Frame',
					info: 'The design area is larger than the text block and establishes the maximum borders for visuals and colored spaces. If the design area is populated with a visual, a white frame is created automatically as a border.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1456/L8/1.png)'
					}
				},
				2: {
					title: 'Type Area',
					info: 'Text is set only within the type area.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1456/L8/2.png)'
					}
				},
				3: false,
				4: false,
				5: false,
				6: false
			},
			layers: [
				{
					title: 'Column Header',
					info: 'On any subsequent pages, the column header includes the newsletter title, possibly the publisher and/or area of validity, the edition number and date of publication.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L8/3.png)'
					},
					contents: '',
					arrow: {
						'class':'left',
						left: '114px',
						top: '65px'
					}
				},{
					title: 'Visuals',
					info: 'Visuals can be placed in the prescribed columns below the column header.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L8/4.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '75px',
						top: '250px'
					}
				},{
					title: 'Photo Captions',
					info: 'Photo captions are generally placed next to or below the photo. The text is set in Corporate S Bold, 7.5 points, with line spacing of 3 mm.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L8/5.png)'
					},
					contents: '',
					//alwaysVisible: true,
					arrow: {
						'class':'left',
						left: '230px',
						top: '449px'
					}
				},{
					title: 'Headlines',
					info: 'Headlines and intros can be designed with the color pairs. To ensure a consistent and harmonious appearance, only one pair of colors should be used within a newsletter.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L8/6.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '205px',
						top: '143px'
					}
				},{
					title: 'Body Copy',
					info: 'Body copy is always on white and light surfaces, in black Corporate S Regular. Headlines are always justified left; the body copy is set flush left. The font size for body copy is 9 points and headlines can be 24 or 18 points.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L8/7.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '370px',
						top: '255px'
					}
				},{
					title: 'Dividing Line',
					info: 'If a newsletter has multiple topics on one page, they can be separated by a 1-point black line.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1456/L8/8.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '370px',
						top: '450px'
					}
				}
			] // layers 2
		} // app 2
	] // apps
}
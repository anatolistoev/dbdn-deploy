{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 L16 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1434/L16/banner_EN.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '430px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '410px',
				width: '480px',
				background: 'url(LE/1434/L16/0.png) 0 70px no-repeat'
			},
			expandText:'Expand',
			collapseText:'',
			leftExpandButton: false,
			tools: {
				1: {
					title: 'Layout and Baseline Grid',
					info: 'The layout grid for all formats is made up of 11 x 12 mm basic elements that are 4 mm apart from each other horizontally and vertically. The baseline grid runs horizontally across the format in 2-mm increments.',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L16/1.png)',
						display: 'none'
					}
				},
				2: {
					title: 'Headline System',
					info: '',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L16/5.png)'
					}
				},
				3: {
					title: 'Design Area and Frame',
					info: 'The design area is larger than the text block and establishes the maximum borders for visuals and colored spaces. If the design area is populated with a visual, a white frame is created automatically as a border.',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L16/2.png)'
					}
				},
				4: {
					title: 'Body Copy',
					info: '',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L16/6.png)'
					}
				},
				5: {
					title: 'Type Area',
					info: 'Text is set only within the type area.',
					css:{
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L16/3.png)'
					}
				},
				6:{
					title: 'Image Motifs',
					info: '',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L16/7.png)'
					}
				}
			},
			layers: [
				{
					title: 'Table',
					info: 'Corporate S Bold in Black or another accent color lends itself to emphasizing specific table content, lines orcolumns. The font size is 7.5 points. Vertical and horizontal colored spaces can be used to divide and accent. <br><img src="LE/1434/L16/4zoom.png" style="width:79%;height:auto;" >',
					css:{
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1434/L16/4.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '80px',
						top: '116px'
					}
				}
			] // layers 1
		}, // END App 1 (L16)
		{ // app 2 L17 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1434/L17/banner_EN.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '430px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '410px',
				width: '480px',
				background: 'url(LE/1434/L17/0.png) 0 70px no-repeat'
			},
			expandText:'Expand',
			collapseText:'',
			leftExpandButton: true,
			tools: {
				1: {
					title: 'Layout and Baseline Grid',
					info: 'The layout grid for all formats is made up of 11 x 12 mm basic elements that are 4 mm apart from each other horizontally and vertically. The baseline grid runs horizontally across the format in 2-mm increments.',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L17/1.png)',
						display: 'none'
					}
				},
				2: {
					title: 'Headline System',
					info: '',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L17/7.png)'
					}
				},
				3: {
					title: 'Design Area and Frame',
					info: 'The design area is larger than the text block and establishes the maximum borders for visuals and colored spaces. If the design area is populated with a visual, a white frame is created automatically as a border.',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L17/2.png)'
					}
				},
				4: {
					title: 'Body Copy',
					info: '',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L17/8.png)'
					}
				},
				5: {
					title: 'Type Area',
					info: 'Text is set only within the type area.',
					css:{
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L17/3.png)'
					}
				},
				6:{
					title: 'Image Motifs',
					info: '',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L17/9.png)'
					}
				}
			},
			layers: [
				{
					title: 'Pie Chart',
					info: 'Pie charts show parts of a whole. Pie charts are well suited for presenting distributions and shares. The text is set in Corporate S Regular or Corporate S Bold; the font size is 7.5 points. Headlines are 9 points; keys are 7.5 points. <br><img src="LE/1434/L17/4zoom.png" style="width:63%;height:auto;" >',
					css:{
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1434/L17/4.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '80px',
						top: '116px'
					}
				},
				{
					title: 'Bar Chart',
					info: 'Bar charts show changes over time. Horizontal bar charts display rankings very well. Type on dark backgrounds is in white for better readability. Otherwise texts and numbers are to be set in Black. Horizontal lines are 0.25 points, 0.75 points or 1.5 points. <br><img src="LE/1434/L17/5zoom.png" style="width:60%;height:auto;" >',
					css:{
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1434/L17/5.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '180px',
						top: '116px'
					}
				},{
					title: 'Chart Key',
					info: 'Chart keys are placed next to, above or below a diagram and set in Corporate S Bold in 7.5 points.',
					css:{
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1434/L17/6.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '20px',
						top: '201px'
					}
				}
			] // layers 2
		}
	] // apps
}
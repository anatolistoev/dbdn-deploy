{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 L9 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1149/L9/banner_EN.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '750px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '730px',
				width: '480px',
				background: 'url(LE/1149/L9/0.png) 0 50px no-repeat'
			},
			expandText:'Expand',
			collapseText:'',
			leftExpandButton: true,
			tools: {
				1: {
					title: 'Layout and Baseline Grid',
					info: 'The layout grid for all formats is made up of 11 x 12 mm basic elements that are 4 mm apart from each other horizontally and vertically. The baseline grid runs horizontally across the format in 2-mm increments.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L9/1.png)',
						display: 'none'
					}
				},
				2: false,
				3: {
					title: 'Design Area and Frame',
					info: 'The design area is larger than the text block and establishes the maximum borders for visuals and colored spaces. If the design area is populated with a visual, a white frame is created automatically as a border.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L9/2.png)'
					}
				},
				4: {
					title: 'Type Area',
					info: 'Text is set only within the type area.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L9/3.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Visuals',
					info: 'Visuals and colored spaces are have a more direct impact for the reader than text alone. They are set from edge to edge of the design area, and can vary in height.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '679px',
						'backgroundImage': 'url(LE/1149/L9/4.png)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'right',
						left: '60px',
						top: '180px'						
					}
				},{
					title: 'Headline System',
					info: 'If no text box is set on the poster, the headline uses the headline system. A space with the height of at least one capital letter must be maintained from the image or colored space above it. Headline and intro must be in an accent color and the proper space color. The color pair Premium Blue/Lucent Blue dictates the typical Daimler color scheme.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '679px',
						'backgroundImage': 'url(LE/1149/L9/5.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '140px',
						top: '600px'
					}
				},{
					title: 'Corporate Logotype',
					info: 'The corporate logotype is placed at the bottom right within the text block. The size and position of the corporate logotype depends on the poster format.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '679px',
						'backgroundImage': 'url(LE/1149/L9/6.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '371px',
						top: '702px'
					}
				},{
					title: 'Additional Information',
					info: 'Sender information and additional information are justified left on a line with the corporate logotype. They are set in black in Corporate S Regular.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '679px',
						'backgroundImage': 'url(LE/1149/L9/7.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '200px',
						top: '650px'
					}
				}
			] // layers 1
		},{ // app 2 L10 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1149/L10/banner_EN.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '750px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '730px',
				width: '480px',
				background: 'url(LE/1149/L10/0.png) 0 50px no-repeat'
			},
			expandText:'Expand',
			collapseText:'',
			leftExpandButton: false,
			tools: {
				1: {
					title: 'Layout and Baseline Grid',
					info: 'The layout grid for all formats is made up of 11 x 12 mm basic elements that are 4 mm apart from each other horizontally and vertically. The baseline grid runs horizontally across the format in 2-mm increments.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L10/1.png)',
						display: 'none'
					}
				},
				2: false,
				3: {
					title: 'Design Area and Frame',
					info: 'The design area is larger than the text block and establishes the maximum borders for visuals and colored spaces. If the design area is populated with a visual, a white frame is created automatically as a border.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L10/2.png)'
					}
				},
				4: {
					title: 'Type Area',
					info: 'Text is set only within the type area.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L10/3.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Visuals',
					info: 'Visuals and colored spaces have a more direct impact for the reader than text alone. They are set from edge to edge of the design area, and can vary in height.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '679px',
						'backgroundImage': 'url(LE/1149/L10/4.png)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'right',
						left: '60px',
						top: '350px'
					}
				},{
					title: 'Text Box',
					info: 'If a text box is used for poster design, it contains the headline. The text box font is in white, Corporate S Regular, preferably in Lucent Blue and runs to the edge of the sheet. Additional, explanatory information is then placed directly under the image or colored space, in Corporate S Regular in black.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '679px',
						'backgroundImage': 'url(LE/1149/L10/5.png)'
					},
					contents: '',
					arrow: {
						'class':'left',
						left: '365px',
						top: '180px'
					}
				},{
					title: 'Corporate Logotype',
					info: 'The corporate logotype is placed at the bottom right within the type area. The size of the corporate logotype depends on the poster format.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '679px',
						'backgroundImage': 'url(LE/1149/L10/6.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '371px',
						top: '702px'
					}
				},{
					title: 'Body Copy and Additional Information',
					info: 'Sender information and additional information are justified left on a line with the corporate logotype. They are set in black in Corporate S Regular.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '679px',
						'backgroundImage': 'url(LE/1149/L10/7.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '90px',
						top: '590px'
					}
				}
			] // layers 2
		}, // app 2
		{ // app 3 L11 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1149/L11/banner_EN.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '410px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '430px',
				width: '480px',
				background: 'url(LE/1149/L11/0.png) 0 50px no-repeat'
			},
			expandText:'Expand',
			collapseText:'',
			leftExpandButton: true,
			tools: {
				1: {
					title: 'Layout and Baseline Grid',
					info: 'The layout grid for all formats is made up of 11 x 12 mm basic elements that are 4 mm apart from each other horizontally and vertically. The baseline grid runs horizontally across the format in 2-mm increments.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L11/1.png)',
						display: 'none'
					}
				},
				2: false,
				3: {
					title: 'Design Area and Frame',
					info: 'The design area is larger than the text block and establishes the maximum borders for visuals and colored spaces. If the design area is populated with a visual, a white frame is created automatically as a border.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L11/2.png)'
					}
				},
				4: {
					title: 'Type Area',
					info: 'Text is set only within the type area.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L11/3.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Visuals',
					info: 'Visuals and colored spaces have a more direct impact for the reader than text alone. In the case of formats up to DIN A2, the space between multiple visuals is 1 mm. From DIN A 1 upwards it is 2 mm.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1149/L11/4.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '95px',
						top: '206px'
					}
				},{
					title: 'Headline System',
					info: 'The headline system is also applied to information posters. Headline and intro must be two colors \u2012 an accent color and in the proper space color. The Premium Blue/Lucent Blue color pair is typical for Daimler\\\'s corporate design and should be given preference.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1149/L11/5.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '307px',
						top: '256px'
					}
				},{
					title: 'Body Copy',
					info: 'Body copy must be set in black, Corporate S Regular. Introductions and subheadlines are set in a space color in Corporate S Bold.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1149/L11/6.png)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'up',
						left: '240px',
						top: '330px'
					}
				},{
					title: 'Corporate Logotype',
					info: 'The corporate logotype is placed at the bottom right within the text block. The size of the corporate logotype depends on the poster format.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1149/L11/7.png)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'down',
						left: '400px',
						top: '324px'
					}
				}
			] // layers 1
		}
	] // apps
}
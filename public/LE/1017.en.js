{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1017/L14/banner_EN.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '750px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '730px',
				width: '480px',
				background: 'url(LE/1017/L14/0.png) 0 50px no-repeat'
			},
			expandText:'Expand',
			collapseText:'',
			leftExpandButton: true,
			tools: {
				1: false,
				2: false,
				3: false,
				4: false,
				5: false,
				6: false
			},
			layers: [
				{
					title: 'Corporate Logotype',
					info: 'The corporate logotype is 42 mm wide. It is centered 20 mm from the upper edge of the page to the base line of the logotype. For technical reasons, the special color Pantone\xae 2757 is used to print the corporate logotype on business stationery.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1017/L14/1.png)'
					},
					contents: '',
					arrow: {
						'class':'left',
						left: '301px',
						top: '79px'
					}
				},{
					title: 'Reference Line',
					info: 'The position of the reference line in the standard letterhead is based on DIN 676. The reference statement is set in Corporate S Regular, 7.5 points, with line spacing of 8.5 points. Pantone\xae Cool Gray 11 is used for all information. The left margin of the letterhead is used as the binding margin and is 24 mm wide.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1017/L14/2.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '289px',
						top: '272px'
					}
				},{
					title: 'Footer',
					info: 'The mandatory information in the footer is prescribed by Section 80 of Germany\'s Stock Corporation Act (AktG). The distance between the block with address and communication information and the left margin of the page is 151 mm. All information from the footer is set in Corporate S Regular in 7.5 points with line spacing of 8.5 points. Pantone\xae Cool Gray 11 is used for all information.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1017/L14/3.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '110px',
						top: '636px'
					}
				}
			] // layers 1
		}
	] // apps
}
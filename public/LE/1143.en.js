{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1: L3 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1143/L3/banner_EN.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '1030px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '1010px',
				width: '480px',
				background: 'url(LE/1143/L3/0.png) 0 50px no-repeat'
			},
			expandText:'Expand',
			collapseText:'',
			leftExpandButton: false,
			tools: {
				1: {
					title: 'Layout and Baseline Grid',
					info: 'The layout grid for all formats is made up of 11 x 12 mm basic elements that are 4 mm apart from each other horizontally and vertically. The baseline grid runs horizontally across the format in 2-mm increments.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						backgroundImage: 'url(LE/1143/L3/1.png)',
						display: 'none'
					}
				},
				2: {
					title: 'Horizontal Axis',
					info: 'The horizontal layout grid in the upper third of the page is known as the shoulder. It divides the format into an upper and lower shoulder area. On front covers, the area above the optical axis is reserved for the corporate logotype.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						backgroundImage: 'url(LE/1143/L3/2.png)'
					}
				},
				3: {
					title: 'Design Area and Frame',
					info: 'The design area is larger than the text block and establishes the maximum borders for visuals and colored spaces. If the design area is populated with a visual, a white frame is created automatically as a border.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						backgroundImage: 'url(LE/1143/L3/3.png)'
					}
				},
				4: {
					title: 'Type Area',
					info: 'Text is set only within the type area.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						backgroundImage: 'url(LE/1143/L3/4.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Corporate Logotype',
					info: 'For flyers, the logotype is used only on front covers and centered in Daimler Blue \u2013 Pantone\xae 534 \u2013 on white. The size of the logotype is in line with the format. The distance from the upper edge of the format to the base line of the logotype is used for positioning purposes.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						'backgroundImage': 'url(LE/1143/L3/5.png)'
					},
					contents: '',
					arrow: {
						'class':'right',
						left: '116px',
						top: '128px'
					}
				},{
					title: 'Visuals',
					info: 'When using visuals, make sure they comply with the Daimler photo style.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						'backgroundImage': 'url(LE/1143/L3/6.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '80px',
						top: '260px'
					}
				},{
					title: 'Headline System',
					info: 'Headlines are set, justified left, in Corporate S Regular. Texts run across the entire width of the type area. Lucent Blue is used for the headline and Premium Blue for the intro.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						'backgroundImage': 'url(LE/1143/L3/7.png)'
					},
					contents: '',
					//alwaysVisible: true,
					arrow: {
						'class':'down',
						left: '220px',
						top: '510px'
					}
				}
			] // layers 1
		},{ // app 2: L4 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1143/L4/banner_EN.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '550px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '530px',
				width: '480px',
				background: 'url(LE/1143/L4/0.png) 0 50px no-repeat'
			},
			expandText:'Expand',
			collapseText:'',
			leftExpandButton: false,
			tools: {
				1: {
					title: 'Layout and Baseline Grid',
					info: 'The layout grid for all formats is made up of 11 x 12 mm basic elements that are 4 mm apart from each other horizontally and vertically. The baseline grid runs horizontally across the format in 2-mm increments.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1143/L4/1.png)',
						display: 'none'
					}
				},
				2: {
					title: 'Horizontal Axis',
					info: 'The horizontal layout grid in the upper third of the page is known as the shoulder. It divides the format into an upper and lower shoulder area. On front covers, the area above the optical axis is reserved for the corporate logotype.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1143/L4/2.png)'
					}
				},
				3: {
					title: 'Design Area and Frame',
					info: 'The design area is larger than the text block and establishes the maximum borders for visuals and colored spaces. If the design area is populated with a visual, a white frame is created automatically as a border.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1143/L4/3.png)'
					}
				},
				4: {
					title: 'Type Area',
					info: 'Text is set only within the type area.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1143/L4/4.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Visuals',
					info: 'The visuals are placed in alignment with the layout grid. The distance between the visuals is 1 mm.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '480px',
						'backgroundImage': 'url(LE/1143/L4/5.png)'
					},
					contents: '',
					arrow: {
						'class':'right',
						left: '218px',
						top: '128px'
					}
				},{
					title: 'Headline System',
					info: 'Headlines are set, justified left, in Corporate S Regular. Texts run across the entire width of the type area. Lucent Blue is used for the headline and Premium Blue for the intro.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '480px',
						'backgroundImage': 'url(LE/1143/L4/6.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '80px',
						top: '87px'
					}
				},{
					title: 'Body Copy',
					info: 'Text is set only within the type area. Body copy is set in Corporate S Regular, 9 points, with 4-mm line spacing; subheadlines are in Corporate S Bold. Headlines are set apart with the respective accent color.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '480px',
						'backgroundImage': 'url(LE/1143/L4/7.png)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'up',
						left: '150px',
						top: '360px'
					}
				}
			] // layers 2
		}, // app 2
		{ // app 3: L5 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1143/L5/banner_EN.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '1030px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '1010px',
				width: '480px',
				background: 'url(LE/1143/L5/0.png) 0 50px no-repeat'
			},
		
			expandText:'Expand',
			collapseText:'',
			leftExpandButton: false,
			tools: {
				1: {
					title: 'Layout and Baseline Grid',
					info: 'The layout grid for all formats is made up of 11 x 12 mm basic elements that are 4 mm apart from each other horizontally and vertically. The baseline grid runs horizontally across the format in 2-mm increments.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						backgroundImage: 'url(LE/1143/L5/1.png)',
						display: 'none'
					}
				},
				2: {
					title: 'Horizontal Axis',
					info: 'The horizontal layout grid in the upper third of the page is known as the shoulder. It divides the format into an upper and lower shoulder area. On front covers, the area above the optical axis is reserved for the corporate logotype.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						backgroundImage: 'url(LE/1143/L5/2.png)'
					}
				},
				3: {
					title: 'Design Area and Frame',
					info: 'The design area is larger than the text block and establishes the maximum borders for visuals and colored spaces. If the design area is populated with a visual, a white frame is created automatically as a border.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						backgroundImage: 'url(LE/1143/L5/3.png)'
					}
				},
				4: {
					title: 'Type Area',
					info: 'Text is set only within the type area.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						backgroundImage: 'url(LE/1143/L5/4.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Corporate Logotype',
					info: 'For flyers, the logotype is used only on the front cover and centered in Daimler Blue \u2013 Pantone\xae 534 \u2013 on white. The size of the logotype is in line with the format. The distance from the upper edge of the format to the base line of the logotype is used for positioning purposes.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						'backgroundImage': 'url(LE/1143/L5/5.png)'
					},
					contents: '',
					arrow: {
						'class':'right',
						left: '116px',
						top: '128px'
					}
				},{
					title: 'Visuals',
					info: 'When using visuals, make sure they comply with the Daimler photo style.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						'backgroundImage': 'url(LE/1143/L5/6.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '80px',
						top: '260px'
					}
				},{
					title: 'Text Box',
					info: 'Text boxes are always in accent colors, with the text box in Lucent Blue standing for Daimler\'s corporate design. Type is set in text boxes in white or the corresponding space color. The text box is not placed on white but always in a colored space or a visual.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						'backgroundImage': 'url(LE/1143/L5/7.png)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'right',
						left: '60px',
						top: '510px'
					}
				}
			] // layers 3
		} // app 3
		,{ // app 4: L6 =================================================================================================================
			
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1143/L6/banner_EN.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '550px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '530px',
				width: '480px',
				background: 'url(LE/1143/L6/0.png) 0 50px no-repeat'
			},

			expandText:'Expand',
			collapseText:'',
			leftExpandButton: false,
			tools: {
				1: {
					title: 'Layout and Baseline Grid',
					info: 'The layout grid for all formats is made up of 11 x 12 mm basic elements that are 4 mm apart from each other horizontally and vertically. The baseline grid runs horizontally across the format in 2-mm increments.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1143/L6/1.png)',
						display: 'none'
					}
				},
				2: {
					title: 'Horizontal Axis',
					info: 'The horizontal layout grid in the upper third of the page is known as the shoulder. It divides the format into an upper and lower shoulder area. On front covers, the area above the optical axis is reserved for the corporate logotype.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1143/L6/2.png)'
					}
				},
				3: {
					title: 'Design Area and Frame',
					info: 'The design area is larger than the text block and establishes the maximum borders for visuals and colored spaces. If the design area is populated with a visual, a white frame is created automatically as a border.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1143/L6/3.png)'
					}
				},
				4: {
					title: 'Type Area',
					info: 'Text is set only within the type area.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1143/L6/4.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Visuals',
					info: 'The visuals are placed in alignment with the layout grid. The distance between the visuals is 1 mm.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '480px',
						'backgroundImage': 'url(LE/1143/L6/5.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '180px',
						top: '354px'
					}
				},{
					title: 'Headline System',
					info: 'Headlines are set, justified left, in Corporate S Regular. Texts run across the entire width of the type area. Lucent Blue is used for the headline and Premium Blue for the intro.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '480px',
						'backgroundImage': 'url(LE/1143/L6/6.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '80px',
						top: '77px'
					}
				},{
					title: 'Body Copy',
					info: 'Text is set only within the type area. Body copy is set in Corporate S Regular, 9 points, with 4-mm line spacing; subheadlines are in Corporate S Bold. Headlines are set apart with the respective accent color.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '480px',
						'backgroundImage': 'url(LE/1143/L6/7.png)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'right',
						left: '228px',
						top: '98px'
					}
				}
			] // layers 4
		}, // app 4
	] // apps
}
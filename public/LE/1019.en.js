{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1019/L15/banner_EN.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '375px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '355px',
				width: '480px',
				background: 'url(LE/1019/L15/0.png) 0 50px no-repeat'
			},
			expandText:'Expand',
			collapseText:'',
			leftExpandButton: true,
			tools: {
				1: false,
				2: false,
				3: false,
				4: false,
				5: false,
				6: false
			},
			layers: [
				{
					title: 'Corporate Logotype',
					info: 'The corporate logotype is 32 mm wide. It is centered 10 mm from the upper edge of the card to the base line of the logotype. For technical reasons, the special color Pantone\xae 2757 is used to print the corporate logotype on business cards.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '305px',
						'backgroundImage': 'url(LE/1019/L15/1.png)'
					},
					contents: '',
					arrow: {
						'class':'left',
						left: '340px',
						top: '86px'
					}
				},{
					title: 'Left-Hand Column',
					info: 'The business card information runs bottom to top; the last line is always 5 mm from the bottom edge. The top line shows the name of the holder in Corporate S Regular, 11 points. The person\'s academic title, if any, goes directly beneath this. The division or position information is given one blank line below (12-pt spacing). The email address is in the bottom line. There is at least one blank line between the e-mail address and the position information.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '305px',
						'backgroundImage': 'url(LE/1019/L15/2.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '110px',
						top: '126px'
					}
				},{
					title: 'Right-Hand Column',
					info: 'The right-hand column with the address information begins 50 mm from the left-hand edge of the card. The first line is level with the first line of the unit or function information. Telephone and fax numbers are structured from right to left in sets of two. All information is typeset in Corporate S Regular, 7.5 points, with 8.5-point line spacing, using the special color Pantone\xae Cool Gray 11. It can cover a maximum of eight lines.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '305px',
						'backgroundImage': 'url(LE/1019/L15/3.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '330px',
						top: '178px'
					}
				}
			] // layers 1
		}
	] // apps
}
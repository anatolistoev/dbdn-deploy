{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1022/L13/banner_DE.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '430px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '410px',
				width: '480px',
				background: 'url(LE/1022/L13/0.png) 0 50px no-repeat'
			},
			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',
			leftExpandButton: true,
			tools: {
				1: false,
				2: false,
				3: false,
				4: false,
				5: false,
				6: false
			},
			layers: [
				{
					title: 'Unternehmenszeichen',
					info: 'Auf den Folgecharts steht das Unternehmenszeichen linksbu\u0308ndig am oberen Rand. Seine Gr\xf6\xdfe und Position sind definiert und in den verfu\u0308gbaren Templates voreingestellt. Es wird durch eine Linie vom Inhalt der Folie getrennt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '360px',
						'backgroundImage': 'url(LE/1022/L13/1.png)'
					},
					contents: '',
					arrow: {
						'class':'left',
						left: '112px',
						top: '57px'
					}
				},{
					title: '\xdcberschriften',
					info: 'Die \xdcberschrift wird aus der Corporate S Bold 28 Pt. gesetzt, in Lucent Blue wiedergegeben und auf zwei Zeilen beschr\xe4nkt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '360px',
						'backgroundImage': 'url(LE/1022/L13/2.png)'
					},
					contents: '',
					arrow: {
						'class':'left',
						left: '369px',
						top: '112px'
					}
				},{
					title: 'Texte',
					info: 'Alle Texte innerhalb des Gestaltungsbereichs werden aus der Corporate S Regular gesetzt, die Standard-Schriftgr\xf6\xdfe betr\xe4gt 20 Pt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '360px',
						'backgroundImage': 'url(LE/1022/L13/3.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '240px',
						top: '215px'
					}
				},{
					title: 'Fu\xdfzeile',
					info: 'Unter der Gestaltungsfl\xe4che beinhaltet eine Fu\xdfzeile z.B. die Absenderangabe, das Datum und die Foliennummer. Sie wird aus der Corporate S Regular in 9 Pt. gesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '360px',
						'backgroundImage': 'url(LE/1022/L13/4.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '70px',
						top: '367px'
					}
				},{
					title: 'Optionale Kennzeichnungszeile',
					info: 'Zur Gliederung oder Kennzeichnung kann oberhalb der Trennlinie optional eine rechtsbu\u0308ndige Zusatzzeile eingestellt werden.\n',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '360px',
						'backgroundImage': 'url(LE/1022/L13/5.png)'
					},
					contents: '',
					arrow: {
						'class':'right',
						left: '328px',
						top: '57px'
					}
				}
			] // layers 1
		}
	] // apps
}
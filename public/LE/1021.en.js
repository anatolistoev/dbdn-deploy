{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1021/L12/banner_EN.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '430px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '410px',
				width: '480px',
				background: 'url(LE/1021/L12/0.png) 0 50px no-repeat'
			},
			expandText:'Expand',
			collapseText:'',
			leftExpandButton: false,
			tools: {
				1:false,
				2:false,
				3:false,
				4:false,
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Corporate Logotype',
					info: 'The corporate logotype is already in the correct size and position in the templates.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '360px',
						'backgroundImage': 'url(LE/1021/L12/1.png)'
					},
					contents: '',
					arrow: {
						'class':'right',
						left: '114px',
						top: '96px'
					}
				},{
					title: 'Headline',
					info: 'The presentation title is underneath the corporate logotype. It is set in Corporate S Bold, 28 points, justified left in Daimler Blue. The additional information for the title is recommended and is set in Lucent Blue, Corporate S Regular, 24 points.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '360px',
						'backgroundImage': 'url(LE/1021/L12/2.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '307px',
						top: '256px'
					}
				},{
					title: 'Footer',
					info: 'The sender information, such as department or date, is listed on the title slide and appears in the lower margin.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '360px',
						'backgroundImage': 'url(LE/1021/L12/3.png)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'down',
						left: '70px',
						top: '368px'
					}
				}
			] // layers 1
		}
	] // apps
}
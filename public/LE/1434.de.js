{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 L16 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1434/L16/banner_DE.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '430px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '410px',
				width: '480px',
				background: 'url(LE/1434/L16/0.png) 0 70px no-repeat'
			},
			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',
			leftExpandButton: false,
			tools: {
				1: {
					title: 'Gestaltungsraster',
					info: 'Der Gestaltungsraster setzt sich fu\u0308r alle Formate aus 11 x 12 mm gro\xdfen Grundelementen zusammen, die\nhorizontal und vertikal in einem Abstand von 4 mm zueinander stehen. Der Grundlinienraster zieht sich in 2-mm-\nSchritten horizontal u\u0308ber das Format.',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L16/1.png)',
						display: 'none'
					}
				},
				2: {
					title: '\xdcberschriftensystematik',
					info: '',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L16/5.png)'
					}
				},
				3: {
					title: 'Gestaltungsfl\xe4che und Rahmen',
					info: 'Die Gestaltungsfl\xe4che ist gr\xf6\xdfer als der Satzspiegel und begrenzt die maximale Ausdehnung der Bilder und\nFarbfl\xe4chen. Wird die Gestaltungsfl\xe4che mit einem Bildmotiv ausgefu\u0308llt, entsteht so automatisch ein wei\xdfer\nRahmen als Begrenzung.',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L16/2.png)'
					}
				},
				4: {
					title: 'Flie\xdftext',
					info: '',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L16/6.png)'
					}
				},
				5: {
					title: 'Satzspiegel',
					info: 'Schrift wird nur innerhalb des Satzspiegels gesetzt.',
					css:{
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L16/3.png)'
					}
				},
				6:{
					title: 'Bildmotive',
					info: '',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L16/7.png)'
					}
				}
			},
			layers: [
				{
					title: 'Tabelle',
					info: 'Zur Hervorhebung einzelner Tabellenwerte, Zeilen oder Spalten eignet sich die Corporate S Bold in Schwarz oder einer anderen Akzentfarbe. Die Schriftgr\xf6\xdfe betr\xe4gt 7,5 Pt. Vertikale und horizontale Farbfl\xe4chen k\xf6nnen hinterlegt werden, um zu gliedern und zu akzentuieren. <br><img src="LE/1434/L16/4zoom.png" style="width:79%;height:auto;" >',
					css:{
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1434/L16/4.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '80px',
						top: '116px'
					}
				}
			] // layers 1
		}, // END App 1 (L16)
		{ // app 2 L17 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1434/L17/banner_DE.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '430px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '410px',
				width: '480px',
				background: 'url(LE/1434/L17/0.png) 0 70px no-repeat'
			},
			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',
			leftExpandButton: true,
			tools: {
				1: {
					title: 'Gestaltungsraster',
					info: 'Der Gestaltungsraster setzt sich fu\u0308r alle Formate aus 11 x 12 mm gro\xdfen Grundelementen zusammen, die\nhorizontal und vertikal in einem Abstand von 4 mm zueinander stehen. Der Grundlinienraster zieht sich in 2-mm-\nSchritten horizontal u\u0308ber das Format.',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L17/1.png)',
						display: 'none'
					}
				},
				2: {
					title: '\xdcberschriftensystematik',
					info: '',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L17/7.png)'
					}
				},
				3: {
					title: 'Gestaltungsfl\xe4che und Rahmen',
					info: 'Die Gestaltungsfl\xe4che ist gr\xf6\xdfer als der Satzspiegel und begrenzt die maximale Ausdehnung der Bilder und\nFarbfl\xe4chen. Wird die Gestaltungsfl\xe4che mit einem Bildmotiv ausgefu\u0308llt, entsteht so automatisch ein wei\xdfer\nRahmen als Begrenzung.',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L17/2.png)'
					}
				},
				4: {
					title: 'Flie\xdftext',
					info: '',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L17/8.png)'
					}
				},
				5: {
					title: 'Satzspiegel',
					info: 'Schrift wird nur innerhalb des Satzspiegels gesetzt.',
					css:{
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L17/3.png)'
					}
				},
				6:{
					title: 'Bildmotive',
					info: '',
					css: {
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1434/L17/9.png)'
					}
				}
			},
			layers: [
				{
					title: 'Tortendiagramm',
					info: 'Ein Tortendiagramm stellt Teilwerte eines Ganzen dar. Tortendiagramme eignen sich gut fu\u0308r die Darstellung von Verteilungen und Anteilen. Der Text wird in der Corporate S Regular oder der Corporate S Bold gesetzt, die Schriftgr\xf6\xdfe betr\xe4gt 7,5 Pt. \xdcberschriften haben die Schriftgr\xf6\xdfe 9 Pt., Legenden die Schriftgr\xf6\xdfe 7,5 Pt. <br><img src="LE/1434/L17/4zoom.png" style="width:63%;height:auto;" >',
					css:{
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1434/L17/4.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '80px',
						top: '116px'
					}
				},
				{
					title: 'Liniendiagramm',
					info: 'Liniendiagramme erkl\xe4ren Ver\xe4nderungen u\u0308ber einen Zeitraum hinweg. Balkendiagramme zeigen sehr gut Rangfolgen auf. Fu\u0308r gute Lesbarkeit steht die Schrift auf dunklen Balken in Wei\xdf. Ansonsten sind Texte und Ziffern in Schwarz zu setzen. Horizontale Linien haben eine St\xe4rke von 0,25 Pt., 0,75 Pt. oder 1,5 Pt. <br><img src="LE/1434/L17/5zoom.png" style="width:60%;height:auto;" >',
					css:{
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1434/L17/5.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '180px',
						top: '116px'
					}
				},{
					title: 'Legende',
					info: 'Abbildungslegenden werden neben, ober- oder unterhalb eines Diagramms platziert und aus der Corporate S Bold in 7,5 Pt. in Schwarz gesetzt.',
					css:{
						"left": '0px',
						"top": '70px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1434/L17/6.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '20px',
						top: '201px'
					}
				}
			] // layers 1
		}
	] // apps
}
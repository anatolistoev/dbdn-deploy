{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1021/L12/banner_DE.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '430px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '410px',
				width: '480px',
				background: 'url(LE/1021/L12/0.png) 0 50px no-repeat'
			},
			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',
			leftExpandButton: false,
			tools: {
				1:false,
				2:false,
				3:false,
				4:false,
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Unternehmenszeichen',
					info: 'Das Unternehmenszeichen ist in den Templates bereits in der richtigen Gr\xf6\xdfe und Position platziert.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '360px',
						'backgroundImage': 'url(LE/1021/L12/1.png)'
					},
					contents: '',
					arrow: {
						'class':'right',
						left: '114px',
						top: '96px'
					}
				},{
					title: '\xdcberschrift',
					info: 'Der Titel der Pr\xe4sentation steht unter dem Unternehmenszeichen. Er wird linksbu\u0308ndig aus der Corporate S Bold in 28 Pt. in Daimler Blue gesetzt. Die Zusatzangaben zum Titel sind empfehlenswert und werden in Lucent Blue aus der Corporate S Regular in 24 Pt angelegt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '360px',
						'backgroundImage': 'url(LE/1021/L12/2.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '307px',
						top: '256px'
					}
				},{
					title: 'Fu\xdfzeile',
					info: 'Die Absenderangaben, z.B. fu\u0308r Abteilung oder Datum, werden auf der Titelfolie angegeben und stehen am Formatrand unten.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '360px',
						'backgroundImage': 'url(LE/1021/L12/3.png)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'down',
						left: '70px',
						top: '368px'
					}
				}
			] // layers 1
		}
	] // apps
}
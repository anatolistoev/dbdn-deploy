{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1017/L14/banner_DE.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0px'
				},
				open:{
					height: '750px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '730px',
				width: '480px',
				background: 'url(LE/1017/L14/0.png) 0 50px no-repeat'
			},
			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',
			leftExpandButton: true,
			tools: {
				1: false,
				2: false,
				3: false,
				4: false,
				5: false,
				6: false
			},
			layers: [
				{
					title: 'Unternehmenszeichen',
					info: 'Die Breite des Unternehmenszeichens betr\xe4gt 42 mm. Es wird im Abstand von 20 mm vom oberen Formatrand zur Grundlinie des Zeichens zentriert auf das Format gesetzt. Fu\u0308r den Druck des Unternehmenszeichens auf Gesch\xe4ftspapieren wird aus drucktechnischen Gru\u0308nden die Sonderfarbe Pantone\xae 2757 verwendet.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1017/L14/1.png)'
					},
					contents: '',
					arrow: {
						'class':'left',
						left: '301px',
						top: '79px'
					}
				},{
					title: 'Bezugszeichen',
					info: 'Die Positionierung der Bezugszeichen im Standardbriefbogen entspricht der DIN 676. Die Leitbegriffe werden aus der Corporate S Regular in 7,5 Pt. mit einem Zeilenabstand von 8,5 Pt. gesetzt. Als Druckfarbe kommt Pantone\xae Cool Gray 11 fu\u0308r alle Angaben zum Einsatz. Der linke Rand des Briefbogens wird als Heftrand benutzt und hat einheitlich eine Breite von 24 mm.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1017/L14/2.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '289px',
						top: '272px'
					}
				},{
					title: 'Fu\xdfleiste',
					info: 'Die Pflichtangaben der Fu\xdfzeile sind in \xa7 80 AktG festgelegt. Der Abstand des Blocks mit Adress- und Kommunikationsangaben zum linken Formatrand betr\xe4gt 151 mm. Alle Angaben der Fu\xdfleiste werden aus der Corporate S Regular in 7,5 Pt. mit einem Zeilenabstand von 8,5 Pt. gesetzt. Als Druckfarbe kommt Pantone\xae Cool Gray 11 fu\u0308r alle Angaben zum Einsatz.\n',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1017/L14/3.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '110px',
						top: '636px'
					}
				}
			] // layers 1
		}
	] // apps
}
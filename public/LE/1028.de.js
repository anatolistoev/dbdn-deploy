{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1028/L2/banner_DE.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '410px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '390px',
				width: '480px',
				background: 'url(LE/1028/L2/0.png) 0 50px no-repeat'
			},
			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',
			leftExpandButton: true,
			tools: {
				1: {
					title: 'Gestaltungsraster',
					info: 'Der Gestaltungsraster setzt sich fu\u0308r alle Formate aus 11 x 12 mm gro\xdfen Grundelementen zusammen, die\nhorizontal und vertikal in einem Abstand von 4 mm zueinander stehen. Der Grundlinienraster zieht sich in 2-mm-\nSchritten horizontal u\u0308ber das Format.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1028/L2/1.png)'
					}
				},
				2: {
					title: 'Horizontale Achse',
					info: 'Die horizontale Gestaltungsachse im oberen Drittel einer Seite wird als \u201eSchulter\u201c bezeichnet. Sie teilt das\nFormat in einen \xdcber- und Unterschulterbereich. Auf Titelseiten ist die Fl\xe4che oberhalb der optischen Achse\ndem Unternehmenszeichen vorbehalten.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1028/L2/2.png)'
					}
				},
				3: {
					title: 'Gestaltungsfl\xe4che und Rahmen',
					info: 'Die Gestaltungsfl\xe4che ist gr\xf6\xdfer als der Satzspiegel und begrenzt die maximale Ausdehnung der Bilder und\nFarbfl\xe4chen. Wird die Gestaltungsfl\xe4che mit einem Bildmotiv ausgefu\u0308llt, entsteht so automatisch ein wei\xdfer\nRahmen als Begrenzung.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1028/L2/3.png)'
					}
				},
				4: {
					title: 'Satzspiegel',
					info: 'Schrift wird nur innerhalb des Satzspiegels gesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1028/L2/4.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Bilder',
					info: 'Werden mehrere Bilder neben oder u\u0308bereinander angeordnet, betr\xe4gt der Abstand zwischen Bildern 1 mm. Die\nPlatzierung der Fl\xe4chen und Bilder orientiert sich am Gestaltungsraster.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1028/L2/5.png)'
					},
					contents: '',
					arrow: {
						'class':'right',
						left: '220px',
						top: '90px'
					}
				},{
					title: '\xdcberschriftensystematik',
					info: '\xdcberschriften werden aus der Corporate S Regular linksbu\u0308ndig gesetzt. Die Zeilen laufen u\u0308ber die komplette\nBreite des Satzspiegels. Lucent Blue wird fu\u0308r die \xdcberschrift und Premium Blue fu\u0308r die Einleitung eingesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1028/L2/6.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '60px',
						top: '106px'
					}
				},{
					title: 'Flie\xdftexte',
					info: 'Flie\xdftexte werden aus der Corporate S Regular in 9 Pt. mit einem Zeilenabstand von 4 mm,\nZwischenu\u0308berschriften aus der Corporate S Bold in 9 Pt. gesetzt. Der Flie\xdftext schlie\xdft sich direkt, also ohne\nUmbruch oder Leerzeile, an die Zwischenu\u0308berschrift an. \xdcberschriften werden durch die jeweilige Akzentfarbe\nhervorgehoben.\n',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1028/L2/7.png)'
					},
					contents: '',
					//alwaysVisible: true,
					arrow: {
						'class':'right',
						left: '80px',
						top: '140px'
					}
				}
			] // layers 1
		}
	] // apps
}
{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1: L3 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1143/L3/banner_DE.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '1030px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '1010px',
				width: '480px',
				background: 'url(LE/1143/L3/0.png) 0 50px no-repeat'
			},
			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',
			leftExpandButton: false,
			tools: {
				1: {
					title: 'Gestaltungsraster',
					info: 'Der Gestaltungsraster setzt sich fu\u0308r alle Formate aus 11 x 12 mm gro\xdfen Grundelementen zusammen, die\nhorizontal und vertikal in einem Abstand von 4 mm zueinander stehen. Der Grundlinienraster zieht sich in 2-mm-\nSchritten horizontal u\u0308ber das Format.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						backgroundImage: 'url(LE/1143/L3/1.png)',
						display: 'none'
					}
				},
				2: {
					title: 'Horizontale Achse',
					info: 'Die horizontale Gestaltungsachse im oberen Drittel einer Seite wird als \u201eSchulter\u201c bezeichnet. Sie teilt das\nFormat in einen \xdcber- und Unterschulterbereich. Auf Titelseiten ist die Fl\xe4che oberhalb der optischen Achse\ndem Unternehmenszeichen vorbehalten.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						backgroundImage: 'url(LE/1143/L3/2.png)'
					}
				},
				3: {
					title: 'Gestaltungsfl\xe4che und Rahmen',
					info: 'Die Gestaltungsfl\xe4che ist gr\xf6\xdfer als der Satzspiegel und begrenzt die maximale Ausdehnung der Bilder und\nFarbfl\xe4chen. Wird die Gestaltungsfl\xe4che mit einem Bildmotiv ausgefu\u0308llt, entsteht so automatisch ein wei\xdfer\nRahmen als Begrenzung.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						backgroundImage: 'url(LE/1143/L3/3.png)'
					}
				},
				4: {
					title: 'Satzspiegel',
					info: 'Schrift wird nur innerhalb des Satzspiegels gesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						backgroundImage: 'url(LE/1143/L3/4.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Unternehmenszeichen',
					info: 'Auf Flyern wird das Unternehmenszeichen nur auf den Titelseiten verwendet und zentriert in Daimler Blue \u2013\nPantone\xae 534 \u2013 auf Wei\xdf gesetzt. Die Gr\xf6\xdfe des Unternehmenszeichens ist auf das Format abgestimmt. Fu\u0308r\ndie Positionierung des Zeichens gilt der Abstand vom oberen Formatrand bis zur Grundlinie des Zeichens.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						'backgroundImage': 'url(LE/1143/L3/5.png)'
					},
					contents: '',
					arrow: {
						'class':'right',
						left: '116px',
						top: '128px'
					}
				},{
					title: 'Bild',
					info: 'Beim Einsatz von Bildern sollte darauf geachtet werden, dass sie dem Daimler-Fotostil entsprechen.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						'backgroundImage': 'url(LE/1143/L3/6.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '80px',
						top: '260px'
					}
				},{
					title: '\xdcberschriftensystematik',
					info: '\xdcberschriften werden aus der Corporate S Regular linksbu\u0308ndig gesetzt. Die Zeilen laufen u\u0308ber die komplette Breite des Satzspiegels. Lucent Blue wird fu\u0308r die \xdcberschrift und Premium Blue fu\u0308r die Einleitung eingesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						'backgroundImage': 'url(LE/1143/L3/7.png)'
					},
					contents: '',
					//alwaysVisible: true,
					arrow: {
						'class':'down',
						left: '220px',
						top: '510px'
					}
				}
			] // layers 1
		},{ // app 2: L4 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1143/L4/banner_DE.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '550px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '530px',
				width: '480px',
				background: 'url(LE/1143/L4/0.png) 0 50px no-repeat'
			},
			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',
			leftExpandButton: false,
			tools: {
				1: {
					title: 'Gestaltungsraster',
					info: 'Der Gestaltungsraster setzt sich fu\u0308r alle Formate aus 11 x 12 mm gro\xdfen Grundelementen zusammen, die\nhorizontal und vertikal in einem Abstand von 4 mm zueinander stehen. Der Grundlinienraster zieht sich in 2-mm-\nSchritten horizontal u\u0308ber das Format.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1143/L4/1.png)',
						display: 'none'
					}
				},
				2: {
					title: 'Horizontale Achse',
					info: 'Die horizontale Gestaltungsachse im oberen Drittel einer Seite wird als \u201eSchulter\u201c bezeichnet. Sie teilt das\nFormat in einen \xdcber- und Unterschulterbereich. Auf Titelseiten ist die Fl\xe4che oberhalb der optischen Achse\ndem Unternehmenszeichen vorbehalten.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1143/L4/2.png)'
					}
				},
				3: {
					title: 'Gestaltungsfl\xe4che und Rahmen',
					info: 'Die Gestaltungsfl\xe4che ist gr\xf6\xdfer als der Satzspiegel und begrenzt die maximale Ausdehnung der Bilder und\nFarbfl\xe4chen. Wird die Gestaltungsfl\xe4che mit einem Bildmotiv ausgefu\u0308llt, entsteht so automatisch ein wei\xdfer\nRahmen als Begrenzung.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1143/L4/3.png)'
					}
				},
				4: {
					title: 'Satzspiegel',
					info: 'Schrift wird nur innerhalb des Satzspiegels gesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1143/L4/4.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Bilder',
					info: 'Die Platzierung der Abbildungen orientiert sich am Gestaltungsraster. Der Abstand zwischen den Bildern betr\xe4gt 1 mm.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '480px',
						'backgroundImage': 'url(LE/1143/L4/5.png)'
					},
					contents: '',
					arrow: {
						'class':'right',
						left: '218px',
						top: '128px'
					}
				},{
					title: '\xdcberschriftensystematik',
					info: '\xdcberschriften werden aus der Corporate S Regular linksbu\u0308ndig gesetzt. Die Zeilen laufen u\u0308ber die komplette Breite des Satzspiegels. Lucent Blue wird fu\u0308r die \xdcberschrift und Premium Blue fu\u0308r die Einleitung eingesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '480px',
						'backgroundImage': 'url(LE/1143/L4/6.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '80px',
						top: '87px'
					}
				},{
					title: 'Flie\xdftext',
					info: 'Schrift wird nur innerhalb des Satzspiegels gesetzt. Flie\xdftexte sind in Corporate S Regular in 9 pt mit einem Zeilenabstand von 4 mm, Zwischenu\u0308berschriften in Corporate S Bold gesetzt. \xdcberschriften werden durch die jeweilige Akzentfarbe hervorgehoben.\n',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '480px',
						'backgroundImage': 'url(LE/1143/L4/7.png)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'up',
						left: '150px',
						top: '360px'
					}
				}
			] // layers 2
		}, // app 2
		{ // app 3: L5 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1143/L5/banner_DE.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '1030px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '1010px',
				width: '480px',
				background: 'url(LE/1143/L5/0.png) 0 50px no-repeat'
			},
		
			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',
			leftExpandButton: false,
			tools: {
				1: {
					title: 'Gestaltungsraster',
					info: 'Der Gestaltungsraster setzt sich fu\u0308r alle Formate aus 11 x 12 mm gro\xdfen Grundelementen zusammen, die\nhorizontal und vertikal in einem Abstand von 4 mm zueinander stehen. Der Grundlinienraster zieht sich in 2-mm-\nSchritten horizontal u\u0308ber das Format.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						backgroundImage: 'url(LE/1143/L5/1.png)',
						display: 'none'
					}
				},
				2: {
					title: 'Horizontale Achse',
					info: 'Die horizontale Gestaltungsachse im oberen Drittel einer Seite wird als \u201eSchulter\u201c bezeichnet. Sie teilt das\nFormat in einen \xdcber- und Unterschulterbereich. Auf Titelseiten ist die Fl\xe4che oberhalb der optischen Achse\ndem Unternehmenszeichen vorbehalten.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						backgroundImage: 'url(LE/1143/L5/2.png)'
					}
				},
				3: {
					title: 'Gestaltungsfl\xe4che und Rahmen',
					info: 'Die Gestaltungsfl\xe4che ist gr\xf6\xdfer als der Satzspiegel und begrenzt die maximale Ausdehnung der Bilder und\nFarbfl\xe4chen. Wird die Gestaltungsfl\xe4che mit einem Bildmotiv ausgefu\u0308llt, entsteht so automatisch ein wei\xdfer\nRahmen als Begrenzung.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						backgroundImage: 'url(LE/1143/L5/3.png)'
					}
				},
				4: {
					title: 'Satzspiegel',
					info: 'Schrift wird nur innerhalb des Satzspiegels gesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						backgroundImage: 'url(LE/1143/L5/4.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Unternehmenszeichen',
					info: 'Auf Flyern wird das Unternehmenszeichen nur auf den Titelseiten verwendet und zentriert in Daimler Blue \u2013 Pantone\xae 534 \u2013 auf Wei\xdf gesetzt. Die Gr\xf6\xdfe des Unternehmenszeichens ist auf das Format abgestimmt. Fu\u0308r die Positionierung des Zeichens gilt der Abstand vom oberen Formatrand bis zur Grundlinie des Zeichens.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						'backgroundImage': 'url(LE/1143/L5/5.png)'
					},
					contents: '',
					arrow: {
						'class':'right',
						left: '116px',
						top: '128px'
					}
				},{
					title: 'Bild',
					info: 'Beim Einsatz von Bildern sollte darauf geachtet werden, dass sie dem Daimler-Fotostil entsprechen.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						'backgroundImage': 'url(LE/1143/L5/6.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '80px',
						top: '260px'
					}
				},{
					title: 'Textbox',
					info: 'Textboxen sind immer in Akzentfarben angelegt, wobei die Textbox in Lucent Blue das visuelle Erscheinungsbild von Daimler pr\xe4gt. Schrift in Textboxen wird in Wei\xdf bzw. der entsprechenden Fl\xe4chenfarbe gesetzt. Die Textbox steht nicht auf Wei\xdf sondern immer auf einer Farbfl\xe4che oder einem Bild.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '960px',
						'backgroundImage': 'url(LE/1143/L5/7.png)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'right',
						left: '60px',
						top: '510px'
					}
				}
			] // layers 3
		} // app 3
		,{ // app 4: L6 =================================================================================================================
			
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1143/L6/banner_DE.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '550px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '530px',
				width: '480px',
				background: 'url(LE/1143/L6/0.png) 0 50px no-repeat'
			},

			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',
			leftExpandButton: false,
			tools: {
				1: {
					title: 'Gestaltungsraster',
					info: 'Der Gestaltungsraster setzt sich fu\u0308r alle Formate aus 11 x 12 mm gro\xdfen Grundelementen zusammen, die\nhorizontal und vertikal in einem Abstand von 4 mm zueinander stehen. Der Grundlinienraster zieht sich in 2-mm-\nSchritten horizontal u\u0308ber das Format.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1143/L6/1.png)',
						display: 'none'
					}
				},
				2: {
					title: 'Horizontale Achse',
					info: 'Die horizontale Gestaltungsachse im oberen Drittel einer Seite wird als \u201eSchulter\u201c bezeichnet. Sie teilt das\nFormat in einen \xdcber- und Unterschulterbereich. Auf Titelseiten ist die Fl\xe4che oberhalb der optischen Achse\ndem Unternehmenszeichen vorbehalten.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1143/L6/2.png)'
					}
				},
				3: {
					title: 'Gestaltungsfl\xe4che und Rahmen',
					info: 'Die Gestaltungsfl\xe4che ist gr\xf6\xdfer als der Satzspiegel und begrenzt die maximale Ausdehnung der Bilder und\nFarbfl\xe4chen. Wird die Gestaltungsfl\xe4che mit einem Bildmotiv ausgefu\u0308llt, entsteht so automatisch ein wei\xdfer\nRahmen als Begrenzung.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1143/L6/3.png)'
					}
				},
				4: {
					title: 'Satzspiegel',
					info: 'Schrift wird nur innerhalb des Satzspiegels gesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1143/L6/4.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Bilder',
					info: 'Die Platzierung der Abbildungen orientiert sich am Gestaltungsraster. Der Abstand zwischen den Bildern betr\xe4gt 1 mm.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '480px',
						'backgroundImage': 'url(LE/1143/L6/5.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '180px',
						top: '354px'
					}
				},{
					title: '\xdcberschriftensystematik',
					info: '\xdcberschriften werden aus der Corporate S Regular linksbu\u0308ndig gesetzt. Die Zeilen laufen u\u0308ber die komplette Breite des Satzspiegels. Lucent Blue wird fu\u0308r die \xdcberschrift und Premium Blue fu\u0308r die Einleitung eingesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '480px',
						'backgroundImage': 'url(LE/1143/L6/6.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '80px',
						top: '77px'
					}
				},{
					title: 'Flie\xdftext',
					info: 'Schrift wird nur innerhalb des Satzspiegels gesetzt. Flie\xdftexte sind in Corporate S Regular in 9 pt mit einem Zeilenabstand von 4 mm, Zwischenu\u0308berschriften in Corporate S Bold gesetzt. \xdcberschriften werden durch die jeweilige Akzentfarbe hervorgehoben.\n',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '480px',
						'backgroundImage': 'url(LE/1143/L6/7.png)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'right',
						left: '228px',
						top: '98px'
					}
				}
			] // layers 4
		}, // app 4
	] // apps
}		
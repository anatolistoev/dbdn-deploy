{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1028/L2/banner_EN.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '410px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '390px',
				width: '480px',
				background: 'url(LE/1028/L2/0.png) 0 50px no-repeat'
			},
			expandText:'Expand',
			collapseText:'',
			leftExpandButton: true,
			tools: {
				1: {
					title: 'Layout and Baseline Grid',
					info: 'The layout grid for all formats is made up of 11 x 12 mm basic elements that are 4 mm apart from each other horizontally and vertically. The baseline grid runs horizontally across the format in 2-mm increments.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1028/L2/1.png)'
					}
				},
				2: {
					title: 'Horizontal Axis',
					info: 'The horizontal layout grid in the upper third of the page is known as the shoulder. It divides the format into an upper and lower shoulder area. On front covers, the area above the optical axis is reserved for the corporate logotype.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1028/L2/2.png)'
					}
				},
				3: {
					title: 'Design Area and Frame',
					info: 'The design area is larger than the text block and establishes the maximum borders for visuals and colored spaces. If the design area is populated with a visual, a white frame is created automatically as a border.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1028/L2/3.png)'
					}
				},
				4: {
					title: 'Type Area',
					info: 'Text is set only within the type area.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1028/L2/4.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Visuals',
					info: 'If multiple visuals are placed next to or on top of each other, the distance between them is 1 mm. The placement of the spaces and visuals is based on the layout grid.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1028/L2/5.png)'
					},
					contents: '',
					arrow: {
						'class':'right',
						left: '220px',
						top: '90px'
					}
				},{
					title: 'Headline System',
					info: 'Headlines are set, justified left, in Corporate S Regular. Texts run across the entire width of the type area. Lucent Blue is used for the headline and Premium Blue for the intro.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1028/L2/6.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '60px',
						top: '106px'
					}
				},{
					title: 'Body Copy',
					info: 'Body copy is set in Corporate S Regular, 9 points, with 4-mm line spacing. Subheadlines are set in Corporate S Bold, 9 points. The body copy immediately follows the subheadline without breaks or blank lines. Headlines are set apart with the respective accent color.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1028/L2/7.png)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'right',
						left: '80px',
						top: '140px'
					}
				}
			] // layers 1
		}
	] // apps
}
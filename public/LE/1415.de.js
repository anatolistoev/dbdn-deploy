{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(img/LE/sampleBGClosed.jpg) no-repeat',
					border: '0px solid #666'
				},
				open:{
					height: '550px',
					background: 'url(img/LE/sampleBG.gif) no-repeat'
				}
			},
			css: {
				height: '340px'
			},
			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',
			tools: {
				axis: false,
				grid: false,
				frame: false,
				area: {
					"left": '10px',
					"top": '50px',
					"width": '478px',
					"height": '338px',
					backgroundImage: 'url(img/LE/sampleArea.gif)'
				}
			},
			layers: [
				{
					title: 'Layer 1',
					info: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget justo mi, et scelerisque odio. Quisque quis pretium tortor. Nullam euismod, urna vel congue viverra, velit turpis cursus turpis, vitae rutrum tortor orci ac lorem. Aenean vel tellus rhoncus tortor elementum molestie. Nam iaculis nulla sit amet lorem tempor semper. Donec in nibh leo, et facilisis risus. Quisque erat sem, ultricies ac dignissim id, fermentum eu est. Integer eros diam, aliquet in suscipit at, lobortis et sem. Sed vel tellus id quam pretium rhoncus. Etiam at risus in arcu volutpat varius. Pellentesque est turpis, consectetur eget posuere id, eleifend et felis.<br><a href="#">more</a>',
					css:{
						"left": '10px',
						"top": '50px',
						"width": '478px',
						"height": '338px',
						'backgroundImage': 'url(img/LE/sampleLayer.jpg)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'down',
						left: '250px',
						top: '110px'
					}
				},{
					title: 'Layer 2',
					info: 'Quisque erat sem, ultricies ac <b>dignissim id</b>, fermentum eu est. Integer eros diam, aliquet in suscipit at, lobortis et sem.',
					css:{
						"left": '16px',
						"top": '118px',
						"width": '105px',
						"height": '57px',
						//display: 'none',
						'backgroundImage': 'url(img/LE/sampleLayer2.jpg)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '60px',
						top: '80px'
					}
				},{
					title: 'Layer 3',
					info: 'Quisque erat sem, ultricies ac dignissim id, fermentum eu est. Integer eros diam, aliquet in suscipit at, lobortis et sem.',
					css:{
						"left": '121px',
						"top": '118px',
						"width": '105px',
						"height": '57px',
						'backgroundImage': 'url(img/LE/sampleLayer3.jpg)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '140px',
						top: '80px'
					}
				}
			] // layers 1
		},
		{ // app 2 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(img/LE/sampleBGClosed.jpg) no-repeat',
					border: '0px solid #666'
				},
				open:{
					height: '550px',
					background: 'url(img/LE/sampleBG.gif) no-repeat'
				}
			},
			css: {
				height: '340px'
			},
			expandText:'Detailansicht &ouml;ffnen (2)',
			collapseText:'',
			tools: {
				axis: false,
				grid: false,
				frame: false,
				area: false
			},
			layers: [
				{
					title: 'App 2 Layer 1',
					info: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget justo mi, et scelerisque odio. Quisque quis pretium tortor. Nullam euismod, urna vel congue viverra, velit turpis cursus turpis, vitae rutrum tortor orci ac lorem. Aenean vel tellus rhoncus tortor elementum molestie. Nam iaculis nulla sit amet lorem tempor semper. Donec in nibh leo, et facilisis risus. Quisque erat sem, ultricies ac dignissim id, fermentum eu est. Integer eros diam, aliquet in suscipit at, lobortis et sem. Sed vel tellus id quam pretium rhoncus. Etiam at risus in arcu volutpat varius. Pellentesque est turpis, consectetur eget posuere id, eleifend et felis.<br><a href="#">more</a>',
					css:{
						"left": '10px',
						"top": '50px',
						"width": '478px',
						"height": '338px',
						'backgroundImage': 'url(img/LE/sampleLayer.jpg)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'left',
						left: '250px',
						top: '50px'
					}
				},{
					title: 'App 2 Layer 2',
					info: 'Quisque erat sem, ultricies ac <b>dignissim id</b>, fermentum eu est. Integer eros diam, aliquet in suscipit at, lobortis et sem.',
					css:{
						"left": '16px',
						"top": '118px',
						"width": '105px',
						"height": '57px',
						//display: 'none',
						'backgroundImage': 'url(img/LE/sampleLayer2.jpg)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '60px',
						top: '80px'
					}
				},{
					title: 'App 2 Layer 3',
					info: '<h1>APP 2</h1>Quisque erat sem, ultricies ac dignissim id, fermentum eu est. Integer eros diam, aliquet in suscipit at, lobortis et sem.',
					css:{
						"left": '121px',
						"top": '118px',
						"width": '105px',
						"height": '57px',
						'backgroundImage': 'url(img/LE/sampleLayer3.jpg)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '140px',
						top: '80px'
					}
				}
			] // layers 1
		}
	] // apps
}
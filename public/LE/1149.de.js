{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 L9 =================================================================================================================
			
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1149/L9/banner_DE.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '750px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '730px',
				width: '480px',
				background: 'url(LE/1149/L9/0.png) 0 50px no-repeat'
			},
			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',
			leftExpandButton: true,
			tools: {
				1: {
					title: 'Gestaltungsraster',
					info: 'Der Gestaltungsraster setzt sich fu\u0308r alle Formate aus 11 x 12 mm gro\xdfen Grundelementen zusammen, die\nhorizontal und vertikal in einem Abstand von 4 mm zueinander stehen. Der Grundlinienraster zieht sich in 2-mm-\nSchritten horizontal u\u0308ber das Format.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L9/1.png)',
						display: 'none'
					}
				},
				2: false,
				3: {
					title: 'Gestaltungsfl\xe4che und Rahmen',
					info: 'Die Gestaltungsfl\xe4che ist gr\xf6\xdfer als der Satzspiegel und begrenzt die maximale Ausdehnung der Bilder und\nFarbfl\xe4chen. Wird die Gestaltungsfl\xe4che mit einem Bildmotiv ausgefu\u0308llt, entsteht so automatisch ein wei\xdfer\nRahmen als Begrenzung.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L9/2.png)'
					}
				},
				4: {
					title: 'Satzspiegel',
					info: 'Schrift wird nur innerhalb des Satzspiegels gesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L9/3.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Bild',
					info: 'Abbildungen und farbige Fl\xe4chen wirken beim Betrachter direkter als Schrift allein. Sie werden innerhalb der Gestaltungsfl\xe4che von Rand zu Rand gesetzt und k\xf6nnen in der H\xf6he variieren.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '679px',
						'backgroundImage': 'url(LE/1149/L9/4.png)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'right',
						left: '60px',
						top: '180px'						
					}
				},{
					title: '\xdcberschriftensystematik',
					info: 'Wird auf dem Plakat keine Textbox eingesetzt, wird die \xdcberschrift u\u0308ber die \xdcberschriftensystematik dargestellt. Es ist ein Abstand von mindestens einer Versalh\xf6he zur daru\u0308ber befindlichen Abbildung oder Farbfl\xe4che einzuhalten. \xdcberschrift und Einleitung sind in einer Akzentfarbe und in der entsprechenden Fl\xe4chenfarbe zu gestalten. Das Farbpaar Premium Blue/Lucent Blue spiegelt das typische Daimler-Farbklima wieder.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '679px',
						'backgroundImage': 'url(LE/1149/L9/5.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '140px',
						top: '600px'
					}
				},{
					title: 'Unternehmenszeichen',
					info: 'Das Unternehmenszeichen wird innerhalb des Satzspiegels unten rechts platziert. In Abh\xe4ngigkeit vom Plakatformat ist die entsprechende Gr\xf6\xdfe und Position des Unternehmenszeichens festgelegt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '679px',
						'backgroundImage': 'url(LE/1149/L9/6.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '371px',
						top: '702px'
					}
				},{
					title: 'Zusatzinformation',
					info: 'Absenderangaben und Zusatzinformationen stehen linksbu\u0308ndig auf einer Linie mit dem Unternehmenszeichen. Sie werden in Schwarz in der Corporate S Regular gesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '679px',
						'backgroundImage': 'url(LE/1149/L9/7.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '200px',
						top: '650px'
					}
				}
			] // layers 1
		},{ // app 2 L10 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1149/L10/banner_DE.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '750px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '730px',
				width: '480px',
				background: 'url(LE/1149/L10/0.png) 0 50px no-repeat'
			},
			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',
			leftExpandButton: false,
			tools: {
				1: {
					title: 'Gestaltungsraster',
					info: 'Der Gestaltungsraster setzt sich fu\u0308r alle Formate aus 11 x 12 mm gro\xdfen Grundelementen zusammen, die\nhorizontal und vertikal in einem Abstand von 4 mm zueinander stehen. Der Grundlinienraster zieht sich in 2-mm-\nSchritten horizontal u\u0308ber das Format.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L10/1.png)',
						display: 'none'
					}
				},
				2: false,
				3: {
					title: 'Gestaltungsfl\xe4che und Rahmen',
					info: 'Die Gestaltungsfl\xe4che ist gr\xf6\xdfer als der Satzspiegel und begrenzt die maximale Ausdehnung der Bilder und\nFarbfl\xe4chen. Wird die Gestaltungsfl\xe4che mit einem Bildmotiv ausgefu\u0308llt, entsteht so automatisch ein wei\xdfer\nRahmen als Begrenzung.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L10/2.png)'
					}
				},
				4: {
					title: 'Satzspiegel',
					info: 'Schrift wird nur innerhalb des Satzspiegels gesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L10/3.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Bild',
					info: 'Abbildungen und farbige Fl\xe4chen wirken beim Betrachter direkter als Schrift allein. Sie werden innerhalb der Gestaltungsfl\xe4che von Rand zu Rand gesetzt und k\xf6nnen in der H\xf6he variieren.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '679px',
						'backgroundImage': 'url(LE/1149/L10/4.png)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'right',
						left: '60px',
						top: '350px'
					}
				},{
					title: 'Textbox',
					info: 'Wird bei der Plakatgestaltung eine Textbox eingesetzt, beinhaltet sie die \xdcberschrift. Die Schrift der Textbox erscheint in Wei\xdf aus der Corporate S Regular, sie wird bevorzugt in Lucent Blue angelegt und l\xe4uft bis zum Formatanschnitt. Zus\xe4tzliche, erkl\xe4rende Hinweise stehen dann direkt unter der Abbildung bzw. Farbfl\xe4che aus der Corporate S Regular in Schwarz.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '679px',
						'backgroundImage': 'url(LE/1149/L10/5.png)'
					},
					contents: '',
					arrow: {
						'class':'left',
						left: '365px',
						top: '180px'
					}
				},{
					title: 'Unternehmenszeichen',
					info: 'Das Unternehmenszeichen wird innerhalb des Satzspiegels unten rechts platziert. In Abh\xe4ngigkeit vom Plakatformat ist die entsprechende Gr\xf6\xdfe des Unternehmenszeichens festgelegt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '679px',
						'backgroundImage': 'url(LE/1149/L10/6.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '371px',
						top: '702px'
					}
				},{
					title: 'Flie\xdftext und Zusatzinformation',
					info: 'Absenderangaben und Zusatzinformationen stehen linksbu\u0308ndig auf einer Linie mit dem Unternehmenszeichen. Sie werden in Schwarz in der Corporate S Regular gesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '679px',
						'backgroundImage': 'url(LE/1149/L10/7.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '90px',
						top: '590px'
					}
				}
			] // layers 2
		}, // app 2
		{ // app 3 L11 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1149/L11/banner_DE.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '410px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '430px',
				width: '480px',
				background: 'url(LE/1149/L11/0.png) 0 50px no-repeat'
			},
			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',
			leftExpandButton: true,
			tools: {
				1: {
					title: 'Gestaltungsraster',
					info: 'Der Gestaltungsraster setzt sich fu\u0308r alle Formate aus 11 x 12 mm gro\xdfen Grundelementen zusammen, die\nhorizontal und vertikal in einem Abstand von 4 mm zueinander stehen. Der Grundlinienraster zieht sich in 2-mm-\nSchritten horizontal u\u0308ber das Format.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L11/1.png)',
						display: 'none'
					}
				},
				2: false,
				3: {
					title: 'Gestaltungsfl\xe4che und Rahmen',
					info: 'Die Gestaltungsfl\xe4che ist gr\xf6\xdfer als der Satzspiegel und begrenzt die maximale Ausdehnung der Bilder und\nFarbfl\xe4chen. Wird die Gestaltungsfl\xe4che mit einem Bildmotiv ausgefu\u0308llt, entsteht so automatisch ein wei\xdfer\nRahmen als Begrenzung.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L11/2.png)'
					}
				},
				4: {
					title: 'Satzspiegel',
					info: 'Schrift wird nur innerhalb des Satzspiegels gesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1149/L11/3.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Bilder',
					info: 'Abbildungen und farbige Fl\xe4chen wirken beim Betrachter direkter als Schrift allein. Bei Formaten bis zu einer Gr\xf6\xdfe von DIN A 2 betr\xe4gt der Abstand mehrerer Abbildungen zueinander 1 mm. Ab DIN A 1 betr\xe4gt der Abstand 2 mm.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1149/L11/4.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '95px',
						top: '206px'
					}
				},{
					title: '\xdcberschriftensystematik',
					info: 'Die \xdcberschriftensystematik findet auch bei Informationsplakaten Anwendung. \xdcberschrift und Einleitung sind somit zweifarbig zu gestalten - in einer Akzentfarbe und in der entsprechenden Fl\xe4chenfarbe. Das Farbpaar Premium Blue/Lucent Blue ist typisch fu\u0308r das Daimler-Erscheinungsbild und sollte deshalb bevorzugt eingesetzt werden.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1149/L11/5.png)'
					},
					contents: '',
					arrow: {
						'class':'up',
						left: '307px',
						top: '256px'
					}
				},{
					title: 'Flie\xdftext',
					info: 'Flie\xdftexte sind in Schwarz in der Corporate S Regular zu setzen. Einfu\u0308hrungstexte und Zwischenu\u0308berschriften werden in einer Fl\xe4chenfarbe in der Corporate S Bold gesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1149/L11/6.png)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'up',
						left: '240px',
						top: '330px'
					}
				},{
					title: 'Unternehmenszeichen',
					info: 'Das Unternehmenszeichen wird innerhalb des Satzspiegels unten rechts platziert. In Abh\xe4ngigkeit vom Plakatformat ist die entsprechende Gr\xf6\xdfe des Unternehmenszeichens festgelegt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '340px',
						'backgroundImage': 'url(LE/1149/L11/7.png)'
					},
					contents: '',
					alwaysVisible: true,
					arrow: {
						'class':'down',
						left: '400px',
						top: '324px'
					}
				}
			] // layers 1
		}
	] // apps
}
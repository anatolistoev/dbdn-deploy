{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1019/L15/banner_DE.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '375px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '355px',
				width: '480px',
				background: 'url(LE/1019/L15/0.png) 0 50px no-repeat'
			},
			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',
			leftExpandButton: true,
			tools: {
				1: false,
				2: false,
				3: false,
				4: false,
				5: false,
				6: false
			},
			layers: [
				{
					title: 'Unternehmenszeichen',
					info: 'Die Breite des Unternehmenszeichens betr\xe4gt 32 mm und wird im Abstand von 10 mm vom oberen Formatrand zur Grundlinie des Zeichens zentriert auf das Format gesetzt. Fu\u0308r den Druck des Unternehmenszeichens auf Visitenkarten wird aus drucktechnischen Gru\u0308nden die Sonderfarbe Pantone\xae 2757 verwendet.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '305px',
						'backgroundImage': 'url(LE/1019/L15/1.png)'
					},
					contents: '',
					arrow: {
						'class':'left',
						left: '340px',
						top: '86px'
					}
				},{
					title: 'Linke Spalte',
					info: 'Die Visitenkarte baut sich von unten nach oben auf, die letzte Zeile steht immer 5 mm vom unteren Rand entfernt. In der obersten Zeile wird der Name des Inhabers in Corporate S Regular in 11Pt. gesetzt. Direkt darunter erfolgt die eventuelle Angabe eines akademischen Titels. Die Bereichs- oder Funktionsangabe steht eine Leerzeile mit einem Abstand von 12 Pt. darunter. In der untersten Zeile steht die E-Mail-Adresse. Der Abstand zwischen der E-Mail-Adresse und den Funktionsangaben betr\xe4gt mindestens eine Leerzeile.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '305px',
						'backgroundImage': 'url(LE/1019/L15/2.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '110px',
						top: '126px'
					}
				},{
					title: 'Rechte Spalte',
					info: 'Die rechte Spalte mit Adressangaben beginnt 50 mm vom linken Formatrand. Die erste Zeile steht in gleicher H\xf6he wie die erste Zeile der Bereichs- oder Funktionsangabe. Die Telefon- und Faxnummern werden in Zweierschritten von rechts nach links gegliedert. Alle Angaben werden aus der Corporate S Regular in 7,5 Pt. mit einem Zeilenabstand von 8,5 Pt. gesetzt, aus der Sonderfarbe Pantone\xae Cool Gray 11 gedruckt und kann maximal 8 Zeilen umfassen.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '305px',
						'backgroundImage': 'url(LE/1019/L15/3.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '330px',
						top: '178px'
					}
				}
			] // layers 1
		}
	] // apps
}
{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1027/L1/banner_EN.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '750px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '730px',
				width: '480px',
				background: 'url(LE/1027/L1/0.png) 0 50px no-repeat'
			},
			expandText:'Expand',
			collapseText:'',
			leftExpandButton: false,
			tools: {
				1: {
					title: 'Layout and Baseline Grid',
					info: 'The layout grid for all formats is made up of 11 x 12 mm basic elements that are 4 mm apart from each other horizontally and vertically. The baseline grid runs horizontally across the format in 2-mm increments.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1027/L1/1.png)'
					}
				},
				2: {
					title: 'Horizontal Axis',
					info: 'The horizontal layout grid in the upper third of the page is known as the shoulder. It divides the format into an upper and lower shoulder area. On front covers, the area above the optical axis is reserved for the corporate logotype.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1027/L1/2.png)'
					}
				},
				3: {
					title: 'Design Area and Frame',
					info: 'The design area is larger than the text block and establishes the maximum borders for visuals and colored spaces. If the design area is populated with a visual, a white frame is created automatically as a border.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1027/L1/3.png)'
					}
				},
				4: {
					title: 'Type Area',
					info: 'Text is set only within the type area.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1027/L1/4.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Corporate Logotype',
					info: 'For brochures, the logotype is used only on front covers and centered in Daimler Blue &#8211; Pantone&#174; 534 &#8211; on white. The size of the logotype is in line with the format. Placement of the logotype is determined by the distance from the upper edge of the page to the baseline of the logotype.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1027/L1/5.png)'
					},
					contents: '',
					//alwaysVisible: true,
					arrow: {
						'class':'left',
						left: '320px',
						top: '90px'
					}
				},{
					title: 'Visuals',
					info: 'When using visuals, make sure they comply with the Daimler photo style.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1027/L1/6.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '60px',
						top: '180px'
					}
				},{
					title: 'Headline System',
					info: 'Headlines are set, justified left, in Corporate S Regular. Texts run across the entire width of the type area. Lucent Blue is used for the headline and Premium Blue for the intro. The headline can be above or below the visual.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1027/L1/7.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '140px',
						top: '620px'
					}
				}
			] // layers 1
		}
	] // apps
}
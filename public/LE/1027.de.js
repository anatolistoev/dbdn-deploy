{
	legend: {
		//marginTop: '200px'
	},
	applications:[
		{ // app 1 =================================================================================================================
			scene:{
				initial:{
					height: '150px',
					background: 'url(LE/1027/L1/banner_DE.jpg) no-repeat',
					border: '0px solid #666'
					, 	width: '960px'
					, margin: '0 0 0 0'
				},
				open:{
					height: '750px',
					background: '#e8e9ea',
					width: '940px'
					, margin: '0 0 0 20px'
				}
			},
			css: {
				position: 'absolute',
				height: '730px',
				width: '480px',
				background: 'url(LE/1027/L1/0.png) 0 50px no-repeat'
			},
			expandText:'Detailansicht &ouml;ffnen',
			collapseText:'',

			leftExpandButton: false,
			tools: {
				1: {
					title: 'Gestaltungsraster',
					info: 'Der Gestaltungsraster setzt sich fu\u0308r alle Formate aus 11 x 12 mm gro\xdfen Grundelementen zusammen, die\nhorizontal und vertikal in einem Abstand von 4 mm zueinander stehen. Der Grundlinienraster zieht sich in 2-mm-\nSchritten horizontal u\u0308ber das Format.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1027/L1/1.png)'
					}
				},
				2: {
					title: 'Horizontale Achse',
					info: 'Die horizontale Gestaltungsachse im oberen Drittel einer Seite wird als \u201eSchulter\u201c bezeichnet. Sie teilt das\nFormat in einen \xdcber- und Unterschulterbereich. Auf Titelseiten ist die Fl\xe4che oberhalb der optischen Achse\ndem Unternehmenszeichen vorbehalten.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1027/L1/2.png)'
					}
				},
				3: {
					title: 'Gestaltungsfl\xe4che und Rahmen',
					info: 'Die Gestaltungsfl\xe4che ist gr\xf6\xdfer als der Satzspiegel und begrenzt die maximale Ausdehnung der Bilder und\nFarbfl\xe4chen. Wird die Gestaltungsfl\xe4che mit einem Bildmotiv ausgefu\u0308llt, entsteht so automatisch ein wei\xdfer\nRahmen als Begrenzung.',
					css: {
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1027/L1/3.png)'
					}
				},
				4: {
					title: 'Satzspiegel',
					info: 'Schrift wird nur innerhalb des Satzspiegels gesetzt.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						backgroundImage: 'url(LE/1027/L1/4.png)'
					}
				},
				5:false,
				6:false
			},
			layers: [
				{
					title: 'Unternehmenszeichen',
					info: 'Bei Broschu\u0308ren wird das Unternehmenszeichen nur auf den Titelseiten verwendet und zentriert in Daimler Blue \u2013\nPantone\xae 534 \u2013 auf Wei\xdf gesetzt. Die Gr\xf6\xdfe des Unternehmenszeichens ist auf das Format abgestimmt. Fu\u0308r\ndie Positionierung des Zeichens gilt der Abstand vom oberen Formatrand bis zur Grundlinie des Zeichens.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1027/L1/5.png)'
					},
					contents: '',
					//alwaysVisible: true,
					arrow: {
						'class':'left',
						left: '320px',
						top: '90px'
					}
				},{
					title: 'Bild',
					info: 'Beim Einsatz von Bildern sollte darauf geachtet werden, dass sie dem Daimler-Fotostil entsprechen.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1027/L1/6.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '60px',
						top: '180px'
					}
				},{
					title: '\xdcberschriftensystematik',
					info: '\xdcberschriften werden aus der Corporate S Regular linksbu\u0308ndig gesetzt. Die Zeilen u\u0308ber die komplette Breite des\nSatzspiegels. Lucent Blue wird fu\u0308r die \xdcberschrift und Premium Blue fu\u0308r die Einleitung eingesetzt. Die\n\xdcberschrift kann ober- oder unterhalb des Bildes stehen.',
					css:{
						"left": '0px',
						"top": '50px',
						"width": '480px',
						"height": '680px',
						'backgroundImage': 'url(LE/1027/L1/7.png)'
					},
					contents: '',
					arrow: {
						'class':'down',
						left: '140px',
						top: '620px'
					}
				}
			] // layers 1
		}
	] // apps
}
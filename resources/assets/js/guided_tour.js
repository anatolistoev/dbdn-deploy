var guided_tour ={
    specialPages : {},
    cookie : 'guided_tour_cookie',
    start_cookie : 'guided_tour_cookie_started',
    page: '',
    isStarted: false,
    elements : [],
    start : function(page){
        var that = this;
        if(typeof generateTourElements === "function" && !docCookies.hasItem(this.cookie) 
                && docCookies.hasItem('viewed_cookie_policy')
                && ((page != 'home' && docCookies.hasItem(this.start_cookie)) || page == 'home')
          ) {
            this.elements = generateTourElements();
            this.page = page;
            switch(page) {
                case 'home':
                    this.elements[2].id = 'hbA';
                    this.elements[2].pos = 'right';
                    this.elements[2].scroll = 260;
                    this.elements[3].id = 'searchDiv';
                    this.elements[3].pos = 'top';
                    this.elements[3].scroll = 140;
                    this.elements[4].id = 'heading-title';
                    this.elements[4].pos = 'right';
                    this.elements[4].top = 20;
                    this.elements[4].scroll = 740;
                    this.elements[5].id = 'profile';
                    this.elements[5].scroll = 0;
                    this.elements[5].left = 30;
                    this.elements[6].id = 'brandSelect';
                    this.elements[6].scroll = 0;
                    this.generateTourHtml(this.elements, 0);
                    break;
                case 'brand':
                    this.elements[0].id = 'rubrics';
                    this.elements[0].scroll = 500;
                    this.elements[1].id = 'heading-title';
                    this.elements[1].pos = 'right';
                    this.elements[1].top = 10;
                    this.elements[1].scroll = 715;
                    this.elements[2].id = 'help';
                    this.elements[2].pos = 'top';
                    this.elements[2].scroll = 500;
                    this.generateTourHtml(this.elements, 1);
                    break;
                case 'identity':
                    this.elements[0].id = 'rubrics';
                    this.generateTourHtml(this.elements, 2);
                    break;
                case 'manuals':
                    this.elements[0].id = 'L2ID2094';
                    this.elements[0].scroll = 200;
                    this.generateTourHtml(this.elements, 3);
                    break;
                case 'content':
                    $('.inline_zoom').first().attr('id', 'first_inline_zoom');
                    $('.tooltip').first().attr('id', 'first_tooltip');
                    this.elements[0].id = 'basic_nav_closed';
                    this.elements[0].pos = 'right';
                    this.elements[0].scroll = 0;
                    this.elements[0].top = 20;
                    this.elements[1].id = 'dot_items';
                    this.elements[1].pos = 'left';
                    this.elements[2].id = 'print-icon';
                    this.elements[2].scroll = 200;
                    this.elements[2].pos = 'left';
                    this.elements[2].top = 20;
                    this.elements[3].id = 'links-icon';
                    this.elements[3].pos = 'left';
                    this.elements[3].top = 20;
                    this.elements[3].scroll = 200;
                    this.elements[4].id = 'first_inline_zoom';
                    this.elements[4].pos = 'left';
                    this.elements[4].scroll = 630;
                    this.elements[5].id = 'first_tooltip';
                    this.elements[5].pos = 'left';
                    this.elements[5].top = 30;
                    this.elements[5].scroll = 500;
                    this.elements[6].id = 'nav_button_right';
                    this.elements[6].pos = 'left';
                    this.generateTourHtml(this.elements, 4);
                    break;
                case 'practice':
                    this.elements[0].id = 'L2ID971';
                    //this.elements[0].pos = 'left';
                    this.elements[0].left = 312;
                    this.generateTourHtml(this.elements, 5);
                    break;
                case 'bp_content':
                    this.elements[0].id = 'basic_nav_closed';
                    this.elements[0].pos = 'right';
                    this.elements[0].scroll = 0;
                    this.elements[0].top = 20;   
                    this.elements[1].id = 'comment-form-header';
                    this.elements[1].pos = 'left';
                    this.elements[1].top = 15;
                    this.elements[1].scroll = 6200;
                    this.generateTourHtml(this.elements, 6);
                    break;
                case 'download':
                    this.elements[0].id = 'L2ID1384';
                    this.elements[0].left = 324;
                    this.generateTourHtml(this.elements, 7);
                    break;
                case 'd_content':
                    this.elements[0].id = 'basic_nav_closed';
                    this.elements[0].pos = 'right';
                    this.elements[0].scroll = 0;
                    this.elements[0].top = 20;
                    this.elements[1].id = 'downloadCol2';
                    this.elements[1].pos = 'right';
                    this.elements[1].scroll = 300;
                    this.elements[2].id = 'download_actions';
                    this.elements[2].scroll = 770;         
                    this.generateTourHtml(this.elements, 8);
                    break;
                default:

            }
            $('#joyRideTipContent').joyride({
              autoStart : true,
                scroll : false, 
                postStepCallback: function(index, currentTip){ // A method to call after each step
                    var scroll = this.$li.attr('data-after_scroll');
                    if($.isNumeric(scroll)){
                        $(window).scrollTop( scroll );
                        $(window).trigger('scroll');
                    }                
                },
                preStepCallback: function(index, next){  // A method to call before each step
                    var scroll = this.$li.attr('data-scroll');
                    if($.isNumeric(scroll)){
                        $(window).scrollTop( scroll );
                        console.log('Set Scroll: ' + scroll);
                        $(window).trigger('scroll');
                        console.log('After Trigger Scroll: ' + $(window).scrollTop());
                    }
                    if(that.page ==  'd_content' && index == that.elements.length - 1) {
                        $('div[data-index="' + index + '" ]').find('a:contains("Next")').css({"display":"none"});
                    }
                },
                postRideCallback: function(index, current, isAborted){ // A method to call once the tour closes (canceled or complete)
                    if(isAborted){
                        var weekFromNow = new Date();
                        weekFromNow.setDate( weekFromNow.getDate() + 7 );
                        docCookies.setItem(that.cookie, 1, weekFromNow, '/', null, true);
                        if(docCookies.hasItem(that.start_cookie)){
                            docCookies.removeItem(that.start_cookie, '/', null);
                        }
                        that.isStarted = false;
                    }else{
                        switch(that.page) {
                            case 'home':
                                $('#brandMenu a')[0].click();
                                break;
                            case 'brand':
                                $('#L2ID1717 a')[0].click();                            
                                break;
                            case 'identity':                                                        
                                $('#L2ID2094 a')[0].click();                            
                                break;
                            case 'manuals':
                                window.location = '/Daimler_Corporate_Logotype';
                                //$('.manuals .page:first-of-type a')[0].click();                            
                                break;
                            case 'content':                                                        
                                $('#L2ID971 a')[0].click();                            
                                break;
                            case 'practice':
                                window.location = '/Daimler_2016_Annual_Meeting';
                                //$('#oneperframe1 .page:first-of-type a')[0].click();                           
                                break;
                            case 'bp_content':                                                        
                                $('#L2ID1384 a')[0].click();                           
                                break;
                            case 'download':
                                window.location = '/Daimler_Brochures_InDesign_DINA4_Templates';
                                //$('.downloads .page:first-of-type a')[0].click();                       
                                break;
                            case 'd_content':
                                that.isStarted = false;
                                var weekFromNow = new Date();
                                weekFromNow.setDate( weekFromNow.getDate() + 7 );
                                docCookies.setItem(that.cookie, 1, weekFromNow, '/', null, true);
                                if(docCookies.hasItem(that.start_cookie)){
                                    docCookies.removeItem(that.start_cookie, '/', null);
                                }
                                break;
                            default:
                        }
                    }
                },
                preRideCallback: function(){ // A method to call before the tour starts (passed index, tip, and cloned exposed element)
                    var hourFromNow = new Date();
                    hourFromNow.setHours(hourFromNow.getHours() + 1);
                    docCookies.setItem(that.start_cookie, 1, hourFromNow, '/', null, true);
                    that.isStarted = true;
                },
                postCalculateCallback: function(li_element, currentTip, position){
                     var top = this.$li.attr('data-top');
                     if($.isNumeric(top) && (position == 'left' || position == 'right')){
                         var elTop = currentTip.css('top');
                         elTop = parseInt(elTop.substring(0, elTop.indexOf("px"))) - parseInt(top);
                         currentTip.css({
                             top: elTop + 'px'
                         })
                    }
                    var left = this.$li.attr('data-left');
                    if($.isNumeric(left) && position == 'bottom'){
                         var elLeft = currentTip.css('left');
                         elLeft = parseInt(elLeft.substring(0, elLeft.indexOf("px"))) - parseInt(left);
                         currentTip.css({
                             left: elLeft + 'px'
                         })
                         if(that.page == 'practice' || that.page ==  'download'){
                             currentTip.find('span.joyride-nub').css({'left': 'auto', 'right':'25px'});
                         }else{
                            currentTip.find('span.joyride-nub').css('left', '25px');
                            }
                    }
                }
                
                
            });
        }
    },
    generateTourHtml: function(elements, start){
        var html = '<ol id="joyRideTipContent">';
        var len = elements.length;
        for(var i= 0; i< len; i++){
            if(start == 0 && (i == 0 || i == 1)){
                if(i == 0){
                     html +=  '<li data-text="'+js_localize['guided_tour.button_start']+'" data-scroll="'+elements[i].scroll +'">'; 
                }else{
                     html +=  '<li data-text="'+js_localize['guided_tour.button_next']+'">'; 
                }
            }else if(start == 8 && i == len - 1){
                html +=  '<li>'; 
            }else{
                html +=  '<li data-left="'+elements[i].left+'" data-top= "'+elements[i].top+'" data-after_scroll = "'+elements[i].after_scroll +'" data-scroll="'+elements[i].scroll +'" data-id="'+ elements[i].id + '" data-text="'+js_localize['guided_tour.button_next']+'" data-options="tipLocation:'+elements[i].pos+'">';
            }        
            html += '<div class="bubble-container">';
            html += '<h2>'+ elements[i].header + '</h2>';
            html +=  '<div class="text">'+ elements[i].text + '</div>';
//            if(!(start == 0 && i == 0)){
//                html += '<div class="tour_pages">'+(start + i) +'/25</div>'
//            }
            if(start == 8 && i == len - 1){
                 html += '<a class="tour_link" href="/">'+js_localize['guided_tour.link_home']+'</a>';
            }
            html += '</div></li>';
        }
        html += '</ol>';
        $('body').append(html);
    }
};
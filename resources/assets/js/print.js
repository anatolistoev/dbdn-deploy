//==========================================================================
function SetDateLabel(){
	d = new Date();
	tl = document.getElementById('timeLabel');
	tl.innerHTML = d.getDate()+'.'+(d.getMonth() + 1)+'.'+d.getFullYear()+' '+d.getHours()+':'+((d.getMinutes()>9)?d.getMinutes():'0'+d.getMinutes());
	fixIcons();
}
//==========================================================================
function fixIcons(){
	$('.backlink, .arrowlink, .zoomlink, .gallerylink, .videolink, .audiolink, .contactlink, .toolkitlink, .externallink, .readmore, .gototop, .prevpage, .nextpage, .termNormal, .termActive, .RemoveLink, .AddDownloadLink, .downloadSelLink, .textLink, .teaserLink').each(function(){
		$this = $(this);
		src = $this.css('background-image');
		src = (src.indexOf('"') > -1) ? src.substr(5, src.length-7) : src.substr(4, src.length-5);
		//pos = $this.css('background-position');
		//if(pos != null && pos.indexOf("right") > -1){
			//$this.html($this.html() + '<img src="'+src+'" style="border:0px; margin:0px 0px 0px 3px; vertical-align: middle;">');
		//}else{
			$this.html('<img src="'+src+'" style="border:0px; margin:0px 3px 0px 0px; vertical-align: middle;">'+ $this.html());
		//}
		$this.css('background','none');
	});
}
//==========================================================================
function TransferContents(){
	var html='';
	for (var i=0;i<window.frames.length;i++){
		//FixPath(window.frames[i].document);
		html += window.frames[i].document.body.innerHTML;
	}
	var col = document.getElementsByTagName("IFRAME");
	for(var j=0; j<col.length;j++){
		col[j].style.display = 'none';
	}
	document.getElementById("printDiv").innerHTML = html;
	/*FixPath(document);*/
}

//==========================================================================

jQuery(document).ready(function($){
    var imageData = [];
    var imagePostData = [];
    var images_list = $('img.zoom, img.inline_zoom, img.simple_no_zoom, img.explainer, img.gallery');
	images_list.first().addClass('first_image');
	if(!images_list.length) {
		window.print();
	}

	images_list.each(function(){
		var me = $(this);
		me.wrap('<div class="image_wrapper cf '+this.className+' "></div>');
//		/me.parent().append('<div class="galMover" style="display:none;"></div>');
		var wrapper = me.parent();
		if(me.hasClass('first_image')){
			wrapper.addClass('first_image');
		}
		imageData.push({ 'wrapper': wrapper, 'me': $(this) });
		imagePostData.push(me.attr('data-gallery').split(','));
	});

	getData(imageData, imagePostData);

});

$( document ).on("ImagesLoaded", function(){
    window.imagesToLoad --;
    if( window.imagesToLoad == 0){
        TransferContents();
        fixListAfterImageSpace();
        window.print();
    }
});

function generateExplainer(container, data, img) {

	var now = new Date();
	var explainerTitle = 'explainer_'+now.getTime();
	var newHTML = '';
	var images = data.images;
    var currentImage = 0;
	var lowresSrc, lowresSrc2x, image, containerHeight;
	var files_str = '';
	$.each(img.attr('data-gallery').split(','),function(index,value){
		files_str += '&files[]='+value;
	});

	newHTML = '<div class="explainer_gallery image_item">';

	newHTML+='</div>';
	container.html(newHTML);
	for(var z=0; z < images.length; z++){
		image=images[z];
		lowresSrc = image['lowres'];
        lowresSrc2x = image['lowres2x'];
			if (!lowresSrc) {
				lowresSrc = image['path'];
			}

			newHTML = '<div class="image_container" >\
						<a class="explainer_layover" class="LE-tool-layer-'+z+'" href="'+image['path']+'" id="'+explainerTitle+'_'+z+'">\
						</a></div><div class="image_formatter_ie">&nbsp;</div>';

			container.find('.explainer_gallery').append(newHTML);


			var newImg = new Image();
			var containerWidth = 740;
			if(img.hasClass('first_image')){
				containerHeight = 493;
			}else{
				containerHeight = 520;
			}
			var el = $(container).find('a[href*="'+image['path']+'"]');;
            newImg.el = el;
            $(newImg).on('load error', function(event){
                if(event.type == 'load'){
                    calculateImageRatio(this, containerHeight, containerWidth);
                    $(this).attr("unselectable","on");
                    $(this.el).prepend(this);
                }

                currentImage ++;
                if(currentImage == images.length){
                    $(document).trigger("ImagesLoaded");
                }
            });

			newImg.src = lowresSrc2x != undefined ? lowresSrc2x : lowresSrc;
            newImg.border="0";
		}
}

function generateImage(container, data, img) {
	var newHTML = '';
	var images = data.images;
    var currentImage = 0;
	var lowresSrc, lowresSrc2x, image, containerHeight;
	for(var z=0; z < images.length; z++){
		image=images[z];
		lowresSrc = image['lowres'];
        lowresSrc2x = image['lowres2x'];
		if (!lowresSrc) {
			lowresSrc = image['path'];
		}
		newHTML = '<div class="image_item cf">\
			<div class="image_container">\
                <a href="' + lowresSrc + '" class="image_layover"></a>\
                <div class="image_formatter_ie">&nbsp;</div>\
            </div>';
        if ((image['descr'] !== '' && typeof(image['descr']) !== 'undefined')) {
            newHTML += '<div class="image_text"><div>' + image['descr'] + '</div></div>'
        }
        newHTML += '</div>';
		container.append(newHTML);
		if (images.length == 1 && $(img).hasClass('hotspot')) {
			var hst_json = JSON.parse($(img).attr('data-hotspot'));
			var html = '<div id="LE-pois-0" class="pois">';
			for (var i = 0; i < hst_json.points.length; i++) {
				html += '<div class="poi ' + hst_json.points[i].pos + '" style="left: ' + hst_json.points[i].left + '; top: ' + hst_json.points[i].top + ';">' + (i + 1) + '</div>';
			}
			html += '</div>';
			$(container).find('.image_item .image_container > a').append(html);
			var image_text = $(container).find('.image_item .image_text');
            if(image_text.length == 0){
                image_text = $('<div class="image_text"></div>');
                $(container).append(image_text);
            }
            $(image_text).html(hst_json.description);
		}

		var newImg = new Image();
		var containerWidth = 740;
		if(img.hasClass('first_image')){
            containerHeight = 493;
		}else{
			containerHeight = 520;
		}
        newImg.el = $(container).find('a[href*="'+lowresSrc+'"]');
        $(newImg).on('load error', function(event){
            if(event.type == 'load'){
                calculateImageRatio(this, containerHeight, containerWidth);
                $(this).attr("unselectable","on");
                $(this.el).prepend(this);
            }

            currentImage ++;
            if(currentImage == images.length){
                $(document).trigger("ImagesLoaded");
            }
        });
		newImg.src = lowresSrc2x != undefined ? lowresSrc2x : lowresSrc;
        newImg.border="0";
	}
}

function fixListAfterImageSpace() {
	console.log('Enter UL/OL space fixing funcion');
	$('ul, ol').each(function() {
		var listPrevSibling = $(this).prev();
		var imageWrapper = $(listPrevSibling).find('.image_wrapper');
		if(imageWrapper) {
			var wrapper = imageWrapper[0];
			if($(wrapper).hasClass('image_wrapper')) {
				$(this).css({'margin-top': '0'});
			}
		}
	})
}

function getData(imageData, imagePostData){
    if (imagePostData.length > 0) {
        var data = {
                'files': imagePostData,
                '_token': $('#token').val()
            };
        $.ajax({
            type : 'POST',
            url : paths.gallery,
            data : data,
            success : function(json){
                var data = json.response;
                for (var i = 0; i < data.length; i++) {
                    var element = imageData[i];
                    if($(element.me).hasClass('explainer')){
                        generateExplainer(element.wrapper, data[i], element.me);
                    }else{
                        generateImage(element.wrapper, data[i], element.me);
                    }
                    element.me.hide();
                }
                window.imagesToLoad = data.length;
                if(data.length == 0){
                    window.imagesToLoad = 1;
                    $(document).trigger( "ImagesLoaded" );
                }
            }
        });
    }
}

function calculateImageRatio(image, containerHeight, containerWidth){
    var height = image.height;
    var width = image.width;
    var imgWidth, imgHeight;

    if(width < containerWidth && height < containerHeight){
        imgHeight = 'auto';
        imgWidth = 'auto';
    }else if(width / height > containerWidth / containerHeight){
        imgWidth = containerWidth+'px';
        imgHeight = 'auto';
    }else{
        imgWidth = 'auto';
        imgHeight = containerHeight+'px';
    }

    $(image).css({'width': imgWidth, 'height' : imgHeight});
}
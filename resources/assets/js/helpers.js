// Test via a getter in the options object to see if the passive property is accessed
var supportsPassive = false;
try {
    var opts = Object.defineProperty({}, 'passive', {
        get: function() {
            supportsPassive = true;
        }
    });
    window.addEventListener("test", null, opts);
} catch (e) {}


(function(namespace) {
    namespace._passiveEL = {};

    namespace.addPassiveEventListener = function(event, fn, prepend) {

        if(!namespace._passiveEL[event])
        {
            namespace._passiveEL[event] = [];
            namespace._passiveEL[event+"_timeout"] = false;

            namespace.addEventListener(event, function()
            {
                if(true) // set to true to disable timeout
                {
                    namespace._passiveEL[event].forEach(function(fn){fn()});
                }
                else
                {
                    if(namespace._passiveEL[event+"_timeout"])
                        clearTimeout(namespace._passiveEL[event+"_timeout"]);

                    namespace._passiveEL[event+"_timeout"] = setTimeout(function(){
                        namespace._passiveEL[event+"_timeout"] = false;
                        namespace._passiveEL[event].forEach(function(fn){fn()});
                    }, 50);
                }

            }, supportsPassive ? { passive: true } : false);
        }

        if(prepend)
            namespace._passiveEL[event].unshift(fn);
        else
            namespace._passiveEL[event].push(fn);

    }
})(window);





(function(namespace) {
    if ('replaceState' in history) {
        namespace.replaceHash = function(newhash) {
            if(!newhash) return;
            if ((''+newhash).charAt(0) !== '#')
                newhash = '#' + newhash;
            history.replaceState('', '', newhash);
        }
    } else {
        var hash = location.hash;
        namespace.replaceHash = function(newhash) {
            if(!newhash) return;
            if (location.hash !== hash)
                history.back();
            location.hash = newhash;
        };
    }

    if('pushState' in history) {
        namespace.setHash = function(newhash) {
            if(!newhash) return;
            history.pushState(null, null, '#'+newhash);
        }
    } else {
        namespace.setHash = function(newhash) {
            if(!newhash) return;
            location.hash = newhash;
        }
    }

})(window);



(function($) {
    $.fn.sticky = function (sticky) {
        $(this).each(function() {
            var el = $(this),
                tagName = el.prop('tagName'),
                no_sticky_class = 1 + $.inArray(tagName, ['HEADER','FOOTER']);

            if(sticky && !el.hasClass('sticky'))
            {
                if ('HEADER' == tagName)
                {
                    if ($('header .hamburger.active').length > 0) {
                        return;
                    }

                    $('<div>')
                        .attr('id','header_paceholder')
                        .css('height', el.outerHeight()+'px')
                        .prependTo('body');
                }

                if (isTabletViewport()
                    && el.filter('section.filters').length > 0
                    && $('#tabletFilters > .menu_label.active, #tabletFilters #filter-panel-selected.active').length > 0) {
                    return;
                }

                el.addClass('sticky');
                if(no_sticky_class)
                    el.removeClass('no-sticky');

            }
            else if(!sticky && el.hasClass('sticky'))
            {
                el.removeClass('sticky');
                if(no_sticky_class)
                    el.addClass('no-sticky');
                if('HEADER' == tagName)
                    $('#header_paceholder').remove();
            }
        });

        return this;
    };
}(jQuery));


(function($) {
     $.fn.scrollTo = function(callBack) {

        var el = $(this);
        if(!el.length)
            return false;

        var dst = $(document).scrollTop(),
            offset = el.offset().top,    // - 50 - $('header.no-sticky').height();
            direction = dst > offset ? 'up' : 'dn', // up => true, down => false,
            filtersHeight = $('.basic_nav_opened.filters').height() || 0,
            headerHeight = $('header').height(),
            headerSticky = headerHeight < 100,   // 50 - sticky, 200 - no-sticky
            scrollTime;

        offset -= 10; // little air above target element

//        if(headerSticky)
//            offset -= 50; // Dusty: jQuery wrong calculates header.sticky height into offset
        if('up' == direction) {
            offset -= 70;
        } else {
            offset -= 20;
        }

        // Correctio for the filters
        if(filtersHeight)
        {
            offset -= filtersHeight;
        }

        if('replaceHash' in window && el.attr('id'))
            window.replaceHash(el.attr('id'));

        scrollTime = Math.abs(offset - dst);

        if (scrollTime > animationTime * 2)
            scrollTime = animationTime * 2;

        if (TABLET) {
            scrollTime = 0;
        }

        $("html, body")
            .animate({scrollTop: Math.ceil(offset) + "px"}, scrollTime, "swing")
            .promise()
            .then(function() {
                if("function" === typeof callBack)
                    callBack();
            });

        return this;
    };
}(jQuery));


(function($) {
     $.fn.inViewport = function(partial, partiallyVisibleOnX) {
        if(!this.length)
            return false;

        partiallyVisibleOnX = partiallyVisibleOnX || false;
        var rec = this[0].getBoundingClientRect(),
            $w = $(window), vpWidth = $w.width(), vpHeight = $w.height(),
            tViz = rec.top >= 0 && Math.round10(rec.top, 0) < vpHeight,
            bViz = rec.bottom > 0 && Math.round10(rec.bottom , 0) <= vpHeight,
            lViz = rec.left >= 0 && Math.round10(rec.left, 0) < vpWidth,
            rViz = rec.right > 0 && Math.round10(rec.right, 0) <= vpWidth,
            vVisible   = partial ? tViz || bViz : tViz && bViz,
            hVisible = partial ? lViz || rViz : lViz && rViz;

        return (vVisible && hVisible) || (partiallyVisibleOnX && hVisible);
        /*
        var rect = this[0].getBoundingClientRect();
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && // or $(window).height()
            rect.right <= (window.innerWidth || document.documentElement.clientWidth) // or $(window).width()
        )
        */

    }
}(jQuery));



(function($) {
    $.fn.splitList = function(rpc, skip) {

        var el = $(this),
            tag = el.prop("tagName"),
            lis,
            cols,
            li,
            width = 0,
            owidth = 0,
            i,
            idx,
            newpos,
            liarr = [];

        /*
        newcol = floor( index / rpc)
        colpos = index % rpc
        newpos = newcol + cols*colpos
        */

        if(!el.length)
            return false;

        if(!skip)
            skip = 0;

        if("OL" != tag && "UL" != tag)
            console.warn("Not list tag");

        else if(!el.is(":visible"))
            console.warn("Only visible elements");

        else
        {
            el.children("li.puttyLI").remove();
            lis = el.children("li");

            if(lis.length - skip >= rpc)
            {

                el.removeAttr("style").css("white-space","normal");
                lis.removeAttr("style").css({"white-space":"nowrap","display":"inline-block"});

                cols   = Math.ceil( (lis.length - skip)/rpc );

                for(i = 0; i < lis.length; i++)
                {
                    li = $(lis[i]);
                    width  = Math.max(width, li.width());
                    owidth = Math.max(owidth, li.outerWidth());
                }

                while (cols*(rpc-1) >= lis.length - skip)
                    --rpc;


                for(i = 0; i < lis.length; i++)
                {
                    li = $(lis[i]);
                    if(!li.attr("orgpos"))
                        li.attr("orgpos",i);

                    idx = parseInt(li.attr("orgpos"));

                    li.css("width", idx < skip ? cols*owidth : width + 'px');

                    newpos = idx < skip ? idx : Math.floor((idx-skip)/rpc) + (cols)*((idx-skip)%rpc) + skip;

                    liarr[newpos] = li.detach();
                }

                for(i = 0; i < cols*rpc + skip; i++)
                {
                    if(liarr[i])
                        el.append(liarr[i]);
                    else
                        el.append($("<li>")
                            .attr("class","puttyLI")
                            .css({"width":width+'px',"height":"20px","display":"inline-block","visibility":"hidden"})
                            .html("&nbsp;"));
                }
            }
        }

        return this;
    };
}(jQuery));


function isEvent(event)
{
    return (event && event.target) ? true : false;
}


function stopEvent(event)
{
    if (isEvent(event)) {
        event.preventDefault();
        event.stopPropagation();
    }
    return event;
}


function imgTest(p) {
    if(!p) p = 1000;
    $('img').each(function(){
        var t = $(this), vW = t.width(), vH = t.height();
        if(0 === vW || 0 === vH ) return true;
        var vR = Math.round((vW/vH)*p)/p, i = new Image();
        i.src = t.attr('src');
        i.onload = function() {
            var oW = i.width, oH = i.height, oR = Math.round((oW/oH)*p)/p, src = i.src;

            if( (oW == vW && oH == vH) || oR == vR ) return;
            console.log('Vis:', vW+'x'+vH,'('+vR+')','Org:',oW+'x'+oH,'('+oR+')',src);
        }
    });
}


/*
function parcali() {
    return  $('*').filter(function() {
        var that = $(this), pos = that.css("position");
        return ('fixed' === pos || "absolute" === pos ) && that.inViewport();
    });
}
*/

var Menu = function() {

    this.menues = {};

    var _this = this;

    $(document).ready(function(){
        $('<div>').attr('id','menu_overlay').prependTo('body');
    });

    this.bodyWidthDiff = 0;

    this.show = function (event) {

        var el = event ? $(event.target) : false,
            fromNav = el ? el.closest('#rubrics').length : false,
            ref = fromNav ? el.closest('li').attr('id') : false,
            basic_nav_opened = $('.basic_nav_opened:not(.filters)'),
            showArguments = arguments;

        if (fromNav) {

            if ( !basic_nav_opened.length || (basic_nav_opened.inViewport(true) && basic_nav_opened.attr('ref') === ref) ) {
                // do nothing; link from menu; just let browser navigate to link
                return this;
            } else {
                stopEvent(event);
            }
        }

        // Prevent scrolling on macOS tablet devices
        if (TABLET
            && isDesktopViewport()
            && typeof document.body.style['webkitOverflowScrolling'] !== 'undefined') {
            $('html').css('position', 'fixed');
        }

        basic_nav_opened.attr('ref', ref).css('z-index',10);
        _this.overlay(1);

        if (ref) {
            $('#rubrics li').removeClass('activeTmp');
            el.closest('li').addClass('activeTmp');

            if (!_this.menues[ref]) {
                _this.fetchMenuContent(ref, function (data) {
                    data = data.trim();
                    if (el && ('' == data || '<span id="menu_close" class="popup-close"></span>' == data) ) { // quick and durty fix (task #14611)
                        _this.hide(function () {
                            window.location.href = el.attr('href');
                        });
                    } else {
                        _this.menues[ref] = data;
                        _this._show(ref, showArguments);
                    }
                });
            } else {
                _this._show(ref, showArguments);
            }
        } else {
            _this._show(false, showArguments);
        }

        return _this;
    },

    this.fetchMenuContent = function (menuId, callback) {
        $.get('menu/' + menuId, function (data) {
            if (typeof callback === 'function') {
                callback(data);
            }
        });
    },

    this._show = function (ref, showArguments) {
        var basic_nav_closed = $('.basic_nav_closed'),
            basic_nav_opened = $('.basic_nav_opened:not(.filters)'),
            header = $('header'),
            hTop = header.offset().top + header.height() - $(window).scrollTop();

        if (isTabletViewport()) {
            var $rubricsWrapper = header.find('#rubrics_wrapper');
            hTop = header.offset().top + header.height() + $rubricsWrapper.outerHeight();
        }

        if (ref && _this.menues[ref]) {
            basic_nav_opened.find('div.basic_nav.basic_menu').html(_this.menues[ref]);
        }

        $(document).trigger('afterAttachingMenuChildItems.DBDN_General');

        // Load images for the current resolution
        imageProcess.setResolutionPictures('#menu_carousel_wrapper');
        imageProcess.lazyLoadWithCallBack($('#menu_carousel_wrapper img.lazyload-thumb'));
        imageProcess.updateLazyload();
        $('#menu_slider').bind('wheel', imageProcess.updateLazyload);
        $('.next.slider_hover, .prev.slider_hover').click(imageProcess.updateLazyload);

		/*
        if($('#fullsize_banner').length || !header.inViewport(true))
        {
            window.scrollTo(0, hTop - 50);
            window.lastScrollTop = $(window).scrollTop() + 1;
            stickyMess();
        }
        */

        if(basic_nav_closed.length)
            basic_nav_closed.hide();

        //flyoutSearch.hide();
        if (!isTabletViewport()) {
            closeShowedPopup('.basic_nav');
        } else {
            closeShowedPopup('.basic_nav, .hamburger');
        }

        basic_nav_opened.css('visibility', 'hidden').show(0);

        $('.menu_carousel').addClass('hidden');

        if ($('.menu_carousel').inViewport() || DBDN.utils.isForcedDesktopMode()) {
           $('.menu_carousel').removeClass('hidden').show(0, createMenuSlider);
        }

        basic_nav_opened.hide(0).css('visibility', 'visible');

        var slideDuration = animationTime;

        if (isTabletViewport()) {
            slideDuration = 0;
        }

        basic_nav_opened
                .css("top", hTop + "px")
                .slideDown(slideDuration / 2)
                .promise()
                .then(function() {
                    _this.overlay(1);
                    // stupid callback - maybe never needed ?!
                    for (var i = 0; i < showArguments.length; i++) {
                        if("function" === typeof showArguments[i])
                            showArguments[i]();
                    }
                });

        return _this;
    },

    this.hide = function (callBack, hideDuration) {
        hideDuration = (typeof hideDuration !== 'undefined' && (hideDuration === 0 || hideDuration > 0)) ? hideDuration : animationTime;

        if (hideDuration < 1) {
            $('.basic_nav_opened:not(.filters)').hide();
            hideProcessComplete();
        } else {
            $('.basic_nav_opened:not(.filters)').slideUp(hideDuration, hideProcessComplete);
        }

        function hideProcessComplete() {
            $('html').css('position', '');

            $(this).css("top","").attr('ref', '');
            $('#rubrics li').removeClass('activeTmp');
            $('#rubrics li.active').addClass('activeTmp');

            var basic_nav_closed = $('.basic_nav_closed');

            if(basic_nav_closed) {
                basic_nav_closed
                    .show()
                    .promise()
                    .then(function() {
                        if("function" === typeof callBack)
                            callBack();
                    });
            } else {
                if("function" === typeof callBack)
                    callBack();
            }
        }

        return _this.overlay(0);
    },

    this.overlay = function (on, el, callback) {

        var menu_overlay = $('#menu_overlay'),
            activated = menu_overlay.is(':visible');

        menu_overlay.css('z-index', el && on ? $(el).css('z-index') - 1 : '');

        if (on && !activated)
        {
            var bw = $('body').width();

            $('html').css('overflow','hidden');

            var bwdiff = $('body').width() - bw;

            $('body').css('width', bw + 'px');

            $('header.sticky, footer.sticky').css('right', bwdiff + 'px');
            $('header.sticky').css('width', $('header.sticky').width() - bwdiff + 'px');
            $('footer.sticky').css('width', $('footer.sticky').width() - bwdiff + 'px');
            $('#cd-vertical-nav.sticky').css('right', bwdiff + parseInt($('#cd-vertical-nav').css('right')) + 'px' )
            $('.basic_nav_opened').css('width', $('.basic_nav_opened').width() + 'px');
            checkFiltersCount();
            menu_overlay.css('border-right-width', bwdiff + 'px').fadeIn(animationTime / 4, function () {
                if (typeof callback === 'function') {
                    callback();
                }
            });
        }
        else if((on && activated) || (!on && !activated)) {
            if (typeof callback === 'function') {
                callback();
            }
        }
        else if(!on && activated)
            menu_overlay.fadeOut(animationTime / 4, function(){
                $('html').css('overflow','');
                $('body').css('width', '');
                $('header, footer, #cd-vertical-nav').css({'right':'','width':''});
                $('.basic_nav_opened').css('width', '');
                checkFiltersCount();

                if (typeof callback === 'function') {
                    callback();
                }
            });
        return _this;
    },


    this.resolve = function(loc) {
        var el = $(loc);
        if(el.length)
        {
            if (isTabletViewport()) {
                el.scrollTo();
                closeShowedPopup();
            } else {
                menu.hide(function () { el.scrollTo() });
            }

            return false;
        }
        if (isTabletViewport()) {
            closeShowedPopup();
        } else {
            menu.hide();
        }

        return true;
    }

    function createMenuSlider() {
        var menu_slider = $('#menu_slider');
        if (menu_slider.length > 0 && menu_slider.data().sly === undefined) {
            var wrap = menu_slider.parent();
            new Sly(menu_slider, {
                horizontal: 1,
                itemNav: 'forceCentered',
                smart: 1,
                activateMiddle: 1,
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1,
                startAt: 0,
                scrollBy: 1,
                pagesBar: wrap.find('.pages'),
                activatePageOn: 'click',
                speed: 300,
                elasticBounds: 1,
                easing: 'easeOutExpo',
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,
                // Buttons
                prev: wrap.find('.prev'),
                next: wrap.find('.next'),
                activeClass: 'active'
            }).init();
        }
    }
}

var menu = new Menu();




/*** Flyout Search ***/

var FlyoutSearch = function(toggleSelector) {

    var _this = this;

    $(document).ready(function() {
        _this.el = $(".flyout_search");
        _this.sb = $("a.search_button");
        if(toggleSelector) {
            $(toggleSelector).click(_this.toggle);
            _this.el.find('.popup-close').click(_this.hide);
        }
    });


    this.show = function (event) {
        stopEvent(event);

        closeShowedPopup();
        //menu.hide();
        menu.overlay(true, _this.el);

        _this.sb.addClass('active');
        var height = _this.el.height();
        _this.el.css({display:'block', height:0}).animate({height:height}, animationTime, function(){
            _this.el.find("input[type=text]").focus();
        });
    }

    this.hide = function (event) {
        if(isEvent(event)) {
            stopEvent(event);
            _this.el.animate({height:0},animationTime, function(){
                _this.el.css({display:'none', height:'auto'});
                menu.overlay(false);
            });
        } else {
            _this.el.css({display:'none', height:'auto'});
        }
        _this.sb.removeClass("active");
    }

    this.toggle = function (event) {
        if(_this.sb.hasClass("active")) {
            _this.hide(event);
        } else {
            _this.show(event);
        }
    }

}

var flyoutSearch = new FlyoutSearch('a.search_button');



var Filter = function() {

    var _this = this;

    $(document).ready(function(){

        var fl = $('.filter_label');

        if(!fl.length)
            return;

        if($("#best_practice_stats").length)
            $("#best_practice_stats").addClass('filters');
        if($("#downloads_stats").length)
            $("#downloads_stats").addClass('filters');

        $("span.filter_close").click(function(){
            if(MOBILE)
            {
                switchMobileFilterMenu();
            } else if(isTabletViewport()) {
                closeShowedPopup();

                return false;
            }
            else
            {
                $(this).closest("li.cf.active").removeClass("active");
            }

            menu.overlay(0);
        });


        fl.each(function(){

            var el = $(this),
                t = el.attr("title");

            if(t)
            {
                el.removeAttr("title").attr("popup-title", t);
                el.hover(_this.popupShow, _this.popupHide);
            }

            el.click(_this.expand);
        });

    });


    this.expand = function()
    {
        /*
        var sf = $("section.basic_nav_opened.filters");
        if(sf.length && !sf.hasClass('sticky'))
            window.scrollTo(0, sf.offset().top);
        */

        var el = $(this),
            p  = el.parent(),
            i  = p.find('input'),
            ul = p.find('ul'),
            split, rpc, skip;
            if(p.hasClass('active'))
            {
                p.removeClass('active');
            }
            else
            {
                p.closest('.basic_nav').find('li.active').removeClass('active');
                if(1 == i.length)
                    i[0].click();
                else
                {
                    p.closest('section.search-section').scrollTo();
                    menu.overlay(1);
                    p.addClass('active');
                    split = ul.attr('splitlist');
                    if(split)
                    {
                        split = split.split(',');
                        rpc = parseInt(split[0]);
                        skip = split[1] ? parseInt(split[1]) : 0;
                        ul.splitList(rpc, skip);
                    }
                }
            }

    };


    this.popupShow = function()
    {
        _this.popupHide();

        if($(this).closest("li").hasClass("active"))
            return;

        var popup = $("<div>")
                        .attr({"id":"filterPopup","class":"popup"})
                        .text($(this).attr("popup-title"))
                        .append($("<div>").attr("class","popup-ticker"))
                        .appendTo($(this).parent());

        var height = popup.outerHeight();

        $("section.basic_nav_opened.best_practice").css("z-index","200");
        //popup.css({visibility:'visible', height:0}).animate({height:height},animationTime / 4);
        popup.css({visibility:'visible'});
    };


    this.popupHide = function()
    {
        $("section.basic_nav_opened.best_practice").css("z-index","");
        $("#filterPopup").remove();
    };


    this.hide = function(callBack)
    {
        if("function" === typeof callBack)
            callBack();
    };

    this.change = function(event)
    {
        addSelection(event.target,event);
        sendBrowserRequest();
    };

    this.count = function()
    {
        return $('#filter-panel-selected .filter-tags').not('#filter-example').length;
    };

    this.getCountOfSelectedFilterOptions = function () {
        return this.count();
    };

    this.drop = function(el)
    {
        $(el).closest(".filter-tags").remove();

        if(0 == _this.count())
            _this.clear();
        else
            if(MOBILE){
                sendMobileBrowserRequest();
            }else{
               sendBrowserRequest();
            }

    };

    this.clear = function()
    {
        //window.location.reload();
        $('#filter-panel-selected .filter-tags').not('#filter-example').remove();

        $('.level3_nav input').each(function(){
           if($(this).attr('type') == 'text')
                $(this).val('');
            else
                $(this).prop('checked',false);
        });
        $('.level3_nav .active span').click();

        if(MOBILE) {
            $('#remove_filters').css('display','none');
            $("#filter-panel-selected").removeClass("dropdown active");
            sendMobileBrowserRequest();
            menu.overlay(false);
        } else if (isTabletViewport()) {
            $('div.filter-panel-title').removeClass('active');
            sendBrowserRequest();
        } else {
            $('.filter-panel').hide();
            sendBrowserRequest();
        }
    };


    this.compactSelection = function()
    {
        var f = $("#filter-panel-selected"),
            l = f.find("label").length - 1,
            section = $("#searchDiv").closest("section");

        $("#active_filter_count").html(l);

        if(l > 1)
        {
            f.addClass("dropdown");
            if(!MOBILE) {
                $("#remove_filters").css('display', 'block');
            }
        }
        else
        {
            f.removeClass("dropdown active");
            $('#remove_filters').css('display','none');
        }

        if(l > 0)
        {
            if(MOBILE){
                $(".basic_nav .filter-panel").addClass('no-empty');
                $("section.section_content.best-practice").css("margin-top", "189px");
                $(".downloads_overview").addClass("filtered");
                
            }
            $(".filter_hide").fadeOut();
            if(! section.hasClass("search-section"))
                section.fadeOut();
        }
        else
        {
            if(MOBILE){
                $(".basic_nav .filter-panel").removeClass('no-empty');
                $("section.section_content.best-practice").css("margin-top", "");
                $(".downloads_overview").removeClass("filtered");
            }
            $(".filter_hide").fadeIn();
            if(! section.hasClass("search-section"))
                section.fadeIn();
        }
    };

};


var filter = new Filter();







function stickyMess()
{
    var $window = $(window), scrollTop = $window.scrollTop(),
            scrollTolerance = 4;


        if (scrollTop > window.lastScrollTop + scrollTolerance)  // scroll Down
        {
            $('header, #basic_nav_closed').sticky(false);

            $('section.filters').sticky(scrollTop > 160 + stickyAddH).removeClass('up');

            $('body').removeClass('show-sticky-filters');
        }
        else if (scrollTop < window.lastScrollTop - scrollTolerance)  // scroll Up
        {
            var isSticky = scrollTop > 140 && !$('#hamburger').hasClass('active') && !(MOBILE && $('.flyout_menu').is(":visible"));
            $('header').sticky(isSticky);
            if (isSticky) {
                $('body').addClass('show-sticky-filters');
            } else {
                $('body').removeClass('show-sticky-filters');
            }
            $('#basic_nav_closed').sticky(scrollTop > 150 + stickyAddH);
            $('section.filters').sticky(scrollTop > 150 + stickyAddH).addClass('up');
        }
        else // no scroll
        {
            //
        }

        $('#cd-vertical-nav, .back-button').sticky(scrollTop > 150 + stickyAddH);

        var $footer = $('footer');

        var isSticky = ($(document).height() - scrollTop - $(window).height()) > 120 && !scrollTop == 0;

        $footer.sticky(isSticky);

        if (isSticky) {
            var currentScrollXOffset = $window.scrollLeft() * -1;

            $footer.css('transform', 'translate3d(' + currentScrollXOffset + 'px, 0px, 0px)');
        }

        window.lastScrollTop = scrollTop;

        /*
        if (scrollTop > 150)
        {
            $('.back-button').sticky(true);
            $('.level3_nav').css('max-height', '');
        }
        else if(scrollTop > 0)
        {
            if(scrollTop > 30){
                $('.back-button').sticky(false);
                $('.back-button').css('top','');
            }

            var con  = $('body').hasClass('smart') ? 441 : 433;

            var height = (con - scrollTop + $('footer.sticky').outerHeight()) + 'px';

            $('.sticky .level3_nav').css('max-height', 'calc(100vh - '+height+')');
        }
        else
        {
            $('.back-button').sticky(false);
            $('.back-button').css('top','');
            $('.sticky .level3_nav').css('max-height', '');
        }
        */
        if($('.user_tools .profile-formular').css('visibility') == "visible")
            $('.user_tools .profile-formular').find('.popup-close').trigger('click');
}



function calcMessTops()
{

    stickyAddH = 0 + $("#fullsize_banner").outerHeight();// + $("#notification:visible").outerHeight();

    var basic_nav_closed = $("#basic_nav_closed"),
        filters = $("section.filters"),
        cd_vertical_nav = $("#cd-vertical-nav");

    if(basic_nav_closed.length)
        basic_nav_closed
            .css("top", "")
            .css("top", Math.round(basic_nav_closed.offset().top + stickyAddH) + "px");

    if(filters.length)
        filters
            .css("top", "")
            .css("top", Math.round(10 + filters.offset().top + stickyAddH) + "px");

    if(cd_vertical_nav.length)
        cd_vertical_nav
            .css("top", "")
            .css("top", Math.round(cd_vertical_nav.offset().top + stickyAddH) + "px");
}


function placeholderFix(el) {
    var el = $(this);
    if('' != el.val()) {
        el.removeClass('placeholder-shown');
    } else {
        el.addClass('placeholder-shown');
    }
}

function addFilterToStickyOnMobile () {
    if(MOBILE) {
        setSmartLabels();
        $('section.filters div.basic_nav').click ( function (e) {
            if ( $(window).scrollTop() > 190 && ($(e.target).hasClass('menu_label') || $(e.target).closest('.filter-panel').length)) {
                $('html, body').animate( {scrollTop:0} );

            }
        });
    }
}

// sticky mess
window.lastScrollTop = 0;
window.stickyAddH = 0;

$(document).ready(function() {

    // fix for unsupported in IE pseudo class :placeholder-shown
    $('input[placeholder]').each(placeholderFix);
    $(document).on('keydown keypress keyup paste', 'input[placeholder]', placeholderFix);

    addFilterToStickyOnMobile();

    calcMessTops();

    stickyMess();

    addPassiveEventListener("scroll", stickyMess); //$(window).scroll(stickyMess);

    $(document).on('tabletDesktopViewportChange.DBDN_General', function () {
        if (isDesktopViewport()) {
            calcMessTops();
        }
    });
});

function setEqualTitle(titlesList) {
    $(titlesList).css('height', 'auto');
    var titleMaxHeight = 0;
    var currentHeight = 0;
    if($(titlesList).length) {
        $(titlesList).each(function() {
            currentHeight = $(this).innerHeight();
            if(currentHeight > titleMaxHeight) {
                titleMaxHeight = currentHeight;
            }
        });
        $(titlesList).css({
            'height': titleMaxHeight
        });
    }
}

function checkForLabelValue() {
    $('.filter-panel .filter-tags label').each(function() {
        if(!$(this).text()) {
            $(this).children('.filter_value').css({
                'display': 'block',
                'max-width': '50%',
                'float': 'left'
            });
        }
    });
}

function setSmartLabels() {
    checkForLabelValue()
    var language = $('#language a').text();
    if(language == 'Deutsch' && $('body').hasClass('smart')) {
        $('.filter-panel .filter-tags label').each(function() {
            if($(this).first().text().indexOf('A-Z') != -1 || $(this).first().text().indexOf('Z-A') != -1) {
                $(this).css('text-transform', 'none');
                //$(this).children().css('text-transform', 'none');
            } else {
                $(this).css('text-transform', 'lowercase');
            }
        });
    } else if (language == 'English' && $('body').hasClass('smart')) {
        $('.filter-panel .filter-tags label').css({
            'text-transform': 'none'
        });
    }
}

function setTitlesCapitalization(titles) {
    var language = $('#language a').text();
    if(language == 'Deutsch') {
        if($(titles).length) {
            $(titles).each(function() {
                //$(this).css('text-transform', 'lowercase');
                //TODO: Check for pseudo elements
                //$(this).text().toLowerCase();
                //$(this).text()[0].toUpperCase();
            });
        }
    } else if (language == 'English') {
        $(titles).css({
            'text-transform': 'none'
        });
    }
}

function checkFiltersCount() {
    if(MOBILE) {
        if(parseInt($('#active_filter_count').text()) <= 1) {
            $('.filter_remove').css('margin-top', '7px');
        } else {
            $('.filter_remove').css('margin-top', '0px');
        }
    }
}

function groupEqualTitlesSections() {
    if($(window).width() >= 767) {
        setEqualTitle($('.slider-item .title'));
        setEqualTitle($('.inner1-wrapper .title'));
        setEqualTitle($(".grid-page .title"));
        setEqualTitle($('.frontblock_col .title'));
        //setEqualTitle($('.title-chb label span'));
        setEqualTitle($('.download_elements .title-chb'));
    }
}

var BrowserDetect = {
    init: function () {
        this.browser = this.searchString(this.dataBrowser) || "Other";
        this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
    },
    searchString: function (data) {
        for (var i = 0; i < data.length; i++) {
            var dataString = data[i].string;
            this.versionSearchString = data[i].subString;

            if (dataString.indexOf(data[i].subString) !== -1) {
                return data[i].identity;
            }
        }
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index === -1) {
            return;
        }

        var rv = dataString.indexOf("rv:");
        if (this.versionSearchString === "Trident" && rv !== -1) {
            return parseFloat(dataString.substring(rv + 3));
        } else {
            return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
        }
    },

    dataBrowser: [
        {string: navigator.userAgent, subString: "Edge", identity: "MS Edge"},
        {string: navigator.userAgent, subString: "MSIE", identity: "Explorer"},
        {string: navigator.userAgent, subString: "Trident", identity: "Explorer"},
        {string: navigator.userAgent, subString: "Firefox", identity: "Firefox"},
        {string: navigator.userAgent, subString: "Opera", identity: "Opera"},  
        {string: navigator.userAgent, subString: "OPR", identity: "Opera"},  

        {string: navigator.userAgent, subString: "Chrome", identity: "Chrome"}, 
        {string: navigator.userAgent, subString: "Safari", identity: "Safari"}       
    ]
};


function showMoreButtonHandle() {
    if(!$('#downloads_related .hided').length) {
        $('#load-more_wrapper').hide();
    }
}


$(document).on('ready', function() {

    showMoreButtonHandle();

    BrowserDetect.init();
    $('html').addClass(BrowserDetect.browser);
    $('html').addClass(navigator.platform);
    //alert("You are using <b>" + BrowserDetect.browser + "</b> with version <b>" + BrowserDetect.version + "</b>");
    setSmartLabels();
    //setTitlesCapitalization($('.hct'));

    groupEqualTitlesSections();

    // Setting related downloads on exposed page template
    var downl = $('#related_downlowds');
    var pag = $('#related_pages');

    if(downl && pag && $(window).width() > 767 && $('.exposed')) {
        if (downl.innerHeight() > pag.innerHeight()) {
            pag.css({ 'height': downl.innerHeight() });
        } else {
            downl.css({ 'height': pag.innerHeight() });
        }
    }
});



var img_id = 'header_bg_1920';

function loadHeaderBrushing() {
    var screen_width = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
    
    if(screen_width <= 1024){
        img_id = 'header_bg_1024';
    }else if(screen_width <= 1280){
        img_id = 'header_bg_1280';
    }else{
        img_id = 'header_bg_1920';
    }
    if($('#'+img_id).length === 0){
        img_id = 'header_bg_1280';
    }   
    
    if($('#'+img_id).length > 0){
        var oldImgSrc = $('#'+img_id).attr('src');
        $('#'+img_id).load(function() {
            $('header').removeClass('bg_non_visible'); //remove class for positioning
            $('header').css('background-image', 'url("' + $(this).attr('src') + '")'); // set background image
            if(window.matchMedia &&
                    (window.matchMedia("(min-resolution: 2dppx)").matches  ||  window.matchMedia("(-webkit-min-device-pixel-ratio: 2)").matches ||
                    window.matchMedia("(min--moz-device-pixel-ratio: 2)").matches ||  window.matchMedia("(-o-min-device-pixel-ratio: 2/1)").matches ||
                    window.matchMedia("(min-device-pixel-ratio: 2)").matches ||  window.matchMedia("( min-resolution: 192dpi)").matches ||
                    window.matchMedia("(min-resolution: 3dppx)").matches || window.matchMedia("(-webkit-min-device-pixel-ratio: 3)").matches ||
                    window.matchMedia("(min--moz-device-pixel-ratio: 3)").matches || window.matchMedia("(-o-min-device-pixel-ratio: 3/1)").matches ||
                    window.matchMedia("(min-device-pixel-ratio: 3) ").matches || window.matchMedia("(min-resolution: 288dpi)").matches)){
                if($('#'+img_id).attr('src').indexOf('@2x') != -1){
                    //$(this).remove(); // remove img tag for load of the image
                }else{
                    $('#'+img_id).attr('src',function(){
                        var current_src = $(this).attr('src');
                        var src_2x = current_src.substr(0,current_src.lastIndexOf('.'))+'@2x'+current_src.substr(current_src.lastIndexOf('.'),current_src.length);
                        return src_2x;
                    })
                }
            }else{
                //$(this).remove(); // remove img tag for load of the image
            }

        })
        .attr('src', '')
        .attr('src', oldImgSrc);
    }
}

loadHeaderBrushing();

lupePreloader = new Image();

lupePreloader.onload =  lupePreloader.onerror = function(event) {
    if(event && event.type && "error" == event.type)
        console.error('No main image:' + this.src);

    $('header').css('visibility','visible');
    setTimeout(function() {
        $('#wrapper').css("visibility", "visible");
        $('#cd-vertical-nav').css('opacity', '1');
        $('#basic_preloader').hide();
        if(bannerImg){
            $('.hBlockContainer a:first-of-type').fadeIn();
//            var img = $(bannerImg).parent().find('svg image');
//            if(img){
//                $(img).attr('width', $(bannerImg).width());
//                $(img).attr('height', $(bannerImg).height());
//            }
        }


    },100);
}

var bannerImg = $('.banner_load_wrapper:visible .banner_image_wrapper img')[0];

var lupeUrl = $('.search_button').css('background-image');


if(bannerImg){
    lupeUrl = bannerImg.src;
}else if($('#'+img_id).length > 0){
    lupeUrl = $('#'+img_id).attr('src');
}

var reg = /htt[^"\)']+/g;
lupeUrl = reg.exec(lupeUrl); // cut the url of the image
lupePreloader.src = lupeUrl[0];

(function () {
    $(document).on('tabletDesktopViewportChange.DBDN_General tabletTabletViewportChange.DBDN_General', function (ev) {
        loadHeaderBrushing();
    });
}());
var oTable;
//var relPath = getRelativePath();

$(document).ready(function() {
    createPageControls();
    bindEditClicks();
    bindBodyClicks();
    
});
/* Get the rows which are currently selected */
function fnGetSelected(oTableLocal) {
	return oTableLocal.$('tr.row_selected');
}
function refreshTable() {
	var search_input = $('.user_search').val() !== js_localize['pages.label.search_term'] ? $('.user_search').val() : "";
	var sort_input = $('.user_sort').val() == -1 ? 1 : $('.user_sort').val();
	oTable.fnSort([[sort_input, 'asc']]);
	oTable.fnFilter(search_input);
	return false;
}

function populateForm(faqId) {
	jQuery.ajax({
		type: "GET",
		url: relPath+"api/v1/faq/faqbyid/" + faqId,
		success: function(data) {
			$.each(data.response, function(key, value) {
				if ($('select[name="' + key + '"]').length>0) {
                    $('select[name="' + key + '"]').parent().find('ul li[rel="'+value+'"]').click();
					//$('select[name="' + key + '"] option[value="'+value+'"]').attr('selected','selected');
				} else if (key == 'tags') {
                    $.each(value, function(key, value) {
                        generateTagElement(value.id, value.name, $('#faqs_tags'));
                    });
				} else if($('p#'+key).length > 0){
					$('p#'+key).html(value);
                }else if($('textarea[name="' + key + '"]').length>0){
                    tinyMCE.get('faq_answer').setContent(value);
				} else {
					$(':input[name="' + key + '"]').val(value);
				}
			});
			$('body').css('cursor','default');
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
			var message = JSON_ERROR.composeMessageFromJSON(data, true);
			jAlert(message, js_localize['faqs.list.title'], "error");
		},
	});
	return false;
}
function parseForm() {
    $('#faq_answer').val(tinyMCE.get('faq_answer').getContent());
	var serialized = $('#faqs_form').serializeArray();
	var s = '';
	var data = {};
    var keywords_arr = [];
	for (s in serialized) {
		if(serialized[s]['name'] == "keywords_arr"){
            keywords_arr.push(JSON.parse(serialized[s]['value']));
        }else {
            data[serialized[s]['name']] = serialized[s]['value'];
		}
	}
    data['keywords'] = keywords_arr;
	return JSON.stringify(data);
}

function generateTagElement(id, name, element) {
   var duplicateTags;
    if(id == ''){
        duplicateTags = element.parent().find("input[data-value='" + name + "']");
    }else{
        duplicateTags = element.parent().find("input[data-id='" + id + "']");
    }
    var result = false;
    if (duplicateTags.length == 0) {
        var newDiv = $('<div></div>').addClass('keywords');
        newDiv.append($('<span></span>').text(name));
        var json = JSON.stringify({tag_id: id, name: name, lang: lang});
        newDiv.append($('<input>').val(json).attr({name: 'keywords_arr', type: 'hidden', 'data-id': id, 'data-value': name}));
        newDiv.append($('<span></span>').text('X').addClass('delete').click(function (e) {
            $(this).parent().remove();
            element.focus();
            e.stopPropagation();
        }));
        element.prev('.tags').append(newDiv);
        element.val('');
        result = true;
    }
    return result;
}
function closeProfile() {
	$('.user_actions .nav_box').removeClass('active');
	$('.user_edit_wrapper .user_fields').hide();
	$(oTable.fnSettings().aoData).each(function() {
		$(this.nTr).removeClass('row_selected');
	});
	$('.user_edit_wrapper').hide();

}
function clearForm(){
    $('#faqs_form')[0].reset();
    $('#faqs_form .input_group').removeClass('error');
    $('#faqs_form input[name="id"]').val('0');
    $('.tags .keywords').remove();
    $('#faqs_form select').parent().find('ul li:eq(0)').click();
}

function bindEditClicks(){
    	/* Add a click handler for the delete row */
	$('.user_actions .nav_box.remove').click(function () {
        jConfirm($(this).attr('confirm'), js_localize['faqs.remove.title'], "success", function (r) {
            if (r) {
                var anSelected = fnGetSelected(oTable);
                closeProfile();
                if (anSelected.length !== 0) {
                    jQuery.ajax({
                        type: "DELETE",
                        url: relPath + "api/v1/faq/delete/" + $(anSelected[0]).find('.faqs_id').html(),
                        success: function (data) {
                            oTable.fnDeleteRow(anSelected[0]);
                            jAlert(js_localize['faqs.remove.success'], js_localize['faqs.remove.title'], "success");
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                            var message = JSON_ERROR.composeMessageFromJSON(data, true);
                            jAlert(message, js_localize['faqs.remove.title'], "error");
                        },
                    });
                }
            }
        });
    });

    $('.user_actions .nav_box.edit').click(function () {
        $('#wrapper_form_faqs').openDialog($('div.user_edit_wrapper .textarea_overlay'));
        $('#faqs_form .input_group').removeClass('error');
        $('body').css('cursor', 'progress');
        $('.user_actions .nav_box').removeClass('active');
        $(this).addClass('active');
        var anSelected = fnGetSelected(oTable);
        $('.user_edit_wrapper .user_fields').show();
        populateForm($(anSelected[0]).find('.faqs_id').html());
        var height = $('.user_edit_wrapper').position().top + $('.user_edit_wrapper').height() + $('.user_fields').height() + 10;
        if ($('.user_table').height() < height) {
            $('.user_table').css('height', height + 'px');
            $('.dataTables_paginate.paging_ellipses').hide();
        }
    });

    $('.user_actions .nav_box.add').click(function () {
        $('#wrapper_form_faqs').openDialog($('div.user_edit_wrapper .textarea_overlay'));
        $('#faqs_form .input_group').removeClass('error');
        $('.user_actions .nav_box').removeClass('active');
        $(this).addClass('active');
        $('#faqs_form input[name="id"]').val('0');
        $('#faqs_form input:text').val('');
        $('#faqs_form p.si_data').html('-');
        $('#faqs_form textarea').val('');
        $('.user_edit_wrapper .user_fields').show();
        var height = $('.user_edit_wrapper').position().top + $('.user_edit_wrapper').height() + $('.user_fields').height() + 10;
        if ($('.user_table').height() < height) {
            $('.user_table').css('height', height + 'px');
            $('.dataTables_paginate.paging_ellipses').hide();
        }
    })


    $('.user_fields .nav_box.cancel').click(function () {
        $('#wrapper_form_faqs').closeDialog();
        $('#faqs_form .input_group').removeClass('error');
        $('.user_actions .nav_box').removeClass('active');
        $('.user_edit_wrapper .user_fields').hide();
        $('.user_table').css('height', '');
        $('.dataTables_paginate.paging_ellipses').show();
        closeProfile();
        clearForm();
    });

    $('.user_fields .nav_box.save').click(function () {
        $('body').css('cursor', 'progress');
        var anSelected = fnGetSelected(oTable);

        if ($('#faqs_form input[name="id"]').val() != "" && $('#faqs_form input[name="id"]').val() > 0) {
            jQuery.ajax({
                type: "PUT",
                url: relPath + "api/v1/faq/update",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: parseForm(),
                // script call was *not* successful
                success: function (data) {
                    $('#wrapper_form_faqs').closeDialog();
                    $('body').css('cursor', 'default');
                    jAlert(data.message, js_localize['faqs.edit.title'], "success")
                    closeProfile();
                    clearForm();
                    oTable.fnUpdate(data.response, anSelected[0]);
                    $('.user_table').css('height', '');
                    $('.dataTables_paginate.paging_ellipses').show();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                    var message = JSON_ERROR.composeMessageFromJSON(data, true);
                    if (data.response !== undefined) {
                        $.each(data.response, function (key, value) {
                            if ($('input[name="' + key + '"]').length > 0) {
                                $('input[name="' + key + '"]').parent().addClass('error');
                            } else if ($('textarea[name="' + key + '"]').length > 0) {
                                $('textarea[name="' + key + '"]').parent().addClass('error');
                            }
                        });
                    }
                    $('body').css('cursor', 'default');
                    jAlert(message, js_localize['faqs.edit.title'], "error");
                }
            });
        } else {
            jQuery.ajax({
                type: "POST",
                url: relPath + "api/v1/faq/create",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: parseForm(),
                success: function (data) {
                    $('#wrapper_form_faqs').closeDialog();
                    $('body').css('cursor', 'default');
                    jAlert(data.message, js_localize['faqs.add.title'], "success")
                    closeProfile();
                    clearForm();
                    oTable.fnAddData(data.response);
                    $('.user_table').css('height', '');
                    $('.dataTables_paginate.paging_ellipses').show();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                    var message = JSON_ERROR.composeMessageFromJSON(data, true);
                    if (data.response !== undefined) {
                        $.each(data.response, function (key, value) {
                            if ($('input[name="' + key + '"]').length > 0) {
                                $('input[name="' + key + '"]').parent().addClass('error');
                            } else if ($('textarea[name="' + key + '"]').length > 0) {
                                $('textarea[name="' + key + '"]').parent().addClass('error');
                            }
                        });
                    }
                    $('body').css('cursor', 'default');
                    jAlert(message, js_localize['faqs.add.title'], "error");
                },
            });
        }
    });
}
function bindBodyClicks() {
    $("#faqs tbody").click(function (event) {
        if (!$(event.target).hasClass('dataTables_empty')) {
            closeProfile();
            $(oTable.fnSettings().aoData).each(function () {
                $(this.nTr).removeClass('row_selected');
            });
            $(event.target.parentNode).addClass('row_selected');

            if (!$('a.edit').is(":visible")) {
                $('a.edit').show();
                $('a.remove').show();
            }
        } else {
            $('a.edit').hide();
            $('a.remove').hide();
        }

        $('.user_edit_wrapper').css('top', $('#faqs_wrapper').position().top + $(event.target.parentNode).position().top + $(event.target.parentNode).height() + 1);
        $('.user_edit_wrapper').show();
    });

    $('body').click(function (event) {
        if ($(event.target).parents('tbody').length == 0 && $(event.target).parents('.user_edit_wrapper').length == 0 && $('#popup_overlay').length == 0 && !$('.textarea_overlay').is(':visible')) {
            closeProfile();
        }
        if (event.target.parentNode) {
            if (!(event.target.parentNode.className == "admin_data" || event.target.parentNode.id == "admin_info" || event.target.parentNode.id == "admin_action")) {
                if ($('#admin_action').is(":visible"))
                {
                    $("#admin_info").removeClass('active');
                    $('#admin_action').hide();
                }
            }
        }
    });
    $('.tags_wrapper').click(function(e){
        $(this).find('input').focus();
    })

}
function createPageControls() {
   oTable = $('#faqs').dataTable({
		"bProcessing": false,
		"sAjaxSource": relPath+"api/v1/faq/allfaqs/"+lang,
		"sAjaxDataProp": 'response',
		"fnServerData": fnServerData,
		"iDisplayLength": 45,
		"bFilter": true,
		"sPaginationType": 'ellipses',
		"bLengthChange": false,
		"iShowPages": 10,
		"bSort": false,
		"bDeferRender": true,
		"oLanguage": {
			"sEmptyTable": " "},
		"aoColumns": [
			{"sType": "numeric", "mData": "id", "sWidth": "30px", "bSearchable": false, "sClass": "faqs_id"},
			{"sType": "string", "mData": "question", "sWidth": "200px", "bSearchable": true},
            {"sType": "string", "mData": function(source){
                    return brands[source.brand_id];
            }, "sWidth": "200px", "bSearchable": true},
            {"sType": "string", "mData": function(source){
                     return categories[source.category_id];
            }, "sWidth": "200px", "bSearchable": true},
			{"sType": "date-eu", "mData": "updated_at", "sWidth": "105px", "bSearchable": false}
		],
	});
    var oSettings = oTable.fnSettings();
	oSettings.sAlertTitle = js_localize['faqs.list.title'];
    
    $("#faqs_tags").autocomplete({
        minLength: 2,
        source: function (request, response) {
            var term = request.term.toLowerCase(),
            element = this.element,
            cache = this.element.data('autocompleteCache') || {},
            foundInCache = false;
            $.each(cache, function (key, data) {
                if (term.indexOf(key) === 0 && data.length > 0) {
                    var result = jQuery.grep(data, function (n) {
                        return (n.value.toLowerCase().indexOf(term) >= 0);
                    });
                    response(result);
                    foundInCache = true;
                    return;
                }
            });
            if (foundInCache)
                return;          
            $.ajax({
                url: relPath + 'api/v1/tag/source/' + lang,
                dataType: "json",
                data: request,
                success: function (data) {
                    var data_arr = new Array();
                    for (var key = 0; key < data.response.length; key++) {
                        data_arr[key] = {value: $('<div/>').html(data.response[key].value).text(), tag_id: $('<div/>').html(data.response[key].id).text()};
                    }
                    cache[term] = data_arr;
                    element.data('autocompleteCache', cache);
                    response(data_arr);
                }
            });
        },
        select: function (event, ui) {
            generateTagElement(ui.item.tag_id, ui.item.value, $(this));
            event.stopPropagation();
            return false
        }
    });
}
var oTable;
//var relPath = getRelativePath();

$(document).ready(function() {

	oTable = $('#history').dataTable({
		"bProcessing": false,
		"sAjaxSource": relPath+"api/v1/history/all",
		"sAjaxDataProp": 'response',
		"fnServerData": fnServerData,
		"iDisplayLength": 45,
		"bFilter": true,
		"sPaginationType": 'ellipses',
		"bLengthChange": false,
		"iShowPages": 10,
		"bSort": true,
		"bDeferRender": true,
		"oLanguage": {
			"sEmptyTable": " "},
		"aoColumns": [
			{"sType": "numeric", "mData": "id", "sWidth": "50px", "bSearchable": false, "sClass": "history_id"},
			{"sType": "string", "mData":  function(source) {
						return strip_tags(source.page_slug + " (" + source.page_id + ")");
					},"bSearchable": true, "bSortable": true},
			{"sType": "string", "mData":  function(source) {
						return source.content_section+" ("+source.content_id+")";
					}, "sWidth": "130px", "sClass": "strong", "bSearchable": true, "bSortable": true},
			{"sType": "string", "mData":  function(source) {
						return strip_tags(source.username + " (" + source.user_id + ")");
					}, "sWidth": "200px", "bSearchable": true, "bSortable": true},
			{"sType": "string", "mData": "action", "sWidth": "100px", "bSearchable": false},
			{"sType": "string", "mData": "ip", "sWidth": "150px", "bSearchable": false, "sClass": "comment_active", "bSortable": true},
			{"sType": "date-eu", "mData": "created_at", "sWidth": "130px", "bSearchable": true, "bSortable": true}
		],
		"fnInitComplete": function(oSettings, json) {
//			if(username){
//				$('.user_search').val(username);
//				refreshTable();
//			}
		}
	});
	
	var oSettings = oTable.fnSettings();
	oSettings.sAlertTitle = js_localize['history.list.title'];

	$("#history tbody").click(function(event) {
		closeProfile();
		$(oTable.fnSettings().aoData).each(function() {
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');

	});
	
	$('body').click(function(event) {
		if($(event.target).parents('tbody').length == 0 && $(event.target).parents('.user_edit_wrapper').length == 0 && $('#popup_overlay').length == 0){
			closeProfile();
		}
		if (event.target.parentNode) {
			if (!(event.target.parentNode.className == "admin_data" || event.target.parentNode.id == "admin_info" || event.target.parentNode.id == "admin_action")) {
				if ($('#admin_action').is(":visible"))
				{
					$("#admin_info").removeClass('active');
					$('#admin_action').hide();
				}
			}
		}
	});
});

function closeProfile() {
	$(oTable.fnSettings().aoData).each(function() {
		$(this.nTr).removeClass('row_selected');
	});
}

/* Get the rows which are currently selected */
function fnGetSelected(oTableLocal) {
	return oTableLocal.$('tr.row_selected');
}
function refreshTable() {
	var search_input = $('.user_search').val() !== js_localize['pages.label.search_history'] ? $('.user_search').val() : "";
	var sort_input = $('.user_sort').val() == -1 ? 1 : $('.user_sort').val();
	if(sort_input == 5){
		oTable.fnSort([[sort_input, 'desc']]);
	}else{
		oTable.fnSort([[sort_input, 'asc']]);
	}
	oTable.fnFilter(search_input);
	return false;
}
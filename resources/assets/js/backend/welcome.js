
var page_table_init = {
    "bProcessing": true,
    "sAjaxSource": relPath + 'api/v1/' + (NEWSLETTER_USER_ONLY? 'newsletter': 'pages') + '/responsible?lang=' + lang,
    "sAjaxDataProp": 'response',
    "fnServerData": fnServerData,
    "iDisplayLength": 10,
    "bFilter": true,
    "sPaginationType": 'ellipses',
    "bLengthChange": false,
    "iShowPages": 20,
    "bSort": true,
    "aaSorting": [],
    "oLanguage": {
        "sEmptyTable": " "},
    "aoColumns": [
        {"sType": "page-title", "mData": function(source) {
                var path = '';
                if (typeof (source.path) != "undefined") {
                    path = '<span style="display: block;font-size: 11px;line-height: 12px;">' + source.path + '</span>';
                }

                path += '<a onClick="return rowExpand(this,event)" href="" class="childless"></a> <span class="title">' + source.title + '</span>';

                return path;
            }, "sWidth": "360px", "sClass": "strong", "bSearchable": true},
        {"sType": "numeric", "mData": "version", "sWidth": "10px", "bSearchable": false},
        {"sType": "numeric", "mData": "id", "sWidth": "35px", "bSearchable": false, "sClass": "page_id"},
        {"sType": "string", "mData": "assigned_by", "sWidth": "140px", "bSearchable": false},
        {"sType": "string", "mData": "editing_state", "sWidth": "50px", "bSearchable": false, "sClass": "publish_unpublish"},
        {"sType": "string", "mData": "due_date", "sWidth": "90px", "bSearchable": false},
        
    ],
    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $(nRow).attr('id', 'page_' + aData.id);
    },
};

$(document).ready(function() {
    $("#pages tbody").click(function(event) {
        $('.moderation_holder').html('');
        $('.moderation_holder').css('height', '0px');
        closeRowEditor();
        $(oTable.fnSettings().aoData).each(function() {
            $(this.nTr).removeClass('row_selected');
        });
        var $tr = $(event.target.parentNode)[0];
        if ($(event.target.parentNode).prop("tagName") != "TD") {
            $(event.target.parentNode).addClass('row_selected');
            $('.user_edit_wrapper').css('top', $('#pages_wrapper').position().top + $(event.target.parentNode).position().top + $(event.target.parentNode).height() + 1);
        } else {
            $(event.target.parentNode).parent().addClass('row_selected');
            $tr = $(event.target.parentNode).parent()[0];
            $('.user_edit_wrapper').css('top', $('#pages_wrapper').position().top + $(event.target.parentNode).parent().position().top + $(event.target.parentNode).parent().height() + 1);
        }
        $('.user_edit_wrapper').show();
        var data = oTable.fnGetData($tr);
        $.ajax({
            url: relPath + 'api/v1/pages/notes/' + data.u_id,
            type: "GET",
            success: function(data) {
                if (data.response.length > 0) {
                    $('.moderation_holder').css('height', 'auto');
                }
                $.each(data.response, function(key, value) {
                    var moderation_tmpl = $('#moderation_template').html();
                    $('.moderation_holder').append(moderation_tmpl);
                    $('.moderation_holder .page_moderation').last().find('h6.moderation_by').html($('#moderation_template h6.moderation_by').html() + value.user.username);
                    $('.moderation_holder .page_moderation').last().find('span.moderation_date').html(value.created);
                    $('.moderation_holder .page_moderation').last().find('p').html(value.text);
                });
            }
        })
    });

    $('body').click(function(event) {
        if ($(event.target).parents('tbody').length == 0 && $(event.target).parents('.user_edit_wrapper').length == 0 && $('#popup_overlay').length == 0 && $("#ui-datepicker-div:hover").length == 0) {
            $('.moderation_holder').html('');
            $('.moderation_holder').css('height', '0px');
        }

    });

    bindRowClicks();
});
function bindRowClicks() {
    $('.welcome .user_actions .nav_box.edit').click(function() {
        var anSelected = fnGetSelected(oTable);
        if (anSelected.length !== 0) {
            var data = oTable.fnGetData(anSelected[0]);
            checkPageAvailability(data.u_id, function() {
                window.location.href = relPath + 'cms/content/' + data.u_id;
            });
        }
    });
}
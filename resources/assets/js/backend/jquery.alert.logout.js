/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var _SessionTimeOut = 1000;
var _SessionTimeOutTimer = null;

function initSessionTimeout() {
    
    var sessionTimeoutReminder = 30;
    
    console.info("Set Timer: " + sessionTimeout + " Reminder: " + sessionTimeoutReminder);
    _SessionTimeOut = sessionTimeout - sessionTimeoutReminder;
    console.info("Session TimeOut: " + _SessionTimeOut);
    
    if (_SessionTimeOutTimer == null) {
        _SessionTimeOutTimer = setInterval(
            function(){ 
                _SessionTimeOut -= 1;
 
                if (_SessionTimeOut == 0) {
                    jConfirmTimeOut("You session will be locked in {timeout} secondes. <p>Do you want to continue?</p>", "Your session is about to expire", "success", 
                        function (result) {
                            if (result) {
                                jQuery.ajax({
                                    type: "GET",
                                    url: relPath+"cms/keepLogin",
                                    success: function(data) {
                                        if (data && data.message == "keepLogin") {
                                            initSessionTimeout();
                                        } else {
                                            jAlert("Server dosen't confirm session timeout is reseted!", "Warrning", "error");
                                        }
                                    },
                                    // script call was *not* successful
                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                            var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                                            var message = JSON_ERROR.composeMessageFromJSON(data);
                                            $('body').css('cursor','default');
                                            jAlert(message, js_localize['comments.add.title'], "error");
                                    },
                                });                            } 
                            else {
                                window.location.assign(logoutUrl);
                            }
                            ;}, sessionTimeoutReminder); 
                }
            }, 
        1000);
    }
}

(function ($) {
    if (isLogin) {
        initSessionTimeout();
        $(document).bind("ajaxComplete", function(){
            initSessionTimeout();
        });
    }
})(jQuery);


var predefinedColors = predefinedColors!= undefined ? predefinedColors:[];
var dbdn_snippets = dbdn_snippets!= undefined ? dbdn_snippets:[];
/*globals $dbdn_snippets, $dbdn_snipetsLite */
var mainSnipets = dbdn_snippets.filter(function(el) {
	return el.section ? el.section.indexOf("main") !== -1 : true;
});

var nlSnipets = dbdn_snippets.filter(function(el) {
	return el.section ? el.section.indexOf("nl_main") !== -1 : true;
});

// Contol state of Simple Image, Zoom Image and Galery butons
function _handleDisabledState(editor, item, selector) {
	var strSelection = selector;
	if (selector instanceof Array) {
		strSelection = selector.join(',');
	}

	item.disabled(editor.dom.getParent(editor.selection.getStart(), strSelection));

	editor.selection.selectorChanged(selector, function(state) {
		item.disabled(state);
	});
}

function handleDisabledState() {
	_handleDisabledState(tinyMCE.activeEditor, this, this.settings.disabledStateSelector);
}

function openFileManagerForImages(editor, imageType, isNewsletter = false) {
	// Add you own code to execute something on click
	editor.focus();
	var gallery_attr = $(editor.selection.getNode()).data('gallery');
	if (gallery_attr === null || typeof gallery_attr == "undefined") {
		gallery_attr = "";
	} else {
		gallery_attr = "" + gallery_attr;
	}
	if (gallery_attr.length > 0) {
		var title = js_localize['tiny_mce.init.edit_' + imageType];
	} else {
		var title = js_localize['tiny_mce.init.insert_' + imageType];
	}
	editor.windowManager.open({
		title: title,
		url: relPath + 'cms/filemanager?filemanager_mode=images' + (isNewsletter ? '&isNewsletter=true' : ''),
		width: 1024,
		height: $(window).height() - 50
	}, {
			callback_selection: function (selection, selection_src, selection_desc) {
				selection_items = [];
				for (var i = 0; i < selection_src.length; i++) {
					selection_items[i] = { src: selection_src[i].replace(/<.+>/gi, '') , isProtected: selection_src[i].indexOf('*') >= 0 };
				}
				var strSelection = selection.join(',');
				var snippet = dbdn_snippets.filter(function (el) {
					return el.section ? el.section.indexOf(imageType + "_image") !== -1 : true;
				});
				//TODO: Check for Protected image!
				var setImageContent = function(src) {
					//var src2x = 'files/images/__cache/content~2x' + selection_src[0] + '.jpg 2x';
					//var src3x = 'files/images/__cache/content~3x' + selection_src[0] + '.jpg 3x';
					//var content = snippet[0].value.replace("{idArray}", strSelection).replace("{selection_src}", src).replace("{selection_srcset}", src + ", " + src2x +', '+ src3x);
					var content = snippet[0].value.replace("{idArray}", strSelection).replace("{selection_src}", src).replace("{title}", srcItem.src).replace('{selection_desc}', selection_desc);
					editor.insertContent(content);
				};

				var srcItem = selection_items[0];
				var src = 'files/images' + srcItem.src;
				if (imageType == 'simple') {
					var src = 'files/images' + srcItem.src;
				} else {
					var extension = srcItem.src.substring(srcItem.src.lastIndexOf('.'), srcItem.src.length);
					if (isNewsletter) {
						var src = 'files/images' + srcItem.src;
					} else {
						if (srcItem.isProtected) {
							getImagePath(selection, function(response_data) {
								if (response_data.length > 0 && response_data[0].images.length > 0) {
									var protected_src = response_data[0].images[0].lowres;
									setImageContent(protected_src);
								}
							});
							return;
						} else {
							var src = 'files/images/__cache/content' + srcItem.src + (extension.toLowerCase() == '.png' ? extension : '.jpg');
						}
					}
				}

				setImageContent(src);
			},
			image_type: imageType,
			dialog_mode: true,
			select_multiple: ((imageType === 'gallery' || imageType === 'explainer') ? true : false),
			initial_selection: gallery_attr
		});
}

function openImageHotspotSelector(editor,node) {
	// Add you own code to execute something on click
	editor.focus();
	var attr = $(editor.selection.getNode()).attr('data-hotspot');
	if(typeof attr !== typeof undefined && attr !== false && attr !== ''){
		var hotspot_attr = JSON.parse($(editor.selection.getNode()).attr('data-hotspot'));
	}else{
		var hotspot_attr = JSON.parse('[]');
	}
	var image_src = $(editor.selection.getNode()).attr('src');

	var title = js_localize['tiny_mce.init.image_hotspot'];

	editor.windowManager.open({
		title: title,
		url: relPath + 'cms/hotspot',
		width: 1024,
		height: $(window).height() - 50
	}, {
		callback_selection: function(json) {
			if($.isEmptyObject(json)){
				$(node).removeClass('hotspot');
			}else{
				$(node).addClass('hotspot');
			}
			$(node).attr('data-hotspot',json);
		},
		hotspots: hotspot_attr,
		dialog_mode: true,
		image_src: image_src,
	});

}

function openExposedFieldEditor(editor) {
    var selectedNode = editor.selection.getNode(), exposed_text = '', exposed_link = '', button_text = '';

    if ($(selectedNode).hasClass('transition-background')) {
            exposed_text = selectedNode.getElementsByTagName('p')[0].textContent;
            exposed_link = $(selectedNode.getElementsByTagName('a')[0]).attr('href');
            button_text = selectedNode.getElementsByTagName('a')[0].textContent;
    }
    
    if ($(selectedNode.parentNode).hasClass('transition-background')) {
        if (selectedNode.tagName == 'P') {
            exposed_text = selectedNode.textContent;
            exposed_link = $(selectedNode.parentNode.getElementsByTagName('a')[0]).attr('href');
            button_text = selectedNode.parentNode.getElementsByTagName('a')[0].textContent;
        }
        if (selectedNode.tagName == 'A') {
            exposed_text = selectedNode.parentNode.getElementsByTagName('p')[0].textContent;
            exposed_link = $(selectedNode).attr('href');
            button_text = selectedNode.textContent;
        }
    }                      

    editor.windowManager.open({
        title: 'Exposed Field',
        body: [{type: 'textbox', name: 'exposed_text', multiline: true, style: 'height: 50px;', size: 200, label: 'Exposed Text', value: exposed_text},
               {type: 'textbox', name: 'exposed_link', size: 40, label: 'Exposed Link', value: exposed_link},
               {type: 'textbox', name: 'button_text', size: 40, label: 'Button Text', value: button_text}],
        onsubmit: function(e) {
            if ($(selectedNode.parentNode).hasClass('transition-background')) {
                editor.selection.select(selectedNode.parentNode.parentNode.parentNode);
                editor.selection.setContent('');
            }
            if ($(selectedNode).hasClass('transition-background')) {
                editor.selection.select(selectedNode.parentNode.parentNode);
                editor.selection.setContent('');
            }
            editor.execCommand('mceReplaceContent', false, 
                editor.dom.createHTML('div', {
                class:'transition-element'
            },'<div class="transition-wrapper"><div class="transition-background"><p>'+e.data.exposed_text+'</p><a class="transition-btn" href="'+e.data.exposed_link+'">'+e.data.button_text+'</a></div></div>'));
        }
    });
}

function myCustomExecCommandHandler(editor_id, elm, command, user_interface, value) {
	var t = this, d = document, w = window, na = navigator, ua = na.userAgent, i, nl, n, base, p, v, inst;
	t.isIE = !t.isWebKit && !t.isOpera && (/MSIE/gi).test(ua) && (/Explorer/gi).test(na.appName);
	inst = tinyMCE.getInstanceById(editor_id);

	if (command == "Indent") {
		function isBlock(n) {
			return /^(P|DIV|H[1-6]|ADDRESS|BLOCKQUOTE|PRE)$/.test(n.nodeName);
		}
		if (!isBlock(inst.selection.getNode())) {
			inst.selection.setContent('<div>' + inst.selection.getContent() + '</div>');
		}
		return false;
	} else if (t.isIE && command == "InsertUnorderedList") {
		inst.selection.setContent('<ul><li>' + inst.selection.getContent() + '</li></ul>');
		return true;
	} else if (t.isIE && command == "InsertOrderedList") {
		inst.selection.setContent('<ol><li>' + inst.selection.getContent() + '</li></ol>');
		return true;
	}
	return false;
}

function openFileManagerForDownloads(editor, initial_selection, callback_selection) {
	// Get list of selected files
	if (initial_selection === null) {
		initial_selection = "";
	} else {
		initial_selection = "" + initial_selection;
	}
	var title = js_localize['tiny_mce.init.add_downloads'];
	editor.windowManager.open({
		title: title,
		url: relPath + 'cms/filemanager?filemanager_mode=downloads',
		width: 1024,
		height: $(window).height() - 50
	}, {
		callback_selection: callback_selection,
		image_type: 'downloads',
		dialog_mode: true,
		select_multiple: true,
		initial_selection: initial_selection,
		isPageRestricted: $("input[name='isPageRestricted']").val()
	});

}

function alignBannerImage(editor, position){
    editor.focus();
    var nodeSelection = editor.selection.getNode(); // DOM Node
    if(nodeSelection.nodeName === "IMG"){
        switch(position) {
            case 'left':
                editor.dom.addClass(nodeSelection, 'left-align');
                editor.dom.removeClass(nodeSelection, 'right-align');
                break;
            case 'right':
                editor.dom.addClass(nodeSelection, 'right-align');
                editor.dom.removeClass(nodeSelection, 'left-align');
                break;
            default:
                editor.dom.removeClass(nodeSelection, 'left-align');
                editor.dom.removeClass(nodeSelection, 'right-align');
        }
    }
}

function TinyMCE_custom_HR(editor){
	editor.addCommand('InsertHorizontalRule', function() {
		editor.execCommand('mceInsertContent', false, '<hr />');
	});
	editor.addButton('dbdnHR', {
		icon: false,
		image: relPath + "img/backend/hr_icon.png",
		tooltip: 'Horizontal line',
		cmd: 'InsertHorizontalRule'
	});
	editor.addMenuItem('dbdnHR', {
		icon: 'none',
		text: 'Horizontal line',
		cmd: 'InsertHorizontalRule',
		context: 'insert'
	});
}
function TinyMCE_AddContextMenu(editor, section){
	editor.settings.plugins.push("contextmenu");
	editor.settings.contextmenu = "AddP AddParagraphBeforeElement AddPAfterElement AddPEndPosition | DelNode DelNodeSection | CreateHotSpots";
	editor.addMenuItem('AddParagraphBeforeElement', { text: 'Add paragraph before element', icon: false,	cmd: 'AddParagraphBeforeElement' });
	editor.addMenuItem('AddP', { text: 'Add paragraph', icon: false,	cmd: 'AddParagraph' });
	editor.addMenuItem('AddPAfterElement', { text: 'Add paragraph after element', icon: false,	cmd: 'AddParagraphAfterElement' });
	editor.addMenuItem('AddPEndPosition', { text: 'Add paragraph on last position', icon: false, cmd: 'AddParagraphOnLastPosition' });
	editor.addMenuItem('DelNode', { text: 'Remove element', icon: false, cmd: 'RemoveElement' });
	editor.addMenuItem('DelNodeSection', { text: 'Remove section', icon: false, cmd: 'RemoveSection' });
	editor.addMenuItem('CreateHotSpots', { text: 'Captioned image', icon: false, cmd: 'CreateHotSpots' });
	// commands

	editor.addCommand('CreateHotSpots', function() {
		var node = editor.dom.getParents(editor.selection.getNode(), null,editor.dom.get(section));
		if($(node)[0].tagName === undefined || $(node)[0].tagName != "IMG" || !$(node).hasClass('zoom')){
			jAlert(js_localize['tiny_mce.init.hotspot_only_zoom'],js_localize['tiny_mce.init.hotspot'],'error');
		}else{
			openImageHotspotSelector(editor,node);
		}
	});

	editor.addCommand('AddParagraph', function() {
		editor.insertContent('<p>&nbsp;</p>');
	});
	editor.addCommand('AddParagraphAfterElement', function() {
		var node = editor.dom.getParents(editor.selection.getNode(), null,editor.dom.get(section));
		if(node.length > 0)
			$(node[node.length - 1]).after('<p>&nbsp;</p>');
	});
	editor.addCommand('AddParagraphBeforeElement', function() {
		var node = editor.dom.getParents(editor.selection.getNode(), null,editor.dom.get(section));
		if(node.length > 0)
			$(node[node.length - 1]).before('<p>&nbsp;</p>');
	});
	editor.addCommand('AddParagraphOnLastPosition', function() {
		editor.dom.add(section, 'p', {}, '&nbsp;');
	});
	editor.addCommand('RemoveElement', function() {
		if(editor.getContent() !== '')
		{
			if($(editor.selection.getNode()) !== $(editor.dom.get(section)))
				editor.dom.remove(editor.selection.getNode());
		}
	});
	editor.addCommand('RemoveSection', function() {
		if(editor.getContent() !== ''){
			var nodeForDelete = editor.dom.getParents(editor.selection.getNode(), null, editor.dom.get(section));
			if($(nodeForDelete) !== $(editor.dom.get(section)))
				editor.dom.remove(nodeForDelete);
		}
	});
}

function TinyMCE_AddContextMenuNL(editor, section){
	editor.settings.plugins.push("contextmenu");
	editor.settings.contextmenu = "DelNode Del";
	editor.addMenuItem('DelNode', { text: 'Remove element', icon: false, cmd: 'RemoveElement' });
	// commands


	editor.addCommand('RemoveElement', function() {
		if(editor.getContent() !== '')
		{
			if($(editor.selection.getNode()) !== $(editor.dom.get(section)))
				editor.dom.remove(editor.selection.getNode());
		}
	});
}

function TinyMCE_custom_OnClick(editor, section){
	editor.on('Click', function () {
		if(editor.getContent().length === 0){
			editor.setContent("<p>&nbsp;</p>", ({format: "raw"}));
			editor.selection.setCursorLocation(editor.dom.select('p')[0]);
		}
	});
}

function TinyMCE_custom_OnContent(editor, section){
	var element = "div#".concat(section, '.div_', section, '_edit');
	editor.on('GetContent', function (e) {
		if(typeof(e.selection) !== 'undefined') return;
		if(typeof(e.content) !== 'undefined'){
			var $content = $(e.content);
			if ( $content.filter(element).add($content.find(element)).length > 0 ){
				e.content = $content.first(element).html();
				if(e.content === '&nbsp;')
					e.content = '';
			}
		}
	});
	editor.on('BeforeSetContent', function (e) {
        if(section == "content" && e.content.indexOf('default_anchor') > 1){
            var style = ' style="color:#00677f; text-decoration:none;" ';
            var anchorIndex = e.content.indexOf('<a') + 3;
            if(anchorIndex !== -1)
             e.content =  e.content.substring(0, anchorIndex) + style + e.content.substring(anchorIndex, e.content.length);
        }
		if(typeof(e.selection) !== 'undefined') {
			if(editor.selection.getNode().nodeName === 'BODY')
				editor.selection.setCursorLocation(editor.dom.get(section));
			return;
		}
		if(typeof(e.content) !== 'undefined'){
			var $content = $(e.content);
			if ( $content.filter(element).add($content.find(element)).length === 0 && e.selection === undefined ){
				if(typeof(e.load) === 'undefined'){
					e.content = '<div id="'.concat(section , '" class="div_', section, '_edit" contenteditable="true" style="width:95%;">', e.content, '</div>');
				}
			}
		}
	});
}

//var css_content = relPath + "css/main.css, " + relPath + "css/backend.css, " + relPath + "css/backend_content.css";
var css_content = relPath + "css/main.css";
if (typeof pageTemplate !== 'undefined') {
	if(pageTemplate === 'smart')
		css_content += ", " + relPath + "css/smart.css";
	if(pageTemplate === 'home')
		css_content += ", " + relPath + "css/home.css";
}
css_content += ", " + relPath + "css/backend_main_fixes.css";

var tiny_main = tinymce.init({
	skin: "dbdnskin",
	element_format : "html",
	entity_encoding: "raw",
    selector: "textarea#main_body",
    plugins: ["code", "visualblocks", "table", "link", "textcolor", "anchor"],
//	menubar: "edit insert view format table",
	menu:{ // this is the complete default configuration
        edit   : {title : 'Edit'  , items : 'undo redo | cut copy paste pastetext | selectall'},
        insert : {title : 'Insert', items : 'snippetsMenu | dbdnHR | imageMenu galleryMenu explainerMenu | exposedFieldMenu summaryMenu'},
        view   : {title : 'View'  , items : 'visualblocks visualaid'},
        format : {title : 'Format', items : 'bold underline superscript subscript | formats alignmentMenu | removeformat'},
        table  : {title : 'Table' , items : 'inserttable tableprops deletetable | cell row column'}
    },
	formats: {
		formatH1: {
			block: 'h1'
		},
		formatSmall: {
			'inline': 'small'
		},
		formatP: {
			block: 'p'
		},
        formatNotice: {
			block: 'p', classes: 'notice'
		},
		formatSubline: {
			block: 'p', classes: 'subline'
		},
		removeformat : [
			{selector : 'h1', remove : 'all', split : true, expand : false, block_expand : true, deep : true},
			{selector : 'small', remove : 'all', split : true, expand : false, block_expand : true, deep : true},
			{
				selector: 'a,b,strong,em,i,font,u,strike,sub,sup,dfn,code,samp,kbd,var,cite,mark,q',
				remove: 'all',
				split: true,
				expand: false,
				block_expand: true,
				deep: true
			},
			{selector: 'span', attributes: ['style', 'class'], remove: 'empty', split: true, expand: false, deep: true},
			{selector: '*', attributes: ['style', 'class'], split: false, expand: false, deep: true}
		]
	},
//	style_formats: [
//        {title: 'Title', block: 'h1'},
//        {title: 'Subheading', block: 'h1', inline: 'small'},
//        {title: 'Copy text', block: 'p'}
////		{title: "_Alignment",items: [{title: "Left",icon: "alignleft",format: "alignleft"}, {title: "Center",icon: "aligncenter",format: "aligncenter"}, {title: "Right",icon: "alignright",format: "alignright"}, {title: "Justify",icon: "alignjustify",format: "alignjustify"}]}
//    ],
    textcolor_map: predefinedColors,
    tools: "inserttable",
    body_class: "content_main content_wrapper",
    content_css: css_content,
    toolbar1: "btnFormats bold underline superscript subscript alignleft aligncenter alignright alignjustify bullist numlist forecolor",
    toolbar2: "btnImages btnGallery btnExplainer link unlink btnAnchorNav btnExposedField dbdnHR removeformat code",
    forced_root_block: 'p',
    valid_children: "+span[div|ul|p]",
	//extended_valid_elements: "table[class=tableblock]",
    convert_urls: false,
    relative_urls: false,
    cleanup: false,
	setup: function(editor) {
		TinyMCE_custom_HR(editor);
		TinyMCE_custom_OnClick(editor, 'main');
		TinyMCE_custom_OnContent(editor, 'main');
		TinyMCE_AddContextMenu(editor, 'main');
		editor.addMenuItem('snippetsMenu', {
			text: 'Snippets',
			context: 'insert',
			menu: [
				{text: mainSnipets[0].text, onclick: function(){editor.insertContent(mainSnipets[0].value);}},
				{text: mainSnipets[1].text, onclick: function(){editor.insertContent(mainSnipets[1].value);}},
				{text: mainSnipets[4].text, onclick: function(){editor.insertContent(mainSnipets[4].value);}},
				{text: mainSnipets[2].text, onclick: function(){editor.insertContent(mainSnipets[2].value);}},
				{text: mainSnipets[3].text, onclick: function(){editor.insertContent(mainSnipets[3].value);}},
				{text: mainSnipets[5].text, onclick: function(){editor.insertContent(mainSnipets[5].value);}},
				{text: mainSnipets[6].text, onclick: function(){editor.insertContent(mainSnipets[6].value);}},
				{text: mainSnipets[7].text, onclick: function(){editor.insertContent(mainSnipets[7].value);}},
//				{text: mainSnipets[8].text, onclick: function(){editor.insertContent(mainSnipets[8].value);}},
//				{text: mainSnipets[9].text, onclick: function(){editor.insertContent(mainSnipets[9].value);}}
			]
		});
		// Insert Menu
		editor.addMenuItem('imageMenu', {
			text: 'Image',
			context: 'insert',
			menu: [
				{text: "Simple image",
					onclick: function() {
						openFileManagerForImages(editor, 'simple_no_zoom');
					},
					disabledStateSelector: ['img.gallery', 'img.zoom', 'img.inline_zoom', 'img.explainer', 'img.simple'],
					onPostRender: handleDisabledState
				},
				{text: "Zoom image",
					onclick: function() {
						openFileManagerForImages(editor, 'zoom');
					},
					disabledStateSelector: ['img.gallery', 'img.simple', 'img.inline_zoom', 'img.explainer', 'img.simple_no_zoom'],
					onPostRender: handleDisabledState
				},
				{text: "Inline zoom image",
					onclick: function() {
						openFileManagerForImages(editor, 'inline_zoom');
					},
					disabledStateSelector: ['img.gallery', 'img.simple', 'img.zoom', 'img.explainer', 'img.simple_no_zoom'],
					onPostRender: handleDisabledState
				}
			],
			disabledStateSelector: ['img.gallery', 'img.explainer'],
			onPostRender: handleDisabledState
		});

		editor.addMenuItem('galleryMenu', {
			text: 'Gallery',
			context: 'insert',
			onclick: function() {
				openFileManagerForImages(editor, 'gallery');
			},
			disabledStateSelector: ['img.simple', 'img.zoom', 'img.inline_zoom', 'img.explainer'],
			onPostRender: handleDisabledState
		});

		editor.addMenuItem('explainerMenu', {
			text: 'Layer gallery',
			context: 'insert',
			onclick: function() {
				openFileManagerForImages(editor, 'explainer');
			},
			disabledStateSelector: ['img.simple', 'img.zoom', 'img.inline_zoom', 'img.gallery'],
			onPostRender: handleDisabledState
		});

		editor.addMenuItem('summaryMenu', {
			text: 'Summary',
			context: 'insert',
			onclick: function() {
				// Add you own code to execute something on click
				editor.focus();
				var strClass = 'summary';
				var strTag = 'span';
				// remove old element with summary class, or remove class only
				var oldNodes = editor.dom.select('.' + strClass);
				for(cNodeIndex in oldNodes){
					var currentNode = oldNodes[cNodeIndex];
					if(currentNode.classList.length === 1 && currentNode.nodeName.toLowerCase() === strTag){
						editor.dom.remove(currentNode, true);
					}
					else{
						editor.dom.removeClass(currentNode, strClass);
					}
				}
				// add summary class for new element or add new element
				var nodeSelection = editor.selection.getNode(); // DOM Node
				var strSelection = editor.selection.getContent();
				if (nodeSelection.innerHTML !== strSelection) {
					// Add span for selection
					var el = editor.dom.create('span', {class: strClass}, strSelection);
					editor.selection.setNode(el);
				} else {
					editor.dom.addClass(nodeSelection, strClass);
				}
			}
		});
        
        editor.addMenuItem('exposedFieldMenu', {
            text: 'Exposed Field',
            context: 'insert',
            onclick: function() {
                openExposedFieldEditor(editor);
            }
        });
        
		editor.addMenuItem('alignmentMenu', {
			text: 'Alignment',
			context: 'format',
			menu: [
				{text: "Left", icon: "alignleft", onclick: function() {
						editor.focus();
						editor.formatter.apply("alignleft");
					}},
				{text: "Center", icon: "aligncenter", onclick: function() {
						editor.focus();
						editor.formatter.apply("aligncenter");
					}},
				{text: "Right", icon: "alignright", onclick: function() {
						editor.focus();
						editor.formatter.apply("alignright");
					}},
				{text: "Justify", icon: "alignjustify", onclick: function() {
						editor.focus();
						editor.formatter.apply("alignjustify");
					}}
			]
		});
		editor.addMenuItem('formatMenu', {
			text: 'Formats',
			context: 'format',
			menu: [
				{text: "Title", onclick: function() {
						editor.focus();
						editor.formatter.apply("formatH1");
						return false;
					}},
				{text: "Subheading", onclick: function() {
						editor.focus();
						editor.formatter.apply("formatH1");
						editor.formatter.apply("formatSmall");
						return false;
					}},
				{text: "Subline", onclick: function() {
						editor.focus();
						editor.formatter.apply("formatSubline");
						return false;
					}},
				{text: "Copy text", onclick: function() {
						editor.focus();
						editor.formatter.apply("formatP");
						return false;
					}},
                {text: "Notice", onclick: function() {
						editor.focus();
						editor.formatter.apply("formatNotice");
						return false;
					}},
			]
		});

		// Toolbar Buttons
		editor.addButton('btnGallery', {
			image: relPath + 'img/backend/gallery_icon.png',
			icon: false,
			tooltip: "Gallery",
			onclick: function() {
				openFileManagerForImages(editor, 'gallery');
			},
			disabledStateSelector: ['img.simple', 'img.zoom', 'img.explainer','img.simple_no_zoom','img.inline_zoom']
		});

		editor.addButton('btnExplainer', {
			image: relPath + 'img/backend/gallery_icon.png',
			icon: false,
			tooltip: "Layer gallery",
			onclick: function() {
				openFileManagerForImages(editor, 'explainer');
			},
			disabledStateSelector: ['img.simple', 'img.zoom', 'img.gallery','img.simple_no_zoom','img.inline_zoom']
		});

		// Prepare configuration for Single and Zoom Image
		var arrImageSettings = [];

		var arrImagesSnipets = dbdn_snippets.filter(function(el) {
			return el.section ? el.section.indexOf("images") !== -1 : true;
		});

		var arrDisableStateSelector = [
			{image_type: 'simple_no_zoom', disabledStateSelector: ['img.gallery', 'img.zoom', 'img.inline_zoom', 'img.simple'], onPostRender: handleDisabledState},
			{image_type: 'zoom', disabledStateSelector: ['img.gallery', 'img.simple_no_zoom', 'img.inline_zoom', 'img.simple'], onPostRender: handleDisabledState},
			{image_type: 'inline_zoom', disabledStateSelector: ['img.gallery', 'img.simple_no_zoom', 'img.zoom', 'img.simple'], onPostRender: handleDisabledState}
		];

		for (var i = 0; i < arrImagesSnipets.length; i++) {
			var item = arrImagesSnipets[i];
			var arrExtension = arrDisableStateSelector.filter(function(el) {
				return el.image_type === item.image_type;
			});
			var merge_result = jQuery.extend(item, arrExtension[0]);
			arrImageSettings.push(merge_result);
		}

		editor.addButton('btnImages', {
			text: ' ',
			type: 'listbox',
			icon: 'image',
			tooltip: 'Images',
			onselect: function(e) {
				this.text('');
				openFileManagerForImages(editor, e.control.settings.image_type);
				// UnMarck the clicked MenuItem
				e.control.removeClass('selected');
			},
			values: arrImageSettings,
			disabledStateSelector: 'img.gallery'
        });

		editor.addButton('btnAnchorNav', {
			icon:'anchor',
			tooltip: "Anchor navigation",
			onclick: function() {
                var selectedNode = editor.selection.getNode(), name = '';

                if (selectedNode.tagName == 'IMG' && $(selectedNode).hasClass('anchor_nav')) {
                    name = selectedNode.name || selectedNode.id || $(selectedNode).attr('data-text') ||'';
                }

                editor.windowManager.open({
                    title: 'Anchor navigation',
                    body: {type: 'textbox', name: 'name', size: 40, label: 'Title', value: name},
                    onsubmit: function(e) {
                        editor.execCommand('mceInsertContent', false, editor.dom.createHTML('img', {
                            'data-text': e.data.name,
                            class:'anchor_nav',
                            src:relPath+'img/backend/anchor.gif'
                        }));
                    }
                });
			},
            stateSelector: 'img.anchor_nav'
		});
        
        
        editor.addButton('btnExposedField', {
            icon:'blockquote',
            tooltip: "Exposed Field",
            onclick: function() {
                openExposedFieldEditor(editor);
            },
            stateSelector: 'div.transition-element'
        });

		editor.addButton('btnFormats', {
			text: 'Formats',
			type: 'listbox',
			tooltip: 'Formats',
			values: [{text: 'Title', onclick: function() {
					editor.focus();
					editor.formatter.apply("formatH1");
					return false;
				}}, {text: "Subheading", onclick: function() {
					editor.focus();
					editor.formatter.apply("formatH1");
					editor.formatter.apply("formatSmall");
					return false;
				}},
				{text: "Subline", onclick: function() {
                    editor.focus();
                    editor.formatter.apply("formatSubline");
                    return false;
                }},
				{text: "Copy text", onclick: function() {
					editor.focus();
					editor.formatter.apply("formatP");
					return false;
				}},
                {text: "Notice", onclick: function() {
                    editor.focus();
                    editor.formatter.apply("formatNotice");
                    return false;
                }},
			]
		});
	}
});

var tiny_right = tinymce.init({
	menubar: false,
	skin: "dbdnskin",
	entity_encoding: "raw",
	selector: "textarea#right_body",
	plugins: ["code", "visualblocks", "link", "textcolor", "anchor"],
	formats: {
		formatH1: {
			block: 'h1'
		},
		formatSmall: {
			'inline': 'small'
		},
		formatP: {
			block: 'p'
		},
		removeformat : [
			{selector : 'h1', remove : 'all', split : true, expand : false, block_expand : true, deep : true},
			{selector : 'small', remove : 'all', split : true, expand : false, block_expand : true, deep : true},
			{
				selector: 'a,b,strong,em,i,font,u,strike,sub,sup,dfn,code,samp,kbd,var,cite,mark,q',
				remove: 'all',
				split: true,
				expand: false,
				block_expand: true,
				deep: true
			},
			{selector: 'span', attributes: ['style', 'class'], remove: 'empty', split: true, expand: false, deep: true},
			{selector: '*', attributes: ['style', 'class'], split: false, expand: false, deep: true}
		]
	},
//	style_formats: [
//		{title: 'Title', block: 'h1'},
//		{title: 'Subheading', block: 'h1', inline: 'small'},
//		{title: 'Copy text', block: 'p'}
//	],
	textcolor_map: predefinedColors,
	tools: "inserttable",
	body_class: "content_right content_wrapper",
	//content_css: relPath + "css/main.css, " + relPath + "css/backend.css, " + relPath + "css/backend_content.css",
	content_css: css_content,
	toolbar1: "btnFormats bold underline superscript subscript forecolor visualblocks",
	toolbar2: "btnSnipets btnImage link unlink dbdnHR removeformat code",
	forced_root_block: 'p',
	valid_children: "+span[div|ul|p]",
	convert_urls: false,
	relative_urls: false,
	cleanup: false,
	setup: function(editor) {
		TinyMCE_custom_HR(editor);
		TinyMCE_custom_OnClick(editor, 'right');
		TinyMCE_custom_OnContent(editor, 'right');
		TinyMCE_AddContextMenu(editor, 'right');
		editor.addButton('btnImage', {
			icon: 'image',
			tooltip: "Simple image",
			onclick: function() {
				openFileManagerForImages(editor, 'simple');
			},
			disabledStateSelector: ['img.gallery', 'img.zoom']
		});
		editor.addButton('btnSnipets', {
			type: 'listbox',
			text: 'Snippets',
			icon: false,
			tooltip: 'Snippets',
			onselect: function(e) {
				editor.insertContent(this.value());
				//$(editor.contentDocument).find('div#right.div_right_edit').append(this.value());
				//$(editor.contentDocument).find('div#right.div_right_edit').html($(editor.contentDocument).find('div#right.div_right_edit').html().replace(/(<br\/?>)/gi,''));
				//editor.setContent(editor.getContent().replace(/(<br\/?>)/gi,''));
				this.text('Snippets');
				this.value('');
			},
			values: dbdn_snippets.filter(function(el) {
				return el.section ? el.section.indexOf("right") !== -1 : true;
			})
		});
		editor.addButton('btnFormats', {
			text: 'Formats',
			type: 'listbox',
			tooltip: 'Formats',
			values: [{text: 'Title', onclick: function() {
						editor.focus();
						editor.formatter.apply("formatH1");
						return false;
					}}, {text: "Subheading", onclick: function() {
						editor.focus();
						editor.formatter.apply("formatH1");
						editor.formatter.apply("formatSmall");
						return false;
					}}, {text: "Copy text", onclick: function() {
						editor.focus();
						editor.formatter.apply("formatP");
						return false;
					}}]
		});
	}
});

var tiny_farRight = tinymce.init({
	menubar: false,
	skin: "dbdnskin",
	entity_encoding: "raw",
	selector: "textarea#farRight_body",
	plugins: ["code", "visualblocks", "table", "link", "textcolor", "anchor"],
	formats: {
		formatH1: {
			block: 'h1'
		},
		formatSmall: {
			'inline': 'small'
		},
		formatP: {
			block: 'p'
		},
		removeformat : [
			{selector : 'h1', remove : 'all', split : true, expand : false, block_expand : true, deep : true},
			{selector : 'small', remove : 'all', split : true, expand : false, block_expand : true, deep : true},
			{
				selector: 'a,b,strong,em,i,font,u,strike,sub,sup,dfn,code,samp,kbd,var,cite,mark,q',
				remove: 'all',
				split: true,
				expand: false,
				block_expand: true,
				deep: true
			},
			{selector: 'span', attributes: ['style', 'class'], remove: 'empty', split: true, expand: false, deep: true},
			{selector: '*', attributes: ['style', 'class'], split: false, expand: false, deep: true}
		]
	},
//	style_formats: [
//		{title: 'Title', block: 'h1'},
//		{title: 'Subheading', block: 'h1', inline: 'small'},
//		{title: 'Copy text', block: 'p'}
//	],
	textcolor_map: predefinedColors,
	tools: "inserttable",
	body_class: "content_farRight content_wrapper",
	//content_css: relPath + "css/main.css, " + relPath + "css/backend.css, " + relPath + "css/backend_content.css",
	content_css: css_content,
	toolbar1: "btnFormats bold underline superscript subscript forecolor visualblocks",
	toolbar2: "btnSnipets btnImage link unlink dbdnHR removeformat code",
	forced_root_block: 'p',
	valid_children: "+span[div|ul|p]",
	convert_urls: false,
	relative_urls: false,
	setup: function(editor) {
		TinyMCE_custom_HR(editor);
		TinyMCE_custom_OnClick(editor, 'farright');
		TinyMCE_custom_OnContent(editor, 'farright');
		TinyMCE_AddContextMenu(editor, 'farright');
		editor.addButton('btnImage', {
			icon: 'image',
			tooltip: "Simple image",
			onclick: function() {
				openFileManagerForImages(editor, 'simple');
			},
			disabledStateSelector: ['img.gallery', 'img.zoom']
		});
		editor.addButton('btnSnipets', {
			type: 'listbox',
			text: 'Snippets',
			icon: false,
			tooltip: 'Snippets',
			onselect: function(e) {
				editor.insertContent(this.value());
				//$(editor.contentDocument).find('div#farright.div_farright_edit').append(this.value());
				//$(editor.contentDocument).find('div#farright.div_farright_edit').html($(editor.contentDocument).find('div#farright.div_farright_edit').html().replace(/(<br\/?>)/gi,''));
				//editor.setContent(editor.getContent().replace(/(<br\/?>)/gi,''));
				this.text('Snippets');
				this.value('');
			},
			values: dbdn_snippets.filter(function(el) {
				return el.section ? el.section.indexOf("right") !== -1 : true;
			})
		});
		editor.addButton('btnFormats', {
			text: 'Formats',
			type: 'listbox',
			tooltip: 'Formats',
			values: [{text: 'Title', onclick: function() {
						editor.focus();
						editor.formatter.apply("formatH1");
						return false;
					}}, {text: "Subheading", onclick: function() {
						editor.focus();
						editor.formatter.apply("formatH1");
						editor.formatter.apply("formatSmall");
						return false;
					}}, {text: "Copy text", onclick: function() {
						editor.focus();
						editor.formatter.apply("formatP");
						return false;
					}}]
		});
	}
});

var tiny_author = tinymce.init({
	menubar: false,
	skin: "dbdnskin",
	entity_encoding: "raw",
	selector: "textarea#author_body",
	plugins: ["code", "visualblocks", "table", "link", "textcolor", "anchor"],
	formats: {
		formatH1: {
			block: 'h1'
		},
		formatSmall: {
			'inline': 'small'
		},
		formatP: {
			block: 'p'
		},
		removeformat : [
			{selector : 'h1', remove : 'all', split : true, expand : false, block_expand : true, deep : true},
			{selector : 'small', remove : 'all', split : true, expand : false, block_expand : true, deep : true},
			{
				selector: 'a,b,strong,em,i,font,u,strike,sub,sup,dfn,code,samp,kbd,var,cite,mark,q',
				remove: 'all',
				split: true,
				expand: false,
				block_expand: true,
				deep: true
			},
			{selector: 'span', attributes: ['style', 'class'], remove: 'empty', split: true, expand: false, deep: true},
			{selector: '*', attributes: ['style', 'class'], split: false, expand: false, deep: true}
		]
	},

	textcolor_map: predefinedColors,
	tools: "inserttable",
	body_class: "content_author content_wrapper",
	//content_css: relPath + "css/main.css, " + relPath + "css/backend.css, " + relPath + "css/backend_content.css",
	content_css: css_content,
	toolbar1: "btnFormats bold underline superscript subscript forecolor visualblocks",
	toolbar2: "btnSnipets btnImage link unlink dbdnHR removeformat code",
	forced_root_block: 'p',
	valid_children: "+span[div|ul|p]",
	convert_urls: false,
	relative_urls: false,
	setup: function(editor) {
		TinyMCE_custom_HR(editor);
		TinyMCE_custom_OnClick(editor, 'author');
		TinyMCE_custom_OnContent(editor, 'author');
		TinyMCE_AddContextMenu(editor, 'author');
		editor.addButton('btnImage', {
			icon: 'image',
			tooltip: "Author image",
			onclick: function() {
				openFileManagerForImages(editor, 'simple');
			},
			disabledStateSelector: ['img.gallery', 'img.zoom']
		});
	}
});

var tiny_banner = tinymce.init({
	menubar: false,
	skin: "dbdnskin",
	selector: "textarea#banner_body",
	plugins: ["code", "link"],
	textcolor_map: predefinedColors,
	toolbar: "btnImage link unlink code",
	forced_root_block: false,
	valid_children: "+span[div|ul|p]",
	convert_urls: false,
	relative_urls: false,
	tools: "inserttable",
	setup: function(editor) {
		editor.addButton('btnImage', {
			icon: 'image',
			tooltip: "Banner image",
			onclick: function() {
				openFileManagerForImages(editor, 'simple');
			},
			disabledStateSelector: ['img.gallery', 'img.zoom']
		});
	}
});
var tablet_banner = tinymce.init({
	menubar: false,
	skin: "dbdnskin",
	selector: "textarea#tablet_banner_body",
	plugins: ["code", "link"],
	textcolor_map: predefinedColors,
	toolbar: "btnImage left-align centrered right-align link unlink code",
	forced_root_block: false,
	valid_children: "+span[div|ul|p]",
	convert_urls: false,
	relative_urls: false,
	tools: "inserttable",
	setup: function(editor) {
		editor.addButton('btnImage', {
			icon: 'image',
			tooltip: "Banner image",
			onclick: function() {
				openFileManagerForImages(editor, 'simple');
			},
			disabledStateSelector: ['img.gallery', 'img.zoom']
		});
        editor.addButton('left-align', {
            icon: 'alignleft',
            tooltip: "Cropped from left",
            onclick: function() {
                alignBannerImage(editor, 'left');
                return false;
            }
        });
        editor.addButton('centrered', {
            icon: 'aligncenter',
            tooltip: "Centrered",
            onclick: function() {
                alignBannerImage(editor, 'center');
                return false;
            }
        });
        editor.addButton('right-align', {
            icon: 'alignright',
            tooltip: "Cropped from right",
            onclick: function() {
                alignBannerImage(editor, 'right');
                return false;
            }
        });
	}
});
var mobile_banner = tinymce.init({
	menubar: false,
	skin: "dbdnskin",
	selector: "textarea#mobile_banner_body",
	plugins: ["code", "link"],
	textcolor_map: predefinedColors,
	toolbar: "btnImage left-align centrered right-align link unlink code",
	forced_root_block: false,
	valid_children: "+span[div|ul|p]",
	convert_urls: false,
	relative_urls: false,
	tools: "inserttable",
	setup: function(editor) {
		editor.addButton('btnImage', {
			icon: 'image',
			tooltip: "Banner image",
			onclick: function() {
				openFileManagerForImages(editor, 'simple');
			},
			disabledStateSelector: ['img.gallery', 'img.zoom']
		});
        editor.addButton('left-align', {
            icon: 'alignleft',
            tooltip: "Cropped from left",
            onclick: function() {
                alignBannerImage(editor, 'left');
                return false;
            }
        });
        editor.addButton('centrered', {
            icon: 'aligncenter',
            tooltip: "Centrered",
            onclick: function() {
                alignBannerImage(editor, 'center');
                return false;
            }
        });
        editor.addButton('right-align', {
            icon: 'alignright',
            tooltip: "Cropped from right",
            onclick: function() {
                alignBannerImage(editor, 'right');
                return false;
            }
        });
	}
});

var tiny_faq_answer = tinymce.init({
        menubar: false,
        skin: "dbdnskin",
        selector: "textarea#faq_answer",
        toolbar: "bold italic link unlink",
        plugins: ["link",],
        forced_root_block: 'p',
        valid_children: "+span[div|ul|p]",
        convert_urls: false,
        relative_urls: false,
        resize: false,
        body_class: "text_editor",
        content_css: css_content,
    });

var nl_css_content = relPath + "css/backend_newsletter.css";

var tiny_header_d = tinymce.init({
	menubar: false,
	skin: "dbdnskin",
	selector: "textarea#header_d_body",
	plugins: ["code", "link"],
	textcolor_map: predefinedColors,
	toolbar: "btnImage code",
	forced_root_block: false,
	valid_children: "+span[div|ul|p]",
	convert_urls: false,
	relative_urls: false,
	tools: "inserttable",
	object_resizing : false,
	setup: function(editor) {
		editor.addButton('btnImage', {
			icon: 'image',
			tooltip: "Newsletter desktop image",
			onclick: function() {
				openFileManagerForImages(editor, 'nl_header_d', true);
			},
			disabledStateSelector: ['img.gallery', 'img.zoom']
		});
	}
});

var tiny_header_m = tinymce.init({
	menubar: false,
	skin: "dbdnskin",
	selector: "textarea#header_m_body",
	plugins: ["code", "link"],
	textcolor_map: predefinedColors,
    body_class: "header_m",
    content_css: nl_css_content,
	toolbar: "btnImage code",
	forced_root_block: false,
	valid_children: "+span[div|ul|p]",
	convert_urls: false,
	relative_urls: false,
	tools: "inserttable",
	object_resizing : false,
	setup: function(editor) {
		editor.addButton('btnImage', {
			icon: 'image',
			tooltip: "Newsletter mobile image",
			onclick: function() {
				openFileManagerForImages(editor, 'nl_header_m', true);
			},
			disabledStateSelector: ['img.gallery', 'img.zoom']
		});
	}
});

var tiny_nl_main = tinymce.init({
	skin: "dbdnskin",
	element_format : "html",
	entity_encoding: "raw",
    selector: "textarea#content_body",
    plugins: ["code", "visualblocks", "table", "link", "textcolor", "anchor"],
//	menubar: "edit insert view format table",
	menu:{ // this is the complete default configuration
        edit   : {title : 'Edit'  , items : 'undo redo | cut copy paste pastetext | selectall'},
        insert : {title : 'Insert', items : 'sectionsMenu | snippetsMenu | headerBoxes | imageMenu'},
        view   : {title : 'View'  , items : 'visualblocks visualaid'},
        format : {title : 'Format', items : 'bold underline superscript subscript | alignmentMenu | removeformat'},
        table  : {title : 'Table' , items : 'inserttable tableprops deletetable | cell row column'}
    },
    textcolor_map: predefinedColors,
    tools: "inserttable",
    body_class: "content_content_text content_main content_wrapper",
    content_css: nl_css_content,
    toolbar1: "bold underline superscript subscript alignleft aligncenter alignright alignjustify bullist numlist forecolor",
    toolbar2: "link unlink btnHref btnAnchorNav removeformat code",
    forced_root_block: '',
    valid_children: "+span[div|ul|p]+a[span|strong]",
    convert_urls: false,
    relative_urls: false,
    cleanup: false,
	object_resizing : false,
    link_class_list:[{title: 'Default', value: 'default_anchor'}],
	setup: function(editor) {
		TinyMCE_custom_OnClick(editor, 'content');
		TinyMCE_custom_OnContent(editor, 'content');
		TinyMCE_AddContextMenuNL(editor, 'content');
		editor.addMenuItem('snippetsMenu', {
			text: 'Snippets',
			context: 'insert',
			menu: [
				{text: nlSnipets[0].text, onclick: function(){editor.insertContent(nlSnipets[0].value);}},
				{text: nlSnipets[1].text, onclick: function(){editor.insertContent(nlSnipets[1].value);}},
				{text: nlSnipets[2].text, onclick: function(){editor.insertContent(nlSnipets[2].value);}},
			]
		});

		editor.addMenuItem('headerBoxes', {
			text: 'Header Boxes',
			context: 'insert',
			menu: [
				{text: nlSnipets[9].text, onclick: function(){editor.insertContent(nlSnipets[9].value);}},
				{text: nlSnipets[3].text, onclick: function(){editor.insertContent(nlSnipets[3].value);}},
			]
		});

		editor.addMenuItem('sectionsMenu', {
			text: 'Section',
			context: 'insert',
			menu: [
				{text: nlSnipets[4].text, onclick: function(){editor.insertContent(nlSnipets[4].value);}},
				{text: nlSnipets[5].text, onclick: function(){editor.insertContent(nlSnipets[5].value);}},
				{text: nlSnipets[6].text, onclick: function(){editor.insertContent(nlSnipets[6].value);}},
			]
		});

		editor.addMenuItem('imageMenu', {
			text: 'Image',
			context: 'insert',
			menu: [
				{text: "Simple image",
					onclick: function() {
						openFileManagerForImages(editor, 'nl_simple', true);
					},
					disabledStateSelector: ['img.explainer', 'img.simple'],
					onPostRender: handleDisabledState
				},
				{text: "Text Right Image",
					onclick: function() {
						openFileManagerForImages(editor, 'nl_text_right', true);
					},
					disabledStateSelector: ['img.explainer', 'img.simple'],
					onPostRender: handleDisabledState
				},
				{text: "Text Bottom Image",
					onclick: function() {
						openFileManagerForImages(editor, 'nl_text_bottom', true);
					},
					disabledStateSelector: ['img.explainer', 'img.simple'],
					onPostRender: handleDisabledState
				},
			],
			disabledStateSelector: ['img.gallery', 'img.explainer'],
			onPostRender: handleDisabledState
		});

		editor.addMenuItem('alignmentMenu', {
			text: 'Alignment',
			context: 'format',
			menu: [
				{text: "Left", icon: "alignleft", onclick: function() {
						editor.focus();
						editor.formatter.apply("alignleft");
					}},
				{text: "Center", icon: "aligncenter", onclick: function() {
						editor.focus();
						editor.formatter.apply("aligncenter");
					}},
				{text: "Right", icon: "alignright", onclick: function() {
						editor.focus();
						editor.formatter.apply("alignright");
					}},
				{text: "Justify", icon: "alignjustify", onclick: function() {
						editor.focus();
						editor.formatter.apply("alignjustify");
					}}
			]
		});
		editor.addButton('btnAnchorNav', {
			icon:'anchor',
			tooltip: "Newsletter Anchor",
			onclick: function() {
                var selectedNode = editor.selection.getNode(), name = '';

                if (selectedNode.tagName == 'A' && $(selectedNode).hasClass('nl_anchor')) {
                    name = selectedNode.name || selectedNode.id || $(selectedNode).attr('name') ||'';
                }

                editor.windowManager.open({
                    title: 'Anchor navigation',
                    body: {type: 'textbox', name: 'name', size: 40, label: 'Name', value: name},
                    onsubmit: function(e) {
                        editor.execCommand('mceInsertContent', false, editor.dom.createHTML('a', {
                            name: e.data.name,
                            class:'nl_anchor',
                        }));
                    }
                });
			},
            stateSelector: 'a.nl_anchor'
		});
		editor.addButton('btnHref', {
			icon:'butotn-href',
			tooltip: "Link location",
			onclick: function() {
                var selectedNode = editor.selection.getNode(), href = '';

                if (selectedNode.tagName == 'A') {
                    href = $(selectedNode).attr('href') ||'';
                }else if($(selectedNode).parents('a').length > 0){
					selectedNode = $(selectedNode).parents('a').first();
					href = $(selectedNode).attr('href') || '';
				}

                editor.windowManager.open({
                    title: 'Link location',
                    body: {type: 'textbox', name: 'href', size: 40, label: 'Location', value: href},
                    onsubmit: function(e) {
						$(selectedNode).attr('href',e.data.href);
						$(selectedNode).attr('data-mce-href',e.data.href);
                    }
                });
			},
            stateSelector: 'a'
		});
	}
});

var tiny_footer_i = tinymce.init({
	skin: "dbdnskin",
	element_format : "html",
	entity_encoding: "raw",
    selector: "textarea#footer_i_body",
    plugins: ["code", "visualblocks", "table", "link", "textcolor", "anchor"],
//	menubar: "edit insert view format table",
	menu:{ // this is the complete default configuration
        edit   : {title : 'Edit'  , items : 'undo redo | cut copy paste pastetext | selectall'},
        insert : {title : 'Insert', items : 'sectionsMenu'},
        view   : {title : 'View'  , items : 'visualblocks visualaid'},
        format : {title : 'Format', items : 'bold underline superscript subscript | alignmentMenu | removeformat'},
        table  : {title : 'Table' , items : 'inserttable tableprops deletetable | cell row column'}
    },
    textcolor_map: predefinedColors,
    tools: "inserttable",
    body_class: "content_content_text content_main content_wrapper",
    content_css: nl_css_content,
    toolbar1: "bold underline superscript subscript alignleft aligncenter alignright alignjustify bullist numlist forecolor",
    toolbar2: "link unlink removeformat code",
    forced_root_block: '',
    valid_children: "+span[div|ul|p]",
    convert_urls: false,
    relative_urls: false,
    cleanup: false,
	object_resizing : false,
	setup: function(editor) {
		TinyMCE_custom_OnClick(editor, 'footer_i');
		TinyMCE_custom_OnContent(editor, 'footer_i');

		editor.addMenuItem('alignmentMenu', {
			text: 'Alignment',
			context: 'format',
			menu: [
				{text: "Left", icon: "alignleft", onclick: function() {
						editor.focus();
						editor.formatter.apply("alignleft");
					}},
				{text: "Center", icon: "aligncenter", onclick: function() {
						editor.focus();
						editor.formatter.apply("aligncenter");
					}},
				{text: "Right", icon: "alignright", onclick: function() {
						editor.focus();
						editor.formatter.apply("alignright");
					}},
				{text: "Justify", icon: "alignjustify", onclick: function() {
						editor.focus();
						editor.formatter.apply("alignjustify");
					}}
			]
		});
        editor.addMenuItem('sectionsMenu', {
			text: 'Section',
			context: 'insert',
			menu: [
				{text: nlSnipets[7].text, onclick: function(){editor.insertContent(nlSnipets[7].value);}},
			]
		});
	}
});

var tiny_footer_e = tinymce.init({
	skin: "dbdnskin",
	element_format : "html",
	entity_encoding: "raw",
    selector: "textarea#footer_e_body",
    plugins: ["code", "visualblocks", "table", "link", "textcolor", "anchor"],
//	menubar: "edit insert view format table",
	menu:{ // this is the complete default configuration
        edit   : {title : 'Edit'  , items : 'undo redo | cut copy paste pastetext | selectall'},
        insert : {title : 'Insert', items : 'sectionsMenu'},
        view   : {title : 'View'  , items : 'visualblocks visualaid'},
        format : {title : 'Format', items : 'bold underline superscript subscript | alignmentMenu | removeformat'},
        table  : {title : 'Table' , items : 'inserttable tableprops deletetable | cell row column'}
    },
    textcolor_map: predefinedColors,
    tools: "inserttable",
    body_class: "content_content_text content_main content_wrapper",
    content_css: nl_css_content,
    toolbar1: "bold underline superscript subscript alignleft aligncenter alignright alignjustify bullist numlist forecolor",
    toolbar2: "link unlink removeformat code",
    forced_root_block: '',
    valid_children: "+span[div|ul|p]",
    convert_urls: false,
    relative_urls: false,
    cleanup: false,
	object_resizing : false,
	setup: function(editor) {
		TinyMCE_custom_OnClick(editor, 'footer_e');
		TinyMCE_custom_OnContent(editor, 'footer_e');

		editor.addMenuItem('alignmentMenu', {
			text: 'Alignment',
			context: 'format',
			menu: [
				{text: "Left", icon: "alignleft", onclick: function() {
						editor.focus();
						editor.formatter.apply("alignleft");
					}},
				{text: "Center", icon: "aligncenter", onclick: function() {
						editor.focus();
						editor.formatter.apply("aligncenter");
					}},
				{text: "Right", icon: "alignright", onclick: function() {
						editor.focus();
						editor.formatter.apply("alignright");
					}},
				{text: "Justify", icon: "alignjustify", onclick: function() {
						editor.focus();
						editor.formatter.apply("alignjustify");
					}}
			]
		});
        editor.addMenuItem('sectionsMenu', {
			text: 'Section',
			context: 'insert',
			menu: [
				{text: nlSnipets[7].text, onclick: function(){editor.insertContent(nlSnipets[7].value);}},
			]
		});
	}
});

var tiny_footer_e_bl = tinymce.init({
	skin: "dbdnskin",
	element_format : "html",
	entity_encoding: "raw",
    selector: "textarea#footer_e_bl_body",
    plugins: ["code", "visualblocks", "table", "link", "textcolor", "anchor"],
//	menubar: "edit insert view format table",
	menu:{ // this is the complete default configuration
        edit   : {title : 'Edit'  , items : 'undo redo | cut copy paste pastetext | selectall'},
        insert : {title : 'Insert', items : 'sectionsMenu'},
        view   : {title : 'View'  , items : 'visualblocks visualaid'},
        format : {title : 'Format', items : 'bold underline superscript subscript | alignmentMenu | removeformat'},
        table  : {title : 'Table' , items : 'inserttable tableprops deletetable | cell row column'}
    },
    textcolor_map: predefinedColors,
    tools: "inserttable",
    body_class: "content_content_text content_main content_wrapper",
    content_css: nl_css_content,
    toolbar1: "bold underline superscript subscript alignleft aligncenter alignright alignjustify bullist numlist forecolor",
    toolbar2: "link unlink removeformat code",
    forced_root_block: '',
    valid_children: "+span[div|ul|p]",
    convert_urls: false,
    relative_urls: false,
    cleanup: false,
	object_resizing : false,
	setup: function(editor) {
		TinyMCE_custom_OnClick(editor, 'footer_e_bl');
		TinyMCE_custom_OnContent(editor, 'footer_e_bl');

		editor.addMenuItem('alignmentMenu', {
			text: 'Alignment',
			context: 'format',
			menu: [
				{text: "Left", icon: "alignleft", onclick: function() {
						editor.focus();
						editor.formatter.apply("alignleft");
					}},
				{text: "Center", icon: "aligncenter", onclick: function() {
						editor.focus();
						editor.formatter.apply("aligncenter");
					}},
				{text: "Right", icon: "alignright", onclick: function() {
						editor.focus();
						editor.formatter.apply("alignright");
					}},
				{text: "Justify", icon: "alignjustify", onclick: function() {
						editor.focus();
						editor.formatter.apply("alignjustify");
					}}
			]
		});
        editor.addMenuItem('sectionsMenu', {
			text: 'Section',
			context: 'insert',
			menu: [
				{text: nlSnipets[7].text, onclick: function(){editor.insertContent(nlSnipets[7].value);}},
			]
		});
	}
});

function createTitleTinyMce() {
    var mode = $('#filemanger_mode').data('value');
    var toolbar, formats, setup;
    if (mode == 'images') {
        toolbar = "imageTitle bold";
        formats = {
            formatH1: {
                block: 'h1'
            },
            removeformat: [
                {selector: 'h1', remove: 'all', split: true, expand: false, block_expand: true, deep: true}
            ]
        };
        setup = function (editor) {
            editor.addButton('imageTitle', {
                text: 'H',
                icon: false,
                classes: 'widget btn image-title',
                tooltip: "Image Title",
                onclick: function () {
                    editor.focus();
                    if (editor.formatter.match("formatH1")) {
                        editor.formatter.remove("formatH1")
                    } else {
                        editor.formatter.apply("formatH1");
                    }
                    return false;
                },
            });
        };
    }else if(mode == 'downloads'){
        toolbar = false;
        formats ={};
        setup = function(editor){};
    }
    var tiny_image = tinymce.init({
        menubar: false,
        skin: "dbdnskin",
        selector: "textarea#textarea_descriptionDE",
        toolbar: toolbar,
        forced_root_block: 'p',
        valid_children: "+span[div|ul|p]",
        convert_urls: false,
        relative_urls: false,
        resize: false,
        body_class: "text_editor",
        content_css: css_content,
        formats: formats,
        setup: setup,
    });
    var tiny_image = tinymce.init({
        menubar: false,
        skin: "dbdnskin",
        selector: "textarea#textarea_descriptionEN",
        toolbar: toolbar,
        forced_root_block: 'p',
        valid_children: "+span[div|ul|p]",
        convert_urls: false,
        relative_urls: false,
        resize: false,
        body_class: "text_editor",
        content_css: css_content,
        formats: formats,
        setup: setup
    });
}

function createTitleDF(){

}

function getImagePath(imagePostData, callback_edit){
    if (imagePostData.length > 0) {
        var data = {
                'files': imagePostData,
                '_token': $('#token').val()
            };
        $.ajax({
            type : 'POST',
            url : '/gallery',
            data : data,
            success : function(json){
				var response_data = json.response;
				callback_edit(response_data);
            }
        });
    }
}
var oTable;
//var relPath = getRelativePath();

$(document).ready(function() {

	oTable = $('#groups').dataTable({
		"bProcessing": false,
		"sAjaxSource": relPath+"api/v1/group/all",
		"sAjaxDataProp": 'response',
		"fnServerData": fnServerData,
		"iDisplayLength": 45,
		"bFilter": true,
		"sPaginationType": 'ellipses',
		"bLengthChange": false,
		"iShowPages": 10,
		"bSort": false,
		"oLanguage": {
			"sEmptyTable": " "},
		"aoColumns": [
			{"sType": "numeric", "mData": "id", "sWidth": "40px", "bSearchable": false, "sClass": "group_id"},
			{"sType": "string", "mData": "name", "sWidth": "570px", "sClass": "strong", "bSearchable": true}
		]
	});

	var oSettings = oTable.fnSettings();
	oSettings.sAlertTitle = js_localize['groups.list.title'];
	
	$("#groups tbody").click(function(event) {		
		if (!$(event.target).hasClass('dataTables_empty')){
			closeProfile();
			$(oTable.fnSettings().aoData).each(function() {
				$(this.nTr).removeClass('row_selected');
			});
			$(event.target.parentNode).addClass('row_selected');
			
			if(!$('a.edit').is(":visible") ){
					$('a.edit').show();
					$('a.remove').show();
				}
		}else{
			$('a.edit').hide();
			$('a.remove').hide();
		}
		
		$('.user_edit_wrapper').css('top', $('#groups_wrapper').position().top + $(event.target.parentNode).position().top + $(event.target.parentNode).height() + 1);
		$('.user_edit_wrapper').show();
		//alert($('#users_wrapper').position().top)

	});
	
	$('body').click(function(event) {
		if($(event.target).parents('tbody').length == 0 && $(event.target).parents('.user_edit_wrapper').length == 0 && $('#popup_overlay').length == 0){
			closeProfile();
		}
		if (event.target.parentNode) {
			if (!(event.target.parentNode.className == "admin_data" || event.target.parentNode.id == "admin_info" || event.target.parentNode.id == "admin_action")) {
				if ($('#admin_action').is(":visible"))
				{
					$("#admin_info").removeClass('active');
					$('#admin_action').hide();
				}
			}
		}
	});
	
	/* Add a click handler for the delete row */
	$('.user_actions .nav_box.remove').click(function() {
		jConfirm($(this).attr('confirm'), js_localize['groups.remove.title'],"success", function(r) {			
			if (r) {
				var anSelected = fnGetSelected(oTable);
				closeProfile();
				if (anSelected.length !== 0) {
					jQuery.ajax({
						type: "DELETE",
						url: relPath+"api/v1/group/delete/" + $(anSelected[0]).find('.group_id').html(),
						success: function(data) {
							oTable.fnDeleteRow(anSelected[0]);
							jAlert(js_localize['groups.remove.success'].replace('{0}',data.response.name),js_localize['groups.remove.title'],"success");
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
							var message = JSON_ERROR.composeMessageFromJSON(data, true);
							jAlert(message, js_localize['groups.remove.title'], "error");
						},
					});
				}
			}
		});	
	});

	$('.user_actions .nav_box.edit').click(function() {
		$('#wrapper_form_groups').openDialog($('div.user_edit_wrapper .textarea_overlay'));
		$('#user_form .input_group').removeClass('error');
		$('body').css('cursor','progress');
		$('.user_actions .nav_box').removeClass('active');
		$(this).addClass('active');
		var anSelected = fnGetSelected(oTable);
		$('.user_edit_wrapper .user_fields').show();
		$('#user_form input:checkbox:checked').removeAttr('checked');
		populateForm($(anSelected[0]).find('.group_id').html());
		var height = $('.user_edit_wrapper').position().top + $('.user_edit_wrapper').height() + $('.user_fields').height() + 10;
		if($('.user_table').height() < height){
			$('.user_table').css('height', height + 'px');
			$('.dataTables_paginate.paging_ellipses').hide();
		}
	})

	$('.user_actions .nav_box.add_group').click(function() {
		$('#wrapper_form_groups').openDialog($('div.user_edit_wrapper .textarea_overlay'));
		$('#user_form .input_group').removeClass('error');
		$('.user_actions .nav_box').removeClass('active');
		$(this).addClass('active');
		$('#user_form input:text').val('');
		$('#user_form p.si_data').html('-');
		$('#user_form input[name="id"]').val('0');
		$('.user_edit_wrapper .user_fields').show();
		var height = $('.user_edit_wrapper').position().top + $('.user_edit_wrapper').height() + $('.user_fields').height() + 10;
		if($('.user_table').height() < height){
			$('.user_table').css('height', height + 'px');
			$('.dataTables_paginate.paging_ellipses').hide();
		}
	})

	$('.user_fields .nav_box.cancel').click(function() {
		$('#wrapper_form_groups').closeDialog();
		$('#user_form .input_group').removeClass('error');
		$('.user_actions .nav_box').removeClass('active');
		$('.user_edit_wrapper .user_fields').hide();
		$('.user_table').css('height', '');
		$('.dataTables_paginate.paging_ellipses').show();
	})

	$('.user_fields .nav_box.save').click(function() {		
		var saveButton = this;
		if($(saveButton).attr("data-is-saving") == "false"){
			$('body').css('cursor','progress');
			$(saveButton).attr("data-is-saving", true);			
			var anSelected = fnGetSelected(oTable);
			if ($('#user_form input[name="id"]').val() != "" && $('#user_form input[name="id"]').val() > 0) {
				jQuery.ajax({
					type: "PUT",
					url: relPath+"api/v1/group/update",
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					data: parseForm(),
					success: function(data) {
						$('#wrapper_form_groups').closeDialog();
						oTable.fnUpdate(data.response, anSelected[0]);
						jAlert(data.message,js_localize['groups.edit.title'],"success");
						closeProfile();
						$('.user_table').css('height', '');
						$('.dataTables_paginate.paging_ellipses').show();
						$(saveButton).attr("data-is-saving", false);
					},
					// script call was *not* successful
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
						var message = JSON_ERROR.composeMessageFromJSON(data,true);
						if (data.response !== undefined) {
							$.each(data.response, function(key, value) {
								$('input[name="' + key + '"]').parent().addClass('error');
							});
						}
						$('body').css('cursor','default');
						jAlert(message, js_localize['groups.edit.title'], "error");
						$(saveButton).attr("data-is-saving", false);
					},
				});
			} else {
				jQuery.ajax({
					type: "POST",
					url: relPath+"api/v1/group/create",
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					data: parseForm(),
					success: function(data) {
						$('#wrapper_form_groups').closeDialog();
						oTable.fnAddData(data.response);
						jAlert(data.message,js_localize['groups.add.title'],"success");
						closeProfile();
						$('.user_table').css('height', '');
						$('.dataTables_paginate.paging_ellipses').show();
						$(saveButton).attr("data-is-saving", false);
					},
					// script call was *not* successful
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
						var message = JSON_ERROR.composeMessageFromJSON(data,true);
						if (data.response !== undefined) {
							$.each(data.response, function(key, value) {
								$('input[name="' + key + '"]').parent().addClass('error');
							});
						}
						$('body').css('cursor','default');
						jAlert(message,js_localize['groups.add.title'],"error");
						$(saveButton).attr("data-is-saving", false);
					},
				});
			}
		}
	});
});

function closeProfile() {
	$('#user_form .input_group').removeClass('error');
	$('.user_actions .nav_box').removeClass('active');
	$('.user_edit_wrapper .user_fields').hide();
	$('#user_form input:text').val('');
	$('#user_form input[name="id"]').val('0');
	$(oTable.fnSettings().aoData).each(function() {
		$(this.nTr).removeClass('row_selected');
	});
	$('.user_edit_wrapper').hide();
	$('body').css('cursor','default');
}

/* Get the rows which are currently selected */
function fnGetSelected(oTableLocal) {
	return oTableLocal.$('tr.row_selected');
}
function refreshTable() {
	var search_input = $('.user_search').val() !== js_localize['pages.label.search_group'] ? $('.user_search').val() : "";
	var sort_input = $('.user_sort').val() == -1 ? 1 : $('.user_sort').val();
	oTable.fnSort([[sort_input, 'asc']]);
	oTable.fnFilter(search_input);
	return false;
}

function populateForm(groupId) {
	jQuery.ajax({
		type: "GET",
		url: relPath+"api/v1/group/read/" + groupId,
				success: function(data) {
			$.each(data.response, function(key, value) {
				if ($('input[name="' + key + '"]').attr('type') == 'checkbox' || $('input[name="' + key + '"]').attr('type') == 'radio') {
					$('input[name="' + key + '"][value="' + value + '"]').prop('checked', true)
				} else if($('p#'+key).length > 0){
					$('p#'+key).html(value);
				} else {
					$('input[name="' + key + '"]').val(value);
				}
			});
			$('body').css('cursor','default');
		}, // success
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
			var message = JSON_ERROR.composeMessageFromJSON(data);
			$('body').css('cursor','default');
			jAlert(message, js_localize['groups.edit.title'], "error");
		},
	});
	return false;
}

function parseForm() {
	var serialized = $('#user_form').serializeArray();
	var s = '';
	var data = {};
	for (s in serialized) {
		data[serialized[s]['name']] = serialized[s]['value'];
	}
	return JSON.stringify(data);
}

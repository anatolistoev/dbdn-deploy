//var relPath = getRelativePath();
$(document).ready(function() {
	$.fn.dataTableExt.oStdClasses.sPaging = 'dataTables_paginate paging_ellipses related_';
	$('#page_downloads.genericRS.viewable').hover(function() {
		if (!$(this).hasClass('unviewable')) {
			$(this).find('.modify_overlay').show();
			$(this).find('.modify_wrapper').show();
			$(this).removeClass('viewable');
		}
	}, function() {
		if (!$(this).hasClass('unviewable')) {
			$(this).find('.modify_overlay').hide();
			$(this).find('.modify_wrapper').hide();
			$(this).addClass('viewable');
		}
	});

	$('#page_downloads.genericRS .modify_wrapper a.nav_box.edit').click(function() {
		$(this).parent().parent().find('div.modify_buttons').show();
		$(this).parent().parent().find('.modify_overlay').hide();
		$(this).parent().parent().find('.modify_wrapper').hide();
		$(this).parent().parent().addClass('unviewable');
	});

	$('#page_downloads.genericRS a.modify_links.back_to_view').click(function() {
		$(this).parent().parent().find('div.related_buttons').hide();
		$(this).parent().hide();
		$(this).parent().parent().removeClass('unviewable');
		$(this).parent().parent().find('div.active').removeClass("active");
		return false;
	});	

	$('#page_downloads.genericRS a.modify_links.add').click(function() {
		var old_selection = '';
		$('#page_downloads .download_records .download_record').each(function() {
			old_selection += $(this).attr('download') + ',';
		});
		old_selection = old_selection.slice(0, -1);

		openFileManagerForDownloads(tinyMCE.activeEditor, old_selection,
				function(selection, selection_src) {
					// TODO: Create 2 lists for Delete and for Add
					var data = {page_u_id: $('div#content_edit_page_u_id').html(), page_id: $('div#content_edit_page_id').html(),downloads:selection};
					$('div#page_downloads div.download_record').remove();
					
						jQuery.ajax({
							type: "POST",
							url: relPath + "api/v1/download/recreate",
							contentType: "application/json; charset=utf-8",
							dataType: "json",
							data: JSON.stringify(data),
							success: function(data) {
								for(var i = 0;i < data.response.length;i++){
									var download = data.response[i];
									var title = download.file.filename;
									$("#page_downloads .download_records .download_record_template").first().clone().attr('class', 'download_record cf').attr('pos', download.position).attr('download', download.file_id).appendTo('#page_downloads .download_records').show();
									$("#page_downloads .download_records .download_record[download='" + download.file_id + "'] span").attr('id', 'download_' + download.file_id).html('> ' + title);
								}
								unbindDownloadFilesButtons();
								bindDownloadFilesButtons();
							},
							error: function(XMLHttpRequest, textStatus, errorThrown) {
								var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
								var message = JSON_ERROR.composeMessageFromJSON(data, true);
								jAlert(message, js_localize['content.edit_download.insert_title'], "error");
							}
						});
					
				}
		);

		return false;
	});
	
	$('#page_downloads div.download_records').click(function(event) {
		if(event.target.tagName == "SPAN"){
			var related_btn = $(event.target).parent().find('.related_buttons');
			if (related_btn.is(":visible")) {
				related_btn.hide();
				$(event.target).parent().removeClass("active");
			}
			else {
				related_btn.show();
				$(event.target).parent().addClass("active");
			}
		}
	});
	
	bindDownloadFilesButtons();
});

function bindDownloadFilesButtons(){
	$('#page_downloads.genericRS a.modify_links.move_up').click(function() {
		if ($(this).parent().parent().attr('pos') == 1) {
			jAlert(js_localize['content.edit_download.move_first_error'], js_localize['content.edit_download.title'], "error");
		} else {
			var data = {page_u_id: $('div#content_edit_page_u_id').html(), page_id: $('div#content_edit_page_id').html(), file_id: $(this).parent().parent().attr('download'), position: $(this).parent().parent().prev('div.download_record').attr('pos')};
			var el = this;
			jQuery.ajax({
				url: relPath + "api/v1/download/modifydownload",
				type: "PUT",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: JSON.stringify(data),
				success: function(data) {
					var tmp = $(el).parent().parent().attr('pos');
					$(el).parent().parent().attr('pos', $(el).parent().parent().prev().attr('pos'));
					$(el).parent().parent().prev().attr('pos', tmp);
					$(el).parent().parent().prev().insertAfter($(el).parent().parent());
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
					var message = JSON_ERROR.composeMessageFromJSON(data, true);
					jAlert(message, js_localize['content.edit_download.title'], "error");
				},
			})
		}
		return false;
	});

	$('#page_downloads.genericRS a.modify_links.move_down').click(function() {
		if ($(this).parent().parent().is(':last-child')) {
			jAlert(js_localize['content.edit_download.move_last_error'], js_localize['content.edit_download.title'], "error");
		} else {
			var data = {page_u_id: $('div#content_edit_page_u_id').html(), page_id: $('div#content_edit_page_id').html(), file_id: $(this).parent().parent().attr('download'), position: $(this).parent().parent().next('div.download_record').attr('pos')};
			var el = this;
			jQuery.ajax({
				url: relPath + "api/v1/download/modifydownload",
				type: "PUT",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: JSON.stringify(data),
				success: function(data) {
					var tmp = $(el).parent().parent().attr('pos');
					$(el).parent().parent().attr('pos', $(el).parent().parent().next().attr('pos'));
					$(el).parent().parent().next().attr('pos', tmp);
					$(el).parent().parent().insertAfter($(el).parent().parent().next());
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
					var message = JSON_ERROR.composeMessageFromJSON(data, true);
					jAlert(message, js_localize['content.edit_download.title'], "error");
				},
			})
		}
		return false;
	});

	$('#page_downloads.genericRS a.modify_links.delete').click(function() {
		var el = this;
		jConfirm(js_localize['content.edit_download.remove_confirm'], js_localize['content.edit_download.remove_title'], "success", function(r) {
			if (r) {
				var data = {page_u_id: $('div#content_edit_page_u_id').html(), page_id: $('div#content_edit_page_id').html(), file_id: $(el).parent().parent().attr('download')};
				jQuery.ajax({
					type: "DELETE",
					url: relPath + "api/v1/download/removedownload",
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					data: JSON.stringify(data),
					success: function(data) {
						$('div#page_downloads div.download_record').each(function() {
							if ($(this).attr('pos') > $(el).parent().parent().attr('pos')) {
								$(this).attr('pos', parseInt($(this).attr('pos')) - 1);
							}
						});
						$(el).parent().parent().remove();
						jAlert(js_localize['content.edit_download.remove_success'], js_localize['content.edit_download.remove_title'], "success");
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
						var message = JSON_ERROR.composeMessageFromJSON(data, true);
						jAlert(message, js_localize['content.edit_download.remove_title'], "error");
					},
				});

			}
		});
		return false;
	});
}

function unbindDownloadFilesButtons(){
	$('#page_downloads.genericRS a.modify_links.move_up').unbind( "click" );
	$('#page_downloads.genericRS a.modify_links.move_down').unbind( "click" );
	$('#page_downloads.genericRS a.modify_links.delete').unbind( "click" );
}
var oTable;
//var relPath = getRelativePath();

$(document).ready(function() {

	$("#user_form :input").change(function() {
		$("#user_form").data('changed', true);
	});

	oTable = $('#users').dataTable({
		"bProcessing": false,
		"sAjaxSource": relPath+"api/v1/user/all",
		"sAjaxDataProp": 'response',
		"fnServerData": fnServerData,
        "fnInitComplete": function (oSettings, json) {
            $('#user_count').html(js_localize['user.count'] +  oSettings.aoData.length);
        },
		"iDisplayLength": 45,
		"bFilter": true,
		"sPaginationType": 'ellipses',
		"bLengthChange": false,
		"iShowPages": 10,
		"bSort": false,
		"bDeferRender": true,
		"oLanguage": {
			"sEmptyTable": " "},
		"aoColumns": [
			{"sType": "numeric", "mData": "id", "sWidth": "70px", "bSearchable": false, "sClass": "user_id"},
			{"sType": "string", "mData": "username", "sWidth": "130px", "sClass": "strong", "bSearchable": true},
			{"sType": "string", "mData": function(source) {
					return source.first_name + ' ' + source.last_name;
				}, "sWidth": "130px", "bSearchable": true},
			{"sType": "string", "mData": function(source) {
					return getRoleTextByID(source.role);
				}, "sWidth": "215px", "bSearchable": false},
			{"sType": "string", "mData": function(source) {
					if (source.newsletter_include === 1)
						return js_localize['users.yes'];
					return js_localize['users.no'];
				}, "sWidth": "105px", "bSearchable": false},
			{"sType": "string", "mData": function(source) {
					if (source.active === 1)
						return js_localize['users.yes'];
					return js_localize['users.no'];
				}, "sWidth": "65px", "bSearchable": false},
			{"sType": "date-eu", "mData": "last_login", "sWidth": "110px", "bSearchable": false},
			{"sType": "numeric", "mData": "logins", "sWidth": "135px", "bSearchable": false}
		]
	});

	var oSettings = oTable.fnSettings();
	oSettings.sAlertTitle = js_localize['users.list.title'];

	$("#users tbody").click(function(event) {
		if (!$(event.target).hasClass('dataTables_empty')){
			closeProfile();
			$(oTable.fnSettings().aoData).each(function() {
				$(this.nTr).removeClass('row_selected');
			});
			$(event.target.parentNode).addClass('row_selected');

			if(!$('a.edit').is(":visible") ){
				$('a.edit').show();
				$('a.remove').show();
			}
		}else{
			$('a.edit').hide();
			$('a.remove').hide();
		}
		$('.user_edit_wrapper').css('top', $('#users_wrapper').position().top + $(event.target.parentNode).position().top + $(event.target.parentNode).height() + 1);
		$('.user_edit_wrapper').show();
		//alert($('#users_wrapper').position().top)
	});

	$('body').click(function(event) {
		if($(event.target).parents('tbody').length == 0 && $(event.target).parents('.user_edit_wrapper').length == 0 && $('#popup_overlay').length == 0){
			closeProfile();
		}
		if (event.target.parentNode) {
			if (!(event.target.parentNode.className == "admin_data" || event.target.parentNode.id == "admin_info" || event.target.parentNode.id == "admin_action")) {
				if ($('#admin_action').is(":visible"))
				{
					$("#admin_info").removeClass('active');
					$('#admin_action').hide();
				}
			}
		}
	});

//	$('.user_edit_wrapper').click(function(event) {
//		event.stopPropagation();
//	})

	/* Add a click handler for the delete row */
	$('.user_actions .nav_box.remove').click(function() {
		jConfirm($(this).attr('confirm'), js_localize['users.remove.title'], "success", function(r) {
			if (r) {
				var anSelected = fnGetSelected(oTable);
				closeProfile();
				if (anSelected.length !== 0) {
					jQuery.ajax({
						type: "DELETE",
						url: relPath+"api/v1/user/delete/" + $(anSelected[0]).find('.user_id').html(),
						success: function(data) {
							oTable.fnDeleteRow(anSelected[0]);
							jAlert(js_localize['users.remove.text'].replace('{0}', data.response.first_name).replace('{1}',data.response.last_name), js_localize['users.remove.title'],"success");
						}, // success
						// script call was *not* successful
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
							var message = JSON_ERROR.composeMessageFromJSON(data, true);
							jAlert(message, js_localize['users.remove.title'], "error");
						}
					});
				}
			}
		});
	});

	$('.user_actions .nav_box.edit').click(function() {
		// For Edit User password is not required
		$('span.fieldset_requared.field_password').hide();
		$('#user_content').openDialog($('div.user_edit_wrapper .textarea_overlay'));
		$('#user_form .input_group').removeClass('error');
		$('body').css('cursor', 'progress');
		$('.user_actions .nav_box').removeClass('active');
		$(this).addClass('active');
		var anSelected = fnGetSelected(oTable);
		$('.user_edit_wrapper .user_fields').show();
		$('#user_form input:checkbox:checked').removeAttr('checked');
		populateForm($(anSelected[0]).find('.user_id').html());
		var height = $('.user_edit_wrapper').position().top + $('.user_edit_wrapper').height() + $('.user_fields').height() + 10;
		if($('.user_table').height() < height){
			$('.user_table').css('height', height + 'px');
			$('.dataTables_paginate.paging_ellipses').hide();
		}
	});

	$('.user_actions .nav_box.user').click(function() {
		// For New User password is required
		$('span.fieldset_requared.field_password').show();

		$('#user_content').openDialog($('div.user_edit_wrapper .textarea_overlay'));
		$('#user_form .input_group').removeClass('error');
		$('.user_actions .nav_box').removeClass('active');
		$(this).addClass('active');
		$('#profile_note_comment p').html('');
		$('#user_form input:text').val('');
		$('#user_form input:password').val('');
		$('#user_form p.si_data').html('-');
		$('#user_form input[name="id"]').val('0');
		$('#user_form input:checkbox:checked').removeAttr('checked');
		$('#user_form input[type="checkbox"][name="role"][value="2"]').prop('checked', true);
		$('#user_form input:radio:checked').removeAttr('checked');
		$('#user_form input[type="radio"][value="0"]').prop('checked', true);
		$('.user_edit_wrapper .user_fields').show();
		var height = $('.user_edit_wrapper').position().top + $('.user_edit_wrapper').height() + $('.user_fields').height() + 10;
		if($('.user_table').height() < height){
			$('.user_table').css('height', height + 'px');
			$('.dataTables_paginate.paging_ellipses').hide();
		}
	});

	$('.user_fields .nav_box.cancel').click(function() {
		$('#user_content').closeDialog();
		$('#user_form .input_group').removeClass('error');
		$('.user_actions .nav_box').removeClass('active');
		$('.user_edit_wrapper .user_fields').hide();
		$('.user_table').css('height', '');
		$('.dataTables_paginate.paging_ellipses').show();
	});

	$('.user_fields .nav_box.save').click(function(event) {
		$('body').css('cursor', 'progress');
		var anSelected = fnGetSelected(oTable);

		if ($('#user_form input[name="id"]').val() != "" && $('#user_form input[name="id"]').val() > 0) {
			jQuery.ajax({
				type: "PUT",
				url: relPath+"api/v1/user/update",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: parseForm(),
				success: function(data) {
					$('#user_content').closeDialog();
					$('.user_table').css('height', '');
					$('.dataTables_paginate.paging_ellipses').show();
					$('body').css('cursor', 'default');
					closeProfile();
					oTable.fnUpdate(data.response, anSelected[0]);
					jAlert(data.message, js_localize['users.edit.title'], "success");
				},
				// script call was *not* successful
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
					var message = JSON_ERROR.composeMessageFromJSON(data,true);
					if (data.response !== undefined) {
						$.each(data.response, function(key, value) {
							$('input[name="' + key + '"]').parent().addClass('error');
						});
					}
					$('body').css('cursor', 'default');
					jAlert(message, js_localize['users.edit.title'], "error");
				}
			});
		} else {
			jQuery.ajax({
				type: "POST",
				url: relPath+"api/v1/user/create",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: parseForm(),
				success: function(data) {
					$('#user_content').closeDialog();
					$('.user_table').css('height', '');
					$('.dataTables_paginate.paging_ellipses').show();
					$('body').css('cursor', 'default');
					closeProfile();
					data.response.last_login = '00.00.0000';
					data.response.logins = 0;
					oTable.fnAddData(data.response);
					jAlert(data.message, js_localize['users.edit.title'], "success");
				},
				// script call was *not* successful
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
					var message = JSON_ERROR.composeMessageFromJSON(data, true);
					if (data.response !== undefined) {
						$.each(data.response, function(key, value) {
							$('input[name="' + key + '"]').parent().addClass('error');
						});
					}
					$('body').css('cursor', 'default');
					jAlert(message, js_localize['users.add.title'], "error");
				}
			});
		}
	});

	$('input[name="email"]').change(function() {
		if ($(this).val().indexOf('@daimler.com') != -1) {
			$('#newsletter_group').hide();
			$('#newsletter_group input').attr('disabled', 'disabled');
			$('#newsletter_hidden').removeAttr('disabled');
		} else {
			$('#newsletter_group').show();
			$('#newsletter_group input').removeAttr('disabled');
			$('#newsletter_hidden').attr('disabled', 'disabled');
		}
	});

});

function updateMembership(userId, message) {
	var lastIndex = $('#user_form input[name="group"]').length - 1;

	$('#user_form input[name="group"]').each(function(index) {
		var member = {user_id: userId, group_id: $(this).val()};
		if ($(this).prop('checked') == true) {
			jQuery.ajax({
				type: "POST",
				url: relPath+"api/v1/group/addmember",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: JSON.stringify(member),
				success: function(data) {
					if (index == lastIndex) {
						$('#user_content').closeDialog();
						$('.user_table').css('height', '');
						$('.dataTables_paginate.paging_ellipses').show();
						$('body').css('cursor', 'default');
						closeProfile();
						jAlert(message, js_localize['users.edit.title'], "success");
					}
				},
				// script call was *not* successful
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					if (XMLHttpRequest.status != 404) {
						var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
						var errMessage = JSON_ERROR.composeMessageFromJSON(data, true);
						jAlert(errMessage, js_localize['users.edit.title'], "error");
					} else {
						if (index == lastIndex) {
							$('#user_content').closeDialog();

							$('.user_table').css('height', '');
							$('.dataTables_paginate.paging_ellipses').show();
							$('body').css('cursor', 'default');
							//closeProfile();
							jAlert(message, js_localize['users.edit.title'], "success");
						}
					}
				},
			});
		} else {
			jQuery.ajax({
				type: "DELETE",
				url: relPath+"api/v1/group/removemember",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: JSON.stringify(member),
				success: function(data) {
					if (index == lastIndex) {
						$('#user_content').closeDialog();
						$('.user_table').css('height', '');
						$('.dataTables_paginate.paging_ellipses').show();
						$('body').css('cursor', 'default');
						closeProfile();
						jAlert(message, js_localize['users.edit.title'], "success");
					}
				},
				// script call was *not* successful
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					if (XMLHttpRequest.status != 404) {
						var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
						var errMessage = JSON_ERROR.composeMessageFromJSON(data, true);
						jAlert(errMessage, js_localize['users.edit.title'], "error");
					} else {
						if (index == lastIndex) {
							$('#user_content').closeDialog();
							$('.user_table').css('height', '');
							$('.dataTables_paginate.paging_ellipses').show();
							$('body').css('cursor', 'default');
							closeProfile();
							jAlert(message, js_localize['users.edit.title'], "success");
						}
					}
				},
			});
		}
	});
}

function closeProfile() {
	$('#user_form .input_group').removeClass('error');
	$('.user_actions .nav_box').removeClass('active');
	$('.user_edit_wrapper .user_fields').hide();
	$('#user_form input:text').val('');
	$('#user_form input:password').val('');
	$('#user_form input[name="id"]').val('0');
	$('#user_form input:checkbox:checked').removeAttr('checked');
	$('#user_form input:radio:checked').removeAttr('checked');
	$(oTable.fnSettings().aoData).each(function() {
		$(this.nTr).removeClass('row_selected');
	});
	$('.user_edit_wrapper').hide();
}

/* Get the rows which are currently selected */
function fnGetSelected(oTableLocal) {
	return oTableLocal.$('tr.row_selected');
}
function refreshTable() {
	var search_input = $('.user_search').val() !== "Search User" ? $('.user_search').val() : "";
	var sort_input = $('.user_sort').val() == -1 ? 1 : $('.user_sort').val();
	if(sort_input == 6 || sort_input == 7){
		oTable.fnSort([[sort_input, 'desc']]);
	}else{
		oTable.fnSort([[sort_input, 'asc']]);
	}
	oTable.fnFilter(search_input);
	return false;
}

function populateForm(userId) {
	jQuery.ajax({
		type: "GET",
		url: relPath+"api/v1/user/read/" + userId,
		success: function(data) {
			$.each(data.response, function(key, value) {
				if ($('input[name="' + key + '"]').attr('type') == 'radio') {
					$('input[name="' + key + '"][value="' + value + '"][type="radio"]').prop('checked', true);
				} else if ($('p#' + key).length > 0) {
					$('p#' + key).html(value);
				} else {
					$('input[name="' + key + '"]').val(value);
				}
			});
			if (data.response.email.indexOf('@daimler.com') != -1) {
				$('#newsletter_group').hide();
				$('#newsletter_group input').attr('disabled', 'disabled');
				$('#newsletter_hidden').removeAttr('disabled');
			}
		}, // success
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
			var message = JSON_ERROR.composeMessageFromJSON(data, true);
			jAlert(message, js_localize['users.edit.title'], "error");
		}
	});
	jQuery.ajax({
		type: "GET",
		url: relPath+"api/v1/comments/readbyuser?user_id=" + userId + "&lang=" + lang,
		success: function(data) {
			$('#profile_note_comment p').html(data.response.length);
		}, // success
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
			var message = JSON_ERROR.composeMessageFromJSON(data, true);
			jAlert(message, js_localize['users.edit.title'], "error");
		}
	});
	jQuery.ajax({
		type: "GET",
		url: relPath+"api/v1/group/readbyuserid/" + userId,
		success: function(data) {
			$.each(data.response, function(key, value) {
				$('input[name="group"][value="' + value.id + '"]').prop('checked', true)
			});
			$('body').css('cursor', 'default');
		}, // success
		// script call was *not* successful
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
			var message = JSON_ERROR.composeMessageFromJSON(data, true);
			jAlert(message, js_localize['users.edit.title'], "error");
		}
	});
	return false;
}

function parseForm() {
	var serialized = $('#user_form').serializeArray();
	var s = '';
	var data = {};
	data['group'] = new Array();
	for (s in serialized) {
		if (serialized[s]['name'] != "group") {
			if (serialized[s]['name'] == "role" || serialized[s]['name'] == "active" || serialized[s]['name'] == "newsletter_include") {
				data[serialized[s]['name']] = parseInt(serialized[s]['value']);
			} else {
				data[serialized[s]['name']] = serialized[s]['value'];
			}
		}else{
			data[serialized[s]['name']].push(serialized[s]['value']);;
		}
	}
	return JSON.stringify(data);
}

function redirectUserComments(url) {
	if ($("#user_form").data('changed')) {
		jConfirm(js_localize['users.redirect.comments_text'], js_localize['users.redirect.comments_title'], "success", function(r) {
			if(r){
				var anSelected = fnGetSelected(oTable);
				window.location.href = url + "?username=" + $(anSelected[0]).find('.strong').html();
				return false;
			}else{
				return false;
			}
		})
	} else {
		var anSelected = fnGetSelected(oTable);
		window.location.href = url + "?username=" + $(anSelected[0]).find('.strong').html();
		return false;
	}

}

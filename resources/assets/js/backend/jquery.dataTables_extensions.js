jQuery.fn.dataTableExt.oApi.fnSortNeutral = function (oSettings)
{
    /* Remove any current sorting */
    oSettings.aaSorting = [];

    /* Sort display arrays so we get them in numerical order */
    oSettings.aiDisplay.sort(function (x, y) {
        return x - y;
    });
    oSettings.aiDisplayMaster.sort(function (x, y) {
        return x - y;
    });

    /* Redraw */
    oSettings.oApi._fnReDraw(oSettings);
};
jQuery.fn.dataTableExt.oSort['checkbox-asc'] = function (a, b) {

    ch_a = $(a).prop('checked') ? 1 : 0;
    ch_b = $(b).prop('checked') ? 1 : 0;
    p_a = $(a).attr('check_pos');
    p_b = $(b).attr('check_pos');

    return ((ch_a < ch_b) ? -1 : ((ch_a > ch_b) ? 1 : ((p_a < p_b) ? -1 : 0)));
}
jQuery.fn.dataTableExt.oSort['checkbox-desc'] = function (a, b) {
    ch_a = $(a).prop('checked') ? 1 : 0;
    ch_b = $(b).prop('checked') ? 1 : 0;
    p_a = $(a).attr('check_pos');
    p_b = $(b).attr('check_pos');

    return ((ch_a > ch_b) ? -1 : ((ch_a < ch_b) ? 1 : ((p_a < p_b) ? -1 : 0)));
}

jQuery.fn.dataTableExt.oSort['page-title-asc'] = function (a, b) {

    a = $(a).text().substring($(a).text().indexOf('+')).toLowerCase();
    b = $(b).text().substring($(b).text().indexOf('+')).toLowerCase();

    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
}
jQuery.fn.dataTableExt.oSort['page-title-desc'] = function (a, b) {
    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
}

jQuery.fn.dataTableExt.oSort['date-eu-pre'] = function (date) {
    var time = "";
    if (date.indexOf(' ') != -1) {
        time = date.substr(date.indexOf(' ') + 1);
        date = date.substr(0, date.indexOf(' '));//date.replace(" ", "");
    }
    if (date.length > 0 && date != "-") {
        if (date.indexOf('.') > 0) {
            /*date a, format dd.mn.(yyyy) ; (year is optional)*/
            var eu_date = date.split('.');
        } else {
            /*date a, format dd/mn/(yyyy) ; (year is optional)*/
            var eu_date = date.split('/');
        }
        /*year (optional)*/
        if (eu_date[2]) {
            var year = eu_date[2];
        } else {
            var year = 0;
        }
        /*month*/
        var month = eu_date[1];
        if (month.length == 1) {
            month = 0 + month;
        }
        /*day*/
        var day = eu_date[0];
        if (day.length == 1) {
            day = 0 + day;
        }
        if (time.length > 0) {
            var time_arr = time.split(':');
            var hour = time_arr[0];
            if (hour.length == 1) {
                hour = 0 + hour;
            }
            var min = time_arr[0];
            if (min.length == 1) {
                min = 0 + min;
            }
            return (year + month + day + hour + min) * 1;
        }
        return (year + month + day) * 1;
    } else {
        return 0;
    }
}
jQuery.fn.dataTableExt.oSort['date-eu-asc'] = function (a, b) {
    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
}
jQuery.fn.dataTableExt.oSort['date-eu-desc'] = function (a, b) {
    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
}

$.extend($.fn.dataTableExt.oStdClasses, {
    'sPageEllipsis': 'paginate_ellipsis',
    'sPageNumber': 'paginate_number',
    'sPageGroup': 'paginate_group',
    'sPageNumbers': 'paginate_numbers'
});

$.fn.dataTableExt.oPagination.ellipses = {
    'oDefaults': {
        'iShowPages': 10,
        'iPageInterval': 10
    },
    'fnClickHandler': function (e) {
        var fnCallbackDraw = e.data.fnCallbackDraw,
                oSettings = e.data.oSettings,
                sPage = e.data.sPage;

        if ($(this).is('[disabled]')) {
            return false;
        }

        oSettings.oApi._fnPageChange(oSettings, sPage);
        fnCallbackDraw(oSettings);

        return true;
    },
    // fnInit is called once for each instance of pager
    'fnInit': function (oSettings, nPager, fnCallbackDraw) {
        var oClasses = oSettings.oClasses,
                oLang = oSettings.oLanguage.oPaginate,
                that = this;

        var iShowPages = oSettings.oInit.iShowPages || this.oDefaults.iShowPages;
        var iPageInterval = oSettings.oInit.iPageInterval || this.oDefaults.iPageInterval;


        $.extend(oSettings, {_iShowPages: iShowPages, _iPageInterval: iPageInterval});

        var oPrevious = $('<a class="' + oClasses.sPageButton + ' ' + oClasses.sPagePrevious + '"></a>'),
                oNumbers = $('<div class="' + oClasses.sPageNumbers + '"></div>'),
                oNext = $('<a class="' + oClasses.sPageButton + ' ' + oClasses.sPageNext + '"></a>');

        oPrevious.click({'fnCallbackDraw': fnCallbackDraw, 'oSettings': oSettings, 'sPage': 'previous'}, that.fnClickHandler);
        oNext.click({'fnCallbackDraw': fnCallbackDraw, 'oSettings': oSettings, 'sPage': 'next'}, that.fnClickHandler);

        // Draw
        $(nPager).append(oPrevious, oNumbers, oNext);
    },
    // fnUpdate is only called once while table is rendered
    'fnUpdate': function (oSettings, fnCallbackDraw) {
        var oClasses = oSettings.oClasses,
                that = this;

        var tableWrapper = oSettings.nTableWrapper;

        // Update stateful properties
        this.fnUpdateState(oSettings);

        if (oSettings._iCurrentPage === 1) {
            $('.' + oClasses.sPagePrevious, tableWrapper).attr('disabled', true);
        } else {
            $('.' + oClasses.sPagePrevious, tableWrapper).removeAttr('disabled');
        }

        if (oSettings._iTotalPages === 0 || oSettings._iCurrentPage === oSettings._iTotalPages) {
            $('.' + oClasses.sPageNext, tableWrapper).attr('disabled', true);
        } else {
            $('.' + oClasses.sPageNext, tableWrapper).removeAttr('disabled');
        }

        var i, oNumber, oNumbers = $('.' + oClasses.sPageNumbers, tableWrapper);

        // Erase
        oNumbers.html('');

        for (i = oSettings._iFirstPage; i <= oSettings._iLastPage; i++) {

            var number = i < 10 ? "0" + i : i;

            if (oSettings._iCurrentPage === i) {
                if (i % oSettings._iShowPages == 1 && i != 1) {
                    oNumbers.append('<span class="paginate_dots">...</span>');
                }
                oNumber = $('<a class="' + oClasses.sPageButton + ' ' + oClasses.sPageNumber + '">' + number + '</a>');
                oNumber.attr('active', true).attr('disabled', true);
            } else {

                if (Math.ceil(oSettings._iCurrentPage / oSettings._iShowPages) === Math.ceil(i / oSettings._iShowPages)) {
                    if (i % oSettings._iShowPages == 1 && i != 1) {
                        oNumbers.append('<span class="paginate_dots">...</span>');
                    }
                    oNumber = $('<a class="' + oClasses.sPageButton + ' ' + oClasses.sPageNumber + '">' + number + '</a>');
                    oNumber.click({'fnCallbackDraw': fnCallbackDraw, 'oSettings': oSettings, 'sPage': i - 1}, that.fnClickHandler);
                } else {
                    if (i != 1) {
                        oNumbers.append('<span class="paginate_dots">...</span>');
                    }
                    var end_page = oSettings.fnFormatNumber(i + oSettings._iPageInterval - 1) > oSettings._iLastPage ? oSettings._iLastPage : (i + oSettings._iPageInterval - 1);
                    if (i <= oSettings._iCurrentPage && end_page >= oSettings._iCurrentPage) {
                        end_page = Math.floor((oSettings._iCurrentPage - 1) / oSettings._iShowPages) * oSettings._iShowPages;
                    }
                    end_page = end_page < 10 ? "0" + end_page : end_page;
                    oNumber = $('<a class="' + oClasses.sPageButton + ' ' + oClasses.sPageGroup + '">' + number + ' - ' + end_page + '</a>');
                    oNumber.click({'fnCallbackDraw': fnCallbackDraw, 'oSettings': oSettings, 'sPage': i - 1}, that.fnClickHandler);
                    i = end_page;
                }
            }

            // Draw
            oNumbers.append(oNumber);
        }

    },
    // fnUpdateState used to be part of fnUpdate
    // The reason for moving is so we can access current state info before fnUpdate is called
    'fnUpdateState': function (oSettings) {
        var iCurrentPage = Math.ceil((oSettings._iDisplayStart + 1) / oSettings._iDisplayLength),
                iTotalPages = Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength),
                iFirstPage = 1,
                iLastPage = iTotalPages;

        if (iTotalPages < oSettings._iShowPages) {
            iFirstPage = 1;
            iLastPage = iTotalPages;
        } else if (iFirstPage < 1) {
            iFirstPage = 1;
            iLastPage = oSettings._iShowPages;
        } else if (iLastPage > iTotalPages) {
            iFirstPage = (iTotalPages - oSettings._iShowPages) + 1;
            iLastPage = iTotalPages;
        }

        $.extend(oSettings, {
            _iCurrentPage: iCurrentPage,
            _iTotalPages: iTotalPages,
            _iFirstPage: iFirstPage,
            _iLastPage: iLastPage
        });
    }
};

jQuery.fn.dataTableExt.oApi.fnAddDataAndDisplay = function (oSettings, aData, isRowExpand) {
    var parent_id = parseInt(aData.parent_id);
    var parent = oSettings.aoData.filter(function (el) {
        return el['_aData']['id'] == parent_id;
    });

    var parentIndex = parseInt(parent[0].nTr._DT_RowIndex);
    var displayParentIndex = oSettings.aiDisplayMaster.indexOf(parentIndex);
    var index = displayParentIndex + 1;
    var position = parseInt(aData.position);
    var level = parseInt(aData.level);
    var foundedChilds = 0;
    if (!oSettings.aoData[parentIndex + 1]) {
        index = parseInt(oSettings.aoData[parentIndex]['nTr']['_DT_RowIndex']) + 1;
    } else if (oSettings.aoData[parentIndex]['_aData'].level == oSettings.aoData[parentIndex + 1]['_aData'].level) {
        index = parseInt(oSettings.aoData[parentIndex]['nTr']['_DT_RowIndex']) + 1;
    } else {
        for (var i = parentIndex + 1; i < oSettings.aoData.length; i++) {
            if ((oSettings.aoData[i]['_aData']['parent_id'] == parent_id && oSettings.aoData[i]['_aData']['position'] < position)) {
                index = parseInt(oSettings.aoData[i]['nTr']['_DT_RowIndex']) + 1;
            } else if (oSettings.aoData[i]['_aData'].level > level) {
                index = parseInt(oSettings.aoData[i]['nTr']['_DT_RowIndex']) + 1;
                foundedChilds++;
            } else if ((oSettings.aoData[i]['_aData']['parent_id'] == aData.parent_id && oSettings.aoData[i]['_aData']['position'] >= position)
                    || oSettings.aoData[i]['_aData'].level < level) {
                i = oSettings.aoData.length;
            }
        }
    }
    /* Add the data */
    var iAdded = this.oApi._fnAddData(oSettings, aData);
    var nAdded = oSettings.aoData[ iAdded ].nTr;
    this.oApi._fnReDraw(oSettings);

    for (var i = 0, iLen = oSettings.aoData.length; i < iLen - 1; i++)
    {
        if (oSettings.aiDisplay[i] >= index) {
            oSettings.aiDisplay[i] += 1;
            oSettings.aiDisplayMaster[i] += 1;
        }
        if (i >= index) {
            oSettings.aoData[i].nTr._DT_RowIndex = i + 1;
        }
        if (oSettings.aoData[i]._aData.level == aData.level && oSettings.aoData[i]._aData.parent_id == aData.parent_id && parseInt(oSettings.aoData[i]._aData.view_pos) >= aData.view_pos) {
            oSettings.aoData[i]._aData.position = parseInt(oSettings.aoData[i]._aData.position) + 1;
            oSettings.aoData[i]._aData.view_pos = parseInt(oSettings.aoData[i]._aData.view_pos) + 1;
        }
    }
    /* Put element to desired spot*/
    var insertedElement = oSettings.aoData.pop();
    insertedElement.nTr._DT_RowIndex = index;
    oSettings.aoData.splice(index, 0, insertedElement);
    oSettings.aiDisplay.splice(displayParentIndex + aData.view_pos + foundedChilds, 0, index);
    oSettings.aiDisplay.pop();
    oSettings.aiDisplayMaster.splice(displayParentIndex + aData.view_pos + foundedChilds, 0, index);
    oSettings.aiDisplayMaster.pop();

    /* Need to re-filter and re-sort the table to get positioning correct, not perfect
     * as this will actually redraw the table on screen, but the update should be so fast (and
     * possibly not alter what is already on display) that the user will not notice
     */
    //this.oApi._fnReDraw( oSettings );
    /*Draw table with same position as it was*/
    var iPos = -1;
    if (isRowExpand) {
        iPos = displayParentIndex;
    } else {
        /* Find it's position in the table */
        for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++)
        {
            if (i == index)
            {
                iPos = i;
                break;
            }
        }
    }
    /* Get starting point, taking account of paging */
    if (iPos >= 0)
    {
        oSettings._iDisplayStart = (Math.floor(iPos / oSettings._iDisplayLength)) * oSettings._iDisplayLength;
        this.oApi._fnCalculateEnd(oSettings);
    }

    this.oApi._fnDraw(oSettings);
    return {
        "nTr": nAdded,
        "iPos": iAdded
    };
};

jQuery.fn.dataTableExt.oApi.fnDeleteDataAndDisplay = function (oSettings, delId)
{
    var searchedRow = oSettings.aoData.filter(function (el) {
        return el['nTr']['id'] == delId;
    });
    var elementIndex = searchedRow[0]['nTr']['_DT_RowIndex'];
    var deleted_rows = 0;
    dataTable = this;
    for (var i = elementIndex + 1; i < oSettings.aoData.length; i++)
    {
        if (oSettings.aoData[i]['_aData']['parent_id'] == oSettings.aoData[elementIndex]['_aData']['parent_id'] ||
                oSettings.aoData[i]['_aData']['level'] < oSettings.aoData[elementIndex]['_aData']['level'])
        {
            i = oSettings.aoData.length;
        } else {
            dataTable.fnDeleteRow(oSettings.aoData[i].nTr._DT_RowIndex, null, true);
            i--;
            deleted_rows++;
        }
    }
    return deleted_rows;
};

jQuery.fn.dataTableExt.oApi.fnSwapData = function (oSettings, elementIndex, swapIndex, direction)
{
    var level = $(oSettings.aoData[elementIndex].nTr).attr('level');
    var position = $(oSettings.aoData[elementIndex].nTr).attr('pos');
    var lookup_pos = $(oSettings.aoData[swapIndex].nTr).attr('pos')
    var index = elementIndex;
    if (elementIndex > swapIndex) {
        index = swapIndex;
    }
    var tmp = new Array(), tmp_2 = new Array(), start_temp = false, start_temp_2 = false;
    for (var i = index, iLen = oSettings.aoData.length; i <= iLen - 1; i++)
    {

        if ($(oSettings.aoData[i].nTr).attr('level') == level && $(oSettings.aoData[i].nTr).attr('pos') == lookup_pos) {
            tmp.push(oSettings.aoData[i]);
            var tmp_i = i;
            start_temp = true;
        } else if ($(oSettings.aoData[i].nTr).attr('level') > level && start_temp == true) {
            tmp.push(oSettings.aoData[i]);
        } else if (start_temp == true) {
            start_temp = false;
        }

        if ($(oSettings.aoData[i].nTr).attr('level') == level && $(oSettings.aoData[i].nTr).attr('pos') == position) {
            tmp_2.push(oSettings.aoData[i]);
            var tmp_2_i = i;
            start_temp_2 = true;
        } else if ($(oSettings.aoData[i].nTr).attr('level') > level && start_temp_2 == true) {
            tmp_2.push(oSettings.aoData[i]);
        } else if (start_temp_2 == true) {
            start_temp_2 = false;
        }

        if (!start_temp_2 && !start_temp)
        {
            i = iLen;
        }
    }
    if (direction == 'up') {
        oSettings.aoData[tmp_i]._aData.position = (parseInt(oSettings.aoData[tmp_i]._aData.position) + 1);
        oSettings.aoData[tmp_2_i]._aData.position = lookup_pos;
        oSettings.aoData[tmp_i]._aData.view_pos = oSettings.aoData[tmp_2_i]._aData.view_pos;
        oSettings.aoData[tmp_2_i]._aData.view_pos = oSettings.aoData[tmp_2_i]._aData.view_pos - 1;
    } else {
        console.log(lookup_pos);
        oSettings.aoData[tmp_i]._aData.position = (parseInt(oSettings.aoData[tmp_i]._aData.position) - 1);
        oSettings.aoData[tmp_2_i]._aData.position = lookup_pos;
        oSettings.aoData[tmp_i]._aData.view_pos = oSettings.aoData[tmp_2_i]._aData.view_pos;
        oSettings.aoData[tmp_2_i]._aData.view_pos = parseInt(oSettings.aoData[tmp_2_i]._aData.view_pos + 1);
    }


    for (var i = index, iLen = oSettings.aoData.length; i <= iLen - 1; i++)
    {
        if (direction == 'up') {
            if (i >= tmp_i) {
                if (tmp_2.length > 0) {
                    oSettings.aoData[i] = tmp_2.shift();
                    oSettings.aoData[i]['nTr']['_DT_RowIndex'] = i;
                } else if (tmp.length > 0) {
                    oSettings.aoData[i] = tmp.shift();
                    oSettings.aoData[i]['nTr']['_DT_RowIndex'] = i;
                } else if (tmp_2.length == 0 && tmp.length == 0) {
                    i = iLen;
                }
            }
        } else {
            if (i >= tmp_2_i) {
                if (tmp.length > 0) {
                    oSettings.aoData[i] = tmp.shift();
                    oSettings.aoData[i]['nTr']['_DT_RowIndex'] = i;
                } else if (tmp_2.length > 0) {
                    oSettings.aoData[i] = tmp_2.shift();
                    oSettings.aoData[i]['nTr']['_DT_RowIndex'] = i;
                } else if (tmp_2.length == 0 && tmp.length == 0) {
                    i = iLen;
                }
            }
        }
    }


    //
    //oSettings.aoData[tmp_2_i] = tmp;


    /* Need to re-filter and re-sort the table to get positioning correct, not perfect
     * as this will actually redraw the table on screen, but the update should be so fast (and
     * possibly not alter what is already on display) that the user will not notice
     */
    this.oApi._fnReDraw(oSettings);

    /* Find it's position in the table */
    var iPos = -1;
    for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++)
    {
        if (i == tmp_2_i)
        {
            iPos = i;
            break;
        }
    }
    /* Get starting point, taking account of paging */
    if (iPos >= 0)
    {
        oSettings._iDisplayStart = (Math.floor(i / oSettings._iDisplayLength)) * oSettings._iDisplayLength;
        this.oApi._fnCalculateEnd(oSettings);
    }

    this.oApi._fnDraw(oSettings);
};
$.fn.dataTableExt.oApi.fnReloadAjax = function (oSettings, sNewSource, fnCallback, bStandingRedraw)
{
    // DataTables 1.10 compatibility - if 1.10 then versionCheck exists.
    // 1.10s API has ajax reloading built in, so we use those abilities
    // directly.
    if ($.fn.dataTable.versionCheck) {
        var api = new $.fn.dataTable.Api(oSettings);

        if (sNewSource) {
            api.ajax.url(sNewSource).load(fnCallback, !bStandingRedraw);
        }
        else {
            api.ajax.reload(fnCallback, !bStandingRedraw);
        }
        return;
    }

    if (sNewSource !== undefined && sNewSource !== null) {
        oSettings.sAjaxSource = sNewSource;
    }

    // Server-side processing should just call fnDraw
    if (oSettings.oFeatures.bServerSide) {
        this.fnDraw();
        return;
    }

    this.oApi._fnProcessingDisplay(oSettings, true);
    var that = this;
    var iStart = oSettings._iDisplayStart;
    var aData = [];

    this.oApi._fnServerParams(oSettings, aData);

    oSettings.fnServerData.call(oSettings.oInstance, oSettings.sAjaxSource, aData, function (json) {
        /* Clear the old information from the table */
        that.oApi._fnClearTable(oSettings);

        /* Got the data - add it to the table */
        var aData = (oSettings.sAjaxDataProp !== "") ?
                that.oApi._fnGetObjectDataFn(oSettings.sAjaxDataProp)(json) : json;

        for (var i = 0; i < aData.length; i++)
        {
            that.oApi._fnAddData(oSettings, aData[i]);
        }

        oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();

        that.fnDraw();

        if (bStandingRedraw === true)
        {
            oSettings._iDisplayStart = iStart;
            that.oApi._fnCalculateEnd(oSettings);
            that.fnDraw(false);
        }

        that.oApi._fnProcessingDisplay(oSettings, false);

        /* Callback user function - for event handlers etc */
        if (typeof fnCallback == 'function' && fnCallback !== null)
        {
            fnCallback(oSettings);
        }
    }, oSettings);
};

jQuery.fn.dataTableExt.oApi.fnSwapRow = function (oSettings, selected, direction) {
    var elementIndex = selected._DT_RowIndex;
    var element = oSettings.aoData[elementIndex];
    var elementForSwitch;
    if ((direction == 'up' && elementIndex > 0) || (direction == 'down' && elementIndex < oSettings.aoData.length)) {
        if (direction == 'up') {
            elementForSwitch = oSettings.aoData[elementIndex - 1];
            element.nTr._DT_RowIndex = elementIndex - 1;
            oSettings.aoData[elementIndex - 1] = element;
            elementForSwitch.nTr._DT_RowIndex = elementIndex;
            oSettings.aoData[elementIndex] = elementForSwitch;
        } else {
            elementForSwitch = oSettings.aoData[elementIndex + 1];
            element.nTr._DT_RowIndex = elementIndex + 1;
            oSettings.aoData[elementIndex + 1] = element;
            elementForSwitch.nTr._DT_RowIndex = elementIndex;
            oSettings.aoData[elementIndex] = elementForSwitch;
        }
        this.oApi._fnDraw(oSettings);
        console.log(selected);
    }
}
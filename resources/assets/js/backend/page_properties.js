var content_save_error = false;
var langArray = {en: 1, de: 2};
var pageTitle = {'content_id': {}, 'text': {}};

var change_workflow_responsible = function(){};

function clearPropertiesFrom() {
	pageTitle.content_id = {};
	$('#user_form .input_group').removeClass('error');
	$('#user_form .role_group').removeClass('error');
	$('#user_form input:checkbox:checked').removeAttr('checked');
	$('#user_form input:radio:checked').removeAttr('checked');
	$('#user_form input:radio[value="0"]').prop('checked', true);
	$('#user_form input#type_file').prop('checked', true);
	$('#user_form input[name="id"]').val('0');
    $('#user_form input[name="u_id"]').val('0');
	$('#user_form input:text').val('');
	$('#user_form textarea').val('');
	$('#user_form input[name="title_de"]').parent().hide();
	$('#user_form input[name="title_en"]').parent().hide();
//	$('#user_form #state_review').attr('disabled','true');
//	$('#user_form #state_approved').attr('disabled','true');
//	$('#user_form #state_draft').prop('checked', true);
	$('#user_form input[name="active"]').removeAttr('disabled');
    $('#template_select ul li:first-of-type').click();
    $('#user_form .tags').html('');

}

function closePropertiesFrom() {
	clearPropertiesFrom();
	$('#properties_wrapper').hide();
	$('#properties_wrapper .user_fields').hide();
	$('.left_navigation .nav_box.properties').removeClass('active');
	$('body').css('cursor', 'default');
}

function savePageProperties(callbackFunc, type, selectedRow) {
    var promise = $.Deferred().reject();
	if (validatePropertiesFields()) {
		var url = relPath + "api/v1/pages/create";
		if (type == "PUT") {
			url = relPath + "api/v1/pages/update";
		}
		promise = jQuery.ajax({
			type: type,
			url: url,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: parseFormProp(),
			success: function(data) {
				if($("input[name = 'isPageRestricted']").length>0){
					$("input[name = 'isPageRestricted']").val(data.response.restricted);
				}
				saveContent((type == "PUT") ? true : false,data.response.id, data.response.u_id, function(has_content_error) {
					if (!has_content_error) {
						if (callbackFunc instanceof Function) {
							callbackFunc(data, selectedRow);
						}
						$('body').css('cursor', 'default');
						jAlert(data.message, js_localize['pages.edit.title'], "success");
					} else {
						$('body').css('cursor', 'default');
						jAlert(js_localize['pages.edit.error'], js_localize['pages.edit.title'], "error");
					}
				});
			},
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
				var message = JSON_ERROR.composeMessageFromJSON(data, true);
				if (data.response !== undefined) {
					$.each(data.response, function(key, value) {
						$('input[name="' + key + '"]').parent().addClass('error');
					});
				}
				$('body').css('cursor', 'default');
				jAlert(message, js_localize['pages.edit.title'], "error");
			}
		});
	}
    return promise;
}

function saveContent(isUpdate, page_id, page_u_id, callbackFunc) {
	content_save_error = false;
	var createContentArray = [];
	var updateContentArray = [];
	for (var lang in langArray)
	{
		if ($('input#language_' + lang).prop('checked') == true) {
			var title = $('input[name="title_' + lang + '"]').val();
			if (isUpdate && pageTitle.content_id[lang] > 0) {
				var content = {id: pageTitle.content_id[lang], body: title};
				updateContentArray.push(content);
			} else {
				// Create Title
				var content = {page_u_id:page_u_id, page_id: page_id, lang: langArray[lang], position: 0, section: 'title', format: 1, body: title, searchable: 1};
				createContentArray.push(content);
			}
		}
	}

	// Create Title
	if (createContentArray.length > 0) {
		jQuery.ajax({
			type: "POST",
			url: relPath + "api/v1/contents/create-many",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(createContentArray),
			success: function(data) {
				if (callbackFunc instanceof Function) {
					callbackFunc(false);
				}
			},
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				//TODO: Get Language for the error
				$('input[name="title_de"]').parent().addClass('error');
				$('input[name="title_en"]').parent().addClass('error');
				if (callbackFunc instanceof Function) {
					callbackFunc(true);
				}
			}
		});
	}
	// Update Title
	if (isUpdate && updateContentArray.length > 0) {
		jQuery.ajax({
			type: "PUT",
			url: relPath + "api/v1/contents/update-many",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(updateContentArray),
			success: function(data) {
				if (callbackFunc instanceof Function) {
					callbackFunc(false);
				}
			},
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				//TODO: Get Language
				$('input[name="title_de"]').parent().addClass('error');
				$('input[name="title_en"]').parent().addClass('error');
				if (callbackFunc instanceof Function) {
					callbackFunc(true);
				}
			}
		});
	}
}

/*
 * Update Access to page only.
 * Not used at the moment to increase the performance.
 *
 */
function updateMembership(pageId, message) {
	$('#user_form input[name="access"]').each(function(index) {
		var access = {page_id: pageId, group_id: $(this).val()};
		if ($(this).prop('checked') == true) {
			jQuery.ajax({
				type: "POST",
				url: relPath + "api/v1/access/grant",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: JSON.stringify(access),
				// script call was *not* successful
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
					var message = JSON_ERROR.composeMessageFromJSON(data, true);
					jAlert(message, js_localize['pages.edit.title'], "error");
				},
				success: function(data) {
				},
				complete: function(data) {
					if (index == $('#user_form input[name="access"]').length - 1) {
						$('body').css('cursor', 'default');
						jAlert(message, js_localize['pages.edit.title'], "success");
					}
				}
			});
		} else {
			jQuery.ajax({
				type: "DELETE",
				url: relPath + "api/v1/access/revoke",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: JSON.stringify(access),
				// script call was *not* successful
				error: function(XMLHttpRequest, textStatus, errorThrown) {
				},
				success: function(data) {
				},
				complete: function(data) {
					if (index == $('#user_form input[name="access"]').length - 1) {
						$('body').css('cursor', 'default');
						jAlert(message, js_localize['pages.edit.title'], "success");
					}
				}
			});
		}
	});
}

function populateFormPage(pageId,data, callbackFunc) {
	jQuery.when(
			jQuery.ajax({
				type: "GET",
				url: relPath + "api/v1/pages/read/" + pageId + "/access",
				success: function(data) {
					if(data.response.workflow.user_responsible != EDITING_USER && (data.response.workflow.editing_state != 'Draft' || data.response.workflow.user_responsible != 0)){
						change_workflow_responsible = function(){
							jConfirm(js_localize['pages.edit.workflow.responsible'],js_localize['pages.edit.title'], 'success',function(r){
								if(r){
									changePageResponsible(data.response.u_id,EDITING_USER);
								}else{
                                    if(data != undefined){
                                        var promise = unlockItem(data.u_id,'page');
                                        promise.always(function(){
                                            editing_timer.stopTimer(false);
                                            $('#wrapper_form_page').closeDialog();
                                            $('.user_actions .nav_box').removeClass('active');
                                            $('.user_edit_wrapper .user_fields').hide();
                                            $('.user_table').css('height', '');
                                            $('.dataTables_paginate.paging_ellipses').show();
                                            active_inactive();
                                        });
                                    }
								}
							});
						}
					}else{
                        change_workflow_responsible = function(){};
                    }
					$.each(data.response, function(key, value) {
						if (key == 'langs') {
							//TODO: Optimize it
							var binary = value.toString(2);
							if (value <= 1) {
								binary = "0" + binary;
							}
							var binary_array = binary.split('');
							$('#user_form input[name="' + key + '"]').each(function(i) {
								if (binary_array[i] == 1) {
									if (i == 0) {
										$('#user_form input[name="title_de"]').parent().show();
									} else {
										$('#user_form input[name="title_en"]').parent().show();
									}
									$(this).prop('checked', true);
								} else {
									if (i == 0) {
										$('#user_form input[name="title_de"]').parent().hide();
									} else {
										$('#user_form input[name="title_en"]').parent().hide();
									}
									$(this).prop('checked', false);
								}
							});
						} else if (key == 'access') {
							$.each(value, function(key, value) {
								$('#user_form input[name="access"][value="' + value.group_id + '"]').prop('checked', true);
							});
						} else if (key == 'tags') {
							$.each(value, function(key, value) {
                                generateTagElement(value.id, value.name, value.lang, $('.tags_lang[data-lang="'+value.lang+'"]'));
							});
						} else if ($('#user_form input[name="' + key + '"]').attr('type') == 'checkbox' || $('#user_form input[name="' + key + '"]').attr('type') == 'radio') {
							$('#user_form input[name="' + key + '"][value="' + value + '"]').prop('checked', true);
							if(key == 'active'){
//								if(data.response.workflow.editing_state != "Approved"){
									$('#user_form input[name="active"]').attr('disabled','disabled');
//								}else{
//									$('#user_form input[name="active"]').removeAttr('disabled');
//								}
							}
						} else if ($('p#' + key).length > 0) {
							$('p#' + key).html(value);
						} else if ($('#user_form select[name="' + key + '"]').length > 0) {
                            if(value){
                                $('#user_form select[name="' + key + '"]').val(value);
                                $('#user_form .styledSelect[page_prop="'+key+'"]').text($('#user_form select[name="' + key + '"]').find(":selected").text());
                                if(key == 'template' && value == 'downloads'){
                                    $('#download_categories').show();
                                }
                            }
						} else {
							$('#user_form input[name="' + key + '"]').val(value);
						}
					});
				}, // success
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
					var message = JSON_ERROR.composeMessageFromJSON(data, true);
					jAlert(message, js_localize['pages.properties.title'], "error");
				}
			}),
	jQuery.ajax({
		type: "GET",
		url: relPath + "api/v1/contents/page-id/" + pageId + "?section=title",
		success: function(data) {
			//Reset title_content_id
			pageTitle.content_id = {};
			$.each(data.response, function(key, value) {
				if (value.lang == 2) {
					$('input[name="' + value.section + '_de"]').val(value.body);
					pageTitle.text['2'] = value.body;
					pageTitle.content_id['de'] = value.id;
				}
				if (value.lang == 1) {
					$('input[name="' + value.section + '_en"]').val(value.body);
					pageTitle.text['1'] = value.body;
					pageTitle.content_id['en'] = value.id;
				}
			});
		}, // success
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
			var message = JSON_ERROR.composeMessageFromJSON(data, true);
			jAlert(message, js_localize['pages.properties.title'], "error");
		}
	})
			).then(function() {
		callbackFunc(true);
	}).fail(function(data) {
		callbackFunc(false);
	});
	return false;
}

function parseFormProp() {
	var serialized = $('#user_form').serializeArray();
	var s = '';
	var langs_binary = 0;
	var data = {};
	var accessArray = [];
    var keywords_arr = [];
	for (s in serialized) {
		if (serialized[s]['name'] == "access") {
			var access = {group_id: serialized[s]['value']};
			accessArray.push(access);
		} else if(serialized[s]['name'] == "keywords_arr"){
            keywords_arr.push(JSON.parse(serialized[s]['value']));
        }else if (serialized[s]['name'] != "title_en" && serialized[s]['name'] != "title_de") {
			if (serialized[s]['type'] == "active" || serialized[s]['name'] == "searchable" || serialized[s]['name'] == "in_navigation") {
				data[serialized[s]['name']] = parseInt(serialized[s]['value']);
			} else if (serialized[s]['name'] == "langs") {
				langs_binary = langs_binary + parseInt(serialized[s]['value']);
			} else {
				data[serialized[s]['name']] = serialized[s]['value'];
			}
		}
	}
	data['langs'] = langs_binary;
	data['access'] = accessArray;
//   data['authorization'] = 0;
	data['home_block'] = $('#home_block').is(':checked') === true ? 1 : 0;
	data['front_block'] = $('#front_block').is(':checked') === true ? 1 : 0;
	data['home_accordion'] = $('#home_accordion').is(':checked') === true ? 1 : 0;
    data['featured_menu'] = $('#featured_menu').is(':checked') === true ? 1 : 0;
    data['keywords'] = keywords_arr;

	return JSON.stringify(data);
}

function hideTitle(el, lang) {
	if ($(el).prop('checked') == true) {
		$('input[name="title_' + lang + '"]').parent().show();
	} else {
		$('input[name="title_' + lang + '"]').parent().hide();
	}
}

function validatePropertiesFields() {
	if (($('input[name="title_en"]').val() == "" && $('input#language_en').prop('checked') == true)
			|| ($('input[name="title_de"]').val() == "" && $('input#language_de').prop('checked') == true)
            ||($('input[name="title_en"]').val() == "" && $('input[name="title_de"]').val() == "") ||  $('input[name="slug"]').val() == "") {
        var msg = js_localize['pages.add.empty_loc'];
		if ($('input[name="title_en"]').val() == "")
			$('input[name="title_en"]').parent().addClass('error');
		else
			$('input[name="title_en"]').parent().removeClass('error');
		if ($('input[name="title_de"]').val() == "")
			$('input[name="title_de"]').parent().addClass('error');
		else
			$('input[name="title_de"]').parent().removeClass('error');
        if($('input[name="slug"]').val() == ""){
            $('input[name="slug"]').parent().addClass('error');
            msg = js_localize['pages.add.empty_slug']
        }
		$('body').css('cursor', 'default');
		jAlert(msg, js_localize['pages.add.title'], "error");
		return false;
	} else {
		return true;
	}
}

$(document).ready(function() {
	$('div.user_fields fieldset .nullify_input').click(function() {
		$(this).parent().children('input').val(CONFIG.get('DATE_TIME_ZERO'));
		if ($('#publish_date').val() == CONFIG.get('DATE_TIME_ZERO')) {
			curentTime.setHours(curentTime.getHours()+ 1);
			$('#unpublish_date').datepicker('option', 'minDate', curentTime);
			$('#unpublish_date').datetimepicker('option', 'minDateTime', curentTime);
		}
		if($(this).parent().parent().hasClass('unpublish_date'))
			$(this).parent().children('input').val(CONFIG.get('DATE_TIME_ZERO'));
	});

    $('.tags span.delete').click(function (e) {
        $(this).parent().remove();
        $("#tags_en").focus();
        e.stopPropagation();
    });

    $("#tags_en, #tags_de").keyup(function (e) {
        if (e.keyCode === 13 || e.keyCode === 188) {
            var newKeyword = $(this).val().replace(',', '');
            addNewKeyword(newKeyword, $(this));
        }
        return false;
    });
    $('.tags_wrapper').click(function(e){
        $(this).find('input').focus();
    })
    $("#tags_en").autocomplete({
        minLength: 2,
        source: autocompleteSource,
        select: autocompleteSelect
    });
    $("#tags_de").autocomplete({
        minLength: 2,
        source: autocompleteSource,
        select: autocompleteSelect
    });
    $('#template_select li').click(function(e){
        if($(this).attr('rel') == 'downloads'){
            $('#download_categories').show();
            $('#download_categories ul li:first-of-type').click();
        }else{
            $('#download_categories').hide();
            $('select[name="usage"], select[name="file_type"]').val('');
        }
    });
	$('.tags_fieldset .tags').sortable({
		sort: function(event, ui) {
			var $target = $(event.target);
			if (!/html|body/i.test($target.offsetParent()[0].tagName)) {
				var top = event.pageY - $target.offsetParent().offset().top - (ui.helper.outerHeight(true) / 2);
				ui.helper.css({'top': top + 'px'});
			}
		}
	});

});

function autocompleteSelect(event, ui) {
    generateTagElement(ui.item.tag_id, ui.item.value, ui.item.lang, $(this));
    event.stopPropagation();
    return false;
}

function autocompleteSource(request, response) {
    var term = request.term.toLowerCase(),
            element = this.element,
            cache = this.element.data('autocompleteCache') || {},
            foundInCache = false;

    $.each(cache, function (key, data) {
        if (term.indexOf(key) === 0 && data.length > 0) {
            var result = jQuery.grep(data, function (n) {
                return (n.value.toLowerCase().indexOf(term) >= 0);
            });
            response(result);
            foundInCache = true;
            return;
        }
    });
    if (foundInCache)
        return;
    var lang_id = this.element.attr('data-lang');
    $.ajax({
        url: relPath + 'api/v1/tag/source/' + lang_id,
        dataType: "json",
        data: request,
        success: function (data) {
            var data_arr = new Array();
            for (var key = 0; key < data.response.length; key++) {
                data_arr[key] = {value: $('<div/>').html(data.response[key].value).text(), tag_id: $('<div/>').html(data.response[key].id).text(), lang: $('<div/>').html(lang_id).text()};
            }
            cache[term] = data_arr;
            element.data('autocompleteCache', cache);
            response(data_arr);
        }
    });
}

function addNewKeyword(newKeyword, element) {
    newKeyword = newKeyword.trim();
    if (newKeyword !== "") {
       generateTagElement('', newKeyword, element.attr('data-lang'), element);
    }
    if ($('#ui-id-1').is(":visible")) {
        $('#ui-id-1').hide();
    }
}

function generateTagElement(id, name, lang, element) {
    var duplicateTags;
    if(id == ''){
        duplicateTags = element.parent().find('input[data-value="' + name + '"]');
    }else{
        duplicateTags = element.parent().find("input[data-id='" + id + "']");
    }
    var result = false;
    if (duplicateTags.length == 0) {
        var newDiv = $('<div></div>').addClass('keywords');
        newDiv.append($('<span></span>').text(name));
        var json = JSON.stringify({tag_id: id, name: name, lang: lang});
        newDiv.append($('<input>').val(json).attr({name: 'keywords_arr', type: 'hidden', 'data-id': id, 'data-value': name}));
        newDiv.append($('<span></span>').text('X').addClass('delete').click(function (e) {
            $(this).parent().remove();
            element.focus();
            e.stopPropagation();
        }));
        element.prev('.tags').append(newDiv);
        element.val('');
        result = true;
    }
    return result;
}

function getPDF(pageU_id, isPreview) {
    jAlert(js_localize['pages.edit.pdf'], js_localize['pages.edit.title'], "success");
    if(isPreview){
        var url = relPath + 'cms/preview_pdf/' + pageU_id;
    }else{
        var url = relPath + 'cms/pdf_print/' + pageU_id;
    }

    var xhr = createXMLHTTPObject();
    xhr.open('GET', url, true);
    xhr.responseType = 'blob';
    xhr.onload = function (e) {
        if (this.status >= 200 && this.status < 300) {
            var filename = "";
            var disposition = this.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1])
                    filename = matches[1].replace(/['"]/g, '');
            }

            var type = xhr.getResponseHeader('Content-Type');
            var blob = e.target.response;

            if (typeof window.navigator.msSaveBlob !== 'undefined') {
                // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                window.navigator.msSaveBlob(blob, filename);
            } else {
                var URL = window.URL || window.webkitURL;
                var downloadUrl = URL.createObjectURL(blob);

                if (filename) {
                    // use HTML5 a[download] attribute to specify filename
                    var a = document.createElement("a");
                    // safari doesn't support this yet
                    if (typeof a.download === 'undefined') {
                        window.location = downloadUrl;
                    } else {
                        a.href = downloadUrl;
                        a.download = filename;
                        document.body.appendChild(a);
                        a.click();
                    }
                } else {
                    window.location = downloadUrl;
                }

                setTimeout(function () {
                    URL.revokeObjectURL(downloadUrl);
                }, 100); // cleanup
            }
        } else {
            jAlert(this.statusText, js_localize['pages.edit.title'], "error");
        }
    };

    xhr.send();
}
function createXMLHTTPObject() {
    var XMLHttpFactories = [
        function () {return new XMLHttpRequest();},
        function () {return new ActiveXObject("Msxml2.XMLHTTP");},
        function () {return new ActiveXObject("Msxml3.XMLHTTP");},
        function () {return new ActiveXObject("Microsoft.XMLHTTP");}
    ];
    var xmlhttp = false;
    for (var i=0;i<XMLHttpFactories.length;i++) {
        try {
            xmlhttp = XMLHttpFactories[i]();
        }
        catch (e) {
            continue;
        }
        break;
    }
    return xmlhttp;
}

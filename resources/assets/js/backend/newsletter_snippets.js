var nl_text_bottom_big_image = '<table width="100%" cellpadding="0" cellspacing="0" style="background-color: #e6e6e6;">\
                                                                <tr>\
                                                                    <td class="img-flex" style="border: 1px solid #c8c8c8;">\
                                                                        <a target="_blank" href="/">\
                                                                        <img src="{selection_src}" style="vertical-align:top; width:620px; height:410px;" width="620" height="410" alt="{selection_desc}" />\
                                                                        </a>\
                                                                    </td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td style="padding:23px 18px 30px;">\
                                                                        <table width="100%" cellpadding="0" cellspacing="0">\
                                                                            <tr>\
                                                                                <td class="h-auto" valign="top">\
                                                                                    <table width="100%" cellpadding="0" cellspacing="0">\
                                                                                        <tr>\
                                                                                            <td style="font:14px/22px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:0 0 8px;">\
                                                                                                <a target="_blank" style="text-decoration: none !important; color: #000; display: inline-block;" href="/">\
                                                                                                <span class="webfont14" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:12px; line-height:20px; color:#000; padding:0 0px;">\
                                                                                                [Copy text]&nbsp; \
                                                                                                </span>\
                                                                                                </a>\
                                                                                            </td>\
                                                                                        </tr>\
                                                                                    </table>\
                                                                                </td>\
                                                                            </tr>\
                                                                            <tr>\
                                                                                <td>\
                                                                                    <table cellpadding="0" cellspacing="0">\
                                                                                        <tr>\
                                                                                            <td class="active" align="center" style="font:14px/22px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#00677f;">\
                                                                                                <a target="_blank" style="text-decoration:none; color:#00677f; display:block;" href="/">\
                                                                                                    <span class="webfont14" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:12px; line-height:20px; padding:0 0px;">\
                                                                                                    '+js_localize['tiny_mce.init.nl_image_more']+'&nbsp;\
                                                                                                    </span>\
                                                                                                    <span style="display:inline-block; line-height:0; vertical-align:-4px;">\
                                                                                                        <img src="https://designnavigator.daimler.com/newsletter/arrow_small.png" style="display:block; vertical-align:top; width:7px; height:16px;" width="7" height="16" alt="&gt;" />\
                                                                                                    </span>\
                                                                                                </a>\
                                                                                            </td>\
                                                                                        </tr>\
                                                                                    </table>\
                                                                                </td>\
                                                                            </tr>\
                                                                        </table>\
                                                                    </td>\
                                                                </tr>\
                                                            </table><div style="height:30px;line-height:30px;margin:0;">&nbsp;</div><br style="display:none;" class="snippet_sep"/>';

var nl_text_right_image_snippet = '<table width="100%" cellpadding="0" cellspacing="0">\
                                                    <tr>\
                                                        <th class="flex" width="300" align="left" style="vertical-align:top; padding:0;">\
                                                            <table width="100%" cellpadding="0" cellspacing="0">\
                                                                <tr>\
                                                                    <td style="padding:0 0 30px;">\
                                                                        <table width="100%" cellpadding="0" cellspacing="0">\
                                                                            <tr>\
                                                                                <td class="img-flex" style="border: 1px solid #c8c8c8;">\
																					 <a target="_blank" href="/">\
																					<img src="{selection_src}" style="vertical-align:top; width:300px; height:200px;" width="300" height="200" alt="{selection_desc}" />\
																					</a>\
																				</td>\
                                                                            </tr>\
                                                                        </table>\
                                                                    </td>\
                                                                </tr>\
                                                            </table>\
                                                        </th>\
                                                        <th class="flex" width="18" height="0" style="padding:0; font-size:0; line-height:0;">&nbsp;</th>\
                                                        <th class="flex" align="left" style="vertical-align:top; padding:0;">\
                                                            <table width="100%" cellpadding="0" cellspacing="0">\
                                                                <tr>\
                                                                    <td class="holder-03" style="padding:0 9px 30px 19px;"><table width="100%" cellpadding="0" cellspacing="0">\
                                                                            <tr>\
                                                                                <td class="h-auto" valign="top">\
                                                                                    <table width="100%" cellpadding="0" cellspacing="0">\
                                                                                        <tr>\
                                                                                            <td style="font:14px/22px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:0 0 8px;">\
																								<a target="_blank" style="text-decoration: none !important; color: #000;display: inline-block;" href="/">\
                                                                                                <span class="webfont14" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:12px; line-height:20px; color:#000; padding:0 0px;">[Copy text]&nbsp; </span>\
                                                                                                </a>\
                                                                                            </td>\
                                                                                        </tr>\
                                                                                    </table>\
                                                                                </td>\
                                                                            </tr>\
                                                                            <tr>\
                                                                                <td>\
                                                                                    <table cellpadding="0" cellspacing="0">\
                                                                                        <tr>\
                                                                                            <td class="active" align="center" style="font:14px/22px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#00677f;" bgcolor="#ffffff">\
                                                                                                <a target="_blank" style="text-decoration:none; color:#00677f; display:block;" href="/">\
																								<span class="webfont14" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:12px; line-height:20px;  padding:0 0px;">\
                                                                                                    '+js_localize['tiny_mce.init.nl_image_more']+'&nbsp;\
                                                                                                </span>\
                                                                                                <span style="display:inline-block; line-height:0; vertical-align:-4px;">\
                                                                                                    <img src="https://designnavigator.daimler.com/newsletter/arrow_small.png" style="display:block; vertical-align:top; width:7px; height:16px;" width="7" height="16" alt="&gt;" />\
                                                                                                </span>\
																								</a>\
                                                                                            </td>\
                                                                                        </tr>\
                                                                                    </table>\
                                                                                </td>\
                                                                            </tr>\
                                                                        </table>\
																	</td>\
                                                                </tr>\
                                                            </table>\
                                                        </th>\
                                                    </tr>\
                                                </table><p style="display:none;" class="snippet_sep"></p>';

var nl_text_bottom_image_snippet = '<table width="100%" cellpadding="0" cellspacing="0">\
                                                                <tr>\
                                                                    <td style="padding:0 0 20px;">\
                                                                        <table width="100%" cellpadding="0" cellspacing="0">\
                                                                            <tr>\
                                                                                <td class="img-flex" style="border: 1px solid #c8c8c8;">\
                                                                                    <a target="_blank" href="/">\
                                                                                    <img src="{selection_src}" style="vertical-align:top; width:300px; height:200px;" width="300" height="200" alt="{selection_desc}" />\
                                                                                    </a>\
                                                                                </td>\
                                                                            </tr>\
                                                                        </table>\
                                                                    </td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td class="holder-02" style="padding:6px 17px 25px;">\
																		<table width="100%" cellpadding="0" cellspacing="0">\
                                                                            <tr>\
                                                                                <td class="h-auto" valign="top">\
                                                                                    <table width="100%" cellpadding="0" cellspacing="0">\
                                                                                        <tr>\
                                                                                            <td style="font:14px/22px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:0 0 8px;">\
                                                                                                 <a target="_blank" style="text-decoration: none !important; color: #000;     display: inline-block;" href="/">\
																								<span class="webfont14" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:12px; line-height:20px; color:#000; padding:0 0px;">\
                                                                                               [Copy text]&nbsp; \
                                                                                                </span>\
																								</a>\
                                                                                            </td>\
                                                                                        </tr>\
                                                                                    </table>\
                                                                                </td>\
                                                                            </tr>\
                                                                            <tr>\
                                                                                <td>\
                                                                                    <table cellpadding="0" cellspacing="0">\
                                                                                        <tr>\
                                                                                            <td class="active" align="center" style="font:14px/22px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#00677f;" bgcolor="#ffffff">\
                                                                                                   <a target="_blank" style="text-decoration:none; color:#00677f; display:block;" href="/">\
																									   <span class="webfont14" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:12px; line-height:20px; padding:0 0px;">\
																										 '+js_localize['tiny_mce.init.nl_image_more']+'&nbsp;\
																										</span>\
																										 <span style="display:inline-block; line-height:0; vertical-align:-4px;">\
																											<img src="https://designnavigator.daimler.com/newsletter/arrow_small.png" style="display:block; vertical-align:top; width:7px; height:16px;" width="7" height="16" alt="&gt;" />\
																										</span>\
																									</a>\
                                                                                            </td>\
                                                                                        </tr>\
                                                                                    </table>\
                                                                                </td>\
                                                                            </tr>\
                                                                        </table>\
																	</td>\
                                                                </tr>\
                                                            </table>';
var nl_title_section = '<table width="100%" cellspacing="0" cellpadding="0">\
                                                                <tbody><tr>\
                                                                    <td style="font:24px/28px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:10px 0 30px;">\
                                                                        <span class="webfont28" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:22px; line-height:26px; color:#000; padding:0 0px;">'+js_localize['tiny_mce.init.nl_greet']+'</span>\
                                                                    </td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td style="font:14px/22px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000;">\
                                                                        <span class="webfont14" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:12px; line-height:20px; color:#000; padding:0 0px;">\
                                                                       [Copy Text] \
                                                                        </span>\
                                                                    </td>\
                                                                </tr>\
                                                            </tbody></table>';
var nl_info_section = '<table width="100%" cellpadding="0" cellspacing="0">\
                                                                <tr>\
                                                                    <td style="font:24px/28px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:10px 20px 15px;">\
                                                                        <span class="webfont28" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:22px; line-height:26px; color:#000; padding:0 0px;">\
                                                                        '+js_localize['tiny_mce.init.nl_inhalt']+'\
                                                                        </span>\
                                                                    </td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td style="padding:15px 20px; border-bottom:1px solid #cbcbcb;">\
                                                                        <table width="100%" cellpadding="0" cellspacing="0">\
                                                                            <tr>\
                                                                                <td height="20" style="font:14px/14px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000;">\
                                                                                    <span class="webfont14" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:12px; line-height:12px; color:#000; padding:0 0px;">\
                                                                                    <a style="color:#000001; text-decoration:none;" href="/">'+js_localize['tiny_mce.init.nl_newest']+'</a>\
                                                                                    </span>\
                                                                                </td>\
                                                                                <td width="20" bgcolor="#00677f" style="border-radius:10px;">\
                                                                                    <table width="100%" cellpadding="0" cellspacing="0">\
                                                                                        <tr>\
                                                                                            <td align="center" style="border-radius:10px; font:12px/14px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#fff;">3</td>\
                                                                                        </tr>\
                                                                                    </table>\
                                                                                </td>\
                                                                            </tr>\
                                                                        </table>\
                                                                    </td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td style="padding:14px 20px 15px; border-bottom:1px solid #fff;">\
                                                                        <table width="100%" cellpadding="0" cellspacing="0">\
                                                                            <tr>\
                                                                                <td height="20" style="font:14px/14px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000;">\
                                                                                    <span class="webfont14" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:12px; line-height:12px; color:#000; padding:0 0px;">\
                                                                                    <a style="color:#000001; text-decoration:none;" href="/">'+js_localize['tiny_mce.init.nl_bp']+'</a>\
                                                                                    </span>\
                                                                                </td>\
                                                                                <td width="20" bgcolor="#00677f" style="border-radius:10px;">\
                                                                                    <table width="100%" cellpadding="0" cellspacing="0">\
                                                                                        <tr>\
                                                                                            <td align="center" style="border-radius:10px; font:12px/14px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#fff;">5</td>\
                                                                                        </tr>\
                                                                                    </table>\
                                                                                </td>\
                                                                            </tr>\
                                                                        </table>\
                                                                    </td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td style="padding:14px 20px 15px; border-bottom:1px solid #fff;">\
                                                                        <table width="100%" cellpadding="0" cellspacing="0">\
                                                                            <tr>\
                                                                                <td height="20" style="font:14px/14px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000;">&nbsp;\
                                                                                </td>\
                                                                                <td width="20" bgcolor="#ffffff" style="border-radius:10px;">\
                                                                                    <table width="100%" cellpadding="0" cellspacing="0">\
                                                                                        <tr>\
                                                                                            <td align="center" style="border-radius:10px;font:10px/12px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#fff;">&nbsp;</td>\
                                                                                        </tr>\
                                                                                    </table>\
                                                                                </td>\
                                                                            </tr>\
                                                                        </table>\
                                                                    </td>\
                                                                </tr>\
                                                                <tr>\
                                                                    <td style="padding:14px 20px 15px; border-bottom:1px solid #fff;">\
                                                                        <table width="100%" cellpadding="0" cellspacing="0">\
                                                                            <tr>\
                                                                                <td height="20" style="font:14px/14px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000;">\
                                                                                    <span class="webfont14" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:12px; line-height:12px; color:#000; padding:0 0px;">\
                                                                                    <a style="color:#000001; text-decoration:none;" href="#footer">\
                                                                                    '+js_localize['tiny_mce.init.nl_impressum']+'\
                                                                                    </a>\
                                                                                    </span>\
                                                                                </td>\
                                                                                <td width="20" bgcolor="#ffffff" style="border-radius:10px;">\
                                                                                    <table width="100%" cellpadding="0" cellspacing="0">\
                                                                                        <tr>\
                                                                                            <td align="center" style="border-radius:10px;font:10px/12px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#fff;">&nbsp;</td>\
                                                                                        </tr>\
                                                                                    </table>\
                                                                                </td>\
                                                                            </tr>\
                                                                        </table>\
                                                                    </td>\
                                                                </tr>\
                                                            </table>';

var header_section_snippet = '<table class="wrapper" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">\
    <tr>\
        <td>\
            <table class="flexible" width="660" bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;">\
                <tr>\
                    <td style="padding:20px 20px 17px;">\
                        <table width="100%" cellpadding="0" cellspacing="0">\
                            <tr>\
                                <td>\
                                    <table width="100%" cellpadding="0" cellspacing="0">\
                                        <tr>\
                                            <th class="flex" width="300" align="left" style="vertical-align:top; padding:0;">\
                                                <table width="100%" cellpadding="0" cellspacing="0">\
                                                    <tr>\
                                                        <td class="holder-01" style="padding:40px 0 0 20px;"><br /></td>\
                                                    </tr>\
                                                </table>\
                                            </th>\
                                            <th class="hide" width="20" height="10" style="padding:0; font-size:0; line-height:0;">&nbsp;</th>\
                                            <th class="flex" width="300" align="left" style="vertical-align:top; padding:0;">\
                                                <table width="100%" cellpadding="0" cellspacing="0">\
                                                    <tr>\
                                                        <td style="padding:40px 0 0;"><br /></td>\
                                                    </tr>\
                                                </table>\
                                            </th>\
                                        </tr>\
                                    </table>\
                                </td>\
                            </tr>\
                        </table>\
                    </td>\
                </tr>\
            </table>\
        </td>\
    </tr>\
</table><br style="display:none;" class="snippet_sep"/>';

var content_section_snippet = '<table class="wrapper" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">\
    <tr>\
        <td>\
            <table class="flexible" width="660" bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;">\
                <tr>\
                    <td style="padding:0 20px;">\
                        <table width="100%" cellpadding="0" cellspacing="0">\
                            <tr>\
                                <td class="indent-top-25" style="padding:35px 0 0;">\
                                    <table width="100%" cellpadding="0" cellspacing="0">\
										<tr>\
										<td style="font:36px/42px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:0 18px 33px;">\
                                                <span class="webfont36" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:34px; line-height:40px; color:#000; padding:0 0px;">\
                                                [Section Title]\
                                                </span>\
                                            </td>\
										</tr>\
                                        <tr>\
											<td><br /></td>\
										</tr>\
                                    </table>\
                                </td>\
                            </tr>\
                        </table>\
                    </td>\
                </tr>\
            </table>\
        </td>\
    </tr>\
</table><br style="display:none;" class="snippet_sep" />';

var two_section_snippet = '<table width="100%" cellpadding="0" cellspacing="0">\
                                                    <tr>\
                                                        <th class="flex" width="300" align="left" style="vertical-align:top; padding:0;"><br/></th>\
                                                        <th class="flex" width="20" height="0" style="padding:0; font-size:0; line-height:0;">&nbsp;</th>\
                                                        <th class="flex" width="300" align="left" style="vertical-align:top; padding:0;"><br/></th>\
                                                    </tr>\
                                                </table><br style="display:none;" class="snippet_sep" />';

var nl_footer = '<table class="flexible" width="660" align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;">\
				<tbody><tr>\
					<td class="footer" bgcolor="#e6e6e6" style="padding:90px 20px;">\
						<table width="100%" cellpadding="0" cellspacing="0">\
							<tbody><tr>\
								<td>\
									<table width="100%" cellpadding="0" cellspacing="0">\
										<tbody><tr>\
											<th class="flex" width="300" align="left" style="vertical-align:top; padding:0;">\
												<table width="100%" cellpadding="0" cellspacing="0">\
													<tbody><tr>\
														<td style="font:14px/22px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:0 0 0 20px;">\
															<span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; color:#000; padding:0 0px;">\
																Daimler AG<br>\
																Sitz und Registergericht/Domicile and Court of Registry: Stuttgart<br>\
																HRB-NrCommercial Register No. 19360<br>\
																Vorsitzender des Aufsichtsrats/Chairman of the Supervisory Board: Manfred Bischoff<br>\
																Vorstand/Board of Management: Dieter Zetsche (Vorsitzender/Chairman), Wolfgang Bernhard, Renata Jungo Brüngger, Ola Källenius, <br>\
																Wilfried Porth, Hubertus Troska, Bodo Uebber, Thomas Weber<br><br>\
															</span>\
														</td>\
													</tr>\
												</tbody></table>\
											</th>\
											<th class="flex" style="padding: 0;" height="4" width="20">&nbsp;</th>\
											<th class="flex" width="300" align="left" style="vertical-align:top; padding:0;">\
												<table width="100%" cellpadding="0" cellspacing="0">\
													<tbody><tr>\
														<td style="padding:0 0 0;">\
															<table width="100%" cellpadding="0" cellspacing="0">\
																<tbody><tr>\
																	<td class="indent-width-16" style="font:14px/16px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:0px 5px 17px 20px; border-top:1px solid #e6e6e6;">\
																		<span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; color:#000; padding:0 0px;">\
																			<a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Provider?lang=de">Anbieter</a> |\
																			<a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Provider?lang=en">Provider</a>\
																		</span>\
																	</td>\
																</tr>\
																<tr>\
																	<td class="indent-width-16" style="font:14px/16px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:17px 5px 17px 20px; border-top:1px solid #cbcbcb;">\
																		<span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; color:#000; padding:0 0px;">\
																			<a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Legal_Notice?lang=de">Rechtliche Hinweise</a> |\
																			<a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Legal_Notice?lang=en">Legal Notice</a>\
																		</span>\
																	</td>\
																</tr>\
																<tr>\
																	<td class="indent-width-16" style="font:14px/16px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:17px 5px 17px 20px; border-top:1px solid #cbcbcb;">\
																		<span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; color:#000; padding:0 0px;">\
																			<a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Privacy_Statement?lang=de">Datenschutz</a> |\
																			<a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Privacy_Statement?lang=en">Privacy Statement</a>\
																		</span>\
																	</td>\
																</tr>\
																<tr>\
																	<td class="indent-width-16" style="font:14px/16px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:17px 5px 17px 20px; border-top:1px solid #cbcbcb;">\
																		<span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; color:#000; padding:0 0px;">\
																			<a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Cookies?lang=de">Cookies-Hinweise</a> |\
																			<a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Cookies?lang=en">Cookies Notice</a>\
																		</span>\
																	</td>\
																</tr>\
																<tr>\
																	<td class="indent-width-16" style="font:14px/16px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:17px 5px 17px 20px; border-top:1px solid #cbcbcb;">\
																		<span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; color:#000; padding:0 0px;">\
																			<a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Contacts?lang=de">Kontakte</a> |\
																			<a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Contacts?lang=en">Contacts</a>\
																		</span>\
																	</td>\
																</tr>\
															</tbody></table>\
														</td>\
													</tr>\
												</tbody></table>\
											</th>\
										</tr>\
									</tbody></table>\
								</td>\
							</tr>\
						</tbody></table>\
					</td>\
				</tr>\
			</tbody></table>';
var nl_unsuscribe_link = '<table class="wrapper" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">\
			<tbody><tr>\
				<td>\
					<table class="flexible" width="660" align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;">\
						<tbody><tr>\
							<td style="padding:20px 20px 30px;">\
								<table width="100%" cellpadding="0" cellspacing="0">\
									<tbody><tr class="table-holder">\
										<th class="tfoot" width="300" align="left" style="vertical-align:top; padding:0;">\
											<table width="100%" cellpadding="0" cellspacing="0">\
												<tbody><tr>\
													<td class="indent-width-16" style="font:14px/16px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:0 20px;">\
														<span class="webfont14" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:12px; line-height:14px; color:#000; padding:0 0px;">\
														© 2016 Daimler AG\
														</span>\
													</td>\
												</tr>\
											</tbody></table>\
										</th>\
										<th class="trow" width="20" height="16" style="padding:0; font-size:0; line-height:0;"></th>\
										<th class="thead" width="300" align="left" style="vertical-align:top; padding:0;">\
											<table width="100%" cellpadding="0" cellspacing="0">\
												<tbody><tr>\
													<td class="indent-width-16" style="font:14px/16px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:0 20px;">\
														<span class="webfont14" style="font-family:\'Arial\', \'Helvetica\', \'sans-serif\'; font-size:12px; line-height:14px; color:#000; padding:0 0px;">\
														<a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Newsletter_Opt_Out?lang=de">Abbestellen</a> |\
														<a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Newsletter_Opt_Out?lang=en">Unsubscribe</a>\
														</span>\
													</td>\
												</tr>\
											</tbody></table>\
										</th>\
									</tr>\
								</tbody></table>\
							</td>\
						</tr>\
					</tbody></table>\
				</td>\
			</tr>\
		</tbody></table>';
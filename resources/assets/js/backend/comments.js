var oTable;
//var relPath = getRelativePath();

$(document).ready(function() {

	oTable = $('#comments').dataTable({
		"bProcessing": false,
		"sAjaxSource": relPath+"api/v1/comments/all/"+lang,
		"sAjaxDataProp": 'response',
		"fnServerData": fnServerData,
		"iDisplayLength": 45,
		"bFilter": true,
		"sPaginationType": 'ellipses',
		"bLengthChange": false,
		"iShowPages": 10,
		"bSort": false,
		"bDeferRender": true,
		"oLanguage": {
			"sEmptyTable": " "},
		"aoColumns": [
			{"sType": "numeric", "mData": "id", "sWidth": "70px", "bSearchable": false, "sClass": "comment_id"},
			{"sType": "string", "mData": "page_title", "sWidth": "130px", "bSearchable": true},
			{"sType": "string", "mData":  function(source) {
					if(source.username == "anonymous"){
						return js_localize['comments.label.deleted'];
					}else{
						return source.username;
					}}, "sWidth": "130px", "sClass": "strong", "bSearchable": true},
			{"sType": "string", "mData": "subject", "sWidth": "215px", "bSearchable": true},
			{"sType": "string", "mData": function(source) {
					if(source.body.length > 27){
						return source.body.substring(0,27)+"...";
					}else{
						return source.body;
					}	}, "sWidth": "215px", "bSearchable": false},
			{"sType": "string", "mData": function(source) {
					if(source.active === 1) return js_localize['users.yes'];
					return js_localize['users.no'];}, "sWidth": "70px", "bSearchable": false, "sClass": "comment_active"},
			{"sType": "date-eu", "mData": "created_at", "sWidth": "135px", "bSearchable": false}
		],
		"fnInitComplete": function(oSettings, json) {
			if(username){
				$('.user_search').val($('<textarea />').html(username).text());
				refreshTable();
				if(!valid_user){
					jAlert(js_localize['comments.no_user.text'].replace('{0}',username),js_localize['comments.no_user.title'],"error");
				}
			}
		}
	});

	var oSettings = oTable.fnSettings();
	oSettings.sAlertTitle = js_localize['comments.list.title'];

	$("#comments tbody").click(function(event) {
		if (!$(event.target).hasClass('dataTables_empty')){
			closeProfile();
			$(oTable.fnSettings().aoData).each(function() {
				$(this.nTr).removeClass('row_selected');
			});
			$(event.target.parentNode).addClass('row_selected');

			var anSelected = fnGetSelected(oTable);

			if($(anSelected[0]).find('.comment_active').html() == "Yes"){
				$('.user_edit_wrapper .user_actions .nav_box.activate').html(js_localize['comments.label.deactivate']);
							$('.user_edit_wrapper .user_actions .nav_box.activate').addClass('deactivate');
			}else{
				$('.user_edit_wrapper .user_actions .nav_box.activate').html(js_localize['comments.label.activate']);
							$('.user_edit_wrapper .user_actions .nav_box.activate').removeClass('deactivate');
			}

			$('.user_edit_wrapper').css('top', $('#comments_wrapper').position().top + $(event.target.parentNode).position().top + $(event.target.parentNode).height() + 1);
			$('.user_edit_wrapper').show();
			//alert($('#users_wrapper').position().top)
		}
	});
	
	$('body').click(function(event) {
		if($(event.target).parents('tbody').length == 0 && $(event.target).parents('.user_edit_wrapper').length == 0 && $('#popup_overlay').length == 0){
			closeProfile();
		}
		if (event.target.parentNode) {
			if (!(event.target.parentNode.className == "admin_data" || event.target.parentNode.id == "admin_info" || event.target.parentNode.id == "admin_action")) {
				if ($('#admin_action').is(":visible"))
				{
					$("#admin_info").removeClass('active');
					$('#admin_action').hide();
				}
			}
		}
	});

	/* Add a click handler for the delete row */
	$('.user_actions .nav_box.remove').click(function() {
		jConfirm($(this).attr('confirm'), js_localize['comments.remove.title'],"success", function(r) {
			if (r) {
				var anSelected = fnGetSelected(oTable);
				closeProfile();
				if (anSelected.length !== 0) {
					jQuery.ajax({
						type: "DELETE",
						url: relPath+"api/v1/comments/delete/" + $(anSelected[0]).find('.comment_id').html(),
						success: function(data) {
							oTable.fnDeleteRow(anSelected[0]);
							//alert("Comment " + data.response.subject  + " was delete successfully!");
							jAlert(js_localize['comments.remove.text'].replace('{0}',data.response.subject),js_localize['comments.remove.title'],"success");
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
							var message = JSON_ERROR.composeMessageFromJSON(data, true);
							jAlert(message, js_localize['comments.remove.title'], "error");
						},
					});
				}
			}
		});	
	});

	$('.user_actions .nav_box.edit').click(function() {
		$('#wrapper_form_comments').openDialog($('div.user_edit_wrapper .textarea_overlay'));
		$('#comment_form .input_group').removeClass('error');
		$('body').css('cursor','progress');
		$('.user_actions .nav_box').removeClass('active');
		$(this).addClass('active');
		var anSelected = fnGetSelected(oTable);
		$('.user_edit_wrapper .user_fields').show();
		$('#comment_form input:checkbox:checked').removeAttr('checked');
		populateForm($(anSelected[0]).find('.comment_id').html());		
		var height = $('.user_edit_wrapper').position().top + $('.user_edit_wrapper').height() + $('.user_fields').height() + 10;
		if($('.user_table').height() < height){
			$('.user_table').css('height', height + 'px');
			$('.dataTables_paginate.paging_ellipses').hide();
		}
		
	});
	
	$('.user_actions .nav_box.activate').click(function() {
		$('body').css('cursor','progress');
		var anSelected = fnGetSelected(oTable);
		if($(anSelected[0]).find('.comment_active').html() == js_localize['users.yes']){
			var active = 0;
		}else{
			var active = 1;
		}
		jQuery.ajax({
				type: "PUT",
				url: relPath+"api/v1/comments/update",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: JSON.stringify({active:active,id:$(anSelected[0]).find('.comment_id').html()}),
				success: function(data) {
					$('body').css('cursor','default');
					closeProfile();
					oTable.fnUpdate(data.response, anSelected[0]);
				},
				// script call was *not* successful
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
					var message = JSON_ERROR.composeMessageFromJSON(data);
					if(data.response !== undefined){
						$.each(data.response, function(key, value) {
							$('input[name="' + key + '"]').parent().addClass('error');
						});
					}
					$('body').css('cursor','default');
					//alert("Error while updating comment!");
					jAlert(message, js_localize['comments.activate.title'], "error");
				},
		});
	});	
	
	

	$('.user_fields .nav_box.cancel').click(function() {
		$('#wrapper_form_comments').closeDialog();
		$('#comment_form .input_group').removeClass('error');
		$('.user_actions .nav_box').removeClass('active');
		$('.user_edit_wrapper .user_fields').hide();
		$('.user_table').css('height', '');
		$('.dataTables_paginate.paging_ellipses').show();
	});

	$('.user_fields .nav_box.save').click(function() {
		$('body').css('cursor','progress');
		var anSelected = fnGetSelected(oTable);
		
		if ($('#comment_form input[name="id"]').val() != "" && $('#comment_form input[name="id"]').val() > 0) {
			jQuery.ajax({
				type: "PUT",
				url: relPath+"api/v1/comments/update",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: parseForm(),
				success: function(data) {
					$('#wrapper_form_comments').closeDialog();
					$('body').css('cursor','default');
					jAlert(data.message,js_localize['comments.edit.title'],"success")
					closeProfile();
					oTable.fnUpdate(data.response, anSelected[0]);
					$('.user_table').css('height', '');
					$('.dataTables_paginate.paging_ellipses').show();
				},
				// script call was *not* successful
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
					var message = JSON_ERROR.composeMessageFromJSON(data);
					if(data.response !== undefined){
						$.each(data.response, function(key, value) {
							$('input[name="' + key + '"]').parent().addClass('error');
						});
					}
					$('body').css('cursor','default');
					//alert("Error while updating comment!");
					jAlert(message, js_localize['comments.edit.title'], "error");
				},
			});
		} else {
			jQuery.ajax({
				type: "POST",
				url: relPath+"api/v1/comments/create",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: parseForm(),
				success: function(data) {
					$('#wrapper_form_comments').closeDialog();
					$('body').css('cursor','default');
					jAlert(data.message,js_localize['comments.add.title'],"success")
					closeProfile();
					oTable.fnAddData(data.response);
					$('.user_table').css('height', '');
					$('.dataTables_paginate.paging_ellipses').show();
				},
				// script call was *not* successful
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
					var message = JSON_ERROR.composeMessageFromJSON(data);
					if(data.response !== undefined){
						$.each(data.response, function(key, value) {
							$('input[name="' + key + '"]').parent().addClass('error');
						});
					}
					$('body').css('cursor','default');
					//alert("Error while adding comment!");
					jAlert(message, js_localize['comments.add.title'], "error");
				},
			});
		}	
	});
});

function closeProfile() {
	$('#comment_form .input_group').removeClass('error');
	$('.user_actions .nav_box').removeClass('active');
	$('.user_edit_wrapper .user_fields').hide();
	$('#comment_form input:text').val('');
	$('#comment_form input:password').val('');
	$('#comment_form input[name="id"]').val('0');
	$('#comment_form input:checkbox:checked').removeAttr('checked');
	$('#comment_form input:radio:checked').removeAttr('checked');
	$('#comment_form textarea').val('');
	$(oTable.fnSettings().aoData).each(function() {
		$(this.nTr).removeClass('row_selected');
	});
	$('.user_edit_wrapper').hide();
}

/* Get the rows which are currently selected */
function fnGetSelected(oTableLocal) {
	return oTableLocal.$('tr.row_selected');
}
function refreshTable() {
	var search_input = $('.user_search').val() !== js_localize['pages.label.search_comments'] ? $('.user_search').val() : "";
	var sort_input = $('.user_sort').val() == -1 ? 1 : $('.user_sort').val();
	oTable.fnSort([[sort_input, 'asc']]);
	oTable.fnFilter(search_input);
	return false;
}

function populateForm(commentId) {
	jQuery.ajax({
		type: "GET",
		url: relPath+"api/v1/comments/read/" + commentId,
		success: function(data) {
			$.each(data.response, function(key, value) {
				if ($('input[name="' + key + '"]').attr('type') == 'checkbox' || $('input[name="' + key + '"]').attr('type') == 'radio') {
					$('input[name="' + key + '"][value="' + value + '"]').prop('checked', true)
				} else if($('p#'+key).length > 0){
					$('p#'+key).html(value);
				} else {
					$(':input[name="' + key + '"]').val(value);
				}
			});
			$('body').css('cursor','default');
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
			var message = JSON_ERROR.composeMessageFromJSON(data, true);
			jAlert(message, js_localize['comments.remove.title'], "error");
		}
	});
	return false;
}

function parseForm() {
	var serialized = $('#comment_form').serializeArray();
	var s = '';
	var data = {};
	for (s in serialized) {
		if(serialized[s]['name'] != "group"){
			if(serialized[s]['name'] == "role" || serialized[s]['name'] == "active" || serialized[s]['name'] == "newsletter"){
				data[serialized[s]['name']] = parseInt(serialized[s]['value']);
			}else{
				data[serialized[s]['name']] = serialized[s]['value'];
			}
		}
	}
	return JSON.stringify(data);
}

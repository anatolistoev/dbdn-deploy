//var relPath = getRelativePath();
var content_text = "";

$(document).ready(function() {
    bindSectionClicks('main');
//    bindSectionClicks('right');
    bindSectionClicks('banner');
    bindSectionClicks('tablet_banner');
    bindSectionClicks('mobile_banner');
//    bindSectionClicks('farRight');
	bindSectionClicks('author');
    bindSectionClicks('related');
	bindPagePropertiesClicks();
    bindRelatedClicks();
	$('#page_save').addClass('save_inactive');
    $('#page_save').click(function() {
            savePage(this);
    });

    $('.top_navigation a,#change_lang, .admin_info a').click(function(e) {
        if ($(this).attr('id') != "page_save") {
            el = this;
            if ($('#content_main_text').hasClass('changed') || $('#content_banner_text').hasClass('changed')|| $('#content_tablet_banner_text').hasClass('changed')|| $('#content_mobile_banner_text').hasClass('changed') || $('#content_author_text').hasClass('changed')) {
                e.preventDefault();
                var cancel_def =  $.alerts.cancelButton;
                $.alerts.cancelButton =  '&nbsp;No&nbsp;';
                jConfirm(js_localize['content.save.confirm'], js_localize['content.save.title'], "error", function(r) {
                    if (r) {
                        if (saveImgs()) {
//                            saveSection('main', function() {
//                                if (!$('#content_right_text').hasClass('changed') && !$('#content_banner_text').hasClass('changed') && !$('#content_farRight_text').hasClass('changed'))
//                                    window.location.href = $(el).attr('href');
//                            });
//                            saveSection('right', function() {
//                                if (!$('#content_banner_text').hasClass('changed') && !$('#content_farRight_text').hasClass('changed'))
//                                    window.location.href = $(el).attr('href');
//                            });
//                            saveSection('banner', function() {
//                                if (!$('#content_farRight_text').hasClass('changed'))
//                                    window.location.href = $(el).attr('href');
//                            });
//                            saveSection('farRight', function() {
//                                    window.location.href = $(el).attr('href');
//                            });
//
                            //var rightPromise = saveSection('right');
                             var bannerPromise, authorPromise, mainPromise, relatedPromise, tabletBannerPromise, mobileBannerPromise;
                             bannerPromise =authorPromise = mainPromise = relatedPromise = $.Deferred();
                            if($('#content_banner_text').hasClass('changed')){
                               bannerPromise = saveSection('banner');
                            }else{
                                 bannerPromise.resolve();
                            }
                            if($('#content_tablet_banner_text').hasClass('changed')){
                               tabletBannerPromise = saveSection('tablet_banner');
                            }else{
                                 tabletBannerPromise.resolve();
                            }
                            if($('#content_mobile_banner_text').hasClass('changed')){
                               mobileBannerPromise = saveSection('mobile_banner');
                            }else{
                                 mobileBannerPromise.resolve();
                            }
                            //var farRightPromise = saveSection('farRight');
                            if($('#content_author_text').hasClass('changed')){
                                authorPromise = saveSection('author');
                            }else{
                                authorPromise.resolve();
                            }
                            if($('#content_main_text').hasClass('changed')){
                                mainPromise = saveSection('main');
                            }else{
                                mainPromise.resolve();
                            }

                            if($('#content_related_text').hasClass('changed') || $('#content_download_text').hasClass('changed') || $('#content_bp_text').hasClass('changed')){
                                relatedPromise = saveSection('related');
                            }else{
                                relatedPromise.resolve();
                            }
                            var promise = $.when(  bannerPromise, mainPromise, authorPromise, relatedPromise, mobileBannerPromise, tabletBannerPromise).always(function() {
                                if($(el).attr('id') != "change_lang"){
                                    var statusPromise = unlockItem($('div#content_edit_page_u_id').html(),'page');
                                    statusPromise.always(function() {
                                         window.location.href = $(el).attr('href');
                                    });
                                }else if($(el).attr('id') != "page_workflow"){
                                    window.location.href = $(el).attr('href');
                                }

                             });
                        }else{
                            jAlert(js_localize['content.images.user_error'], js_localize['content.images.title'], "error");
                        }
                    } else {
                        if($(el).attr('id') != "change_lang" && $(el).attr('id') != "page_workflow"){
							var statusPromise = unlockItem($('div#content_edit_page_u_id').html(),'page');
							statusPromise.always(function() {
								window.location.href = $(el).attr('href');
							});
						}else if($(el).attr('id') == "change_lang"){
							window.location.href = $(el).attr('href');
						}
                    }
                });
				$.alerts.cancelButton =  cancel_def;
            }else{
                if($(el).attr('id') != "change_lang" && $(el).attr('id') != "page_workflow"){
                    var statusPromise = unlockItem($('div#content_edit_page_u_id').html(),'page');
                    statusPromise.always(function() {
                        window.location.href = $(el).attr('href');
                    });
                }else if($(el).attr('id') == "change_lang"){
					window.location.href = $(el).attr('href');
				}
            }
        }
        return false;
    });

    $('a#pdf').click(function(e){
        e.stopPropagation();
        e.preventDefault();
        return false;
    })
});

function savePage(element) {
    var result = $.Deferred().reject();
    if ($('#page_save').hasClass('not_saved')) {
        if (saveImgs()) {
            var bannerPromise, authorPromise, mainPromise, relatedPromise, mobileBannerPromise, tabletBannerPromise;
            bannerPromise =authorPromise = mainPromise = relatedPromise = mobileBannerPromise = tabletBannerPromise =  $.Deferred();
            if($('#content_banner_text').hasClass('changed')){
                bannerPromise = saveSection('banner');
            }else{
                bannerPromise.resolve();
            }
            if($('#content_mobile_banner_text').hasClass('changed')){
                mobileBannerPromise = saveSection('mobile_banner');
            }else{
                mobileBannerPromise.resolve();
            }
            if($('#content_tablet_banner_text').hasClass('changed')){
                tabletBannerPromise = saveSection('tablet_banner');
            }else{
                tabletBannerPromise.resolve();
            }
            if($('#content_main_text').hasClass('changed')){
               mainPromise = saveSection('main');
            }else{
               mainPromise.resolve();
            }
            if($('#content_author_text').hasClass('changed')){
               authorPromise = saveSection('author');
            }else{
               authorPromise.resolve();
            }
            if($('#content_related_text').hasClass('changed') || $('#content_download_text').hasClass('changed') || $('#content_bp_text').hasClass('changed')){
                relatedPromise = saveSection('related');
            }else{
                relatedPromise.resolve();
            }
            $("#page_save").removeClass('not_saved');
            result = $.when(bannerPromise, mainPromise,authorPromise,relatedPromise, mobileBannerPromise, tabletBannerPromise);
        } else {
            jAlert(js_localize['content.images.user_error'], js_localize['content.images.title'], "error");
        }
    } else {
        // Disable Save Button there is no chages
        $(element).addClass('save_inactive');
    }
    return result;
}

function bindSectionClicks(section) {
    if(section == 'related'){
        $('.content_related a.modify_links.back_to_view').click(function() {
            var text_container = $(this).parent().prev();
            if(text_container.attr('changed')){
                text_container.addClass('changed');
                text_container.closest(".content_related").addClass('not_saved');
                $("#page_save").addClass('not_saved');
            }
            $(this).parent().parent().find('div.related_buttons').hide();
            $(this).parent().hide();
            $(this).parent().parent().removeClass('unviewable').css('z-index', 0);
            $(this).parent().parent().find('div.active').removeClass("active");
            $('.content_related .edit').removeClass('clicked');
            $('.textarea_overlay').hide();
            $('body').focus();
            return false;
        });
    }else{
        var image_attr = '';
        $('#content_' + section + '_text').find('img.single_image').each(function() {
            image_attr += $(this).data('gallery') + ',';
        });
        $('#content_' + section + '_text').attr('gallery', image_attr.slice(0, -1));
        $('div.content_' + section + ' .modify_wrapper a.nav_box.back_to_view').click(function() {
            //alert( tinyMCE.get(section + '_body').getContent() );
            if(section == 'farRight' || section == 'right'){
                    $(this).parent().children('.mce-tinymce').css('top', '0');
            }
            $(this).hide();
            $('.textarea_overlay').hide();
            tinyMCE.get(section + '_body').hide();
            // Workaround to hide TinyMCE menus
            $('.mce-menu').hide();
            var content_text_new = tinyMCE.get(section + '_body').getContent();
            content_text_new = $('#content_' + section + '_text').html(content_text_new).html();
            $(this).parent().parent().removeClass('unviewable');
            $('div.content_' + section + ' .modify_wrapper a.nav_box.edit').removeClass('clicked');
            $(this).parent().parent().find('.modify_overlay').hide();
            $(this).parent().parent().find('.modify_wrapper').hide();
            if(content_text.trim() !=  content_text_new.trim()){
                $(this).parent().parent().find('#content_' + section + '_text').addClass('changed');
                $(this).parent().parent().find('#content_' + section + '_text').attr('gallery', '');
                var image_attr = '';
                $('#content_' + section + '_text').find('img.simple,img.zoom,img.inline_zoom,img.simple_no_zoom,img.gallery,img.explainer').each(function() {
                    image_attr += $(this).data('gallery') + ',';
                });
                $('#content_' + section + '_text').attr('gallery', image_attr.slice(0, -1));

                $(this).parent().parent().addClass('not_saved');
                $("#page_save").addClass('not_saved');
            }

            $('#content_main_text').find('table').each( function() {
                max_width = 740; //$(this).parent().parent().width();
                if($(this).width() > max_width)
                    $(this).width(max_width);
            });

            content_text = "";

            $('body').focus();

            return false;
        });
    }
    $('div.content_' + section).hover(function() {
        if (!$(this).hasClass('unviewable')) {
            $(this).find('.modify_overlay').show();
            $(this).find('.modify_wrapper').show();
            $(this).removeClass('viewable');
        }
    }, function() {
        if (!$(this).hasClass('unviewable')) {
            $(this).find('.modify_overlay').hide();
            $(this).find('.modify_wrapper').hide();
            $(this).addClass('viewable');
        }
    });

    $('div.content_' + section + ' .modify_wrapper a.nav_box.edit').click(function() {
        if (!$(this).hasClass('clicked')) {
			if(section == 'author'){
				if($(this).offset().top - 310 > $(window).scrollTop()){
					$(this).parent().children('.mce-tinymce').css('top', '-387px');
				}
			}
            if(section == 'related'){
                $(this).parent().parent().find('div.related_buttons').show();
                $(this).parent().parent().find('div.modify_buttons').show();
                $(this).parent().parent().find('.modify_overlay').hide();
                $(this).parent().parent().find('.modify_wrapper').hide();
                $(this).parent().parent().css('z-index', 135);
            }else{
                content_text = $('#content_' + section + '_text').html();
                tinyMCE.get(section + '_body').show();
                tinyMCE.get(section + '_body').setContent(content_text);
                if(section !== 'banner')
                    tinyMCE.get(section + '_body').getBody().setAttribute('contenteditable', false);
                $('div.content_' + section + ' .modify_wrapper a.nav_box.back_to_view').show();
            }
            $('.textarea_overlay').show();
            $(this).parent().parent().addClass('unviewable');
            $(this).addClass('clicked');
        }
    });


}

function bindPagePropertiesClicks() {
	$('.left_navigation .nav_box.properties').click(function() {
		clearPropertiesFrom();
		$('body').css('cursor', 'progress');
		$('a').css('cursor', 'progress');
		populateFormPage($('div#content_edit_page_u_id').html(),undefined, function (data) {
			$('#wrapper_form_page').openDialog($('div#properties_wrapper .textarea_overlay'));
			$('#properties_wrapper').show();
			$('#properties_wrapper .user_fields').show();
			$('body').css('cursor', 'default');
			$('a').css('cursor', 'pointer');
		});

		$(this).addClass('active');
	});

	$('#properties_wrapper .user_fields .nav_box.cancel').click(function() {
		$('#wrapper_form_page').closeDialog();
		$('#properties_wrapper').hide();
		$('#user_form .input_group').removeClass('error');
		$('#user_form .role_group').removeClass('error');
		$('.left_navigation .nav_box.properties').removeClass('active');
		$('#properties_wrapper .user_fields').hide();
	});

	$('#properties_wrapper .user_fields .nav_box.save').click(function () {
        $('body').css('cursor', 'progress');
        if ($('#page_active').is(':checked')) {
            jConfirm(js_localize['pages.version.activate'], js_localize['pages.version.title'], "success", function (r) {
                if (r) {
                    savePageProperties(closePropertiesFrom, "PUT");
                } else {
                    $('body').css('cursor', 'default');
                }
            });
        } else {
            savePageProperties(closePropertiesFrom, "PUT");
        }
    });

	$('#page_workflow').click(function(){
		$('#wrapper_form_workflow').openDialog($('div#workflow_wrapper .textarea_overlay'));
		$('#workflow_wrapper').show();
		$('#workflow_wrapper .workflow_fields').show();
		$('body').css('cursor', 'default');
		$('a').css('cursor', 'pointer');
		$(this).addClass('active');
		return false;
	});

	$('#workflow_wrapper .user_fields .nav_box.cancel').click(function() {
		$('#wrapper_form_workflow').closeDialog();
		$('#workflow_wrapper').hide();
		$('#user_form .input_group').removeClass('error');
		$('#user_form .role_group').removeClass('error');
		$('.top_navigation #page_workflow').removeClass('active');
		$('#workflow_wrapper .user_fields').hide();
	});

	$('#workflow_wrapper .user_fields .nav_box.save').click(function() {
		$('body').css('cursor', 'progress');
		savePageWorkflow();
	});

	$('.left_navigation .nav_box.moderations').click(function(){
		$('#moderations_wrapper_inner').openDialog($('div#moderations_wrapper .textarea_overlay'));
		$('#moderations_wrapper').show();
		$('#workflow_wrapper_inner').show();
		$('body').css('cursor', 'default');
		$('a').css('cursor', 'pointer');
		$(this).addClass('active');

		$('.moderation_holder').html('');
		$('.moderation_holder').css('height', '0px');

		$.ajax({
			url: relPath + 'api/v1/pages/notes/' + $('div#content_edit_page_u_id').html(),
			type: "GET",
			success: function(data) {
				if (data.response.length > 0) {
					$('.moderation_holder').css('height', 'auto');
				}
				$.each(data.response, function(key, value) {
					var moderation_tmpl = $('#moderation_template').html();
					$('.moderation_holder').append(moderation_tmpl);
					$('.moderation_holder .page_moderation').last().find('h6.moderation_by').html($('#moderation_template h6.moderation_by').html() + value.user.username);
					$('.moderation_holder .page_moderation').last().find('span.moderation_date').html(value.created);
					$('.moderation_holder .page_moderation').last().find('p').html(value.text);
				});
			}
		})
		return false;
	});

	$('#moderations_wrapper .user_fields .nav_box.cancel').click(function() {
		$('#moderations_wrapper_inner').closeDialog();
		$('#moderations_wrapper').hide();
		$('.left_navigation .nav_box.moderations').removeClass('active');
		$('#moderations_wrapper_inner').hide();
	});

    $('.left_navigation .restore').click(function(e){
        $('body').css('cursor', 'progress');
        jConfirm(js_localize['pages.edit.restore'], js_localize['pages.edit.title'], "success", function (r) {
            if (r) {
                var data = {page_u_id: $('div#content_edit_page_u_id').html(), lang: lang, }
                jQuery.ajax({
                    type: "POST",
                    url: relPath + "api/v1/contents/reset-related",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(data),
                    success: function(data) {
                        $('#related').html(data.response.content);
                        bindSectionClicks('related');
                        bindRelatedClicks();
                        $('body').css('cursor', 'default');
                    },
                    // script call was *not* successful
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                        var message = JSON_ERROR.composeMessageFromJSON(data, true);
                        $('body').css('cursor', 'default');
                        jAlert(message, js_localize['pages.edit.title'], "error");
                    }
                });
            } else {
                $('body').css('cursor', 'default');
            }
        });

    });
}

function savePageWorkflow() {

		var url = relPath + "api/v1/workflow/update";
		var promise = jQuery.ajax({
			type: "PUT",
			url: url,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: parseFormWorkflow(),
			success: function(data) {

				$('#wrapper_form_workflow').closeDialog();
				$('#workflow_wrapper').hide();
				$('#workflow_form .input_group').removeClass('error');
				$('#workflow_form .role_group').removeClass('error');
				$('.top_navigation #page_workflow').removeClass('active');
				$('#workflow_wrapper .user_fields').hide();
				$('#workflow_wrapper textarea[name="note"]').val('');
				$('body').css('cursor', 'default');
                 jAlert(data.message, js_localize['pages.edit.title'], "success");
			},
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
				var message = JSON_ERROR.composeMessageFromJSON(data, true);
				if (data.response !== undefined) {
					$.each(data.response, function(key, value) {
						$('input[name="' + key + '"]').parent().addClass('error');
					});
				}
				$('body').css('cursor', 'default');
				jAlert(message, js_localize['pages.edit.title'], "error");
			}
		});
}

function parseFormWorkflow() {
	var serialized = $('#wrapper_form_workflow #workflow_form').serializeArray();
	var s = '';
	var data = {};
	for (s in serialized) {
		data[serialized[s]['name']] = serialized[s]['value'];
	}

	return JSON.stringify(data);
}


function saveSection(section, callback) {
    var promise = $.Deferred();
    var sectionId = parseInt($('.content_' + section).attr('content_id'));
	var url = '';
	var httpMethod = 'POST';
	var tinyMCE_Content = $('#content_' + section + '_text').html();
    if(tinyMCE_Content == undefined && section != 'related'){
        promise.resolve();
    }else if (sectionId > 0) {
        var content = {id: sectionId, body: tinyMCE_Content};
		url = relPath + "api/v1/contents/update";
		httpMethod = 'PUT';
    } else if (tinyMCE_Content.trim() != "" && section != 'related') {
        var content = {page_u_id: $('div#content_edit_page_u_id').html(), page_id: $('div#content_edit_page_id').html(), lang: lang, position: 0, section: section, format: 1, body: tinyMCE_Content, searchable: 1};
		url = relPath + "api/v1/contents/create";
    }else if (tinyMCE_Content.trim() == "" && $('.content_' + section).hasClass('not_saved') && section != 'related') {
        $('.content_' + section).removeClass('not_saved');
		$('#content_' + section + '_text').removeClass('changed');
		if (typeof (callback) === 'function') {
			callback();
		}
    }else if(section == 'related'){
        url = relPath + "api/v1/contents/related";
        httpMethod = "POST";
        content = [];
        $('.content_related input:checkbox').each(function(index,element){
            var data = {};
            if($(element).is(':checked') != JSON.parse($(element).attr('data-checked'))){
                data.is_hidden = $(element).is(':checked')== true ? null : 1;
            }
            if($(element).attr('data-newpos') && $(element).attr('data-newpos') != $(element).attr('data-position')){
                data.position = $(element).attr('data-newpos');
            }
            if(!jQuery.isEmptyObject(data)){
                data.related_id = $(element).attr('name');
                data.page_u_id = $('div#content_edit_page_u_id').html();
               content.push(data);
            }
        });
        if(content.length == 0){
            url = '';
            $('.content_' + section).removeClass('not_saved');
            $('#content_' + section + '_text').removeClass('changed');
            $('#content_download_text').removeClass('changed');
            $('#content_bp_text').removeClass('changed');
            if (typeof (callback) === 'function') {
                callback();
            }
        }
    }

	if (url.length > 0) {
		promise = jQuery.ajax({
			type: httpMethod,
			url: url,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(content),
			success: function(data) {
				$('.content_' + section).removeClass('not_saved');
				$('#content_' + section + '_text').removeClass('changed');
                if(section == 'related'){
                    $('#content_download_text').removeClass('changed');
                    $('#content_bp_text').removeClass('changed');
                    $.each(data, function(index,element){
                        var input = $('.content_related').find('input[name='+element.related_id+']');
                        input.attr('data-position', element.position);
                        input.attr('data-checked', element.is_hidden == null ? true: false);
                        input.removeAttr('data-newpos');
                    });
                }
				if (typeof (callback) === 'function') {
					callback();
				}
				if(url == relPath + "api/v1/contents/create"){
					$('.content_' + section).attr('content_id',data.response[0].id);
				}
			},
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
				var message = JSON_ERROR.composeMessageFromJSON(data, true);
				jAlert(message, "Save " + section, "error");
			}
		});
	}
     return promise;
}

function saveImgs() {
    var file_data = "";
    if($('#content_main_text').attr('gallery') != "" && typeof($('#content_main_text').attr('gallery')) != "undefined") file_data += $('#content_main_text').attr('gallery')+',';
    if($('#content_right_text').attr('gallery') != "" && typeof($('#content_right_text').attr('gallery')) != "undefined") file_data +=  $('#content_right_text').attr('gallery')+',';
    if($('#content_banner_text').attr('gallery') != "" && typeof($('#content_banner_text').attr('gallery')) != "undefined") file_data += $('#content_banner_text').attr('gallery')+',';
    if($('#content_tablet_banner_text').attr('gallery') != "" && typeof($('#content_tablet_banner_text').attr('gallery')) != "undefined") file_data += $('#content_tablet_banner_text').attr('gallery')+',';
    if($('#content_mobile_banner_text').attr('gallery') != "" && typeof($('#content_mobile_banner_text').attr('gallery')) != "undefined") file_data += $('#content_mobile_banner_text').attr('gallery')+',';
	if($('#content_author_text').attr('gallery') != "" && typeof($('#content_author_text').attr('gallery')) != "undefined") file_data += $('#content_author_text').attr('gallery')+',';
    file_data = file_data.slice(0,-1);
    var file_data_array = file_data.split(',');
    if (checkDuplicates(file_data_array) > 0) {
        return false;
    } else {
        var file_info = {};
        file_info = {files: file_data_array};
        jQuery.ajax({
            type: "PUT",
            url: relPath + "api/v1/gallery/update/" + $('div#content_edit_page_u_id').html(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(file_info),
            success: function() {
            },
			// script call was *not* successful
            error: function(XMLHttpRequest, textStatus, errorThrown) {
				var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
				var message = JSON_ERROR.composeMessageFromJSON(data, true);
				if(textStatus == 'Unauthorized'){
					jAlert(message, js_localize['command.error'], "error");
				}
				jAlert(message, js_localize['content.images.title'], "error");
			}
        });
        return true;
    }
}

function checkDuplicates(arr) {
    arr.sort();
    var last = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (arr[i] == last)
            return i;
        last = arr[i];
    }
}

function openPreview(ev, pageId, isPrint = false) {
    ev.stopPropagation();
    ev.preventDefault();
    ev.stopImmediatePropagation();
	var url = relPath + 'preview/' + pageId + (isPrint ? '?isPrint=true' : '');
	//.replace(/"/g, "&quot;");
	var main_content = $('#content_main_text').html().replace(/"/g, "'");
	//var right_content = $('#content_right_text').html().replace(/"/g, "'");
    var banner_content = "";
    if($('#content_banner_text').length > 0)
	banner_content = $('#content_banner_text').html().replace(/"/g, "'");
    var tablet_banner_content = "";
    if($('#content_tablet_banner_text').length > 0)
	tablet_banner_content = $('#content_tablet_banner_text').html().replace(/"/g, "'");
    var mobile_banner_content = "";
    if($('#content_mobile_banner_text').length > 0)
	mobile_banner_content = $('#content_mobile_banner_text').html().replace(/"/g, "'");
	//var farRight_content = $('#content_farRight_text').html().replace(/"/g, "'");
	var author_content = $('#content_author_text').html().replace(/"/g, "'");
    var token = document.getElementsByName('_token')[0].value;
	var form = $('<form action="' + url + '" method="post" target="_blank">' +
            '<input type="hidden" name="_token" value="' + token + '" />' +
			'<input type="text" name="main" value="' + main_content + '" />' +
			'<input type="text" name="banner" value="' + banner_content + '" />' +
            '<input type="text" name="tablet_banner" value="' + tablet_banner_content + '" />' +
            '<input type="text" name="mobile_banner" value="' + mobile_banner_content + '" />' +
			'<input type="text" name="author" value="' + author_content + '" />' +
			'</form>');
	// $('input[name="main"]', form).attr('value', $('#content_main_text').html());
	//$('input[name="right"]', form).attr('value', $('#content_right_text').html());
	$('body').append(form);

	$(form).submit();
	$(form).remove();
}

function sendMailNotification(event) {
    event.stopPropagation();
    event.preventDefault();
	var page_id = $('#update_alert_form input[name="id"]').val();
	var data = {comments_en: $('#upd_al_en').val(), comments_de: $('#upd_al_de').val()};

	jQuery.ajax({
		type: 'PUT',
		url: relPath + 'api/v1/pages/sendalert/' + page_id,
		dataType: "json",
		data: JSON.stringify(data),
		success: function(data) {
			if (data.response !== undefined) {
				notificationCounts = data.response.count;
			}
			jAlert(js_localize['pages.alerts.success.text'].replace('{0}', notificationCounts), js_localize['pages.alerts.success.title'], "success");
            $('#update_alert_wrapper .user_fields .nav_box.cancel').click();
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
			var message = JSON_ERROR.composeMessageFromJSON(data, true);
			jAlert(message, js_localize['mail.send.title'], "error");
		}
	});

	return false;
}

function bindRelatedClicks(){
//    $(".content_related .move_up, .content_related .move_down").click(function() {
//        var wrapper = $(this).parent().parent();
//        var text_container = wrapper.parent();
//        if(!text_container.attr('changed')){
//            text_container.attr('changed', true);
//        }
//        switchPositions(wrapper, this);
//        return false;
//    });
    $(".content_related input:checkbox").click(function() {
        var text_container= $(this).closest('.related_record').parent();
        if(!text_container.attr('changed')){
            text_container.attr('changed', true);
        }
    });
    $("#content_related_text, #content_bp_text, #content_download_text").sortable({
		sort: function(event, ui) {
			var $target = $(event.target);
            //console.log($target);
		},
         update: function( event, ui ) {
             var $target = $(event.target);
             reCalculatePositions(ui);
         }
	});

    $('.order').click(function(ev){
        var container = $(this).parent().parent();
         var elements = container.find('.related_record');
         elements.sort(function(a,b){
            var compA = $(a).find('label').text().toUpperCase();
            var compB = $(b).find('label').text().toUpperCase();
            return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
        });

        var text_container = container.find('.ui-sortable');
        text_container.empty();
        text_container.append(elements);
        text_container.find('.related_record').each(function(i, el){
            var elementInput = $(el).find('input:checkbox');
            elementInput.attr('data-newpos', $(el).index());
        });
        if(!text_container.attr('changed')){
            text_container.attr('changed', true);
        }
        return false;
    });
    $('.disable').click(function(ev){
        var container = $(this).parent().parent();
        var checkboxes = container.find('.related_record input:checkbox');
        checkboxes.each(function(i, el){
            $(el).attr('checked', false);
        });
        var text_container = container.find('.ui-sortable')
        if(!text_container.attr('changed')){
            text_container.attr('changed', true);
        }
        return false;
    });
}

//function switchPositions(wrapper, element){
//    if($(element).hasClass('move_up')){
//        var siblingWrapper = wrapper.prev();
//        wrapper.insertBefore(siblingWrapper);
//        wrapper.find('.move_down').css("visibility","visible");
//    }else{
//        var siblingWrapper = wrapper.next();
//         wrapper.insertAfter(siblingWrapper);
//         wrapper.find('.move_up').css("visibility","visible");
//    }
//    var siblingInput = siblingWrapper.find('input:checkbox');
//    if(siblingInput.attr('data-newpos') || siblingInput.attr('data-position')){
//        siblingInput.attr('data-newpos', siblingWrapper.index());
//    }
//    var input = wrapper.find('input:checkbox');
//    input.attr('data-newpos', wrapper.index());
//    var childrenCount = wrapper.parent().children().length - 1;
//    if(wrapper.index() == 0){
//        wrapper.find('.move_up').css("visibility","hidden");
//         siblingWrapper.find('.move_up').css("visibility","visible");
//    }else if(childrenCount == wrapper.index()){
//         wrapper.find('.move_down').css("visibility","hidden");
//         siblingWrapper.find('.move_down').css("visibility","visible");
//    }
//    if(siblingWrapper.index() == 0){
//         siblingWrapper.find('.move_up').css("visibility","hidden");
//    }else if(childrenCount == siblingWrapper.index()){
//         siblingWrapper.find('.move_down').css("visibility","hidden");
//    }
//}

function reCalculatePositions(ui){
    var element = ui.item;
    //var new_pos = $('.related_record').index(element);
    var new_pos = element.index();
    element.find('input:checkbox').attr('data-newpos', new_pos);
    var nextElements = element.nextAll();
    nextElements.each(function(i, el){
        var siblingInput = $(el).find('input:checkbox');
        if(siblingInput.attr('data-newpos') || siblingInput.attr('data-position')){
            //siblingInput.attr('data-newpos', $('.related_record').index(el));
            siblingInput.attr('data-newpos', $(el).index());
        }
    });
    var text_container = element.parent();
    if(!text_container.attr('changed')){
        text_container.attr('changed', true);
    }
}
function getPDFPreview(ev, pageU_id){
    ev.stopPropagation();
    ev.preventDefault();
    ev.stopImmediatePropagation();
     if ($('#page_save').hasClass('not_saved')) {
         jAlert(js_localize['content.save.not_save'], js_localize['content.save.title'], "error");
     }else{
         getPDF(pageU_id, true);
     }
}
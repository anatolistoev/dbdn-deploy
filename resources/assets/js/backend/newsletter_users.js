var oTable;
//var relPath = getRelativePath();
var url_split = location.href.split('#');
var newsletter_list = (url_split[0].indexOf('test') > 1)? 'test_list' : 'productive_list';

$(document).ready(function() {
	oTable = $('#users_newsletter').dataTable({
		"bProcessing": false,
		"sAjaxSource": relPath+"api/v1/newsletter_list/all/" + newsletter_list,
		"sAjaxDataProp": 'response',
		"fnServerData": fnServerData,
        "fnInitComplete": function (oSettings, json) {
            $('#subscriber_count').html(oSettings.aoData.length);
        },		
		"iDisplayLength": 45,
		"bFilter": true,
		"sPaginationType": 'ellipses',
		"bLengthChange": false,
		"iShowPages": 10,
		"bSort": false,
		"bDeferRender": true,
		"oLanguage": {
			"sEmptyTable": " "},
		"aoColumns": [
			{"sType": "numeric", "mData": "id", "sWidth": "70px", "bSearchable": false, "sClass": "user_id"},
			{"sType": "string", "mData": "username", "sWidth": "130px", "sClass": "strong", "bSearchable": true},
			{"sType": "string", "mData": function(source) {
					return source.first_name + ' ' + source.last_name;
				}, 
				"sWidth": "130px", "bSearchable": true
			},
			{"sType": "string", "mData": function(source) {
					return getRoleTextByID(source.role);
				}, 
				"sWidth": "215px", "bSearchable": false
			},
			{"sType": "string", "mData": function(source) {
					if (source.active === 1)
						return js_localize['users.yes'];
					return js_localize['users.no'];
				}, "sWidth": "65px", "bSearchable": false},
			{"sType": "date-eu", "mData": "last_login", "sWidth": "110px", "bSearchable": false},
			{"sType": "numeric", "mData": "logins", "sWidth": "135px", "bSearchable": false}
		]
	});

	var oSettings = oTable.fnSettings();
	oSettings.sAlertTitle = js_localize['users.list.title'];

	if (newsletter_list == 'test_list') {
		$("#users_newsletter tbody").click(function(event) {
			if (!$(event.target).hasClass('dataTables_empty')){
				closeProfile();
				$(oTable.fnSettings().aoData).each(function() {
					$(this.nTr).removeClass('row_selected');
				});
				$(event.target.parentNode).addClass('row_selected');

				if(!$('a.edit').is(":visible") ){
					$('a.edit').show();
					$('a.remove').show();
				}
			}else{
				$('a.edit').hide();
				$('a.remove').hide();
			}
			$('.user_edit_wrapper').css('top', 
				$('#users_newsletter_wrapper').position().top + $(event.target.parentNode).position().top + $(event.target.parentNode).height() + 1);
			$('.user_edit_wrapper').show();
		});
	}
	
	/* Add a click handler for the delete row */
	$('.user_actions .nav_box.remove').click(function() {
		jConfirm($(this).attr('confirm'), js_localize['users.remove.title'], "success", function(r) {
			if (r) {
				var anSelected = fnGetSelected(oTable);
				closeProfile();
				if (anSelected.length !== 0) {
					jQuery.ajax({
						type: "DELETE",
						url: relPath + "api/v1/newsletter_list/remove-subscriber/" + newsletter_list + "/" + $(anSelected[0]).find('.user_id').html(),
						success: function(data) {
							oTable.fnDeleteRow(anSelected[0]);
							jAlert(data.message, js_localize['users.remove.title'], "success");
						}, // success
						// script call was *not* successful
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
							var message = JSON_ERROR.composeMessageFromJSON(data, true);
							jAlert(message, js_localize['users.remove.title'], "error");
						}
					});
				}
			}
		});
	});

	$('.user_actions .nav_box.subscriber').click(function() {
		$('#user_content').openDialog($('div.user_edit_wrapper .textarea_overlay'));
		$('#user_content').keydown(function( event ) {
  			if ( event.which == 13 ) {
   				event.preventDefault();
				$('.user_fields .nav_box.user.add').click();
  			}
		});
		$('#user_form .input_group').removeClass('error');
		$('.user_actions .nav_box').removeClass('active');
		$(this).addClass('active');
		$('#user_form input:text').val('');
		$('.user_edit_wrapper .user_fields').show();
		var height = $('.user_edit_wrapper').position().top + $('.user_edit_wrapper').height() + $('.user_fields').height() + 10;
		if($('.user_table').height() < height){
			$('.user_table').css('height', height + 'px');
			$('.dataTables_paginate.paging_ellipses').hide();
		}
	});

	$('.user_fields .nav_box.cancel').click(function() {
		$('#user_content').closeDialog();
		$('#user_form .input_group').removeClass('error');
		$('.user_actions .nav_box').removeClass('active');
		$('.user_edit_wrapper .user_fields').hide();
		$('.user_table').css('height', '');
		$('.dataTables_paginate.paging_ellipses').show();
	});
	
	$('.user_fields .nav_box.user.add').click(function(event) {
		$('body').css('cursor', 'progress');
		var username = $('#user_form input[name="username"]').val();
		var subscribeData = {username: username};
		jQuery.ajax({
			type: "POST",
			url: relPath + "api/v1/newsletter_list/add-subscriber/" + newsletter_list,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(subscribeData),
			success: function(data) {
				$('#user_content').closeDialog();
				$('.user_table').css('height', '');
				$('.dataTables_paginate.paging_ellipses').show();
				$('body').css('cursor', 'default');
				closeProfile();
				oTable.fnAddData(data.response);
				jAlert(data.message, js_localize['users.edit.title'], "success");
			},
			// script call was *not* successful
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
				var message = JSON_ERROR.composeMessageFromJSON(data, true);
				$('body').css('cursor', 'default');
				jAlert(message, js_localize['users.add.title'], "error");
			}
		});
	});	
});

function closeProfile() {
	$('#user_content').off('keydown');
	$(oTable.fnSettings().aoData).each(function() {
		$(this.nTr).removeClass('row_selected');
	});
	$('.user_edit_wrapper').hide();
}

/* Get the rows which are currently selected */
function fnGetSelected(oTableLocal) {
	return oTableLocal.$('tr.row_selected');
}

function refreshTable() {
	var search_input = $('.user_search').val() !== "Search User" ? $('.user_search').val() : "";
	var sort_input = $('.user_sort').val() == -1 ? 1 : $('.user_sort').val();
	if(sort_input == 6 || sort_input == 7){
		oTable.fnSort([[sort_input, 'desc']]);
	}else{
		oTable.fnSort([[sort_input, 'asc']]);
	}
	oTable.fnFilter(search_input);
	return false;
}



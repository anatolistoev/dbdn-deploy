//***********************************************************************************************************************************/
//	LyteBox v3.22
//
//	 Author: Markus F. Hay
//  Website: http://www.dolem.com/lytebox
//	   Date: October 2, 2007
//	License: Creative Commons Attribution 3.0 License (http://creativecommons.org/licenses/by/3.0/)
// Browsers: Tested successfully on WinXP with the following browsers (using no DOCTYPE and Strict/Transitional/Loose DOCTYPES):
//				* Firefox: 2.0.0.7, 1.5.0.12
//				* Internet Explorer: 7.0, 6.0 SP2, 5.5 SP2
//				* Opera: 9.23
//
// Releases: For up-to-date and complete release information, visit http://www.dolem.com/forum/showthread.php?tid=62
//				* v3.22 (10/02/07)
//				* v3.21 (09/30/07)
//				* v3.20 (07/12/07)
//				* v3.10 (05/28/07)
//				* v3.00 (05/15/07)
//				* v2.02 (11/13/06)
//
//   Credit: LyteBox was originally derived from the Lightbox class (v2.02) that was written by Lokesh Dhakar. For more
//			 information please visit http://huddletogether.com/projects/lightbox2/
//***********************************************************************************************************************************/
Array.prototype.removeDuplicates = function () {
    for (var i = 1; i < this.length; i++) {
        if (this[i][0] == this[i - 1][0]) {
            this.splice(i, 1);
        }
    }
}
Array.prototype.empty = function () {
    for (var i = 0; i <= this.length; i++) {
        this.shift();
    }
}
String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, '');
}

function LyteBox() {
    /*** Start Global Configuration ***/
    this.theme = 'white';	// themes: grey (default), red, green, blue, gold
    this.hideFlash = true;		// controls whether or not Flash objects should be hidden
    this.outerBorder = true;		// controls whether to show the outer grey (or theme) border
    this.resizeSpeed = 8;		// controls the speed of the image resizing (1=slowest and 10=fastest)
    this.maxOpacity = 80;		// higher opacity = darker overlay, lower opacity = lighter overlay
    this.navType = 1;		// 1 = "Prev/Next" buttons on top left and left (default), 2 = "<< prev | next >>" links next to image number
    this.autoResize = true;		// controls whether or not images should be resized if larger than the browser window dimensions
    this.doAnimations = true;		// controls whether or not "animate" Lytebox, i.e. resize transition between images, fade in/out effects, etc.

    this.borderSize = 60;		// if you adjust the padding in the CSS, you will need to update this variable -- otherwise, leave this alone...
    /*** End Global Configuration ***/

    /*** Configure Slideshow Options ***/
    this.slideInterval = 4000;		// Change value (milliseconds) to increase/decrease the time between "slides" (10000 = 10 seconds)
    this.showNavigation = true;		// true to display Next/Prev buttons/text during slideshow, false to hide
    this.showClose = true;		// true to display the Close button, false to hide
    this.showDetails = true;		// true to display image details (caption, count), false to hide
    this.showPlayPause = true;		// true to display pause/play buttons next to close button, false to hide
    this.autoEnd = true;		// true to automatically close Lytebox after the last image is reached, false to keep open
    this.pauseOnNextClick = false;	// true to pause the slideshow when the "Next" button is clicked
    this.pauseOnPrevClick = true;		// true to pause the slideshow when the "Prev" button is clicked
    /*** End Slideshow Configuration ***/

    if (this.resizeSpeed > 10) {
        this.resizeSpeed = 10;
    }
    if (this.resizeSpeed < 1) {
        resizeSpeed = 1;
    }
    this.resizeDuration = (11 - this.resizeSpeed) * 0.15;
    this.resizeWTimerArray = new Array();
    this.resizeWTimerCount = 0;
    this.resizeHTimerArray = new Array();
    this.resizeHTimerCount = 0;
    this.showContentTimerArray = new Array();
    this.showContentTimerCount = 0;
    this.overlayTimerArray = new Array();
    this.overlayTimerCount = 0;
    this.imageTimerArray = new Array();
    this.imageTimerCount = 0;
    this.timerIDArray = new Array();
    this.timerIDCount = 0;
    this.slideshowIDArray = new Array();
    this.slideshowIDCount = 0;
    this.AllTimerCollection = [
        this.resizeWTimerArray,
        this.resizeHTimerArray,
        this.showContentTimerArray,
        this.overlayTimerArray,
        this.imageTimerArray,
        this.timerIDArray,
        this.slideshowIDArray];
    this.imageArray = new Array();
    this.activeImage = null;
    this.slideArray = new Array();
    this.activeSlide = null;
    this.frameArray = new Array();
    this.activeFrame = null;
    this.checkFrame();
    this.isSlideshow = false;
    this.isLyteframe = false;
    this.initZoom = false;
    this.bodyScroll = null;
    this.boundEventCollection = {};
    //this.ie = (document.all && !window.opera) ? true : false;
    /*@cc_on
     /*@if (@_jscript)
     this.ie = (document.all && !window.opera) ? checkVersion() : false;
     /*@else @*/
    this.ie = false;
    /*@end
     @*/
    this.ie7 = (this.ie && window.XMLHttpRequest);
    this.initialize();
}

function checkVersion() {
    if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
        var ieversion = new Number(RegExp.$1)
        if (ieversion >= 8)
            return false;
        else if (ieversion < 8)
            return true;
    }
    return false;
}

LyteBox.prototype.initialize = function () {
    this.updateLyteboxItems();
    var objBody = this.doc.getElementsByTagName("body").item(0);
    if (this.doc.getElementById('lbOverlay')) {
        objBody.removeChild(this.doc.getElementById("lbOverlay"));
        objBody.removeChild(this.doc.getElementById("lbMain"));
    }
    var objOverlay = this.doc.createElement("div");
    objOverlay.setAttribute('id', 'lbOverlay');
    objOverlay.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    if ((this.ie && !this.ie7) || (this.ie7 && this.doc.compatMode == 'BackCompat')) {
        objOverlay.style.position = 'absolute';
    }
    objOverlay.style.display = 'none';
    objBody.appendChild(objOverlay);
    var objLytebox = this.doc.createElement("div");
    objLytebox.setAttribute('id', 'lbMain');
    objLytebox.style.display = 'none';
    objBody.appendChild(objLytebox);
    var lbWrapper = this.doc.createElement("div");
    lbWrapper.setAttribute('id', 'lbWrapper');
    objLytebox.appendChild(lbWrapper);
    var objOuterContainer = this.doc.createElement("div");
    objOuterContainer.setAttribute('id', 'lbOuterContainer');
    objOuterContainer.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    lbWrapper.appendChild(objOuterContainer);
    var objIframeContainer = this.doc.createElement("div");
    objIframeContainer.setAttribute('id', 'lbIframeContainer');
    objIframeContainer.style.display = 'none';
    objLytebox.appendChild(objIframeContainer);
    var objIframe = this.doc.createElement("iframe");
    objIframe.setAttribute('id', 'lbIframe');
    objIframe.setAttribute('name', 'lbIframe');
    objIframe.style.display = 'none';
    objIframeContainer.appendChild(objIframe);
    var objImageContainer = this.doc.createElement("div");
    objImageContainer.setAttribute('id', 'lbImageContainer');
    objOuterContainer.appendChild(objImageContainer);
    var objImageWrapper = this.doc.createElement("div");
    objImageWrapper.setAttribute('id', 'lbImageWrapper');
    objImageContainer.appendChild(objImageWrapper);
    var objLyteboxImage = this.doc.createElement("img");
    objLyteboxImage.setAttribute('id', 'lbImage');
    objImageWrapper.appendChild(objLyteboxImage);
    var objLoadingWrapper = this.doc.createElement("div");
    objLoadingWrapper.setAttribute('id', 'lbLoadingWrapper');
    var objLoading = this.doc.createElement("div");
    objLoading.setAttribute('class', 'basic_lbLoading');
    objLoading.setAttribute('id', 'lbLoading');
    objLoadingWrapper.appendChild(objLoading);
    objLytebox.appendChild(objLoadingWrapper);
    for (var i = 1; i < 4; i++) {
        var innderDiv = this.doc.createElement("div");
        innderDiv.setAttribute('class', ('cssload-rect' + i));
        objLoading.appendChild(innderDiv);
    }
    var objDetailsContainer = this.doc.createElement("div");
    objDetailsContainer.setAttribute('id', 'lbDetailsContainer');
    objDetailsContainer.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    lbWrapper.appendChild(objDetailsContainer);
    var objDetailsData = this.doc.createElement("div");
    objDetailsData.setAttribute('id', 'lbDetailsData');
    objDetailsData.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    objDetailsContainer.appendChild(objDetailsData);
    var objDetails = this.doc.createElement("div");
    objDetails.setAttribute('id', 'lbDetails');
    objDetailsData.appendChild(objDetails);
    var objCaption = this.doc.createElement("span");
    objCaption.setAttribute('id', 'lbCaption');
    objDetails.appendChild(objCaption);
    var objHoverNav = this.doc.createElement("div");
    objHoverNav.setAttribute('id', 'lbHoverNav');
    objImageContainer.appendChild(objHoverNav);
    var objBottomNav = this.doc.createElement("div");
    objBottomNav.setAttribute('id', 'lbBottomNav');
    objLytebox.appendChild(objBottomNav);
    var objPrev = this.doc.createElement("a");
    objPrev.setAttribute('id', 'lbPrev');
    objPrev.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    objPrev.setAttribute('href', '#');
    objHoverNav.appendChild(objPrev);
    var objNumberDisplay = this.doc.createElement("span");
    objNumberDisplay.setAttribute('id', 'lbNumberDisplay');
    //objDetails.appendChild(objNumberDisplay);
    objHoverNav.appendChild(objNumberDisplay);
    var objNext = this.doc.createElement("a");
    objNext.setAttribute('id', 'lbNext');
    objNext.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    objNext.setAttribute('href', '#');
    objHoverNav.appendChild(objNext);

    var objNavDisplay = this.doc.createElement("span");
    objNavDisplay.setAttribute('id', 'lbNavDisplay');
    objNavDisplay.style.display = 'none';
    objDetails.appendChild(objNavDisplay);

    var objCopySpan = this.doc.createElement("span");
    objCopySpan.setAttribute('id', 'copyspan');
    //objCopySpan.innerHTML = lcopy();
    //objDetailsContainer.appendChild(objCopySpan);
    objDetails.appendChild(objCopySpan);
    var objCountSpan = this.doc.createElement("div");
    objCountSpan.setAttribute('id', 'imageCounter');
    objDetails.appendChild(objCountSpan);

    var objInnerSpan = this.doc.createElement("span");
    objInnerSpan.setAttribute('id', 'innercopy');
    objInnerSpan.innerHTML = lcopy();
    objCopySpan.appendChild(objInnerSpan);

    var objClose = this.doc.createElement("a");
    objClose.setAttribute('id', 'lbClose');
    objClose.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    objClose.setAttribute('href', '#');
    objBottomNav.appendChild(objClose);
    var objTextEsc = this.doc.createElement("span");
    objTextEsc.innerHTML = 'esc';
    objTextEsc.setAttribute('id', 'lbEsc');
    objBottomNav.appendChild(objTextEsc);
    var objPause = this.doc.createElement("a");
    objPause.setAttribute('id', 'lbPause');
    objPause.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    objPause.setAttribute('href', '#');
    objPause.style.display = 'none';
    objBottomNav.appendChild(objPause);
    var objPlay = this.doc.createElement("a");
    objPlay.setAttribute('id', 'lbPlay');
    objPlay.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    objPlay.setAttribute('href', '#');
    objPlay.style.display = 'none';
    objBottomNav.appendChild(objPlay);
};
LyteBox.prototype.updateLyteboxItems = function () {
    var anchors = (this.isFrame) ? window.parent.frames[window.name].document.getElementsByTagName('a') : document.getElementsByTagName('a');
    for (var i = 0; i < anchors.length; i++) {
        var anchor = anchors[i];
        var relAttribute = String(anchor.getAttribute('rel'));
        if (anchor.getAttribute('href')) {
            if (relAttribute.toLowerCase().match('lytebox')) {
                this.addInlineEvent(anchor, 'onclick', function () {
                    myLytebox.start(this, false, false);
                    return false;
                });
            } else if (relAttribute.toLowerCase().match('lyteshow')) {
                this.addInlineEvent(anchor, 'onclick', function () {
                    myLytebox.start(this, true, false);
                    return false;
                });
            } else if (relAttribute.toLowerCase().match('lyteframe')) {
                this.addInlineEvent(anchor, 'onclick', function () {
                    myLytebox.start(this, false, true);
                    return false;
                });
            }
        }
    }
};
LyteBox.prototype.start = function (imageLink, doSlide, doFrame) {
    if (this.ie && !this.ie7) {
        this.toggleSelects('hide');
    }
    if (this.hideFlash) {
        this.toggleFlash('hide');
    }
    this.isLyteframe = (doFrame ? true : false);
    var pageSize = this.getPageSize();
    var objOverlay = this.doc.getElementById('lbOverlay');
    var objBody = this.doc.getElementsByTagName("body").item(0);
    this.bodyScroll = $(window).scrollTop();
    $('html').css('overflow', "hidden");
    //objBody.style.overflow="hidden";
    objOverlay.style.height = pageSize[1] + "px";
    objOverlay.style.display = '';
    this.appear('lbOverlay', (this.doAnimations ? 0 : this.maxOpacity));
    var anchors = (this.isFrame) ? window.parent.frames[window.name].document.getElementsByTagName('a') : document.getElementsByTagName('a');
    if (this.isLyteframe) {
        this.frameArray = [];
        this.frameNum = 0;
        if ((imageLink.getAttribute('rel') == 'lyteframe')) {
            var rev = imageLink.getAttribute('rev');
            this.frameArray.push(new Array(imageLink.getAttribute('href'), imageLink.getAttribute('title'), (rev == null || rev == '' ? 'width: 400px; height: 400px; scrolling: auto;' : rev)));
        } else {
            if (imageLink.getAttribute('rel').indexOf('lyteframe') != -1) {
                for (var i = 0; i < anchors.length; i++) {
                    var anchor = anchors[i];
                    if (anchor.getAttribute('href') && (anchor.getAttribute('rel') == imageLink.getAttribute('rel'))) {
                        var rev = anchor.getAttribute('rev');
                        this.frameArray.push(new Array(anchor.getAttribute('href'), anchor.getAttribute('data-title'), (rev == null || rev == '' ? 'width: 400px; height: 400px; scrolling: auto;' : rev)));
                    }
                }
                this.frameArray.removeDuplicates();
                while (this.frameArray[this.frameNum][0] != imageLink.getAttribute('href')) {
                    this.frameNum++;
                }
            }
        }
    } else {
        this.imageArray = [];
        this.imageNum = 0;
        this.slideArray = [];
        this.slideNum = 0;
        if ((imageLink.getAttribute('rel') == 'lytebox')) {
            this.imageArray.push(new Array(imageLink.getAttribute('href'), imageLink.getAttribute('data-title'), imageLink));
        } else {
            if (imageLink.getAttribute('rel').indexOf('lytebox') != -1) {
                for (var i = 0; i < anchors.length; i++) {
                    var anchor = anchors[i];
                    if (anchor.getAttribute('href') && (anchor.getAttribute('rel') == imageLink.getAttribute('rel'))) {
                        this.imageArray.push(new Array(anchor.getAttribute('href'), anchor.getAttribute('data-title'), anchor));
                    }
                }
                this.imageArray.removeDuplicates();
                while (this.imageArray[this.imageNum][0] != imageLink.getAttribute('href')) {
                    this.imageNum++;
                }
            }
            if (imageLink.getAttribute('rel').indexOf('lyteshow') != -1) {
                for (var i = 0; i < anchors.length; i++) {
                    var anchor = anchors[i];
                    if (anchor.getAttribute('href') && (anchor.getAttribute('rel') == imageLink.getAttribute('rel'))) {
                        this.slideArray.push(new Array(anchor.getAttribute('href'), anchor.getAttribute('data-title')));
                    }
                }
                this.slideArray.removeDuplicates();
                while (this.slideArray[this.slideNum][0] != imageLink.getAttribute('href')) {
                    this.slideNum++;
                }
            }
        }
    }
    var object = this.doc.getElementById('lbMain');
    var img = $(imageLink).find('img').first();

    var imageCont = this.doc.getElementById('lbImageContainer');
    var imgc_style = imageCont.currentStyle || window.getComputedStyle(imageCont);

    var detc = this.doc.getElementById('lbDetailsContainer');
    var detc_style = detc.currentStyle || window.getComputedStyle(detc);
    var imageTitle = (this.isSlideshow ? this.slideArray[this.slideNum][1] : ((this.imageArray[this.imageNum]) ? this.imageArray[this.imageNum][1] : ''));

    if (imageTitle == '' && !$(imageLink).find('img.inline_zoom').length > 0 && (typeof ($(imageLink).find('img').attr('data-hotspot')) == 'undefined' || $(imageLink).find('img').attr('data-hotspot').length <= 0)) {
        var x = pageSize[2] - (parseInt(parseInt($('#lbMain').width(),10)*parseFloat($('#lbDetailsContainer').width(), 10)/100) + parseInt($('#lbImageContainer').css('margin-left')) + parseInt($('#lbImageContainer').css('margin-right')) + 1 + 60) ;
    } else {
        var x = pageSize[2] - (parseInt(parseInt($('#lbMain').width(),10)*parseFloat($('#lbDetailsContainer').width(), 10)/100)+ parseInt($('#lbImageContainer').css('margin-left')) + parseInt($('#lbImageContainer').css('margin-right')) + 1);
    }
    var y = pageSize[3] - parseInt($('#lbImageContainer').css('margin-top')) - parseInt($('#lbImageContainer').css('margin-bottom'));


    var ratioHeight = 840;
    var ratioWidth = 1190;
    if ($(img).hasClass('first_image')) {
        ratioHeight = 793;
    }

    if (pageSize[2] <= 1280) {
        this.borderSize = 49;
        //$(object).css('left', '21px');
        ratioWidth = 740;
        ratioHeight = 520;
        if ($(img).hasClass('first_image')) {
            ratioHeight = 493;
        }
    }

    if (ratioWidth > x) {
        ratioHeight = Math.round(ratioHeight * (x / ratioWidth));
        ratioWidth = x;
        if (ratioHeight > y) {
            ratioWidth = Math.round(ratioWidth * (y / ratioHeight));
            ratioHeight = y;
        }
    } else if (ratioHeight > y) {
        ratioWidth = Math.round(ratioWidth * (y / ratioHeight));
        ratioHeight = y;
        if (ratioWidth > x) {
            ratioHeight = Math.round(ratioHeight * (x / ratioWidth));
            ratioWidth = x;
        }
    }

    var ratio = $(img).width() / $(img).height();
    var height = 0;
    if (ratio > ratioWidth / ratioHeight) {
        height = Math.ceil(ratioWidth / ratio);
    } else {
        height = ratioHeight;
    }
    $('#lbImageContainer').height(height);
    //object.style.height = (height + 2 * (this.borderSize)) + 'px';
    //object.style.top = (this.getPageScroll() + (pageSize[3] - (height + 2 * (this.borderSize))) / 2) + "px";
    var lbOuterContainer = this.doc.getElementById('lbOuterContainer');
    //console.log(lbOuterContainer.offsetHeight());
//    lbOuterContainer.style.lineHeight = lbOuterContainer.offsetHeight;
    object.style.display = '';
//    if(this.isLyteframe){
//        lbOuterContainer.style.width = '100%';
//    }
    if (!this.outerBorder) {
        lbOuterContainer.style.border = 'none';
        lbOuterContainer.style.border = 'none';
    } else {
        lbOuterContainer.style.borderBottom = '';
        lbOuterContainer.setAttribute((this.ie ? 'className' : 'class'), this.theme);
    }
    this.addInlineEvent(this.doc.getElementById('lbOverlay'), 'onclick', function () {
        myLytebox.end();
        return false;
    });
    this.addInlineEvent(this.doc.getElementById('lbMain'), 'onclick', function (e) {
        var e = e;
        if (!e) {
            //if (window.parent.frames[window.name] && (parent.document.getElementsByTagName('frameset').length <= 0)) {
            //	e = window.parent.window.event;
            //} else {
            e = window.event;
            //}
        }
        //var id = (e.target ? e.target.id : e.srcElement.id);
        //if (id == 'lbMain') { myLytebox.end(); return false; }
    });
    this.addInlineEvent(this.doc.getElementById('lbClose'), 'onclick', function () {
        myLytebox.end();
        return false;
    });
    this.addInlineEvent(this.doc.getElementById('lbPause'), 'onclick', function () {
        myLytebox.togglePlayPause("lbPause", "lbPlay");
        return false;
    });
    this.addInlineEvent(this.doc.getElementById('lbPlay'), 'onclick', function () {
        myLytebox.togglePlayPause("lbPlay", "lbPause");
        return false;
    });
    this.isSlideshow = doSlide;
    this.isPaused = (this.slideNum != 0 ? true : false);
    if (this.isSlideshow && this.showPlayPause && this.isPaused) {
        this.doc.getElementById('lbPlay').style.display = '';
        this.doc.getElementById('lbPause').style.display = 'none';
    }

    $('#lbImageWrapper .pois').remove();
    this.initZoom = false;
    if ($(imageLink).find('img.inline_zoom').length > 0) {
        this.initZoom = true;
    }

    if (this.isLyteframe) {
        this.changeContent(this.frameNum);
    } else {
        if (this.isSlideshow) {
            this.changeContent(this.slideNum);
        } else {
            this.changeContent(this.imageNum);
        }
    }
        this.doc.getElementById('lbMain').style.lineHeight =  this.doc.getElementById('lbMain').offsetHeight + 'px';
};
LyteBox.prototype.changeContent = function (imageNum) {
    if (this.isSlideshow) {
        for (var i = 0; i < this.slideshowIDCount; i++) {
            window.clearTimeout(this.slideshowIDArray[i]);
        }
    }
    this.activeImage = this.activeSlide = this.activeFrame = imageNum;
    if (!this.outerBorder) {
        this.doc.getElementById('lbOuterContainer').style.border = 'none';
        this.doc.getElementById('lbDetailsContainer').style.border = 'none';
    } else {
        this.doc.getElementById('lbOuterContainer').style.borderBottom = '';
        this.doc.getElementById('lbOuterContainer').setAttribute((this.ie ? 'className' : 'class'), this.theme);
    }
    this.doc.getElementById('lbLoadingWrapper').style.display = '';
    this.doc.getElementById('lbImage').style.display = 'none';
    this.doc.getElementById('lbIframe').style.display = 'none';
    this.doc.getElementById('lbPrev').style.display = 'none';
    this.doc.getElementById('lbNext').style.display = 'none';
    this.doc.getElementById('lbIframeContainer').style.display = 'none';
    this.doc.getElementById('lbDetailsContainer').style.visibility = 'hidden';
    //this.doc.getElementById('lbDetailsContainer').style.display = 'none';
    this.doc.getElementById('lbNumberDisplay').style.display = 'none';
    if (this.navType == 2 || this.isLyteframe) {
        object = this.doc.getElementById('lbNavDisplay');
        //object.innerHTML = '<b id="lbSpacer" class="' + this.theme + '"></b><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td width="30%" align="left" style="padding-left:20px;"><span id="lbPrev2_Off" style="display: none;" class="' + this.theme + '">&nbsp;</span><a href="#" id="lbPrev2" class="' + this.theme + '" style="display: none;">'+lprev()+'</a></td><td style="text-align:center;">' + eval(this.activeSlide + 1) + " "+lof()+" " + this.imageArray.length+'</td><td width="30%" align="right" style="padding-right:20px;"><span id="lbNext2_Off" style="display: none;" class="' + this.theme + '">&nbsp;</span><a href="#" id="lbNext2" class="' + this.theme + '" style="display: none;">'+lnext()+'</a></td></tr></table>';
        object.innerHTML = '<b id="lbSpacer" class="' + this.theme + '"></b><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td width="30%" align="left"><span id="lbPrev2_Off" style="display: none;" class="' + this.theme + '">&nbsp;</span><a href="#" id="lbPrev2" class="' + this.theme + '" style="display: none;">' + lprev() + '</a></td><td style="text-align:center;">' + eval(this.activeSlide + 1) + " " + lof() + " " + this.imageArray.length + '</td><td width="30%" align="right"><span id="lbNext2_Off" style="display: none;" class="' + this.theme + '">&nbsp;</span><a href="#" id="lbNext2" class="' + this.theme + '" style="display: none;">' + lnext() + '</a></td></tr></table>';
        //<tr><td colspan="3"><span id="copyspan">'+lcopy()+'</span></td></tr><span id="lbNumberDisplay"></span>
        object.style.display = 'none';
    }

    if (this.isLyteframe) {
        var iframe = myLytebox.doc.getElementById('lbIframe');
        var styles = this.frameArray[this.activeFrame][2];
        var aStyles = styles.split(';');
        for (var i = 0; i < aStyles.length; i++) {
            if (aStyles[i].indexOf('width:') >= 0) {
                var w = aStyles[i].replace('width:', '');
                iframe.width = w.trim();
            } else if (aStyles[i].indexOf('height:') >= 0) {
                var h = aStyles[i].replace('height:', '');
                iframe.height = h.trim();
            } else if (aStyles[i].indexOf('scrolling:') >= 0) {
                var s = aStyles[i].replace('scrolling:', '');
                iframe.scrolling = s.trim();
            } else if (aStyles[i].indexOf('border:') >= 0) {
                // Not implemented yet, as there are cross-platform issues with setting the border (from a GUI standpoint)
                //var b = aStyles[i].replace('border:', '');
                //iframe.style.border = b.trim();
            }
        }
        this.resizeContainer(parseInt(iframe.width), parseInt(iframe.height));
    } else {
        imgPreloader = new Image();
        var contWidth = 1190;
        var contHeight = 840;
        var pagesize = myLytebox.getPageSize();
        // Get image source
        var origImgUrl = (myLytebox.isSlideshow ? myLytebox.slideArray[myLytebox.activeSlide][0] : myLytebox.imageArray[myLytebox.activeImage][0]);
        var imgSrc = origImgUrl;
        //var imgSrcSet = null;
        var contentImageWrraper = (myLytebox.isSlideshow ? myLytebox.slideArray[myLytebox.activeSlide][2] : myLytebox.imageArray[myLytebox.activeImage][2]);
        if ($(contentImageWrraper).find("img")) {
            imgSrc = $(contentImageWrraper).find("img").attr('src');
            //imgSrcSet = $(contentImageWrraper).find("img").attr('srcset');
//            if (pagesize[2] > 1280 && imgSrc) {
            if (imgSrc) {
                // Src: files/images/__cache/content/Daimler/applications/05_presentations/PPT_Abb_00.png.jpg
                //      protected-file/10474/content?policy=$2y$08$FRYG80ONjB2oS.eTP1Ay4eJmWZzlynBtGma9XwW3KcF8TVeS/EKVO
                if (imgSrc.indexOf('protected-file/') >= 0) {
                    strSearch = '/content';
                    strReplace = '/zoom';
                } else {
                    strSearch = '/__cache/content';
                    strReplace = '/__cache/zoom';
                }
                imgSrc = imgSrc.split(strSearch).join(strReplace);
                //imgSrcSet = imgSrcSet.split(strSearch).join(strReplace);
            }
        }

        imgPreloader.onload = function () {
            var imageTitle = (myLytebox.isSlideshow ? myLytebox.slideArray[myLytebox.activeSlide][1] : myLytebox.imageArray[myLytebox.activeImage][1]);
            var imageLink = (myLytebox.isSlideshow ? myLytebox.slideArray[myLytebox.activeSlide][2] : myLytebox.imageArray[myLytebox.activeImage][2]);

            var imageWidth = imgPreloader.width;
            var imageHeight = imgPreloader.height;
            if (myLytebox.autoResize) {
                var imageCont = myLytebox.doc.getElementById('lbImageContainer');
                //imageCont.style.marginLeft = myLytebox.borderSize + 'px';
                var imgc_style = imageCont.currentStyle || window.getComputedStyle(imageCont);

                var detc = myLytebox.doc.getElementById('lbDetailsContainer');
                var detc_style = detc.currentStyle || window.getComputedStyle(detc);

                var x = ($('#lbMain').width() - 120) * 69.56/100;
//                if (imageTitle == '' && !myLytebox.initZoom && (typeof ($(imageLink).find('img').attr('data-hotspot')) == 'undefined' || $(imageLink).find('img').attr('data-hotspot').length <= 0)) {
//                    //var x = parseInt($('#lbOuterContainer').width(), 10) - (1 + parseInt($('#lbImageContainer').css('margin-left')) + parseInt($('#lbImageContainer').css('margin-right')));
//                } else {
//                    var x = ($('#lbMain').width() * 69.56/100);
//                    //var x = parseInt($('#lbOuterContainer').width(), 10) - 1 - parseInt($('#lbImageContainer').css('margin-left')) - parseInt($('#lbImageContainer').css('margin-right'));
//                }
               //var y = parseInt($('#lbOuterContainer').height(), 10) - parseInt($('#lbImageContainer').css('margin-top')) - parseInt($('#lbImageContainer').css('margin-bottom'));
               var y = $('#lbMain').height() - 120;
               if ($(myLytebox.imageArray[myLytebox.activeImage][2]).closest('.galCropper').hasClass('first_image') || $(myLytebox.imageArray[myLytebox.activeImage][2]).closest('.image_wrapper').hasClass('first_image')) {
                    contHeight = 793;
                }

                if (pagesize[2] <= 1280) {
                    myLytebox.borderSize = 49;
                    contWidth = 740;
                    contHeight = 520;
                    if ($(myLytebox.imageArray[myLytebox.activeImage][2]).closest('.galCropper').hasClass('first_image') || $(myLytebox.imageArray[myLytebox.activeImage][2]).closest('.image_wrapper').hasClass('first_image')) {
                        contHeight = 493;
                    }
                }
                if (contWidth > x) {
                    contHeight = Math.round(contHeight * (x / contWidth));
                    contWidth = x;
                    if (contHeight > y) {
                        contWidth = Math.round(contWidth * (y / contHeight));
                        contHeight = y;
                    }
                } else if (contHeight > y) {
                    contWidth = Math.round(contWidth * (y / contHeight));
                    contHeight = y;
                    if (contWidth > x) {
                        contHeight = Math.round(contHeight * (x / contWidth));
                        contWidth = x;
                    }
                }

                if (imageWidth < contWidth && imageHeight < contHeight) {
                } else if (imageWidth / imageHeight > contWidth / contHeight) {
                    oldImageWidth = imageWidth;
                    imageWidth = contWidth;
                    imageHeight = parseInt((imageHeight / oldImageWidth) * imageWidth);
                } else {
                    oldImageHeight = imageHeight;
                    imageHeight = contHeight;
                    imageWidth = parseInt((imageWidth / oldImageHeight) * imageHeight);
                }
            }

            if (contHeight > imageHeight) {
                contHeight = imageHeight;
            }

            if (imageTitle == '' && !myLytebox.initZoom && (typeof ($(imageLink).find('img').attr('data-hotspot')) == 'undefined' || $(imageLink).find('img').attr('data-hotspot').length <= 0)) {
//                $('#lbDetailsContainer').css('width', '0px');
                contWidth = imageWidth;
                //	imageCont.style.marginLeft = (myLytebox.borderSize + parseInt(detc_style.width,10)/ 2 - 1)+'px';
            } else {
//                $('#lbDetailsContainer').css('width', '');
                //	imageCont.style.marginLeft = myLytebox.borderSize+'px';
            }

            imageCont.style.width = imageWidth + 'px';
//            if (myLytebox.imageArray.length > 1) {
//                $('#lbMain').css('top', (myLytebox.getPageScroll() + (pagesize[3] - (contHeight + 2 * myLytebox.borderSize)) / 2) + "px");
//            }
            imageCont.style.height = imageHeight + 'px';
            imageCont.style.lineHeight = imageHeight + 'px';

            //$('#lbMain').height(contHeight + 2 * (myLytebox.borderSize));
            //

            var lbImage = myLytebox.doc.getElementById('lbImage');
            lbImage.src = imgSrc;
//            if(imgSrcSet){
//                lbImage.srcset = imgSrcSet;
//            }

            $('#lbOuterContainer').height(imageHeight);
            $('#lbDetailsContainer').height(imageHeight);
            $('#lbDetailsContainer').css('line-height', imageHeight + 'px');
            $('#lbOuterContainer').width(imageWidth);
            lbImage.width = imageWidth;
            lbImage.height = imageHeight;
                        myLytebox.resizeContainer(contWidth, contHeight);
            if (myLytebox.initZoom) {
                $('#lbImageWrapper').trigger('zoom.destroy');
                $('#lbLoadingWrapper').css('z-index', 100).show();
                $('#lbImageWrapper').zoom({url: origImgUrl, callback: function () {
                    $('#lbLoadingWrapper').css('z-index', 11).hide();
                    }});
            } else {
                $('#lbImageWrapper').trigger('zoom.destroy');
            }

        };
        imgPreloader.src = imgSrc;
    }
};
LyteBox.prototype.resizeContainer = function (imgWidth, imgHeight) {
    this.wCur = this.doc.getElementById('lbOuterContainer').offsetWidth;
    this.hCur = this.doc.getElementById('lbOuterContainer').offsetHeight;
    //this.doc.getElementById('lbOuterContainer').style.lineHeight = this.hCur + 'px';
    this.xScale = ((imgWidth + (this.borderSize * 2)) / this.wCur) * 100;
    this.yScale = ((imgHeight + (this.borderSize * 2)) / this.hCur) * 100;

    var imageTitle = (this.isSlideshow ? this.slideArray[this.activeSlide][1] : ((this.imageArray[this.activeImage]) ? this.imageArray[this.activeImage][1] : ''));
    var imageLink = (this.isSlideshow ? this.slideArray[this.activeSlide][2] : ((this.imageArray[this.activeImage]) ? this.imageArray[this.activeImage][2] : ''));
    if (imageTitle == '' && !this.initZoom) {
        var wDiff = (this.wCur - this.borderSize * 2) - imgWidth - 60;
    } else {
        var wDiff = (this.wCur - this.borderSize * 2) - imgWidth;
    }
    var hDiff = (this.hCur - this.borderSize * 2) - imgHeight;
//    if (!(hDiff == 0)) {
//        this.hDone = false;
//        this.maxHeight = imgHeight + this.borderSize * 2;
//        this.resizeH('lbOuterContainer', this.hCur, imgHeight + this.borderSize * 2, this.getPixelRate(this.hCur, imgHeight));
//    } else {
        this.hDone = true;
//    }
//    if (!(wDiff == 0)) {
//        this.wDone = false;
//
//        if (imageTitle == '' && !this.initZoom && (typeof ($(imageLink).find('img').attr('data-hotspot')) == 'undefined' || $(imageLink).find('img').attr('data-hotspot').length <= 0)) {
//            this.maxWidth = imgWidth + this.borderSize * 2 + 60;
//            this.resizeW('lbOuterContainer', this.wCur, imgWidth + this.borderSize * 2 + 60, this.getPixelRate(this.wCur, imgWidth + 60));
//        } else {
//            this.maxWidth = imgWidth + this.borderSize * 2;
//            this.resizeW('lbOuterContainer', this.wCur, imgWidth + this.borderSize * 2, this.getPixelRate(this.wCur, imgWidth));
//        }
//    } else {
        this.wDone = true;
//    }
    if ((hDiff == 0) && (wDiff == 0)) {
        if (this.ie) {
            this.pause(250);
        } else {
            this.pause(100);
        }
    }
    //this.doc.getElementById('lbPrev').style.height = imgHeight + "px";
    //this.doc.getElementById('lbNext').style.height = imgHeight + "px";
    //this.doc.getElementById('lbDetailsContainer').style.width = (imgWidth + (this.borderSize * 2) + (this.ie && this.doc.compatMode == "BackCompat" && this.outerBorder ? 2 : 0)) + "px";
    this.showContent();
};
LyteBox.prototype.showContent = function () {
    if (this.wDone && this.hDone) {
        for (var i = 0; i < this.showContentTimerCount; i++) {
            window.clearTimeout(this.showContentTimerArray[i]);
        }
        if (this.outerBorder) {
            this.doc.getElementById('lbOuterContainer').style.borderBottom = 'none';
        }
        if (!myLytebox.initZoom && !this.isLyteframe) {
            this.doc.getElementById('lbLoadingWrapper').style.display = 'none';
        }
        if (this.isLyteframe) {
            this.doc.getElementById('lbIframe').style.display = '';
            this.appear('lbIframe', (this.doAnimations ? 0 : 100));
        } else {
            this.doc.getElementById('lbImage').style.display = '';
            this.appear('lbImage', (this.doAnimations ? 0 : 100));
            this.preloadNeighborImages();
        }
        if (this.isSlideshow) {
            if (this.activeSlide == (this.slideArray.length - 1)) {
                if (this.autoEnd) {
                    this.slideshowIDArray[this.slideshowIDCount++] = setTimeout("myLytebox.end('slideshow')", this.slideInterval);
                }
            } else {
                if (!this.isPaused) {
                    this.slideshowIDArray[this.slideshowIDCount++] = setTimeout("myLytebox.changeContent(" + (this.activeSlide + 1) + ")", this.slideInterval);
                }
            }
            this.doc.getElementById('lbHoverNav').style.display = (this.showNavigation && this.navType == 1 ? '' : 'none');
            this.doc.getElementById('lbClose').style.display = (this.showClose ? '' : 'none');
            this.doc.getElementById('lbDetails').style.display = (this.showDetails ? '' : 'none');
            this.doc.getElementById('lbPause').style.display = (this.showPlayPause && !this.isPaused ? '' : 'none');
            this.doc.getElementById('lbPlay').style.display = (this.showPlayPause && !this.isPaused ? 'none' : '');
            this.doc.getElementById('lbNavDisplay').style.display = (this.showNavigation && this.navType == 2 ? '' : 'none');
        } else {
            this.doc.getElementById('lbHoverNav').style.display = (this.navType == 1 && !this.isLyteframe ? '' : 'none');
            if ((this.navType == 2 && !this.isLyteframe && this.imageArray.length > 1) || (this.frameArray.length > 1 && this.isLyteframe)) {
                this.doc.getElementById('lbNavDisplay').style.display = '';
            } else {
                this.doc.getElementById('lbNavDisplay').style.display = 'none';
            }
            this.doc.getElementById('lbClose').style.display = '';
            this.doc.getElementById('lbDetails').style.display = '';
            this.doc.getElementById('lbPause').style.display = 'none';
            this.doc.getElementById('lbPlay').style.display = 'none';

            if (!this.isLyteframe) {
                var imageLink = myLytebox.imageArray[myLytebox.activeImage][2];

                if (typeof ($(imageLink).find('img').attr('data-hotspot')) != 'undefined' && $(imageLink).find('img').attr('data-hotspot').length > 0) {
                    var hst_json = JSON.parse($(imageLink).find('img').attr('data-hotspot'));
                    var html = '<div id="LE-pois-0" class="pois">';
                    for (var i = 0; i < hst_json.points.length; i++) {
                        html += '<div class="poi ' + hst_json.points[i].pos + '" style="left: ' + hst_json.points[i].left + '; top: ' + hst_json.points[i].top + ';">' + (i + 1) + '</div>';
                    }
                    html += '</div>';
                    $('#lbImageWrapper').append(html);

                    var caption_html = '<h1>' + js_localize['hotspot.default_text'] + '</h1>' + hst_json.description;

                    $('#lbCaption').html(caption_html);
//                    if ($('#lbCaption').outerHeight() + (2 * this.borderSize) > $('#lbMain').height()) {
//                        //$('#lbMain').height($('#lbCaption').outerHeight() + (2*this.borderSize));
//                    }

                    myLytebox.imageArray[myLytebox.activeImage][1] = caption_html;
                }
                 $('#lbDetailsContainer').removeClass('zero-width');
                if((!this.initZoom  && this.imageArray[this.activeImage][1] == '' && this.imageArray.length == 1) || this.isLyteframe){
                    $('#lbOuterContainer').addClass('full-width');
                    $('#lbDetailsContainer').addClass('zero-width');
                }else if(this.imageArray.length > 1){
                    $('#lbOuterContainer').addClass('max-width');
                }
            }
        }
        this.doc.getElementById('lbWrapper').style.display = (this.isLyteframe ? 'none' : '');
        this.doc.getElementById('lbBottomNav').style.display = (this.isLyteframe ? 'none' : '');
        this.doc.getElementById('lbIframeContainer').style.display = (this.isLyteframe ? '' : 'none');
        if(this.isLyteframe){
            this.doc.getElementById('lbOuterContainer').style.width = '100%';
            this.doc.getElementById('lbOuterContainer').style.height ='100%';
        }
        try {
            if (this.isLyteframe) {
                var iframeEl = this.doc.getElementById('lbIframe');
                iframeEl.onload = function () {
                    $('#lbLoadingWrapper').hide();
                    console.log('iframe loaded!');
                };

                iframeEl.src = this.frameArray[this.activeFrame][0];

            }
        } catch (e) {
        }
    } else {

        this.showContentTimerArray[this.showContentTimerCount++] = setTimeout("myLytebox.showContent()", 200);
    }
    //$('#lbImageWrapper').trigger('showContent');
};
LyteBox.prototype.updateDetails = function () {
    var object = this.doc.getElementById('lbCaption');
    var sTitle = (this.isSlideshow ? this.slideArray[this.activeSlide][1] : (this.isLyteframe ? this.frameArray[this.activeFrame][1] : this.imageArray[this.activeImage][1]));
    object.style.display = '';
    var nbps = "<p>" + Array(90).join("&nbsp;") + "</p>";
    object.innerHTML = (sTitle == null ? '' : sTitle) + nbps;
    $(object).removeClass('bottom');
     $('#lbDetailsContainer').removeClass('bottom');

    if(this.imageArray.length > 1){
        $(object).addClass('bottom');
        $('#lbDetailsContainer').addClass('bottom');
    }
    if (this.initZoom) {
        $(object).addClass('inline_zoom');
        object.innerHTML = object.innerHTML + '<p id="inline_zoom_addition">' + js_localize['inline_zoom.zoom_text'] + '</p>';
        //$('#inline_zoom_addition').css('left', ($('#lbOuterContainer').width() + parseInt($('#lbDetailsData').css('padding-left'), 10)) + 'px');
    } else {
        $(object).removeClass('inline_zoom');
    }

    if ($(object).outerHeight() + (2 * this.borderSize) > $('#lbMain').height()) {
        //$('#lbMain').height($(object).outerHeight() + (2*this.borderSize));
    }
    //if(object.innerHTML.length > 0){
    object.style.display = 'block';
    this.doc.getElementById('lbNavDisplay').style.borderTopWidth = '1px';
    //}else{
    //     object.style.display =  'none';
    //    this.doc.getElementById('lbNavDisplay').style.borderTopWidth = '0px';
    // }


    this.updateNav();
    var imageCounter =  this.doc.getElementById('imageCounter');
    imageCounter.style.display = 'none';
    this.doc.getElementById('lbDetailsContainer').style.visibility = '';
    this.doc.getElementById('lbDetailsContainer').style.display = '';
    object = this.doc.getElementById('lbNumberDisplay');
    if (this.isSlideshow && this.slideArray.length > 1) {
        object.style.display = '';
        var nav_html = '';
        for (var i = 0; i < this.slideArray.length; i++) {
            nav_html += '<span class="slider_block ' + (i == this.activeSlide ? 'active' : '') + '" onClick="myLytebox.changeContent(' + i + ');"><span>';
        }
        object.innerHTML = "" + eval(this.activeSlide + 1) + " " + lof() + " " + this.slideArray.length;
        this.doc.getElementById('lbNavDisplay').style.display = (this.navType == 2 && this.showNavigation ? '' : 'none');
    } else if (this.imageArray.length > 1 && !this.isLyteframe) {
        object.style.display = 'none';
        imageCounter.style.display = '';
        //if(typeof(lof) == "undefined"){
        var nav_html = '<span class="curentCount">' + (this.activeImage + 1) +'</span>' + '<span class="count"> / '+this.imageArray.length+ '</span>';
//        for (var i = 0; i < this.imageArray.length; i++) {
//            nav_html += '<span class="slider_block ' + (i == this.activeImage ? 'active' : '') + '" onClick="myLytebox.changeContent(' + i + ');"></span>';
//        }
        imageCounter.innerHTML = nav_html;
//        object.style.marginLeft = '-' + (object.offsetWidth - 20) / 2 + 'px';
        this.doc.getElementById('lbNavDisplay').style.display = (this.navType == 2 ? '' : 'none');
        //}
    } else if (this.frameArray.length > 1 && this.isLyteframe) {
        object.style.display = '';
        object.innerHTML = "Page " + eval(this.activeFrame + 1) + " " + lof() + " " + this.frameArray.length;
        this.doc.getElementById('lbNavDisplay').style.display = '';
    } else {
        this.doc.getElementById('lbNavDisplay').style.display = 'none';
    }
    //alert(object.innerHTML.length);
    // show / hide the details container
    //if(object.innerHTML.length > 1){ // && this.doc.getElementById('lbNavDisplay').style.display != 'none'){
    //if(object.innerHTML.length > 1 || this.doc.getElementById('lbCaption').style.display != 'none'){
    if (this.doc.getElementById('lbNavDisplay').style.display != 'none' || this.doc.getElementById('lbCaption').style.display != 'none') {
        if (!this.isLyteframe) {
            this.appear('lbDetailsContainer', (this.doAnimations ? 0 : 100));
        } else {
            this.doc.getElementById('lbDetailsContainer').style.display = 'none';
        }
    } else {
        this.doc.getElementById('lbDetailsContainer').style.display = 'none';
    }
};
LyteBox.prototype.updateNav = function () {
    if (this.isSlideshow) {
        if (this.activeSlide != 0) {
            var object = (this.navType == 2 ? this.doc.getElementById('lbPrev2') : this.doc.getElementById('lbPrev'));
            object.style.display = '';
            this.addInlineEvent(object, 'onclick', function () {
                if (myLytebox.pauseOnPrevClick) {
                    myLytebox.togglePlayPause("lbPause", "lbPlay");
                }
                myLytebox.changeContent(myLytebox.activeSlide - 1);
                return false;
            });
        } else {
            if (this.navType == 2) {
                this.doc.getElementById('lbPrev2_Off').style.display = '';
            }
        }
        if (this.activeSlide != (this.slideArray.length - 1)) {
            var object = (this.navType == 2 ? this.doc.getElementById('lbNext2') : this.doc.getElementById('lbNext'));
            object.style.display = '';
            this.addInlineEvent(object, 'onclick', function () {
                if (myLytebox.pauseOnNextClick) {
                    myLytebox.togglePlayPause("lbPause", "lbPlay");
                }
                myLytebox.changeContent(myLytebox.activeSlide + 1);
                return false;
            });
        } else {
            if (this.navType == 2) {
                this.doc.getElementById('lbNext2_Off').style.display = '';
            }
        }
    } else if (this.isLyteframe) {
        if (this.activeFrame != 0) {
            var object = this.doc.getElementById('lbPrev2');
            object.style.display = '';
            this.addInlineEvent(object, 'onclick', function () {
                myLytebox.changeContent(myLytebox.activeFrame - 1);
                return false;
            });
        } else {
            this.doc.getElementById('lbPrev2_Off').style.display = '';
        }
        if (this.activeFrame != (this.frameArray.length - 1)) {
            var object = this.doc.getElementById('lbNext2');
            object.style.display = '';
            this.addInlineEvent(object, 'onclick', function () {
                myLytebox.changeContent(myLytebox.activeFrame + 1);
                return false;
            });
        } else {
            this.doc.getElementById('lbNext2_Off').style.display = '';
        }
    } else {
        if (this.activeImage != 0) {
            var object = (this.navType == 2 ? this.doc.getElementById('lbPrev2') : this.doc.getElementById('lbPrev'));
            object.style.display = '';
            this.addInlineEvent(object, 'onclick', function () {
                myLytebox.changeContent(myLytebox.activeImage - 1);
                return false;
            });
        } else {
            if (this.navType == 2) {
                this.doc.getElementById('lbPrev2_Off').style.display = '';
            }
        }
        if (this.activeImage != (this.imageArray.length - 1)) {
            var object = (this.navType == 2 ? this.doc.getElementById('lbNext2') : this.doc.getElementById('lbNext'));
            object.style.display = '';
            this.addInlineEvent(object, 'onclick', function () {
                myLytebox.changeContent(myLytebox.activeImage + 1);
                return false;
            });
        } else {
            if (this.navType == 2) {
                this.doc.getElementById('lbNext2_Off').style.display = '';
            }
        }
    }
    this.enableKeyboardNav();
};
LyteBox.prototype.enableKeyboardNav = function () {
    document.onkeydown = this.keyboardAction;
};
LyteBox.prototype.disableKeyboardNav = function () {
    document.onkeydown = '';
};
LyteBox.prototype.keyboardAction = function (e) {
    var keycode = key = escape = null;
    keycode = (e == null) ? event.keyCode : e.which;
    key = String.fromCharCode(keycode).toLowerCase();
    escape = (e == null) ? 27 : e.DOM_VK_ESCAPE;
    if ((key == 'x') || (key == 'c') || (keycode == escape)) {
        myLytebox.end();
    } else if ((key == 'p') || (keycode == 37)) {
        if (myLytebox.isSlideshow) {
            if (myLytebox.activeSlide != 0) {
                myLytebox.disableKeyboardNav();
                myLytebox.changeContent(myLytebox.activeSlide - 1);
            }
        } else if (myLytebox.isLyteframe) {
            if (myLytebox.activeFrame != 0) {
                myLytebox.disableKeyboardNav();
                myLytebox.changeContent(myLytebox.activeFrame - 1);
            }
        } else {
            if (myLytebox.activeImage != 0) {
                myLytebox.disableKeyboardNav();
                myLytebox.changeContent(myLytebox.activeImage - 1);
            }
        }
    } else if ((key == 'n') || (keycode == 39)) {
        if (myLytebox.isSlideshow) {
            if (myLytebox.activeSlide != (myLytebox.slideArray.length - 1)) {
                myLytebox.disableKeyboardNav();
                myLytebox.changeContent(myLytebox.activeSlide + 1);
            }
        } else if (myLytebox.isLyteframe) {
            if (myLytebox.activeFrame != (myLytebox.frameArray.length - 1)) {
                myLytebox.disableKeyboardNav();
                myLytebox.changeContent(myLytebox.activeFrame + 1);
            }
        } else {
            if (myLytebox.activeImage != (myLytebox.imageArray.length - 1)) {
                myLytebox.disableKeyboardNav();
                myLytebox.changeContent(myLytebox.activeImage + 1);
            }
        }
    }
};
LyteBox.prototype.preloadNeighborImages = function () {
    if (this.isSlideshow) {
        if ((this.slideArray.length - 1) > this.activeSlide) {
            preloadNextImage = new Image();
            preloadNextImage.src = this.slideArray[this.activeSlide + 1][0];
        }
        if (this.activeSlide > 0) {
            preloadPrevImage = new Image();
            preloadPrevImage.src = this.slideArray[this.activeSlide - 1][0];
        }
    } else {
        if ((this.imageArray.length - 1) > this.activeImage) {
            preloadNextImage = new Image();
            preloadNextImage.src = this.imageArray[this.activeImage + 1][0];
        }
        if (this.activeImage > 0) {
            preloadPrevImage = new Image();
            preloadPrevImage.src = this.imageArray[this.activeImage - 1][0];
        }
    }
};
LyteBox.prototype.togglePlayPause = function (hideID, showID) {
    if (this.isSlideshow && hideID == "lbPause") {
        for (var i = 0; i < this.slideshowIDCount; i++) {
            window.clearTimeout(this.slideshowIDArray[i]);
        }
    }
    this.doc.getElementById(hideID).style.display = 'none';
    this.doc.getElementById(showID).style.display = '';
    if (hideID == "lbPlay") {
        this.isPaused = false;
        if (this.activeSlide == (this.slideArray.length - 1)) {
            this.end();
        } else {
            this.changeContent(this.activeSlide + 1);
        }
    } else {
        this.isPaused = true;
    }
};
LyteBox.prototype.end = function (caller) {
    $('html').css('overflow', "visible");
    if (this.bodyScroll) {
        $(window).scrollTop(this.bodyScroll);
        this.bodyScroll = null;
    }
    var closeClick = (caller == 'slideshow' ? false : true);
    if (this.isSlideshow && this.isPaused && !closeClick) {
        return;
    }
    this.disableKeyboardNav();
    this.doc.getElementById('lbMain').style.display = 'none';
    this.fade('lbOverlay', (this.doAnimations ? this.maxOpacity : 0));
    this.toggleSelects('visible');
    if (this.hideFlash) {
        this.toggleFlash('visible');
    }
    if (this.isSlideshow) {
        for (var i = 0; i < this.slideshowIDCount; i++) {
            window.clearTimeout(this.slideshowIDArray[i]);
        }
    }
    if (this.isLyteframe) {
        $('#lbIframe').attr('src', '');
        this.initialize();
    }

    $('#lbImageWrapper').trigger('zoom.destroy');
};
LyteBox.prototype.checkFrame = function () {
    //if (window.parent.frames[window.name] && (parent.document.getElementsByTagName('frameset').length <= 0)) {
    //	this.isFrame = true;
    //	this.lytebox = "window.parent." + window.name + ".myLytebox";
    //	this.doc = parent.document;
    //} else {
    this.isFrame = false;
    this.lytebox = "myLytebox";
    this.doc = document;
    //}
};
LyteBox.prototype.getPixelRate = function (cur, img) {
    var diff = (img > cur) ? img - cur : cur - img;
    if (diff >= 0 && diff <= 100) {
        return 10;
    }
    if (diff > 100 && diff <= 200) {
        return 15;
    }
    if (diff > 200 && diff <= 300) {
        return 20;
    }
    if (diff > 300 && diff <= 400) {
        return 25;
    }
    if (diff > 400 && diff <= 500) {
        return 30;
    }
    if (diff > 500 && diff <= 600) {
        return 35;
    }
    if (diff > 600 && diff <= 700) {
        return 40;
    }
    if (diff > 700) {
        return 45;
    }
};
LyteBox.prototype.appear = function (id, opacity) {
//    if (opacity > 0 && (id == 'lbDetailsContainer' || id == 'lbIframe')) {
//        var main = this.doc.getElementById('lbMain');
//        var pagesize = myLytebox.getPageSize();
////        main.style.left = Math.floor((((pagesize[2] - main.offsetWidth) / 2) + $('html').scrollLeft())) + "px";
////        main.style.top = (this.getPageScroll() + (pagesize[3] - main.offsetHeight) / 2) + "px";
//    }
    var object = this.doc.getElementById(id).style;
    object.opacity = (opacity / 100);
    object.MozOpacity = (opacity / 100);
    object.KhtmlOpacity = (opacity / 100);
    object.filter = "alpha(opacity=" + (opacity + 10) + ")";
    if (opacity == 100 && (id == 'lbImage' || id == 'lbIframe')) {
        try {
            object.removeAttribute("filter");
        } catch (e) {
        }	/* Fix added for IE Alpha Opacity Filter bug. */
        this.updateDetails();
    } else if (opacity >= this.maxOpacity && id == 'lbOverlay') {
        for (var i = 0; i < this.overlayTimerCount; i++) {
            window.clearTimeout(this.overlayTimerArray[i]);
        }
        return;
    } else if (opacity >= 100 && id == 'lbDetailsContainer') {
        try {
            object.removeAttribute("filter");
        } catch (e) {
        }	/* Fix added for IE Alpha Opacity Filter bug. */
        for (var i = 0; i < this.imageTimerCount; i++) {
            window.clearTimeout(this.imageTimerArray[i]);
        }
        this.doc.getElementById('lbOverlay').style.height = this.getPageSize()[1] + "px";
    } else {
        if (id == 'lbOverlay') {
            this.overlayTimerArray[this.overlayTimerCount++] = setTimeout("myLytebox.appear('" + id + "', " + (opacity + 20) + ")", 1);
        } else {
            this.imageTimerArray[this.imageTimerCount++] = setTimeout("myLytebox.appear('" + id + "', " + (opacity + 10) + ")", 1);
        }
    }
};
LyteBox.prototype.fade = function (id, opacity) {
    var object = this.doc.getElementById(id).style;
    object.opacity = (opacity / 100);
    object.MozOpacity = (opacity / 100);
    object.KhtmlOpacity = (opacity / 100);
    object.filter = "alpha(opacity=" + opacity + ")";
    if (opacity <= 0) {
        try {
            object.display = 'none';
        } catch (err) {
        }
    } else if (id == 'lbOverlay') {
        this.overlayTimerArray[this.overlayTimerCount++] = setTimeout("myLytebox.fade('" + id + "', " + (opacity - 20) + ")", 1);
    } else {
        this.timerIDArray[this.timerIDCount++] = setTimeout("myLytebox.fade('" + id + "', " + (opacity - 10) + ")", 1);
    }
};
LyteBox.prototype.resizeW = function (id, curW, maxW, pixelrate, speed) {
    if (this.maxWidth == maxW) {
        if (!this.hDone) {
            this.resizeWTimerArray[this.resizeWTimerCount++] = setTimeout("myLytebox.resizeW('" + id + "', " + curW + ", " + maxW + ", " + pixelrate + ")", 100);
            return;
        }
        var object = this.doc.getElementById(id);
        var timer = speed ? speed : (this.resizeDuration / 2);
        var newW = (this.doAnimations ? curW : maxW);
        object.style.width = (newW) + "px";
//        if (id == 'lbOuterContainer') {
//            var main = this.doc.getElementById('lbMain');
//            var pagesize = myLytebox.getPageSize();
//            main.style.left = (((pagesize[2] - main.offsetWidth) / 2) + $('html').scrollLeft()) + "px";
//        }
        if (newW < maxW) {
            newW += (newW + pixelrate >= maxW) ? (maxW - newW) : pixelrate;
        } else if (newW > maxW) {
            newW -= (newW - pixelrate <= maxW) ? (newW - maxW) : pixelrate;
        }
        this.resizeWTimerArray[this.resizeWTimerCount++] = setTimeout("myLytebox.resizeW('" + id + "', " + newW + ", " + maxW + ", " + pixelrate + ", " + (timer + 0.02) + ")", timer + 0.02);
        if (parseInt(object.style.width) == maxW) {
            this.wDone = true;
            for (var i = 0; i < this.resizeWTimerCount; i++) {
                window.clearTimeout(this.resizeWTimerArray[i]);
            }
        }
    }
};
LyteBox.prototype.resizeH = function (id, curH, maxH, pixelrate, speed) {
    if (this.maxHeight == maxH) {
        var timer = speed ? speed : (this.resizeDuration / 2);
        var object = this.doc.getElementById(id);
        var newH = (this.doAnimations ? curH : maxH);
        object.style.height = (newH) + "px";
        if (newH < maxH) {
            newH += (newH + pixelrate >= maxH) ? (maxH - newH) : pixelrate;
        } else if (newH > maxH) {
            newH -= (newH - pixelrate <= maxH) ? (newH - maxH) : pixelrate;
        }
        this.resizeHTimerArray[this.resizeHTimerCount++] = setTimeout("myLytebox.resizeH('" + id + "', " + newH + ", " + maxH + ", " + pixelrate + ", " + (timer + .02) + ")", timer + .02);
        if (parseInt(object.style.height) == maxH) {
            this.hDone = true;
            for (var i = 0; i < this.resizeHTimerCount; i++) {
                window.clearTimeout(this.resizeHTimerArray[i]);
            }
        }
    }
};
LyteBox.prototype.getPageScroll = function () {
    if (self.pageYOffset) {
        return this.isFrame ? parent.pageYOffset : self.pageYOffset;
    } else if (this.doc.documentElement && this.doc.documentElement.scrollTop) {
        return this.doc.documentElement.scrollTop;
    } else if (document.body) {
        return this.doc.body.scrollTop;
    }
};
LyteBox.prototype.getPageSize = function () {
    var xScroll, yScroll, windowWidth, windowHeight;
    if (window.innerHeight && window.scrollMaxY) {
        xScroll = this.doc.scrollWidth;
        yScroll = (this.isFrame ? parent.innerHeight : self.innerHeight) + (this.isFrame ? parent.scrollMaxY : self.scrollMaxY);
    } else if (this.doc.body.scrollHeight > this.doc.body.offsetHeight) {
        xScroll = this.doc.body.scrollWidth;
        yScroll = this.doc.body.scrollHeight;
    } else {
        xScroll = this.doc.getElementsByTagName("html").item(0).offsetWidth;
        yScroll = this.doc.getElementsByTagName("html").item(0).offsetHeight;
        xScroll = (xScroll < this.doc.body.offsetWidth) ? this.doc.body.offsetWidth : xScroll;
        yScroll = (yScroll < this.doc.body.offsetHeight) ? this.doc.body.offsetHeight : yScroll;
    }
    if (self.innerHeight) {
        windowWidth = (this.isFrame) ? parent.innerWidth : $('html').innerWidth();
        windowHeight = (this.isFrame) ? parent.innerHeight : self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) {
        windowWidth = this.doc.documentElement.clientWidth;
        windowHeight = this.doc.documentElement.clientHeight;
    } else if (document.body) {
        windowWidth = this.doc.getElementsByTagName("html").item(0).clientWidth;
        windowHeight = this.doc.getElementsByTagName("html").item(0).clientHeight;
        windowWidth = (windowWidth == 0) ? this.doc.body.clientWidth : windowWidth;
        windowHeight = (windowHeight == 0) ? this.doc.body.clientHeight : windowHeight;
    }
    var pageHeight = (yScroll < windowHeight) ? windowHeight : yScroll;
    var pageWidth = (xScroll < windowWidth) ? windowWidth : xScroll;
    return new Array(pageWidth, pageHeight, windowWidth, windowHeight);
};
LyteBox.prototype.toggleFlash = function (state) {
    var objects = this.doc.getElementsByTagName("object");
    for (var i = 0; i < objects.length; i++) {
        objects[i].style.visibility = (state == "hide") ? 'hidden' : 'visible';
    }
    var embeds = this.doc.getElementsByTagName("embed");
    for (var i = 0; i < embeds.length; i++) {
        embeds[i].style.visibility = (state == "hide") ? 'hidden' : 'visible';
    }
    if (this.isFrame) {
        for (var i = 0; i < parent.frames.length; i++) {
            try {
                objects = parent.frames[i].window.document.getElementsByTagName("object");
                for (var j = 0; j < objects.length; j++) {
                    objects[j].style.visibility = (state == "hide") ? 'hidden' : 'visible';
                }
            } catch (e) {
            }
            try {
                embeds = parent.frames[i].window.document.getElementsByTagName("embed");
                for (var j = 0; j < embeds.length; j++) {
                    embeds[j].style.visibility = (state == "hide") ? 'hidden' : 'visible';
                }
            } catch (e) {
            }
        }
    }
};
LyteBox.prototype.toggleSelects = function (state) {
    var selects = this.doc.getElementsByTagName("select");
    for (var i = 0; i < selects.length; i++) {
        selects[i].style.visibility = (state == "hide") ? 'hidden' : 'visible';
    }
    if (this.isFrame) {
        for (var i = 0; i < parent.frames.length; i++) {
            try {
                selects = parent.frames[i].window.document.getElementsByTagName("select");
                for (var j = 0; j < selects.length; j++) {
                    selects[j].style.visibility = (state == "hide") ? 'hidden' : 'visible';
                }
            } catch (e) {
            }
        }
    }
};
LyteBox.prototype.pause = function (numberMillis) {
    var now = new Date();
    var exitTime = now.getTime() + numberMillis;
    while (true) {
        now = new Date();
        if (now.getTime() > exitTime) {
            return;
        }
    }
};
LyteBox.prototype.addInlineEvent = function (element, eventName, handler) {
    if (!element) {
        return;
    }

    element[eventName] = handler;

    var elementUUID = $(element)
        .generateUUID()
        .getUUID();

    this.boundEventCollection[elementUUID] = { element: element, eventName: eventName };
};
LyteBox.prototype.resizeHandler = function () {
    if (typeof (myLytebox) !== 'undefined') {
        if (myLytebox.activeFrame !== null || myLytebox.activeImage !== null || myLytebox.activeSlide !== null) {
            if (myLytebox.activeFrame !== null) {
                myLytebox.changeContent(myLytebox.activeFrame);
            } else if (myLytebox.activeImage !== null) {
                myLytebox.changeContent(myLytebox.activeImage);
            } else if (myLytebox.activeFrame !== null) {
                activeSlide.changeContent(myLytebox.activeFrame);
            }
        }
    }
};

// Let's try to get rid of this horrific mess once and for all!
LyteBox.prototype.destroy = function () {
    this.doAnimations = false;
    this.end();

    for (prop in this.boundEventCollection) {
        if (this.boundEventCollection.hasOwnProperty(prop)) {
            var eventItem = this.boundEventCollection[prop];

            eventItem.element[eventItem.eventName] = null;
        }
    }

    $('#lbImageWrapper').trigger('zoom.destroy');
    this.disableKeyboardNav();
    window.removeEventListener('load', initLytebox, false);
    window.removeEventListener('resize', LyteBox.prototype.resizeHandler, false);

    for (var i = 0; i < this.AllTimerCollection.length; i++) {
        for (var k = 0; k < this.AllTimerCollection[i].length; k++) {
            clearTimeout(this.AllTimerCollection[i][k]);
        }
    }

    window.myLytebox = null;
    delete window.myLytebox;
};

if (window.addEventListener) {
    window.addEventListener("load", initLytebox, false);
} else if (window.attachEvent) {
    window.attachEvent("onload", initLytebox);
} else {
    window.onload = function () {
        initLytebox();
    }
}

function initLytebox() {
    if (typeof (myLytebox) !== 'undefined'
        && myLytebox !== null) {
        myLytebox.updateLyteboxItems();
    } else {
        myLytebox = new LyteBox();

        if (window.addEventListener) {
            window.addEventListener('resize', LyteBox.prototype.resizeHandler, false);
        } else if (window.attachEvent) {
            window.attachEvent('onresize', LyteBox.prototype.resizeHandler);
        }
    }
}

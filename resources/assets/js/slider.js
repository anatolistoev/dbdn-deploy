$(document).ready(function() {
    ///*** Home carousel ***//
    if($('#hbA').length > 0){
//        var res = '';
//        var num = $('#hbACont a').length;
//       $('#hbACont .banner_image_wrapper img:last-of-type').each(function(index, img){
//           res += '<a class="hBlockPage" href="javascript:;" onclick="showHBA(this);"><img src="'+img.src.replace('brand_banner', 'menu_carousel')+'"></a>';
//       });
    //	for (var i = 0; i < num; i++) {
    //		res += '<a class="hBlockPage" href="javascript:;" onclick="scrollHBA(this);"></a>';
    //	}
//        $('#hbA .hBlockPages').html(res);
       // $('#hbA .hBlockPages a:first-of-type').addClass('hBlockPageActive');
//        var smallImage = new Image();
//        smallImage.onload = function(){
//            $('#hbA .hBlockPages').show();
//            // autoShow();
//        }
//
//        var lupeUrl = $('#hbA .hBlockPages img:last-of-type')[0].src;
//        var reg = /htt[^"\)']+/g;
//        lupeUrl = reg.exec(lupeUrl); // cut the url of the image
//        smallImage.src = lupeUrl[0];

    //	$('#hbA .hBlockPages').css('margin-left','-'+($('#hbA .hBlockPages').width() - 10)/2+'px')


    //	if(num > 1){
    //		$('.hBlockLeft,.hBlockRight').show();
    //		$('.hBlockLeft').click(function() {
    //			moveHBA('left');
    //			return false;
    //		})
    //		$('.hBlockRight').click(function() {
    //			moveHBA('right');
    //			return false;
    //		})
    //	}
        //generateHovers();
    }
    if($('#hbA').length > 0){
        $(window).scroll(function(){
            calculateOpacity($('.banner_text_wrapper, .banner_image_wrapper'))
        });
    }
});

// code for home slider
function generateHovers(){
    var i, hbACont = $("#hbACont a"), hBlockPage = $("a.hBlockPage");

    if(!hBlockPage.length || !hbACont.length)
        return;

    if(hBlockPage.length != hbACont.length)
    {
        console.error("Banners do not match");
        return;
    }

    $("<div>")
        .attr({"id":"sliderPopup","class":"popup"})
        .appendTo($(hBlockPage[0]).parent());

    $(hbACont).each(function(idx, el){
        var t = $(el).find('.banner_text_wrapper h1').html(), h = $(hBlockPage[idx]);

        if(t)
        {
            h.attr("popup-title", t);

            h.hover(
                function(event)
                {
                     var position = $('.hBlockPage').eq(idx).position();
                    $("#sliderPopup")
                        .html($(this).attr("popup-title"))
                        .append($("<div>").attr("class","popup-ticker"))
                        .css({'visibility':'visible', 'overflow':'visible', "left": (position.left - 30) + "px", "top": (position.top - $("#sliderPopup").outerHeight() - 9)+ "px"});
                },
                function(event)
                {
                    $("#sliderPopup").css({visibility:"hidden"}).empty();
                }
            );
        }
    });
}

//function scrollHBA(callingA) {
//	var $caller = $(callingA);
//	var pos = $caller.index();
//	$('#hbACont')
//			.stop(true, false)
//			.animate({left: pos * -1180}, 400, 'swing');
//	$caller.siblings().removeClass('hBlockPageActive');
//	$caller.addClass('hBlockPageActive');
//}

function moveHBA(direction) {
	var pos = $('.hBlockPages .hBlockPageActive').index();
	if (direction == 'right') {
		pos++;
	} else {
		pos--;
	}

	if (pos < 0 || pos >= $('.hBlockPages .hBlockPage').length)
		return false;
	$('#hbACont')
			.stop(true, false)
			.animate({left: pos * -1180}, 400, 'swing');
	$('.hBlockPages .hBlockPage').removeClass('hBlockPageActive');
	$('.hBlockPages .hBlockPage').eq(pos).addClass('hBlockPageActive');
}

//function autoScroll() {
//
//	var $hbACont = $('#hbACont');
//	var lft = parseInt($hbACont.css('left'), 10);
//	$('#hbA a.hBlockPage').removeClass('hBlockPageActive');
//	$('#hbA a.hBlockPage').eq(lft / -1180).addClass('hBlockPageActive');
//	if (lft > (-1180 * ($hbACont.children('a').length - 1))) { // is container.left > the total (negative) width of its children
//		$hbACont
//				.delay(10000)
//				//.delay(1000)
//				.animate({left: lft - 1180}, 700, 'swing',
//						function() {
//							autoScroll();
//						});
//	} else {
//		$hbACont
//				.delay(10000)
//				.animate({left: 0}, 900, 'swing',
//						function() {
//							autoScroll();
//						});
//	}
//}
function showHBA(callingA) {
	var $caller = $(callingA);
    if(!$caller.hasClass('hBlockPageActive')){
        var index = $caller.index();
        var prevIndex = $('.hBlockPageActive').index();
        var prevSelected = $('.hBlockPageActive');
        $(prevSelected).removeClass('hBlockPageActive');
            $caller.addClass('hBlockPageActive');
        $('#hbACont a').eq(prevIndex).fadeOut();
        $('#hbACont a').eq(index).fadeIn();
    }
}
function autoShow() {
    setInterval(function(){
        var currIndex = $('.hBlockPageActive').index();
        var currSelected = $('.hBlockPageActive');
        var nextIndex = (currIndex + 1) < $('.hBlockPage').length ? (currIndex + 1): 0;
        currSelected.removeClass('hBlockPageActive');
        $('.hBlockPage').eq(nextIndex).addClass('hBlockPageActive');
        $('#hbACont a').eq(currIndex).fadeOut();
        $('#hbACont a').eq(nextIndex).fadeIn();
    }, 10000);
}
//function calculateOpacity(){
//    var scrollTop = $(window).scrollTop(), opacityElement = $('.banner_text_wrapper, .banner_image_wrapper');
////    if(scrollTop < 72){
////        if(opacityElement.css('opacity') != '1'){
////            opacityElement.css('opacity', '1');
////        }
////        return;
////    }
//    if(scrollTop > 750){
//        if(opacityElement.css('opacity') != '0'){
//            opacityElement.css('opacity', '0');
//        }
//
//        return;
//    }
//    var opacity = (11 - (scrollTop / 68)) / 10;
//    opacityElement.css('opacity', opacity);
////    if(scrollTop >= 72 && scrollTop < 144){
////        opacityElement.css('opacity', '0.9');
////    }else if(scrollTop < 216){
////        opacityElement.css('opacity', '0.8');
////    }else if(scrollTop < 288){
////        opacityElement.css('opacity', '0.7');
////    } else if(scrollTop < 360){
////        opacityElement.css('opacity', '0.6');
////    }else if(scrollTop < 432){
////        opacityElement.css('opacity', '0.5');
////    }else if(scrollTop < 504){
////        opacityElement.css('opacity', '0.4');
////    }else if(scrollTop < 576){
////        opacityElement.css('opacity', '0.3');
////    }else if(scrollTop < 648){
////        opacityElement.css('opacity', '0.2');
////    }else if(scrollTop < 720){
////        opacityElement.css('opacity', '0.1');
////    }else if(scrollTop < 800){
////        opacityElement.css('opacity', '0');
////    }
//}

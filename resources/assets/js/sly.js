// Intercept Sly load event in order to hide the pagination
// when there is only single page. #13960
(function () {
    var slyReal = Sly;

    window.Sly = function Sly(frame, options, callbackMap) {
        callbackMap = callbackMap || {};

        var callbackMapLoadReal = null;

        if (typeof callbackMap === 'object'
            && callbackMap.hasOwnProperty('load')) {
            callbackMapLoadReal = callbackMap.load;
        }

        callbackMap.load = function (eventName) {
            var loadContext = this;

            if (this.pages.length < 2) {
                this.options.pagesBar.hide();
            }

            if (callbackMapLoadReal instanceof Function) {
                callbackMapLoadReal.call(loadContext, eventName);
            }

            // Providing global load event for all Sly sliders for better handling
            // of edge cases.
            var loadedGlobalEvent = $.Event('slyLoaded');
            loadedGlobalEvent.customData = { sliderInstance: loadContext };

            $(frame).trigger(loadedGlobalEvent);
        };

        return new slyReal(frame, options, callbackMap);
    };

    $.event.special.slyLoaded = {
        add: function (handleObj) {
            var selector = this;

            if (typeof handleObj.selector !== 'undefined') {
                selector = handleObj.selector;
            }

            $(selector).each(function () {
                var slyInstance = $.data(this, 'sly');

                if (slyInstance instanceof slyReal) {
                    var event = $.Event('slyLoaded');
                    event.customData = { sliderInstance: slyInstance };

                    handleObj.handler.call(this, event);
                }
            });
        }
    };
}());

var SliderInit, sliderFrames = [];
jQuery(function ($) {
    'use strict';
// -------------------------------------------------------------
// Basic Navigation
// -------------------------------------------------------------
    (function () {
        var $frame = $('.carousel_basic');
        var $slidee = $frame.children('ul').eq(0);
        var $wrap = $frame.parent();
// Call Sly on frame
        var sly = $frame.sly({
                horizontal: 1,
                itemNav: 'forceCentered',
                smart: 1,
                activateMiddle: 1,
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1,
                startAt: 0,
                scrollBy: 1,
                pagesBar: $wrap.find('.pages'),
                activatePageOn: 'click',
                speed: 300,
                elasticBounds: 1,
                easing: 'easeOutExpo',
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,
                // Buttons
                prev: $wrap.find('.prev'),
                next: $wrap.find('.next'),
                activeClass: 'active'
        },
        {
            load: function(e){
                generateCarouselIfo($(this.items[0].el.children[0]));
            }
        }
    );

        $slidee.find('.slider_element').on('click',function(){
            generateCarouselIfo($(this));
       });
       function generateCarouselIfo($element){
           $('.slider_element.active').removeClass('active');
            $element.addClass('active');
            $('#carousel_text').html($element.attr('data-description')+ '<span class="link_more"></span>');
            $('#carousel_title').html($element.attr('data-title'));
            $('.carousel_href').attr('href', $element.attr('data-href'));
            var img = $element.find('img');
            if(img.length == 0){
                $('#carousel_img').css('display', 'none');
                $('#carousel_img').after($element.find('a').html());
            }else{
                var svg = $('#carousel_img_wrapper').find('svg');
                if(svg.length > 0){
                    svg.remove();
                }
                $('#carousel_img').css('display', 'inline');
				var factor = 1;
                //TODO: Extract for common use.
				if(window.matchMedia){
					if( window.matchMedia("(min-resolution: 3dppx)").matches ||
						window.matchMedia("(-webkit-min-device-pixel-ratio: 3)").matches ||
						window.matchMedia("(min--moz-device-pixel-ratio: 3)").matches ||
						window.matchMedia("(-o-min-device-pixel-ratio: 3/1)").matches ||
						window.matchMedia("(min-device-pixel-ratio: 3) ").matches ||
						window.matchMedia("(min-resolution: 288dpi)").matches){
						factor = 3;
					}else if( window.matchMedia("(min-resolution: 2dppx)").matches ||
							  window.matchMedia("(-webkit-min-device-pixel-ratio: 2)").matches ||
							  window.matchMedia("(min--moz-device-pixel-ratio: 2)").matches ||
							  window.matchMedia("(-o-min-device-pixel-ratio: 2/1)").matches ||
							  window.matchMedia("(min-device-pixel-ratio: 2)").matches ||
							  window.matchMedia("( min-resolution: 192dpi)").matches){
						factor = 2;
					}
				}

				if((factor === 2 || factor === 3)){
						var carouselPreloader_x1 = new Image();
						carouselPreloader_x1.onload = function() {
							if(img.attr('src').indexOf('small_carousel~'+factor+'x') > -1){
								var carouselPreloader = new Image();
								carouselPreloader.onload = function() {
                                    var srcImgCurrent = $(this).attr('src')
                                    var srcCarousel = $('#carousel_img').attr('src');
                                    //TODO: Extend for Protected files.
									if(srcCarousel.substr(srcCarousel.indexOf('carousel') + 8, srcCarousel.length) == srcImgCurrent.substr(srcImgCurrent.indexOf('carousel~'+factor+'x')+11, srcImgCurrent.length)){
										$('#carousel_img').attr('src',$(this).attr('src'));
									}
								};
                                //TODO: Extend for Protected files.
								carouselPreloader.src = $('#carousel_img').attr('src').replace('__cache/carousel', '__cache/carousel~'+factor+'x');;
							}
						};
                        //TODO: Extend for Protected files.
						var image_src = img.attr('src').replace(new RegExp('__cache/small_carousel(~'+factor+'x)?/', 'g'), '__cache/carousel/')
						carouselPreloader_x1.src = image_src;
						$('#carousel_img').attr('src', image_src);

						$('#carousel_img').siblings().each(function(){
							$(this).remove();
						});
				}else{
					$('#carousel_img').attr('src', img.attr('src').replace('__cache/small_carousel', '__cache/carousel'));
				}
                //$('#carousel_img').attr('srcset', img.attr('srcset').replace(new RegExp('__cache/small_carousel', 'g'), '__cache/carousel'));
            }
       }
    }());
// -------------------------------------------------------------
// One Item Per Frame
// -------------------------------------------------------------



    (function () {

        var mslide = $('.mslide');

        if (mslide.length > 0) {
            SliderInit = createSlider();

            mslide.each(function () {
                sliderFrames.push(new SliderInit($(this)));
            });

        }

        $('a.slider-grid').click( function() {

            var top = this.getBoundingClientRect().top;

            mslide.each(function() {
                var _this = $(this);
                _this.parent().find('.btn_navigation').css('visibility', 'hidden');
                _this.removeClass('frame');
                _this.addClass('overview');
                _this.parent().find('.pages').hide();
            });

            if ( mslide.length > 0 ) {
                $.each(sliderFrames, function( idx, val ) {
                    val.destroy();
                });
            }

            setTimeout(function() {
                $('a.view-slider').removeClass('active');
                $('a.slider-grid').addClass('active');
                window.scrollTo(0, arguments[0].offset().top - arguments[1]);
            }, 100, $(this), top);

            return false;
        });

        $('a.view-slider').click( function() {

            var top = this.getBoundingClientRect().top;

            mslide.each(function() {
                var _this = $(this);
                _this.parent().find('.btn_navigation').css('visibility', 'visible');
                _this.removeClass('overview');
                _this.addClass('frame');
                _this.parent().find('.pages').show();
            });


            if ( mslide.length > 0 ) {
                $.each(sliderFrames, function( idx, val ) {
                    val.init();
                });
            }

            setTimeout(function() {
                $('a.slider-grid').removeClass('active');
                $('a.view-slider').addClass('active');
                window.scrollTo(0, arguments[0].offset().top - arguments[1]);
            }, 10, $(this), top);

            return false;
        });
    }());

// -------------------------------------------------------------
// Home Sliders
// -------------------------------------------------------------
    (function () {
        $('.front_slider:not(.carousel_basic)').each(function(index, slider){
            var $frame = $(slider);
            var $wrap = $frame.parent();
    // Call Sly on frame

        var sly = new Sly($frame,{
                horizontal: 1,
                itemNav: 'forceCentered',
                smart: 1,
                activateMiddle: 1,
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1,
                startAt: 0,
                scrollBy: 1,
                pagesBar: $wrap.find('.pages'),
                activatePageOn: 'click',
                speed: 300,
                elasticBounds: 1,
                easing: 'easeOutExpo',
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,
                // Buttons
                prev: $wrap.find('.prev'),
                next: $wrap.find('.next'),
                activeClass: 'active'
        });
        sly.init();
        });
    }());
});
	function createSlider(){
		return function (module) {
            var currentSlider = $(module);
            var currentWrap = currentSlider.parent();
            var options = {
                horizontal: 1,
                itemNav: 'forceCentered',
                smart: 1,
                activateMiddle: 1,
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1,
                startAt: 0,
                scrollBy: 1,
                pagesBar: currentWrap.find('.pages'),
                activatePageOn: 'click',
                speed: 300,
                elasticBounds: 1,
                easing: 'easeOutExpo',
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,
                // Buttons
                prev: currentWrap.find('.prev'),
                next: currentWrap.find('.next'),
                activeClass: 'active'
            };
            var slider =  new Sly(currentSlider, options);
            // if($('#oneperframe1').hasClass('frame')){
            //     slider.init();
            // }

            if($('.mslide').hasClass('frame')){
                slider.init();
            }
            //resizeContainerImages('div.overview div.page div.image img',580,387);
            return slider;
        };
	}

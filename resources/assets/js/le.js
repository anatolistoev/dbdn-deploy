function setResolutionPicture(imageEl) {

	if (parent.window.matchMedia) {
		var factor = 1;
		if( parent.window.matchMedia("(min-resolution: 3dppx)").matches ||
				parent.window.matchMedia("(-webkit-min-device-pixel-ratio: 3)").matches ||
				parent.window.matchMedia("(min--moz-device-pixel-ratio: 3)").matches ||
				parent.window.matchMedia("(-o-min-device-pixel-ratio: 3/1)").matches ||
				parent.window.matchMedia("(min-device-pixel-ratio: 3) ").matches ||
				parent.window.matchMedia("(min-resolution: 288dpi)").matches){
			factor = 3;
		} else if (parent.window.matchMedia("(min-resolution: 2dppx)").matches ||
					  parent.window.matchMedia("(-webkit-min-device-pixel-ratio: 2)").matches ||
					  parent.window.matchMedia("(min--moz-device-pixel-ratio: 2)").matches ||
					  parent.window.matchMedia("(-o-min-device-pixel-ratio: 2/1)").matches ||
					  parent.window.matchMedia("(min-device-pixel-ratio: 2)").matches ||
					  parent.window.matchMedia("( min-resolution: 192dpi)").matches) {
			factor = 2;
		}

		if (factor != 1) {

			var img = $('<img>');
			if ($(imageEl).attr('style') != '') {
				img.attr('style', 'display:none;' + $(imageEl).attr('style'));
			} else {
				img.attr('style', 'display:none;');
			}
			img.attr('class', $(imageEl).attr('class'));
			if ($(imageEl).attr('id') != "") {
				img.attr('id', $(imageEl).attr('id'));
			}
			if ($(imageEl).attr('data-hotspot') != "") {
				img.attr('data-hotspot', $(imageEl).attr('data-hotspot'));
			}
			img.attr('border', '0');
			img.attr('unselectable', 'on');
			img.load(function() {
				$(this).prev().remove();
				$(this).show();
			});
			var img_src_arr = $(imageEl).attr('src').split('/');
			var cache_index = img_src_arr.indexOf('__cache') + 1;
			img_src_arr[cache_index] = img_src_arr[cache_index] + '~' + factor + 'x';
			img.attr('src', img_src_arr.join('/'));
			$(imageEl).after(img);
		}
	}
}

$(document).ready(function() {
//	var pagesize = parent.myLytebox.getPageSize();

//	var x = pagesize[2] - ($('#LE-right').outerWidth() + parseInt($('.LEG_img_container').css('margin-right'), 10) + 2 * parent.myLytebox.borderSize);
//	var y = pagesize[3] - (2 * parent.myLytebox.borderSize + $('#LE-tools').outerHeight() + parseInt($('#LE-tools').css('margin-top'), 10));

//	if (pagesize[2] <= 1280) {
//		contWidth = 740;
//		contHeight = 520;
//	}
//
//	if (contWidth > x) {
//		contHeight = Math.round(contHeight * (x / contWidth));
//		contWidth = x;
//		if (contHeight > y) {
//			contWidth = Math.round(contWidth * (y / contHeight));
//			contHeight = y;
//		}
//	} else if (contHeight > y) {
//		contWidth = Math.round(contWidth * (y / contHeight));
//		contHeight = y;
//		if (contWidth > x) {
//			contHeight = Math.round(contHeight * (x / contWidth));
//			contWidth = x;
//		}
//	}

    var iframeWidth = window.parent.$('#lbIframe').parent().width();
    var iframeHeight = window.parent.$('#lbIframe').parent().height();
    window.parent.$('#lbIframe').width(iframeWidth);
    window.parent.$('#lbIframe').height(iframeHeight);
    $('#content_wrapper').css('line-height', iframeHeight + 'px');
    var contWidth = (iframeWidth - 120)*69.19/100;
    var contHeight = iframeHeight - 120;
    //$('.LEG_img_container').css('line-height', $('#LE').outerHeight() + 'px');
    var maxWidth = 0;
    var maxHeight = 0;
    var elements = $('.LEG_img_container').length;
    var elementLoaded = 0;
    $('.LEG_img_container').each(function (index, value) {
        var newImg = new Image();
        var el = this;
        newImg.onload = function () {
            elementLoaded ++;
            var height = newImg.height;
            var width = newImg.width;
            if (width <= contWidth && height <= contHeight) {
                //nothing
                $(el).find('img').width(width);
                $(el).find('img').height(height);
            } else if (width / height > contWidth / contHeight) {
                $(el).find('img').width(contWidth);
                $(el).find('img').height('auto');
            } else {
                $(el).find('img').width('auto');
                $(el).find('img').height(contHeight);
            }
            var img = $(el).find('img');
            var imgWidth = img.width();
            var imgHeight = img.height();
            if(imgWidth < contWidth && imgWidth > maxWidth){
                maxWidth = imgWidth;
            }
            if(imgHeight < contHeight && imgHeight > maxHeight){
                maxHeight = imgHeight;
            }

            if(elementLoaded == elements){
                if(maxWidth == 0){
                    maxWidth = contWidth;
                }
                if(maxHeight == 0){
                    maxHeight = contHeight;
                }
                $('#LEG, #LE').width(maxWidth);
                if($('#LE-right').height() > maxHeight){
                    maxHeight = $('#LE-right').height();
                }
                $('#LEG, #LE, #LE-right').height(maxHeight);
                 $('#LE').css('line-height', maxHeight + 'px');
            }
            setResolutionPicture($(el).find('img'));
        }
        newImg.onerror = function(){
            elementLoaded ++;
            if(elementLoaded == elements){
                if(maxWidth == 0){
                    maxWidth = contWidth;
                }
                if(maxHeight == 0){
                    maxHeight = contHeight;
                }
                $('#LEG, #LE').width(maxWidth);
                $('#LEG, #LE, #LE-right').height(maxHeight);
                 $('#LE').css('line-height', maxHeight + 'px');
            }
        }
        newImg.src = $(this).find('img').attr('src');
    });


    $('div.LE-tool').each(function () {
        var prop = $(this).attr('value');
        var info = '<div>' + $('#explainer_' + prop).data('info') + '</div>';
        var object = $('<div/>').html(info).contents();
        $(this).find('span.title').html($(object).find('h1').text());
    });

    $('div.LE-tool').click(function () {
        var $this = $(this).toggleClass('on');
        var prop = $this.attr('value');
        $('#LEG .LE-tool-layer-' + prop).toggle();

    });
//            .mouseenter(function () {
//        var $this = $(this);
//        var prop = $this.attr('value');
//        $('#LE-legend-info').html($('#LEG .LE-tool-layer-' + prop).data('info'));
//    });
})
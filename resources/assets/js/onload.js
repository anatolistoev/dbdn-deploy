(function () {
    var lastTime = 0;
    var vendors = ['webkit', 'moz'];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame =
          window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function (callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function () { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function (id) {
            clearTimeout(id);
        };
}());
(function () {
    function decimalAdjust(type, value, exp) {
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;

        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }

        if (value < 0) {
            return -decimalAdjust(type, -value, exp);
        }

        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));

        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }

    if (!Math.round10) {
        Math.round10 = function (value, exp) {
            return decimalAdjust('round', value, exp);
        };
    }
    if (!Math.floor10) {
        Math.floor10 = function (value, exp) {
            return decimalAdjust('floor', value, exp);
        };
    }
    if (!Math.ceil10) {
        Math.ceil10 = function (value, exp) {
            return decimalAdjust('ceil', value, exp);
        };
    }
})();
(function (namespace) {
    namespace.storage = namespace.storage || {};
    var me = namespace.storage;

    var globalMemoryStore = {};

    me.InMemoryStorage = function InMemoryStorage(memorySegmentIdentifier) {
        memorySegmentIdentifier = memorySegmentIdentifier || '__default__';

        var instance = this;
        var instanceMemoryStore = {};

        function init() {
            if (!(instance instanceof InMemoryStorage)) {
                return new InMemoryStorage(memorySegmentIdentifier);
            }

            if (globalMemoryStore.hasOwnProperty(memorySegmentIdentifier)) {
                return globalMemoryStore[memorySegmentIdentifier];
            }

            globalMemoryStore[memorySegmentIdentifier] = instance;

            Object.defineProperty(instance, 'length', {
                enumerable: true,
                configurable: true,
                get: function () {
                    return Object.keys(instanceMemoryStore).length;
                }
            });

            return instance;
        }

        instance.key = function (keyIndex) {
            var instanceMemoryStoreKeysCollection = Object.keys(instanceMemoryStore);

            if (instanceMemoryStoreKeysCollection.hasOwnProperty(keyIndex)) {
                return instanceMemoryStoreKeysCollection[keyIndex];
            }

            return null;
        };

        instance.getItem = function (key) {
            if (!instanceMemoryStore.hasOwnProperty(key)) {
                return null;
            }

            return instanceMemoryStore[key];
        };

        instance.setItem = function (key, value) {
            instanceMemoryStore[key] = value;
        };

        instance.removeItem = function (key) {
            delete instanceMemoryStore[key];
        };

        instance.clear = function () {
            instanceMemoryStore = {};
        };

        return init();
    };
}(window.DBDN = window.DBDN || {}));
(function () {
    String.prototype.prependCSSSelector = function (toBePrependedToSelector) {
        return this.split(',').map(function (selectorItem) {
            return toBePrependedToSelector + ' ' + selectorItem;
        })
        .join(',');
    };

    String.prototype.combineOrCSSSelector = function (selectorToBeOrCombinedWith) {
        return this + ',' + selectorToBeOrCombinedWith;
    };
}());
(function () {
    var instance = this;
    this.orientation = {portrait: 'portrait', landscape: 'landscape', square: 'square'};

    function checkViewport(minWidth, maxWidth) {
        minWidth = minWidth || null;
        maxWidth = maxWidth || null;

        var changeDeviceWidth = false;
        var $viewportElement = $("meta[name='viewport']");

        if ($viewportElement.length > 0
            && !$viewportElement.attr('content').match(/width=device-width/i)) {
            changeDeviceWidth = true;
        }

        if (changeDeviceWidth) {
            var oldAttrValue = $viewportElement.attr('content');
            // Otherwise the matchMedia will return always true!
            $viewportElement.attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0');
        }

        var matchMediaResult = null;

        if (minWidth !== null && maxWidth !== null) {
            matchMediaResult = matchMedia('(min-width:' + minWidth + ')').matches
                        && matchMedia('(max-width:' + maxWidth + ')').matches;
        } else if (minWidth !== null) {
            matchMediaResult = matchMedia('(min-width:' + minWidth + ')').matches;
        } else if (maxWidth !== null) {
            matchMediaResult = matchMedia('(max-width:' + maxWidth + ')').matches;
        }

        if (changeDeviceWidth) {
            $viewportElement.attr('content', oldAttrValue);
        }

        return matchMediaResult;
    }

    this.isTabletViewport = function (ignoreRequestDesktopState) {
        ignoreRequestDesktopState = ignoreRequestDesktopState || false;

        if (DBDN.utils.isForcedDesktopMode() && !ignoreRequestDesktopState) {
            return false;
        }

        return checkViewport('768px', '1023px');
    };

    this.isMobileViewport = function (ignoreRequestDesktopState) {
        ignoreRequestDesktopState = ignoreRequestDesktopState || false;

        if (DBDN.utils.isForcedDesktopMode() && !ignoreRequestDesktopState) {
            return false;
        }

        return checkViewport(null, '767px');
    };

    this.isDesktopViewport = function (ignoreRequestDesktopState) {
        ignoreRequestDesktopState = ignoreRequestDesktopState || false;

        return !instance.isTabletViewport(ignoreRequestDesktopState) && !instance.isMobileViewport(ignoreRequestDesktopState);
    };

    this.getScreenOrientation = function () {
        if ($(window).width() > $(window).height()) {
            return this.orientation.landscape;
        }

        if ($(window).width() == $(window).height()) {
            return this.orientation.square;
        }

        return this.orientation.portrait;
    };

}());
(function (namespace) {
    namespace.events = namespace.events || {};
    var me = namespace.events;

    var trackedEvents = {};

    // BaseEvent
    function BaseEvent(eventName, instanceEventNamespace, eventTargetObject) {
        var ticking = false;

        var eventName = eventName || 'base';
        var eventRootNamespace = BaseEvent.eventRootNamespace;
        var instanceEventNamespace = instanceEventNamespace || new Date().getTime();
        var eventTargetObject = eventTargetObject instanceof HTMLElement || eventTargetObject instanceof jQuery ? eventTargetObject : window;

        function doWork(ev, callback) {
            if (typeof callback === 'function') {
                callback(ev);
            }

            ticking = false;
        }

        this.tick = function (ev, callback) {
            if (!ticking) {
                requestAnimationFrame(function () {
                    doWork(ev, callback);
                });
            }

            ticking = true;
        }

        this.getEventNameWithNamespace = function (includeInstanceNamespace) {
            includeInstanceNamespace = includeInstanceNamespace || true;

            if (includeInstanceNamespace) {
                return this.getEventName() + '.' + this.getEventRootNamespace() + '.' + this.getInstanceEventNamespace();
            } else {
                return this.getEventName() + '.' + this.getEventRootNamespace();
            }
        };

        this.getEventNameWithInstanceNamespace = function () {
            return this.getEventName() + '.' + this.getInstanceEventNamespace();
        };

        this.getEventName = function () {
            return eventName;
        };

        this.getInstanceEventNamespace = function () {
            return instanceEventNamespace;
        };

        this.getEventRootNamespace = function () {
            return eventRootNamespace;
        };

        this.stopEvent = function () {
            $(eventTargetObject).off(this.getEventNameWithNamespace());
        };

        this.trackEvent = function () {
            trackedEvents[this.getEventNameWithNamespace()] = this;
        };
    };
    BaseEvent.eventRootNamespace = 'DBDN_Events';
    BaseEvent.prototype.removeTrackedEvents = function (namespace){
        if (typeof namespace === 'undefined') {
            throw new Error('Please provide namespace value.');
        }

        for (property in trackedEvents) {
            if (trackedEvents.hasOwnProperty(property)
                && property.indexOf(namespace) !== -1) {
                trackedEvents[property].stopEvent();
            }
        }
    };

    // Resize
    me.WindowResizeEvent = function WindowResizeEvent(timeoutInMs, onResizeCallback, instanceEventNamespace) {
        BaseEvent.call(this, WindowResizeEvent.eventName, instanceEventNamespace);
        var instance = this;

        onResizeCallback = onResizeCallback || timeoutInMs;
        var debounceDurationInMs = typeof timeoutInMs === 'function' ? 200 : (timeoutInMs | 0) || 200;
        var debounceTimeoutHandle = null;

        if (typeof onResizeCallback !== 'function') {
            throw new Error('onResizeCallback not a function!');
        }

        this.startEvent = function () {
            $(window).on(this.getEventNameWithNamespace(), function (ev) {
                clearTimeout(debounceTimeoutHandle);

                debounceTimeoutHandle = setTimeout(function () {
                    instance.tick(ev, onResizeCallback);
                }, debounceDurationInMs);
            });
        };

        this.changeDebounceTimeout = function (newTimeoutInMs) {
            debounceDurationInMs = (newTimeoutInMs | 0) || debounceDurationInMs;
        };

        this.startEvent();
    };
    me.WindowResizeEvent.eventName = 'resize';
    extend(BaseEvent, me.WindowResizeEvent);
    me.WindowResizeEvent.prototype.clearAllEvents = function (namespace) {
        namespace = namespace ? '.' + namespace : '';

        $(window).off(me.WindowResizeEvent.eventName + '.' + BaseEvent.eventRootNamespace + namespace);
    };

    // Orientation change
    me.OrientationChangeEvent = function OrientationChangeEvent(onOrientationChangeCallback, instanceEventNamespace) {
        BaseEvent.call(this, OrientationChangeEvent.eventName, instanceEventNamespace);
        var instance = this;

        var orientation = { portrait: 'portrait', landscape: 'landscape', square: 'square' }
        var previousOrientation = null;
        var resizeInstance = null;

        function init() {
            previousOrientation = getScreenOrientation();

            instance.startEvent();
        }

        function getScreenOrientation() {
            if ($(window).width() > $(window).height()) {
                return orientation.landscape;
            }

            if ($(window).width() == $(window).height()) {
                return orientation.square;
            }

            return orientation.portrait;
        }

        function baseEventCallback() {
            var currentOrientation = getScreenOrientation();

            if (currentOrientation !== previousOrientation
                && typeof onOrientationChangeCallback === 'function') {
                previousOrientation = currentOrientation;

                var event = $.Event(instance.getEventName());
                event.orientation = currentOrientation;

                onOrientationChangeCallback(event);
            }
        }

        this.startEvent = function () {
            if (!DBDN.utils.isCSSPropertySupported('-webkit-overflow-scrolling')) {
                resizeInstance = new me.WindowResizeEvent(100, function () {
                    baseEventCallback();
                }, instance.getEventNameWithInstanceNamespace());

            } else {
                // Needed because of the Mobile Safary buggy behaviour!
                $(window).on('orientationchange', function () {
                    baseEventCallback();
                });
            }
        };

        this.stopEvent = function () {
            resizeInstance.stopEvent();
        };

        init();
    };
    me.OrientationChangeEvent.eventName = 'orientationChange';
    extend(BaseEvent, me.OrientationChangeEvent);
    me.OrientationChangeEvent.prototype.clearAllEvents = function () {
        me.WindowResizeEvent.prototype.clearAllEvents.call(null, me.OrientationChangeEvent.eventName);
    };

    // Scroll
    me.ScrollEvent = function ScrollEvent(element, timeoutInMs, onScrollCallback, instanceEventNamespace) {
        BaseEvent.call(this, ScrollEvent.eventName, instanceEventNamespace, element);
        var instance = this;

        onScrollCallback = onScrollCallback || timeoutInMs || element;
        var targetElement = element instanceof HTMLElement || element instanceof jQuery ? element : window;
        var debounceDurationInMs = typeof timeoutInMs === 'number' ? timeoutInMs : 200;
        var debounceTimeoutHandle = null;

        if (typeof element === 'number') {
            debounceDurationInMs = element;
        }

        if (typeof onScrollCallback !== 'function') {
            throw new Error('onScrollCallback not a function!');
        }

        this.startEvent = function () {
            $(targetElement).on(this.getEventNameWithNamespace(), function (ev) {
                clearTimeout(debounceTimeoutHandle);

                debounceTimeoutHandle = setTimeout(function () {
                    instance.tick(ev, onScrollCallback);
                }, debounceDurationInMs);
            });
        };

        this.changeDebounceTimeout = function (newTimeoutInMs) {
            debounceDurationInMs = typeof newTimeoutInMs === 'number' ? newTimeoutInMs : debounceDurationInMs;
        };

        this.startEvent();
        this.trackEvent();
    };
    me.ScrollEvent.eventName = 'scroll';
    extend(BaseEvent, me.ScrollEvent);
    me.ScrollEvent.prototype.clearAllEvents = function (namespace) {
        namespace = namespace ? '.' + namespace : '';

        BaseEvent.prototype.removeTrackedEvents.call(null, me.ScrollEvent.eventName + '.' + BaseEvent.eventRootNamespace + namespace);
    };

    // Common
    function extend(parentObject, childObject) {
        childObject.prototype = new parentObject();
        childObject.prototype.constructor = childObject;
    }
}(window.DBDN = window.DBDN || {}));
(function () {
    $(document).on('tabletDesktopViewportChange.DBDN_General tabletTabletViewportChange.DBDN_General desktopDesktopViewportChange.DBDN_General', function (ev) {
        DBDN.scaleWebsite.scale();
    });
}());
(function (namespace) {
    namespace.utils = namespace.utils || {};
    var me = namespace.utils;

    me.isScaledViewport = function () {
        return DBDN.scaleWebsite.isViewportScaled();
    };

    me.isDesktopScaledViewport = function () {
        return DBDN.scaleWebsite.isViewportScaled() && isDesktopViewport();
    };

    me.isCSSPropertySupported = function (propertyName) {
        if (propertyName.startsWith('-')) {
            propertyName = propertyName.substr(1);
        }

        var propertyNameTokens = propertyName.split('-');

        if (propertyNameTokens.length > 1) {
            Array.prototype.forEach.call(propertyNameTokens, function (item, index, collection) {
                if (index > 0) {
                    collection[index] = item.substr(0, 1).toUpperCase() + item.substr(1);
                }
            });

            propertyName = propertyNameTokens.join('');
        }

        var inMemoryElement = document.createElement('darthvader');

        if (typeof inMemoryElement.style[propertyName] !== 'undefined') {
            return true;
        }

        return false;
    };

    me.poll = function poll(conditionFn, callback, stopPollingConditionFn, interval, timeout) {
        interval = interval || 50;
        timeout = new Date().getTime() + (timeout || 2000);

        var pollingStatus = false;
        var rafIdentifier = null;

        (function pollStatus() {
            if (stopPollingConditionFn !== null && stopPollingConditionFn()) {
                cancelAnimationFrame(rafIdentifier);

                return;
            }

            if (pollingStatus) {
                return;
            }

            if (conditionFn()) {
                pollingStatus = true;

                rafIdentifier = requestAnimationFrame(doWork);
            } else if (new Date().getTime() < timeout) {
                setTimeout(function () {
                    pollStatus();
                }, interval)
            }
        }());

        function doWork() {
            callback();

            pollingStatus = false;
        }
    };

    me.getSessionLanguage = function () {
        var fallbackLang = 'de';

        if (typeof lang_label !== 'undefined') {
            return lang_label;
        }

        var lang = document.querySelector('html').getAttribute('lang');
        if (lang) {
            return lang;
        }

        return fallbackLang;
    };

    me.getBoundingClientRectOfHiddenElement = function ($element) {
        if (typeof document.body['getBoundingClientRect'] === 'undefined') {
            return false;
        }

        var isElementVisible = $element.is(':visible');

        if (!isElementVisible) {
            $element.css('visibility', 'hidden');
            $element.show();
        }

        var boundingRectangle = $element[0].getBoundingClientRect();

        if (!isElementVisible) {
            $element.hide();
            $element.css('visibility', '');
        }

        return boundingRectangle;
    };

    me.toggleTabletDesktopEvent = function (desktopCallback, tabletCallback, htmlViewportMode) {
        if (htmlViewportMode.previous === DBDN.enum.HtmlViewportMode.TABLET
            && htmlViewportMode.active === DBDN.enum.HtmlViewportMode.DESKTOP) {
            if (typeof desktopCallback === 'function') {
                desktopCallback();
            }
        }

        if (htmlViewportMode.previous === DBDN.enum.HtmlViewportMode.DESKTOP
            && htmlViewportMode.active === DBDN.enum.HtmlViewportMode.TABLET) {
            if (typeof tabletCallback === 'function') {
                tabletCallback();
            }
        }
    };

    me.toggleTabletDesktopHtml = function ($container,
        $desktopHtml,
        $tabletHtml,
        htmlViewportMode,
        insertCallback) {
        if (htmlViewportMode.previous === DBDN.enum.HtmlViewportMode.TABLET
            && htmlViewportMode.active === DBDN.enum.HtmlViewportMode.DESKTOP) {
            $container.hide();

            if (typeof insertCallback !== 'function') {
                $container
                    .append($desktopHtml);
            } else {
                /* toBeInserted, toBeDeleted */
                insertCallback($desktopHtml, $tabletHtml);
            }

            $tabletHtml
                .not($tabletHtml)
                .add($tabletHtml.detach());

            $container.show();
        }

        if (htmlViewportMode.previous === DBDN.enum.HtmlViewportMode.DESKTOP
            && htmlViewportMode.active === DBDN.enum.HtmlViewportMode.TABLET) {
            $container.hide();

            if (typeof insertCallback !== 'function') {
                $container
                    .append($tabletHtml);
            } else {
                insertCallback($tabletHtml, $desktopHtml);
            }

            $desktopHtml
                .not($desktopHtml)
                .add($desktopHtml.detach());

            $container.show();
        }
    };

    me.getActiveHtmlViewportMode = function () {
        if (isTabletViewport()) {
            return DBDN.enum.HtmlViewportMode.TABLET
        }

        if (isMobileViewport()) {
            return DBDN.enum.HtmlViewportMode.MOBILE
        }

        if (!isTabletViewport() && !isMobileViewport()) {
            return DBDN.enum.HtmlViewportMode.DESKTOP
        }
    };

    me.createXMLHTTPObject = function() {
        var XMLHttpFactories = [
            function () {return new XMLHttpRequest();},
            function () {return new ActiveXObject("Msxml2.XMLHTTP");},
            function () {return new ActiveXObject("Msxml3.XMLHTTP");},
            function () {return new ActiveXObject("Microsoft.XMLHTTP");}
        ];
        var xmlhttp = false;
        for (var i=0;i<XMLHttpFactories.length;i++) {
            try {
                xmlhttp = XMLHttpFactories[i]();
            }
            catch (e) {
                continue;
            }
            break;
        }
        return xmlhttp;
    };

    me.downloadFileAndDisplayNotificationWhileBeingDownloaded = function (element, notificationMsg, isItForm){
        isItForm = isItForm || false;

        $("#alertsContents .alert_text").html(notificationMsg);
        $("#alertsBG").unbind('click');
        $("#alertsBG").show();
        $('#alerts .close-button, #alerts .ok_button').hide();
        $("#alerts").addClass('center');
        $('#alerts').show();

        var url = isItForm ? $(element).prop('action') : $(element).attr('href');
        var xhr = DBDN.utils.createXMLHTTPObject();
        var requestMethod = isItForm ? $(element).prop('method') : 'GET';

        xhr.open(requestMethod, url, true);
        xhr.responseType = 'blob';
        xhr.onload = function (e) {
            if (this.status >= 200 && this.status < 300) {
                var filename = "";
                var disposition = this.getResponseHeader('Content-Disposition');
                if (disposition && disposition.indexOf('attachment') !== -1) {
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1])
                        filename = matches[1].replace(/['"]/g, '');
                }

                var type = xhr.getResponseHeader('Content-Type');
                var blob = e.target.response;

                if (typeof window.navigator.msSaveBlob !== 'undefined') {
                    // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                    window.navigator.msSaveBlob(blob, filename);
                } else {
                    var URL = window.URL || window.webkitURL;
                    var downloadUrl = URL.createObjectURL(blob);

                    if (filename) {
                        // use HTML5 a[download] attribute to specify filename
                        var a = document.createElement("a");
                        // safari doesn't support this yet
                        if (typeof a.download === 'undefined') {
                            window.location = downloadUrl;
                        } else {
                            a.href = downloadUrl;
                            a.download = filename;
                            document.body.appendChild(a);
                            a.click();
                        }
                    } else {
                        window.location = downloadUrl;
                    }

                    setTimeout(function () {
                        URL.revokeObjectURL(downloadUrl);

                        if (typeof a !== 'undefined') {
                            try {
                                document.body.removeChild(a);
                            } catch (e) {

                            }
                        }
                    }, 100); // cleanup
                }
                $("#alertsBG").on("click", function(){
                    $("#alerts, #alertsBG").fadeOut(300,function(){$("#alerts").removeClass('red');$("#alerts").removeClass('center');});
                });
                $("#alertsBG").hide();
                $('#alerts').hide();
                $("#alerts").removeClass('center');
                $('#alerts .close-button, #alerts .ok_button').show();
            } else {
                $("#alerts").addClass('red');
                $('#alerts .close-button, #alerts .ok_button').show();
                $('#alerts .alert_text').html(this.statusText);
            }
        };

        if (isItForm) {
            xhr.send(new FormData($(element)[0]));
        } else {
            xhr.send();
        }
};

    me.validateProfileForm = function (selector, notRequaredFields){
        notRequaredFields = notRequaredFields ? notRequaredFields : [];
        if($(selector + ' #iagree').length > 0){
            var submit_button = $(selector + ' .more_info .submit_button');
            if(!$(selector + ' #iagree').is(":checked")){
                submit_button.attr('disabled', 'disabled');
            }
            $(selector + ' #iagree').click(function(){
                if($(this).is(":checked")){
                    submit_button.removeAttr("disabled");
                }else{
                   submit_button.attr('disabled', 'disabled');
                }
            });
        }
        $(selector).submit(function(){
            var requaredFields = $('span.important').closest('.input').find('.input__field');
            var error = false;
            var errorMsg = '';
            requaredFields.each(function(index, element){
                if($(element).val() === ''){
                    var fieldName = $(element).attr('name');
                    var notRequared = notRequaredFields.find(
                                function(str) {
                                    return str == fieldName;
                                }
                            );
                    if(!notRequared){
                        fieldName = js_localize['field.'+fieldName];
                        errorMsg += js_localize['field.required'].replace('{0}', fieldName);
                        errorMsg += '<br>';
                        $(element).addClass('error');
                        error = true;
                    }

                }else{
                    $(element).removeClass('error');
                }
            });
            if(error){
                msg(errorMsg,  true);
                return false;
            }
        });
    };

}(window.DBDN = window.DBDN || {}));
(function (namespace) {
    namespace.enum = namespace.enum || {};
    var me = namespace.enum;

    me.HtmlViewportMode = Object.freeze({
        TABLET: 'tablet',
        MOBILE: 'mobile',
        DESKTOP: 'desktop',
        SCALED: 'scaled-viewport'
    });
}(window.DBDN = window.DBDN || {}));
// Raise special event after orientation change occurs
// if the viewport width falls within certain predefined ranges.
(function () {
    var previousHtmlViewportMode = DBDN.utils.getActiveHtmlViewportMode();
    var wasPreviousHtmlViewportModeScaled = DBDN.utils.isScaledViewport();

    new DBDN.events.OrientationChangeEvent(function () {
        var event = null;

        if (isDesktopViewport()
            && previousHtmlViewportMode === DBDN.enum.HtmlViewportMode.TABLET) {
            event = getTabletDesktopViewportChangeEvent();
        }

        if (isTabletViewport()
            && previousHtmlViewportMode === DBDN.enum.HtmlViewportMode.DESKTOP) {
            event = getTabletDesktopViewportChangeEvent();
        }

        if (isTabletViewport()
            && previousHtmlViewportMode === DBDN.enum.HtmlViewportMode.TABLET) {
            event = getTabletTabletViewportChangeEvent();
        }

        if (isMobileViewport()
            && previousHtmlViewportMode === DBDN.enum.HtmlViewportMode.MOBILE) {
            event = getMobileMobileViewportChangeEvent();
        }

        if (isDesktopViewport()
            && previousHtmlViewportMode === DBDN.enum.HtmlViewportMode.DESKTOP) {
            event = getDesktopDesktopViewportChangeEvent();
        }

        if (event !== null) {
            event.htmlViewportMode.previous = previousHtmlViewportMode;
            event.htmlViewportMode.active = DBDN.utils.getActiveHtmlViewportMode();
            event.htmlViewportMode.wasPreviousScaled = wasPreviousHtmlViewportModeScaled;
            event.htmlViewportMode.isActiveScaled = DBDN.utils.isScaledViewport();
        }

        updatePreviousHtmlViewportMode();

        raiseEvent(event);
    });

    function raiseEvent(event) {
        if (event === null) {
            return;
        }

        $(document).trigger(event);
    }

    function updatePreviousHtmlViewportMode() {
        previousHtmlViewportMode = DBDN.utils.getActiveHtmlViewportMode();
    }

    function getEvent(eventName) {
        var event = $.Event(eventName);
        event.htmlViewportMode = {};

        return event;
    }

    function getTabletDesktopViewportChangeEvent() {
        return getEvent('tabletDesktopViewportChange.DBDN_General');
    }

    function getTabletTabletViewportChangeEvent() {
        return getEvent('tabletTabletViewportChange.DBDN_General');
    }

    function getMobileMobileViewportChangeEvent() {
        return getEvent('mobileMobileViewportChange.DBDN_General');
    }

    function getDesktopDesktopViewportChangeEvent() {
        return getEvent('desktopDesktopViewportChange.DBDN_General');
    }
}());
(function () {
    jQuery.fn.getUUID = function () {
        var uuidKey = 'selfUuid';

        return this.data(uuidKey) || null;
    };

    jQuery.fn.generateUUID = function () {
        var uuidKey = 'selfUuid';

        if (this.getUUID() !== null) {
            return this;
        }

        var timestamp = new Date().getTime();

        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (char) {
            var randomNumber = (timestamp + Math.random() * 16) % 16 | 0;
            timestamp = Math.floor(timestamp / 16);

            return (char == 'x' ? randomNumber : (randomNumber & 0x3 | 0x8)).toString(16);
        });

        this.data(uuidKey, uuid);

        return this;
    };

    jQuery.fn.hideForDesktop = function (hide) {
        var className = 'desktop-hidden';

        if (typeof hide !== 'undefined' && !hide) {
            return this.removeClass(className);
        }

        return this.addClass(className);
    };

    jQuery.fn.hideForTablet = function (hide) {
        var className = 'tablet-hidden';

        if (typeof hide !== 'undefined' && !hide) {
            return this.removeClass(className);
        }

        return this.addClass(className);
    };

    jQuery.fn.hideBeforeTransition = function () {
        var className = 'transition-hide';

        return this.addClass(className);
    };

    jQuery.fn.showAfterTransition = function () {
        var className = 'transition-hide';

        return this.removeClass(className);
    };

    jQuery.fn.visibleToTabletOnly = function (conditionFn, isTransitionEnd) {
        var className = 'tablet-only';
        isTransitionEnd = isTransitionEnd || true;

        if (typeof htmlViewportMode === 'undefined') {
            this.addClass(className);
        }

        if (typeof conditionFn === 'function'
            && conditionFn()) {
            this.addClass(className);
        }

        return this.showAfterTransition();
    };
}());
/*\
|*|  :: cookies.js ::
|*|
|*|  A complete cookies reader/writer framework with full unicode support.
|*|
|*|  Revision #1 - September 4, 2014
|*|
|*|  https://developer.mozilla.org/en-US/docs/Web/API/document.cookie
|*|  https://developer.mozilla.org/User:fusionchess
|*|
|*|  This framework is released under the GNU Public License, version 3 or later.
|*|  http://www.gnu.org/licenses/gpl-3.0-standalone.html
|*|
|*|  Syntaxes:
|*|
|*|  * docCookies.setItem(name, value[, end[, path[, domain[, secure]]]])
|*|  * docCookies.getItem(name)
|*|  * docCookies.removeItem(name[, path[, domain]])
|*|  * docCookies.hasItem(name)
|*|  * docCookies.keys()
\*/
var docCookies = {
  getItem: function (sKey) {
    if (!sKey) { return null; }
    return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
  },
  setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
    if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
    var sExpires = "";
    if (vEnd) {
      switch (vEnd.constructor) {
        case Number:
          sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
          break;
        case String:
          sExpires = "; expires=" + vEnd;
          break;
        case Date:
          sExpires = "; expires=" + vEnd.toUTCString();
          break;
      }
    }
    document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
    return true;
  },
  removeItem: function (sKey, sPath, sDomain) {
    if (!this.hasItem(sKey)) { return false; }
    document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
    return true;
  },
  hasItem: function (sKey) {
    if (!sKey) { return false; }
    return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
  },
  keys: function () {
    var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
    for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
    return aKeys;
  }
};
/* END COOKIES */

/* Fix for litebox; issue #10853 */
if ("undefined" !== typeof KeyboardEvent && false === KeyboardEvent.hasOwnProperty('DOM_VK_ESCAPE')) {
    KeyboardEvent.prototype.DOM_VK_ESCAPE = 27;
}

/*******************************************************************************
 * JSON Error Handling
 *******************************************************************************/
/**
 * @returns {JSON}
 */
var JSON_ERROR = {
};

JSON_ERROR.errorResponse2JSON = function(XMLHttpRequest) {
	var fixedResponse = XMLHttpRequest.responseText.replace(/\\'/g, "'");
	var data = JSON.parse(fixedResponse);
	return data;
};

JSON_ERROR.composeMessageFromJSON = function(jsonData, processRespnce) {
	var message = jsonData.message;
	if (processRespnce && jsonData.response !== undefined) {
		for (var key in jsonData.response) {
			if (key === 'length' || !jsonData.response.hasOwnProperty(key)) continue;
			var value = jsonData.response[key];
			if (value instanceof Array) {
				message+= ' ' + value.join('; ');
			} else {
				message+= ' ' + value;
			}
		}
	}
	return message;
};

var imageProcess = {
    imageData : [],
    imagePostData : [],
    galleryData:[],
    galleryPostData:[],
    isImageProccessStart : false,
    isGalleryProccessStart : false,
    isImageProcessingComleted: false,
    start:function(){
        this.isImageProccessStart = false;
        this.isGalleryProccessStart = false;
        this.prepareImageData();
        this.prepareGalleryData();
    },
    prepareImageData :function(){
        $('img.explainer').each(function(index){
            var me = $(this);
            var wrapper = me.closest('.image_wrapper');
            imageProcess.imageData.push({'wrapper': wrapper, 'me': $(this)});
            imageProcess.imagePostData.push(me.attr('data-gallery').split(','));
        });
        this.getData();

        function updateValuesOnDemand() {
            $('img.zoom, img.inline_zoom, img.simple_no_zoom').each(function (index) {
                var me = $(this);
                var wrapper = me.closest('.image_wrapper');
                imageProcess.generateImage(wrapper, me);
            });
        }
        updateValuesOnDemand();

        $(document).on('tabletDesktopViewportChange.DBDN_General tabletTabletViewportChange.DBDN_General', function () {
            updateValuesOnDemand();
        });
        if (!DBDN.utils.isForcedDesktopMode() && isDesktopViewport() && !TABLET) {
            new DBDN.events.WindowResizeEvent(50, function () {
                updateValuesOnDemand();
            });
        }
    },
    prepareGalleryData:function(){
        $('img.gallery').each(function () {
            var me = $(this);
            // More distance craziness!
            me.parent().addClass('single-view-carousel');
            me.wrap('<div class="gallery-main-wrapper"><div class="galCropper"></div></div>');
            if(me.hasClass('first_image')){
                me.parent().addClass('first_image');
            }
            me.parent().append('<div class="galMover" style="display:none;"></div>');
            var wrapper = me.siblings('.galMover');
            imageProcess.galleryData.push({'wrapper': wrapper, 'me': $(this)});
            imageProcess.galleryPostData.push(me.attr('data-gallery').split(','));
        });
        this.getData(true);
    },
    getData: function(isGallery){
        isGallery = typeof isGallery === 'undefined' ? false: true;
        var postData = isGallery === false ? this.imagePostData : this.galleryPostData;
        var galleryCroppersCollection = [];
        if (postData.length > 0) {
            if(isGallery){
                this.isGalleryProccessStart = true;
            }else{
                this.isImageProccessStart = true;
            }
            var data = {
                'files': postData,
                '_token': $('form#searchForm2 input[name=_token]').val()
            };
            $.ajax({
                    type : 'POST',
                    url : paths.gallery,
                    data : data,
                    success : function(json){
                        var data = json.response;
                        for (var i = 0; i < data.length; i++) {
                            if(isGallery){
                                var element = imageProcess.galleryData[i];
                                element.me.attr({'unselectable': 'on',
                                    'border': '0'});
                                var hasClassLazyloadThumb = element.me.hasClass('lazyload-thumb') ? true : false;
                                element.me.removeClass();
                                if (hasClassLazyloadThumb) {
                                    element.me.addClass('lazyload-thumb');
                                }
                                imageProcess.generateGallery(element.wrapper, data[i], element.me, i);
                                element.wrapper.siblings().hide();
                                element.wrapper.show();
                                var $closestGalCropper = element.wrapper.closest('.galCropper');
                                galleryCroppersCollection.push($closestGalCropper);
                                imageProcess.setupCroppers($closestGalCropper);
                                imageProcess.isGalleryProccessStart = false;
                            }else{
                                var element = imageProcess.imageData[i];
                                imageProcess.generateExplainer(element.wrapper, data[i], element.me);
                                imageProcess.isImageProccessStart = false;
                            }
                        }
                        if (isGallery) {
                            $(document).on('tabletDesktopViewportChange.DBDN_General tabletTabletViewportChange.DBDN_General mobileMobileViewportChange.DBDN_General', function () {
                                $.each(galleryCroppersCollection, function () {
                                    imageProcess.setupCroppers(this, false);
                                });
                            });

                            $(document).trigger('singleImageGalleryCarouselLoaded.DBDN_General');
                        }
                        if(!imageProcess.isImageProccessStart && !imageProcess.isGalleryProccessStart){
                            $(document).trigger('imagesLoaded');
                            imageProcess.isImageProcessingComleted = true;
                        }
                    }
            });
        }else if(isGallery && !imageProcess.isImageProccessStart && !imageProcess.isGalleryProccessStart){
            $(document).trigger('imagesLoaded');
            imageProcess.isImageProcessingComleted = true;
        }
    },
    generateExplainer: function (container, data, img) {
        var imgAtag = img.closest('a');
        var explainerTitle = imgAtag.attr('id');
        explainerTitle = explainerTitle.substring(0,explainerTitle.lastIndexOf("_"));
        var newHTML = '';
        var images = data.images;
        var lowresSrc, lowresSrc2x, image;
        var files_str = '';
        $.each(img.attr('data-gallery').split(','), function (index, value) {
            files_str += '&files[]=' + value;
        });
        img.removeAttr('data-gallery');
        for (var z = 0; z < images.length; z++) {
            image = images[z];
            lowresSrc = image['lowres'];
            //lowresSrc2x = image['lowres2x'];
            if (!lowresSrc) {
                lowresSrc = image['path'];
                //lowresSrc2x = image['path'];
            }
            if (z == 0) {
                imgAtag.attr('href', relPath + 'explainer?_token=' + $('form#searchForm2 input[name=_token]').val() + files_str);
                imgAtag.attr('data-info', image['descr'].replace(/"/g, '&quot;'));
            }else{
                newHTML = '<div class="image_container" style="position:absolute;' +
                    '"><a class="explainer_layover" rev="scrolling:no;" rel="lyteframe" href="' + relPath + 'explainer?_token=' + $('form#searchForm2 input[name=_token]').val() + files_str + '" id="' + explainerTitle + '_' + z + '" unselectable="on" data-info="' + image['descr'].replace(/"/g, '&quot;') + '">\
                    </a></div>';
                container.find('.image_text').before(newHTML);
                var el = $(container).find('.explainer_gallery .explainer_layover:last');
                //el.append('<img src="' + lowresSrc + '" srcset="' + lowresSrc + ', ' + lowresSrc2x + ' 2x" border="0" unselectable="on" />');
                el.append('<img src="' + lowresSrc + '" border="0" unselectable="on" />');
            }
        }
        initLytebox();
    },
    _positioningImageText:function($container, img) {
        var parent = $container.find('.image_item');
        var text = parent.find('.image_text');
        var left = parent.find('a').outerWidth();
        if ($('body').hasClass('smart')) {
            if (left !== null) {
                text.css('width', left + 'px');
            }
        } else {
            if (isTabletViewport()) {
                text.css('width', ($container.outerWidth() - img.outerWidth()) + 'px');
            } else {
                text.css('width', '');
            }
            if (left !== null) {
                if (parent.hasClass("right_image")) {
                    left = parent.outerWidth() - (text.outerWidth() + left);
                }
                text.css('left', left + 'px');
            } else {
                console.debug('Wrong left: ' + left);
            }
        }
    },
    generateImage:function(container, img) {
        var $container = $(container);
        var title = $container.find('a').attr('data-title');
        if(title && title.length > 0){
            var cleartitle = title.replace(/<\s*br\s*\/?\s*>|<\s*\/p\s*>/gi, "\n").replace(/&nbsp;/gi, ' ').replace(/<\s*h1\s*>.*<\s*\/\s*h1\s*>|<[^>]+>/gi, '');
            cleartitle = $('<textarea />').html(cleartitle).text();
            $container.find('a').attr('title', cleartitle);
        }
        if (img.height() > img.width() && !isTabletViewport()) {
            $container.css('width', '740px');
        } else {
            $container.css('width', '');
        }
        imageProcess._positioningImageText($container, img);

        initLytebox();
    },
    generateGallery:function(container, data, img, index) {
        img.removeAttr('data-gallery');
        var arrowTop = Math.ceil(img.height()/2) + 7;
        var counterTop = MOBILE ? img.height() + 10 : img.height() - 94;
        var imgWidth = $(window).width() - 40;
        var now = new Date();
        var galleryTitle = 'lytebox[gallery_'+now.getTime()+'_'+ index +']';
        var newHTML = '';
        var images = data.images;
        var lowresSrc, lowresSrc2x, image;
        var imgToAppend = img;
        /*
            Quick and rudimentary fix

            On devices with DPR > 1x the img argument remains a reference to
            the default (1x) image, and as a consequence, the newly loaded higher
            quality image (Nx) overlays the gallery component and interferes with
            its navigation.

            The fix is the 8 lines below this comment/note.
        */
        var imgToCompareWith = container.prev('img');
        if (imgToAppend[0] != imgToCompareWith[0]) {
            imgToAppend = imgToCompareWith.removeClass();

            // If the 1x image wasn't removed by the setResolutionPictures method for any
            // reason, try to remove it again!
            img.remove();
        }
        //container.html(newHTML);
        for(var z=0; z < images.length; z++){
            image=images[z];
            lowresSrc = image['lowres'];
            lowresSrc2x = image['lowres2x'];
            if (!lowresSrc) {
                lowresSrc = image['path'];
                lowresSrc2x = lowresSrc;
            }

                var info = '<div>'+image['descr']+'</div>';
                var img_header = info;
                var descr = image['descr'].replace(/"/g, '&quot;');
                newHTML='<div class="galItem '+(z == 0 ? 'shown':"")+'" unselectable="on">\
                    <div class="image_container">\
                     <a class="gallery_layover" href="'+image['path']+'" rel="'+galleryTitle+'" data-title="'+ descr +'" unselectable="on" >\
                    </a></div>'+
                    ((image['descr'] != '' && typeof(image['descr']) != 'undefined') ?'<div class="imgText961">'+ img_header+'</div>':"");

                newHTML+='</div>';
                container.append(newHTML);
                var el = $(container).find('.galItem:last a.gallery_layover');
                if(image['descr'].length > 0){
                    var title = image['descr'].replace(/<\s*br\s*\/?\s*>|<\s*\/p\s*>/gi, "\n").replace(/&nbsp;/gi, ' ').replace(/<\s*h1\s*>.*<\s*\/\s*h1\s*>|<[^>]+>/gi, '');
                    title = $('<textarea />').html(title).text();
                    $(el).attr('title', title);
                }

                if (z > 0) {
                    //el.append('<img src="' + lowresSrc + '" srcset="' + lowresSrc + ', ' + lowresSrc2x + ' 2x" border="0" unselectable="on" />');
                    imgToAppend = $('<img src="' + lowresSrc + '" border="0" unselectable="on" class="ready-for-enhanced-resolution" data-check="04" />');
                }
                if(isMobileViewport()){
                   var ratio = parseInt(imgToAppend.attr('width')) / parseInt(imgToAppend.attr('height'));
                   var height = (imgWidth / ratio).toFixed(2);
                    imgToAppend.css({"width":imgWidth + "px", 'height': height + 'px'});
                }
                el.append(imgToAppend);
        }
        if (images.length > 1) {
            newHTML = '<div class="carouselNav">\
                            <a href="javascript:;" class="carouselLeft" style="top:'+ arrowTop +'px"></a>\
                            <div class="carouselCounter" style="top:'+ counterTop +'px"><span>1</span> / ' + images.length + '</div>\
                            <a href="javascript:;" class="carouselRight" style="top:'+ arrowTop +'px"></a>\
                        </div>';
        }
        container.closest('.galCropper').after(newHTML);
        container.closest('.single-view-carousel').css({ 'position': 'relative', 'line-height': '0px' });
       initLytebox();
    },
    setupCroppers: function ($croppers, bindEvents) {
        if (typeof bindEvents === 'undefined') {
            bindEvents = true;
        }
        var maxWidth = 0;
        var $me = $croppers.find('.galItem:first a img').first();
        var $dad = $me.closest('.image_container');
        var $grand = $dad.closest('.galCropper');
        var $firstImgDescr = $croppers.find('.galItem:first .imgText961');
        var $carouselNav = $croppers.next('.carouselNav');
        var $carouselCounter = $carouselNav.find('.carouselCounter')

        var counterMobileTop = $me.height() + 25;
        if (MOBILE && $firstImgDescr.outerHeight(true) > 50) {
            var counterMobileTop = $me.height() + 40;
        }
        var counterTop = MOBILE ? counterMobileTop : $me.height() - 94;
        $carouselCounter.css('top', counterTop + 'px');

        if ($dad.index() == 0) {
            var cropperHeight = $dad.closest('.galItem').outerHeight(true);
            if (MOBILE && cropperHeight < counterTop + $carouselCounter.outerHeight(true)) {
                cropperHeight = counterTop + $carouselCounter.outerHeight(true);
            }
            $grand.height(cropperHeight);
            $grand.width($dad.closest('.galItem').outerWidth(true));
            //$grand
            //  .width($me.width())
            //  .height($dad.outerHeight(true));
        }
        //$me.parent().width($me.width());
        //$dad.width($me.width());

        var $mover = $dad.closest('.galMover');
        var curW = 0;
        $mover.find('.galItem > .image_container').each(function () {
            //$(this).css('left', curW+'px');
            //curW += $(this).outerWidth();
            curW += Math.round10($(this).get(0).getBoundingClientRect().width, -2);
        });

        var moverLeftPosition = 0;

        if ($mover.length > 0) {
            moverLeftPosition = $mover.position().left;
        }

        var previousItemWidth = $mover.data('previousWidth');
        var moverLeftPositionFactor = 1;

        if (typeof previousItemWidth !== 'undefined') {
            moverLeftPositionFactor = $dad.width() / (previousItemWidth | 0);
        }

        $mover.width(Math.ceil10(curW, 0));

        if (Math.abs(moverLeftPosition) > 0) {
            $mover.css('left', moverLeftPosition * moverLeftPositionFactor + 'px');
        }

        $mover.data('previousWidth', $dad.width());

        var $carouselRight = $carouselNav.find('.carouselRight');
        var $carouselLeft = $carouselNav.find('.carouselLeft');
        if (bindEvents) {
            $carouselRight.on('click', this.showNextGalleryItem);
            $carouselLeft.on('click', this.showPrevGalleryItem);

            $croppers.on("swipeleft", this.showNextGalleryItem);
            $croppers.on("swiperight", this.showPrevGalleryItem);
        }

        // position the carousel navigation
        var arrowTop = Math.ceil($me.height()/2) + 7;
        $carouselRight.css('top', arrowTop + 'px');
        $carouselLeft.css('top', arrowTop + 'px');

        $croppers.trigger('afterGallerySetupCroppers.DBDN_General');
    },

    showPrevGalleryItem : function(){
        var $btn = $(this);
        if ($btn.hasClass('galCropper')) {
            $btn = $btn.next('.carouselNav').find('.carouselLeft')
        }
        var $cropper = $btn.closest('.carouselNav').prev('.galCropper');
        var $mover = $cropper.find('.galMover');
        $mover.stop(true, true);
        // This is needed if you want to animate images with various sizes
        $mover.closest('.galCropper').stop(true, true);
        var cLeft = parseInt( $mover.css('left'), 10);
        var $item = $cropper.find('.galItem.shown');
        var $carouselCounter = $cropper.next().find('.carouselCounter');
        var minMobileHeight = parseInt($carouselCounter.css('top')) + $carouselCounter.outerHeight(true);


        if ($item.prev().length) {
            $mover.animate({'left': cLeft + $item.prev().width()}, 400);
            var newHeight = $item.prev().height();
            if (MOBILE) {
                newHeight = newHeight < minMobileHeight ? minMobileHeight : newHeight;
            }
            // A comment would be nice here, explaining what is the purpose of animating
            // the cropper element.
            // UPDATE (15.1.2018): This is used to smooth the transition when the container is resized due
            // to images with different sizes.
            // UPDATE (16.1.2018): As addition to the above, the container size is also affected by the text size.
            //$mover.parent().animate({height:newHeight, width:$item.prev().width()},400);
            // For the time being assume that only the height of the container will be resized.
            $mover.closest('.galCropper').animate({ height: newHeight }, 400);
            $item.removeClass('shown');
            $item.prev().addClass('shown');
            $btn.closest('.carouselNav').find('.carouselCounter').html('<span>' + parseInt($item.closest('.galMover').find('.galItem').index($item.prev()) + 1) + '</span> / ' + $item.closest('.galMover').find('.galItem').length);
        }else{
            var $last = $item.siblings().last();
            $item.removeClass('shown');
            $last.addClass('shown');
            var childWidth = $item.outerWidth(true);
            //var childWidth = 0;
            $item.siblings().each(function() {
                childWidth += $(this).outerWidth( true );
            });
            childWidth -= $last.outerWidth(); // correction
            $mover.animate({'left': 0-childWidth}, 400);
            var newHeight = $last.height();
            if (MOBILE) {
                newHeight = newHeight < minMobileHeight ? minMobileHeight : newHeight;
            }
            // See the comment in the IF block.
            $mover.closest('.galCropper').animate({ height: newHeight }, 400);
            $btn.closest('.carouselNav').find('.carouselCounter').html('<span>' + $item.closest('.galMover').find('.galItem').length + '</span> / ' + $item.closest('.galMover').find('.galItem').length);
        }
    },

    showNextGalleryItem : function(){
        var $btn = $(this);
        if ($btn.hasClass('galCropper')) {
            $btn = $btn.next('.carouselNav').find('.carouselRight')
        }
        var $cropper = $btn.closest('.carouselNav').prev('.galCropper');
        var $mover = $cropper.find('.galMover');
        $mover.stop(true, true);
        // See the comment in the showPrevGalleryItem method
        $mover.closest('.galCropper').stop(true, true);
        var cLeft = parseInt($mover.css('left'), 10);
        var $item = $cropper.find('.galItem.shown');
        var $carouselCounter = $cropper.next().find('.carouselCounter');
        var minMobileHeight = parseInt($carouselCounter.css('top')) + $carouselCounter.outerHeight(true);

        if ($item.next().length) {
            $item.removeClass('shown');
            $item.next().addClass('shown');
            $mover.animate({'left': cLeft - $item.width()}, 400);
            var newHeight = $item.next().height();
            if (MOBILE) {
                newHeight = newHeight < minMobileHeight ? minMobileHeight : newHeight;
            }
            // See the comment in the showPrevGalleryItem method
            $mover.closest('.galCropper').animate({ height: newHeight }, 400)
            $btn.closest('.carouselNav').find('.carouselCounter').html('<span>' + parseInt($item.closest('.galMover').find('.galItem').index($item.next()) + 1) + '</span> / ' + $item.closest('.galMover').find('.galItem').length);
        }else{
            $mover.animate({'left': 0}, 400);
            var $first = $item.siblings().first();
            $item.removeClass('shown');
            $first.addClass('shown');
            var newHeight = $first.height();
            if (MOBILE) {
                newHeight = newHeight < minMobileHeight ? minMobileHeight : newHeight;
            }
            // See the comment in the showPrevGalleryItem method
            $mover.closest('.galCropper').animate({ height: newHeight }, 400)
            $btn.closest('.carouselNav').find('.carouselCounter').html('<span>1</span> / ' + $item.closest('.galMover').find('.galItem').length);
        }
    },

    setResolutionPictures: function (section, force) {
        if (window.matchMedia) {
            if (typeof (section) === 'undefined') {
                section = 'body';
            }
            if (typeof (force) === 'undefined') {
                force = false;
            }

            var factor = 1;
            if( window.matchMedia("(min-resolution: 3dppx)").matches ||
                window.matchMedia("(-webkit-min-device-pixel-ratio: 3)").matches ||
                window.matchMedia("(min--moz-device-pixel-ratio: 3)").matches ||
                window.matchMedia("(-o-min-device-pixel-ratio: 3/1)").matches ||
                window.matchMedia("(min-device-pixel-ratio: 3) ").matches ||
                window.matchMedia("(min-resolution: 288dpi)").matches){
                factor = 3;
            }else if( window.matchMedia("(min-resolution: 2dppx)").matches ||
                      window.matchMedia("(-webkit-min-device-pixel-ratio: 2)").matches ||
                      window.matchMedia("(min--moz-device-pixel-ratio: 2)").matches ||
                      window.matchMedia("(-o-min-device-pixel-ratio: 2/1)").matches ||
                      window.matchMedia("(min-device-pixel-ratio: 2)").matches ||
                      window.matchMedia("( min-resolution: 192dpi)").matches){
                factor = 2;
            }

            if (factor != 1) {
                var image_selector = '.ready-for-enhanced-resolution';
                if (force) {
                    image_selector = '.content-img';
                }
                var imgs = $(section).find(image_selector);
                imgs.each(function (index) {
                    var $cur_img = $(this);
                    $cur_img.removeClass('ready-for-enhanced-resolution');

                    //TODO: Recalculate image qiuality on rotation
                    var cur_img_factor = factor;
                    if (MOBILE && cur_img_factor > 1) {
                        var curent_orientation = getScreenOrientation();
                        var aspectRatio = $cur_img.width() / $cur_img.height();

                        if (curent_orientation === orientation.portrait && aspectRatio < 1) {
                            ++cur_img_factor;
                        } else if (curent_orientation === orientation.landscape && aspectRatio >= 1) {
                            ++cur_img_factor;
                        }
                    }

                    if ($cur_img.data('loadedDPIFactor') == cur_img_factor || $cur_img.attr('src') == '') {
                        return true;
                    }

                    if (cur_img_factor === 1) {
                        // Skeep this image for hi resolution.
                        return true;
                    }

                    var img = $('<img>');
                    //var svg = $(this).next();

                    if ($cur_img.attr('style') != '') {
                        img.attr('style', $cur_img.attr('style') + ';display:none;');
                    }else{
                        img.attr('style', 'display:none;');
                    }
                    if ($cur_img.hasClass('content-img')) {
                        img.attr('class', $cur_img.attr('class'));
                    } else {
                        img.attr('class', $cur_img.attr('class') + ' content-img');
                    }

                    if ($cur_img.attr('id') != "") {
                        img.attr('id', $cur_img.attr('id'));
                    }
                    if ($cur_img.attr('data-hotspot') != "") {
                        img.attr('data-hotspot', $cur_img.attr('data-hotspot'));
                    }
                    img.attr('border', '0');
                    img.attr('unselectable', 'on');
                    img.load(function () {
                        var $loaded_img = $(this);
                        if(window.location.search.indexOf('debugDPIImages') > -1){
                            $loaded_img.parent().addClass('debug_images_x' + cur_img_factor);
                        }
                        $loaded_img.prev().remove();
                        $loaded_img.show();
//                        if(svg.is('#toBeBlured')){
//                            var svgImg =  svg.find('image');
//                            $(svgImg).attr('xlink:href', $loaded_img.attr('src'));
//                            $(svgImg).attr('width', $loaded_img.width());
//                            $(svgImg).attr('height', $loaded_img.height());
//                        }
                    });

                    var img_src = $cur_img.attr('data-original');
                    if (!img_src) {
                        img_src = $cur_img.attr('src');
                    }

                    img.attr('data-original', img_src);

                    var img_src_arr = img_src.split('/');
                    if(img_src_arr.indexOf('__cache') != -1){
                        var cache_index = img_src_arr.indexOf('__cache') + 1;
                        img_src_arr[cache_index] = img_src_arr[cache_index] + '~' + cur_img_factor + 'x';
                    }else if(img_src_arr.indexOf('protected-file') != -1){
                        var cache_index = img_src_arr.indexOf('protected-file') + 2;
                        if(img_src_arr[cache_index] != 'thumb' && img_src_arr[cache_index] != 'doc'){
                            var split_query = img_src_arr[cache_index].split('?');
                            img_src_arr[cache_index] = split_query[0] + '~' + cur_img_factor + 'x?' + split_query[1];
                        }
                    }
                    img.attr('src', img_src_arr.join('/'));
                    img.data('loadedDPIFactor', cur_img_factor);
                    $cur_img.after(img);
                });
            }
        }
    },

    lazyLoadWithCallBack: function (images){
        if(!images.length){
            if (!imageProcess.isImageProcessingComleted) {
                imageProcess.start();
            }else{
                $(document).trigger('imagesLoaded');
            }
            return false;
        }

        var fi = images.first();
        var isFirst = fi.closest('#fullsize_banner').length > 0? true: false;
        var settings = {
            effect: "show",
            effectTime: 0,
            event: "scroll",
            threshold: -120,
            load: function() {
                var $img = $(this);
                if ($img.hasClass('lazyload-thumb')) {
                    $img.removeClass('lazyload-thumb');
                    //Fix border of the image.
                    $img.closest('a.image_layover').width('auto');

                    $img.addClass('ready-for-enhanced-resolution');
                    // trigger the loading of the scaled (x2, x3) image
                    $(document).trigger('imagesLoaded');
                    if(isFirst) {
                        $('#coverBackground').show();
                    }
                    var $parentCropper = $img.closest(".galCropper");
                    if ($parentCropper.length > 0) {
                        imageProcess.setupCroppers($parentCropper, false);
                    }
                }
            },
            error: function(e) {
                var $img = $(this);
                console.log('Error: Image load: ' + $img.attr('src') + ' error ' + e);
            }
        };

        // Handle an edge case when the device orientation changes from tablet to desktop
        // and sets the original width and height.
        if(isDesktopViewport() && TABLET && fi.closest('#content_main').length > 0){
            fi.attr('width', fi.data('original_width'));
            fi.attr('height', fi.data('original_height'));
        }

        fi.lazyload(settings);

        // Init lazyLoad for next image
        images = images.slice(1);
        this.lazyLoadWithCallBack(images);
    },

    updateLazyload: function () {
        setTimeout(function () { $(window).trigger('scroll'); }, 200);
    }

};

var vertical_navigation = {
    marker_offsets : new Array(),
    marker_select_disable: false,
    marker_selectd: false,
    hasDocumentReadyBeenCalled: false,
    hasWindowLoadedBeenCalled: false,

    windowLoad: function(initialCall){

        this.markerSelectInit(initialCall);
        if (initialCall) {
            this.hasWindowLoadedBeenCalled = true;

            if(document.location.href.lastIndexOf('#') > -1){
                var anch = document.location.href.substr(document.location.href.lastIndexOf('#') + 1);
                if (anch.length !== 0) {
                    this.marker_selectd = anch;
                }
            }
        }
        if (this.marker_selectd) {
            var anchor = $('#cd-vertical-nav ul.dot_items li.dots').find('a#'+this.marker_selectd);
            if (anchor.length > 0) {
                anchor.click();
//                $('header, #basic_nav_closed').sticky(false);
            }
        }
    },

    documentReady: function(){

        // disable anchor nav on mobile
        //if(MOBILE)
        //    return;

        var that = this;

        $('#cd-vertical-nav #add-icon a,#cd-vertical-nav #remove-icon a').click(function(){
            $('#watchlistForm').submit();
            return false;
        });
        $('#cd-vertical-nav #rate-icon a').click(function(){
            if($(this).parent().hasClass('unactive')){
                $("#notification .alert_text").html(js_localize['page.rated'])
                generateNotification();
            }else{
                $('#rateForm').submit();
            }
            return false;
        });

        if(isDesktopViewport()){
            this.createAnchorNav();
            /*** Hide right navigation ***/
            addPassiveEventListener("scroll", function() {
                that.checkOffset();
            });

            this.hasDocumentReadyBeenCalled = true;
            }
            //this.windowLoad(true);
        },

    markerMap : function(){
        var that = this;

        this.marker_offsets = new Array();

        $('#cd-vertical-nav .dot_items a').each(function() {
            var target = $('section #' + $(this).attr('id') + '.block_title');
            var new_offset = Math.floor(target.offset().top) - 50 ;//- $('header.no-sticky').height(); //header no-sticky height + header sticky height
            that.marker_offsets.push(new_offset);
        });
    },

    markerSelectInit : function(initialCall){
        var that = this;

        that.markerMap();
        if (initialCall) {
            $(window).bind('myScrollEvent', function() {
                that.markerSelect();
            });

            // Use custom Event handling instead of $(window).scroll(function(){
            addPassiveEventListener("scroll", function() {
                $(window).trigger('myScrollEvent');
            });
        }
    },

    markerSelect : function(){
        if (this.marker_select_disable) {
            // Skip the selection!
            return;
        }

        var current_anchor_index = 0,
            scrollTop = $(window).scrollTop(),
            sticky_nav = $("header.sticky"),
            top_nav = $("div#topNotifications");

        if (!MOBILE && top_nav.length && top_nav.is(":visible")) {
            scrollTop += top_nav.outerHeight();
        }

        if(sticky_nav.length && sticky_nav.is(":visible")) {
            scrollTop += sticky_nav.position().top + sticky_nav.outerHeight();
        }

        while (current_anchor_index < this.marker_offsets.length && scrollTop >= this.marker_offsets[current_anchor_index]) {
            current_anchor_index++;
        }

        if (current_anchor_index === 0 && scrollTop < this.marker_offsets[current_anchor_index]) {
            current_anchor_index = -1;
        } else {
            current_anchor_index -= 1;
        }
        var active_anchor = $('#cd-vertical-nav .dot_items a.is-selected');

        if (current_anchor_index >=0 && current_anchor_index < this.marker_offsets.length) {

            var active_anchor_index = active_anchor.parent().index();

            if (current_anchor_index !== active_anchor_index) {
                active_anchor.removeClass('is-selected');
                var activeLink = $('#cd-vertical-nav .dot_items li').eq(current_anchor_index).find('a');
                activeLink.addClass('is-selected');
                if('replaceHash' in window)
                    window.replaceHash(activeLink.attr('id'));
            }
        } else {
            active_anchor.removeClass('is-selected');
            console.log("Out of range: " + current_anchor_index);
        }
    },

    createAnchorNav: function (){

        var that = this;

        if($('#cd-vertical-nav').length > 0){
            if($('img.anchor_nav').length > 0 || $('.block_title').length > 0 ) {
                $('#cd-vertical-nav ul.dot_items').show();
            }
            var link = $('#topLinks #language a').attr('href');
            if(link != undefined){
                link = link.substr(0, link.length - 2)+lang_label;
            }else{
                link = window.location.href+ '?lang=' + lang_label;
            }

            if(!($('.block_title').length < 2 && $('.downloads_overview').length > 0)){
                $('.block_title').each(function(index) {
                    var _this = $(this),
                        id = _this.attr('id').replace(/\-\d+$/,'').trim(),
						// Remove Counter [X]
                        txt = _this.html().trim().replace(/\[\d+\]$/,'');
                        // Replace Double dagger and dot
                        txt = txt.replace(/[\u2021\.]/g,'').trim();

                    _this.attr('id',id);

                    var html = '<li class="dots">\
                                <a href="'+link+'#'+id+'" id="'+id+'">\
                                    <span class="cd-dot"></span>\
                                    <span class="cd-label">'+txt+'</span>\
                                </a>\
                            </li>';

                    if ($('#cd-vertical-nav ul.dot_items li.dots').length > 0) {
                        $('#cd-vertical-nav ul.dot_items li.dots').last().after(html);
                    } else {
                        $('#cd-vertical-nav ul.dot_items').prepend(html);
                    }
                });
            }

            $('img.anchor_nav').each(function(index) {

                var _this = $(this),
                    id_attr = uriEncode(_this.data('text'));

                _this.addClass('block_title').attr('id',id_attr);

                var html = '<li class="dots">\
                            <a href="'+link+'#'+id_attr+'" id="'+id_attr+'">\
                                <span class="cd-dot"></span>\
                                <span class="cd-label">'+_this.data('text')+'</span>\
                            </a>\
                        </li>';

                if($('#cd-vertical-nav ul.dot_items li.dots').length > 0) {
                    $('#cd-vertical-nav ul.dot_items li.dots').last().after(html);
                } else {
                    $('#cd-vertical-nav ul.dot_items').prepend(html);
                }
            });

            $('li.dots a').click(function(){
                //$(window).unbind('myScrollEvent');
                that.marker_select_disable = true;
                $('li.dots a').removeClass('is-selected');
                $(this).addClass('is-selected');

                $('section #' + $(this).attr('id')).scrollTo(function () {
//                    $(window).bind('myScrollEvent', function(){that.markerSelect();});
                    // TODO: Clear scroll events
                    // Skip the events
                    setTimeout(function() {that.marker_select_disable = false;}, 60);
                });
                return false;
            });
        }
    },

    checkOffset: function() {
        var vn = $('#cd-vertical-nav');

        if(vn.length > 0){
            if(vn.offset().top + vn.height() >= $('.footer_content').offset().top )
                $('#cd-vertical-nav, .r-links, .related-links-right').css('visibility', 'hidden');
            else
                vn.css('visibility', 'visible');
        }
    }
};

$(window).load(function(){

    var initNav = true;

//    $(document).on('imagesLoaded', function(){
        // disable anchor nav on mobile and tablet
        var scroolToAnchor = isDesktopViewport()? true : false;
        if(scroolToAnchor){
            vertical_navigation.windowLoad(initNav);
        }
        initNav = false;
//    });

    /*Hack e-tracker for braking escape on IE*/
    var old_et_escape = ('function' === typeof window.et_escape) ? window.et_escape : null;
    window.et_escape = function(){
        if(old_et_escape && 1 === arguments.length && 'function' === typeof window.escape)
            return old_et_escape(arguments[0]);
        return 27;
    };

});

jQuery(document).ready(function($){

    $(document).on('imagesLoaded', function () {
        imageProcess.setResolutionPictures();
    });

    $(document).on('mobileMobileViewportChange.DBDN_General', function () {
        imageProcess.setResolutionPictures('body', true);
    });

    $('a#save-icon_button').click(function (e) {
        e.preventDefault();
        DBDN.utils.downloadFileAndDisplayNotificationWhileBeingDownloaded(this,js_localize['pdf.download']);

        return false;
    });

    $("#alerts .ok_button, #alerts .close_button, #alertsBG").on("click", function(){
        $("#alerts, #alertsBG").fadeOut(300,function(){$("#alerts").removeClass('red'); $("#alerts").removeClass('center');});
    });

    if(MOBILE) {
        // $("section.basic_nav_opened.filters.best_practice > div.basic_nav").on("click", ".menu_label",function(){
        $("div.basic_nav").on("click", ".menu_label", switchMobileFilterMenu);
    }

    var imgs = $('img.lazyload-thumb');
    imageProcess.lazyLoadWithCallBack(imgs);
    imageProcess.updateLazyload();
    setTimeout(imageProcess.updateLazyload, 3000); // Tontchev complains that the header image sometimes doesn't get loaded

    $('.slider-item, .front_slider').bind('wheel', imageProcess.updateLazyload);
    $(document).on('touchend', '.slider_container, .grid-3cols > .inner1-wrapper', imageProcess.updateLazyload);
    $('.next.btn_navigation, .prev.btn_navigation, .next.slider_hover, .prev.slider_hover').click(imageProcess.updateLazyload);

    (function () {
        var vcp = docCookies.hasItem('viewed_cookie_policy') ? docCookies.getItem('viewed_cookie_policy') : '';

        if (!vcp || (-1 == vcp.split(',').indexOf(lang_label))) {
            var yearFromNow = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
            var loc_lang_label = (typeof lang_label !== "undefined") ? lang_label : '';
            var newVcp = vcp ? vcp + ',' + loc_lang_label : loc_lang_label;
            var secure = -1 === window.location.hostname.indexOf('192');
            docCookies.setItem('viewed_cookie_policy', newVcp, yearFromNow, '/', null, secure);
            $("#cookie_notification").show();//fadeIn(300);

            $("#cookie_notification .ok_button, #cookie_notification .close_button").on("click", function(){
                $("#cookie_notification").fadeOut(300, calcMessTops);
            });
        }
    })();


    generateNotification();


    $('#topLinks #privacy').click(function(){
        $("#provider_privacy, #alertsBG").fadeIn(300);
        $("#provider_privacy .ok_button, #provider_privacy .close_button, #alertsBG").on("click", function() {
            $("#provider_privacy, #alertsBG").fadeOut(300, function() {
                $("#provider_privacy").removeClass('red');
            });
        });
        return false;
    });

    $('.middle_page_text a, .row a').each(function(){
        if($(this).attr('href') != '' && typeof($(this).attr('href')) != 'undefined'){
            if(isExternal($(this).attr('href'))){
                $(this).addClass('externallink');
            }
        }
    });

    $("a.externallink").click(function(e) {
        e.preventDefault();
        var url = encodeURIComponent($(this).attr('href'));
        location.href = relPath + 'external?url=' + url
    });

    $("#searchForm_faq input#search,#searchForm input#search,input#search2").each(function () {
        var $this = $(this);
        var $parent = $this.parent();
        var classPerInstance = '';
        var classPerInstanceSuffix = '_ac';

        if ($parent.length > 0 && $parent[0].id) {
            classPerInstance = $parent[0].id + classPerInstanceSuffix;
        }

        if ($parent.length < 1 || ($parent.length > 0 && !$parent[0].id)
                && this.id) {
            classPerInstance = this.id + classPerInstanceSuffix;
        }

        $this.autocomplete({
            minLength: 2,
            position: { my: "left top", at: "left bottom", collision: "none" },
            source: function (request, response) {
                var term = request.term.toLowerCase(),
                        element = this.element,
                        cache = this.element.data('autocompleteCache') || {},
                        foundInCache = false;

                var type = 'page';
                var urlParams = '';
                if ('searchForm_faq' === this.element.parent().attr('id')) {
                    type = 'faq';
                    var actionParts = this.element.parent().attr('action').split('/');
                    urlParams = '/' + actionParts[actionParts.length - 1];
                }

                $.each(cache, function (key, data) {
                    if (term.indexOf(key) === 0 && data.length > 0) {
                        var result = jQuery.grep(data, function (n) {
                            return (n.value.toLowerCase().indexOf(term) >= 0);
                        });
                        response(result);
                        foundInCache = true;
                        return;
                    }
                });
                if (foundInCache)
                    return;
                request._token = $(element).parent().find('input[name="_token"]').val();
                $.ajax({
                    url: relPath + 'tag/autocomplete/' + type + urlParams,
                    type: 'POST',
                    dataType: "json",
                    data: request,
                    success: function (data) {
                        var data_arr = new Array();
                        for (var key = 0; key < data.response.length; key++) {
                            tag_value = $('<div/>').html(data.response[key].value).text();
                            if ('faq' == type) {
                                tag_value = tag_value.split('[')[0].trim();
                            }
                            data_arr[key] = {value: tag_value, tag_id: $('<div/>').html(data.response[key].id).text()};
                        }
                        cache[term] = data_arr;
                        element.data('autocompleteCache', cache);
                        response(data_arr);
                    }
                });
            },
            select: function (event, ui) {
                $(this).val(ui.item.value);
                $(this).parent().submit();
                return false;
            }
        })
        .autocomplete('instance')
        .menu
        .element.addClass(classPerInstance);
    });

    $("input#search2").autocomplete('option', 'position',
        { my: "left top", at: "left-20 bottom", collision: "none" });

    /*
    if($(".back-button").length > 0){
        $('.basic_nav_closed').css('margin-top', '67px');
    }else{
        $('.basic_nav_closed').css('margin-top', '16px');
        $('.basic_nav_opened').css('margin-top','-53px');
    }
    */

    $('.scrollbar-rail').scrollbar({
        "autoUpdate": true
    });
//    if($('#fullsize_banner').lenght > 0){
//        var img = $('#fullsize_banner').find('img');
//    }
    if($('.coverBackground').length > 0){
        $(window).scroll(function(){
            calculateOpacity($('#fullsize_banner .simple, #fullsize_banner .background, #fullsize_banner .text'));
        });
    }

    // disable anchor nav on mobile and tablet
    var scroolToAnchor = isDesktopViewport() || isTabletViewport()? true : false;
    if(scroolToAnchor){
        vertical_navigation.documentReady();
    }

    $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
        DBDN.utils.toggleTabletDesktopEvent(function () {
            if (!vertical_navigation.hasDocumentReadyBeenCalled) {
                vertical_navigation.documentReady();

                if (!vertical_navigation.hasWindowLoadedBeenCalled) {
                    vertical_navigation.windowLoad(true);
                }
            } else {
                vertical_navigation.markerMap();
            }
        }, function () {
        }, ev.htmlViewportMode);
    });

    //validate register form!
    if($('#profileForm #iagree').length > 0){
        DBDN.utils.validateProfileForm('#profileForm');
    // validate profile form and remove password field from requarements
    }else{
        DBDN.utils.validateProfileForm('#profileForm', ['password', 'password_confirmation'] );
    }
});

//Force the load of icons for the footer
(function () {
    $.each(['help', 'glossary', 'faq', 'sitemap'], function () {
        var img = new Image();
        img.src = relPath + 'img/template/svg/' + this + '-hover.svg';
    });
    if(isMobileViewport()){
        $.each(['plus_bold_b', 'plus_bold_w', 'minus_bold_b', 'minus_bold_w'], function () {
            var img = new Image();
            img.src = relPath + 'img/template/svg/' + this + '.svg';
        });
    }
}());


///Polyfill for find
// https://tc39.github.io/ecma262/#sec-array.prototype.find
if (!Array.prototype.find) {
  Object.defineProperty(Array.prototype, 'find', {
    value: function(predicate) {
     // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If IsCallable(predicate) is false, throw a TypeError exception.
      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }

      // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
      var thisArg = arguments[1];

      // 5. Let k be 0.
      var k = 0;

      // 6. Repeat, while k < len
      while (k < len) {
        // a. Let Pk be ! ToString(k).
        // b. Let kValue be ? Get(O, Pk).
        // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
        // d. If testResult is true, return kValue.
        var kValue = o[k];
        if (predicate.call(thisArg, kValue, k, o)) {
          return kValue;
        }
        // e. Increase k by 1.
        k++;
      }

      // 7. Return undefined.
      return undefined;
    }
  });
}
//===========

function isExternal(url) {
    var internal_hosts = [location.host,"www.daimler.com","intra.corpintra.net"];
    var internal_protocols = ["http:","https:", "mailto:"];
    var match = url.match(/^([^:\/?#]+:)?(?:\/\/([^\/?#]*))?([^?#]+)?(\?[^#]*)?(#.*)?/);

    if (typeof match[1] === "string" && match[1].length > 0 && internal_protocols.indexOf(match[1].toLowerCase()) < 0) return true;
    if (typeof match[2] === "string" && match[2].length > 0 && internal_hosts.indexOf(match[2].replace(new RegExp(":("+{"http:":80,"https:":443}[location.protocol]+")?$"), "")) < 0) return true;
    return false;
}

function uriEncode(str) {
    str = str.toLowerCase();

     // convert spaces to '-'
    str = str = str.replace(/ /g, "-");
    // Make lowercase

    str = str.replace(/\u00df/g, "ss");
    str = str.replace(/\u00e4/g, "ae");
    str = str.replace(/\u00f6/g, "oe");
    str = str.replace(/\u00FC/g, "ue");


   // Remove characters that are not alphanumeric or a '-'
   str = str.replace(/[^a-z0-9-]/g, "-");
   // Combine multiple dashes (i.e., '---') into one dash '-'.
   str = str.replace(/[-]+/g, "-");
   str = str.replace(/^-/g, "");
   str = str.replace(/-$/g, "");
   return str;

}

function msg(strHTML, isAlert) {
   isAlert = isAlert ? isAlert : false;
   if(isAlert){
       $("#notification").addClass('red');
    }
    $("#notification .alert_text").html(strHTML);
//    $("#alertsContents .alert_text").html(strHTML);
//    $("#alerts, #alertsBG").fadeIn(300);
//    $("#alerts .ok_button, #alerts .close_button, #alertsBG").on("click", function(){
//        $("#alerts, #alertsBG").fadeOut(300,function(){$("#alerts").removeClass('red');});
//    });
    generateNotification();
}

function generateNotification() {
    var $ac = $($("#notification .alert_text")[0]);
    if ($ac.length > 0 && $ac.text().replace(/\s/g, '').length > 10) {
        $("#notification").fadeIn(300, function () {
            if(document.location.href.lastIndexOf('#') == -1){
                $('body').scrollTo();
            }
        });
        $("#notification .close_button").on("click", function () {
            $("#notification").fadeOut(300, function () {
                calcMessTops();
                $("#notification").removeClass('red');
            });
        });
    }
}


function switchMobileFilterMenu() {

    var t = $(this), a = (window === this) ? true : t.hasClass('active');

    $("div.basic_nav > .menu_label").removeClass('active');
    $("body").toggleClass('filter-dialog-active');

    if(a) {
        menu.overlay(0);
    } else {
        menu.overlay(1);
        t.addClass('active');
    }
}

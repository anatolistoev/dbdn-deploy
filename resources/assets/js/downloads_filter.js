$(document).ready(function(){

    var start_period = $( "#start_period" ),
        end_period = $( "#end_period" );

    if(!start_period.length || !end_period.length)
        return;

    var start_period_val = start_period.attr('value').trim(),
        end_period_val = end_period.attr('value').trim();


    start_period.datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        maxDate: '0'
    });
    end_period.datepicker({
         showOtherMonths: true,
         selectOtherMonths: true,
         maxDate: '0'
    });

    if (lang == 1) {
        start_period.datepicker("option", $.datepicker.regional[ "" ]);
        start_period.datepicker("option", "dateFormat", "mm/dd/yy");
        end_period.datepicker("option", $.datepicker.regional[ "" ]);
        end_period.datepicker("option", "dateFormat", "mm/dd/yy");
    } else {
        start_period.datepicker("option", $.datepicker.regional[ "de" ]);
        start_period.datepicker("option", "dateFormat", "dd.mm.yy");
        end_period.datepicker("option",$.datepicker.regional[ "de" ]);
        end_period.datepicker("option", "dateFormat", "dd.mm.yy");
    }

    if(start_period_val){
        start_period.datepicker("setDate", start_period_val);
    }
    if(end_period_val){
        end_period.datepicker("setDate", end_period_val);
    }
    //Prevent to close flayot on datepicker click
    $("#ui-datepicker-div").click( function(event) {
        event.stopPropagation();
    });
	
    $(".level4_nav input").each(function(e){
        addSelection(this,e);
    });


    $(".level4_nav input").change(filter.change);

    $("#remove_filters").on("click", function(e){
        filter.clear();
    });

    if($('#filter-panel-selected').attr('action').indexOf('search_results_filter') != -1){
        if(filter.count() > 0){
            sendBrowserRequest();
        }
    }
});

$(window).load(function(){
    filter.compactSelection();
    /*
    if($('#filter-panel-selected').closest('section').hasClass('best_practice') && filter.count() > 0){
        $(".goDown").click();
    }
    */
});

function addSelection(el,e){

    el = $(el);

    var _type   = el.attr('type'),
        _name   = el.attr('name'),
        _action = el.attr('action'),
        _val    = el.val(),
        _id     = el.attr('id');

    if ('text' == _type)
    {
        if (_val)
        {
            $('.filter-panel').show();

            if($('#filter_' + _id).length == 0)
            {
                $('#filter-example').clone().attr('id', 'filter_' + _id).attr('action', _action).appendTo('#filter-panel-selected');

                $('#filter_' + _id)
                    .find('label')
                    .attr('for', 'input_' + _id)
                    .prepend(
                        el.closest('ul.level4_nav').parent().find('.filter_title').text() +
                        ' (' + el.parent().find('label[for="' + _id + '"]').text() + ')'
                    );
            }

            $('#filter_' + _id).find('input').attr('id', 'input_' + _id).attr('name', _name).val(_val).click(function() {

                $('input#'+$(this).attr('id').substring(6)).val('');

                filter.drop(this);
            });

            $('#filter_' + _id).find('label .filter_value').html(_val);
        }
        else
        {
            $('#filter_' + _id).remove();
            if(filter.count() == 0)
            {
                $('.filter-panel').hide();
            }
        }
    }
    else
    {
        if (el.prop('checked') == true)
        {
            if (_action == 'order')
            {
                $('.filter-tags[action="order"]').not('#filter_' + _name).each(function() {
                    if ($(this).find('input').attr('name') != _name)
                    {
                        $('.level3_nav input[name="' + $(this).find('input').attr('name') + '"]').prop('checked', false);
                    }
                    $(this).remove();
                });
            }
            if($('#filter_' + _name).length == 0)
            {
                $('.filter-panel').show();

                $('#filter-example').clone().attr('id', 'filter_' + _name).attr('action', _action).appendTo('#filter-panel-selected');

                $('#filter_' + _name).find('input').change(function(){

                    $('input[name="'+$(this).attr('id').substring(6)+'"]').prop('checked',false);

                    filter.drop(this);
                });


                $('#filter_' + _name).find('label').attr('for', 'input_' + _name);

                if( -1 == $.inArray(_name, ['time', 'rating', 'comments', 'viewed']) )
                {
                    var filter_label_text = el.closest('ul.level4_nav').parent().find('.filter_title').text();
                    if (_name === 'brand') {
                        filter_label_text = js_localize['filter.selected.prefix'] + ' ' + filter_label_text;
                    }
                    $('#filter_' + _name).find('label').prepend(filter_label_text);
                }
            }

            if ('radio' == _type)
            {
                $('#filter_' + _name).find('input').attr('id', 'input_' + _name).attr('name', _name).val( el.attr('value') );
                if (-1 !== $.inArray(_name, ['time', 'title'])) {
                    $('#filter_' + _name).find('label .filter_value').html( el.parent().find('label[for="' + _id + '"]').html() );
                }
            }
            else if('checkbox' == _type)
            {
                if( 'all' == _val )
                {
                    // $('#filter_' + _name).find('label .filter_value').html(js_localize['filter.all']);
                    $('.level4_nav input[name="'+_name+'"]').prop('checked',true);
                    $('#filter_' + _name).find('input').attr('id', 'input_' + _name).attr('name', _name);
                    var val_arr = new Array();
                    $('.level4_nav input[name="'+_name+'"]').not('.level4_nav input[name="'+_name+'"][value="all"]').each(function(){
                        if($(this).prop('checked') == true){
                            val_arr.push($(this).val());
                        }
                    });

                    $('#filter_' + _name).find('input').val(val_arr.join(','));
                }
                else
                {
                    $('#filter_' + _name).find('input').attr('id', 'input_' + _name).attr('name', _name);
                    var val_arr = new Array();
                    $('.level4_nav input[name="'+_name+'"]').each(function(){
                        if($(this).prop('checked') == true){
                            val_arr.push($(this).val());
                        }
                    });
                    $('#filter_' + _name).find('input').val(val_arr.join(','));
                    if($('.level4_nav input[name="'+_name+'"][value="all"]').length > 0){
                        if($('.level4_nav input[name="'+_name+'"]').not('.level4_nav input[name="'+_name+'"][value="all"]').not(':checked').length == 0){
                            // $('#filter_' + _name).find('label .filter_value').html(js_localize['filter.all']);
                            $('.level4_nav input[name="'+_name+'"][value="all"]').prop('checked',true);
                        }else{
                            /*
                            if(val_arr.length == 1){
                                $('#filter_' + _name).find('label .filter_value').html(el.next('label').html());
                            }else{
                                $('#filter_' + _name).find('label .filter_value').html(js_localize['filter.selected']);
                            }
                            */
                        }
                    }else{
                        if(-1 !== $.inArray(_name, ['viewed', 'rating', 'comments'])) {
                            $('#filter_' + _name).find('label .filter_value').html( el.next('label').html() );
                        }
                    }

                }
            }
        }
        else
        {
            if('checkbox' == _type)
            {
                if('all' == _val)
                {
                    if(e.type == 'change')
                    {
                        $('.level4_nav input[name="'+_name+'"]:checked').prop('checked',false);

                        $('#filter_' + _name).remove();

                        if(filter.count() == 0)
                        {
                            $('.filter-panel').hide();
                        }
                    }
                }
                else if($('.level4_nav input[name="'+_name+'"]:checked').not('.level4_nav input[name="'+_name+'"][value="all"]').length > 0)
                {

                    $('.level4_nav input[name="'+_name+'"][value="all"]').prop('checked',false);
                    $('#filter_' + _name).find('input').attr('id', 'input_' + _name).attr('name', _name);
                    var val_arr = new Array();
                    $('.level4_nav input[name="'+_name+'"]').each(function(){
                        if($(this).prop('checked') == true){
                            val_arr.push($(this).val());
                        }
                    });
                    $('#filter_' + _name).find('input').val(val_arr.join(','));
                    /*
                    if(val_arr.length == 1){
                        $('#filter_' + _name).find('label .filter_value').html(val_arr[0]);
                    }else{

                        $('#filter_' + _name).find('label .filter_value').html(js_localize['filter.selected']);
                    }
                    */
                }
                else
                {
                    $('#filter_' + _name).remove();
                    if(filter.count() == 0){
                        $('.filter-panel').hide();
                    }
                }
            }
            else
            {
                //$('#filter_' + _name).remove();
            }
        }
        if(val_arr !== undefined && -1 !== $.inArray(_name, ['tags', 'usage', 'file_type']) && $('#filter_' + _name).find('label')[0] != undefined){
            var filter_label_text = '';
            if(val_arr.length == 1){
                filter_label_text= ' ' + js_localize['filter.' + _name + '.singular'];
            }else{
                if ('tags' === _name) {
                    filter_label_text= ' ' + js_localize['filter.' + _name + '.plural'];
                } else {
                    filter_label_text= el.closest('ul.level4_nav').parent().find('.filter_title').text();
                }
            }
            $('#filter_' + _name).find('label')[0].firstChild.nodeValue = js_localize['filter.selected.prefix'] + filter_label_text;
        }
    }
}

function _sendBrowserRequestCommon()
{

    filter.compactSelection();

	if(MOBILE) {
        window.scrollTo(0,0);
        checkFiltersLength();
        setSmartLabels();
	} else {
	    $('#basic_preloader_overlay').css('display','none'); // double overlay...
        menu.overlay(1);
        setSmartLabels();
	}

    $('#basic_preloader').show(0, function(){

        var json = $('#filter-panel-selected').serialize();

        if($('#filter-panel-selected').attr('action').indexOf('search_results_filter') != -1){
            json += '&search='+$('#searchForm2 #search2').val()+'&filter=active';
        }

        if($('#filter-panel-selected').attr('action').indexOf('faq_filter') != -1){
            json += '&search='+$('#searchForm_faq #search').val();
        }

        $.ajax({
            url: $('#filter-panel-selected').attr('action'),
            type: 'POST',
            data: json,
            success: function(data) {
                if ($('#filter-panel-selected').attr('action').indexOf('faq_filter') != -1) {
                    $('.faq-list-menu').html(data);
                    FaqList.bindClicks();
                } else if ($('#filter-panel-selected').attr('action').indexOf('search_results_filter') != -1) {
                    $('.glossary-search-container').html(data);
                } else {
                    if ($('#parent_path').length > 0) {
                        window.location = $('#parent_path').html().trim();
                    } else {
                        $('.downloads_overview,.best-practice').empty().append($(data).html());
                        if($('.load-more').length > 0){
                            $('.load-more').attr('page',1);
                        }
                        if(!MOBILE)
                        {
                            if (filter.count() > 0) {
                                $('#cd-vertical-nav ul.dot_items li').remove();
                            } else {
                                if ($('#cd-vertical-nav ul.dot_items li').length == 0) {
                                    vertical_navigation.createAnchorNav();
                                    vertical_navigation.markerSelectInit();
                                }
                            }

                        }

                        if(MOBILE){
                            if($('.downloads_overview').hasClass('bwgrid')){
                                $('.downloads_overview').addClass('bwlist').removeClass('bwgrid');
                            }
                        }
                    }
                }
                var imgs = $('.downloads_overview img.lazyload-thumb, .best-practice .grid-page img.lazyload-thumb');

                imageProcess.lazyLoadWithCallBack(imgs);
                imageProcess.updateLazyload();
                groupEqualTitlesSections();
            },
            complete:function(){
                if(MOBILE) {
                    $('.basic_nav .menu_label').removeClass('active');
                    $("body").removeClass('filter-dialog-active');
                    
                }
                $('#basic_preloader').hide(0);
                $('#basic_preloader_overlay').css('display', '');
                menu.overlay(0);

                if (isTabletViewport()) {
                    $(document).trigger('filteredAjaxDataLoaded.DBDN_General');
                }
                groupEqualTitlesSections();
            }
        });

    });
}

function checkFiltersLength() {
    if($('.filter-tags').length > 2) {
        $('.filter_remove').css('margin-top', '0px');
    }
}

function sendBrowserRequest()
{
    if (MOBILE)
        return;

    _sendBrowserRequestCommon();
}

function sendMobileBrowserRequest()
{
    if (!MOBILE)
        return;

    _sendBrowserRequestCommon();
}
var FaqList = {
	openFirstLevel: function(el) {
		var that = this;
		$(el).siblings().each(function(){
			that.closeFirstLevel(this);
		});

		$(el).find('.toggle').show();
		$(el).addClass('current');
		$(el).unbind('click');

		$(el).bind('click', function() {
			that.closeFirstLevel(el);
		});

		$("html, body").finish();
		$("html, body").animate({
			scrollTop: ($(el).offset().top - $('header.no-sticky').height() - 50) + "px"
		}, 1000, "swing");
	},
	closeFirstLevel: function(el) {
		var that = this;
		$(el).removeClass('current');
		$(el).find('.toggle').hide();
		$(el).find('.toggle .second-ul').each(function(){
			that.closeSecondLevel(this);
		});

		$(el).unbind('click');
		$(el).bind('click', function() {
			that.openFirstLevel(el);
		});
	},

	openSecondLevel : function(el){
		var that = this;
		$(el).addClass('opened');
		$(el).siblings().each(function(){
			that.closeSecondLevel($(this));
		});
		$(el).find('.toggle-two').show();
		$(el).find('.faq_title_row').hide();
		$(el).unbind('click');
		$(el).bind('click', function(event){
			event.stopPropagation();
		});
		$(el).find('.close-toggle-two').bind('click', function(event){
			event.stopPropagation();
			that.closeSecondLevel(el);
		});

		$("html, body").finish();
		$("html, body").animate({
			scrollTop: ($(el).offset().top - $('header.no-sticky').height() - 50) + "px"
		}, 1000, "swing");

		this.updateViews($(el).attr('faq_id'));
	},
	closeSecondLevel : function(el){
		var that = this;
		$(el).removeClass('opened');
		$(el).find('.faq_title_row').show();
		$(el).find('.toggle-two').hide();

		$(el).find('.close-toggle-two').unbind('click');
		$(el).unbind('click');
		$(el).bind('click', function(event) {
			event.stopPropagation();
			that.openSecondLevel(el);
			return false;
		});
	},
	updateViews : function(faq_id){
		$.ajax({
			url : relPath + 'faq/views/'+faq_id,
			type : 'POST',
			data : {}
		});
	},
	updateLikes: function(el){
		$.ajax({
			url : relPath + 'faq/likes/'+$(el).attr('faq_id'),
			type : 'POST',
			data : {'_token':$("input[name='_token']").val()},
			success : function(data){
				$(el).find('.vote-icon').html(data.response.faq_rates);
				$(el).find('.vote-icon-front').html(data.response.faq_rates);
                msg(data.message);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
				var message = JSON_ERROR.composeMessageFromJSON(data, true);
                msg(message, true);
			}
		});
	},
	bindClicks: function() {
		var that = this;
		$('.faq-list-menu ul li.first-ul').not('.current').bind('click', function() {
			that.openFirstLevel(this);
		});

		$('.faq-list-menu ul li.first-ul.current').bind('click', function() {
			that.closeFirstLevel(this);
		});

		$('.faq-list-menu ul li.first-ul ul li.second-ul').not('.opened').bind('click', function(event) {
			event.stopPropagation();
			that.openSecondLevel(this);
			return false;
		});

		$('.faq-list-menu ul li.first-ul ul li.second-ul.opened .close-toggle-two').bind('click', function(event) {
			event.stopPropagation();
			that.closeSecondLevel($(this).parents('.second-ul'));
		});

		$('.faq-list-menu ul li.first-ul ul li.second-ul .vote-icon').click(function(){
			that.updateLikes($(this).parents('.second-ul'));
			return false;
		});
        $('.faq-list-menu ul li.first-ul ul li.second-ul.opened').bind('click', function(event) {
            event.stopPropagation();
		});
	}

};

$(document).ready(function(){
	if($('.faq-list-menu ul li.first-ul ul li.second-ul.opened').length > 0){
		$("html, body").animate({
			scrollTop: ($('.faq-list-menu ul li.first-ul ul li.second-ul.opened').offset().top - $('header.no-sticky').height() - 50) + "px"
		}, 1000, "swing");
	}else if($('.faq-list-menu ul li.first-ul.current') > 0){
		$("html, body").animate({
			scrollTop: ($('.faq-list-menu ul li.first-ul.current').offset().top - $('header.no-sticky').height() - 50) + "px"
		}, 1000, "swing");
	}
	FaqList.bindClicks();
});


/* German initialisation for the jQuery UI date picker plugin. */
/* Written by Milian Wolff (mail@milianw.de). */
( function( factory ) {
	if ( typeof define === "function" && define.amd ) {

		// AMD. Register as an anonymous module.
		define( [ "../widgets/datepicker" ], factory );
	} else {

		// Browser globals
		factory( jQuery.datepicker );
	}
}( function( datepicker ) {

datepicker.regional.de = {
	closeText: js_localize['calendar.closeText'],
	prevText: js_localize['calendar.prevText'],
	nextText: js_localize['calendar.nextText'],
	currentText: js_localize['calendar.currentText'],
	monthNames: [ js_localize['calendar.monthNames.1'],js_localize['calendar.monthNames.2'],js_localize['calendar.monthNames.3'],js_localize['calendar.monthNames.4'],
		js_localize['calendar.monthNames.5'],js_localize['calendar.monthNames.6'],js_localize['calendar.monthNames.7'],js_localize['calendar.monthNames.8'],
		js_localize['calendar.monthNames.9'],js_localize['calendar.monthNames.10'],js_localize['calendar.monthNames.11'],js_localize['calendar.monthNames.12'] ],
	monthNamesShort: [ js_localize['calendar.monthNamesShort.1'],js_localize['calendar.monthNamesShort.2'],js_localize['calendar.monthNamesShort.3'],js_localize['calendar.monthNamesShort.4'],
		js_localize['calendar.monthNamesShort.5'],js_localize['calendar.monthNamesShort.6'],js_localize['calendar.monthNamesShort.7'],js_localize['calendar.monthNamesShort.8'],
		js_localize['calendar.monthNamesShort.9'],js_localize['calendar.monthNamesShort.10'],js_localize['calendar.monthNamesShort.11'],js_localize['calendar.monthNamesShort.12'] ],
	dayNames: [ js_localize['calendar.dayNames.1'],js_localize['calendar.dayNames.2'],js_localize['calendar.dayNames.3'],js_localize['calendar.dayNames.4'],
		js_localize['calendar.dayNames.5'],js_localize['calendar.dayNames.6'],js_localize['calendar.dayNames.7'] ],
	dayNamesShort: [ js_localize['calendar.dayNamesShort.1'],js_localize['calendar.dayNamesShort.2'],js_localize['calendar.dayNamesShort.3'],js_localize['calendar.dayNamesShort.4'],
		js_localize['calendar.dayNamesShort.5'],js_localize['calendar.dayNamesShort.6'],js_localize['calendar.dayNamesShort.7']],
	dayNamesMin: [ js_localize['calendar.dayNamesMin.1'],js_localize['calendar.dayNamesMin.2'],js_localize['calendar.dayNamesMin.3'],js_localize['calendar.dayNamesMin.4'],
		js_localize['calendar.dayNamesMin.5'],js_localize['calendar.dayNamesMin.6'],js_localize['calendar.dayNamesMin.7'] ],
	weekHeader: "KW",
	dateFormat: "dd.mm.yy",
	firstDay: 1,
	isRTL: false,
	showMonthAfterYear: false,
	yearSuffix: "" };
datepicker.setDefaults( datepicker.regional.de );

return datepicker.regional.de;

} ) );
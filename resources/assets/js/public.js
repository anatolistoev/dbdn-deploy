var animationTime= 400;

$(document).ready(function () {
/*** Hide popups ***/
    $('body').click(function(e){
        var popupParent = $(e.target).parents('.popup').length > 0;
        if ($(e.target).hasClass('popup-close')
            || popupParent
            || $(e.target).hasClass('popup')
            || (popupParent && $(e.target).parents('a'))
            || $(e.target).closest('.tablet-popup, .tablet-popup-entry').length > 0) {
        }else{
            closeShowedPopup();
        }
    });

/*** Tooltip hover***/
if(!MOBILE && !TABLET)
{
    $('.middle_page_text a.tooltip').hover(function(){
        var span = $(this).children();
        var height = span.height();
        span.css({display:'block', height:0});
        span.animate({height:height},animationTime/4,function(){
             span.css({overflow:'visible'});
        })}, function(){
            var span = $(this).children();
            span.animate({height:0},animationTime/4, function(){
                 span.css({display:'none', height:'auto', 'overflow': 'hidden'});
            });
        });

    if($('.middle_page_text .tooltip').length > 0) {
        $('.middle_page_text .tooltip').sort(function(a,b){
            return $(a).find('span > b').html() > $(b).find('span > b').html();
        }).each(function(index){
                var html = '<li><a href="'+relPath+'glossary/'+$(this).attr('glossary_id')+'">'+$(this).find('span > b').html()+'</a></li>';
                $('#glossary-formular ul').append(html);
        })

        $("li#glossary > a").click(function(event){
            openPopup(event, this);
        });
    }
}

/*** Scroll Down ***/
    $(".goDown").click(function(event){
        event.preventDefault();
        $('#searchDiv').scrollTo();
        return false;
    });
    $(".linkDown").click(function(event){
        event.preventDefault();
        var elHeight = $(this).closest('#banner, #fullsize_banner').offset().top +  $(this).closest('#banner, #fullsize_banner').outerHeight();
        $("html, body")
            .animate({scrollTop: Math.ceil(elHeight) + "px"}, animationTime * 2, "swing")
        return false;
    });

/*** Scroll Up ***/
    $("a.goUp").click(function(event){
        event.preventDefault();
        $('body').scrollTo();
        return false;
    });

    $('#download-icon, #links-icon').click(function(){
        $('#related_wrapper').scrollTo();
        return false;
    });

/*** Flyout Menu ***/
    $(".brandSelect, #closeFlyoutMenu").click(function(event){
        event.preventDefault();
        event.stopPropagation();

        $('div#hamburger').removeClass('active'); // JIC close hamburger menu


        var flyout_menu = $('.flyout_menu.popup');

        // Dusty: for legacy-compatible code... doesn't look good.
        var menuIsVisible = $(".menu_click").length ?
                $(".menu_click").hasClass("hidden") :
                'block' === flyout_menu.css('display');

        if ( menuIsVisible ) {
            $(".menu_click").removeClass("hidden");
            $('.brandSelect').removeClass('active');

            if (MOBILE) {
                flyout_menu.hide();
                menu.overlay(0);
            } else {
                flyout_menu.animate({height:0}, animationTime, function(){
                    flyout_menu.css({display:'none', height:'390px'});
                    menu.overlay(0);
                });
            }

        } else {
            closeShowedPopup();
            menu.overlay(1);
            $(".menu_click").addClass("hidden");
            $('.brandSelect').addClass('active');

            if (MOBILE) {
                flyout_menu.show();
                window.scrollTo(0, 0);
            } else {
                if(isTabletViewport()){
                    $('html').attr('style', 'overflow:hidden!important;');
                }
                var height = $('.flyout_menu').height();
                flyout_menu.css({display:'block', height:0});
                flyout_menu.animate({height:height},animationTime);
            }
        }
    });


/*** Popup Footer Tools ***/
//#tools ul.footer_tools li a,
    $("li#help > a,div#profile").click(function(event){
        if ((isTabletViewport() || isMobileViewport()) && $(this).parent('#help').length > 0) {
            return true;
        }
        openPopup(event, this);
    });

    $(".form_item a.popup-close, footer a.popup-close, #topLinks a.popup-close").click(function(event){
        event.preventDefault();
        event.stopPropagation();
        var popup;
        popup = $(this).parent().parent().find('.popup');
        if (popup.length > 0) {

            popup.css('overflow','hidden');
            $scroll = popup.find(".related-links-right,.related-downloads-right");
            $scroll.css({visibility:"hidden"});
            popup.animate({height:0},animationTime, function(){
                popup.parent().parent().find('.active:not(.hamburger)').removeClass('active');
                popup.css({visibility:'hidden', height:'auto'});

            });
        }
    });

    if(!MOBILE && $('li#faq > .popup').length > 0){
        $('li#faq > a').click(function(event){
            openPopup(event, $('li#faq > a'));
        });
    }

    $('#help-formular #messageText').keyup(function(){
        var len = $(this).val().length;
        $('#messageText_counter').html(len);
        if(len > 1000){
            $('#counter_wrapper').addClass('error');
        }else if($('#counter_wrapper').hasClass('error')){
            $('#counter_wrapper').removeClass('error');
        }
    });
     // issue warning } - javascript


/*** Left Navigation ***/
    $(".menuitems li").click(function(event){
        event.preventDefault();
        var goToItem = $(this).find("a").attr('href');
        if($("#"+goToItem).length > 0){
            $('html, body').animate({ scrollTop: $("#"+goToItem).offset().top-70 }, 1000);
        }
    });

    // Is this still used anywhere? I think not.
	//$(".level3_nav li span").click(function() {
	//	if($(this).parent().hasClass('active')){
	//		$(this).parent().find(".level4_nav").hide();
	//		$(this).parent().removeClass('active');
	//	}else{
	//		$(this).parent().parent().find(".level4_nav").hide();
	//		$(this).parent().parent().find('li').removeClass('active');
	//		$(this).parent().find(".level4_nav").show();
	//		$(this).parent().addClass('active');
	//	}
	//});

    $('.filter_filters .filter').click(function(){
        if($(this).hasClass('closed')){
            $(this).removeClass('closed');
            $(this).addClass('opened');
        }else{
            $(this).removeClass('opened');
            $(this).addClass('closed');
        }
        $(this).find('.filter_content').toggle();
    });






    $('.basic_nav_closed wrap div, #rubrics li a').click(menu.show);

    if (isTabletViewport()) {
        $('div.filter-panel-title').addClass('tablet-popup-entry');
    }

    $('div.filter-panel-title').click(function(event){
        var el = $(this).parent();

        if(el.hasClass('active')) {
            el.removeClass('active');
            $(this).removeClass('active');

            if(MOBILE) {
                $('#remove_filters').css('display','none');
            }
            menu.overlay(false);
        } else {
            if (isTabletViewport()) {
                closeShowedPopup('.filter-panel-title');
            }

            el.addClass('active');
            $(this).addClass('active');

            if(MOBILE) {
                $("div.basic_nav > .menu_label").removeClass('active');
                $('#remove_filters').css('display','block');
            }
            menu.overlay(true);
        }
    });



    $(document).on('click', 'span#menu_close, #menu_overlay, section.basic_nav_opened', function(event) {
        var target = $(event.target),
            tId = target.attr('id');

        if ( 'menu_close' == tId || target.hasClass('basic_nav_opened') ) {
            stopEvent(event);
            if (isTabletViewport()) {
                return;
            }
            menu.hide();
        }
        else if ('menu_overlay' == tId) {
            if ( $(".basic_nav_opened").inViewport() ) {
                stopEvent(event);
                menu.hide();
            } else if($(".flyout_search.popup").inViewport()) {
                flyoutSearch.hide(event);
            }
        } else {
            // console.warn(tId);
        }

    });

    $('.load-more').click(function(){
        loadBPPages(this);
    });

    // Grid and List view
//    $('#view-controls a').click(function(event){
    $('body').on('click', '#view-controls a', function(event) {
        event.preventDefault();
        event.stopPropagation();

        var t = $(this), bw = t.closest('div.blocks_wrap');

        if(!bw.length)
            bw = t.closest('section.downloads_overview');

        $('#view-controls a').removeClass('active');
        t.addClass('active');

        if(t.hasClass('view-list')) {
            bw.removeClass('bwgrid').addClass('bwlist');
            // $('div.blocks_wrap > section.section_content').removeClass('grid-view');
            $('.grid-page').addClass('list-view');
            if($('.downloads_overview').length) {
                sessionStorage.setItem('list-view', true);
            }
        } else {
            bw.removeClass('bwlist').addClass('bwgrid');
            // $('div.blocks_wrap > section.section_content').addClass('grid-view');
            $('.grid-page').removeClass('list-view');
            if($('.downloads_overview').length) {
                sessionStorage.removeItem('list-view');
            }
        }

        if (filter && filter.count() == 0) {
            vertical_navigation.markerMap();
        }
        imageProcess.updateLazyload();
        //return false;
    });

    $('#load-more_wrapper .black-button').click(function(){
        $('#related_wrapper .hided').toggleClass('showed');
        if($('#related_wrapper .showed').length > 0){
            $(this).text(js_localize['bp.show_less']);
            $('#related_wrapper ul li:nth-child(6)').css('border-bottom-width', '1px');
        }else{
            $('#related_wrapper').scrollTo();
            $(this).text(js_localize['bp.load_more']);
            $('#related_wrapper ul li:nth-child(6)').css('border-bottom-width', 0);
        }
    });

    if ( $('.banner_text_wrapper p').length > 0 && $('body').hasClass('smart') ) {
        $('.banner_text_wrapper p').last().append('<span class="right_arrow"></span>');
    }

    $('.reset_captcha').click(function(){
        var button = this;
        $.ajax({
            url : relPath + "reset_captcha",
            type : 'GET',
            success: function (data) {
                var brand = $('body').hasClass('smart') ? 'smart' : 'daimler';
                $($(button).parent().find('img')).attr("src", relPath + "captcha?brand=" + brand + "&timestamp=" + new Date().getTime());
            }
        });
         return false;
    });
}); // $(document).ready()

function loadBPPages(button){
    var page = $(button).attr('page');
    $.ajax({
        url : relPath + "load_best_practice",
        type : 'POST',
        data : {pageid :$(button).attr('page_id'), page : page,_token : $('#searchForm2 input[name="_token"]').val()},
        success : function(data){
            var response = data.response;
            $('.best-practice.grid-page').append(response.html);
            $(button).attr('page',parseInt(page)+1);
            if(response.islast){
                $('a.load-more').hide();
            }else{
                $('a.load-more').show();
            }
        }
    });
}

$(function loadMoreTen () {
    var size_li = $("#load-more-tags li").size();
    var  maxVisibleTagCount = 10;
    $('#load-more-tags li:lt('+maxVisibleTagCount+')').show();
    if(size_li <= maxVisibleTagCount){
        $('#load-more-ten').hide();
    }else{
        $('#load-more-ten').show();
         $('#load-more-ten').click(function () {
        maxVisibleTagCount = (maxVisibleTagCount+10 <= size_li) ? maxVisibleTagCount+10 : size_li;
        $('#load-more-tags li:lt('+maxVisibleTagCount+')').show();
        if(size_li <= maxVisibleTagCount){
            $('#load-more-ten').hide();
        }

    });
   }
});

function closeShowedPopup(exclude) {
    $.fx.off = true;
    $('.popup').each(function() {
        var el = $(this);
        if(el.is(':visible') && el.css("visibility") == "visible") {
            if(exclude && el.is(exclude)) {
                // skip
                console.log('skipping: ' + el);
            } else {
                el.find('.popup-close').trigger('click');
            }
        }
    });

    exclude = exclude || '';

    if (isTabletViewport()) {
        $('.tablet-popup-entry').filter('.active').not(exclude).trigger('click');
    }

    $.fx.off = false;
}

function openPopup(event, el){
    var parent = $(el).parent();
    var popup  = parent.find('.popup');
    //$(el).parent().css('overflow', 'visible');

    if(popup.length > 0){
        event.preventDefault();
        event.stopPropagation();
        if(popup.css('visibility') == "visible"){
            popup.find('.popup-close').trigger('click');
        }else{
            if(!(isMobileViewport()
                && el.id == 'profile')) {
                menu.hide();
                closeShowedPopup();
            }
            $(el).addClass('active');


            if($('#links-icon').length){
                maxHeight();
            }
            var height = popup.outerHeight();
            if(parent.hasClass('right-menu-item')) {
                if(parent.position().top < height) {
                    popup.css({top:0 , bottom:'auto'});
                }
            }
            $scroll = popup.find(".related-links-right,.related-downloads-right");
console.log('here - open', height, $scroll);
            $scroll.css({visibility:"hidden"});
            popup.css({visibility:'visible', height:0});
            //popup.fadeToggle();
            popup.animate({height:height},animationTime, function(){
                popup.css({'overflow':'visible'});
            });
        }
    }
}

function maxHeight(){
    var top = $(window).height();
    if ($('footer.sticky').offset()) {
        top = $('footer.sticky').offset().top;
    }
    var height = top - $('#links-icon').offset().top - 103;
    $('.scrollbar-rail').css('max-height', height +"px");
    $('.related-links-right .scrollbar-rail').scrollbar({
        "autoUpdate": true
    });
    $('.related-downloads-right .scrollbar-rail').scrollbar({
        "autoUpdate": true
    });
}

function moveNav(height){
    $("html, body").animate({
        scrollTop: height + "px"
    }, 1700,"swing");
    return false;
}

function calculateOpacity($object){
    var scrollTop = $(window).scrollTop(), opacityElement = $object;

    if(scrollTop > 750){
        if(opacityElement.css('opacity') != '0'){
            opacityElement.css('opacity', '0');
        }

        return;
    }
    var opacity = (11 - (scrollTop / 68)) / 10;
    opacityElement.css('opacity', opacity);
}

//
// Download Files functions
//
function startDownload(id, ev) {
    // Create download IFRAME if not exists
    var $iframe = $('#downloadIFrame');
    if (0 === $iframe.length) {
        $iframe = $('<iframe id="downloadIFrame" name="downloadIFrame" style="display:none;" />');
        $iframe.appendTo('body');
    }

    var $form = $('#downloadsForm');
    if (id !== undefined) {
        if (0 === $form.length) {
            $form = $('<form id="downloadsForm"/>');
            $form.attr('method', 'POST');
            $form.appendTo('body');
        }
        var checkbox = $($form.find('#file_' + id));
        if (0 === checkbox.length) {
            $form.append('<input type="checkbox" \n\
                id="file_' + id + '" class="chb checkbox1 " value="' + id + '" name="auid[]">');
            checkbox = $form.find('#file_' + id);
        }
        checkbox.prop("checked", true);
        $form.attr('action', 'download');

        // Token
        var tokenElement = $form.find('input[name="_token"]');
        if (0 === tokenElement.length) {
            $form.append('<input name="_token" type="hidden" value="' + $('meta[name="csrf-token"]').attr('content') + '">');
        }
    }
    $form.find('input[name="_method"]').val('post');
    $form.attr('target', 'downloadIFrame');
    if (isDesktopViewport()) {
        $form.submit();
    } else {
        DBDN.utils.downloadFileAndDisplayNotificationWhileBeingDownloaded($form, js_localize['file.download'], true);
    }

    if (undefined !== id) {
        checkbox.prop("checked", false);
    }

    return false;
}

function addToCart(id, ev) {
    ev.preventDefault();
    ev.stopPropagation();
    $('body').css('cursor', 'progress');
    var $form = $('#downloadsForm');
    $form.find('input[name="_method"]').val('put');
    var linkTag = $(ev.target);
    var textTag = linkTag.find('span');
    // Download Page
    if ('A' === linkTag.prop('tagName') && linkTag.hasClass('cart')) {
        textTag = linkTag;
    } else {
        // Most ... and Dawnload List
        var grid = linkTag.parents('.download_hover');
        if (grid.length > 0) {
            var div = grid.find(".cart_wrapper");
        } else {
            var div = linkTag.parents('.list-hover').find(".cart_wrapper");
        }
        textTag = div.find('span');
    }

    $.ajax({
        method: "PUT",
        url: 'Download_Cart/add/' + id,
        data: $form.serialize()
    })
            .done(function (data) {
                $('.topItem#downloads .counter').html(data.response.myDownloadsCount);
                ev.target.onclick = function () {
                    return false;
                };
                linkTag.addClass('not-active');
                textTag.html(data.message);
                $('body').css('cursor', 'default');
            })
            .fail(function (XMLHttpRequest) {
                var data = JSON_ERROR.errorResponse2JSON(XMLHttpRequest);
                var message = JSON_ERROR.composeMessageFromJSON(data, true);
                msg(message, true);
                $('body').css('cursor', 'default');
            });

    return false;
}

if (!DBDN.utils.isForcedDesktopMode()) {
    // Tablet only: Add the active viewport as class to the page class list
    $(function () {
        var $htmlElement = $('html').addClass(DBDN.utils.getActiveHtmlViewportMode());

        if (DBDN.utils.isScaledViewport()) {
            $htmlElement.addClass(DBDN.enum.HtmlViewportMode.SCALED);
        }

        if (isTabletViewport() || isDesktopViewport()) {
            $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
                $htmlElement.removeClass(ev.htmlViewportMode.previous);
                $htmlElement.addClass(ev.htmlViewportMode.active);

                if (ev.htmlViewportMode.isActiveScaled) {
                    $htmlElement.addClass(DBDN.enum.HtmlViewportMode.SCALED);
                } else {
                    $htmlElement.removeClass(DBDN.enum.HtmlViewportMode.SCALED);
                }
            });
        }
    });

    // Tablet only: Fix the crazy markup in the section "oftheday"
    $(function () {
        if (isTabletViewport() || isDesktopViewport()) {
            var $ofTheDaySection = $('#oftheday');
            var newOfTheDayElementCollection = [];

            $ofTheDaySection.find('div:nth-of-type(2) > .container').each(function (index) {
                var $this = $(this);

                var $newOfTheDayElement = $('<\div class="obj container">').visibleToTabletOnly();

                var $mainHead = $this.children().clone().addClass('mainHead');
                var $subHead = $this.parent().next().children().eq(index).children().clone().addClass('subHead');
                var $text = $this.parent().next().next().children().eq(index).children().clone().addClass('text');
                var $linkButton = $this.parent().next().next().next().children().eq(index).children().clone().addClass('linkButton');

                $newOfTheDayElement.append([$mainHead, $subHead, $text, $linkButton]);
                newOfTheDayElementCollection.push($newOfTheDayElement);
            });

            $ofTheDaySection
                .hide()
                .append(newOfTheDayElementCollection)
                .show();
        }
    });

    // Tablet only: Split the footer metalinks in two lists
    $(function () {
        if (isTabletViewport() || isDesktopViewport()) {
            var $originalSingleList = $('#footer_wrap ul.metalinks');
            var $originalSingleListParent = $originalSingleList.parent();
            var $originalSingleListItems = $originalSingleList.children('li');
            var numberOfListItemsPerList = Math.round($originalSingleListItems.length / 2);
            var $firstTabletList = $('<ul class="metalinks"></ul>').visibleToTabletOnly();
            var $secondTabletList = $firstTabletList.clone();

            $originalSingleListItems
                .slice(0, numberOfListItemsPerList)
                .clone()
                .last()
                .addClass('last')
                .end()
                .appendTo($firstTabletList);

            $originalSingleListItems
                .slice(numberOfListItemsPerList)
                .clone()
                .appendTo($secondTabletList);

            var $tabletLists = $firstTabletList.add($secondTabletList);

            if (isTabletViewport()) {
                $originalSingleList = $originalSingleList
                    .parent()
                    .hide()
                    .end()
                    .after($tabletLists)
                    .detach();

                $originalSingleListParent.show();
            }

            $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
                DBDN.utils.toggleTabletDesktopHtml($originalSingleListParent,
                    $originalSingleList,
                    $tabletLists,
                    ev.htmlViewportMode,
                    function ($toBeInserted, $toBeDeleted) {
                        $toBeDeleted
                            .last()
                            .after($toBeInserted);
                    });
            });
        }
    });

    // Tablet only: Position elements
    $(function () {
        function positionElementsOnce() {
            // Design manuals content page (basic) - pages navigation buttons
            $('.nav_buttons')
                .clone()
                .hideBeforeTransition()
                .insertAfter('.middle_page_text #content_main')
                .visibleToTabletOnly();
            // Best practice content page - contact info box
            $(document).on('stats_loaded.DBDN_Tablet', function () {
                $('.middle_page_text #contact_info')
                    .clone()
                    .hideBeforeTransition()
                    .insertAfter('.middle_page_text #content_main')
                    .visibleToTabletOnly();
            });
        }

        if (isTabletViewport() || isDesktopViewport()) {
            positionElementsOnce();
        }
    });

    // Tablet/Desktop: Close some components when switching between desktop/tablet.
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var isThisFunctionalitySuspended = false;
        var $brandSelectMenu = $('header .brandSelect');

        function closeSearchPopup() {
            flyoutSearch.hide();
            menu.overlay(0);
        }

        function closeBrandSelectMenu() {
            if ($brandSelectMenu.hasClass('active')) {
                $brandSelectMenu.trigger('click');
            }
        }

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            if (isThisFunctionalitySuspended) {
                return;
            }

            $.fx.off = true;
            closeSearchPopup();
            closeBrandSelectMenu();
            $.fx.off = false;
        });

        $(document).on('suspendComponentClosingOnTabletDesktop.DBDN_General', function () {
            isThisFunctionalitySuspended = true;
        });

        $(document).on('unsuspendComponentClosingOnTabletDesktop.DBDN_General', function () {
            isThisFunctionalitySuspended = false;
        });
    });

    // Tablet only: Design Manuals content page (basic) - stats on top and bottom of the page
    // Downloads single pages - stats top of page
    $(function () {
        $(document).on('pageActions_showUnauthAlertText.DBDN_Tablet', function (ev, alertMessage) {
            showMsg(alertMessage);
        });

        function showMsg(text) {
            var $notificationElement = $('body #wrapper #notification');
            var eventSelector = '.ok_button, .no_button';
            $notificationElement.addClass('page-action');

            $notificationElement
                .off('click', eventSelector)
                .on('click', eventSelector, function (ev) {
                    var $this = $(this);

                    if ($this.hasClass('ok_button')) {
                        var pattern = relPath;
                        var regex = new RegExp(pattern, 'i');

                        location.href = relPath + 'login/' + location.href.replace(regex, '');
                    } else {
                        $notificationElement.hide();
                    }
                });

            msg(text);
        }

        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var collectionOfElements = [];
        var $verticalNavigation = $('#cd-vertical-nav > .form_item');

        if ($verticalNavigation.children('li#add-icon').length > 0) {
            var $addToWatchListElement = $('<\span class="add-watchlist page-action" data-require-auth="1" data-target="#add-icon a" data-msg="watchlist">');
            collectionOfElements.push($addToWatchListElement[0]);
        } else if ($verticalNavigation.children('li#remove-icon').length > 0) {
            var $removeFromWatchListElement = $('<\span class="remove-watchlist page-action" data-target="#remove-icon a">');
            collectionOfElements.push($removeFromWatchListElement[0]);
        }

        if ($verticalNavigation.children('li#rate-icon').length > 0) {
            var $ratePageElement = $('<\span class="rate-page page-action" data-require-auth="1" data-target="#rate-icon a" data-msg="rate">');
            collectionOfElements.push($ratePageElement[0]);
        }

        if ($verticalNavigation.children('li#print-icon').length > 0) {
            var $printPageElement = $('<\span class="print-page page-action" data-target="#print-icon a">');
            collectionOfElements.push($printPageElement[0]);
        }

        if ($verticalNavigation.children('li#save-icon').length > 0) {
            var $saveAsPDF = $('<\span class="save-pdf page-action" data-target="#save-icon a">');
            collectionOfElements.push($saveAsPDF[0]);
        }

        var $statsContainerTop = $('div#best_practice_stats div.stats, div#downloads_stats div.stats');
        if ($statsContainerTop.length > 0) {
            $()
                .pushStack(collectionOfElements.reverse())
                .hideBeforeTransition()
                .appendTo($statsContainerTop)
                .visibleToTabletOnly();

            var $clonedStatsContainerTop = new jQuery;

            if ($statsContainerTop.closest('div#downloads_stats').length < 1) {
                $clonedStatsContainerTop = $statsContainerTop
                    .closest('div#best_practice_stats')
                    .clone()
                    .hideBeforeTransition();

                $clonedStatsContainerTop[0].id = $clonedStatsContainerTop[0].id + '_bottom';

                $clonedStatsContainerTop
                    .insertAfter('.middle_page_text #content_main')
                    .visibleToTabletOnly();
            }

            $statsContainerTop
                .add($clonedStatsContainerTop)
                .find('div.stats')
                .addBack('div.stats')
                .on('click', '.page-action', function () {

                    var $this = $(this);
                    var targetSelector = $this.data('target');

                    if (!DBDN.utils.isSessionAuthenticated()
                        && typeof $this.data('requireAuth') !== 'undefined') {
                        var specificMsg = $this.data('msg');
                        showMsg(js_localize['page.actions.unauth_alert_text.' + specificMsg]);
                        $(window).scrollTop(0);

                        return;
                    }

                    if (targetSelector) {
                        $verticalNavigation.find(targetSelector).click();
                    }
                });
        }

        $(document).trigger('stats_loaded.DBDN_Tablet');
    });

    // Tablet only: Handle front_slider like carousels (bottom of some pages)
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var $sliderWrapper = $('.slider_wrapper');

        if ($sliderWrapper.length < 1) {
            return;
        }

        function setCarouselHeight() {
            $sliderWrapper.each(function () {
                var $singleWrapper = $(this);

                var maxItemHeight = isTabletViewport() ? $singleWrapper.children('.slider_title').height() + $singleWrapper.children('.slider_container').height() : 'auto';

                $singleWrapper.height(maxItemHeight);
            });
        }

        setCarouselHeight();

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            setCarouselHeight();
        });
    });

    // Tablet only: Handle destruction and initialization of carousels
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var eventSelector = '.front_slider:not(.carousel_basic), .mslide';
        var slyInstanceCollection = {};

        if ($(eventSelector).length < 1) {
            return;
        }

        $(document).on('slyLoaded', eventSelector, function (ev) {
            var $this = $(this);

            $this.generateUUID();
            if (isTabletViewport()) {
                ev.customData.sliderInstance.destroy();
            }

            slyInstanceCollection[$this.getUUID()] = ev.customData.sliderInstance;
        });

        function destroyCarousels() {
            for (slyInstance in slyInstanceCollection) {
                if (slyInstanceCollection.hasOwnProperty(slyInstance)) {
                    slyInstanceCollection[slyInstance].destroy();
                }
            }
        }

        function initializeCarousels() {
            for (slyInstance in slyInstanceCollection) {
                if (slyInstanceCollection.hasOwnProperty(slyInstance)) {
                    slyInstanceCollection[slyInstance].init();
                }
            }
        }

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            DBDN.utils.toggleTabletDesktopEvent(function () {
                if ($('#view-controls > a.view-grid.active').length < 1) {
                    initializeCarousels();
                }
            }, function () {
                destroyCarousels();
            }, ev.htmlViewportMode);
        });
    });

    // Tablet only: Design Manuals content page (basic) - Carousel at page bottom
    // And other pages where this type of carousel is used
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var $grid3cols = $('.grid-3cols:not(.manuals)').has('.page');

        if (!$grid3cols.length > 0) {
            return;
        }

        $grid3cols.wrapInner('<\div class="inner1-wrapper">');

        function setCarouselHeight() {
            $grid3cols.each(function () {
                var $carouselMainWrapper = $(this);
                var height = isTabletViewport() ? $carouselMainWrapper.find('.inner1-wrapper').height() : '';
                $carouselMainWrapper.css("height", height);
            });
        }

        setCarouselHeight();

        // The 'most*' components should be composed of 3 items when on desktop and 4 on tablet.
        // - This is needed because of the unreliable user-agent backend detection logic.
        if ($grid3cols.closest('.mostsomethingblocks').length > 0) {
            $grid3cols.each(function () {
                var $teasers = $(this).find('.page');
                if ($teasers.length >= 4) {
                    $teasers.last().visibleToTabletOnly();
                }
            });
        }

        $(document).on('tabletDesktopViewportChange.DBDN_General filteredAjaxDataLoaded.DBDN_General', function (ev) {
            setCarouselHeight();
        });
    });

    // Tablet only: Header - Hamburger menu
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var $header = $('header');
        var $rubricsWrapper = $header.find('#rubrics_wrapper').addClass('tablet-popup');
        var $activeMenu = $rubricsWrapper.find('ul#rubrics > li.active a');
        var childMenuItemsSelector = '.basic_nav_opened .content.tablet-only.basic-menu';

        if ($rubricsWrapper.length < 1) {
            return;
        }

        var $hamburgerButton = $('<\div class="hamburger">').addClass('tablet-popup-entry');

        var $linesContainer = $hamburgerButton
        .clone()
        .removeClass()
        .addClass('lines-container');

        $hamburgerButton
            .hideBeforeTransition()
            .append($linesContainer)
            .visibleToTabletOnly();

        $header.find('#navContainer > a.search_button').after($hamburgerButton);

        var scrollOldPosition = 0;
        var togglingHamburgerInProgress = false;
        $header.on('click', 'div.hamburger', function (ev) {
            if (togglingHamburgerInProgress) {
                return;
            }

            togglingHamburgerInProgress = true;
            var $window = $(window);

            if ($rubricsWrapper.is(':visible')) {
                hideBurgerMenu(function () {
                    $window.scrollTop(scrollOldPosition);

                    togglingHamburgerInProgress = false;
                });
            } else {
                scrollOldPosition = $window.scrollTop();

                if ($header.hasClass('sticky')) {
                    $window.scrollTop(0);
                }

                showBurgerMenu(function () {
                    $activeMenu.trigger('click');
                    closeShowedPopup('.basic_nav, .hamburger');

                    togglingHamburgerInProgress = false;
                });
            }
        });

        // Handle the active child menu items
        $(document).on('afterAttachingMenuChildItems.DBDN_General', function () {
            var activeUriRelativeToDocumentRoot = location.href.replace(relPath.substr(0, relPath.length - 1), '');

            var activeChildMenuItemSelector = childMenuItemsSelector + ' a[href="' + activeUriRelativeToDocumentRoot + '"]';
            activeChildMenuItemSelector += ', ' + childMenuItemsSelector + ' a[href="' + location.href + '"]';

            var $activeChildMenuItem = $(activeChildMenuItemSelector);

            clearPreviousActiveMenuChildPage();

            // Case-insensitive search until CSS4 has broader support
            if ($activeChildMenuItem.length < 1) {
                $activeChildMenuItem = $(childMenuItemsSelector + ' a[href]').filter(function () {
                    return this.href.toLowerCase() == activeUriRelativeToDocumentRoot.toLowerCase() || this.href.toLowerCase() == location.href.toLowerCase();
                });
            }

            if ($activeChildMenuItem.length > 0) {
                $activeChildMenuItem.addClass('active');

                if (!$activeChildMenuItem.hasClass('parent')) {
                    var parentClass = $activeChildMenuItem.data('parent');

                    if (typeof parentClass !== 'undefined') {
                        parentClass = '.' + parentClass;

                        $(childMenuItemsSelector).find(parentClass).addClass('active');
                    }
                }
            }
        });

        function showBurgerMenu(callback) {
            $rubricsWrapper.show();
            $hamburgerButton.addClass('active');

            menu.overlay(1, null, function () {
                if (typeof callback === 'function') {
                    callback();
                }
            });
        }

        function hideBurgerMenu(callback, hideRubricsWrapper, hideDuration) {
            if (typeof hideRubricsWrapper === 'undefined') {
                hideRubricsWrapper = true;
            }

            menu.hide(function () {
                if (hideRubricsWrapper) {
                    $rubricsWrapper.css('display', '');
                }

                $hamburgerButton.removeClass('active');

                if (typeof callback === 'function') {
                    callback();
                }
            }, hideDuration);
        }

        function isBurgerActive() {
            return $hamburgerButton.filter('.active').length > 0;
        }

        function isDesktopMenuActive() {
            return $('section.basic_nav_opened:visible').length > 0;
        }

        function clearPreviousActiveMenuChildPage() {
            $(childMenuItemsSelector + ' li a').removeClass('active');
        }

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            DBDN.utils.toggleTabletDesktopEvent(function () {
                if (isBurgerActive()) {
                    hideBurgerMenu(function () {
                        $rubricsWrapper.css('display', '');
                    }, false, 0);
                }
            }, function () {
                if (isDesktopMenuActive()) {
                    menu.hide(null, 0);
                }
            }, ev.htmlViewportMode);
        });
    });

    // Tablet only: Prefetch page menus
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var prefetchedMenusKey = 'prefetchedMenus_' + DBDN.utils.getSessionLanguage();
        var $rubricsWrapper = $('header #rubrics_wrapper');

        var fetchMenuContentReal = null;
        var menuShowReal = null;
        var menuHideReal = null;
        var menuItemRealClickHandler = null;
        var fetchMenuWaitingList = {};
        var storageEngine = null;

        // The use of the InMemoryStorage is primarily meant as a workaround
        // for a bug in Safari < 11 when using web store in private mode.
        try {
            storageEngine = sessionStorage;

            var tempKey = '__' + (new Date()).getTime();

            storageEngine.setItem(tempKey, true);
            storageEngine.removeItem(tempKey);
        } catch (e) {
            storageEngine = new DBDN.storage.InMemoryStorage('mainTopMenu');
        }

        // CTRL-F5 to reload the page and clear the prefetched menus
        $(document).keydown(function (ev) {
            if (ev.keyCode == 116 && ev.ctrlKey) {
                clearPrefetchedMenus();
            }
        });

        if (isTabletViewport()) {
            setupTabletEnvironment();
        }

        function setupDesktopEnvironment() {
            menu.fetchMenuContent = fetchMenuContentReal;
            menu.show = menuShowReal;
            menu.hide = menuHideReal;

            if (typeof menuItemRealClickHandler === 'function') {
                $('#rubrics li a')
                    .off('click')
                    .on('click', menuItemRealClickHandler);
            }
        }

        function setupTabletEnvironment() {
            fetchMenuContentReal = menu.fetchMenuContent;
            menuShowReal = menu.show;
            menuHideReal = menu.hide;

            var menuMainMenuItems = $('#rubrics li a');
            var menuMainMenuItemsEvents = menuMainMenuItems.length > 0 ? $._data($('#rubrics li a').eq(0)[0], 'events') || null : null;

            if (menuMainMenuItemsEvents !== null
                && menuMainMenuItemsEvents.hasOwnProperty('click')
                && Array.isArray(menuMainMenuItemsEvents.click)) {
                menuItemRealClickHandler = menuMainMenuItemsEvents.click[0].handler;
            }

            menu.fetchMenuContent = function (menuId, callback) {
                var prefetchedMenu = getPrefetchedMenu(menuId);

                if (prefetchedMenu) {
                    if (typeof callback === 'function') {
                        callback(prefetchedMenu);
                    }

                    return;
                }

                fetchMenuContentReal.call(this, menuId, function () {
                    callback.apply(null, arguments);

                    if (isItInFetchMenuWaitingList(menuId)) {
                        var waitingCallback = getFromFetchMenuWaitingList(menuId);

                        waitingCallback();

                        clearFetchMenuWaitingList();
                    }
                });
            };

            $rubricsWrapper
                .find('ul#rubrics > li')
                .each(function () {
                    var menuItem = this;

                    if (!isMenuAlreadyPrefetched(menuItem)) {
                        setTimeout(function () {
                            menu.fetchMenuContent(menuItem.id, function (data) {
                                addMenuToPrefetchedMenuCollection(menuItem.id, data);
                            });
                        }, 200);
                    }
                });

            // Show preloader while waiting for menu to be prefetched
            // and prevent fetching of menu items for which fetch requests were already
            // fired.
            menu.show = function (ev) {
                var showContext = this;
                var showArguments = arguments;
                var $this = $(showContext);

                Array.prototype.unshift.call(showArguments, ev);

                if (this instanceof HTMLAnchorElement) {
                    var menuId = $this.parent()[0].id;

                    if (isMenuAlreadyPrefetched(menuId)) {
                        menuShowReal.apply(showContext, showArguments);

                        return;
                    }

                    if (!isItInFetchMenuWaitingList(menuId)) {
                        showFetchWaitingPreloader();

                        addToFetchMenuWaitingList(menuId, function () {
                            Array.prototype.push.call(showArguments, function () {
                                hideFetchWaitingPreloader();
                            });

                            menuShowReal.apply(showContext, showArguments);
                        });

                        return false;
                    }
                }
            };

            menu.hide = function (callBack, hideDuration) {
                hideFetchWaitingPreloader();

                menuHideReal.call(this, callBack, hideDuration);
            };

            $('#rubrics li a')
                .off('click')
                .on('click', menu.show);
        }

        function isMenuAlreadyPrefetched(menuId) {
            var prefetchedMenu = getPrefetchedMenu(menuId);

            if (!prefetchedMenu) {
                return false;
            }

            return true;
        }

        function clearPrefetchedMenus() {
            storageEngine.removeItem(prefetchedMenusKey)
        }

        function getPrefetchedMenu(menuId) {
            var prefetchedMenuCollection = getPrefetchedMenuCollection();

            if (prefetchedMenuCollection.hasOwnProperty(menuId)) {
                return prefetchedMenuCollection[menuId];
            }

            return null;
        }

        function getPrefetchedMenuCollection() {
            return JSON.parse(storageEngine.getItem(prefetchedMenusKey) || "{}");
        }

        function addMenuToPrefetchedMenuCollection(menuId, menuContent) {
            var prefetchedMenuCollection = getPrefetchedMenuCollection();

            if (!prefetchedMenuCollection.hasOwnProperty(menuId)) {
                prefetchedMenuCollection[menuId] = menuContent;
            }

            storageEngine.setItem(prefetchedMenusKey, JSON.stringify(prefetchedMenuCollection))
        }

        function addToFetchMenuWaitingList(menuId, callback) {
            fetchMenuWaitingList[menuId] = callback;
        }

        function clearFetchMenuWaitingList() {
            fetchMenuWaitingList = {};
        }

        function isItInFetchMenuWaitingList(menuId) {
            return fetchMenuWaitingList.hasOwnProperty(menuId);
        }

        function getFromFetchMenuWaitingList(menuId) {
            return fetchMenuWaitingList[menuId];
        }

        function showFetchWaitingPreloader() {
            $('#basic_preloader').show();
        }

        function hideFetchWaitingPreloader() {
            $('#basic_preloader').hide();
        }

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            DBDN.utils.toggleTabletDesktopEvent(function () {
                setupDesktopEnvironment();
            }, function () {
                setupTabletEnvironment();
            }, ev.htmlViewportMode);
        });
    });

    // Tablet only: top level overview page - list/view toggle switch, headers, etc.
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var desktopContentSelectorCollection = ['#downloadsForm',
            '.best-practice > .blocks_wrap',
            ':not(.best-practice) > .blocks_wrap',
            '#myDownloadsForm',
            '#watchlistForm'];

        var $desktopContent = null;

        function init() {
            updateDesktopContent();

            if (isTabletViewport()) {
                setupTabletEnvironment();
            }
        }

        function updateDesktopContent() {
            $(desktopContentSelectorCollection.join(','))
                .each(function () {
                    var $this = $(this);
                    var $children = $this.children();

                    if ($children.length > 0) {
                        $desktopContent = $children.clone(true, true);
                        $desktopContent.$parentObject = $this;

                        return false;
                    }
                });
        }

        function updateTabletContent() {
            setupTabletEnvironment();
        }

        function setupDesktopEnvironment() {
            if ($desktopContent === null) {
                return;
            }

            $desktopContent
                .$parentObject
                .hide()
                .empty()
                .append($desktopContent)
                .show();
        }

        function setupTabletEnvironment() {
            // Downloads page
            $('#downloadsForm').hide();

            $('#downloadsForm h1.block_title').each(function () {
                $(this).next('a').addBack().wrapAll('<\div class="second-row-info cf">');
            });
            $('#downloadsForm #view-controls').each(function () {
                $(this).next('h1.hct,h1.single').addBack().wrapAll('<\div class="first-row-info cf">');
            });

            $('#downloadsForm .grid-4cols > div[style*="clear:both"]').remove();
            $('#downloadsForm').show();

            // Bestpractice page
            $('.best-practice > .blocks_wrap').hide();

            $('.best-practice > .blocks_wrap h1.block_title').each(function () {
                $(this).next('a').addBack().wrapAll('<\div class="second-row-info cf">');
            });
            $('.best-practice > .blocks_wrap #view-controls').each(function () {
                $(this).next('h1.hct, h1').addBack().wrapAll('<\div class="first-row-info cf">');
            });

            $('.best-practice > .blocks_wrap .grid-4cols > div[class="cf"]').remove();

            $('.best-practice > .blocks_wrap').show();

            // Design manuals & Corporate Identity
            $(':not(.best-practice) > .blocks_wrap').hide();

            $(':not(.best-practice) > .blocks_wrap h2:first-child').each(function () {
                $(this).next('a').addBack().wrapAll('<\div class="second-row-info cf">');
            });
            $(':not(.best-practice) > .blocks_wrap #view-controls').each(function () {
                $(this).next('h1.hct, h1').addBack().wrapAll('<\div class="first-row-info cf">');
            });

            $(':not(.best-practice) > .blocks_wrap .grid-4cols > div[class="cf"]').remove();

            $(':not(.best-practice) > .blocks_wrap').show();

            // Download cart & Watchlist
            $('#myDownloadsForm, #watchlistForm').hide();

            $('#myDownloadsForm .grid-4cols > div[class="cf"], #watchlistForm .grid-3cols > div[class="cf"]').remove();

            $('#myDownloadsForm, #watchlistForm').show();
        }
        init();

        $(document).on('filteredAjaxDataLoaded.DBDN_General', function () {
            updateDesktopContent();
            updateTabletContent();
        });

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            DBDN.utils.toggleTabletDesktopEvent(function () {
                setupDesktopEnvironment();
            }, function () {
                updateDesktopContent();
                setupTabletEnvironment();
            }, ev.htmlViewportMode);
        });
    });

    // Tablet only: Glossary page - remove margin from the last letter on each row
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var clearedCollection = [];

        function clearMarginStyleFromPreviousLastLetter() {
            if (clearedCollection.length > 0) {
                clearedCollection.pop().css('margin-right', '');
            }
        }

        function removeMarginFromLastLetter() {
            if (!isTabletViewport()) {
                return;
            }

            clearMarginStyleFromPreviousLastLetter();

            var letterPerRow = Math.floor(($('.glossary-navigation').width() - 50) / 58.8) + 1;
            var $lastElement = $('.glossary-navigation a:nth-of-type(' + letterPerRow + 'n) div');
            $lastElement.css('margin-right', 0);

            clearedCollection.push($lastElement);
        }

        removeMarginFromLastLetter();

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            DBDN.utils.toggleTabletDesktopEvent(function () {
                clearMarginStyleFromPreviousLastLetter();
            }, function () {
                removeMarginFromLastLetter();
            }, ev.htmlViewportMode);
        });
    });

    // Tablet only: Filters
    $(function () {
        if (!(isTabletViewport() || isDesktopViewport())) {
            return;
        }

        var $basicNavElement = $('.basic_nav');
        var $menuLabels = $basicNavElement.children('.menu_label').addClass('tablet-popup-entry');
        var filterCompactSelectionReal = null;
        var me = {};

        if ($menuLabels.length < 1) {
            return;
        }

        $basicNavElement = $menuLabels.parent();
        var $filterPanel = $basicNavElement.children('.filter-panel');
        var $filterPanelTitle = $filterPanel.find('.filter-panel-title');
        var $filterPanelSelected = $filterPanel.children('#filter-panel-selected');
        var $window = $(window);
        var basicNavElementIDName = 'tabletFilters';
        var basicNavElementIDSelector = '#' + basicNavElementIDName;
        var sortByFiltersContainerSelector = '.content.second > .level3_nav';
        var collectionOfSortBySelectorsExcludedFromHiddenCheckboxLogic = ['#title_filter'];
        var sortByFiltersWithHiddenCheckboxSelector = basicNavElementIDSelector.prependCSSSelector('.tablet')
            + ' ' + sortByFiltersContainerSelector + ' > li:not(' + collectionOfSortBySelectorsExcludedFromHiddenCheckboxLogic.join(',') + ') .filter_title';
        var sortByFiltersSelector = basicNavElementIDSelector.prependCSSSelector('.tablet')
            + ' ' + sortByFiltersContainerSelector + ' > li .filter_title';
        var hideSortByCheckboxClassName = 'hide-sortby-checkbox';
        var sortByFiltersInputSelector = basicNavElementIDSelector.prependCSSSelector('.tablet')
            + ' ' + sortByFiltersContainerSelector + ' .level4_nav input';
        var selectedSortByFilterClassName = 'selected';
        //var $filterPanelForm = $filterPanel.children('form');

        $filterPanelSelected.addClass('tablet-popup');
        $basicNavElement.children('.menu_label, .content').addClass('tablet-popup');

        $basicNavElement.addClass('filter cf');
        $basicNavElement[0].id = basicNavElementIDName;

        setupTabletEnvironment();
        setupGeneralEnvironment();

        $(document).on('click', '.tablet .basic_nav > .menu_label', function (ev) {
            var $this = $(this);

            toggleFilterFlyout($this);
            closeShowedPopup('.basic_nav > .menu_label');
        });

        $(document).on('click',
            basicNavElementIDSelector.prependCSSSelector('.tablet'),
            function () {
                if ($(this).parent().hasClass('sticky')) {
                    $window.scrollTop(0);
                }
            });

        $(document).on('filteredAjaxDataLoaded.DBDN_General', function () {
            hideFilterFlyout();
        });

        $(document).on('hideFilters.DBDN_Tablet', function () {
            hideFilterFlyout();
        });

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            DBDN.utils.toggleTabletDesktopEvent(function () {
                hideFilterFlyout();
                hideActiveOptionsFlyout();
                setupDesktopEnvironment();
            }, function () {
                $("span.filter_close").trigger('click');
                hideActiveOptionsFlyout();
                setupTabletEnvironment();
                filter.compactSelection();
            }, ev.htmlViewportMode);
        });

        function setupGeneralEnvironment() {
            if (isDesktopViewport()) {
                $basicNavElement.hideForTablet();
            }

            hideSortByFilterCheckboxes();
        }

        function setupDesktopEnvironment() {
            unbindTabletEvents();
            me.reverseChangesMadeByTabletProxyFunctions();

            $basicNavElement.hideForTablet();

            $basicNavElement
                .find('#remove_filters')
                .appendTo($basicNavElement);

            $filterPanel
                .insertBefore($basicNavElement.children('#remove_filters'));

            $basicNavElement.hideForDesktop(false);
        }

        function setupTabletEnvironment() {
            if (!isTabletViewport()) {
                return;
            }

            setupTabletProxyFunctions();

            $basicNavElement.hideForDesktop();

            $basicNavElement
                .children('.unchecked-all')
                .hideBeforeTransition()
                .insertAfter($filterPanelTitle)
                .showAfterTransition();

            $filterPanel
                .hideBeforeTransition()
                .prependTo($basicNavElement)
                .showAfterTransition();

            synchronizeSortByFilterState();
            bindTabletEvents();

            $basicNavElement.hideForTablet(false);
        }

        function setupTabletProxyFunctions() {
            filterCompactSelectionReal = filter.compactSelection;
            var emptyActiveOptionsContent = js_localize['filter.active_options.empty'];
            var $nonEmptyActiveOptionsContent = $filterPanelTitle
                .contents()
                .clone();
            var emptyClassName = 'empty-filter';
            var jqueryShow = jQuery.fn.show;

            filter.compactSelection = function () {
                if (filter.getCountOfSelectedFilterOptions() < 1) {
                    $filterPanel.addClass(emptyClassName);
                    $filterPanelTitle
                        .empty()
                        .text(emptyActiveOptionsContent);
                } else {
                    $filterPanel.removeClass(emptyClassName);
                    $filterPanelTitle
                        .empty()
                        .append($nonEmptyActiveOptionsContent);
                }

                filterCompactSelectionReal.apply(this, arguments);
            };

            jQuery.fn.show = function () {
                if (this.hasClass(emptyClassName)) {
                    this[0].style.setProperty('display', 'block', 'important');

                    return this;
                }

                return jqueryShow.apply(this, arguments);
            };

            me.reverseChangesMadeByTabletProxyFunctions = function () {
                filter.compactSelection = filterCompactSelectionReal;

                $filterPanel.removeClass(emptyClassName);
                $filterPanelTitle
                    .empty()
                    .append($nonEmptyActiveOptionsContent);

                jQuery.fn.show = jqueryShow;
            }
        }

        function unbindTabletEvents() {
            $(document).off('change', sortByFiltersInputSelector);
            $(document).off('click', sortByFiltersWithHiddenCheckboxSelector);
        }

        function bindTabletEvents() {
            $(document).on('change', sortByFiltersInputSelector, function (ev) {
                var $this = $(this);

                clearSortByFilterStateClass();
                setSortByFilterState($this);
            });

            $(document).on('click', sortByFiltersWithHiddenCheckboxSelector, function () {
                var $this = $(this);
                $this.next('li')
                    .children('input[type=checkbox]')
                    .click()
            });
        }

        function hideActiveOptionsFlyout() {
            $filterPanelSelected.removeClass('active');
            menu.overlay(0);
        }

        function hideFilterFlyout(leaveOverlayActive) {
            leaveOverlayActive = leaveOverlayActive || false;
            $menuLabels.removeClass('active');

            if (!leaveOverlayActive) {
                menu.overlay(0);
            }
        }

        function toggleFilterFlyout($filterObj) {
            if ($filterObj.hasClass('active')) {
                $filterObj.removeClass('active');
                menu.overlay(0);
            } else {
                menu.overlay(1);
                $menuLabels.removeClass('active');
                $filterObj.addClass('active');
            }
        }

        function hideSortByFilterCheckboxes() {
            $(sortByFiltersWithHiddenCheckboxSelector).next('li').addClass(hideSortByCheckboxClassName);
        }

        function setSortByFilterState($input) {
            var $filterTitleElement = $input.parent().prev('.filter_title');

            if ($input.prop('checked')) {
                $filterTitleElement.addClass(selectedSortByFilterClassName);
            }
        }

        function clearSortByFilterStateClass() {
            var $sortByFilters = $(sortByFiltersSelector);

            $sortByFilters.removeClass(selectedSortByFilterClassName);
        }

        function synchronizeSortByFilterState() {
            clearSortByFilterStateClass();

            $(sortByFiltersInputSelector).each(function () {
                setSortByFilterState($(this));
            });
        }
    });

    // Tablet only: Make embedded videos size fluid
    $(function () {
        //    if (!(isTabletViewport() || isDesktopViewport())) {
        //        return;
        //    }

        var $allEmbeddedVideos = $('iframe[src*="youtube"]');
        var videoWrapperSelector = 'video-fluid-container';

        function wrapVideoElement() {
            var wrap_selector = 'div.' + videoWrapperSelector;
            var wrap_element = '<\div class="' + videoWrapperSelector + '">';
            $allEmbeddedVideos.each(function () {
                var parent = $(this).parent(wrap_selector);
                if (0 === parent.length) {
                    $(this).wrap(wrap_element)
                    .show()
                    .closest('p').addClass('embedded-video');
                } else {
                    // If already wrapped
                    $(this).show();
                }
            });
        }
        wrapVideoElement();

        $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
            wrapVideoElement();

            //        DBDN.utils.toggleTabletDesktopEvent(function () {
            //            $allEmbeddedVideos.unwrap(videoWrapperSelector);
            //        }, function () {
            //            wrapVideoElement();
            //        }, ev.htmlViewportMode);
        });
    });

    // Tablet only: New zoom-in behaviour of the responsive popup images
    (function () {
        if (isZoomInAllowedToRun()) {
            preventLyteBoxInitialization();
        }

        $(function () {
            var allPopupImagesSelector = 'a[rel*="lytebox"], a[rel*="lyteshow"], a[rel*="lyteframe"]';
            var closeButtonSelector = '.zoomInImgContainer a.close_button';
            var globalHtmlClass = 'zoomed-img-modal';
            var $outerContainer = $('<\div class="zoomInImgContainer">').hide();
            var $innerContainer = $('<\div class="innerImgContainer">');
            var $img = $('<\img class="zoomIn">');
            var $closeButton = $('<div class="buttons_wrapper"><\a class="close_button"></div>');
            var $htmlElement = $('html');
            var $window = $(window);
            var totalMovedDistanceX = 0;
            var totalMovedDistanceY = 0;
            var maxMoveDistanceX = 0;
            var maxMoveDistanceY = 0;
            var scrollOldPosition = 0;
            var outerContainerHeightCorrectionFactor = 0.9375;
            var outerContainerHeightOnceCorrected = false;

            $innerContainer
                .append($img)
                .appendTo($outerContainer);

            $outerContainer.append($closeButton);
            $outerContainer.appendTo(document.body);

            $img.on('load', function () {
                showImgModal();
            });

            allPopupImagesSelector = allPopupImagesSelector
                .prependCSSSelector('.tablet')
                .combineOrCSSSelector(allPopupImagesSelector.prependCSSSelector('.mobile'))
                .combineOrCSSSelector(allPopupImagesSelector.prependCSSSelector('.scaled-viewport.desktop'));
            $(document).on('click', allPopupImagesSelector, function (ev) {
                ev.preventDefault();

                var newImgSource = this.getAttribute('href');

                $img[0].src = '';
                if (newImgSource) {
                    togglePreloader();
                    menu.overlay(1);

                    $img.one('error', function () {
                        this.src = '';
                        this.src = $(this).data('errorSource');
                    });

                    $img.data('errorSource', $(this).children('img')[0].src);
                    $img[0].src = newImgSource;
                }
            });

            $(document).on('click', closeButtonSelector, function () {
                hideImgModal();
            });

            $img.on('movestart', function () {
            })
            .on('move', function (ev) {
                totalMovedDistanceX += ev.deltaX;
                totalMovedDistanceY += ev.deltaY;

                if (totalMovedDistanceX > 0) {
                    totalMovedDistanceX = 0;
                }

                if (totalMovedDistanceY > 0) {
                    totalMovedDistanceY = 0;
                }

                if (Math.abs(totalMovedDistanceX) >= maxMoveDistanceX) {
                    totalMovedDistanceX = -maxMoveDistanceX;
                }

                if (Math.abs(totalMovedDistanceY) >= maxMoveDistanceY) {
                    totalMovedDistanceY = -maxMoveDistanceY;
                }

                moveImage(totalMovedDistanceX, totalMovedDistanceY);
            });

            $(document).on('tabletDesktopViewportChange.DBDN_General', function (ev) {
                DBDN.utils.toggleTabletDesktopEvent(function () {
                    if (!DBDN.utils.isDesktopScaledViewport()) {
                        hideImgModal();
                        lyteBoxInitialize();
                    }
                }, function () {
                    destroyLyteBoxInstance();
                    // Prevent an attempt to initialize at later stage in the execution flow.
                    preventLyteBoxInitialization();
                }, ev.htmlViewportMode);
            });

            $(document).on('tabletTabletViewportChange.DBDN_General mobileMobileViewportChange.DBDN_General tabletDesktopViewportChange.DBDN_General desktopDesktopViewportChange.DBDN_General', function () {
                if (!isZoomInAllowedToRun()) {
                    return;
                }

                if (!$outerContainer.is(':visible')) {
                    return;
                }

                correctContainerHeightOnBuggyBrowsers();
                updateMaxMoveDistance();

                if (Math.abs(totalMovedDistanceX) >= maxMoveDistanceX) {
                    totalMovedDistanceX = -maxMoveDistanceX;
                }

                if (Math.abs(totalMovedDistanceY) >= maxMoveDistanceY) {
                    totalMovedDistanceY = -maxMoveDistanceY;
                }

                moveImage(totalMovedDistanceX, totalMovedDistanceY);
            });

            function clearContainerCorrections() {
                $outerContainer.css('max-height', '');
                $innerContainer.css('max-height', '');
                outerContainerHeightOnceCorrected = false;
            }

            function correctContainerHeightOnBuggyBrowsers() {
                var outerContainerBoundingRectangle = DBDN.utils.getBoundingClientRectOfHiddenElement($outerContainer);

                if (typeof outerContainerBoundingRectangle.height === 'undefined' || !outerContainerBoundingRectangle) {
                    return;
                }

                // The min method is necessary because some browsers behave differently in portrait and
                // landscape orientations. Amazed, huh?
                var windowHeightMinusTopAndBottomBrowserBars = Math.min($(window).height(), window.innerHeight);

                if (outerContainerBoundingRectangle.height > windowHeightMinusTopAndBottomBrowserBars || outerContainerHeightOnceCorrected) {
                    var maxHeight = windowHeightMinusTopAndBottomBrowserBars * outerContainerHeightCorrectionFactor;

                    $outerContainer.css('max-height', maxHeight + 'px');
                    $innerContainer.css('max-height', maxHeight + 'px');

                    outerContainerHeightOnceCorrected = true;
                }
            }

            function goBackToOldScrollPosition() {
                $window.scrollTop(scrollOldPosition);
            }

            function updateOldScrollValue() {
                scrollOldPosition = $window.scrollTop();
            }

            function hideImgModal() {
                menu.overlay(0);
                $outerContainer.hide();
                $htmlElement.removeClass(globalHtmlClass);
                clearContainerCorrections();
                goBackToOldScrollPosition();
                restoreInterferingFunctionalities();
            }

            function showImgModal() {
                updateOldScrollValue();
                resetTotalMovedDistance();
                resetImagePositionToInitialCoordinates();
                correctContainerHeightOnBuggyBrowsers();
                togglePreloader();
                $htmlElement.addClass(globalHtmlClass);
                $outerContainer.show();
                updateMaxMoveDistance();
                suspendInterferingFunctionalities();
            }

            function togglePreloader() {
                var $preloader = $('#basic_preloader');

                if ($preloader.is(':visible')) {
                    $preloader.hide();
                } else {
                    $preloader.show();
                }
            }

            function resetTotalMovedDistance() {
                totalMovedDistanceX = 0;
                totalMovedDistanceY = 0;
            }

            function resetImagePositionToInitialCoordinates() {
                moveImage(0, 0);
            }

            function moveImage(x, y) {
                $img.css('transform', 'translate3d(' + x + 'px, ' + y + 'px, 0px)');
            }

            function updateMaxMoveDistance() {
                maxMoveDistanceX = $img.outerWidth() - $outerContainer.width();
                maxMoveDistanceY = $img.outerHeight() - $outerContainer.height();
            }
        });

        function isZoomInAllowedToRun() {
            if (isTabletViewport()
                || isMobileViewport()
                || DBDN.utils.isDesktopScaledViewport()) {
                return true;
            }

            return false;
        }

        function suspendInterferingFunctionalities() {
            $(document).trigger('suspendComponentClosingOnTabletDesktop.DBDN_General');
        }

        function restoreInterferingFunctionalities() {
            $(document).trigger('unsuspendComponentClosingOnTabletDesktop.DBDN_General');
        }

        function preventLyteBoxInitialization() {
            var myLytebox = {};
            window.myLytebox = myLytebox;

            myLytebox.updateLyteboxItems = function () {
                // simply do nothing
            };
        }

        function lyteBoxInitialize() {
            if (typeof window.myLytebox !== 'undefined'
                && window.myLytebox instanceof LyteBox) {
                return;
            }

            window.myLytebox = null;
            delete window.myLytebox;

            initLytebox();
        }

        function destroyLyteBoxInstance() {
            if (typeof window.myLytebox !== 'undefined'
                && window.myLytebox instanceof LyteBox) {
                window.myLytebox.destroy();
            }
        }
    }());

    // Tablet/Mobile only: Add custom horizontal scrolls to specific components
    $(function () {
        var listOfSelectorsToAddScrollBarTo = [{
            mainParentContainerSelector: '.grid-3cols:not(.manuals)',
            attachToSelector: '.grid-3cols:not(.manuals)',
            viewportSelector: '.grid-3cols:not(.manuals)',
            scrollableContentSelector: '.inner1-wrapper',
            scrollBarClassList: 'most-something',
            scrollBarInlineStyles: { 'width': '-webkit-calc(100% - 20px)', 'width': 'calc(100% - 20px)' },
            scrollBarHandleType: 'fixed',
            applyCondition: function () {
                if (isTabletViewport()) {
                    return true;
                }

                return false;
            },
            position: { 'offset_top': 20 }
        },
        {
            mainParentContainerSelector: 'table',
            attachToSelector: '#content_main table',
            viewportSelector: 'table',
            scrollableContentSelector: 'tbody',
            applyCondition: function () {
                if (isMobileViewport()) {
                    return true;
                }

                return false;
            },
            hideBrowserScroll: true,
            position: { 'offset_top': 10 }
        },
        {
            mainParentContainerSelector: '.slider_wrapper',
            attachToSelector: '.slider_wrapper',
            viewportSelector: '.slider_wrapper',
            scrollableContentSelector: '.slider_container',
            scrollBarClassList: 'most-something',
            scrollBarInlineStyles: { 'width': '-webkit-calc(100% - 20px)', 'width': 'calc(100% - 20px)' },
            scrollBarHandleType: 'fixed',
            applyCondition: function () {
                if (isTabletViewport()) {
                    return true;
                }

                return false;
            },
            position: { 'offset_top': 20 }
        },
        {
            mainParentContainerSelector: '.gallery-main-wrapper',
            attachToSelector: '.gallery-main-wrapper',
            viewportSelector: '.galCropper',
            scrollableContentSelector: '.galMover',
            initializeOnEvent: 'singleImageGalleryCarouselLoaded.DBDN_General',
            initializeOnEventTargetElement: document,
            updateScrollDimensionPropertiesOnEvent: 'afterGallerySetupCroppers.DBDN_General',
            observeValueCallback: function () {
                if (arguments[0][0].target.style.left !== galleryMoverOldLeftPosition) {
                    galleryMoverOldLeftPosition = arguments[0][0].target.style.left;

                    return Math.abs(parseFloat(arguments[0][0].target.style.left));
                }
            },
            observeOptions: {
                attributes: true,
                attributeFilter: ['style']
            },
            applyCondition: function () {
                if (isMobileViewport()) {
                    return true;
                }

                return false;
            },
            position: { 'offset_top': 10 }
        }];
        var $body = $(document.body);
        var galleryMoverOldLeftPosition = null;
        var appName = 'custom_scrollbar';
        var eventWaitingKey = appName + '_' + 'eventWaiting';

        function ScrollBar(targetElement, userTargetOptions) {
            if (typeof ScrollBar.getPublicInstance(targetElement) !== 'undefined') {
                return ScrollBar.getPublicInstance(targetElement);
            }

            var targetOptions = {
                scrollSourceType: 'browserScroll',
                scrollBarClassList: '',
                behaviour: 'wrap',
                scrollBarInlineStyles: {},
                scrollBarHandleType: 'dynamic',
                applyCondition: '',
                hideBrowserScroll: false, // works only with 'wrap' behaviour
                updateScrollBarHandlePositionManually: false,
                initializeOnEvent: null,
                initializeOnEventTargetElement: null,
                observeValueCallback: null,
                observeOptions: {},
                pollForChangesAfterExternalUpdateOfScrollDimensionProperties: false,
                updateScrollDimensionPropertiesOnEvent: null,
                updateScrollDimensionPropertiesOnEventTargetElement: null
            };
            var $scrollBarElement = null;
            var $scrollBarHandleElement = null;
            var $scrollableContentElement = null;
            var $mainParentContainerElement = null;
            var $viewportContainerElement = null;
            var $targetElementToAttachScrollTo = $(targetElement);
            var scrollEventObj = null;
            var resizeEventObj = null;
            var lastScrollXPos = 0;
            var scrollXFactor = 0;
            var $scrollBarMainWrapper = $('<\div class="custom-scrollbar-wrapper">');
            var instanceInitialized = false;
            var instanceDestroyed = false;
            var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
            var mutationObserverInstance = null;
            var publicInstance = {};

            $.extend(targetOptions, userTargetOptions);

            function init() {
                setRefToMainParentContainerElement();
                setRefToViewportContainerElement();
                setRefToScrollableContentElement();

                if (!isScrollNeeded()) {
                    return false;
                }

                setRefToScrollBarElement();
                setRefToScrollBarHandle();

                setupScrollBarBehaviour();
                setupScrollBarInlineStyles();

                updateScrollDimensionProperties();

                if (!targetOptions.updateScrollBarHandlePositionManually) {
                    bindEvents();
                    bindObservers();
                }

                $scrollBarElement.addClass(targetOptions.scrollBarClassList);

                $scrollBarElement.show();

                hideBrowserScroll();

                checkTheScrollableComponentForSizeChanges();

                storePublicInstance();

                instanceInitialized = true;
            }

            function setupScrollBarInlineStyles() {
                $scrollBarElement.css(targetOptions.scrollBarInlineStyles);
            }

            function setupScrollBarBehaviour() {
                switch (targetOptions.behaviour) {
                    case 'attach':
                        $body.append($scrollBarElement);
                        break;
                    case 'wrap':
                        $targetElementToAttachScrollTo.wrap($scrollBarMainWrapper);
                        $targetElementToAttachScrollTo.parent().append($scrollBarElement);
                        break;
                    default:
                }
            }

            function bindObservers() {
                if (targetOptions.observeValueCallback === null) {
                    return;
                }

                if (mutationObserverInstance === null) {
                    mutationObserverInstance = new MutationObserver(function (mutations) {
                        var newPositionValue = targetOptions.observeValueCallback(mutations);

                        updateScrollHandlePosition(newPositionValue);
                    });
                }

                mutationObserverInstance.observe($scrollableContentElement[0], targetOptions.observeOptions);
            }

            function bindEvents() {
                if (targetOptions.observeValueCallback === null) {
                    scrollEventObj = new DBDN.events.ScrollEvent($scrollableContentElement, 0, function (ev) {
                        var currentScrollXPos = $scrollableContentElement.scrollLeft();

                        updateScrollHandlePosition(currentScrollXPos);
                    });
                }

                if (targetOptions.updateScrollDimensionPropertiesOnEvent !== null) {
                    var target = getUpdateScrollDimensionPropertiesOnEventTarget();

                    $(target).on(targetOptions.updateScrollDimensionPropertiesOnEvent, function () {
                        updateScrollDimensionProperties();
                    });
                }
            }

            function isScrollNeeded() {
                if (typeof targetOptions.applyCondition === 'function'
                    && !targetOptions.applyCondition()) {
                    return false;
                }

                if ($scrollableContentElement[0].scrollWidth > $viewportContainerElement.width()) {
                    return true;
                }

                return false;
            }

            function setRefToScrollBarHandle() {
                $scrollBarHandleElement = $scrollBarElement.find('.handle');
            }

            function setRefToScrollBarElement() {
                $scrollBarElement = getNewScrollBarElement().hide();
            }

            function setRefToViewportContainerElement() {
                $viewportContainerElement = $targetElementToAttachScrollTo.closest(targetOptions.viewportSelector);

                if ($viewportContainerElement.length < 1) {
                    $viewportContainerElement = $targetElementToAttachScrollTo.find(targetOptions.viewportSelector);
                }
            }

            function setRefToMainParentContainerElement() {
                $mainParentContainerElement = $targetElementToAttachScrollTo.closest(targetOptions.mainParentContainerSelector);

                if ($mainParentContainerElement.length < 1) {
                    throw new Error('Cannot update the ref. to the main parent container.');
                }
            }

            function setRefToScrollableContentElement() {
                $scrollableContentElement = $mainParentContainerElement.find(targetOptions.scrollableContentSelector);
            }

            function updateScrollDimensionProperties() {
                updateScrollBarPosition();
                updateScrollHandleSize();

                scrollXFactor = ($scrollBarElement.width() - $scrollBarHandleElement.width()) / ($scrollableContentElement[0].scrollWidth - $viewportContainerElement.width());
            }

            function updateScrollHandleSize() {
                if (targetOptions.scrollBarHandleType == 'fixed') {
                    return;
                }

                var newWidthSize = ($viewportContainerElement.width() / $scrollableContentElement[0].scrollWidth) * $scrollBarElement.width();

                $scrollBarHandleElement.css('width', newWidthSize + 'px');
            }

            function updateScrollHandlePosition(newXPosition) {
                if (lastScrollXPos !== newXPosition) {
                    $scrollBarHandleElement.css('transform', 'translate3d(' + (newXPosition * scrollXFactor) + 'px, 0px, 0px)');

                    lastScrollXPos = newXPosition;
                }
            }

            function updateScrollBarPosition() {
                var targetDocsOffset = $targetElementToAttachScrollTo.offset();
                var targetHeight = $targetElementToAttachScrollTo.outerHeight();
                var newScrollVerticalPosition = targetDocsOffset.top + targetHeight + targetOptions.position.offset_top;

                if (targetOptions.behaviour == 'wrap') {
                    //newScrollVerticalPosition = $targetElementToAttachScrollTo.height() + targetOptions.position.offset_top;
                    $scrollBarElement.css('bottom', 'calc(0% - ' + targetOptions.position.offset_top + 'px)');
                } else {
                    $scrollBarElement.css('top', newScrollVerticalPosition + 'px');
                }
            }

            function checkTheScrollableComponentForSizeChanges() {
                var initialScrollWidth = $scrollableContentElement[0].scrollWidth;

                DBDN.utils.poll(function () {
                    var currentScrollWidth = $scrollableContentElement[0].scrollWidth;

                    if (initialScrollWidth < currentScrollWidth || initialScrollWidth > currentScrollWidth) {
                        return true;
                    }

                    return false;
                }, function () {
                    initialScrollWidth = $scrollableContentElement[0].scrollWidth;

                    updateScrollDimensionProperties();
                }, function () {
                    return instanceDestroyed;
                }, 300, 10000);
            }

            function getUpdateScrollDimensionPropertiesOnEventTarget() {
                if (targetOptions.updateScrollDimensionPropertiesOnEventTargetElement !== null) {
                    return targetOptions.updateScrollDimensionPropertiesOnEventTargetElement;
                }

                return $targetElementToAttachScrollTo;
            }

            function getNewScrollBarElement() {
                return $(getScrollBarTemplate());
            }

            function getScrollBarTemplate() {
                var html = '<div class="custom-scrollbar"><div class="handle"></div></div>';

                return html;
            }

            function hideBrowserScroll(show) {
                if (targetOptions.hideBrowserScroll
                    && targetOptions.behaviour == 'wrap') {
                    var scrollableFixedHeight = $scrollableContentElement.outerHeight();

                    $viewportContainerElement
                        .addClass('default-scroll-hidden');

                    $viewportContainerElement[0].style.setProperty('height', scrollableFixedHeight + 'px', 'important');

                    $scrollableContentElement
                        .addClass('scrollable');
                }
            }

            function showBrowserScroll() {
                if (targetOptions.hideBrowserScroll
                    && targetOptions.behaviour == 'wrap') {
                    $viewportContainerElement
                        .css({ 'height': '' })
                        .removeClass('default-scroll-hidden');

                    $scrollableContentElement
                        .removeClass('scrollable');
                }
            }

            function setWaitingOnEventFlag(waiting) {
                if (typeof waiting === 'undefined') {
                    waiting = false;
                }

                $.data(targetElement, eventWaitingKey, waiting);
            }

            function storePublicInstance() {
                $.data(targetElement, appName, publicInstance);
            }

            function removePublicInstance() {
                $.removeData(targetElement, appName);
            }

            function destroy(partially) {
                if (mutationObserverInstance !== null) {
                    mutationObserverInstance.disconnect();
                }

                if (mutationObserverInstance === null) {
                    scrollEventObj.stopEvent();
                }

                if (partially !== true) {
                    removePublicInstance();
                }

                if (targetOptions.updateScrollDimensionPropertiesOnEvent !== null) {
                    $(getUpdateScrollDimensionPropertiesOnEventTarget()).off(targetOptions.updateScrollDimensionPropertiesOnEvent);
                }

                $scrollBarElement.remove();
				if($targetElementToAttachScrollTo.parent().is($scrollBarMainWrapper[0].className)){
                    $targetElementToAttachScrollTo.unwrap($scrollBarMainWrapper[0].className);
                }
                showBrowserScroll();
                instanceDestroyed = true;
            }

            publicInstance.hide = function () {
                destroy(true);
            };

            publicInstance.reinit = function () {
                init();
            };

            publicInstance.destroy = function () {
                destroy();
            };

            publicInstance.updateScrollDimensionProperties = function () {
                updateScrollDimensionProperties();

                if (targetOptions.pollForChangesAfterExternalUpdateOfScrollDimensionProperties) {
                    checkTheScrollableComponentForSizeChanges();
                }
            };

            publicInstance.updateScrollBarHandlePosition = function (newXPosition) {
                if (targetOptions.updateScrollBarHandlePositionManually) {
                    updateScrollHandlePosition(newXPosition);
                }
            };

            publicInstance.isScrollNeeded = function () {
                return isScrollNeeded();
            };

            publicInstance.isInstanceInitialized = function () {
                return instanceInitialized;
            };

            publicInstance.isInstanceDestroyed = function () {
                return instanceDestroyed;
            };

            if (targetOptions.initializeOnEvent !== null) {
                setWaitingOnEventFlag(true);

                $(targetOptions.initializeOnEventTargetElement).one(targetOptions.initializeOnEvent, function () {
                    init();
                    setWaitingOnEventFlag(false);
                });
            } else {
                init();
            }

            return publicInstance;
        }

        ScrollBar.alreadyWaitingToBeInitializedOnEvent = function (targetElement) {
            var flag = $.data(targetElement, eventWaitingKey);

            if (typeof flag !== 'undefined' && flag) {
                return true;
            }

            return false;
        };

        ScrollBar.getPublicInstance = function (targetElement) {
            return $.data(targetElement, appName);
        };

        function createOrUpdateScrollBarInstances() {
            $.each(listOfSelectorsToAddScrollBarTo, function () {
                var options = this;

                $(options.attachToSelector).each(function () {
                    var existingInstance = ScrollBar.getPublicInstance(this);

                    if (typeof existingInstance !== 'undefined') {
                        if (existingInstance.isScrollNeeded() && !existingInstance.isInstanceDestroyed()) {
                            existingInstance.updateScrollDimensionProperties();
                        }

                        if (existingInstance.isScrollNeeded() && existingInstance.isInstanceDestroyed()) {
                            existingInstance.reinit();
                        }

                        if (!existingInstance.isScrollNeeded()) {
                            existingInstance.hide();
                        }

                        return true;
                    }

                    if (!ScrollBar.alreadyWaitingToBeInitializedOnEvent(this)) {
                        new ScrollBar(this, options);
                    }
                });
            });
        }

        if (!isDesktopViewport()) {
            createOrUpdateScrollBarInstances();
        }

        $(document).on('tabletDesktopViewportChange.DBDN_General tabletTabletViewportChange.DBDN_General mobileMobileViewportChange.DBDN_General filteredAjaxDataLoaded.DBDN_General', function (ev) {
            createOrUpdateScrollBarInstances();
        });
    });

    // Tablet/Desktop only: Change header banner with device specific image
    $(function () {
        if (isTabletViewport() || isDesktopViewport()) {
            $(document).on('tabletDesktopViewportChange.DBDN_General', function () {
                var $bannerVisibleImage = $('.banner_load_wrapper:visible img.simple');
                imageProcess.lazyLoadWithCallBack($bannerVisibleImage);
                imageProcess.updateLazyload();
            });
        }
    });

    // Mobile only: ADD and LIKE
    $(function () {
        if (!isMobileViewport()) {
            return;
        }
        $(document).on('click', 'nav#cd-vertical-nav li#add-icon a, nav#cd-vertical-nav li#rate-icon a, nav#cd-vertical-nav li#remove-icon a', function (event) {
            event.preventDefault();

            if (!DBDN.utils.isSessionAuthenticated()) {
                var what = $(this).closest('li').attr('id').split('-')[0];
                $(document).trigger('pageActions_showUnauthAlertText.DBDN_Tablet', js_localize['page.actions.unauth_' + what + '_alert_text']);
                $(window).scrollTop(0);
            } else {
                var $parent = $(this).parent();
                if ($parent[0].id == 'rate-icon' && $parent.hasClass('unactive')) {
                    $("#notification .alert_text").html(js_localize['page.rated'])
                    generateNotification();
                } else {
                    $parent.submit();
                }
            }
        });

        if (MOBILE) {
            $.each(['div#downloads:not(.topItem)', 'div.comment-form', 'div#contact_info', 'div#content_main'], function (index, selector) {
                var el = $(selector);
                if (1 === el.length) {
                    el.after(
                        $('<section />').attr({ id: 'clonedItems', style: 'position:relative;' }).append(
                                $('div#best_practice_stats').clone(),
                                $('div#downloads_stats').clone(),
                                $('nav#cd-vertical-nav').clone()
                            )
                    );
                    return false;
                }
            });

            $('div#hamburger').on('click', 'div.switch', function () {
                var p = $(this).parent();
                if (p.hasClass('active')) {
                    $('div#searchDiv > div.basic_nav').css('z-index', '');
                    menu.overlay(false);
                    p.removeClass('active');
                } else {
                    $('div#searchDiv > div.basic_nav').css('z-index', 4);
                    menu.overlay(true);
                    p.addClass('active');
                }
                window.scrollTo(0, 0);
            });

            $('h1.block_title + a').each(function () {
                var t = $(this), h = t.prev().height();
                t.css('margin-top', 20 + h / 2 + 'px');
                // console.log(h, 40 + h/2 + 'px');
            });

            // fix for #16454
            var glr = $('div.glossary-result');
            if (1 === glr.length)
                window.scrollTo(0, glr.offset().top);

            // fix for #16458
            $('div.basic_nav div.content.second ul.level4_nav').each(function () {
                var t = $(this), lis = t.find('li');

                if (2 !== lis.length)
                    return;

                var tit = $(lis[0]), fld = $(lis[1]), inp = fld.find('input');

                if (1 !== inp.length)
                    return;

                fld.hide();

                if (inp[0].checked)
                    tit.addClass('checked');


                tit.click(function () {
                    $(this).next().find('input').trigger('click');
                });


            });

            $('div.basic_nav div.content.second input').change(function () {
                $('div.basic_nav div.content.second ul.level4_nav li.filter_title').removeClass('checked');
                $(this).parent('li').prev().toggleClass('checked', this.checked);
            });
            // end of fix for #16458


            // fix for #16380
            $(document).on('click', 'div.filter-panel.no-empty', function (e) {
                if ('FORM' == e.target.tagName) {
                    $('div.filter-panel-title').trigger('click');
                }
            });


        } // if( MOBILE)


    });
}

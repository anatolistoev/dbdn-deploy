<?php 

return array(

	'column.id' => 'Id',
	'column.name'     => 'Name',
	'placeholder.group_search'     => 'Group Search',
	'placeholder.group_sort'     => 'Group Sort',
	'button.filter'     => 'FILTER',
	'edit'     => 'Edit Group',
	'add'     => 'Add Group',
	'remove'     => 'Remove',
	'remove.confirm' => 'Are you sure you want to delete the selected group?',
	'form.registered'     => 'Date added',
	'form.last_update'     => 'Date modified',
	'form.name'     => 'Name',
);
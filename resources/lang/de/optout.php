<?php

return array(
	/*
	|--------------------------------------------------------------------------
	| Newsletter Opt-out email message Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'subject'               => 'Daimler Brand & Design Navigator – Newsletter-Abmeldung',
    'greeting'              => 'Hallo :FIRST_NAME :LAST_NAME,',
	'content'               => 'Sie erhalten diese E-Mail, weil Sie die Einstellungen in Ihrem Benutzerprofil für den Daimler Brand & Design Navigator geändert und die Checkbox „Newsletter abonnieren“ abgewählt haben. <br>
                                <br>
                                Wir möchten Sie darüber informieren, dass Sie den Daimler Brand & Design Navigator Newsletter erfolgreich abbestellt haben.<br>
                                <br>
                                Bitte beachten Sie, dass Sie weiterhin automatische Benachrichtigungen zu erfolgten Änderungen auf Seiten aus Ihrer Merkliste im Daimler Brand & Design Navigator erhalten. Um solche Benachrichtigungen abzubestellen, entfernen Sie bitte die gespeicherten Seiten aus Ihrer Merkliste, indem Sie sich mit Ihrem Benutzernamen und dem dazugehörigen Passwort  in Ihrem Benutzerprofil anmelden.<br>
                                <br>
                                <br>
                                 Mit freundlichen Grüßen<br>',
    'footer'                => 'Daimler Communications<br>
                                Corporate Design<br>
                                Daimler AG<br>
                                096-E402<br>
                                70546 Stuttgart, Germany<br>
                                corporate.design@daimler.com<br>
                                <br>
                                <br>
                                Daimler AG<br>
                                Sitz und Registergericht/Domicile and Court of Registry: Stuttgart<br>
                                HRB-Nr./Commercial Register No. 19360<br>
                                Vorsitzender des Aufsichtsrats/Chairman of the Supervisory Board: Manfred Bischoff<br>
                                Vorstand/Board of Management: Ola Källenius (Vorsitzender/Chairman), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm<br>',


);
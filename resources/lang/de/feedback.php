<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Feedback Page Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/
	
	
	'path'					=> 'Feedback geben',
	'page.title'			=> 'E-Mail an Autor senden.',
	'contents'				=> 'Feedback betreffend:',
		
	'thanks'				=> 'Ihre Nachricht wurde erfolgreich versendet.<br>Vielen Dank. ',
	'back'					=> 'Zur&uuml;ck',
		
	'email'					=> 'Ihre E-Mail-Adresse:',
	'message'				=> 'Ihre Mitteilung:',
		
	'captcha'				=> 'Bitte geben Sie die Zeichen ein:',
	'send'					=> 'Abschicken',
	
	'question_regarding' 	=> 'Frage betreffend',
	'email.email'			=> 'E-Mail',
	'email.name'			=> 'Name',
	'email.subject'			=> 'Daimler Brand & Design Navigator - Hilfeanfrage',
	'email.message'			=> 'Frage',
	
	);
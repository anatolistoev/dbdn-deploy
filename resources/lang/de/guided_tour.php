<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Guided Tour texts
	|--------------------------------------------------------------------------
	|
	|
	*/
    'tour_0.header'         => 'Kurz vorgestellt – Guided Tour',
    'tour_0.text'           => '<p>Nutzen Sie die Guided Tour, um mehr über den Daimler Brand & Design Navigator zu erfahren und einen Überblick über seine Themen und Funktionen zu bekommen. Nach dem Start können Sie die Guided Tour jederzeit beenden, indem Sie das Fenster schließen.</p>',
    'tour_1.header'         => 'Die Startseite',
    'tour_1.text'           => '<p>Als erster Zugangspunkt zum Online-Portal bietet die Startseite eine stets aktuelle Übersicht über alle Neuerungen und Änderungen zu aktuellen Themen einzelner Marken und Gesellschaften.</p>',
    'tour_2.header'         => 'Exponierte Bühne',
    'tour_2.text'           => '<p>Die Banner nehmen die volle Breite der Startseite ein und verweisen auf aktuelle Themen. Mit einem Klick auf die Bannerfläche gelangen Nutzer zur verknüpften Seite.</p>',
    'tour_3.header'         => 'Die Schnellsuche',
    'tour_3.text'           => '<p>Das Suchfeld unterstützt die Nutzer mittels Autovervollständigung beim schnellen Auffinden von Informationen. Unterhalb des Suchfeldes können Ansichten über meist gesuchte, häufig besuchte und beliebte Seiten sowie meist heruntergeladene Dateien in Echtzeit aufgerufen werden.</p>',
    'tour_4.header'         => 'Das Neueste',
    'tour_4.text'           => '<p>Die Neuigkeiten können in zwei verschiedenen Ansichten angezeigt werden: Als Kompaktansicht oder in detaillierter Form.</p>',
    'tour_5.header'         => 'Personalisierter Zugang',
    'tour_5.text'           => '<p>Registrierte Nutzer erhalten Zugriff auf zusätzliche Funktionen und können Zugangsberechtigung für passwortgeschützte Seiten beantragen. Die Merkliste benachrichtigt Nutzer automatisch, wenn Änderungen an gespeicherten Inhalten erfolgt sind. Der Downloadkorb dient als virtuelle Ablage für Dateien, die einzeln oder gesammelt heruntergeladen werden können.</p>',
    'tour_6.header'         => 'Der erste Klick',
    'tour_6.text'           => '<p>Brand & Design Navigator präsentiert Design Manuals und Downloads verschiedener Marken im Daimler Konzern. Dieses Auswahlmenü bietet einen schnellen Zugriff auf alle Marken und Gesellschaften im Online-Portal.</p>',
    'tour_7.header'         => 'Zentrale Navigation',
    'tour_7.text'           => '<p>Die zentrale Navigationsleiste mit den Hauptrubriken befindet sich horizontal über jeder Seite und bleibt beim Scrollen in einer reduzierten Variante immer sichtbar.</p>',
    'tour_8.header'         => 'Karussellartige Übersicht',
    'tour_8.text'           => '<p>In diesem Abschnitt werden weitere neue Themen der jeweiligen Marke oder Gesellschaft angezeigt. Jeder Teaser besteht aus einem Vorschaubild und einem kurzen Einleitungstext. Die untere Leiste dient als Navigationshilfe zu den vorgestellten Seiten.</p>',
    'tour_9.header'         => 'Serviceleiste',
    'tour_9.text'           => '<p>Die Serviceleiste mit den Links Hilfe, Glossar, FAQ und Sitemap haftet am unteren Rand im Browserfenster. Mit dem Hilfeformular haben Nutzer die Möglichkeit Seiteninhalte zu kommentieren oder dem Redaktionsteam eine Nachricht zu senden. Das Glossar und die FAQs geben weitere Hilfestellung.</p>',
    'tour_10.header'        => 'Corporate Identity – Die Einführung',
    'tour_10.text'          => '<p>Dieser Themenbereich beschreibt die Positionierung, Markenarchitektur und Prinzipien für Branding der Unternehmensmarke und bildet die Basis für das bessere Verständnis der visuellen Identität von Daimler. </p>',
    'tour_11.header'        => 'Design Manuals – Das Herzstück',
    'tour_11.text'          => '<p>Die Inhalte des Brand & Design Navigator haben Richtliniencharakter. Mit praxisnahen Regeln und geeigneten Visualisierungen macht das Online-Portal das Erscheinungsbild von Daimler online erlebbar.</p>',
    'tour_12.header'        => 'Quickmenü',
    'tour_12.text'          => '<p>Mit Hilfe des linken Menüs können Nutzer durch die verfügbaren Manuals navigieren. Der dazugehörige Quicklink „Zurück“ ermöglicht ein einfaches Zurückblättern.</p>',
    'tour_13.header'        => 'Unternavigation',
    'tour_13.text'          => '<p>Mit den Punkten in der rechten Leiste können wesentliche Abschnitte einer Inhaltsseite gezielt abgerufen werden. Die Unternavigation kann als Scrollleiste nach oben und nach unten genutzt werden.</p>',
    'tour_14.header'        => 'Funktionen',
    'tour_14.text'          => '<p>Die Funktionen für Drucken und Bewerten bewegen sich synchron mit der Navigationsrichtung und bleiben am rechten Rand im Browserfenster immer sichtbar. Die druckoptimierte Version einer Seite wird in einem separaten Browserfenster geöffnet.</p>',
    'tour_15.header'        => 'Verwandtes',
    'tour_15.text'          => '<p>Listen mit verwandten Manuals oder Downloads können per Klick in der rechten Leiste eingeblendet werden. </p>',
    'tour_16.header'        => 'Zoomfaktor',
    'tour_16.text'          => '<p>Die Bildvergrößerung zeigt die Beispiele zu den Designregeln in geeigneter Weise und bietet in der Detailansicht einen Zoomfaktor von bis zu 400%.</p>',
    'tour_17.header'        => 'Glossar',
    'tour_17.text'          => '<p>Unterstreichungen in den Designregeln weisen auf Begriffe im Glossar hin, die durch Anklicken aufgerufen werden können.</p>',
    'tour_18.header'        => 'Buchnavigation',
    'tour_18.text'          => '<p>Die Schaltflächen stellen eine weitere Option für Vorwärts- und Rückwärtsnavigation durch die Manuals dar. Inhalte können so Schritt für Schritt abgerufen und gelesen werden.</p>',
    'tour_19.header'        => 'Best Practice – Aktuelles Magazin',
    'tour_19.text'          => '<p>In den Best Practice-Beispielen, dem Magazin des Brand & Design Navigator, werden regelmäßig gelungene und CD-konforme Umsetzungen aus dem Konzern vorgestellt.</p>',
    'tour_20.header'        => 'Ausklappbares Filtermenü',
    'tour_20.text'          => '<p>Die veröffentlichten Berichte können alphabetisch und aktualitätsbezogen oder nach verschiedenen Kategorien und Schlagworten sortiert werden. Das Filtermenü ist immer sichtbar – auf Unterseiten und beim Scrollen.</p>',
    'tour_21.header'        => 'Mehrwert durch Dialog',
    'tour_21.text'          => '<p>Registrierte Nutzer können die Beispiele kommentieren und Erfahrungen mit anderen Nutzern austauschen.</p>',
    'tour_22.header'        => 'Downloads – Anwendung in Aktion',
    'tour_22.text'          => '<p>Zur Sicherung der markengerechten Umsetzung des Corporate Design übernimmt Brand & Design Navigator die Bereitstellung von Grafiken, Vorlagen und weiteren Dokumenten für alle Anwendungsbereiche der Daimler Unternehmenskommunikation.</p>',
    'tour_23.header'        => 'Ausklappbares Filtermenü',
    'tour_23.text'          => '<p>Die bereitgestellten Dateien können alphabetisch und aktualitätsbezogen oder nach verschiedenen Kategorien und Schlagworten sortiert werden. Als optionale Auswahlmenüs stehen „Verwendung“ und „Dateityp“ zur Verfügung.</p>',
    'tour_24.header'        => 'Die richtige Verwendung',
    'tour_24.text'          => '<p>Alle Downloads sind markenübergreifend nach einem einheitlichen Prinzip eingepflegt. Die Beschreibung und weitere technische Spezifikationen bieten schnelle Orientierung beim richtigen Einsatz der Grafiken, Vorlagen und weiteren Dokumente.</p>',
    'tour_25.header'        => 'Downloadoptionen',
    'tour_25.text'          => '<p>Die Downloads können mit einem Klick bequem im Browserfenster heruntergeladen werden. Registrierte Nutzer können Dateien jederzeit zum Downloadkorb hinzufügen und zu einem späteren Zeitpunkt gesammelt herunterladen.</p>',
    'tour_26.header'        => 'Ende der Guided Tour',
    'tour_26.text'          => '<p>Die Guided Tour endet hier. Probieren Sie nun die Inhalte und Funktionen des Daimler Brand & Design Navigator selbst aus. Bei Fragen unterstützen wir Sie jederzeit per E-Mail unter <a class=\"tour_link\" href=\"mailto:corporate.design@daimler.com?Subject=\" target=\"_top\">corporate.design@daimler.com</a>.</p>',

	);
<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Activation email message Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/



	'subject'				=> 'Daimler Brand & Design Navigator – Ihr Benutzerkonto muss noch aktiviert werden',
    'greeting'              => 'Hallo :FIRST_NAME :LAST_NAME,',
    'content'               => 'Sie erhalten diese E-Mail, weil mit Ihrer E-Mail-Adresse eine Registrierung für das Online-Portal „Daimler Brand & Design Navigator“ erfolgt ist.<br>
                                <br>
                                Ihr Benutzername: :USERNAME<br>
                                <br>
                                Bitte klicken Sie auf den folgenden Link, um Ihr Benutzerkonto zu aktivieren:<br>
                                <a href=":CONFIRMATION_LINK">Ich möchte mein Benutzerkonto aktivieren</a><br>
                                <br>
                                Dieser Link wird nach 24 Stunden automatisch ungültig.<br>
                                <br>
                                Wenn diese E-Mail nicht für Sie bestimmt ist, bitten wir Sie, uns umgehend über den irrtümlichen Empfang zu informieren und diese E-Mail zu löschen. Wir danken Ihnen für Ihre Unterstützung.<br>
                                <br>
                                <br>
                                Mit freundlichen Grüßen<br>',
    'footer'                => 'Daimler Communications<br>
                                Corporate Design<br>
                                Daimler AG<br>
                                096-E402<br>
                                70546 Stuttgart, Germany<br>
                                corporate.design@daimler.com<br>
                                <br>
                                <br>
                                Daimler AG<br>
                                Sitz und Registergericht/Domicile and Court of Registry: Stuttgart<br>
                                HRB-Nr./Commercial Register No. 19360<br>
                                Vorsitzender des Aufsichtsrats/Chairman of the Supervisory Board: Manfred Bischoff<br>
                                Vorstand/Board of Management: Ola Källenius (Vorsitzender/Chairman), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm<br>',

	);
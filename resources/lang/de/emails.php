<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Emails message Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/



	'request_access_confirm.subject'				=> 'Daimler Brand & Design Navigator – Ihre Zugriffsberechtigung wird bearbeitet',
	'request_access_confirm.greeting'				=> 'Hallo :FIRST_NAME :LAST_NAME,',
	'request_access_confirm.content'				=>
			'vielen Dank für Ihre Anfrage über Zugriffsberechtigung für nicht öffentliche Materialien im Online-Portal „Daimler Brand & Design Navigator“.<br>
			<br>
			Wir freuen uns Ihnen bezüglich Ihres Anliegens behilflich zu sein und sind bemüht alle Anfragen zeitnah zu beantworten. Alle hinterlegten Angaben werden sorgfältig geprüft und Zugriffsanfragen werden in der Regel noch am gleichen Tag bearbeitet. In Ausnahmefällen und bei hohem Anfrageaufkommen kann sich die Beantwortung werktags bis zu 48 Stunden verzögern.<br>
			<br>
			Nach erfolgter Verifizierung Ihrer Angaben werden Sie systemseitig per E-Mail benachrichtigt und erhalten weiterführende Informationen zum Zugriff auf die angeforderten nicht öffentlichen Materialien.<br>
			<br>
			Alle übermittelten personenbezogenen Daten werden gemäß der Datenschutzerklärung der Daimler AG vertraulich behandelt und ausschließlich zweckgebunden verarbeitet:<br>
			<a href="'.asset('/Privacy_Statement?lang=de').'">Datenschutzerklärung</a><br>
			<br>
            <br>
			Mit freundlichen Grüßen<br>',
	'request_access_confirm.download.content'		=> 
			'vielen Dank für Ihre Anfrage über Zugriffsberechtigung für nicht öffentliche Materialien im Online-Portal „Daimler Brand & Design Navigator“.<br>
			<br>
			Wir freuen uns Ihnen bezüglich Ihres Anliegens behilflich zu sein und sind bemüht alle Anfragen zeitnah zu beantworten. Alle hinterlegten Angaben werden sorgfältig geprüft und Zugriffsanfragen werden in der Regel noch am gleichen Tag bearbeitet. In Ausnahmefällen und bei hohem Anfrageaufkommen kann sich die Beantwortung werktags bis zu 48 Stunden verzögern.<br>
			<br>
			Nach erfolgter Autorisierung werden die angefragten Dateien zu Ihrem Downloadkorb hinzugefügt. Anschließend werden Sie systemseitig benachrichtigt und können per Direktlink auf Ihren Downloadkorb zugreifen.<br>
			<br>
			Alle übermittelten personenbezogenen Daten werden gemäß der Datenschutzerklärung der Daimler AG vertraulich behandelt und ausschließlich zweckgebunden verarbeitet:<br>
			<a href="'.asset('/Privacy_Statement?lang=de').'">Datenschutzerklärung</a><br>
			<br>
            <br>
			Mit freundlichen Grüßen<br>',
	'request_access_confirm.footer'				=>
			'Daimler Communications<br>
			Corporate Design<br>
			Daimler AG<br>
			096-E402<br>
			70546 Stuttgart, Germany<br>
			corporate.design@daimler.com<br>
			<br>
			<br>
			Daimler AG<br>
			Sitz und Registergericht/Domicile and Court of Registry: Stuttgart<br>
			HRB-Nr./Commercial Register No. 19360<br>
			Vorsitzender des Aufsichtsrats/Chairman of the Supervisory Board: Manfred Bischoff<br>
			Vorstand/Board of Management: Ola Källenius (Vorsitzender/Chairman), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm<br>',




	'request_access_grant.subject'					=> 'Daimler Brand & Design Navigator – Zugriffsberechtigung für Ihr Benutzerkonto ist erfolgt',
	'request_access_grant.greeting'					=> 'Hallo :FIRST_NAME :LAST_NAME,',
	'request_access_grant.download.content'			=>
		'nach Prüfung der Richtigkeit Ihrer Angaben haben wir die Zugriffsrechte für Ihr Benutzerkonto im Online-Portal „Daimler Brand & Design Navigator“ entsprechend angepasst und die angeforderten nicht öffentlichen Materialien zu Ihrem Downloadkorb hinzugefügt. <br>
		<br>
		Bitte melden Sie sich mit Ihren gültigen Zugangsdaten an und rufen Sie Ihren Downloadkorb auf, um auf die Dateien zugreifen und diese herunterladen zu können:<br>
		<a href="'.asset('/Download_Cart?lang=de').'">Downloadkorb</a><br>
		<br>
		Bitte lesen Sie die nachfolgenden Hinweise, bevor Sie die nicht öffentlichen Materialien verwenden:<br>
		<br>
		Das nach Zugriffsautorisierung durch die Daimler AG eingeräumte Nutzungsrecht ist nicht an Dritte übertragbar. Sie dürfen Ihre Zugangsdaten für das Online-Portal „Daimler Brand & Design Navigator“ unter keinen Umständen Dritten zugänglich machen. Die gemeinsame Nutzung von Zugangsdaten ist nicht gestattet.<br>
		<br>
		Eine Weiterleitung oder Übertragung von nicht öffentlichen Materialien (insbesondere Schriftarten) durch Mitarbeiter der Daimler AG oder eines Mercedes-Benz/smart Händlers/Servicepartners an externe Dritte ist aufgrund der erforderlichen Dokumentationspflicht nicht erlaubt.<br>
		<br>
		Jegliche Änderungen an vordefinierten Positionen von Elementen und Formatierungen in Vorlagen (u.a. InDesign) oder Grundeinstellungen von Schriftarten (TrueType Fonts, OpenType Fonts oder Webfonts) sind nicht zulässig.<br>
		<br>
		Alle gängigen Nutzungsarten sind ohne Einschränkung erlaubt, soweit die nicht öffentlichen Materialien nur zu solchen Zwecken eingesetzt werden, die entweder internen Projektzwecken dienen oder in unmittelbarem Zusammenhang mit der Erfüllung von Aufträgen der Daimler AG, ihrer Tochtergesellschaften oder eines Mercedes-Benz/smart Händlers/Servicepartners stehen.<br>
		<br>
		Bitte beachten Sie außerdem die betreffenden Nutzungsbedingungen im Online-Portal „Daimler Brand & Design Navigator“:<br>
		<a href="'.asset('/Terms_of_Use?lang=de').'">Nutzungsbedingungen</a><br>
		<br>
		Alle übermittelten personenbezogenen Daten werden gemäß der Datenschutzerklärung der Daimler AG vertraulich behandelt und ausschließlich zweckgebunden verarbeitet:<br>
		<a href="'.asset('/Privacy_Statement?lang=de').'">Datenschutzerklärung</a><br>
		<br>
		Wenn Sie Fragen zum regelkonformen Umgang mit nicht öffentlichen Materialien haben oder weitere Unterstützung benötigen, stehen wir Ihnen gerne zur Verfügung.<br>
		<br>
        <br>
		Mit freundlichen Grüßen<br>',
	'request_access_grant.content'					=>
		'nach Prüfung der Richtigkeit Ihrer Angaben haben wir die Zugriffsrechte für Ihr Benutzerkonto im Online-Portal „Daimler Brand & Design Navigator“ entsprechend angepasst. <br>
		<br>
		Bitte melden Sie sich mit Ihren gültigen Zugangsdaten an und rufen Sie den folgenden Link auf, um auf die angeforderten nicht öffentlichen Materialien zuzugreifen:<br>
		<a href ="'.asset('/').':SLUG?lang=de">:TITLE</a><br>
		<br>
		Bitte lesen Sie die nachfolgenden Hinweise, bevor Sie die nicht öffentlichen Materialien verwenden:<br>
		<br>
		Das nach Zugriffsautorisierung durch die Daimler AG eingeräumte Nutzungsrecht ist nicht an Dritte übertragbar. Sie dürfen Ihre Zugangsdaten für das Online-Portal „Daimler Brand & Design Navigator“ unter keinen Umständen Dritten zugänglich machen. Die gemeinsame Nutzung von Zugangsdaten ist nicht gestattet.<br>
		<br>
		Eine Weiterleitung oder Verteilung von nicht öffentlichen Materialien durch Mitarbeiter der Daimler AG oder eines Mercedes-Benz/smart Händlers/Servicepartners an externe Dritte ist aufgrund der erforderlichen Dokumentationspflicht nicht erlaubt.<br>
		<br>
		Alle gängigen Nutzungsarten sind ohne Einschränkung erlaubt, soweit die nicht öffentlichen Materialien nur zu solchen Zwecken eingesetzt werden, die entweder internen Projektzwecken dienen oder in unmittelbarem Zusammenhang mit der Erfüllung von Aufträgen der Daimler AG, ihrer Tochtergesellschaften oder eines Mercedes-Benz/smart Händlers/Servicepartners stehen.<br>
		<br>
		Bitte beachten Sie außerdem die betreffenden Nutzungsbedingungen im Online-Portal „Daimler Brand & Design Navigator“:<br>
		<a href="'.asset('/Terms_of_Use?lang=de').'">Nutzungsbedingungen</a><br>
		<br>
		Alle übermittelten personenbezogenen Daten werden gemäß der Datenschutzerklärung der Daimler AG vertraulich behandelt und ausschließlich zweckgebunden verarbeitet:<br>
		<a href="'.asset('/Privacy_Statement?lang=de').'">Datenschutzerklärung</a><br>
		<br>
		Wenn Sie Fragen zum regelkonformen Umgang mit nicht öffentlichen Materialien haben oder weitere Unterstützung benötigen, stehen wir Ihnen gerne zur Verfügung.<br>
		<br>
        <br>
		Mit freundlichen Grüßen<br>',
	'request_access_grant.footer'					=>
		'Daimler Communications<br>
		Corporate Design<br>
		Daimler AG<br>
		096-E402<br>
		70546 Stuttgart, Germany<br>
		corporate.design@daimler.com<br>
		<br>
		<br>
		Daimler AG<br>
		Sitz und Registergericht/Domicile and Court of Registry: Stuttgart<br>
		HRB-Nr./Commercial Register No. 19360<br>
		Vorsitzender des Aufsichtsrats/Chairman of the Supervisory Board: Manfred Bischoff<br>
		Vorstand/Board of Management: Ola Källenius (Vorsitzender/Chairman), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm<br>',
	);
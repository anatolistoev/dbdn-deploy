<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Template Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/
	'dialog'					=> 'Dialog',
	'contacts'					=> 'Kontakte',
	'currLocale'				=> 'de',
	'altLocale'					=> 'en',
	'altLang'					=> 'English',

	'brand'						=> 'Marke/Gesellschaft',
//	'subsidiary'				=> 'Gesellschaft',
//    'brand.home'                => 'MARKE/GESELLSCHAFT AUSWÄHLEN',

	'home'						=> 'Home',

	'related'					=> 'Verwandte Themen',
	'downloads'					=> 'Verwandte Downloads',

	'personal services' 		=> 'Persönliche Services',
	'login.error'				=> 'Die von Ihnen eingegebene Kombination von Benutzername und Passwort ist nicht bekannt. Bitte geben Sie die Daten erneut ein.',
	'welcome'					=> 'Herzlich willkommen',
	'username'					=> 'Benutzername',
	'password'					=> 'Passwort',
	'login'						=> 'Anmelden',
    'login.subheading'			=> 'Bitte melden Sie sich mit Ihrem Benutzernamen und Ihrem persönlichen Passwort an.',
    'login.signup'              => 'Wenn Sie noch nicht registriert sind, <a href="'.asset('/registration').'">legen Sie ein neues Benutzerkonto an</a>, um fortzufahren.',
	'registration'				=> 'Registrieren',
	'forgot_link'				=> 'Passwort vergessen?',

	'user profile'				=> 'Benutzerprofil',
	'watchlist'					=> 'Merkliste',
	'logout'					=> 'Abmelden',

	'print_page'				=> 'Drucken',
	'print_pdf'					=> 'Als PDF speichern',
	'send page'					=> 'Seite versenden',
	'rate_page'					=> 'Seite bewerten',
	'addwatch'					=> 'Seite merken',
	'removewatch'				=> 'Aus der Merkliste entfernen',

	'help'						=> 'Hilfe',
	'send feedback'				=> 'Feedback geben',
	'faq'						=> 'FAQ',
	'tips'						=> 'Hilfe',
	'glossary'					=> 'Glossar',
	'viewallterms'				=> 'Alle Begriffe anzeigen',
	'sitemap'					=> 'Sitemap',
	'index'						=> 'Index',

	'gotop' 					=> 'Seitenanfang',

	'copyright'     			=> 'Daimler AG. Alle Rechte vorbehalten.',

	'provider'					=> 'Anbieter',
	'notices'					=> 'Rechtliche Hinweise',
	'cookies'					=> 'Cookies',
	'privacy.footer'			=> 'Datenschutz',
	'privacy.header'			=> 'Anbieter/Datenschutz',
	'terms'						=> 'Nutzungsbedingungen',
	'commentterms'				=> 'Kommentar-Regeln',

	'glossaryPageTitle'			=> 'Corporate Design Glossar',
	'registration.page.title'	=> 'Registrieren Sie sich für zusätzliche Funktionen und nicht öffentliche Materialien',
	'profile.page.title'		=> 'Ihr Benutzerprofil mit persönlichen Daten und Einstellungen',
	'profile'					=> 'Mein Profil',

	'rating.text'				=> 'Wie hilfreich ist diese Seite?',
	'rating.not_useful'			=> 'nicht hilfreich',
	'rating.very_useful'		=> 'sehr hilfreich',
	'rating.button'				=> 'Abschicken',

	'recent_topics'				=> 'Aktuelles',
	'most_visited'				=> 'Meistbesucht',
	'most_rated'				=> 'Meistbewertet',

	'comments.title'			=> 'Kommentare',
	'comments.says'				=> 'kommentiert:',
	'comments.headline'			=> 'überschrift:',
	'comments.add_your'			=> 'Kommentar verfassen',
	'comments.iagree'			=> 'Ich akzeptiere die',
	'comments.terms'			=> '<a target="_blank" href="'.url('Rules_for_Comment_Submissions').'">Kommentar-Regeln</a>',
	'comments.send'				=> 'Abschicken',
	'comments.not_logged'		=> 'Wir freuen uns über Kommentare. Bitte melden Sie sich an, um einen Kommentar zu verfassen. Bitte beachten Sie auch die <a href="'.url('Rules_for_Comment_Submissions').'">“Kommentar-Regeln“</a>.',
	'comments.thanks'			=> 'Vielen Dank für Ihren Kommentar. Ihr Kommentar wurde erfolgreich abgeschickt.<br>Bitte beachten Sie, dass Ihr Kommentar nach Prüfung durch die Redaktion des Daimler Brand &amp; Design Navigator veröffentlicht wird.',
	'and'						=> 'und',
    'design_manuals_downloads'  => 'Design Manuals und Downloads',

	'zoomimage'					=> 'Vergrößern',
	'lprev'						=> '\u005A\u0075\u0072\u00FC\u0063\u006B', //zuruck
	'lnext'						=> 'Weiter',
	'lof'						=> 'von',

	'epos_block'				=> 'Electronic Publishing and Ordering System (ePOS)<br/>
									Online Konfigurator f&#252;r Standardmedien.<br/><br/>
									<a href="http://portal.e.corpintra.net/go/epos" class="textLink" target="_blank" immune="true">Daimler ePOS</a><br/>
									(nur interner Zugriff)',
	'footer.about'				=> '<h2>Über Daimler Brand<br />
									& Design Navigator</h2>
									<p>Die Daimler AG gehört zu den größten Anbietern von Premium-Pkw und ist der größte weltweit aufgestellte Nutzfahrzeug-Hersteller. Das Unternehmen bietet Finanzierung, Leasing, Flottenmanagement, Versicherungen und innovative Mobilitätsdienstleistungen an.</p>

									<p>Das Online-Portal Brand & Design Navigator unterstützt Mitarbeiterinnen und Mitarbeiter sowie externe Dienstleister mit Informationen rund um das Erscheinungsbild des Unternehmens und seiner Marken &mdash; von den Basiselementen über ihre praktische Anwendung bis hin zu Gestaltungsvorlagen und Best Practice-Beispielen.</p>',
	'explainer.default_text'	=> 'Mauszeiger über die Icons bewegen und mehr Details über die Elemente und Layoutprinzipien erfahren.',

	'navigation.selection'      => 'Aktive Optionen',
	'navigation.clear'          => 'Zurücksetzen',
	'navigation.select_all'		=> 'Alle',
    'navigation.by_time'        => 'Datum',
    'navigation.by_tag'         => 'Schlagwort: ',
	'navigation.by_title'       => 'Titel',
	'navigation.a_z'            => 'A-Z',
	'navigation.z_a'            => 'Z-A',
	'navigation.by_period'      => 'Datum',
	'navigation.newest'         => 'Neueste',
	'navigation.oldest'         => 'Älteste',
    'navigation.define_perion'  => 'Zeitraum festlegen:',
    'navigation.from'           => 'von',
    'navigation.to'             => 'bis',
	'navigation.by_usage'       => 'Kategorie',
	'navigation.usage_label'    => 'Kategorien',
	'navigation.by_type'        => 'Dateityp',
	'navigation.type_label'     => 'Dateitypen',
	'navigation.by_views'       => 'Seitenaufrufe',
	'navigation.most_viewed'    => 'Meistbesucht',
	'navigation.by_rating'      => 'Bewertungen',
	'navigation.most_rated'     => 'Meistbewertet',
	'navigation.by_tags'        => 'Schlagwort',
	'navigation.tags_label'     => 'Schlagwörter',
	'navigation.by_comments'    => 'Kommentare',
    'navigation.comments_label' => 'Meistkommentiert',
	'navigation.by_brand'		=> 'Marke',
	'navigation.back'           => 'Zurück',
    'navigation.menu'           => 'Menü',
    'navigation.filters'        => 'Filter',
    'navigation.filter_by'      => 'Filtern nach',
    'navigation.sort_by'        => 'Sortieren nach',
    'filters.chooseopt'         => 'Keine aktiven Optionen',

    'navigation._best_practice' => 'Alles anzeigen',
    'navigation._downloads'  	=> 'Alles anzeigen',

	'interest.most_recent'      => 'Das Neueste',
	'interest.most_searched'    => 'Meistgesucht',
	'interest.most_viewed'      => 'Meistbesucht',
	'interest.most_rated'       => 'Meistbewertet',
	'interest.most_downloaded'  => 'Meistheruntergeladen',

    'best_practice.contact'     => 'Kontakt',
	'no_results'				=> '<h1 class="hct">Keine Treffer gefunden</h1>
									<h1 class="hct">
										<small>
											<p>Die gewählte Kombination ergab leider keine Treffer.</p>
											<p>Versuchen Sie es erneut, indem Sie andere Filteroptionen einstellen.</p>
										</small>
									</h1>',
	'wl.user_profile'			=> 'Daten/Einstellungen',
	'wl.watch_list'				=> 'Merkliste',
	'wl.download_cart'			=> 'Downloadkorb',
	'ask_question'				=> 'Eine Frage stellen',
	'dialog.messageText'		=> 'Wie können wir Ihnen behilflich sein?',
	'dialog.name'				=> 'Name',
	'dialog.email'				=> 'E-Mail',
	'dialog.send'				=> 'Abschicken',

	'terms.frequently'			=> 'Oben genannte Begriffe',
	'terms.view_all'			=> 'Alle Begriffe anzeigen',
	'faq.related'				=> 'Verwandte FAQ',
	'faq.view_all'				=> 'Alle FAQ anzeigen',

	'faq.header'				=> 'Häufig gestellte Fragen (FAQ)',
	'faq.related_links'			=> 'Verwandte Seiten',

	'search_box.label'			=> 'Wonach suchen Sie?',
	'search_box.top.placeholder'=> 'Geben Sie einen Suchbegriff ein',

	'search.advanced.tootip'	=> 'Erweiterte Suchoptionen',

	'print.header'				=> 'Daimler Brand & Design Navigator',
	'print.footer'				=> '&#169; '.date('Y').' Daimler AG. Alle Rechte vorbehalten.',

	'navigation.search.title'	=> 'Erweiterte Suche',

	'comment.headline'			=> 'Überschrift',
	'comment.text'				=> 'Kommentar',

	'bp.load_more'				=> 'Mehr anzeigen',
    'carousel.more'             => 'Mehr erfahren',
	'brand_choose'				=> 'Marke auswählen',
	'company_choose'			=> 'Gesellschaft auswählen',
	'pdf.header'                => '<p><b>Diese Seite wurde am :time Uhr ausgedruckt.</b> <br> Hinweis: Das Online-Portal "Daimler Brand & Design Navigator" steht als einzige Quelle für Design Manuals und Downloads online zur Verfügung. Die Aktualität, Korrektheit und Vollständigkeit der Informationen auf Seiten, die als Datei im Format PDF gespeichert wurden, ist nicht garantiert. Die ausgedruckte Version der Seiteninhalte kann gegebenenfalls nicht den jeweils letzten verbindlichen Stand abbilden.</p>',
    'pdf.footer'                => '<p>Veröffentlicht am :time Uhr</p><p>https://designnavigator.daimler.com </p><p>&#169; ' . date('Y') . ' Daimler AG. Alle Rechte vorbehalten.</p>',
    'pdf.from'                  => 'Seite ',
    'pdf.to'                    => ' von ',
    'related.downloads'         => 'Verwandte downloads',
    'related.pages'             => 'Verwandte themen',
    'related.bp'                => 'Aktuelle Best Practice Berichte',
    'bp.related'                => 'Empfohlene Design Manuals',
    'intro'                     => 'In seiner tagesaktuellen Fassung stellen die Inhalte des Online-Portals „Daimler Brand & Design Navigator“ die Regeln des Corporate Design von Daimler dar, die für alle Daimler namenstragenden Gesellschaften bindend sind: <a href="http://erd3.e.corpintra.net/ERD/Home/ruleProfile/32102"  target="_blank">Gesellschaftsrichtlinie D 1715.0 in der Einheitliche Regelungsdatenbank (ERD) im Mitarbeiter-Portal</a> (nur intern abrufbar).',
    'file'                      => ' Datei',
    'files'                     => ' Dateien',
    'protected'                 => 'Nicht öffentlich',
    'recommended'               => 'Empfohlen',
    'learn_more'				=> 'mehr erfahren',
    'choose_brand_company'		=> 'Marke oder Gesellschaft auswählen',
    'home.faq.title'            => 'FAQ des Tages',
    'home.faq.button'           => 'Zur FAQ',
    'home.term.title'           => 'Begriff des Tages',
    'home.term.button'          => 'Zum Glossar',
    'latest_design_manuals' 	=> 'Aktuelle Design Manuals',
    'all_design_manuals' 	    => 'Alle Design Manuals',
    'latest_best_practice'      => 'Aktuelle Best Practice',
    'all_best_practice' 	    => 'Alle Best Practice',
	'latest_downloads'			=> 'Aktuelle Downloads',
	'all_downloads'				=> 'Alle Downloads',
	'filter_result.fields_sort'	=> '[:result_count] Treffer gefunden',
	'filter_result.fields'		=> '[:result_count] Treffer gefunden',
	'filter_result.sort'		=> '[:result_count] Treffer neu sortiert',

    'help_form'                 => 'Hilfe',
    'help_form.subheading'      => 'Eine Frage stellen.',
    'meta.description'          => 'Das Online-Portal „Daimler Brand & Design Navigator“ unterstützt Mitarbeiterinnen und Mitarbeiter sowie externe Dienstleister mit Informationen rund um das Corporate Design der Daimler AG und ihrer Marken.',

	'nav.corporate_brand'		=> 'Unternehmensmarke',
	'nav.subsidaries'			=> 'Tochtergesellschaften',
	'nav.brands'				=> 'Marken'
);

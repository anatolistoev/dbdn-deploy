<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" 			=> "Ung&uuml;ltiges Passwort",

	"user"     			=> "Der von Ihnen eingegebene Benutzername kann nicht gefunden werden. Bitte geben Sie einen gültigen Benutzernamen ein und klicken Sie dann auf den Button \"Abschicken\".</br></br></br>",

	"token"    			=> "Ihre Anfrage zur Passwort-Zur&uuml;cksetzung ist bereits abgelaufen.",

    'email.subject'     => 'Daimler Brand & Design Navigator – Ihr Passwort kann jetzt geändert werden',
	'email.greeting'	=> 'Hallo :FIRST_NAME :LAST_NAME,',
    'email.content'     => 'wir haben Ihre Anfrage zum Zurücksetzen des Passwortes für Ihr Benutzerkonto im Online-Portal „Daimler Brand & Design Navigator“ erhalten.<br>
                            <br>
                            Bitte klicken Sie auf den folgenden Link, um ein neues Passwort auszuwählen:<br>
                            <a href=":CONFIRMATION_LINK">Ich möchte mein Passwort zurücksetzen</a><br>
                            <br>
                            Dieser Link wird nach 15 Minuten automatisch ungültig.<br>
                            <br>
                            Wenn diese E-Mail nicht für Sie bestimmt ist, bitten wir Sie, uns umgehend über den irrtümlichen Empfang zu informieren und diese E-Mail zu löschen. Wir danken Ihnen für Ihre Unterstützung.<br>
                            <br>
                            <br>
                            Mit freundlichen Grüßen<br>',
    'email.footer'      => 'Daimler Communications<br>
                            Corporate Design<br>
                            Daimler AG<br>
                            096-E402<br>
                            70546 Stuttgart, Germany<br>
                            corporate.design@daimler.com<br>
                            <br>
                            <br>
                            Daimler AG<br>
                            Sitz und Registergericht/Domicile and Court of Registry: Stuttgart<br>
                            HRB-Nr./Commercial Register No. 19360<br>
                            Vorsitzender des Aufsichtsrats/Chairman of the Supervisory Board: Manfred Bischoff<br>
                            Vorstand/Board of Management: Ola Källenius (Vorsitzender/Chairman), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm<br>',

);

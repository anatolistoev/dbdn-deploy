<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Dialog Page Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/
	

	'dialog'							=> 'Dialog',
	'page.title'						=> 'Dialog.',
	'contents'							=> 'Mit diesem Formular können Sie Fragen stellen oder einen Kommentar zum Daimler&nbsp;Brand &amp; Design Navigator abgeben. Sie erhalten eine Antwort innerhalb von 48 Stunden an die von Ihnen angegebene E-Mail-Adresse.',
	'note'								=> '<b>Hinweis:</b> Bitte füllen Sie alle mit einem Stern (*) markierten Pflichtfelder aus.',
	'academic_title'					=> 'Akademischer Titel:',
	'first_name'						=> 'Vorname:',
	'last_name'							=> 'Nachname:',
	'email'								=> 'E-Mail:',
	'subject'							=> 'Betreff:',
	'message'							=> 'Nachricht:',
	'captcha'							=> 'Bitte geben Sie die Zeichen ein:',
	'note2'								=> 'Hinweis: Ihre E-Mail-Adresse wird ausschließlich zu Bearbeitungszwecken Ihrer Anfrage gespeichert.',
	'send'								=> 'Abschicken',
	
	'sent'								=> '<br>Vielen Dank!<br>
											Ihre Nachricht wurde erfolgreich an <a href="mailto:corporate.design@daimler.com">corporate.design@daimler.com</a> versendet. <br>
											Sie erhalten eine Antwort innerhalb von 48 Stunden an die von Ihnen angegebene E-Mail-Adresse.<br><br>',
	);
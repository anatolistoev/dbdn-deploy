<?php 

return array(

	'column.id' => 'Id',
	'column.username'     => 'Username',
	'column.name'     => 'Name',
	'column.role'     => 'Role',
	'column.newsletter'     => 'Newsletter',
	'column.active'     => 'Active',
	'column.last_login'     => 'Last Login',
	'column.logins_count'     => 'Logins Count',
	'placeholder.user_search'     => 'User Search',
	'placeholder.user_sort'     => 'User Sort',
	'button.filter'     => 'FILTER',
	'edit'     => 'Edit User',
	'add'     => 'Add User',
	'remove'     => 'Remove',
	'remove.confirm' => 'Are you sure you want to delete user?',
	'form.active'     => 'Active',
	'form.newsletter'     => 'Newsletter',
	'form.registered'     => 'Registered',
	'form.last_update'     => 'Last update',
	'form.last_login'     => 'Last Login',
	'form.logins_count'     => 'Logins Count',
	'form.first_name'     => 'First Name',
	'form.last_name'     => 'Last Name',
	'form.position'     => 'Position',
	'form.country'     => 'Country',
	'form.company'     => 'Company',
	'form.city'     => 'City',
	'form.username'     => 'Username',
	'form.department'     => 'Office Department',
	'form.postal_code'     => 'ZIP',
	'form.email'     => 'E-mail',
	'form.phone'     => 'Phone',
	'form.address'     => 'Address',
	'form.password'     => 'Password',
	'form.re_password'     => 'Confirm password',
	'form.fax'     => 'Fax',
	'form.role'     => 'Role',
	'form.role.admin'     => 'Administrator',
	'form.role.approver'     => 'Approver',
	'form.role.member'     => 'Member',
	'form.role.exporter'   => 'Exporter',
	'form.role.editor_smart'=>	'Editor smart',
	'form.role.editor_mb'	=>	'Editor MB',
	'form.role.editor_dfs'	=>	'Editor DFS',
    'form.role.editor_dmo'  =>  'Editor DMO',
	'form.role.editor_dfm'	=>	'Editor DFM',
	'form.role.editor_dtf'	=>	'Editor DTF',
	'form.role.editor_ff'	=>	'Editor FF',
	'form.role.editor_dp'	=>	'Editor DP',
	'form.role.editor_tss'	=>	'Editor TSS',
	'form.role.editor_bkk'	=>	'Editor BKK',
    
	'form.role.editor_db'	=>	'Editor DB',
	'form.role.editor_eb'	=>	'Editor EB',
	'form.role.editor_setra'=>	'Editor Setra',
	'form.role.editor_op'	=>	'Editor OP',
	'form.role.editor_bs'	=>	'Editor BS',
	'form.role.editor_fuso'	=>	'Editor FUSO',
	'form.role.editor_dt'	=>	'Editor DT',
 
	'form.role.newsletter_approver'	=>	'Newsletter Approver',
	'form.role.newsletter_editor'	=>	'Newsletter Editor',
	'form.group'     => 'Group',

);
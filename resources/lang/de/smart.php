<?php
return array(
	'login'						=> 'Anmelden',
	'registration'				=> 'Registrieren',
    
    'provider'					=> 'Anbieter',
	'notices'					=> 'Rechtliche Hinweise',
	'cookies'					=> 'Cookies',
	'privacy.footer'			=> 'Datenschutz',
	'privacy.header'			=> 'Anbieter/Datenschutz',
	'terms'						=> 'Nutzungsbedingungen',
	'commentterms'				=> 'Kommentar-Regeln',
    
    'design_manuals_downloads'  => '‡ Design Manuals und Downloads.',
    
    'faq.header'				=> '‡ Häufig gestellte Fragen (FAQ).',
);
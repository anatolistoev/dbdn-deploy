<?php 

return array(

	'column.id' => 'Id',
	'column.name' => 'Name',
	'column.modified_at' => 'Modified',
	'placeholder.glossary_search' => 'Glossary Search',
	'placeholder.glossary_sort' => 'Glossary Sort',
	'button.filter' => 'FILTER',
	'edit' => 'Edit Term',
	'add' => 'Add Term',
	'remove' => 'Remove',
	'remove.confirm' => 'Are you sure you want to delete the selected term?',
	'form.created_at' => 'Date added',
	'form.modified_at' => 'Date modified',
	'form.name' => 'Name',
	'form.description' => 'Description',
	'change_lang' => 'Language'
);
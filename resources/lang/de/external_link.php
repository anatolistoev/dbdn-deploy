<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| External Link Page Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/
	'path'		=> 'Hinweis',
	'text'		=> '<h1>Sie verlassen den Daimler Brand &amp; Design Navigator</h1>
					<h1><small>Sie wechseln vom Daimler Brand &amp; Design Navigator auf eine Website, die sich möglicherweise im Intranet der Daimler AG befindet oder nicht von der Daimler AG betrieben wird.</small></h1>
					<p>
					<strong>Interne Links</strong><br>
					Alle Links auf Websites oder Applikationen im Intranet bei Daimler sind ausschließlich innerhalb des internen Servernetzes des Unternehmens abrufbar. Sie stehen nur für interne Zwecke zur Verfügung und unterliegen den Regelungen für die Nutzung des Intranets im Unternehmen. Alle externen Zugriffsversuche werden automatisch abgelehnt.</p>
					<p><strong>Externe Links</strong><br>
					Daimler AG übernimmt keine Verantwortung für den lnhalt oder die Funktionsfähigkeit, Fehlerfreiheit oder Rechtmäßigkeit von Websites Dritter, auf die mittels Link (Internetverknüpfung) verwiesen wird.
					</p>',
	'continue'	=> 'Weiter zu',
	'back'		=> 'Zurück',
	
	);
<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| GDPR Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines include messages that are related to
	| a user deleting his own account, under the GDPR rules
	*/
    'request.boxheader' => 'Löschung des Benutzerkontos',
    'request.boxtext'   => 'Bitte beachten Sie, dass Sie nach der unwiderruflichen Löschung Ihres Benutzerkontos keine personenbezogenen Daten mehr abrufen und auf keine nicht öffentlichen Materialien zugreifen können. Wenn Sie Ihr Benutzerkonto löschen möchten, klicken Sie bitte auf die Checkbox:',
    'request.boxchbx'   => 'Benutzerkonto löschen',
    'request.faqtext'   => '<a href="/faq/daimler/291/wie-kann-ich-mein-benutzerkonto-loschen" target="_blank">Erfahren Sie mehr über die Löschung von Benutzerkonten</a>',
    'request.success'   => '<h1>Sie haben sich entschieden Ihr Benutzerkonto endgültig zu löschen.</h1><p>In Kürze erhalten Sie eine Benachrichtigung an die in Ihrem Benutzerkonto gespeicherte E-Mail-Adresse um die Löschung zu bestätigen und den Vorgang abzuschließen. Bitte öffnen Sie dazu Ihr Postfach und folgen Sie den Hinweisen. Vielen Dank.</p>', //The template for the text in the success page that appears only if the gdpr button is clicked //tbt
    'request.nouser'    => '<h1>Die angeforderte Löschung konnte nicht erfolgen.</h1><p>Das mit diesem Link korrespondierende Benutzerkonto existiert nicht mehr und wurde bereits unwiderruflich gelöscht. Der Link ist nur einmalig gültig und kann nicht erneut verwendet werden.</p>',
    'request.wrongtoken'=> '<h1>Die angeforderte Löschung konnte nicht erfolgen.</h1><p>Die angeforderte Löschung Ihres Benutzerkontos konnte nicht erfolgen, da Sie im Moment einen ungültigen Link verwenden. Sie können nach erfolgreicher Anmeldung diesen Vorgang erneut initialisieren.</p>',
    'operation.complete'=> 'Wie von Ihnen angefordert, bestätigen wir Ihnen hiermit verbindlich, dass Ihr Benutzerkonto soeben samt aller personenbezogenen Daten und Zugriffsberechtigungen unwiderruflich gelöscht wurde.',
    'mail.subject'      => 'Daimler Brand & Design Navigator – Ihre Anfrage über die endgültige Löschung Ihres Benutzerkontosn',
    'mail.greeting'     =>  '<p>Hello, :FIRST_NAME :LAST_NAME,</p>',
    'mail.content'      => '<p>Sie erhalten diese E-Mail, weil Sie sich entschieden haben Ihr Benutzerkonto im Online-Portal „Daimler Brand & Design Navigator“ unwiderruflich zu löschen.</p>
                            <p>Wir möchten Sie darauf hinweisen, dass Sie nach Löschung Ihres Benutzerkontos keine gespeicherten personenbezogenen Daten mehr abrufen können und falls vorhanden Ihre Zugriffsberechtigung für nicht öffentliche Materialien endgültig aufgehoben wird und gegebenenfalls neu beantragt werden muss.</p>
                            <p>Bitte klicken Sie auf den nachfolgenden Link um Ihr Benutzerkonto zu löschen:<br/>
                            <a href=":CONFIRMATION_LINK">:CONFIRMATION_LINK</a></p>
                            <p>Dieser Link wird nach 15 Minuten automatisch ungültig.</p>
                            <p>Sollten Sie diese Option versehentlich gewählt haben und Ihr Benutzerkonto nicht löschen wollen; oder falls Sie diesen Vorgang nicht selbst angefordert haben, betrachten Sie diese Nachricht als gegenstandslos.</p>
                            <p>Bitte informieren Sie uns, wenn Sie weitere Unterstützung zu Ihrem Benutzerkonto benötigen und wir würden Sie uns freuen Ihnen bezüglich Ihres Anliegens behilflich zu sein. Antworten Sie einfach auf Ihre E-Mail und wir werden uns zeitnah mit Ihnen in Verbindung setzen.</p>
                            <p>Wenn diese E-Mail nicht für Sie bestimmt ist, bitten wir Sie, uns umgehend über den irrtümlichen Empfang zu informieren und diese E-Mail zu löschen. Wir danken Ihnen für Ihre Unterstützung.</p>
                            <p>Mit freundlichen Grüßen</p>',
    'mail.footer'       => '<p>Daimler Communications<br/>
                            Corporate Design<br/>
                            Daimler AG<br/>
                            096-E402<br/>
                            70546 Stuttgart, Germany<br/>
                            corporate.design@daimler.com
                            </p>
                            <p>Daimler AG<br/>
                            Sitz und Registergericht/Domicile and Court of Registry: Stuttgart<br/>
                            HRB-Nr./Commercial Register No. 19360<br/>
                            Vorsitzender des Aufsichtsrats/Chairman of the Supervisory Board: Manfred Bischoff<br/>
                            Vorstand/Board of Management: Ola Källenius (Vorsitzender/Chairman), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm
                            </p>'
);

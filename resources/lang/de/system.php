<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| System Pages Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'registration_success'				=> 'Registrierung',
	'registration_success.page.title'	=> 'Ihre Registrierung wurde erfolgreich übermittelt.',
	'registration_success.subheading'	=> 'Sie erhalten umgehend eine E-Mail mit weiteren Hinweisen zur Aktivierung Ihres neuen Benutzerkontos. Bitte klicken Sie auf den Aktivierungslink in der Bestätigungsmail, um die Registrierung abzuschließen.',
	'registration_success.contents'		=> 'Wenn Sie im Rahmen Ihrer Registrierung den Newsletter abonniert haben, erhalten Sie einen weiteren Aktivierungslink per E-Mail.<br>
											<br>
											Benötigen Sie weitere Unterstützung bei der Aktivierung Ihres Benutzerkontos?<br>
											Dann senden Sie eine E-Mail an <a href="mailto:corporate.design@daimler.com?Subject=Registrierung" target="_top">corporate.design@daimler.com</a> und wir helfen Ihnen gerne zeitnah weiter.',

	'profile_success'					=> 'Benutzerprofil',
	'profile_success.page.title'		=> 'Benutzerprofil',
	'profile_success.subheading'		=> 'Ihre persönlichen Daten wurden erfolgreich aktualisiert.',
	'profile_success.contents'			=> '<span class="normal">Vielen Dank für die durchgeführte Aktualisierung.</span><br>
											Alle Änderungen wurden gespeichert und in den Daimler Brand &amp; Design Navigator übernommen. Bitte halten Sie Ihre persönlichen Daten aktuell.',

	'profile_success.subscription'		=> 'Sie haben den Daimler Brand & Design Navigator Newsletter erfolgreich abonniert. Als Nächstes erhalten Sie eine E-Mail an die von Ihnen angegebene E-Mail-Adresse. Bitte bestätigen Sie Ihre Anmeldung zum Newsletter abschließend, indem Sie auf den versendeten Link klicken.',

	'profile_success.cancellation'		=> 'Sie haben den Daimler Brand & Design Navigator erfolgreich abbestellt. Sie erhalten eine letzte E-Mail, mit der Ihre erfolgte Abmeldung bestätigt wird.',
	'profile_success.further'			=> 'Sollten Sie weitere Fragen zur Behandlung Ihrer persönlichen Daten haben, lesen Sie sorgfältig die Seite mit detaillierten Informationen zum <a href="'.asset('/de/Privacy_Statement').'" target="_blank">&quot;Datenschutz&quot;</a> bei Daimler oder schicken Sie eine E-Mail an
											<a class="contactlink" href="mailto:corporate.design@daimler.com" target="_top">corporate.design@daimler.com</a>.',
	'activation_success'				=> 'Registrieren',
	'activation_success.page.title'		=> 'Sie haben Ihr Benutzerkonto erfolgreich aktiviert.',
	'activation_success.subheading'		=> 'Sie können sich ab sofort mit Ihrem Benutzernamen und Ihrem Passwort anmelden.',
	'activation_success.contents'		=> 'Haben Sie noch Fragen zu Ihrem Benutzerkonto?<br>
											Dann senden Sie eine E-Mail an <a class="contactlink" href="mailto:corporate.design@daimler.com" target="_top">corporate.design@daimler.com</a> und wir helfen Ihnen gerne zeitnah weiter.',

	'activation_failure'				=> 'Registrieren',
	'activation_failure.page.title'		=> 'Die Aktivierung Ihres Benutzerkontos ist fehlgeschlagen.',
	'activation_failure.subheading'		=> 'Registrieren Sie sich erneut, wenn der verwendete Link bereits abgelaufen ist oder das Benutzerkonto nicht mehr existiert.',
	'activation_failure.contents'		=> 'Wenn Sie Ihr Benutzerkonto schon erfolgreich aktiviert haben, <a href="'.asset('/login?lang=de').'">können Sie sich jetzt anmelden</a>.<br>
											<br>
											Benötigen Sie weitere Unterstützung bei der Registrierung?<br>
											Dann senden Sie eine E-Mail an <a class="contactlink" href="mailto:corporate.design@daimler.com" target="_top">corporate.design@daimler.com</a> und wir helfen Ihnen gerne zeitnah weiter.',

	'forgotten_form'					=> 'Passwort vergessen',
	'forgotten_form.page.title'			=> 'Passwort vergessen',
	'forgotten_form.contents'			=> 'Wenn Sie Ihr Passwort vergessen haben, können Sie es hier zurücksetzen: Geben Sie Ihren Benutzernamen ein und Sie erhalten eine E-Mail mit weiteren Informationen, wie Sie Ihr Passwort ändern können.',
	'forgotten_sent.page.title'         => 'Ihr Passwort kann jetzt geändert werden.',
    'forgotten_sent.page.subheading'    => 'Eine E-Mail mit einem Link zum Zurücksetzen Ihres Passwortes wurde soeben per E-Mail gesendet.',
	'forgotten_sent.contents'			=> 'Detaillierte Informationen zur Verarbeitung und Speicherung Ihrer persönlichen Daten finden Sie in der <a href="'.asset('/de/Privacy_Statement').'" target="_blank">Datenschutzerklärung</a> der Daimler AG. Bei weiteren Fragen zum Datenschutz senden Sie eine E-Mail an <a class="contactlink" href="mailto:corporate.design@daimler.com" target="_top">corporate.design@daimler.com</a> und Sie erhalten zeitnah eine Antwort.',

	'reset_form'						=> 'Passwort-Zurücksetzung',
	'reset_form.page.title'				=> 'Zurücksetzen Ihres Passworts',
	'reset_form.page.subheading'        => 'Ihr Passwort kann jetzt geändert werden.',
	'reset_form.contents'				=> '<span class="red">Hinweis: Bevor Sie Ihr neues Passwort auswählen, beachten Sie bitte folgende Regeln:<br>
											- mindestens 1 Großbuchstabe<br>
											- mindestens 1 Kleinbuchstabe<br>
											- mindestens 1 Ziffer<br>
											- mindestens 1 Sonderzeichen (!"#$%&amp;\'()*+,%;:=?_@&gt;-)<br>
											- Mindestlänge 8 Zeichen <br>
											- Höchstlänge 25 Zeichen<br>
											- Kein Teil des Benutzernamens im Passwort<br>
											- Keine Umlaute im Passwort</span>',
	'reset_form.username.label'			=> 'Bitte geben Sie Ihren Benutzernamen ein:',
	'reset_form.password.label'			=> 'Bitte geben Sie Ihr neues Passwort ein:',
	'reset_form.confirmation.label'		=> 'Bitte wiederholen Sie Ihre Eingabe:',
	'reset_form.captcha.label'			=> 'Bitte geben Sie die Zeichen ein:',
	'reset_form.send'					=> 'Abschicken',

	'reset_subject'						=> 'Daimler Brand & Design Navigator - Ihre Passwortanfrage',


	'reset_success'						=> 'Passwort-Zurücksetzung',
	'reset_success.page.title'			=> 'Sie haben Ihr Passwort erfolgreich geändert.',
	'reset_success.subheading'			=> 'Bitte halten Sie Ihre persönlichen Daten stets auf dem neuesten Stand und aktualisieren Sie Ihr Passwort aus Sicherheitsgründen regelmäßig. Sie sind für den sicheren Umgang mit Ihrem Passwort verantwortlich.',
	'reset_success.contents'			=> 'Detaillierte Informationen zur Verarbeitung und Speicherung Ihrer persönlichen Daten finden Sie in der <a href="'.asset('/de/Privacy_Statement').'" target="_blank">Datenschutzerklärung</a> der Daimler AG. Bei weiteren Fragen zum Datenschutz senden Sie eine E-Mail an <a class="contactlink" href="mailto:corporate.design@daimler.com" target="_top">corporate.design@daimler.com</a> und Sie erhalten zeitnah eine Antwort.',

	'login_required.page.title'			=> 'Anmeldung erforderlich',
	'login_required.subheading'			=> '<h1><small>Für die von Ihnen aufgerufene Seite müssen Sie sich im Brand & Design Navigator anmelden.</small></h1>',
	'login_required.contents'			=> 'Sie müssen sich erst anmelden, bevor Sie auf diesen Inhalt zugreifen können. Für nicht öffentliche Materialien kann eine zusätzliche Zugriffsberechtigung erforderlich sein.',
	'no_access.title'					=> 'Zugriffsberechtigung für nicht öffentliche Materialien erforderlich',
	'no_access.subheading'				=> 'Bestimmte Materialien sind nicht für alle registrierten Benutzer zugänglich und erfordern eine zusätzliche Autorisierung.',
	'no_access.contents'				=> 'Bestätigen Sie die Nutzungsbedingungen und die Datenschutzerklärung der Daimler AG:',
	'mydownloads'						=> 'Downloadkorb',
	'mydownloads.page.title'			=> 'Downloadkorb',
	'mydownloads.contents'				=> 'Mit dieser Funktion können Sie Dateien für einen späteren Zeitpunkt speichern und diese als ZIP Archiv gesammelt herunterladen.',
    'mydownloads.notfound'              => 'Download nicht mehr verfügbar.',
	'of'								=> 'von',
	'downloads_list.selectall'			=> 'Auswählen',
	'downloads_list.removeSelected'		=> 'Auswahl entfernen',
	'downloads_list.alert'				=> 'Die Aktion konnte nicht ausgeführt werden, da Sie keine Objekte ausgewählt haben. Bitte versuchen Sie es erneut.</br></br>',
	'downloads_list.downloadLink'		=> 'Auswahl herunterladen',
	'downloads_list.downloadNow'		=> 'Sofort herunterladen',
	'downloads_list.addToCart'			=> 'Zum Downloadkorb hinzufügen',
	'downloads_list.addToCart.tablet'	=> 'In Downloadkorb legen',
	'downloads_list.added'				=> 'Hinzugefügt',
	'downloads_list.added_pages'		=> 'Meine hinzugefügten Dateien',
	'downloads_list.auth_pages'			=> 'Angeforderte nicht öffentliche Dateien',
	'downloads_list.notice'             => 'Das nach Zugriffsautorisierung durch die Daimler AG eingeräumte Nutzungsrecht ist nicht an Dritte übertragbar. Sie dürfen Ihre Zugangsdaten unter keinen Umständen Dritten zugänglich machen. Die gemeinsame Nutzung von Zugangsdaten ist nicht gestattet.',
	'download_overview.addToCart'       => 'Zum Downloadkorb',
	'download_overview.goTo'            => 'Öffnen',
	'download_overview.goToLib'         => 'Zur Bibliothek',

	'download_na'						=> 'Download nicht mehr verfügbar',

	'downloads_list.categories.1'		=> 'Audiovisuelle Medien',
	'downloads_list.categories.2'		=> 'Basiselemente',
	'downloads_list.categories.3'		=> 'Bildschirmmedien',
	'downloads_list.categories.4'		=> 'Digitale Medien',
	'downloads_list.categories.5'		=> 'Dreidimensionale Medien',
	'downloads_list.categories.6'		=> 'Druckmedien',
	'downloads_list.categories.7'		=> 'Spezialanwendungen',

	'downloads_list.category'			=> 'Kategorie',
	'downloads_list.size'				=> 'Größe',
	'downloads_list.fileformat'			=> 'Dateiformat',
	'downloads_list.program'			=> 'Programm',
	'downloads_list.lang'				=> 'Sprache',
	'downloads_list.dimensions'			=> 'Abmessungen',
	'downloads_list.dUnits'				=> 'Messgröße',
	'downloads_list.format'				=> 'Format',
	'downloads_list.resolution'			=> 'Auflösung',
	'downloads_list.rUnits'				=> 'Messgröße (Auflösung)',
	'downloads_list.color_mode'			=> 'Farbmodus',
	'downloads_list.pages'				=> 'Seitenanzahl',
	'downloads_list.version'			=> 'Version',
	'downloads_list.uploaded'			=> 'Stand',
	'downloads_list.remLink'			=> 'Dieses Objekt entfernen',
	'downloads_list.usage_terms'		=> 'Nutzungsbedingungen',
	'downloads_list.copyrights'			=> 'Urheberrechtsvermerk',
	'downloads_list.usage'				=> 'Verwendung',
	'downloads_list.file_type'			=> 'Dateityp',
	'downloads_list.description'		=> 'Beschreibung',
	'mydownloads.noneselected'			=> 'Hinweis:<br><br>Die Aktion konnte nicht ausgeführt werden, da Sie keine Objekte ausgewählt haben. Bitte versuchen Sie es erneut.<br>',
	'mydownloads.failed'				=> 'Bei der Verarbeitung Ihrer Anfrage trat ein Fehler auf.<br>Bitte versuchen Sie es erneut.',
	'downloads_list.overlimit'			=> 'Die Größe des Download-Archivs übersteigt das Limit.<br>
											Die ausgewählten Objekte übersteigen das voreingestellte Limit von 750 MB für das Download-Archiv. Bitte entfernen Sie ein oder mehrere Objekte und versuchen Sie diese erneut gesammelt herunterzuladen.',

	'empty_list'						=> 'Keine Einträge.',

	'watchlist'							=> 'Merkliste',
	'watchlist.page.title'				=> 'Merkliste',
	'watchlist.contents'				=> 'Mit dieser Funktion werden Sie automatisch über Anpassungen und Änderungen in den Inhalten der ausgewählten Seiten per E-Mail benachrichtigt.',
	'watchlist.remLink'					=> 'Entfernen',
	'watchlist.added'					=> 'Diese Seite wurde zu Ihrer Merkliste hinzugefügt.<br>
											Sie werden automatisch per E-Mail benachrichtigt, wenn diese Seite aktualisiert wird.',
	'watchlist.removed'					=> 'Eine oder mehrere Seiten wurden aus Ihrer Merkliste entfernt.<br>
                                            Sie werden nun keine Benachrichtigungen mehr zu diesen Seiten erhalten.',
	//'watchlist.already'					=> '<br>',
	'watchlist.na'						=> 'Seite ist nicht mehr verfügbar',
	'watchlist.couldnt_save'			=> 'Bei der Verarbeitung Ihrer Anfrage trat ein Fehler auf.<br>Bitte versuchen Sie es erneut.',

	'rating.added'						=> 'Vielen Dank für Ihre Bewertung.',
	'rating.failed'						=> 'Die Bewertung dieser Seite ist fehlgeschlagen.',

	'faq_rating.added'					=> 'Vielen Dank für Ihre Bewertung.',
	'faq_rating.failed'					=> 'Die Bewertung dieser Frage ist fehlgeschlagen.',
	'faq_rating.rated'					=> 'Sie haben diese Frage bereits bewertet.',

	'no_access.form.t1'					=> 'Bitte füllen Sie das nachfolgende Formular aus, um eine Zugriffserweiterung im Daimler Brand & Design Navigator per E-Mail zu beantragen.',
	'no_access.form.t2'					=> 'Alle Felder sind Pflichtfelder.',
	'no_access.form.t3'					=> 'Sie sind ...',
	'no_access.form.t4'					=> 'Mitarbeiter/Mitarbeiterin der Daimler AG/einer Tochtergesellschaft der Daimler AG oder eines Mercedes-Benz/smart Vertragshändlers/Servicepartners im Auftrag der Daimler AG',
	'no_access.form.t5'					=> 'Mitarbeiter/Mitarbeiterin einer Agentur, eines Zuliefererbetriebs, einer Ausbildungseinrichtung bzw. einer anderen Organisation im Auftrag der Daimler AG/einer Tochtergesellschaft der Daimler AG oder im Auftrag eines Mercedes-Benz/smart Vertragshändlers/Servicepartners',
	'no_access.form.t6'					=> 'Damit wir Ihre Anfrage projektbezogen zuordnen und zeitnah bearbeiten können, teilen Sie uns bitte noch mit:',
	'no_access.form.t7'					=> 'E-Mail',
	'no_access.form.t8'					=> 'Grund für die Zugangsberechtigung',
	'no_access.form.t9'					=> 'Ansprechpartner',
	'no_access.form.t10'				=> 'E-Mail-Adresse des Ansprechpartners',
	'no_access.form.button'				=> 'ZUGRIFFSANFRAGE ÜBERMITTELN',
	'no_access.form.captcha'			=> 'Bitte geben Sie die Zeichen ein',
	'no_access.iagree'					=> 'Ich bin mit der Verarbeitung und Speicherung meiner persönlichen Daten zwecks Bearbeitung meiner Zugriffsanfrage über nicht öffentliche Materialien einverstanden.',
	'no_access.message.warning_email'	=> 'Bitte vergewissern Sie sich noch einmal, dass Sie eine gültige E-Mail-Adresse eingetragen haben.',
	'no_access.message.warning'			=> 'Bitte füllen Sie alle erforderlichen Felder aus, bevor Sie das Formular abschicken.',
	'no_access.message.captcha'			=> 'Bitte geben Sie die Zeichen erneut ein.',
	'no_access.message.required'		=> 'Bitte geben Sie die Zeichen erneut ein.',
	'no_access.agreed_access.accepted'  => 'Bitte bestätigen Sie, dass Sie mit der Verarbeitung und Speicherung Ihrer persönlichen Daten einverstanden sind.',
	'no_access.success.title'			=> '<strong>Zugriffsberechtigung erfolgreich angefordert.</strong></br>',
	'no_access.success.subheading'		=> '<strong>Wir haben Ihre Anfrage über Zugriffsberechtigung erhalten und prüfen zeitnah alle hinterlegten Angaben. Sie erhalten in Kürze eine Antwort.</strong></br>',
	'no_access.success.message'			=> 'Nach erfolgter Verifizierung Ihrer Angaben werden Sie systemseitig per E-Mail benachrichtigt und erhalten weiterführende Informationen zum Zugriff auf die angeforderten nicht öffentlichen Materialien.',

	'no_access.wrong_name'              => 'Wir haben bemerkt, dass der hinterlegte Name der Person zur Genehmigung der Zugriffsberechtigung dem registrierten Namen in Ihrem Benutzerprofil entspricht. Vergewissern Sie sich, dass Sie tatsächlich autorisiert sind Ihre Zugriffsanfrage selbst zu genehmigen, bevor Sie Ihre Daten übertragen.',
	'no_access.wrong_email'             => 'Wir haben bemerkt, dass die hinterlegte E-Mail-Domain der Person zur Genehmigung der Zugriffsberechtigung der registrierten E-Mail-Domain in Ihrem Benutzerprofil entspricht. Vergewissern Sie sich, dass die Person mit der angegebenen E-Mail-Adresse tatsächlich autorisiert ist Ihre Zugriffsanfrage zu genehmigen, bevor Sie Ihre Daten übertragen.',
	'no_access.wrong_both'              => 'Wir haben bemerkt, dass der hinterlegte Name sowie die E-Mail-Domain der Person zur Genehmigung der Zugriffsberechtigung mit den gespeicherten Angaben in Ihrem Benutzerprofil identisch sind. Bestätigen Sie, dass alle Felder korrekt ausgefüllt sind, bevor Sie Ihre Daten übertragen.',
	'no_access.confirm'                 => 'Bestätigen Sie die Richtigkeit der unten gemachten Angaben mit JA. Wählen Sie NEIN, um weitere Änderungen vorzunehmen.',

	'cookie_notification.heading'		=> 'Verwendung von Cookies',
	'cookie_notification.text'			=> '<b>Wir verwenden Cookies.</b> Damit wollen wir unsere Webseiten nutzerfreundlicher gestalten und fortlaufend verbessern. Wenn Sie die Webseiten weiter nutzen, stimmen Sie dadurch der Verwendung von Cookies zu. <a href="'.url('/Cookies').'" >Weitere Informationen erhalten Sie in unseren Cookie-Hinweisen.</a>',
	'cookie_notification.close'			=> 'Schliessen',
	'provider_notification.heading'		=> 'Anbieter/Datenschutz',
	'provider_notification.text'		=> 'Daimler AG<br>
											Mercedesstraße 137<br>
											70327 Stuttgart<br>
											Tel.: +49 711 17 0<br>
											E-Mail: dialog@daimler.com<br><br>
											Vertreten durch den Vorstand: Ola Källenius (Vorsitzender), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm<br>
											Vorsitzender des Aufsichtsrats: Manfred Bischoff<br>
											Handelsregister beim Amtsgericht Stuttgart, Nr. HRB 19360<br>
											Umsatzsteueridentifikationsnummer: DE 81 25 26 315<br><br>
											<a href="'.url('/Legal_Notice').'">Rechtliche Hinweise</a><br>
											<a href="'.url('/Cookies').'">Cookies</a><br>
											<a href="'.url('/Privacy_Statement').'">Datenschutz</a>',
	'provider_notification.close'		=> 'Schliessen',

	'alert.title'						=> 'Daimler Brand & Design Navigator',
	'alert.ok'							=> 'OK',
	'alert.cancel'						=> 'CANCEL',
	'alert.yes'							=> 'Ja',
	'alert.no'							=> 'Nein',
	'filters.refine'					=> 'Anwenden',
	'session.expired' => 'Aus Sicherheitsgründen wurden Sie automatisch weitergeleitet und eventuell zusätzlich ausgeloggt. Die Websitzung ist möglicherweise abgelaufen.',
);

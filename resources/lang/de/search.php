<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Dialog Page Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/


	'path'						=> 'Suchergebnisse',
	'page.title'				=> 'Suchergebnisse für',
	'note'						=> 'Die Suchergebnisse werden nach ihrer Aktualität sortiert.',
	'for1'						=> 'Für Ihre Suche nach ',
	'for2'						=> ' gibt es ',
	'for3'						=> ' Treffer.',
	'advanced'					=> 'Erweiterte Suche',
	'advanced.title'			=> 'Erweiterte Suche.',
	'advanced.note'				=> 'Die erweiterte Suche enthält nur solche Ergebnisse, in denen exakt der eingegebene Wortlaut vorkommt.',
	'advanced.enter'			=> 'Suchbegriff(e) eingeben:',
	'advanced.categories'		=> 'In diesen Kategorien suchen:',
	'advanced.brand'			=> 'Marke',
	'advanced.company'			=> 'Tochtergesellschaft',
	'advanced.all'				=> 'Alle',
	'advanced.from'				=> 'Von:',
	'advanced.to'				=> 'Bis:',
	'advanced.search'			=> 'Suche',
	);

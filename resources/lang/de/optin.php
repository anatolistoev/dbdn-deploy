<?php

return array(
	/*
	|--------------------------------------------------------------------------
	| Newsletter Opt-in email message Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'subject'               => 'Daimler Brand & Design Navigator – Die Newsletter-Option muss noch bestätigt werden',
    'greeting'              => 'Hallo :FIRST_NAME :LAST_NAME,',
    'content'               => 'Sie erhalten diese E-Mail, weil Sie den Newsletter des Online-Portals „Daimler Brand & Design Navigator“ abonniert haben.<br>
                                <br>
                                Bitte bestätigen Sie die Newsletter-Option, indem Sie auf den nachfolgenden Link klicken:<br>
                                <a href=":CONFIRMATION_LINK">Ich möchte den Newsletter abonnieren</a><br>
                                <br>
                                Dieser Link wird nach 24 Stunden automatisch ungültig.<br>
                                <br>
                                Wenn diese E-Mail nicht für Sie bestimmt ist, bitten wir Sie, uns umgehend über den irrtümlichen Empfang zu informieren und diese E-Mail zu löschen. Wir danken Ihnen für Ihre Unterstützung.<br>
                                <br>
                                <br>
                                Mit freundlichen Grüßen<br>',
    'footer'                => 'Daimler Communications<br>
                                Corporate Design<br>
                                Daimler AG<br>
                                096-E402<br>
                                70546 Stuttgart, Germany<br>
                                corporate.design@daimler.com<br>
                                <br>
                                <br>
                                Daimler AG<br>
                                Sitz und Registergericht/Domicile and Court of Registry: Stuttgart<br>
                                HRB-Nr./Commercial Register No. 19360<br>
                                Vorsitzender des Aufsichtsrats/Chairman of the Supervisory Board: Manfred Bischoff<br>
                                Vorstand/Board of Management: Ola Källenius (Vorsitzender/Chairman), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm<br>',

    'success_title' 		=> 'Sie haben den Newsletter erfolgreich abonniert.',
    'success_subheading'    => 'Um den Newsletter wieder abzubestellen, ändern Sie die entsprechende Einstellung in Ihrem Benutzerprofil, indem Sie die Checkbox „Newsletter abonnieren“ deaktivieren.',
    'success_message' 		=> 'Haben Sie noch Fragen zu Ihrem Benutzerkonto?<br>
                                Dann senden Sie eine E-Mail an <a class="contactlink" href="mailto:corporate.design@daimler.com" target="_top">corporate.design@daimler.com</a> und wir helfen Ihnen gerne zeitnah weiter.',
    'failure_title' 		=> 'Die Anmeldung zum Newsletter war nicht erfolgreich.',
    'failure_subheading'    => 'Der verwendete Link zur Newsletter-Anmeldung ist leider bereits abgelaufen. Um den Newsletter erneut zu abonnieren, ändern Sie die entsprechende Einstellung in Ihrem Benutzerkonto, indem Sie die Checkbox „Newsletter abonnieren“ aktivieren.',
    'failure_message' 		=> 'Haben Sie noch Fragen zur Registrierung oder Ihrem Benutzerkonto?<br>
                                <br>
                                Dann senden Sie eine E-Mail an <a class="contactlink" href="mailto:corporate.design@daimler.com" target="_top">corporate.design@daimler.com</a> und wir helfen Ihnen gerne zeitnah weiter.',
     'failure_not_active'    => "Bitte aktivieren Sie Ihr Benutzerkonto, bevor Sie den Newsletter abonnieren. Sie sollten dazu eine weitere Benachrichtigung mit einem Aktivierungslink bereits erhalten haben.",
    'no_user_title'         => 'Die Abmeldung zum Newsletter war nicht erfolgreich.',
    'no_user_subheading'    => 'Der verwendete Link zur Newsletter-Abmeldung ist nicht gültig oder wurde bereits verwendet.',
    'no_user_message'       => 'Möchten Sie den Newsletter erneut abonnieren?</br>
                                Um den Newsletter erneut zu abonnieren, ändern Sie die entsprechende Einstellung in Ihrem Benutzerkonto, indem Sie die Checkbox „Newsletter abonnieren“ aktivieren.</br>
                                </br>
                                Haben Sie noch Fragen zur Registrierung oder Ihrem Benutzerkonto?</br>
                                Dann senden Sie eine E-Mail an <a href="mailto:corporate.design@daimler.com">corporate.design@daimler.com</a> und wir helfen Ihnen gerne zeitnah weiter.',
);
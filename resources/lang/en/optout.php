<?php

return array(
	/*
	|--------------------------------------------------------------------------
	| Newsletter Opt-out email message Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'subject'               => 'Daimler Brand & Design Navigator – Newsletter Cancellation',
    'greeting'              => 'Hello :FIRST_NAME :LAST_NAME,',
	'content'               => 'You are receiving this e-mail, because you have unsubscribed (opted-out) from the Daimler Brand & Design Navigator newsletter.<br>
                                <br>
                                Hereby, we would like to confirm that you have successfully unsubscribed from the newsletter mailing list.<br>
                                <br>
                                Please note that after opting-out of the Daimler Brand & Design Navigator newsletter you will still receive alert notifications related to the saved pages on your Watchlist. If you wish to unsubscribe from receiving alert notifications regarding future page updates, please log in to your userprofile by using your username and password and then remove all saved pages from your Watchlist.<br>
                                <br>
                                Kind regards,<br>',
    'footer'                => 'Daimler Communications<br>
                                    Corporate Design<br>
                                    Daimler AG<br>
                                    096-E402<br>
                                    70546 Stuttgart, Germany<br>
                                    corporate.design@daimler.com<br>
                                    <br>
                                    <br>
                                    Daimler AG<br>
                                    Sitz und Registergericht/Domicile and Court of Registry: Stuttgart<br>
                                    HRB-Nr./Commercial Register No. 19360<br>
                                    Vorsitzender des Aufsichtsrats/Chairman of the Supervisory Board: Manfred Bischoff<br>
                                    Vorstand/Board of Management: Ola Källenius (Vorsitzender/Chairman), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm<br>',

);
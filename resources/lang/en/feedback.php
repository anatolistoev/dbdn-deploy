<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Feedback Page Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/
	

	'path'					=> 'Feedback',
	'page.title'			=> 'Email to Author.',
	'contents'				=> 'Feedback regarding:',
		
	'thanks'				=> 'Your message has been sent successfully.<br>Thank you.',
	'back'					=> 'Back',
		
	'email'					=> 'Your e-mail address:',
	'message'				=> 'Your message:',
		
	'captcha'				=> 'Enter the code shown:',
	'send'					=> 'Send',
	
	'question_regarding' 	=> 'Question related to',	
	'email.email'			=> 'E-mail',
	'email.name'			=> 'Name',
	'email.subject'			=> 'Daimler Brand & Design Navigator - Help Request',
	'email.message'			=> 'Question',
	
	);
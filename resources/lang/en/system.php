<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| System Pages Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'registration_success'				=> 'Registration',
	'registration_success.page.title'	=> 'Your registration has been successfully submitted.',
	'registration_success.subheading'	=> 'You will immediately receive an e-mail with further instructions on how to activate your new user account. Please click on the activation link in the confirmation e-mail to complete the registration.',
	'registration_success.contents'		=> 'If you subscribed to the newsletter as part of your registration, you will receive a further activation link via e-mail.<br>
                                            <br>
                                            Do you need any further assistance in activating your account?<br>
                                            Then send an e-mail to <a class="contactlink" href="mailto:corporate.design@daimler.com?Subject=Registrierung" target="_top">corporate.design@daimler.com</a> and we will help you shortly.',

	'profile_success'					=> 'Profile',
	'profile_success.page.title'		=> 'User profile',
	'profile_success.subheading'		=> 'Your personal data has been successfully updated.',
	'profile_success.contents'			=> '<span class="normal">Thank you sincerely for your cooperation.</span><br>
											Your changes have been saved. The results are now available in the Daimler Brand &amp; Design Navigator. Please keep your personal information up to date.',
	'profile_success.subscription'		=> 'You have chosen to subscribe (opt-in) to the newsletter from the Daimler Brand & Design Navigator. You will receive an e-mail to finally confirm your subscription request. By clicking the link in that e-mail, you will be added to the newsletter mailing list.',
	'profile_success.cancellation'		=> 'You have been removed from the newsletter mailing list. You will receive one final e-mail to confirm that you successfully opted out.',
	'profile_success.further'			=> 'If you have any further questions about the treatment of your personal data, please read carefully the information provided in the section <a href="'.asset('/en/Privacy_Statement').'">&quot;Privacy Statement&quot;</a> or write us by e-mail to <a class="contactlink" href="mailto:corporate.design@daimler.com">corporate.design@daimler.com</a>.',

	'activation_success'				=> 'Registration',
	'activation_success.page.title'		=> 'You have successfully activated your user account.',
	'activation_success.subheading'		=> 'You can now login with your username and password.',
	'activation_success.contents'		=> 'Still have questions about user account?<br>
                                            Then send an e-mail to <a class="contactlink" href="mailto:corporate.design@daimler.com" target="_top">corporate.design@daimler.com</a> and we will help you shortly.',
	'activation_failure'				=> 'Registration',
	'activation_failure.page.title'		=> 'The activation of your user account has failed.',
	'activation_failure.subheading'		=> 'Register again if the link used is already expired or the account no longer exists. ',
	'activation_failure.contents'		=> 'If you have your user account already successfully activated, <a href="'.asset('/login?lang=en').'">you can log in now</a>.<br>
                                            <br>
                                            Do you need any further assistance during the registration?<br>
                                            Then send an e-mail to <a class="contactlink" href="mailto:corporate.design@daimler.com" target="_top">corporate.design@daimler.com</a> and we will help you shortly.',

	'forgotten_form'					=> 'Forgotten Password',
	'forgotten_form.page.title'			=> 'Forgotten Password',
	'forgotten_form.contents'			=> 'If you don\'t remember your password, you can reset it here: Simply enter your username, and you will immediately receive an e-mail with further instructions on how to change your password.',
    'forgotten_sent.page.title'         => 'Now you can change your password as needed.',
    'forgotten_sent.page.subheading'    => 'An e-mail with a link to reset your password has just been sent to you.',
	'forgotten_sent.contents'			=> 'For detailed information on the processing and storage of your personal data, please refer to the <a href="'.asset('/en/Privacy_Statement').'" target="_blank">Privacy Statement</a> of Daimler AG. If you have any further questions, please send an e-mail to <a class="contactlink" href="mailto:corporate.design@daimler.com" target="_top">corporate.design@daimler.com</a> and you will receive an answer promptly.',

	'reset_form'						=> 'Reset Password',
	'reset_form.page.title'				=> 'Reset Your Password',
    'reset_form.page.subheading'		=> 'Now you can change your password as needed.',
	'reset_form.contents'				=> '<span class="red">Important: Before choosing a new password, please observe the following rules:<br>
											- 1 upper case letter as a minimum<br>
											- 1 lower case letter as a minimum<br>
											- 1 digit as a minimum <br>
											- 1 special character as a minimum (!"#$%&amp;\'()*+,%;:=?_@&gt;-)<br>
											- 8 characters as a minimum <br>
											- 25 characters as a maximum<br>
											- No part of the username in the password<br>
											- No umlauts in the password<br></span>',
	'reset_form.username.label'			=> 'Please enter your username:',
	'reset_form.password.label'			=> 'Please enter your new password:',
	'reset_form.confirmation.label'		=> 'Please confirm the password you entered above:',
	'reset_form.captcha.label'			=> 'Enter the code shown:',
	'reset_form.send'					=> 'Send',

	'reset_subject'						=> 'Daimler Brand & Design Navigator - Password Reset Confirmation',

	'reset_success'						=> 'Reset Password',
	'reset_success.page.title'			=> 'You have successfully changed your password. ',
	'reset_success.subheading'			=> 'Please keep your personal data up-to-date and change your password regularly for security reasons. You are responsible for keeping your password secure.',
	'reset_success.contents'			=> 'For detailed information on the processing and storage of your personal data, please refer to the <a href="'.asset('/en/Privacy_Statement').'" target="_blank">Privacy Statement</a> of Daimler AG. If you have any further questions, please send an e-mail to <a class="contactlink" href="mailto:corporate.design@daimler.com" target="_top">corporate.design@daimler.com</a> and you will receive an answer promptly.',

	'login_required.page.title'			=> 'Login Required',
	'login_required.subheading'			=> 'This page requires authentication. In order to get access to this page you need to login.</br>',
	'login_required.contents'			=> 'You must first log in before accessing this content. For non-public materials, an additional access authorization could be required.',

	'no_access.title'					=> 'Authorization Required for Non-Public Materials',
	'no_access.subheading'				=> 'Please note that certain materials are not available to all registered users and additional authorization is required. ',
	'no_access.contents'				=> 'Please confirm that you accept the Daimler AG’s terms of use and privacy statement:',
	'no_access.wrong_name'              => 'We noticed that the name of the contact person entered is identical with your name used during the registration. Before processing, please ensure that you are really authorized to approve this access request by yourself.',
    'no_access.wrong_email'             => 'We noticed that the e-mail domain of the contact person entered is identical with your e-mail domain used during the registration. Before processing, please ensure that the contact person provided is really authorized to approve your access request in this matter.',
    'no_access.wrong_both'              => 'We noticed that the name and the e-mail domain of the contact person entered are identical with your name and e-mail domain used during the registration. Before processing, please confirm that you have filled out all fields correctly.',
    'no_access.confirm'                 => 'By selecting YES you confirm the accuracy of the below-entered information. By selecting NO you can make changes as needed.',

	'mydownloads'						=> 'Download Cart',
	'mydownloads.page.title'			=> 'Download Cart',
	'mydownloads.contents'				=> 'You can use this function to store for later any files and then download the selection in a dynamically generated ZIP file.',
    'mydownloads.notfound'              =>  'Download not available.',
	'of'								=> 'of',
	'downloads_list.selectall'			=> 'Select all',
	'downloads_list.removeSelected'		=> 'Remove selected',
	'downloads_list.alert'				=> 'The requested action could not be executed because you did not select any objects. Please try again.</br></br>',
	'downloads_list.downloadLink'		=> 'Download selected',
	'downloads_list.downloadNow'		=> 'Download now',
	'downloads_list.addToCart'			=> 'Add to Download Cart',
	'downloads_list.addToCart.tablet'	=> 'Add to Download Cart',
	'downloads_list.added'				=> 'Added',
    'downloads_list.added_pages'		=> 'My Added Files',
    'downloads_list.auth_pages'			=> 'Requested Non-Public Files',
    'downloads_list.notice'             => 'Please note, the usage right granted by Daimler AG after access is authorized cannot be transferred to third parties. You must not give a third party access to your login details under any circumstances. The use of the same login details by more than one person is not permitted.',
	'download_overview.addToCart'       => 'Add to Download Cart',
    'download_overview.goTo'            => 'Go to page',
    'download_overview.goToLib'         => 'Go to library',

	'download_na'						=> 'Download not available',

	'downloads_list.categories.1'		=> 'Audio visual media',
	'downloads_list.categories.2'		=> 'Basic elements',
	'downloads_list.categories.3'		=> 'Onscreen media',
	'downloads_list.categories.4'		=> 'Digital media',
	'downloads_list.categories.5'		=> 'Three-dimensional media',
	'downloads_list.categories.6'		=> 'Printed media',
	'downloads_list.categories.7'		=> 'Special applications',

	'downloads_list.category'			=> 'Category',
	'downloads_list.size'				=> 'Size',
	'downloads_list.fileformat'			=> 'File format',
	'downloads_list.program'			=> 'Program',
	'downloads_list.lang'				=> 'Language',
	'downloads_list.dimensions'			=> 'Dimensions',
	'downloads_list.dUnits'				=> 'Dimensions (measured value)',
	'downloads_list.format'				=> 'Format',
	'downloads_list.resolution'			=> 'Resolution',
	'downloads_list.rUnits'				=> 'Resolution (measured value)',
	'downloads_list.color_mode'			=> 'Color mode',
	'downloads_list.pages'				=> 'Number of pages',
	'downloads_list.version'			=> 'Version',
	'downloads_list.uploaded'			=> 'Date added',
	'downloads_list.remLink'			=> 'Remove this item',
	'downloads_list.usage_terms'		=> 'Usage terms',
	'downloads_list.copyrights'			=> 'Copyright notes',
	'downloads_list.usage'				=> 'Usage',
	'downloads_list.file_type'			=> 'File type',
	'downloads_list.description'		=> 'Description',
	'mydownloads.noneselected'			=> 'Note:<br><br>The requested action could not be executed because you did not select any objects. Please try again.<br>',
	'mydownloads.failed'				=> 'Тhere was an error while processing your request.<br>Please try again.',
	'downloads_list.overlimit'			=> 'The combined file size exceeds the limit.<br>
											The selected items have exceeded the maximum amount of data (750 MB) set for merging files into a download archive. You must delete one or more items and then try again to download them as a combined file.',

	'empty_list'						=> 'Empty list.',

	'watchlist'							=> 'Watchlist',
	'watchlist.page.title'				=> 'Watchlist',
	'watchlist.contents'				=> 'You can use this function to receive automatic e-mail notification of adaptations and changes to the contents of the selected pages.',
	'watchlist.remLink'					=> 'Remove',
	'watchlist.added'					=> 'This page has been added to your Watchlist.<br>
											You will automatically receive an e-mail notification, whenever this page is updated.',
	'watchlist.removed'					=> 'One or more pages have been removed from your Watchlist.<br>
                                            Once you have removed a page, you will no longer receive notifications for it.',
	//'watchlist.already'					=> 'You have already added this page to your Watchlist.<br>You have already added this item to your Watchlist and the Daimler Brand & Design Navigator editorial team keeps you informed when it is updated.',
	'watchlist.na'						=> 'Page is not available',

	'rating.added'						=> 'Thank you for your rating!',
	'rating.failed'						=> 'Your rating of the page failed.',

	'faq_rating.added'					=> 'Thank you for your rating!',
	'faq_rating.failed'					=> 'Your rating of the question failed.',
	'faq_rating.rated'					=> 'You already have rated this question!',

	'no_access.form.t1'					=> 'Please complete the following form if you wish to apply for extended access rights for your user account and authorization for the non-public materials.',
	'no_access.form.t2'					=> 'All fields are mandatory.',
	'no_access.form.t3'					=> 'You are...',
	'no_access.form.t4'					=> 'an employee of Daimler AG/a Daimler subsidiary or of a Mercedes-Benz/smart authorized dealer/service partner acting on behalf of Daimler AG',
	'no_access.form.t5'					=> 'an employee of an agency, a supplier company, an educational institution or another organization acting on behalf of Daimler AG/a Daimler subsidiary or on behalf of a Mercedes-Benz/smart authorized dealer/service partner',
	'no_access.form.t6'					=> 'To enable us to match your request to the correct project and deal with it promptly, please tell us:',
	'no_access.form.t7'					=> 'E-Mail',
	'no_access.form.t8'					=> 'Reason for access authorization',
	'no_access.form.t9'					=> 'Contact person',
	'no_access.form.t10'				=> 'Email address of contact person',
	'no_access.form.button'				=> 'SEND ACCESS REQUEST',
	'no_access.form.captcha'			=> 'Enter the code shown',
	'no_access.iagree'					=> 'I agree to the storage and processing of my personal data for the purpose of dealing with my access request for non-public materials.',
	'no_access.message.warning_email'	=> 'Please ensure that you have entered a valid e-mail address.',
	'no_access.message.warning'			=> 'Please make sure all required fields are filled out correctly, before submitting the form.',
	'no_access.message.captcha'			=> 'Please re-enter the code shown.',
	'no_access.message.required'		=> 'Please enter the code shown.',
	'no_access.agreed_access.accepted'  => 'Please confirm, that you agree to the storage and processing of your personal data.',
	'no_access.success.title'			=> 'Access Authorization Successfully Requested',
	'no_access.success.subheading'		=> 'We have received your request for access authorization, and will check all of the information entered shortly. You will receive a response soon.',
	'no_access.success.message'			=> 'Once your information has been verified, you will be notified by e-mail and receive further information on accessing the requested non-public materials.',

	'cookie_notification.heading'		=> 'Use of Cookies',
	'cookie_notification.text'			=> '<b>We use cookies.</b> We want to make our website more user-friendly and continuously improve it. If you continue to use the website, you agree to the use of cookies. <a href="'.url('/Cookies').'" class="arrowlink">For more information, please refer to our Cookie Statement.</a>',
	'cookie_notification.close'			=> 'Close',
	'provider_notification.heading'		=> 'Provider/Privacy',
	'provider_notification.text'		=> 'Daimler AG<br>
											Mercedesstraße 137<br>
											70327 Stuttgart<br>
											Phone: +49 711 17 0<br>
											E-mail: dialog@daimler.com<br><br>
											Represented by the Board of Management: Ola Källenius (Vorsitzender/Chairman), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm<br>
											Chairman of the Supervisory Board: Manfred Bischoff<br>
											Commercial Register Stuttgart, No. HRB 19360<br>
											VAT registration number: DE 81 25 26 315<br><br>
											<a href="'.url('/Legal_Notice').'">Legal Notice</a><br>
											<a href="'.url('/Cookies').'">Cookies</a><br>
											<a href="'.url('/Privacy_Statement').'">Privacy Statement</a>',
	'provider_notification.close'			=> 'Close',
	'alert.title'						=> 'Daimler Brand & Design Navigator',
	'alert.ok'							=> 'OK',
    'alert.cancel'						=> 'CANCEL',
    'alert.yes'                         => 'Yes',
    'alert.no'                          => 'No',
	'filters.refine'					=> 'Apply',
	'session.expired' => 'You have been automatically redirected and possibly logged out for security reasons. The web session may have expired.',
	);

<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Template Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/
	'dialog'					=> 'Dialogue',
	'contacts'					=> 'Contacts',
	'currLocale'				=> 'en',
	'altLocale'					=> 'de',
	'altLang'					=> 'Deutsch',

	'brand'						=> 'Brand/Subsidiary',
//	'subsidiary'				=> 'Company',
//    'brand.home'                => 'SELECT A BRAND/COMPANY',

	'home'						=> 'Home',

	'related'					=> 'Related links',
	'downloads'					=> 'Related downloads',

	'personal services'			=> 'Personal Services',
	'login.error'				=> 'Failed Login<br><br>The combination of username and password you entered is not known. Please try again.',
	'welcome'					=> 'Welcome',
	'username'					=> 'Username',
	'password'					=> 'Password',
	'login'						=> 'Log In',
    'login.subheading'			=> 'Please log in using your user name and password.',
    'login.signup'              => 'If you have not yet registered, <a href="'.asset('/registration').'">create a new user account</a> in order to continue.',
	'registration'				=> 'Sign Up',
	'forgot_link'				=> 'Forgotten your password?',

	'user profile'				=> 'User profile',
	'watchlist'					=> 'Watchlist',
	'logout'					=> 'Log out',

	'print_page'				=> 'Print',
	'print_pdf'					=> 'Save as PDF',
	'send page'					=> 'Send this page',
	'rate_page'					=> 'Rate this page',
	'addwatch'					=> 'Add this page to Watchlist',
	'removewatch'				=> 'Remove this page from Watchlist',

	'help'						=> 'Help',
	'send feedback'				=> 'Send us feedback',
	'faq'						=> 'FAQ',
	'tips'						=> 'Help',
	'glossary'					=> 'Glossary',
	'viewallterms'				=> 'Vew all terms',
	'sitemap'					=> 'Sitemap',
	'index'						=> 'Index',

	'gotop' 					=> 'Go to top',

	'copyright' 				=> 'Daimler AG. All rights reserved.',

	'provider'					=> 'Provider',
	'notices'					=> 'Legal Notice',
	'cookies'					=> 'Cookies',
	'privacy.header'			=> 'Provider / Privacy',
	'privacy.footer'			=> 'Privacy Statement',
	'terms'						=> 'Terms of Use',
	'commentterms'				=> 'Comment Terms and Conditions',

	'glossaryPageTitle'			=> 'Corporate Design Glossary',
	'registration.page.title'	=> 'Sign Up and Get Access to More Advanced Functions or Non-Public Materials',
	'profile.page.title'		=> 'Your User Profile with Personal Data and Settings ',
	'profile'					=> 'My profile',

	'rating.text'				=> 'Do you find this page useful?',
	'rating.not_useful'			=> 'Not useful',
	'rating.very_useful'		=> 'Very useful',
	'rating.button'				=> 'Rate',

	'recent_topics'				=> 'Recent Topics',
	'most_visited'				=> 'Most Visited',
	'most_rated'				=> 'Most Rated',

	'comments.title'			=> 'Comments',
	'comments.says'				=> 'says:',
	'comments.headline'			=> 'Headline:',
	'comments.add_your'			=> 'Add Your Comment',
	'comments.iagree'			=> 'I agree to the',
	'comments.terms'			=> '<a target="_blank" href="'.url('Rules_for_Comment_Submissions').'">Comment Terms and Conditions</a>',
	'comments.send'				=> 'Send comment',
	'comments.not_logged'		=> 'Your comments are welcome. Please login to add your comment. Please read the <a href="'.url('Rules_for_Comment_Submissions').'">Comment Terms and Conditions</a> carefully.',
	'comments.thanks'			=> 'Thank you for your comment. This has been successfully sent.<br>Please note, your comment will be checked before it is posted to the Daimler Brand &amp; Design Navigator.',
	'and'						=> 'and',
    'design_manuals_downloads'  => 'Design Manuals and Downloads',

	'zoomimage'					=> 'Zoom image',
	'lprev'						=> 'Previous',
	'lnext'						=> 'Next',
	'lof'						=> 'of',

	'epos_block'				=> 'Electronic Publishing and Ordering System (ePOS)<br/>
									Online configurator for various standard media.<br/><br/>
									<a href="http://portal.e.corpintra.net/go/epos" class="textLink" target="_blank" immune="true">Daimler ePOS</a><br/>
									(internal access only)',
	'footer.about'				=> '<h2>About Daimler Brand<br />
									& Design Navigator</h2>
									<p>Daimler AG is one of the biggest producers of premium cars and the world\'s biggest manufacturer of commercial vehicles with a global reach. We provide financing, leasing, fleet management, insurance and innovative mobility services.</p>
									<p>The Brand & Design Navigator online portal supports employees and external service providers with information on the Daimler AG\'s Corporate Design and its brands &mdash; from the basic elements to their practical application as well as layout templates and best practice examples.</p>',
	'explainer.default_text'	=> 'Navigate with mouse over icons to explore the elements and find out more details on the layout principles.',

    'navigation.selection'      => 'Active options',
	'navigation.clear'          => 'Reset',
	'navigation.select_all'		=> 'All',
    'navigation.by_time'        => 'Time',
    'navigation.by_tag'         => 'Tag: ',
	'navigation.by_title'       => 'Title',
	'navigation.a_z'            => 'A-Z',
	'navigation.z_a'            => 'Z-A',
	'navigation.by_period'      => 'Time',
	'navigation.newest'         => 'Latest',
	'navigation.oldest'         => 'Oldest',
    'navigation.define_perion'  => 'Define period:',
    'navigation.from'           => 'from',
    'navigation.to'             => 'to',
	'navigation.by_usage'       => 'Category',
	'navigation.usage_label'    => 'Categories',
	'navigation.by_type'        => 'File type',
	'navigation.type_label'     => 'File types',
	'navigation.by_views'       => 'Views',
	'navigation.most_viewed'    => 'Most Viewed',
	'navigation.by_rating'      => 'Rating',
	'navigation.most_rated'     => 'Most Rated',
	'navigation.by_tags'        => 'Tag',
	'navigation.tags_label'     => 'Tags',
	'navigation.by_comments'    => 'Comments',
    'navigation.comments_label' => 'Most commented',
	'navigation.by_brand'		=> 'Brand',
	'navigation.back'           => 'Back',
    'navigation.menu'           => 'Menu',
    'navigation.filters'        => 'Filters',
    'navigation.filter_by'      => 'Filter by',
    'navigation.sort_by'        => 'Sort by',
    'filters.chooseopt'         => 'No active options',

    'navigation._best_practice' => 'Show All',
    'navigation._downloads'  	=> 'Show All',

	'interest.most_recent'      => 'Most recent',
	'interest.most_searched'    => 'Most searched',
	'interest.most_viewed'      => 'Most viewed',
	'interest.most_rated'       => 'Most rated',
	'interest.most_downloaded'  => 'Most downloaded',

    'best_practice.contact'     => 'Contact',
	'no_results'				=> '<h1 class="hct">No results found</h1>
									<h1 class="hct">
										<small>
											<p>Unfortunately, no results were found for the selected combination.</p>
											<p>Please try again using a different set of filter criteria.</p>
										</small>
									</h1>',
	'wl.user_profile'			=> 'Data and Settings',
	'wl.watch_list'				=> 'Watch List',
	'wl.download_cart'			=> 'Download Cart',
	'ask_question'				=> 'Ask a question',
	'dialog.messageText'		=> 'Can we be of any further assistance to you?',
	'dialog.name'				=> 'Name',
	'dialog.email'				=> 'E-mail',
	'dialog.send'				=> 'Send',

	'terms.frequently'			=> 'Above-Referred Terms',
	'terms.view_all'			=> 'View all terms',
	'faq.related'				=> 'Related FAQ',
	'faq.view_all'				=> 'View all FAQ',

	'faq.header'				=> 'Frequently Asked Questions (FAQ)',
	'faq.related_links'			=> 'Related links',

	'search_box.label'			=> 'What are you looking for?',
	'search_box.top.placeholder'=> 'Start typing a search term',

	'search.advanced.tootip'	=> 'Advanced search options',

	'print.header'				=> 'Daimler Brand & Design Navigator',
	'print.footer'				=> '&#169; '.date('Y').' Daimler AG. All rights reserved.',

	'navigation.search.title'	=> 'Advanced Search',

	'comment.headline'			=> 'Headline',
	'comment.text'				=> 'Comment',

	'bp.load_more'				=> 'Show more',
    'carousel.more'             => 'Read more',
	'brand_choose'				=> 'Select a brand',
	'company_choose'			=> 'Select a company',
	'pdf.header'                => '<p><b>This content has been printed on :time </b> <br> Please note that the Daimler Brand & Design Navigator online portal is the only source for Design Manuals and Downloads and it is only available online. Hence, liability for the up-to-dateness, accuracy and completeness of the provided information cannot be assured. The printed version may not present the most recent changes or improvements of the contents.</p>',
    'pdf.footer'                => '<p>Published on :time</p><p>https://designnavigator.daimler.com </p><p>&#169; ' . date('Y') . ' Daimler AG. All rights reserved.</p>',
    'pdf.from'                  => 'Page ',
    'pdf.to'                    => ' of ',
    'related.downloads'         => 'Related downloads',
    'related.pages'             => 'Related links',
    'related.bp'                => 'Latest Best Practice Articles',
    'bp.related'                => 'Recommended Manuals',
    'intro'                     => 'The up-to-the-minute content on the online portal "Daimler Brand & Design Navigator" presents Daimler\'s mandatory Corporate Design rules which are binding for all companies bearing the corporate name: <a href="http://erd3.e.corpintra.net/ERD/Home/ruleProfile/32102"  target="_blank">Legal Entity Policy D 1715.0 in the Enterprise Regulations Database (ERD) on the Employee Portal</a> (available internally only).',
    'file'                      => ' File',
    'files'                     => ' Files',
    'protected'                 => 'Non-public',
    'recommended'               => 'Recommended',
    'learn_more'				=> 'learn more',
    'choose_brand_company'		=> 'Select a Brand or Company',
    'home.faq.title'            => 'FAQ of the day',
    'home.faq.button'           => 'go to FAQ',
    'home.term.title'           => 'Term of the day',
    'home.term.button'          => 'go to glossary',
    'latest_design_manuals' 	=> 'Latest Design Manuals',
    'all_design_manuals' 	    => 'All Design Manuals',
    'latest_best_practice'      => 'Latest Best Practice',
    'all_best_practice' 	    => 'All Best Practice',
	'latest_downloads'			=> 'Latest Downloads',
	'all_downloads'				=> 'All Downloads',
	'filter_result.fields_sort'	=> '[:result_count] results found',
	'filter_result.fields'		=> '[:result_count] results found',
	'filter_result.sort'		=> '[:result_count] results re-sorted',

    'help_form'                 => 'Help',
    'help_form.subheading'      => 'Ask a question.',
    'meta.description'          => 'The “Daimler Brand & Design Navigator” online portal supports employees and external service providers with information on the Daimler AG’s Corporate Design and its brands.',

	'nav.corporate_brand'		=> 'Corporate Brand',
	'nav.subsidaries'			=> 'Subsidaries',
	'nav.brands'				=> 'Brands'
);

<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Guided Tour texts
	|--------------------------------------------------------------------------
	|
	|
	*/
    'tour_0.header'         => 'Brief outline – Guided Tour',
    'tour_0.text'           => '<p>Use the Guided Tour to find out more about the Daimler Brand & Design Navigator and to get an overview of its sections and functions. Once you have started the Guided Tour, you can end it at any time by closing the window.',
    'tour_1.header'         => 'Frontpage',
    'tour_1.text'           => '<p>As the first access point to the Online Portal, the frontpage provides an up-to-date overview of all news and changes relating to current sections of individual brands and companies.</p>',
    'tour_2.header'         => 'Spotlight',
    'tour_2.text'           => '<p>The banners take up the full width of the frontpage and refer to current topics. Clicking on the banner area takes users to the linked page.</p>',
    'tour_3.header'         => 'Quick Search',
    'tour_3.text'           => '<p>The search field has an autocomplete function to help users locate information quickly. Users can call up views of most searched, frequently visited and favorite pages as well as most downloaded files in real time below the search field. </p>',
    'tour_4.header'         => 'Most Recent',
    'tour_4.text'           => '<p>There are two different views available for the latest updates: compact or detailed.</p>',
    'tour_5.header'         => 'Personalized Access',
    'tour_5.text'           => '<p>Registered users get access to additional functions and can request access authorization for password-protected pages. The Watch List informs users automatically when saved contents are changed. The Download Cart serves as a virtual storage area for files that can be downloaded individually or collectively.</p>',
    'tour_6.header'         => 'First Click',
    'tour_6.text'           => '<p>Brand & Design Navigator presents Design Manuals and Downloads from various brands in the Daimler Group. This drop-down menu gives you fast access to all brands and companies in the Online Portal.</p>',
    'tour_7.header'         => 'Main Navigation',
    'tour_7.text'           => '<p>The navigation bar with the main tabs is located horizontally above each page and remains visible at all times when scrolling in a reduced variant. </p>',
    'tour_8.header'         => 'Carousel-Type Overview',
    'tour_8.text'           => '<p>This section displays other new topics from the particular brand or company. Each teaser comprises a preview image and a brief introductory text. The bottom bar is a navigation aid for the presented pages. </p>',
    'tour_9.header'         => 'Footer Bar',
    'tour_9.text'           => '<p>The footer bar - including the Help, Glossary, FAQs and Sitemap links - is locked at the bottom edge of the browser window. The Help form allows users to comment on page contents or send a message to the editorial team. The Glossary and the FAQs provide further assistance.</p>',
    'tour_10.header'        => 'Corporate Identity – Introduction',
    'tour_10.text'          => "<p>This topic section describes the positioning, brand architecture and principles for the company brand's branding, and forms the basis for better understanding of Daimler's visual identity. </p>",
    'tour_11.header'        => 'Design Manuals – Core',
    'tour_11.text'          => "<p>The contents of the Brand & Design Navigator have the character of policies. Using practically relevant rules and suitable visualizations, the Online Portal enables experiencing Daimler's visual presentation online.</p>",
    'tour_12.header'        => 'Quick Menu',
    'tour_12.text'          => '<p>The left-hand menu enables users to navigate through the available Manuals. The accompanying \"Back\" quick-link makes it easy to scroll back.</p>',
    'tour_13.header'        => 'Subnavigation',
    'tour_13.text'          => '<p>The items in the right-hand bar allow users to call up specific main sections of a contents page. The subnavigation bar can be used for scrolling up and down.</p>',
    'tour_14.header'        => 'Functions',
    'tour_14.text'          => '<p>The Print and Rate functions move in sync with the navigation direction and remain visible at all times at the right edge of the browser window. The print-optimized version of a page is opened in a separate browser window.</p>',
    'tour_15.header'        => 'Related',
    'tour_15.text'          => '<p>Lists of related Manuals or Downloads can be made to appear in the right-hand bar by clicking.</p>',
    'tour_16.header'        => 'Zooming',
    'tour_16.text'          => '<p>The zoom function displays the design rule examples in an appropriate manner and offers a zoom factor of up to 400% in the detailed view.</p>',
    'tour_17.header'        => 'Glossary',
    'tour_17.text'          => '<p>Underlined words in the design rules refer to terms in the Glossary that can be called up by clicking on them.</p>',
    'tour_18.header'        => 'Book Navigation',
    'tour_18.text'          => '<p>The buttons provide another option for navigating back and forth through the Manuals. In this way it is possible to call up and read contents step by step.</p>',
    'tour_19.header'        => 'Best Practice – Latest Magazine',
    'tour_19.text'          => '<p>Successful and CD-compliant implementations within the Group are presented regularly in the Best Practice examples, the Brand & Design Navigator magazine.</p>',
    'tour_20.header'        => 'Drop-Down Filter Menu',
    'tour_20.text'          => '<p>The published reports can be sorted alphabetically and by date or based on various categories and tags. The filter menu is visible at all times – on subpages and when scrolling.</p>',
    'tour_21.header'        => 'Added Value through Dialog',
    'tour_21.text'          => '<p>Registered users can comment on the examples and swap experiences with other users.</p>',
    'tour_22.header'        => 'Downloads – Application in Action',
    'tour_22.text'          => '<p>To ensure brand-compatible implementation of the Corporate Design, Brand & Design Navigator provides artworks, templates and other documents for all areas of application within Daimler corporate communications.</p>',
    'tour_23.header'        => 'Drop-Down Filter Menu',
    'tour_23.text'          => '<p>The download files provided can be sorted alphabetically and by date or based on various categories and keywords. Optional drop-down menus include \"Usage\" and \"File Type\".</p>',
    'tour_24.header'        => 'Correct Use',
    'tour_24.text'          => '<p>All download assets for all brands are added based on a uniform principle. The description and other technical specifications provide quick guidance for correct use of the artworks, templates and other documents.</p>',
    'tour_25.header'        => 'Download Options',
    'tour_25.text'          => '<p>The assets can be downloaded in the browser window conveniently with just one click. Registered users can add files to the Download Cart at any time and download them together at a later time.</p>',
    'tour_26.header'        => 'End of the Guided Tour',
    'tour_26.text'          => '<p>The Guided Tour ends here. You can now try out the contents and functions of the Daimler Brand & Design Navigator for yourself. Should you have any questions or comments, you are welcome to email us at <a class=\"tour_link\" href=\"mailto:corporate.design@daimler.com?Subject=\" target=\"_top\">corporate.design@daimler.com<a>.</p>',

	);
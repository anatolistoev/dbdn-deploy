<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| such as the size rules. Feel free to tweak each of these messages.
	|
	*/

	"accepted"         => "The :attribute must be accepted.",
    "uploaded"         => "The file size exceeds the upload limit (250 MB) allowed and cannot be saved.",
	"active_url"       => "The :attribute is not a valid URL.",
	"after"            => "The :attribute must be a date after :date.",
	"alpha"            => "The :attribute may only contain letters.",
	"alpha_dash"       => "The :attribute may only contain letters, numbers, and dashes.",
	"alpha_num"        => "The :attribute may only contain letters and numbers.",
	"array"            => "The :attribute must be an array.",
	"before"           => "The :attribute must be a date before :date.",
	"between"          => array(
		"numeric" => "The :attribute must be between :min - :max.",
		"file"    => "The :attribute must be between :min - :max kilobytes.",
		"string"  => "The :attribute must be between :min - :max characters.",
		"array"   => "The :attribute must have between :min - :max items.",
	),
	"confirmed"        => "The :attribute confirmation does not match.",
	"date"             => "The :attribute is not a valid date.",
	"date_format"      => "The :attribute does not match the format :format.",
	"different"        => "The :attribute and :other must be different.",
	"digits"           => "The :attribute must be :digits digits.",
	"digits_between"   => "The :attribute must be between :min and :max digits.",
	"email"            => "The :attribute format is invalid.",
	"exists"           => "The selected :attribute is invalid.",
	"image"            => "The :attribute must be an image.",
	"in"               => "The selected :attribute is invalid.",
	"integer"          => "The :attribute must be an integer.",
	"ip"               => "The :attribute must be a valid IP address.",
	"max"              => array(
		"numeric" => "The :attribute may not be greater than :max.",
		"file"    => "The :attribute may not be greater than :max kilobytes.",
		"string"  => "The :attribute may not be greater than :max characters.",
		"array"   => "The :attribute may not have more than :max items.",
	),
	"mimes"            => "The :attribute must be a file of type: :values.",
	"min"              => array(
		"numeric" => "The :attribute must be at least :min.",
		"file"    => "The :attribute must be at least :min kilobytes.",
		"string"  => "The :attribute must be at least :min characters.",
		"array"   => "The :attribute must have at least :min items.",
	),
	"not_in"           => "The selected :attribute is invalid.",
	"numeric"          => "The :attribute must be a number.",
	"regex"            => "The :attribute format is invalid.",
	"required"         => "The :attribute field is required.",
	"required_if"      => "The :attribute field is required when :other is :value.",
	"required_with"    => "The :attribute field is required when :values is present.",
	"required_without" => "The :attribute field is required when :values is not present.",
	"same"             => ":other does not match.",
	"size"             => array(
		"numeric" => "The :attribute must be :size.",
		"file"    => "The :attribute must be :size kilobytes.",
		"string"  => "The :attribute must be :size characters.",
		"array"   => "The :attribute must contain :size items.",
	),
	"unique"           => "The :attribute has already been taken.",
	"url"              => "The :attribute format is invalid.",

	/*
	|--------------------------------------------------------------------------
	| DBDN-specific Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| These rules are defined to accomodate custom validation for the DBDN project
	| 
	*/
	'username_rule'		=> "Invalid :attribute: only a-z, A-Z, 0-9, '-', '_', '’', '.' symbols allowed.",
	'has_upper' 		=> "Invalid :attribute: minimum 1 upper case letter required",
	'has_lower' 		=> "Invalid :attribute: minimum 1 lower case letter required",
	'has_digit' 		=> "Invalid :attribute: minimum 1 digit required",
	'not_in_username' 	=> "The :attribute cannot contain part of the username",
	'has_special' 		=> "Invalid :attribute: minimum 1 special character required (!\"#$%&'()*+,%;:=?_@>-)",
	'no_umlauts' 		=> "The :attribute cannot contain umlaut symbols",
    'no_tags'         => ":attribute cannot contain HTML tags.",
	'is_slug'	 		=> "Invalid :attribute: only a-z, A-Z, 0-9, '-', '_', '~' symbols allowed.",
	
	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
		'captcha' => array(
			'same'		=> 'Please re-enter the code shown.',
			'required'	=> 'Please re-enter the code shown.',
		),
		'agreed' => array(
			'accepted'	=> 'In order to register, please agree to the Terms of Use.'
		),
		'agreed_access' => array(
			'accepted'	=> 'In order to request access, please agree to the Terms of Use.'
		),
		'messageText' => array(
			'required'	=> 'The message field is required.'
		),
		'comment_agree' => array(
			'accepted'	=> 'You have to accept the "Comment Terms and Conditions".'
		),
		'headline'	=> array(
			'required'	=> 'Please enter a headline in your comment.'	
		),
		'comments'	=> array(
			'required'	=> 'Please enter your comment.'
		),
		'related_id' => array(
			"different"        => "The selected page cannot be added as a related link, because it appears to be the same as the current one.",
		),
		'langs' => array(
			'min' => "Please choose at least one language. ",
		),
		'slug' => array(
			'required'	=> 'A slug URL is required.'
		),
		'email' => array(
			'required'	=> 'Please enter an e-mail address.'
		)
	),
	

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array('password_confirmation'=>'Password confirmation'),

);

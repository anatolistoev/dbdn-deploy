<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Activation email message Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/



	'subject'               => 'Daimler Brand & Design Navigator - Login Attempt',
    'greeting'              => 'Dear :FIRST_NAME :LAST_NAME,',
    'content'               => 'You received this e-mail because you have attempted to log in to the Daimler Brand & Design<br>
                                Navigator CMS. Please note, access is only permitted to authorized users acting on behalf of<br>
                                Daimler Communications – Corporate Design. Violation of any technical security or editorial<br>
                                regulations may result not only in excluding user from the Daimler Brand & Design Navigator CMS but<br>
                                even in suspending the account as well.<br>
                                <br>
                                Your username: :USERNAME<br>
                                <br>
                                Due to security restrictions, please copy and enter the following verification code into the log in form in order to access the Daimler Brand & Design Navigator CMS: <br>
                                <br>
                                :CONFIRM_CODE <br>
                                <br>
                                This token will automatically expire 2 hours after this e-mail was sent.<br>
                                <br>
                                We kindly ask you to pay attention to the correctness of your entries.<br>
                                <br>
                                Thank you for your cooperation.<br>
                                <br>
                                Kind regards,<br>',
    'footer'                => 'Daimler Communications<br>
                                Corporate Design<br>
                                Daimler AG<br>
                                096-E402<br>
                                70546 Stuttgart, Germany<br>
                                corporate.design@daimler.com<br>
                                <br>
                                <br>
                                Daimler AG<br>
                                Sitz und Registergericht/Domicile and Court of Registry: Stuttgart<br>
                                HRB-Nr./Commercial Register No. 19360<br>
                                Vorsitzender des Aufsichtsrats/Chairman of the Supervisory Board: Manfred Bischoff<br>
                                Vorstand/Board of Management: Ola Källenius (Vorsitzender/Chairman), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm<br>',

	);
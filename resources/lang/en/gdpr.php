<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| GDPR Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines include messages that are related to
	| a user deleting his own account, under the GDPR rules
	*/
    'request.boxheader' => 'User Profile Deletion',
    'request.boxtext'   => 'Please note, that once your user profile is permanently deleted, you would not be able to retrieve any personal data or access any non-public materials. In case you want to delete your user profile, please click below on the checkbox:',
    'request.boxchbx'   => 'Delete User Profile',
    'request.faqtext'   => '<a href="/faq/daimler/292/how-can-i-delete-my-user-account" target="_blank">Learn more about user profile deletion</a>',
    'request.success'   => '<h1>You have chosen to permanently delete your user profile.</h1><p>You will shortly receive a notification on the email address saved in your user profile in order to confirm and complete the deletion process. Please check your inbox and follow the instructions. Thank you.</p>', //The template for the text in the success page that appears only if the gdpr button is clicked //tbt
    'request.nouser'    => '<h1>The requested deletion has failed and could not be processed.</h1><p>The user account corresponding to this link no longer exists and has already been permanently deleted. The deletion link is only valid once and cannot be used again.</p>',
    'request.wrongtoken'=> '<h1>The requested deletion has failed and could not be processed.</h1><p>The requested deletion of your user account could not be completed because you are currently using an invalid link. You can re-initialize this process after logging in successfully again.</p>',
    'operation.complete'=> 'As requested, we hereby confirm, that your user profile is now permanently deleted, along with all personal data and all access permissions to non-public materials.',
    'mail.subject'      => 'Daimler Brand & Design Navigator – Your Request for User Profile Deletion',
    'mail.greeting'     =>  '<p>Hello, :FIRST_NAME :LAST_NAME,</p>',
    'mail.content'      => '<p>You are receiving this email because you have chosen to permanently delete your user profile on the “Daimler Brand & Design Navigator” online portal.</p>
                            <p>We would like to kindly remind you that by deleting your user profile, all your personal data stored, and all access privileges related to non-public materials will be lost. You will not be able to retrieve them.</p>
                            <p>Please follow the link below to delete your user profile:<br/>
                            <a href=":CONFIRMATION_LINK">:CONFIRMATION_LINK</a></p>
                            <p>This link will automatically expire 15 minutes after this e-mail was sent.</p>
                            <p>If you have selected this option by mistake and do not want to delete your user profile; or if you haven’t initialized this process, please ignore this message.</p>
                            <p>Please let us know, if any further assistance on your user profile is needed and we would be glad to assist you in this matter. Simply reply to this email and we will contact you as soon as possible.</p>
                            <p>If you are not the intended addressee, please inform us immediately that you have received this e-mail by mistake and delete it. We thank you for your support.</p>
                            <p>Kind regards,</p>',
    'mail.footer'       => '<p>Daimler Communications<br/>
                            Corporate Design<br/>
                            Daimler AG<br/>
                            096-E402<br/>
                            70546 Stuttgart, Germany<br/>
                            corporate.design@daimler.com
                            </p>
                            <p>Daimler AG<br/>
                            Sitz und Registergericht/Domicile and Court of Registry: Stuttgart<br/>
                            HRB-Nr./Commercial Register No. 19360<br/>
                            Vorsitzender des Aufsichtsrats/Chairman of the Supervisory Board: Manfred Bischoff<br/>
                            Vorstand/Board of Management: Ola Källenius (Vorsitzender/Chairman), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm
                            </p>'
);

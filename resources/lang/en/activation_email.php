<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Activation email message Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/



	'subject'				=> 'Daimler Brand & Design Navigator – Your User Account Needs to Be Activated',
    'greeting'              => 'Hello :FIRST_NAME :LAST_NAME,',
    'content'               => 'You received this e-mail because your e-mail address was used in a registration for the Daimler Brand & Design Navigator online portal.<br>
                                <br>
                                Your username: :USERNAME<br>
                                <br>
                                Please click on the following link to activate your user account:<br>
                                <a href=":CONFIRMATION_LINK">I want to activate my user account</a><br>
                                <br>
                                This link will automatically expire 24 hours after this e-mail was sent.<br>
                                <br>
                                If you are not the intended addressee, please inform us immediately that you have received this e-mail by mistake and delete it. We thank you for your support.<br>
                                <br>
                                Kind regards,<br>',
    'footer'                => 'Daimler Communications<br>
                                Corporate Design<br>
                                Daimler AG<br>
                                096-E402<br>
                                70546 Stuttgart, Germany<br>
                                corporate.design@daimler.com<br>
                                <br>
                                <br>
                                Daimler AG<br>
                                Sitz und Registergericht/Domicile and Court of Registry: Stuttgart<br>
                                HRB-Nr./Commercial Register No. 19360<br>
                                Vorsitzender des Aufsichtsrats/Chairman of the Supervisory Board: Manfred Bischoff<br>
                                Vorstand/Board of Management: Ola Källenius (Vorsitzender/Chairman), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm<br>',
	);
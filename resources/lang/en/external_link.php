<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Dialog Page Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/
	
	'path'			=> 'Notice',
	'text'		=> '<h1>You Are Now Exiting Daimler Brand &amp; Design Navigator</h1>
					<h1><small>You are now moving from the Daimler Brand & Design Navigator to a website which is not administered by Daimler AG.</small></h1>
					<p>
					<strong>Internal Links</strong><br>
					All links to websites or online applications at Daimler are accessible exclusively through the Group\'s internal server network. They are intended for internal purposes only and are subject to the Group\'s rules for use of the Intranet. All external access attempts are automatically denied.</p>
					<p><strong>External Links</strong><br>
					Daimler AG does not accept any responsibility for the content, function, absence of errors, or legality of the external websites to which you are directed by means of a link.
					</p>',
	'continue'		=> 'Continue to',
	'back'			=> 'Back',

	);
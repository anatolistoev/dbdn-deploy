<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Dialog Page Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/
	

	'dialog'							=> 'Dialogue',
	'page.title'						=> 'Dialogue.',
	'contents'							=> 'This form enables you to ask questions or to send us your comments about the Daimler Brand & Design Navigator. We will send our reply within 48 hours to the e-mail address you have provided.',
	'note'								=> '<b>Note:</b> Please fill out all the required fields marked with *.',
	'academic_title'					=> 'Academic title:',
	'first_name'						=> 'First name',
	'last_name'							=> 'Last name:',
	'email'								=> 'E-Mail:',
	'subject'							=> 'Subject:',
	'message'							=> 'Message:',
	'captcha'							=> 'Enter the code shown:',
	'note2'								=> 'Note: Your e-mail address will only be saved for the purposes of dealing with your inquiry.',
	'send'								=> 'Send',
	
	'sent'								=> '<br>Thank you! <br>
											Your message has been sent successfully to  <a href="mailto:corporate.design@daimler.com">corporate.design@daimler.com</a>. <br>
											We will send our reply within 48 hours  to the email address you have provided.',
	);
<?php 

return array(

	'column.id' => 'Id',
	'column.question' => 'Question',
    'column.brand' => 'Brand',
    'column.category' => 'Topic',
	'column.modified_at' => 'Modified',
	'placeholder.faq_search' => 'FAQ Search',
	'placeholder.faq_sort' => 'FAQ Sort',
	'button.filter' => 'FILTER',
	'edit' => 'Edit FAQ',
	'add' => 'Add FAQ',
	'remove' => 'Remove',
	'remove.confirm' => 'Are you sure you want to delete the selected FAQ?',
	'form.created_at' => 'Date added',
	'form.modified_at' => 'Date modified',
	'form.question' => 'Question',
	'form.answer' => 'Answer',
    'form.tags' => 'Tags',
    'form.brand' => 'Brand',
    'form.category' => 'Topic',
	'change_lang' => 'Language'
);
<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Emails message Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/
	

	
	'request_access_confirm.subject'				=> 'Daimler Brand & Design Navigator – Your Access Authorization Is Being Processed',
	'request_access_confirm.greeting'				=> 'Hello :FIRST_NAME :LAST_NAME,',
	'request_access_confirm.content'				=> 
			'Thank you for your request for authorization to access non-public materials on the "Daimler Brand & Design Navigator" online portal.<br>
			<br>
			We are glad to assist you in this matter, and we try to respond promptly to all requests. All of the information entered is checked carefully, and access requests are usually processed the same day. In exceptional cases, and if we are experiencing a high volume of requests, a response may be delayed by up to 48 hours.<br>
			<br>
			Once your information has been verified, you will be notified by e-mail and receive further information on accessing the requested non-public materials.<br>
			<br>
			All personal data provided will be treated as confidential and only used for the intended purpose in accordance with the Daimler AG’s privacy statement:<br>
			<a href="'.asset('/Privacy_Statement?lang=en').'">Privacy Statement</a><br>
			<br>
			Kind regards,<br>',
	'request_access_confirm.download.content'		=> 
			'Thank you for your request for authorization to access non-public materials on the "Daimler Brand & Design Navigator" online portal.<br>
			<br>
			We are glad to assist you in this matter, and we try to respond promptly to all requests. All of the information entered is checked carefully, and access requests are usually processed the same day. In exceptional cases, and if we are experiencing a high volume of requests, a response may be delayed by up to 48 hours.<br>
			<br>
			Once you have been authorized, the requested files will be added to your download cart. You will then be notified by e-mail, and can access your download cart using a direct link. <br>
			<br>
			All personal data provided will be treated as confidential and only used for the intended purpose in accordance with the Daimler AG’s privacy statement:<br>
			<a href="'.asset('/Privacy_Statement?lang=en').'">Privacy Statement</a><br>
			<br>
			Kind regards,<br>',
	'request_access_confirm.footer'				=> 
			'Daimler Communications<br>
			Corporate Design<br>
			Daimler AG<br>
			096-E402<br>
			70546 Stuttgart, Germany<br>
			corporate.design@daimler.com<br>
			<br>
			<br>
			Daimler AG<br>
			Sitz und Registergericht/Domicile and Court of Registry: Stuttgart<br>
			HRB-Nr./Commercial Register No. 19360<br>
			Vorsitzender des Aufsichtsrats/Chairman of the Supervisory Board: Manfred Bischoff<br>
			Vorstand/Board of Management: Ola Källenius (Vorsitzender/Chairman), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm<br>',
	
	
	
	
	'request_access_grant.subject'					=> 'Daimler Brand & Design Navigator – Access Authorization for Your User Account Has Been Granted',
	'request_access_grant.greeting'					=> 'Hello :FIRST_NAME :LAST_NAME,',
	'request_access_grant.download.content' 		=> 
			'After verifying your information, we have granted access authorization for your user account on the "Daimler Brand & Design Navigator" online portal accordingly, and added the requested, non-public materials to your download cart.<br>
			<br>
			Please log in using your valid access data and go to your download cart in order to access the files and be able to download them:<br>
			<a href ="'.asset('/Download_Cart?lang=en').'">Download Cart</a><br>
			<br>		
			Please read the following instructions before using the non-public materials:<br>
			<br>
			The usage right granted by Daimler AG after access is authorized cannot be transferred to third parties. You may not give third parties access to your access details for the "Daimler Brand & Design Navigator" online portal under any circumstances. The joint use of access data is not permissible.<br>
			<br>
			Employees of Daimler AG or a Mercedes-Benz/smart dealer/service partner may not forward or transfer non-public materials (fonts in particular) to external third parties due to the required documentation obligation.<br>
			<br>
			Changes to the predefined position of elements and formatting of templates (including InDesign) or basic settings of fonts (TrueType fonts, OpenType fonts or webfonts) are not permitted. <br>
			<br>
			All common forms of use are permitted without restrictions provided the non-public materials are only used for the purposes of internal projects or in direct connection with the fulfillment of contracts for Daimler AG, its subsidiaries or a Mercedes-Benz/smart dealer/service partner. <br>
			<br>
			Please also note the relevant terms of use on the "Daimler Brand & Design Navigator" online portal:<br>
			<a href="'.asset('/Terms_of_Use?lang=en').'">Tems of Use</a><br>
			<br>	
			All personal data provided will be treated as confidential and only used for the intended purpose in accordance with the Daimler AG’s privacy statement:<br>
			<a href="'.asset('/Privacy_Statement?lang=en').'">Privacy Statement</a><br>
			<br>	
			We are glad to answer any questions you may have regarding the proper handling of non-public materials or offer any other support you may require in this matter.<br>
			<br>
			Kind regards,<br>',
	'request_access_grant.content'					=> 
			'After verifying your information, we have granted access authorization for your user account on the "Daimler Brand & Design Navigator" online portal accordingly.<br>
			<br>
			Please log in using your valid access data and go to the following link in order to access the requested non-public materials:<br>
			<a href ="'.asset('/').':SLUG?lang=en">:TITLE</a><br>
			<br>		
			Please read the following instructions before using the non-public materials:<br>
			<br>
			The usage right granted by Daimler AG after access is authorized cannot be transferred to third parties. You may not give third parties access to your access details for the "Daimler Brand & Design Navigator" online portal under any circumstances. The joint use of access data is not permissible.<br>
			<br>
			Employees of Daimler AG or a Mercedes-Benz/smart dealer/service partner may not forward or share non-public materials to external third parties due to the required documentation obligation. <br>
			<br>
			All common forms of use are permitted without restrictions provided the non-public materials are only used for the purposes of internal projects or in direct connection with the fulfillment of contracts for Daimler AG, its subsidiaries or a Mercedes-Benz/smart dealer/service partner. <br>
			<br>
			Please also note the relevant terms of use on the "Daimler Brand & Design Navigator" online portal:<br>
			<a href="'.asset('/Terms_of_Use?lang=en').'">Tems of Use</a><br>
			<br>	
			All personal data provided will be treated as confidential and only used for the intended purpose in accordance with the Daimler AG’s privacy statement:<br>
			<a href="'.asset('/Privacy_Statement?lang=en').'">Privacy Statement</a><br>
			<br>	
			We are glad to answer any questions you may have regarding the proper handling of non-public materials or offer any other support you may require in this matter.<br>
			<br>
			Kind regards,<br>',
	'request_access_grant.footer'					=>  
			'Daimler Communications<br>
			Corporate Design<br>
			Daimler AG<br>
			096-E402<br>
			70546 Stuttgart, Germany<br>
			corporate.design@daimler.com<br>
			<br>
			<br>
			Daimler AG<br>
			Sitz und Registergericht/Domicile and Court of Registry: Stuttgart<br>
			HRB-Nr./Commercial Register No. 19360<br>
			Vorsitzender des Aufsichtsrats/Chairman of the Supervisory Board: Manfred Bischoff<br>
			Vorstand/Board of Management: Ola Källenius (Vorsitzender/Chairman), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm<br>',
	);
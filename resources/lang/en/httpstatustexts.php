<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| HTTP Status Texts
	|--------------------------------------------------------------------------
	|
	| Localizations for HTTP status texts
	|
	*/

	200 => "OK Success!",
	304 => "Not Modified There was no new data to return.",
	400 => "Bad Request The request was invalid or cannot be otherwise served. An accompanying error message will explain further. In API v1.1, requests without authentication are considered invalid and will yield this response.",
	401 => "Unauthorized Authentication credentials were missing or incorrect.",
	403 => "Forbidden The request is understood, but it has been refused or access is not allowed. An accompanying error message will explain why. This code is used when requests are being denied due to update limits.",
	404 => "Not Found The URI requested is invalid or the resource requested, such as a user, does not exists. Also returned when the requested format is not supported by the requested method.",
	406 => "Not Acceptable Returned by the Search API when an invalid format is specified in the request.",
	410 => "Gone This resource is gone. Used to indicate that an API endpoint has been turned off. For example: \"The REST API v1 will soon stop functioning. Please migrate to API v1.1.\"",
	420 => "Enhance Your Calm Returned by the version 1 Search and Trends APIs when you are being rate limited.",
	422 => "Unprocessable Entity Returned when an image uploaded to POST account/update_profile_banner is unable to be processed.",
	429 => "Too Many Requests Returned in API v1.1 when a request cannot be served due to the application's rate limit having been exhausted for the resource. See Rate Limiting in API v1.1.",
	500 => "Internal Server Error Something is broken. Please post to the group so our team can investigate.",
	502 => "Bad Gateway Server is down or being upgraded.",
	503 => "Service Unavailable The servers are up, but overloaded with requests. Try again later.",
	504 => "Gateway timeout The servers are up, but the request couldn't be serviced due to some failure within our stack. Try again later.",

);

<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" 			=> "Invalid password.",

	"user"     			=> "The username you entered can not be found. Please enter a valid user name, and then click the \"Send\" button.</br></br></br>",

	"token" 			=> "This password reset token is invalid.",

    'email.subject'     => 'Daimler Brand & Design Navigator – Now You Can Change Your Password',
	'email.greeting'	=> 'Hello :FIRST_NAME :LAST_NAME,',
    'email.content'     => 'We have received a password reset request for your user account on the Daimler Brand & Design Navigator online portal.<br>
                            <br>
                            Please follow the link provided below to choose a new password:<br>
                            <a href=":CONFIRMATION_LINK">I want to reset my password</a><br>
                            <br>
                            This link will automatically expire 15 minutes after this e-mail was sent.<br>
                            <br>
                            If you are not the intended addressee, please inform us immediately that you have received this e-mail by mistake and delete it. We thank you for your support.<br>
                            <br>
                            Kind regards,<br>',
    'email.footer'      => 'Daimler Communications<br>
                            Corporate Design<br>
                            Daimler AG<br>
                            096-E402<br>
                            70546 Stuttgart, Germany<br>
                            corporate.design@daimler.com<br>
                            <br>
                            <br>
                            Daimler AG<br>
                            Sitz und Registergericht/Domicile and Court of Registry: Stuttgart<br>
                            HRB-Nr./Commercial Register No. 19360<br>
                            Vorsitzender des Aufsichtsrats/Chairman of the Supervisory Board: Manfred Bischoff<br>
                            Vorstand/Board of Management: Ola Källenius (Vorsitzender/Chairman), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm<br>',

);
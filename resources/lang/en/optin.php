<?php

return array(
    /*
    |--------------------------------------------------------------------------
    | Newsletter Opt-in email message Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

'subject'               => 'Daimler Brand & Design Navigator – Your Newsletter Subscription Needs to Be Confirmed',
'greeting'              => 'Hello :FIRST_NAME :LAST_NAME,',
'content'               => 'You received this e-mail because you have subscribed (opted-in) to the Daimler Brand & Design Navigator newsletter.<br>
                            <br>
                            Please confirm your subscription by clicking the link below:<br>
                            <a href=":CONFIRMATION_LINK">I subscribe to the newsletter</a><br>
                            <br>
                            This link will automatically expire 24 hours after this e-mail was sent.<br>
                            <br>
                            If you are not the intended addressee, please inform us immediately that you have received this e-mail by mistake and delete it. We thank you for your support.<br>
                            <br>
                            Kind regards,<br>',
'footer'                => 'Daimler Communications<br>
                            Corporate Design<br>
                            Daimler AG<br>
                            096-E402<br>
                            70546 Stuttgart, Germany<br>
                            corporate.design@daimler.com<br>
                            <br>
                            <br>
                            Daimler AG<br>
                            Sitz und Registergericht/Domicile and Court of Registry: Stuttgart<br>
                            HRB-Nr./Commercial Register No. 19360<br>
                            Vorsitzender des Aufsichtsrats/Chairman of the Supervisory Board: Manfred Bischoff<br>
                            Vorstand/Board of Management: Ola Källenius (Vorsitzender/Chairman), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm<br>',

'success_title' 		=> 'You have successfully subscribed to the newsletter.',
'success_subheading'    => 'To unsubscribe, change the settings in your user profile and uncheck the „Subscribe to newsletter“ checkbox.',
'success_message' 		=> 'Still have questions about user account? <br>
                            Then send an e-mail to <a class="contactlink" href="mailto:corporate.design@daimler.com" target="_top">corporate.design@daimler.com</a> and we will help you shortly.',
'failure_title' 		=> 'The newsletter subscription has failed.',
'failure_subheading'    => 'Unfortunately, this newsletter subscription token is no longer valid. To subscribe again, please change the settings in your user account and check the „Subscribe to newsletter“ checkbox.',
'failure_message' 		=> 'Still have questions about your registration or user account?<br>
                            <br>
                            Then send an e-mail to <a class="contactlink" href="mailto:corporate.design@daimler.com" target="_top">corporate.design@daimler.com</a> and we will help you shortly.',
'failure_not_active'    => "Please activate your user account before you subscribe to the newsletter. You should already have received an additional notification including an activation link.",
'no_user_title'         => 'Your newsletter subscription has been already cancelled.',
'no_user_subheading'    => 'Unfortunately, this unsubscribe link is not valid or has been used previously.',
'no_user_message'       => 'Did you cancel your subscription, but want to subscribe again?</br>
                            To subscribe again, please change the settings in your user account and check the „Subscribe to newsletter“ checkbox.</br>
                            </br>
                            Still have questions about your registration or user account?</br>
                            Then send an e-mail to <a href="mailto:corporate.design@daimler.com">corporate.design@daimler.com</a> and we will help you shortly.',
);
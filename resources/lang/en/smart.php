<?php

return array(
    
    'login'						=> 'Log in',
	'registration'				=> 'Sign up',
    
    'provider'					=> 'Provider',
	'notices'					=> 'Legal notice',
	'cookies'					=> 'Cookies',
	'privacy.header'			=> 'Provider/Privacy',
	'privacy.footer'			=> 'Privacy statement',
	'terms'						=> 'Terms of use',
	'commentterms'				=> 'Comment terms and conditions',
    
    'design_manuals_downloads'  => '‡ Design manuals and downloads.',
    
    'faq.header'				=> '‡ Frequently asked questions (FAQ).',

);

<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Dialog Page Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/


	'path'						=> 'Search Results',
	'page.title'				=> 'Search Results for',
	'note'						=> 'The search results are sorted based on their relevance.',
	'for1'						=> 'Search for :',
	'for2'						=> '',
	'for3'						=> ' matches',
	'advanced'					=> 'Advanced Search',
	'advanced.title'			=> 'Advanced Search.',
	'advanced.note'				=> 'Advanced search will find results that contain the exact phrase as entered.',
	'advanced.enter'			=> 'Enter search term(s):',
	'advanced.categories'		=> 'Search in these categories:',
	'advanced.brand'			=> 'Brand',
	'advanced.company'			=> 'Company',
	'advanced.all'				=> 'All',
	'advanced.from'				=> 'From:',
	'advanced.to'				=> 'To:',
	'advanced.search'			=> 'Search',
	);
<!DOCTYPE html>
<html>
<head>
<title>Daimler Brand &amp; Design Navigator</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="viewport" content="width=1200">
    <meta name="MobileOptimized" content="1200">
    <link rel="stylesheet" href="{!! mix('/css/all.print.css') !!}"  type="text/css" />
	@if(isset($PAGE->brand) && $PAGE->brand == SMART)
		<link rel="stylesheet" href="{!! mix('/css/styleSmart.css') !!}" />
		<link rel="stylesheet" href="{!! mix('/css/styleSmartPrint.css') !!}" />
	@endif
	<script>
		var paths = {
			'images': '{!! asset('') !!}',
			'gallery': '{!! asset('gallery') !!}'
		}
        var relPath  = '<?php echo asset(""); ?>';
	</script>
    <script src="{!! mix('js/all.print.js') !!}" type="text/javascript"></script>


	 <link rel="SHORTCUT ICON" href="{!! asset('/favicon48.ico') !!}" type="image/x-icon"/>
	<link rel="icon" href="{!! asset('/DAI_Favicon_16.png')!!}" sizes="16x16" type="image/png"/>
	<link rel="icon" href="{!! asset('/DAI_Favicon_32.png') !!}" sizes="32x32" type="image/png">
	<link rel="icon" href="{!! asset('/DAI_Favicon_96.png')!!}" sizes="96x96" type="image/png"/>
	<link rel="icon" href="{!! asset('/DAI_Favicon_196.png')!!}" sizes="196x196" type="image/png"/>
	<link rel="icon" href="{!! asset('/DAI_Favicon_160.png')!!}" sizes="160x160" type="image/png"/>

	<link rel="apple-touch-icon" href="{!! asset('/DAI_Favicon_57.png')!!}" sizes="57x57"/>
	<link rel="apple-touch-icon" href="{!! asset('/DAI_Favicon_72.png')!!}" sizes="72x72"/>
</head>
<body bgcolor="#FFFFFF" text="#000000"
	  leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" rightmargin="0" bottommargin="0"
	  class="{!! ($PAGE->brand == SMART ? 'smart' : 'basic') !!}">
	<input type="hidden" name="_token" id="token" value="{!! csrf_token() !!}">
	<div class="container-pdf">
		<div class="header row">
			<div class="column">
				@if(isset($PAGE->brand) && $PAGE->brand == SMART)
					<img class="header_image" src="{!! asset('/img/template/nav/smart.png')!!}" />
				@else
					<img class="header_image" src="{!! asset('/img/template/nav/daimler_logotype_black.png')!!}" />
				@endif
			</div>
			<div class="column left">
				<p class="pdf-header">@lang('template.print.header')</p>
			</div>
		</div>
		<section class="pdf-body">
			<div class="date-and-rating">
				<p class="date">
					@if( $PAGE->template == 'downloads' )
                        @if(count($DOWNLOAD_FILES)>0)
                        @if(Session::get('lang') == LANG_EN){!!$DOWNLOAD_FILES->first()->file->updated_at->formatLocalized('%B %d, %Y')!!}@else{!!$DOWNLOAD_FILES->first()->file->updated_at->formatLocalized('%d. %B %Y')!!}@endif
                        @endif
					@else
						@if(Session::get('lang') == LANG_EN){!!$PAGE->updated_at->formatLocalized('%B %d, %Y')!!}@else{!!$PAGE->updated_at->formatLocalized('%d. %B %Y')!!} @endif

					@endif
				</p>
				<div class="stats">
					@if($PAGE->ratings()->count() > 0)<span class="rate"><img src="{!! asset('/img/template/svg/rate-black.svg') !!}" />{!!$PAGE->ratings()->count()!!}</span>@endif
					@if($PAGE->activeComments()->count() > 0)<span class="comments"><img src="{!! asset('/img/template/svg/comments-black.svg') !!}" />{!!$PAGE->activeComments()->count()!!}</span>@endif
					<span class="views"><img src="{!! asset('/img/template/svg/views-black.svg') !!}" />{!!$PAGE->visits!!}</span>
				</div>
			</div>
			<div style="clear:both;">
				@if($PAGE->template != 'downloads' && $PAGE->template != 'basic' && $PAGE->template != 'exposed')
					@if($PAGE->joinTags())
					<div class="pdf-tags"><img src="{!! asset('/img/template/svg/tags.svg') !!}" /><span>{!!$PAGE->joinTags()!!}</span></div>
					@endif
				@endif
			</div>

			@section('main')
				<div class="middle_page_text">
					<div id="content_main">
						@if(isset($CONTENTS['main']))
                            {!! $CONTENTS['main'] !!}
						@endif
						@if( $PAGE->template == 'downloads' )
							@include('partials.download_list')
						@endif
					</div>
				</div>
			@show
			<div id="printDiv"></div>
		</section>
		<section class="print-footer">
			<div>
				<p>@lang('template.print.footer')</p>
			</div>
		</section>
	</div>
</body>
</html>
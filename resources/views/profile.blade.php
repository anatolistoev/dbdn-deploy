@extends('layouts.base')
@section('bodyClass'){!! 'basic' !!}@stop

@section('left_nav')

@stop
@section('main')
<link rel="stylesheet" href="{!! mix('/css/chosen/chosen.css') !!}" />
<script type="text/javascript" src="{!! mix('/js/chosen/chosen.jquery.js') !!}"></script>
<script type="text/javascript" src="{!! mix('/js/chosen/chosen.proto.js') !!}"></script>
<section>
    @include('partials.back_button')
</section>
<section>
    <div class="user_profile_fields">
        <h1>@lang('template.'.$PAGE->id.'.page.title')</h1>
        <h1><small>@lang('profile.'.$PAGE->id.'.subheading')</small></h1>
        <p class="note">@lang('profile.pwd.rules')</p>

        {!! Form::model($USER, array('url'=> $PAGE->id, 'id'=>'profileForm')) !!}
        @if(!isset($USER->id))
            <p class="subline">@lang('profile.profile_title')</p>
        @else
            <p class="subline">@lang('profile.logged_profile_title', array('username'=> $USER->username))</p>
        @endif
        <section>
            <div class="whole_line ">
                @if(!isset($USER->id))
                <span class="input input--yoshiko">
                    {!! Form::text('username', null, array('autocomplete'=>'off','class'=> ($errors->first('username') ? 'error ' : '' ).'input__field input__field--yoshiko'))!!}
                    <label class="input__label input__label--yoshiko" for="username">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.username')">@lang('profile.username')</span>
                    </label>
                </span>
                @endif
            </div>
        </section>
        <section>
            <div class="whole_line">
                <span class="input input--yoshiko">
                    {!! Form::text('email', $USER->email, array('onchange' => 'if(this.value.toLowerCase().indexOf(\'daimler.com\') != -1)$(\'#newsletter\').prop(\'checked\', false);  $(\'#newsletterDiv\').toggle(this.value.toLowerCase().indexOf(\'daimler.com\') == -1);', 'class'=> ($errors->first('email') ? 'error ' : '' ).'input__field input__field--yoshiko')) !!}
                    <label class="input__label input__label--yoshiko" for="email">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.email')">@lang('profile.email')</span>
                    </label>
                </span>
            </div>
        </section>
        @if(!isset($USER->id))
        <section class="cf">
            <div class="label_left">
                <span class="input input--yoshiko" style="margin-bottom: 0;">
                    {!! Form::input('password', 'password', ( substr($USER->password, 0, 3) != '$P$' || strlen($USER->password) != 34 ? $USER->password : ''), array('autocomplete'=>'off', 'class'=> ($errors->first('password') ? 'error ' : '' ).'input__field input__field--yoshiko')) !!}
                    <label class="input__label input__label--yoshiko" for="password">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.password')">@lang('profile.password')</span>
                    </label>
                </span>
            </div>
            <div class="label_right">
                <span class="input input--yoshiko" style="margin-bottom: 0;">
                    {!! Form::input('password', 'password_confirmation', Request::get('password_confirmation'), array('autocomplete'=>'off', 'class'=> ($errors->first('password_confirmation') ? 'error ' : '' ).'input__field input__field--yoshiko')) !!}
                    <label class="input__label input__label--yoshiko" for="password_confirmation">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.confirm')">@lang('profile.confirm')</span>
                    </label>
                </span>

            </div>
            <div class="input input--yoshiko password_rule" style="margin-top: 10px;">
                @lang('profile.profile.faq')
            </div>
        </section>
        @endif
        <p class="subline">@lang('profile.data_title')</p>
        <section class="cf">
            <div class="label_left">
                <span class="input input--yoshiko">
                    {!! Form::text('first_name', null, array('autocomplete'=>'off', 'class'=> ($errors->first('first_name') ? 'error ' : '' ).'input__field input__field--yoshiko')) !!}
                    <label class="input__label input__label--yoshiko" for="first_name">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.firstname')">@lang('profile.firstname')</span>
                    </label>
                </span>
            </div>
            <div class="label_right">
                <span class="input input--yoshiko">
                    {!! Form::text('last_name', null, array('autocomplete'=>'off', 'class'=> ($errors->first('last_name') ? 'error ' : '' ).'input__field input__field--yoshiko')) !!}
                    <label class="input__label input__label--yoshiko" for="last_name">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.lastname')">@lang('profile.lastname')</span>
                    </label>
                </span>
            </div>
        </section>
        <section>
            <div class="whole_line">
                <span class="input input--yoshiko">
                    {!! Form::text('company', null, array('class'=> ($errors->first('company') ? 'error ' : '' ).'input__field input__field--yoshiko')) !!}
                    <label class="input__label input__label--yoshiko" for="company">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.company')">@lang('profile.company')</span>
                    </label>
                </span>
            </div>
        </section>
        <section class="cf">
            <div class="label_left">
                <span class="input input--yoshiko">
                    {!! Form::text('department', null, array('class'=> ($errors->first('department') ? 'error ' : '' ).'input__field input__field--yoshiko')) !!}
                    <label class="input__label input__label--yoshiko" for="department">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.department')">@lang('profile.department')</span>
                    </label>
                </span>
            </div>
            <div class="label_right">
                <span class="input input--yoshiko">
                    {!! Form::text('position', null, array('class'=> ($errors->first('position') ? 'error ' : '' ).'input__field input__field--yoshiko')) !!}
                    <label class="input__label input__label--yoshiko" for="position">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.position')">@lang('profile.position')</span>
                    </label>
                </span>
            </div>
        </section>
        <p class="subline">@lang('profile.contact_title')</p>
        <section>
            <div class="whole_line">
                <span class="input input--yoshiko">
                    {!! Form::text('address', null, array('class'=> ($errors->first('address') ? 'error ' : '' ).'input__field input__field--yoshiko')) !!}
                    <label class="input__label input__label--yoshiko" for="address">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.address')">@lang('profile.address')</span>
                    </label>
                </span>
            </div>
        </section>
        <section class="cf">
            <div class="label_left">
                <span class="input input--yoshiko">
                    {!! Form::text('code', null, array('class'=> ($errors->first('code') ? 'error ' : '' ).'input__field input__field--yoshiko')) !!}
                    <label class="input__label input__label--yoshiko" for="code">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.code')">@lang('profile.code')</span>
                    </label>
                </span>
            </div>
            <div class="label_right">
                <span class="input input--yoshiko">
                    {!! Form::text('city', null, array('class'=> ($errors->first('city') ? 'error ' : '' ).'input__field input__field--yoshiko')) !!}
                    <label class="input__label input__label--yoshiko" for="city">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.city')">@lang('profile.city')</span>
                    </label>
                </span>
            </div>

        </section>
        <section>
             <div class="whole_line">
                <span class="input input--yoshiko country_wrapper">
                    <label class="input__label input__label--yoshiko" for="country">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.country')">@lang('profile.country')</span>
                    </label>
                    {!! Form::select('country', Lang::get('profile.countries'), null, array('style' => (Form::getValueAttribute('country', null) == '' ? 'color:#757575;' : 'color:#000000;'),'',
							'data-placeholder'=> trans("profile.placeholder"),'class'=> ($errors->first('country') ? 'error ' : '' ).'input__field input__field--yoshiko')) !!}
                    <script type="text/javascript"> $('select').chosen({no_results_text: "@lang('profile.no_results')"});</script>
                </span>
             </div>
        </section>
        <section class="cf">
            <div class="label_left">
                <span class="input input--yoshiko">
                    {!! Form::text('phone', null, array('class'=> ($errors->first('phone') ? 'error ' : '' ).'input__field input__field--yoshiko')) !!}
                    <label class="input__label input__label--yoshiko" for="phone">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.phone')">@lang('profile.phone')</span>
                    </label>
                </span>
            </div>
            <div class="label_right">
                <span class="input input--yoshiko">
                    {!! Form::text('fax', null, array('class'=> ($errors->first('fax') ? 'error ' : '' ).'input__field input__field--yoshiko')) !!}
                    <label class="input__label input__label--yoshiko" for="fax">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.fax')">@lang('profile.fax')</span>
                    </label>
                </span>
            </div>
        </section>
        <section class="more_info" style="margin-top: 0;">
            <div id="newsletterDiv" class="accept_dialog" style="clear:both;{!! (((preg_match('/@(?!(?:[a-zA-Z0-9]+(?:-[a-zA-Z0-9]+)*\.)*daimler\.com$)/mi', $USER->email)&& Request::old('email') == null) || (Request::old('email')!== null && preg_match('/@(?!(?:[a-zA-Z0-9]+(?:-[a-zA-Z0-9]+)*\.)*daimler\.com$)/mi', Request::old('email'))))) ? '':'display:none;' !!}">
                <p class="subline">@lang('profile.newsletter')</p>
                <p>@lang('profile.subscription.text')</p>
                <p>
		{!! Form::checkbox('newsletter', 1, null, [
			"class" => "chb",
			"id"	=> "newsletter"
		]) !!}
                    <!-- <input id="newsletter" class="chb" type="checkbox" value="1" name="newsletter"<?php

                         //if (Request::old('newsletter') !== null) {
                         //    if(Request::old('newsletter') == 1) {
                         //        echo('checked');
                         //    }
                         //} else {
                         //    if($USER->newsletter_include == 1) {
                         //       echo('checked');
                         //    }
                        // }
                     ?>> -->
                           <!--							{!! Form::checkbox('newsletter_include', 1, Request::old('newsletter'), array('id'=>'newsletter', 'class' => 'chb', 'name'=>'newsletter')) !!}-->
                           {!! Form::label('newsletter', Lang::get('profile.subscription.text2') ) !!}
                </p>
                <p>{!! Lang::get('profile.subscription.text3') !!}</p>
            </div>
        </section>

        {{--
        @if(isset($USER->id))
            <section class="gray_info">
                <div class="accept_dialog">
                    <p class="subline">@lang('gdpr.request.boxheader')</p>
                    <p>@lang('gdpr.request.boxtext')</p>
                    <p class="checkboxp">
                        <input id="gdprerase" class="chb" type="checkbox" value="1" name="gdprerase">

                        {!! Form::label('gdprerase', Lang::get('gdpr.request.boxchbx') ) !!}
                    </p>
                    {!! Lang::get('gdpr.request.faqtext') !!}
                </div>
            </section>
        @endif
        --}}
        
        @if(isset($USER->id))
        <p>@lang('profile.password_note')&nbsp;</p>
        
        <section class="cf">
            <div class="label_left">
                <span class="input input--yoshiko password" style="margin-bottom: 0;">
                    {!! Form::input('password', 'password', ( substr($USER->password, 0, 3) != '$P$' || strlen($USER->password) != 34 ? $USER->password : ''), array('autocomplete'=>'off', 'class'=> ($errors->first('password') ? 'error ' : '' ).'input__field input__field--yoshiko')) !!}
                    <label class="input__label input__label--yoshiko" for="password">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.password')">@lang('profile.password')</span>
                    </label>
                </span>
            </div>
            <div class="input input--yoshiko password_rule" style="margin-top: 10px;">
                @lang('profile.password_change')
            </div>
        </section>
        @endif
        @if(!isset($USER->id))
        <section>
            <div class="accept_dialog">
                <p class="copy-text">@lang('profile.legal')</p>
                <p >@lang('profile.legal.text')</p>
                <p style="padding-bottom: 0">
                    <input id="iagree" class="chb" type="checkbox" name="agreed" {!! Request::old('agreed') ? 'checked' : '' !!} />
                    <label for="iagree">@lang('profile.iagree')</label>
                </p>
            </div>
        </section>
        @endif
        <section class="cf profile_captcha">
            <div class="label_left">
                <img class="captcha_img" src="{!! asset('captcha'). '?brand=' . ((isset($PAGE->brand) && $PAGE->brand == SMART) ?'smart':'daimler') !!}">
                <div class="reset_captcha"></div>
            </div>
            <div class="label_left">
                <span class="input input--yoshiko">
                    <input autocomplete="off" type="text" name="captcha" class="input__field input__field--yoshiko @if($errors->first('captcha'))error @endif">
                    <label class="input__label input__label--yoshiko" for="captcha">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.enter.code')">@lang('profile.enter.code')</span>
                    </label>
                </span>
            </div>
        </section>
        <section class="more_info">
            <input class="submit_button" type="submit" value="@lang('profile.send')">
        </section>
        {!! Form::close() !!}
    </div>
</section><!-- main -->
<script type="text/javascript" src="{!! mix('/js/classie.js') !!}"></script>
@stop


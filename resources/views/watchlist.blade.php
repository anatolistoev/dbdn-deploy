@extends('basic')
@section('bullet_nav')
@stop
@section('left_nav')

@stop
@section('main')
<section>
    @include('partials.back_button')
</section>
	<section>
		<div class="user_download_fields">
			<section class="h1 cf">
				<h1>@lang('system.watchlist.page.title') @if(isset($SUBSCRIPTIONS) && count($SUBSCRIPTIONS)>0)[{!!count($SUBSCRIPTIONS)!!}]@endif</h1>
                @if(isset($SUBSCRIPTIONS) && count($SUBSCRIPTIONS)>0)
                    <?php $listView = false; ?>
                    @include('partials.view_controls')
				@endif
			</section>

			<section>
				<p class="p">@lang('system.watchlist.contents')</p>
			</section>
		</div>
		<div class="download_elements watchlist">
			@if(!isset($SUBSCRIPTIONS) || count($SUBSCRIPTIONS)==0)
            <section><p class="empty_list">@lang('system.empty_list')</p></section>
			@else
				<script>
					function checkToggle() {
						var cboxes = $('.manuals input[type=checkbox]');
						cboxes.prop('checked', !cboxes.prop('checked'));
					}
					function removeSelected() {
						var $form = $('#watchlistForm');
						if($form.find('input[type=checkbox]:checked').length > 0){
							$form.find('input[name="_method"]').val('delete');
							$form.attr('target', '_self');
							$form.submit();
						}else{
							msg('@lang('system.downloads_list.alert')')
						}
					}
					function  removeSubscription(id) {
						var $form = $('#watchlistForm');
						$form.find('input[name=pageid]').val(id);
						$form.submit();
					}
				</script>

				<section>
                    <div class="select_all" style="margin-top: 37px">
						<input id="selecctall" class="chb" type="checkbox" />
						<label for="selecctall" onclick="checkToggle();">@lang('system.downloads_list.selectall')</label>
					</div>
                    <div class="download_selected_2" style="margin-top: 39px;">
						<span class="remove_selected" onclick="removeSelected();">@lang('system.downloads_list.removeSelected')</span>
					</div>
				</section>
                <section>
                    <div class="row">
                        <div style="clear:both;"></div>
                    </div>
                    {!! Form::open(array('url' => 'Watchlist', 'id' => 'watchlistForm', 'method'=>'delete')) !!}
                    {!! Form::hidden('pageid', 0) !!}
                    @if($count = 0)
                    @endif
                    <div class="grid-page grid-3cols manuals cf">
                        <?php $i=0?>
                        @foreach($SUBSCRIPTIONS as $key=>$block)
                                <div class="page" style='margin-bottom:0px;'>
                                    <a class="page_link" href="{!!$block['slug']!!}">
                                        <div class="image story-layover">
                                        @if(strtolower(pathinfo($block['img'], PATHINFO_EXTENSION)) == 'svg')
                                            {!!\App\Helpers\FileHelper::getSVGContent(public_path().$block['img']);!!}
                                        @else
                                            {!!\App\Helpers\HtmlHelper::generateImg($block['img'], $block['type'], 'watchlist', isset($block['imgext'])? $block['imgext'] : \App\Helpers\FileHelper::getFileExtension($block['img']), isset($block['thumb_visible']) )!!}
                                        @endif

                                        @if(isset($block['protected']) && $block['protected'])
                                            <div class="protected" title='@lang('template.protected')'></div>
                                        @endif
                                        </div>
                                        <div class="title-chb">
                                            <input type="checkbox" id="page_{!! $key !!}" class="chb checkbox1 " value="{!! $key !!}" name="pageid[]" autocomplete="off">
                                            <label for="page_{!! $key !!}"><span>{!!\App\Helpers\HtmlHelper::fixDoubleDaggerForDaimler($block['title'])!!}</span></label>
                                        </div>
                                        <div class="list-hover cf">
                                            <div onclick='window.location.href="{!! url($block['slug']) !!}"' class="file_page">
                                                <div class="icon"></div>
                                                <span>@lang('system.download_overview.goTo')</span>
                                            </div>
                                            <div class="file_date">@if(Session::get('lang') == LANG_EN){!!$block['date']->formatLocalized('%B %d, %Y')!!}@else{!!$block['date']->formatLocalized('%d. %B %Y')!!} @endif</div>
                                            @if($block['tags'] && $block['template'] != 'basic')
                                                <div class="file_tags">{!!$block['tags']!!}</div>
                                            @endif
                                        </div>
                                        <div class="date">@if(Session::get('lang') == LANG_EN){!!$block['date']->formatLocalized('%B %d, %Y')!!}@else{!!$block['date']->formatLocalized('%d. %B %Y')!!} @endif</div>
                                        @if($block['tags'] && $block['template'] != 'basic')
                                        <div class="tags">{!!$block['tags']!!}</div>
                                        @endif
                                        <div style="clear: both;"></div>
                                    </a>
                                </div>
                        <?php $i++?>
                        @if($i % 3 == 0 && $block != end($SUBSCRIPTIONS))
                             <div class="cf"></div>
                        @endif
                        @endforeach
                    </div>
                    {!! Form::close() !!}
                </section>
				<section>
					<div class="select_all">
						<input id="selecctall" class="chb" type="checkbox" />
						<label for="selecctall" onclick="checkToggle();">@lang('system.downloads_list.selectall')</label>
					</div>
                    <div class="download_selected_2" style="margin-bottom: 60px">
						<span class="remove_selected" onclick="removeSelected();">@lang('system.downloads_list.removeSelected')</span>
					</div>
				</section>
			@endif
		</div><!-- main -->
        <div style="clear:both;"></div>
	</section>
@stop
@section('right')

@stop
@section('farright')

@stop




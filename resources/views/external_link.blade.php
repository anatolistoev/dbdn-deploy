@extends('layouts.base')
@section('bodyClass'){!! 'basic' !!}@stop

@section('main')
	<section>
		@include('partials.back_button')
	</section>
	<section>
		<div class="user_profile_fields">
			@lang('external_link.text')
			<p><a id="extLink" class="arrowlink" href="{!! $URL !!}" target="_blank">@lang('external_link.continue')&nbsp;{!! $URL !!}</a></p>


		</div><!-- main -->
	</section>
@stop


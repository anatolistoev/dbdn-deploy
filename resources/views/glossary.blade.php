@extends('layouts.base')
@section('bodyClass'){!! 'basic' !!}@stop
@section('main')
	<div class="glossary-body">
		<section>
			<span class="title-of-glossary">@lang('template.glossaryPageTitle')</span>
			<div class="glossary-navigation">
				@foreach($LETTERS as $letter)
					<a href="{!!asset('glossary/'. $letter->letter) !!}" >
						<div class="{!! isset($TERMS[0],$TERMS[0]->name) && $TERMS[0]->name[0] == $letter->letter ? 'active':''!!}">
							<p class="index-top-right">{!! $letter->occurences !!}</p>
							<br />
							<p class="letter">{!! $letter->letter !!}</p>
						</div>
					</a>
				@endforeach
			</div>
		</section>
		@if(isset($TERMS))
			<section>
				<div class="glossary-result">
					<ul>
						@foreach($TERMS as $i=>$word)
							<li>
								<p>{!! $word->name !!}</p> 
								<span>{!!$word->description!!}</span>
							</li>	
						@endforeach
					</ul>
					
				</div>
			</section>	
		@endif
	</div>
@stop
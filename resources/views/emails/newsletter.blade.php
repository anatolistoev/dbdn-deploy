<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <title>{!!$title!!}</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<style type="text/css">
			@font-face {
				font-family: 'DaimlerCSRegular';
				src: url('{!!$font_base!!}DaimlerCS-Regular.eot');
				src: url('{!!$font_base!!}DaimlerCS-Regular.eot?#iefix') format('embedded-opentype'),
				url('{!!$font_base!!}DaimlerCS-Regular.svg#wf') format('svg'),
				url('{!!$font_base!!}DaimlerCS-Regular.woff2') format('woff2'),
				url('{!!$font_base!!}DaimlerCS-Regular.woff') format('woff'),
				url('{!!$font_base!!}DaimlerCS-Regular.ttf') format('truetype');
				font-weight:normal;
				font-style: normal;
				mso-font-alt: Arial, Helvetica, sans-serif;
			}
			@font-face {
				font-family: 'DaimlerCSBold';
				src: url('{!!$font_base!!}DaimlerCS-Bold.eot');
				src: url('{!!$font_base!!}DaimlerCS-Bold.eot?#iefix') format('embedded-opentype'),
				url('{!!$font_base!!}DaimlerCS-Bold.svg#wf') format('svg'),
				url('{!!$font_base!!}DaimlerCS-Bold.woff2') format('woff2'),
				url('{!!$font_base!!}DaimlerCS-Bold.woff') format('woff'),
				url('{!!$font_base!!}DaimlerCS-Bold.ttf') format('truetype');
				font-weight:bold;
				font-style: normal;
				mso-font-alt: Arial, Helvetica, sans-serif;
			}
			@font-face {
				font-family: 'DaimlerCARegular';
				src: url('{!!$font_base!!}DaimlerCA-Regular.eot');
				src: url('{!!$font_base!!}DaimlerCA-Regular.eot?#iefix') format('embedded-opentype'),
				url('{!!$font_base!!}DaimlerCA-Regular.svg#wf') format('svg'),
				url('{!!$font_base!!}DaimlerCA-Regular.woff2') format('woff2'),
				url('{!!$font_base!!}DaimlerCA-Regular.woff') format('woff'),
				url('{!!$font_base!!}DaimlerCA-Regular.ttf') format('truetype');
				font-weight:normal;
				font-style: normal;
				mso-font-alt: Arial, Helvetica, sans-serif;
			}
			{
				webkit-font-smoothing: antialiased;
			}
			* {-ms-text-size-adjust:100%; -webkit-text-size-adjust: none; -webkit-text-resize: 100%; text-resize: 100%;}
			img {border: 0 !important; outline: none !important;}
			table {border-collapse: collapse; mso-table-lspace: 0px; mso-table-rspace: 0px;}
			td, a, span {border-collapse: collapse !important; mso-line-height-rule: exactly !important;	}
			p{margin:0;}
			a{outline:none; text-decoration:none; color:#00677f;}
			a:hover{text-decoration:underline !important;}
			.active a:hover {text-decoration:none !important;}
			.active:hover{opacity:0.8;}
			.active{
				-webkit-transition:all 0.3s ease;
				-moz-transition:all 0.3s ease;
				-ms-transition:all 0.3s ease;
				transition:all 0.3s ease;
			}@media screen
			{
 			.webfont14
 			{
    		font: 14px "DaimlerCSRegular", Arial !important;
			line-height: 20px !important;
			font-weight: normal !important;
 			}
			.webfont16
 			{
    		font: 16px "DaimlerCSRegular", Arial !important;
			line-height: 22px !important;
			font-weight: normal !important;
			}
			.webfont18-30
 			{
			font: 18px "DaimlerCSRegular", Arial !important;
			line-height: 30px !important;
			font-weight: normal !important;
    		}
			.webfont18-26
 			{
			font: 18px "DaimlerCSRegular", Arial !important;
			line-height: 26px !important;
			font-weight: normal !important;
    		}
			.webfont_18-26
 			{
			font: 18px "DaimlerCARegular", Arial !important;
			line-height: 26px !important;
			font-weight: normal !important;
    		}
			.webfont14-14
 			{
			font: 14px "DaimlerCSRegular", Arial !important;
			line-height: 14px !important;
			font-weight: normal !important;
			}
			.webfont36
 			{
			font: 36px "DaimlerCSRegular", Arial !important;
			line-height: 42px !important;
			font-weight: normal !important;
			 }
			 .webfont24
 			 {
			font: 24px "DaimlerCSRegular", Arial !important;
			line-height: 30px !important;
			font-weight: normal !important;
			}
			.webfont14-16
 			{
			font: 14px "DaimlerCSRegular", Arial !important;
			line-height: 16px !important;
			font-weight: normal !important;
    		}
			.ul
			{
			font: 16px "DaimlerCSRegular", Arial !important;
			line-height: 22px !important;
			font-weight: normal !important;
			}
			a
			{
			outline:none;
			text-decoration:none !important;
			color:#00677f;
			}
			a:hover
			{
			text-decoration:none !important;
			}
			a[x-apple-data-detectors]{
			color: inherit !important;
			text-decoration: none !important;
			font-size: inherit !important;
			font-family: inherit !important;
			font-weight: inherit !important;
			line-height: inherit !important;
			}
			table td {border-collapse: collapse !important;}
			.ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div
			{
			line-height:inherit;
			}
			p, br, table td, div, span {
			font-weight: normal !important;
    		font-family: "DaimlerCSRegular", Arial !important;
			}
			strong, b {
			font-weight: bold !important;
    		font-family: "DaimlerCSBold", Arial !important;
			}
			span.MsoHyperlink {
			mso-style-priority:99;
			color:inherit;
			}
			span.MsoHyperlinkFollowed {
			mso-style-priority:99;
			color:inherit;
			}
			strong, b {
			    font-weight: bold !important;
			    font-family: "DaimlerCSBold", Arial !important;
			}
			a[x-apple-data-detectors]{color: inherit !important; text-decoration: none !important;}
			a img{border:none;}
			table td{mso-line-height-rule:exactly;}
			.ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}
			@media only screen and (max-width:479px) {
				/*default style*/
				table[class="flexible"]{width:100% !important;}
				*[class="hide"]{display:none !important; width:0 !important; height:0 !important; padding:0 !important; font-size:0 !important; line-height:0 !important;}
				span[class="db"]{display:block !important;}
				td[class="img-flex"] img{width:100% !important; height:auto !important;}
				td[class="aligncenter"]{text-align:center !important;}
				tr[class="table-holder"]{display:table !important; width:100% !important;}
				th[class="thead"]{display:table-header-group !important; width:100% !important;}
				th[class="trow"]{display:table-row !important; width:100% !important;}
				th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}
				th[class="flex"]{display:block !important; width:100% !important;}
				/*custom style*/
				td[class="img-flex"]{display:table-cell !important;}
				td[class="holder-01"]{padding:41px 10px 0 20px !important;}
				td[class="holder-01-2"]{padding:41px 10px 0 0 !important;}
				td[class="holder-02"]{padding:11px 18px 26px !important;}
                td[class="holder-03"]{padding:11px 18px 26px !important;}
				td[class="footer"]{padding:30px 20px !important;}
				td[class="indent-top-25"]{padding-top:25px !important;}
				td[class="indent-top-32"]{padding-top:32px !important;}
				td[class="indent-top-57"]{padding-top:57px !important;}
				td[class="indent-bottom-23"]{padding-bottom:23px !important;}
				td[class="indent-bottom-32"]{padding-bottom:32px !important;}
				td[class="indent-width-16"]{padding-left:20px !important; padding-right:16px !important; font-size:14px !important; line-height:16px !important;}
				td[class="h-auto"]{height:auto !important;}
			}
			@media only screen and (min-width : 321px) and (max-width : 479px) {
			/*default style*/
			table[class="wrapper"]{min-width:320px !important;}
			table[class="flexible"]{width:100% !important;}

			td[class="img-flex"] img{width:100% !important; 	height:auto !important;}
			td[class="em_hide"], br[class="em_hide"] {display: none !important;}
			div[class="show"] {display: block !important; margin: 0 !important; padding: 0 !important; overflow: visible !important; width: auto !important; max-height: inherit !important;}
			td[class="aligncenter"]{text-align:center !important;}
			th[class="flex"]{display:block !important; width:100% !important;}
			tr[class="table-holder"]{display:table !important; width:100% !important;}
			th[class="thead"]{display:table-header-group !important; width:100% !important;}
			th[class="trow"]{display:table-row !important; width:100% !important;}
			th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}
			/*custom style*/
			td[class="header"]{padding:43px 39px 34px !important;}
			td[class="holder-01"]{padding:30px 20px 0 !important;}
			td[class="holder-02"]{padding:7px 18px 36px !important;}
            td[class="holder-03"]{padding:0px 18px 26px !important;}
			td[class="footer"]{padding:21px 20px 35px !important;}
			td[class="indent-top-57"]{padding-top:57px !important;}
			td[class="h-auto"]{height:auto !important; padding:0 0 0px !important;}
			img[class="em_width_340"] {width: 340px !important; height: 250px !important; 	max-width: 100% !important;}
			img[class="em_full_width"] {width: 100% !important; height: auto !important; max-width: 100% !important;}
			a[x-apple-data-detectors]{color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important;}
			}
			@media only screen and (-webkit-min-device-pixel-ratio : 2) and (device-width: 683px) and (orientation: landscape), screen and (device-pixel-ratio : 1.5) and (device-width: 400px) and (orientation: portrait){
			td[class="em_hide"], br[class="em_hide"] {display: none !important;}
			div[class="show"] {display: block !important; margin: 0 !important; padding: 0 !important; overflow: visible !important; width: auto !important; max-height: inherit !important;}
			img[class="em_width_340"] {width: 340px !important; height: 250px !important; max-width: 100% !important;}
			img[class="em_full_width"] {width: 100% !important; height: auto !important; max-width: 100% !important;}
			a[x-apple-data-detectors]{color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important;}
			}
			@media only screen and (max-width : 320px) {
			/*default style*/
			table[class="wrapper"]{min-width:320px !important;}
			table[class="flexible"]{width:100% !important;}
			td[class="img-flex"] img{width:100% !important; 	height:auto !important;}
			td[class="em_hide"], br[class="em_hide"] {display: none !important;}
			div[class="show"] {display: block !important; margin: 0 !important; padding: 0 !important; overflow: visible !important; width: auto !important; max-height: inherit !important;}
			td[class="aligncenter"]{text-align:center !important;}
			th[class="flex"]{display:block !important; width:100% !important;}
			tr[class="table-holder"]{display:table !important; width:100% !important;}
			th[class="thead"]{display:table-header-group !important; width:100% !important;}
			th[class="trow"]{display:table-row !important; width:100% !important;}
			th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}
			/*custom style*/
			td[class="header"]{padding:43px 39px 34px !important;}
			td[class="holder-01"]{padding:30px 20px 0 !important;}
			td[class="holder-02"]{padding:7px 18px 36px !important;}
            td[class="holder-03"]{padding:0px 18px 26px !important;}
			td[class="footer"]{padding:21px 20px 35px !important;}
			td[class="indent-top-57"]{padding-top:57px !important;}
			td[class="h-auto"]{height:auto !important; padding:0 0 0px !important;}
			img[class="em_width_340"] {width: 340px !important; height: 250px !important; 	max-width: 100% !important;}
			img[class="em_full_width"] {width: 100% !important; height: auto !important; max-width: 100% !important;}
			a[x-apple-data-detectors]{color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important;}
			}
		</style>
		<!--[if gte mso 9]>
		<style type="text/css">
			td{mso-ascii-font-family:Arial, sans-serif !important;}
		</style>
		<![endif]-->
	</head>
	<body style="margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust: none;" bgcolor="#ffffff">

		{!!$piwik!!}

		<a name="top"></a>

		<!-- Mail preview text -->
		<div style="display:none;font-size:1px;color:#333333;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">
			Hier ist die neue Ausgabe des Daimler Corporate Design Newsletters. / Here is the new issue of the Daimler Corporate Design Newsletter.
		</div>

		<!-- header -->
		<table class="wrapper" width="100%" cellspacing="0" cellpadding="0" style="min-width:320px;" bgcolor="#ffffff">
			<!-- fix for Inbox -->
			<tr>
				<td style="line-height:0;"><div style="display:none; white-space:nowrap; font:15px/1px courier;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td>
			</tr>
			<tr>
				<td>
					<table class="flexible" width="660" bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;">
						<!-- fix for gmail -->
						<tr>
							<td class="hide">
								<table width="660" cellpadding="0" cellspacing="0" style="width:660px !important;">
									<tr><td style="min-width:660px; font-size:0; line-height:0;">&nbsp;</td></tr>
								</table>
							</td>
						</tr>
						<!-- header -->
						<tr>
							<td>
								<table class="flexible" width="600" align="center" cellpadding="0" cellspacing="0">
									<tr>
										<td class="hide">
											<a target="_blank" href="{!!$newsletter_link!!}">
                                                <img src="{!!$header_d!!}" style="vertical-align:top; width:660px; height:450px;" width="660" height="450" alt="{!!$title!!}" />
											</a>
										</td>
									</tr>
									<!--[if !mso]><!-->
									<tr>
										<td class="img-flex" style="line-height:0; font-size:0; display:none;">
											<a target="_blank" href="{!!$newsletter_link!!}">
                                            	<img src="{!!$header_m!!}" width="0" height="0" alt="{!!$title!!}" />
											</a>
										</td>
									</tr>
									<!--<![endif]-->
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>


		{!!$langswitch_de!!}


		{!!$content_de!!}


		{!!$langswitch_en!!}


		{!!$content_en!!}


		<!-- on top button -->
		<table class="wrapper" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
			<tr>
				<td>
					<table class="flexible" width="660" bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;">
						<tr>
							<td style="padding:0 0 18px;">
								<table cellpadding="0" cellspacing="0" align="center" style="margin:0 auto !important;">
									<tr>
										<td style="line-height:0;">
											<a href="#top"><img src="{!!$img_base!!}arrow-01.png" style="width:24px; height:15px;" align="left" hspace="0" vspace="0" width="24" height="15" alt="Nach oben / Back to top" /></a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>


		<!-- footer -->
        <a name="footer"></a>
		<table class="wrapper" width="100%" cellspacing="0" cellpadding="0" bgcolor="#e6e6e6">
			<tr>
				<td>
					<table class="flexible" width="660" bgcolor="#e6e6e6" align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;">
						<tr>
							<td>
								{!!$footer!!}
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>



        <!-- copyright and unsubscribe -->
		<table class="wrapper" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
			<tr>
				<td>
					<table class="flexible" width="660" align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;">
						<tr>
							<td style="padding:20px 20px 30px;">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tr class="table-holder">
										<th class="tfoot" width="300" align="left" style="vertical-align:top; padding:0;">
											<table width="100%" cellpadding="0" cellspacing="0">
												<tr>
													<td class="indent-width-16" style="font:14px/16px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:0 20px;">
														<span class="webfont14" style="font-family:'Arial', 'Helvetica', 'sans-serif'; font-size:12px; line-height:14px; color:#000; padding:0 0px;">
														&copy; {!!$copyright_year!!} Daimler AG
														</span>
													</td>
												</tr>
											</table>
										</th>
										<th class="trow" width="20" height="16" style="padding:0; font-size:0; line-height:0;"></th>
										<th class="thead" width="300" align="left" style="vertical-align:top; padding:0;">
											{!!$unsubscribe!!}
										</th>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

	</body>
</html>

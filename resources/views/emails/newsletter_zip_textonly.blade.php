--- See below for English text ---

Ihr E-Mail-Programm unterstützt leider keine E-Mails im Format HTML.

Sie können den Newsletter hier online lesen:
{!!$newsletter_link!!}


Mitarbeiterinnen und Mitarbeiter der Daimler AG und ihrer Tochtergesellschaften erhalten diesen Newsletter über den zentralen E-Mail-Verteiler des jeweiligen Standortes. Eine manuelle Abbestellung ist daher nicht möglich.

Dieser Newsletter wird im Auftrag der Daimler Communications (Daimler AG - Corporate Design) versendet. Alle Inhalte dieses Newsletters sind urheberrechtlich geschützt. Alle Rechte liegen beim Herausgeber.

---------------------------------------------

Your e-mail program doesn’t support HTML e-mails.

You will find this newsletter online at the following link:
{!!$newsletter_link!!}


Employees of Daimler AG and its subsidiaries receive this newsletter via the central mailing list based on their location. Therefore, it is not possible to manually unsubscribe from this newsletter.

This newsletter has been sent on behalf of Daimler Communications (Daimler AG - Corporate Design). All contents of this newsletter are protected by copyright. All rights remain with the publisher.

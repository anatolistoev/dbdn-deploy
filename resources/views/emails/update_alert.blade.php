<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Daimler Corporate Design News #03/2016</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <style type="text/css">
            @font-face {
                font-family: 'DaimlerCSRegular';
                src: url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCS-Regular.eot');
                src: url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCS-Regular.eot?#iefix') format('embedded-opentype'),
                    url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCS-Regular.svg#wf') format('svg'),
                    url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCS-Regular.woff2') format('woff2'),
                    url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCS-Regular.woff') format('woff'),
                    url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCS-Regular.ttf') format('truetype');
                font-weight:normal;
                font-style: normal;
                mso-font-alt: Arial, Helvetica, sans-serif;
            }
            @font-face {
                font-family: 'DaimlerCSBold';
                src: url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCS-Bold.eot');
                src: url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCS-Bold.eot?#iefix') format('embedded-opentype'),
                    url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCS-Bold.svg#wf') format('svg'),
                    url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCS-Bold.woff2') format('woff2'),
                    url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCS-Bold.woff') format('woff'),
                    url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCS-Bold.ttf') format('truetype');
                font-weight:bold;
                font-style: normal;
                mso-font-alt: Arial, Helvetica, sans-serif;
            }
            @font-face {
                font-family: 'DaimlerCARegular';
                src: url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCA-Regular.eot');
                src: url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCA-Regular.eot?#iefix') format('embedded-opentype'),
                    url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCA-Regular.svg#wf') format('svg'),
                    url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCA-Regular.woff2') format('woff2'),
                    url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCA-Regular.woff') format('woff'),
                    url('https://designnavigator.daimler.com/newsletter/fonts/DaimlerCA-Regular.ttf') format('truetype');
                font-weight:normal;
                font-style: normal;
                mso-font-alt: Arial, Helvetica, sans-serif;
            }
            * {-ms-text-size-adjust:100%; -webkit-text-size-adjust: none; -webkit-text-resize: 100%; text-resize: 100%; -webkit-font-smoothing: antialiased;}
            img {border: 0 !important; outline: none !important;}
            table {border-collapse: collapse; mso-table-lspace: 0px; mso-table-rspace: 0px;}
            td, a, span {border-collapse: collapse !important; mso-line-height-rule: exactly !important;    }
            a{outline:none; text-decoration:none; color:#00677f;}
            a:hover{text-decoration:underline !important;}
            .active a:hover {text-decoration:none !important;}
            .active:hover{opacity:0.8;}
            .active{
                -webkit-transition:all 0.3s ease;
                -moz-transition:all 0.3s ease;
                -ms-transition:all 0.3s ease;
                transition:all 0.3s ease;
            }

            @media screen {

                .webfont14
                {
                    font: 14px DaimlerCSRegular, Arial !important;
                    line-height: 20px !important;
                    font-weight: normal !important;
                }
                .webfont16
                {
                    font: 16px DaimlerCSRegular, Arial !important;
                    line-height: 22px !important;
                    font-weight: normal !important;
                }
                .webfont18-30
                {
                    font: 18px DaimlerCSRegular, Arial !important;
                    line-height: 30px !important;
                    font-weight: normal !important;
                }
                .webfont18-26
                {
                    font: 18px DaimlerCSRegular, Arial !important;
                    line-height: 26px !important;
                    font-weight: normal !important;
                }
                .webfont_18-26
                {
                    font: 18px "DaimlerCARegular", Arial !important;
                    line-height: 26px !important;
                    font-weight: normal !important;
                }
                .webfont14-14
                {
                    font: 14px DaimlerCSRegular, Arial !important;
                    line-height: 14px !important;
                    font-weight: normal !important;
                }
                .webfont36
                {
                    font: 36px DaimlerCSRegular, Arial !important;
                    line-height: 42px !important;
                    font-weight: normal !important;
                }
                .webfont24
                {
                    font: 24px DaimlerCSRegular, Arial !important;
                    line-height: 30px !important;
                    font-weight: normal !important;
                }
                .webfont14-16
                {
                    font: 14px DaimlerCSRegular, Arial !important;
                    line-height: 16px !important;
                    font-weight: normal !important;
                }
                .ul
                {
                    font: 16px DaimlerCSRegular, Arial !important;
                    line-height: 22px !important;
                    font-weight: normal !important;
                }
                a
                {
                    outline:none;
                    text-decoration:none !important;
                    color:#00677f;
                }
                a:hover
                {
                    text-decoration:none !important;
                }
                a[x-apple-data-detectors]{
                    color: inherit !important;
                    text-decoration: none !important;
                    font-size: inherit !important;
                    font-family: inherit !important;
                    font-weight: inherit !important;
                    line-height: inherit !important;
                }
                table td {border-collapse: collapse !important;}
                .ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div
                {
                    line-height:inherit;
                }
                p, br, table td, div, span {
                    font-weight: normal !important;
                    font-family: DaimlerCSRegular, Arial !important;
                }
                strong, b {
                    font-weight: bold !important;
                    font-family: "DaimlerCSBold", Arial !important;
                }
                span.MsoHyperlink {
                    mso-style-priority:99;
                    color:inherit;
                }
                span.MsoHyperlinkFollowed {
                    mso-style-priority:99;
                    color:inherit;
                }
                strong, b {
                    font-weight: bold !important;
                    font-family: "DaimlerCSBold", Arial !important;
                }
                svg{
                    width:100%;
                    height:100%;
                    background-color:#e6e6e6;
                }
                svg polyline{
                    stroke:#fff;
                }
                svg text{
                    font-family: "DaimlerCSRegular", Arial;
                    fill: #fff;
                }
                a[x-apple-data-detectors]{color: inherit !important; text-decoration: none !important;}
                a img{border:none;}
                table td{mso-line-height-rule:exactly;}
                .ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{
                    line-height:inherit;
                    text-decoration: none;
                }
            }

                @media only screen and (max-width:479px) {
                    /*default style*/
                    table[class="flexible"]{width:100% !important;}
                    *[class="hide"]{display:none !important; width:0 !important; height:0 !important; padding:0 !important; font-size:0 !important; line-height:0 !important;}
                    span[class="db"]{display:block !important;}
                    td[class="img-flex"] img{width:100% !important; height:auto !important;}
                    td[class="aligncenter"]{text-align:center !important;}
                    tr[class="table-holder"]{display:table !important; width:100% !important;}
                    th[class="thead"]{display:table-header-group !important; width:100% !important;}
                    th[class="trow"]{display:table-row !important; width:100% !important;}
                    th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}
                    th[class="flex"]{display:block !important; width:100% !important;}
                    /*custom style*/
                    td[class="img-flex"]{display:table-cell !important;}
                    td[class="holder-01"]{padding:41px 10px 0 20px !important;}
                    td[class="holder-01-2"]{padding:41px 10px 0 0 !important;}
                    td.holder-02{padding:11px 18px 26px !important;}
                    td[class="footer"]{padding:30px 20px !important;}
                    td[class="indent-top-25"]{padding-top:25px !important;}
                    td[class="indent-top-32"]{padding-top:32px !important;}
                    td[class="indent-top-57"]{padding-top:57px !important;}
                    td[class="indent-bottom-23"]{padding-bottom:23px !important;}
                    td[class="indent-bottom-32"]{padding-bottom:32px !important;}
                    td[class="indent-width-16"]{padding-left:20px !important; padding-right:16px !important; font-size:14px !important; line-height:16px !important;}
                    td[class="h-auto"]{height:auto !important;}
                }
                @media only screen and (min-width : 321px) and (max-width : 479px) {
                    /*default style*/
                    table[class="wrapper"]{min-width:320px !important;}
                    table[class="flexible"]{width:100% !important;}

                    td[class="img-flex"] img{width:100% !important;     height:auto !important;}
                    td[class="em_hide"], br[class="em_hide"] {display: none !important;}
                    div[class="show"] {display: block !important; margin: 0 !important; padding: 0 !important; overflow: visible !important; width: auto !important; max-height: inherit !important;}
                    td[class="aligncenter"]{text-align:center !important;}
                    th[class="flex"]{display:block !important; width:100% !important;}
                    tr[class="table-holder"]{display:table !important; width:100% !important;}
                    th[class="thead"]{display:table-header-group !important; width:100% !important;}
                    th[class="trow"]{display:table-row !important; width:100% !important;}
                    th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}
                    /*custom style*/
                    td[class="header"]{padding:43px 39px 34px !important;}
                    td[class="holder-01"]{padding:30px 20px 0 !important;}
                    td.holder-02{padding:7px 18px 36px !important;}
                    td[class="footer"]{padding:21px 20px 35px !important;}
                    td[class="indent-top-57"]{padding-top:57px !important;}
                    td[class="h-auto"]{height:auto !important; padding:0 0 0px !important;}
                    img[class="em_width_340"] {width: 340px !important; height: 250px !important;   max-width: 100% !important;}
                    img[class="em_full_width"] {width: 100% !important; height: auto !important; max-width: 100% !important;}
                    a[x-apple-data-detectors]{color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important;}
                }
                @media only screen and (-webkit-min-device-pixel-ratio : 2) and (device-width: 683px) and (orientation: landscape), screen and (device-pixel-ratio : 1.5) and (device-width: 400px) and (orientation: portrait){
                    td[class="em_hide"], br[class="em_hide"] {display: none !important;}
                    div[class="show"] {display: block !important; margin: 0 !important; padding: 0 !important; overflow: visible !important; width: auto !important; max-height: inherit !important;}
                    img[class="em_width_340"] {width: 340px !important; height: 250px !important; max-width: 100% !important;}
                    img[class="em_full_width"] {width: 100% !important; height: auto !important; max-width: 100% !important;}
                    a[x-apple-data-detectors]{color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important;}
                }
                @media only screen and (max-width : 320px) {
                    /*default style*/
                    table[class="wrapper"]{min-width:320px !important;}
                    table[class="flexible"]{width:100% !important;}
                    td[class="img-flex"] img{width:100% !important;     height:auto !important;}
                    td[class="em_hide"], br[class="em_hide"] {display: none !important;}
                    div[class="show"] {display: block !important; margin: 0 !important; padding: 0 !important; overflow: visible !important; width: auto !important; max-height: inherit !important;}
                    td[class="aligncenter"]{text-align:center !important;}
                    th[class="flex"]{display:block !important; width:100% !important;}
                    tr[class="table-holder"]{display:table !important; width:100% !important;}
                    th[class="thead"]{display:table-header-group !important; width:100% !important;}
                    th[class="trow"]{display:table-row !important; width:100% !important;}
                    th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}
                    /*custom style*/
                    td[class="header"]{padding:43px 39px 34px !important;}
                    td[class="holder-01"]{padding:30px 20px 0 !important;}
                    td[class="footer"]{padding:21px 20px 35px !important;}
                    td[class="indent-top-57"]{padding-top:57px !important;}
                    td[class="h-auto"]{height:auto !important; padding:0 0 0px !important;}
                    img[class="em_width_340"] {width: 340px !important; height: 250px !important;   max-width: 100% !important;}
                    img[class="em_full_width"] {width: 100% !important; height: auto !important; max-width: 100% !important;}
                    a[x-apple-data-detectors]{color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important;}
                }
            </style>
        <!--[if gte mso 9]>
        <style type="text/css">
            td{mso-ascii-font-family:Arial, sans-serif !important;}
        </style>
        <![endif]-->
    </head>
    <body style="margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust: none;" bgcolor="#ffffff">
        <a name="top" id="top"></a>
        <table class="wrapper" width="100%" cellspacing="0" cellpadding="0" style="min-width:320px;" bgcolor="#ffffff">
            <!-- fix for Inbox -->
            <tr>
                <td style="line-height:0;"><div style="display:none; white-space:nowrap; font:15px/1px courier;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td>
            </tr>
            <tr>
                <td>
                    <table class="flexible" width="660" bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;">
                        <!-- fix for gmail -->
                        <tr>
                            <td class="hide">
                                <table width="660" cellpadding="0" cellspacing="0" style="width:660px !important;">
                                    <tr><td style="min-width:660px; font-size:0; line-height:0;">&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>
                        <!-- header -->
                        <tr>
                            <td>
                                <table class="flexible" width="600" align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="hide">
                                            <a target="_blank" href="{!!asset('')!!}">
                                                <img src="{!!$header!!}" style="vertical-align:top; width:660px; height: 295px;" height="295" width="660" alt="Daimler Corporate Design Update Alert" />
                                            </a>
                                        </td>
                                    </tr>
                                     <!--[if !mso]><!-->
                                    <tr>
                                        <td class="img-flex" style="line-height:0; font-size:0; display:none;">
                                            <a target="_blank" href="{!! asset('')!!}">
                                                <img src="{!!$header_mobile!!}" width="0" height="0" alt="Daimler Corporate Design Update Alert" />
                                            </a>
                                        </td>
                                    </tr>
                                    <!--<![endif]-->
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        @if($HAS_DE)
        <a name="de"></a>
        <!-- main-de begin-->
        <table class="wrapper" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
            <tr>
                <td>
                    <table class="flexible" width="660" bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;">
                        <tr>
                            <td style="padding:20px 20px 0;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <!-- languages-de -->
                                    @if($HAS_EN)
                                    <tr>
                                        <td style="padding:0 20px;">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="60">
                                                        <a href="#en"><img src="{!!$btn_01!!}" style="vertical-align:top; font:10px/12px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000001; width:60px; height:25px;" width="60" height="25" alt="ENGLISH" /></a>
                                                    </td>
                                                    <td width="20"></td>
                                                    <td width="60">
                                                        <img src="{!!$btn_02!!}" style="vertical-align:top; font:10px/12px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000001; width:60px; height:25px;" width="60" height="25" alt="DEUTSCH" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    @endif
                                    <!-- block-01-de -->
                                    <tr>
                                        <td style="padding-top:40px;">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="holder-02" style="padding:0 20px 40px 20px;font-family:DaimlerCSRegular, Arial, Helvetica, sans-serif;">
                                                         <span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; color:#000;">
                                                             Hallo {!!$FIRST_NAME!!} {!!$LAST_NAME!!},
                                                             <br><br>
                                                             Diese Seite von Ihrer Merkliste wurde aktualisiert:</span>
                                                     </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <th class="flex" width="300" align="left" style="vertical-align:top; padding:0;">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="padding:0 0 10px;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="img-flex" style="position:relative">
                                                                                <a target="_blank" href="{!!$PATH_DE!!}" class="img_holder">
                                                                                    @if(strtolower(pathinfo($IMG_DE, PATHINFO_EXTENSION)) == 'svg')
                                                                                        {!!\App\Helpers\FileHelper::getSVGContent(public_path().$IMG_DE);!!}
                                                                                    @else
                                                                                        <table cellpadding="0" cellspacing="0"><tr><td style="border: 1px solid #c8c8c8;">
                                                                                            <img src="{!!$IMG_DE!!}" style="vertical-align:top; width:300px; height:200px;" width="300" height="200" alt="{!!$TITLE_DE!!}" />
                                                                                        </td></tr></table>
                                                                                    @endif
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </th>
                                                    <th class="flex" width="18" height="0" style="padding:0; font-size:0; line-height:0;">&nbsp;</th>
                                                    <th class="flex" align="left" style="vertical-align:top; padding:0;">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="holder-02" style="padding:0 9px 20px 19px;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="h-auto" valign="top">
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td style="font:12px/20px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:0 0 8px;">
                                                                                            <a target="_blank" style="text-decoration: none !important; color: #000; display: inline-block;" href="{!!$PATH_DE!!}">
                                                                                                <span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; color:#000; padding:0 0px;">
                                                                                                    <b>{!!$TITLE_DE!!}</b>
                                                                                                    <br />
                                                                                                    {!!nl2br($COMMENTS_DE)!!}
                                                                                                </span>
                                                                                            </a>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td class="active" align="center" style="font:12px/20px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#00677f;" bgcolor="#ffffff">
                                                                                            <a target="_blank" style="text-decoration:none; color:#00677f; display:block;" href="{!!$PATH_DE!!}">
                                                                                                <span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px;  padding:0 0px;">
                                                                                                    Mehr erfahren&nbsp;
                                                                                                </span>
                                                                                                <span style="display:inline-block; line-height:0; vertical-align:-4px;">
                                                                                                    <img src="{!!$arrow_small!!}" style="display:block; vertical-align:top; width:7px; height:16px;" width="7" height="16" alt="&gt;" />
                                                                                                </span>
                                                                                            </a>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </th>
                                                </tr>
                                            </table>
                                        </td>
                                     </tr>
                                    <tr>
                                        <td>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="holder-02" style="padding: 30px 20px 20px 20px;font:12px/20px DaimlerCSRegular, Arial, Helvetica, sans-serif;">
                                                        <span class="webfont14" style="font-family: Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; color:#000;">
                                                        Sie erhalten diese E-Mail, da Sie die personalisierte Funktion „Merkliste“ im Online-Portal „Daimler Brand & Design Navigator“ abonniert haben. Sie werden automatisch benachrichtigt, sobald eine der von Ihnen gespeicherten Seiten aktualisiert wurde. Sie können die Benachrichtigungsoption jederzeit deaktivieren, indem Sie die betreffende Seite von Ihrer Merkliste entfernen.
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="active holder-02" align="center" style="font:12px/20px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#00677f; padding:0 20px 40px 20px;" bgcolor="#ffffff">
                                                        <a target="_blank" style="text-decoration:none; color:#00677f; display:block;" href="{!!asset('Watchlist?lang=de')!!}">
                                                            <span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px;  padding:0 0px;">
                                                                Zur Merkliste&nbsp;
                                                            </span>
                                                            <span style="display:inline-block; line-height:0; vertical-align:-4px;">
                                                                <img src="{!!$arrow_small!!}" style="display:block; vertical-align:top; width:7px; height:16px;" width="7" height="16" alt=">">
                                                            </span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    @if($HAS_EN)
                                    <tr>
                                        <td height="1" style="font-size:0; line-height:0;height: 1px;" bgcolor="#000000">
                                        </td>
                                    </tr>
                                    @endif
                                </table>
                            </td>
                         </tr>
                     </table>
                </td>
            </tr>
        </table>
        @endif
        @if($HAS_EN)
        <a name="en"></a>
        <!-- main-de end-->
        <!-- main-en begin-->
        <table class="wrapper" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
            <tr>
                <td>
                    <table class="flexible" width="660" bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;">
                        <tr>
                            <td style="padding:20px 20px 0;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <!-- languages-en -->
                                    @if($HAS_DE)
                                    <tr>
                                        <td style="padding:0 20px;">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="60">
                                                        <img src="{!!$btn_03!!}" style="vertical-align:top; font:10px/12px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000001; width:60px; height:25px;" width="60" height="25" alt="ENGLISH" />
                                                    </td>
                                                    <td width="20"></td>
                                                    <td width="60">
                                                        <a href="#de"><img src="{!!$btn_04!!}" style="vertical-align:top; font:10px/12px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000001; width:60px; height:25px;" width="60" height="25" alt="DEUTSCH" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    @endif
                                    <!-- block-01-en -->
                                    <tr>
                                        <td style="padding-top:40px;">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                     <td class="holder-02" style="padding:0 20px 40px 20px;">
                                                        <span class="webfont14" style="font-family: Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; color:#000;">
                                                        Hello {!!$FIRST_NAME!!} {!!$LAST_NAME!!},
                                                        <br><br>
                                                        This page on your “Watchlist” has been updated:</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <th class="flex" width="300" align="left" style="vertical-align:top; padding:0;">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="padding:0 0 10px;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="img-flex" style="position:relative">
                                                                                <a target="_blank" href="{!!$PATH_EN!!}" class="img_holder">
                                                                                    @if(strtolower(pathinfo($IMG_EN, PATHINFO_EXTENSION)) == 'svg')
                                                                                        {!!\App\Helpers\FileHelper::getSVGContent(public_path().$IMG_EN);!!}
                                                                                    @else
                                                                                    <table cellpadding="0" cellspacing="0"><tr><td style="border: 1px solid #c8c8c8;">
                                                                                        <img src="{!!$IMG_EN!!}" style="vertical-align:top; width:300px; height:200px; border: 1px solid #c8c8c8; display:block;" width="300" height="200" alt="{!!$TITLE_EN!!}" />
                                                                                        </td></tr></table>
                                                                                    @endif
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                    </table>
                                                    </th>
                                                    <th class="flex" width="18" height="0" style="padding:0; font-size:0; line-height:0;">&nbsp;</th>
                                                    <th class="flex" align="left" style="vertical-align:top; padding:0;">
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="holder-02" style="padding:0 9px 20px 19px;">
                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td class="h-auto" valign="top">
                                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td style="font:12px/20px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:0 0 8px;">
                                                                                                <a target="_blank" style="text-decoration: none !important; color: #000;     display: inline-block;" href="{!!$PATH_EN!!}">
                                                                                                    <span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; color:#000; padding:0 0px;">
                                                                                                        <b>{!!$TITLE_EN!!}</b>
                                                                                                        <br />
                                                                                                        {!!nl2br($COMMENTS_EN)!!}
                                                                                                    </span>
                                                                                                </a>
                                                                                                <br/>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td class="active" align="center" style="font:12px/20px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#00677f;" bgcolor="#ffffff">
                                                                                                <a target="_blank" style="text-decoration:none; color:#00677f; display:block;" href="{!!$PATH_EN!!}">
                                                                                                    <span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px;  padding:0 0px;">
                                                                                                        Read more&nbsp;
                                                                                                    </span>
                                                                                                    <span style="display:inline-block; line-height:0; vertical-align:-4px;">
                                                                                                        <img src="{!!$arrow_small!!}" style="display:block; vertical-align:top; width:7px; height:16px;" width="7" height="16" alt="&gt;" />
                                                                                                    </span>
                                                                                                </a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                    </th>
                                                </tr>
                                            </table>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="holder-02" style="padding: 30px 20px 20px 20px;font:12px/20px DaimlerCSRegular, Arial, Helvetica, sans-serif;">
                                                        <span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; color:#000;">
                                                        You are receiving this e-mail because you subscribed to the Daimler Brand & Design Navigator online portal to keep you up-to-date whenever page updates matching your personal “Watchlist” are available. To unsubscribe from getting notifications, please remove this page from your “Watchlist” after logging in.
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="active holder-02" align="center" style="font:12px/20px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#00677f; padding:0 20px 40px 20px;" bgcolor="#ffffff">
                                                        <a target="_blank" style="text-decoration:none; color:#00677f; display:block;" href="{!!asset('Watchlist?lang=en')!!}">
                                                            <span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px;  padding:0 0px;">
                                                                Go to “Watchlist”&nbsp;
                                                            </span>
                                                            <span style="display:inline-block; line-height:0; vertical-align:-4px;">
                                                                <img src="{!!$arrow_small!!}" style="display:block; vertical-align:top; width:7px; height:16px;" width="7" height="16" alt=">">
                                                            </span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        @endif
        <!-- on top button -->
        <table class="wrapper" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
            <tr>
                <td>
                    <table class="flexible" width="660" bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;">
                        <tr>
                            <td style="padding:0 0 18px;">
                                <table cellpadding="0" cellspacing="0" align="center" style="margin:0 auto !important;">
                                    <tr>
                                        <td style="line-height:0;">
                                            <a href="#top" style="cursor:pointer;"><img src="{!!$arrow_01!!}" style="width:24px; height:15px;" align="left" hspace="0" vspace="0" width="24" height="15" alt="Nach oben / Back to top" /></a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!-- footer -->
        <a name="footer"></a>
        <table class="wrapper" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
            <tr>
                <td bgcolor="#e6e6e6">
                    <table class="flexible" width="660" align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;">
                        <tr>
                            <td class="footer" bgcolor="#e6e6e6" style="padding:90px 20px;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <th class="flex" width="300" align="left" style="vertical-align:top; padding:0;">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="font:14px/22px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:0 0 0 20px;">
                                                                    <span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; color:#000; padding:0 0px;">
                                                                        Daimler AG<br />
                                                                        Sitz und Registergericht/Domicile and Court of Registry: Stuttgart<br />
                                                                        HRB-Nr./Commercial Register No. 19360<br />
                                                                        Vorsitzender des Aufsichtsrats/Chairman of the Supervisory Board: Manfred Bischoff<br />
                                                                        Vorstand/Board of Management: Ola Källenius (Vorsitzender/Chairman), Martin Daum, Renata Jungo Brüngger, Wilfried Porth, Markus Schäfer, Britta Seeger, Hubertus Troska, Harald Wilhelm<br /><br />
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </th>
                                                    <th class="flex" width="20" height="4" style="padding:0; font-size:0; line-height:0;"></th>
                                                    <th class="flex" width="300" align="left" style="vertical-align:top; padding:0;">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="padding:0 0 0;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="indent-width-16" style="font:14px/16px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:0px 5px 17px 20px; border-top:1px solid #e6e6e6;">
                                                                                <span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; color:#000; padding:0 0px;">
                                                                                    <a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Provider?lang=de">Anbieter</a> |
                                                                                    <a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Provider?lang=en">Provider</a>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="indent-width-16" style="font:14px/16px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:17px 5px 17px 20px; border-top:1px solid #cbcbcb;">
                                                                                <span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; color:#000; padding:0 0px;">
                                                                                    <a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Legal_Notice?lang=de">Rechtliche Hinweise</a> |
                                                                                    <a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Legal_Notice?lang=en">Legal Notice</a>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="indent-width-16" style="font:14px/16px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:17px 5px 17px 20px; border-top:1px solid #cbcbcb;">
                                                                                <span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; color:#000; padding:0 0px;">
                                                                                    <a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Privacy_Statement?lang=de">Datenschutz</a> |
                                                                                    <a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Privacy_Statement?lang=en">Privacy Statement</a>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="indent-width-16" style="font:14px/16px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:17px 5px 17px 20px; border-top:1px solid #cbcbcb;">
                                                                                <span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; color:#000; padding:0 0px;">
                                                                                    <a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Cookies?lang=de">Cookies-Hinweise</a> |
                                                                                    <a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Cookies?lang=en">Cookies Notice</a>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="indent-width-16" style="font:14px/16px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:17px 5px 17px 20px; border-top:1px solid #cbcbcb;">
                                                                                <span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; color:#000; padding:0 0px;">
                                                                                    <a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Contacts?lang=de">Kontakte</a> |
                                                                                    <a target="_blank" style="color:#000001; text-decoration:none;" href="https://designnavigator.daimler.com/Contacts?lang=en">Contacts</a>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </th>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table class="wrapper" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
            <tr>
                <td>
                    <table class="flexible" width="660" align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;">
                        <tr>
                            <td style="padding:20px 20px 30px;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr class="table-holder">
                                        <th class="tfoot" width="300" align="left" style="vertical-align:top; padding:0;">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="indent-width-16" style="font:14px/16px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000; padding:0 20px;">
                                                        <span class="webfont14" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; color:#000; padding:0 0px;">
                                                            &copy; {!! date("Y") !!} Daimler AG
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </th>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
<span style="font-family:Arial;font-size:10pt;">
	The access inquiry concerns the following password protected asset in the Daimler Brand & Design Navigator:
	<br>
	<br>
	<a href="{!! url($slug) !!}">{!! url($slug) !!}</a>
	<br>
	<br>
	<br>
	The person requesting the extended access to the password protected asset is:
	<br>
	<br>
	@if($rbChoice == 1)
     an employee of Daimler AG/a Daimler subsidiary or of a Mercedes-Benz/smart authorized dealer/service partner acting on behalf of Daimler AG
	@endif
    @if($rbChoice == 2)
     an employee of an agency, a supplier company, an educational institution or another organization acting on behalf of Daimler AG/a Daimler subsidiary or on behalf of a Mercedes-Benz/smart authorized dealer/service partner'
	@endif
    <br>
	<br>
	<br>
	E-mail:
	<br>
	<br>
	{!!$email!!}
	<br>
	<br>
	<br>
	Username:
	<br>
	<br>
	{!! $user_name !!}
	<br>
	<br>
	<br>
	Reason for access:
	<br>
	<br>
	<span style="white-space: pre-wrap;">{!!$text_area!!}</span>
	<br>
	<br>
	<br>
	@if($rbChoice != 1)
	Name of the contact person in the responsible department of Daimler AG:
	<br>
	<br>
	{!!$contact_name!!}
	<br>
	<br>
	<br>
	E-mail of the contact person in the responsible department of Daimler AG:
	<br>
	<br>
	{!!$contact_email!!}
	@endif
</span>
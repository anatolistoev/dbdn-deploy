<!DOCTYPE html>
<html lang="en-US">
	<head>
        <meta name="viewport" content="width=1280">
        <meta name="MobileOptimized" content="1280">
		<meta charset="utf-8">
	</head>
	<body>
		<div style="font-family:Arial;font-size:10pt;">
            <p span style="font-family:Arial;font-size:10pt;">
                @lang('passwords.email.greeting', array('FIRST_NAME'=> $user->first_name, 'LAST_NAME'=> $user->last_name))
                <br>
            </p>
            <p span style="font-family:Arial;font-size:10pt;">
                @lang('passwords.email.content', array('CONFIRMATION_LINK'=>URL::to(App::getLocale().'/reset', array($token))))
            </p>
            <p style="font-family:Arial;font-size:10pt;">
                @lang('passwords.email.footer')
            </p>
		</div>
	</body>
</html>
<p style="font-family:Arial;font-size:10pt;">
	@lang('emails.request_access_confirm.greeting', array('FIRST_NAME'=> $user_first_name, 'LAST_NAME'=> $user_last_name))
	<br>
</p>
<p style="font-family:Arial;font-size:10pt;">
@if($template == 'downloads')
	@lang('emails.request_access_confirm.download.content')
@else
	@lang('emails.request_access_confirm.content')
@endif
</p> 
<p style="font-family:Arial;font-size:10pt;">
	@lang('emails.request_access_confirm.footer')
	<br>
</p> 

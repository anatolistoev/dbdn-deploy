--- See below for English text ---

Ihr E-Mail-Programm unterstützt leider keine E-Mails im Format HTML.

Sie können den Newsletter hier online lesen:
{!!$newsletter_link!!}


Wenn Sie diese E-Mail nicht mehr erhalten möchten, können Sie diese abbestellen. Ändern Sie dazu die Einstellung in Ihrem Benutzerprofil, indem Sie die Checkbox „Newsletter abonnieren“ abwählen:
https://designnavigator.daimler.com/login

Alternativ können Sie den Newsletter per Direktlink abbestellen:
{!!$unsubscribe_link!!}lang=de

Dieser Newsletter wird im Auftrag der Daimler Communications (Daimler AG - Corporate Design) versendet. Alle Inhalte dieses Newsletters sind urheberrechtlich geschützt. Alle Rechte liegen beim Herausgeber.

--------------------------------------------------------------

Your e-mail program doesn’t support HTML e-mails.

You will find this newsletter online at the following link:
{!!$newsletter_link!!}


To unsubscribe from the mailing list, first log in and then modify the settings in your user profile by unchecking the “Subscribe to newsletter” option:
https://designnavigator.daimler.com/login

Alternatively, you can unsubscribe via direct link:
{!!$unsubscribe_link!!}lang=en

This newsletter has been sent on behalf of Daimler Communications (Daimler AG - Corporate Design). All contents of this newsletter are protected by copyright. All rights remain with the publisher.

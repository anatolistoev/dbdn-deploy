<span style="font-family:Arial;font-size:10pt;">@lang('feedback.question_regarding'): <a href="{!! url($pid) !!}">{!! url($pid) !!}</a><br>
@lang('feedback.email.message'):<br>
<span style="white-space: pre-wrap;">{!! $messageText !!}</span><br>
<br>
<br>
@lang('feedback.email.email'): {!!$email!!}<br />
<br />
@lang('feedback.email.name'): {!!$name!!}
</span>
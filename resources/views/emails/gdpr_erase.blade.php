<!DOCTYPE html>
<html lang="en-US">
	<head>
        <meta name="viewport" content="width=1280">
        <meta name="MobileOptimized" content="1280">
		<meta charset="utf-8">
	</head>
	<body>
		<div style="font-family:Arial;font-size:10pt;">
            <p span style="font-family:Arial;font-size:10pt;">
                @lang('gdpr.mail.greeting', array('FIRST_NAME'=> $user->first_name, 'LAST_NAME'=> $user->last_name))
                <br>
            </p>
            <p span style="font-family:Arial;font-size:10pt;">
                @lang('gdpr.mail.content', array('CONFIRMATION_LINK'=>URL::to('/user/gdprerase', array($user->id,$token))))
            </p>
            <p style="font-family:Arial;font-size:10pt;">
                @lang('gdpr.mail.footer')
            </p>
		</div>
	</body>
</html>
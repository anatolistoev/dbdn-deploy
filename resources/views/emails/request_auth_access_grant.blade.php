<p span style="font-family:Arial;font-size:10pt;">
	{!!Lang::get('emails.request_access_grant.greeting', array('FIRST_NAME'=>$FIRST_NAME,'LAST_NAME'=>$LAST_NAME), $LANG)!!}
	<br>
</p>
<p span style="font-family:Arial;font-size:10pt;">
@if($PAGE && $PAGE->template == 'downloads')
	{!!Lang::get('emails.request_access_grant.download.content', array(), $LANG)!!}
@else
	{!!Lang::get('emails.request_access_grant.content', array('TITLE'=>$PAGE->title, 'SLUG'=>$PAGE->slug), $LANG)!!}
@endif
</p>
<p style="font-family:Arial;font-size:10pt;">
	{!!Lang::get('emails.request_access_grant.footer', array(), $LANG)!!}
</p>


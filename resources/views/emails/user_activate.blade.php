<p span style="font-family:Arial;font-size:10pt;">
    @lang($MAILTYPE.'.greeting', array('FIRST_NAME'=> $FIRST_NAME, 'LAST_NAME'=> $LAST_NAME))
	<br>
</p>
<p span style="font-family:Arial;font-size:10pt;">
	@lang($MAILTYPE.'.content', array('CONFIRMATION_LINK'=>isset($CONFIRMATION_LINK)?$CONFIRMATION_LINK:'', 'USERNAME'=> isset($USERNAME)?$USERNAME :''))
</p>
<p style="font-family:Arial;font-size:10pt;">
	@lang($MAILTYPE.'.footer')
</p>

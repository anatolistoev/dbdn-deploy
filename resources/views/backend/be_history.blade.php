@extends('layouts.backend')
@section('left_navigation')
<a href="{!! url('/cms/comments')!!}" class="nav_box @if($ACTIVE == 'comments')active @endif" style="background-image:url('{!! url('/img/backend/comments_icon.png')!!}');margin-top:80px;">@lang('backend.comments')</a>
<a href="{!! url('/cms/history')!!}" class="nav_box @if($ACTIVE == 'history')active @endif" style="background-image:url('{!! url('/img/backend/history_icon.png')!!}')">@lang('backend.history')</a>		
@stop
@section('user_content')
<script type="text/javascript" src="{!! url('/js/history.js')!!}"></script>
<div class="user_table">
	<form merhod="POST" action="" class="filter_form">
		<input type="text" name="user_search" value="" class="user_search" placeholder="@lang('backend_comments.placeholder.history_search')"/>
		<select name="user_sort" class="user_sort">
			<option value="-1">@lang('backend_comments.placeholder.history_sort')</option>
			<option value="0">Id</option>
			<option value="1">Page</option>
			<option value="2">Content</option>
			<option value="2">User</option>
			<option value="3">Action</option>
			<option value="4">IP</option>
			<option value="5">Date</option>
		</select>
		<input class="user_submit" type="submit" value="@lang('backend_comments.button.filter')" onClick="return refreshTable();" />
	</form>

	<table cellpadding="0" cellspacing="0" border="0" id="history">
		<thead>
			<tr>
				<th>Id</th>
				<th>Page</th>
				<th>Content</th>
				<th>User</th>
				<th>Action</th>
				<th>IP</th>
				<th>Date</th>				
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>	
</div>
@stop
<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Daimler Brand &amp; Design Navigator</title>
		@include('partials_backend.backend_global_insert')
		@section('scripts')
		<script type="text/javascript" src="{!! url('/js/tinymce_4.1.7/tinymce.min.js')!!}"></script>
		<script type="text/javascript" src="{!! url('/js/functions.js')!!}"></script>
		@show
		@section('styles')
		@show
		@include('partials_backend.js_localize')
	</head>
	<body>
		<div class="wrapper" style="padding:0px;width:960px;">
			<div class="content_wrapper">

				<script>
					var lang = '{!!$LANG!!}';
					var lang_label;
					if(lang == {!!LANG_EN!!}){
						lang_label = 'en';
					}else{
						lang_label = 'de';
					}

				</script>
				<script type="text/javascript" src="{!! url('/js/hotspot.js')!!}"></script>
				<div id="hotspot" class="user_table cf">
					<div id="hotspot_template" style="display: none;">
						<div class="hotspot" id="hotspot_{hotspot_id}">
							<div class="hotspot_row cf">
								<a id="coords_btn_{hotspot_id}" class="nav_box white select_area coords_btn">@lang('backend.hotspot.select_area')</a>
								<a class="nav_box white remove">@lang('backend.hotspot.remove')</a>
							</div>
							<div class="hotspot_row half">
								<label for="top_{hotspot_id}">@lang('backend.hotspot.top'):</label>
								<input id="top_{hotspot_id}" type="text" name="top_{hotspot_id}" value="" />
							</div>
							<div class="hotspot_row half" style="margin-left:20px;">
								<label for="top_{hotspot_id}">@lang('backend.hotspot.left'):</label>
								<input id="left_{hotspot_id}" type="text" name="left_{hotspot_id}" value="" />
							</div>
							<div class="hotspot_row">
								<label for="pos_{hotspot_id}">@lang('backend.hotspot.direction'):</label>
								<select name="pos_{hotspot_id}" id="pos_{hotspot_id}">
									<option value="left">@lang('backend.hotspot.direction.left')</option>
									<option value="right">@lang('backend.hotspot.direction.right')</option>
									<option value="up">@lang('backend.hotspot.direction.up')</option>
									<option value="down">@lang('backend.hotspot.direction.down')</option>
                                    <option value="no_arrow">@lang('backend.hotspot.direction.no_arrow')</option>
								</select>
							</div>
						</div>
					</div>
					<div class="hotspot_left">
						<div class="hotspot_image_holder">
							<img id="hotspot_img" src="" width="697" height="523"/>
						</div>
					</div>
					<div class="hotspot_right">
						<div class="hotspot_points cf">

						</div>
						<textarea style="margin-bottom: 50px; width: 925px;" name="hotspot_description" id="hotspot_description"></textarea>
						<div style="clear:both;height:30px;"></div>
						<a id="add_hotspot" class="nav_box white add_child" >@lang('backend.hotspot.add')</a>
						<a class="nav_box white save">@lang('backend.save')</a>
						<a class="nav_box white cancel" style="border:medium none;">@lang('backend.cancel')</a>
					</div>
				</div>

			</div>
			<div style="clear:both;"></div>
		</div>
	</body>
</html>

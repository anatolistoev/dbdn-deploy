@extends('layouts.backend')
@section('left_navigation')
<a href="{!! url('/cms/newsletter_users')!!}" class="nav_box @if($ACTIVE == 'newsletter_users')active @endif" style="background-image:url('{!! url('/img/backend/user_icon.png')!!}'); margin-top: 80px;">@lang('backend.newsletter_users')</a>
<a href="{!! url('/cms/newsletter_users_test')!!}" class="nav_box @if($ACTIVE == 'newsletter_users_test')active @endif" style="background-image:url('{!! url('/img/backend/user_icon.png')!!}');">@lang('backend.newsletter_test_users')</a>
@stop
@section('user_content')
<script>
	var lang = '{!!$LANG!!}';
</script>
<script type="text/javascript" src="{!! url('/js/newsletter_users.js')!!}"></script>
<div class="user_table">
	<form merhod="POST" action="" class="filter_form">
		<input type="text" name="user_search" value="" class="user_search" placeholder="@lang('backend_users.placeholder.user_search')"/>
		<select name="user_sort" class="user_sort">
			<option value="-1">@lang('backend_users.placeholder.user_sort')</option>
			<option value="0">@lang('backend_users.column.id')</option>
			<option value="1">@lang('backend_users.column.username')</option>
			<option value="2">@lang('backend_users.column.name')</option>
			<option value="3">@lang('backend_users.column.role')</option>
			<option value="4">@lang('backend_users.column.active')</option>
			<option value="5">@lang('backend_users.column.last_login')</option>
			<option value="6">@lang('backend_users.column.logins_count')</option>
		</select>
		<input class="user_submit" type="submit" value="@lang('backend_users.button.filter')" onClick="return refreshTable();" />
        <span id="user_count">@lang('backend_newsletter.subscriber_count')<span id="subscriber_count"></span></span>
	</form>

	<table cellpadding="0" cellspacing="0" border="0" id="users_newsletter">
		<thead>
			<tr>
				<th>@lang('backend_users.column.id')</th>
				<th>@lang('backend_users.column.username')</th>
				<th>@lang('backend_users.column.name')</th>
				<th>@lang('backend_users.column.role')</th>
				<th>@lang('backend_users.column.active')</th>
				<th>@lang('backend_users.column.last_login')</th>
				<th>@lang('backend_users.column.logins_count')</th>
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>
	<div class="user_edit_wrapper" style="display:none">
		<div class="user_actions">
			<a class="nav_box white user subscriber">@lang('backend_newsletter.add_subscriber')</a>
			<a class="nav_box white remove" confirm="@lang('backend_newsletter.remove_subscriber.confirm')">@lang('backend_newsletter.remove_subscriber')</a>
			<div style="clear:both;"></div>
		</div>
		<div class="textarea_overlay"></div>
		<div id="user_content" class="user_fields" style="display:none;">
			<form id="user_form" action="" method="POST">
				<div class="form_right cf" style="padding-top: 0px;">
					<div class="input_section">
						<div class="input_group">
							<label for="username">@lang('backend_newsletter.add_subscriber.form.username_or_id')<span class="fieldset_requared">*</span></label>
							<input type="text" value="" name="username" tabindex="1"/>
						</div>
					</div>
					<div style="clear:both;height:51px;"></div>
					<a class="nav_box white user add" tabindex="2">@lang('backend.add')</a>
					<a class="nav_box white cancel" tabindex="3">@lang('backend.cancel')</a>
				</div>
			</form>
		</div>
	</div>	
</div>
@stop
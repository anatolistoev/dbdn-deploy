@extends('layouts.backend')
@section('left_navigation')
<a @if(Session::get('lang') == LANG_EN) href="{!! url('/cms/faqs?lang=de')!!}" style="background-image:url('{!! url('/img/backend/change_lang_icon_EN.png')!!}');" @else href="{!! url('/cms/faqs?lang=en')!!}" style="background-image:url('{!! url('/img/backend/change_lang_icon_DE.png')!!}');"@endif class="nav_box" >@lang('backend.change_lang')</a>
@stop
@section('user_content')
<script>
	var lang = '{!!$LANG!!}';
    var brands = [];
    brands[{!!\App\Helpers\UserAccessHelper::getSpecialPage(DAIMLER)->ID!!}] = '{!!\App\Helpers\UserAccessHelper::getSpecialPage(DAIMLER)->slug!!}';
    brands[{!!\App\Helpers\UserAccessHelper::getSpecialPage(SMART)->ID!!}] = '{!!\App\Helpers\UserAccessHelper::getSpecialPage(SMART)->slug!!}';
     var categories = [];
       @foreach (Config::get('app.parameters.faq_category') as $key => $value)
        categories[{!!$key!!}] = '@lang('ws_general_controller.faq_category_'.$value)';
    @endforeach
</script>
<script type="text/javascript" src="{!! url('/js/tinymce_4.1.7/tinymce.min.js')!!}"></script>
 <script type="text/javascript" src="{!! url('/js/tiny_mce_init.js')!!}"></script>
<script type="text/javascript" src="{!! url('/js/faqs.js')!!}"></script>
<div class="user_table">
	<form merhod="POST" action="" class="filter_form">
		<input type="text" name="user_search" value="" class="user_search" placeholder="@lang('backend_faqs.placeholder.faq_search')"/>
		<select name="user_sort" class="user_sort">
			<option value="-1">@lang('backend_faqs.placeholder.faq_sort')</option>
			<option value="0">@lang('backend_faqs.column.id')</option>
			<option value="1">@lang('backend_faqs.column.question')</option>
            <option value="2">@lang('backend_faqs.column.brand')</option>
            <option value="3">@lang('backend_faqs.column.category')</option>
		</select>
		<input class="user_submit" type="submit" value="@lang('backend_comments.button.filter')" onClick="return refreshTable();" />
	</form>

	<table cellpadding="0" cellspacing="0" border="0" id="faqs">
		<thead>
			<tr>
				<th>@lang('backend_faqs.column.id')</th>
				<th>@lang('backend_faqs.column.question')</th>
                <th>@lang('backend_faqs.column.brand')</th>
                <th>@lang('backend_faqs.column.category')</th>
                <th>@lang('backend_faqs.column.modified_at')</th>
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>
	<div class="user_edit_wrapper" style="display:none">
		<div class="user_actions">
			<a class="nav_box white edit">@lang('backend_faqs.edit')</a>
			<a class="nav_box white add">@lang('backend_faqs.add')</a>
			<a class="nav_box white remove" confirm="@lang('backend_faqs.remove.confirm')">@lang('backend_faqs.remove')</a>
			<div style="clear:both;"></div>
		</div>
		<div class="textarea_overlay"></div>
		<div class="user_fields" id="wrapper_form_faqs" style="display:none;">
            <form id="faqs_form" action="" method="POST" style="display: flex">
				<input type="hidden" name="id" value="0" />
				<input type="hidden" name="lang" value="{!!$LANG!!}" />
				<div class="form_left" style="align-items: stretch;padding-top:30px;">
					<div class="system_info">
						<div class="system_info_row">
							<p class="si_label">@lang('backend_faqs.form.created_at')</p>
							<p class="si_data" id="created_at"></p>
							<div style=clear:both;"></div>
						</div>
						<div class="system_info_row">
							<p class="si_label">@lang('backend_faqs.form.modified_at')</p>
							<p class="si_data" id="updated_at"></p>
							<div style=clear:both;"></div>
						</div>
					</div>
				</div>
				<div class="form_right" style="height:100%;">
					<div class="input_section">
						<div class="input_group">
							<label for="faq_question">@lang('backend_faqs.form.question')<span class="fieldset_requared">*</span></label>
							<input type="text" value="" id='faq_question' name="question" tabindex="1"/>
						</div>
                        <div class="cf"></div>
						<div class="input_group">
							<label for="faq_answer">@lang('backend_faqs.form.answer')<span class="fieldset_requared">*</span></label>
                            <textarea  id ="faq_answer" name="answer" tabindex="2" style="height: 150px"></textarea>
						</div>
                        <div class="cf"></div>
                        <div class="input_group" style="width: 48%;">
                           <label for="faq_brand_id">@lang('backend_faqs.form.brand')<span class="fieldset_requared">*</span></label>
                            <select id="faq_brand_id" name="brand_id" tabindex="1">
                                @if(Auth::user()->role != USER_ROLE_EDITOR_SMART)
                                <option value="{!!\App\Helpers\UserAccessHelper::getSpecialPage(DAIMLER)->ID!!}">{!!\App\Helpers\UserAccessHelper::getSpecialPage(DAIMLER)->slug!!}</option>
                                @endif
                                <option value="{!!\App\Helpers\UserAccessHelper::getSpecialPage(SMART)->ID!!}">{!!\App\Helpers\UserAccessHelper::getSpecialPage(SMART)->slug!!}</option>
                            </select>
                        </div>
                        <div class="input_group" style="width: 48%; float: right">
                           <label for="category_id">@lang('backend_faqs.form.category')<span class="fieldset_requared">*</span></label>
                            <select id="faq_category_id" name="category_id" tabindex="1">
                             @foreach (Config::get('app.parameters.faq_category') as $key => $value)
                                <option value="{!!$key!!}">@lang('ws_general_controller.faq_category_'.$value)</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="cf"></div>
                        <div class="input_group" style="width: 48%">
                           <label for="faqs_tags">@lang('backend_faqs.form.tags')</label>
                           <div class="role_section tags_wrapper" style="margin: 0; width: 369px">
                            <div class="tags"></div>
                            <input type="text" id="faqs_tags" class='tags_lang' value="" />
                           </div>
                        </div>
                        <div class="cf"></div>
					</div>
					<div style="clear:both;height:80px;"></div>
					<a class="nav_box white save" href='#' tabindex="3">@lang('backend.save')</a>
					<a class="nav_box white cancel" href='#' tabindex="4">@lang('backend.cancel')</a>
                </div>
				<div style="clear:both;"></div>
			</form>
		</div>
	</div>

</div>
@stop
@extends('layouts.backend')
@section('left_navigation')
<a href="{!! url('/cms/recycle_bin')!!}" class="nav_box recycle @if($ACTIVE == 'recycle_bin')active @endif">@lang('backend.recycle_bin')</a>
<a @if(Session::get('lang') == LANG_EN) href="{!! url('/cms/pages?lang=de')!!}" style="background-image:url('{!! url('/img/backend/change_lang_icon_EN.png')!!}');" @else href="{!! url('/cms/pages?lang=en')!!}" style="background-image:url('{!! url('/img/backend/change_lang_icon_DE.png')!!}');"@endif class="nav_box">@lang('backend.change_lang')</a>
@stop
@section('user_content')
<script>
	var lang = '{!!$LANG!!}';
</script>
<script type="text/javascript" src="{!! url('/js/recycle_bin.js')!!}"></script>
<div class="user_table">
	<form merhod="POST" action="" class="filter_form">
		<input type="text" name="user_search" value="" class="user_search" placeholder="@lang('backend_pages.placeholder.page_search')"/>
		<select name="user_sort" class="user_sort">
			<option value="-1">@lang('backend_pages.placeholder.page_sort')</option>
			<option value="0">@lang('backend_pages.column.title')</option>
			<option value="1">@lang('backend_pages.column.id')</option>
			<option value="2">@lang('backend_pages.column.type')</option>
			<option value="3">@lang('backend_pages.column.status')</option>
			<option value="4">@lang('backend_pages.column.access')</option>
			<option value="5">@lang('backend_pages.column.last_modified')</option>
		</select>
		<input class="user_submit" type="submit" value="@lang('backend_pages.button.filter')" onClick="return refreshTable();" />
	</form>

	<table cellpadding="0" cellspacing="0" border="0" id="pages">
		<thead>
			<tr>
				<th>@lang('backend_pages.column.title')</th>
				<th>@lang('backend_pages.column.id')</th>
				<th>@lang('backend_pages.column.type')</th>
				<th>@lang('backend_pages.column.status')</th>
				<th>@lang('backend_pages.column.access')</th>
				<th>@lang('backend_pages.column.last_modified')</th>
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>

	<div class="user_edit_wrapper" style="display:none">
		<div class="user_actions">
			<a class="nav_box white restore">@lang('backend_pages.restore')</a>
            @if(\App\Helpers\UserAccessHelper::hasAccessAction(PERMANENT_DELETE))
            <a class="nav_box white remove" confirm="@lang('backend_pages.permanentremove.confirm')">@lang('backend_pages.remove')</a>
            @endif
			<div style="clear:both;"></div>
		</div>
	</div>

</div>
@stop
@extends('layouts.backend')
@section('left_navigation')
<a href="{!! url('/cms/comments')!!}" class="nav_box @if($ACTIVE == 'comments')active @endif" style="background-image:url('{!! url('/img/backend/comments_icon.png')!!}');margin-top:80px;">@lang('backend.comments')</a>
<a href="{!! url('/cms/history')!!}" class="nav_box @if($ACTIVE == 'history')active @endif" style="background-image:url('{!! url('/img/backend/history_icon.png')!!}')">@lang('backend.history')</a>
@stop
@section('user_content')
<script>
	var lang = '{!!$LANG!!}';
</script>
<script type="text/javascript" src="{!! url('/js/users.js')!!}"></script>
<div class="user_table">
	<form merhod="POST" action="" class="filter_form">
		<input type="text" name="user_search" value="" class="user_search" placeholder="@lang('backend_users.placeholder.user_search')"/>
		<select name="user_sort" class="user_sort">
			<option value="-1">@lang('backend_users.placeholder.user_sort')</option>
			<option value="0">@lang('backend_users.column.id')</option>
			<option value="1">@lang('backend_users.column.username')</option>
			<option value="2">@lang('backend_users.column.name')</option>
			<option value="3">@lang('backend_users.column.role')</option>
			<option value="4">@lang('backend_users.column.newsletter')</option>
			<option value="5">@lang('backend_users.column.active')</option>
			<option value="6">@lang('backend_users.column.last_login')</option>
			<option value="7">@lang('backend_users.column.logins_count')</option>
		</select>
		<input class="user_submit" type="submit" value="@lang('backend_users.button.filter')" onClick="return refreshTable();" />
        <span id="user_count"></span>
	</form>

	<table cellpadding="0" cellspacing="0" border="0" id="users">
		<thead>
			<tr>
				<th>@lang('backend_users.column.id')</th>
				<th>@lang('backend_users.column.username')</th>
				<th>@lang('backend_users.column.name')</th>
				<th>@lang('backend_users.column.role')</th>
				<th>@lang('backend_users.column.newsletter')</th>
				<th>@lang('backend_users.column.active')</th>
				<th>@lang('backend_users.column.last_login')</th>
				<th>@lang('backend_users.column.logins_count')</th>
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>
	<div class="user_edit_wrapper" style="display:none">
		<div class="user_actions">
			<a class="nav_box white edit">@lang('backend_users.edit')</a>
			<a class="nav_box white user">@lang('backend_users.add')</a>
			<a class="nav_box white remove" confirm="@lang('backend_users.remove.confirm')">@lang('backend_users.remove')</a>
			<div style="clear:both;"></div>
		</div>
		<div class="textarea_overlay"></div>
		<div id="user_content" class="user_fields" style="display:none;">
			<form id="user_form" action="" method="POST">
				<input type="hidden" name="id" value="0" />

				<div class="form_left cf" style="height:630px;">
					<!--<div class="profile_image_holder">
						<img src="{!! url('/img/backend/user_img_profile.png')!!}" />
					</div>-->
					<div class="radio_group">
						<label>@lang('backend_users.form.active')</label><br />
						<input id="user_non_active" class="first_choice" type="radio" name="active" value="0" checked="true" tabindex="1"/><label for="user_non_active"></label>
						<input id="user_active" class="second_choice" type="radio" name="active" value="1" checked="false" tabindex="2"/><label for="user_active"></label>
						<div style="clear:both;"></div>
					</div>
					<div class="radio_group" id="newsletter_group">
						<label>@lang('backend_users.form.newsletter')</label><br />
						<input id="user_non_newsletter" class="first_choice" type="radio" name="newsletter_include" value="0" checked="true" tabindex="3"/><label for="user_non_newsletter"></label>
						<input id="user_newsletter" class="second_choice" type="radio" name="newsletter_include" value="1" checked="false" tabindex="4"/><label for="user_newsletter"></label>
						<div style="clear:both;"></div>
						<input type="hidden" name="newsletter_include" value="0" disabled="disabled" id="newsletter_hidden"/>
					</div>
					<div style="clear:both;height:30px;"></div>
					<div class="system_info">
						<div class="system_info_row">
							<p class="si_label">@lang('backend_users.form.registered')</p>
							<p class="si_data" id="created_at"></p>
							<div style=clear:both;"></div>
						</div>
						<div class="system_info_row">
							<p class="si_label">@lang('backend_users.form.last_update')</p>
							<p class="si_data" id="updated_at"></p>
							<div style=clear:both;"></div>
						</div>
						<div class="system_info_row">
							<p class="si_label">@lang('backend_users.form.last_login')</p>
							<p class="si_data" id="last_login"></p>
							<div style=clear:both;"></div>
						</div>
						<div class="system_info_row">
							<p class="si_label">@lang('backend_users.form.logins_count')</p>
							<p class="si_data" id="logins"></p>
							<div style=clear:both;"></div>
						</div>
					</div>
					<div id="profile_note_comment" class="profile_note" style="left:82px;" onClick="return redirectUserComments('{!! url('/cms/comments')!!}');">
						<a title="@lang('backend.comments')" ><img src="{!! url('/img/backend/comments_icon.png')!!}" title="@lang('backend.comments')" /></a>
						<p></p>
					</div>
					<div class="profile_note" style="display: none">
						<a href="" title="@lang('backend.tasks')"><img src="{!! url('/img/backend/task_icon.png')!!}" title="@lang('backend.tasks')" /></a>
						<p>11</p>
					</div>
				</div>
				<div class="form_right cf">
					<div class="input_section">
						<div class="input_group">
							<label for="first_name">@lang('backend_users.form.first_name')<span class="fieldset_requared">*</span></label>
							<input type="text" value="" name="first_name" tabindex="5"/>
						</div>
						<div class="input_group">
							<label for="position">@lang('backend_users.form.position')</label>
							<input type="text" value="" name="position" tabindex="6"/>
						</div>
						<div class="input_group">
							<label for="country">@lang('backend_users.form.country')<span class="fieldset_requared">*</span></label>
							<input type="text" value="" name="country" tabindex="7"/>
						</div>
						<div class="input_group">
							<label for="last_name">@lang('backend_users.form.last_name')<span class="fieldset_requared">*</span></label>
							<input type="text" value="" name="last_name" tabindex="8"/>
						</div>
						<div class="input_group">
							<label for="company">@lang('backend_users.form.company')<span class="fieldset_requared">*</span></label>
							<input type="text" value="" name="company" tabindex="9"/>
						</div>
						<div class="input_group">
							<div class="input_group_medium">
								<label for="city">@lang('backend_users.form.city')</label>
								<input type="text" value="" name="city" tabindex="10"/>
							</div>
							<div class="input_group_short">
								<label for="code">@lang('backend_users.form.postal_code')</label>
								<input type="text" value="" name="code" tabindex="11"/>
							</div>
						</div>
						<div class="input_group">
							<label for="username">@lang('backend_users.form.username')<span class="fieldset_requared">*</span></label>
							<input type="text" value="" name="username" tabindex="12"/>
						</div>
						<div class="input_group">
							<label for="department">@lang('backend_users.form.department')</label>
							<input type="text" value="" name="department" tabindex="13"/>
						</div>
						<div class="input_group">
							<label for="address">@lang('backend_users.form.address')</label>
							<input type="text" value="" name="address" tabindex="14"/>
						</div>
						<div class="input_group">
							<label for="email">@lang('backend_users.form.email')<span class="fieldset_requared">*</span></label>
							<input type="text" value="" name="email" tabindex="15"/>
						</div>
						<div class="input_group">
							<label for="phone">@lang('backend_users.form.phone')</label>
							<input type="text" value="" name="phone" tabindex="16"/>
						</div>
						<div class="input_group">
							<label for="fax">@lang('backend_users.form.fax')</label>
							<input type="text" value="" name="fax" tabindex="17"/>
						</div>
						<div style="clear:both;height:50px;"></div>
						<div class="input_group">
							<label for="password">@lang('backend_users.form.password')<span class="fieldset_requared field_password">*</span></label>
							<input type="password" value="" name="password" tabindex="18" autocomplete="off"/>
						</div>
						<div class="input_group">
							<label for="password_confirmation">@lang('backend_users.form.re_password')<span class="fieldset_requared field_password">*</span></label>
							<input type="password" value="" name="password_confirmation" tabindex="19" onpaste="return false;" autocomplete="off"/>
						</div>

					</div>
					<div style="clear:both;height:61px;"></div>
					<div class="user_input_footer">
						<div class="role_section">
							<div class="role_group head">
								<label>@lang('backend_users.form.role')</label>
							</div>
							<div class="role_group">
								<input id="check_admin" type="radio" name="role" value="{!!USER_ROLE_ADMIN!!}" tabindex="20"/><label for="check_admin">@lang('backend_users.form.role.admin')</label>
							</div>
							<div class="role_group">
								<input id="check_editor" type="radio" name="role" value="{!!USER_ROLE_EDITOR!!}" tabindex="21"/><label for="check_editor">@lang('backend_users.form.role.approver')</label>
							</div>
							<div class="role_group">
								<input id="check_member" type="radio" name="role" value="{!!USER_ROLE_MEMBER!!}" tabindex="22"/><label for="check_member">@lang('backend_users.form.role.member')</label>
							</div>
							<div class="role_group">
								<input id="check_editor_smart" type="radio" name="role" value="{!!USER_ROLE_EDITOR_SMART!!}" tabindex="23"/><label for="check_editor_smart">@lang('backend_users.form.role.editor_smart')</label>
							</div>
							<div class="role_group">
								<input id="check_editor_mb" type="radio" name="role" value="{!!USER_ROLE_EDITOR_MB!!}" tabindex="24"/><label for="check_editor_mb">@lang('backend_users.form.role.editor_mb')</label>
							</div>
							<div class="role_group">
								<input id="check_editor_dfs" type="radio" name="role" value="{!!USER_ROLE_EDITOR_DFS!!}" tabindex="25"/><label for="check_editor_dfs">@lang('backend_users.form.role.editor_dfs')</label>
							</div>
                            <div class="role_group">
                                <input id="check_editor_dmo" type="radio" name="role" value="{!!USER_ROLE_EDITOR_DMO!!}" tabindex="25"/><label for="check_editor_dmo">@lang('backend_users.form.role.editor_dmo')</label>
                            </div>
							<div class="role_group">
								<input id="check_editor_dfm" type="radio" name="role" value="{!!USER_ROLE_EDITOR_DFM!!}" tabindex="26"/><label for="check_editor_dfm">@lang('backend_users.form.role.editor_dfm')</label>
							</div>
							<div class="role_group">
								<input id="check_editor_dtf" type="radio" name="role" value="{!!USER_ROLE_EDITOR_DTF!!}" tabindex="27"/><label for="check_editor_dtf">@lang('backend_users.form.role.editor_dtf')</label>
							</div>
							<div class="role_group">
								<input id="check_editor_ff" type="radio" name="role" value="{!!USER_ROLE_EDITOR_FF!!}" tabindex="28"/><label for="check_editor_ff">@lang('backend_users.form.role.editor_ff')</label>
							</div>
							<div class="role_group">
								<input id="check_editor_dp" type="radio" name="role" value="{!!USER_ROLE_EDITOR_DP!!}" tabindex="29"/><label for="check_editor_dp">@lang('backend_users.form.role.editor_dp')</label>
							</div>
							<div class="role_group">
								<input id="check_editor_tss" type="radio" name="role" value="{!!USER_ROLE_EDITOR_TSS!!}" tabindex="30"/><label for="check_editor_tss">@lang('backend_users.form.role.editor_tss')</label>
							</div>
							<div class="role_group">
								<input id="check_editor_bkk" type="radio" name="role" value="{!!USER_ROLE_EDITOR_BKK!!}" tabindex="31"/><label for="check_editor_bkk">@lang('backend_users.form.role.editor_bkk')</label>
							</div>
							<div class="role_group">
								<input id="check_editor_db" type="radio" name="role" value="{!!USER_ROLE_EDITOR_DB!!}" tabindex="32"/><label for="check_editor_db">@lang('backend_users.form.role.editor_db')</label>
							</div>
							<div class="role_group">
								<input id="check_editor_eb" type="radio" name="role" value="{!!USER_ROLE_EDITOR_EB!!}" tabindex="33"/><label for="check_editor_eb">@lang('backend_users.form.role.editor_eb')</label>
							</div>
							<div class="role_group">
								<input id="check_editor_setra" type="radio" name="role" value="{!!USER_ROLE_EDITOR_SETRA!!}" tabindex="34"/><label for="check_editor_setra">@lang('backend_users.form.role.editor_setra')</label>
							</div>
							<div class="role_group">
								<input id="check_editor_op" type="radio" name="role" value="{!!USER_ROLE_EDITOR_OP!!}" tabindex="35"/><label for="check_editor_op">@lang('backend_users.form.role.editor_op')</label>
							</div>
							<div class="role_group">
								<input id="check_editor_bs" type="radio" name="role" value="{!!USER_ROLE_EDITOR_BS!!}" tabindex="36"/><label for="check_editor_bs">@lang('backend_users.form.role.editor_bs')</label>
							</div>
							<div class="role_group">
								<input id="check_editor_fuso" type="radio" name="role" value="{!!USER_ROLE_EDITOR_FUSO!!}" tabindex="37"/><label for="check_editor_fuso">@lang('backend_users.form.role.editor_fuso')</label>
							</div>
							<div class="role_group">
								<input id="check_editor_dt" type="radio" name="role" value="{!!USER_ROLE_EDITOR_DT!!}" tabindex="38"/><label for="check_editor_dt">@lang('backend_users.form.role.editor_dt')</label>
							</div>
                            
							<div class="role_group">
								<input id="check_newsletter_approver" type="radio" name="role" value="{!!USER_ROLE_NEWSLETTER_APPROVER!!}" tabindex="38"/><label for="check_newsletter_approver">@lang('backend_users.form.role.newsletter_approver')</label>
							</div>
							<div class="role_group">
								<input id="check_newsletter_editor" type="radio" name="role" value="{!!USER_ROLE_NEWSLETTER_EDITOR!!}" tabindex="39"/><label for="check_newsletter_editor">@lang('backend_users.form.role.newsletter_editor')</label>
							</div>
							<!--<div class="role_group">
								<input id="check_exporter" type="radio" name="role" value="{!!USER_ROLE_EXPORTER!!}" tabindex="23"/><label for="check_exporter">@lang('backend_users.form.role.exporter')</label>
							</div>-->
						</div>
						<div class="role_section">
							<div class="role_group head">
								<label>@lang('backend_users.form.group')</label>
							</div>
							<?php $count = 33; ?>
							@foreach($GROUPS as $group)
							<div class="role_group">
								<input id="check_group_{!!$group->id!!}" type="checkbox" name="group" value="{!!$group->id!!}" tabindex="{!!$count++!!}"/><label for="check_group_{!!$group->id!!}">{!!$group->name!!}</label>
							</div>

							@endforeach
						</div>
					</div>
					<div style="height: 51px;"></div>
					<a class="nav_box white save" href="#" tabindex="{!!$count++!!}">@lang('backend.save')</a>
					<a class="nav_box white cancel" href="#" tabindex="{!!$count++!!}">@lang('backend.cancel')</a>
				</div>
			</form>
		</div>
	</div>

</div>
@stop

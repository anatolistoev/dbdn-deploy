@extends('layouts.backend')
@section('left_navigation')
    <a @if(Session::get('lang') == LANG_EN) href="{!! url('/cms/newsletter?lang=de')!!}" style="background-image:url('{!! url('/img/backend/change_lang_icon_EN.png')!!}');" @else href="{!! url('/cms/newsletter?lang=en')!!}" style="background-image:url('{!! url('/img/backend/change_lang_icon_DE.png')!!}');"@endif class="nav_box" >@lang('backend.change_lang')</a>
    <a href="{!! url('/cms/newsletter_users')!!}" class="nav_box @if($ACTIVE == 'newsletter_users')active @endif" style="background-image:url('{!! url('/img/backend/user_icon.png')!!}');">@lang('backend.newsletter_users')</a>
	<a href="{!! url('/cms/newsletter_users_test')!!}" class="nav_box @if($ACTIVE == 'newsletter_users_test')active @endif" style="background-image:url('{!! url('/img/backend/user_icon.png')!!}');">@lang('backend.newsletter_test_users')</a>
@stop
@section('user_content')
<script>
	var lang = '{!!$LANG!!}';
	var lang_label;
	if(lang == 1){
		lang_label = 'en';
	}else{
		lang_label = 'de';
	}
	// Configuration for edit of page , downloads and image
    var EDIT_MAX_TIME_SECONDS = {!!Config::get('app.edit_max_time_seconds')!!};
	var EDIT_TIME_EXTENSION_ALERT_SECONDS = {!!Config::get('app.edit_time_extension_alert_seconds')!!};
	var EDIT_COMPLEATED_ALERT_SECONDS = {!!Config::get('app.edit_compleated_alert_seconds')!!};
	var EDITING_USER = {!!Auth::user()->id!!};
</script>
<script type="text/javascript" src="{!! url('/js/newsletter.js')!!}"></script>
<script type="text/javascript" src="{!! url('/js/newsletter_properties.js')!!}"></script>
<script type="text/javascript" src="{!! url('/js/editing_timer.js')!!}"></script>
<div class="user_table">
	<form merhod="POST" action="" class="filter_form">
		<input type="text" name="user_search" value="" class="user_search" placeholder="@lang('backend_newsletter.placeholder.newsletter_search')"/>
		<select name="user_sort" class="user_sort">
			<option value="-1">@lang('backend_newsletter.placeholder.newsletter_sort')</option>
			<option value="0">@lang('backend_newsletter.column.id')</option>
			<option value="1">@lang('backend_newsletter.column.name')</option>
            <option value="3">@lang('backend_newsletter.column.status')</option>
		</select>
		<input class="user_submit" type="submit" value="@lang('backend_comments.button.filter')" onClick="return refreshTable();" />
	</form>

	<table cellpadding="0" cellspacing="0" border="0" id="newsletters">
		<thead>
			<tr>
				<th>@lang('backend_newsletter.column.name')</th>
				<th>@lang('backend_newsletter.column.id')</th>
                <th>@lang('backend_newsletter.column.editing_state')</th>
                <th>@lang('backend_newsletter.column.status')</th>
				<th>@lang('backend_newsletter.column.last_modified')</th>
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>
	<div class="user_edit_wrapper" style="display:none">
		<div class="user_actions">
			<a class="nav_box white edit">@lang('backend_newsletter.edit')</a>
			<a class="nav_box white add_newsletter">@lang('backend_newsletter.add')</a>
            <a class="nav_box white copy">@lang('backend_newsletter.copy')</a>
            <a class="nav_box white properties">@lang('backend_newsletter.properties')</a>
			<a class="nav_box white remove" confirm="@lang('backend_newsletter.remove.confirm')">@lang('backend_newsletter.remove')</a>
			<div style="clear:both;height:1px;"></div>
            @if(in_array(Auth::user()->role, Config::get('auth.CMS_NL_EDITOR')))
            <a class="nav_box white test">@lang('backend_newsletter.test')</a>
            @endif
            @if(in_array(Auth::user()->role, Config::get('auth.CMS_NL_APPROVER')))
            <a class="nav_box white package">@lang('backend_newsletter.package')</a>
            <a class="nav_box white send">@lang('backend_newsletter.send')</a>
            @endif
			<div style="clear:both;"></div>
		</div>

		@include('partials_backend.newsletter_properties')
        <div id="test_email_wrapper" style="display: none;">
            <form id="test_newsletter_form" action="" method="POST">
                <input type="hidden" name="id" value="0" />
                <div class="form_left" style="height:315px;padding-top:30px;">
					<fieldset>
						<div class="input_group">
							<legend>Receivers:</legend>
							<br>
							<div class="role_group" >
								<input id="receivers_emails" type="radio" name="receivers" value="true" 
									tabindex="2" checked="checked" 
									onClick="$('textarea[name=\'test_emails\']').parent().show();"/>
								<label class="normal_label" for="receivers_emails">Test emails</label>
							</div>
							<div class="role_group">
								<input id="receivers_test_subscribers" type="radio" name="receivers" value="false" 
									tabindex="3" 
									onClick="$('textarea[name=\'test_emails\']').parent().hide();"/>
								<label class="normal_label" for="receivers_test_subscribers">Test Subscribers</label>
							</div>
						</div>
					</fieldset>
				</div>
                <div class="form_right" style="height:345px;">
                    <fieldset class="title">
                        <div class="input_group">
                            <label for="test_emails">@lang('backend_newsletter.form.test_emails')<span class="fieldset_requared">*</span></label>
                            <textarea value="" name="test_emails" tabindex="17"></textarea>
                        </div>
                    </fieldset>
                    <div style="clear:both;height:42px;"></div>
                    <a class="nav_box white test" href='#' tabindex="3">@lang('backend_newsletter.test')</a>
                    <a class="nav_box white cancel" href='#' tabindex="4">@lang('backend.cancel')</a>

                </div>
                <div style="clear:both;"></div>
            </form>
        </div>

	</div>

</div>
@stop

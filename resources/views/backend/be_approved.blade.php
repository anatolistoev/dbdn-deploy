@extends('layouts.backend')

@section('user_content')
<div class="page_error">
    <br>
    <br>
    <p>This page is approved and you cannot edit it.</p>
</div>
@stop
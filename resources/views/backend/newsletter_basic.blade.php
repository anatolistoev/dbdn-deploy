@extends('layouts.backend_newsletter')
@section('bodyClass'){!! 'basic' !!}@stop
@section('edit_scripts')
<script type="text/javascript" src="{!! url('/js/editing_timer.js')!!}"></script>
<script>

	// Configuration for edit of page , downloads and image
    var EDIT_MAX_TIME_SECONDS = {!!Config::get('app.edit_max_time_seconds')!!};
	var EDIT_TIME_EXTENSION_ALERT_SECONDS = {!!Config::get('app.edit_time_extension_alert_seconds')!!};
	var EDIT_COMPLEATED_ALERT_SECONDS = {!!Config::get('app.edit_compleated_alert_seconds')!!};

	var EDITING_USER = {!!Auth::user()->id!!};
$(document).ready(function(){
	var change_workflow_responsible = function(){
		@if($CHANGE_WORKFLOW_RESPONSIBLE)
		jConfirm(js_localize['pages.edit.workflow.responsible'],js_localize['pages.edit.title'], 'success',function(r){
			if(r){
				changeNewsletterResponsible({!!$NEWSLETTER->id!!},{!!Auth::User()->id!!});
			}else{
				var statusPromise = unlockItem($('div#content_edit_newsletter_id').html(),'newsletter');
                statusPromise.always(function() {
					window.location.href = relPath + 'cms/newsletter';
                });
			}
		});
		@endif
	}

    editing_timer.start(
        {!!$TIME_LEFT!!},
        function(){
            //Close all possible opened forms
            $(".mce-close").click();
            $("#tree_wrapper #popup_close img").click();
            $(".back_to_view").click();
            var promise = savePage($("#page_save")[0]);
            promise.always(function(){
                window.location.href = relPath + 'cms/newsletter';
            });
        },
        function(){
             var statusPromise = unlockItem($('div#content_edit_newsletter_id').html(),'newsletter');
             statusPromise.always(function(){
                window.location.href = relPath + 'cms/newsletter';
            });
        },
        {!!$NEWSLETTER->id!!},
        'newsletter',
        '',
        EDIT_MAX_TIME_SECONDS
    );


		var time_left = {!!$TIME_LEFT!!};
		if(time_left > EDIT_TIME_EXTENSION_ALERT_SECONDS){
			jAlert(
                js_localize['pages.edit.start']
                    .replace('{time_left}', editing_timer.formatTimeInMinutes(time_left))
                    .replace('{alert_minutes}', editing_timer.formatTimeInMinutes(EDIT_TIME_EXTENSION_ALERT_SECONDS)),
                js_localize['pages.edit.title'],
                'success',
                change_workflow_responsible
            );
		}
});
</script>
@stop
@section('left_navigation')
<a id="change_lang" @if(Session::get('lang') == LANG_EN) href="{!! url('/cms/newsletter/'.$NEWSLETTER->id.'?lang=de')!!}" class="change_lang_en  @else href="{!! url('/cms/newsletter/'.$NEWSLETTER->id.'?lang=en')!!}" class="change_lang_de @endif  nav_box" >@lang('backend.change_lang')</a>
<a class="nav_box properties" >@lang('backend.properties')</a>
<a class="nav_box copy">@lang('backend.copy_footers')</a>
@stop
@section('banner_contents')
	<div class="RSTitle" style="background-color:#3683AB;">@lang('backend.header_d')</div>
	<div class="content_header_d" content_id="{!!$CONTENTIDS['header_d']!!}">
		<div class="modify_overlay" style="display:none;"></div>
		<div class="modify_wrapper" style="display:none;width:100%;">
			<a class="nav_box white edit">@lang('backend_pages.edit')</a>
			<a class="nav_box white back_to_view" style="float:right;display:none;">Back to view</a>
			<div style="clear:both;height:1px;"></div>
			<textarea style="height:200px;display:none;" name="header_d_body" id="header_d_body"></textarea>
		</div>
		<div id="content_header_d_text">
			{!!$CONTENTS['header_d']!!}
		</div>
	</div>
	<div style="clear:both;height:5px;background-color:#3683ab;"></div>
	<div class="RSTitle" style="background-color:#3683AB;">@lang('backend.header_m')</div>
	<div class="content_header_m" content_id="{!!$CONTENTIDS['header_m']!!}">
		<div class="modify_overlay" style="display:none;"></div>
		<div class="modify_wrapper" style="display:none;width:100%;">
			<a class="nav_box white edit">@lang('backend_pages.edit')</a>
			<a class="nav_box white back_to_view" style="float:right;display:none;">Back to view</a>
			<div style="clear:both;height:1px;"></div>
			<textarea style="height:200px;display:none;" name="header_m_body" id="header_m_body"></textarea>
		</div>
		<div id="content_header_m_text">
			{!!$CONTENTS['header_m']!!}
		</div>
	</div>
@stop
@section('path')
@stop
@section('main_contents')
<div class="textarea_overlay"></div>

<div class="RSTitle">@lang('backend.main')</div>
<div id="frontend_div" class="content_content" content_id="{!!$CONTENTIDS['content']!!}">
    <div class="modify_overlay" style="display:none;"></div>
    <div class="modify_wrapper" style="display:none;width:100%;">
        <a class="nav_box white edit">@lang('backend_pages.edit')</a>
        <a class="nav_box white back_to_view" style="float:right;display:none;">Back to view</a>
        <div style="clear:both;height:1px;"></div>
        <textarea style="height:600px;display:none;" name="content_body" id="content_body"></textarea>
    </div>
    <div id="content_content_text" class="content_content_text">
		{!!trim($CONTENTS['content'],'&nbsp;')!!}
    </div>
</div>
<div style="clear:both;height:5px;background-color:#3683ab;"></div>
<div class="RSTitle">@lang('backend.footer_internal')</div>
<div class="content_footer_i" content_id="{!!$CONTENTIDS['footer_i']!!}">
	<div class="modify_overlay" style="display:none;"></div>
	<div class="modify_wrapper" style="display:none;width:100%;">
		<a class="nav_box white edit">@lang('backend_pages.edit')</a>
		<a class="nav_box white back_to_view" style="float:right;display:none;">Back to view</a>
		<div style="clear:both;height:1px;"></div>
		<textarea style="height:400px;display:none;" name="footer_i_body" id="footer_i_body"></textarea>
	</div>
	<div id="content_footer_i_text" class="content_content_text">
		{!!$CONTENTS['footer_i']!!}
	</div>
</div>
<div style="clear:both;height:5px;background-color:#3683ab;"></div>
<div class="RSTitle">@lang('backend.footer_external')</div>
<div class="content_footer_e" content_id="{!!$CONTENTIDS['footer_e']!!}">
	<div class="modify_overlay" style="display:none;"></div>
	<div class="modify_wrapper" style="display:none;width:100%;">
		<a class="nav_box white edit">@lang('backend_pages.edit')</a>
		<a class="nav_box white back_to_view" style="float:right;display:none;">Back to view</a>
		<div style="clear:both;height:1px;"></div>
		<textarea style="height:400px;display:none;" name="footer_e_body" id="footer_e_body"></textarea>
	</div>
	<div id="content_footer_e_text" class="content_content_text">
		{!!$CONTENTS['footer_e']!!}
	</div>
</div>
<div class="RSTitle">@lang('backend.footer_bilingual')</div>
<div class="content_footer_e_bl" content_id="{!!$CONTENTIDS['footer_e_bl']!!}">
	<div class="modify_overlay" style="display:none;"></div>
	<div class="modify_wrapper" style="display:none;width:100%;">
		<a class="nav_box white edit">@lang('backend_pages.edit')</a>
		<a class="nav_box white back_to_view" style="float:right;display:none;">Back to view</a>
		<div style="clear:both;height:1px;"></div>
		<textarea style="height:400px;display:none;" name="footer_e_bl_body" id="footer_e_bl_body"></textarea>
	</div>
	<div id="content_footer_e_bl_text" class="content_content_text">
		{!!$CONTENTS['footer_e_bl']!!}
	</div>
</div>
@stop

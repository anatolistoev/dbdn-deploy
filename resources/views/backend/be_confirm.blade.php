<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="text/html; charset=UTF-8" http-equiv="content-type">
        <meta name="viewport" content="width=1280">
        <meta name="MobileOptimized" content="1280">
		<title>Daimler Brand &amp; Design Navigator</title>
		@include('partials_backend.backend_global_insert')
		@section('scripts')
		<script type="text/javascript" src="{!! url('/js/jquery.dataTables.js')!!}"></script>
		<script type="text/javascript" src="{!! url('/js/jquery.dataTables_extensions.js')!!}"></script>
		<script>
            $(function(event) {
                if($('#admin_action').length != 0){
                    if($('.admin_data').hasClass('editMode'))
                    {
                        $('.admin_data').removeClass('editMode');
                    }
                    $("#admin_info").click(function(){
                        if($('#admin_action').is(":visible"))
                        {
                            $("#admin_info").removeClass('active');
                            $('#admin_action').hide();
                        }else{
                            $("#admin_info").addClass('active');
                            $('#admin_action').show();
                        }
                    });
                }else{
                    $('.admin_data').addClass('editMode');
                }
            });
        </script>
		@endsection
        @yield('scripts')
		@section('styles')
		<link rel="stylesheet" href="{!! url('/css/jquery.dataTables.css')!!}" />
		@endsection
		<script src="{!! url('/js/jquery.ui.draggable.js')!!}" type="text/javascript"></script>
	</head>
	<body>
		<div class="wrapper" style="position:relative;">
			<div class="page_title">@lang('backend.page_title')</div>
			@section('admin_info')
			<div id="admin_info">
				 <div class="admin_data">
						<p class="admin_name">{!!Auth::user()->first_name!!} {!!Auth::user()->last_name!!}</p>
						<p>
							@if(Auth::user()->role == USER_ROLE_EDITOR)
							 @lang('backend_users.form.role.approver')
							@elseif(Auth::user()->role == USER_ROLE_ADMIN)
							 @lang('backend_users.form.role.admin')
                            @elseif(Auth::user()->role == USER_ROLE_NEWSLETTER_APPROVER)
                             @lang('backend_users.form.role.newsletter_approver')
                            @elseif(Auth::user()->role == USER_ROLE_NEWSLETTER_EDITOR)
                             @lang('backend_users.form.role.newsletter_editor')
							@else
							 @lang('backend_users.form.role.member')
							@endif
						</p>
                </div>
			</div>
			@endsection
			@yield('admin_info')
            @section('top_navigation')
			<div id="admin_action" style="display: none; height: 80px; position: absolute;right: 0;top: 84px;">
				<a href="{!! url('/cms/logout')!!}" title="Logout" class="nav_box white logout">Logout</a>
			</div>
			@endsection
            @yield('top_navigation')
			<div class="content_wrapper">
				<div class="login_info">
					<p><b>Security check to access the Daimler Brand & Design Navigator CMS.</b></p>
					<p>Due to security restrictions, we have sent you a confirmation e-mail containing a verification code. Please enter the verification code in the text field and click the Verify Me button in order to access the Daimler Brand & Design Navigator CMS.</p>
				</div>
				<form class="login_form" style="margin-top: 25px;" action="{!! url('/cms/confirm')!!}" method="POST">
					{!! csrf_field() !!}
					<div class="login_error">
					@if($errors->first('confirm_code'))
					{!!$errors->first('confirm_code')!!}
					@endif
					</div>
					<input class="login_input" type="text" name="confirm_code" value="" placeholder="Enter Verification Code"/>
					<input class="user_submit" type="submit" name="confirm_submit" value="Verify Me" />
				</form>
			</div>
			<div style="clear:both;"></div>
		</div>
	</body>
</html>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Daimler Brand &amp; Design Navigator</title>
		@include('partials_backend.backend_global_insert')
		@section('scripts')
		<script type="text/javascript" src="{!! url('/js/jquery.dataTables.js')!!}"></script>
		<script type="text/javascript" src="{!! url('/js/jquery.dataTables_extensions.js')!!}"></script>
		<script type="text/javascript" src="{!! url('/js/functions.js')!!}"></script>
        <script type="text/javascript" src="{!! asset('/js/jquery-ui.min.js') !!}"></script>
		@show
		@section('styles')
		<link rel="stylesheet" href="{!! url('/css/jquery.dataTables.css')!!}" />
		@show
		@include('partials_backend.js_localize')
	</head>
	<body>
		<div class="wrapper">
			<div class="content_wrapper">
@include('partials_backend.filemanager')
			</div>
			<div style="clear:both;"></div>
		</div>
	</body>
</html>

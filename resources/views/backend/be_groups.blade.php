@extends('layouts.backend')
@section('left_navigation')
<a href="{!! url('/cms/comments')!!}" class="nav_box @if($ACTIVE == 'comments')active @endif" style="background-image:url('{!! url('/img/backend/comments_icon.png')!!}');margin-top:80px;">@lang('backend.comments')</a>
<a href="{!! url('/cms/history')!!}" class="nav_box @if($ACTIVE == 'history')active @endif" style="background-image:url('{!! url('/img/backend/history_icon.png')!!}')">@lang('backend.history')</a>		
@stop
@section('user_content')
<script type="text/javascript" src="{!! url('/js/groups.js')!!}"></script>
<div class="user_table">
	<form merhod="POST" action="" class="filter_form">
		<input type="text" name="user_search" value="" class="user_search" placeholder="@lang('backend_groups.placeholder.group_search')"/>
		<select name="user_sort" class="user_sort">
			<option value="-1">@lang('backend_groups.placeholder.group_sort')</option>
			<option value="0">@lang('backend_groups.column.id')</option>
			<option value="1">@lang('backend_groups.column.name')</option>
		</select>
		<input class="user_submit" type="submit" value="@lang('backend_groups.button.filter')" onClick="return refreshTable();" />
	</form>

	<table cellpadding="0" cellspacing="0" border="0" id="groups">
		<thead>
			<tr>
				<th>@lang('backend_groups.column.id')</th>
				<th>@lang('backend_groups.column.name')</th>						
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>	
	<div class="user_edit_wrapper" style="display:none">
		<div class="user_actions">
			<a class="nav_box white edit">@lang('backend_groups.edit')</a>
			<a class="nav_box white add_group">@lang('backend_groups.add')</a>
			<a class="nav_box white remove" confirm="@lang('backend_groups.remove.confirm')">@lang('backend_groups.remove')</a>
			<div style="clear:both;"></div>
		</div>
		<div class="textarea_overlay"></div>
		<div class="user_fields" id="wrapper_form_groups" style="display:none;">
			<form id="user_form" action="" method="POST">
				<input type="hidden" name="id" value="0" />
				<div class="form_left" style="height: 258px;">
					<div class="system_info">
						<div class="system_info_row">
							<p class="si_label">@lang('backend_groups.form.registered')</p>
							<p class="si_data" id="created_at"></p>
							<div style=clear:both;"></div>
						</div>
						<div class="system_info_row">
							<p class="si_label">@lang('backend_groups.form.last_update')</p>
							<p class="si_data" id="updated_at"></p>
							<div style=clear:both;"></div>
						</div>
					</div>
				</div>
				<div class="form_right">
					<div class="input_section">
						<div class="input_group">
							<label for="name">@lang('backend_groups.form.name')<span class="fieldset_requared">*</span></label>
							<input type="text" value="" name="name" tabindex="1"/>
						</div>
						<div style="clear:both;height:43px;"></div>
					</div>
					<div style="clear:both;height:43px;"></div>
					<a class="nav_box white save" href="#" tabindex="2" data-is-saving="false">@lang('backend.save')</a>
					<a class="nav_box white cancel" href="#" tabindex="3">@lang('backend.cancel')</a>
				</div>
				<div style="clear:both;"></div>
			</form>
		</div>
	</div>

</div>
@stop
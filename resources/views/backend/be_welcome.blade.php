@extends('layouts.backend')
@section('user_content')
<div class="content cf">
	<script>
		var lang = '{!!$LANG!!}';
		var lang_label;
		if(lang == 1){
			lang_label = 'en';
		}else{
			lang_label = 'de';
		}
		// Configuration for edit of page , downloads and image
		var EDIT_MAX_TIME_SECONDS = {!!Config::get('app.edit_max_time_seconds')!!};
		var EDIT_TIME_EXTENSION_ALERT_SECONDS = {!!Config::get('app.edit_time_extension_alert_seconds')!!};
		var EDIT_COMPLEATED_ALERT_SECONDS = {!!Config::get('app.edit_compleated_alert_seconds')!!};

		var EDITING_USER = {!!Auth::user()->id!!};
		var NEWSLETTER_USER_ONLY = {{ json_encode(!\App\Helpers\UserAccessHelper::hasAccess('pages')) }};

	</script>
	<script type="text/javascript" src="{!! url('/js/pages.js')!!}"></script>
	<script type="text/javascript" src="{!! url('/js/page_properties.js')!!}"></script>
	<script type="text/javascript" src="{!! url('/js/editing_timer.js')!!}"></script>
	<script type="text/javascript" src="{!! url('/js/welcome.js')!!}"></script>
	<div class="welcome">
		<h2 class="tasks_title">@lang('backend_pages.my_tasks')</h2>
		<div class="user_table welcome_table">
		{{-- <form merhod="POST" action="" class="filter_form">
			<input type="text" name="user_search" value="" class="user_search" placeholder="@lang('backend_pages.placeholder.page_search')"/>
			<select name="user_sort" class="user_sort">
				<option value="-1">@lang('backend_pages.placeholder.page_sort')</option>
				<option value="0">@lang('backend_pages.column.title')</option>
				<option value="1">@lang('backend_pages.column.id')</option>
				<option value="2">@lang('backend_pages.column.type')</option>
				<option value="3">@lang('backend_pages.column.status')</option>
				<option value="4">@lang('backend_pages.column.status')</option>
			</select>
			<input class="user_submit" type="submit" value="@lang('backend_pages.button.filter')" onClick="return refreshTable(event);" />
		</form> --}}

		<table cellpadding="0" cellspacing="0" border="0" id="pages" class="content_table">
			<thead>
				<tr>
					<th>@lang('backend_pages.column.title')</th>
					<th>@lang('backend_pages.column.version')</th>
					<th>@lang('backend_pages.column.id')</th>
					<th>@lang('backend_pages.column.assigned')</th>
					<th>@lang('backend_pages.column.editing_state')</th>
					<th>@lang('backend_pages.column.due_date')</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>

		<div class="user_edit_wrapper" style="display:none">
			<div class="user_actions">
				<a class="nav_box white edit">@lang('backend_pages.edit')</a>
				<a class="nav_box white properties">@lang('backend_pages.properties')</a>
				<div style="clear:both;"></div>
			</div>


			@include('partials_backend.page_properties')

		</div>

		</div>

	</div>
	<div class="support" style="display:none;">
		<h2 class="messages_title">@lang('backend_pages.page.moderations')</h2>
		<div id="moderation_template" style="display:none;">
			<div class="page_moderation">
				<h6 class="moderation_by">@lang('backend_pages.moderations.from')</h6>
				<span class="moderation_date"></span>
				<p></p>
			</div>
		</div>
		<div class="moderation_holder"></div>
	</div>
</div>
@stop
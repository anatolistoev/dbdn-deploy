@extends('layouts.backend')
@section('left_navigation')
<a @if(Session::get('lang') == LANG_EN) href="{!! url('/cms/glossary?lang=de')!!}" style="background-image:url('{!! url('/img/backend/change_lang_icon_EN.png')!!}');" @else href="{!! url('/cms/glossary?lang=en')!!}" style="background-image:url('{!! url('/img/backend/change_lang_icon_DE.png')!!}');"@endif class="nav_box" >@lang('backend.change_lang')</a>
@stop
@section('user_content')
<script>
	var lang = '{!!$LANG!!}';
</script>
<script type="text/javascript" src="{!! url('/js/glossary.js')!!}"></script>
<div class="user_table">
	<form merhod="POST" action="" class="filter_form">
		<input type="text" name="user_search" value="" class="user_search" placeholder="@lang('backend_glossary.placeholder.glossary_search')"/>
		<select name="user_sort" class="user_sort">
			<option value="-1">@lang('backend_glossary.placeholder.glossary_sort')</option>
			<option value="0">@lang('backend_glossary.column.id')</option>
			<option value="1">@lang('backend_glossary.column.name')</option>
		</select>
		<input class="user_submit" type="submit" value="@lang('backend_comments.button.filter')" onClick="return refreshTable();" />
	</form>

	<table cellpadding="0" cellspacing="0" border="0" id="glossary">
		<thead>
			<tr>
				<th>@lang('backend_glossary.column.id')</th>
				<th>@lang('backend_glossary.column.name')</th>
				<th>@lang('backend_glossary.column.modified_at')</th>
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>	
	<div class="user_edit_wrapper" style="display:none">
		<div class="user_actions">
			<a class="nav_box white edit">@lang('backend_glossary.edit')</a>
			<a class="nav_box white add">@lang('backend_glossary.add')</a>
			<a class="nav_box white remove" confirm="@lang('backend_glossary.remove.confirm')">@lang('backend_glossary.remove')</a>
			<div style="clear:both;"></div>
		</div>
		<div class="textarea_overlay"></div>
		<div class="user_fields" id="wrapper_form_glossary" style="display:none;">
			<form id="glossary_form" action="" method="POST">
				<input type="hidden" name="id" value="0" />
				<input type="hidden" name="lang" value="{!!$LANG!!}" />
				<div class="form_left" style="height:503px;padding-top:30px;">
					<div style="clear:both;height:3px;"></div>
					<div class="system_info">
						<div class="system_info_row">
							<p class="si_label">@lang('backend_glossary.form.created_at')</p>
							<p class="si_data" id="created_at"></p>
							<div style=clear:both;"></div>
						</div>
						<div class="system_info_row">
							<p class="si_label">@lang('backend_glossary.form.modified_at')</p>
							<p class="si_data" id="updated_at"></p>
							<div style=clear:both;"></div>
						</div>
					</div>
				</div>
				<div class="form_right" style="height:503px;">
					<div class="input_section">
						<div class="input_group">
							<label for="name">@lang('backend_glossary.form.name')<span class="fieldset_requared">*</span></label>
							<input type="text" value="" name="name" style="width:400px" tabindex="1"/>
						</div>
						<div style="clear:both;"></div>
						<div class="input_group">
							<label for="description">@lang('backend_glossary.form.description')<span class="fieldset_requared">*</span></label>
							<textarea name="description" style="width:400px;height:200px;" tabindex="2"></textarea>
						</div>
					</div>
					<div style="clear:both;height:80px;"></div>
					<a class="nav_box white save" href='#' tabindex="3">@lang('backend.save')</a>
					<a class="nav_box white cancel" href='#' tabindex="4">@lang('backend.cancel')</a>

				</div>
				<div style="clear:both;"></div>
			</form>
		</div>
	</div>

</div>
@stop
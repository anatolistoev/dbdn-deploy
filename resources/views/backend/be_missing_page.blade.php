@extends('layouts.backend')

@section('user_content')
<div class="page_error">
	<br>
	<b>A problem has occurred.</b>
	<br>
	<br>
	You are trying to access a wrong page redirect or the page does not longer exist.
	Please try again.
</div>
@stop
@extends('layouts.backend')
@section('left_navigation')
<a href="#" class="nav_box recycle" >@lang('backend.recycle_bin')</a>
<a href="#" class="nav_box clear_cache" >@lang('backend.clear_cache')</a>
@stop
@section('user_content')
@include('partials_backend.filemanager')
@stop

@extends('layouts.backend')
@section('left_navigation')
<a href="#" class="nav_box recycle" >@lang('backend.recycle_bin')</a>
@if(\App\Helpers\UserAccessHelper::isAdmin())<a href="#" class="nav_box clear_cache" >{!!trans('backend.clear_cache')!!}</a>@endif
@stop
@section('user_content')
@include('partials_backend.filemanager')
@stop

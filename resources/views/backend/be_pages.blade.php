@extends('layouts.backend')
@section('left_navigation')
<a href="{!! url('/cms/recycle_bin')!!}" class="nav_box recycle @if($ACTIVE == 'recycle_bin')active @endif" >@lang('backend.recycle_bin')</a>
<a @if(Session::get('lang') == LANG_EN) href="{!! url('/cms/pages?lang=de')!!}" style="background-image:url('{!! url('/img/backend/change_lang_icon_EN.png')!!}');" @else href="{!! url('/cms/pages?lang=en')!!}" style="background-image:url('{!! url('/img/backend/change_lang_icon_DE.png')!!}');"@endif class="nav_box" >@lang('backend.change_lang')</a>
<a href="#" class="nav_box clear_cache" >@lang('backend.clear_cache')</a>
@stop
@section('user_content')
<script>
	var lang = '{!!$LANG!!}';
	var lang_label;
	if(lang == 1){
		lang_label = 'en';
	}else{
		lang_label = 'de';
	}
	// Configuration for edit of page , downloads and image
    var EDIT_MAX_TIME_SECONDS = {!!Config::get('app.edit_max_time_seconds')!!};
	var EDIT_TIME_EXTENSION_ALERT_SECONDS = {!!Config::get('app.edit_time_extension_alert_seconds')!!};
	var EDIT_COMPLEATED_ALERT_SECONDS = {!!Config::get('app.edit_compleated_alert_seconds')!!};
	var EDITING_USER = {!!Auth::user()->id!!};
</script>
<script type="text/javascript" src="{!! url('/js/pages.js')!!}"></script>
<script type="text/javascript" src="{!! url('/js/page_properties.js')!!}"></script>
<script type="text/javascript" src="{!! url('/js/editing_timer.js')!!}"></script>
<script type="text/javascript" src="{!! url('/js/jQuery-File-Upload-9.10.6/js/jquery.iframe-transport.js')!!}"></script>
<script type="text/javascript" src="{!! url('/js/jQuery-File-Upload-9.10.6/js/jquery.fileupload.js')!!}"></script>
<div class="page_move_div"></div>
<div class="user_table">
	<form merhod="POST" action="" class="filter_form">
		<input type="text" name="user_search" value="" class="user_search" placeholder="@lang('backend_pages.placeholder.page_search')"/>
		<select name="user_sort" class="user_sort">
			<option value="-1">@lang('backend_pages.placeholder.page_sort')</option>
			<option value="0">@lang('backend_pages.column.title')</option>
			<option value="1">@lang('backend_pages.column.id')</option>
			<option value="2">@lang('backend_pages.column.type')</option>
			<option value="3">@lang('backend_pages.column.status')</option>
			<option value="4">@lang('backend_pages.column.access')</option>
			<option value="5">@lang('backend_pages.column.last_modified')</option>
		</select>
		<input class="user_submit" type="submit" value="@lang('backend_pages.button.filter')" onClick="return refreshTable(event);" />
	</form>

	<table cellpadding="0" cellspacing="0" border="0" id="pages" class="content_table">
		<thead>
			<tr>
				<th>@lang('backend_pages.column.title')</th>
				<th>@lang('backend_pages.column.id')</th>
<!--				<th>@lang('backend_pages.column.type')</th>-->
				<th>@lang('backend_pages.column.status')</th>
				<th>@lang('backend_pages.column.access')</th>
				<th>@lang('backend_pages.column.last_modified')</th>
                <th></th>
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>

	<div class="user_edit_wrapper" style="display:none">
		<div class="user_actions">
			<a class="nav_box white view_draft">@lang('backend_pages.view_draft')</a>
			<a class="nav_box white add_before">@lang('backend_pages.add_before')</a>
			<a class="nav_box white add_after">@lang('backend_pages.add_after')</a>
			<a class="nav_box white add_child">@lang('backend_pages.add_as_child')</a>
			<a class="nav_box white remove" confirm="@lang('backend_pages.remove.confirm')">@lang('backend_pages.remove')</a>
<!--			<a class="nav_box white publish">@lang('backend_pages.publish')</a>-->
			<a class="nav_box white unpublish">@lang('backend_pages.unpublish')</a>
            <a class="nav_box white revisions">@lang('backend.revisions')</a>
			<div style="clear:both;height:1px;"></div>
			<a class="nav_box white properties">@lang('backend_pages.properties')</a>
			<a class="nav_box white move_up">@lang('backend_pages.move_up')</a>
			<a class="nav_box white move_down">@lang('backend_pages.move_down')</a>
			<a class="nav_box white move_cursor">@lang('backend_pages.move_cursor')</a>
			<a class="nav_box white copy">@lang('backend_pages.copy')</a>
			<a class="nav_box white copy_all">@lang('backend_pages.copy_all')</a>
			<div style="clear:both;"></div>
		</div>


		@include('partials_backend.page_properties')

        <div id="revision_edit_wrapper" style="display:none;">
            <div class="textarea_overlay"></div>
            <div id="update_alert_wrapper" style="display:none;">
                @include('partials_backend.page_update_alert')
            </div>
            <div id='pdf_upload_wrapper' style="display:none;position: relative;">
                <div class="textarea_overlay" style="z-index: 140;"></div>
                <div class="user_fields" id="wrapper_form_pdf_upload" style="z-index: 141;">
                    <div id="pdf_upload_form" action="" method="POST" style = "display:flex">
                        <div class="form_left">
                        </div>
                         <div class="form_right cf" style="height: 100%">
                             <fieldset>
                                <div class="input_group">
                                    <label id="label_for_file_input" for="label_file_input">File</label>
                                    <img class="thumb_remove" src="{!! url('/img/backend/delete_icon.png')!!}" onClick="$('#label_file_input').val(''); $('#file_input').val('');"/>
                                    <input id="label_file_input" readonly spellcheck="false" value=""  name="file_title">
                                    <div class="btn_input_files">
                                        <button type="button" id="btn_uploadImg" class="btn_upload_icon"  onclick="$('#file_input').click();"></button>
                                        <div style="display: none;">
                                            <input id="file_input" accept="application/pdf" type="file" name="file"  >
                                        </div>
                                    </div>
                                </div>
                                <div style="clear:both;height:10px;"></div>
                                <a class="nav_box white save">Save</a>
                                <a class="nav_box white cancel" >Cancel</a>
                             </fieldset>
                         </div>
                     </div>
                </div>
            </div>
            <div class="user_actions" style="display: none;width: 100%;">
                <a class="nav_box white edit">@lang('backend_pages.edit')</a>
                <a class="nav_box white properties">@lang('backend_pages.properties')</a>
                <a class="nav_box white revert ">@lang('backend.revert')</a>
                <a class="nav_box white publish">@lang('backend_pages.publish')</a>
                <a class="nav_box white unpublish">@lang('backend_pages.unpublish')</a>
                <a class="nav_box white preview">@lang('backend.preview')</a>
                <a class="nav_box white update_alert">@lang('backend.update_alert')</a>
                <a class="nav_box white upload_pdf">@lang('backend.upload_pdf')</a>
                <a class="nav_box white get_pdf">@lang('backend.get_pdf')</a>
                <a class="nav_box white remove" confirm="">@lang('backend_pages.remove')</a>
                <div style="clear:both;height:1px;"></div>
            </div>
            <div class="revision_table">
                <input type="hidden" id='revision_page_id'>
                <table cellpadding="0" cellspacing="0" border="0" id="pages_revision" class="content_table">
                    <thead>
                        <tr>
                            <th>@lang('backend_pages.column.title')</th>
                            <th>@lang('backend_pages.column.version')</th>
                            <th>@lang('backend_pages.column.version_id')</th>
                            <th>@lang('backend_pages.column.status')</th>
                            <th>@lang('backend_pages.column.access')</th>
                            <th>@lang('backend_pages.column.last_modified')</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div class="properties_buttons">
                <a class="nav_box white cancel" href="#" tabindex="30">OK</a>
            </div>
            </div>
        </div>

	</div>
</div>
@stop

@extends('layouts.backend_content')
@section('bodyClass'){!! 'basic' !!}@stop
@section('edit_scripts')
<script type="text/javascript" src="{!! url('/js/editing_timer.js')!!}"></script>
<script>

	// Configuration for edit of page , downloads and image
    var EDIT_MAX_TIME_SECONDS = {!!Config::get('app.edit_max_time_seconds')!!};
	var EDIT_TIME_EXTENSION_ALERT_SECONDS = {!!Config::get('app.edit_time_extension_alert_seconds')!!};
	var EDIT_COMPLEATED_ALERT_SECONDS = {!!Config::get('app.edit_compleated_alert_seconds')!!};

	var EDITING_USER = {!!Auth::user()->id!!};
$(document).ready(function(){
	var change_workflow_responsible = function(){
		@if($CHANGE_WORKFLOW_RESPONSIBLE)
		jConfirm(js_localize['pages.edit.workflow.responsible'],js_localize['pages.edit.title'], 'success',function(r){
			if(r){
				changePageResponsible({!!$PAGE->u_id!!},{!!Auth::User()->id!!});
			}else{
				var statusPromise = unlockItem($('div#content_edit_page_u_id').html(),'page');
                statusPromise.always(function() {
					window.location.href = relPath + 'cms/pages';
                });
			}
		});
		@endif
	}

    editing_timer.start(
        {!!$TIME_LEFT!!},
        function(){
            //Close all possible opened forms
            $(".mce-close").click();
            $("#tree_wrapper #popup_close img").click();
            $(".back_to_view").click();
            var promise = savePage($("#page_save")[0]);
            promise.always(function(){
                window.location.href = relPath + 'cms/pages';
            });
        },
        function(){
             var statusPromise = unlockItem($('div#content_edit_page_u_id').html(),'page');
             statusPromise.always(function(){
                window.location.href = relPath + 'cms/pages';
            });
        },
        {!!$PAGE->u_id!!},
        'page',
        '',
        EDIT_MAX_TIME_SECONDS
    );


		var time_left = {!!$TIME_LEFT!!};
		if(time_left > EDIT_TIME_EXTENSION_ALERT_SECONDS){
			jAlert(
                js_localize['pages.edit.start']
                    .replace('{time_left}', editing_timer.formatTimeInMinutes(time_left))
                    .replace('{alert_minutes}', editing_timer.formatTimeInMinutes(EDIT_TIME_EXTENSION_ALERT_SECONDS)),
                js_localize['pages.edit.title'],
                'success',
                change_workflow_responsible
            );
		}
});
</script>
@stop
@section('left_navigation')
<a id="change_lang" @if(Session::get('lang') == LANG_EN) href="{!! url('/cms/content/'.$PAGE->u_id.'?lang=de')!!}" class="change_lang_en  @else href="{!! url('/cms/content/'.$PAGE->u_id.'?lang=en')!!}" class="change_lang_de @endif  nav_box" >@lang('backend.change_lang')</a>
<!--<a class="nav_box revisions">@lang('backend.revisions')</a>-->
<a class="nav_box moderations">@lang('backend.moderations')</a>
<!--<a id="update_alert" class="nav_box" >@lang('backend.update_alert')</a>-->
<a class="nav_box properties" >@lang('backend.properties')</a>
<a class="nav_box restore" >@lang('backend.restore')</a>
@stop
@section('banner_contents')
<div class="RSTitle" style="background-color:#3683AB;">@lang('backend.banner')</div>
<div class="content_banner" content_id="{!!$CONTENTIDS['banner']!!}">
    <div class="modify_overlay" style="display:none;"></div>
    <div class="modify_wrapper" style="display:none;width:100%;">
        <a class="nav_box white edit">@lang('backend_pages.edit')</a>
        <a class="nav_box white back_to_view" style="float:right;display:none;">Back to view</a>
        <div style="clear:both;height:1px;"></div>
        <textarea style="height:200px;display:none;" name="banner_body" id="banner_body"></textarea>
    </div>
    <div id="content_banner_text" class="content_banner_text">
        {!!$CONTENTS['banner']!!}
    </div>
</div>
<div class="RSTitle" style="background-color:#3683AB;">@lang('backend.tablet.banner')</div>
<div class="content_tablet_banner" content_id="{!!$CONTENTIDS['tablet_banner']!!}">
    <div class="modify_overlay" style="display:none;"></div>
    <div class="modify_wrapper" style="display:none;width:100%;">
        <a class="nav_box white edit">@lang('backend_pages.edit')</a>
        <a class="nav_box white back_to_view" style="float:right;display:none;">Back to view</a>
        <div style="clear:both;height:1px;"></div>
        <textarea style="height:200px;display:none;" name="tablet_banner_body" id="tablet_banner_body"></textarea>
    </div>
    <div id="content_tablet_banner_text" class="content_banner_text">
        {!!$CONTENTS['tablet_banner']!!}
    </div>
</div>
<div class="RSTitle" style="background-color:#3683AB;">@lang('backend.mobile.banner')</div>
<div class="content_mobile_banner" content_id="{!!$CONTENTIDS['mobile_banner']!!}">
    <div class="modify_overlay" style="display:none;"></div>
    <div class="modify_wrapper" style="display:none;width:100%;">
        <a class="nav_box white edit">@lang('backend_pages.edit')</a>
        <a class="nav_box white back_to_view" style="float:right;display:none;">Back to view</a>
        <div style="clear:both;height:1px;"></div>
        <textarea style="height:200px;display:none;" name="mobile_banner_body" id="mobile_banner_body"></textarea>
    </div>
    <div id="content_mobile_banner_text"  class="content_banner_text">
        {!!$CONTENTS['mobile_banner']!!}
    </div>
</div>
@stop
@section('path')
<div id="path"><span class="pathlink">
        @include('partials.path')
    </span></div>
@stop
@section('main_contents')
 <input type="hidden" name="isPageRestricted" value="{!!$IS_PAGE_RESTRICTED!!}">
<div class="textarea_overlay"></div>

<div class="RSTitle">@lang('backend.main')</div>
<div id="frontend_div" class="content_main" content_id="{!!$CONTENTIDS['main']!!}">
    <div class="modify_overlay" style="display:none;"></div>
    <div class="modify_wrapper" style="display:none;width:100%;">
        <a class="nav_box white edit">@lang('backend_pages.edit')</a>
        <a class="nav_box white back_to_view" style="float:right;display:none;">Back to view</a>
        <div style="clear:both;height:1px;"></div>
        <textarea style="height:600px;display:none;" name="main_body" id="main_body"></textarea>
    </div>
    <div id="content_main_text">
		{!!$CONTENTS['main']!!}
    </div>
</div>

<div class="RSTitle">@lang('backend.page_downloads')</div>
<div id="page_downloads" class="genericRS viewable" >
	<div class="modify_overlay" style="display:none;"></div>
	<div class="modify_wrapper" style="display:none"><a class="nav_box white edit">@lang('backend_pages.edit')</a></div>

	<div class="download_records cf">
		<div class="download_record_template" pos="0" related="0" style="display:none">
			<span id="download_0"></span>
			<div class="related_buttons cf" style="display:none;">
				<a href="" class="modify_links move_up"></a>
				<a href="" class="modify_links move_down"></a>
				<a href="" class="modify_links delete"></a>
			</div>
		</div>

	@if($DOWNLOAD_FILES)
		@foreach($DOWNLOAD_FILES as $down)
            <div class="download_record cf" pos="{!!$down->position!!}" download="{!! $down->file->id !!}">
			<span id="download_{!! $down->file->id !!}">> {!!$down->file->filename!!}</span>
			<div class="related_buttons cf">
				<a href="" class="modify_links move_up" ></a>
				<a href="" class="modify_links move_down" ></a>
				<a href="" class="modify_links delete" ></a>
			</div>
		</div>
		@endforeach
	@endif

	</div>
	<div class="modify_buttons cf">
		<a href="" class="modify_links add" ></a>
		<a href="" class="modify_links back_to_view"></a>
	</div>
</div>

<div class="RSTitle">@lang('backend.author')</div>
<div class="content_author" content_id="{!!$CONTENTIDS['author']!!}">
	<div class="modify_overlay" style="display:none;"></div>
	<div class="modify_wrapper" style="display:none;width:100%;">
		<a class="nav_box white edit">@lang('backend_pages.edit')</a>
		<a class="nav_box white back_to_view" style="float:right;display:none;">Back to view</a>
		<div style="clear:both;height:1px;"></div>
		<textarea style="height:200px;display:none;" name="author_body" id="author_body"></textarea>
	</div>
	<div id="content_author_text">
		{!!$CONTENTS['author']!!}
	</div>
</div>

<!--<div class="RSTitle">page related @lang('backend.page_downloads')</div>-->
<div id="related">
    @include('partials_backend.related_links')
</div>
@stop
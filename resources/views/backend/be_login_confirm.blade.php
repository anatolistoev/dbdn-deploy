<p span style="font-family:Arial;font-size:10pt;">
	@lang('confirmation_email.greeting', array('FIRST_NAME'=>$FIRST_NAME,'LAST_NAME'=>$LAST_NAME))
	<br>
</p>
<p span style="font-family:Arial;font-size:10pt;">
	@lang('confirmation_email.content', array('USERNAME'=> $USERNAME, 'CONFIRM_CODE'=>$CONFIRM_CODE))
</p>
<p style="font-family:Arial;font-size:10pt;">
	@lang('confirmation_email.footer')
</p>
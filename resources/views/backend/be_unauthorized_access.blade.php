<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Daimler Brand &amp; Design Navigator</title>
		@include('partials_backend.backend_global_insert')
		@section('styles')
		<link rel="stylesheet" href="{!! url('/css/jquery.dataTables.css')!!}" />
		@show
	</head>
	<body>
		<div class="wrapper">
			<div class="page_title">@lang('backend.page_title')</div>
			
			<div class="content_wrapper">
				<div class="error_message">
					Unauthorized Access
				</div>
			</div>
			<div style="clear:both;"></div>
		</div>
	</body>
</html>
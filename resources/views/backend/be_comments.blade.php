@extends('layouts.backend')
@section('left_navigation')
<a @if(Session::get('lang') == LANG_EN)href="{!! url('/cms/comments?lang=de')!!}" style="background-image:url('{!! url('/img/backend/change_lang_icon_EN.png')!!}');" @else href="{!! url('/cms/comments?lang=en')!!}" style="background-image:url('{!! url('/img/backend/change_lang_icon_DE.png')!!}');"@endif class="nav_box" >@lang('backend.change_lang')</a>
<a href="{!! url('/cms/comments')!!}" class="nav_box @if($ACTIVE == 'comments')active @endif" style="background-image:url('{!! url('/img/backend/comments_icon.png')!!}');">@lang('backend.comments')</a>
<a href="{!! url('/cms/history')!!}" class="nav_box @if($ACTIVE == 'history')active @endif" style="background-image:url('{!! url('/img/backend/history_icon.png')!!}')"> @lang('backend.history')</a>		
@stop
@section('user_content')
<script>
	var lang = '{!!$LANG!!}';
	var username = '{!!$USERNAME!!}';
	var valid_user = {!!$VALID_USER!!};
</script>
<script type="text/javascript" src="{!! url('/js/comments.js')!!}"></script>
<div class="user_table">
	<form merhod="POST" action="" class="filter_form">
		<input type="text" name="user_search" value="" class="user_search" placeholder="@lang('backend_comments.placeholder.comment_search')"/>
		<select name="user_sort" class="user_sort">
			<option value="-1">@lang('backend_comments.placeholder.comment_sort')</option>
			<option value="0">@lang('backend_comments.column.id')</option>
			<option value="1">@lang('backend_comments.column.page')</option>
			<option value="2">@lang('backend_comments.column.username')</option>
			<option value="3">@lang('backend_comments.column.subject')</option>
			<option value="4">@lang('backend_comments.column.body')</option>
			<option value="5">@lang('backend_comments.column.active')</option>
			<option value="6">@lang('backend_comments.column.date')</option>
		</select>
		<input class="user_submit" type="submit" value="@lang('backend_comments.button.filter')" onClick="return refreshTable();" />
	</form>

	<table cellpadding="0" cellspacing="0" border="0" id="comments">
		<thead>
			<tr>
				<th>@lang('backend_comments.column.id')</th>
				<th>@lang('backend_comments.column.page')</th>
				<th>@lang('backend_comments.column.username')</th>
				<th>@lang('backend_comments.column.subject')</th>
				<th>@lang('backend_comments.column.body')</th>
				<th>@lang('backend_comments.column.active')</th>
				<th>@lang('backend_comments.column.date')</th>				
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>	
	<div class="user_edit_wrapper" style="display:none">
		<div class="user_actions">
			<a class="nav_box white edit">@lang('backend_comments.edit')</a>
			<a class="nav_box white activate">@lang('backend_comments.activate')</a>
			<a class="nav_box white remove" confirm="@lang('backend_comments.remove.confirm')">@lang('backend_comments.remove')</a>
			<div style="clear:both;"></div>
		</div>
		<div class="textarea_overlay"></div>
		<div class="user_fields" id="wrapper_form_comments" style="display:none;">
			<form id="comment_form" action="" method="POST">
				<input type="hidden" name="id" value="0" />
				<div class="form_left" style="height:503px;padding-top:30px;">
					<div class="radio_group" style="margin-bottom:20px;">
						<label>@lang('backend_comments.form.active')</label><br />
						<input id="comment_non_active" class="first_choice" type="radio" name="active" value="0" checked="true" tabindex="1"/><label for="comment_non_active"></label>
						<input id="comment_active" class="second_choice" type="radio" name="active" value="1" checked="false" tabindex="2"/><label for="comment_active"></label>
						<div style="clear:both;"></div>
					</div>
					<div style="clear:both;height:3px;"></div>
					<div class="system_info">
						<div class="system_info_row">
							<p class="si_label">@lang('backend_comments.form.user')</p>
							<p class="si_data" id="username"></p>
							<div style=clear:both;"></div>
						</div>
						<div class="system_info_row">
							<p class="si_label">@lang('backend_comments.form.user_id')</p>
							<p class="si_data" id="user_id"></p>
							<div style=clear:both;"></div>
						</div>
						<div class="system_info_row">
							<p class="si_label">@lang('backend_comments.form.page')</p>
							<p class="si_data" id="page_title"></p>
							<div style=clear:both;"></div>
						</div>
						<div class="system_info_row">
							<p class="si_label">@lang('backend_comments.form.page_id')</p>
							<p class="si_data" id="page_id"></p>
							<div style=clear:both;"></div>
						</div>
						<div class="system_info_row">
							<p class="si_label">@lang('backend_comments.form.created_at')</p>
							<p class="si_data" id="created_at"></p>
							<div style=clear:both;"></div>
						</div>
						<div class="system_info_row">
							<p class="si_label">@lang('backend_comments.form.modified_at')</p>
							<p class="si_data" id="updated_at"></p>
							<div style=clear:both;"></div>
						</div>
					</div>
				</div>
				<div class="form_right" style="height:503px">
					<div class="input_section">
						<div class="input_group">
							<label for="first_name">@lang('backend_comments.form.subject')<span class="fieldset_requared">*</span></label>
							<input type="text" value="" name="subject" style="width:400px" tabindex="3"/>
						</div>
						<div style="clear:both;"></div>
						<div class="input_group">
							<label for="body">@lang('backend_comments.form.body')<span class="fieldset_requared">*</span></label>
							<textarea name="body" style="width:400px;height:200px;" tabindex="4"></textarea>
						</div>
					</div>
					<div style="clear:both;height:80px;"></div>
					<a class="nav_box white save" href="#" tabindex="5">@lang('backend.save')</a>
					<a class="nav_box white cancel" href="#" tabindex="6">@lang('backend.cancel')</a>

				</div>
				<div style="clear:both;"></div>
			</form>
		</div>
	</div>

</div>
@stop
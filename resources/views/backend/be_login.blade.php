<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="text/html; charset=UTF-8" http-equiv="content-type">
		<title>Daimler Brand &amp; Design Navigator</title>
		@include('partials_backend.backend_global_insert')
		@section('scripts')
		<script type="text/javascript" src="{!! url('/js/jquery.dataTables.js')!!}"></script>
		<script type="text/javascript" src="{!! url('/js/jquery.dataTables_extensions.js')!!}"></script>
		@show
		@section('styles')
		<link rel="stylesheet" href="{!! url('/css/jquery.dataTables.css')!!}" />
		@show
		<script src="{!! url('/js/jquery.ui.draggable.js')!!}" type="text/javascript"></script>
	</head>
	<body>
		<div class="wrapper">
			<div class="page_title">@lang('backend.page_title')</div>
			
			<div class="content_wrapper">
				<form class="login_form" action="{!! url('/cms/login')!!}" method="POST" autocomplete="off">
                    {!! csrf_field() !!}
					@if(!empty($errors) && $errors->first('username'))
					<div class="login_error">
					{!!$errors->first('username')!!}
					</div>
					@endif
					<input class="login_input" type="text" name="username" value="{!!Session::has('username') ? Session::get('username') : ''!!}" autocomplete="off"/>
					<input class="login_input" type="password" name="password" value="" autocomplete="off"/>
					<input class="user_submit" type="submit" name="userform_submit" value="SIGN IN" />
					<a class="forgotten" href="{!!url('/forgotten')!!}">@lang('backend.forgotten')</a>
				</form>
			</div>
			<div style="clear:both;"></div>
		</div>
	</body>
</html>
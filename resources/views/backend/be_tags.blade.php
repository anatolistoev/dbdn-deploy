@extends('layouts.backend')
@section('left_navigation')
<a @if(Session::get('lang') == LANG_EN) href="{!! url('/cms/tags?lang=de')!!}" style="background-image:url('{!! url('/img/backend/change_lang_icon_EN.png')!!}');" @else href="{!! url('/cms/tags?lang=en')!!}" style="background-image:url('{!! url('/img/backend/change_lang_icon_DE.png')!!}');"@endif class="nav_box" >@lang('backend.change_lang')</a>
<a href="#" class="nav_box count_pages" >@lang('backend.count_pages')</a>
@stop
@section('user_content')
<script>
	var lang = '{!!$LANG!!}';
</script>
<script type="text/javascript" src="{!! url('/js/tags.js')!!}"></script>
<div class="user_table">
	<form merhod="POST" action="" class="filter_form">
		<input type="text" name="user_search" value="" class="user_search" placeholder="@lang('backend_tags.placeholder.tags_search')"/>
		<select name="user_sort" class="user_sort">
			<option value="-1">@lang('backend_tags.placeholder.tags_sort')</option>
			<option value="0">@lang('backend_tags.column.id')</option>
			<option value="1">@lang('backend_tags.column.name')</option>
		</select>
		<input class="user_submit" type="submit" value="@lang('backend_comments.button.filter')" onClick="return refreshTable();" />
	</form>

	<table cellpadding="0" cellspacing="0" border="0" id="tags">
		<thead>
			<tr>
				<th>@lang('backend_tags.column.id')</th>
				<th>@lang('backend_tags.column.name')</th>
				<th>@lang('backend_tags.column.modified_at')</th>
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>
	<div class="user_edit_wrapper" style="display:none">
		<div class="user_actions">
			<a class="nav_box white remove" confirm="@lang('backend_tags.remove.confirm')">@lang('backend_tags.remove')</a>
			<div style="clear:both;"></div>
		</div>
		<div class="textarea_overlay"></div>
		<div class="user_fields" id="wrapper_form_tags" style="display:none;">
			<form id="tag_form" action="" method="POST">
				<input type="hidden" name="id" value="0" />
				<input type="hidden" name="lang" value="{!!$LANG!!}" />
				<div class="form_left" style="height:503px;padding-top:30px;">
					<div style="clear:both;height:3px;"></div>
					<div class="system_info">
						<div class="system_info_row">
							<p class="si_label">@lang('backend_tags.form.created_at')</p>
							<p class="si_data" id="created_at"></p>
							<div style=clear:both;"></div>
						</div>
						<div class="system_info_row">
							<p class="si_label">@lang('backend_tags.form.modified_at')</p>
							<p class="si_data" id="updated_at"></p>
							<div style=clear:both;"></div>
						</div>
					</div>
				</div>
				<div class="form_right" style="height:503px;">
					<div class="input_section">
						<div class="input_group">
							<label for="name">@lang('backend_tags.form.name')<span class="fieldset_requared">*</span></label>
							<input type="text" value="" name="name" style="width:400px" tabindex="1"/>
						</div>
					</div>
					<div style="clear:both;height:80px;"></div>
					<a class="nav_box white save" href='#' tabindex="3">@lang('backend.save')</a>
					<a class="nav_box white cancel" href='#' tabindex="4">@lang('backend.cancel')</a>

				</div>
				<div style="clear:both;"></div>
			</form>
		</div>
	</div>

</div>
@stop
@extends('layouts.base')
@section('bodyClass'){!! $PAGE->brand == SMART? 'smart' :'basic' !!}@stop
@section('path')
{{--<div id="path"><span class="pathlink">
    @include('partials.path')
</span></div>--}}
@stop
@section('left_nav')
    @if( isset($SECTION_PID_INDEX, $L2ID, $SECTION_PID_INDEX[$L2ID]) && $L2ID > 1 )
        <?php
        // commented by Dusty - unnesesary check addon leading to wrong menu behaviour:
        //  && ((isset($SECTION_PID_INDEX[$PAGE->id]) && $LEVEL == 2 ) || $LEVEL != 2)
        ?>

        {{--
        <section class="basic_nav_closed" id='basic_nav_closed'>
            <wrap style="">
                <div>
                    @lang('template.navigation.menu')
                </div>
            </wrap>
        </section>
        --}}

        <section class="basic_nav_opened">
            <div class="basic_nav basic_menu popup">
            </div>
        </section>
    @endif

@stop

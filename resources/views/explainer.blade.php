<!doctype html>
<html lang="en">
<head>
	@if($IMG = asset('img/'))
	@endif
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1280">
    <meta name="MobileOptimized" content="1280">
	<title>Daimler Brand &amp; Design Navigator</title>
		@section('scripts')
			<script>
				var paths = {
					'images': '{!! asset('') !!}',
					'gallery': '{!! asset('gallery') !!}'
				}
			</script>
            <script type="text/javascript" src="{!! mix('/js/all.explainer.js') !!}"></script>

            @include('partials/gallery_localization')
			<!--[if (gte IE 6)&(lte IE 8)]>
				<script type="text/javascript" src="{!! mix('/js/selectivizr-min.js') !!}"></script>
			<![endif]-->
		@show
		@section('styles')
        @if(Config::get('app.testing'))
            <link rel="stylesheet" href="{!! asset('/css/desktop.css') !!}" />
            <link rel="stylesheet" href="{!! asset('/css/le.css') !!}" />
            <link rel="stylesheet" href="{!! asset('/css/lytebox.css') !!}" type="text/css" media="screen" />
        @else
            <link rel="stylesheet" href="{!! asset('/css/all.explainer.css') !!}" type="text/css" media="screen" />
        @endif
			<style>
				html{
					overflow:hidden;
				}
                body{
                    font-size:18px;
                    line-height:26px;
                    min-width:100%;
                }
			</style>
		@show
        <link rel="SHORTCUT ICON" href="{!! asset('/favicon48.ico') !!}" type="image/x-icon"/>
        <link rel="icon" href="{!! asset('/DAI_Favicon_16.png')!!}" sizes="16x16" type="image/png"/>
        <link rel="icon" href="{!! asset('/DAI_Favicon_32.png') !!}" sizes="32x32" type="image/png">
        <link rel="icon" href="{!! asset('/DAI_Favicon_96.png')!!}" sizes="96x96" type="image/png"/>
        <link rel="icon" href="{!! asset('/DAI_Favicon_196.png')!!}" sizes="196x196" type="image/png"/>
        <link rel="icon" href="{!! asset('/DAI_Favicon_160.png')!!}" sizes="160x160" type="image/png"/>
        <link rel="apple-touch-icon" href="{!! asset('/DAI_Favicon_57.png')!!}" sizes="57x57"/>
        <link rel="apple-touch-icon" href="{!! asset('/DAI_Favicon_72.png')!!}" sizes="72x72"/>
        <script>
            $(document).keyup(function(e) {
                if (e.keyCode == 27) {
                    window.parent.myLytebox.end();
               }
            });
        </script>
        <meta name=”description” content="@lang('template.meta.description')">
	</head>
	<body>

		<div id="lbClose" style="top:49px;right:48px;" onClick="window.parent.myLytebox.end();"></div>
        <span id="lbEsc" style="top:86px;right:46px;">esc</span>
        <div id="content_wrapper">
            <div id="frame_wrapper">
            <div id="LE">
                <div id="LEG">
                    <?php $z=0; ?>
                    @foreach($files['images'] as $key=>$value)
                        <div class="LEG_img_container" style="@if($z == 0) position:relative; @else position:absolute; @endif">
                            <a class="LE-tool-layer-{!!$z!!}"  id="explainer_{!!$z!!}">
                                <img src="{!!$value['lowres']!!}" border="0" unselectable="on" />
                            </a>
                        </div>
                        <?php $z++; ?>
                    @endforeach
                    </div>
            </div>
            <div id="LE-right">
                <div id="LE-legend">
                    <h1>@lang('template.explainer.default_text')</h1>
                    <div id="LE-legend-info">
                        <?php $z=0; ?>
                        @foreach($files['images'] as $key=>$value)
                            @if($z != 0)
                            <div class="LE-wrapper cf">
                                <div value="{!!$z!!}" id="LE-tool-{!!$z!!}" style="display: block;" class="on LE-tool">
                                    <span class="icon"></span>
                                    <span class="title"></span>
                                </div>
                                <span class="LE-legend-info">{!!$value['descr']!!}</span>
                            </div>
                            @endif
                            <?php $z++; ?>
                        @endforeach
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div style="clear:both;"></div>
	</div>
    <script>
        if($(parent.document).find('body').hasClass('smart'))
			$('body').addClass('smart');
    </script>
	</body>
</html>

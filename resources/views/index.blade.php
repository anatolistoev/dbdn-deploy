@extends('basic')
@section('bodyClass'){!! 'basic' !!}@stop
@section('path')
<div id="path"><span class="pathlink">
	<a href="{!! asset('home') !!}" class="pathlink">@lang('template.home')</a> / 
	<a href="{!! asset($PAGE->id) !!}" class="pathlink">Index</a>
</span></div>
@stop
@section('main')
	<div id="main">
		<h1>Index.</h1>
		<br>
		@foreach($LETTERS as $row)
			@if($CURR_LETTER == $row->letter)
			
				<span class="activeLetter">{!! $row->letter !!}</span> 
			@else
				<a href="{!! url('index/'. rawurlencode($row->letter) ) !!}" class="letterlink" target="_self">{!! $row->letter !!}</a> 
			@endif
		@endforeach
		<br>
		<br>
		@if(sizeof($PAGES))
			<script type="text/javascript" src="{!! asset('js/jquery.paging.min.js') !!}"></script>
<script>
	$(function($) {
			var prev = {start: 0, stop: 0},
			cont = $('span.indexBlock');
			
			$(".pagination").paging(cont.length, { 
			format: '-[< nncnn >]', // define how the navigation should look like and in which order onFormat() get's called
			perpage: 10, // show 10 elements per page
			lapping: 0, // don't overlap pages for the moment
			page: 1, // start at page, can also be "null" or negative
			onSelect: function (page) {
				var data = this.slice;
		
				cont.slice(prev[0], prev[1]).css('display', 'none');
				cont.slice(data[0], data[1]).fadeIn("slow");
		
				prev = data;
		
				return true; // locate!
			},
			onFormat: function (type) {
				switch (type) {
				case 'block': // n and c
					return this.value != this.page ?
							   '<a href="#" class="pageNumber">' + this.value + '</a>' :
							   '<a href="#" class="currpage">' + this.value + '</a>';
				case 'next': // >
					return '<a href="#" class="nextpage"></a>';
				case 'prev': // <
					return '<a href="#" class="prevpage"></a>';
				case 'first': // [
					return '<a href="#" class="fbackward"></a>';
				case 'last': // ]
					return '<a href="#" class="fforward"></a>';
				case "fill": // -
					if (this.active)
						return ((this.page - 1) * this.perpage)+1 +' - '+ Math.min(this.number, (this.page * this.perpage)) + ' @lang('system.of') ' + this.number+'<br>';
					return "";
				}
			}
		});	
    });
</script>
			<div class="pagination"></div>
			@foreach($PAGES as $page)
				@if($page->parent_id == HOME)
					<span class="indexBlock"><b>{!! link_to($page->slug, $page->title, array('class'=>'')) !!}</b><br>
				@else
					/ 
				@endif
				{!! link_to($page->parentSlug, $page->parentTitle, array('class'=>'') ) !!}
				@if($page->level == 1)
					/ {!! link_to($page->slug, $page->title, array('class'=>'')) !!}<br><br></span>
				@endif
			@endforeach
			<div class="pagination"></div>
		@else
			@lang('system.empty_list')
		@endif
		<br><br>
	</div><!-- main -->
@stop
@section('right')

@stop
@extends('layouts.base')
@section('bodyClass'){!! 'basic' !!}@stop
@section('bullet_nav')@overwrite
@section('main')
	<section>
		<div class="user_profile_fields">
			<h1>@lang($TITLE_KEY)</h1>
            <h1><small>@lang($SUBHEADING_KEY)</small></h1>
			<p>@lang($MESSAGE_KEY)</p>
		</div>
	</section>
@stop


@extends('layouts.base')
@section('bodyClass'){!! 'basic' !!}@stop
@section('path')
<!--<div id="path"><span class="pathlink">
	<a href="{!! asset('home') !!}" class="pathlink">@lang('template.home')</a> /
	<a href="{!! asset('reset/'. $TOKEN) !!}" class="pathlink">@lang('system.'.$PAGE->id)</a>
</span></div>-->
@stop
@section('main')
<section>
	<div class="user_profile_fields">
		<h1>@lang('system.'.$PAGE->id.'.page.title') </h1>
                <h1><small>@lang('system.'.$PAGE->id.'.page.subheading')</small></h1>
                <p class="note">@lang('profile.pwd.rules')</p>
<!--                <section>
                    <div class="whole_line">
                            @lang('system.'.$PAGE->id.'.contents')
                    </div>
                </section>-->
		{!! Form::open(array(
			"url"       	=> "reset/".$TOKEN,
			"autocomplete"	=> "off",
			"id"			=> "resetForm"
		)) !!}
                <section>
                        <input type="hidden" name="token" value="{!! $TOKEN !!}">
                        <div class="whole_line">
                            <span class="input input--yoshiko">
                                {!! Form::text('username', null, array('class'=> ($errors->first('username') ? 'error input__field input__field--yoshiko' : 'input__field input__field--yoshiko'))) !!}
                            <label class="input__label input__label--yoshiko" for="password">
                                <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.username')">@lang('profile.username')</span>
                            </label>
                            </span>
                        </div>
                </section>
                <section class="cf">
                        <div class="label_left">
                            <span class="input input--yoshiko">
                                {!! Form::input('password', 'password', null, array('autocomplete'=>'off', 'class'=> ($errors->first('password') ? 'error ' : '' ).'input__field input__field--yoshiko')) !!}
                                <label class="input__label input__label--yoshiko" for="password">
                                    <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.password')">@lang('profile.password')</span>
                                </label>
                            </span>
                        </div>
                        <div class="label_right">
                            <span class="input input--yoshiko">
                                {!! Form::input('password', 'password_confirmation', null, array('autocomplete'=>'off', 'class'=> ($errors->first('password_confirmation') ? 'error ' : '' ).'input__field input__field--yoshiko')) !!}
                                <label class="input__label input__label--yoshiko" for="password_confirmation">
                                    <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.confirm')">@lang('profile.confirm')</span>
                                </label>
                            </span>
                        </div>
                </section>
                <section class="cf profile_captcha">
                    <div class="label_left" style="margin-right:12px;">
                        <img class="captcha_img" src="{!! asset('captcha'). '?brand=' . ((isset($PAGE->brand) && $PAGE->brand == SMART) ?'smart':'daimler') !!}">
                        <div class="reset_captcha"></div>
                    </div>
                    <div class="label_left" style="width:354px">
                        <span class="input input--yoshiko whole_line">
                            <input class="input__field input__field--yoshiko @if($errors->first('captcha'))error @endif" type="text" name="captcha" autocomplete="off" />
                            <label class="input__label input__label--yoshiko" for="captcha">
                                <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.enter.code')">@lang('profile.enter.code')</span>
                            </label>
                        </span>
                    </div>
                </section>
                <section class="more_info">
                    <input class="submit_button" type="submit" value="@lang('system.reset_form.send')">
                </section>
		{!! Form::close() !!}
	</div><!-- main -->
</section>
<script type="text/javascript" src="{!! mix('/js/classie.js') !!}"></script>
@stop
@section('right')

@stop
@section('farright')

@stop



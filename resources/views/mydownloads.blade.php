@extends('basic')
@section('bullet_nav')
@stop
@section('left_nav')

@stop
@section('main')
	<section>
		@include('partials.back_button')
	</section>
	<section>
		<div class="user_download_fields">
			<section class="h1 cf">
				<h1>@lang('system.mydownloads.page.title') @if((isset($DOWNLOAD_FILES)&& count($DOWNLOAD_FILES)>0) || (isset($AUTH_FILES) && count($AUTH_FILES)>0 )) [{!!count($AUTH_FILES) + count($DOWNLOAD_FILES)!!}] @endif</h1>
				@if((isset($DOWNLOAD_FILES)&& count($DOWNLOAD_FILES)>0) || (isset($AUTH_FILES) && count($AUTH_FILES)>0 ))
					<?php $listView = false; ?>
				    @include('partials.view_controls')
				@endif
			</section>

			<section>
				<p class="p">@lang('system.mydownloads.contents')</p>
			</section>
		</div>
		<div class="download_elements">
			@if((!isset($DOWNLOAD_FILES) && !isset($AUTH_FILES)) || (count($DOWNLOAD_FILES)==0 && count($AUTH_FILES)==0 ))
            <section><p class="empty_list">@lang('system.empty_list')</p></section>
			@else
				@include('partials.mydownloads_list')
			@endif
		</div>
        <div style="clear:both;"></div>
	</section>
@stop

@extends('layouts.base')
@section('scripts')
@parent
@stop
@section('styles')
@parent
<!--	<link rel="stylesheet" href="{!! asset('/css/home.css') !!}" />-->
@stop
@section('bodyClass'){!! $PAGE->brand == SMART? 'smart' : 'home' !!}
@stop
@section('bullet_nav') @stop
@section('left_nav')
    <section class="basic_nav_opened">
        <div class="basic_nav basic_menu popup">
        </div>
    </section>
@stop
@section('banner')
    @if(! MOBILE)
        <section class="banner-section">
            <div id="banner" @if(strlen($CONTENTS['banner'])< 10)style="height:0" @endif>
        <!--        <div class="homeblock" id="hbA">
                    <div class="hBlockContainer" id="hbACont" style="left: 0px;">-->
                        {!! $CONTENTS['banner'] !!}
        <!--            </div>
                    <div class="background"></div>
                    <div id="coverBackground"></div>
                    <div class="hBlockPages"></div>
                </div>-->
        <!--        <div class="hBlockNav">
                    <a style="display:none;" class="hBlockLeft" href=""></a>
                    <a style="display:none;" class="hBlockRight" href=""></a>
                </div>hbA-->
            <div class="linkDown"></div>
            </div><!-- banner -->
        </section>
    @endif
@stop
@section('main')

{{--
<section>
    <div id="searchDiv">
        {!! Form::open(array('id'=>"searchForm", 'url'=>url('search'))) !!}
            <input name="search" id="search" placeholder="@lang('template.search_box.label')" value="" type="text"/>
            <button type="submit" id="doSearch" class="newSearchBtn"></button>
        {!! Form::close() !!}
    </div>
    <div class="goDown">
         <a href=""></a>
    </div>
    <div style="clear:both;"></div>
</section>
--}}


<style type="text/css">
    div.overview div.page.idx1 {margin-right:20px;}
    /* div.overview .slider-item > div.page {min-height:630px;} */
</style>

@if(!MOBILE || $PAGE->brand == DAIMLER)
    <section class="homeblocks">
        @if($PAGE->brand == DAIMLER)
            <div style="margin-top:-35px;">
                @include('partials.homeblocks')
                @include('partials.guided_tour', array('tour_page'=>'brand', 'from' => 7, 'to' => 10))
            </div>
        @elseif(!MOBILE && !TABLET && isset($MOSTRECENTBLOCKS) && count($MOSTRECENTBLOCKS) )
            {{-- workaround over workaround over workaround... --}}
            <div style="clear:both;height:140px;"></div>
        @endif
        <div style="clear:both;"></div>
    </section>
@endif

@if( isset($MOSTRECENTBLOCKS) && count($MOSTRECENTBLOCKS) )
    <section class="mostrecentblocks" style="margin-top:-130px;">
        @include('partials.mostrecentblocks')
        <div style="clear:both;"></div>
    </section>
@else
    {{-- workaround over workaround over workaround... --}}
    <div class="mostrecentblocks-shifter" style="clear:both;height:20px;"></div>
@endif


@foreach($MOSTSOMETHING as $MOSTSOMETHINGTITLE => $MOSTSOMETHINGBLOCKS)
    @if( count($MOSTSOMETHINGBLOCKS) )
        <section class="mostsomethingblocks" style="margin-bottom:24px;">
            @include('partials.mostSomethingBlocks')
            <div style="clear:both;"></div>
        </section>
    @endif
@endforeach

@if(isset($ACCORDIONS) && sizeof($ACCORDIONS))
    <section>
        <div class="heading-row">
            <h1 id='heading-title'>{!!(isset($PAGE->brand)&& $PAGE->brand == SMART) ? trans('smart.design_manuals_downloads') : trans('template.design_manuals_downloads')!!}</h1>
            <div style="clear:both;"></div>
        </div>

            <div class="carousel_area">
                <div id="link_wrapper">
                    <a class='carousel_href' href="" >
                        <span id="carousel_img_wrapper" class="story-layover">
                            <img id='carousel_img' class="carousel_img" src="" title="" alt="" />
                        </span>
                        <div id="text wrapper">
                            <h2 id="carousel_title"></h2>
                           <div id="carousel_text" class="carousel_text"></div>
                        </div>
                    </a>
                </div>

                <div style="clear: both;"></div>
                <div id="carousel_basic">
                        <div class="slider_container">
                            <a class="prev slider_hover" @if(!isset($ACCORDIONS) || count($ACCORDIONS) < 5) style="display:none"@endif></a>
                            <a class="next slider_hover" @if(!isset($ACCORDIONS) || count($ACCORDIONS) < 5) style="display:none"@endif></a>
                            <div class="front_slider cf carousel_basic">
                                <ul class="carousel_menu">
                                    <?php $index = 1;?>
                                    <li index="{!!$index!!}" >
                                        @foreach($ACCORDIONS as $key=>$row)
                                            <div class="slider_element"
                                                    data-title="{!!$row['title']!!}"
                                                    data-href="{!!$row['href']!!}"
                                                    data-description=@if (mb_strlen($row['description']) > 300)
                                                                            "{!! (preg_match('/^.{1,300}\b/su', $row['description'], $match) ? htmlspecialchars($match[0], ENT_QUOTES, 'UTF-8').'...' : htmlspecialchars(mb_substr($row['description'], 0, 300), ENT_QUOTES, 'UTF-8') ) !!}"
                                                                        @else
                                                                            "{!! htmlspecialchars($row['description'], ENT_QUOTES, 'UTF-8') !!}"
                                                                        @endif >
                                                    <a href="#" onclick="return false;" class="page_link">
                                                        <div class="slider-image">
                                                            {!!$row['img']!!}
                                                        </div>
                                                        <div class="title"><span>{!! $row['title'] !!}</span></div>
                                                        <div class="date">@if(Session::get('lang') == LANG_EN){!!$row['updated_at']->formatLocalized('%B %d, %Y')!!}@else{!!$row['updated_at']->formatLocalized('%d. %B %Y')!!}@endif</div>
                                                        @if($row['tags']!= '')
                                                        <div class="tags">{!!$row['tags']!!}</div>
                                                        @endif
                                                        @if(isset($row['protected']) && $row['protected'])
                                                            <div class="protected" title='@lang('template.protected')'></div>
                                                        @endif
                                                    </a>
                                            </div>
                                            @if($index % 4 == 0 && $index < count($ACCORDIONS))
                                                    </li>
                                                    <li index="{!!$index/4 +1!!}">
                                           @endif
                                            <?php $index++; ?>

                                    @endforeach
                                    </li>
                                </ul>
                            </div>
                            <ul class="pages slider_hover"></ul>
                    </div>
                </div>
            </div>
    </section>
@endif


@stop
@section('right')

@stop


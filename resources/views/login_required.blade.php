@extends('layouts.base')
@section('bodyClass'){!! 'basic' !!}@stop

@section('main')
	<section>
		<div class="user_profile_fields">
			<h1>@lang('system.login_required.page.title')</h1>

			<p>@lang('system.login_required.contents')</p>
		</div>
	</section>
@stop





@extends('layouts.base')
@section('bodyClass'){!! 'basic' !!}@stop
@section('main')
	<script lang="javascript">
		$(document).ready(function() {
            var check = true;
			@if(isset($OLD_rbChoice)&&$OLD_rbChoice==1)
				$("#form_request_access #ContactName, #form_request_access #ContactEmail").hide();
                var isDaimlerUser = true;
			@else
				$("#form_request_access #ContactName, #form_request_access #ContactEmail").show();
                var isDaimlerUser = false;
			@endif
			$("#form_request_access input[name='rbChoice']").click(function(){
				if($("#form_request_access input[name='rbChoice']:checked").val() === '1'){
					$("#form_request_access #ContactName, #form_request_access #ContactEmail").hide();
                    isDaimlerUser = true;
				}
				else{
					$("#form_request_access #ContactName, #form_request_access #ContactEmail").show();
                    isDaimlerUser = check == true ? false : true;
				}
			});
            $('#form_request_access').submit(function(ev){
                if(check && !isDaimlerUser){
                    $("#notification").removeClass('red');
                    var contact_name = ($('input[name="contact_name"]').val()).split(/\s+/);
                    var contact_email = $('input[name="contact_email"]').val();
                    var msg = "";
                    var isnotValid = false;
                    if(contact_name.length == 2 && contact_name[0] == "{!!(Auth::user()->first_name)!!}" && contact_name[1] == "{!!(Auth::user()->last_name)!!}"){
                        msg = "{!!trans('system.no_access.wrong_name')!!}";
                        isnotValid = true;
                    }
                    var user_email = "{!!Auth::user()->email!!}";
                    var domain = user_email.replace(/.*@/, "").split('.');
                    domain = domain.slice(-2).join(".");
                    var regex = '\@(?:[a-zA-Z0-9]+(?:-[a-zA-Z0-9]+)*\.)*'+domain+'$';
                    var thisRegex = new RegExp(regex);
                    if(thisRegex.test(contact_email)){
                        isnotValid = true;
                        if(msg.length > 1){
                            msg = "{!!trans('system.no_access.wrong_both')!!}";
                        }else{
                            msg = "{!!trans('system.no_access.wrong_email')!!}";
                        }
                    }
                    if(isnotValid){
                        $("#notification .alert_text").html(msg + "</br>{!!trans('system.no_access.confirm')!!}");
                        $("#notification .alert_title").html("{!!trans('system.alert.title')!!}");
                        $('#notification .notification_content .buttons_wrapper').css('display', 'table-cell');
                        $('#notification').show();
                        $('body').scrollTo();
                        return false;
                    }
                }
            });
            $("#notification .ok_button").click(function(){
                check = false;
                $('#form_request_access').submit();
            });
            $("#notification .no_button, #notification .close_button").click(function(){
                check = false;
                $('#notification .notification_content .buttons_wrapper').css('display', 'none');
                $('#notification').hide();
            });
		});
	</script>
	<section>
		<div class="user_profile_fields">
			@if(isset($frmRA_success) && $frmRA_success == 'OK')
				<h1>{!! trans('system.no_access.success.title') !!}</h1>
				<h1><small>{!! trans('system.no_access.success.subheading') !!}</small></h1>
				<p>{!! trans('system.no_access.success.message') !!}</p>
			@else
			<h1>{!! trans('system.no_access.title') !!}</h1>
			<h1><small>{!! trans('system.no_access.subheading') !!}</small></h1>
			<form id="form_request_access" action="{!!asset('requestaccess/'.$PAGE->u_id) !!}" method="post">
                    {{ csrf_field() }}
					<div id="form_request_access_container">
						<div class="accept_dialog">
							<p>{!! trans('system.no_access.form.t1') !!}<br>{!! trans('system.no_access.form.t2') !!}</p>
							<section>
                                <p class="bold">{!! trans('system.no_access.form.t3') !!}</p>
								<div>

									<input id="rbChoice1" class="chb" type="radio" name="rbChoice" value="1" {!! isset($OLD_rbChoice)&&$OLD_rbChoice==1?'checked':'false' !!}>
									<label @if($errors->first('rbChoice')) class='error' @endif for="rbChoice1">{!! trans('system.no_access.form.t4') !!}</label>
									<div style="clear: both; height: 10px;"></div>

									<input id="rbChoice2" class="chb" type="radio" name="rbChoice" value="2" {!! isset($OLD_rbChoice)&&$OLD_rbChoice==2?'checked':'false' !!}>
									<label @if($errors->first('rbChoice')) class='error' @endif  for="rbChoice2">{!! trans('system.no_access.form.t5') !!}</label>
<!--									<input id="rbChoice3" class="chb" type="radio" name="rbChoice" value="3" {!! isset($OLD_rbChoice)&&$OLD_rbChoice==3?'checked':'false' !!}>
									<label @if($errors->first('rbChoice')) class='error' @endif for="rbChoice3">{!! trans('system.no_access.form.t6') !!}</label>
									<div style="clear: both; height: 10px;"></div>

									<input id="rbChoice4" class="chb" type="radio" name="rbChoice" value="4" {!! isset($OLD_rbChoice)&&$OLD_rbChoice==4?'checked':'false' !!}>
									<label @if($errors->first('rbChoice')) class='error' @endif for="rbChoice4">{!! trans('system.no_access.form.t11') !!}</label>-->

                                    <p></p>
								</div>
                                <p class="bold">{!! trans('system.no_access.form.t6') !!}</p>
							</section>
							<section>
								<div class="whole_line">
                                    <span class="input input--yoshiko textarea">
                                        <textarea class="input__field input__field--yoshiko @if($errors->first('text_area'))error @endif" name="text_area">{!! isset($OLD_text_area)?$OLD_text_area:'' !!}</textarea><span class="required"></span>
                                        <label class="input__label input__label--yoshiko" for="text_area">
                                            <span class="input__label-content input__label-content--yoshiko" data-content="{!!trans('system.no_access.form.t8')!!}">{!!trans('system.no_access.form.t8')!!}</span>
                                        </label>
                                    </span>
								</div>
							</section>
							<section id="ContactName">
								<div class="whole_line">
                                   <span class="input input--yoshiko">
                                        <input class="input__field input__field--yoshiko @if($errors->first('contact_name'))error @endif" type="text" name="contact_name" value="{!! isset($OLD_contact_name)?$OLD_contact_name:'' !!}"><span class="required"></span>
                                        <label class="input__label input__label--yoshiko" for="contact_name">
                                            <span class="input__label-content input__label-content--yoshiko" data-content="{!!trans('system.no_access.form.t9')!!}">{!!trans('system.no_access.form.t9')!!}</span>
                                        </label>
                                    </span>
								</div>
							</section>
							<section id="ContactEmail">
								<div class="whole_line">
                                    <span class="input input--yoshiko">
                                        <input class="input__field input__field--yoshiko  @if($errors->first('contact_email'))error @endif" type="text" name="contact_email" value="{!! isset($OLD_contact_email)?$OLD_contact_email:'' !!}"><span class="required"></span>
                                        <label class="input__label input__label--yoshiko" for="contact_email">
                                            <span class="input__label-content input__label-content--yoshiko" data-content="{!!trans('system.no_access.form.t10')!!}">{!!trans('system.no_access.form.t10')!!}</span>
                                        </label>
                                    </span>
								</div>
							</section>
                            <section>
                                <p>
                                    <input id="iagree" class="chb" type="checkbox" name="agreed" {!! Request::old('agreed') ? 'checked' : '' !!} />
                                    <label @if($errors->first('agreed_access')) class='error' @endif for="iagree">{!! trans('system.no_access.iagree') !!}</label>
                                </p>
                            </section>
                            <section class="cf access_captcha">
                                <div class="label_left" style="margin-right:12px;">
                                    <img class="captcha_img" src="{!! asset('captcha'). '?brand=' . ((isset($PAGE->brand) && $PAGE->brand == SMART) ?'smart':'daimler') !!}">
                                    <div class="reset_captcha"></div>
                                </div>
                                @if(MOBILE)
                                    <div style="width:100%;height:40px;"></div>
                                @endif
                                <div class="label_left" style="width:354px">
                                    <span class="input input--yoshiko whole_line">
                                        <input class="input__field input__field--yoshiko @if($errors->first('captcha'))error @endif" type="text" name="captcha" autocomplete="off"/>
                                        <label class="input__label input__label--yoshiko" for="captcha">
                                            <span class="input__label-content input__label-content--yoshiko" data-content="{!!trans('system.no_access.form.captcha')!!}">{!!trans('system.no_access.form.captcha')!!}</span>
                                        </label>
                                    </span>
                                </div>
							</section>
							<section class="more_info">
								<input class="submit_button" type="submit" value="{!! trans('system.no_access.form.button') !!}">
							</section>
							<div style="clear:both;"></div>
						</div>
					</div>

			</form>

			@endif
		</div><!-- main -->
        <script type="text/javascript" src="{!! mix('/js/classie.js') !!}"></script>
	</section>
@stop

@extends('layouts.base')
@section('bodyClass'){!! 'basic' !!}@stop
@section('left_nav')

@stop
@section('main')
	<section>
		<div class="user_profile_fields">
			<h1>@lang('system.profile_success.page.title')</h1>
			<h1><small>@lang('system.profile_success.subheading')</small></h1>

			<p>@lang('system.profile_success.contents')</p>
				@if(!empty($IS_OPT_OUT))
					<p>@lang('system.profile_success.cancellation')</p>
				@elseif(!empty($IS_OPT_IN))
					<p>@lang('system.profile_success.subscription')</p>
				@endif
				<p>@lang('system.profile_success.further')</p>
		</div><!-- main -->
	</section>
@stop
@section('right')

@stop
@section('farright')

@stop

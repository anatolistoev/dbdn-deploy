@extends('layouts.base')
@section('bodyClass'){!! 'basic' !!}@stop

@section('main')
	<section>
		<div class="user_profile_fields">

			@if (Session::has('success'))
                <h1>@lang('system.forgotten_sent.page.title') </h1>
				<h1><small>@lang('system.forgotten_sent.page.subheading')</small></h1>
                <p>@lang('system.forgotten_sent.contents')</p>
			@else
				<h1>@lang('system.forgotten_form.page.title') </h1>
				<h1><small>@lang('system.forgotten_form.contents')</small></h1>
				{!! Form::open(array(
					"url"       	=> "forgotten",
					"autocomplete"	=> "off",
					"id"			=> "forgottenForm"
				)) !!}

					<section>
						<div class="whole_line">
                            <span class="input input--yoshiko">
                            {!! Form::text("username", Request::get("username"),array('class'=> 'input__field input__field--yoshiko')) !!}
                            <label class="input__label input__label--yoshiko" for="username">
                                <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.username')">@lang('profile.username')</span>
                            </label>
                        </span>

						</div>
					</section>
                    <section class="cf captcha_wrapper">
                        <div class="label_left">
                            <img class="captcha_img" src="{!! asset('captcha'). '?brand=' . ((isset($PAGE->brand) && $PAGE->brand == SMART) ?'smart':'daimler') !!}">
                            <div class="reset_captcha"></div>
                        </div>
                        <div class="label_left">
                            <span class="input input--yoshiko">
                                <input autocomplete="off" type="text" name="captcha" class="input__field input__field--yoshiko @if($errors->first('captcha'))error @endif">
                                <label class="input__label input__label--yoshiko" for="captcha">
                                    <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.enter.code')">@lang('profile.enter.code')</span>
                                </label>
                            </span>
                        </div>
                    </section>

					<section class="more_info">
						<input class="submit_button" type="submit" value="@lang('profile.send')">
					</section>
				{!! Form::close() !!}

			@endif

		</div>
        <script type="text/javascript" src="{!! mix('/js/classie.js') !!}"></script>
	</section>
@stop

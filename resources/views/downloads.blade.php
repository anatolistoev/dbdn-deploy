@extends('basic')
@section('scripts')
@parent
	<script type="text/javascript" src="{!! mix('/js/downloads_filter.js')!!}"></script>
@stop

@section('left_nav')
    <section class="basic_nav_opened">
        <div class="basic_nav basic_menu popup"></div>
    </section>

    @if($PAGE->overview != 0)
        <section class="basic_nav_opened filters best_practice">
            <div class="basic_nav tablet-hidden">
                @include('partials.basic_nav')
            </div>
        </section>
    @endif
@stop

{{--
@section('left_nav')
	<section class="basic_nav_closed" id='basic_nav_closed'>
        <div></div>
        <span>@lang('template.navigation.menu')</span>
	</section>
	<section class="basic_nav_opened">
		<div class="basic_nav">
			@if(isset($SECTION_PID_INDEX, $L2ID, $SECTION_PID_INDEX[$L2ID]) && $L2ID > 1)
				@foreach($SECTION_PID_INDEX[$L2ID] as $L2page)
					@if($L2page->in_navigation == 1 && in_array($L2page->id, $PATH_IDS))
						<p class="level2_title">{!! $L2page->title !!}</p>
						<span id="menu_close"></span>
						<div style="clear:both;"></div>
					@endif
				@endforeach
			@endif
            @include('partials.basic_nav')
		</div>
	</section>
@stop
--}}

@section('main')
	@if($PAGE->overview != 0)
		@include('partials.download_overview')
	@else
    <section>
        @include('partials.back_button')
    </section>
    <section class="download-resource-single">
		<div id='downloads_stats' class="overview cf">
			<div class="stats">
				@if($PAGE->ratings()->count() > 0)<span class="rate">{!!$PAGE->ratings()->count()!!}</span>@endif
				@if($PAGE->activeComments()->count() > 0)<span class="comments">{!!$PAGE->activeComments()->count()!!}</span>@endif
				<span class="views">{!!$PAGE->visits!!}</span>
			</div>
			@if($PAGE->joinTags() && $PAGE->template == 'best_practice')
				<div class="tags">{!!$PAGE->joinTags()!!}</div>
			@endif
		</div>
		@if($CONTENTS['main']!= '')
			<div class="heading-row downloads">
                {!! $CONTENTS['main'] !!}
				<div style="clear:both;"></div>
			</div>
		@else
			<div class="heading-row downloads">
				<h1>{!! $CONTENTS['title'] !!}</h1>
				<div style="clear:both;"></div>
			</div>
		@endif
		@include('partials.download_list')
	</section>
	@endif
@overwrite
@section('right')

@stop

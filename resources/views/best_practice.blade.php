@extends('basic')
@section('scripts')
@parent
    <script type="text/javascript" src="{!! mix('/js/downloads_filter.js')!!}"></script>
@stop

@section('left_nav')
    <section class="basic_nav_opened">
        <div class="basic_nav basic_menu popup"></div>
    </section>

    @if($PAGE->overview != 0)
        <section class="basic_nav_opened filters best_practice">
            <div class="basic_nav tablet-hidden">
                @include('partials.basic_nav')
            </div>
        </section>
    @endif
@stop

@section('main')
    @if($PAGE->overview != 0)
        @include('partials.best_practice_overview')
    @else
        @include('partials.content')
    @endif

@overwrite

@section('right')

@stop

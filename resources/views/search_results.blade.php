@extends('basic')
@section('scripts')
    @parent
    <script type="text/javascript" src="{!! mix('/js/downloads_filter.js')!!}"></script>
@stop
@section('bodyClass'){!! 'basic' !!}@stop

{{--
@section('left_nav')
    <section class="basic_nav_closed">
        <div></div>
        <span>@lang('template.navigation.menu')</span>
    </section>
    <section class="basic_nav_opened">
        <div class="basic_nav">
            <p class="level2_title">@lang('template.navigation.search.title')</p>
            <span id="menu_close"></span>
            <div style="clear:both;"></div>
            @include('partials.basic_nav')
        </div>
    </section>
@stop
--}}

@section('main')
    <div class='search-form-wrapper'>
        <section class="search-section">
            <div id="searchDiv">
                {!! Form::open(array('id'=>"searchForm", 'url'=>url('search'))) !!}
                    <input type="hidden" name="action" value="search" />
                    <?php $value = $Q .' ['.sizeof($ROWS).']'; ?>
                    <input name="search" id="search" placeholder="@lang('template.search_box.label')" value="{!! $value !!}" type="text"/>
                    <button type="submit" class="newSearchBtn"></button>
                {!! Form::close() !!}

                <div class="basic_nav tablet-hidden">
                    @include('partials.basic_nav')
                </div>
            </div>
        </section>
    </div>

    <div class="glossary-search-container cf">
         @include('partials.results')
    </div><!-- main -->
@stop

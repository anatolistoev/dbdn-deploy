<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta charset="UTF-8">
    @if(isset($PDF_HTML) && $PDF_HTML)
    <base href="/" target="_blank">
    @endif
    @if($PAGE->brand == SMART)
        <link rel="stylesheet" href="css/pdfSmart.css"  type="text/css" />
    @else
    	<link rel="stylesheet" href="css/pdf.css"  type="text/css" />
    @endif
</head>
<body class="{!! ($PAGE->brand == SMART ? 'smart' : 'basic') !!}">
    <span style="display: none;">0123456789>@lang('template.pdf.from').@lang('template.pdf.to')</span>
    <div class="header">
        <?php
        // Date and time of generating PDF
        if ($PAGE->template == 'downloads') {
            $publish_date = $DOWNLOAD_FILES->first()->file->updated_at;
        } else {
            $publish_date = $PAGE->updated_at;
        }
        if (Session::get('lang') == LANG_EN) {
            $time = $publish_date->formatLocalized('%B %d, %Y at %H:%M');

        } else {
            $time = $publish_date->formatLocalized('%d. %B %Y um %H:%M');
        }
        ?>
<!--        <div class='header_info'>@lang('template.pdf.header', array('time'=>$time))</div>-->
        <div class="column">
            @if(isset($PAGE->brand) && $PAGE->brand == SMART)
                <p class="pdf-header">@lang('template.print.header')</p>
                <img class="header_image" src="img/template/nav/smart.png" />
            @else
                <img class="header_image" src="img/template/nav/daimler.png" />
                <p class="pdf-header">@lang('template.print.header')</p>
            @endif
        </div>
    </div>
    <div class="breaker">@lang('template.pdf.footer',array('time'=>$time))</div>
    <div class="pdf-body">
        <div class="date-and-rating" >
            <p class="date">
                @if(Session::get('lang') == LANG_EN)
                    {!!$publish_date->formatLocalized('%B %d, %Y')!!}
                @else
                    {!!$publish_date->formatLocalized('%d. %B %Y')!!}
                @endif
            </p>
            <!--<div class="stats">
                <span class="views"><img width="25" src="img/template/views-black.png" /><span>{!!$PAGE->visits!!}</span></span>
                @if($PAGE->ratings()->count() > 0)<span class="rate"><img width="25" src="img/template/rate-black.png" /><span>{!!$PAGE->ratings()->count()!!}</span></span>@endif
                @if($PAGE->activeComments()->count() > 0)<span class="comments"><img width="25" src="img/template/comments-black.png" /><span>{!!$PAGE->activeComments()->count()!!}</span></span>@endif
            </div>-->
        </div>
        <div id="pdf-tags">
            @if($PAGE->template != 'downloads' && $PAGE->template != 'basic' && $PAGE->template != 'exposed')
                <?php $tags =  $PAGE->joinTags()?>
                @if($tags)
                <img src="img/template/tags.png" /><span>{!!$tags!!}</span>
                @endif
            @endif
        </div>
        <div class="middle_page_text">
            <div id="content_main">
                @if(isset($CONTENTS['main']))
                    @yield('main_contents', $CONTENTS['main'])
                @endif
                @if( $PAGE->template == 'downloads' )
                    @if($DOWNLOAD_FILES != NULL && $DOWNLOAD_FILES->count() )
                        <div id="downloads">
                        @foreach($DOWNLOAD_FILES as $key=>$download)
                            <div class="downloadDiv">
                                <div class="downloadCol1">
                                    <div class="image">
                                        <?php $ext = explode(',',$download->file->extension);
                                                $filepath = 'img/template/file_type/'.($PAGE->brand == SMART ? 'smart' : 'daimler').'/'.$ext[0].'.png';?>
                                        @if($download->file->hasThumbSourceFile())
                                            {!!\App\Helpers\HtmlHelper::generatePDFDownloadImg($download)!!}
                                        @elseif(is_file(public_path().'/'.$filepath))
                                            <img height="230" src='{!!$filepath!!}' style="margin-top: 45px;">
                                        @else
                                            {!!\App\Helpers\HtmlHelper::generateImg($DEFAULT_THUMBNAIL, \App\Helpers\FileHelper::MODE_BRAND_THUMB, 'content', \App\Helpers\FileHelper::getFileExtension($DEFAULT_THUMBNAIL),FALSE, array('max-height'=> '320px', 'max-width'=> '480px'))!!}
                                        @endif
                                    </div>
                                    <div class="link">
                                        <?php $parts = explode('/',$download->file->filename); echo end($parts); ?>
                                    </div>
                                    <div class="date">@if(Session::get('lang') == LANG_EN){!!$download->file->updated_at->formatLocalized('%B %d, %Y')!!}@else{!!$download->file->updated_at->formatLocalized('%d. %B %Y')!!}@endif</div>
                                    @if($PAGE->joinTags())
                                    <div class="tags"><span class="pdf-tags"><img src="img/template/tags.png" /><div style="display: inline-block">{!!$PAGE->joinTags()!!}</div></span></div>
                                    @endif
                                </div>
                                <div class="downloadCol2">
                                    @if(!is_file(public_path().'/'.$download->file->getPath(FILEITEM_REAL_PATH)))
                                        <div class="text-item">@lang('system.download_na')</div>
                                    @else
                                        @if(strlen($download->file->info->first()->description) > 1)
                                            <div class="text-item">
                                                <label>@lang('system.downloads_list.description'):</label>{!!$download->file->info->first()->description!!}
                                            </div>
                                        @endif
                                        @if(!is_null($PAGE->usage) && $PAGE->usage != '' &&  $PAGE->usage != 1)
                                            <div class="table-item first" id="downloadCol2"><label>@lang('system.downloads_list.usage'):</label>@lang('ws_general_controller.page_usage_'.\App\Helpers\UserAccessHelper::getPageConfiguration('usage',$PAGE->usage))</div>
                                        @endif
                                        @if(!is_null($PAGE->file_type) && $PAGE->file_type != '' && $PAGE->file_type != 1)
                                            <div class="table-item"><label>@lang('system.downloads_list.file_type'):</label>@lang('ws_general_controller.page_file_type_'.\App\Helpers\UserAccessHelper::getPageConfiguration('file_type',$PAGE->file_type))</div>
                                        @endif
                                        @if($download->file->extension != '')
                                            <div class="table-item" id="downloadCol2"><label>@lang('system.downloads_list.fileformat'):</label>{!!strtoupper($download->file->extension)!!}</div>
                                        @endif
                                        @if($download->file->size != '')
                                        <div class="table-item"><label>@lang('system.downloads_list.size'):</label>
                                            {!!\App\Helpers\FileHelper::formatSize($download->file->size)!!}
                                        </div>
                                        @endif
                                        @if($download->file->format != '' && $download->file->format != 0)
                                            <div class="table-item"><label>@lang('system.downloads_list.format'):</label>{!!\App\Helpers\UserAccessHelper::getPageConfiguration('file_format',$download->file->format)!!}</div>
                                        @endif
                                        @if( $download->file->dimensions != '')
                                            <div class="table-item"><label>@lang('system.downloads_list.dimensions'):</label>{!!$download->file->dimensions.' '.\App\Helpers\UserAccessHelper::getPageConfiguration('file_dimension',$download->file->dimension_units)!!}</div>
                                        @endif
                                        @if( $download->file->resolution != '')
                                            <div class="table-item"><label>@lang('system.downloads_list.resolution'):</label>{!!$download->file->resolution.' '.\App\Helpers\UserAccessHelper::getPageConfiguration('file_resolution', $download->file->resolution_units)!!}</div>
                                        @endif
                                        @if($download->file->color_mode != '' && $download->file->color_mode != 0)
                                            <div class="table-item"><label>@lang('system.downloads_list.color_mode'):</label>@lang('ws_general_controller.color_mode_' . \App\Helpers\UserAccessHelper::getPageConfiguration('file_color_mode', $download->file->color_mode))</div>
                                        @endif
                                        @if($download->file->lang != '')
                                            <div class="table-item"><label>@lang('system.downloads_list.lang'):</label>{!!$download->file->lang!!}</div>
                                        @endif
                                        @if($download->file->pages != '')
                                            <div class="table-item"><label>@lang('system.downloads_list.pages'):</label>{!!$download->file->pages!!}</div>
                                        @endif
                                        @if($download->file->version != '')
                                            <div class="table-item last"><label>@lang('system.downloads_list.version'):</label>{!!$download->file->version!!}</div>
                                        @endif
                                        @if($download->file->copyright_notes != '')
                                            <div class="text-item">
                                                <label>@lang('system.downloads_list.copyrights'):</label> {!!$download->file->copyright_notes!!}
                                            </div>
                                        @endif
                                        <?php
                                            if(Session::get('lang') == LANG_DE){
                                                $field = 'usage_terms_de';
                                            }else{
                                                $field = 'usage_terms_en';
                                            }
                                        ?>
                                        @if($download->file->$field != '')
                                            <div class="text-item">
                                                <label>@lang('system.downloads_list.usage_terms'):</label> {!!$download->file->$field!!}
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div><!-- downloads -->
                    @endif
                @endif
            </div>
        </div>
    </div>
    <div class="footer">
<!--         <div>
             <p>@lang('template.print.footer')</p>
         </div>-->
    </div>
</body>
</html>
<script type="text/php">
    // Set Page number and page total in PHP script rendered in Dompdf!
    if (isset($pdf))
    {
        $text = trans('template.pdf.from').'{PAGE_NUM}'.trans('template.pdf.to').'{PAGE_COUNT}';
        $size = 9;
        $height = $pdf->get_height() - 87.3;
        $width = $pdf->get_width() - 98;
        if ($pdf->get_page_count() > 9) {
            $width = $width - 8;
        }
        // Init brand for evalution in dompdf renderer!
        $brand = {!! $PAGE->brand !!};

        if ($brand == SMART) {
            $size = 8;
            $height = $height + 8.5;
            $fontName = 'smart_light';
        } else {
            $fontName = 'DaimlerCS-Regular';
        }

        $font = $fontMetrics->get_font($fontName, 'normal');

        $pdf->page_text($width, $height, $text, $font, $size);
    }
</script>
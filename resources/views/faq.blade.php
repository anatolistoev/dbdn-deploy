@extends('layouts.base')
@section('scripts')
@parent
    <script type="text/javascript" src="{!! mix('/js/faqs_list.js')!!}"></script>
    <script type="text/javascript" src="{!! mix('/js/downloads_filter.js')!!}"></script>
@stop
@section('bodyClass'){!! $PAGE->brand == SMART? 'smart' :'basic' !!}@stop


{{--
@section('left_nav')
    <section class="basic_nav_closed">
        <div></div>
        <span>@lang('template.navigation.menu')</span>
    </section>
    <section class="basic_nav_opened">
        <div class="basic_nav">
            <p class="level2_title">FAQ</p>
            <span id="menu_close"></span>
            <div style="clear:both;"></div>
            @include('partials.basic_nav')
        </div>
    </section>
@stop
--}}

@section('main')
<div class="faq-container" style="width:1180px;">
    <section  style="width:1100px;margin-left:40px;" class="title-container">
        <div>
            <p class="faq-title">{!! (isset($PAGE->brand)&& $PAGE->brand == SMART)? trans('smart.faq.header') :trans('template.faq.header')!!}</p>
        </div>
    </section>

    <section class="search-section">
        <div id="searchDiv">

            {!! Form::open(array('id'=>"searchForm_faq", 'url'=>url('faq/'.((isset($PAGE->brand) && $PAGE->brand == SMART) ? 'smart' :'daimler')))) !!}
                <input type="hidden" name="action" value="search" />
                <input name="search" id="search" placeholder="@lang('template.search_box.label')" value="{!! $Q !!}" type="text" />
                <button type="submit" class="newSearchBtn"></button>
            {!! Form::close() !!}

            <div class="basic_nav tablet-hidden">
                @include('partials.basic_nav')
            </div>
        </div>

        <div class="goDown">
            <a href=""></a>
        </div>
    </section>
    <section  class="title-container-responsive">
        <div>
            <p class="faq-title">{!! (isset($PAGE->brand)&& $PAGE->brand == SMART)? trans('smart.faq.header') :trans('template.faq.header')!!}</p>
        </div>
    </section>
    @if(isset($FAQS) && count($FAQS) > 0)
    <section style="width:1100px;margin-left:40px;">
        <div class="faq-list-menu">
             @include('partials.faq_list')
        </div>
    </section>
    @endif

</div>
@stop
@section('right')

@stop
@section('farright')

@stop

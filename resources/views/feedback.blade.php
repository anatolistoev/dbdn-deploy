@extends('layouts.base')
@section('bodyClass'){!! 'basic' !!}@stop
@section('path')
<div id="path"><span class="pathlink">
	<a href="{!! asset('home') !!}" class="pathlink">@lang('template.home')</a> /
	<a href="{!! asset($PAGE->id) !!}" class="pathlink">@lang('feedback.path')</a>
</span></div>
@stop
@section('main')
	<div id="main">
	<h1>@lang('feedback.page.title')</h1><br>
	@if(isset($SENT))
		<span class="normal">@lang('feedback.thanks')<br><br><a href="javascript:history.back();" class="backlink">@lang('feedback.back')</a><br><br></span>
	@else
		{!! Form::open(array('id' => 'feedbackForm')) !!}
			@lang('feedback.contents')<br>
			<b>{!! $SUBJECT_TITLE !!}</b><br><br>
			<label class="required">@lang('feedback.email')</label><br>
			{!! Form::text('email', Request::get('email') ) !!}<br>
			<label class="required">@lang('feedback.message')</label><br>
			{!! Form::textarea('comments', Request::get('comments') ) !!}<br>
			<img src="{!! url('captcha') !!}" width="147" height="75">
            <div class="reset_captcha"></div><br>
			<br>
			<label class="required">@lang('feedback.captcha')</label><br>
			<input type="text" name="captcha"><br>
			<a href="javascript:;" onclick="$('#feedbackForm').submit();" class="button" style="width:145px;">@lang('feedback.send')</a>
			<br>
		{!! Form::close() !!}
	@endif
	</div>	<!-- main -->
@stop
@section('right')
@stop

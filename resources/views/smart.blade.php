@extends('basic')
@section('bodyClass'){!! 'smart' !!}@stop
@section('main')
	<div id="main">
	@if(isset($CONTENTS['main']))
		@yield('main_contents', $CONTENTS['main'])
	@endif
	@if($PAGE->overview != 'No')
		@include('partials.overview')
	@else
		@include('partials.download_list')
	@endif
	</div>
@stop
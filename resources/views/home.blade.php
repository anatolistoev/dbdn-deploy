@extends('layouts.base')
@section('styles')
@parent
	<!--<link rel="stylesheet" href="{!! asset('/css/home.css') !!}" />-->
@stop
@section('scripts')
@parent
@stop
@section('bodyClass'){!! 'home' !!}@stop
@section('L2')

@stop
@section('bullet_nav') @stop
@section('banner')
<div id="banner" style="margin-bottom:0;@if(strlen($CONTENTS['banner'])< 10) height:0 @endif">
<!--	<div class="homeblock" id="hbA">
        <div class="hBlockContainer" id="hbACont" style="left: 0px;">-->
			{!! $CONTENTS['banner'] !!}
<!--		</div>
        <div class="background"></div>
        <div id="coverBackground"></div>
        <div class="hBlockPages"></div>
    </div>-->
<!--	<div class="hBlockNav">
		<a style="display:none;" class="hBlockLeft" href=""></a>
		<a style="display:none;" class="hBlockRight" href=""></a>
	</div>hbA-->
 <div class="linkDown"></div>

</div><!-- banner -->
@stop
@section('main')

    {{--
	<!-- hbB -->
<!--	<script>
    $(document).ready(function(){
        $('.quicklinks a').click(function(){
              var grid = false;
              if($('#oneperframe1').hasClass('overview')){
                    grid = true;
                }
            var el = this;
            $.ajax({
                url : "{!!asset('/front_blocks/'.$PAGE->id)!!}/"+$(el).attr('type'),
                type : "GET",
                success : function(data){
                    $('.heading-row h1').html($(el).html());
                    $('.overview').html(data);
                    if(!SliderInit){
                        SliderInit = createSlider();
                    }
                    fr = new SliderInit($('#oneperframe1'));
                    //resizeContainerImages('div.overview div.page div.image img',580,387);
                    if(grid || $('#oneperframe1 ul li').length < 2){
                        $('.view-grid').trigger('click');
                    }
                    if($('#oneperframe1 ul li').length < 2){
                        $('#homeblocks .btn_navigation, #view-controls').css('display', 'none');
                    }else{
                        $('#homeblocks .btn_navigation, #view-controls').css('display', 'block');
                    }
					imageProcess.setResolutionPictures('#wrapper');
                }
            })
            return false;
        })
    })

	</script>-->
    --}}

    @include('partials.guided_tour', array('tour_page'=>'home', 'from' => 0, 'to' => 7))

    {{--
	<section>
		<div id="searchDiv">
			{!! Form::open(array('id'=>"searchForm", 'url'=>url('search'))) !!}
				<input name="search" id="search" placeholder="@lang('template.search_box.label')" value="" type="text"/>
                <button type="submit" id="doSearch" class="newSearchBtn"></button>
			{!! Form::close() !!}
		</div>

        <div class="goDown">
             <a href=""></a>
        </div>
		<div style="clear:both;"></div>
	</section>
    --}}

	<section>
        @include('partials.homeblocks')
        <div style="clear:both;"></div>
        @if(sizeof($HOMEBLOCKS)<3)
            <script>
                $(function() {
                    $('.slider-grid').trigger('click');
                });
            </script>
        @endif
	</section>

    <style type="text/css">

        section#frontblock > div, section#oftheday > div {width:1200px;margin:0px auto;clear:both;}

        #frontblock .frontblock_col {float:left;width:260px;padding:37px 20px 20px 20px;font-family:DaimlerCS-Regular, Arial, "Helvetica Neue", Helvetica, sans-serif;}
        #frontblock .frontblock_col:hover {background: #e6e6e6;}
        #frontblock .frontblock_col:hover a.clp  {border-color: #c8c8c8;}
        #frontblock .frontblock_col a {display:block;box-sizing:border-box;padding:20px;color:black;font-weight:normal; margin-bottom: 0px; }
        #frontblock .frontblock_col a:last-child {border:none;}
        #frontblock .frontblock_col a.exp {padding-top:0;margin-top:20px;margin-bottom:34px;border:none;}
        #frontblock .frontblock_col a.clp {font:normal 14px/20px DaimlerCS-Demi, Arial, "Helvetica Neue", Helvetica, sans-serif;border-bottom:1px solid #e6e6e6;padding:15px 40px 15px 20px;}
        #frontblock .frontblock_col a.clp:hover {background-image: url("img/template/svg/story-layover.svg"); background-position:220px center; background-repeat: no-repeat;}
        #frontblock .frontblock_col a.clp:hover, #frontblock .frontblock_col a.exp:hover h3 {color:white !important;background-color:#00677f;}
        #frontblock .frontblock_col .slider-image {margin:0 0 10px -30px;}
        #frontblock .frontblock_col h2 {font:normal 24px/30px DaimlerCS-Demi, Arial, "Helvetica Neue", Helvetica, sans-serif;margin:0 0 37px 0;padding:0 0 0 20px;white-space:nowrap;}
        #frontblock .frontblock_col h3 {font-size:18px;font-weight:normal;width:220px;margin:0 0 10px -20px;padding:5px 20px;}
        #frontblock .frontblock_col .date {font-size:14px;padding:0;margin:10px 0 0 0;}
        #frontblock .frontblock_col .tags {font-size:14px;padding:0 0 0 20px;margin:10px 0 0 0;background-position: left center;}
        #frontblock .frontblock_col .gallery_image{ width: 280px; position: relative;}
        #frontblock .frontblock_col .gallery{ position: relative; margin-bottom: 5px;}
        #frontblock .frontblock_col .gallery a {border-left: 1px solid #e6e6e6; margin-bottom: 5px;}
        #frontblock .frontblock_col .gallery_image:after,
        #frontblock .frontblock_col .gallery_image:before,
        #frontblock .frontblock_col .gallery:after,
        #frontblock .frontblock_col .gallery:before{
            content: '';
            position: absolute;
            width: 100%;
            height: 100%;
            top: 2px;
            right: 32px;
            border: 1px solid #c8c8c8;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            border-top: none;
            border-right: none;
            pointer-events:none;
        }
        #frontblock .frontblock_col .gallery_image:before{ top:4px; right: 34px}
        #frontblock .frontblock_col .gallery:before{ top:4px; right: 4px; border-color: #e6e6e6;}
        #frontblock .frontblock_col .gallery:after{ top:2px; right: 2px; border-color: #e6e6e6;}
        #frontblock .frontblock_col:hover .gallery:after,#frontblock .frontblock_col:hover .gallery:before{border-color: #c8c8c8;}

        #oftheday .container {box-sizing:border-box;float:left;width:50%;padding:0 40px;}
        #oftheday .container h2 {font-weight:normal;background:#00677f;color:white;display:inline-block;padding:9px 16px;margin-top:40px;text-transform:uppercase;}
        #oftheday .container > a {background:black;color:white;display:inline-block;width:140px;padding:20px 0;margin-top:40px;text-align:center;text-transform:uppercase;}
        #oftheday .container > a:hover {background:#00677f;}
        #oftheday .container > h3 {font:normal 24px/30px DaimlerCS-Demi, Arial, "Helvetica Neue", Helvetica, sans-serif;margin:30px 0 40px 0;}
        #oftheday .container > p {font:normal 18px/26px DaimlerCS-Regular, Arial, "Helvetica Neue", Helvetica, sans-serif;}
        #oftheday .container > p a {color:#000;text-decoration:none;border-bottom:1px dashed #000;}


    </style>

    <section id="frontblock">
        <div>
            @if(isset($RECENT) && sizeof($RECENT))
                @include('partials.front_blocks',array('BLOCKS'=> $RECENT, 'TITLE'=> trans('template.interest.most_recent')))
            @endif
            @if(false && isset($SEARCHED) && sizeof($SEARCHED))
                @include('partials.front_blocks',array('BLOCKS'=> $SEARCHED, 'TITLE'=> trans('template.interest.most_searched')))
            @endif
            @if(isset($VIEWED) && sizeof($VIEWED))
                @include('partials.front_blocks',array('BLOCKS'=> $VIEWED, 'TITLE'=> trans('template.interest.most_viewed')))
            @endif
            @if(isset($RATED) && sizeof($RATED))
                 @include('partials.front_blocks',array('BLOCKS'=> $RATED, 'TITLE'=> trans('template.interest.most_rated')))
            @endif
            @if(isset($DOWNLOADED) && sizeof($DOWNLOADED))
                @include('partials.front_blocks',array('BLOCKS'=> $DOWNLOADED, 'TITLE'=> trans('template.interest.most_downloaded')))
            @endif
        </div>
        <div>&nbsp;</div>
    </section>


    <section id="oftheday">

        @if(MOBILE)

            <div class="container">
                <h2>@lang('template.home.faq.title')</h2>
                <h3>{!!$FAQ_OF_THE_DAY['q']!!}</h3>
                <p>{!!$FAQ_OF_THE_DAY['a']!!}</p>
                <a href="faq/daimler">@lang('template.home.faq.button')</a>
            </div>

            <div class="container">
                <h2>@lang('template.home.term.title')</h2>
                <h3>{!!$TERM_OF_THE_DAY['q']!!}</h3>
                <p>{!!$TERM_OF_THE_DAY['a']!!}</p>
                <a href="glossary">@lang('template.home.term.button')</a>
            </div>

        @else

            <div>&nbsp;<br />&nbsp;</div>
            <div>
                <div class="container">
                    <h2>@lang('template.home.faq.title')</h2>

                </div>
                <div class="container">
                    <h2>@lang('template.home.term.title')</h2>
                </div>
            </div>
            <div>
                <div class="container">
                    <h3>{!!$FAQ_OF_THE_DAY['q']!!}</h3>
                </div>
                <div class="container">
                    <h3>{!!$TERM_OF_THE_DAY['q']!!}</h3>
                </div>
            </div>
            <div>
                <div class="container">
                    <p>{!!$FAQ_OF_THE_DAY['a']!!}</p>
                </div>
                <div class="container">
                    <p>{!!$TERM_OF_THE_DAY['a']!!}</p>
                </div>
            </div>
            <div>
                <div class="container">
                    <a href="faq/daimler">@lang('template.home.faq.button')</a>
                </div>
                <div class="container">
                    <a href="glossary">@lang('template.home.term.button')</a>
                </div>
            </div>

        @endif

    </section>

@stop


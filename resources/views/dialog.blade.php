@extends('basic')
@section('path')
<div id="path"><span class="pathlink">
	<a href="{!! asset('home') !!}" class="pathlink">@lang('template.home')</a> /
	<a href="{!! asset($PAGE->id) !!}" class="pathlink">@lang('dialog.'.$PAGE->id)</a>
</span></div>
@stop
@section('main')
	<div id="main">
		<h1>@lang('dialog.page.title')</h1>
		<br>
		@if(isset($SUCCESS))

			@lang('dialog.sent')
		@else
			<b>@lang('dialog.contents')</b>
			<br><br>
			<span class="red">@lang('dialog.note')</span><br><br>

			{!! Form::open(array('url' => url('dialog'), 'id'=>'dialogForm')) !!}
			<label>@lang('dialog.academic_title')</label><br>
			{!! Form::text('academic_title', Request::old('academic_title')) !!}
			<br>
			<div class="fltlft">
				<label>@lang('dialog.first_name')</label><br>
				{!! Form::text('first_name', Request::old('first_name')) !!}
			</div>
			<div class="fltlft" style="margin-left:20px;">
				<label>@lang('dialog.last_name')</label><br>
				{!! Form::text('last_name', Request::old('last_name')) !!}
			</div>
			<br class="clear">
			<label class="required">@lang('dialog.email')</label><br>
			{!! Form::text('email', Request::old('email')) !!}
			<br>
			<label>@lang('dialog.subject')</label><br>
			{!! Form::text('subject', Request::old('subject')) !!}
			<br>
			<label class="required">@lang('dialog.message')</label><br>
			{!! Form::textarea('messageText', Request::old('messageText')) !!}
			<br>
			<img src="{!! url('captcha') !!}" width="147" height="75">
            <div class="reset_captcha"></div><br>
			<br>
			<label class="required">@lang('dialog.captcha')</label><br>
			<input type="text" name="captcha">
			<br>
			@lang('dialog.note2') <br>
			<br>
			<a href="#" onclick="document.getElementById('dialogForm').submit();" class="button" style="width:150px;">@lang('dialog.send')</a>
				{!! Form::close() !!}
		@endif
		<br><br><br><br><br><br>
</div><!-- main -->
@stop
@section('right')
	<div id="right">
		{{-- @include('partials.l4asl5') --}}
		@include('partials.related_links')
	</div>
@stop
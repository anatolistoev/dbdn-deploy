@extends('layouts.base')
@section('bodyClass'){!! 'basic' !!}@stop
@section('path')

@stop
@section('main')
	<div id="main" class="sitemap-container">
		<h1>Sitemap</h1>
		<br>   
                <ul class="level-one">  
		<!--{{--@foreach($PAGES as $page)
			{!! link_to($page->slug, $page->body, array('class'=>'smLink smLevel'.min($page->maxLevel,4) )) !!}
		@endforeach--}}-->
		@foreach($BYPARENT[1] as $key => $page)
                    <li>
                        <a href="{!!url($page->slug)!!}">{!!$page->title!!}</a>
			@if(isset($BYPARENT[$page->id]) && is_array($BYPARENT[$page->id]))
                            <ul class="level-two backbone">
                                @foreach($BYPARENT[$page->id] as $key => $page)
                                    <li class="middle">
                                        <a href="{!!url($page->slug)!!}">{!!$page->title!!}</a>
                                        @if(isset($BYPARENT[$page->id]) && is_array($BYPARENT[$page->id]))
                                            <ul class="level-three">
                                                @foreach($BYPARENT[$page->id] as $key => $page)
                                                    <li class="middle">
                                                        <a href="{!!url($page->slug)!!}">{!!$page->title!!}</a>
                                                        @if(isset($BYPARENT[$page->id]) && is_array($BYPARENT[$page->id]))
                                                            <ul class="level-four">
                                                                @foreach($BYPARENT[$page->id] as $key => $page)
                                                                    <li class="middle">
                                                                        <a href="{!!url($page->slug)!!}">{!!$page->title!!}</a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        @endif
                                                    </li>
                                                @endforeach     
                                            </ul>
                                        @endif
                                    </li>
                                @endforeach 
                            </ul>         
                        @endif
                    </li>
                @endforeach 
                </ul>
		<br><br>
	</div><!-- main -->
@stop
@section('right')
	
@stop
@section('farright')
	
@stop
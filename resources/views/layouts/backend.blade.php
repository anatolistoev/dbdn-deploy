<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="text/html; charset=UTF-8" http-equiv="content-type">
		
		<meta name="csrf-token" content="{!! csrf_token() !!}" />

		<title>Daimler Brand &amp; Design Navigator</title>
		@include('partials_backend.backend_global_insert')
		@section('scripts')
			<script type="text/javascript" src="{!! url('/js/jquery.dataTables.js')!!}"></script>
			<script type="text/javascript" src="{!! url('/js/jquery.dataTables_extensions.js')!!}"></script>
			<script type="text/javascript" src="{!! url('/js/functions.js')!!}"></script>
			<link rel="stylesheet" media="all" type="text/css" href="{!! asset('/css/smoothness/jquery-ui-1.10.4.custom.css')!!}"/>
			<link rel="stylesheet" media="all" type="text/css" href="{!! url('/js/jquery-ui-timepicker-addon.css')!!}"></link>
			<script type="text/javascript" src="{!! url('/js/jquery-ui.min.js')!!}"></script>
			<script type="text/javascript" src="{!! url('/js/jquery-ui-timepicker-addon.js')!!}"></script>
			<script src="{!! url('/js/jquery-ui-sliderAccess.js')!!}" type="text/javascript"></script>
		@show
		@section('styles')
		<link rel="stylesheet" href="{!! url('/css/jquery.dataTables.css')!!}" />
		@show
        <link rel="SHORTCUT ICON" href="{!! asset('/favicon48.ico') !!}" type="image/x-icon"/>
        <link rel="icon" href="{!! asset('/DAI_Favicon_16.png')!!}" sizes="16x16" type="image/png"/>
        <link rel="icon" href="{!! asset('/DAI_Favicon_32.png') !!}" sizes="32x32" type="image/png">
        <link rel="icon" href="{!! asset('/DAI_Favicon_96.png')!!}" sizes="96x96" type="image/png"/>
        <link rel="icon" href="{!! asset('/DAI_Favicon_196.png')!!}" sizes="196x196" type="image/png"/>
        <link rel="icon" href="{!! asset('/DAI_Favicon_160.png')!!}" sizes="160x160" type="image/png"/>
        <link rel="apple-touch-icon" href="{!! asset('/DAI_Favicon_57.png')!!}" sizes="57x57"/>
        <link rel="apple-touch-icon" href="{!! asset('/DAI_Favicon_72.png')!!}" sizes="72x72"/>
	</head>
	<body>
		<div class="wrapper">
			<div class="page_title">@lang('backend.page_title')</div>
			@section('admin_info')
			<div id="admin_info">
				 <div class="admin_data">
						<p class="admin_name">{!!Auth::user()->first_name!!} {!!Auth::user()->last_name!!}</p>
						<p id="user_role">
    						@lang('backend_users.form.role.member')
						</p>
                        <script> $('p#user_role').text(getRoleTextByID({!!Auth::user()->role!!})); </script>
                </div>

				<!--<div class="note">
					<a href="" title="@lang('backend.messages')"><img src="{!! url('/img/backend/message_icon.png')!!}" title="@lang('backend.messages')" /></a>
					<p>11</p>
				</div>
				<div class="note">
					<a href="" title="@lang('backend.tasks')"><img src="{!! url('/img/backend/task_icon.png')!!}" title="@lang('backend.tasks')" /></a>
					<p>11</p>
				</div>-->
			</div>
			@show
			<div style="clear:both; height:1px;"></div>
			@section('top_navigation')
			<div class="top_navigation cf">
				@if(\App\Helpers\UserAccessHelper::hasAccess('workflow'))<a href="{!! url('/cms')!!}" class="nav_box tasks @if($ACTIVE == 'workflow')active @endif" >@lang('backend.tasks')</a>@endif
				@if(\App\Helpers\UserAccessHelper::hasAccess('pages'))<a href="{!! url('/cms/pages')!!}" class="nav_box content @if($ACTIVE == 'content')active @endif">@lang('backend.content')</a>@endif
				@if(\App\Helpers\UserAccessHelper::hasAccess('downloads'))<a href="{!! url('/cms/downloads')!!}" class="nav_box downloads @if($ACTIVE == 'downloads')active @endif" >@lang('backend.downloads')</a>@endif
				@if(\App\Helpers\UserAccessHelper::hasAccess('images'))<a href="{!! url('/cms/images')!!}" class="nav_box @if($ACTIVE == 'images')active @endif" style="background-image:url('{!! url('/img/backend/images_icon.png')!!}')">@lang('backend.images')</a>@endif
                @if(\App\Helpers\UserAccessHelper::hasAccess('faqs'))<a href="{!! url('/cms/faqs')!!}" class="nav_box faqs @if($ACTIVE == 'faqs')active @endif" >@lang('backend.faqs')</a>@endif
                @if(\App\Helpers\UserAccessHelper::hasAccess('glossary'))<a href="{!! url('/cms/glossary')!!}" class="nav_box glossary @if($ACTIVE == 'glossary')active @endif" >@lang('backend.glossary')</a>@endif
				@if(\App\Helpers\UserAccessHelper::hasAccess('tags'))<a href="{!! url('/cms/tags')!!}" class="nav_box tags @if($ACTIVE == 'tags')active @endif" >@lang('backend.tags')</a>@endif
				@if(\App\Helpers\UserAccessHelper::hasAccess('newsletter'))<a href="{!! url('/cms/newsletter')!!}" class="nav_box newsletter @if($ACTIVE == 'newsletter')active @endif" >@lang('backend.newsletter')</a>@endif
				@if(\App\Helpers\UserAccessHelper::hasAccess('users'))<a href="{!! url('/cms/users')!!}" class="nav_box users @if($ACTIVE == 'users')active @endif">@lang('backend.users')</a>@endif
				@if(\App\Helpers\UserAccessHelper::hasAccess('groups'))<a href="{!! url('/cms/groups')!!}" class="nav_box groups @if($ACTIVE == 'groups')active @endif">@lang('backend.groups')</a>@endif
				<!--<a class="nav_box" style="background-image:url('{!! url('/img/backend/settings_icon.png')!!}');float:right;margin-left:112px;">@lang('backend.settings')</a>
				<a class="nav_box" style="background-image:url('{!! url('/img/backend/save_icon.png')!!}');float:right;">@lang('backend.save')</a>-->
				<div id="admin_action">
<!--					<a href="" title="Edit" class="nav_box white edit">Edit</a>-->
					<a href="{!! url('/cms/logout')!!}" title="Logout" class="nav_box white logout">Logout</a>
				</div>
			</div>
			@show
			<div style="clear:both;height:1px;"></div>
			<div class="left_navigation">
				@yield('left_navigation')
			</div>
			<div class="content_wrapper">
				@yield('user_content')
			</div>
			<div style="clear:both;"></div>
		</div>
        <script>
            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
        </script>		
	</body>
</html>

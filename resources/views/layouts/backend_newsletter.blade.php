<!doctype html>
<html lang="en">
    <head>
        @if($IMG = asset('img/'))
        @endif
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="text/html; charset=UTF-8" http-equiv="content-type">
        
		<meta name="csrf-token" content="{!! csrf_token() !!}" />

        <title>Daimler Brand &amp; Design Navigator</title>
		@section('styles')

		<link rel="stylesheet" href="{!! url('/css/jquery.dataTables.css')!!}" />
        <link rel="stylesheet" href="{!! url('/css/backend_content.css')!!}" />
		<link rel="stylesheet" href="{!! url('/css/backend_newsletter.css')!!}" />
        @show
		@include('partials_backend.backend_global_insert')
        @section('scripts')

        <script type="text/javascript" src="{!! url('/js/jquery.dataTables.js')!!}"></script>
        <script type="text/javascript" src="{!! url('/js/jquery.dataTables_extensions.js')!!}"></script>
        <script type="text/javascript" src="{!! url('/js/functions.js')!!}"></script>
        <script type="text/javascript" src="{!! url('/js/newsletter_properties.js')!!}"></script>
        <script type="text/javascript" src="{!! url('/js/newsletter_content.js')!!}"></script>
        <script type="text/javascript" src="{!! url('/js/tinymce_4.1.7/tinymce.js')!!}"></script>
        <script type="text/javascript" src="{!! url('/js/newsletter_snippets.js')!!}"></script>
        <script type="text/javascript" src="{!! url('/js/snippets.js')!!}"></script>
        <script type="text/javascript" src="{!! url('/js/predefinedColors.js')!!}"></script>
		<link rel="stylesheet" media="all" type="text/css" href="{!! asset('/css/smoothness/jquery-ui-1.10.4.custom.css')!!}"/>
		<link rel="stylesheet" media="all" type="text/css" href="{!! url('/js/jquery-ui-timepicker-addon.css')!!}"></link>
		<script type="text/javascript" src="{!! url('/js/jquery-ui.min.js')!!}"></script>
		<script type="text/javascript" src="{!! url('/js/jquery-ui-timepicker-addon.js')!!}"></script>
		<script src="{!! url('/js/jquery-ui-sliderAccess.js')!!}" type="text/javascript"></script>
        <script type="text/javascript" src="{!! url('/js/tiny_mce_init.js')!!}"></script>
        <script>
            var lang = '{!!Session::get("lang", 1)!!}';
            var lang_label;
            if (lang == 1) {
                lang_label = 'en';
            } else {
                lang_label = 'de';
            }
        </script>
        @show
		@yield('edit_scripts')
    </head>
    <body class="@yield('bodyClass')">
        <div class="wrapper">
            <div class="page_title">@lang('backend.page_title')</div>
            @section('admin_info')
            <div id="admin_info">
                <div class="admin_data">
                    <p class="admin_name">{!!Auth::user()->first_name!!} {!!Auth::user()->last_name!!}</p>
                    <p id="user_role">
    					@lang('backend_users.form.role.member')
					</p>
                    <script> $('p#user_role').text(getRoleTextByID({!!Auth::user()->role!!})); </script>
                </div>
            </div>
            @show
            <div style="clear:both"></div>
            @section('top_navigation')
            <div class="top_navigation">
                @if(\App\Helpers\UserAccessHelper::hasAccess('workflow'))<a href="{!! url('/cms')!!}" class="nav_box tasks @if($ACTIVE == 'workflow')active @endif" >@lang('backend.tasks')</a>@endif
                @if(\App\Helpers\UserAccessHelper::hasAccess('pages'))<a href="{!! url('/cms/pages')!!}" class="nav_box content @if($ACTIVE == 'content')active @endif">@lang('backend.content')</a>@endif
                @if(\App\Helpers\UserAccessHelper::hasAccess('downloads'))<a href="{!! url('/cms/downloads')!!}" class="nav_box downloads @if($ACTIVE == 'downloads')active @endif" >@lang('backend.downloads')</a>@endif
                @if(\App\Helpers\UserAccessHelper::hasAccess('images'))<a href="{!! url('/cms/images')!!}" class="nav_box images @if($ACTIVE == 'images')active @endif" >@lang('backend.images')</a>@endif
                @if(\App\Helpers\UserAccessHelper::hasAccess('faqs'))<a href="{!! url('/cms/faqs')!!}" class="nav_box faqs @if($ACTIVE == 'faqs')active @endif" >@lang('backend.faqs')</a>@endif
                @if(\App\Helpers\UserAccessHelper::hasAccess('glossary'))<a href="{!! url('/cms/glossary')!!}" class="nav_box glossary @if($ACTIVE == 'glossary')active @endif" >@lang('backend.glossary')</a>@endif
                @if(\App\Helpers\UserAccessHelper::hasAccess('tags'))<a href="{!! url('/cms/tags')!!}" class="nav_box tags @if($ACTIVE == 'tags')active @endif" >@lang('backend.tags')</a>@endif
                @if(\App\Helpers\UserAccessHelper::hasAccess('newsletter'))<a href="{!! url('/cms/newsletter')!!}" class="nav_box newsletter @if($ACTIVE == 'newsletter')active @endif" >@lang('backend.newsletter')</a>@endif
				<div class="content_div">
                    @if(\App\Helpers\UserAccessHelper::hasAccess('users'))<a href="{!! url('/cms/users')!!}" class="nav_box users">@lang('backend.users')</a>@endif
                    @if(\App\Helpers\UserAccessHelper::hasAccess('groups'))<a href="{!! url('/cms/groups')!!}" class="nav_box groups">@lang('backend.groups')</a>@endif
                </div>

                <!--<a href="" class="nav_box" style="background-image:url('{!! url('/img/backend/settings_icon.png')!!}');float:right;margin-left:112px;">@lang('backend.settings')</a>-->
                <a id="page_workflow" class="nav_box workflow" >@lang('backend.workflow')</a>
				<a id="page_save" class="nav_box page_save" >@lang('backend.save')</a>
                {{-- <a id="preview" onClick="openPreview(event,{!!$NEWSLETTER->id!!}); return false;" class="nav_box preview" >@lang('backend.preview')</a> --}}

            </div>
            @show
            <div style="clear:both;height:1px;"></div>
            <div class="left_navigation">
                @yield('left_navigation')
            </div>
            <div class="content_wrapper">
                <div id="properties_wrapper" style="display:none;">
                    @include('partials_backend.newsletter_properties')
                </div>
				<div id="workflow_wrapper" style="display:none;">
                    @include('partials_backend.newsletter_workflow')
                </div>
                <div id="content_edit_newsletter_id" style="display:none;">{!!$NEWSLETTER->id!!}</div>
                @section('header')

                @show
                @section('banner')
                @if (isset($CONTENTS['header_d']) || isset($CONTENTS['header_m']))
                <div id="banner">
                    @if (isset($CONTENTS['header_d']) || isset($CONTENTS['header_m']))
                    @yield('banner_contents')
                    @endif
                </div>
                @endif
                @show

                @section('main')
                <div id="main">
					@if(isset($CONTENTS['content']))
					@yield('main_contents', $CONTENTS['content'])
					@endif
                </div>
                @show

                @section('footer')
                <div style="clear:both;"></div>
                @show

            </div>
            <div style="clear:both;"></div>
        </div>
        <script>
            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
        </script>        
    </body>
</html>

<!doctype html>
<html lang="en" class="@if(\App\Helpers\HtmlHelper::isIE() !== false) ie @endif">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    @include('partials.js_inline_headerASAP')
    <!-- ID {!! $PAGE->id !!} -->
    @if($IMG = asset('img/'))
    @endif
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="robots" content="index, follow" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="MobileOptimized" content="1280">
    <meta name="format-detection" content="telephone=no">

    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <title>Daimler Brand &amp; Design Navigator</title>
       @section('styles')
            @if(!TABLET && !MOBILE)
                <link rel="stylesheet" href="{!!mix('/css/all-desktop.css') !!}">
            @else
                <link rel="stylesheet" href="{!!mix('/css/all-hhd.css') !!}">
            @endif
            @if(isset($PAGE->brand) && $PAGE->brand == SMART)
                <link rel="stylesheet" href="{!! mix('/css/styleSmart.css') !!}" />
                <link rel="stylesheet" href="{!! mix('/css/tabletSmart.css') !!}" />
                <link rel="stylesheet" href="{!! mix('/css/phoneSmart.css') !!}" />
            @endif
        @show
        @section('scripts')
            <script>
                var paths = {
                    'images': '{!! asset('') !!}',
                    'gallery': '{!! asset('gallery') !!}'
                }
                var relPath  = '<?php echo asset(""); ?>';
                var lang = '{!!Session::get("lang", 1)!!}';
                var lang_label;
                if (lang == 1) {
                    lang_label = 'en';
                } else {
                    lang_label = 'de';
                }

                var TABLET = <?php echo (int)TABLET;?>;
                var MOBILE = <?php echo (int)MOBILE;?>;

            </script>
            @include('partials.js_localize_fe')
                <script type="text/javascript" src="{!! mix('/js/all.js') !!}"></script>
            @include('partials/gallery_localization')
            <!--[if (gte IE 6)&(lte IE 8)]>
                <script type="text/javascript" src="{!! mix('/js/selectivizr-min.js') !!}"></script>
            <![endif]-->
        @show

    <link rel="SHORTCUT ICON" href="{!! asset('/favicon48.ico') !!}" type="image/x-icon"/>
    <link rel="icon" href="{!! asset('/DAI_Favicon_16.png')!!}" sizes="16x16" type="image/png"/>
    <link rel="icon" href="{!! asset('/DAI_Favicon_32.png') !!}" sizes="32x32" type="image/png">
    <link rel="icon" href="{!! asset('/DAI_Favicon_96.png')!!}" sizes="96x96" type="image/png"/>
    <link rel="icon" href="{!! asset('/DAI_Favicon_196.png')!!}" sizes="196x196" type="image/png"/>
    <link rel="icon" href="{!! asset('/DAI_Favicon_160.png')!!}" sizes="160x160" type="image/png"/>

    <link rel="apple-touch-icon" href="{!! asset('/DAI_Favicon_57.png')!!}" sizes="57x57"/>
    <link rel="apple-touch-icon" href="{!! asset('/DAI_Favicon_72.png')!!}" sizes="72x72"/>
    <link rel="apple-touch-icon" href="{!! asset('/DAI_Favicon_114.png')!!}" sizes="57x57"/>
    <link rel="apple-touch-icon" href="{!! asset('/DAI_Favicon_144.png')!!}" sizes="72x72"/>

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{!! asset('/DAI_Favicon_57.png')!!}" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{!! asset('/DAI_Favicon_72.png')!!}" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{!! asset('/DAI_Favicon_114.png')!!}" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{!! asset('/DAI_Favicon_144.png')!!}" />

    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="{!! asset('/DAI_Favicon_mstile_144.png')!!}" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Daimler" />
    <meta property="og:title" content="Startseite | Daimler" />
    <meta property="og:description" content="@lang('template.meta.description')" />
    <meta property="og:url" content="{!! Request::url() !!}?lang={!! App::getLocale() !!}" />
    <meta property="og:image" content="{!! asset('img/og/daimler_1200x1200.png') !!}" />

    @if(Config::get('app.isFE'))
        @include('partials.tracker')
    @endif

    </head>
    <body  class="@yield('bodyClass')" >
    <div id="basic_preloader">
        <div id="basic_preloader_overlay"></div>
        <div class="basic_lbLoading">
            <div class="cssload-rect1"></div>
            <div class="cssload-rect2"></div>
            <div class="cssload-rect3"></div>
        </div>
    </div>
        <div id="wrapper" class="no-touch" style="visibility: hidden;">
            @section('notifications')
                <div id="topNotifications">
                    <div id="cookie_notification">
                        <div class="notification_content">
                            @lang('system.cookie_notification.text')
                        </div>
                        <div class="buttons_wrapper">
                            <a class="close_button"></a>
                        </div>
                    </div><!-- cookie_notification -->

                    @if(isset($errors) && $errors->first() || Session::has('error'))
                    <div id="notification" @if(!$errors->first('success')) class="red" @endif>
                         <div id="notificationWrapper">
                            <div class="notification_content_wrapper">
                                <div class="notification_content">
                                    <p class="alert_text">
                                        @if(isset($errors) && $errors->first())
                                        @foreach($errors->all() as $message)
                                        {!! $message !!}<br>
                                        @endforeach
                                        @endif
                                        @if(Session::has('error'))
                                        @lang(Session::get('reason'))
                                        @endif
                                    </p>
                                    <div class="buttons_wrapper">
                                        <a class="ok_button btn">@lang('system.alert.yes')</a>
                                        <a class="no_button btn">@lang('system.alert.no')</a>
                                    </div>
                                </div>
                                <div class="buttons_wrapper">
                                    <a class="close_button"></a>
                                </div>
                            </div>
                        </div>
                    </div><!-- alerts -->
                    @else
                    <div id="notification">
                        <div id="notificationWrapper" class="cf">
                            <div class="notification_content_wrapper">
                                <div class="notification_content">
                                    @if (Session::has('gdpr_val'))
                                        <p class="alert_text">
                                            @lang('gdpr.operation.complete')
                                        </p>
                                    @else
                                        <p class="alert_text">
                                            @if(
                                                 ( !Session::has('intro_msg') && (Session::put('intro_msg', Session::get('lang')) || 1 ) )
                                                  ||
                                                 (
                                                    !in_array(Session::get('lang'), explode(',', Session::get('intro_msg') ) ) &&
                                                    ( Session::put('intro_msg', Session::get('intro_msg') . ',' .Session::get('lang') ) || 1 )
                                                 )
                                            )
                                                @lang('template.intro')
                                            @endif
                                        </p>
                                        <div class="buttons_wrapper">
                                            <a class="ok_button btn">@lang('system.alert.yes')</a>
                                            <a class="no_button btn">@lang('system.alert.no')</a>
                                        </div>
                                    @endif
                                </div>
                                <div class="buttons_wrapper">
                                    <a class="close_button"></a>
                                </div>
                            </div>

                        </div>
                    </div><!-- alerts -->
                    @endif
                </div>
            @show

            @section('header')
                <header class="no-sticky">
                    @if(!isset($PAGE->brand) || (isset($PAGE->brand) && $PAGE->brand !== SMART))
                        @if(MOBILE)
                        <img id="header_bg_1024" style="display:none;"  src="{!!asset('/img/template/dbdn_bg_768x120@2x.jpg')!!}"/>
                        @else
                            @if (TABLET)
                            <img id="header_bg_1024" style="display:none;"  src="{!!asset('/img/template/dbdn_bg_1024x200@2x.jpg')!!}"/>
                            <img id="header_bg_1280" style="display:none;"  src="{!!asset('/img/template/dbdn_bg_1280.jpg')!!}"/>
                            @else
                            <img id="header_bg_1920" style="display:none;"  src="{!!asset('/img/template/dbdn_bg_1920.jpg')!!}"/>
                            <img id="header_bg_1280" style="display:none;"  src="{!!asset('/img/template/dbdn_bg_1280.jpg')!!}"/>
                            @endif
                        @endif
                    @endif

                    <div id="header_wrap">

                        <div id="topLinks">
                            @if(!MOBILE)
                                @if(isset($ALT_LANG) && ($PAGE->langs >> ($ALT_LANG-1) & 1) == 1)
                                    <div class="topItem" id="language">
                                        <a href="{!! asset($PAGE->slug != '' ? $PAGE->slug : $PAGE->id) !!}?lang=@lang('template.altLocale')">@lang('template.altLang')</a>
                                    </div>
                                @endif
                                @if(Auth::check())
                                    <div class="topItem" id="watchlist"><a href="{!!url('/Watchlist')!!}">&nbsp;</a><span class="counter">{!! Session::get('watchlistCount') !!}</span></div>
                                    <div class="topItem" id="downloads"><a href="{!!url('/Download_Cart')!!}">&nbsp;</a><span class="counter">{!! Session::get('myDownloadsCount') !!}</span></div>
                                    <div class="topItem" id="profile"></div>
                                    <div class="popup profile-formular">
                                        <a class="popup-close" style="display: none;"></a>
                                        <a href="{!!url('/profile')!!}">@lang('template.profile')</a>
                                         <a href="{!!url('logout')!!}">@lang('template.logout')</a>
                                    </div>
                                @else
                                    <div class="topItem profile_{!! trans('template.currLocale') !!}" id="profile">
                                        <a href="{!!url('login/'.$PAGE->slug)!!}" >{!!(isset($PAGE->brand)&& $PAGE->brand == SMART)? trans('smart.login') :trans('template.login')!!}</a>
                                        <span>|</span>
                                        <a href="{!!url('/registration')!!}" id="signup">{!!(isset($PAGE->brand)&& $PAGE->brand == SMART)? trans('smart.registration') :trans('template.registration')!!}</a>
                                    </div>
                                @endif
                            @endif
                            <div class="topItem" id="privacy"><a href="{!!asset('privacy')!!}">@lang('template.privacy.header')</a></div>
                        </div>

                        <div id="logos">
                            <a class="logo_link" href="{!! asset('/home') !!}"><img class="logo_img" @if(isset($PAGE->brand) && $PAGE->brand == SMART)src="{!!asset('/img/template/brand_mark_smart.png')!!}" alt="Smart brand mark"  width="90px" @else src="{!!asset('/img/template/brand_mark_daimler_x2.png')!!}" alt="Daimler brand mark" width="227.5px" @endif  /></a>
                            <div id="navigatorLogo">Daimler Brand & Design Navigator</div>
                        </div>

                        <div id="navContainer">

                            @include('partials.navmenus')

                            @if(MOBILE)

                                @include('partials.hamburger')

                            @else

                                @section('L2')
                                    @include('partials.level2')
                                @show

                                <div class="flyout_search popup">
                                    {!! Form::open(array('id'=>"searchForm2", 'url'=>asset('search'))) !!}
                                        <input name="search" id="search2" value="@if(isset($Q)){!! $Q !!}@endif" type="text" placeholder="@lang('template.search_box.top.placeholder')" autocomplete="off" />
                                        <button type="submit" id="doFlyoutSearch" class="newSearchBtn"></button>
                                    {!! Form::close() !!}
                                    <fake class="popup-close"></fake>
                                </div>

                            @endif

                        </div>
                    </div>
                </header>
            @show

            @section('banner')
                @if(!MOBILE || 'home' === $PAGE->slug)
                    @if((isset($PAGE->overview) && $PAGE->overview != 1) || !isset($PAGE->overview))
                        @if (isset($CONTENTS['banner']) && trim($CONTENTS['banner']) != '')
                            <section>
                                <div id="fullsize_banner">{!! $CONTENTS['banner'] !!}</div>
                            </section>
                        @endif
                    @endif
                @endif
            @show

           <script type="text/javascript" src="{!! mix('/js/header_load.js') !!}"></script>

            @section('bullet_nav')
            <nav id="cd-vertical-nav" style="opacity:0;">
                <ul class="dot_items" id='dot_items' style="display: none">
                </ul>
                @if((!isset($PAGE->overview) || $PAGE->overview == 0) && ($PAGE->template == 'basic' || $PAGE->template == 'downloads' || $PAGE->template == 'best_practice' || $PAGE->template == 'exposed'))
                <ul class="form_item">
                    @if(Auth::check() && is_numeric($PAGE->id) && $PAGE->id > 1 && !($PAGE->template == 'basic' && $LEVEL <= 1))
                    <!-- Static items -->
                        @if(!isset($SUBSCRIBED) || $SUBSCRIBED == 0)
                            <li id="add-icon" class="right-menu-item first test">
                                {!! Form::open(array('url' => 'Watchlist', 'id' => 'watchlistForm', 'method'=>'PUT')) !!}
                                    {!! Form::hidden('pageid', $PAGE->id) !!}
                                    <a href="">
                                        <span class="cd-dot"></span>
                                        <span class="cd-label">@lang('template.addwatch')</span>
                                    </a>
                                {!! Form::close() !!}
                            </li>
                        @else
                            <li id="remove-icon" class="right-menu-item first">
                                {!! Form::open(array('url' => 'Watchlist', 'id' => 'watchlistForm', 'method'=>'DELETE')) !!}
                                    {!! Form::hidden('pageid', $PAGE->id) !!}
                                    <a href="">
                                        <span class="cd-dot"></span>
                                        <span class="cd-label">@lang('template.removewatch')</span>
                                    </a>
                                {!! Form::close() !!}
                            </li>
                        @endif
                    <li id="rate-icon" class="right-menu-item @if($RATING > 0) unactive @endif">
                        @if($RATING > 0)
                            <a href="">
                                <span class="cd-dot"></span>
                                <span class="cd-label">@lang('template.rate_page')</span>
                            </a>
                        @else
                        {!! Form::open(array('url' => 'rating/add', 'id' => 'rateForm', 'method'=>'PUT')) !!}
                            {!! Form::hidden('pageid', $PAGE->id) !!}
                            {!! Form::hidden('vote', 10) !!}
                            <a href="">
                                <span class="cd-dot"></span>
                                <span class="cd-label">@lang('template.rate_page')</span>
                            </a>
                        {!! Form::close() !!}
                        @endif
                    </li>
                    @endif
                    @if($PAGE->id != MERCEDES_BENZ)
                    <li id="print-icon" class="right-menu-item">
                        <a href="" onclick="window.open('{!! asset('print/'. (sizeof(Request::segments()) ? implode('/', Request::segments()) : $PAGE->id )) !!}','','width=800, height=550, resizable=yes, location=no, menubar=no,toolbar=yes,scrollbars=yes',true); return false;">
                            <span class="cd-dot"></span>
                            <span class="cd-label">@lang('template.print_page')</span>
                        </a>
                    </li>
					@endif
                    @if($PAGE->template != 'downloads' && $PAGE->id != MERCEDES_BENZ)
                    <li id="save-icon" class="right-menu-item">
                        <a href='{!! asset('pdf_print/'. $PAGE->slug . '?t=' . time() ) !!}' id="save-icon_button">
                            <span class="cd-dot"></span>
                            <span class="cd-label">@lang('template.print_pdf')</span>
                        </a>
                    </li>
                    @endif
                    @if(isset($RELATED_DOWNLOADS) && count($RELATED_DOWNLOADS) > 0)
                    <li id="download-icon" class="right-menu-item">
                        <a href="" onlick="return false;">
                            <span class="cd-dot"></span>
                            <span class="cd-label">@lang('template.downloads')</span>
                        </a>
                    </li>
                    @endif
                    @if(isset($RELATED_PAGES) && count($RELATED_PAGES) > 0)
                    <li id="links-icon" class="right-menu-item">
                        <a href="" onlick="return false;">
                            <span class="cd-dot"></span>
                            <span class="cd-label">@lang('template.related')</span>
                        </a>
                    </li>
                    @endif
                </ul>
                @endif
            </nav>
            @show
            @section('path')

            @show
            @section('left_nav')

            @show
            @section('main')
                @if(isset($PAGE->overview) && $PAGE->overview == 1)
                    @include('partials.overview')
                @else
                    @include('partials.content')
                @endif
            @show
            @section('footer')
                <section>
                    <div class="footer_content">
                    <div id="footer_wrap">
                        <div class="text_area">
                            @lang('template.footer.about')
                        </div>
                        <ul class="metalinks">
                            <li><a href="{!!asset('Provider')!!}">{!!(isset($PAGE->brand)&& $PAGE->brand == SMART)? trans('smart.provider') :trans('template.provider')!!}</a></li>
                            <li><a href="{!!asset('Legal_Notice')!!}">{!!(isset($PAGE->brand)&& $PAGE->brand == SMART)? trans('smart.notices') :trans('template.notices')!!}</a></li>
                            <li><a href="{!!asset('Privacy_Statement')!!}">{!!(isset($PAGE->brand)&& $PAGE->brand == SMART)? trans('smart.privacy.footer') :trans('template.privacy.footer')!!}</a></li>
                            <li><a href="{!!asset('Cookies')!!}">@lang('template.cookies')</a></li>
                            <li><a href="{!!asset('Terms_of_Use')!!}">{!!(isset($PAGE->brand)&& $PAGE->brand == SMART)? trans('smart.terms') :trans('template.terms')!!}</a></li>
                            <li><a href="{!!asset('Rules_for_Comment_Submissions')!!}">{!!(isset($PAGE->brand)&& $PAGE->brand == SMART)? trans('smart.commentterms') :trans('template.commentterms')!!}</a></li>
                            <li class="last"><a href="{!!asset('Contacts')!!}">@lang('template.contacts')</a></li>
                        </ul>
                        <div style="clear: both;"></div>
                    </div>
                    <div id="copyrights">
                        <div class="text">© {!! date("Y") !!} @lang('template.copyright')</div>
                    </div>
                    </div>
                </section>

                <footer class="no-sticky">
                    <a href="" class="goUp">&nbsp;</a>
                    <div id="tools">
                        <div class="tools_wrap">
                            <ul class="footer_tools">
                                <li id="help">
                                    <a href="{!!url('/help'.((isset($PAGE->brand) && $PAGE->brand == SMART)? '/smart': ''))!!}">@lang('template.tips')</a>

                                    <div class="popup" id="help-formular"

                                         @if( isset($errors) && $errors->has('dialog_error')) style="display:block;overflow:visible;" @endif>
										<h2>{!! (isset($PAGE->brand) && $PAGE->brand == SMART) ? '‡ ' . trans('template.ask_question') . '.' : trans('template.ask_question') !!}</h2>
                                        <a class="popup-close"></a>
                                        {!! Form::open(array('url' => 'dialog/'.$PAGE->slug,  'method'=>'POST')) !!}
                                            <div id="messageText_wrapper" class="{!!( $errors->has('dialog_error') && $errors->first('messageText')) ? 'error' : ''!!}">
                                            {!! Form::textarea('messageText', Request::old('messageText'), array('placeholder' => trans('template.dialog.messageText'), 'id' => 'messageText','maxlength'=> 1000)) !!}
                                            <div id="counter_wrapper"><span id="messageText_counter">0</span><span>/1000</span></div>
                                            </div>
                                            {!! Form::text('name', (Auth::check() ? Auth::user()->first_name.' '.Auth::user()->last_name : null), array('placeholder' => trans('template.dialog.name'), 'class'=> (( $errors->has('dialog_error') && $errors->first('name')) ? 'error' : '' ))) !!}
                                            {!! Form::text('email',(Auth::check() ? Auth::user()->email : null), array( 'placeholder' => trans('template.dialog.email'), 'class'=> (( $errors->has('dialog_error') && $errors->first('email')) ? 'error' : '' ))) !!}

											<img class="captcha_img" src="{!! asset('captcha'). '?brand=' . ((isset($PAGE->brand) && $PAGE->brand == SMART) ?'smart':'daimler') !!}">
                                            <div class="reset_captcha"></div>
                                            {!! Form::text('captcha', null, array('class' => 'captcha_input '.(( $errors->has('dialog_error') && $errors->first('messageText')) ? 'error' : '' ),'autocomplete'=>'off')) !!}
                                            {!! Form::submit(trans('template.dialog.send')) !!}
                                        {!! Form::close()!!}
                                        <div class="popup-ticker"></div>
                                    </div>
                                </li>
                                <li id="glossary">
                                    <a href="{!!url('/glossary')!!}">@lang('template.glossary')</a>
                                    <div id="glossary-formular" class="popup">
                                        <h2>@lang('template.terms.frequently')</h2>
                                        <a class="popup-close"></a>
                                                <div class="content">
                                                    <div class="scbr-footer">
                                                        <div class="scrollbar-rail">
                                        <ul>

                                        </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                        <a href="{!!url('/glossary')!!}" class="black-button" >@lang('template.terms.view_all')</a>
                                        <div class="popup-ticker"></div>
                                    </div>
                                </li>
                                <li id="faq">
                                    <a href="{!!(isset($PAGE->brand) && $PAGE->brand == SMART) ? url('/faq/smart') : url('/faq/daimler')!!}">@lang('template.faq')</a>
                                    @if(isset($RELATED_TAGS) && count($RELATED_TAGS) > 0)
                                        <div id="faq-formular" class="popup">
                                            <h2>@lang('template.faq.related')</h2>
                                            <a class="popup-close"></a>
                                                <div class="content">
                                                    <div class="scbr-footer">
                                                        <div class="scrollbar-rail">
                                            <ul>
                                                @foreach($RELATED_TAGS as $TAG)
                                                <li><a href="{!!(isset($PAGE->brand) && $PAGE->brand == SMART) ? url('/faq/smart/'.$TAG->faq_id.'/'.\App\Helpers\UriHelper::converturi($TAG->question)) : url('/faq/daimler/'.$TAG->faq_id.'/'.\App\Helpers\UriHelper::converturi($TAG->question))!!}">{!!$TAG->question!!}</a></li>
                                                @endforeach
                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            <a href="{!!(isset($PAGE->brand) && $PAGE->brand == SMART) ? url('/faq/smart') : url('/faq/daimler')!!}" class="black-button" >@lang('template.faq.view_all')</a>
                                            <div class="popup-ticker"></div>
                                        </div>
                                    @endif
                                </li>
                                <li id="sitemap"><a href="{!!url('/sitemap')!!}">@lang('template.sitemap')</a></li>
                            </ul>
                            <div class="user_tools">
                                @if(Auth::check())
                                <div class="topItem" id="watchlist"><span class="counter">{!! Session::get('watchlistCount') !!}</span><a href="{!!url('/Watchlist')!!}">&nbsp;</a></div>
                                <div class="topItem" id="downloads"><span class="counter">{!! Session::get('myDownloadsCount') !!}</span><a href="{!!url('/Download_Cart')!!}">&nbsp;</a></div>
                                <div class="topItem" id="profile"></div>
                                <div class="popup profile-formular">
                                    <a class="popup-close" style="display: none;"></a>
                                    <a href="{!!url('/profile')!!}">@lang('template.profile')</a>
                                     <a href="{!!url('logout')!!}">@lang('template.logout')</a>
                                </div>
                                @else
                                <div class="topItem profile_{!! trans('template.currLocale') !!}" id="profile">
                                    <a href="{!!url('login/'.$PAGE->slug)!!}" >{!!(isset($PAGE->brand)&& $PAGE->brand == SMART)? trans('smart.login') :trans('template.login')!!}</a>
                                    <span>|</span>
                                    <a href="{!!url('/registration')!!}" id="signup">{!!(isset($PAGE->brand)&& $PAGE->brand == SMART)? trans('smart.registration') :trans('template.registration')!!}</a>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>

                </footer>
            @show
            @section('alerts')
                <div id="alertsBG"></div>
                <div id="alerts">
                    <p class="alert_title">@lang('system.alert.title')</p>
                    <a class="close-button"></a>
                    <div id="alertsContents">
                        <p class="alert_text"></p>
                        <a class="ok_button">@lang('system.alert.ok')</a>
                    </div>
                </div><!-- alerts -->

                <!--
                <div id="_cookie_notification">
                    <p class="alert_title">@lang('system.cookie_notification.heading')</p>
                    <a class="close_button"></a>
                    <div id="alertsContents">
                        <p class="alert_text">
                            @lang('system.cookie_notification.text')
                        </p>
                        <a class="ok_button">@lang('system.cookie_notification.close')</a>
                    </div>
                </div>
                -->
                <!-- cookie_notification -->

                <div id="provider_privacy">
                    <p class="alert_title">@lang('system.provider_notification.heading')</p>
                    <a class="close_button"></a>
                    <div id="privalertsContents">
                        <p class="alert_text">
                            @lang('system.provider_notification.text')
                        </p>
                        <a class="ok_button">@lang('system.provider_notification.close')</a>
                    </div>
                </div><!-- provider_privacy -->
            @show
        </div><!--container-->
        @section('doc_end')
        @show
        <script>
            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
        </script>
        <!-- loading_bar -->
    </body>
</html>

<!doctype html>
<html lang="en">
    <head>
        @if($IMG = asset('img/'))
        @endif
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="text/html; charset=UTF-8" http-equiv="content-type">

		<meta name="csrf-token" content="{!! csrf_token() !!}" />
                
        <title>Daimler Brand &amp; Design Navigator</title>
		@section('styles')
		<link rel="stylesheet" href="{!! asset('/css/main.css') !!}" />
        @if(isset($PAGE->template) && $PAGE->template == 'smart')
        <link rel="stylesheet" href="{!! asset('/css/smart.css') !!}" />
        @endif
		@if(isset($PAGE->template) && $PAGE->template == 'home')
        <link rel="stylesheet" href="{!! asset('/css/home.css') !!}" />
        @endif

		<link rel="stylesheet" href="{!! url('/css/jquery.dataTables.css')!!}" />

        <link rel="stylesheet" href="{!! url('/css/backend_content.css')!!}" />
        @show
		@include('partials_backend.backend_global_insert')
        @section('scripts')
<!--        <script>
            var my_renderer = {
                postRender: function(ctrl) {
                    console.log(1);
                    if(tinymce.activeEditor){
                    ctrl.disabled(!tinymce.activeEditor.dom.getParent(tinymce.activeEditor.selection.getStart(), 'img'));

                    tinymce.activeEditor.selection.selectorChanged(selector, function(state) {
                        ctrl.disabled(!state);
                    });
                }
                }

            }
        </script>-->
        <script type="text/javascript" src="{!! url('/js/jquery.dataTables.js')!!}"></script>
        <script type="text/javascript" src="{!! url('/js/jquery.dataTables_extensions.js')!!}"></script>
        <script type="text/javascript" src="{!! url('/js/functions.js')!!}"></script>
<!--        <script type="text/javascript" src="{!! url('/js/content_related.js')!!}"></script>-->
        <script type="text/javascript" src="{!! url('/js/page_properties.js')!!}"></script>
        <script type="text/javascript" src="{!! url('/js/content_main.js')!!}"></script>
        <script type="text/javascript" src="{!! url('/js/content_downloads.js')!!}"></script>
        <!--<script type="text/javascript" src="http://tinymce.cachefly.net/4.1/tinymce.min.js"></script>-->
        <script type="text/javascript" src="{!! url('/js/tinymce_4.1.7/tinymce.js')!!}"></script>
        <script type="text/javascript" src="{!! url('/js/newsletter_snippets.js')!!}"></script>
        <script type="text/javascript" src="{!! url('/js/snippets.js')!!}"></script>
        <script type="text/javascript" src="{!! url('/js/predefinedColors.js')!!}"></script>
		<link rel="stylesheet" media="all" type="text/css" href="{!! asset('/css/smoothness/jquery-ui-1.10.4.custom.css')!!}"/>
		<link rel="stylesheet" media="all" type="text/css" href="{!! url('/js/jquery-ui-timepicker-addon.css')!!}"></link>
		<script type="text/javascript" src="{!! url('/js/jquery-ui.min.js')!!}"></script>
		<script type="text/javascript" src="{!! url('/js/jquery-ui-timepicker-addon.js')!!}"></script>
		<script src="{!! url('/js/jquery-ui-sliderAccess.js')!!}" type="text/javascript"></script>
		@if(isset($PAGE->template) && $PAGE->template == 'smart')
        <script>var pageTemplate = 'smart';</script>
        @endif
		@if(isset($PAGE->template) && $PAGE->template == 'home')
        <script>var pageTemplate = 'home';</script>
        @endif
        <script type="text/javascript" src="{!! url('/js/tiny_mce_init.js')!!}"></script>
        <script>
            var lang = '{!!Session::get("lang", 1)!!}';
            var lang_label;
            if (lang == 1) {
                lang_label = 'en';
            } else {
                lang_label = 'de';
            }
        </script>
        @show
		@yield('edit_scripts')
    </head>
    <body class="@yield('bodyClass')">
        <div class="wrapper">
            <div class="page_title">@lang('backend.page_title')</div>
            @section('admin_info')
            <div id="admin_info">
                <div class="admin_data">
                    <p class="admin_name">{!!Auth::user()->first_name!!} {!!Auth::user()->last_name!!}</p>
                    <p id="user_role">
                        @lang('backend_users.form.role.member')
                    </p>
                    <script> $('p#user_role').text(getRoleTextByID({!!Auth::user()->role!!})); </script>
                </div>
                <!--				<div class="admin_buttons">
                                                        <a href="{!! url('/cms/logout')!!}" title="Logout" class="nav_box white logout">Logout</a>
                                                </div>-->
                <!--<div class="note">
                    <a href="" title="{!!trans('backend.messages')!!}"><img src="{!! url('/img/backend/message_icon.png')!!}" title="{!!trans('backend.messages')!!}" /></a>
                    <p>11</p>
                </div>
                <div class="note">
                    <a href="" title="{!!trans('backend.tasks')!!}"><img src="{!! url('/img/backend/task_icon.png')!!}" title="{!!trans('backend.tasks')!!}" /></a>
                    <p>11</p>
                </div>-->
            </div>
            @show
            <div style="clear:both"></div>
            @section('top_navigation')
            <div class="top_navigation">
                @if(\App\Helpers\UserAccessHelper::hasAccess('workflow'))<a href="{!! url('/cms')!!}" class="nav_box tasks @if($ACTIVE == 'workflow')active @endif" >@lang('backend.tasks')</a>@endif
                @if(\App\Helpers\UserAccessHelper::hasAccess('pages'))<a href="{!! url('/cms/pages')!!}" class="nav_box content @if($ACTIVE == 'content')active @endif">@lang('backend.content')</a>@endif
                @if(\App\Helpers\UserAccessHelper::hasAccess('downloads'))<a href="{!! url('/cms/downloads')!!}" class="nav_box downloads @if($ACTIVE == 'downloads')active @endif" >@lang('backend.downloads')</a>@endif
                @if(\App\Helpers\UserAccessHelper::hasAccess('images'))<a href="{!! url('/cms/images')!!}" class="nav_box images @if($ACTIVE == 'images')active @endif" >@lang('backend.images')</a>@endif
                @if(\App\Helpers\UserAccessHelper::hasAccess('faqs'))<a href="{!! url('/cms/faqs')!!}" class="nav_box faqs @if($ACTIVE == 'faqs')active @endif" >@lang('backend.faqs')</a>@endif
                @if(\App\Helpers\UserAccessHelper::hasAccess('glossary'))<a href="{!! url('/cms/glossary')!!}" class="nav_box glossary @if($ACTIVE == 'glossary')active @endif" >@lang('backend.glossary')</a>@endif
                @if(\App\Helpers\UserAccessHelper::hasAccess('tags'))<a href="{!! url('/cms/tags')!!}" class="nav_box tags @if($ACTIVE == 'tags')active @endif" >@lang('backend.tags')</a>@endif
                <div class="content_div">
                    <a href="#" class="nav_box reports" >@lang('backend.reports')</a>
                    @if(\App\Helpers\UserAccessHelper::hasAccess('newsletter'))<a href="{!! url('/cms/newsletter')!!}" class="nav_box newsletter" >@lang('backend.newsletter')</a>@endif
                    @if(\App\Helpers\UserAccessHelper::hasAccess('users'))<a href="{!! url('/cms/users')!!}" class="nav_box users">@lang('backend.users')</a>@endif
                    @if(\App\Helpers\UserAccessHelper::hasAccess('groups'))<a href="{!! url('/cms/groups')!!}" class="nav_box groups">@lang('backend.groups')</a>@endif
                </div>

                <!--<a href="" class="nav_box" style="background-image:url('{!! url('/img/backend/settings_icon.png')!!}');float:right;margin-left:112px;">@lang('backend.settings')</a>-->
                {!! csrf_field() !!}
                <a id="page_workflow" class="nav_box workflow" >@lang('backend.workflow')</a>
				<a id="page_save" class="nav_box page_save" >@lang('backend.save')</a>
                <a id="preview" onClick="openPreview(event,{!!$PAGE->u_id!!}); return false;" class="nav_box preview" >@lang('backend.preview')</a>
                @if($PAGE->template!='downloads')<a id="pdf" onClick="getPDFPreview(event,{!!$PAGE->u_id!!}); return false;" class="nav_box pdf" >@lang('backend.pdf')</a>@endif
                <a id="print" onClick="openPreview(event,{!!$PAGE->u_id!!}, true); return false;" class="nav_box print" >@lang('backend.print')</a>
            </div>
            @show
            <div style="clear:both;height:1px;"></div>
            <div class="left_navigation">
                @yield('left_navigation')
            </div>
            <div class="content_wrapper">
                <div id="properties_wrapper" style="display:none;">
                    @include('partials_backend.page_properties')
                </div>
				<div id="workflow_wrapper" style="display:none;">
                    @include('partials_backend.page_workflow')
                </div>
				<div id="moderations_wrapper" style="display:none;">
                    @include('partials_backend.page_moderations')
                </div>
<!--                <div id="update_alert_wrapper" style="display:none;">
                    @include('partials_backend.page_update_alert')
                </div>	-->
                <div id="content_edit_page_id" style="display:none;">{!!$PAGE->id!!}</div>
                <div id="content_edit_page_u_id" style="display:none;">{!!$PAGE->u_id!!}</div>
                @section('header')
<!--                <div id="header">
                    <div id="topLinks"><a href="index.php" class="topLink">Home</a><a href="http://www.daimler.com" class="topLink" target="_blank">www.daimler.com</a><a href="{!!asset(974)!!}" class="topLink">{!!trans('template.dialog')!!}</a><a href="{!!asset(205)!!}" class="topLink">{!!trans('template.contacts')!!}</a><a href="{!! asset(trans('template.altLocale')) !!}/{!! $PAGE->id !!}" class="topLink">{!!trans('template.altLang')!!}</a><div id="searchDiv">{!! Form::open(array('id'=>"searchForm", 'url'=>asset('search'))) !!}<input type="text" name="search" id="search" value=""><input type="image" id="searchBtn" src="{!!$IMG!!}/template/searchBtn.gif">{!! Form::close() !!}</div>
                    </div> topLinks
                    <div id="logos"><div id="Dlogo"><a href="index.php"><img src="{!!$IMG!!}/template/daimler.gif" border="0"></a></div><div id="BDN">Daimler Brand &amp; Design Navigator</div></div>
                    <div id="navContainer">
                        <div id="nav">
                            @include('partials.navmenus')
							@if(isset($PAGE->template) && $PAGE->template != 'home')
								@section('L2')
									@include('partials.level2')
								@show
							@endif
                        </div> nav
                    </div> navContainer
                </div> header -->
                @show
                @section('banner')
                @if (isset($CONTENTS['banner']))
                <div id="banner">
                    @if(isset($CONTENTS['banner']))
                    @yield('banner_contents', $CONTENTS['banner'])
                    @endif
                </div>
                @endif
                @show
                @section('path')

                @show
                @section('main')
                <div id="main">
					@if(isset($CONTENTS['main']))
					@yield('main_contents', $CONTENTS['main'])
					@endif
                </div>
                @show
                @section('footer')
                <div style="clear:both;"></div>
                <div id="footer"><span>&copy; {!! date("Y") !!} @lang('template.copyright')  |  <a href="{!!asset(192)!!}">@lang('template.provider')</a>  |  <a href="{!!asset(193)!!}">@lang('template.notices')</a>  |  <a href="{!!asset(1739)!!}">@lang('template.cookies')</a>  |  <a href="{!!asset(194)!!}">@lang('template.privacy')</a>  |  <a href="{!!asset(980)!!}">@lang('template.terms')</a>  |  <a href="{!!asset(1518)!!}">@lang('template.commentterms')</a></span></div>
                @show

            </div>
            <div style="clear:both;"></div>
        </div>
        <script>
            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
        </script>        
    </body>
</html>

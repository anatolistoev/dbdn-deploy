@extends('layouts.backend')
@section('user_content')
<div class="page_error">
    <br>
    @if(isset($error))
    	<div id="main">
            <h1>There are errors:</h1>
            <p>{!!$message!!}</p>
        </div>
    @else
	<div id="main">
        <p>This page is currently being updated by another user.</p>
        <br>
        <p>Please try again later.</p>
    </div>
    @endif
<div>
@stop

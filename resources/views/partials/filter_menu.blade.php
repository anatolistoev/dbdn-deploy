@foreach($SECTION_PID_INDEX[$L2ID] as $L2page)
    @if($L2page->in_navigation == 1 && in_array($L2page->id, $PATH_IDS))
        <span id="menu_close" class="popup-close"></span>

        <div class="content cf fixedws filter-menu">
            <ul class="multi_nav">
                <li>
                    <h3>
                        <a href="{!! asset($L2page->slug != '' ? $L2page->slug : $L2page->id) !!}?filter=none">
                            @lang('template.navigation._'.$L2page->template)
                        </a>
                    </h3>
                </li>
            </ul>
            <ul class="multi_nav">
                <li>
                    <h3>
                        <a href="{!! asset($L2page->slug != '' ? $L2page->slug : $L2page->id) !!}?filter=time:desc">
                            @lang('template.navigation.newest')
                        </a>
                    </h3>
                </li>
            </ul>
            <ul class="multi_nav">
                <li>
                    <h3>
                        <a href="{!! asset($L2page->slug != '' ? $L2page->slug : $L2page->id) !!}?filter=viewed:desc">
                            @lang('template.navigation.most_viewed')
                        </a>
                    </h3>
                </li>
            </ul>
            <ul class="multi_nav">
                <li>
                    <h3>
                        <a href="{!! asset($L2page->slug != '' ? $L2page->slug : $L2page->id) !!}?filter=rating:desc">
                            @lang('template.navigation.most_rated')
                        </a>
                    </h3>
                </li>
            </ul>
        </div>

        @if(isset($MENUBLOCKS) && count($MENUBLOCKS)> 0)
        <div id='menu_carousel_wrapper'>
            <div class="menu_carousel">
                <div class="slider_title">@lang('template.recommended')</div>
                <div class="slider_container">
                    <a class="prev slider_hover" @if(!isset($MENUBLOCKS) || count($MENUBLOCKS) < 5) style="display:none"@endif></a>
                    <a class="next slider_hover" @if(!isset($MENUBLOCKS) || count($MENUBLOCKS) < 5) style="display:none"@endif></a>
                    <div class="cf" id="menu_slider">
                        <ul>
                            <?php $index = 1 ?>
                            <li index="{!!$index!!}">
                                @foreach($MENUBLOCKS as $key=>$row)
                                <div class="slider_element">
                                    <a href="{!! url($row['slug'] != '' ? $row['slug'] : $row['u_id']) !!}" class="page_link">
                                        <div class="slider-image">
                                            @if(strtolower(pathinfo($row['img'], PATHINFO_EXTENSION)) == 'svg')
                                                {!!\App\Helpers\FileHelper::getSVGContent(public_path().$row['img']);!!}
                                            @else
                                                {!!\App\Helpers\HtmlHelper::generateImg($row['img'], $row['type'], 'menu_carousel', isset($row['imgext'])? $row['imgext'] : \App\Helpers\FileHelper::getFileExtension($row['img']), isset($row['thumb_visible']), array("max-height"=> "146px", "max-width" => "220px"))!!}
                                            @endif

                                            @if(isset($row['protected']) && $row['protected'])
                                            <div class="protected" title='@lang('template.protected')'></div>
                                            @endif
                                        </div>
                                        <div class="title"><span>{!! $row['title'] !!}</span></div>
                                    </a>
                                </div>
                                @if($index % 4 == 0 && $index < count($MENUBLOCKS))
                            </li>
                            <li index="{!!$index/4 +1!!}">
                                @endif
                                <?php $index++; ?>

                                @endforeach
                            </li>
                        </ul>
                    </div>
                    <ul class="pages slider_hover"></ul>
                </div>
            </div>
            </div>
        @endif
    @endif
@endforeach

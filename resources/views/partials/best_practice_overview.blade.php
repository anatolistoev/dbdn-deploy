<style type="text/css">
    div.overview div.page.idx1 {margin-right:20px;}
    div.overview .slider-item > div.page {min-height:630px;}

    div.blocks_wrap {width:1180px;margin:0px auto;}

    section.section_content {min-height:0;}
    section.section_content h2 {width:1108px;margin:0px auto 40px;font:normal 24px/30px "DaimlerCS-Demi", "Arial", "Helvetica Neue", "Helvetica", sans-serif;}


    section.section_content div.list-view div.page:hover div.title {width:69%;}

    section.most_recent {margin-top:28px;}
    div.heading-row h1 {margin-top:20px;}
    h1.single {margin-top:153px;}

    body.smart div.heading-row h1,
    body.smart div.page div.title {font-family: "FORsmartNextRegular", Arial, 'Roboto', "SF Compact Text",sans-serif;}
    body.smart section.section_content h2 {font-family: "FORsmartNextBold", Arial, 'Roboto', "SF Compact Text",sans-serif;}
    body.smart h1:before {position:relative;top:-4px;margin-right:1rem;content:"\2021"; font:normal 36px "DaimlerCS-Bold", "Arial", "Helvetica Neue", "Helvetica", sans-serif;}
    body.smart section.section_content h2:before {margin-right:.5rem;content:"\2021"; font:normal 20px "DaimlerCS-Bold", "Arial", "Helvetica Neue", "Helvetica", sans-serif;}
    body.smart h1:after {content:".";}
    h1.block_title {font-size: 24px; line-height: 30px; margin-bottom: 33px;display:inline-block;width:auto;margin-left:36px;margin-right:20px;font-family:DaimlerCS-Demi, Arial, "Helvetica Neue", Helvetica, sans-serif;}
    .best_practise_goto {display:inline-block;background:black url("/img/template/svg/story-layover.svg") 15px center no-repeat;color:white;text-transform: uppercase;line-height:40px;padding:0 15px 0 55px;}
    .best_practise_goto:hover {background-color:#00677f;}
</style>

@if( !MOBILE )
    <div style="height:93px;width:100%;float:left;clear:both;"></div>
@endif

@if( isset($MOSTRECENTBLOCKS) && count($MOSTRECENTBLOCKS) )
<section class="most_recent filter_hide">
    @include('partials.mostrecentblocks')
    <div style="clear:both;"></div>
</section>
@endif


@if( isset($MOSTSOMETHING) && count($MOSTSOMETHING) )
    @foreach($MOSTSOMETHING as $MOSTSOMETHINGTITLE => $MOSTSOMETHINGBLOCKS)
        @if( count($MOSTSOMETHINGBLOCKS) )
        <section style="margin-bottom:5px;" class="mostsomethingblocks filter_hide">
                @include('partials.mostSomethingBlocks')
                <div style="clear:both;"></div>
            </section>
        @endif
    @endforeach
@endif

<div style="float:left;clear:both;width:100%;"></div>

@include('partials.best_practice_blocks')

@if($PAGE->id == 971)
    @include('partials.guided_tour', array('tour_page'=>'practice', 'from' => 19, 'to' => 20))
@endif

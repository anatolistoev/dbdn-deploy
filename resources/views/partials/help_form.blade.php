<div class="user_profile_fields" id='help-formular'>
    <h1>@lang('template.help_form')</h1>
    <h1><small>@lang('template.help_form.subheading')</small></h1>
    {!! Form::open(array('url' => 'dialog/'.$PAGE->slug,  'method'=>'POST', 'id'=>'help_form')) !!}

            <span class="input input--yoshiko whole_line">
                 <div id="messageText_wrapper" class="{!!( $errors->has('dialog_error') && $errors->first('messageText')) ? 'error' : ''!!}">
                    {!! Form::textarea('messageText', Request::old('messageText'), array('class' => 'input__field input__field--yoshiko' , 'id' => 'messageText','maxlength'=> 1000)) !!}
                    <label class="input__label input__label--yoshiko" for="messageText">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('template.dialog.messageText')">@lang('template.dialog.messageText')</span>
                    </label>
                    <div id="counter_wrapper"><span id="messageText_counter">0</span><span>/1000</span></div>
                </div>
            </span>

        <span class="input input--yoshiko whole_line">
            {!! Form::text('name', (Auth::check() ? Auth::user()->first_name.' '.Auth::user()->last_name : null), array('class'=> 'input__field input__field--yoshiko '. (( $errors->has('dialog_error') && $errors->first('name')) ? 'error' : '' ))) !!}
            <label class="input__label input__label--yoshiko" for="name">
                <span class="input__label-content input__label-content--yoshiko" data-content="@lang('template.dialog.name')">@lang('template.dialog.name')</span>
            </label>
        </span>
        <span class="input input--yoshiko whole_line">
            {!! Form::text('email',(Auth::check() ? Auth::user()->email : null), array('class'=> 'input__field input__field--yoshiko '. (( $errors->has('dialog_error') && $errors->first('email')) ? 'error' : '' ))) !!}
            <label class="input__label input__label--yoshiko" for="email">
                <span class="input__label-content input__label-content--yoshiko" data-content="@lang('template.dialog.email')">@lang('template.dialog.email')</span>
            </label>
        </span>
        <div class="cf captcha_wrapper">
            <div class="label_left">
                <img class="captcha_img" src="{!! asset('captcha'). '?brand=' . ((isset($PAGE->brand) && $PAGE->brand == SMART) ?'smart':'daimler') !!}">
                <div class="reset_captcha"></div>
            </div>
            <div class="label_left">
                <span class="input input--yoshiko whole_line">
                    {!! Form::text('captcha', null, array('class' => 'input__field input__field--yoshiko '.(( $errors->has('dialog_error') && $errors->first('messageText')) ? 'error' : '' ),'autocomplete'=>'off')) !!}
                    <label class="input__label input__label--yoshiko" for="captcha">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.enter.code')">@lang('profile.enter.code')</span>
                    </label>
                </span>
            </div>
        </div>
        <div class="more_info">
            {!! Form::submit(trans('template.dialog.send'), array('class' => 'submit_button')) !!}
        </div>
    {!! Form::close()!!}
</div>
<script type="text/javascript" src="{!! mix('/js/classie.js') !!}"></script>
<style type="text/css">
    .btw_button.dropup {font-size:18px;width:320px;border:1px solid white;background:transparent;position:relative;opacity:1;box-sizing:border-box;text-transform:none;}
    .btw_button.dropup:not(.active):hover {border-color:#00677f;background:transparent;}
    .btw_button.dropup > div:first-child:after {content:" ";display:block;width:10px;height:100%;background:transparent url("img/template/svg/arrow_up_icon_wh.svg") right center no-repeat;position:absolute;top:0;right:20px; background-size: 10px;}
    .btw_button.dropup:not(.active):hover > div:first-child:after {background-image:url("img/template/svg/arrow_up_icon_d.svg");}
    .btw_button.dropup.active {background:white;color:#c8c8c8;}
    .btw_button.dropup.active > div:first-child:after {background-image:url("img/template/svg/arrow_down_icon.svg");}
    .btw_button.dropup.active .brands {display:block;}
    .btw_button.dropup .brands {display:none;width:320px;box-sizing:border-box;position:absolute;left:-1px;bottom:calc(100% - 20px);padding:20px;border:1px solid white;background:white;color:black;}
    .btw_button.dropup .brands > li {padding:0;}
    .btw_button.dropup .brands > li {cursor:pointer;display:block;color:black;text-transform:none;font-size:16px;line-height:24px;}
    .btw_button.dropup .brands > li:hover {color:#00677f;}
    .btw_button.dropup .brands > li:last-child {height:16px;border-bottom:1px solid #c8c8c8;}
/*	#toBeBlured{
		display:none;
		overflow:hidden;
		background-color:transparent;
	}
	#toBeBlured.blured {
		display:block;
	}
	#toBeBlured.blured image{
		transition: opacity 500ms ease;
	}*/
	.blured{
        opacity: 0.3 !important;
		-webkit-animation: fadeAut 0.5s ease-out;
		-moz-animation: fadeAut 0.5s ease-out;
		-o-animation: fadeAut 0.5s ease-out;
		animation: fadeAut 0.5s ease-out;
	}
	.not-blured{
		opacity: 1;
		-webkit-animation: fadeIn 0.5s ease-out;
		-moz-animation: fadeIn 0.5s ease-out;
		-o-animation: fadeIn 0.5s ease-out;
		animation: fadeIn 0.5s ease-out;
	}

@-webkit-keyframes fadeAut {
    0% {
        opacity: 1;
    }
    33% {
        opacity: 0.7;
    }
    66% {
        opacity: 0.5;
    }

    100% {
        opacity: 0.3;
    }
}

@-moz-keyframes fadeAut {
    0% {
        opacity: 1;
    }
    33% {
        opacity: 0.7;
    }
    66% {
        opacity: 0.5;
    }

    100% {
        opacity: 0.3;
    }
}

@-o-keyframes fadeAut {
    0% {
        opacity: 1;
    }
    33% {
        opacity: 0.7;
    }
    66% {
        opacity: 0.5;
    }

    100% {
        opacity: 0.3;
    }
}

@keyframes fadeAut {
    0% {
        opacity: 1;
    }
    33% {
        opacity: 0.7;
    }
    66% {
        opacity: 0.5;
    }

    100% {
        opacity: 0.3;
    }
}
@-webkit-keyframes fadeIn {
    0% {
        opacity: 0.3;
    }
    33% {
        opacity: 0.5;
    }
    66% {
        opacity: 0.7;
    }

    100% {
        opacity: 1;
    }
}

@-moz-keyframes fadeIn {
    0% {
        opacity: 0.3;
    }
    33% {
        opacity: 0.5;
    }
    66% {
        opacity: 0.7;
    }

    100% {
        opacity: 1;
    }
}

@-o-keyframes fadeIn {
    0% {
        opacity: 0.3;
    }
    33% {
        opacity: 0.5;
    }
    66% {
        opacity: 0.7;
    }

    100% {
        opacity: 1;
    }
}

@keyframes fadeIn {
    0% {
        opacity: 0.3;
    }
    33% {
        opacity: 0.5;
    }
    66% {
        opacity: 0.7;
    }

    100% {
        opacity: 1;
    }
}
  .remove_hover:hover{
		cursor:default;
	}
</style>

<script type="text/javascript">
    function addBlur(){
        $(".banner_image_wrapper" ).addClass("blured");
        $(".banner_image_wrapper").removeClass('not-blured');
    }
    function removeBlur(){
        $( ".banner_image_wrapper" ).removeClass('blured');
        var elFloat = parseFloat($(".banner_image_wrapper").css('opacity'));
        var opacity = isNaN(elFloat) ? 1 :  elFloat;
        if(opacity >= 1 || 0 == opacity)
            $(".banner_image_wrapper").addClass('not-blured');
    }
    $(document).on("click", function(e){
        $(".btw_button.dropup").removeClass('active');
        removeBlur();
    });
    $(document).on("click", ".btw_button.dropup", function(e){
        e.stopPropagation();
        e.preventDefault();
        $(this).toggleClass('active');
        if(!isDesktopViewport()){
            if($(this).hasClass('active')){
                addBlur();
            }else{
                removeBlur();
            }
        }
    });
	@if($isHome)
        if(isDesktopViewport()){
            $(document).on("mouseenter", ".btw_button", function(e){
                addBlur();
            }).on("mouseleave", ".btw_button", function(e){
                if(!$(".btw_button").hasClass("active")){
                    removeBlur();
                }
            });
        }
	@else
	$(document).on("mouseenter", ".for_blur", function(e){
        addBlur();
    }).on("mouseleave", ".for_blur", function(e){
        removeBlur();
    });
	@endif
</script>

<div class="homeblock" id="hbA">
    <div class="hBlockContainer" id="hbACont" style="left: 0px;">
        <div class="banner_load_wrapper @if($tablet_image) desktop_banner @endif">
        <a href="{!!$image['link'] ? $image['link'] : 'javascript:void(0)'!!}" title="{!!$image['title']!!}" class="@if(!$image['link']) remove_hover @endif">
           <div class="banner_text_wrapper">
               @if( !empty($image['description']) )
                   <div class='for_blur'>
                       {!!$image['description']!!}
                   </div>
               @endif

               @if($isHome)
                   {{-- @include('partials.baner_brand_selector') --}}
               @elseif ($image['btw_button'])
                   <div class="btw_button for_blur">{!!$image['btw_button']!!}</div>
               @endif
           </div>
           <div class="banner_image_wrapper">
           <img class="{!!$image['class']!!} banner lazyload-thumb" src="{!!\App\Helpers\UriHelper::getBlurredPreviewUrl($image['src'])!!}" data-original="{!!$image['src']!!}" alt="{!!$image['alt']!!}" data-gallery="{!!$image['data_gallery']!!}" >
           </div>
       </a>
    </div>
        @if($tablet_image)
        <div class="banner_load_wrapper tablet_banner">
        <a href="{!!$tablet_image['link'] ? $tablet_image['link'] : 'javascript:void(0)'!!}" title="{!!$tablet_image['title']!!}" class="@if(!$tablet_image['link']) remove_hover @endif">
           <div class="banner_text_wrapper">
               @if( !empty($tablet_image['description']) )
                   <div class='for_blur'>
                       {!!$tablet_image['description']!!}
                   </div>
               @endif

               @if($isHome)
                   {{-- @include('partials.baner_brand_selector') --}}
               @elseif ($tablet_image['btw_button'])
                   <div class="btw_button for_blur">{!!$tablet_image['btw_button']!!}</div>
               @endif
           </div>
           <div class="banner_image_wrapper">
           <img class="{!!$tablet_image['class']!!} banner lazyload-thumb" src="{!!\App\Helpers\UriHelper::getBlurredPreviewUrl($tablet_image['src'])!!}" data-original="{!!$tablet_image['src']!!}" alt="{!!$tablet_image['alt']!!}" data-gallery="{!!$tablet_image['data_gallery']!!}" >
           </div>
       </a>
        </div>
        @endif
        @if($mobile_image)
        <div class="banner_load_wrapper mobile_banner">
        <a href="{!!$mobile_image['link'] ? $mobile_image['link'] : 'javascript:void(0)'!!}" title="{!!$mobile_image['title']!!}" class="@if(!$mobile_image['link']) remove_hover @endif">
           <div class="banner_text_wrapper">
               @if( !empty($mobile_image['description']) )
                   <div class='for_blur'>
                       {!!$mobile_image['description']!!}
                   </div>
               @endif

               @if($isHome)
                   {{-- @include('partials.baner_brand_selector') --}}
               @elseif ($mobile_image['btw_button'])
                   <div class="btw_button for_blur">{!!$image['btw_button']!!}</div>
               @endif
           </div>
           <div class="banner_image_wrapper">
           <img class="{!!$mobile_image['class']!!} banner lazyload-thumb" src="{!!\App\Helpers\UriHelper::getBlurredPreviewUrl($mobile_image['src'])!!}" data-original="{!!$mobile_image['src']!!}" alt="{!!$mobile_image['alt']!!}" data-gallery="{!!$mobile_image['data_gallery']!!}" >
           </div>
       </a>
        </div>
        @endif
    </div>
    <div class="background"></div>
</div>

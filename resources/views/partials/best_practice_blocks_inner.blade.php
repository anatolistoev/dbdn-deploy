<?php $i = 0; ?>
@foreach($BLOCKS as $key=>$block_group)
	@foreach($block_group as $block)
	<div class="page" >
		<a href="{!!($block['slug'])!!}" class="page_link">
			<div class="image story-layover">
                {!!\App\Helpers\HtmlHelper::generateImg($block['img'], \App\Http\Controllers\FileController::MODE_IMAGES, 'overview', isset($block['imgext'])? $block['imgext'] : \App\Helpers\FileHelper::getFileExtension($block['img']))!!}
				@if(isset($block['protected']) && $block['protected'])
                    <div class="protected" title='@lang('template.protected')'></div>
                @endif
			</div>
			<div class="title"><span>{!!$block['title']!!}</span></div>
			<div class="list-hover cf">
				<div onclick='window.location.href="{!! url($block['slug']) !!}"' class="file_page">
					<div class="icon"></div>
					<span>@lang('system.download_overview.goTo')</span>
				</div>
			</div>
			<div class="date">@if(Session::get('lang') == LANG_EN){!!$block['date']->formatLocalized('%B %d, %Y')!!}@else{!!$block['date']->formatLocalized('%d. %B %Y')!!} @endif</div>
			<div class="stats">
				@if($block['ratings'] > 0)<span class="rate">{!!$block['ratings']!!}</span>@endif
				@if($block['comments'] > 0)<span class="comments">{!!$block['comments']!!}</span>@endif
				<span class="views">{!!$block['visits']!!}</span>
			</div>
			@if($block['tags'])
			<div class="tags">{!!$block['tags']!!}</div>
			@endif
		</a>
	</div>
	@if(($i + 1) % 3 == 0)
	<div class="cf"></div>
	@endif
	<?php $i++; ?>
	@endforeach
@endforeach
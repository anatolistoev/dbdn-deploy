<div class="heading-row" style="margin-top:48px;">
    <h1 class="hct">{!! \App\Helpers\HtmlHelper::formatTitle($MOSTSOMETHINGTITLE, $PAGE->brand) !!}</h1>
    <div style="clear:both;"></div>
</div>
<div class="grid-3cols overview">
    @foreach($MOSTSOMETHINGBLOCKS as $key => $page)
        <div class="page idx{!!$key!!}{!!($page->template == 'downloads')?' download':''!!}">

            <a class="page_link" href="{!!asset($page->slug)!!}">
                <div class="image story-layover">
                    @if($page->template == 'downloads')
                    <div class="download_hover">
                        <div onclick='window.location.href="{!! asset($page->slug) !!}"' class="info_wrapper view_wrapper">
                            <div class="icon"></div>
                            <span>@lang('system.download_overview.goTo')</span>
                        </div>
                        @if(Auth::check() && isset($page->file_id))
                            @if($page->is_file_added_cart || Auth::user()->isPageAuth($page->id))
                                <div onclick="return false;" class="info_wrapper cart_wrapper">
                                    <div class="icon"></div>
                                    <span>@lang('system.downloads_list.added')</span>
                                </div>
                            @else
                                <div @if($page->has_access) onclick="addToCart({!!$page->file_id!!}, event)" @endif class="info_wrapper cart_wrapper">
                                    <div class="icon"></div>
                                    <span>@lang('system.download_overview.addToCart')</span>
                                </div>
                            @endif
                            <div @if($page->has_access) onclick="return startDownload({!!$page->file_id!!}, event);"@endif class="info_wrapper download_wrapper">
                                @if($page->has_access)
                                    <input type="checkbox" id="file_{!! $page->file_id !!}" class="chb checkbox1 " value="{!!$page->file_id!!}" name="fid[]">
                                @endif
                                <div class="icon"></div><span>@lang('system.downloads_list.downloadNow')</span>
                            </div>
                        @endif

                        @if(isset($page->extension) && $page->size)
                        <div class="info_wrapper stats_wrapper">
                            <span>{!!$page->extension . ','!!}</span>
                            <span>{!!\App\Helpers\FileHelper::formatSize($page->size)!!}</span>
                        </div>
                        @endif
                    </div>
                    @endif
                    {!!$page->img!!}

                    @if(isset($page->protected) && $page->protected)
                        <div class="protected" title='@lang('template.protected')'></div>
                    @endif
                </div>
                <div class="title"><span>{!!$page->title!!}</span></div>
                <div class="date">@if(Session::get('lang') == LANG_EN){!!$page->updated_at->formatLocalized('%B %d, %Y')!!}@else{!!$page->updated_at->formatLocalized('%d. %B %Y')!!}@endif</div>
                <div class="stats" style="width:auto;">
                    @if(1 || $page->ratings()->count() > 0)<span class="rate">{!!$page->ratings()->count()!!}</span>@endif
                    @if($page->template=='best_practice' && $page->activeComments()->count() > 0)<span class="comments">{!!$page->activeComments()->count()!!}</span>@endif
                    <span class="views">{!!$page->visits!!}</span>
                </div>
                @if($page->tags)
                    <div class="tags">{!!$page->tags!!}</div>
                @endif
            </a>
        </div>
    @endforeach
</div>


<div class="blockTitle">@lang('template.personal services')</div>
<div id="personal">
	@if(Auth::check())
		<div class="blockText">
			@lang('template.welcome'),<br>
			<b>{!! Auth::user()->first_name .' '. Auth::user()->last_name !!}!</b>
			<br>
			<br>
		</div>
		<div class="personalHL"></div>
		<div class="linkNum">
			<a href="{!!asset('profile')!!}" class="personalLink">@lang('template.user profile')</a>
		</div>
		
		<div class="linkNum">
			<a href="{{asset('Download_Cart')}}" class="personalLink">@lang('template.downloads')
				<div class="num">{{ Session::get('myDownloadsCount') }}</div>
			</a>
		</div>
		<div class="linkNum">
			<a href="{{asset('Watchlist')}}" class="personalLink">@lang('template.watchlist')
				<div class="num">{{ Session::get('watchlistCount') }}</div>
			</a>
		</div>
		
		<a href="{{asset('logout')}}" class="btn">@lang('template.logout')</a>
		
	@else
	<div class="blockText">
		<form action="{{asset('login/'.$PAGE->id)}}" method="post" id="loginForm" autocomplete="off">
		{{-- $errors->first('username') --}}
		{{-- $errors->first('password') --}}
			@lang('template.username'):
			<input type="Text" name="username" value="{{ Request::has("username") ? Request::get("email") : '' }}" class="loginBox"><br><br>
			@lang('template.password'):<br>
			<input type="Password" name="password" value="" class="loginBox"><br>
			<button class="btn">@lang('template.login')</button>
			<a href="{{ asset('registration') }}" class="loginLink">@lang('template.registration')</a>
			<a href="{{ asset('forgotten') }}" class="loginLink">@lang('template.forgot_link')</a>
		</form>
	</div>
	@endif
</div>
<div class="user_profile_fields">
    <h1>@lang('template.login')</h1>
    <h1><small>@lang('template.login.subheading')</small></h1>
    <form action="{!!asset('doLogin/'.$PAGE->id)!!}" method="post" id="loginForm" autocomplete="off">
        {!! csrf_field() !!}
        {{-- $errors->first('username') --}}
        {{-- $errors->first('password') --}}
        <span class="input input--yoshiko whole_line">
            <input class="input__field input__field--yoshiko @if($errors->first('username'))error @endif" type="text" name="username" value="{!! Request::has("username") ? Request::get("email") : '' !!}" />
            <label class="input__label input__label--yoshiko" for="username">
                <span class="input__label-content input__label-content--yoshiko" data-content="@lang('template.username')">@lang('template.username')</span>
            </label>
        </span>
        <span class="input input--yoshiko whole_line">
            <input class="input__field input__field--yoshiko @if($errors->first('username'))error @endif" type="password" name="password" value="" />
            <label class="input__label input__label--yoshiko" for="password">
                <span class="input__label-content input__label-content--yoshiko" data-content="@lang('template.password')">@lang('template.password')</span>
            </label>
        </span>
        <div class="cf captcha_wrapper">
            <div class="label_left">
                <img class="captcha_img" src="{!! asset('captcha'). '?brand=' . ((isset($PAGE->brand) && $PAGE->brand == SMART) ?'smart':'daimler') !!}">
                <div class="reset_captcha"></div>
            </div>
            <div class="label_left">
                <span class="input input--yoshiko whole_line">
                    <input class="input__field input__field--yoshiko @if($errors->first('captcha'))error @endif" type="text" name="captcha" autocomplete="off"/>
                           <label class="input__label input__label--yoshiko" for="captcha">
                        <span class="input__label-content input__label-content--yoshiko" data-content="@lang('profile.enter.code')">@lang('profile.enter.code')</span>
                    </label>
                </span>
            </div>
        </div>
        <div class="more_info">
            <input class="submit_button" type="submit" value="@lang('profile.send')">
            <a href="{!! asset('forgotten') !!}" class="loginLink">@lang('template.forgot_link')</a>
        </div>
        <div class="registerLink">
            <span>@lang('template.login.signup')</span>
        </div>
    </form>
</div>
<script type="text/javascript" src="{!! mix('/js/classie.js') !!}"></script>

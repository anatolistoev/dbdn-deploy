<?php $simple = (1 == count($BLOCKS) && $PAGE->slug!='bestpractice' && ($tmp = array_keys($BLOCKS)));?>
<section class="section_content best-practice @if($simple) simple @endif">
@if(isset($BLOCKS) && sizeof($BLOCKS) > 0)
    <div class="blocks_wrap @if(MOBILE) bwlist @else bwgrid @endif">

        @include('partials.view_controls')

        @if(!empty($ALLBLOCKSTITLE))
            <h1>{!!$ALLBLOCKSTITLE!!}</h1>
        @elseif($simple)
            <h1 class="single">{!! trim($tmp[0], '‡ .') !!}</h1>
        @endif
        
        @if($FILTERED_BLOCKS)
            <div class="view_controls_holder"></div>
        @endif

    @foreach($BLOCKS as $key => $block_group)

        @if(!$FILTERED_BLOCKS)
            @if( !$simple )
               <h1 class="block_title" id="{!!\App\Helpers\UriHelper::converturi(trim($key, '‡ .'))!!}">
                   {!! trim($key, '‡ .') !!}
               </h1>
           @endif

            <?php $first_value = reset($block_group);?>
            @if($first_value && isset($first_value['parent_slug']))
                <a href="{!!url($first_value['parent_slug'])!!}" class="best_practise_goto">
                    @lang('system.download_overview.goTo')
                </a>
            @endif
        @endif

        <div class="grid-page grid-4cols cf @if(MOBILE) list-view @endif">
            @foreach($block_group as $block_group_key => $block)
                <div class="page">
                    <a class="page_link" href="{!! url($block['slug']) !!}">
                        <div class="image story-layover">
                            {!!\App\Helpers\HtmlHelper::generateImgFromBlock($block, 'overview', array("border"=> "0"))!!}
                            @if(isset($block['protected']) && $block['protected'])
                                <div class="protected" title='@lang('template.protected')'></div>
                            @endif
                        </div>
                        <div class="title"><span>{!! $block['title'] !!}</span></div>
                        <div class="list-hover cf">
                            <div onclick='window.location.href="{!! url($block['slug']) !!}"' class="file_page">
                                <div class="icon"></div>
                                <span>@lang('system.download_overview.goTo')</span>
                            </div>
                        </div>
                        <div class="date">@if(Session::get('lang') == LANG_EN){!!$block['date']->formatLocalized('%B %d, %Y')!!}@else{!!$block['date']->formatLocalized('%d. %B %Y')!!}@endif</div>
                        {{--
                        <!-- Dusty: following is not required, but good to have here ;) -->
                        <div class="stats">
                            <span class="rate">{!! $block['ratings'] !!}</span>
                            <span class="comments">{!! $block['comments'] !!}</span>
                            <span class="views">{!! $block['visits'] !!}</span>
                        </div>
                        --}}
                        @if(!empty($block['tags']))
                            <div class="tags">{!! $block['tags'] !!}</div>
                        @endif
                    </a>
                </div>
                @if(0 === ++$block_group_key % 4)
                    <div class="cf"></div>
                @endif
            @endforeach
        </div>
        <div style="clear: both;"></div>
    @endforeach

    </div>
@else
        <div class="grid-page grid-4cols">
            <div class="no_results">@lang('template.no_results')</div>
        </div>
@endif
</section>

<style type="text/css">
    div.overview div.page.idx1 {margin-right:20px;}
    div.overview .slider-item > div.page {min-height:630px;}

    div.blocks_wrap {width:1180px;margin:0px auto;}

    section.section_content {min-height:0;}
    section.section_content h2 {width:1108px;margin:0px auto 40px;font:normal 24px/30px "DaimlerCS-Demi", "Arial", "Helvetica Neue", "Helvetica", sans-serif;}

    section.section_content div.list-view {margin-bottom:70px;}
    /*section.section_content div.list-view div.page:hover div.title {width:69%;}*/

    section.most_recent {margin-top:28px;}
    div.heading-row h1 {margin-top:20px;}
    h1.single {
        margin-top: -12px;
        float: left;
        margin-left: 36px;
        width: auto;
    }

    body.smart h1.single {
        margin-top: -4px;
    }
    .downloads_overview .single{
        margin-top: 0;
        margin-bottom: 40px;
    }
    body.smart section.downloads_overview h1.block_title {font-family:"FORsmartNextBold", Arial, 'Roboto', "SF Compact Text",sans-serif;}
    body.smart section.downloads_overview h1.block_title + a,
    body.smart section.downloads_overview h1.block_title + a:hover {background-color:#f6ba35;}

    section.downloads_overview { margin-top:142px; }
    section.downloads_overview h1.block_title {display:inline-block;width:auto;margin-left:36px;margin-right:20px;font:normal 24px/30px "DaimlerCS-Demi", "Arial", "Helvetica Neue", "Helvetica", sans-serif;}
    section.downloads_overview h1.block_title + a {box-sizing:border-box;display:inline-block;position:relative;top:-1px;background:black url("/img/template/svg/story-layover.svg") 15px center no-repeat;color:white;height:40px;line-height:40px;padding:0 15px 0 55px;}
    section.downloads_overview h1.block_title + a:hover {background-color:#00677f;}
    section.single_download{
        margin-top: 195px;
    }
</style>

@if ($PAGE->in_navigation == 1)

    @if( isset($MOSTRECENTBLOCKS) || isset($MOSTSOMETHING) )

    @if( !MOBILE )
    <div style="height:93px;width:100%;float:left;clear:both;"></div>
    @endif

        @if( isset($MOSTRECENTBLOCKS) && count($MOSTRECENTBLOCKS) )
        <section class="most_recent filter_hide" style="{!! !empty($DW_FILTERS)?'display:none;':''!!}">
            @include('partials.mostrecentblocks')
            <div style="clear:both;"></div>
        </section>
        @endif


        @if( isset($MOSTSOMETHING) && count($MOSTSOMETHING) )
            @foreach($MOSTSOMETHING as $MOSTSOMETHINGTITLE => $MOSTSOMETHINGBLOCKS)
                @if( count($MOSTSOMETHINGBLOCKS) )
                <section style="margin-bottom:5px;{!! !empty($DW_FILTERS)?'display:none':''!!}" class="mostsomethingblocks filter_hide">
                        @include('partials.mostSomethingBlocks')
                        <div style="clear:both;"></div>
                </section>
                @endif
            @endforeach
        @endif

    @endif
    
@endif

@include('partials.download_blocks')

@if(isset($PAGE) && $PAGE->brand == DAIMLER)
    @include('partials.guided_tour', array('tour_page'=>'download', 'from' => 22, 'to' => 23))
@endif

<script>
    $(document).ready(function(){
        var data = sessionStorage.getItem('list-view');
        if(data){
            $('.view-list').trigger( "click" );
        }
    });
    jQuery(document).ready(function($){
        $(document).on('mouseover', 'div.downloads div.page div.cart_wrapper, div.downloads div.page div.download_wrapper', function(e) {
           $(e.target).closest('.download_hover').find('.view_wrapper span').css('display', 'none');
        });
        $(document).on('mouseleave', 'div.downloads div.page div.cart_wrapper, div.downloads div.page div.download_wrapper', function(e) {
           $(e.target).closest('.download_hover').find('.view_wrapper span').css('display', '');
        });
    });

    // Prevents browser back cache
    $(window).on('unload', function(){
    });

    $(window).on('pageshow', function(event){
        if(event.persisted){
            window.location.reload();
        }
    });
</script>

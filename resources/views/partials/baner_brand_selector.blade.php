<div class="btw_button dropup">
    <div>
        @lang('template.choose_brand_company')
    </div>
    <ul class="brands">
        <li onclick="window.location='{!! asset(UserAccessHelper::getSpecialPage(DAIMLER)->slug) !!}'">Daimler</li>
        <li onclick="window.location='{!! asset(UserAccessHelper::getSpecialPage(MERCEDES_BENZ)->slug) !!}'">Mercedes-Benz</li>
        <li onclick="window.location='{!! asset(UserAccessHelper::getSpecialPage(SMART)->slug) !!}'">smart</li>
        <li onclick="window.location='{!! asset(UserAccessHelper::getSpecialPage(FINANCIAL_SERVICES)->slug) !!}'">Daimler Financial Services</li>
        <li onclick="window.location='{!! asset(UserAccessHelper::getSpecialPage(DAIMLER_MOBILITY)->slug) !!}'">Daimler Mobility</li>
        <li onclick="window.location='{!! asset(UserAccessHelper::getSpecialPage(TRUCK_FINANCIAL)->slug) !!}'">Daimler Truck Financial</li>
        <li onclick="window.location='{!! asset(UserAccessHelper::getSpecialPage(FLEET_MANAGEMENT)->slug) !!}'">Daimler Fleet Management</li>
        <li onclick="window.location='{!! asset(UserAccessHelper::getSpecialPage(FUSO_FINANCIAL)->slug) !!}'">FUSO Financial</li>
        <li onclick="window.location='{!! asset(UserAccessHelper::getSpecialPage(DAIMLER_BKK)->slug) !!}'">Daimler BKK</li>
        <li>&nbsp;</li>
    </ul>
</div>

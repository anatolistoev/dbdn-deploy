{{--
Variables from Dispatcher: PAGE, L2ID, PATH_IDS, CONTENTS, $ALT_LANG, $Q, $BRAND_NAV
--}}
<script type="text/javascript">
    var brandLabel = $("#brandLabel").text().trim();
    if (brandLabel) {
        $("#brandSelect").text(brandLabel);
    }
</script>

<div id="hamburger">
    <div class="switch">
        <a class="close_button"></a>
        <span></span>
        <span></span>
        <span></span>
    </div>

    <div id="rightMenu">


        <div id="topLinks">
            @if(isset($ALT_LANG) && ($PAGE->langs >> ($ALT_LANG-1) & 1) == 1)
                <div class="topItem" id="language">
                    <a href="{!! asset($PAGE->slug != '' ? $PAGE->slug : $PAGE->id) !!}?lang=@lang('template.altLocale')">@lang('template.altLang')</a>
                </div>
            @endif
            @if(Auth::check())
                <div class="topItem" id="watchlist"><a href="{!!url('/Watchlist')!!}">&nbsp;</a><span class="counter">{!! Session::get('watchlistCount') !!}</span></div>
                <div class="topItem" id="downloads"><a href="{!!url('/Download_Cart')!!}">&nbsp;</a><span class="counter">{!! Session::get('myDownloadsCount') !!}</span></div>
                <div class="topItem" id="profile"></div>
                <div class="popup profile-formular">
                    <a class="popup-close" style="display: none;"></a>
                    <a href="{!!url('/profile')!!}">@lang('template.profile')</a>
                     <a href="{!!url('logout')!!}">@lang('template.logout')</a>
                </div>
            @else
                <div class="topItem profile_{!!trans('template.currLocale')!!}" id="profile">
                    <a href="{!!url('login/'.$PAGE->slug)!!}" >{!!(isset($PAGE->brand)&& $PAGE->brand == SMART)? trans('smart.login') :trans('template.login')!!}</a>
                    <span>|</span>
                    <a href="{!!url('/registration')!!}" id="signup">{!!(isset($PAGE->brand)&& $PAGE->brand == SMART)? trans('smart.registration') :trans('template.registration')!!}</a>
                </div>
            @endif
        </div>

        {!! Form::open(array('id'=>"searchForm2", 'url'=>asset('search'))) !!}
            <input name="search" id="search2" value="@if(isset($Q)){!! $Q !!}@endif" type="text" placeholder="@lang('template.search_box.top.placeholder')" autocomplete="off" />
            <button type="submit" id="doFlyoutSearch" class="newSearchBtn"></button>
        {!! Form::close() !!}

        @if(isset($BRAND_NAV))
            @include('partials.brand_nav_mobile')
        @endif

    </div>
</div>
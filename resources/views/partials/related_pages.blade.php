@if(count($RELATED_PAGES)>0 || count($RELATED_DOWNLOADS)>0)
<?php $COUNT = 6 ?>
<div id='related_wrapper' class='cf'>
    @if(count($RELATED_DOWNLOADS)>0)
    <div id='related_downlowds' class="related_links">
        <h2>@lang('template.related.downloads')</h2>
        <ul>
             <?php $index = 1 ;?>
            @foreach($RELATED_DOWNLOADS as $rd)
            @if(!$rd->is_hidden)
                <li @if($index > $COUNT) class="hided" @endif @if($index == $COUNT)style='border-bottom-width: 0;'@endif>
                    <a href="{!!($rd->slug != '' ? url($rd->slug) : url($rd->u_id))!!}">
                        @if($rd->authorization>0)<span class="related-protected"></span>@endif 
                        <span class="link-title">@if($rd->brand_id != SMART) [{!!$rd->brand_title!!}] @endif {!!$rd->body!!}</span>
                    </a>
                </li>
                <?php $index++; ?>
             @endif
            @endforeach
        </ul>
    </div>
    @endif
    @if(count($RELATED_PAGES)>0)
    <div id='related_pages' class="related_links">
        <h2>@lang('template.related.pages')</h2>
        <ul>
            <?php $index = 1 ;?>
            @foreach($RELATED_PAGES as $rd)
                @if(!$rd->is_hidden)
                <li @if($index > $COUNT) class="hided" @endif @if($index == $COUNT)style='border-bottom-width: 0;'@endif>
                    <a href="{!!($rd->slug != '' ? url($rd->slug) : url($rd->u_id))!!}">
                        @if($rd->authorization>0)<span class="related-protected"></span>@endif 
                        <span class="link-title">@if($rd->brand_id != SMART) [{!!$rd->brand_title!!}] @endif {!!$rd->body!!}</span>
                    </a>
                </li>
                <?php $index++; ?>
                @endif
            @endforeach
        </ul>
    </div>
    @endif
    @if(count($RELATED_DOWNLOADS) > $COUNT || count($RELATED_PAGES) > $COUNT)
    <div class="cf"></div>
    <div id='load-more_wrapper'>
        <a class="black-button">@lang('js_localize_fe.bp.load_more')</a>
    </div>
    @endif
</div>
@endif


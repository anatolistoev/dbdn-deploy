<div class="heading-row">
    <h1 class="hct">{!! \App\Helpers\HtmlHelper::formatTitle(trans('template.interest.most_recent'), $PAGE->brand) !!}</h1>
    <div style="clear:both;"></div>
</div>
@if(isset($MOSTRECENTBLOCKS) && sizeof($MOSTRECENTBLOCKS))
    <div class="overview slider">
        <div class="wrap" style="margin-bottom:0;">
            @if(isset($MOSTRECENTBLOCKS) && sizeof($MOSTRECENTBLOCKS)>2 && ($ADDCLASS = 'frame') )
                <a class="prev btn_navigation"></a>
                <a class="next btn_navigation"></a>
            @elseif( ($ADDCLASS = 'overview') )
            @endif
            <div class="{!!$ADDCLASS!!} mslide" id="oneperframe1">
                <ul class="cf">
                    <li index="1">
                        <div class="slider-item">
                @foreach($MOSTRECENTBLOCKS as $key => $page)
                    @if(($key +1) % 2 == 0)
                        <div class="col2 page">
                    @else
                        <div class="col1 page">
                    @endif
                            <a href="{!!asset($page->slug)!!}" class="page_link">
                                <div class="image story-layover">
                                    {!!$page->img!!}

                                    @if(isset($page->protected) && $page->protected)
                                        <div class="protected" title='@lang('template.protected')'></div>
                                    @endif
                                </div>
                                <div class="title"><span>{!!$page->title!!}</span></div>
                                <div class="date">@if(Session::get('lang') == LANG_EN){!!$page->updated_at->formatLocalized('%B %d, %Y')!!}@else{!!$page->updated_at->formatLocalized('%d. %B %Y')!!}@endif</div>
                                <div class="stats">
                                    @if(1 || $page->ratings()->count() > 0)<span class="rate">{!!$page->ratings()->count()!!}</span>@endif
                                    @if($page->template=='best_practice' && $page->activeComments()->count() > 0)<span class="comments">{!!$page->activeComments()->count()!!}</span>@endif
                                    <span class="views">{!!$page->visits!!}</span>
                                </div>
                                @if($page->tags)
                                    <div class="tags">{!!$page->tags!!}</div>
                                @endif
                            </a>
                        </div>
                        @if(($key +1) % 2 == 0 && $key < count($MOSTRECENTBLOCKS)-1)
                                    </div>
                                </li>
                                <li index="{!!($key +1)/2 +1!!}">
                                    <div class="slider-item">
                       @endif
                @endforeach
                       </div>
                    </li>
                </ul>
            </div>
            @if(isset($MOSTRECENTBLOCKS) && sizeof($MOSTRECENTBLOCKS)>2)
                <ul class="pages">
                    @for($p = 0; $p < ceil(count($MOSTRECENTBLOCKS) / 2); $p++)
                        <li>{!!$p+1!!}</li>
                    @endfor
                </ul>
            @endif
        </div>
    </div>
@endif

<script>

	function checkToggle() {
		var cboxes = $('.downloads input[type=checkbox]');
		cboxes.prop('checked', !cboxes.prop('checked'));
	}
	function removeSelected() {
		var $form = $('#myDownloadsForm');
		if($form.find('input[type=checkbox]:checked').length > 0){
			$form.find('input[name="_method"]').val('delete');
			$form.attr('target', '_self');
			$form.submit();
		}else{
			msg('@lang('system.downloads_list.alert')')
		}
	}
	function removeDownload(id) {
		var $form = $('#myDownloadsForm');
		$form.find('input[type=checkbox]').prop('checked', false);

		$form.find('#file_'+id).prop('checked', true);
		$form.find('input[name="_method"]').val('delete');
		$form.attr('target', '_self');
		$form.submit();
	}
	function startDownload() {
		var hiddenIFrameID = 'downloadIFrame',
			$form = $('#myDownloadsForm');
		if($form.find('input[type=checkbox]:checked').length > 0){
			$form.find('input[name="_method"]').val('post');
			$form.attr('target', 'downloadIFrame');

            if (isDesktopViewport()) {
                $form.submit();
            } else {
                DBDN.utils.downloadFileAndDisplayNotificationWhileBeingDownloaded($form, js_localize['file.download'], true);
            }
		}else{
			msg('@lang('system.downloads_list.alert')')
		}
		return false;
	}
</script>
<section>
	<div class="select_all"  style="margin-top: 37px;">
		<input id="selecctall" class="chb" type="checkbox" />
		<label for="selecctall" onclick="checkToggle();">@lang('system.downloads_list.selectall')</label>
	</div>
	<div class="download_selected_2" style="margin-top: 40px;" >
        <span class="download_selected" onclick="return startDownload();">@lang('system.downloads_list.downloadLink')</span>
        <span class="remove_selected" onclick="removeSelected();">@lang('system.downloads_list.removeSelected')</span>
	</div>
</section>

<section>
	<div class="row">
		<div style="clear:both;"></div>
	</div>
	{!! Form::open(array('url' => 'Download_Cart', 'id' => 'myDownloadsForm', 'method'=>'post')) !!}
		{!! Form::hidden('_method', 'post') !!}
        @if(isset($AUTH_FILES) && count($AUTH_FILES) > 0)
            <h1 id="auth_files_list" class="block_title">@lang('system.downloads_list.auth_pages')</h1>
            <div class="grid-page grid-4cols downloads auth_pages">
                <?php $i = 0?>
                @foreach($AUTH_FILES as $key => $block)
                    <div class="page {!!$key!!}">
                        <a class="page_link" href="{!!$block['slug']!!}">
                            <div class="image story-layover">
                                @if(strtolower(pathinfo($block['img'], PATHINFO_EXTENSION)) == 'svg')
                                    {!!\App\Helpers\FileHelper::getSVGContent(public_path().$block['img']);!!}
                                @else
                                    {!!\App\Helpers\HtmlHelper::generateImg($block['img'], $block['type'], 'overview', isset($block['imgext'])? $block['imgext'] : \App\Helpers\FileHelper::getFileExtension($block['img']), isset($block['thumb_visible']), array("max-height"=> "186px", "max-width" => "280px"))!!}
                                @endif
                                @if($block['protected'])
                                    <div class="protected" title='@lang('template.protected')'></div>
                                @endif
                            </div>
                            <div class="title-chb">
                                @if(isset($block['has_access']) && $block['has_access'])
                                <input type="checkbox" id="file_{!! $block['download_id'] !!}" class="chb checkbox1 " value="{!! $block['download_id'] !!}" name="auid[]">
                                @endif
                                <label for="file_{!! $block['download_id'] !!}">
                                    @if($block['protected'])
                                        <div class="protected" title='@lang('template.protected')'></div>
                                    @endif
                                    <span @if($block['protected']) class="has_protected" @endif>{!!\App\Helpers\HtmlHelper::fixDoubleDaggerForDaimler($block['title']) !!}</span>
                                </label>
                            </div>
                            <div class="list-hover cf">
                                <div onclick='window.location.href="{!! url($block['slug']) !!}"' class="file_page">
                                    <div class="icon"></div>
                                    <span>@lang('system.download_overview.goTo')</span>
                                </div>
                                <div class="file_exsize">
                                    {!!\App\Helpers\FileHelper::formatSize($block['size'])!!}
                                </div>
                                <div class="file_extension">{!! $block['extension']!!}</div>
                            </div>
                            <div class="date">@if(Session::get('lang') == LANG_EN){!!$block['date']->formatLocalized('%B %d, %Y')!!}@else{!!$block['date']->formatLocalized('%d. %B %Y')!!} @endif</div>
                            @if($block['tags'])
                            <div class="tags">{!!$block['tags']!!}</div>
                            @endif
                            <div style="clear: both;"></div>
                        </a>
                    </div>
                <?php $i++?>
                @if($i % 4 == 0 && $block != end($AUTH_FILES))
                    <div class="cf"></div>
                @endif
                @endforeach
            </div>
            <div class="cf"></div>
            <span class="notice">@lang('system.downloads_list.notice')</span>
        @endif
        @if(isset($DOWNLOAD_FILES) && count($DOWNLOAD_FILES) > 0)
            <h1 id="download_files_list" class="block_title">@lang('system.downloads_list.added_pages')</h1>
            <div class="grid-page grid-4cols downloads">
            <?php $i = 0?>
            @foreach($DOWNLOAD_FILES as $key => $block)
                <div class="page {!!$key!!}">
                    <a class="page_link" href="{!!$block['slug']!!}">
                        <div class="image story-layover">
                            @if(strtolower(pathinfo($block['img'], PATHINFO_EXTENSION)) == 'svg')
                                {!!\App\Helpers\FileHelper::getSVGContent(public_path().$block['img']);!!}
                            @else
                                {!!\App\Helpers\HtmlHelper::generateImg($block['img'], $block['type'], 'overview', isset($block['imgext'])? $block['imgext'] : \App\Helpers\FileHelper::getFileExtension($block['img']), isset($block['thumb_visible']), array("max-height"=> "186px", "max-width" => "280px"))!!}
                            @endif
                            @if($block['protected'])
                                <div class="protected" title='@lang('template.protected')'></div>
                            @endif
                        </div>
                        <div class="title-chb">
                            @if(isset($block['has_access']) && $block['has_access'])
                            <input type="checkbox" id="file_{!! $block['download_id'] !!}" class="chb checkbox1 " value="{!! $block['download_id'] !!}" name="fid[]" autocomplete="off">
                            @endif
                            <label for="file_{!! $block['download_id'] !!}">
                                @if($block['protected'])
                                    <div class="protected" title='@lang('template.protected')'></div>
                                @endif
                                <span @if($block['protected']) class="has_protected" @endif>{!!\App\Helpers\HtmlHelper::fixDoubleDaggerForDaimler($block['title']) !!}</span>
                            </label>
                        </div>
                        <div class="list-hover cf">
                            <div onclick='window.location.href="{!! url($block['slug']) !!}"' class="file_page">
                                <div class="icon"></div>
                                <span>@lang('system.download_overview.goTo')</span>
                            </div>
                            <div class="file_exsize">
                                {!!\App\Helpers\FileHelper::formatSize($block['size'])!!}
                            </div>
                            <div class="file_extension">{!! $block['extension']!!}</div>
                        </div>
                        <div class="date">@if(Session::get('lang') == LANG_EN){!!$block['date']->formatLocalized('%B %d, %Y')!!}@else{!!$block['date']->formatLocalized('%d. %B %Y')!!} @endif</div>
                        @if($block['tags'])
                        <div class="tags">{!!$block['tags']!!}</div>
                        @endif
                        <div style="clear: both;"></div>
                    </a>
                </div>
            <?php $i++?>
            @if($i % 4 == 0 && $block != end($DOWNLOAD_FILES))
                <div class="cf"></div>
            @endif
            @endforeach
            </div>
            <div class="cf"></div>
        @endif
	{!! Form::close() !!}
<iframe id="downloadIFrame" name="downloadIFrame" style="display:none;"></iframe>
</section>
<section>
	<div class="select_all">
		<input id="selecctall" class="chb" type="checkbox" />
		<label for="selecctall" onclick="checkToggle();">@lang('system.downloads_list.selectall')</label>
	</div>
    <div class="download_selected_2" style="margin-bottom:60px;">
		<span class="download_selected" onclick="return startDownload();">@lang('system.downloads_list.downloadLink')</span>
		<span class="remove_selected" onclick="removeSelected();">@lang('system.downloads_list.removeSelected')</span>
	</div>
</section>

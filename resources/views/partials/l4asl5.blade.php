{{-- @if(isset($ASCENDANT_ID_INDEX[$PAGE->parent_id], $SECTION_PID_INDEX) && sizeof($SECTION_PID_INDEX) && $dad = $ASCENDANT_ID_INDEX[$PAGE->parent_id]) --}}
@if( isset($ASCENDANT_ID_INDEX[$PAGE->parent_id], $SECTION_PID_INDEX) && sizeof($SECTION_PID_INDEX) && ($dad = $ASCENDANT_ID_INDEX[$PAGE->parent_id]) &&
		(
			$LEVEL == 3 && isset($SECTION_PID_INDEX[$PAGE->id]) ||
			$LEVEL == 4 && isset($SECTION_PID_INDEX[$PAGE->parent_id])
		)
	)
	<div id="L5">
	@if($LEVEL == 3 && isset($SECTION_PID_INDEX[$PAGE->id]))
		<div class="RSTitle">{!! $CONTENTS['title'] !!}</div>
			{{-- L5FromL3 --}}
		@foreach($SECTION_PID_INDEX[$PAGE->id] as $child)
			<a href="{!! asset($child->slug!='' ? $child->slug : $child->id) !!}" {!! ($child->id == $PAGE->id ? 'class="L5Active"' : '' ) !!}>{!! $child->title !!}</a>
		@endforeach
	@elseif( $LEVEL == 4 && isset($SECTION_PID_INDEX[$PAGE->parent_id]))
		<div class="RSTitle">{!! $dad->title !!}</div>
		{{-- L5FromL5 --}}
		@foreach($SECTION_PID_INDEX[$PAGE->parent_id] as $child)
			<a href="{!! asset($child->slug!='' ? $child->slug : $child->id) !!}" {!! ($child->id == $PAGE->id ? 'class="L5Active"' : '' ) !!}>{!! $child->title !!}</a>
		@endforeach
	@endif
	</div><!--L5-->
@endif
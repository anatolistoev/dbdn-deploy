@if(isset($RELATED_DOWNLOADS) && sizeof($RELATED_DOWNLOADS))
	<div class="genericRS"><div class="RSTitle">@lang('template.downloads')</div>
	@foreach($RELATED_DOWNLOADS as $relID => $relTitle)
		<a href="{!! $relID !!}">{!! $relTitle !!}</a>
	@endforeach
	</div>
@endif
@if(isset($RELATED_PAGES) && sizeof($RELATED_PAGES))
	<div class="genericRS"><div class="RSTitle">@lang('template.related')</div>
	@foreach($RELATED_PAGES as $relID => $relTitle)
		<a href="{!! $relID !!}">{!! $relTitle !!}</a>
	@endforeach
	</div>
@endif
@if(isset($TERMS) && sizeof($TERMS))
	<div class="genericRS"><div class="RSTitle">@lang('template.glossary')</div>
	@foreach($TERMS as $term)
		<a href="{!! asset('glossary') !!}/{!! $term->id !!}">{!! $term->name !!}</a>
	@endforeach
		<a href="{!! asset('glossary') !!}">@lang('template.viewallterms')</a>
	</div>
@endif
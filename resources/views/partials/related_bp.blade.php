@if(isset($RELATED_BP) && count($RELATED_BP)> 0)
<div class="slider_wrapper" style="margin-top:160px;">
    <div class="slider_title">{!!$TITLE!!}</div>
    <div class="slider_container">
        <a class="prev slider_hover" @if(!isset($RELATED_BP) || count($RELATED_BP) < 5) style="display:none"@endif></a>
        <a class="next slider_hover" @if(!isset($RELATED_BP) || count($RELATED_BP) < 5) style="display:none"@endif></a>
        <div class="front_slider cf">
            <ul>
                <?php $index = 1 ?>
                <li index="{!!$index!!}">
                @foreach($RELATED_BP as $key=>$row)
                    <div class="slider_element">
                        <a href="{!! url($row->slug != '' ? $row->slug : $row->u_id) !!}" class="page_link">
                            <div class="slider-image">
                                {!!$row->img!!}
                            </div>
                            <div class="title"><span>@if(isset($row->brand_title))[{!!$row->brand_title!!}] @endif{!! $row->title !!}</span></div>
                            <div class="date">@if(Session::get('lang') == LANG_EN){!!$row->updated_at->formatLocalized('%B %d, %Y')!!}@else{!!$row->updated_at->formatLocalized('%d. %B %Y')!!}@endif</div>
                            @if($row->tags)
                            <div class="tags">{!!$row->tags!!}</div>
                            @endif
                            
                            @if(isset($row->protected) && $row->protected)
                                <div class="protected" title='@lang('template.protected')'></div>
                            @endif
                        </a>
                    </div>
                    @if($index % 4 == 0 && $index < count($RELATED_BP))
                        </li>
                        <li index="{!!$index/4 +1!!}">
                    @endif
                    <?php $index++; ?>
                @endforeach
                </li>
            </ul>
        </div>
        <ul class="pages slider_hover"></ul>
    </div>
</div>
@endif
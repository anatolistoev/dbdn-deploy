<ul id="brandNav">
	<li class="brandSelect" id="brandSelect">
        @if(isset($PAGE->template) && $PAGE->template == 'home')
        <div id='blink_wrapper'>
            <div id='blinking_dot'></div>
        </div>
        @endif
        <a href="" class="menu_click">
            @lang('template.brand')
        </a>
    </li>
	<li id="brandLabel">

			@if(isset($L2ID) && in_array($L2ID, \App\Helpers\UserAccessHelper::getSpecialPagesByType(BRAND)))
				@if(isset($ASCENDANT_ID_INDEX[$L2ID]))
				<a href="{!! $ASCENDANT_ID_INDEX[$L2ID]->slug!!}">{!! $ASCENDANT_ID_INDEX[$L2ID]->title !!}</a>
				@elseif(isset($PAGE->id) && $L2ID == $PAGE->id)
				<a href="{!!$PAGE->slug!!}">{!! $CONTENTS['title'] !!}</a>
				@endif
			@endif
				@if(isset($L2ID) && in_array($L2ID, \App\Helpers\UserAccessHelper::getSpecialPagesByType(COMPANY)))
					@if(isset($ASCENDANT_ID_INDEX[$L2ID]))
					<a href="{!! $ASCENDANT_ID_INDEX[$L2ID]->slug!!}">{!! $ASCENDANT_ID_INDEX[$L2ID]->title !!}</a>
				@elseif(isset($PAGE->id) && $L2ID == $PAGE->id)
					<a href="{!!$PAGE->slug!!}">{!! $CONTENTS['title'] !!}</a>
				@endif
			@endif
	</li>
</ul>
<div class="flyout_menu popup">
	<a id="closeFlyoutMenu" class="closeFlyout popup-close"></a>
    <div id="brandMenu" class="cf">
    {{-- Set $L2ID if missing to prevent crash of the blade --}}
        <?php if (!isset($L2ID)) { $L2ID = 0; } ?>
        <div class="cf">
            <ul class="tree-nav-item">
                <li class="brand_group">
                    <a href="#">@lang('template.nav.corporate_brand')</a>
                </li>
                {!! \App\Helpers\HtmlHelper::generateBrandMenuItem(DAIMLER, $L2ID, true) !!}
            </ul>
            <ul class="tree-nav-item">
                <li class="brand_group">
                    <a href="#">@lang('template.nav.subsidaries')</a>
                </li>
                {!! \App\Helpers\HtmlHelper::generateBrandMenuItem(DAIMLER_BUSES, $L2ID, true) !!}
                {!! \App\Helpers\HtmlHelper::generateBrandMenuItem(DAIMLER_MOBILITY, $L2ID, true) !!}
                {!! \App\Helpers\HtmlHelper::generateBrandMenuItem(DAIMLER_TRUCKS, $L2ID, true) !!}
                {!! \App\Helpers\HtmlHelper::generateBrandMenuItem(DAIMLER_BKK, $L2ID, true) !!}
            </ul>
        </div>
        <div class="cf">
            <ul class="tree-nav-item">
                <li class="brand_group">
                    <a href="#">@lang('template.nav.brands')</a>
                </li>
                {!! \App\Helpers\HtmlHelper::generateBrandMenuItem(MERCEDES_BENZ, $L2ID, true) !!}
                {!! \App\Helpers\HtmlHelper::generateBrandMenuItem(SMART, $L2ID, true) !!}
                {!! \App\Helpers\HtmlHelper::generateBrandMenuItem(FUSO, $L2ID, true) !!}
                {!! \App\Helpers\HtmlHelper::generateBrandMenuItem(SETRA, $L2ID,true) !!}
                {!! \App\Helpers\HtmlHelper::generateBrandMenuItem(OMNIPLUS, $L2ID,true) !!}

            </ul>
            <ul class="tree-nav-item">
                <li class="brand_group empty">
                        &nbsp;
                </li>
                {!! \App\Helpers\HtmlHelper::generateBrandMenuItem(BUSSTORE, $L2ID,true) !!}
                {!! \App\Helpers\HtmlHelper::generateBrandMenuItem(TRUCK_FINANCIAL, $L2ID,true) !!}
                {!! \App\Helpers\HtmlHelper::generateBrandMenuItem(FLEET_MANAGEMENT, $L2ID,true) !!}
                {!! \App\Helpers\HtmlHelper::generateBrandMenuItem(FUSO_FINANCIAL, $L2ID,true) !!}
            </ul>
        </div>
    </div>
</div>
<a class="search_button" ></a>

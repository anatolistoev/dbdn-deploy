<section>
    @include('partials.back_button')
</section>
<section @if($PAGE->template == 'best_practice') class='best-practice' @else class="section_content" @endif>

    @if($PAGE->template == 'best_practice')
		<div id="parent_path" style="display:none">
            @foreach($SECTION_PID_INDEX[$L2ID] as $L2page)
                @if(in_array($L2page->id, $PATH_IDS))
                    {!!url($L2page->slug)!!}
                @endif
            @endforeach
        </div>
    @endif

    @if($PAGE->template == 'best_practice' || $PAGE->template == 'basic' || $PAGE->template == 'exposed')
	<div id='best_practice_stats' class="overview cf">
        <div class="date">@if(Session::get('lang') == LANG_EN){!!$PAGE->updated_at->formatLocalized('%B %d, %Y')!!}@else{!!$PAGE->updated_at->formatLocalized('%d. %B %Y')!!} @endif</div>
        <div class="stats">
            @if($PAGE->ratings()->count() > 0)<span class="rate">{!!$PAGE->ratings()->count()!!}</span>@endif
			@if($PAGE->activeComments()->count() > 0)<span class="comments">{!!$PAGE->activeComments()->count()!!}</span>@endif
			<span class="views">{!!$PAGE->visits!!}</span>
        </div>
        @if($PAGE->joinTags() && $PAGE->template == 'best_practice')
            <div class="tags">{!!$PAGE->joinTags()!!}</div>
        @endif
    </div>
    @endif

    <div class="middle_page_text @if($PAGE->template == 'exposed') exposed @endif">

        @if(!MOBILE && isset($CONTENTS['author']) && trim($CONTENTS['author'])!= "")
            <div id='contact_info'>
                <div id='author_title'><h2>@lang('template.best_practice.contact')</h2></div>
                <div id='author_content'>
                        {!!$CONTENTS['author']!!}
                </div>
           </div>
            <script>
            $(function() {
                var firtsHeader = $('#content_main :header').first();
                if(firtsHeader.length == 0){
                   firtsHeader = $('#content_main').children().first();
                }
                $('#contact_info').css('top', firtsHeader.position().top+firtsHeader.outerHeight()- $('#contact_info h2').outerHeight() - 3 +'px');
            });
            </script>
        @endif

        <div id="content_main">
            @if(isset($CONTENTS['main']))
		 {!!$CONTENTS['main']!!}
            @endif
        </div>

        @if(MOBILE && isset($CONTENTS['author']) && trim($CONTENTS['author'])!= "")
            <div id='contact_info'>
                <div id='author_title'><h2>@lang('template.best_practice.contact')</h2></div>
                <div id='author_content'>
                    {!!$CONTENTS['author']!!}
                </div>
           </div>
        @endif


        @if($PAGE->template == 'basic' || $PAGE->template == 'exposed')
            @include('partials.related_pages')
        @endif

    </div>

    @if(!MOBILE && ($PAGE->template == 'basic' || $PAGE->template == 'best_practice' || $PAGE->template == 'exposed'))
        @include('partials.related_bp', array('TITLE' => trans($PAGE->template == 'basic'?'template.related.bp': 'template.bp.related')))
    @endif

    @include('partials.comments')

    @if(!MOBILE && isset($PAGE->overview) && ($PAGE->overview != 1 && ($PAGE->template == 'basic' || $PAGE->template == 'best_practice' || $PAGE->template == 'exposed')))
        <div class="nav_buttons">
            <a class="move_up_button" href="" onClick="window.history.back(); return false;">
                <div></div>
                <!-- <span>@lang('template.navigation.back')</span>-->
            </a>
            @if(!is_null($PREV_PAGE))
            <a class="nav_button left" href="{!! asset($PREV_PAGE->slug != '' ? $PREV_PAGE->slug : $PREV_PAGE->id) !!}"><span>{!!$PREV_PAGE->title!!}</span></a>
            @endif
            @if(!is_null($NEXT_PAGE))
            <a id='nav_button_right' class="nav_button right @if(is_null($PREV_PAGE))last @endif" href="{!! asset($NEXT_PAGE->slug != '' ? $NEXT_PAGE->slug : $NEXT_PAGE->id) !!}"><span>{!!$NEXT_PAGE->title!!}</span></a>
            @endif
            <div style="clear:both;"></div>
        </div>
    @endif

    @if($PAGE->slug == 'Daimler_Corporate_Logotype')
        @include('partials.guided_tour', array('tour_page'=>'content', 'from' => 12, 'to' => 19))
    @elseif($PAGE->slug == 'Daimler_2016_Annual_Meeting')
        @include('partials.guided_tour', array('tour_page'=>'bp_content', 'from' => 20, 'to' => 22))
    @endif

</section>

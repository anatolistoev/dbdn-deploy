<script type="text/javascript">
    (function (namespace) {
        namespace.utils = namespace.utils || {};
        var me = namespace.utils;

        me.isForcedDesktopMode = function() {
            return Boolean(<?php echo !TABLET && !MOBILE ? 1 : 0; ?>) && matchMedia('(max-width:1025px)').matches;
        };
    }(window.DBDN = window.DBDN || {}));

        (function(namespace){
            namespace.scaleWebsite = namespace.scaleWebsite || {};
            me = namespace.scaleWebsite;

            var SCALE_FACTOR_DEFAULT = 1;
            var scaleFactor = SCALE_FACTOR_DEFAULT;

            var defaultScaleConditionFn = function() {
                return !matchMedia('(max-width:1023px)').matches
                && matchMedia('(max-width:1279px)').matches;
            };

            var viewportTagAttributes = {
                width: {enabled: true, attributeName: 'width', defaultValue: 'device-width'},
                initialScale: {enabled: true, attributeName: 'initial-scale', defaultValue: '%s'},
                maximumScale: {enabled: true, attributeName: 'maximum-scale', defaultValue: '%s'},
                minimumScale: {enabled: true, attributeName: 'minimum-scale', defaultValue: '%s'},
                userScalable: {enabled: true, attributeName: 'user-scalable', defaultValue: 0}
            };

            // Currently should support most mobile browsers.
            me.scale = function(viewportWidth, fixedWidth, conditionFn) {
                resetScaleFactorToDefaultValue();
                setViewportScaleFactor(SCALE_FACTOR_DEFAULT);

                if (typeof conditionFn !== 'function') {
                    conditionFn = defaultScaleConditionFn;
                }

                if (!conditionFn()) {
                    return;
                }

                viewportWidth = (viewportWidth | 0) || getViewportWidthASAP();
                viewportWidthOrig = viewportWidth;
                fixedWidth = (fixedWidth | 0) || 1280;

                if (!viewportWidth) {
                    console.log('The website cannot be scaled!');

                    return;
                }

                scaleFactor = viewportWidth / fixedWidth;

                setViewportScaleFactor(scaleFactor);

                // Guess it is safe to assume desktop browser
                //if (getViewportWidthASAP() === viewportWidthOrig) {
                //    setViewportScaleFactor(1);
                //    var htmlElement = document.querySelector('html');
                //    htmlElement.className += ' desktop-scaled';
                //    setStyles(htmlElement, {zoom:scaleFactor});
                //}
            };

            me.getScaleFactor = function (){
                return scaleFactor;
            };

            me.isViewportScaled = function (){
                if (scaleFactor > 1 || scaleFactor < 1) {
                    return true;
                }

                return false;
            };

            function resetScaleFactorToDefaultValue(){
                scaleFactor = SCALE_FACTOR_DEFAULT;
            }

            // Excluding scrollbar width
            function getViewportWidthASAP(){
                document.getElementsByTagName('html')[0].style.minHeight = '2200px';
                var viewport = getViewportWidth();
                document.getElementsByTagName('html')[0].style.minHeight = '';

                return viewport;
            }

            function getViewportWidth(){
                return ((document.documentElement || document.body)['clientWidth'] || document.body['clientWidth']) | 0;
            }

            function setStyles(element, styles){
                for(styleProperty in styles){
                    if(styles.hasOwnProperty(styleProperty)){
                        var correctPropertyName = getTheCorrectStylePropertyName(styleProperty);
                        if(!correctPropertyName){
                            continue;
                        }

                        element.style[correctPropertyName] = styles[styleProperty];
                    }
                }
            }

            function getTheCorrectStylePropertyName(propertyName){
                var inMemoryElement = document.createElement('darthVader');

                if (typeof inMemoryElement.style[propertyName] !== 'undefined') {
                    return propertyName;
                }

                var browserPrefixes = ['webkit', 'moz', 'ms', 'o'];

                for(var i = 0; i < browserPrefixes.length; i++){
                    var prefixedPropertyName = browserPrefixes[i] + propertyName;

                    if (typeof inMemoryElement.style[prefixedPropertyName] !== 'undefined') {
                        return prefixedPropertyName;
                    }
                }

                return false;
            }

            function setViewportScaleFactor(sFactor) {
                var contentBuffer = '';

                for (attribute in viewportTagAttributes) {
                    if (viewportTagAttributes.hasOwnProperty(attribute)
                        && viewportTagAttributes[attribute].enabled) {
                        contentBuffer += viewportTagAttributes[attribute].attributeName + '=' + viewportTagAttributes[attribute].defaultValue + ', ';
                    }
                }

                contentBuffer = contentBuffer.trim();
                contentBuffer = contentBuffer.substr(0, contentBuffer.length - 1);
                contentBuffer = contentBuffer.replace(/%s/g, sFactor);

                document
                    .querySelector('meta[name="viewport"]')
                    .setAttribute('content', contentBuffer);
            }

            // This trial and error voodoo should supposedly force the
            // browser to scale the website when in Request Desktop mode.
            var cookieRegexPattern = /(?:^|;\s?)__fdcnt=/;
            var cookieName = '__fdcnt';

            if (DBDN.utils.isForcedDesktopMode() 
                && !document.cookie.match(cookieRegexPattern)) {
                document.cookie = cookieName + '=1';

                location.href = location.pathname;
            }

            // Cleanup and prepare the environment for future use of Request Desktop mode.
            if (!DBDN.utils.isForcedDesktopMode()
                && document.cookie.match(cookieRegexPattern)) {
                document.cookie = cookieName + '=; expires=Thu, 01 Jan 1970 00:00:00 GMT';
            }

            if (DBDN.utils.isForcedDesktopMode()) {
                var scaleReal = me.scale;

                viewportTagAttributes.maximumScale.enabled = false;
                viewportTagAttributes.minimumScale.enabled = false;
                viewportTagAttributes.userScalable.defaultValue = 'yes';

                me.scale = function(){
                    scaleReal.call(me, null, null, function() {
                        return !matchMedia('(max-width:319px)').matches
                        && matchMedia('(max-width:1025px)').matches;
                    });
                };
            }

            me.scale();
        }(window.DBDN = window.DBDN || {}));
</script>
<script type="text/javascript">
        (function(NS){
            NS.utils = NS.utils || {};
            var me = NS.utils;

            @if(Auth::check())
                var isAuthenticated = true;
            @else
                var isAuthenticated = false;
            @endif

            me.isSessionAuthenticated = function(){
                return isAuthenticated;
            };
        }(window.DBDN = window.DBDN || {}));
</script>
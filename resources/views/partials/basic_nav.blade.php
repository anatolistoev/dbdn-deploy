<div class="menu_label first">
    @lang('template.navigation.filter_by')
</div>

<div class="content first">
    <ul class="level3_nav taxonomic">

        <li class="cf" id="period_filter">
            <div class="filter_label" title="@lang('template.navigation.by_period')"></div>
            <ul class="level4_nav">
                <li class="filter_title">
                    @lang('template.navigation.by_period')
                    <span class="filter_close"></span>
                </li>
                <li class="_cf">
                    <input type="radio" name="time" value="desc" class="chb" id="by_period_newest" action="order" @if(isset($DW_FILTERS['time']) && $DW_FILTERS['time'] == 'desc') checked='checked' @endif/>
                    <label for="by_period_newest">@lang('template.navigation.newest')</label>
                    <input type="radio" name="time" value="asc" class="chb" id="by_period_oldest" action="order" @if(isset($DW_FILTERS['time']) && $DW_FILTERS['time'] == 'asc') checked='checked' @endif/>
                    <label for="by_period_oldest">@lang('template.navigation.oldest')</label>
                    <label class="calendar">@lang('template.navigation.define_perion')</label>
                    <div class="period_time">
                        <div class="cf">
                            <label for="start_period">@lang('template.navigation.from')</label>
                            <input type="text" name="time_from" value="@if(isset($DW_FILTERS['time_from']) && $DW_FILTERS['time_from'] != ''){!!$DW_FILTERS['time_from']!!} @endif" class="has-datepicker" id="start_period" action="filter" onpaste="return false"/>
                        </div>
                        <div class="cf">
                            <label for="end_period">@lang('template.navigation.to')</label>
                            <input type="text" name="time_to" value="@if(isset($DW_FILTERS['time_to']) && $DW_FILTERS['time_to'] != ''){!!$DW_FILTERS['time_to']!!} @endif" class="has-datepicker" id="end_period" action="filter" onpaste="return false"/>
                        </div>
                    </div>
                </li>
            </ul>
        </li>

        @if($PAGE->template == 'search_results')
            <li class="cf" id="brand_filter">
                <div class="filter_label" title="@lang('template.navigation.by_brand')"></div>
                <ul class="level4_nav">
                    <li class="filter_title">
                        @lang('template.navigation.by_brand')
                        <span class="filter_close"></span>
                    </li>
                    <li class="_cf">
                       {{--  <input type="checkbox" name="brand" class="chb" id="by_brand_all" action="filter" value="all" @if(isset($DW_FILTERS['brand']) && is_array($DW_FILTERS['brand']) && in_array('all',$DW_FILTERS['brand'])) checked='checked' @endif/>
                        <label for="by_brand_all">@lang('template.navigation.select_all')</label>--}}
                        @foreach($BRAND_TITLES as $brand)
                            <input type="radio" name="brand" value="{!!$brand->page_id!!}" class="chb" id="by_brand_{!!$brand->page_id!!}" action="filter" @if(isset($DW_FILTERS['brand']) && is_array($DW_FILTERS['brand']) && in_array($brand->page_id,$DW_FILTERS['brand'])) checked='checked' @endif/>
                            <label for="by_brand_{!!$brand->page_id!!}">{!!$brand->title!!}</label>
                        @endforeach
                    </li>
                </ul>
            </li>
        @endif

        @if( 'search_results' != $PAGE->template )
            <li class="cf" id="tags_filter">
                <div class="filter_label" title="@lang('template.navigation.by_tags')"></div>
                <ul class="level4_nav" id="_load-more-tags" splitlist="10,2">
                    <li class="filter_title">
                        @lang('template.navigation.tags_label')
                        <span class="filter_close"></span>
                    </li>
                    <li class="_cf">
                        <input type="checkbox" name="tags" class="chb" id="tag_all" value="all" action="filter"  @if(isset($DW_FILTERS['tags']) && is_array($DW_FILTERS['tags']) && in_array('all',$DW_FILTERS['tags'])) checked='checked' @endif/>
                        <label for="tag_all">@lang('template.navigation.select_all')</label>
                    </li>
                    @foreach ($NAV_TAGS as $key=>$tag)
                        <li class="_cf">
                            <input type="checkbox" name="tags" class="chb" id="tag_{!!$key!!}" value="{!!$tag!!}" action="filter"  @if(isset($DW_FILTERS['tags']) && is_array($DW_FILTERS['tags']) && in_array($tag,$DW_FILTERS['tags'])) checked='checked' @endif/>
                            <label for="tag_{!!$key!!}">{!!$tag!!}</label>
                        </li>
                    @endforeach
                </ul>
            </li>
        @endif


        @if( 'downloads' == $PAGE->template )
            <li class="cf" id="usage_filter">
                <div class="filter_label" title="@lang('template.navigation.by_usage')"></div>
                <ul class="level4_nav">
                    <li class="filter_title">
                        @lang('template.navigation.usage_label')
                        <span class="filter_close"></span>
                    </li>
                    <li class="_cf">
                        <input type="checkbox" name="usage" class="chb" id="by_usage_all" action="filter" value="all" @if(isset($DW_FILTERS['usage']) && is_array($DW_FILTERS['usage']) && in_array('all',$DW_FILTERS['usage'])) checked='checked' @endif/>
                        <label for="by_usage_all">@lang('template.navigation.select_all')</label>
                    </li>
                    @foreach ($USAGE_FILTERS as $key => $value)
                        @if($key != 1)
                            <li class="_cf">
                                <input type="checkbox" name="usage" class="chb" id="by_usage_{!!$key!!}" action="filter" value="{!!$key!!}" @if(isset($DW_FILTERS['usage']) && is_array($DW_FILTERS['usage']) && in_array($key,$DW_FILTERS['usage'])) checked='checked' @endif/>
                                <label for="by_usage_{!!$key!!}">@lang('ws_general_controller.page_usage_'.\App\Helpers\UserAccessHelper::getPageConfiguration('usage',$key))</label>
                            </li>
                        @endif
                   @endforeach
                </ul>
            </li>

            <li class="cf" id="type_filter">
                <div class="filter_label" title="@lang('template.navigation.by_type')"></div>
                <ul class="level4_nav">
                    <li class="filter_title">
                        @lang('template.navigation.type_label')
                        <span class="filter_close"></span>
                    </li>
                    <li class="_cf">
                        <input type="checkbox" name="file_type" class="chb" id="file_type_all" action="filter" value="all" @if(isset($DW_FILTERS['file_type']) && is_array($DW_FILTERS['file_type']) && in_array('all',$DW_FILTERS['file_type'])) checked='checked' @endif />
                        <label for="file_type_all">@lang('template.navigation.select_all')</label><br/>
                    </li>
                    @foreach ($FILE_TYPES as $key => $value)
                        @if($key != 1)
                            <li class="_cf">
                                <input type="checkbox" name="file_type" class="chb" id="file_type_{!!$key!!}" action="filter" value="{!!$key!!}" @if(isset($DW_FILTERS['file_type']) && is_array($DW_FILTERS['file_type']) && in_array($key,$DW_FILTERS['file_type'])) checked='checked' @endif />
                                <label for="file_type_{!!$key!!}">@lang('ws_general_controller.page_file_type_'.\App\Helpers\UserAccessHelper::getPageConfiguration('file_type',$key))</label><br/>
                            </li>
                        @endif
                   @endforeach
                </ul>
            </li>
        @endif

    </ul>
    @if(MOBILE)
        <button type="button" class="filter-refine" onclick="sendMobileBrowserRequest(this)">@lang('system.filters.refine')</button>
    @endif
</div>



<div class="menu_label second">
    @lang('template.navigation.sort_by')
</div>


<div class="content second">
    <ul class="level3_nav taxonomic">

        @if( $PAGE->template == 'downloads' || $PAGE->template == 'faq' )
        <li class="cf" id="title_filter">
            <div class="filter_label" title="@lang('template.navigation.by_title')"></div>
            <ul class="level4_nav">
                <li class="filter_title">
                    @lang('template.navigation.by_title')
                    <span class="filter_close"></span>
                </li>
                <li class="_cf">
                    <input type="radio" name="title" value="asc" class="chb" id="by_title_a_z" action="order" @if(isset($DW_FILTERS['title']) && $DW_FILTERS['title'] == 'asc') checked='checked' @endif/>
                    <label for="by_title_a_z">@lang('template.navigation.a_z')</label>
                    <input type="radio" name="title" value="desc" class="chb" id="by_title_z_a" action="order" @if(isset($DW_FILTERS['title']) && $DW_FILTERS['title'] == 'desc') checked='checked' @endif/>
                    <label for="by_title_z_a">@lang('template.navigation.z_a')</label>
                </li>
            </ul>
        </li>
        @endif

        <li class="cf" id="view_filter">
            <div class="filter_label" title="@lang('template.navigation.by_views')"></div>
            <ul class="level4_nav">
                <li class="filter_title">
                    @lang('template.navigation.by_views')
                    <span class="filter_close"></span>
                </li>
                <li class="_cf">
                    <input type="checkbox" name="viewed" class="chb" id="by_view_most_viewed" action="order" value="desc" @if(isset($DW_FILTERS['viewed']) && $DW_FILTERS['viewed'] == 'desc') checked='checked' @endif />
                    <label for="by_view_most_viewed">@lang('template.navigation.most_viewed')</label>
                </li>
            </ul>
        </li>

        <li class="cf" id="rating_filter">
            <div class="filter_label" title="@lang('template.navigation.by_rating')"></div>
            <ul class="level4_nav">
                <li class="filter_title">
                    @lang('template.navigation.by_rating')
                    <span class="filter_close"></span>
                </li>
                <li class="_cf">
                    <input type="checkbox" name="rating" class="chb" id="rating_most_rated" value="desc" action="order" @if(isset($DW_FILTERS['rating']) && $DW_FILTERS['rating'] == 'desc') checked='checked' @endif />
                    <label for="rating_most_rated">@lang('template.navigation.most_rated')</label>
                </li>
            </ul>
        </li>

        @if( 'best_practice' == $PAGE->template )
            <li class="cf" id="comments_filter">
                <div class="filter_label" title="@lang('template.navigation.by_comments')"></div>
                <ul class="level4_nav">
                    <li class="filter_title">
                        @lang('template.navigation.by_comments')
                        <span class="filter_close"></span>
                    </li>
                    <li class="_cf">
                        <input type="checkbox" name="comments" class="chb" id="by_comments_most" action="order" value="desc"  @if(isset($DW_FILTERS['comments']) && $DW_FILTERS['comments'] == 'desc') checked='checked' @endif/>
                        <label for="by_comments_most">@lang('template.navigation.comments_label')</label>
                    </li>
                </ul>
            </li>
        @endif

    </ul>
    @if(MOBILE)
        <button type="button" class="filter-refine" onclick="sendMobileBrowserRequest(this)">@lang('system.filters.refine')</button>
    @endif
</div>

<div class="filter-panel">


    {!! Form::open(array('url' => $PAGE->template.'_filter', 'id' => 'filter-panel-selected', 'method'=>'POST')) !!}
        @if($PAGE->template == 'faq')
            {!! Form::hidden('brand_id', (isset($PAGE->brand) && $PAGE->brand == SMART) ? SMART : DAIMLER) !!}
        @else
            {!! Form::hidden('pageid', $PAGE->id) !!}
        @endif

        @if(MOBILE)
            <div class="filter-panel-empty" onclick="($('div.menu_label.active').length && $('div.menu_label.active').click()) || $('div.menu_label.first').click();">
                @lang('template.filters.chooseopt')
            </div>
        @endif

        <div class="filter-panel-title">
            @lang('template.navigation.selection')
            [<span id="active_filter_count"></span>]
        </div>

        <span class="filter-tags" id="filter-example">
            <label for="">
                <input type="checkbox" checked="checked" class="__chbx" name="" id=""/>
                <span class="filter_value"></span>
                <span class="filter_remove"></span>
            </label>
        </span>
    {!! Form::close() !!}
</div>

<span class="unchecked-all" value="unchecked all" id="remove_filters">
    <span class="clear-filter">@lang('template.navigation.clear')</span>
    <span class="filter_remove"></span>
</span>

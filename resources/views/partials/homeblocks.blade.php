    <div class="heading-row">
        <h1>Best Practice</h1>
        <div id="view-controls"  @if(!isset($HOMEBLOCKS) || sizeof($HOMEBLOCKS)<3) style="display:none"@endif>
            <a class="view-grid slider-grid" href="">&nbsp;</a>
            <a class="view-slider active" href="" >&nbsp;</a>
        </div>
        <div style="clear:both;"></div>
    </div>
    <?php  ?>
    @if(isset($HOMEBLOCKS) && sizeof($HOMEBLOCKS))
    <div class="overview slider">
        <div class="wrap">
            @if(isset($HOMEBLOCKS) && sizeof($HOMEBLOCKS)>2)
            <a class="prev btn_navigation"></a>
            <a class="next btn_navigation"></a>
            @endif
            <div class="frame mslide" id="oneperframe1">
                <ul class="cf">
                    <li index="1">
                        <div class="slider-item">
                @foreach($HOMEBLOCKS as $key=>$page)
                    @if(($key +1) % 2 == 0)
                        <div class="col2 page">
                    @else
                        <div class="col1 page">
                    @endif
                            <a href="{!!asset($page->slug)!!}" class="page_link">
                                <div class="image story-layover">
                                    {!!$page->img!!}
                                    @if(isset($page->protected) && $page->protected)
                                        <div class="protected" title='@lang('template.protected')'></div>
                                    @endif
                                </div>
                                <div class="title"><span>{!!$page->title!!}</span></div>
                                <div class="date">@if(Session::get('lang') == LANG_EN){!!$page->updated_at->formatLocalized('%B %d, %Y')!!}@else{!!$page->updated_at->formatLocalized('%d. %B %Y')!!}@endif</div>
                                <div class="stats">
                                    @if(1 || $page->ratings()->count() > 0)<span class="rate">{!!$page->ratings()->count()!!}</span>@endif
									@if($page->template=='best_practice' && $page->activeComments()->count() > 0)<span class="comments">{!!$page->activeComments()->count()!!}</span>@endif
									<span class="views">{!!$page->visits!!}</span>
                                </div>
                                @if($page->tags)
                                    <div class="tags">{!!$page->tags!!}</div>
                                @endif
                            </a>
                        </div>
                        @if(($key +1) % 2 == 0 && $key < count($HOMEBLOCKS)-1)
                                    </div>
                                </li>
                                <li index="{!!($key +1)/2 +1!!}">
                                    <div class="slider-item">
                       @endif
                @endforeach
                       </div>
                    </li>
                </ul>
            </div>
            <ul class="pages">
                @for($p = 0; $p < ceil(count($HOMEBLOCKS) / 2); $p++)
                    <li>{!!$p+1!!}</li>
                @endfor
            </ul>
        </div>
    </div>
    @endif
    @if(sizeof($HOMEBLOCKS)<3)
    <script>
        $(function() {
            $('.view-grid').trigger('click');
        });
    </script>
    @endif

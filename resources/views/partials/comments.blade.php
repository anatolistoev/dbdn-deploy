@if($PAGE->template == 'best_practice')

	<div class="comment-form">
		@if($PAGE->activeComments()->count() > 0)
			<h1>@lang('template.comments.title')</h1>
			@foreach($PAGE->activeComments() as $comment)
				<div class="comments-layout">
					<h2 class="comment-writer">{!!$comment->user->first_name!!} {!!$comment->user->last_name!!}</h2>
					<span class="comment-time">{!!date('H:i',strtotime($comment->created_at))!!} | @if(Session::get('lang') == LANG_EN){!!date('F d, Y',strtotime($comment->created_at))!!}@else{!!date('d. F Y',strtotime($comment->created_at))!!}@endif</span>
					<div class="cf" style="margin-bottom: 38px;"></div>
					<h1><small>{!!$comment->subject!!}</small></h1>
					<p class="comment-content">{!!$comment->body!!}</p>
				</div>
			@endforeach
		@endif
        <div style="clear: both;"></div>
        @if(Auth::check())

			<h1>@lang('template.comments.add_your')</h1>
			{!! Form::open(array('url' => '/comments/'.$PAGE->slug, 'method'=>'POST')) !!}
			{!! Form::text('headline', Request::old('headline'), array('placeholder' => trans('template.comment.headline'),'class'=> 'headline '.($errors->has('headline') ? 'error' : '' ))) !!}<br/>
			{!! Form::textarea('comments', Request::old('comments'), array('placeholder' =>  trans('template.comment.text'), 'class'=> 'comment-box '.($errors->has('comments') ? 'error' : '' ) )) !!}<br/>
			<input type="checkbox" name="comment_agree" class="chb" id="comments_agree"/>
			<label for="comments_agree" class="agree_label @if($errors->has('comments')) error @endif ">@lang('template.comments.iagree') @lang('template.comments.terms')</label>
			{!! Form::submit(trans('template.comments.send'), array('class' => 'post-button')) !!}
			{!! Form::close() !!}
			<div style="clear: both;"></div>
		@else
			<h1 id="comment-form-header">@lang('template.comments.add_your')</h1>
			<p>@lang('template.comments.not_logged')</p>
		@endif
	</div>
@endif
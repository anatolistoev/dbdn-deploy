<style>
	#LE{width:960px; margin: 0 0 0px 0px; padding: 0 0px; overflow: hidden; position: relative; height: 150px;}
	#LE-bgs{width: 10000px; height: 100%; position: absolute; left:10px; top: 0;}
	.LE-bg{width: 960px; height: 100%; float: left; position: relative;}
	#LE-apps{width: 510px; position: absolute; left:10px; top: 10px;}
	/*#LE-legend:before{ content: 'Layout elements'; display: block; background: #e9eaeb; text-align: center; padding: 3px; font-weight: bold;}*/
	
	.LE-app{display: none;}
	
	.pois{position: absolute; left: 0; top: 0; width: 100%; height: 100%;}
	.poi{background: #a12129; color: #fff; font: bold 14px/20px Arial; width: 20px; height: 20px; text-align: center; z-index: 100; cursor: pointer; }
	div.poi{position: absolute;}
	span.poi{margin: 0 10px 15px 0; display: inline-block;}
	.poi.up:after{ content: ''; display: block; position: absolute; top: -15px; left:9px; width: 2px; height: 15px; background: #a12129;}
	.poi.right:after{ content: ''; display: block; position: absolute; top: 9px; left:20px; width: 15px; height: 2px; background: #a12129; }
	.poi.down:after{ content: ''; display: block; position: absolute; top: 20px; left:9px; width: 2px; height: 15px; background: #a12129; }
	.poi.left:after{ content: ''; display: block; position: absolute; top: 9px; left:-15px; width: 15px; height: 2px; background: #a12129; }
	
	.LE-layer,.LE-tool-layer{position: absolute; background-repeat: no-repeat; background-position: left top;}
	/*.axis, .grid, .frame, .area{ display: none; }*/
	/*.hilight{ border: 1px solid #a12129; margin:-1px 0 0 -1px;}*/
	
	#LE-legend{display: none; margin: 0px; padding: 0; width: 410px; height: 280; font: 12px/20px Arial; color: #666; position: absolute; left: 520px; top: 59px;}
	#LE-legend-title{font:20px/1.5 corporateS; color: #000;}
	#LE-legend div a{ background: url(img/template/icons/arrow.gif) left 4px no-repeat; padding: 0 0 0 9px; }
	#LE-legend-info img {margin: 10px 0px;}
	
	#LE-btns{ border: 1px dashed cyan; position: absolute; top: 10px; left: 510px; right: 0; bottom: 0;}
	.expand-collapse{position: absolute; width: 50px; height: 50px; bottom: 10px; right: 20px; background-color: #f3f3f4; cursor: pointer;}
	.expand-collapse span{width:32px; height: 17px; background: #f3f3f4 url(img/LE/expandcollapse.png) top left no-repeat;display: block; left: 9px; top: 17px; position: absolute;}
	.expand-collapse:hover span{background-position: top right;}
	#LE.open .expand-collapse span{background-position: bottom left;}
	#LE.open .expand-collapse:hover span{background-position: bottom right;}
	.expand-collapse div.instructions{ position: absolute; right: 100%; padding: 0 10px; font: 12px/50px Arial; color: #666; white-space: nowrap; }
	.toOpen.expand-collapse div.instructions{ background-color: rgb(200,200,200); background-color: rgba(255,255,255, 0.7);filter:alpha(opacity=70); -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=70)"; }
	.expand-collapse:hover div.instructions{ color: #3683AB; }
	.toOpen{ display: block; }
	#LE-collapse{ display: none; }
	
	.expand-collapse.leftSide{ right: auto; left: 20px;}
	.expand-collapse.leftSide div.instructions{ right: auto; left: 100%; }
	
	#LE.open .toOpen, #LE.open #LE-btns{ display: none; }
	#LE.open #LE-collapse{ display: block; right: 10px; }
	
	/*.LE-expand { display: block; padding: 0 0 0 9px; margin: 0 0 0 20px; background: #fff url(img/template/icons/arrow.gif) left 4px no-repeat; overflow: hidden; width: 220px; }*/
	
	.LE-expand:hover {background-color: #F0F1F2;}
	#LE-tools{ display:none; position: absolute; top: 10px; left: 10px; height: 40px; width: 610px;}
	#LE-tools div{ height: 20px; width: 214px; float: left; cursor: pointer; display:none;}
	#LE-tools div span.icon{background: url(img/LE/eyes.gif) left bottom no-repeat; width: 21px; height: 11px; margin: 0; display: inline-block; position: relative; top: 1px;}
	#LE-tools div span.title{display: inline-block; margin-left: 2px; }
	/*#LE-tools > div:first-child + div,
	#LE-tools > div:first-child + div + div + div,
	#LE-tools > div:first-child + div + div + div + div + div{ margin-left: 10px; }*/
	#LE-tools div.on span.icon{ background-position: left top;}
	
	#LE-nav{ position: relative; left: 50%; margin: 10px 0 0 -50px; height: 4px; }
	.LE-nav-btn{ width: 19px; height: 4px; background-color: #c8c8c8; margin-right: 1px; float: left; }
	.LE-nav-btn.active, .LE-nav-btn:hover{ background-color: #3683ab; }
</style>
<script type="text/javascript" src="{!! mix('js/jquery-ui-1.10.2.custom.min.js') !!}"></script>
<script>
	var data = {!! file_get_contents(realpath(public_path('LE/'. $PAGE->id . '.' . App::getLocale() . '.js'))) !!};
	var descriptions = {
	@if(Session::get('lang')=='en')
		axis: {
			title: 'Horizontal Axis',
			info: 'On inside pages pictures, surfaces and typography take up the horizontal axis time and again, yet break through it deliberately. This creates an energetic rhythm.'
		},
		grid: {
			title: 'Layout Grid',
			info: 'The design grid for all formats is made up of 11 mm x 12 mm sized basic elements. These are positioned horizontally and vertically at a 4 mm distance to each other. The design grid is centered horizontally in the format. The baseline grid primarily serves to position type. It is drawn horizontally across the format in 2 mm steps and corresponds to the design grid.'
		},
		frame: {
			title: 'Design Area and Frame',
			info: 'The design area is larger than the type area and is closer to the margin. It defines a frame, on the inside boundary of which images and surfaces may adjoin. Type is only set within the somewhat smaller type area. Design area and frame are always white, the headline is not placed over an area or image. A white frame ensures the Daimler appearance when surfaces are completely filled.'
		},
		"area": {
			title: 'Type Area',
			info: 'Type is only set within the type area.'
		}
	@else
		axis: {
			title: 'Horizontale Achse',
			info: 'Die horizontale Gestaltungsachse im oberen Drittel einer Seite wird als \u201eSchulter\u201c bezeichnet. Sie teilt das Format in einen \xdcber- und Unterschulterbereich. Auf Titelseiten ist die Fl\xe4che oberhalb der optischen Achse dem Unternehmenszeichen vorbehalten.'
		},
		grid: {
			title: 'Gestaltungsraster',
			info: 'Die Breite des Unternehmenszeichens betr\xe4gt 32 mm und wird im Abstand von 10 mm vom oberen Formatrand zur Grundlinie des Zeichens zentriert auf das Format gesetzt. F\xfcr den Druck des Unternehmenszeichens auf Visitenkarten wird aus drucktechnischen Gr\xfcnden die Sonderfarbe Pantone&reg; 2757 verwendet.'
		},
		frame: {
			title: 'Gestaltungsfl\xe4che und Rahmen',
			info: 'Die Gestaltungsfl\xe4che ist gr\xf6\xdfer als der Satzspiegel und begrenzt die maximale Ausdehnung der Bilder und Farbfl\xe4chen. Wird die Gestaltungsfl\xe4che mit einem Bildmotiv ausgef\xfcllt, entsteht so automatisch ein wei\xdfer Rahmen als Begrenzung.'
		},
		"area": {
			title: 'Satzspiegel',
			info: 'Schrift wird nur innerhalb des Satzspiegels gesetzt.'
		}	
	@endif
	}
	var defaultInfo = '{!! App::getLocale() == 'en' ? 'Navigate with mouse over image to explore the layout elements.' : 'Bewegen Sie den Mauszeiger &uuml;ber die Ziffern im Bild, um n&auml;here Informationen zu den Layoutelementen zu bekommen.' !!}';
	
	function les(data) {
		var $LE = $('#LE').css(data.applications[0].scene.initial)//.css(data.scene.initial);
		for (var a=0; a < data.applications.length; a++) {
			createApp(a, data.applications[a], $LE);
		}
		//set handlers for poi
		$LE.find('#LE-apps div.LE-app div.pois div.poi').hover(
			function () {
				var $this = $(this);
				var $legend = $('#LE-legend');
				var $app = $this.parent().parent();
				var appID = $app.attr('id').split('-')[2];

				$legend.children('#LE-legend-title').html('<span class="poi">'+ $this.text() +'</span>'+data.applications[appID].layers[$this.index()].title);
				$legend.children('#LE-legend-info').html(data.applications[appID].layers[$this.index()].info);
				//	  pois     app 
				$this.parent().parent().find('div.LE-layer').each(
					function(ind){
						var $el = $(this);
						if( ind == $this.index()){
							$el.addClass('hilight');
						}else{
							if($el.is(':visible'))
								$el.fadeTo(200, 0.4);
						}
					}
				);	
			},
			function () {
				var $this = $(this);
				$this.parent().parent().find('div.LE-layer').each(function(ind){
					var $el = $(this);
					if( ind == $this.index()){
						$el.removeClass('hilight');
					}else{
						if ($el.is(':visible'))
							$el.fadeTo(200, 1);
					}
				});
			}
		);
		// expand button(s)
		$LE.find('#LE-bgs .toOpen, #LE-btns .LE-expand').click(function(){
			$(this).parent().parent().stop(true, false)
			var me = $(this);
			//if(me.hasClass('leftSide')){
			//	$('#LE-collapse').addClass('leftSide');
			//}
			var $LE = $('#LE');
			var appID = this.id.split('-')[2];
			if ($LE.hasClass('open')) {
				$('#LE #LE-apps .LE-app:visible').hide(400);
			}else{
				$LE.addClass('open');
				$LE.animate(data.applications[appID].scene.open, 400, function(){
					$LE.css(data.applications[appID].scene.open);
				});
			}
			$('#LE #LE-bgs, #LE-nav').hide(400);
			var $app = $LE.find('.LE-app').eq(appID).show(400, function(){
				var $tool;
				for (var t = 1; t < 7; t++){
					//console.log(t+' '+$LE.find('.LE-app:visible #LE-tool-layer-'+t).is(':visible'));
					$tool = $('div#LE-tool-'+t)
						.toggle( data.applications[appID].tools[t] && data.applications[appID].tools[t] != '')
						.toggleClass('on', $LE.find('.LE-app:visible #LE-tool-layer-'+t).is(':visible'));
					//if( $LE.find('.LE-app:visible #LE-tool-layer-'+t).is(':visible')){
					//	$tool.addClass('on');
					//}
				}
			});
			$LE.find('#LE-legend, #LE-tools').fadeIn(400);
			//hide tools
			/*$('#LE-axis').toggle( data.applications[appID].tools.axis && data.applications[appID].tools.axis != '');
			$('#LE-grid').toggle( data.applications[appID].tools.grid && data.applications[appID].tools.grid != '');
			$('#LE-frame').toggle( data.applications[appID].tools.frame && data.applications[appID].tools.frame != '');
			$('#LE-area').toggle( data.applications[appID].tools.area && data.applications[appID].tools.area != '');*/
			
			setTimeout(function(){
				window.scrollTo(0,150);
			},300);
		});
		// collapse button
		$LE.find('#LE-collapse').click(function(){
			var $this = $(this);
			var $dad = $LE;
			var appID = $('#LE #LE-apps .LE-app:visible').attr('id').split('-')[2];
			$('#LE #LE-bgs, #LE-nav').show(400);
			$dad.animate(data.applications[appID].scene.initial, 400, function(){
				$dad.css(data.applications[appID].scene.initial);
				$dad.removeClass('open');
				$('#LE #LE-apps .LE-app:visible').hide(400);
				$dad.find('#LE-legend-title').html('');
				$dad.find('#LE-legend-info').html(defaultInfo);
				$dad.find('#LE-legend, #LE-tools').hide(400);
				//$this.removeClass('leftSide');
				window.scrollTo(0,0);
			});
			
		});
		// tools
		$('#LE-tools div').click(function(){
			var $this = $(this).toggleClass('on');
			//if ($this.not(':visible')) return false;
			//var appID = $('#LE #LE-apps .LE-app:visible').attr('id').split('-')[2];
			//var prop = this.id.split('-')[2];
			var prop = $this.attr('value');
			var appID = $('#LE #LE-apps .LE-app:visible #LE-tool-layer-'+prop).toggle();
			
			/*if (!appID || !prop) {
				return false;
			}*/
			
		}).mouseenter(function(){
			var $this = $(this);
			//console.log($this.attr('class'))
			//var prop = this.id.split('-')[1];
			var prop = $this.attr('value');
			var appID = $('#LE #LE-apps .LE-app:visible').attr('value');
			$('#LE-legend-title').html(data.applications[appID].tools[prop].title);
			$('#LE-legend-info').html(data.applications[appID].tools[prop].info);
		})
	}
	/**************************************
	 * generate HTML for an application
	 **************************************/
	function createApp(index, settings, $LE) {
		// create bg
		var $bg = $('<div id="LE-bg-'+index+'" class="LE-bg"></div>');
		$bg.css(settings.scene.initial);
		//create the app
		var $app = $('<div id="LE-app-'+index+'" class="LE-app" value="'+index+'"></div>');
		$app.css(settings.css);
		var $pois = $('<div id="LE-pois-'+index+'" class="pois"></div>');
		
			//create layers
		var layer, newLayer, poi, newItem;
		
		for (var i=0; i<settings.layers.length; i++) {
			layer = settings.layers[i];
			newLayer = $('<div class="LE-layer">'+ layer.contents +'</div>');
			newLayer.css(layer.css);
			$app.append(newLayer);
			
			//add poi
			poi = $('<div class="poi '+layer.arrow['class']+'">'+(i+1)+'</div>');
			poi.css(layer.arrow);
			$pois.append(poi);
		}
		$app.append($pois);
/*		if (settings.tools.axis !== false ){
			$app.append($('<div class="axis"></div>').css(settings.tools.axis));
		}
		if (settings.tools.grid !== false ){
			$app.append($('<div class="grid"></div>').css(settings.tools.grid));
		}
		if (settings.tools.frame !== false ){
			$app.append($('<div class="frame"></div>').css(settings.tools.frame));
		}
		if (settings.tools.area !== false ){
			$app.append($('<div class="area"></div>').css(settings.tools.area));
		}*/
		var $tools = $LE.find("#LE-tools");
		var $tool;
		for (var t = 1; t < 7; t++) {
			if (settings.tools[t] !== false) {
				$app.append($('<div class="LE-tool-layer" id="LE-tool-layer-'+t+'"></div>').css(settings.tools[t].css));
				$tool = $tools.find('#LE-tool-'+t);
				$tool.children('span.title').html(settings.tools[t].title);
				$tool.show();
			}
		}
		$LE.children('#LE-apps').append($app);
		
		//add expand/close
		//if (data.applications.length == 1) {
			var btn = $('<div id="LE-btn-'+index+'" class="expand-collapse toOpen'+(settings.leftExpandButton ? ' leftSide':'')+'">\
				<span></span>\
				<div class="instructions">'+settings.expandText+'</div>\
			</div>');
		/*}else{
			var btn = '<a href="#" class="LE-expand" id="LE-expand-'+index+'">'+settings.expandText+'</a>';
		}*/
		//$LE.children('#LE-btns').append(btn);
		$bg.append(btn);
		$LE.children('#LE-bgs').append($bg);
		$('#LE-nav').append('<a href="javascript:;" onclick="manualScrollLE(this);" class="LE-nav-btn'+(index == 0 ? ' active':'')+'"></a>');
		
		$app = newLayer = poi = layer = null; // clean up
	}
	/**************************************
	 *
	 **************************************/
	function manualScrollLE(callingA){
		var $caller = $(callingA);
		var offset = 10;
		var pos = $caller.index();
		$('#LE-bgs')
			.stop(true, false)
			.animate({left: offset+pos*-960}, 400, 'easeInOutQuad');
		$caller.siblings().removeClass('active');
		$caller.addClass('active');
	}
	/**************************************
	 *
	 **************************************/
	function autoScrollLE(){
		var $scrolling = $('#LE-bgs');
		var offset = 10;
		var lft = parseInt($scrolling.css('left'), 10)-offset;
		$('#LE-nav a.LE-nav-btn').removeClass('active');
		$('#LE-nav a.LE-nav-btn').eq(lft/-960).addClass('active');
		//if( lft > (960 * -4)){
		if( lft > (-960 * ($scrolling.children('div').length - 1)) ){ // is container.left > the total (negative) width of its children
			$scrolling
				.delay(5000)
				//.delay(1000)
				.animate({left: offset+lft-960}, 700, 'easeInOutQuad',
						 function(){
							autoScrollLE();
						});
		}else{
			$scrolling
				.delay(7500)
				//.delay(1500)
				.animate({left: offset}, 900, 'easeInOutQuad',
						function(){
							autoScrollLE();
						});
		}
	}
	jQuery(function( $ ){
		les(data);
		autoScrollLE();
	});
</script>
<div id="LE">
	<div id="LE-bgs"></div>
	<div id="LE-apps"></div>
	
	<div id="LE-legend">
		<div id="LE-legend-title"></div>
		<div id="LE-legend-info">{!! Session::get('lang') == '1' ? 'Navigate with mouse over image to explore the layout elements.' : 'Bewegen Sie den Mauszeiger &uuml;ber die Ziffern im Bild, um n&auml;here Informationen zu den Layoutelementen zu bekommen.' !!}</div>
	</div>
	<div id="LE-collapse" class="expand-collapse">
		<span></span>
		<div class="instructions">{!! Session::get('lang') == '2' ? 'Detailansicht schlie&szlig;en': 'Collapse' !!}</div>
	</div>
	<!--<div id="LE-btns"></div>-->
	<div id="LE-tools">
		<div id="LE-tool-1" value="1"><span class="icon"></span><span class="title"></span></div>
		<div id="LE-tool-2" value="2"><span class="icon"></span><span class="title"></span></div>
		<div id="LE-tool-3" value="3"><span class="icon"></span><span class="title"></span></div>
		<div id="LE-tool-4" value="4"><span class="icon"></span><span class="title"></span></div>
		<div id="LE-tool-5" value="5"><span class="icon"></span><span class="title"></span></div>
		<div id="LE-tool-6" value="6"><span class="icon"></span><span class="title"></span></div>
	</div>
</div>
<div id="LE-nav"></div>
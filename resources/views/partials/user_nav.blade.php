@if(Auth::check())
	<section class="basic_nav_closed">
        <div></div>
        <span>@lang('template.navigation.menu')</span>
	</section>
	<section class="basic_nav_opened">
		<div class="basic_nav mydownloads">			
            <p class="level2_title">{!! Auth::user()->first_name !!} {!! Auth::user()->last_name !!}</p>
			<span id="menu_close"></span>
			<div style="clear:both;"></div>
                            <div class="content">
                                <div class="scbr">
                                    <div class="scrollbar-rail">
                                    <ul class="level3_nav">
                                        <li id="left_user_profile" class="cf @if($PAGE->slug == 'profile')active @endif">
                                            <a href="{!!url('profile')!!}">@lang('template.wl.user_profile')</a>
                                        </li>
                                        <li id="left_download_cart" class="cf @if($PAGE->slug == 'Download_Cart')active @endif">
                                            <a href="{!!url('Download_Cart')!!}">@lang('template.wl.download_cart')</a>
                                                <span class="counter">{!! Session::get('myDownloadsCount') !!}</span>
                                        </li>
                                        <li id="left_watch_list" class="cf @if($PAGE->slug == 'Watchlist')active @endif">
                                            <a href="{!!url('Watchlist')!!}">@lang('template.wl.watch_list')</a>
                                                <span class="counter">{!! Session::get('watchlistCount') !!}</span>
                                        </li>
                                        <li id="left_logout" class="cf">
                                            <a href="{!!url('logout')!!}">@lang('template.logout')</a>
                                        </li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                </div>
	</section>
@endif
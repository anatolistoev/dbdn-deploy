<div id="view-controls">

    @if(isset($listView))
        @if($listView)
            <a class="view-grid" href="">&nbsp;</a>
            <a class="view-list active" href="">&nbsp;</a>
        @else
            <a class="view-grid active" href="">&nbsp;</a>
            <a class="view-list" href="">&nbsp;</a>
        @endif
    @else
        @if(MOBILE)
            <a class="view-grid" href="">&nbsp;</a>
            <a class="view-list active" href="">&nbsp;</a>
        @else
            <a class="view-grid active" href="">&nbsp;</a>
            <a class="view-list" href="">&nbsp;</a>
        @endif
    @endif
</div>

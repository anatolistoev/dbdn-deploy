{{--
Variables from Dispatcher: PAGE, L2ID, PATH_IDS, CONTENTS
--}}
@if(isset($SECTION_PID_INDEX, $L2ID, $SECTION_PID_INDEX[$L2ID]) && $L2ID > 1)
<div id="rubrics_wrapper" class="cf">
    <ul id="rubrics" class="cf">
		@foreach($SECTION_PID_INDEX[$L2ID] as $L2page)
			@if($L2page->in_navigation == 1)
            <li id="L2ID{!! $L2page->id !!}" class="{!! (in_array($L2page->id, $PATH_IDS) ? 'active activeTmp' : '') !!}"><a href="{!!asset($L2page->slug != '' ? $L2page->slug : $L2page->id)!!}">{!! $L2page->title !!}</a></li>
{{--				<div id="L2ID{!! $L2page->id !!}" class="L2Link{!! (in_array($L2page->id, $PATH_IDS) ? ' L2LinkActive' : '') !!}"><a href="{!! asset($L2page->slug != '' ? $L2page->slug : $L2page->id) !!}" class="L2LinkA">{!! $L2page->title !!}</a> --}}
{{--					@if(isset($SECTION_PID_INDEX[$L2page->id]) && sizeof($SECTION_PID_INDEX[$L2page->id]) > 0)
						<div class="L3"><table class="L3table"><tr><td><div class="L3Group">
						@if(($increase = 1) && $items = 0)
						@endif
						@foreach($SECTION_PID_INDEX[$L2page->id] as $L3page)
							@if($L3page->in_navigation == 1)
								@if($items > 0 && ($items+1) % $SETTINGS['colLimit'] == 0)
									</div></td><td><div class="L3Group">
								@endif

								<a href="{!! asset($L3page->slug != '' ? $L3page->slug : $L3page->id) !!}" class="{!! $L3page->id == $PAGE->id ? 'L3LinkActive' : 'L3Link' !!}">{!! $L3page->title !!}</a>
								@if( ($items+= $increase) && $increase = ceil(mb_strlen(html_entity_decode($L3page->title, ENT_NOQUOTES, 'UTF-8'))/$SETTINGS['lineSize3']))
								@endif
								@if(isset($SECTION_PID_INDEX[$L3page->id]))
									@foreach($SECTION_PID_INDEX[$L3page->id] as $L4page )
										@if($L4page->in_navigation == 1)
											@if(($items+1) % $SETTINGS['colLimit'] == 0)
												</div></td><td><div class="L3Group">
											@endif
											<a href="{!! asset($L4page->slug != '' ? $L4page->slug : $L4page->id) !!}" class="{!! (in_array($L4page->id, $PATH_IDS) ? 'L4LinkActive' : 'L4Link') !!}">{!! $L4page->title !!}</a>
											@if( ($items+= $increase) && $increase = ceil(mb_strlen(html_entity_decode($L4page->title, ENT_NOQUOTES, 'UTF-8'))/$SETTINGS['lineSize4']))
											@endif
										@endif
									@endforeach
								@endif
							@endif
						@endforeach
						</div></td></tr></table></div>
					@endif --}}
{{--				</div> --}}
			@endif
		@endforeach
    </ul>
</div>
@endif

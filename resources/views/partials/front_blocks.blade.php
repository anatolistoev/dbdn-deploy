<div class="frontblock_col slider_element">

    <h2 class="homeblock_title">{!!$TITLE!!}</h2>

    <?php $index = 1 ?>

    @foreach($BLOCKS as $key => $row)

        @if( 1 == $index)
            <a href="{!! url($row->slug != '' ? $row->slug : $row->id) !!}" class="exp page_link {!!($row->template == 'downloads')?' download':''!!}">

            @if(isset($row['overview']) && $row['overview']==1) <div class='gallery_image'>@endif
                <div class="slider-image">
                    @if(isset($row['protected']) && $row['protected']==true)<span class="related-protected"></span>@endif
                    @if($row['file_type'] == 'downloads')
                    <div class="download_hover">
                        <div onclick='window.location.href="{!! asset($row->slug) !!}"' class="info_wrapper view_wrapper">
                            <div class="icon"></div>
                            <span>@lang('system.download_overview.goTo')</span>
                        </div>
                        @if(Auth::check() && isset($row->file_id))
                            @if($row->is_file_added_cart || Auth::user()->isPageAuth($row->id))
                                <div onclick="return false;" class="info_wrapper cart_wrapper">
                                    <div class="icon"></div>
                                    <span>@lang('system.downloads_list.added')</span>
                                </div>
                            @else
                                <div @if($row->has_access) onclick="addToCart({!!$row->file_id!!}, event)" @endif class="info_wrapper cart_wrapper">
                                    <div class="icon"></div>
                                    <span>@lang('system.download_overview.addToCart')</span>
                                </div>
                            @endif
                            <div @if($row->has_access) onclick="return startDownload({!!$row->file_id!!}, event);"@endif class="info_wrapper download_wrapper">
                                @if($row->has_access)
                                    <input type="checkbox" id="file_{!! $row->file_id !!}" class="chb checkbox1 " value="{!!$row->file_id!!}" name="fid[]">
                                @endif
                                <div class="icon"></div><span>@lang('system.downloads_list.downloadNow')</span>
                            </div>
                        @endif

                        @if(isset($row->extension) && $row->size)
                        <div class="info_wrapper stats_wrapper">
                            <span>{!!$row->extension . ','!!}</span>
                            <span>{!!\App\Helpers\FileHelper::formatSize($row->size)!!}</span>
                        </div>
                        @endif
                    </div>
                    @endif
                    <?php 
                        // file_put_contents('/srv/www/dbdn-l55/storage/pdf/image_cache.log', var_export($row['img'],true)."\n",FILE_APPEND); 
                        $row['img']=str_replace('blurred_preview','small_carousel',$row['img']);
                    ?>
                    {!!$row['img']!!}

                    @if($row['protected'])
                    <div class="protected" title='@lang('template.protected')'></div>
                    @endif
                </div>
            @if(isset($row['overview']) && $row['overview']==1) </div>@endif
                <h3 class="title"><span>[{!! $row->brandName !!}] {!! $row->content_title !!}</span></h3>

                <div class="date">@if(Session::get('lang') == LANG_EN){!!$row->updated_at->formatLocalized('%B %d, %Y')!!}@else{!!$row->updated_at->formatLocalized('%d. %B %Y')!!}@endif</div>

                @if(count($row->tags) > 0 && $row->template != 'basic')
                    <div class="tags">{!!$row->tags[0]->name!!}</div>
                @endif
            </a>

        @elseif($index < 9)
            @if(isset($row['overview']) && $row['overview']==1 ) <div class='gallery'>@endif
            <a href="{!! url($row->slug != '' ? $row->slug : $row->id) !!}" class="clp page_link">
                @if(isset($row['protected']) && $row['protected']==true)<span class="related-protected"></span>@endif
                <span class="link-title">[{!! $row->brandName !!}] {!! $row->content_title !!}</span>
            </a>
            @if(isset($row['overview']) && $row['overview']==1) </div>@endif
        @else
        @endif

        <?php ++$index; ?>

    @endforeach

</div>

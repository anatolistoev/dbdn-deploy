<ul id="brandSubNav">

    @if(isset($SECTION_PID_INDEX, $L2ID, $SECTION_PID_INDEX[$L2ID], $PATH_IDS) && $L2ID > 1)

        @foreach($SECTION_PID_INDEX[$L2ID] as $L2page)

            @if(1 === $L2page->in_navigation && 'basic' !== $L2page->template)
                <li>
                    @if(in_array($L2page->id, $PATH_IDS))
                        <h3 class="currentBrandSubNav">
                    @else
                        <h3>
                    @endif
                        <a href="{!!$L2page->slug!!}">{!! $L2page->title !!}</a>

                        <span onclick="$(this).closest('li').toggleClass('active')"></span>
                    </h3>
                    <ul>
                        <li>
                            <a href="{!! asset($L2page->slug != '' ? $L2page->slug : $L2page->id) !!}?filter=none">
                                @lang('template.navigation._'.$L2page->template)
                            </a>
                        </li>
                        <li>
                            <a href="{!! asset($L2page->slug != '' ? $L2page->slug : $L2page->id) !!}?filter=time:desc">
                                @lang('template.navigation.newest')
                            </a>
                        </li>
                        <li>
                            <a href="{!! asset($L2page->slug != '' ? $L2page->slug : $L2page->id) !!}?filter=viewed:desc">
                                @lang('template.navigation.most_viewed')
                            </a>
                        </li>
                        <li>
                            <a href="{!! asset($L2page->slug != '' ? $L2page->slug : $L2page->id) !!}?filter=rating:desc">
                                @lang('template.navigation.most_rated')
                            </a>
                        </li>
                    </ul>
                </li>

            @elseif($L2page->in_navigation == 1)

                @if($BN = (object)$BRAND_NAV[$L2page->id])
                    {{-- not real "if", just assigning variable --}}
                @endif

                <li>

                    @foreach($BN->SECTION_PID_INDEX[$BN->L2ID] as $BN_L2page)

                        @if( $BN_L2page->in_navigation == 1 && in_array($BN_L2page->id, $BN->PATH_IDS) && isset($BN->SECTION_PID_INDEX[$BN_L2page->id]) && count($BN->SECTION_PID_INDEX[$BN_L2page->id]) > 0 )

                            @if(in_array($BN_L2page->id, $PATH_IDS))
                                <h3 class="currentBrandSubNav">
                            @else
                                <h3>
                            @endif
                                <a href="{!!$BN_L2page->slug!!}">{!! $BN_L2page->title !!}</a>

                                <span onclick="$(this).closest('li').toggleClass('active')"></span>
                            </h3>

                            <ul>

                                @foreach($BN->SECTION_PID_INDEX[$BN_L2page->id] as $L3page)
                                    @if($L3page->in_navigation == 1)

                                        @if(in_array($L3page->id, $PATH_IDS))
                                            <li class="currentBrandSubNav">
                                        @else
                                            <li>
                                        @endif

                                        @if(isset($BN->SECTION_PID_INDEX[$L3page->id]))
                                                <h4 onclick="$(this).closest('li').toggleClass('active')">
                                                    {!! $L3page->title !!}
                                                    <span></span>
                                                </h4>
                                                <ul>
                                                    @foreach($BN->SECTION_PID_INDEX[$L3page->id] as $L4page )
                                                        @if($L4page->in_navigation == 1)

                                                            @if(in_array($L4page->id, $PATH_IDS))
                                                                <li class="currentBrandSubNav">
                                                            @else
                                                                <li>
                                                            @endif
                                                                @if($L4page->authorization == 1)
                                                                    <span class="protected protected-menu-title protected-pl4" title="Non-public"></span>
                                                                @endif
                                                                <a href="{!! asset($L4page->slug != '' ? $L4page->slug : $L4page->id) !!}">{!! $L4page->title !!}</a>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                        @else
                                                @if($L3page->authorization == 1)
                                                    <span class="protected protected-menu-title protected-pl3" title="Non-public"></span>
                                                @endif
                                                <a href="{!! asset($L3page->slug != '' ? $L3page->slug : $L3page->id) !!}">{!! $L3page->title !!}</a>
                                        @endif

                                        </li>

                                    @endif
                                @endforeach

                            </ul>

                        @endif

                    @endforeach

                </li>

            @endif
        @endforeach
    @endif

</ul>

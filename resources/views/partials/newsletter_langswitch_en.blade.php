<!-- languages-en -->
<a name="en"></a>
<table class="wrapper" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
    <tr>
        <td>
            <table class="flexible" width="660" bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;">
                <tr>
                    <td style="padding:20px 20px 0;">
                        <table width="100%" cellpadding="0" cellspacing="0">
							<tr><td height="1" style="font-size:0; line-height:0;" bgcolor="#000000"></td></tr>
                            <tr><td height="20" style="font-size:0; line-height:0;" bgcolor="#FFFFFF"></td></tr>
                            <tr>
                                <td style="padding:0 20px;">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="60">
                                                <img src="https://designnavigator.daimler.com/newsletter/btn-03.png" style="vertical-align:top; font:10px/12px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000001; width:60px; height:25px;" width="60" height="25" alt="ENGLISH" />
                                            </td>
                                            <td width="20"></td>
                                            <td width="60">
                                                <a href="#de"><img src="https://designnavigator.daimler.com/newsletter/btn-04.png" style="vertical-align:top; font:10px/12px DaimlerCSRegular, Arial, Helvetica, sans-serif; color:#000001; width:60px; height:25px;" width="60" height="25" alt="DEUTSCH" /></a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- /languages-en -->

@foreach($SECTION_PID_INDEX[$L2ID] as $L2page)
    @if($L2page->in_navigation == 1 && in_array($L2page->id, $PATH_IDS))
        <span id="menu_close" class="popup-close"></span>

        @if(isset($SECTION_PID_INDEX[$L2page->id]) && sizeof($SECTION_PID_INDEX[$L2page->id]) > 0)

            <?php
                $fixedws = ' fixedws';
                foreach($SECTION_PID_INDEX[$L2page->id] as $L3page) {
                    if( $L3page->in_navigation == 1 && isset($SECTION_PID_INDEX[$L3page->id]) ) {
                        $fixedws = '';
                        break;
                    }
                }
            ?>


            <div class="content desktop cf{!!$fixedws!!}">

                @foreach($SECTION_PID_INDEX[$L2page->id] as $L3page)
                    @if($L3page->in_navigation == 1)
                        <ul class="multi_nav">
                            @if(isset($SECTION_PID_INDEX[$L3page->id]))
                                <li>
                                    <h3>
                                        @if($L3page->authorization == 1)
                                        <span class="protected protected-menu-title protected-pl3" title="Non-public"></span>
                                        @endif
                                        <a href="{!!$L2page->slug!!}#{!!\App\Helpers\UriHelper::converturi($L3page->title)!!}"
                                            onclick="return menu.resolve('h1#{!!\App\Helpers\UriHelper::converturi($L3page->title)!!}')">{!! $L3page->title !!}</a>
                                    </h3>
                                </li>

                                <?php $pageCnt = 0;?>

                                @foreach($SECTION_PID_INDEX[$L3page->id] as $L4page )

                                    @if($L4page->in_navigation == 1)

                                        <?php
                                            if(++$pageCnt > 10)
                                            {
                                                echo '</ul><ul class="multi_nav"><li><h3>&nbsp;</h3></li>';
                                                $pageCnt = 0;
                                            }
                                        ?>

                                        <li class="{!! (in_array($L4page->id, $PATH_IDS) ? 'active' : '') !!}">                                           
                                            @if($L4page->authorization == 1)
                                            <span class="protected protected-menu-title protected-pl4" title="Non-public"></span>
                                            @endif
                                            <a href="{!! asset($L4page->slug != '' ? $L4page->slug : $L4page->id) !!}">{!! $L4page->title !!}</a>
                                        </li>

                                    @endif
                                @endforeach
                            @else
                                <li>
                                    <h3>
                                        @if($L3page->authorization == 1)
                                        <span class="protected protected-menu-title protected-pl3" title="Non-public"></span>
                                        @endif
                                        <a href="{!! asset($L3page->slug != '' ? $L3page->slug : $L3page->id) !!}">{!! $L3page->title !!}</a>
                                    </h3>
                                </li>
                            @endif
                        </ul>
                    @endif
                @endforeach
                </div>
                 <div class="content tablet-only basic-menu cf{!!$fixedws!!}">
                @foreach($SECTION_PID_INDEX[$L2page->id] as $L3page)
                    @if($L3page->in_navigation == 1)
                        <ul class="multi_nav">
                            @if(isset($SECTION_PID_INDEX[$L3page->id]))
                                <li>
                                    <h3>
                                        @if($L3page->authorization == 1)
                                        <span class="protected protected-menu-title protected-pl3" title="Non-public"></span>
                                        @endif
                                        <a class="parent parent-page-{!!$L3page->id!!}" href="/{!!$L2page->slug!!}#{!!\App\Helpers\UriHelper::converturi($L3page->title)!!}"
                                            onclick="return menu.resolve('h1#{!!\App\Helpers\UriHelper::converturi($L3page->title)!!}')">{!! $L3page->title !!}</a>
                                    </h3>
                                </li>

                                <?php $pageIndex = 0;
                                $in_navigation = array_filter($SECTION_PID_INDEX[$L3page->id], function($obj){
                                        if (1 == $obj->in_navigation) {
                                            return true;
                                        }
                                        return false;
                                });
                                $pageCnt = count($in_navigation);
                                ?>

                                @foreach($in_navigation as $L4page )

                                        <?php
                                            ++$pageIndex;
                                            if($pageCnt > 2 && $pageIndex > ceil($pageCnt / 2))
                                            {
                                                echo '</ul><ul class="multi_nav"><li><h3>&nbsp;</h3></li>';
                                                $pageIndex = 0;
                                            }
                                        ?>

                                        <li class="{!! (in_array($L4page->id, $PATH_IDS) ? 'active' : '') !!}">
                                            @if($L4page->authorization == 1)
                                            <span class="protected protected-menu-title protected-pl4" title="Non-public"></span>
                                            @endif
                                            <a data-parent="parent-page-{!!$L3page->id!!}" href="{!! asset($L4page->slug != '' ? $L4page->slug : $L4page->id) !!}">{!! $L4page->title !!}</a>
                                        </li>
                                        @if($pageCnt < 3 && $pageIndex == $pageCnt)
                                            </ul><ul class="multi_nav empty"><li><h3>&nbsp;</h3></li>
                                        @endif
                                @endforeach
                            @else
                                <li>
                                    <h3>
                                        @if($L3page->authorization == 1)
                                        <span class="protected protected-menu-title protected-pl3" title="Non-public"></span>
                                        @endif
                                        <a href="{!! asset($L3page->slug != '' ? $L3page->slug : $L3page->id) !!}">{!! $L3page->title !!}</a>
                                    </h3>
                                </li>
                            @endif
                        </ul>
                    @endif
                @endforeach
            </div>
        @endif
        @if(isset($MENUBLOCKS) && count($MENUBLOCKS)> 0)
            <div id='menu_carousel_wrapper'>
                <div class="menu_carousel">
                    <div class="slider_title">@lang('template.recommended')</div>
                    <div class="slider_container">
                        <a class="prev slider_hover" @if(!isset($MENUBLOCKS) || count($MENUBLOCKS) < 5) style="display:none"@endif></a>
                        <a class="next slider_hover" @if(!isset($MENUBLOCKS) || count($MENUBLOCKS) < 5) style="display:none"@endif></a>
                        <div class="cf" id="menu_slider">
                            <ul>
                                <?php $index = 1 ?>
                                <li index="{!!$index!!}">
                                    @foreach($MENUBLOCKS as $key=>$row)
                                    <div class="slider_element">
                                        <a href="{!! url($row['slug'] != '' ? $row['slug'] : $row['u_id']) !!}" class="page_link">
                                            <div class="slider-image">
                                                @if(strtolower(pathinfo($row['img'], PATHINFO_EXTENSION)) == 'svg')
                                                    {!!\App\Helpers\FileHelper::getSVGContent(public_path().$row['img']);!!}
                                                @else
                                                    {!!str_replace('blurred_preview','small_carousel',\App\Helpers\HtmlHelper::generateImg($row['img'], $row['type'], 'menu_carousel', isset($row['imgext'])? $row['imgext'] : \App\Helpers\FileHelper::getFileExtension($row['img']), isset($row['thumb_visible']), array("max-height"=> "146px", "max-width" => "220px")))!!}
                                                @endif

                                                @if(isset($row['protected']) && $row['protected'])
                                                <div class="protected" title='@lang('template.protected')'></div>
                                                @endif
                                            </div>
                                            <div class="title"><span>{!! $row['title'] !!}</span></div>
                                        </a>
                                    </div>
                                    @if($index % 4 == 0 && $index < count($MENUBLOCKS))
                                </li>
                                <li index="{!!$index/4 +1!!}">
                                    @endif
                                    <?php $index++; ?>
                                    @endforeach
                                </li>
                            </ul>
                        </div>
                        <ul class="pages slider_hover"></ul>
                    </div>
                </div>
            </div>
        @endif
    @endif
@endforeach

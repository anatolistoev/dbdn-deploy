<div id="tools">
	<div class="RSTitle">Tools</div>
	<a href="javascript:;" id="print" onclick="window.open('{!! asset('print/'. (sizeof(Request::segments()) ? implode('/', Request::segments()) : $PAGE->id )) !!}','','width=650, height=550, resizable=yes, location=no, menubar=no,toolbar=yes,scrollbars=yes',true);">@lang('template.print page')</a>
	{{--<a href="{!!asset(1742, array('pageID'=>$PAGE->id))!!}" id="recommend">@lang('template.send page')</a>--}}
	@if(!Request::is('feedback/*'))
		<a href="{!! asset('feedback/'.$PAGE->id) !!}" class="feedback">@lang('template.send feedback')</a>
	@else
		<a href="{!! Request::url() !!}" class="helpActive">@lang('template.send feedback')</a>
	@endif
	@if(Auth::check() && is_numeric($PAGE->id))
		<a href="javascript:;" id="rate" onclick="$('#ratingDiv, #alertsBG').fadeIn(300); $('#alertsBG').on('click', function(event){$('#ratingDiv, #alertsBG').fadeOut(300).off( event );});">@lang('template.rate page')</a>
	@endif
	@if(Auth::check() && is_numeric($PAGE->id) && $PAGE->id > 1 && (!isset($SUBSCRIBED) || $SUBSCRIBED == 0)  )
		{!! Form::open(array('url' => 'Watchlist', 'id' => 'watchlistForm', 'method'=>'put')) !!}
		{!! Form::hidden('pageid', $PAGE->id) !!}
		<a href="javascript:;" id="store" onclick="document.getElementById('watchlistForm').submit();">@lang('template.addwatch')</a>
		{!! Form::close() !!}
	@endif
</div>
<div id="help">
	<div class="RSTitle">@lang('template.help')</div>
	<a href="{!!asset(1689)!!}" id="faq" class="{!! $PAGE->id == 1689 ? 'helpActive' : 'faq' !!}">@lang('template.faq')</a>
	<a href="{!!asset(1478)!!}" id="tips" class="{!! $PAGE->id == 1478 ? 'helpActive' : 'tips' !!}">@lang('template.tips')</a>
	<a href="{!!asset('glossary')!!}" id="glossaryA" class="{!! $PAGE->id == 'glossary' ? 'helpActive' : 'glossaryA' !!}">@lang('template.glossary')</a>
	<a href="{!!asset('sitemap')!!}" id="sitemap" class="{!! $PAGE->id == 'sitemap' ? 'helpActive' : 'sitemap' !!}">@lang('template.sitemap')</a>
	<a href="{!!asset('index')!!}" id="index" class="{!! $PAGE->id == 'index' ? 'helpActive' : 'index' !!}">@lang('template.index')</a>
</div>
<style type="text/css">
    div.overview div.page.idx1 {margin-right:20px;}
    div.overview .slider-item > div.page {min-height:630px;}

    div.blocks_wrap {width:1180px;margin:0px auto;}

    section.section_content {min-height:0;}
    section.section_content h2 {width:1108px;margin:0px auto 40px;font:normal 24px/30px "DaimlerCS-Demi", "Arial", "Helvetica Neue", "Helvetica", sans-serif;}

    section.section_content div.list-view {margin-bottom:70px;}
    section.section_content div.list-view div.page:hover div.title {width:69%;}

    section.mostrecentblocks {margin-top:28px;}
    div.heading-row h1 {margin-top:20px;}
    h1.single {margin-top:153px;}
</style>

@if( isset($MOSTRECENTBLOCKS) && count($MOSTRECENTBLOCKS) )
    <section class="mostrecentblocks">
        @include('partials.mostrecentblocks')
        <div style="clear:both;"></div>
    </section>
@elseif( isset($MOSTSOMETHING) && count($MOSTSOMETHING) )
    <div class="mostsomething-shifter" style="clear:both;height:83px;"></div>
@endif

@if( isset($MOSTSOMETHING) && count($MOSTSOMETHING) )
    @foreach($MOSTSOMETHING as $MOSTSOMETHINGTITLE => $MOSTSOMETHINGBLOCKS)
        @if( count($MOSTSOMETHINGBLOCKS) )
            <section class="mostsomethingblocks" style="margin-bottom:5px;">
                @include('partials.mostSomethingBlocks')
                <div style="clear:both;"></div>
            </section>
        @endif
    @endforeach
@endif


@if(isset($BLOCKS) && sizeof($BLOCKS) > 0)


    <?php
        if(
            ( !isset($MOSTRECENTBLOCKS) || 0 === count($MOSTRECENTBLOCKS) )
            &&
            ( !isset($MOSTSOMETHING) || 0 === count($MOSTSOMETHING) )
        ) {
            // no top blocks
            $listView = false;
            $noTopBlocks = true;
        } else {
            // there are top blocks
            $listView = MOBILE ? true : false;
            $noTopBlocks = false;
        }
    ?>


    <div style="float:left;clear:both;width:100%;height:68px;"></div>
    <section class="section_content simple">
    <div class="blocks_wrap @if($listView) bwlist @else bwgrid @endif">

        @include('partials.view_controls')

        @if(!empty($ALLBLOCKSTITLE))
            <h1 class="hct">{!! \App\Helpers\HtmlHelper::formatTitle($ALLBLOCKSTITLE, $PAGE->brand) !!}</h1>
        @elseif( 1 == count($BLOCKS) && ($tmp = array_keys($BLOCKS)) )
            <h1 class="single">
                @if(2090 == $PAGE->id)
                    ‡ {!! trim($tmp[0]) !!}.
                @else
                    {!! $tmp[0] !!}
                @endif
            </h1>
        @endif


        @foreach($BLOCKS as $key => $block_group)
            <section class="section_content">

                @if( count($BLOCKS) > 1 )
                    <h2 id="{!! preg_replace('/\W+/','-',strtolower(trim($key, '‡ .'))) !!}" class="block_title">{!! trim($key, '‡ .') !!}</h2>
                @endif
                <div class="grid-page grid-4cols @if($listView) list-view @endif">
                    @foreach($block_group as $block_group_key => $block)
                        <div class="page">
                            <a class="page_link no-list-hover" href="{!! url($block['slug']) !!}">
                                <div class="image story-layover">
                                    {!!\App\Helpers\HtmlHelper::generateImg($block['img'], isset($block['type']) ? $block['type'] : \App\Http\Controllers\FileController::MODE_IMAGES, 'overview', isset($block['imgext'])? $block['imgext'] : \App\Helpers\FileHelper::getFileExtension($block['img']), FALSE, array("border"=> "0"))!!}

                                    @if(isset($block['protected']) && $block['protected'])
                                        <div class="protected" title='@lang('template.protected')'></div>
                                    @endif
                                </div>

                                <div class="title">
                                    @if(isset($block['protected']) && $block['protected'])
                                        <div class="protected" title='@lang('template.protected')'></div>
                                    @endif
                                    <span>{!! $block['title'] !!}</span>
                                </div>
                                <div class="date">@if(Session::get('lang') == LANG_EN){!!$block['date']->formatLocalized('%B %d, %Y')!!}@else{!!$block['date']->formatLocalized('%d. %B %Y')!!}@endif</div>
                                {{--
                                <!-- Dusty: following is not required, but good to have here ;) -->
                                <div class="stats">
                                    <span class="rate">{!! $block['ratings'] !!}</span>
                                    <span class="comments">{!! $block['comments'] !!}</span>
                                    <span class="views">{!! $block['visits'] !!}</span>
                                </div>
                                --}}
                                @if(!empty($block['tags']))
                                    <div class="tags">{!! $block['tags'] !!}</div>
                                @endif
                            </a>
                        </div>
                        @if(0 === ++$block_group_key % 4 && !MOBILE)
                            <div class="cf"></div>
                        @endif
                    @endforeach
                </div>
            </section>
        @endforeach
    </div>
    </section>
@else
    <section class="section_content"></section>
@endif

@if($PAGE->id == 1717)
    @include('partials.guided_tour', array('tour_page'=>'identity', 'from' => 10, 'to' => 11))
@elseif($PAGE->id == 2094)
    @include('partials.guided_tour', array('tour_page'=>'manuals', 'from' => 11, 'to' => 12))
@endif

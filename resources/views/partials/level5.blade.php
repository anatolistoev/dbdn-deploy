@if( isset($PAGE, $PAGE->parent_id, $ASCENDANT_ID_INDEX[$PAGE->parent_id], $SECTION_PID_INDEX) && sizeof($SECTION_PID_INDEX) && ($dad = $ASCENDANT_ID_INDEX[$PAGE->parent_id]) &&
	($LEVEL == 4 && isset($SECTION_PID_INDEX[$PAGE->id]) ||
	$LEVEL == 5 && isset($SECTION_PID_INDEX[$PAGE->parent_id]))
	)
		<div id="L5">
		@if($LEVEL == 4)
            {{--<xsl:value-of select="//pages/stdClass[currID = id]/name" disable-output-escaping="yes" />--}}
			<div class="RSTitle">{!! $CONTENTS['title'] !!}</div>
			{{-- L5FromL4 --}}
			@foreach($SECTION_PID_INDEX[$PAGE->id] as $child)
				<a href="{!! asset($child->slug!='' ? $child->slug : $child->id) !!}" {!! ($child->id == $PAGE->id ? 'class="L5Active"' : '' ) !!}>{!! $child->title !!}</a>
			@endforeach
        @else
            {{--<xsl:value-of select="//pages/stdClass[child::id = //stdClass[currID = id]/parentID and child::active = 'Yes']/name" disable-output-escaping="yes" />--}}
			<div class="RSTitle">{!! $dad->title !!}</div>
			{{-- L5FromL5 --}}
			@foreach($SECTION_PID_INDEX[$PAGE->parent_id] as $child)
				<a href="{!! asset($child->slug!='' ? $child->slug : $child->id) !!}" {!! ($child->id == $PAGE->id ? 'class="L5Active"' : '' ) !!}>{!! $child->title !!}</a>
			@endforeach
        @endif
	</div><!-- L5 -->
@endif
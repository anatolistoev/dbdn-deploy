@if($desktop_image)
    <div class="banner_load_wrapper @if($tablet_image) desktop_banner @endif">
        @if($desktop_image['hasDescription'])
            <div class="coverBackground"></div>
        @endif
        <img class="{!!$desktop_image['class']!!} banner lazyload-thumb" src="{!!\App\Helpers\UriHelper::getBlurredPreviewUrl($desktop_image['data_original'])!!}" alt="{!!$desktop_image['alt']!!}" data-original ="{!!$desktop_image['data_original']!!}" data-gallery="{!!$desktop_image['data_gallery']!!}" >
        @if($desktop_image['hasDescription'])
            <div class="background"></div>
            <div class="text">
                {!!$desktop_image['description']!!}
            </div>
        @endif
        <div class="linkDown"></div>
    </div>
@endif
@if($tablet_image)
    <div class="banner_load_wrapper tablet_banner">
        @if($tablet_image['hasDescription'])
            <div class="coverBackground"></div>
        @endif
        <img class="{!!$tablet_image['class']!!} banner lazyload-thumb" src="{!!\App\Helpers\UriHelper::getBlurredPreviewUrl($tablet_image['data_original'])!!}" alt="{!!$tablet_image['alt']!!}" data-original ="{!!$tablet_image['data_original']!!}" data-gallery="{!!$tablet_image['data_gallery']!!}" >
        @if($tablet_image['hasDescription'])
            <div class="background"></div>
            <div class="text">
                {!!$tablet_image['description']!!}
            </div>
        @endif
        <div class="linkDown"></div>
    </div>
@endif
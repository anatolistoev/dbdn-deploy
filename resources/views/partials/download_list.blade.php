@if($DOWNLOAD_FILES != NULL && $DOWNLOAD_FILES->count() )
<div class="pagination"></div>
<div id="downloads">

{{-- id	filename	size	extension	thumbnail	zoompic		category_id	software	lang	dimensions	dimension_units	format	resolution	resolution_units	color_mode	pages	version	brand_id	created_at	updated_at
	 id	file_id		title	description	lang		created_at	updated_at --}}
	{!! Form::open(array('url' => url('download'), 'id' => 'downloadsForm', 'method'=>'post')) !!}
	{!! Form::hidden('_method', 'post') !!}
    <?php $isPrintView = isset($PRINT_VIEW) && $PRINT_VIEW == 1; ?>

    @if(!$isPrintView)
	<div id="parent_path" style="display:none">
        @if(isset($SECTION_PID_INDEX[$L2ID]))
            @foreach($SECTION_PID_INDEX[$L2ID] as $L2page)
                @if(in_array($L2page->id, $PATH_IDS))
                    {!!url($L2page->slug)!!}
                @endif
            @endforeach
        @endif
        </div>
    @endif
	@foreach($DOWNLOAD_FILES as $key=>$download)
	<div class="downloadDiv">
		@if (empty($download->file))
		<div class="downloadCol1">
		</div>
		<div class="downloadCol2">
			<!-- <div class="text-item">@lang('system.download_na')</div> -->
		</div>
		@else
		{!! Form::hidden('fid[]', $download->file->id) !!}
		<div class="downloadCol1">
			<div class="image">
				<?php $ext = explode(',',$download->file->extension);?>
				@if($download->file->hasThumbSourceFile())
					<a href="{!! asset($download->file->getThumbPath()) !!}" rel="lytebox" class="hasZoom image_layover" data-title="{!!$download->file->info->first()->description!!}" title="{!!preg_replace(array('/<\s*br\s*\/?\s*>|<\s*\/p\s*>/i', '/&nbsp;/i', '/<\s*h1\s*>.*<\s*\/\s*h1\s*>|<[^>]+>/i'), array('&#13', ' ', ''),$download->file->info->first()->description)!!}">
						{!!\App\Helpers\HtmlHelper::generateImg($download->file->getCachedThumbPath('content'), \App\Http\Controllers\FileController::MODE_DOWNLOADS, 'content', \App\Helpers\FileHelper::getFileExtension($download->file->getThumbPath()), $download->file->protected, NULL, 2, $isPrintView)!!}
					</a>
				@elseif(is_file(public_path().'/img/template/svg/file_type/black/'.$ext[0].'.svg'))
					{!!\App\Helpers\FileHelper::getSVGContent(public_path().'/img/template/svg/file_type/black/'.$ext[0].'.svg')!!}
				@else
					{!!\App\Helpers\HtmlHelper::generateImg($DEFAULT_THUMBNAIL, \App\Helpers\FileHelper::MODE_BRAND_THUMB, 'content', \App\Helpers\FileHelper::getFileExtension($DEFAULT_THUMBNAIL),FALSE, array('max-height'=> '320px', 'max-width'=> '480px'))!!}
				@endif
			</div>
			<div class="link">
				<?php 
                if (trim($download->file->info->first()->title)=='') {
                    $parts = explode('/',$download->file->filename); echo end($parts);
                } else {
                    echo($download->file->info->first()->title);
                }
                ?>
			</div>
			<div class="date">@if(Session::get('lang') == LANG_EN){!!$download->file->updated_at->formatLocalized('%B %d, %Y')!!}@else{!!$download->file->updated_at->formatLocalized('%d. %B %Y')!!}@endif</div>
			@if($PAGE->joinTags())
			<div class="tags"><img src="{!! asset('/img/template/svg/tags.svg') !!}" /><span>{!!$PAGE->joinTags()!!}</span></div>
			@endif
			@if(!$isPrintView)
			<div class="actions cf" id="download_actions">
				@if(Auth::check() && (isset($download->file->carts()->where('active',1)->whereNull('deleted_at')->where('user_id', Auth::user()->id)->first()->file_id)|| Auth::user()->isPageAuth($download->page_id)))
				<a class="cart not-active" href="#" onClick="return false;">@lang('system.downloads_list.added')</a>
				@elseif(Auth::check())
				<a class="cart" href="#" onclick="addToCart({!! $download->file->id !!}, event)"> @if(TABLET){!!trans('system.downloads_list.addToCart.tablet')!!}@else{!!trans('system.downloads_list.addToCart')!!}@endif</a>
				@endif
				<a class="now" href="{!! url($download->file->getPath()) !!}" onClick="return startDownload();">@lang('system.downloads_list.downloadNow')</a>
			</div>
			@endif
		</div>
		<div class="downloadCol2">
			@if(!is_file(public_path().'/'.$download->file->getPath(FILEITEM_REAL_PATH)))
				<div class="text-item">@lang('system.download_na')</div>
			@else
				@if(strlen($download->file->info->first()->description) > 1)
					<div class="text-item">
						<label>@lang('system.downloads_list.description'):</label>{!!$download->file->info->first()->description!!}
					</div>
				@endif
				@if(!is_null($PAGE->usage) && $PAGE->usage != '' &&  $PAGE->usage != 1)
					<div class="table-item first" id="downloadCol2"><label>@lang('system.downloads_list.usage'):</label>@lang('ws_general_controller.page_usage_'.\App\Helpers\UserAccessHelper::getPageConfiguration('usage',$PAGE->usage))</div>
				@endif
				@if(!is_null($PAGE->file_type) && $PAGE->file_type != '' && $PAGE->file_type != 1)
					<div class="table-item"><label>@lang('system.downloads_list.file_type'):</label>@lang('ws_general_controller.page_file_type_'.\App\Helpers\UserAccessHelper::getPageConfiguration('file_type',$PAGE->file_type))</div>
				@endif
				@if($download->file->extension != '')
					<div class="table-item" id="downloadCol2"><label>@lang('system.downloads_list.fileformat'):</label>{!!strtoupper($download->file->extension)!!}</div>
				@endif
				@if($download->file->size != '')
				<div class="table-item"><label>@lang('system.downloads_list.size'):</label>{!!\App\Helpers\FileHelper::formatSize($download->file->size)!!}</div>
				@endif
				@if($download->file->format != '' && $download->file->format != 0)
					<div class="table-item"><label>@lang('system.downloads_list.format'):</label>{!!\App\Helpers\UserAccessHelper::getPageConfiguration('file_format',$download->file->format)!!}</div>
				@endif
				@if( $download->file->dimensions != '')
					<div class="table-item"><label>@lang('system.downloads_list.dimensions'):</label>{!!$download->file->dimensions.' '.\App\Helpers\UserAccessHelper::getPageConfiguration('file_dimension',$download->file->dimension_units)!!}</div>
				@endif
				@if( $download->file->resolution != '')
					<div class="table-item"><label>@lang('system.downloads_list.resolution'):</label>{!!$download->file->resolution.' '.\App\Helpers\UserAccessHelper::getPageConfiguration('file_resolution', $download->file->resolution_units)!!}</div>
				@endif
				@if($download->file->color_mode != '' && $download->file->color_mode != 0)
					<div class="table-item"><label>@lang('system.downloads_list.color_mode'):</label>@lang('ws_general_controller.color_mode_' . \App\Helpers\UserAccessHelper::getPageConfiguration('file_color_mode', $download->file->color_mode))</div>
				@endif
				@if($download->file->lang != '')
					<div class="table-item"><label>@lang('system.downloads_list.lang'):</label>{!!$download->file->lang!!}</div>
				@endif
				@if($download->file->pages != '')
					<div class="table-item"><label>@lang('system.downloads_list.pages'):</label>{!!$download->file->pages!!}</div>
				@endif
				@if($download->file->version != '')
					<div class="table-item last"><label>@lang('system.downloads_list.version'):</label>{!!$download->file->version!!}</div>
				@endif
				@if($download->file->copyright_notes != '')
					<div class="text-item">
						<label>@lang('system.downloads_list.copyrights'):</label> {!!$download->file->copyright_notes!!}
					</div>
				@endif
				<?php
					if(Session::get('lang') == 2){
						$field = 'usage_terms_de';
					}else{
						$field = 'usage_terms_en';
					}
				?>
				@if($download->file->$field != '')
					<div class="text-item">
						<label>@lang('system.downloads_list.usage_terms'):</label> {!!$download->file->$field!!}
					</div>
				@endif
			@endif
		</div>
		<div style="clear: both;"></div>
		@endif
	</div>
	@endforeach
	{!! Form::close() !!}
</div><!-- downloads -->
@if($PAGE->template == 'downloads' && !$isPrintView)
    <div id="downloads_related">
    @include('partials.related_pages')
    </div>
@endif
<br>
@else
	{{-- trans('system.empty_list') --}}
@endif
@if($PAGE->slug == 'Daimler_Brochures_InDesign_DINA4_Templates')
      @include('partials.guided_tour', array('tour_page'=>'d_content', 'from' => 23, 'to' => 27))
@endif

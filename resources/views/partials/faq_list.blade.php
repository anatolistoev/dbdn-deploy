<ul>
	@foreach($FAQS as $key=>$faq_group)
	<li class="first-ul @if($key == $OPEN_GROUP) current @endif">
		@if(!$FILTERED_BLOCKS)
			<div class="category_title">
				{!!$key!!} <a class="open-icon-pluse"></a>
			</div>
		@endif
		<ul class="toggle"  @if($key == $OPEN_GROUP || $FILTERED_BLOCKS) style="display:block;" @endif >
			@foreach($faq_group as $faq)
			<li class="second-ul @if($faq->id == $FAQ_ID) opened @endif" faq_id="{!!$faq->id!!}" id="{!!\App\Helpers\UriHelper::converturi($faq->question)!!}">
				<a href="{!!url('/faq/'.((isset($faq->brand_id) && $faq->brand_id == SMART) ? 'smart' : 'daimler').'/'.$faq->id.'/'.\App\Helpers\UriHelper::converturi($faq->question))!!}" class="faq_title_row cf"  @if($faq->id == $FAQ_ID) style="display:none;" @endif>
					 <div class="faq_title">{!!$faq->question!!}</div>	<span class="open-icon-pluse-second-ul"></span>
					@if(Auth::check())<span class="vote-icon-front">{!!$faq->rate!!}</span>@endif
				</a>
				<div class="toggle-two"  @if($faq->id == $FAQ_ID) style="display:block;" @endif>
					 <div>
						<a class="close-toggle-two"></a>
						@if(Auth::check())<a href="#"><span class="vote-icon">{!!$faq->rate!!}</span></a>@endif
					</div>
					<div class="row" style="clear:both;">
						<div class="column">
							<div class="quetion-info">
								<p class="toggle-two-title">
									{!!$faq->question!!}
								</p>

								{!!$faq->answer!!}
							</div>
						</div>
						<?php $related = $faq->related_pages; ?>
						@if(is_array($related) && count($related) > 0)
						<div class="column right">
							<div class="related-links  ">
								<h2 class="related-links-title-form">@lang('template.faq.related_links')</h2>
								<ul>
									@foreach($related as $rel)
									<li> <a href="{!!url($rel->slug)!!}">{!!$rel->body!!}</a></li>
									@endforeach
								</ul>
							</div>
						</div>
						@endif
					</div>
					<div style="clear:both;"></div>
				</div>
			</li>

			@endforeach
		</ul>
	</li>
	@endforeach
</ul>
<section class="downloads_overview @if(MOBILE) bwlist @else bwgrid @endif @if( 1 == count($BLOCKS)) single_download @endif">
    {!! Form::open(array('url' => url('download'), 'id' => 'downloadsForm', 'method'=>'post')) !!}
    {!! Form::hidden('_method', 'post') !!}

    @if(isset($BLOCKS) && sizeof($BLOCKS) > 0)

        @include('partials.view_controls')


        @if(!empty($ALLBLOCKSTITLE) && $ALLBLOCKSTITLE = trim($ALLBLOCKSTITLE, '‡ .'))
            <h1 class="hct">{!! \App\Helpers\HtmlHelper::formatTitle($ALLBLOCKSTITLE, $PAGE->brand) !!}</h1>
        @elseif( 1 == count($BLOCKS) && ($ddd = array_keys($BLOCKS)) && ($ALLBLOCKSTITLE = $ddd[0]) )
            <h1 class="single" style="text-transform:none;">{!! $ALLBLOCKSTITLE !!}</h1>
        @endif


        @if($FILTERED_BLOCKS)
            <div class="view_controls_holder"></div>
        @endif

        @foreach($BLOCKS as $key => $block_group)

            @if(!$FILTERED_BLOCKS)

                @if(count($BLOCKS) > 1 || (count($BLOCKS) == 1 && $PAGE->type=='Folder'))
                    <h1 class="block_title" id="{!!\App\Helpers\UriHelper::converturi($key)!!}">
                        {!!$key!!}
                    </h1>
                @else
                <div style="width:100%;height:30px;float:none;clear:both;" class="first-row-info_shifter"></div>
                @endif

                <?php $first_value = reset($block_group);?>

                @if($first_value && isset($first_value['parent_slug']))
                    <a href="{!!url($first_value['parent_slug'])!!}">
                        @if(SMART === $PAGE->brand)
                            @lang('system.download_overview.goTo'.(isset($first_value['overview'])?'Lib':''))
                        @else
                            {!! strtoupper(trans('system.download_overview.goTo'.(isset($first_value['overview'])?'Lib':''))) !!}
                        @endif
                    </a>
                @endif
            @endif

            <div class="grid-page grid-4cols downloads @if(MOBILE) list-view @endif">
                <?php $i = 0; $count = 4;?>
                @foreach($block_group as $pageId=>$block)
                <?php if(isset($block['overview'])) $count= 3; ?>
                    <div class="page @if(isset($block['overview'])) tree_cols @endif">
                        <a class="page_link"  href="{!! url($block['slug']) !!}">
                            @if(isset($block['overview'])) <div class='image-wrapper'>@endif
                            <div class="image story-layover">
                                <div class="download_hover">
                                    <div onclick='window.location.href="{!! url($block['slug']) !!}"' class="info_wrapper view_wrapper"><div class="icon"></div><span>@lang('system.download_overview.goTo'.(isset($block['overview'])?'Lib':''))</span></div>
                                    @if(Auth::check() && isset($block['DOWNLOAD_FILES']) && $block['DOWNLOAD_FILES'] != NULL && $block['DOWNLOAD_FILES']->count() && $block['DOWNLOAD_FILES'][0]->file!=null)
                                        @if(isset($block['DOWNLOAD_FILES'][0]->file->carts()->where('active',1)->whereNull('deleted_at')->where('user_id', Auth::user()->id)->first()->file_id) || Auth::user()->isPageAuth($block['DOWNLOAD_FILES'][0]->page_id))
                                            <div onclick="return false;" class="info_wrapper cart_wrapper"><div class="icon"></div><span>@lang('system.downloads_list.added')</span></div>
                                        @else
                                            <div @if(isset($block['has_access']) && $block['has_access'])onclick="addToCart({!!$block['DOWNLOAD_FILES'][0]->file->id!!}, event)"@endif class="info_wrapper cart_wrapper"><div class="icon"></div><span>@lang('system.download_overview.addToCart')</span></div>
                                        @endif
                                        <div @if(isset($block['has_access']) && $block['has_access']) onclick="return startDownload({!!$block['DOWNLOAD_FILES'][0]->file->id!!}, event);"@endif class="info_wrapper download_wrapper">
                                            @if(isset($block['has_access']) && $block['has_access'])
                                                <input type="checkbox" id="file_{!! $block['DOWNLOAD_FILES'][0]->file->id !!}" class="chb checkbox1 " value="{!!$block['DOWNLOAD_FILES'][0]->file->id!!}" name="fid[]">
                                            @endif
                                            <div class="icon"></div><span>@lang('system.downloads_list.downloadNow')</span>
                                        </div>
                                    @endif
                                    @if(isset($block['extension']) && $block['size'] && !isset($block['overview']))
                                    <div class="info_wrapper stats_wrapper">
                                        <span>{!!$block['extension'] . ','!!}</span>
                                        <span>{!!\App\Helpers\FileHelper::formatSize($block['size'])!!}</span>
                                    </div>
                                    @elseif(isset($block['extension']) && $block['size'])
                                    <div class="info_wrapper ext_wrapper">
                                        <span>
                                        {!!$block['extension']!!}
                                        </span>
                                    </div>
                                    <div class="info_wrapper size_wrapper">
                                        <span>
                                        {!!\App\Helpers\FileHelper::formatSize($block['size'])!!}
                                        </span>
                                    </div>
                                    @endif
                                </div>
                                @if(strtolower(pathinfo($block['img'], PATHINFO_EXTENSION)) == 'svg')
                                    {!!\App\Helpers\FileHelper::getSVGContent(public_path().$block['img']);!!}
                                @else
                                    {!!\App\Helpers\HtmlHelper::generateImg($block['img'], $block['type'], isset($block['overview'])? 'lib_display': 'overview', isset($block['imgext'])? $block['imgext'] : \App\Helpers\FileHelper::getFileExtension($block['img']), isset($block['thumb_visible']), isset($block['overview'])?array("max-height"=> "120px", "max-width" => "180px") :array("max-height"=> "186px", "max-width" => "280px"))!!}
                                @endif
                                @if(isset($block['protected']) && $block['protected'])
                                    <div class="protected" title='@lang('template.protected')'></div>
                                @endif
                            </div>
                            @if(isset($block['overview'])) </div>@endif
                            @if(isset($block['overview'])) <div class='title-wrapper'>@endif
                            <div class="title">
                                @if(isset($block['protected']) && $block['protected'])
                                    <div class="protected" title='@lang('template.protected')'></div>
                                @endif
                                <span @if(isset($block['protected']) && $block['protected']) class='is_protected' @endif>{!! $block['title'] !!} @if(isset($block['overview'])&& isset($block['count']))[{!!$block['count']!!}]@endif</span>
                            </div>
                            <div class="date">@if(Session::get('lang') == LANG_EN){!!$block['date']->formatLocalized('%B %d, %Y')!!}@else{!!$block['date']->formatLocalized('%d. %B %Y')!!} @endif</div>
                            @if($block['tags'])
                                <div class="tags">{!! $block['tags'] !!}</div>
                            @endif
                            <div class="list-hover cf">
                                <div onclick='window.location.href="{!! url($block['slug']) !!}"' class="file_page">
                                    <div class="icon"></div>
                                    <span>@lang('system.download_overview.goTo'.(isset($block['overview'])?'Lib':''))</span>
                                </div>
                                @if(Auth::check() && isset($block['DOWNLOAD_FILES']) && $block['DOWNLOAD_FILES'] != NULL && $block['DOWNLOAD_FILES']->count() && $block['DOWNLOAD_FILES'][0]->file!=null)
                                    <div @if(isset($block['has_access']) && $block['has_access']) onclick="return startDownload({!!$block['DOWNLOAD_FILES'][0]->file->id!!}, event);"@endif class="download_wrapper">
                                        @if(isset($block['has_access']) && $block['has_access'])
                                            <input type="checkbox" id="file_{!! $block['DOWNLOAD_FILES'][0]->file->id !!}" class="chb checkbox1 " value="{!!$block['DOWNLOAD_FILES'][0]->file->id!!}" name="fid[]">
                                        @endif
                                        <div class="icon"></div>
                                        <span>@lang('system.downloads_list.downloadNow')</span>
                                    </div>
                                    @if(isset($block['DOWNLOAD_FILES'][0]->file->carts()->where('active',1)->whereNull('deleted_at')->where('user_id', Auth::user()->id)->first()->file_id) || Auth::user()->isPageAuth($block['DOWNLOAD_FILES'][0]->page_id))
                                        <div onclick="return false;" class="cart_wrapper">
                                            <div class="icon"></div>
                                            <span>@lang('system.downloads_list.added')</span>
                                        </div>
                                    @else
                                        <div @if(isset($block['has_access']) && $block['has_access'])onclick="addToCart({!!$block['DOWNLOAD_FILES'][0]->file->id!!}, event)"@endif class="cart_wrapper">
                                            <div class="icon"></div>
                                            <span>@lang('system.download_overview.addToCart')</span>
                                        </div>
                                    @endif
                                @endif
                                @if(isset($block['size']))
                                    <div class="file_exsize">
                                        {!!\App\Helpers\FileHelper::formatSize($block['size'])!!}
                                    </div>
                                    @endif
                                @if(isset($block['extension']))
                                    <div class="file_extension">
                                        {!!$block['extension']!!}
                                    </div>
                                @endif
                                </div>
                            @if(isset($block['overview'])) </div>@endif
                        </a>
                    </div>
                    <?php $i++; ?>
                    @if($i % $count == 0 && !MOBILE)
                        <div style="clear:both;"></div>
                    @endif
                @endforeach

            </div>
            <div style="clear:both;"></div>

        @endforeach
    @else
        <div class="grid-page grid-4cols downloads">
            <div class="no_results">{!! \App\Helpers\HtmlHelper::formatTitle(trans('template.no_results'), $PAGE->brand) !!}</div>
        </div>
    @endif

    {!! Form::close() !!}

</section>
<script>
    if($(window).width() > 767) {
        if($('.most_recent').length || $('.single_download').length > 0) {
            
        } else {
            $('.downloads_overview').css({
                'margin-top': '93px'
            })
        }
        if($('.downloads_overview .hct').next().hasClass('block_title')) {
            $('.downloads_overview .hct').css({
                'margin-bottom': '0px'
            }) 
        } else {
            $('.downloads_overview .hct').css({
                //'margin-bottom': '39px'
            })  
            $('#view-controls').css({
                'top': '33%'
            })
        }
    } else {
        if($('.most_recent').length || $('.single_download').length > 0) {
            
        } else {
            $('.downloads_overview').css({
                'margin-top': '189px'
            })
        }
        if($('.downloads_overview h1.single, .downloads_overview h1.hct').next().hasClass('block_title')) {
            $('.downloads_overview h1.single, .downloads_overview h1.hct').css({
                'margin-bottom': '0px'
            }) 
        } else {
            $('.downloads_overview h1.single, .downloads_overview h1.hct').css({
                'margin-bottom': '39px'
            })  
            $('#view-controls').css({
                'top': '33%'
            })
        }
    }
</script>

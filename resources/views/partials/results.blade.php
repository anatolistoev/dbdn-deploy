<script type="text/javascript" src="{!!mix('/js/jquery.simplePagination.js')!!}"></script>
<section>
	<p class="glossary-search-title">
        <span style="display:inline-block;padding-bottom:10px;">@lang('search.page.title')</span>
        @if(isset($Q) && trim($Q) !="")
            <span class="searched-tag">{!! $Q !!}</span>
            <span> [{!!sizeof($ROWS)!!}]</span>
        @else
            <span>&nbsp;</span>
        @endif
    </p>
	@if(!$FILTERED)
	<p class="sub-title">
        @lang('search.note')
    </p>
	@endif

	@if(sizeof($ROWS))
	<ul class="search-results">
        <?php $index = 1 ?>
        <li index="{!!$index!!}" class="active">
		@foreach($ROWS as $row)
            <div class="search-description-field">
                <p class="path">{!!$row['path']!!}</p>
                <div class="title">
                    @if(isset($row['protected']) && $row['protected'])
                        <div class="protected" title='@lang('template.protected')'></div>
                    @endif
                    <p class="description-title"><a href="{!!url($row['slug'])!!}">{!!$row['title'] !!}</a></p>
                </div>
                <div class="description-paragraph">
                    {!! \App\Helpers\HtmlHelper::fixDoubleDaggerForDaimler($row['text']) !!}
                </div>
                <div class="cf">
                    <span class="updated_at">
                        {!!$row['updated_at']->formatLocalized((Session::get('lang') == LANG_EN)? '%B %d, %Y': '%d. %B %Y')!!}
                    </span>
                    <div class="stats_wrapper">
                        @if($row['rating'] != 0)
                            <span class="rating">{!!$row['rating']!!}</span>
                        @endif
                        <span class="visits">{!!$row['visits']!!}</span>
                    </div>
                </div>
                <div class="tags">{!!$row['tags']!!}</div>
            </div>
            @if($index % 10 == 0 && $index < count($ROWS))
                </li>
                <li index="{!!$index/10 +1!!}" class="hide_result">
            @endif
            <?php $index++; ?>
		@endforeach
        </li>
	</ul>
    @if(count($ROWS) > 10)
        <ul class="search_pagination"></ul>
        <script>
        $(function() {
            <?php $length = ceil(count($ROWS) / SEARCH_RESULT_BY_PAGE); ?>
            $('.search_pagination').pagination({
                pages:{!!$length!!},
                prevText:'<',
                nextText:'>',
                edges:1,
                displayedPages:10,
                onPageClick:function(pageNumber, event){
                    $(".search-results li.active").addClass('hide_result');
                    $(".search-results li.active").removeClass('active');
                    $(".search-results li[index='"+ pageNumber +"']" ).addClass('active');
                    $(".search-results li[index='"+ pageNumber +"']" ).removeClass('hide_result');
                    $(window).scrollTop(0);
                    return false;
                }
            });
        });
    </script>
    @endif
	@endif
</section>

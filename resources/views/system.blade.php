@extends('layouts.base')
@section('bodyClass'){!! 'basic' !!}@stop
@section('bullet_nav')@overwrite
@section('main')
	<section>
		<div class="user_profile_fields">
			<h1>@lang('system.'.$PAGE->id.'.page.title')</h1>
			<h1><small>@lang('system.'.$PAGE->id.'.subheading')</small></h1>
			<p>@lang('system.'.$PAGE->id.'.contents')</p>
		</div>
	</section>
@stop


<div  class="@if($PAGE->template != 'basic') two_cols @else three_cols @endif">
    <div class="RSTitle">Related Links</div>
    <div id='related_page' class="content_related viewable">
        <div class="modify_overlay" style="display:none;"></div>
        <div class="modify_wrapper" style="display:none"><a class="nav_box white edit">@lang('backend_pages.edit')</a></div>
        <div id="content_related_text">
            <?php $endRelated =  end($RELATED_PAGES); $startRelated = reset($RELATED_PAGES);?>
            @foreach($RELATED_PAGES as $rp)
            <div class="related_record cf" data-u_id="{!!$rp->u_id !!}">
                <input type="checkbox" id="chk_{!!$rp->id !!}" name="{!!$rp->id !!}" @if($rp->is_hidden)data-checked = false @else checked data-checked = true @endif data-position={!!$rp->position!!}>
                <label for="chk_{!!$rp->id !!}"> [{!!$rp->brand_title!!}]{!!$rp->body!!}</label>
<!--                <div class="related_buttons cf">
                        <a href="" class="modify_links move_up" @if($startRelated == $rp)style="visibility:hidden;" @endif></a>
                        <a href="" class="modify_links move_down" @if($endRelated == $rp)style="visibility:hidden;" @endif ></a>
                </div>-->
            </div>
            @endforeach
        </div>
        <div class="modify_buttons cf">
            <a href="" class="modify_links order"></a>
            <a href="" class="modify_links disable"></a>
            <a href="" class="modify_links back_to_view"></a>
        </div>
    </div>
</div>
@if($PAGE->template == 'basic')
<div  class="three_cols">
    <div class="RSTitle">Related Best Practice</div>
    <div id='related_BP' class="content_related viewable">
        <div class="modify_overlay" style="display:none;"></div>
        <div class="modify_wrapper" style="display:none"><a class="nav_box white edit">@lang('backend_pages.edit')</a></div>
        <div id="content_bp_text">
            <?php $endRelated =  end($RELATED_BP); $startRelated = reset($RELATED_BP);?>
            @foreach($RELATED_BP as $rp)
            <div class="related_record cf" data-u_id="{!!$rp->u_id !!}">
                <input type="checkbox" id="chk_{!!$rp->id !!}" name="{!!$rp->id !!}" @if($rp->is_hidden)data-checked = false @else checked data-checked = true @endif data-position={!!$rp->position!!}>
                <label for="chk_{!!$rp->id !!}"> [{!!$rp->brand_title!!}]{!!$rp->body!!}</label>
<!--                <div class="related_buttons cf">
                        <a href="" class="modify_links move_up" @if($startRelated == $rp)style="visibility:hidden;" @endif></a>
                        <a href="" class="modify_links move_down" @if($endRelated == $rp)style="visibility:hidden;" @endif ></a>
                </div>-->
            </div>
            @endforeach
        </div>
        <div class="modify_buttons cf">
            <a href="" class="modify_links order"></a>
            <a href="" class="modify_links disable"></a>
            <a href="" class="modify_links back_to_view"></a>
        </div>
    </div>
</div>
@endif
@if($PAGE->template != 'best_practice')
<div class="@if($PAGE->template != 'basic') two_cols @else three_cols @endif">
    <div class="RSTitle">Related Downloads</div>
    <div id='related_downlowds' class="content_related viewable">
        <div class="modify_overlay" style="display:none;"></div>
        <div class="modify_wrapper" style="display:none"><a class="nav_box white edit">@lang('backend_pages.edit')</a></div>
        <div id="content_download_text">
            <?php $endDownload =  end($RELATED_DOWNLOADS); $startDownload = reset($RELATED_DOWNLOADS);?>
            @foreach($RELATED_DOWNLOADS as $rd)
                <div class="related_record cf" data-u_id="{!!$rd->u_id !!}">
                        <input type="checkbox" id="chk_{!!$rd->id !!}" name="{!!$rd->id !!}" @if($rd->is_hidden)data-checked = false @else checked data-checked = true @endif data-position="{!!$rd->position!!}">
                        <label for="chk_{!!$rd->id !!}"> [{!!$rd->brand_title!!}]{!!$rd->body!!}</label>
<!--                    <div class="related_buttons cf">
                        <a href="" class="modify_links move_up" @if($startDownload == $rd)style="visibility:hidden;" @endif></a>
                        <a href="" class="modify_links move_down" @if($endDownload == $rd)style="visibility:hidden;" @endif ></a>
                    </div>-->
                </div>
            @endforeach
        </div>
        <div class="modify_buttons cf">
            <a href="" class="modify_links order" ></a>
            <a href="" class="modify_links disable"></a>
            <a href="" class="modify_links back_to_view"></a>
        </div>
    </div>
</div>
@endif
<link rel="stylesheet" href="{!! url('/js/tinymce_4.1.7/skins/dbdnskin/skin.min.css')!!}" />
<link rel="stylesheet" type="text/css" href="{!! url('/js/jquery.filetree/jqueryFileTree.css')!!}" />
<script type="text/javascript" src="{!! url('/js/jQuery-File-Upload-9.10.6/js/jquery.iframe-transport.js')!!}"></script>
<script type="text/javascript" src="{!! url('/js/jQuery-File-Upload-9.10.6/js/jquery.fileupload.js')!!}"></script>
<script>
	var lang = '{!!$LANG!!}';
	var lang_label;
	if(lang == {!!LANG_EN!!}){
		lang_label = 'en';
	}else{
		lang_label = 'de';
	}
	// Configuration for edit of page , downloads and image
    var EDIT_MAX_TIME_SECONDS = {!!Config::get('app.edit_max_time_seconds')!!};
	var EDIT_TIME_EXTENSION_ALERT_SECONDS = {!!Config::get('app.edit_time_extension_alert_seconds')!!};
	var EDIT_COMPLEATED_ALERT_SECONDS = {!!Config::get('app.edit_compleated_alert_seconds')!!};

    var hasAccessForDelete = {!!\App\Helpers\UserAccessHelper::hasAccessAction(PERMANENT_DELETE)? 'true' : 'false'!!};

</script>
<script type="text/javascript" src="{!! url('/js/tinymce_4.1.7/tinymce.js')!!}"></script>
<script type="text/javascript" src="{!! url('/js/tiny_mce_init.js')!!}"></script>
<script type="text/javascript" src="{!! url('/js/jquery.form.min.js')!!}"></script>
<script type="text/javascript" src="{!! url('/js/dbdnfilemanager/dbdnfilemanager.js')!!}"></script>
<script type="text/javascript" src="{!! url('/js/jquery.filetree/jqueryFileTree.js')!!}"></script>
<script type="text/javascript" src="{!! url('/js/editing_timer.js')!!}"></script>
@if(\App\Helpers\UserAccessHelper::getUserRootFolder() != "all")
	<script>
		fileRoot = "/{!!\App\Helpers\UserAccessHelper::getUserRootFolder()!!}/";//fileRoot declared in dbdnfilemanager.js
	</script>
@endif
<script>
	// Map for folder (brand) and corecponding recycle info
	var recycleInfo = {};
	recycleInfo[fileRoot] = {!!json_encode($FOLDER_STATUS)!!};
</script>
<div id="filemanager" class="@if($FILEMANAGER_MODE == 'downloads') downloads @endif">
<div id="progress_wrapper">
    <div id="progress">
        <span id="progress_title">Upload File</span>
        <div class="cf" id="main_progress">
            <div class="ball_wrapper">0%</div>
            <div class="bar_wrapper">
                <div class="bar_border">
                    <div class="bar"></div>
                </div>
            </div>
            <span class="cancel_submit" id="cancel_files_upload">X</span>
            <div class="info_wrapper">
                <div class="file_name">Total</div>
                <div class="file_size">0 MB/0 MB</div>
            </div>
        </div>
    </div>
</div>
	<input id="filemanger_mode" data-value="{!!$FILEMANAGER_MODE!!}" type="hidden"/>
    <input id="isNewsletter" data-value="{!!isset($IS_NEWSLETTER)?$IS_NEWSLETTER: false !!}" type="hidden"/>
	<div class="user_table cf">
		<form method="POST" action="" class="filter_form">
			<input type="text" name="user_search" value="" class="user_search" placeholder="@lang('backend_filemanager.placeholder.file_search')"/>
			<select name="user_sort" class="user_sort">
				<option value="-1">@lang('backend_filemanager.placeholder.file_sort')</option>
				<option value="filename">@lang('backend_filemanager.column.title')</option>
				<option value="id">@lang('backend_filemanager.column.id')</option>
				<option value="extension">@lang('backend_filemanager.column.type')</option>
				<option value="size">@lang('backend_filemanager.column.size')</option>
				<option value="updated_at">@lang('backend_filemanager.column.last_modified')</option>
			</select>
			<input class="user_submit" type="submit" value="@lang('backend_filemanager.button.filter')" onClick="return refreshTable();" />
		</form>
		<div id="currentfolder"><span>Path:</span><span id="folder_selected">/</span></div>
        <div class="free_space" id="aviable_space">&nbsp;</div>
        <div class="free_space" id="total_aviable_space"></div>
		<div class="form_left">
			<div id="tree_Title">Folder</div>
			<div id="fileTree_wrapper" class="cl">
				<div id="folder_control" class="filetree_control">
					<input id="create_folder" class="user_submit" type="button" value="" />
					<input id="remove_folder" class="user_submcreate_folderit" type="button" value="" confirm="@lang('backend_filemanager.folder.remove.confirm')"/>
				</div>
				<div style="clear:both;"></div>
				<div id="filetree"></div>
			</div>
		</div>
		<div class="form_right">
			<table cellpadding="0" cellspacing="0" border="0" id="pages">
				<thead>
					<tr>
						<th>&nbsp;</th>
						<th>@lang('backend_filemanager.column.title')</th>
						<th>@lang('backend_filemanager.column.id')</th>
						<th>@lang('backend_filemanager.column.type')</th>
						<th>@lang('backend_filemanager.column.size')</th>
						<th>@lang('backend_filemanager.column.last_modified')</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
			<div id="user_table_btns" class="cf">
				<div id="filemanager_btn_upload">
					<a class="nav_box white upload upload_icon">@lang('backend_filemanager.upload')</a>
				</div>
				<div id="filemanager_btn_right">
					<a id="user_table_btnInsert" class="nav_box white upload insert">@lang('backend.add')</a>
					<a id="user_table_btnCancel" class="nav_box white upload cancel">@lang('backend.cancel')</a>
				</div>
			</div>
			<div style="" class="textarea_overlay"></div>
			<div class="user_edit_wrapper" style="display:none">
				<div class="user_actions">
					<a class="nav_box white edit">@lang('backend.edit')</a>
                    <a class="nav_box white restore" confirm="@lang('backend_filemanager.file.restore.confirm')">@lang('backend_pages.restore')</a>
                    <div class="up_down_wrapper">
                        <a class="nav_box white move_up">@lang('backend.move_up')</a>
                        <a class="nav_box white move_down">@lang('backend.move_down')</a>
                    </div>
					<a class="nav_box white remove" confirm="@lang('backend_filemanager.remove.confirm')">@lang('backend_filemanager.remove')</a>
					<div style="clear:both;height:1px;"></div>
					<div style="clear:both;"></div>
				</div>
			</div>
			<div id="wrapper_form_upload">
				<input id="fileId" type="hidden" tabindex="-1" />
				<input id="fileInfoIdEN" type="hidden" tabindex="-1" />
				<input id="fileInfoIdDE" type="hidden" tabindex="-1" />
				<div>
					<form id="upload" class="filter_form cf" action="/public/api/v1/files/createfile" method="POST"  enctype="multipart/form-data">
						<input id="currentpath" name="currentpath" type="hidden" tabindex="-1"/>
						<div class="form_body">
							<div id="wrapper_titleDE" class="input_group">
								<label for="titleDE" >@lang('backend_filemanager.form.title_de')</label>
								<input id="titleDE" name="title[{!!LANG_DE!!}]" value="" tabindex="1">
							</div>
							<div id="wrapper_titleEN" class="input_group">
								<label for="titleEN">@lang('backend_filemanager.form.title_en')</label>
								<input id="titleEN" name="title[{!!LANG_EN!!}]" value="" tabindex="2">
							</div>
							<div id="wrapper_DescriptionDE" class="input_group">
								<label for="textarea_descriptionDE" >@lang('backend_filemanager.form.description_de')</label>
								<textarea id="textarea_descriptionDE" value="" tabindex="3"></textarea>
								<input id="descriptionDE" name="description[{!!LANG_DE!!}]" type="hidden">
							</div>
							<div id="wrapper_DescriptionEN" class="input_group">
								<label for="textarea_descriptionEN" >@lang('backend_filemanager.form.description_en')</label>
								<textarea id="textarea_descriptionEN" tabindex="4"></textarea>
								<input id="descriptionEN" name="description[{!!LANG_EN!!}]" type="hidden">
							</div>
							@if($FILEMANAGER_MODE == \App\Http\Controllers\FileController::MODE_DOWNLOADS)
                            <div id="edit_mode"  class="input_section">
                                <div class="input_group">
                                    <label for="format">@lang('backend_filemanager.form.format')</label>
                                    <select name="format" tabindex="5">
                                         <option value="0"></option>
                                        @foreach (Config::get('app.parameters.file_format') as $key => $value)
                                            <option value="{!!$key!!}">{!!$value!!}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div style="clear:both;"></div>
                                <div class="input_group">
                                    <label for="dimensions">@lang('backend_filemanager.form.dimension')</label>
                                    <input type="text" value="" name="dimensions" tabindex="6"/>
                                </div>
                                <div class="input_group">
                                    <label for="dimension_units">@lang('backend_filemanager.form.unit')</label>
                                    <select name="dimension_units" tabindex="7">
                                        @foreach (Config::get('app.parameters.file_dimension') as $key => $value)
                                            <option value="{!!$key!!}">{!!$value!!}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div style="clear:both;"></div>
                                <div class="input_group">
                                    <label for="resolution">@lang('backend_filemanager.form.resolution')</label>
                                    <input type="text" value="" name="resolution" tabindex="8" maxlength="4"/>
                                </div>
                                <div class="input_group">
                                    <label for="resolution_units">@lang('backend_filemanager.form.unit')</label>
                                    <select name="resolution_units" tabindex="9">
                                        @foreach (Config::get('app.parameters.file_resolution') as $key => $value)
                                            <option value="{!!$key!!}">{!!$value!!}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div style="clear:both;"></div>
                                <div class="input_group">
                                    <label for="color_mode">@lang('backend_filemanager.form.color_mode')</label>
                                    <select name="color_mode" tabindex="10">
                                        <option value="0"></option>
                                        @foreach (Config::get('app.parameters.file_color_mode') as $key => $value)
                                            <option value="{!!$key!!}">{!!trans('ws_general_controller.color_mode_'.$value, array(),'en').'/'.trans('ws_general_controller.color_mode_'.$value, array(),'de')!!}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div style="clear:both;"></div>
                                <div class="input_group">
                                    <label for="lang">@lang('backend_filemanager.form.language')</label>
                                    <input type="text" value="" name="lang" tabindex="11"/>
                                </div>
                                <div style="clear:both;"></div>
                                <div class="input_group">
                                    <label for="pages">@lang('backend_filemanager.form.pages')</label>
                                    <input type="text" value="" name="pages" tabindex="12" maxlength="4"/>
                                </div>
                                <div style="clear:both;"></div>
                                <div class="input_group">
                                    <label for="version">@lang('backend_filemanager.form.version')</label>
                                    <input type="text" value="" name="version" tabindex="13" maxlength="4"/>
                                </div>
                                <div style="clear:both;"></div>
                                <div class="input_group">
                                    <label for="copyright_notes">@lang('backend_filemanager.form.copyright_notes')</label>
                                    <textarea name="copyright_notes" tabindex="14"></textarea>
                                </div>
                                <div style="clear:both;"></div>
                                 <div class="input_group">
                                    <label for="usage_terms_de">@lang('backend_filemanager.form.usage_terms_de')</label>
                                    <textarea name="usage_terms_de" tabindex="15"></textarea>
                                </div>
                                <div class="input_group">
                                    <label for="usage_terms_en">@lang('backend_filemanager.form.usage_terms_en')</label>
                                    <textarea name="usage_terms_en" tabindex="16"></textarea>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            @endif
							<div id="filemanager_checkbox" class="role_section">
								<div class="role_group">
									<input id="check_protected" type="checkbox" name="protected" tabindex="17"/><label for="check_protected">@lang('backend_filemanager.form.protection')</label>
								</div>
                                @if($FILEMANAGER_MODE == \App\Http\Controllers\FileController::MODE_DOWNLOADS)
                                <div class="role_group">
									<input id="check_visible" type="checkbox" name="thumb_visible" tabindex="18"/><label for="check_visible">@lang('backend_filemanager.form.thumb_visible')</label>
								</div>
                                @endif
							</div>

							<div id="fileInput_fields">
								<div id="wrapper_file_input" class="input_group">
									<label id="label_for_file_input" for="label_file_input">Image</label>
									<input id="label_file_input" readonly spellcheck="false" value="" tabindex="-1" name="file_title">
									<div class="btn_input_files">
										<button type="button" id="btn_uploadImg" class="btn_upload_icon" tabindex="19" onclick="$('#file_input').click();"></button>
										<div >
											<input id="file_input" accept="image/jpeg, image/png" type="file" name="file"  tabindex="-1">
										</div>
									</div>

								</div>
								<div id="wrapper_file_zoom" class="input_group" @if($FILEMANAGER_MODE == 'downloads') style="display:none" @endif >
									<label for="label_file_zoom" style="float:left">Image Zoom</label>
									<img class="thumb_remove" src="{!! url('/img/backend/delete_icon.png')!!}" onClick="$('#label_file_zoom').val(''); $('#file_zoom').val('');"/>
									<input name="file_zoom_title" id="label_file_zoom" readonly spellcheck="false" value="" tabindex="-1">
									<div class="btn_input_files">
										<button type="button" class="btn_upload_icon" tabindex="20" onclick="$('#file_zoom').click();"></button>
										<div>
											<input id="file_zoom" type="file" name="file_zoom" accept="image/jpeg, image/png"  tabindex="-1">
										</div>
									</div>

								</div>
								<div id="div_file_thumb" class="input_group">
									<label for="label_file_thumb" style="float:left">File Thumb</label>
									<img class="thumb_remove" src="{!! url('/img/backend/delete_icon.png')!!}" onClick="$('#label_file_thumb').val(''); $('#file_thumb').val('');"/>
									<input name="file_thumb_title" id="label_file_thumb" readonly spellcheck="false" value="" tabindex="-1">
									<div class="btn_input_files">
										<button type="button" class="btn_upload_icon" tabindex="21" onclick="$('#file_thumb').click();"></button>
										<div>
											<input id="file_thumb" type="file" name="file_thumb" accept="image/jpeg, image/png" tabindex="-1">
										</div>
									</div>

								</div>
							</div>
						</div>
						<div class="form_footer">
							<input id="save_upload" type="submit" value="@lang('backend.save')" onClick="return uploadFile();" tabindex="9">
							<a id="cancel_upload" class="nav_box white upload cancel" tabindex="22">@lang('backend.cancel')</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

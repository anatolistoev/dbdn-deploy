<div class="textarea_overlay"></div>
<div class="user_fields" id="wrapper_form_page" style="display:none;">
    <form id="user_form" action="" method="POST" style="height: 100%; display: flex;">
        <input type="hidden" name="id" value="0" />
        <input type="hidden" name="u_id" value="0" />
        <input type="hidden" name="parent_id" value="0" />
        <input type="hidden" name="position" value="0" />
        <div class="form_left cf">
            <fieldset>
                <div class="input_group" id="template_select">
                    <legend>@lang('backend_pages.form.legend.template')<span class="fieldset_requared">*</span></legend>
                    <select name="template" tabindex="1">
                        <option value="basic">Basic Page</option>
                        <option value="smart">Basic Page (smart)</option>
                        <option value="exposed">Exposed Page</option>
                        <option value="best_practice">Best Practice</option>
                        <option value="downloads">Download Page</option>
                        <option value="home">Home Page</option>
                        <option value="brand">Brand Page</option>
                    </select>
                </div>
                <div class="input_group">
                    <legend>@lang('backend_pages.form.overview')</legend>
                    <div class="role_group" >
                        <input id="overview_no" type="radio" name="overview" value="0" tabindex="2"/><label class="normal_label" for="overview_no">@lang('backend_pages.form.overview.no')</label>
                    </div>
                    <div class="role_group">
                        <input id="overview_yes" type="radio" name="overview" value="1" tabindex="3"/><label class="normal_label" for="overview_yes">@lang('backend_pages.form.overview.yes')</label>
                    </div>
                </div>
                <div class="input_group">
                    <legend>@lang('backend_pages.form.legend.languages')<span class="fieldset_requared">*</span></legend>
                    <!--					<div class="role_group head">
                                            <label>@lang('backend_pages.form.languages')</label>
                                        </div>-->
                    <div class="role_group">
                        <input id="language_de" type="checkbox" name="langs" value="2" onClick="hideTitle(this, 'de');" tabindex="4"/><label for="language_de">@lang('backend_pages.form.languages.german')</label>
                    </div>
                    <div class="role_group">
                        <input id="language_en" type="checkbox" name="langs" value="1" onClick="hideTitle(this, 'en');" tabindex="5"/><label for="language_en">@lang('backend_pages.form.languages.english')</label>
                    </div>
                    <div style="clear:both;"></div>
                </div>
            </fieldset>
            @if(in_array(Auth::user()->role,Config::get('auth.CMS_APPROVER')))
            <fieldset style="position: relative;">
                <legend>@lang('backend_pages.form.legend.access')</legend>
                <div class="input_group" style="position: relative;">
                    <div class="access_overlay" @if(isset($PAGE) && $PAGE->version == 0 && $PAGE->is_visible == 1 && $PAGE->workflow->editing_state != STATE_APPROVED )style="display:none"@endif></div>
                    <?php $count = 5; ?>
                    @foreach($GROUPS as $group)
                    <div class="role_group">
                        <input id="check_group_{!!$group->id!!}" type="checkbox" name="access" value="{!!$group->id!!}" tabindex="{!!$count++!!}"/><label for="check_group_{!!$group->id!!}">{!!$group->name!!}</label>
                    </div>
                    @endforeach
                    <div style="clear:both;"></div>
                </div>
            </fieldset>

            <fieldset>
                <div class="input_group">
                    <legend>@lang('backend_pages.form.legend.publish_date')</legend>
                    <div class="role_section date_picker">
                        <input type="text" tabindex="9" name="publish_date" value="" id="publish_date" readonly>
                        <script>
                            var curentTime = new Date("{!!date('Y-m-d\TH:i')!!}");
                            $('#publish_date').datetimepicker({
                                dateFormat: 'yy-mm-dd',
                                timeFormat: 'HH:mm',
                                minDate: new Date("{!!date('Y-m-d\TH:i', strtotime('1 hour'))!!}"),
                                onSelect: function (date) {
                                    var curentDatetime = new Date();
                                    curentDatetime.setMinutes(curentDatetime.getMinutes() + 60);
                                    var date1 = $('#publish_date').datepicker('getDate');
                                    if(date1 < curentDatetime){
                                        $('#publish_date').datetimepicker('option', 'minDateTime', curentDatetime);
                                    }
                                    var date2 = $('#unpublish_date').val();
                                    date1.setMinutes(date1.getMinutes() + 60);
                                    $('#unpublish_date').datepicker('option', 'minDate', date1);
                                    $('#unpublish_date').datetimepicker('option', 'minDateTime', date1);
                                    if (date2 == CONFIG.get('DATE_TIME_ZERO')) {
                                        $('#unpublish_date').val(CONFIG.get('DATE_TIME_ZERO'));
                                    }
                                }
                            });
                        </script>
                        <span class="nullify_input">X</span>
                    </div>
                </div>
                <div class="input_group">
                    <legend>@lang('backend_pages.form.legend.unpublish_date')</legend>
                    <div class="role_section date_picker">
                        <input type="text" tabindex="10" name="unpublish_date" value="" id="unpublish_date" readonly>
                        <script>
                            $('#unpublish_date').datetimepicker({
                                dateFormat: 'yy-mm-dd',
                                timeFormat: 'HH:mm',
                                minDate: new Date("{!!date('Y-m-d\TH:i', strtotime('1 hour'))!!}")
                            });
                        </script>
                        <span class="nullify_input">X</span>
                    </div>
                </div>
            </fieldset>
            @endif
            <fieldset>
                @if(in_array(Auth::user()->role,Config::get('auth.CMS_APPROVER')))
                <div class="radio_group">
                    <label>@lang('backend_pages.form.active')</label><br />
                    <input id="page_non_active" class="first_choice" type="radio" name="active" value="0" checked="true" tabindex="11"/><label for="page_non_active"></label>
                    <input id="page_active" class="second_choice" type="radio" name="active" value="1" tabindex="12" checked="false"/><label for="page_active"></label>
                    <div style="clear:both;"></div>
                </div>
                @endif
                <div class="radio_group">
                    <label>@lang('backend_pages.form.visible')</label><br />
                    <input id="page_non_visible" class="first_choice" type="radio" name="in_navigation" value="0" checked="true" tabindex="13"/><label for="page_non_visible"></label>
                    <input id="page_visible" class="second_choice" type="radio" name="in_navigation" value="1" checked="false" tabindex="14"/><label for="page_visible"></label>
                    <div style="clear:both;"></div>
                </div>

                <div class="radio_group">
                    <label>@lang('backend_pages.form.searchable')</label><br />
                    <input id="page_non_searchable" class="first_choice" type="radio" name="searchable" value="0" checked="true" tabindex="15"/><label for="page_non_searchable"></label>
                    <input id="page_searchable" class="second_choice" type="radio" name="searchable" value="1" checked="false" tabindex="16"/><label for="page_searchable"></label>
                    <div style="clear:both;"></div>
                </div>
                <div class="radio_group">
                    <label>@lang('backend_pages.form.glossary')</label><br />
                    <input id="page_non_glossary" class="first_choice" type="radio" name="inline_glossary" value="0" checked="true" tabindex="17"/><label for="page_non_glossary"></label>
                    <input id="page_glossary" class="second_choice" type="radio" name="inline_glossary" value="1" checked="false" tabindex="18"/><label for="page_glossary"></label>
                    <div style="clear:both;"></div>
                </div>

            </fieldset>

        </div>
        <div class="form_right cf">

            <!--			<fieldset>
                            <legend>@lang('backend_pages.form.legend.type')</legend>
                            <div class="role_section">
                                <div class="role_group head">
                                    <label>@lang('backend_pages.form.type')</label>
                                </div>
                                <div class="role_group">
                                    <input id="type_file" type="radio" name="type" value="File" checked tabindex="18"/><label for="type_file">@lang('backend_pages.form.type.file')</label>
                                </div>
                                <div class="role_group">
                                    <input id="type_folder" type="radio" name="type" value="Folder" tabindex="19"/><label for="type_folder">@lang('backend_pages.form.type.folder')</label>
                                </div>
                                <div class="role_group">
                                    <input id="type_shortcut" type="radio" name="type" value="Shortcut" tabindex="20"/><label for="type_shortcut">@lang('backend_pages.form.type.shortcut')</label>
                                </div>
                            </div>
                        </fieldset>-->
            <fieldset class="title">
                <div class="input_group">
                    <label for="title_de">@lang('backend_pages.form.title_de')<span class="fieldset_requared">*</span></label>
                    <input type="text" value="" name="title_de" tabindex="19"/>
                </div>
                <div class="input_group">
                    <label for="title_en">@lang('backend_pages.form.title_en')<span class="fieldset_requared">*</span></label>
                    <input type="text" value="" name="title_en" tabindex="20"/>
                </div>
                <div class="input_group" style="position: relative;">
                    <div class="access_overlay" @if(isset($PAGE) && $PAGE->version == 0 && $PAGE->is_visible == 1 && $PAGE->workflow->editing_state != STATE_APPROVED )style="display:none"@endif></div>
                    <label for="slug">@lang('backend_pages.form.slug')<span class="fieldset_requared">*</span></label>
                    <input type="text" value="" name="slug" tabindex="21"/>
                </div>
            </fieldset>
            <fieldset id="download_categories">
                <div class="input_group" style="width: 396px" id="page_usage">
                    <legend>@lang('backend_pages.form.legend.usage')<span class="fieldset_requared">*</span></legend>
                    <select name="usage" tabindex="23">
                            @foreach (Config::get('app.page_usage') as $key => $value)
                                <option value="{!!$key!!}">{!!trans('ws_general_controller.page_usage_'.$value, array(),'en')!!}/{!!trans('ws_general_controller.page_usage_'.$value, array(),'de')!!}</option>
                            @endforeach
                    </select>
                </div>
                <div class="input_group" id="page_file_type">
                    <legend>@lang('backend_pages.form.legend.file_type')<span class="fieldset_requared">*</span></legend>
                    <select name="file_type" tabindex="22">
                            @foreach (Config::get('app.page_file_type') as $key => $value)
                                <option value="{!!$key!!}">{!!trans('ws_general_controller.page_file_type_'.$value, array(),'en')!!}/{!!trans('ws_general_controller.page_file_type_'.$value, array(),'de')!!}</option>
                            @endforeach
                    </select>
                </div>
            </fieldset>
            <fieldset class="tags_fieldset">
                <div class="input_group" style="width: 48%;">
                <label>@lang('backend_pages.form.legend.tags_de')</label>
                <div class="role_section tags_wrapper" style=" width: 369px">
                    <div class="tags"></div>
                    <input type="text" id="tags_de" class='tags_lang' data-lang ="{!!LANG_DE!!}" value="" tabindex="25"/>
                </div>
                </div>
                 <div class="input_group" style="width: 48%; float: right;">
                <label>@lang('backend_pages.form.legend.tags_en')</label>
                <div class="role_section tags_wrapper" style=" width: 369px">
                    <div class="tags"></div>
                    <input type="text" id="tags_en" class='tags_lang' data-lang ="{!!LANG_EN!!}" value="" tabindex="24"/>
                </div>
                </div>
            </fieldset>
            <div class='fieldset_border cf'></div>
            <fieldset>
                <div class="input_group">
                    <legend>@lang('backend_pages.form.legend.home_blocks')</legend>
                    <div class="role_section" style="width: 700px;">
                        <div class="role_group" style="width: 160px">
                            <input id="front_block" type="checkbox" name="front_block" value="1" tabindex="26"/><label for="front_block">@lang('backend_pages.form.front_blocks')</label>
                        </div>
                        <div class="role_group" style="width: 160px">
                            <input id="home_block" type="checkbox" name="home_block" value="1" tabindex="27"/><label for="home_block">@lang('backend_pages.form.home_blocks')</label>
                        </div>
                        <!-- Temporary hidden because is not used at the moment -->
                        <div class="role_group" style="width: 160px; display: none;">
                            <input id="home_accordion" type="checkbox" name="home_accordion" value="1" tabindex="28"/><label for="home_accordion">@lang('backend_pages.form.accordion')</label>
                        </div>
                        <div class="role_group" style="width: 160px">
                            <input id="featured_menu" type="checkbox" name="featured_menu" value="1" tabindex="29"/><label for="featured_menu">@lang('backend_pages.form.featured_menu')</label>
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
            </fieldset>
            <div style="clear:both;height:92px;"></div>

            <!--			<fieldset>
                            <div class="input_group">
                                <label>@lang('backend_pages.form.update_alert.german')</label>
                                <textarea id="upd_al_de" tabindex="29"></textarea>
                            </div>
                            <div class="input_group">
                                <label>@lang('backend_pages.form.update_alert.english')</label>
                                <textarea id="upd_al_en" tabindex="30"></textarea>
                            </div>
                            <div style="clear:both;height:10px;"></div>
                            <a class="update_alert" href="#" onClick="return sendMailNotification();" tabindex="28">@lang('backend_pages.form.update_alert.label')</a>

                        </fieldset>-->
            <div class="properties_buttons">
                <a class="nav_box white save" href="#" tabindex="30">@lang('backend.save')</a>
                <a class="nav_box white cancel" href="#" tabindex="31">@lang('backend.cancel')</a>
            </div>
        </div>
        <div style="clear:both;"></div>
    </form>
</div>

<div class="textarea_overlay" style="z-index: 140;"></div>
<div class="user_fields" id="wrapper_form_update_alert" style="display:none;z-index: 141;">
	<form id="update_alert_form" action="" method="POST" style = "display:flex">
		<input type="hidden" name="id" value="" />
        <div class="form_left">     
		</div>
        <div class="form_right cf" style="height: 100%">
            <fieldset>
                <div class="input_group" style="width: 49%; margin-right: 12px;">
                    <label>@lang('backend_pages.form.update_alert.german')</label>
                    <textarea id="upd_al_de" tabindex="29"></textarea>
                </div>	
                <div class="input_group" style="width: 49%;margin-right: 0;">
                    <label>@lang('backend_pages.form.update_alert.english')</label>
                    <textarea id="upd_al_en" tabindex="30"></textarea>
                </div>
                <div style="clear:both;height:10px;"></div>
                <a class="update_alert" href="#" onClick="return sendMailNotification(event);" tabindex="28">@lang('backend_pages.form.update_alert.label')</a>
                <a class="nav_box white cancel" href="#" tabindex="8">@lang('backend.cancel')</a>
            </fieldset>			
			<div style="clear:both;"></div>
		</div>
		<div style="clear:both;"></div>
	</form>
</div>

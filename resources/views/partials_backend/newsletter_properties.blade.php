<div class="textarea_overlay"></div>
<div class="user_fields" id="wrapper_form_newsletter" style="display:none;">
	<form id="newsletter_form" action="" method="POST">
		<input type="hidden" name="id" value="0" />
		<div class="form_left" style="height:362px;padding-top:30px;">
			<div style="clear:both;height:3px;"></div>
			<div class="system_info">
				<div class="system_info_row">
					<p class="si_label">@lang('backend_tags.form.created_at')</p>
					<p class="si_data" id="created_at"></p>
					<div style=clear:both;"></div>
				</div>
				<div class="system_info_row">
					<p class="si_label">@lang('backend_tags.form.modified_at')</p>
					<p class="si_data" id="updated_at"></p>
					<div style=clear:both;"></div>
				</div>
			</div>
			<fieldset>
				<div class="input_group">
					<legend>@lang('backend_pages.form.legend.languages')<span class="fieldset_requared">*</span></legend>
					<!--					<div class="role_group head">
											<label>@lang('backend_pages.form.languages')</label>
										</div>-->
					<br />
					<div class="role_group">
						<input id="language_de" type="checkbox" name="langs" value="2" onClick="hideTitle(this, 'de');" tabindex="2"/><label for="language_de">@lang('backend_pages.form.languages.german')</label>
					</div>
					<div class="role_group">
						<input id="language_en" type="checkbox" name="langs" value="1" onClick="hideTitle(this, 'en');" tabindex="3"/><label for="language_en">@lang('backend_pages.form.languages.english')</label>
					</div>
					<div style="clear:both;"></div>
				</div>
			</fieldset>
            <fieldset>
                <div class="radio_group">
                    <label>@lang('backend_pages.form.active')</label><br />
                    <input id="newsletter_non_active" class="first_choice" type="radio" name="active" value="0" checked="true" tabindex="9"/><label for="newsletter_non_active"></label>
                    <input id="newsletter_active" class="second_choice" type="radio" name="active" value="1" tabindex="10" checked="false"/><label for="newsletter_active"></label>
                    <div style="clear:both;"></div>
                </div>
            </fieldset>
		</div>
		<div class="form_right" style="height:362px;">
			<fieldset class="title">
				<div class="input_group">
					<label for="title_de">@lang('backend_newsletter.form.title_de')<span class="fieldset_requared">*</span></label>
					<input type="text" value="" name="title_de" tabindex="17"/>
				</div>
				<div class="input_group">
					<label for="title_en">@lang('backend_newsletter.form.title_en')<span class="fieldset_requared">*</span></label>
					<input type="text" value="" name="title_en" tabindex="18"/>
				</div>
				<div class="input_group">
					<label for="slug">@lang('backend_newsletter.form.slug')<span class="fieldset_requared">*</span></label>
					<input type="text" value="" name="slug" tabindex="19"/>
				</div>
			</fieldset>

			<div style="clear:both;height:80px;"></div>
			<a class="nav_box white save" href='#' tabindex="3">@lang('backend.save')</a>
			<a class="nav_box white cancel" href='#' tabindex="4">@lang('backend.cancel')</a>

		</div>
		<div style="clear:both;"></div>
	</form>
</div>
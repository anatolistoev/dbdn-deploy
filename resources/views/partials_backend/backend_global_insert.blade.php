<script>
    var relPath  = '{!!asset("")!!}';
    var sessionTimeout = {!! Config::get('session.lifetime') * 60!!};
    var isLogin = {!! Auth::check()? "true" : "false" !!};
    var logoutUrl = "{!! url('/cms/logout')!!}";
</script>
@include('partials_backend.js_localize')
<script type="text/javascript" src="{!! url('/js/jquery-1.11.1.min.js')!!}"></script>

<script src="{!! url('/js/jquery.alerts.js')!!}" type="text/javascript"></script>
<script src="{!! url('/js/jquery.alert.logout.js')!!}" type="text/javascript"></script>
<link href="{!! url('/js/jquery.alerts.css')!!}" rel="stylesheet" type="text/css" media="screen" />
<!--[if (lte IE 8)]>
	<script type="text/javascript" src="{!! asset('/js/ie_extensions.js') !!}"></script>
<![endif]-->	

<link rel="stylesheet" href="{!! url('/css/backend.css')!!}" />
<link rel="SHORTCUT ICON" href="{!! asset('/favicon48.ico') !!}" type="image/x-icon"/>
<link rel="icon" href="{!! asset('/DAI_Favicon_16.png')!!}" sizes="16x16" type="image/png"/>
<link rel="icon" href="{!! asset('/DAI_Favicon_32.png') !!}" sizes="32x32" type="image/png">
<link rel="icon" href="{!! asset('/DAI_Favicon_96.png')!!}" sizes="96x96" type="image/png"/>
<link rel="icon" href="{!! asset('/DAI_Favicon_196.png')!!}" sizes="196x196" type="image/png"/>
<link rel="icon" href="{!! asset('/DAI_Favicon_160.png')!!}" sizes="160x160" type="image/png"/>
<link rel="apple-touch-icon" href="{!! asset('/DAI_Favicon_57.png')!!}" sizes="57x57"/>
<link rel="apple-touch-icon" href="{!! asset('/DAI_Favicon_72.png')!!}" sizes="72x72"/>

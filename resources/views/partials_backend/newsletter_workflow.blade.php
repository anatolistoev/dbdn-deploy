<div class="textarea_overlay"></div>
<div class="user_fields" id="wrapper_form_workflow" style="display:none;">
	<form id="workflow_form" action="" method="POST">
		<input type="hidden" name="id" value="{!!$NEWSLETTER->id!!}" />
		<div class="form_left" style="height:410px">
			<fieldset>
				<legend>@lang('backend_newsletter.workflow.label')<span class="fieldset_requared">*</span></legend>
				<div class="role_section" style="padding-left:0px;">
					@if ($NEWSLETTER->status == STATE_SENT)
						<p>@lang('backend_newsletter.workflow.state.sent_text')</p>
					@elseif ($NEWSLETTER->status == STATE_APPROVED && !in_array(Auth::user()->role,Config::get('auth.CMS_APPROVER')))
						<p>@lang('backend_newsletter.workflow.state.no_change')</p>
					@else
						<div class="role_group">
							<input class="editing_state" id="state_draft" type="radio" name="editing_state" value="{!!STATE_DRAFT!!}" tabindex="1" @if($NEWSLETTER->editing_state == STATE_DRAFT) checked @endif/><label for="state_draft">@lang('backend_newsletter.workflow.state.Draft')</label>
						</div>
						<div class="role_group">
							<input class="editing_state" id="state_review" type="radio" name="editing_state" value="{!!STATE_REVIEW!!}" tabindex="2" @if($NEWSLETTER->editing_state == STATE_REVIEW) checked @endif/><label for="state_review">@lang('backend_newsletter.workflow.state.Review')</label>
						</div>
						@if(in_array(Auth::user()->role,Config::get('auth.CMS_NL_APPROVER')))
							<div class="role_group">
								<input class="editing_state" id="state_approved" type="radio" name="editing_state" value="{!!STATE_APPROVED!!}" tabindex="3" @if($NEWSLETTER->editing_state == STATE_APPROVED) checked @endif/><label for="state_approved">@lang('backend_newsletter.workflow.state.Approved')</label>
							</div>
						@endif
					@endif
				</div>
			</fieldset>
		</div>
		<div class="form_right cf" style="height:410px">

			<fieldset class="fieldset_right due_date half_width">
				<legend>@lang('backend_newsletter.due_date.label')</legend>
				<div class="role_section">
					<input  style="width:318px;" type="text" tabindex="28" name="due_date" value="@if(strtotime($NEWSLETTER->due_date) <= 0)@lang('backend_newsletter.no_due_date') @else {!!date('Y-m-d H:i',strtotime($NEWSLETTER->due_date))!!} @endif" id="due_date" readonly>
					<script>
						$('#due_date').datetimepicker({
							dateFormat: 'yy-mm-dd',
							timeFormat: 'HH:mm',
							minDate: new Date("{!!date('Y-m-d\TH:i')!!}")
						});
					</script>
					<span class="nullify_input">X</span>
				</div>
			</fieldset>
			<fieldset class="half_width">
				<legend>@lang('backend_newsletter.responsible.label')</legend>

					<select name="user_responsible" tabindex="5" style="width:318px;">
						<option value="0">@if($NEWSLETTER->user_responsible == 0 && $NEWSLETTER->status == STATE_DRAFT) @lang('ws_general_controller.page.workflow.unassigned') @else Please select...@endif </option>
						@foreach($WORKFLOW_ADMINS as $au)
						<option value="{!!$au->id!!}" @if($NEWSLETTER->user_responsible == $au->id) selected="selected" @endif >{!!$au->username!!} ({!!$au->resolveRole()!!})</option>
						@endforeach
					</select>

			</fieldset>

			<div style="clear:both;"></div>
			<fieldset>
				<a class="nav_box white save" href="#" tabindex="7">@lang('backend.save')</a>
				<a class="nav_box white cancel" href="#" tabindex="8">@lang('backend.cancel')</a>
			</fieldset>


		</div>
		<div style="clear:both;"></div>
	</form>
</div>

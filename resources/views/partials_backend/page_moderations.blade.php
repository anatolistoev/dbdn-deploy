<div class="textarea_overlay"></div>
<div class="user_fields" id="moderations_wrapper_inner" style="display:none;">
	<div id="moderation_template" style="display:none;">
		<div class="page_moderation">
			<h6 class="moderation_by">@lang('backend_pages.moderations.from') </h6>
			<span class="moderation_date"></span>
			<p></p>
		</div>
	</div>
	<div class="moderation_holder"></div>
	<div style="clear:both;"></div>
	<fieldset>			
		<a style="float:right" class="nav_box white cancel" href="#" tabindex="8">@lang('backend.cancel')</a>
	</fieldset>
</div>

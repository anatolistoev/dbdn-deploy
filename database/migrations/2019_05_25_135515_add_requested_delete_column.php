<?php

use Illuminate\Database\Migrations\Migration;

class AddRequestedDeleteColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            ALTER TABLE `user`
            ADD COLUMN `requested_delete_at`  timestamp NULL DEFAULT NULL AFTER `deleted_at`,
            ADD INDEX `user_delreq` (`requested_delete_at`) USING BTREE ;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("ALTER TABLE `user` DROP COLUMN `requested_delete_at`;");
    }
}

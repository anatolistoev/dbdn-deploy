<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AlterNewsletterTableForEditingWorkflow extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //ALTER TABLE `newsletter` CHANGE `status` `status` ENUM('Draft','Pending','Approved','Sending','Sent') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
        //alter table `newsletter` add `editing_state` enum('Draft', 'Review', 'Approved') not null default 'Draft',
        //add `user_edit_id` int null,
        //add `end_edit` timestamp null,
        //add `langs` int not null,
        //add `created_by` int not null,
        //add `assigned_by` int not null,
        //add `user_responsible` int not null,
        //add `due_date` timestamp default 0 not null,
        //add `deleted_at` timestamp null
        DB::statement("ALTER TABLE `newsletter` CHANGE `status` `status` ENUM('Draft','Pending','Approved','Sending','Sent') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
        Schema::table('newsletter', function (Blueprint $table) {
            $table->enum('editing_state', ['Draft', 'Review','Approved'])->default('Draft');
            $table->integer('user_edit_id')->nullable()->default(null);
            $table->timestamp('end_edit')->nullable()->default(null);
            $table->integer('langs');
            $table->integer('created_by');
            $table->integer('assigned_by');
            $table->integer('user_responsible');
            $table->timestamp('due_date');
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `newsletter` CHANGE `status` `status` ENUM('draft','pending','approved','sending','sent') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
        Schema::table('newsletter', function (Blueprint $table) {
            $table->dropColumn('editing_state');
            $table->dropColumn('user_edit_id');
            $table->dropColumn('end_edit');
            $table->dropColumn('langs');
            $table->dropColumn('created_by');
            $table->dropColumn('assigned_by');
            $table->dropColumn('user_responsible');
            $table->dropColumn('due_date');
            $table->dropColumn('deleted_at');
        });
    }
}

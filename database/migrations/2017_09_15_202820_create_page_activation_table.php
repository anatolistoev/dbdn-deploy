<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePageActivationTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('page_activation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_u_id')->unsigned();
            $table->timestamp('publish_date')->index();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('page_u_id')->references('u_id')->on('page')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('page_activation');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;

class AlterUsersAddUnsubscribeCode extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function ($table) {
            $table->binary('unsubscribe_code')->after('newsletter_lang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function ($table) {
            $table->dropColumn('unsubscribe_code');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;

class CreateGdprProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            DROP PROCEDURE IF EXISTS `gdpr_erase`;
            CREATE DEFINER=CURRENT_USER PROCEDURE `gdpr_erase`(IN `del_user_id` int, IN `del_email` varchar(255), IN `admin_uid` int)
            BEGIN
                DELETE FROM `user` WHERE `user`.id = del_user_id;
                DELETE FROM `request_auth` WHERE `request_auth`.user_id = del_user_id;
                DELETE FROM `usergroup` WHERE `usergroup`.user_id = del_user_id;
                DELETE FROM `password_reminders` WHERE `password_reminders`.email = del_email;
                DELETE FROM `password_resets` WHERE `password_resets`.email = del_email;
                UPDATE `history` SET `history`.username = CONCAT(`history`.username,' (deleted)') WHERE `history`.user_id = del_user_id;
                DELETE FROM `faqrate` WHERE `faqrate`.user_id = del_user_id;
                UPDATE `page` SET `page`.user_edit_id = admin_uid WHERE `page`.user_edit_id = del_user_id;
                UPDATE `file` SET `file`.user_edit_id = admin_uid WHERE `file`.user_edit_id = del_user_id;
                UPDATE `workflow` SET `workflow`.user_responsible = admin_uid WHERE `workflow`.user_responsible = del_user_id;
                UPDATE `workflow` SET `workflow`.assigned_by = admin_uid WHERE `workflow`.assigned_by = del_user_id;
            END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP PROCEDURE IF EXISTS `gdpr_erase`;");
    }
}

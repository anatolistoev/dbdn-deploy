<?php

use Illuminate\Database\Migrations\Migration;

class AlterUsersAddNewsletterLang extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function ($table) {
            $table->smallInteger('newsletter_lang')->unsigned()->nullable()->after('newsletter_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function ($table) {
            $table->dropColumn('newsletter_lang');
        });
    }
}

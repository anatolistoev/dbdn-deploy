<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AlterNewsletterContentAddPositionFooterBl extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //alter table `newsletter_content` add `deleted_at` timestamp null
        DB::statement("ALTER TABLE `newsletter_content` CHANGE `position` `position` ENUM('title', 'header_d', 'header_m', 'footer_i', 'footer_e','footer_e_bl' ,'content') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `newsletter_content` CHANGE `position` `position` ENUM('title', 'header_d', 'header_m', 'footer_i', 'footer_e','content') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
    }
}

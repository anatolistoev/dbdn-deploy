<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNewsletterSubscriberTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('newsletter_subscriber', function (Blueprint $table) {
            $table->integer('list_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->primary(['list_id', 'user_id']);
            
            $table->integer('created_by')->unsigned();
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('newsletter_subscriber');
    }
}

<?php

class CommentTableSeeder extends Seeder
{

    public function run()
    {

        $comments = [
            [
                'user_id' => 1,
                'page_id' => 1,
                'lang' => 1,
                'subject' => Crypt::encrypt("Test subject"),
                'body' => Crypt::encrypt("Some test comment body to test with more symbols, because of the cut in the comments table in the backend view"),
                'active' => 0
            ],
            [
                'user_id' => 1,
                'page_id' => 1,
                'lang' => 1,
                'subject' => Crypt::encrypt("Test subject 2"),
                'body' => Crypt::encrypt("Test body 2"),
                'active' => 0
            ],
            [
                'user_id' => 1,
                'page_id' => 2,
                'lang' => 1,
                'subject' => Crypt::encrypt("Test subject 3"),
                'body' => Crypt::encrypt("Test body 3"),
                'active' => 0
            ],
            [
                'user_id' => 1,
                'page_id' => 2,
                'lang' => 1,
                'subject' => Crypt::encrypt("Test subject 4"),
                'body' => Crypt::encrypt("Test body 4"),
                'active' => 0
            ],
            [
                'user_id' => 2,
                'page_id' => 2,
                'lang' => 1,
                'subject' => Crypt::encrypt("Test subject 5"),
                'body' => Crypt::encrypt("Test body 5"),
                'active' => 0
            ],
            [
                'user_id' => 3,
                'page_id' => 2,
                'lang' => 1,
                'subject' => Crypt::encrypt("Test subject 6"),
                'body' => Crypt::encrypt("Test body 6"),
                'active' => 0
            ],
            [
                'user_id' => 3,
                'page_id' => 1,
                'lang' => 1,
                'subject' => Crypt::encrypt("Test subject 7"),
                'body' => Crypt::encrypt("Test body 8"),
                'active' => 0
            ]
            
        ];

         DB::table('comment')->insert($comments);
    }
}

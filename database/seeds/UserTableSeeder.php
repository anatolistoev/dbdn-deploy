<?php
use Hautelook\Phpass\PasswordHash;

class UserTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
         //DB::table('page')->truncate();
         DB::disableQueryLog();
        $hasher = new PasswordHash(8, true);
        $users = [
            ['id' => '1', 'username' => 'slowpog', 'password' => '$P$BB9EkkwDIowCjp8.rvdLx8sMw2NcLl0', 'first_name' => Crypt::encrypt('Kiril'), 'last_name' => Crypt::encrypt('Dimitrov'), 'email' => Crypt::encrypt('slowpog@gmail.com'), 'company' => Crypt::encrypt(''), 'position' => Crypt::encrypt('Developer'), 'department' => Crypt::encrypt('IT'), 'address' => Crypt::encrypt(''), 'code' => Crypt::encrypt('1000'), 'city' => Crypt::encrypt('Maryland'), 'country' => Crypt::encrypt('United States of America'), 'phone' => Crypt::encrypt('n/a'), 'fax' => Crypt::encrypt(''), 'active'=>1, 'last_login'=> '2014-03-11 22:59:40', 'logins'=> 1439, 'unlock_code' => Crypt::encrypt('ltherlrfsmnhnca'), 'role' => 2, 'newsletter' => 0, 'force_pwd_change' => 1, 'created_at' => '2007-08-16 01:22:57', 'updated_at'=>'2014-02-17 00:15:36'],
            
            ['id' => '2', 'username' => 'esof', 'password' => '$P$BspdCfu6OT4Vi7AMn2u5XV.VtNujvj1', 'first_name' => Crypt::encrypt('Martin'), 'last_name' => Crypt::encrypt('Stefanov'), 'email' => Crypt::encrypt('martin.stefanov@esof.net'), 'company' => Crypt::encrypt('ESOF'), 'position' => Crypt::encrypt('Developer'), 'department' => Crypt::encrypt('IT'), 'address' => Crypt::encrypt(''), 'code' => Crypt::encrypt(''), 'city' => Crypt::encrypt(''), 'country' => Crypt::encrypt(''), 'phone' => Crypt::encrypt(''), 'fax' => Crypt::encrypt(''), 'active'=>1, 'last_login'=> '2014-05-05 11:34:25', 'logins'=> 1, 'unlock_code' => Crypt::encrypt('ifttpxbsoevmqfs'), 'role' => 2, 'newsletter' => 0, 'force_pwd_change' => 1, 'created_at' => '2014-05-05 09:20:29', 'updated_at'=>'2014-05-05 09:43:52'],
            
            ['id' => '3', 'username' => 'watto', 'password' => '$P$BckztFXNQQHigXybcXQW7Mnr9VuXVG0', 'first_name' => Crypt::encrypt('Kalin'), 'last_name' => Crypt::encrypt('Varbanov'), 'email' => Crypt::encrypt('k.varbanov@gmail.com'), 'company' => Crypt::encrypt(''), 'position' => Crypt::encrypt(''), 'department' => Crypt::encrypt(''), 'address' => Crypt::encrypt('Slatina, bl.27, ap.5'), 'code' => Crypt::encrypt(''), 'city' => Crypt::encrypt(''), 'country' => Crypt::encrypt(''), 'phone' => Crypt::encrypt(''), 'fax' => Crypt::encrypt(''), 'active'=>1, 'last_login'=> '2012-10-25 11:34:25', 'logins'=> 17, 'unlock_code' => Crypt::encrypt('ifgtpxbsoevmqfs'), 'role' => 2, 'newsletter' => 0, 'force_pwd_change' => 1, 'created_at' => '2007-08-21 09:20:29', 'updated_at'=>'2012-10-04 08:43:52'],
            
            ['id' => '4', 'username' => 'test', 'password' => '$P$BmBgy9rXonzwxqH0c/xMXygMIXBByi0', 'first_name' => Crypt::encrypt('fn'), 'last_name' => Crypt::encrypt('ln'), 'email' => Crypt::encrypt('slowpog@gmail.com'), 'company' => Crypt::encrypt('co'), 'position' => Crypt::encrypt('pos'), 'department' => Crypt::encrypt('off'), 'address' => Crypt::encrypt('add'), 'code' => Crypt::encrypt('zip'), 'city' => Crypt::encrypt('city'), 'country' => Crypt::encrypt('Afghanistan'), 'phone' => Crypt::encrypt('ph'), 'fax' => Crypt::encrypt('fax'), 'active'=>1, 'last_login'=> '2012-05-02 11:20:32', 'logins'=> 17, 'unlock_code' => Crypt::encrypt('yvwxyjigxqcppro'), 'role' => 0, 'newsletter' => 0, 'force_pwd_change' => 0, 'created_at' => '2007-09-27 16:56:19', 'updated_at'=>'2007-09-27 17:07:30'],
            
            ['id' => '5', 'username' => 'eminuth', 'password' => '$P$Bm7t4GpFrZR1V2s/R8V3w3VJwzWwQW.', 'first_name' => Crypt::encrypt('Eckhard'), 'last_name' => Crypt::encrypt('Minuth'), 'email' => Crypt::encrypt('eckhard.minuth@daimler.com'), 'company' => Crypt::encrypt('Daimler AG'), 'position' => Crypt::encrypt(''), 'department' => Crypt::encrypt('COM/M'), 'address' => Crypt::encrypt('096-E402'), 'code' => Crypt::encrypt(''), 'city' => Crypt::encrypt(''), 'country' => Crypt::encrypt('Germany'), 'phone' => Crypt::encrypt(''), 'fax' => Crypt::encrypt(''), 'active'=>1, 'last_login'=> '2013-08-19 17:53:25', 'logins'=> 26, 'unlock_code' => Crypt::encrypt('ullugzyzbrgeekq'), 'role' => 2, 'newsletter' => 0, 'force_pwd_change' => 1, 'created_at' => '2007-09-27 18:30:03', 'updated_at'=>'2012-05-21 09:21:29'],
            
            ['id' => '6', 'username' => 'atontchev', 'password' => '$P$BHIHZEgtgyHqeMkNiD1LgrrJMpLakm.', 'first_name' => Crypt::encrypt('Anton'), 'last_name' => Crypt::encrypt('Tontchev'), 'email' => Crypt::encrypt('anton.tontchev@interton1.com'), 'company' => Crypt::encrypt(''), 'position' => Crypt::encrypt(''), 'department' => Crypt::encrypt(''), 'address' => Crypt::encrypt(''), 'code' => Crypt::encrypt(''), 'city' => Crypt::encrypt(''), 'country' => Crypt::encrypt('Deutschland'), 'phone' => Crypt::encrypt(''), 'fax' => Crypt::encrypt(''), 'active'=>1, 'last_login'=> '2014-05-02 23:11:37', 'logins'=> 1228, 'unlock_code' => Crypt::encrypt('wrdjiinvjcmaedw'), 'role' => 2, 'newsletter' => 0, 'force_pwd_change' => 1, 'created_at' => '2007-09-27 18:32:57', 'updated_at'=>'2012-05-15 21:54:18'],

        ];

        //DB::table('user')->truncate();
        //DB::table('user')->insert($users);
        DB::transaction(function () use (&$users) {
            foreach ($users as $user) {
                $res = DB::table('user')->insert($user);
            }
        });
    }
}

<?php

class TermTableSeeder extends Seeder
{

    public function run()
    {

        $terms = [
            [
                'name' => 'Default EN',
                'lang' => 1,
                'description' => 'Default Description EN'
            ],
            [
                'name' => 'Default DE',
                'lang' => 2,
                'description' => 'Default Description DE'
            ]
        ];

         DB::table('term')->insert($terms);
    }
}

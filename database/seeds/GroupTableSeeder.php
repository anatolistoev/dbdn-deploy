<?php

class GroupTableSeeder extends Seeder
{

    public function run()
    {

        $groups = [
            [
                'name' => 'Member',
                'visible' => 1,
                'active' => 1
            ],
            [
                'name' => 'Agency',
                'visible' => 1,
                'active' => 1
            ],
            [
                'name' => 'MB External',
                'visible' => 1,
                'active' => 1
            ],
            [
                'name' => 'MB Internal',
                'visible' => 1,
                'active' => 1
            ]
            
        ];

         DB::table('group')->insert($groups);
    }
}

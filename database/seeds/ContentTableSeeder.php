<?php

class ContentTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
         //DB::table('content')->truncate();

        $content = [
            [
                'page_id'       => 1,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Home',
                'searchable'    => 1
            ],
            [
                'page_id'       => 1,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'keywords',
                'format'        => 1,
                'body'          => 'Brand & Design Navigator provides easy access and quick orientation in the multifaceted world of Corporate Design at Daimler.',
                'searchable'    => 1
            ],
            [
                'page_id'       => 1,
                'lang'          => 1,
                'position'      => 2,
                'section'       => 'banner',
                'format'        => 1,
                'body'          => '<img src="img/contents/banners/1.2150.Newsletter_03_2013.jpg">',
                'searchable'    => 1
            ],
            [
                'page_id'       => 1,
                'lang'          => 1,
                'position'      => 3,
                'section'       => 'main',
                'format'        => 1,
                'body'          => 'Home page content EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 1,
                'lang'          => 1,
                'position'      => 4,
                'section'       => 'farRight',
                'format'        => 1,
                'body'          => '<BR><IMG style="border:none; width: 65px; height: 103px;" alt="iF communication design award" align="left" src="img/brand_mark_ifaward.gif"><BR><BR><BR><BR><BR><BR><STRONG>Daimler Awarded <BR>for Its Corporate Design</STRONG></SPAN><BR></STRONG><SPAN class=normal>Daimler AG received the prestigious iF communication design award for its unique Corporate Design.<BR></SPAN><BR>',
                'searchable'    => 1
            ],
            [
                'page_id'       => 1,
                'lang'          => 2,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Home',
                'searchable'    => 1
            ],
            [
                'page_id'       => 1,
                'lang'          => 2,
                'position'      => 1,
                'section'       => 'keywords',
                'format'        => 1,
                'body'          => 'Brand & Design Navigator bietet einfachen Zugang und schnelle Orientierung in der facettenreichen Welt des Corporate Design bei Daimler.',
                'searchable'    => 1
            ],
            [
                'page_id'       => 1,
                'lang'          => 2,
                'position'      => 2,
                'section'       => 'banner',
                'format'        => 1,
                'body'          => '<img src="img/banner.jpg">',
                'searchable'    => 1
            ],
            [
                'page_id'       => 1,
                'lang'          => 2,
                'position'      => 3,
                'section'       => 'main',
                'format'        => 1,
                'body'          => 'Home page content DE',
                'searchable'    => 1
            ],
            [
                'page_id'       => 1,
                'lang'          => 2,
                'position'      => 4,
                'section'       => 'farRight',
                'format'        => 1,
                'body'          => '<BR><IMG style="border: none; width: 65px; height: 103px;" alt="iF communication design award" align="left" src="img/brand_mark_ifaward.gif">&nbsp;<BR><BR><BR><BR><BR><BR><STRONG>Ausgezeichnetes <BR>Corporate Design</STRONG></SPAN><BR><SPAN class=normal>F&uuml;r sein Erscheinungsbild wurde Daimler mit dem renommierten iF communication design award&nbsp;ausgezeichnet.<BR></SPAN><BR>',
                'searchable'    => 1
            ],
            # END HOMEPAGE CONTENT
            # EN TITLES + DEFAULT CONTENT
            [
                'page_id'       => 2,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Daimler',
                'searchable'    => 1
            ],
            [
                'page_id'       => 2,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => 'Daimler default contents EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 2,
                'lang'          => 2,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Daimler',
                'searchable'    => 1
            ],
            [
                'page_id'       => 2,
                'lang'          => 2,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => 'Daimler default contents DE',
                'searchable'    => 1
            ],
            [
                'page_id'       => 3,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Corporate Identity',
                'searchable'    => 1
            ],
            [
                'page_id'       => 3,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Corporate Identity</h1> default contents EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 3,
                'lang'          => 2,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Corporate Identity',
                'searchable'    => 1
            ],
            [
                'page_id'       => 3,
                'lang'          => 2,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Corporate Identity</h1> default contents DE',
                'searchable'    => 1
            ],
            [
                'page_id'       => 4,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Design Manuals',
                'searchable'    => 1
            ],
            [
                'page_id'       => 4,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Design Manuals</h1> default contents EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 4,
                'lang'          => 2,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Design Manuals',
                'searchable'    => 1
            ],
            [
                'page_id'       => 4,
                'lang'          => 2,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Design Manuals</h1> default contents DE',
                'searchable'    => 1
            ],
            [
                'page_id'       => 5,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Basic Elements',
                'searchable'    => 1
            ],
            [
                'page_id'       => 5,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Basic Elements</h1> default contents EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 5,
                'lang'          => 2,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Basiselemente',
                'searchable'    => 1
            ],
            [
                'page_id'       => 5,
                'lang'          => 2,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Basiselemente</h1> default contents DE',
                'searchable'    => 1
            ],
            [
                'page_id'       => 6,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Corporate Logotype',
                'searchable'    => 1
            ],
            [
                'page_id'       => 6,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Daimler Corporate Logotype.</h1><span class="gallery961">
<div class="galCropper" style="width: 480px; height: 375px;">
<div class="galMover">
<div class="galItem" unselectable="on" style="width: 480px; left: 0px;">
<a title="" href="https://designnavigator.daimler.com/img2/be_update/teasers/teaser_logotype_940X.jpg" rel="lytebox[gallery_1372938784545]" unselectable="on" immune="true" position="132" style="width: 480px;"><img src="https://designnavigator.daimler.com/img2/be_update/teasers/teaser_logotype_480X.jpg" border="0" unselectable="on"></a>
<div class="galNav">
<a title="teaser_logotype_940X.jpg" class="zoomlinkBox961" href="https://designnavigator.daimler.com/img2/be_update/teasers/teaser_logotype_940X.jpg" rel="lytebox[gallery_1372938784545]" immune="true" position="133">Zoom image</a> </div>
</div>
</div>
</div></span><br><span class="normal"><strong>No Alterations, No Decoration, No Modification.<br></strong>The <a href="index.php?id=196&amp;termID=414" class="tooltip"><span class="ina"><b>Corporate Logotype</b><br>It can be a picture, a word or a combination of words and pictures. Colloquially, it is often referr...</span>corporate logotype</a> is a graphic item that must not be altered. Any form of deviation from Corporate A font or another font is not allowed. Only original files may be used. The corporate logotype is only used on material that is of high quality and shows no signs of wear and tear. The character and significance of the logotype prohibit its use on trivial objects. <br><span class="gallery961">
<div class="galCropper" style="width: 480px; height: 231px;">
<div class="galMover">
<div class="galItem" unselectable="on" style="width: 480px; left: 0px;">
<a title="The corporate logotype is always presented in Daimler Blue." href="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_1_zeichen_p01_940X360.jpg" rel="lytebox[gallery_1372944300358]" unselectable="on" immune="true" position="134" style="width: 480px;"><img src="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_1_zeichen_p01_480X170_.jpg" border="0" unselectable="on"></a>
<div class="imgText961">The corporate logotype is always presented in Daimler Blue.</div>
<div class="galNav">
<a title="7_1_zeichen_p01_940X360.jpg" class="zoomlinkBox961" href="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_1_zeichen_p01_940X360.jpg" rel="lytebox[gallery_1372944300358]" immune="true" position="135">Zoom image</a> </div>
</div>
</div>
</div></span><br>The corporate logotype is not used as a decorative element. Inflated use in particular, for example as part of a background or pattern, is to be avoided (with the exception of <a href="index.php?id=196&amp;termID=404" class="tooltip"><span class="ina"><b>Sponsorship</b><br>Promotion of a company, product or service by supporting a sporting, cultural or social event.</span>sponsorship</a> walls). The logotype may never be reproduced, distorted, spaced or modified in any other way. <br><br><strong>Free Space.<br></strong>The <a href="index.php?id=196&amp;termID=343" class="tooltip"><span class="ina"><b>Free Space</b><br>Clearly defined space around a corporate logotype or brand mark in which no other element, e.g. text...</span>free space</a> on all sides around the corporate logotype must measure at least twice the height of the upper case letter "D". If it\'s not possible to use the corporate logotype with sufficient surrounding space, please refrain from using it. <br><span class="gallery961">
<div class="galCropper" style="width: 480px; height: 261px;">
<div class="galMover">
<div class="galItem" unselectable="on" style="width: 480px; left: 0px;">
<a title="The free space around the corporate logotype is at least 2D." href="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_2_zeichen_p01_940X395_.jpg" rel="lytebox[gallery_1372944323887]" unselectable="on" immune="true" position="136" style="width: 480px;"><img src="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_2_zeichen_p01_480X200_.jpg" border="0" unselectable="on"></a>
<div class="imgText961">The free space around the corporate logotype is at least 2D.</div>
<div class="galNav">
<a title="7_2_zeichen_p01_940X395_.jpg" class="zoomlinkBox961" href="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_2_zeichen_p01_940X395_.jpg" rel="lytebox[gallery_1372944323887]" immune="true" position="137">Zoom image</a> </div>
</div>
</div>
</div></span><br><strong>Positioning.<br></strong>In all formats and layouts, the corporate logotype is in principle positioned horizontally centered. However, some media, e.g., presentation material or flags require a position that deviates from this rule. <br><span class="gallery961">
<div class="galCropper" style="width: 480px; height: 401px;">
<div class="galMover">
<div class="galItem" unselectable="on" style="width: 480px; left: 0px;">
<a title="The corporate logotype is always set in the center." href="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_3_zeichen_p01_940X665_.jpg" rel="lytebox[gallery_1372944347122]" unselectable="on" immune="true" position="138" style="width: 480px;"><img src="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_3_zeichen_p01_480X340_.jpg" border="0" unselectable="on"></a>
<div class="imgText961">The corporate logotype is always set in the center.</div>
<div class="galNav">
<a title="7_3_zeichen_p01_940X665_.jpg" class="zoomlinkBox961" href="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_3_zeichen_p01_940X665_.jpg" rel="lytebox[gallery_1372944347122]" immune="true" position="139">Zoom image</a> </div>
</div>
</div>
</div></span><br><strong>Additions to the Corporate Logotype.<br></strong>As the logotype represents a self-contained unit, additions in written form, as a word and <a href="index.php?id=196&amp;termID=375" class="tooltip"><span class="ina"><b>Brand Mark</b><br>The visual depiction of a brand in the form of a logo and/or mark designation.</span>brand mark</a>, are not allowed. <br><br><strong>Copyright Protection.<br></strong>The corporate logotype is legally protected. Its use by third parties always requires the approval of Daimler AG (through the Daimler legal department or&nbsp;Corporate Design) in all cases. <br><br><strong>Divisional, Project and Promotional Logos.<br></strong>Divisional logos are not permitted in either internal or external communication at Daimler. Project and promotional logos (which, as opposed to divisional logos, are used for a limited time) are allowed, but they are not usually depicted in the immediate vicinity of the corporate logotype. Key visuals in form of images and/or graphics can be used as leitmotiv for an <a href="index.php?id=196&amp;termID=326" class="tooltip"><span class="ina"><b>Event</b><br>Events refer to "stage-managed" events and their planning and organization as part of corporate comm...</span>event</a>. They may, however, be used for a limited amount of time only and are not allowed to be used as a permanent "divisional identification". </span><br><br><br>',
                'searchable'    => 1
            ],
            [
                'page_id'       => 6,
                'lang'          => 2,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Unternehmenszeichen',
                'searchable'    => 1
            ],
            [
                'page_id'       => 6,
                'lang'          => 2,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Daimler-Unternehmenszeichen.</h1><span class="gallery961">
<div class="galCropper" style="width: 480px; height: 375px;">
<div class="galMover">
<div class="galItem" unselectable="on" style="width: 480px; left: 0px;">
<a title="" href="https://designnavigator.daimler.com/img2/be_update/teasers/teaser_logotype_940X.jpg" rel="lytebox[gallery_1372938784545]" unselectable="on" immune="true" position="132" style="width: 480px;"><img src="https://designnavigator.daimler.com/img2/be_update/teasers/teaser_logotype_480X.jpg" border="0" unselectable="on"></a>
<div class="galNav">
<a title="teaser_logotype_940X.jpg" class="zoomlinkBox961" href="https://designnavigator.daimler.com/img2/be_update/teasers/teaser_logotype_940X.jpg" rel="lytebox[gallery_1372938784545]" immune="true" position="133">Vergrößern</a> </div>
</div>
</div>
</div></span><br><span class="normal"><strong>Keine Nachbildungen, keine Dekoration, keine Modifizierung.<br></strong>Das <a href="index.php?id=196&amp;termID=276" class="tooltip"><span class="ina"><b>Unternehmenszeichen</b><br>Es kann aus einem bildhaften Zeichen, einem Wort oder einer Kombination aus Wort und Bild bestehen. ...</span>Unternehmenszeichen</a> ist eine Grafik, die nicht nachgebildet werden darf. Jede Art von Nachkonstruktion aus der Schriftart Corporate A oder aus irgendeiner anderen Schriftart ist nicht erlaubt. Es dürfen nur Original-Dateien eingesetzt werden. Die Anwendung des Unternehmenszeichens erfolgt nur auf Materialien, die qualitativ hochwertig sind und keine Abnutzungsspuren aufweisen. Der Charakter und die Bedeutung des Unternehmenszeichens verbieten seinen Einsatz auf Trivialgegenständen. <br><span class="gallery961">
<div class="galCropper" style="width: 480px; height: 231px;">
<div class="galMover">
<div class="galItem" unselectable="on" style="width: 480px; left: 0px;">
<a title="Das Unternehmenszeichen wird immer in Daimler Blue wiedergegeben." href="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_1_zeichen_p01_940X360.jpg" rel="lytebox[gallery_1372944300358]" unselectable="on" immune="true" position="134" style="width: 480px;"><img src="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_1_zeichen_p01_480X170_.jpg" border="0" unselectable="on"></a>
<div class="imgText961">Das Unternehmenszeichen wird immer in Daimler Blue wiedergegeben.</div>
<div class="galNav">
<a title="7_1_zeichen_p01_940X360.jpg" class="zoomlinkBox961" href="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_1_zeichen_p01_940X360.jpg" rel="lytebox[gallery_1372944300358]" immune="true" position="135">Vergrößern</a> </div>
</div>
</div>
</div></span><br>Das Unternehmenszeichen wird nicht als Dekorations- oder Zierelement eingesetzt. Insbesondere ist ein inflationärer Einsatz zu vermeiden, etwa die Verwendung als Hintergrund- oder Muster-Bestandteil (Ausnahme Sponsoringwand). Das Unternehmenszeichen darf nicht nachgesetzt, verzerrt, gesperrt oder in irgendeiner anderen Weise modifiziert werden. <br><br><strong>Freiraum.<br></strong>Der <a href="index.php?id=196&amp;termID=205" class="tooltip"><span class="ina"><b>Freiraum</b><br>Klar definierter Raum um ein Marken- oder Unternehmenszeichen, in dem keine anderen Elemente, wie z....</span>Freiraum</a> um das Unternehmenszeichen beträgt nach allen Seiten hin mindestens zweimal die <a href="index.php?id=196&amp;termID=278" class="tooltip"><span class="ina"><b>Versalhöhe</b><br>Höhe der Großbuchstaben. Zu Zeiten des Bleisatzes definierte die Kegelhöhe (Bleisockel auf dem der B...</span>Versalhöhe</a> "D". Kann der Freiraum um das Zeichen nicht eingehalten werden, wird auf seine Abbildung verzichtet. <br><span class="gallery961">
<div class="galCropper" style="width: 480px; height: 261px;">
<div class="galMover">
<div class="galItem" unselectable="on" style="width: 480px; left: 0px;">
<a title="Der Freiraum um das Unternehmenszeichen beträgt mindestens 2D." href="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_2_zeichen_p01_940X395_.jpg" rel="lytebox[gallery_1372944323887]" unselectable="on" immune="true" position="136" style="width: 480px;"><img src="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_2_zeichen_p01_480X200_.jpg" border="0" unselectable="on"></a>
<div class="imgText961">Der Freiraum um das Unternehmenszeichen beträgt mindestens 2D.</div>
<div class="galNav">
<a title="7_2_zeichen_p01_940X395_.jpg" class="zoomlinkBox961" href="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_2_zeichen_p01_940X395_.jpg" rel="lytebox[gallery_1372944323887]" immune="true" position="137">Vergrößern</a> </div>
</div>
</div>
</div></span><br><strong>Platzierung.<br></strong>Das Unternehmenszeichen steht in allen Formaten und Layout-Flächen grundsätzlich waagerecht und wird zentriert platziert. Verschiedene Medien, z.B. Präsentationsvorlagen oder Flaggen, erfordern jedoch eine von dieser Regel abweichende Position des Unternehmenszeichens. <br><span class="gallery961">
<div class="galCropper" style="width: 480px; height: 401px;">
<div class="galMover">
<div class="galItem" unselectable="on" style="width: 480px; left: 0px;">
<a title="Das Unternehmenszeichen wird immer zentriert eingesetzt." href="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_3_zeichen_p01_940X665_.jpg" rel="lytebox[gallery_1372944347122]" unselectable="on" immune="true" position="138" style="width: 480px;"><img src="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_3_zeichen_p01_480X340_.jpg" border="0" unselectable="on"></a>
<div class="imgText961">Das Unternehmenszeichen wird immer zentriert eingesetzt.</div>
<div class="galNav">
<a title="7_3_zeichen_p01_940X665_.jpg" class="zoomlinkBox961" href="https://designnavigator.daimler.com/img2/be_update/basiselemente_2/7_3_zeichen_p01_940X665_.jpg" rel="lytebox[gallery_1372944347122]" immune="true" position="139">Vergrößern</a> </div>
</div>
</div>
</div></span><br><strong>Zusätze zum Unternehmenszeichen.<br></strong>Da das Unternehmenszeichen eine in sich geschlossene Einheit darstellt, sind Zusätze in Schriftform, als Wort- und Bildmarke, nicht erlaubt. <br><br><strong>Urheberschutz.<br></strong>Das Unternehmenszeichen ist rechtlich geschützt. Seine Verwendung durch Dritte bedarf immer der Genehmigung durch die Daimler AG (Rechtsabteilung oder&nbsp;Corporate Design). <br><br><strong>Bereichs-, Projekt- und Aktionslogos.<br></strong>Bereichslogos sind sowohl in der internen als auch in der externen Kommunikation bei Daimler nicht zulässig. Projekt- und Aktionslogos, die im Gegensatz zu Bereichslogos zeitlich begrenzt sind, sind grundsätzlich erlaubt, werden&nbsp;in&nbsp;der Regel aber nicht in unmittelbarer Nähe zum Unternehmenszeichen&nbsp;abgebildet. Key-Visuals in Form von Bildern und/oder Grafiken können als Leitmotiv einer Veranstaltung eingesetzt werden. Sie haben aber immer eine zeitlich begrenzte Nutzungsdauer und sind als permanente "Bereichskennzeichnung" nicht erlaubt. </span><br><br><br>',
                'searchable'    => 1
            ],
            [
                'page_id'       => 7,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Best Practice',
                'searchable'    => 1
            ],
            [
                'page_id'       => 7,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Best Practice</h1> default contents EN
				<table>
					<tr><th>Lorem</th><th>ipsum</th><th>dolor</th></tr>
					<tr><td>Sit</td><td>amed</td><td>consectetur</td></tr>
					<tr><td>Pellentesque tristique</td><td>augue vitae massa</td><td>eleifend convallis</td></tr>
					<tr><td>Etiam rutrum</td><td>justo a lorem </td><td>dictum mattis</td></tr>
					</table>',
                'searchable'    => 1
            ],
            [
                'page_id'       => 7,
                'lang'          => 2,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Best Practice',
                'searchable'    => 1
            ],
            [
                'page_id'       => 7,
                'lang'          => 2,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Best Practice</h1> default contents DE',
                'searchable'    => 1
            ],
            [
                'page_id'       => 8,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Downloads',
                'searchable'    => 1
            ],
            [
                'page_id'       => 8,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Downloads</h1> default contents EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 8,
                'lang'          => 2,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Downloads',
                'searchable'    => 1
            ],
            [
                'page_id'       => 8,
                'lang'          => 2,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Downloads</h1> default contents DE',
                'searchable'    => 1
            ],
            [
                'page_id'       => 9,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Brochures and Flyers',
                'searchable'    => 1
            ],
            [
                'page_id'       => 9,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Brochures and Flyers</h1> default contents EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 9,
                'lang'          => 2,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Brosch&uuml;ren und Flyer',
                'searchable'    => 1
            ],
            [
                'page_id'       => 9,
                'lang'          => 2,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Brosch&uuml;ren und Flyer</h1> default contents DE',
                'searchable'    => 1
            ],
            [
                'page_id'       => 10,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Daimler A4 Brochure Templates',
                'searchable'    => 1
            ],
            [
                'page_id'       => 10,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Daimler A4 Brochure Templates</h1> default contents EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 10,
                'lang'          => 2,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Daimler-Brosch&uuml;renvorlagen (DIN A4)',
                'searchable'    => 1
            ],
            [
                'page_id'       => 10,
                'lang'          => 2,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Daimler-Brosch&uuml;renvorlagen (DIN A4).</h1> default contents DE',
                'searchable'    => 1
            ],
            [
                'page_id'       => 11,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Daimler A5 Brochure Templates',
                'searchable'    => 1
            ],
            [
                'page_id'       => 11,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Daimler A5 Brochure Templates</h1> default contents EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 11,
                'lang'          => 2,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Daimler-Brosch&uuml;renvorlagen (DIN A5)',
                'searchable'    => 1
            ],
            [
                'page_id'       => 11,
                'lang'          => 2,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Daimler-Brosch&uuml;renvorlagen (DIN A5).</h1> default contents DE',
                'searchable'    => 1
            ],
            [
                'page_id'       => 12,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Document Templates',
                'searchable'    => 1
            ],
            [
                'page_id'       => 12,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Document Templates</h1> default contents EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 12,
                'lang'          => 2,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Dokumentvorlagen',
                'searchable'    => 1
            ],
            [
                'page_id'       => 12,
                'lang'          => 2,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Dokumentvorlagen</h1> default contents DE',
                'searchable'    => 1
            ],
            [
                'page_id'       => 13,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Daimler Document Templates (Office 2010)',
                'searchable'    => 1
            ],
            [
                'page_id'       => 13,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Daimler Document Templates (Office 2010)</h1> default contents EN <span class="summary">Summary: Maecenas elementum sem non semper mattis. Etiam pharetra luctus massa non placerat. Praesent et pharetra lorem, ac fermentum odio. Pellentesque vulputate justo lacus, non tincidunt sapien laoreet a. Maecenas nec lorem eget urna euismod pretium ac at mi.</span>',
                'searchable'    => 1
            ],
            [
                'page_id'       => 13,
                'lang'          => 2,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Daimler-Dokumentvorlagen (Office 2010)',
                'searchable'    => 1
            ],
            [
                'page_id'       => 13,
                'lang'          => 2,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Daimler-Dokumentvorlagen (Office 2010)</h1> default contents DE <span class="summary">Summary: Maecenas elementum sem non semper mattis. Etiam pharetra luctus massa non placerat. Praesent et pharetra lorem, ac fermentum odio. Pellentesque vulputate justo lacus, non tincidunt sapien laoreet a. Maecenas nec lorem eget urna euismod pretium ac at mi.</span>',
                'searchable'    => 1
            ],
            [
                'page_id'       => 14,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Daimler Document Templates (Office 2003)',
                'searchable'    => 1
            ],
            [
                'page_id'       => 14,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Daimler Document Templates (Office 2003)</h1> default contents EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 14,
                'lang'          => 2,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Daimler-Dokumentvorlagen (Office 2003)',
                'searchable'    => 1
            ],
            [
                'page_id'       => 14,
                'lang'          => 2,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Daimler-Dokumentvorlagen (Office 2003)</h1> default contents DE',
                'searchable'    => 1
            ],
            [
                'page_id'       => 15,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Newsletter 3/2013',
                'searchable'    => 1
            ],
            [
                'page_id'       => 15,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Newsletter 3/2013</h1> default contents EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 16,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Daimler Corporate Design Communication',
                'searchable'    => 1
            ],
            [
                'page_id'       => 16,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Daimler Corporate Design Communication</h1> default contents EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 17,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Individual Plant Transportation System IWV.next with New Design',
                'searchable'    => 1
            ],
            [
                'page_id'       => 17,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Individual Plant Transportation System IWV.next with New Design</h1> default contents EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 18,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'The 6th Sustainability Dialogue at the Mercedes-Benz Museum',
                'searchable'    => 1
            ],
            [
                'page_id'       => 18,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>The 6th Sustainability Dialogue at the Mercedes-Benz Museum</h1> default contents EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 19,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Newsletter 2/2013',
                'searchable'    => 1
            ],
            [
                'page_id'       => 19,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Newsletter 2/2013</h1> default contents EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 20,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'New Corporate Photos',
                'searchable'    => 1
            ],
            [
                'page_id'       => 20,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>New Corporate Photos</h1> default contents EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 21,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Interactively Exploring Design Manuals',
                'searchable'    => 1
            ],
            [
                'page_id'       => 21,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Interactively Exploring Design Manuals</h1> default contents EN',
                'searchable'    => 1
            ],
            [
                'page_id'       => 22,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'smart',
                'searchable'    => 1
            ],
            [
                'page_id'       => 22,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>smart.<small>Consistency in the use of design elements enhances recognition ? and, with it, the degree of renown. This adds to the efficiency of advertising measures and individual initiatives, and campaigns can unequivocally be assigned to their originator (synergistic effects).</small></h1><br>
				<table>
					<tr><th>Lorem</th><th>ipsum</th><th>dolor</th></tr>
					<tr><td>Sit</td><td>amed</td><td>consectetur</td></tr>
					<tr><td>Pellentesque tristique</td><td>augue vitae massa</td><td>eleifend convallis</td></tr>
					<tr><td>Etiam rutrum</td><td>justo a lorem </td><td>dictum mattis</td></tr>
					</table>',
                'searchable'    => 1
            ],
            [
                'page_id'       => 23,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'The smart Brand',
                'searchable'    => 1
            ],
            [
                'page_id'       => 23,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>The smart Brand.</h1><br><br>
<span class="normal">
	<div class="block_482">
		<a class="hasZoom" title="" href="https://designnavigator.daimler.com/img2/smart2/smart_brand/smart_01_750X500.jpg" rel="lytebox[gallery_0]" position="89"><img border="0" src="https://designnavigator.daimler.com/img2/smart2/smart_brand/smart_01_480X320.jpg">&nbsp;</a><br>
		<a class="zoomlink" href="https://designnavigator.daimler.com/img2/smart2/smart_brand/smart_01_750X500.jpg" rel="lytebox[gallery_0]" position="90">Zoom image</a> <br>
		<em>smart ebike in crystal white with electric green accents&nbsp;and smart fortwo electric drive as coupé </em>
	</div><br>
	<strong>The smart brand stands for innovation, functionality and joy of life.</strong>
	smart products have an unconventional design, high technology standards and are the ideal vehicles for use in urban areas.
	True to the maxim urban mobility at its best, smart is the&nbsp;most innovative automotive think tank.
	By now smart is represented in 47 markets worldwide with the&nbsp;Russia as the latest new market.
	The main markets are Germany, Italy and China.
	<div class="block_482">
		<a class="hasZoom" title="" href="https://designnavigator.daimler.com/img2/smart2/smart_brand/smart_02_750X500.jpg" rel="lytebox[gallery_1]" position="91"><img border="0" src="https://designnavigator.daimler.com/img2/smart2/smart_brand/smart_02_480X320.jpg">&nbsp;</a><br>
		<a class="zoomlink" href="https://designnavigator.daimler.com/img2/smart2/smart_brand/smart_02_750X500.jpg" rel="lytebox[gallery_1]" position="92">Zoom image</a> <br>
		<em>smart fortwo 2012 cabrio </em>
	</div><br>
	<strong>The new 2012 smart fortwo model generation is the continuation of a unique success story for smart.</strong>
	As a trendsetter the smart fortwo has been redefining individual urban mobility for more than ten years now – with a high fun factor and ecological standards.
	With its compact dimensions the two-seater takes up less road and parking space than any other car around and it also sets new safety standards: the tridion safety cell is regarded as a safety milestone in the small car class.
	And the smart fortwo is also a pioneer in its segment in several respects with regard to drive technology. <br><br><br>
</span>',
                'searchable'    => 1
            ],
            [
                'page_id'       => 24,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Design Manuals',
                'searchable'    => 1
            ],
            [
                'page_id'       => 24,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Design Manuals.</h1>',
                'searchable'    => 1
            ],
            [
                'page_id'       => 25,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Downloads',
                'searchable'    => 1
            ],
            [
                'page_id'       => 25,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Downloads.</h1>',
                'searchable'    => 1
            ],
            [
                'page_id'       => 26,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Fixed Elements',
                'searchable'    => 1
            ],
            [
                'page_id'       => 26,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Fixed Elements.</h1>',
                'searchable'    => 1
            ],
            [
                'page_id'       => 27,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Logo, Label and Claim',
                'searchable'    => 1
            ],
            [
                'page_id'       => 27,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>The smart Logo, Label and Claim.</h1>',
                'searchable'    => 1
            ],
            [
                'page_id'       => 28,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Logo, Label and Claim',
                'searchable'    => 1
            ],
            [
                'page_id'       => 28,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>smart Logo, Label and Claim.</h1>',
                'searchable'    => 1
            ],
            [
                'page_id'       => 29,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'smart Downloads',
                'searchable'    => 1
            ],
            [
                'page_id'       => 29,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>smart Downloads.</h1>',
                'searchable'    => 1
            ],
            [
                'page_id'       => 30,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'About Brand &amp; Design Navigator',
                'searchable'    => 1
            ],
            [
                'page_id'       => 30,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>Daimler\'s Corporate Design Gives the Company a Unique Visual Identity.</h1><br><span class="normal"><b>The combination of&nbsp;basic elements, corporate logotype, corporate typeface, color system, design principles and photographic style ensures a unified brand image worldwide.</b><br><span class="gallery961">
<div class="galCropper" style="width: 480px; height: 395px;">
<div class="galMover">
<div class="galItem" unselectable="on" style="width: 480px; left: 0px;">
<a title="" href="https://designnavigator.daimler.com/img2/ci_update/dbdn_neu_940X_.jpg" rel="lytebox[gallery_1372943333930]" unselectable="on" immune="true" style="width: 480px;"><img border="0" src="https://designnavigator.daimler.com/img2/ci_update/dbdn_neu_480X_.jpg" unselectable="on"></a>
<div class="galNav">
<a class="zoomlinkBox961" title="dbdn_neu_940X_.jpg" href="https://designnavigator.daimler.com/img2/ci_update/dbdn_neu_940X_.jpg" rel="lytebox[gallery_1372943333930]" immune="true">Zoom image</a> </div>
</div>
</div>
</div></span><br><span class="normal">Corporate Design makes things visible that may not be apparent at first glance – the consistent and distinctive design language that, as a whole, represents Daimler\'s visual <a href="index.php?id=196&amp;termID=355" class="tooltip"><span class="ina"><b>Identity</b><br>The unmistakable profile of a corporate, brand, product or service personality.</span>identity</a>. Just as the Company is developing dynamically all the time, its Corporate Design is also subject to constant change within defined limits. Corporate Design as a living system: This philosophy shapes the content and design concepts in the <a href="index.php?id=196&amp;termID=314" class="tooltip"><span class="ina"><b>Brand &amp; Design Navigator</b><br>Web-based online tool, in which the visual appearance of  Daimler AG is documented. </span>Brand &amp; Design Navigator</a>.&nbsp; </span><br><span class="normal"><br>
<strong>The content of the Daimler Brand &amp; Design Navigator is a binding policy. The policy is documented in the Employee Portal in the ERD under the heading "Company,” subheading “Policies &amp; Guidelines,” module “House of Policies.”&nbsp; </strong><br>
<br><a class="externallink" href="https://designnavigator.daimler.com/index.php?id=191&amp;link=http%3A//erd.e.corpintra.net/Default.aspx%3Fmode%3Ddetail%26ruleId%3D2330%26contentLang%3Den-US" position="120">Internal link</a>&nbsp;<br>(internal access only)</span> <br><br><span class="normal">The Brand &amp; Design Navigator supports employees and external service providers with information on the Company\'s Corporate Design and its brands – from the <a href="index.php?id=196&amp;termID=292" class="tooltip"><span class="ina"><b>Basic Elements</b><br>This term is used within Corporate Design to refer to the key basic design elements: corporate logot...</span>basic elements</a> to their practical application to download options for design templates.&nbsp; In the Best Practice examples, the magazine in the Brand &amp; Design Navigator, we regularly present outstanding sample designs from the Group. Registered users can share their own projects, comment on the samples, and exchange experiences with each other. <br><br></span><span class="normal">If you have any questions on the Daimler Brand &amp; Design Navigator, please send an e-mail to&nbsp; <a class="contactlink" href="mailto:corporate.design@daimler.com" position="121">corporate.design@daimler.com</a>&nbsp;or by using the <a class="readmore" href="https://designnavigator.daimler.com/index.php?id=974" position="122">Dialogue form</a>.</span> <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
</span>',
                'searchable'    => 1
            ],
            [
                'page_id'       => 31,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Branding',
                'searchable'    => 1
            ],
            [
                'page_id'       => 31,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>The Significance of Branding in Communications</h1>
				<span class="normal"><br><strong>Daimler understands branding to be the use of its company name, its Daimler corporate logotype and its Corporate Design in all internal and external presentation of the brand. </strong><br><br>A key aspect of good <a href="index.php?id=196&amp;termID=373" class="tooltip"><span class="ina"><b>Brand Management</b><br>The development, management and execution of marketing plans for the brands, products and services o...</span>brand management</a> is the combination of excellent visual presentation, intelligent use of design elements and <a href="index.php?id=196&amp;termID=360" class="tooltip"><span class="ina"><b>Integrated Communication</b><br>The coordination of all marketing activities of a company with the aim of strengthening a brand.</span>integrated communication</a> over a range of information levels. Daimler\'s aim is for its employees, customers and target groups to make a direct visual connection between their positive experiences with the company\'s services and the brand <a href="index.php?id=196&amp;termID=356" class="tooltip"><span class="ina"><b>Image</b><br>The publicly perceived brand image of a company, brand, product or service. Determined primarily by ...</span>image</a>. Therefore, “encountering” the company by means of direct contact with employees, products and services is another image-defining form of corporate branding.</span>
<div class="block_482">
<a class="hasZoom" href="https://designnavigator.daimler.com/img2/ci/1802_2_750x500.jpg" rel="lytebox[gallery_0]" position="189"><img border="0" src="https://designnavigator.daimler.com/img2/ci/1802_1_480x320.jpg"></a><a class="textLink" href="https://designnavigator.daimler.com/img2/ci/1802_2_750x500.jpg" rel="lytebox[gallery_1]" position="190">Zoom image</a>
</div>
<div class="block_gallery_teaser">
<a class="hasZoom" href="https://designnavigator.daimler.com/img2/ci/1802_9_750x500.jpg" rel="lytebox[gallery_2]" position="191"><img src="https://designnavigator.daimler.com/img2/ci/1802_2_120x80.jpg"></a><a class="hasZoom" href="https://designnavigator.daimler.com/img2/ci/1802_3_750x500.jpg" rel="lytebox[gallery_3]" position="192"><img src="https://designnavigator.daimler.com/img2/ci/1802_3_120x80.jpg"></a><a class="hasZoom" href="https://designnavigator.daimler.com/img2/ci/1802_4_750x500.jpg" rel="lytebox[gallery_4]" position="193"><img src="https://designnavigator.daimler.com/img2/ci/1802_4_120x80.jpg"></a>
</div>
<span class="normal">Branding and its consistent implementation in communications is of key significance at Daimler: A striking Corporate Design in communication media has a positive impact on recognition and perception of the company. The levers behind this are to be found not only in the virtual presence of the <a href="index.php?id=196&amp;termID=308" class="tooltip"><span class="ina"><b>Corporate Brand</b><br>The entirety of all cultural and physical properties of an organization including its philosophy.</span>corporate brand</a> itself (by means of a “self-similar” – i.e. striking, differentiating and consistent – configuration), but also in the consistent use of design elements, particularly the Daimler logotype and the brand name, in communications and design elements in terms of <a href="index.php?id=196&amp;termID=351" class="tooltip"><span class="ina"><b>Corporate Typeface</b><br>The font with which a company identifies and presents itself externally and internally. At Daimler C...</span>corporate typeface</a>, <a href="index.php?id=196&amp;termID=333" class="tooltip"><span class="ina"><b>Color System</b><br>Is the controlled interaction of several colors. Daimler\'s color system contains the primary color w...</span>color system</a>, design principles and photographic style. The design thus acts as a visual “marker” in the minds of target groups, facilitating orientation in the wide field of competition. Here, Daimler acts either as a sole originator or in the context of the <a href="index.php?id=196&amp;termID=323" class="tooltip"><span class="ina"><b>Endorsement</b><br>The use of an corporate brand in order to endorse a sub-brand. The degree of endorsement varies from...</span>endorsement</a> in collaboration with the product brands. </span><br><br><span class="normal">To build on and secure the brand\'s strength in the long term, Daimler maintains a consistent media presence wherever possible: on the Internet, at events (e.g. Annual General Meeting) and trade shows, in publications and audiovisual media (e.g. annual report, quarterly reports, brochures, videos), on information signs, in image advertisements, etc. Frequent contact with the corporate brand and continuity in branding makes it easier for the target groups to experience the Daimler brand.</span>
<div class="block_482">
<a class="hasZoom" href="https://designnavigator.daimler.com/img2/ci/1802_5_750x500.jpg" rel="lytebox[gallery_5]" position="194"><img border="0" src="https://designnavigator.daimler.com/img2/ci/1802_5_480x320.jpg"></a><a class="textLink" href="https://designnavigator.daimler.com/img2/ci/1802_5_750x500.jpg" rel="lytebox[gallery_6]" position="195">Zoom image</a>
</div>
<div class="block_gallery_teaser">
<a class="hasZoom" href="https://designnavigator.daimler.com/img2/ci/1802_6_750x500.jpg" rel="lytebox[gallery_7]" position="196"><img src="https://designnavigator.daimler.com/img2/ci/1802_6_120x80.jpg"></a><a class="hasZoom" href="https://designnavigator.daimler.com/img2/ci/1802_7_750x500.jpg" rel="lytebox[gallery_8]" position="197"><img src="https://designnavigator.daimler.com/img2/ci/1802_7_120x80.jpg"></a><a class="hasZoom" href="https://designnavigator.daimler.com/img2/ci/1802_8_750x500.jpg" rel="lytebox[gallery_9]" position="198"><img src="https://designnavigator.daimler.com/img2/ci/1802_8_120x80.jpg"></a>
</div>
<span class="normal">In terms of the different target groups of the Daimler corporate brand, this means that consistent implementation of branding in communications has a positive effect on employee identification and motivation. Developing a global corporate <a href="index.php?id=196&amp;termID=355" class="tooltip"><span class="ina"><b>Identity</b><br>The unmistakable profile of a corporate, brand, product or service personality.</span>identity</a> supports the attractiveness of the company in the eyes of highly qualified staff, builds confidence in future viability, attracts potential shareholders and, last but not least, supports sales of the product brands. </span><br><br><br><br>',
                'searchable'    => 1
            ],
            [
                'page_id'       => 32,
                'lang'          => 1,
                'position'      => 0,
                'section'       => 'title',
                'format'        => 1,
                'body'          => 'Brand Architecture',
                'searchable'    => 1
            ],
            [
                'page_id'       => 32,
                'lang'          => 1,
                'position'      => 1,
                'section'       => 'main',
                'format'        => 1,
                'body'          => '<h1>The Corporate Brand and Its Relationship with the Product Brands</h1>
<span class="normal"><br><strong>The Daimler name stands for a wide portfolio of well-known car and commercial vehicle brands as well as international success in automotive construction. <br></strong><br>With production facilities and state-of-the-art research and development centers on five continents, international collaborations and joint ventures, Daimler fulfills its role as a strong umbrella brand. Management of the <a href="index.php?id=196&amp;termID=308" class="tooltip"><span class="ina"><b>Corporate Brand</b><br>The entirety of all cultural and physical properties of an organization including its philosophy.</span>corporate brand</a>, the development of a corporate <a href="index.php?id=196&amp;termID=355" class="tooltip"><span class="ina"><b>Identity</b><br>The unmistakable profile of a corporate, brand, product or service personality.</span>identity</a> and strategic relationships between the corporate brand and product brands are controlled within the framework of the brand architecture.</span>
<div class="block_482">
<a class="hasZoom" href="https://designnavigator.daimler.com/img2/ci/1801_1_572x600.jpg" rel="lytebox[gallery_0]" position="137"><img border="0" src="https://designnavigator.daimler.com/img2/ci/1801_1_480x503.jpg"></a><a class="textLink" href="https://designnavigator.daimler.com/img2/ci/1801_1_572x600.jpg" rel="lytebox[gallery_1]" position="138">Zoom image</a>
</div>
<span class="normal"><br>Daimler has decided on the “Branded House of Brands” (house of brands with endorsements) for its brand architecture model. Strong product brands in different positions (Mercedes-Benz, Smart, Setra, Freightliner, etc.) are the main focus in this form of <a href="index.php?id=196&amp;termID=373" class="tooltip"><span class="ina"><b>Brand Management</b><br>The development, management and execution of marketing plans for the brands, products and services o...</span>brand management</a>. They are connected to the Daimler corporate brand by means of the <a href="index.php?id=196&amp;termID=323" class="tooltip"><span class="ina"><b>Endorsement</b><br>The use of an corporate brand in order to endorse a sub-brand. The degree of endorsement varies from...</span>endorsement</a> “A Daimler Brand”. This strategy makes optimum use of communication synergies between the product brands and corporate brand and gives the product brands the widest possible scope for establishing an independent position. <br></span><br><span class="normal">The <strong>endorsements</strong> that are used with the Group\'s product brands are a clear consequence of deciding on this brand strategy. The following endorsements have been defined: </span>
<p>&nbsp;</p>
<span class="normal">
<ul>
<li>“A Daimler Brand”: For brands where Daimler AG is the brand owner</li>
<li>“A Daimler Group Brand”: For brands where subsidiaries are the brand owners.</li>
</ul></span> <span class="normal">The product brands that use these endorsements and thus publically establish a relationship with the Daimler umbrella brand fulfill an important function for themselves and the umbrella brand with regard to target groups – and are a clear indication of which company is behind the product brand displayed. This transfer of information takes place via the marketing communications of the product brands. Independent corporate entities (subsidiaries) are labeled with the endorsement “A Daimler Company” in communications.</span><br><br><span class="normal">The strongest brand in the Group, Mercedes-Benz, plays an important role here, since the world\'s most valuable automobile brand, with the latest vehicle models and products, is integrated in overall Group events held by Daimler AG. Synergies are boosted thanks to the high recognition of the Mercedes-Benz star and <a href="index.php?id=196&amp;termID=375" class="tooltip"><span class="ina"><b>Brand Mark</b><br>The visual depiction of a brand in the form of a logo and/or mark designation.</span>brand mark</a>.</span><br><br><br><br>',
                'searchable'    => 1
            ],

        ];

        // Uncomment the below to run the seeder
         DB::table('content')->insert($content);
    }
}

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Frontend Routes for services these routes are no needed anymore */
//Route::group(['prefix' => 'v1/'], function () {
//    Route::get('tag/source/{lang}', 'TagController@getSource')->where('lang', '[0-9]+');
//    Route::get('faq/faqbyid/{$id}', 'TagController@getSource')->where('lang', '[0-9]+');
//    Route::get('faq/allfaqs/{$lang}', 'TagController@getSource')->where('lang', '[0-9]+');
//});

Route::prefix('v1')->middleware('ws_error')->group(function () {

    Route::middleware('be_access_approver')->group(function () {
        Route::delete('pages/permanentdelete/{id}', 'PageController@deletePermanentdelete'); // Added access
        Route::delete('files/permanentdelete/{id}', 'FileController@deletePermanentdelete');

        Route::prefix('tag')->group(function () {
            Route::get('all/{lang?}', 'TagController@getAll');
            Route::delete('delete/{id}', 'TagController@deleteDelete');
            // Currently not used! Tags are created from page properties!
            // Route::get('read/{id?}', 'TagController@getRead');
            // Route::post('create', 'TagController@postCreate');
            // Route::put('update', 'TagController@putUpdate');
        });

        // GlossaryController
        Route::prefix('glossary')->group(function () {
            Route::get('glossary/{page_id}', 'GlossaryController@getGlossary');
        });
    });

    Route::middleware('be_access_cms')->group(function () {

        // PageController
        Route::prefix('pages')->group(function () {
            Route::get('read/{id?}/{load_access?}', 'PageController@getRead'); // Added Access and Tests
            Route::get('responsible', 'PageController@getResponsible'); //I cannot find it but there is a Test for it in Newsletter
            Route::get('all', 'PageController@getAll'); // Added Access and Tests
            Route::get('search', 'PageController@getSearch'); // Added Access
            Route::get('deleted/{lang?}', 'PageController@getDeleted'); // Added Access
            Route::get('isavailable/{page_id}', 'PageController@getIsavailable'); // Added Access and Tests
            Route::get('notes/{page_id}', 'PageController@getNotes'); // Added Access and Tests
            Route::get('versions/{id?}', 'PageController@getVersions'); // Added Access and Tests
            Route::post('create', 'PageController@postCreate'); // Added Access and Tests
            Route::post('copy', 'PageController@postCopy'); // Added Access and Tests
            Route::post('clearcache', 'PageController@postClearcache'); // No page access check! Clean blades cache of Laravel!
//            Route::post('addrelated', 'PageController@postAddrelated'); // Not in use at the moment!
            Route::post('appendtime', 'PageController@postAppendtime'); // Added Access
            Route::post('unlock', 'PageController@postUnlock'); // Added Access
            Route::post('versioncopy', 'PageController@postVersioncopy'); // Added Access and Tests
            Route::put('update/{params?}', 'PageController@putUpdate'); // Added Access and Tests
            Route::put('sendalert/{id}', 'PageController@putSendalert'); // Added Access and Tests
            Route::put('restore', 'PageController@putRestore'); // Added Access
//            Route::put('editrelated', 'PageController@putEditrelated'); // Not in use at the moment!
            Route::delete('delete/{id?}', 'PageController@deleteDelete'); // Added Access and Tests
//            Route::delete('permanentdelete/{id?}', 'PageController@deletePermanentdelete'); // Added Access
//            Route::delete('removerelated', 'PageController@deleteRemoverelated'); // Not in use at the moment!
         });

        // ContentController
        Route::prefix('contents')->group(function () {
            Route::get('read/{id?}', 'ContentController@getRead')->middleware('test_enviroment'); // Not in use! Only for JUnit tests
            Route::get('page-id/{page_id?}/{lang?}', 'ContentController@getPageId'); //Added Access and Tests
            Route::post('create', 'ContentController@postCreate'); //Added Access
            Route::post('related', 'ContentController@postRelated'); //Added Access
            Route::post('reset-related', 'ContentController@postResetRelated'); //Added Access and Tests
            Route::post('create-many', 'ContentController@postCreateMany'); //Added Access
            Route::put('update', 'ContentController@putUpdate'); //Added Access
            Route::put('update-many', 'ContentController@putUpdateMany'); //Added Access
            Route::delete('delete/{id?}', 'ContentController@deleteDelete')->middleware('test_enviroment'); // Not in use! Only for JUnit tests
        });

        // CartController
        Route::prefix('cart')->group(function () {
            Route::get('cartitems/{user_id?}', 'CartController@getCartitems');
            Route::post('additemtocart', 'CartController@postAdditemtocart');
            Route::delete('removecartitem', 'CartController@deleteRemovecartitem');
            Route::delete('emptycart/{user_id?}', 'CartController@deleteEmptycart');
        });

        // DownloadController
        Route::prefix('download')->group(function () {
            Route::get('downloads/{page_id?}', 'DownloadController@getDownloads');
            Route::post('createdownload', 'DownloadController@postCreatedownload');
            Route::post('recreate', 'DownloadController@postRecreate');
            Route::put('modifydownload', 'DownloadController@putModifydownload');
            Route::delete('removedownload', 'DownloadController@deleteRemovedownload');
            Route::delete('removedownloadbypage', 'DownloadController@deleteRemovedownloadbypage');
        });

        // FavoriteController
        Route::prefix('favorite')->group(function () {
            Route::get('favorites/{user_id?}', 'FavoriteController@getFavorites');
            Route::post('createfavorite', 'FavoriteController@postCreatefavorite');
            Route::delete('removefavorite', 'FavoriteController@deleteRemovefavorite');
            Route::post('insertuser', 'FavoriteController@postInsertuser');
            Route::post('deleteuser', 'FavoriteController@postDeleteuser');
        });

        // TermController
        Route::prefix('term')->group(function () {
            Route::get('allterms/{lang?}', 'TermController@getAllterms');
            Route::get('termbyid/{id?}', 'TermController@getTermbyid');
            Route::post('create', 'TermController@postCreate');
            Route::put('update', 'TermController@putUpdate');
            Route::delete('delete/{id?}', 'TermController@deleteDelete');
        });

        // AccessController
        Route::prefix('access')->group(function () {
            Route::get('readbypage/{page_id?}', 'AccessController@getReadbypage');
            Route::post('grant', 'AccessController@postGrant');
            Route::delete('revoke', 'AccessController@deleteRevoke');
        });

        // RatingController
        Route::prefix('rating')->group(function () {
            Route::get('read', 'RatingController@getRead');
            Route::get('readavg/{page_id}', 'RatingController@getReadavg');
            Route::post('create', 'RatingController@postCreate');
            Route::put('modify', 'RatingController@putModify');
            Route::delete('delete', 'RatingController@deleteDelete');
        });

        // SubscriptionController
        Route::prefix('subscription')->group(function () {
            Route::get('readbyuser/{user_id?}', 'SubscriptionController@getReadbyuser');
            Route::get('readbypage/{page_id?}', 'SubscriptionController@getReadbypage');
            Route::post('create', 'SubscriptionController@postCreate');
            Route::delete('delete', 'SubscriptionController@deleteDelete');
        });

        // GalleryController
        Route::prefix('gallery')->group(function () {
            Route::get('images/{page_id?}', 'GalleryController@getImages');
            Route::post('create', 'GalleryController@postCreate');
            Route::put('update/{page_id?}', 'GalleryController@putUpdate');
            Route::delete('remove', 'GalleryController@deleteRemove');
        });

        // WorkflowController
        Route::prefix('workflow')->group(function () {
//            Route::get('read/{id?}', 'WorkflowController@getRead');
//            Route::post('create', 'WorkflowController@postCreate');
            Route::put('update', 'WorkflowController@putUpdate'); // Added Access and Tests
            Route::put('changeresponsible', 'WorkflowController@putChangeresponsible'); // Added Access and Tests
//            Route::delete('delete/{id?}', 'WorkflowController@deleteDelete');
        });

        // TagController
        Route::prefix('tag')->group(function () {
            Route::get('source/{lang?}', 'TagController@getSource');
        });
    });

    Route::middleware('be_access_nl_or_cms')->group(function () {
        // FileController
        Route::prefix('files')->group(function () {
//            Route::get('full-path/{filemanager_mode?}/{path?}/{is_protected?}/{is_deleted?}', 'FileController@getFullPath');
//            Route::get('folder-status/{filemanager_mode?}/{path?}/{is_deleted?}', 'FileController@getFolderStatus');
            Route::get('read/{id?}', 'FileController@getRead');
            Route::get('discription/{id?}', 'FileController@getDiscription');
//            Route::get('resized-image/{filemanager_mode}/{file_path_cached}/{is_protected?}', 'FileController@getResizedImage');
            Route::get('all/{filemanager_mode}', 'FileController@getAll');
//            Route::get('filesbytype', 'FileController@getFilesbytype');
            Route::get('subfolders', 'FileController@getSubfolders');
            Route::get('isavailable/{file_id}', 'FileController@getIsavailable');
            Route::post('createfile', 'FileController@postCreatefile');
            Route::post('upload', 'FileController@postUpload');
            Route::post('pdfupload', 'FileController@postPdfupload');
            Route::post('updatefile/{id?}', 'FileController@postUpdatefile');
            Route::post('makedir/{filemanager_mode}', 'FileController@postMakedir');
            Route::post('appendtime', 'FileController@postAppendtime');
            Route::post('unlock', 'FileController@postUnlock');
            Route::put('restore/{id?}', 'FileController@putRestore');
            Route::delete('deletefile/{id?}/{is_permanent?}/{skip_relation_check?}', 'FileController@deleteDeletefile');
            Route::delete('permanentdelete/{id?}', 'FileController@deletePermanentdelete');

            Route::delete('deletedir/{filemanager_mode}', 'FileController@deleteDeletedir');
        });
    });

    Route::middleware('be_access_nl')->group(function () {

        // NewsletterApiController
        Route::prefix('newsletter')->group(function (){
            Route::get('read/{id?}', 'NewsletterApiController@getRead');
            Route::get('responsible', 'NewsletterApiController@getResponsible');
            Route::get('all', 'NewsletterApiController@getAll');
            Route::get('search', 'NewsletterApiController@getSearch');
            Route::get('package/{id?}', 'NewsletterApiController@getPackage');
            Route::get('isavailable/{newsletter_id}', 'NewsletterApiController@getIsavailable');
            Route::post('send', 'NewsletterApiController@postSend');
            Route::post('sendtest', 'NewsletterApiController@postSendtest');
            Route::post('appendtime', 'NewsletterApiController@postAppendtime');
            Route::post('unlock', 'NewsletterApiController@postUnlock');
            Route::post('copy', 'NewsletterApiController@postCopy');
            Route::post('create', 'NewsletterApiController@postCreate');
            Route::put('update', 'NewsletterApiController@putUpdate');
            Route::put('changeresponsible', 'NewsletterApiController@putChangeresponsible');
            Route::delete('delete/{id?}', 'NewsletterApiController@deleteDelete');
        });

        // NewsletterContentController
        Route::prefix('nl_contents')->group(function (){
            Route::get('read/{id?}', 'NewsletterContentController@getRead');
            Route::get('newsletter-content/{nl_id}/{section?}/{lang?}', 'NewsletterContentController@getNewsletterContent');
            Route::get('copy-footer/{lang?}/{newsletter_id}', 'NewsletterContentController@getCopyFooter');
            Route::post('create', 'NewsletterContentController@postCreate');
            Route::post('create-many', 'NewsletterContentController@postCreateMany');
            Route::put('update', 'NewsletterContentController@putUpdate');
            Route::put('update-many', 'NewsletterContentController@putUpdateMany');
            Route::delete('delete/{id?}', 'NewsletterContentController@deleteDelete');
         });

        //Route::any('/user/newsletterusers', 'UserController@getNewsletterusers');

        // NewsletterListController
        Route::prefix('newsletter_list')->group(function (){
            Route::get('all/{newsletter_list}', 'NewsletterListController@getAll');
            Route::post('add-subscriber/{list_name}', 'NewsletterListController@postAddSubscriber');
            Route::delete('remove-subscriber/{list_name}/{user_id?}', 'NewsletterListController@deleteRemoveSubscriber');
        });
    });

    Route::middleware('be_access_ums')->group(function (){

        // UserController
        Route::prefix('user')->group(function (){
            Route::get('all', 'UserController@getAll');
            Route::get('read/{id?}', 'UserController@getRead');
//            Route::get('newsletterusers', 'UserController@getNewsletterusers');
//            Route::get('readfromgroup/{id?}', 'UserController@getReadfromgroup');
//            Route::get('newsletter-list', 'UserController@getNewsletterList');
            Route::post('create', 'UserController@postCreate');
            Route::put('update', 'UserController@putUpdate');
            Route::delete('delete/{id?}', 'UserController@deleteDelete');
        });

        // GroupController
        Route::prefix('group')->group(function (){
            Route::get('all', 'GroupController@getAll');
            Route::get('read/{id?}', 'GroupController@getRead');
            Route::get('readbyuserid/{id?}', 'GroupController@getReadbyuserid');
            Route::post('create', 'GroupController@postCreate');
            Route::post('addmember', 'GroupController@postAddmember');
            Route::put('update', 'GroupController@putUpdate');
            Route::delete('delete/{id?}', 'GroupController@deleteDelete');
            Route::delete('removemember', 'GroupController@deleteRemovemember');
        });

        // CommentController
        Route::prefix('comments')->group(function (){
            Route::get('all/{lang?}', 'CommentController@getAll');
            Route::get('read/{id?}', 'CommentController@getRead');
            Route::get('readbypage', 'CommentController@getReadbypage');
            Route::get('readbyuser', 'CommentController@getReadbyuser');
            Route::post('create', 'CommentController@postCreate');
            Route::put('update', 'CommentController@putUpdate');
            Route::delete('delete/{id?}', 'CommentController@deleteDelete');
        });

        // HistoryController
        Route::prefix('history')->group(function (){
            Route::get('all', 'HistoryController@getAll');
            Route::get('read/{id?}', 'HistoryController@getRead');
            Route::delete('delete/{id?}', 'HistoryController@deleteDelete');
            Route::delete('deletebypage/{id?}', 'HistoryController@deleteDeletebypage');
        });
    });

    Route::middleware('be_access_faqs')->prefix('faq')->group(function (){
        Route::get('faqbyid/{id?}', 'FaqController@getFaqbyid');
        Route::get('allfaqs/{lang?}', 'FaqController@getAllfaqs');
        Route::post('create', 'FaqController@postCreate');
        Route::put('update', 'FaqController@putUpdate');
        Route::delete('delete/{id?}', 'FaqController@deleteDelete');
    });

    /*
     *  Tests roots
     */

    Route::middleware(['test_enviroment', 'be_access_ums'])->group(function(){
        Route::delete('contents/forcedelete/{id?}', 'ContentController@deleteForcedelete');
        Route::delete('term/forcedelete/{id?}', 'TermController@deleteForcedelete');
        Route::delete('workflow/forcedelete/{id?}', 'WorkflowController@deleteForcedelete');
        Route::delete('files/forcedeletefile/{id?}', 'FileController@deleteForcedeletefile');
        Route::delete('user/forcedelete/{id?}', 'UserController@deleteForcedelete');
        Route::delete('group/forcedelete/{id?}', 'GroupController@deleteForcedelete');
        Route::delete('group/forceremovemember', 'GroupController@deleteForceremovemember');
        Route::delete('comments/forcedelete/{id?}', 'CommentController@deleteForcedelete');
        Route::delete('history/forcedeletebypage/{id?}', 'HistoryController@deleteForcedeletebypage');
        Route::delete('nl_contents/forcedelete/{id?}', 'NewsletterContentController@deleteForcedelete');
        Route::delete('newsletter/forcedelete/{id?}', 'NewsletterApiController@deleteForcedelete');

        Route::prefix('glossary')->group(function () {
            Route::post('create', 'GlossaryController@postCreate');
            Route::put('modify', 'GlossaryController@putModify');
            Route::delete('remove', 'GlossaryController@deleteRemove');
        });
        Route::prefix('pages')->group(function () {
            Route::delete('forcedelete/{id?}', 'PageController@deleteForcedelete'); // Added Access
            Route::get('descendants/{page_id}', 'PageController@getDescendants');
            Route::get('ascendants/{page_id}', 'PageController@getAscendants');
            Route::get('related/{page_id?}/{lang?}', 'PageController@getRelated');
        });
    });
});
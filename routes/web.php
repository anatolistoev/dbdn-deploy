<?php

use App\Helpers\ContentUpdater;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Response;

// Decrypt user data in the database
// TODO: remove when ready for production
Route::get('mcrypt-migration', 'MigrationController@migrate');



// Change the encryption algorithm from mcrypt to bcrypt
Route::get('mcrypt-migration', 'MigrationController@migrate');



/*
|--------------------------------------------------------------------------
| Web Console routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'cms/', 'middleware' => ['web', 'debug_enviroment', 'be_access_ums', 'console_protect']], function () {
    Route::get('web_console',  '\Teepluss\Console\Http\Controllers\ConsoleController@getIndex');
    Route::post('web_console', [
        'as' => 'console_execute',
        'uses' => '\Teepluss\Console\Http\Controllers\ConsoleController@postExecute'
    ]);
});

// Webfonts handler to prevet direct downloading
Route::get('webfonts/{font}', 'FontController@index')->where('font', '(.*)?');
Route::get('build/webfonts/{font}', 'FontController@index')->where('font', '(.*)?');

Route::get('migrate', function () {
    if (config('app.debug') && config('app.testing')) {
        Artisan::call('migrate:refresh');

        return Artisan::call('db:seed');
    } else {
        return "Not available in runtime!";
    }
});

Route::middleware(['ws_error', 'be_access_approver'])->prefix('system')->group(function () {
    Route::get('artisan/{command}', 'SystemController@runArtisanCommand');
    Route::get('clear/views', 'SystemController@clearViews');
    Route::get('view/{filename}', 'SystemController@displayView');
    Route::get('clear/fonts', 'SystemController@clearFonts');
    Route::get('clear/cache/{filemanager_mode}', 'SystemController@deleteCache')->where(['filemanager_mode' => 'images|downloads']);
    Route::get('sync-files-local/{filemanager_mode}', 'SystemController@syncFilesLocal')->where(['filemanager_mode' => 'images|downloads']);
    Route::get('sync-files-url/{param}', 'SystemController@syncFilesURL');
    Route::get('send-newsletter-emails/{slug}', 'SystemController@sendNewsletterEmails');
    Route::get('system-load', 'SystemController@systemLoad');
    Route::get('count-pages-by-tag', 'SystemController@countPagesByTag');
    Route::get('cleanPDFCache', 'SystemController@cleanPDFCache');
    Route::get('generatePDFCache/{force}', 'SystemController@generateAllPDFs')->where(['force' => 'true|false']);
    Route::get('backup-db/{param}', 'SystemController@backupDb');
    Route::get('/updateNewsletterUnsubscribeCode', function () {
        LoginController::updateNewsletterUnsubscribeCode();
    });

    Route::get('/updateImg', function () {
        ContentUpdater::updateImgTags();
    });

    Route::get('/updateSrcset', function () {
        ContentUpdater::updateSrcsetTags();
    });

    Route::get('/updateSrcTags', function () {
        ContentUpdater::updateSrcTags();
    });

    Route::get('/updateFixRelationImageToPage', function () {
        ContentUpdater::fixFileRelationsToPage();
    });

});

/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
|
*/
Route::prefix('cms')->group(function () {

    Route::any('login', 'BackendDispatcher@loginBE');
    Route::any('confirm', 'BackendDispatcher@confirmBE');

    Route::middleware('be_auth_logout')->group(function () {
        Route::any('logout', 'BackendDispatcher@logoutBE');
    });

    //Web Console routes
    Route::middleware(['web', 'debug_enviroment', 'be_access_ums', 'console_protect'])->group(function () {
        Route::get('web_console',  '\Teepluss\Console\Http\Controllers\ConsoleController@getIndex');
        Route::post('web_console', [
            'as' => 'console_execute',
            'uses' => '\Teepluss\Console\Http\Controllers\ConsoleController@postExecute'
        ]);
    });

    Route::middleware('be_auth')->group(function () {
        Route::any('', 'BackendDispatcher@welcomeBE');
        Route::any('keepLogin', 'BackendDispatcher@keepLoginBE');

        Route::middleware('be_access_ums')->group(function () {
            Route::any('users', 'BackendDispatcher@getusersBEVIew');
            Route::any('groups', 'BackendDispatcher@getgroupsBEVIew');
            Route::any('history', 'BackendDispatcher@gethistoryBEVIew');
        });

        Route::middleware('be_access_cms')->group(function () {
            Route::any('pages', 'BackendDispatcher@pageBEView');
            Route::any('recycle_bin', 'BackendDispatcher@recyclebinBEView');
            Route::prefix('content')->group(function () {
                Route::get('files/{filemanager_mode}/{file}', 'Dispatcher@getFile')->where(['filemanager_mode' => 'images']);                
                Route::any('{pageid}', 'BackendContentDispatcher@getContents')->where(['pageid' => '[0-9]+']); //Added Access and Tests
                Route::get('/protected-file/{id}/{ftype}', 'FileController@protectedFileContent');
            });
            Route::any('downloads', 'BackendDispatcher@downloadsBEView');
            Route::any('comments', 'BackendDispatcher@getcommentsBEVIew');
            Route::any('hotspot', 'BackendDispatcher@hotspotBEView');
            Route::get('/preview_pdf/{u_id}', 'Dispatcher@getPdfPreviewById');
            Route::get('/pdf_print/{u_id}', 'Dispatcher@getPdfPrintById');
        });

        Route::get('faqs', 'BackendDispatcher@getFaqsBEView')->middleware('be_access_faqs');

        Route::middleware('be_access_approver')->group(function () {
            Route::get('glossary', 'BackendDispatcher@getGlossaryBEView');
            Route::get('tags', 'BackendDispatcher@tagsBEView');
        });

        Route::any('content/{slug}', function () {
            return Response::view('backend.be_missing_page', ['ACTIVE' => ''], 404);
        });

        Route::middleware('be_access_nl')->group(function () {
            Route::prefix('newsletter')->group(function () {
                Route::any('', 'BackendDispatcher@newsletterBEView');
                Route::any('{newsletterid}', 'BackendContentDispatcher@getNewsletterContents')->where(['newsletterid' => '[0-9]+']);
                Route::get('files/{filemanager_mode}/{file}', 'Dispatcher@getFile')->where(['filemanager_mode' => 'images']);
            });
            Route::get('newsletter_users', 'BackendDispatcher@getNewsletterUsersBEView');
            Route::get('newsletter_users_test', 'BackendDispatcher@getNewsletterListUsersBEView');
        });

        Route::middleware('be_access_nl_or_cms')->group(function () {
            Route::any('filemanager', 'BackendDispatcher@filemanagerBEView');
            Route::any('images', 'BackendDispatcher@imagesBEView');
        });

        // This is general handler for all unknown routes cms/{?}
        Route::any('/{slug}', function () {
            return Response::view('backend.be_missing_page', ['ACTIVE' => ''], 404);
        });        

    });

});


/* Export of emails */
Route::any('/newsletterlist', ['middleware' => 'ws_access_export', 'uses' => 'UserController@getNewsletterList',]);

/* Preview of pages if for BE users only! */
Route::prefix('preview')->middleware(['be_auth', 'be_access_cms'] )->group(function (){
    Route::any('{id}', 'Dispatcher@getPreview');
    Route::get('files/{filemanager_mode}/{file}', 'Dispatcher@getFile')->where(['filemanager_mode' => 'images|downloads']);
    Route::get('protected-file/{id}/{ftype}', 'FileController@protectedFileContent');
});

/*
|--------------------------------------------------------------------------
| Frontend Routes
|--------------------------------------------------------------------------
|
*/
Route::get('{locale}/{idslug_path}', 'Dispatcher@changeLang')->where(['locale' => '[a-z]{2}', 'idslug_path'=>'[A-Za-z0-9_/\.\-\~]+']);

/* User control roots*/
Route::any('/login/{idslug?}', 'LoginController@login')->where('idslug', '[A-Za-z0-9_/:\.\-\~]*');
Route::any('/doLogin/{idslug}', 'LoginController@doLogin')->where('idslug', '[A-Za-z0-9_/:\.\-\~]*');
Route::any('/logout', 'LoginController@logout');
Route::get('/captcha', 'LoginController@generateCAPTCHA');
Route::get('/reset_captcha', 'LoginController@resetCaptcha');
Route::prefix('registration')->group(function (){
    Route::get('', 'LoginController@registrationForm');
    Route::post('', 'LoginController@register');
});
Route::any('/requestaccess/{idslug}', 'LoginController@requestAccess')->where('idslug', '[A-Za-z0-9_/:\.\-\~]*');
Route::get('/user/activate/{code}', 'LoginController@unlockUser');
Route::get('/optin/{code}', 'LoginController@optin');
Route::get('/unsubscribe/{code}', 'LoginController@unsubscribe');
Route::prefix('profile')->middleware('auth')->group(function (){
    Route::get('', 'LoginController@profileForm');
    Route::post('', 'LoginController@updateProfile');
});
Route::get('/user/gdprerase/{userid}/{token}', 'LoginController@performGDPRErase')->where(['userid' => '[0-9]+', 'token' => '[A-Za-z0-9]*']);

Route::put('/rating/add', ['middleware' => 'auth', 'uses' => 'RatingPageController@store']);
Route::post('/comments/{idslug}', ['middleware' => 'auth', 'uses' => 'CommentsPageController@store']);

 // Reset request email
Route::prefix('forgotten')->group(function (){
    Route::get('', 'LoginController@forgottenForm')->name('password.email');
    Route::post('', 'LoginController@createReset')->name('password.email');
});

// Reset form
Route::prefix('reset')->group(function (){
    Route::get('{token?}', 'LoginController@resetForm')->name('password.request');
    Route::post('{token?}', 'LoginController@doReset')->name('password.reset');
});

/* User functions routs*/
Route::prefix('Download_Cart')->middleware('auth')->group(function (){
    Route::get('', 'MyDownloadsPageController@listDownloads');
    Route::post('','MyDownloadsPageController@startDownload');
    Route::put('add/{id}', 'MyDownloadsPageController@addDownload');
    Route::delete('', 'MyDownloadsPageController@removeDownloads');
});
Route::post('/download', 'MyDownloadsPageController@startDownload');
Route::prefix('Watchlist')->middleware('auth')->group(function (){
    Route::get('', 'WatchlistPageController@index');
    Route::put('', 'WatchlistPageController@store');
    Route::delete('', 'WatchlistPageController@destroy');
});

Route::prefix('print')->group(function (){
   Route::get('Download_Cart', ['middleware' => 'auth', 'uses' => 'MyDownloadsPageController@listDownloads']);
   Route::post('gallery', 'FileController@galleryData');
   Route::get('protected-file/{id}/{ftype}', 'FileController@protectedFileContent');
   Route::get('feedback/{idslug}', 'FeedbackPageController@index');
   Route::get('sitemap', 'SitemapPageController@sitemap');
   Route::get('index/{letter?}', 'SitemapPageController@index');
   Route::get('Watchlist', ['middleware' => 'auth', 'uses' => 'WatchlistPageController@index']);
   Route::get('{id}', 'Dispatcher@getPrintById');
   Route::get('{slug}', 'Dispatcher@getPrintBySlug');
   Route::get('files/{filemanager_mode}/{file}', 'Dispatcher@getFile')->where(['filemanager_mode' => 'images|downloads']);
});
/* Files routes*/
Route::get('/explainer', 'FileController@explainerView');
Route::post('/gallery', 'FileController@galleryData');
Route::get('/protected-file/{id}/{ftype}', 'FileController@protectedFileContent');
Route::get('/files/{filemanager_mode}/{file}', 'FileController@getResizedImage')->where(['filemanager_mode' => 'images|downloads']);
Route::get('/protected-file-noaccess', function () {
    return redirect('/');
});
Route::get('newsletter/files/{filemanager_mode}/{file}', 'Dispatcher@getFile')->where(['filemanager_mode' => 'images']);

/* Page display help routs*/
Route::post('/dialog/{idslug}/{brand?}', 'DialogPageController@send');
Route::get('/help/{brand?}', 'DialogPageController@getHelpForm');
Route::post('/downloads_filter', 'Dispatcher@generateDownloadsFilter');
Route::post('/best_practice_filter', 'Dispatcher@generateBestPracticeFilter');
Route::post('/faq_filter', 'FaqPageController@generateFaqFilter');
Route::any('/front_blocks/{pageId}/{type}', 'Dispatcher@getFrontBlocksView')->where(['type' => '[a-z]+', 'pageId' => '[0-9]+']);
Route::get('menu/{url}', 'Dispatcher@getPageMenu')->where('url', '(.*)');

Route::prefix('feedback')->group(function (){
    Route::get('{idslug}', 'FeedbackPageController@index');
    Route::post('{idslug}', 'FeedbackPageController@send');
});
/* Sitemap*/
Route::get('/sitemap', 'SitemapPageController@sitemap');
Route::get('/index/{letter?}', 'SitemapPageController@index');
/* Search */
Route::any('/search/{qparam?}', 'SearchPageController@showResults');
Route::post('/search_results_filter', 'SearchPageController@showResults');
Route::post('/tag/autocomplete/{type}/{brand?}', 'SearchPageController@searchAutocomplete')->where(['type' => 'page|faq']);

Route::prefix('glossary')->group(function (){
    Route::get('{letter?}', 'GlossaryPageController@index');
    Route::get('{id?}', 'GlossaryPageController@index');
});
Route::prefix('faq')->group(function (){
    Route::get('{brand?}/{faq_id?}/{faq_uri?}', 'FaqPageController@index')->where(['faq_uri' => '[A-Za-z0-9_\.\-\~]+']);
    Route::post('{brand?}', 'FaqPageController@index');
    Route::post('views/{faq_id}', 'FaqPageController@updateViews');
    Route::post('likes/{faq_id}', 'FaqPageController@updateLikes');
});

Route::get('/pdf_print/{slug}', ['after' => 'instructSearchEnginesNotToIndex', 'uses' => 'Dispatcher@getPdfPrintBySlug']);
Route::any('/external', 'Dispatcher@externalPage');
Route::get('/Guided_Tour', 'Dispatcher@getGuideTour');
Route::any('/{id}', 'Dispatcher@getPageById');
Route::any('/{slug}', 'Dispatcher@getPageBySlug');
Route::any('/', 'Dispatcher@index');

Route::prefix('CD-News')->group(function (){
    Route::get('files/{filemanager_mode}/{file}', 'Dispatcher@getFile')->where(['filemanager_mode' => 'images']);
    Route::get('download/{slug?}', 'NewsletterController@tozip');
    Route::get('{slug?}', 'NewsletterController@index');
});
// Caseinsensitive fix
Route::prefix('cd-news')->group(function (){
    Route::get('files/{filemanager_mode}/{file}', 'Dispatcher@getFile')->where(['filemanager_mode' => 'images']);
    Route::get('download/{slug?}', 'NewsletterController@tozip');
    Route::get('{slug?}', 'NewsletterController@index');
});

// This is general handler for all unknown routes /{?}
Route::any('{all}', function () {
    return response()->view('errors.fatal', ['PAGE' => (object)['id' => 404, 'slug' => '']], 404);
})->where('all', '.*');

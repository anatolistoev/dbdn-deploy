/// <binding ProjectOpened='watch' />
var elixir = require('laravel-elixir');
elixir.config.assetsPath = 'public';
elixir.config.viewPath = 'app/views';
elixir.config.sourcemaps = true;
elixir.config.production = true;
elixir.config.css.autoprefix = {
    options: {
        browsers: [
            "iOS >= 6",
            "last 2 versions"
        ]
    }
};
elixir(function(mix) {
    mix.sass(['./public/css/style.scss'], 'public/css/desktop.css')
        .sass(['./public/css/handheld-devices.scss'], 'public/css/handheld-devices.css')
        .styles([
        'le.css',
        'right_navigation.css',
        'lytebox.css',
        'smoothness/jquery-ui-1.10.4.custom.css'
        ], 'public/css/all-common.css')
        // Includes styles only for desktop
        .styles([
        'desktop.css',
        'all-common.css'
        ], 'public/css/all-desktop.css')
        // Includes styles for Desktop, Tablet, Mobile
        .styles([
        'handheld-devices.css',
        'all-common.css'
        ], 'public/css/all-hhd.css')
        .styles([
        'desktop.css',
        'le.css',
        'print.css'
        ], 'public/css/all.print.css')
        .styles([
        'desktop.css',
        'le.css',
        'lytebox.css'
    ], 'public/css/all.explainer.css'
    ).scripts([
        'es-shim.min.js',
        'jquery-1.11.1.min.js',
        'onload.js',
        'slider.js',
        'jquery.zoom.min.js',
        'jquery_lazyload_1.9.7/jquery.lazyload.js',
        'lytebox.js',
        'jquery.scrollbar.js',
        'public.js',
        'jquery-ui.min.js',
        'datepicker-de.js',
        'jquery.collapse.js',
        'jquery.event.move.js',
        'jquery.event.swipe.js',
        'modernizr.js',
        'joyride-2.1/jquery.joyride-2.1.js',
        'guided_tour.js',
        'sly.min.js',
        'sly.js'
    ], 'public/js/all.js'
//    ).scripts([
//        'sly.min.js',
//        'sly.js',
//        'slider.js'
//    ], 'public/js/all.slider.js'
    ).scripts([
        'es-shim.min.js',
        'jquery-1.11.1.min.js',
        'onload.js',
        'jquery.scrollbar.js',
        'public.js',
        'jquery.zoom.min.js',
        'jquery.event.move.js',
        'jquery.event.swipe.js',
        'jquery-ui.min.js',
        'jquery.collapse.js',
        'modernizr.js',
        'helpers.js',
    ], 'public/js/all.fatal.js'
    ).scripts([
        'jquery-1.11.1.min.js',
        'jquery-ui.min.js',
        'jquery.scrollbar.js',
        'le.js'
    ], 'public/js/all.explainer.js'
    ).scripts([
        'jquery-1.11.1.min.js',
        'print.js',
    ], 'public/js/all.print.js')
    .version([
        // styles
        '/css/desktop.css',
        '/css/handheld-devices.css',
        '/css/le.css',
        '/css/right_navigation.css',
        '/css/all-desktop.css',
        '/css/all-hhd.css',
        '/css/all.print.css',
        '/css/lytebox.css',
        '/css/styleSmart.css',
        '/css/smoothness/jquery-ui-1.10.4.custom.css',
        // scripts
        '/js/jquery-1.11.1.min.js',
        '/js/onload.js',
        '/js/slider.js',
        '/js/jquery.zoom.min.js',
        '/js/jquery_lazyload_1.9.7/jquery.lazyload.js',
        '/js/sly.min.js',
        '/js/sly.js',
        '/js/all.js',
        '/js/lytebox.js',
        '/js/jquery.scrollbar.js',
        '/js/public.js',
        '/js/jquery-ui.min.js',
        '/js/datepicker-de.js',
        '/js/jquery.collapse.js',
        '/js/jquery.event.move.js',
        '/js/jquery.event.swipe.js',
        '/js/modernizr.js',
        '/js/helpers.js',
        '/js/header_load.js',
        '/js/all.fatal.js',
        '/js/downloads_filter.js',
        '/js/faqs_list.js',
        '/js/es-shim.min.js'
    ]);
});

<?php

namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Ardent
{
    use SoftDeletes;
    //TODO: add new column page_u_id becouse page_id is confusing it's connected to page.u_id
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gallery';

    protected $guarded = ['created_at', 'updated_at'];

    public $incrementing = false;

    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'page_id' => ['required', 'integer'],
        'file_id' => ['required', 'integer']
    ];




    public function file()
    {
        return $this->belongsTo(\App\Models\FileItem::class, 'file_id');
    }
}

<?php

namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Newsletter extends Ardent
{
    use SoftDeletes;                                         
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'newsletter';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'slug' => ['required', 'max:100', 'is_slug'],
        'status' => ['required', 'in:Draft,Pending,Approved,Sending,Sent'],
        'editing_state' => ['required', 'in:Draft,Review,Approved'],
        'active' => ['required', 'integer'],
    ];

    /**
     * Relation to Newsletter Content
     */
    public function contents()
    {
        return $this->hasMany(\App\Models\NewsletterContent::class);
    }


    public static function getBySlug($slug)
    {
        $newsletter = $slug ?
            self::where('slug', '=', $slug) :
            self::where('active', '=', 1)->orderBy('created_at', 'desc');

        return $newsletter->firstOrFail();
    }


    public static function sending()
    {
        try {
            $newsletter = self::whereRaw("`status` = 'Sending' and `active` = 1")->firstOrFail();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return false;
        }

        return $newsletter;
    }


    public function setStatus($status)
    {
        $this->status = $status;
        $this->save();
    }


    public function markSent()
    {
        $this->setStatus('Sent');
    }


    /**
     * Scope function to include only pages from the current language
     */
    //public function scopeHaveLang($query) {
    //  return $query->whereRaw('page.langs >> (?-1) & 1 = 1', array(Session::get('lang')));
    //}
}

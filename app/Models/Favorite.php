<?php

namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Favorite extends Ardent
{
    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'favorite';

    protected $guarded = ['created_at', 'updated_at'];

    public $incrementing = false;

    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'user_id' => ['required', 'integer'],
        'page_id' => ['required', 'integer']
    ];




    public function page()
    {
        return $this->belongsTo(\App\Models\Page::class, 'page_id', 'id');
    }
}

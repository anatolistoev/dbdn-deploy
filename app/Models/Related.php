<?php

namespace App\Models;

use LaravelArdent\Ardent\Ardent;

class Related extends Ardent
{
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'related';

    protected $primaryKey = "page_u_id";

    public $incrementing = false;

    protected $guarded = ['created_at', 'updated_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'page_u_id' => ['required', 'integer'],
        'related_id' => ['required', 'integer', 'different:page_u_id']
    ];




    /**
     * relation to Page
     */
    public function page()
    {
        return $this->belongsTo(\App\Models\Page::class, 'page_u_id');
    }
}

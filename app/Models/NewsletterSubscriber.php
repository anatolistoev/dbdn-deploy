<?php

namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewsletterSubscriber extends Ardent
{
    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'newsletter_subscriber';

    protected $guarded = ['created_at', 'updated_at'];

    protected $hidden = ['deleted_at'];

    public $incrementing = false;

    public $throwOnValidation = true;

    public static $rules = [
        'user_id' => ['required', 'integer'],
        'list_id' => ['required', 'integer']
    ];




    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }
}

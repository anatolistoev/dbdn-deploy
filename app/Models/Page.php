<?php

namespace App\Models;

use App\Helpers\UriHelper;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Ardent
{
    use SoftDeletes;

    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'page';
    protected $guarded = ['u_id', 'id', 'created_by', 'created_at', 'updated_at', 'visits', 'deleted', 'deletable' /** TODO: Not in use remove it! */];
    protected $hidden = ['deleted_at'];
    protected $primaryKey = 'u_id';

    public $throwOnValidation = true;

    public static $rules = [
        'parent_id' => ['required', 'integer'],
        'slug' => ['required', 'max:255', 'is_slug'],
//		'type' => array('required', 'in:File,Folder,Shortcut'),
        'active' => ['required', 'integer'],
        'position' => ['required', 'integer'],
        'in_navigation' => ['required', 'integer'],
        'authorization' => ['integer'],
        'visits' => ['integer'],
        'template' => ['required', 'max:255'],
        'langs' => ['integer', 'min:1'],
        'deletable' => ['integer'],
        'deleted' => ['integer'],
        'searchable' => ['required', 'integer'],
        'inline_glossary' => ['required', 'integer'],
        'home_block' => ['integer'],
        'home_accordion' => ['integer'],
        'overview' => ['required', 'integer'],
    ];

    // Events hooks
    /**
     * created hook
     */
    public function afterCreate()
    {
        $this->logHistory('created');
    }

    /**
     * updated hook
     */
    public function afterUpdate()
    {
        $this->logHistory('modified');
    }

    /**
     * deleted hook
     */
    public function afterDelete()
    {
        $this->logHistory('removed');
    }

    public function logHistory($action)
    {
        if (Auth::check()) {
            $history = new History();
            $history->page_slug = $this->slug;
            $history->page_id = $this->id;
            $history->content_section = "";
            $history->content_id = 0;
            $history->username = Auth::user()->username;
            $history->user_id = Auth::user()->id;
            $history->action = $action;
            $history->ip = UriHelper::getClientIpAddr(Request::instance());
            $history->save();
        }
    }

    //Model
    /**
     * Relation to Content
     */
    public function contents()
    {
        return $this->hasMany(\App\Models\Content::class, 'page_u_id');
    }

    /**
     * Relation "Related pages"
     */
    public function relatedPages()
    {
        //return $this->hasMany('App\Models\Related');
        return $this->belongsToMany(\App\Models\Page::class, 'related', 'page_id', 'related_id');
    }

    /**
     * Reverse Relation - pages to which this one is related
     */
    public function relatedToPages()
    {
        return $this->belongsToMany(\App\Models\Page::class, 'related', 'related_id', 'page_id');
    }

    /**
     *
     */
    public function downloads()
    {
        return $this->hasMany(\App\Models\Download::class, 'page_u_id');
    }

    public function comments()
    {
        return Comment::where('page_id', '=', $this->id);
    }

    public function activeComments()
    {
        $comments = $this->comments()
            ->where('active', 1)
            ->where('lang', Session::get('lang'))
            ->orderBy('comment.created_at', 'asc')
            ->get();

        $dummyuser = new User();

        foreach($comments as $comment) {
            if ($comment->user == null) {
                $comment->setUserAttribute($dummyuser->getDeletedUserData($comment->user_id));
            }
        }

        return $comments;
    }

    /**
     *  Relation to glossary terms
     */
    public function terms()
    {
        return $this->belongsToMany(\App\Models\Term::class, 'glossary', 'page_id', 'term_id');
    }

    /**
     * Relation to Subscription
     */
    public function subscriptions()
    {
        return Subscription::where('page_id', '=', $this->id)->where('active', 1);
    }

    /**
     * Relation to Rating
     */
    public function ratings()
    {
        return Rating::where('page_id', '=', $this->id);
    }

    public function averageRate()
    {
        if ($this->ratings()->count() > 0) {
            return number_format($this->ratings()->sum('vote') / $this->ratings()->count(), 1);
        } else {
            return '0.0';
        }
    }

    /**
     * Relation to Note
     */
    public function notes()
    {
        return $this->hasMany(\App\Models\Note::class, 'page_id');
    }

    /**
     * Relation to Workflow
     */
    public function workflow()
    {
        return $this->hasOne(\App\Models\Workflow::class, 'page_id');
    }

    /**
     * Relation "Ancestor pages"
     */
    public function ancestors()
    {
        //return $this->hasMany('App\Models\Related');
        return $this->belongsToMany(\App\Models\Page::class, 'hierarchy', 'page_id', 'parent_id');
    }

    /**
     * Relation to Tag
     */
    public function tags()
    {
        // removed ->where('tag.lang','=',Session::get('lang'))
        return $this->belongsToMany(\App\Models\Tag::class, 'pagetag', 'page_id', 'tag_id')->whereNull('pagetag.deleted_at')->orderBy('pagetag.updated_at', 'asc');
    }

    public function joinTags($limit = null)
    {

        $res = '';
        $tags = $this->tags()->where('tag.lang', '=', Session::get('lang'));
        if ($limit) {
            $tags = $tags->take($limit)->get()->toArray();
        } else {
            $tags = $tags->get()->toArray();
        }
        $numItems = count($tags);
        $i = 0;
        foreach ($tags as $key => $tag) {
            $res .= $tag['name'];
            if (++$i != $numItems) {
                $res .= ', ';
            }
        }

        return $res;
    }

    public function hasProtectedFiles()
    {
        $protectedFiles = DB::table('page')
            ->join('download', 'page.id', '=', 'download.page_id')
            ->join('file', 'file.id', '=', 'download.file_id')
            ->where('page.u_id', '=', $this->u_id)
            ->whereNull('download.deleted_at')
            ->where('file.protected', '=', 1)->get();
        if (empty($protectedFiles->toArray())) {
            return false;
        }

        return true;
    }

    /**
     * Scope function to filter out the pages with inactive/deleted ascendants
     */
    public function scopeAlive($query)
    {
        return $query->where('page.active', 1)
            ->whereRaw('(page.publish_date = "' . DATE_TIME_ZERO . '" OR page.publish_date < "' . date('Y-m-d H:i:s') . '")
								AND (page.unpublish_date = "' . DATE_TIME_ZERO . '" OR page.unpublish_date > "' . date('Y-m-d H:i:s') . '")')
            ->whereRaw('((SELECT Count(*)
                                FROM hierarchy hAlive
                                LEFT JOIN page pAlive ON hAlive.parent_id = pAlive.id
                                WHERE hAlive.page_id = page.id AND pAlive.active = 1 AND pAlive.id <> page.id
                                AND ( pAlive.publish_date = "' . DATE_TIME_ZERO . '" OR pAlive.publish_date < "' . date('Y-m-d H:i:s') . '")
                                AND ( pAlive.unpublish_date = "' . DATE_TIME_ZERO . '"	OR pAlive.unpublish_date > "' . date('Y-m-d H:i:s') . '"))
                                =
                                (Select hierarchy.level FROM hierarchy where hierarchy.page_id = page.id AND hierarchy.parent_id = 1))');
    }

    /**
     * Scope function for visible pages
     */
    public function scopeVisible($query)
    {
        return $query->where('page.in_navigation', 1);
    }

    /**
     * Scope function to include only pages from the current language
     */
    public function scopeHaveLang($query)
    {
        return $query->whereRaw('page.langs >> (?-1) & 1 = 1', [Session::get('lang')]);
    }

    /**
     * Scope function to get page by most criteria
     */
    public function scopeMostSomething($query, $pageId, $excludeIds, $mostWhat){
        $lang =  Session::get('lang');
        // defaults
        $where = [1, 1];
        $order = ['page.updated_at','DESC'];
        $addon = '';

        switch ($mostWhat) {
            case 'recent':
                $where = ['page.front_block', 1];
                break;

            case 'viewed':
                $order = ['visits', 'DESC'];
                break;

            case 'rated':
                $order = ['votes', 'DESC'];
                $addon = '(select count(`rating`.`vote`) from `rating` where `rating`.`page_id` = page.id)  as `votes`,';
                break;

            case 'downloaded':
                $where = ['page.template', '"downloads"'];
                $order = ['page.visits', 'DESC'];
                break;

            case 'best_practice':
            default:
                $where = ['page.home_block', 1];
        }

        $query ->whereRaw('page.overview <> 1')
                ->where('page.template', '<>', 'brand')
                ->join('content', 'page.u_id', '=', 'content.page_u_id')
                ->where('content.lang', '=', $lang)
                ->where('content.section', '=', 'title')
                ->leftJoin('comment', function ($join) use ($lang) {
                    $join->on('page.id', '=', 'comment.page_id');
                    $join->on('comment.lang', '=', DB::raw($lang));
                    $join->on('comment.active', '=', DB::raw('1'));
                    $join->whereNull('comment.deleted_at');
                   // $join->on('comment.deleted_at', 'is', DB::raw('NULL'));
                })
                ->whereNull('comment.deleted_at')
                ->whereNull('content.deleted_at')
                ->whereRaw('EXISTS(
                            SELECT * FROM hierarchy h
                            WHERE h.parent_id = ? AND h.page_id = page.id
                        )', [$pageId])
                ->join('content as c', 'page.u_id', '=', 'c.page_u_id')
                ->where('c.lang', '=', $lang)
                ->where('c.section', '=', 'main')
                ->whereNull('c.deleted_at')
                ->haveLang()
                ->groupby('page.id')
                ->whereRaw($where[0] . ' = ' . $where[1])
                ->whereNotIn('page.id', $excludeIds)
                ->orderBy($order[0], $order[1])
                ->select(DB::raw('
                        page.*,
                        c.body as main,
                        content.body as title,
                        COUNT(comment.page_id) as comments,
                        ' . $addon . '
                        (SELECT tag.NAME
                         FROM pagetag
                         JOIN tag on tag.id = pagetag.tag_id
                         WHERE pagetag.page_id = page.u_id
                           AND pagetag.deleted_at IS NULL
                           AND tag.deleted_at IS NULL
                           AND tag.lang = '.$lang.'
                        ORDER BY pagetag.updated_at
                        LIMIT 1
                        ) as tags
                '));
                return $query;
    }

    /**
     *
     */
    public function scopeDescendants($query, $pageId = 0, $lang = 0, $depth = 0, $searchDraft = false)
    {
        if ($pageId > 0) {
            //create query order by level ascending primary and by id secondary
            $query->join('hierarchy as h', 'h.page_id', '=', 'page.id')
                ->join('page as p', 'p.id', '=', 'h.parent_id')
                ->where('p.id', '=', $pageId)
                ->where('h.level', '>', '0')
                ->where('p.is_visible', '=', 1)
//                    ->whereRaw("(`p`.`active` = 1	OR (`p`.`version` = 0 AND `p`.`id` NOT IN(SELECT id FROM `page` as sp2 WHERE sp2.id = p.id AND sp2.active = 1 AND sp2.version <> 0)))")
                ->orderBy('level', 'asc')
                ->orderBy('position', 'asc');

            //add depth restriction if set
            if (isset($depth) && $depth > 0) {
                $query->where('h.level', '<=', $depth);
            }

            //show title if lang set
            if (isset($lang) && $lang > 0) {
                //$query->whereRaw('page.langs >> (?-1) & 1 = 1', array($lang));
                $query->join('content as c', 'c.page_u_id', '=', 'page.u_id')
                    ->where('c.lang', '=', $lang)
                    ->where('c.section', '=', 'title');
                if ($searchDraft) {
                    $query->select(DB::raw("page.* , h.level , c.body as title , (SELECT `p`.`u_id` from `page` AS `p` JOIN  `workflow` AS `w` ON `w`.`page_id` = `p`.`u_id` WHERE `p`.`id` = `page`.`id` AND `w`.`editing_state` = 'Draft' AND `p`.`version` > `page`.`version` AND `p`.`deleted_at` IS NULL Order by `p`.`version` DESC LIMIT 1) as draft"));
                } else {
                    $query->select('page.*', 'h.level', 'c.body as title');
                }
            } else {
                if ($searchDraft) {
                    $query->select(DB::raw("page.* ,h.level, (SELECT `p`.`u_id` from `page` AS `p` JOIN  `workflow` AS `w` ON `w`.`page_id` = `p`.`u_id` WHERE `p`.`id` = `page`.`id` AND `w`.`editing_state` = 'Draft' AND (`p`.`version` > `page`.`version` OR  `p`.`version` = 0) AND `p`.`deleted_at` IS NULL Order by `p`.`version` DESC LIMIT 1) as draft"));
                } else {
                    $query->select('page.*', 'h.level');
                }
            }
        } else {
            //create query order by level ascending primary and by id secondary
            $query->join('hierarchy as h', 'h.page_id', '=', 'page.id')
                ->groupBy('h.page_id')
                ->orderBy('level', 'asc')
                ->orderBy('position', 'asc')
                ->where('h.parent_id', '=', HOME);;

            //add depth restriction if set
            if (isset($depth) && $depth > 0) {
                $query->where('h.level', '<=', $depth - 1);
            }

            //show title if lang set
            if (isset($lang) && $lang > 0) {
                //$query->whereRaw('page.langs >> (?-1) & 1 = 1', array($lang));
                $query->join('content as c', 'c.page_u_id', '=', 'page.u_id')
                    ->where('c.lang', '=', $lang)
                    ->where('c.section', '=', 'title');
                if ($searchDraft) {
                    $query->select(DB::raw("page.*, MAX(h.level) as level,c.body as title, (SELECT `p`.`u_id` from `page` AS `p` JOIN  `workflow` AS `w` ON `w`.`page_id` = `p`.`u_id` WHERE `p`.`id` = `page`.`id` AND `w`.`editing_state` = 'Draft' AND (`p`.`version` > `page`.`version` OR  `p`.`version` = 0) AND `p`.`deleted_at` IS NULL Order by `p`.`version` DESC LIMIT 1) as draft"));
                } else {
                    $query->select(DB::raw('page.*, MAX(h.level) as level,c.body as title'));
                }
            } else {
                if ($searchDraft) {
                    $query->select(DB::raw("page.*, MAX(h.level) as level, (SELECT `p`.`u_id` from `page` AS `p` JOIN  `workflow` AS `w` ON `w`.`page_id` = `p`.`u_id` WHERE `p`.`id` = `page`.`id` AND `w`.`editing_state` = 'Draft' AND (`p`.`version` > `page`.`version` OR  `p`.`version` = 0) AND `p`.`deleted_at` IS NULL Order by `p`.`version` DESC LIMIT 1) as draft"));
                } else {
                    $query->select(DB::raw('page.*, MAX(h.level) as level'));
                }
            }
        }
        $query->where('page.is_visible', '=', 1);

//        $query->whereRaw("(`page`.`active` = 1	OR (`page`.`version` = 0 AND `page`.`id` NOT IN(SELECT id FROM `page` as sp2 WHERE sp2.id = page.id AND sp2.active = 1 AND sp2.version <> 0)))");
        return $query;
    }

    /**
     * Scope for accendants by page:id, lang, depth and order
     *
     */
    public function scopeAscendants($query, $pageId = 0, $lang = 0, $depth = 0, $order = 'desc')
    {

        //create query order by level primary and by id secondary
        $query->join('hierarchy as h', 'h.parent_id', '=', 'page.id')
            ->join('page as p', 'p.id', '=', 'h.page_id')
            ->where('p.is_visible', '=', 1)
            ->where('page.is_visible', '=', 1)
//                ->whereRaw("(`p`.`active` = 1 OR (`p`.`version` = 0 AND `p`.`id` NOT IN(SELECT id FROM `page` as sp2 WHERE sp2.id = p.id AND sp2.active = 1 AND sp2.version <> 0)))")
//                ->whereRaw("(`page`.`active` = 1	OR (`page`.`version` = 0 AND `page`.`id` NOT IN(SELECT id FROM `page` as sp2 WHERE sp2.id = page.id AND sp2.active = 1 AND sp2.version <> 0)))")
            ->where('p.id', '=', $pageId)
            ->where('h.level', '>', '0')
            ->orderBy('h.level', $order);
        //->orderBy('position', 'asc');
        //add depth restriction if set
        if (isset($depth) && $depth > 0) {
            $query->where('h.level', '<=', $depth);
        }

        //show title if lang set
        if (isset($lang) && $lang > 0) {
            $query->whereRaw('page.langs >> (?-1) & 1 = 1', [$lang]);
            $query->join('content as c', 'c.page_u_id', '=', 'page.u_id')
                ->where('c.lang', '=', $lang)
                ->where('c.section', '=', 'title')
                ->select('page.*', 'h.level', 'c.body as title');
        } else {
            $query->select('page.*', 'h.level');
        }

        return $query;
    }

    public function scopeGroups()
    {
        return Group::select('group.*', "pagegroup.*")->join('pagegroup', 'pagegroup.group_id', '=', 'group.id')->join('page', 'page.id', '=', 'pagegroup.page_id')->where('page.id', '=', $this->id)->whereNull('pagegroup.deleted_at')->groupBy('pagegroup.group_id')->get();

    }

    //get structure[id, title, slug, template, brand_id] from related pages To get related by downloawds foreach array and add onli with this template
    //Have to check is page is deleted, deleted_at , is active and its' parent is active to see it's related page
    public function getBestPractiseRelated($lang, $brand_id)
    {
        $id = $this->id;
        $u_id = $this->u_id;
        $select = ",(SELECT tag.NAME FROM pagetag
                    JOIN tag on tag.id = pagetag.tag_id
                    WHERE pagetag.page_id = t2.u_id
                    AND pagetag.deleted_at IS NULL
                    AND tag.deleted_at IS NULL
                    AND tag.lang = '" . $lang . "'
                    ORDER BY pagetag.updated_at LIMIT 1) as tags,
                    (SELECT h.parent_id  FROM hierarchy h
                            WHERE h.page_id = t2.id AND h.parent_id <> 1
                            order by h.`level` desc
                            LIMIT 1) as brand_id";
        if ($this->template == 'best_practice') {
            $select .= ",(SELECT c4.body
                            FROM content AS c4
                            JOIN page as p1 ON p1.u_id = c4.page_u_id
                            JOIN hierarchy AS h4 ON h4.parent_id = p1.id
                            WHERE h4.page_id = t2.id
                                AND h4.parent_id <> 1
                                AND c4.lang = " . $lang . "
                                AND c4.section = 'title'
                                AND p1.active
                            ORDER BY h4.`level` DESC LIMIT 1) as brand_title";
        }
        $query = DB::table('pagetag as t0')
            ->select(DB::raw("t2.id, t2.u_id, t2.parent_id, t2.slug, t2.authorization, t2.updated_at, t3.body as title, t4.body as main, t2.template, r0.is_hidden, r0.position" . $select))
            ->join('page as t2', 't2.u_id', '=', 't0.page_id')
            ->where('t2.active', '=', 1)
            ->where('t2.deleted', '=', 0)
            ->whereNull('t2.deleted_at')
            ->join('content as t3', 't2.u_id', '=', 't3.page_u_id')
            ->whereNull('t3.deleted_at')
            ->where('t3.lang', '=', $lang)
            ->where('t3.section', '=', 'title')
            ->join('content as t4', 't2.u_id', '=', 't4.page_u_id')
            ->whereNull('t4.deleted_at')
            ->where('t4.lang', '=', $lang)
            ->where('t4.section', '=', 'main')
            ->whereIn('t0.tag_id', function ($query) use ($u_id, $lang) {
                $query->select('t1.tag_id')
                    ->from('pagetag as t1')
                    ->join('tag as t4', 't4.id', '=', 't1.tag_id')
                    ->where('t1.page_id', '=', $u_id)
                    ->whereNull('t1.deleted_at')
                    ->where('t4.lang', '=', $lang)
                    ->whereNull('t4.deleted_at');
            })
            ->whereNotIn('t0.page_id', function ($query) use ($id) {
                $query->select('t1.page_id')
                    ->from('pagetag as t1')
                    ->join('page as p2', 'p2.u_id', '=', 't1.page_id')
                    ->where('p2.id', '=', $id);
            })
            ->whereNull('t0.deleted_at')
            ->leftjoin('related as r0', 'r0.related_id', '=', DB::raw('t2.id AND r0.page_u_id = ' . $u_id))
//                ->where(function($query){
//                        $query->whereNull('r0.is_hidden')
//                              ->orWhere('r0.is_hidden', 0);
//                        })
            ->whereRaw('((SELECT Count(*)
                    FROM hierarchy hAlive
                    LEFT JOIN page pAlive ON hAlive.parent_id = pAlive.id
                    WHERE hAlive.page_id = t2.id AND pAlive.active = 1 AND pAlive.id <> t2.id AND  pAlive.deleted = 0
                    AND ( pAlive.publish_date = "' . DATE_TIME_ZERO . '" OR pAlive.publish_date < "' . date('Y-m-d H:i:s') . '")
                    AND ( pAlive.unpublish_date = "' . DATE_TIME_ZERO . '"	OR pAlive.unpublish_date > "' . date('Y-m-d H:i:s') . '"))
                    =
                    (Select hierarchy.level FROM hierarchy where hierarchy.page_id = t2.id AND hierarchy.parent_id = 1))');

        if ($this->template == 'basic' || $this->template == 'exposed') {
            $query = $query->where('t2.template', 'best_practice');
        } else {
            if ($this->template == 'best_practice') {
                $query = $query->where('t2.template', '<>', 'downloads')
                    ->where('t2.template', '<>', 'best_practice');
            }
        }
        if ($brand_id == SMART) {
            $query = $query->where(DB::raw("(SELECT h.parent_id  FROM hierarchy h
                    WHERE h.page_id = t2.id AND h.parent_id <> 1
                    order by h.`level` desc
                    LIMIT 1)"), '=', SMART);
        } else {
            $query = $query->where(DB::raw("(SELECT h.parent_id  FROM hierarchy h
                    WHERE h.page_id = t2.id AND h.parent_id <> 1
                    order by h.`level` desc
                    LIMIT 1)"), '<>', SMART);
        }
        $query = $query->groupBy('t0.page_id')
            ->having(DB::raw('COUNT(t0.tag_id)'), '>=', MIN_MATCH)
            ->orderBy(DB::raw("FIELD(brand_id, $brand_id)"), 'desc')
            ->orderBy(DB::raw('COUNT(t0.tag_id)'), 'desc')
            ->orderBy('t0.updated_at', 'desc')
            ->orderBy('t2.updated_at', 'desc')
            ->orderBy('t3.updated_at', 'desc');
        $related = $query->get();

        return $related;
    }

    public function getPageRelatedArray($lang, $brand_id)
    {
        $id = $this->id;
        $u_id = $this->u_id;
        $query = DB::table('pagetag as t0')
            ->select(DB::raw("t2.id, t2.u_id, t2.parent_id, t2.slug, t2.authorization, t3.body, t2.template, r0.is_hidden, r0.position,
                    (SELECT h.parent_id  FROM hierarchy h
                            WHERE h.page_id = t2.id AND h.parent_id <> 1
                            order by h.`level` desc
                            LIMIT 1) as brand_id,
					(SELECT c4.body
                            FROM content AS c4
                            JOIN page as p1 ON p1.u_id = c4.page_u_id
                            JOIN hierarchy AS h4 ON h4.parent_id = p1.id
                            WHERE h4.page_id = t2.id
                                AND h4.parent_id <> 1
                                AND c4.lang = " . $lang . "
                                AND c4.section = 'title'
                                AND p1.active
                            ORDER BY h4.`level` DESC LIMIT 1) as brand_title"))
            ->join('page as t2', 't2.u_id', '=', 't0.page_id')
            ->where('t2.active', '=', 1)
            ->where('t2.deleted', '=', 0)
            ->whereNull('t2.deleted_at')
            ->join('content as t3', 't2.u_id', '=', 't3.page_u_id')
            ->whereNull('t3.deleted_at')
            ->where('t3.lang', '=', $lang)
            ->where('t3.section', '=', 'title')
            ->whereIn('t0.tag_id', function ($query) use ($u_id, $lang) {
                $query->select('t1.tag_id')
                    ->from('pagetag as t1')
                    ->join('tag as t4', 't4.id', '=', 't1.tag_id')
                    ->where('t1.page_id', '=', $u_id)
                    ->whereNull('t1.deleted_at')
                    ->where('t4.lang', '=', $lang)
                    ->whereNull('t4.deleted_at');
            })
            ->whereNotIn('t0.page_id', function ($query) use ($id) {
                $query->select('t1.page_id')
                    ->from('pagetag as t1')
                    ->join('page as p2', 'p2.u_id', '=', 't1.page_id')
                    ->where('p2.id', '=', $id);
            })
            ->whereNull('t0.deleted_at')
            ->leftjoin(
                'related as r0',
                'r0.related_id',
                '=',
                DB::raw('t2.id AND r0.page_u_id = ' . $u_id)
            )
            ->whereRaw('((SELECT Count(*)
                                    FROM hierarchy hAlive
                                    LEFT JOIN page pAlive ON hAlive.parent_id = pAlive.id
                                    WHERE hAlive.page_id = t2.id AND pAlive.active = 1 AND pAlive.id <> t2.id AND  pAlive.deleted = 0
                                    AND ( pAlive.publish_date = "' . DATE_TIME_ZERO . '" OR pAlive.publish_date < "' . date('Y-m-d H:i:s') . '")
                                    AND ( pAlive.unpublish_date = "' . DATE_TIME_ZERO . '"	OR pAlive.unpublish_date > "' . date('Y-m-d H:i:s') . '"))
                                    =
                                    (Select hierarchy.level FROM hierarchy where hierarchy.page_id = t2.id AND hierarchy.parent_id = 1))');
        if (config('app.isFE')) {
            $query = $query->where('t2.template', '<>', 'best_practice');
        } else {
            if ($this->template != 'basic' && $this->template != 'exposed') {
                $query = $query->where('t2.template', '<>', 'best_practice');
            }
            if ($this->template == 'best_practice') {
                $query = $query->where('t2.template', '<>', 'downloads');
            }
        }
        if ($brand_id == SMART) {
            $query = $query->where(DB::raw("(SELECT h.parent_id  FROM hierarchy h
                    WHERE h.page_id = t2.id AND h.parent_id <> 1
                    order by h.`level` desc
                    LIMIT 1)"), '=', SMART);
        } else {
            $query = $query->where(DB::raw("(SELECT h.parent_id  FROM hierarchy h
                    WHERE h.page_id = t2.id AND h.parent_id <> 1
                    order by h.`level` desc
                    LIMIT 1)"), '<>', SMART);
        }
        $query = $query->groupBy('t0.page_id')
            ->having(DB::raw('COUNT(t0.tag_id)'), '>=', MIN_MATCH)
            ->orderBy(DB::raw("FIELD(brand_id, $brand_id)"), 'desc')
            ->orderBy(DB::raw('COUNT(t0.tag_id)'), 'desc')
            ->orderBy('t0.updated_at', 'desc')
            ->orderBy('t2.updated_at', 'desc')
            ->orderBy('t3.updated_at', 'desc');
        $related = $query->get();
        //       $queries = DB::getQueryLog();
//        $last_query = end($queries);
//        print_r($last_query);die;
        return $related;
    }
    //get structure[id, question, answer, category_id, brand_id] from related faqs
    //Have to check is page is deleted, deleted_at and active to see it's related FAQs
    //In query we check if it's parent is active
    public function getFagRelated($lang, $brand_id)
    {
        $related = collect();

        if ($brand_id == SMART || $brand_id == DAIMLER) {
            $id = $this->id;
            $query = DB::table('pagetag as pt')
                ->select('ft.faq_id', 'fq.question', 'fq.answer', 'fq.category_id', 'fq.brand_id')
                ->join('faqtag as ft', 'pt.tag_id', '=', 'ft.tag_id')
                ->join('faq as fq', 'fq.id', '=', 'ft.faq_id')
                ->where('pt.page_id', '=', $this->u_id)
                ->where('fq.lang', '=', $lang)
                ->whereNull('pt.deleted_at')
                ->whereNull('ft.deleted_at')
                ->whereNull('fq.deleted_at');

            if ($brand_id == SMART) {
                $query = $query->where('fq.brand_id', '=', SMART);
            } else {
                if ($brand_id == DAIMLER) {
                    $query = $query->where('fq.brand_id', '=', DAIMLER);
                }
            }
            $related = $query->groupBy('ft.faq_id')
                ->having(DB::raw('COUNT(pt.tag_id)'), '>=', MIN_MATCH)
                ->orderBy(DB::raw('COUNT(pt.tag_id)'), 'desc')
                ->take(5)
                ->get();
        }

        return $related;
    }

    public function getPublishDateAttribute($value)
    {
        if (strtotime($value) <= 0) {
            return DATE_TIME_ZERO;
        }

        return date('Y-m-d H:i', strtotime($value));
    }

    public function getUnpublishDateAttribute($value)
    {
        if (strtotime($value) <= 0) {
            return DATE_TIME_ZERO;
        }

        return date('Y-m-d H:i', strtotime($value));
    }

    //TODO: remove download table we don't need it
    public function getDownloadFiles($withCart = null)
    {
        $withCart = ($withCart == null) ? Auth::check() : $withCart;
        //return Download::with($withCart ?
 	$result = Download::with(Auth::check() ?
            [
                'file.info' => function ($query) {
                    return $query->where('lang', Session::get('lang'));
                },
                'file.carts' => function ($query) {
                    return $query->where('user_id', Auth::user()->id)->where('active', 1);
                }
            ]
            :
            [
                'file.info' => function ($query) {
                    return $query->where('lang', Session::get('lang'));
                },
            ])->where('download.page_u_id', $this->u_id)
              ->whereNull('deleted_at')
	    ->orderby('position')
            ->get();
	return($result);
    }

    public function getAccess()
    {
        $result =
            DB::table('page')->join('pagegroup', 'pagegroup.page_id', '=', 'page.id')
                ->where('page.id', '=', $this->id)
                ->whereNull('pagegroup.deleted_at')
                ->select('pagegroup.page_id', 'pagegroup.group_id')
                ->groupBy('pagegroup.group_id')->get();

        return json_decode(json_encode($result), true);
    }
}

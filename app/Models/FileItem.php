<?php

namespace App\Models;

use App\Http\Controllers\FileController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\SoftDeletes;
use LaravelArdent\Ardent\Ardent;

class FileItem extends Ardent
{
    use SoftDeletes;

    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'file';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    /**
     * Default array of rules.
     *
     * @var array
     */
    public static $rules = [
        'filename' => ['required', 'max:255'],
        'size' => ['integer'],
        'extension' => ['required', 'max:255'],
        'thumbnail' => ['max:255'],
        'zoompic' => ['max:255'],
        'category_id' => ['integer'],
        'software' => ['max:255'],
        'lang' => ['max:255'],
        'dimensions' => ['max:255'],
        'dimension_units' => ['max:255'],
        'format' => ['max:255'],
        'resolution' => ['max:255'],
        'resolution_units' => ['max:255'],
        'color_mode' => ['max:255'],
        'pages' => ['max:255'],
        'version' => ['max:255'],
        'brand_id' => ['integer']
    ];


    public function carts()
    {
        return $this->hasMany(\App\Models\Cart::class, 'file_id');
    }

    public function downloads()
    {
        return $this->hasMany(\App\Models\Download::class, 'file_id', 'file_id');
    }

    public function info()
    {
        return $this->hasMany(\App\Models\FileInfo::class, 'file_id');
    }

    protected function getPolicySecret()
    {
        $secret = $this->id . Request::getClientIp() . Session::getId();
//        $secret = $this->id . Session::getId();

        return $secret;
    }

    public function checkPolicy($policy)
    {
        $secret = $this->getPolicySecret();

        return Hash::check($secret, $policy);
    }


    protected function getPolicy()
    {
        $secret = $this->getPolicySecret();

        return Hash::make($secret);
    }

    /**
     * For physical file location call with constant FILEITEM_REAL_PATH
     *
     *
     */
    protected function getFilePath($uri_type, $filename, $file_type)
    {
        $filename = ($filename[0] != '/' ? '/' : '') . $filename;
        $filemanager_mode = strtolower($this->type);
        $relative_dest_path = 'files/' . $filemanager_mode . $filename;
        if ($this->protected != 0) {
            if ($uri_type == FILEITEM_REAL_PATH) {
                $relative_dest_path = 'protected_files/' . $filemanager_mode . $filename;
            } else {
                $relative_dest_path = 'protected-file/' . $this->id . '/' . $file_type . '?policy=' . self::getPolicy();
            }
        }

        return $relative_dest_path;
    }

    public function getPath($uri_type = FILEITEM_URL_PATH)
    {
        return $this->getFilePath($uri_type, $this->filename, 'doc');
    }

    public function getZoomPath($uri_type = FILEITEM_URL_PATH)
    {
        return $this->getFilePath($uri_type, ($this->zoompic != '' ? $this->zoompic : $this->filename), 'zoom');
    }

    public function getThumbPath($uri_type = FILEITEM_URL_PATH, $type = 'thumb')
    {
        return $this->getFilePath($uri_type, ($this->thumbnail != '' ? $this->thumbnail : ($this->zoompic != '' ? $this->zoompic : $this->filename)), $type);
    }

    public function hasThumbFile()
    {
        return $this->thumbnail != '' && is_file(public_path() . '/' . $this->getThumbPath(FILEITEM_REAL_PATH));
    }

    public function hasThumbSourceFile()
    {
        $hasThumb = false;
        $filemanager_mode = strtolower($this->type);
        switch ($filemanager_mode) {
            case FileController::MODE_DOWNLOADS:
                if ($this->thumbnail != '' && is_file(public_path() . '/' . $this->getThumbPath(FILEITEM_REAL_PATH))) {
                    $hasThumb = true;
                } else {
                    $srcImageFilePath = public_path() . '/' . $this->getZoomPath(FILEITEM_REAL_PATH);
                    if (is_file($srcImageFilePath)) {
                        // Check filename for Image type
                        $imageFormats = config('assets.image_formats');
                        $fileExtension = strtolower(pathinfo($srcImageFilePath, PATHINFO_EXTENSION));
                        if (!empty($fileExtension) && in_array($fileExtension, $imageFormats)) {
                            $hasThumb = true;
                        }
                    }
                }
                break;
            case FileController::MODE_IMAGES:
                if ($this->thumbnail != '' && is_file(public_path() . '/' . $this->getThumbPath(FILEITEM_REAL_PATH))) {
                    $hasThumb = true;
                } else {
                    $srcImageFilePath = public_path() . '/' . $this->getPath(FILEITEM_REAL_PATH);
                    if (is_file($srcImageFilePath)) {
                        $hasThumb = true;
                    }
                }
                break;

            default:
                break;
        }

        return $hasThumb;
    }

    public function getCachedThumbPath($type = 'thumb')
    {
        return $this->getFilePath(FILEITEM_URL_PATH, $this->filename, $type);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Pagetag extends BaseModel
{

    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pagetag';

    protected $guarded = ['created_at', 'updated_at'];

    public $incrementing = false;

    protected $hidden = ['deleted_at'];
    protected $primaryKey = 'tag_id';

    public $throwOnValidation = true;

    public static $rules = [
        'tag_id' => ['required', 'integer'],
        'page_id' => ['required', 'integer']
    ];




    public function page()
    {
        return $this->belongsTo(\App\Models\Page::class, 'page_id');
    }

    public function tag()
    {
        return $this->belongsTo(\App\Models\Tag::class, 'tag_id');
    }

    public function scopeReadbypage($query, $pageId)
    {

        return $query->where('page_id', '=', $pageId);
    }
}

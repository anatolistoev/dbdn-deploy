<?php

namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faqrate extends Ardent
{
    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'faqrate';

    protected $guarded = ['created_at', 'updated_at'];

    public $incrementing = false;

    protected $hidden = ['deleted_at'];

    protected $primaryKey = 'faq_id';

    public $throwOnValidation = true;

    public static $rules = [
        'faq_id' => ['required', 'integer'],
        'user_id' => ['required', 'integer']
    ];




    public function faq()
    {
        return $this->belongsTo(\App\Models\Faq::class, 'faq_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }
}

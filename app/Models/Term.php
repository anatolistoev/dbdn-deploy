<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Term extends Ardent
{
    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'term';

    protected $guarded = ['created_at', 'updated_at'];

    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'name' => ['required', 'max:255'],
        'lang' => ['required', 'integer'],
        'description' => ['required', 'max:750'],
        'related_links' => ['max:255']
    ];



//	public static function validateInput($input){
//		$validatorFactory = new Factory(app('translator'), app());
//		$validator = new Term::$validationService($input, $validatorFactory);
//		$validator->validate();
//
//	}


    /**
     * scope: Terms of current language only
     */
    public function scopeCurrentLang($query)
    {
        $lang = Session::get('lang');

        return $this->scopeOfLang($query, $lang);
    }

    /**
     * scope: Terms of given language only
     */
    public function scopeOfLang($query, $lang)
    {
        return $query->where('lang', '=', $lang);
    }

    /**
     * scope: Terms that are conneted to one page
     */
    public function scopeGlossaryOfPage($query, $page_id)
    {
        return $query->join('glossary', function ($join) use ($page_id) {
            $join->on('glossary.term_id', '=', 'term.id')
                ->on('glossary.page_id', '=', DB::raw($page_id));
        })->select('term.*');
    }

        /**
     * scope: Random Term of the day
     */
    public function scopeTermOfTheDay($query, $lang, $queryAddon)
    {
        return $query->where('term.lang', '=', $lang)
                        ->whereRaw($queryAddon . ' length(`term`.`description`) between 100 and 500 ')
                        ->orderBy(DB::raw('RAND()'));
    }
}

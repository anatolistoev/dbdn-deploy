<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends BaseModel
{
    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cart';

    protected $guarded = ['created_at', 'updated_at'];

    protected $softDelete = false;

    public $incrementing = false;

    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'user_id' => ['required', 'integer'],
        'file_id' => ['required', 'integer']
    ];



    public function file()
    {
        return $this->belongsTo(\App\Models\FileItem::class, 'file_id');
    }

    public function download()
    {
        return $this->hasMany(\App\Models\Download::class, 'file_id', 'file_id');
    }
//	public function info()
//    {
//        return $this->belongsTo('App\Models\FileInfo','file_id');
//    }

    public function scopeCartItems($query, $userId)
    {
        $file_ids = $query->where('user_id', '=', $userId)->where('active', 1)->get();
        /*$file_ids = $query->where('user_id', '=', $userId)
							->with(
								   array(
										'info' => function($query){
											$query->where('lang', '=', Session::get('lang'));
										}
									)
							)->get(); */
        $file_items = [];
        foreach ($file_ids as $file_id) {
            $file_item = $file_id->file()->first()->attributesToArray();
            $file_item['title'] = $file_id->file()->first()->info->first() ? $file_id->file()->first()->info->first()->title : '';
            $file_item['description'] = $file_id->file()->first()->info->first() ? $file_id->file()->first()->info->first()->description : '';
            $file_items[] = $file_item;
        }
        if (empty($file_items)) {
            return -1;
        }

        return $file_items;
    }
}

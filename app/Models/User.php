<?php

namespace App\Models;

use App\Notifications\MailGDPRErase;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use App\Helpers\ValidationHelper;
use Hautelook\Phpass\PasswordHash;
use App\Notifications\MailResetPasswordToken;

class User extends BaseModel implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, 
	//SoftDeletes, 
	Notifiable;

    private $username_unique_rule_index = 2;
    private $email_unique_rule_index = 3;
    private $company_required_rule_index = 0;
    private $country_required_rule_index = 0;
    private $new_users_date = '2016-04-21';
    const ADMIN_ROLE_ID = 2;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';

    protected $guarded = ['created_at', 'updated_at'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'deleted_at'];

    protected $fillable = [
        'username',
        'password', 'password_confirmation',
        'first_name', 'last_name',
        'company', 'department', 'position',
        'address', 'code', 'city', 'country',
        'email', 'phone', 'fax', 'newsletter', 'role', 'newsletter_include', 'active', 'group'
    ];

    // Validation
    public $throwOnValidation = false;

    public static $rules = [
        'username' => ['required', 'max:255', 'unique:user,username'],
        'email' => ['required', 'max:255', 'email', 'unique:user,email'],
        'password' => ['required', 'confirmed', 'max:25', 'min:8', 'has_upper', 'has_lower', 'has_digit', 'not_in_username', 'has_special', 'no_umlauts'],
        'first_name' => ['required', 'max:255', 'no_tags'],
        'last_name' => ['required', 'max:255', 'no_tags'],
        'company' => ['required', 'max:255',  'no_tags'],
        'department' => ['max:255', 'no_tags'],
        'position' => ['max:255', 'no_tags'],
        'address' => ['max:255', 'no_tags'],
        'code' => ['max:64', 'no_tags'],
        'city' => ['max:255', 'no_tags'],
        'country' => ['required', 'max:255', 'no_tags'],
        'phone' => ['max:255', 'regex:/^[0-9\s()\/+-]+$|(n\/a)/'],
        'fax' => ['max:255', 'regex:/^[0-9\s()\/+-]+$|(n\/a)/'],
        'active' => ['integer'],
        'last_login' => ['max:255'],
        'logins' => ['integer'],
        'unlock_code' => ['max:255'],
        'role' => ['integer'],
        'newsletter' => ['integer'],
        'newsletter_include' => ['integer'],
        'newsletter_code' => ['max:255'],
        'force_pwd_change' => ['integer']
    ];

    /**
     * Default validation for before creating
     *
     * @throws ValidateException
     */
    public function creatingValidationRules()
    {
        $rules = self::$rules;
        $rules['username'][] = 'username_rule';
        $rules['username'][] = 'max:25';

        return $rules;
    }

    /**
     * Default validation for before updating
     *
     * @throws ValidateException
     */
    public function updatingValidationRules()
    {
        $user_id = $this->id;
        $rules = self::$rules;

        if (strtotime($this->getOriginal('created_at')) < strtotime($this->new_users_date)) {
            unset($rules['company'][$this->company_required_rule_index]);
            unset($rules['country'][$this->country_required_rule_index]);
        }

        if ($this->getOriginal('username') != $this->username) {
            $rules['username'][] = 'username_rule';
            $rules['username'][] = 'max:25';
        }
        //if(preg_match('/^\$2[ay]\$(0[4-9]|[1-2][0-9]|3[0-1])\$[a-zA-Z0-9.\/]{53}/',$this->input['password'])){
        $user_pass = $this->password;
        if (substr($user_pass, 0, 3) == '$P$' && strlen($user_pass) == 34) {
            unset($rules['password']);
        }

        $rules['username'][$this->username_unique_rule_index] .= ','.$user_id;

        return $rules;
    }

    public function usernameAndPasswordValidation()
    {
        Validator::extend('username_rule', function ($attribute, $value, $parameters) {
            return preg_match("/^[a-zA-z0-9-_']*\.?[a-zA-z0-9-_']*$/", $value);
        });

        #---------
        Validator::extend('has_upper', function ($attribute, $value, $parameters) {
            return preg_match("/[A-Z]/", $value);
        });
        #---------
        Validator::extend('has_lower', function ($attribute, $value, $parameters) {
            return preg_match("/[a-z]/", $value);
        });
        #---------
        Validator::extend('has_digit', function ($attribute, $value, $parameters) {
            return preg_match("/[0-9]/", $value);
        });
        #---------
        $username = $this->username;
        Validator::extend('not_in_username', function ($attribute, $value, $parameters) use ($username) {
            $part_size = 5;

            if (strlen($username) == 0) {
                return false;
            }

            if (strlen($username) < $part_size) {
                if (strpos($value, $username) !== false) {
                    return false;
                }
            }
            for ($i = 0; $i + $part_size <= strlen($username); $i++) {
                if (strpos($value, substr($username, $i, $part_size)) !== false) {
                    return false;
                }
            }
            return true;
        });
        #---------
        Validator::extend('has_special', function ($attribute, $value, $parameters) {
            return preg_match("/\W/", $value);
        });
        #---------
        Validator::extend('no_umlauts', function ($attribute, $value, $parameters) {
            //if(preg_match("/[\x{0080}-\x{00FF}]+/u", $value)) return false;
            return mb_detect_encoding($value, 'ASCII', true);
        });

        Validator::extend('no_tags', function ($attribute, $value, $parameters) {
            return (strip_tags($value)==$value);
        });
	
    }

    // Events hooks
    public function beforeCreate()
    {
        //$model->password = Hash::make($model->password);
        $hasher = new PasswordHash(8, true);
        $this->password = $hasher->HashPassword($this->password);
        unset($this->password_confirmation);
    }

    public function afterCreate()
    {
        $this->addToGroup();
    }

    /**
     * updating hook
     */
    public function beforeUpdate()
    {
        unset($this->password_confirmation);

        if (substr($this->password, 0, 3) != '$P$' || strlen($this->password) != 34) {
            $hasher = new PasswordHash(8, true);
            $this->password = $hasher->HashPassword($this->password);
        }
    }

    /**
     * deleting hook
     */
    public function beforeDelete()
    {
        $ids = [];
        $ids[] = config('app.default_workflow_admin');
        $ids[] = config('app.default_workflow_admin2');
        $cancel = in_array($this->id, $ids);

        return !$cancel;
    }

    /**
     * Save the model to the database.
     *
     * @param array   $rules
     * @param array   $customMessages
     * @param array   $options
     * @param Closure $beforeSave
     * @param Closure $afterSave
     *
     * @return bool
     * @see Ardent::forceSave()
     */
    
    public function save(array $rules = array(),
        array $customMessages = array(),
        array $options = array(),
        \Closure $beforeSave = null,
        \Closure $afterSave = null
    ) {
	if( $this->role != self::ADMIN_ROLE_ID ) // if the user is admin, the email should be unique, if regular user email can repeat
	{
		$rules["email"] = "required|max:255";
	}

	$rulesToAdd = $rules;
        $this->usernameAndPasswordValidation();
	if (!$this->exists) {
            $rules = $this->creatingValidationRules();
        } else {
            $rules = $this->updatingValidationRules();
        }
	$rules = array_merge($rules, $rulesToAdd);
	// IF FE set the language from validation_fe.php
	if (config('app.isFE')) {
            $customMessages         = ValidationHelper::getFEValidationTranslation();
            User::$customAttributes = $customMessages['attributes'];
        }
        // file_put_contents('/srv/www/dbdn-l55/storage/pdf/validation.log', "Custom msgs: ".var_export($this->customAttributes,true)."\n",FILE_APPEND);
	
	return $this->internalSave($rules, $customMessages, $options, $beforeSave, $afterSave, false);
	}



    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return '';
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }


    // Model

    /**
     *  Add user to member group on create
     */
    public function addToGroup()
    {
        $usergroup_item = new Usergroup();
        $usergroup_item->user_id = $this->id;
        $usergroup_item->group_id = 1;
        $usergroup_item->save();
    }

    private function formatDate($value, $defaultOutput)
    {
        $outputDate = strtotime($value);
        if ($outputDate <= 0) {
            return $defaultOutput;
        } else {
            return date('d.m.Y', $outputDate);
        }
    }

    private function formatDateTime($value, $defaultOutput)
    {
        $outputDate = strtotime($value);
        if ($outputDate <= 0) {
            return $defaultOutput;
        } else {
            return date('Y-m-d H:i:s', $outputDate);
        }
    }

    public function getLastLoginAttribute($value)
    {
        return $this->formatDate($value, "-");
    }

    public function getCreatedAtAttribute($value)
    {
        return $this->formatDateTime($value, "-");
    }

    public function getUpdatedAtAttribute($value)
    {
        return $this->formatDateTime($value, "-");
    }

    /**
     *
     */
    public function groups()
    {
        return $this->belongsToMany(\App\Models\Group::class, 'usergroup', 'user_id', 'group_id');
    }

    public function resolveRole()
    {
        if ($this->role == USER_ROLE_EDITOR) {
            return trans('backend_users.form.role.approver');
        } elseif ($this->role == USER_ROLE_ADMIN) {
            return trans('backend_users.form.role.admin');
		} elseif ($this->role == USER_ROLE_EXPORTER) {
			return trans('backend_users.form.role.exporter');
        } elseif ($this->role == USER_ROLE_EDITOR_SMART) {
            return trans('backend_users.form.role.editor_smart');
        } elseif ($this->role == USER_ROLE_EDITOR_MB) {
            return trans('backend_users.form.role.editor_mb');
        } elseif ($this->role == USER_ROLE_EDITOR_DFS) {
            return trans('backend_users.form.role.editor_dfs');
        } elseif ($this->role == USER_ROLE_EDITOR_DFM) {
            return trans('backend_users.form.role.editor_dfm');
        } elseif ($this->role == USER_ROLE_EDITOR_DTF) {
            return trans('backend_users.form.role.editor_dtf');
        } elseif ($this->role == USER_ROLE_EDITOR_FF) {
            return trans('backend_users.form.role.editor_ff');
        } elseif ($this->role == USER_ROLE_EDITOR_DP) {
            return trans('backend_users.form.role.editor_dp');
        } elseif ($this->role == USER_ROLE_EDITOR_TSS) {
            return trans('backend_users.form.role.editor_tss');
		} elseif ($this->role == USER_ROLE_EDITOR_BKK) {
			return trans('backend_users.form.role.editor_bkk');
		// Brands 2018
		} elseif ($this->role == USER_ROLE_EDITOR_DB) {
			return trans('backend_users.form.role.editor_db');
		} elseif ($this->role == USER_ROLE_EDITOR_EB) {
			return trans('backend_users.form.role.editor_eb');
		} elseif ($this->role == USER_ROLE_EDITOR_SETRA) {
			return trans('backend_users.form.role.editor_setra');
		} elseif ($this->role == USER_ROLE_EDITOR_OP) {
			return trans('backend_users.form.role.editor_op');
		} elseif ($this->role == USER_ROLE_EDITOR_BS) {
			return trans('backend_users.form.role.editor_bs');
		} elseif ($this->role == USER_ROLE_EDITOR_FUSO) {
			return trans('backend_users.form.role.editor_fuso');
		} elseif ($this->role == USER_ROLE_EDITOR_DT) {
			return trans('backend_users.form.role.editor_dt');
		// Newsletter
        } elseif ($this->role == USER_ROLE_NEWSLETTER_APPROVER) {
            return trans('backend_users.form.role.newsletter_approver');
        } elseif ($this->role == USER_ROLE_NEWSLETTER_EDITOR) {
            return trans('backend_users.form.role.newsletter_editor');
        } else {
            return trans('backend_users.form.role.member');
        }
    }

    public function isPageAuth($pageID)
    {
        return DB::table('request_auth')->where('user_id', $this->id)->where('page_id', $pageID)->whereNull('deleted_at')->where('active', 1)->count() > 0;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token) {
        $this->notify(new MailResetPasswordToken($token));
    }

    public function getDeletedUserData(int $user_id, ?string $username = null) {
        foreach($this->getFillable() as $column) {
            $this->$column = '';
        }

        $this->user_id = $user_id;
        if ($username !== null) {
            $this->username = $username;
        }
        else {
            $this->username = trans('profile.anonymous').' ('.$this->user_id.') ('. trans('profile.deleted') .')';
        }

        $this->first_name = trans('profile.anonymous').' ('.$this->user_id.')';
        $this->last_name = '('. trans('profile.deleted') .')';

        return $this;
    }

    public function GDPRErase() {
        $admin_id = config('app.default_workflow_admin');
        $user_id = $this->id;
        $user_email = $this->email;
        DB::unprepared("call gdpr_erase($user_id,'$user_email',$admin_id);");

        Log::info('Deleted user with ID '.$user_id);
    }

    public function requestGDPRErase() {
        if ($this->requested_delete_at !== null) {
            return false; //The user has already requested an erase - do nothing
        }
        else {
            $this->requested_delete_at = date('Y-m-d H:i:s');
            $this->save();

            $this->notify(new MailGDPRErase($this->generateGDPREraseToken()));
            return true;
        }
    }

    private function generateGDPREraseToken() {
        return md5($this->password . $this->requested_delete_at);
    }

    public function validateGDPREraseToken(string $token) {
        if ($this->generateGDPREraseToken() == $token) return true;
        return false;
    }
}

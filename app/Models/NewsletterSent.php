<?php

namespace App\Models;

use LaravelArdent\Ardent\Ardent;

class NewsletterSent extends Ardent
{

    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'newsletter_sent';
    protected $primaryKey = 'newsletter_sent_id';
    protected $guarded = ['newsletter_sent', 'created_at', 'updated_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'newsletter_id' => ['required', 'integer'],
        'user_id' => ['required', 'integer'],
        'status' => ['required', 'integer'],
    ];




    /*** update newsletter update_time on content update *****/
    // protected $touches = array('newsletter');

    /**
     * relation to Page
     */
    public function newsletter()
    {
        return $this->belongsTo(\App\Models\Newsletter::class);
    }
}

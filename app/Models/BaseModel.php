<?php

namespace App\Models;

use LaravelArdent\Ardent\Ardent;

class BaseModel extends Ardent
{    
    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'deleted_at';

}
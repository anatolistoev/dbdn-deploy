<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Access extends BaseModel
{

    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pagegroup';

    protected $guarded = ['created_at', 'updated_at'];

    public $incrementing = false;

    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'group_id' => ['required', 'integer'],
        'page_id' => ['required', 'integer']
    ];




    public function page()
    {
        return $this->belongsTo(\App\Models\Page::class, 'page_id');
    }

    public function group()
    {
        return $this->belongsTo(\App\Models\Group::class, 'group_id');
    }

    public function scopeReadbypage($query, $pageId)
    {

        return $query->where('page_id', '=', $pageId);
    }
}

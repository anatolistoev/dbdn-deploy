<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use LaravelArdent\Ardent\Ardent;

class FileInfo extends Ardent
{
    use SoftDeletes;

    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fileinfo';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'file_id' => ['required', 'integer'],
        'description' => ['max:2000'],
        'title' => ['max:255'],
        'lang' => ['required', 'integer']
    ];




    //public function item(){
    //	 return $this->belongsTo('App\Models\FileItem');
    //}
}

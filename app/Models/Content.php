<?php

namespace App\Models;

use App\Helpers\UriHelper;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Ardent
{
    use SoftDeletes;

    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /** 
     * List of allowed attributes for update
     */
    public static $updateFillable = ['body'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'content';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'page_id' => ['required', 'integer'],
        'lang' => ['required', 'integer'],
        'position' => ['required', 'integer'],
        'section' => ['required', 'max:255'],
        'format' => ['required', 'integer'],
        'body' => ['max:5592405'],//remove 'required', because of can have empty content
        'searchable' => ['required', 'integer']
    ];




    /*** update page update_time on content update *****/
    protected $touches = ['page'];

    // Events hooks
    /**
     * created hook
     */
    public function afterCreate()
    {
        $this->logHistory('created');
    }

    /**
     * updated hook
     */
    public function afterUpdate()
    {
        $this->logHistory('modified');
    }

    /**
     * deleted hook
     */
    public function afterDelete()
    {
        $this->logHistory('removed');
    }   

    /**
     * relation to Page
     */
    public function page()
    {
        return $this->belongsTo(\App\Models\Page::class, 'page_u_id');
    }

    /**
     * scope: current language only
     */
    public function scopeCurrentLang($query)
    {
        return $query->where('lang', '=', Session::get('lang'));
    }

    public function logHistory($action)
    {
        if (Auth::check()) {
            $history = new History();
            $history->content_section = $this->section;
            $history->username = Auth::user()->username;
            $history->user_id = Auth::user()->id;
            $history->ip = UriHelper::getClientIpAddr(Request::instance());
            $history->action = $action;
            $page = $this->page()->first();
            // Check if page is deleted
            if ($page == null) {
                $page = $this->page()->withTrashed()->first();
            }
            $history->page_id = $page->id;
            $history->page_slug = $page->slug;
            $history->content_id = $this->id;
            $history->save();
        }
    }
}

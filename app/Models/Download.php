<?php

namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Download extends Ardent
{
    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'download';

    protected $guarded = ['created_at', 'updated_at'];

    public $incrementing = false;

    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'page_id' => ['required', 'integer'],
        'file_id' => ['required', 'integer'],
        'position' => ['required', 'integer']
    ];




    public function file()
    {
        return $this->belongsTo(\App\Models\FileItem::class, 'file_id');
    }

    public function page()
    {
        return $this->belongsTo(\App\Models\Page::class, 'page_u_id');
    }
}

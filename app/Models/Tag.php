<?php

namespace App\Models;

use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends BaseModel
{
    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tag';

    protected $guarded = ['created_at', 'updated_at'];

    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'name' => ['required', 'max:255'],
        'lang' => ['required', 'integer']
    ];




    /**
     * scope: Tags of current language only
     */
    public function scopeCurrentLang($query)
    {
        $lang = Session::get('lang');

        return $this->scopeOfLang($query, $lang);
    }

    /**
     * scope: Tags of given language only
     */
    public function scopeOfLang($query, $lang)
    {
        return $query->where('lang', '=', $lang);
    }
}

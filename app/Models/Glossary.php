<?php

namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Glossary extends Ardent
{
    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'glossary';

    protected $guarded = ['created_at', 'updated_at'];

    public $incrementing = false;

    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'page_id' => ['required', 'integer'],
        'term_id' => ['required', 'integer'],
        'position' => ['required', 'integer']
    ];




    public function terms()
    {
        return $this->belongsTo(\App\Models\Term::class);
    }
}

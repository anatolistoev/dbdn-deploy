<?php

namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faqtag extends Ardent
{
    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'faqtag';

    protected $guarded = ['created_at', 'updated_at'];

    public $incrementing = false;

    protected $hidden = ['deleted_at'];
    protected $primaryKey = 'tag_id';

    public $throwOnValidation = true;

    public static $rules = [
        'tag_id' => ['required', 'integer'],
        'faq_id' => ['required', 'integer']
    ];




    public function faq()
    {
        return $this->belongsTo(\App\Models\Faq::class, 'faq_id');
    }

    public function tag()
    {
        return $this->belongsTo(\App\Models\Tag::class, 'tag_id');
    }

    public function scopeReadbyfaq($query, $faqId)
    {

        return $query->where('faq_id', '=', $faqId);
    }
}

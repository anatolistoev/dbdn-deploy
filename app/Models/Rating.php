<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rating extends Ardent
{
    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rating';

    protected $guarded = ['created_at', 'updated_at'];

    protected $hidden = ['deleted_at'];

    public $incrementing = false;
    #protected $primaryKey = array('user_id','page_id',);  # Not supported
    protected $primaryKey = 'page_id';

    public $throwOnValidation = true;

    public static $rules = [
        'page_id' => ['required', 'integer'],
        'user_id' => ['required', 'integer'],
        'vote' => ['required', 'integer', 'between:1,10']
    ];




    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function page()
    {
        return $this->belongsTo(\App\Models\Page::class, 'page_id', 'id');
    }

    /**
     * Scope "authuser";
     * Returns rating for the current user only;
     * Returns 0 if the user is unauthorized
     *
     */
    public function scopeAuthuser($query)
    {
        if (Auth::check()) {
            return $query->where('user_id', '=', Auth::user()->id);
        } else {
            return $query->where('user_id', '=', 0);
        }
    }

    public function scopeRead($query, $page_id, $user_id)
    {
        $query->where('page_id', '=', $page_id)->where('user_id', '=', $user_id);

        return $query;
    }

    public function scopeReadavg($query, $page_id)
    {
        $ratings = $query->where('page_id', '=', $page_id)->get();

        if ($ratings->isEmpty()) {
            return -1;
        }

        $avg_r = 0;
        foreach ($ratings as $r) {
            $avg_r += $r->vote;
        }
        $avg_r = round($avg_r / $ratings->count(), 2);

        return $avg_r;
    }
}

<?php

namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Ardent
{
    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'group';

    protected $guarded = ['created_at', 'updated_at'];

    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'name' => ['required', 'max:255'],
        'visible' => ['integer'],
        'active' => ['integer']
    ];




    /**
     * Relation to users
     */
    public function users()
    {
        return $this->belongsToMany(\App\Models\User::class, 'usergroup', 'group_id', 'user_id');
    }

    /**
     *
     */
    public function scopeAllgroups($query, $req)
    {
        $sort_fields = ['id', 'name', 'visible', 'active', 'updated_at', 'created_at'];

        if (empty($req['filter'])) {
            $filter = null;
        } else {
            $filter = $req['filter'];
        }

        if (!empty($req['sort-by']) && in_array($req['sort-by'], $sort_fields)) {
            $field = $req['sort-by'];
        } else {
            $field = null;
        }
        if (!empty($req['sort-direction']) && strtoupper($req['sort-direction']) == "DESC") {
            $order = "DESC";
        } else {
            $order = "ASC";
        }

        if (is_null($filter) && is_null($field)) {
            $groups = $query->get();
        } elseif (!is_null($filter) && is_null($field)) {
            $groups = $query->where('name', 'like', "%" . $filter . "%")->get();
        } elseif (!is_null($filter) && !is_null($field)) {
            $groups = $query->where('name', 'like', "%" . $filter . "%")->orderBy($field, $order)->get();
        }

        return $groups;
    }

    private function formatDate($value, $defaultOutput)
    {
        $outputDate = strtotime($value);
        if ($outputDate <= 0) {
            return $defaultOutput;
        } else {
            return date('d.m.Y', $outputDate);
        }
    }

    /**
     *
     */
    public function getCreatedAtAttribute($value)
    {
        return $this->formatDate($value, "-");
    }

    /**
     *
     */
    public function getUpdatedAtAttribute($value)
    {
        return $this->formatDate($value, "-");
    }
}

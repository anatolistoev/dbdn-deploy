<?php

namespace App\Models;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Ardent
{
    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comment';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'user_id' => ['required', 'integer'],
        'page_id' => ['required', 'integer'],
        'lang' => ['required', 'integer'],
        'subject' => ['required'],
        'body' => ['required'],
        'active' => ['integer']
    ];

    public function setUserAttribute($value) {
        $this->relations['user'] = $value;
    }

    public function page()
    {
        return Page::where('page.id', '=', $this->page_id);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function setSubjectAttribute($value)
    {
        $this->attributes['subject'] = Crypt::encrypt($value);
    }

    public function getSubjectAttribute($value)
    {
        if ($value == "") {
            return $value;
        }

        return Crypt::decrypt($value);
    }

    public function setBodyAttribute($value)
    {
        $this->attributes['body'] = Crypt::encrypt($value);
    }

    public function getBodyAttribute($value)
    {
        if ($value == "") {
            return $value;
        }

        return Crypt::decrypt($value);
    }

    private function formatDate($value, $defaultOutput)
    {
        $outputDate = strtotime($value);
        if ($outputDate <= 0) {
            return $defaultOutput;
        } else {
            return date('d.m.Y H:i', $outputDate);
        }
    }

    public function getCreatedAtAttribute($value)
    {
        return $this->formatDate($value, "-");
    }

    public function getUpdatedAtAttribute($value)
    {
        return $this->formatDate($value, "-");
    }

    public function scopeJoinUserData($query)
    {
        return $query->leftJoin('user', function ($join) {
            $join->on('user.id', '=', $this->table . '.user_id');
        });
    }

    public function scopeJoinPageUserData($query, $user_id)
    {
        return $query->leftJoin('content_userdata', function ($join) use ($user_id) {
            $join->on('content_userdata_content_id', '=', 'content.content_id')
                ->on('content_userdata_user_id', '=', DB::raw($user_id));
        });
    }
}

<?php

namespace App\Models;

use Illuminate\Support\Facades\Session;
use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;


class NewsletterContent extends Ardent
{
    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /** 
     * List of allowed attributes for update
     */
    public static $updateFillable = ['content'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'newsletter_content';

    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'newsletter_id' => ['required', 'integer'],
        'lang' => ['required', 'integer'],
        'position' => ['required', 'in:title,header_d,header_m,footer_i,footer_e,footer_e_bl,content'],
        'content' => ['required'],
    ];




    /*** update newsletter update_time on content update *****/
    protected $touches = ['newsletter'];

    /**
     * relation to Page
     */
    public function newsletter()
    {
        return $this->belongsTo(\App\Models\Newsletter::class);
    }

    /**
     * scope: current language only
     */
    public function scopeCurrentLang($query)
    {
        return $query->where('lang', '=', Session::get('lang'));
    }
}

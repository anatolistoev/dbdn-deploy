<?php

namespace Validation;

/**
 * Base validation service
 *
 * @author Martin Stefanov
 */
abstract class BaseValidationService extends ValidationService
{

    /**
     *
     */
    public function __construct($input, $validator)
    {
        parent::__construct($input, $validator);
        if (config('app.isFE')) {
            $this->messages = self::getFEValidationTranslation();
        }
    }

    public static function getFEValidationTranslation()
    {
        $locale = \Illuminate\Support\Facades\App::getLocale();

        return \Illuminate\Support\Facades\File::getRequire(base_path() . '/app/lang/' . $locale . '/validation_fe.php');
    }

    /**
     * Validates the input.
     *
     * @throws ValidateException
     * @return void
     */
    protected function validate()
    {
        //Create the validator
        $this->validator = $this->validatorFactory->make($this->input, $this->rules, $this->messages);
        // Set custom attributes Names for FE
        if (config('app.isFE')) {
            if (isset($this->messages['attributes'])) {
                $this->validator->setAttributeNames($this->messages['attributes']);
            }
        }

        //If the validator fails, trhow a exception
        if ($this->validator->fails()) {
            throw new ValidateException(get_called_class(), $this->validator);
        } else {
            return true;
        }
    }
}

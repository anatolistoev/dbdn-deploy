<?php

namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class History extends Ardent
{
    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'history';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'page_slug' => ['required', 'max:255'],
        'page_id' => ['integer'],
        'content_section' => ['max:255'],
        'content_id' => ['integer'],
        'username' => ['required', 'max:255'],
        'user_id' => ['integer'],
        'action' => ['required', 'in:created,modified,removed'],
        'ip' => ['required', 'max:255']
    ];




    public function scopeShowall($query, $from = null, $page_id = null, $user_id = null, $content_id = null)
    {
        if (!is_null($from)) {
            $query->where('created_at', '>=', $from);
        }
        if (!is_null($page_id)) {
            $query->where('page_id', '=', $page_id);
        }
        if (!is_null($content_id)) {
            $query->where('content_id', '=', $content_id);
        }
        if (!is_null($user_id)) {
            $query->where('user_id', '=', $user_id);
        }
        
        return $query;
    }

    public function getCreatedAtAttribute($value)
    {
        if (strtotime($value) > 0) {
            return date('d.m.Y H:i', strtotime($value));
        }

        return "-";
    }

    public function getUpdatedAtAttribute($value)
    {
        if (strtotime($value) > 0) {
            return date('d.m.Y H:i', strtotime($value));
        }

        return "-";
    }
}

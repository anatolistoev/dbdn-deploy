<?php namespace App\Observers;


class Content
{

    /**
     * created hook
     *
     * @param \App\Models\Content $model
     */
    public function created(\App\Models\Content $model)
    {

        $model->logHistory('created');
    }

    /**
     * updated hook
     *
     * @param \App\Models\Content $model
     */
    public function updated(\App\Models\Content $model)
    {

        $model->logHistory('modified');
    }

    /**
     * deleting hook
     *
     * @param \App\Models\Content $model
     */
    public function deleted(\App\Models\Content $model)
    {

        $model->logHistory('removed');
    }
}

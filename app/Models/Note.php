<?php

namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Note extends Ardent
{
    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notes';

    protected $guarded = ['created_at', 'updated_at'];

    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'page_id' => ['required', 'integer'],
        'text' => ['required'],
        'user_id' => ['required', 'integer'],
        'date' => ['required']
    ];




    public function page()
    {
        return $this->belongsTo(\App\Models\Page::class, 'page_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function setUserAttribute($value) {
        $this->relations['user'] = $value;
    }
}

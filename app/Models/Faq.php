<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends Ardent
{
    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'faq';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'question' => ['required', 'max:500'],
        'answer' => ['required'],
        'category_id' => ['required', 'integer'],
        'brand_id' => ['required', 'integer']
    ];




    /**
     * scope: Tags of current language only
     */
    public function scopeCurrentLang($query)
    {
        $lang = Session::get('lang');

        return $this->scopeOfLang($query, $lang);
    }

    /**
     * scope: Tags of given language only
     */
    public function scopeOfLang($query, $lang)
    {
        return $query->where('faq.lang', '=', $lang);
    }
    /**
     * scope: Random Faqs of the day
     */
    public function scopeFaqOfTheDay($query, $lang, $queryAddon)
    {
        return $query->where('faq.lang', '=', $lang)
                        ->whereRaw('`faq`.`brand_id` = ' . DAIMLER)
                        ->whereRaw($queryAddon . ' length(`faq`.`answer`) between 100 and 500 ')
                        ->orderBy(DB::raw('RAND()'));
    }
    /**
     * Relation to Tag
     */
    public function tags()
    {
        return $this->belongsToMany(\App\Models\Tag::class, 'faqtag', 'faq_id', 'tag_id')->whereNull('faqtag.deleted_at')->orderBy('faqtag.updated_at', 'asc');
    }

    public function rates()
    {
        return $this->hasMany(\App\Models\Faqrate::class, 'faq_id');
    }

    //Only for Daimler barnd page or smart
    //return page slug, id, title and brand
    // have to check FAQ is deleted
    public function getPageRelatedArray()
    {
        $brand = $this->brand_id == SMART ? SMART : DAIMLER;
        $query = self::getPageRelatedQuery($this->id, $this->lang, $brand);

        $related = $query->get();
//        $queries = DB::getQueryLog();
//        $last_query = end($queries);
//        print_r($last_query);die;
        return $related;
    }

    public static function getPageRelatedQuery($faq_id_arr, $lang, $brand_id)
    {
        //TODO: Do we need brand query?
        $brandQuery = 'SELECT h.parent_id  FROM hierarchy h
                        WHERE h.page_id = p.id AND h.parent_id <> 1
                        order by h.`level` desc
                        LIMIT 1';
        $brandQuery = $brand_id;

        $query = DB::table('faqtag as ft')
            ->select('ft.faq_id', DB::raw('p.slug, p.u_id, c.body,
                     (' . $brandQuery . ') as brand_id'))
            ->join('pagetag as pt', 'ft.tag_id', '=', 'pt.tag_id')
            ->join('page as p', 'p.u_id', '=', 'pt.page_id')
            ->join('content as c', 'p.u_id', '=', 'c.page_u_id')
            ->whereNull('ft.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('c.lang', '=', $lang)
            ->where('c.section', '=', 'title')
            ->whereNull('c.deleted_at')
            ->where('p.active', '=', 1)
            ->where('p.deleted', '=', 0)
            ->whereNull('p.deleted_at')
//                ->whereNotExists(function($query){
//                    $query->select(DB::raw('*'))
//                        ->from('hierarchy as h2')
//                        ->leftJoin('page as p3', 'h2.parent_id', '=', 'p3.id')
//                        ->whereNull('p3.deleted_at')
//                        ->where('h2.page_id', '=', DB::raw('p.id'))
//                        ->where(function($query){
//                            $query->orWhere('p3.active','=', 0)
//                                    ->orWhere('p3.deleted','=', 1)
//                                    ->orWhere('p3.publish_date', '>',date('Y-m-d H:i:s'))
//                                    ->orWhere(function($query)
//                                        {
//                                            $query->where('p3.unpublish_date', '<>', DATE_TIME_ZERO)
//                                                  ->where('p3.unpublish_date', '<', date('Y-m-d H:i:s'));
//                                        });
//                        });
//                })
            ->whereRaw('((SELECT Count(*)
                        FROM hierarchy hAlive
                        LEFT JOIN page pAlive ON hAlive.parent_id = pAlive.id
                        WHERE hAlive.page_id = p.id AND pAlive.active = 1 AND pAlive.id <> p.id
                        AND ( pAlive.publish_date = "' . DATE_TIME_ZERO . '" OR pAlive.publish_date < "' . date('Y-m-d H:i:s') . '")
                        AND ( pAlive.unpublish_date = "' . DATE_TIME_ZERO . '"	OR pAlive.unpublish_date > "' . date('Y-m-d H:i:s') . '"))
                        =
                        (Select hierarchy.level FROM hierarchy where hierarchy.page_id = p.id AND hierarchy.parent_id = 1))');

        $query = $query->where(DB::raw("(SELECT h.parent_id  FROM hierarchy h
                    WHERE h.page_id = p.id AND h.parent_id <> 1
                    order by h.`level` desc
                    LIMIT 1)"), '=', $brand_id);

        if (!empty($faq_id_arr)) {
            $query->whereIn('ft.faq_id', $faq_id_arr);
        }

        $query = $query->groupBy('ft.faq_id', 'p.id')
            ->having(DB::raw('COUNT(ft.tag_id)'), '>=', MIN_MATCH)
            ->orderBy(DB::raw('ft.faq_id, COUNT(ft.tag_id)'), 'desc');

        return $query;
    }
}

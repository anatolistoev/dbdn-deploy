<?php 

namespace App\Models\Extensions;

// Workaround for Old user data crypted with L40 Encrypter
use App\Helpers\Encrypter as CryptL40;

trait Encryptable
{
    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);
        
        if ($value == "") {
            return $value;
        }

        if (in_array($key, $this->encryptable)) {
            $value = CryptL40::decrypt($value);
        }

        return $value;
    }

    public function setAttribute($key, $value)
    {
        if ($value != "" && in_array($key, $this->encryptable)) {
            $value = CryptL40::encrypt($value);
        }

        return parent::setAttribute($key, $value);
    }
    
    public function attributesToArray()
    {
        $attributes = parent::attributesToArray(); // call the parent method

        foreach ($this->encryptable as $key) {
            if (isset($attributes[$key])){
                $value = $attributes[$key];
                if ($value != "") {
                    $attributes[$key] = CryptL40::decrypt($value);
                }
            }
        }
        return $attributes;
    }

    public function encryptAttributes(array &$attributes)
    {
        foreach ($this->encryptable as $key) {
            if (isset($attributes[$key])){
                $value = $attributes[$key];
                if ($value != "") {
                    $attributes[$key] = CryptL40::encrypt($value);
                }
            }
        }

        return $attributes;
    }

    /**
     * Get all of the current attributes on the model.
     *
     * @return array
     */
    public function getAttributes()
    {
        $attributes = $this->attributes;

        foreach ($this->encryptable as $key) {
            if (isset($this->attributes[$key])){
                $value = $attributes[$key];
                if ($value != "") {
                    $attributes[$key] = CryptL40::decrypt($value);
                }
            }
        }

        return $attributes;
    }

    /**
     * Get the model's original attribute values.
     *
     * @param  string|null  $key
     * @param  mixed  $default
     * @return mixed|array
     */
    public function getOriginal($key = null, $default = null)
    {
        $value = parent::getOriginal($key, $default);
        
        if ($value == "" || $value == $default) {
            return $value;
        }
        
        if (in_array($key, $this->encryptable)) {
            $value = CryptL40::decrypt($value);
        }

        return $value;
    }

    /**
     * Get the attributes that have been changed since last sync.
     *
     * @return array
     */
    public function getDirty()
    {
        $dirty = parent::getDirty();
        $this->encryptAttributes($dirty);
        
        return $dirty;
    }    
}
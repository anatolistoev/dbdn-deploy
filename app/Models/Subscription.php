<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

use LaravelArdent\Ardent\Ardent;

class Subscription extends Ardent
{
    use SoftDeletes;

    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subscription';

    protected $guarded = ['created_at', 'updated_at'];

    protected $hidden = ['deleted_at'];

    public $incrementing = false;

    public $throwOnValidation = true;

    public static $rules = [
        'user_id' => ['required', 'integer'],
        'page_id' => ['required', 'integer']
    ];

    public function scopePage($query)
    {
        return $query->join('page', 'page.id', '=', 'subscription.page_id')->where('subscription.active', 1)->where('page.active', 1);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    /**
     * Scope "authuser";
     * Filters subscriptions for the current user only;
     * Returns no subscriptions if the user is unauthorized
     *
     */
    public function scopeAuthuser($query)
    {
        if (Auth::check()) {
            return $query->where('user_id', '=', Auth::user()->id);
        } else {
            return $query->where('user_id', '=', 0);
        }
    }

    public function scopeReadbyuser($query,$userId){
		return $query->where('user_id', '=', $userId);
	}
	
	public function scopeReadbypage($query,$pageId){
        return $query->where('page_id', '=', $pageId);
    }
}

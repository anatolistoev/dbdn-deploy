<?php

namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Workflow extends Ardent
{
    use SoftDeletes;
    //setting $timestamp to true so Eloquent
    //will automatically set the created_at
    //and updated_at values

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'workflow';

    protected $guarded = ['id', 'page_id', 'assigned_by', 'created_at', 'updated_at'];

    protected $hidden = ['deleted_at'];

    public $throwOnValidation = true;

    public static $rules = [
        'page_id' => ['required', 'integer'],
        'user_responsible' => [],
        'due_date' => [],
        'editing_state' => ['required', 'in:Draft,Review,Approved'],
        'assigned_by' => ['integer']
    ];

    public function page()
    {
        return $this->belongsTo(\App\Models\Page::class, 'page_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_responsible');
    }
}

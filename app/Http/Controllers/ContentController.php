<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Helpers\ContentUpdater;
use App\Helpers\DispatcherHelper;
use App\Helpers\UserAccessHelper;
use App\Models\Content;
use App\Models\Page;
use App\Models\Related;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Description of ContentController
 *
 * @author m.stefanov, v.apostolov
 */
class ContentController extends RestController
{

    /**
     * Return Content by ID
     * URL: content/read/{ID}
     * METHOD: GET
     */
    public function getRead($id = null)
    {
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $content = Content::find($id);

        if (is_null($content)) {
            return Response::json(RestController::generateJsonResponse(true, 'Content not found.'), 404);
        } else {
            // Check acsess
            if (is_null($content->page) || !UserAccessHelper::hasAccessForPageAndAscendants($content->page->id)) {
                return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
            }

            return Response::json(RestController::generateJsonResponse(false, 'Content found.', $content));
        }
    }

    /**
     * Return Content for Language
     * URL: contents/page-id/{pageId}/{lang?}?section={section}
     * METHOD: GET
     */
    public function getPageId($pageId = null, $lang = 0)
    {
        if (empty($pageId)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.parameter')), 400);
        }
        $page = Page::find($pageId);
        // Check acsess
        if (is_null($page) || !UserAccessHelper::hasAccessForPageAndAscendants($page->id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
        }

        // Build query
        $contentsQuery = Content::where('page_u_id', '=', $pageId);

        if ($lang <> 0) {
            $contentsQuery->where('lang', '=', $lang);
        }

        if (Request::has('section')) {
            $section = Request::get('section');
            $contentsQuery->where('section', '=', $section);
        }

        $contents = $contentsQuery->get();

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of contents found.', $contents), 200);

        return $jsonResponse;
    }

    /**
     * Save content from JSON
     * URL: contents/create
     * METHOD: POST
     */
    public function postCreate()
    {
        $newContent = Request::json()->all();

        return $this->createMany([$newContent]);
    }

    /**
     * Save content from JSON
     * URL: contents/related
     * METHOD: POST
     */
    public function postRelated()
    {
        $newRelateds = Request::json()->all();
        $arrContent = [];
        $arrUpdate = [];
        $page = null;
        foreach ($newRelateds as $newRelated) {
            if (empty($newRelated['related_id']) || empty($newRelated['page_u_id'])) {
                return Response::json(RestController::generateJsonResponse(true, 'Parameter page ID is required!'), 400);
            }
            $related = Related::where('related_id', $newRelated['related_id'])->where('page_u_id', $newRelated['page_u_id'])->first();
            if (is_null($related)) {
                $related = new Related();
                $page = Page::find($newRelated['page_u_id']);
            } else {
                $page = $related->page;
            }

            if (is_null($page) || !UserAccessHelper::hasAccessForPageAndAscendants($page->id)) {
                return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
            }
    
            $related->fill($newRelated);
            $arrContent[] = $related;
            $arrUpdate[] = $related->toArray();
        }

        $reponse = $this->trySave($arrContent, true, true);
        if ($reponse->isSuccessful()) {
            return Response::json($arrUpdate, 200);
        } else {
            return $reponse;
        }
    }

    /**
     * Reset related pages
     * URL: contents/reset-related
     * METHOD: POST
     */
    public function postResetRelated()
    {
        $data = Request::json()->all();
        if (empty($data['page_u_id'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page ID is required!'), 400);
        }
        if (empty($data['lang'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter lang is required!'), 400);
        }
        $page = Page::find($data['page_u_id']);
        if (!$page) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is wrong'), 400);
        }
        $ascendants = Page::ascendants($page->id, $data['lang'])->get();
        if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id, $ascendants)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
        }

        // Reset the related pages
        Related::where('page_u_id', $page->u_id)->delete();
        $sectionId = $page->parent_id == HOME ? $page->id : DispatcherHelper::getActiveL2($ascendants);
        $allRelated = $page->getPageRelatedArray($data['lang'], $sectionId);
        //print_r($allRelated);die;
        # generate [id=>title, ...] array of:
        $relatedPages = []; // related pages
        $position = [];
        $relatedDownloads = []; // related downloads
        $positionDownload = [];
        $relatedBP = [];// related BP pages
        $positionBP = [];
        foreach ($allRelated as $relative) {
            if (!$relative->body) {
                continue; #skip if there's no title
            }
            # separate related records to "pages" & "downloads" & 'best_practise'
            if ($relative->template == 'best_practice') {
                if (is_null($relative->position)) {
                    $relatedBP[] = $relative;
                } else {
                    $positionBP[] = $relative;
                }
            } else {
                if ($relative->template == 'downloads') {
                    if (is_null($relative->position)) {
                        $relatedDownloads[] = $relative;
                    } else {
                        $positionDownload[] = $relative;
                    }
                } else {
                    if (is_null($relative->position)) {
                        $relatedPages[] = $relative;
                    } else {
                        $position[] = $relative;
                    }
                }
            }
        }
        $allRelated = null;
        $relatedPages = ContentUpdater::sortRelated($position, $relatedPages);
        $relatedDownloads = ContentUpdater::sortRelated($positionDownload, $relatedDownloads);
        $relatedBP = ContentUpdater::sortRelated($positionBP, $relatedBP);
        $content = view('partials_backend.related_links', 
            ['PAGE' => $page, 'RELATED_PAGES' => $relatedPages, 'RELATED_DOWNLOADS' => $relatedDownloads, 'RELATED_BP' => $relatedBP]
        )->render();

        return Response::json(RestController::generateJsonResponse(false, 'Reset related pages.', ['content' => $content]), 200);
    }

    /**
     * Save content from JSON
     * URL: contents/create-many
     * METHOD: POST
     */
    public function postCreateMany()
    {
        $newContentImput = Request::json()->all();
        
        return $this->createMany($newContentImput);
    }


    protected function createMany($arrCreateContent)
    {
        // Validate
        $arrPageID = [];
        
        foreach ($arrCreateContent as $newContent) {
            if (empty($newContent['page_u_id'])) {
                return Response::json(RestController::generateJsonResponse(true, 'Parameter page ID is required!'), 400);
            }

            $pageID = $newContent['page_u_id'];
            $arrPageID[$pageID] = $pageID;
        }

        $arrUniquePageID = array_keys($arrPageID);
        $arrDBPage = Page::findMany($arrUniquePageID);
        if (empty($arrDBPage) || count($arrDBPage) != count($arrUniquePageID)) {
            return Response::json(RestController::generateJsonResponse(true, 'Page not found!'), 404);
        }

        // Check acsess
        foreach ($arrDBPage as $page) {
            if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id)) {
                return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
            }
            if (!UserAccessHelper::hasWritePageOrContent($page, true)) {
                $responsible_name = "";
                if ($page->workflow->user_responsible > 0) {
                    $user = User::find($page->workflow->user_responsible);
                    if (!is_null($user)) {
                        $responsible_name = trim($user->first_name) . ' ' . trim($user->last_name);
                    }
                }            
                return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.page.state_error', ['name' => $responsible_name])), 403);
            }
        }

        $arrNewDBContent = new Collection();
        foreach ($arrCreateContent as $newContent) {
            // Prepare Content
            // Clean the the ID
            unset($newContent['id']);
            // Purify HTML content
            if (isset($newContent['body'])) {
                $newContent['body'] = \Purifier::clean($newContent['body']);
            }

            $content = new Content();

            $content->fill($newContent);
            $arrNewDBContent->add($content);
        }

        return $this->trySave($arrNewDBContent, false);
    }

    /**
     * Return Update content from JSON
     * URL: contents/update/
     * METHOD: PUT
     */
    public function putUpdate()
    {
        $updateContentInput = Request::json()->all();
        return $this->updateMany([$updateContentInput]);
    }

    /**
     * Return Update content from JSON
     * URL: contents/update-many/
     * METHOD: PUT
     */
    public function putUpdateMany()
    {
        $updateContentInput = Request::json()->all();
        return $this->updateMany($updateContentInput);
    }

    protected function updateMany($arrUpdateContent)
    {
        // Validate
        $arrUpdateContentID = [];
        foreach ($arrUpdateContent as $updateContent) {
            if (empty($updateContent['id'])) {
                return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
            }
            $arrUpdateContentID[] = $updateContent['id'];
        }
        // Load contents for update
        $arrDBContent = Content::findMany($arrUpdateContentID);

        foreach ($arrUpdateContent as $updateContent) {
            $content = null;
            foreach ($arrDBContent as $struct) {
                if ($updateContent['id'] == $struct->id) {
                    $content = $struct;
                    break;
                }
            }

            if (is_null($content)) {
                return Response::json(RestController::generateJsonResponse(true, 'Content not found.', $updateContent), 404);
            }

            // Check acsess
            if (is_null($content->page) || !UserAccessHelper::hasAccessForPageAndAscendants($content->page->id)) {
                return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
            }

            if (!UserAccessHelper::hasWritePageOrContent($content->page, true)) {
                $responsible_name = "";
                if ($content->page->workflow->user_responsible > 0) {
                    $user = User::find($content->page->workflow->user_responsible);
                    if (!is_null($user)) {
                        $responsible_name = trim($user->first_name) . ' ' . trim($user->last_name);
                    }
                }
                return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.page.state_error', ['name' => $responsible_name])), 403);
            }

            // Purify HTML content in 'body'
            if (isset($updateContent['body'])) {
                $updateContent['body']=\Purifier::clean($updateContent['body']);
            }

            $safeUpdateContent = array_intersect_key($updateContent, array_flip(Content::$updateFillable));
            $content->fill($safeUpdateContent);
        }

        return $this->trySave($arrDBContent, true);
    }

    /**
     * Return save result
     */
    public function trySave($arrContent, $isUpdate = false, $isRelated = false)
    {
        try {
            // Start transaction!
            DB::beginTransaction();

            if ($isUpdate) {
                $message = "Content was updated.";
            } else {
                $message = "Content was added.";
            }

            // Disable Triggers for page hierarchy. We update only timestamp of page!
            DB::unprepared('SET @disable_trigger = 1;');
            foreach ($arrContent as $content) {
                if ($content->exists == 1 && $isRelated) {
                    Related::where('related_id', $content->related_id)
                        ->where('page_u_id', $content->page_u_id)
                        ->update($content->getAttributes());
                } else {
                    $content->save();
                }
            }

            DB::unprepared('SET @disable_trigger = NULL;');

            //Success !!!
            // Commit the queries!
            DB::commit();

            $result = Response::json(RestController::generateJsonResponse(false, $message, $arrContent));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            DB::rollback();

            throw new ModelValidationException($e->getErrors());
        } catch (\Exception $e) {
            DB::rollback();

            throw $e;
        }

        return $result;
    }

    /**
     * Return Update content from JSON
     * URL: contents/delete/{ID}
     * METHOD: DELETE
     */
    public function deleteDelete($id = null) {

        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $content = Content::find($id);

        if (is_null($content)) {
            return Response::json(RestController::generateJsonResponse(true, 'Content not found.', $content), 404);
        }

        // Check acsess
        if (is_null($content->page) || !UserAccessHelper::hasAccessForPageAndAscendants($content->page->id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
        }

        if (!UserAccessHelper::hasWritePageOrContent($content->page, true)) {
            $responsible_name = "";
            if ($content->page->workflow->user_responsible > 0) {
                $user = User::find($content->page->workflow->user_responsible);
                if (!is_null($user)) {
                    $responsible_name = trim($user->first_name) . ' ' . trim($user->last_name);
                }
            }
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.page.state_error', ['name' => $responsible_name])), 403);
        }

        $deletedcontent = $content;
        // TODO: Handle Errors!
        $content->delete();

        return Response::json(RestController::generateJsonResponse(false, 'Content was deleted.', $deletedcontent));
    }

    /**
     * Force Delete Only for tests
     * URL: contents/forcedelete/{ID}
     * METHOD: DELETE
     */
    public function deleteForcedelete($id = null) {
        $content = Content::withTrashed()->find($id);
        $content->delete();
    }
}

<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Models\Faqtag;
use App\Models\Pagetag;
use App\Models\Tag;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Description of TagController
 *
 * @author i.traykov
 */
class TagController extends RestController
{
    /**
     * Return Tags By Lang
     * URL: tag/all/{lang}
     * METHOD: GET
     */

    public function getAll($lang = 0)
    {

        $sort_fields = ['id', 'name', 'updated_at', 'created_at'];
        $req = Request::all();

        //parameter lang must be set
        if ($lang <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter lang is required!'), 400);
        }

        if (empty($req['filter'])) {
            $filter=null;
        } else {
            $filter=$req['filter'];
        }

        if (!empty($req['sort-by']) && in_array($req['sort-by'], $sort_fields)) {
            $field=$req['sort-by'];
        } else {
            $field=null;
        }
        if (!empty($req['sort-direction']) && strtoupper($req['sort-direction']) == "DESC") {
            $order="DESC";
        } else {
            $order="ASC";
        }

        if (is_null($filter) && is_null($field)) {
            $tags = Tag::where('lang', '=', $lang)->get();
        } elseif (!is_null($filter) && is_null($field)) {
            $tags = Tag::where('lang', '=', $lang)->where('name', 'like', "%".$filter."%")->orderBy('name', $order)->get();
        } elseif (!is_null($filter) && !is_null($field)) {
            $tags = Tag::where('lang', '=', $lang)->where('name', 'like', "%".$filter."%")->orderBy($field, $order)->get();
        }

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of Tags found', $tags), 200);

        return $jsonResponse;
    }

    public function getSource($lang = 0)
    {
        $req = Request::all();

        //parameter lang must be set
        if ($lang <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter lang is required!'), 400);
        }

        $tags = Tag::select('name as value', 'id')->where('lang', '=', $lang)->where('name', 'like', "%".$req['term']."%")->whereNull('deleted_at')->orderBy('name', 'ASC')->get();

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of Tags found', $tags), 200);

        return $jsonResponse;
    }

    /**
     * Return Tag by id
     * URL: tag/read/{id}
     * METHOD: GET
     */
    public function getRead($id = null)
    {
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $tag = Tag::find($id);

        if (is_null($tag)) {
            return Response::json(RestController::generateJsonResponse(true, 'Tag not found'), 404);
        } else {
            return Response::json(RestController::generateJsonResponse(false, 'Tag found.', $tag));
        }
    }


    /**
     * Create Tag
     * URL: tag/create
     * METHOD: GET
     */

    public function postCreate()
    {

        $req = Request::all();

        $same_tag = Tag::where('lang', '=', $req['lang'])->where('name', '=', $req['name'])->get();
        if (!$same_tag->isEmpty()) {
            return Response::json(RestController::generateJsonResponse(true, trans('backend_tags.error.same')), 400);
        }

        $tag = new Tag();
        $tag->fill($req);

        return $this->trySave($tag, false);
    }

    /**
     * Return save result
     */
    protected function trySave(Tag $tag, $isUpdate)
    {
        try {
            if ($isUpdate) {
                $message = "Tag was updated.";
            } else {
                $message = "Tag was added.";
            }

            $tag->save();
            //Success !!!

            $result = Response::json(RestController::generateJsonResponse(false, $message, $tag));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            throw new ModelValidationException($e->getErrors());
        }
        return $result;
    }


    /**
     * Return Update Tag from JSON
     * URL: tag/update/{$id}
     * METHOD: PUT
     */
    public function putUpdate()
    {
        $update_tag = Request::json()->all();

        //Term::validateInput($update_term);
        //TODO validate
        if (empty($update_tag['id'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $same_tag = Tag::where('lang', '=', $update_tag['lang'])->where('name', '=', $update_tag['name'])->where('id', '<>', $update_tag['id'])->get();
        if (!$same_tag->isEmpty()) {
            return Response::json(RestController::generateJsonResponse(true, trans('backend_tags.error.same')), 400);
        }

        $tag = Tag::find($update_tag['id']);
        if (is_null($tag)) {
            return Response::json(RestController::generateJsonResponse(true, 'Tag not found.', $update_tag), 404);
        }

        $tag->fill($update_tag);
        return $this->trySave($tag, true);
    }


    /**
     * Delete Tag
     * URL: tag/delete
     * METHOD: DELETE
     */

    public function deleteDelete($id = null)
    {

        $req = Request::all();

        //parameters id must be set
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $tag = Tag::find($id);

        if (is_null($tag)) {
            return Response::json(RestController::generateJsonResponse(true, 'Tag not found.'), 404);   // in response term not exists
        }
        $deletedtag = $tag;
        try {
            DB::beginTransaction();
            $pageTags = Pagetag::where('tag_id', '=', $deletedtag->id)->get();
            foreach ($pageTags as $pageTag) {
                $pageTag->delete();
            }
            $deletedtag->delete();
            DB::commit();
            $responce = Response::json(RestController::generateJsonResponse(false, 'Tag was deleted!', $tag));
        } catch (\Exception $e) {
            DB::rollback();

            $message = $e->getMessage();
            $errorArray = RestController::generateJsonResponse(true, $message);
            if (config('app.debug')) {
                // Add stack trace
                $errorArray['debug'] = ['exception_message' => $message, 'stacktrace' => $e->getTraceAsString()];
            }

            $responce = Response::json($errorArray, 500);
        }

        return $responce;
    }

    public static function updatePageTags($pageId, $arrNewTags)
    {
        $all_paged_tags = Pagetag::withTrashed()->readbypage($pageId)->get();
        $updatedTags = TagController::updateTags($arrNewTags);
        // Update page access
        foreach ($all_paged_tags as $pageTag) {
            // Check if old tag still exists
            $bKeepTag = false;
            $indexTag = -1;
            foreach ($updatedTags as $key => $tag) {
                if ($pageTag->tag_id == $tag->id) {
                    $indexTag = $key;
                    $bKeepTag = true;
                    break;
                }
            }
            if ($bKeepTag) {
                // Remove it from New Tags
                unset($updatedTags[$indexTag]);
                if ($pageTag->trashed()) {
                    Pagetag::withTrashed()
                        ->where('tag_id', '=', $pageTag->tag_id)
                        ->where('page_id', '=', $pageTag->page_id)
                        ->restore();
                } else {
                    //$updateFieldsArray = array(Pagetag::UPDATED_AT => $time);
                    //$updateFieldsArray = array();
                }
                DB::table('pagetag')
                    ->where('page_id', '=', $pageTag->page_id)
                    ->where('tag_id', '=', $pageTag->tag_id)
                    ->update([Pagetag::UPDATED_AT => date('Y-m-d H:i:s', strtotime('+'.$indexTag.' seconds'))]);
            } else {
                if (!$pageTag->trashed()) {
                    // Delete the Tag
                    Pagetag::where('tag_id', '=', $pageTag->tag_id)
                        ->where('page_id', '=', $pageTag->page_id)
                        ->delete();
                }
            }
        }

        // Save new access
        foreach ($updatedTags as $key => $tag) {
            $pageTag = new Pagetag();
            $pageTag->page_id = $pageId;
            $pageTag->tag_id = $tag->id;
            $pageTag->save();
            DB::table('pagetag')->where('page_id', '=', $pageTag->page_id)->where('tag_id', '=', $pageTag->tag_id)->update([Pagetag::UPDATED_AT => date('Y-m-d H:i:s', strtotime('+'.$key.' seconds'))]);
        }
    }

    public static function updateTags($arrNewTags)
    {
        $result = [];
        foreach ($arrNewTags as $tag) {
            $searched = Tag::withTrashed()->where('name', '=', $tag['name'])->where('lang', '=', $tag['lang'])->first();
            if (!$searched) {
                $searched = new Tag();
                $searched->name = $tag['name'];
                $searched->lang = $tag['lang'];
                $time = $searched->freshTimestamp();
                $searched->created_at = $time;
                $searched->updated_at = $time;
                $searched->save();
                $searched = Tag::where('name', '=', $tag['name'])->where('lang', '=', $tag['lang'])->first();
            } else if ($searched->trashed()) {
                Tag::withTrashed()
                    ->where('id', '=', $searched->id)
                    ->restore();
            }
            $result[] = $searched;
        }
        return $result;
    }

    public static function updateFaqTags($faqId, $arrNewTags)
    {
        $all_faq_tags = Faqtag::withTrashed()->readbyfaq($faqId)->get();
        $updatedTags = TagController::areTagsExists($arrNewTags);
        // Update page access
        foreach ($all_faq_tags as $faqTag) {
            // Check if old tag still exists
            $bKeepTag = false;
            $indexTag = -1;
            foreach ($updatedTags as $key => $tag) {
                if ($faqTag->tag_id == $tag->id) {
                    $indexTag = $key;
                    $bKeepTag = true;
                    break;
                }
            }
            if ($bKeepTag) {
                // Remove it from New Tags
                unset($updatedTags[$indexTag]);
                if ($faqTag->trashed()) {
                    Faqtag::withTrashed()
                        ->where('tag_id', '=', $faqTag->tag_id)
                        ->where('faq_id', '=', $faqTag->faq_id)
                        ->restore();
                }
                Faqtag::where('faq_id', '=', $faqTag->faq_id)
                    ->where('tag_id', '=', $faqTag->tag_id)
                    ->update([Faqtag::UPDATED_AT => date('Y-m-d H:i:s', strtotime('+'.$indexTag.' seconds'))]);
            } else {
                if (!$faqTag->trashed()) {
                    // Delete the Tag
                    Faqtag::where('tag_id', '=', $faqTag->tag_id)
                        ->where('faq_id', '=', $faqTag->faq_id)
                        ->delete();
                }
            }
        }
        // Save new access
        foreach ($updatedTags as $key => $tag) {
            $faqTag = new Faqtag();
            $faqTag->faq_id = $faqId;
            $faqTag->tag_id = $tag->id;
            $faqTag->save();
            DB::table('faqtag')->where('faq_id', '=', $faqTag->faq_id)->where('tag_id', '=', $faqTag->tag_id)->update([Faqtag::UPDATED_AT => date('Y-m-d H:i:s', strtotime('+'.$key.' seconds'))]);
        }
    }

    public static function areTagsExists($arrNewTags)
    {
        $result = [];
        foreach ($arrNewTags as $tag) {
            $searched = Tag::where('name', '=', $tag['name'])->where('lang', '=', $tag['lang'])->where('id', '=', $tag['tag_id'])->first();
            if (!$searched) {
                throw new \Exception("This tag do not exists ".$tag['name']);
            } else if ($searched->trashed()) {
                throw new \Exception("This tag was deleted ".$tag['name']);
            }
            $result[] = $searched;
        }
        return $result;
    }
}

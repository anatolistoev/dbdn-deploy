<?php

namespace App\Http\Controllers;

use App\Helpers\HtmlHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class SitemapPageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($letter = 'a')
    {

        /*$letters = DB::select("SELECT DISTINCT UPPER(LEFT(c.body,1)) AS letter
		FROM content c
		WHERE c.section='title' AND c.lang=? AND LEFT(c.body, 1) != '&' AND LEFT(c.body, 1) != '\"'
		ORDER BY letter ASC", array(Session::get('lang')));*/

        $letters = DB::select("SELECT DISTINCT UPPER(CONVERT(LEFT(c.body,1) USING latin1)) AS letter
		FROM content c
		WHERE c.section='title' AND c.deleted_at is NULL AND c.lang=? AND LEFT(c.body, 1) != '&' AND LEFT(c.body, 1) != '\"'
		ORDER BY letter COLLATE latin1_german2_ci ASC", [Session::get('lang')]);


        $pages = DB::select(
            "SELECT c.page_u_id AS id, c.body AS title, p.slug, h.parent_id, h.level, c2.body AS parentTitle, p2.slug AS parentSlug
								FROM `content` c
								LEFT JOIN page p ON p.u_id = c.page_u_id
								LEFT JOIN hierarchy h ON p.id = h.page_id
                                LEFT JOIN page p2 ON h.parent_id = p2.id
								LEFT JOIN content c2 ON p2.u_id = c2.page_u_id AND c2.section = 'title' AND c2.lang = ?
								WHERE c.section = 'title' AND c.lang = ? AND CONVERT(LEFT(c.body,1) USING latin1) = ?
								AND p.active = 1 AND p.deleted = 0 AND p.in_navigation = 1 AND p.langs >> (?-1) & 1 = 1
								AND (p.publish_date = '".DATE_TIME_ZERO."' OR p.publish_date < '".date('Y-m-d H:i:s')."')
								AND (p.unpublish_date = '".DATE_TIME_ZERO."' OR p.unpublish_date > '".date('Y-m-d H:i:s')."')
                                AND p2.active = 1
								AND h.level > 0
                                                                AND c.deleted_at is NULL
                                                                AND p.deleted_at is NULL
                                                                AND h.deleted_at IS NULL
                                                                AND c2.deleted_at IS NULL
                                                                AND p2.deleted_at IS NULL
                                AND ((SELECT Count(*)
                                FROM hierarchy hAlive
                                LEFT JOIN page pAlive ON hAlive.parent_id = pAlive.id
                                WHERE hAlive.page_id = p.id AND pAlive.active = 1 AND pAlive.id <> p.id
                                AND ( pAlive.publish_date = '".DATE_TIME_ZERO."' OR pAlive.publish_date < '".date('Y-m-d H:i:s')."')
                                AND ( pAlive.unpublish_date = '".DATE_TIME_ZERO."'	OR pAlive.unpublish_date > '".date('Y-m-d H:i:s')."'))
                                =
                                (Select hierarchy.level FROM hierarchy where hierarchy.page_id = p.id AND hierarchy.parent_id = 1))
								AND EXISTS (
									SELECT * FROM hierarchy h3
									WHERE h3.page_id = p.id
									AND h3.parent_id = 1
                                    AND h3.deleted_at is NULL
								)

								ORDER BY c.body ASC, c.page_id ASC, h.level DESC",
            [Session::get('lang'),
                                  Session::get('lang'),
                                  $letter,
                                  Session::get('lang')]
        );

        return Response::view('index', [
            'PAGE'=> (object)['id'=>'index', 'template'=>'system', 'slug' => 'index' , 'brand'=> DAIMLER],
            'LETTERS' => $letters,
            'CURR_LETTER' => '',
            'PAGES' => $pages,
            'ASCENDANTS' => []
        ]);
    }
    /**
     * Helper for recursive walk on array
     */
    private function getChildren($byparent, $current, &$result)
    {

        if (is_array($current)) {
            foreach ($current as $key => $value) {
                $this->getChildren($byparent, $value, $result);
            }
        } elseif (isset($byparent[$current]) && is_array($byparent[$current])) {
            $result[] = $current;
            foreach ($byparent[$current] as $key => $value) {
                $this->getChildren($byparent, $value, $result);
            }
        } else {
            $result[] = $current;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function sitemap()
    {
        $pages = DB::select(
            "SELECT MAX(h.level) as maxLevel, h.page_id as id, p.parent_id, p.slug, c.body as title, p.template
								FROM hierarchy h
								LEFT JOIN page p ON p.id = h.page_id
								LEFT JOIN content c ON c.page_u_id = p.u_id AND c.lang=? AND c.section='title'
								WHERE p.active = '1'
								AND p.deleted = '0'
								AND (p.publish_date = '".DATE_TIME_ZERO."' OR p.publish_date < '".date('Y-m-d H:i:s')."')
								AND (p.unpublish_date = '".DATE_TIME_ZERO."' OR p.unpublish_date > '".date('Y-m-d H:i:s')."')
								AND p.in_navigation = '1'
								AND p.langs >> (?-1) & 1 != 0
								AND p.id > 1
                                AND h.deleted_at is NULL
                                AND p.deleted_at is NULL
                                AND c.deleted_at is NULL
                                AND ((SELECT Count(*)
                                    FROM hierarchy hAlive
                                    LEFT JOIN page pAlive ON hAlive.parent_id = pAlive.id
                                    WHERE hAlive.page_id = h.page_id  AND pAlive.active = 1 AND pAlive.id <> h.page_id
                                    AND ( pAlive.publish_date = '".DATE_TIME_ZERO."' OR pAlive.publish_date < '".date('Y-m-d H:i:s')."')
                                    AND ( pAlive.unpublish_date = '".DATE_TIME_ZERO."'	OR pAlive.unpublish_date > '".date('Y-m-d H:i:s')."'))
                                        =
                                    (Select hierarchy.level FROM hierarchy where hierarchy.page_id = h.page_id  AND hierarchy.parent_id = 1))
								GROUP BY p.parent_id, p.position, p.id",
            [Session::get('lang'),Session::get('lang')]
        );
        //$homeDesc = Page::descendants(1, Session::get('lang'), 0)->alive()->get();

        $pagesById = [];
        $byparent = [];
        foreach ($pages as $page) {
        //foreach($homeDesc as $page){
            if (($page->template != 'downloads' && $page->template != 'best_practice') || $page->maxLevel == 2) {
                $pagesById[$page->id] = $page;
                $byparent[$page->parent_id][] = $page;
            }
            $page->title = HtmlHelper::fixDoubleDaggerForDaimler($page->title);
        }
        unset($pages);
        $flat = [];
        //$this->getChildren($byparent,1, $flat);

        if (Session::get('lang') == 1) {
            $altLang = 2;
        } else {
            $altLang = 1;
        }

        return Response::view('sitemap', [
                'PAGE'          => (object)['id'=>'sitemap', 'slug'=>'sitemap', 'template'=>'system', 'langs' => 3],
                "PAGES"         => $pagesById,
                'PARENT_INDEX'  => $flat,
                'ASCENDANTS'    => [],
                'BYPARENT' => $byparent,
                'ALT_LANG' => $altLang,]);
    }
}

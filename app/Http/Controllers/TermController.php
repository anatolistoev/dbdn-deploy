<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Models\Term;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Description of TermController
 *
 * @author i.traykov
 */
class TermController extends RestController
{
    /**
     * Return Terms By Lang
     * URL: term/allterms/{$lang]
     * METHOD: GET
     */

    public function getAllterms($lang = 0)
    {

        $sort_fields = ['id', 'name', 'description', 'related_links', 'updated_at', 'created_at'];
        $req = Request::all();

        //parameter lang must be set
        if ($lang <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter lang is required!'), 400);
        }

        if (empty($req['filter'])) {
            $filter=null;
        } else {
            $filter=$req['filter'];
        }

        if (!empty($req['sort-by']) && in_array($req['sort-by'], $sort_fields)) {
            $field=$req['sort-by'];
        } else {
            $field=null;
        }
        if (!empty($req['sort-direction']) && strtoupper($req['sort-direction']) == "DESC") {
            $order="DESC";
        } else {
            $order="ASC";
        }

        if (is_null($filter) && is_null($field)) {
            $terms = Term::where('lang', '=', $lang)->get();
        } elseif (!is_null($filter) && is_null($field)) {
            $terms = Term::where('lang', '=', $lang)->where('name', 'like', "%".$filter."%")->orderBy('name', $order)->get();
        } elseif (!is_null($filter) && !is_null($field)) {
            $terms = Term::where('lang', '=', $lang)->where('name', 'like', "%".$filter."%")->orderBy($field, $order)->get();
        }

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of Terms found', $terms), 200);

        return $jsonResponse;
    }

    /**
     * Return Term by id
     * URL: term/termbyid/{id}
     * METHOD: GET
     */
    public function getTermbyid($id = null)
    {
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $term = Term::find($id);

        if (is_null($term)) {
            return Response::json(RestController::generateJsonResponse(true, 'Term not found'), 404);
        } else {
            return Response::json(RestController::generateJsonResponse(false, 'Term found.', $term));
        }
    }


    /**
     * Create Term
     * URL: term/createterm
     * METHOD: GET
     */

    public function postCreate()
    {

        $req = Request::all();
        
        // file_put_contents('/home/www/logs/purify.log', 'Create term before purify: '.var_export($req,true)."\n",FILE_APPEND);
        
        if (isset($req['name'])) {
            $req['name']=\Purifier::clean($req['name']);
        }
        
        if (isset($req['description'])) {
            $req['description']=\Purifier::clean($req['description']);
        }
        
        // file_put_contents('/home/www/logs/purify.log', 'Create term after purify: '.var_export($req,true)."\n",FILE_APPEND);
        

        $same_term = Term::where('lang', '=', $req['lang'])->where('name', '=', $req['name'])->get();
        if (!$same_term->isEmpty()) {
            return Response::json(RestController::generateJsonResponse(true, 'Term with same name already exists!'), 400);
        }

        $term = new Term();
        $term->fill($req);

        return $this->trySave($term, false);
    }

    /**
     * Return save result
     */
    protected function trySave($term, $isUpdate)
    {
        try {
            if ($isUpdate) {
                $message = "Term was updated.";
            } else {
                $message = "Term was added.";
            }

            $term->save();
            //Success !!!

            $result = Response::json(RestController::generateJsonResponse(false, $message, $term));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
			// Get messages
			$messages = $e->getErrors();

			$result = Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.validation.failed'), $messages), 400);
        }
        return $result;
    }


    /**
     * Return Update Term from JSON
     * URL: term/update/{$id}
     * METHOD: PUT
     */
    public function putUpdate()
    {
        $update_term = Request::json()->all();
        
        // file_put_contents('/home/www/logs/purify.log', 'Update term before purify: '.var_export($update_term,true)."\n",FILE_APPEND);
        
        if (isset($update_term['name'])) {
            $update_term['name']=\Purifier::clean($update_term['name']);
        }
        
        if (isset($update_term['description'])) {
            $update_term['description']=\Purifier::clean($update_term['description']);
        }
        
        // file_put_contents('/home/www/logs/purify.log', 'Update term after purify: '.var_export($update_term,true)."\n",FILE_APPEND);

        //Term::validateInput($update_term);
        //TODO validate
        if (empty($update_term['id'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $same_term = Term::where('lang', '=', $update_term['lang'])->where('name', '=', $update_term['name'])->where('id', '<>', $update_term['id'])->get();
        if (!$same_term->isEmpty()) {
            return Response::json(RestController::generateJsonResponse(true, 'Term with same name already exists!'), 400);
        }

        $term = Term::find($update_term['id']);
        if (is_null($term)) {
            return Response::json(RestController::generateJsonResponse(true, 'Term not found.', $update_term), 404);
        }

        $term->fill($update_term);
        return $this->trySave($term, true);
    }


    /**
     * Delete Term
     * URL: term/deleteterm
     * METHOD: DELETE
     */

    public function deleteDelete($id = null)
    {

        $req = Request::all();

        //parameters id must be set
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $term = Term::find($id);

        if (is_null($term)) {
            return Response::json(RestController::generateJsonResponse(true, 'Term not found.'), 404);  // in response term not exists
        }

        $deletedterm = $term;
        $deletedterm->delete();

        return Response::json(RestController::generateJsonResponse(false, 'Term was deleted!', $term));
    }

    /**
     * Froce Delete Term for Test only
     * URL: term/deleteterm
     * METHOD: DELETE
     */

    public function deleteForcedelete($id = null)
    {
        $term = Term::withTrashed()->find($id);
        $term->forceDelete();
    }
}

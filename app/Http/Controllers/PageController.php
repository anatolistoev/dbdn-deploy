<?php

namespace App\Http\Controllers;

use App\Exception\CopyPageException;
use App\Exception\ModelValidationException;
use App\Helpers\DispatcherHelper;
use App\Helpers\FileHelper;
use App\Helpers\HtmlHelper;
use App\Helpers\UriHelper;
use App\Helpers\UserAccessHelper;
use App\Models\Access;
use App\Models\Content;
use App\Models\Gallery;
use App\Models\FileItem;
use App\Models\Note;
use App\Models\Page;
use App\Models\Related;
use App\Models\Subscription;
use App\Models\User;
use App\Models\Newsletter;
use App\Models\NewsletterContent;
use App\Models\Workflow;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

/**
 * Description of PageController
 *
 * @author m.stefanov
 */
class PageController extends RestController
{

    /**
     * Return Page by ID
     * URL: pages/read/{ID}/{loadAccess}
     * METHOD: GET
     */
    public function getRead($id = null, $loadAccess = false)
    {
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }

        if ($loadAccess) {
            $pageQuery = Page::with('workflow');
        } else {
            $pageQuery = Page::query();
        }
        $page = $pageQuery->with('tags')->find($id);
        //       print_r($page);die;
        if (is_null($page)) {
            return Response::json(RestController::generateJsonResponse(true, 'Page not found.'), 404);
        } else {
            if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id)) {
                return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
            }

            $page->access = $page->getAccess();
            
            return Response::json(RestController::generateJsonResponse(false, 'Page found.', $page));
        }
    }

    /**
     * Return List of Pages the user is Responsible for
     * URL: pages/responsible
     * METHOD: GET
     */
    public function getResponsible()
    {
        $req = Request::all();
        // Load pages of the current user
        $query = Page::join('content as c', 'c.page_u_id', '=', 'page.u_id')
            ->join('workflow as wf', 'wf.page_id', '=', 'page.u_id')
            ->where('c.lang', '=', $req['lang'])
            ->where('c.section', '=', 'title')
            ->where('wf.user_responsible', '=', Auth::user()->id)
            ->where('wf.editing_state', '<>', STATE_APPROVED)
            ->select('page.*', 'c.body as title', 'wf.due_date', 'wf.editing_state', 'wf.assigned_by');
        $pages = $query->get();
        foreach ($pages as $key => $p) {
            $ascendants = Page::ascendants($p->id, $req['lang'])->get();
            //print_r($ascendants->toArray());
            //req['related'] used to load grid with related pages in edit page
            //related_pages.js line 73 (sAjaxSource)
            if (!UserAccessHelper::hasAccessForPageAndAscendants($p->id, $ascendants) && (!isset($req['related']) || $req['related'] != 1)) {
                $pages->forget($key);
                continue;
            }

            $p->path = '/';
            foreach ($ascendants as $pa) {
                if ($pa->id != HOME) {
                    $p->path .= $pa->title . '/';
                }
            }

            $user = User::find($p->assigned_by);
            if ($user) {
                $p->assigned_by = $user->username . ' (' . $user->resolveRole() . ')';
            } else {
                $p->assigned_by = '';
            }

            if (strtotime($p->due_date) <= 0) {
                $p->due_date = trans('backend_pages.no_due_date');
            } else {
                $p->due_date = date('d.m.Y H:i', strtotime($p->due_date));
            }
        }
        
        $array_result = array_values($pages->toArray());
        
        // Load newsletters of the current user
        $newsletters = Newsletter::where('newsletter.user_responsible', '=', Auth::user()->id)->
                                   where('newsletter.editing_state', '<>', STATE_APPROVED)->
                                   select('newsletter.*')->get();
        
        foreach($newsletters as $newsletter_data) {
            $user = User::find($newsletter_data->assigned_by);
            if ($user) {
                $assigned_by = $user->username . ' (' . $user->resolveRole() . ')';
            } else {
                $assigned_by = '';
            }
            
            $newsletter_content = NewsletterContent::where('newsletter_content.newsletter_id', '=', $newsletter_data->id)->
                                                     where('newsletter_content.position', '=', 'title')->
                                                     select('newsletter_content.content as newsletter_title')->get(); 
            
            if (strtotime($newsletter_data->due_date) <= 0) {
                $newsletter_data->due_date = trans('backend_pages.no_due_date');
            } else {
                $newsletter_data->due_date = date('d.m.Y H:i', strtotime($newsletter_data->due_date));
            }
            
            array_push($array_result,array('version'=>'0',
                                           'title'=>$newsletter_content->first()->newsletter_title,
                                           'id'=>$newsletter_data->id,
                                           'assigned_by'=>$assigned_by,
                                           'editing_state'=>$newsletter_data->editing_state,
                                           'due_date' => $newsletter_data->due_date));
        }
        
        
        //print_r($pages);die;
       
        $return = Response::json(RestController::generateJsonResponse(false, 'List of pages found.', $array_result), 200);

        return $return;
    }

    /**
     * Return List of Pages
     * URL: pages/all/
     * METHOD: GET
     */
    public function getAll()
    {
        $req = Request::all();

        if (!isset($req['parent_id'])) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.parameter')), 400);
        }

        $query = Page::descendants($req['parent_id'], $req['lang'], $req['depth'], true);
        $pages = $query->get();
        $view_pos = 1;
        foreach ($pages as $key => $p) {
            $ascendants = Page::ascendants($p->id, $req['lang'])->get();
            //req['related'] used to load grid with related pages in edit page
            //related_pages.js line 73 (sAjaxSource)
            if (!UserAccessHelper::hasAccessForPageAndAscendants($p->id, $ascendants) && (!isset($req['related']) || $req['related'] != 1)) {
                $pages->forget($key);
                continue;
            }

            $descendants = Page::descendants($p->id, $req['lang'], 1)->get();
            $p->isChildless = count($descendants) == 0;
            $p->editing_state = $p->workflow->editing_state;
            $p->path = '/';
            foreach ($ascendants as $pa) {
                if ($pa->id != HOME) {
                    $p->path .= $pa->title . '/';
                    if ($pa->id == config('app.smart_downloads_id')) {
                        $p->template = 'downloads';
                    }
                }
            }
            if (Access::readbypage($p->id)->count() > 0) {
                $p->authorization = 1;
            }
            $p->view_pos = $view_pos;
            if ($p->level == 0) {
                $view_pos = 1;
            } else {
                $view_pos++;
            }
            if ($p->active) {
                $date = date('Y-m-d H:i');
                if ($p->publish_date != DATE_TIME_ZERO && $p->publish_date > $date) {
                    $p->isNotActive = 1;
                } else {
                    if ($p->unpublish_date != DATE_TIME_ZERO && $p->unpublish_date < $date) {
                        $p->isNotActive = 1;
                    }
                }
            }
        }
        //print_r($pages);die;
        $return = Response::json(RestController::generateJsonResponse(false, 'List of pages found.', array_values($pages->toArray())), 200);

        return $return;
    }

    /**
     * Return Search result of pages
     * URL: pages/search/
     * METHOD: GET
     */
    public function getSearch()
    {
        $req = Request::all();

        if (empty($req['filter'])) {
            $filter = null;
        } else {
            $filter = $req['filter'];
        }

        $query = Page::descendants(1, $req['lang']);
        $query->where('c.body', 'LIKE', '%' . $filter . '%')->with('workflow');
        $pages = $query->get();

        foreach ($pages as $key => $p) {
            $ascendants = Page::ascendants($p->id, $req['lang'])->get();
            if (!UserAccessHelper::hasAccessForPageAndAscendants($p->id, $ascendants)) {
                $pages->forget($key);
                continue;
            }
            $p->path = '/';
            foreach ($ascendants as $pa) {
                if ($pa->id != HOME) {
                    $p->path .= $pa->title . '/';
                }
            }
            if (Access::readbypage($p->id)->count() > 0) {
                $p->authorization = 1;
            }
            $p->editing_state = $p->workflow->editing_state;
        }

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of pages found.', array_values($pages->toArray())), 200);

        return $jsonResponse;
    }

    /**
     * Return Save page from JSON
     * URL: pages/create
     * METHOD: POST
     */
    public function postCreate()
    {
        $newpage = Request::json()->all();
        if ($newpage['slug'] == "") {
            return Response::json(RestController::generateJsonResponse(
                true,
                'Slug for the page is requared!'
            ), 404);
        } else {
            $results = DB::select('select distinct p.id, c.body from page p, content c where p.deleted_at is null and p.slug = :slug and p.id=c.page_id and c.section=\'title\'', ['slug' => $newpage['slug']]);
            // file_put_contents('/home/www/logs/slug.log', 'Slug results: '.var_export($results,true)."\n",FILE_APPEND);
            if (!empty($results)) {
                return Response::json(RestController::generateJsonResponse(
                true,
                "The slug entered is already used as follows:\n".$results[0]->body.' (ID:'.$results[0]->id.")\n\nPlease change the slug and then try to save the page properties again."
            ), 404);
            }
        }
        
        
        if ($newpage['langs'] == 0) {
            return Response::json(RestController::generateJsonResponse(
                true,
                'You have to choose at least one language!'
            ), 404);
        }
        $parentPage = Page::find($newpage['parent_id']);
        if (is_null($parentPage) || !UserAccessHelper::hasAccessForPageAndAscendants($parentPage->id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
        }

        if ($newpage['home_block'] == 1 && $newpage['home_accordion'] == 1) {
            return Response::json(RestController::generateJsonResponse(
                true,
                'The options Picked as best practice and Promoted on carousel can\'t be checked at same time.'
            ), 404);
        }

        if (isset($newpage['access'])) {
            $arrAccess = $newpage['access'];
            unset($newpage['access']);
        } else {
            $arrAccess = null;
        }

        if (isset($newpage['keywords'])) {
            $arrTags = $newpage['keywords'];
            unset($newpage['keywords']);
        } else {
            $arrTags = null;
        }
        if ($newpage['template'] != 'downloads') {
            $newpage['file_type'] = null;
            $newpage['usage'] = null;
        }
        $page = new Page();
        $page->fill($newpage);
        $page->is_visible = 1;
        $page->version = 0;
        $page->created_by = Auth::user()->id;
        //while workflow is not in Approved state page must be unactive
        $page->active = 0;

        return $this->trySave($page, false, -1, -1, $arrAccess, $arrTags);
    }

    /**
     * Return Update Page from JSON
     * URL: pages/update/
     * METHOD: PUT
     */
    public function putUpdate($params = null)
    {
        $startTime = microtime(true);

        $updatepage = Request::json()->all();
        if ($params) {
            $updatepage = $params;
        }
        
        if (isset($updatepage['slug'])) {
        // Validate slug only if update for this field is requested
            if ($updatepage['slug'] == "") {
                return Response::json(RestController::generateJsonResponse(
                    true,
                    'Slug for the page is requared!'
                ), 404);
            } else {
                $results = DB::select('select distinct p.id, c.body from page p, content c where p.deleted_at is null and p.slug = :slug and p.id <> :id and p.id=c.page_id and c.section=\'title\'', ['slug' => $updatepage['slug'], 'id' => $updatepage['id']]);
                // file_put_contents('/home/www/logs/slug.log', 'Slug results: '.var_export($results,true)."\n",FILE_APPEND);
                if (!empty($results)) {
                    return Response::json(RestController::generateJsonResponse(
                    true,
                    "The slug entered is already used as follows:\n".$results[0]->body.' (ID:'.$results[0]->id.")\n\nPlease change the slug and then try to save the page properties again."
                ), 404);
                }
            }
        
        }
        
        //Validate
        if (empty($updatepage['u_id'])) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        $page = Page::find($updatepage['u_id']);
        if (is_null($page)) {
            return Response::json(RestController::generateJsonResponse(true, 'Page not found.'), 404);
        }
        if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
        }
        if (!UserAccessHelper::hasWritePage($page)) {
            $responsible_name = "";
            if ($page->workflow->user_responsible > 0) {
                $user = User::find($page->workflow->user_responsible);
                if (!is_null($user)) {
                    $responsible_name = trim($user->first_name) . ' ' . trim($user->last_name);
                }
            }            
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.page.state_error', ['name' => $responsible_name])), 403);
        }
        if ($page->workflow->editing_state == STATE_APPROVED) {
            if (!(isset($updatepage['action']) && $updatepage['action'] == 'move' && count($updatepage) == 4 && isset($updatepage['parent_id']) && isset($updatepage['position']))  // Move is allowed
                && !(count($updatepage) == 2 && ( isset($updatepage['active']) || isset($updatepage['position']) )) // Change position and Activate/Deactivate are allowed
                ) {
                return Response::json(RestController::generateJsonResponse(
                    true,
                    'This page is already approved you cannot edit it.'
                ), 403);
            }
        }
        if (isset($updatepage['publish_date']) || isset($updatepage['unpublish_date'])) {
            if (strtotime($updatepage['publish_date']) > 0 && strtotime($updatepage['unpublish_date']) > 0) {
                if (strtotime($updatepage['publish_date']) >= strtotime($updatepage['unpublish_date'])) {
                    return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.validation.dates.failed')), 400);
                }
            }
            if (strtotime($updatepage['publish_date']) > 0 && strtotime($updatepage['publish_date']) < strtotime(date('Y-m-d H:i:s', strtotime('+1 hour')))) {
                return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.validation.dates.hourfailed')), 400);
            }
        }

        if (isset($updatepage['home_block']) && $updatepage['home_block'] == 1 && isset($updatepage['home_accordion']) && $updatepage['home_accordion'] == 1) {
            return Response::json(RestController::generateJsonResponse(
                true,
                'The options Picked as best practice and Promoted on carousel can\'t be checked at same time.'
            ), 404);
        }
        try {
            DB::beginTransaction();

            if (self::isPageInEditMode($page)) {
                return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.page.edited')), 400);
            }
            
            if (!($page->version == 0 && $page->is_visible == 1 && $page->workflow->editing_state != STATE_APPROVED)) {
                unset($updatepage['access']);
                unset($updatepage['slug']);
            } else {
                if (isset($updatepage['slug']) && $updatepage['slug'] != $page->slug) {
                    $vesrions = Page::where('id', '=', $page->id)->where('page.u_id', '<>', $page->u_id)->get();
                    foreach ($vesrions as $version) {
                        $version->slug = $updatepage['slug'];
                        $this->trySave($version, true, $version->position, $version->parent_id, null, null);
                    }
                }
            }
            
            if (isset($updatepage['access']) && count($updatepage['access']) == 0) {
                if ($page->hasProtectedFiles()) {
                    return Response::json(RestController::generateJsonResponse(true, trans('ws_file_controller.protected.files.contain')), 404);
                }
            }

            if ($page->workflow->editing_state != STATE_APPROVED && isset($updatepage['active']) && $updatepage['active'] == 1) {
                return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.page.workflow_active')), 400);
            }
            if (isset($updatepage['active']) && $updatepage['active'] == 1) {
                $page->is_visible = 1;
                $activePages = Page::join('workflow', 'workflow.page_id', '=', 'page.u_id')->where('page.id', '=', $page->id)->where('workflow.editing_state', '=', STATE_APPROVED)->where('page.u_id', '<>', $page->u_id)->get();
                $downloadArray = DB::table('download')->where("page_u_id", "=", $page->u_id)->whereNull('deleted_at')->select('file_id')->pluck('file_id')->toArray();
                $deletedFilesArray = DB::table('download')
                    ->where("page_u_id", "=", $page->u_id)
                    ->join('file', 'download.file_id', '=', 'file.id')
                    ->whereNull('download.deleted_at')
                    ->where(function ($query) {
                        $query->whereNotNull('file.deleted_at')
                            ->orWhere('file.deleted', 1);
                    })->select('file.id')->pluck('file.id');
                if (count($deletedFilesArray) > 0) {
                    return Response::json(RestController::generateJsonResponse(true, "Page cannot be activated, because it has reference(s) to deleted file(s)"), 400);
                }
                foreach ($activePages as $active) {
                    if ($active->version > $page->version) {
                        return Response::json(RestController::generateJsonResponse(true, "Page cannot be activated, because there is higher approved version"), 400);
                    }
                    if ($active->active = 1) {
                        $active->active = 0;
                        $active->is_visible = 0;
                        $this->trySave($active, true, $active->position, $active->parent_id, null, null);
                        $downloadActiveArray = DB::table('download')->where("page_u_id", "=", $active->u_id)->whereNull('deleted_at')->pluck('file_id')->toArray();
                        if (count($downloadActiveArray) > 0) {
                            $diffArray = array_diff($downloadActiveArray, $downloadArray);
                            if (count($diffArray) > 0) {
                                DB::table('cart')->whereIn("file_id", $diffArray)->update(['deleted_at' => date('Y-m-d H:i:s'), 'active' => 0]);
                            }
                        }
                    }
                }
                $visiblePages = Page::where('is_visible', 1)->where('page.id', '=', $page->id)->where('page.u_id', '<>', $page->u_id)->get();
                foreach ($visiblePages as $visible) {
                    $visible->is_visible = 0;
                    $this->trySave($visible, true, $visible->position, $visible->parent_id, null, null);
                }
            }

            $oldPos = $page->position;
            $oldParent = $page->parent_id;
            $oldActive = $page->active;

            if (isset($updatepage['access'])) {
                $arrAccess = $updatepage['access'];
                unset($updatepage['access']);
            } else {
                $arrAccess = null;
            }
            if (isset($updatepage['keywords'])) {
                $arrTags = $updatepage['keywords'];
                unset($updatepage['keywords']);
            } else {
                $arrTags = null;
            }
            unset($updatepage['action']);
            if (isset($updatepage['template']) && $updatepage['template'] != 'downloads') {
                $updatepage['file_type'] = null;
                $updatepage['usage'] = null;
            }

            $page->fill($updatepage);
            $result = $this->trySave($page, true, $oldPos, $oldParent, $arrAccess, $arrTags);

            if ($page->active == 0 && $oldActive == 1) {
                // Deactivate page and all descendants in Cart, Subscriptions and Request Authentifications
                $desc_pages = Page::descendants($page->id, 0, 0)->get();
                DB::unprepared('SET @disable_trigger = 1;');
                $desc_pages[] = $page;
                foreach ($desc_pages as $dp) {
                    DB::table('cart')
                        ->whereRaw("file_id IN (
                            SELECT file_id FROM download
                            WHERE page_u_id = " . $dp->u_id . ")")
                        ->update(['deleted_at' => date('Y-m-d H:i:s'), 'active' => 0]);
                    DB::table('subscription')
                        ->where("page_id", "=", $dp->id)
                        ->update(['deleted_at' => date('Y-m-d H:i:s'), 'active' => 0]);
                    DB::table('request_auth')
                        ->where('page_id', $dp->id)
                        ->update(['deleted_at' => date('Y-m-d H:i:s'), 'active' => 0]);
                }
                DB::table('cart')
                    ->whereRaw("file_id IN (
                        SELECT file_id FROM download
                        WHERE page_u_id = " . $page->u_id . ")")
                    ->update(['deleted_at' => date('Y-m-d H:i:s'), 'active' => 0]);
                DB::table('subscription')->where("page_id", "=", $page->id)
                    ->update(['deleted_at' => date('Y-m-d H:i:s'), 'active' => 0]);
                DB::table('request_auth')
                    ->where('page_id', $page->id)
                    ->update(['deleted_at' => date('Y-m-d H:i:s'), 'active' => 0]);
                DB::unprepared('SET @disable_trigger = NULL;');
            }
            
            DB::commit();
            $endTime = microtime(true);
            Log::debug("Page id: " . $page->u_id . " update time: " . number_format($endTime - $startTime, 4));

            //TODO: Generate PDFs in background
            // file_put_contents('/srv/www/dbdn-l55/storage/pdf/activate_page.log', "Now we generate PDF for: ".var_export($page->template,true)."\n",FILE_APPEND);
            // DO not generate PDF file for downloads template pages
            if (!in_array($page->template, ['downloads'])) {
                $pdfResult = $this->generatePagePDFs($page);
                if ($pdfResult !== false) {
                    $result = $pdfResult;
                }
            }

        } catch (\Exception $e) {
            DB::rollback();

            throw $e;
        }

        return $result;
    }

    private function generatePagePDFs(Page $page)
    {
        $result = false;
        $brands = array_keys(config('app.pages'));
        array_unshift($brands, HOME);

        if ($page->active == 1 && $page->overview == 0 && !in_array($page->id, $brands)) {
            $hasError = false;
            try {
                $timeForPDFGenerationDE = DispatcherHelper::genereateAndCachePdfByLang($page, LANG_DE, true);
                Log::debug("Page id: " . $page->u_id . " DE generation time: " . number_format($timeForPDFGenerationDE, 4));
            } catch (\Exception $e) {
                $hasError = true;
                $userMsg = "The page is successfully updated but there was a problem with generating the pdf for this version. Please upload it manually. For german version";
                Log::error($e->getMessage());
                Log::error($e->getTraceAsString());
            }

            try {
                $timeForPDFGenerationEN = DispatcherHelper::genereateAndCachePdfByLang($page, LANG_EN, true);
                Log::debug("Page id: " . $page->u_id . " EN generation time: " . number_format($timeForPDFGenerationEN, 4));
            } catch (\Exception $e) {
                if ($hasError) {
                    $userMsg .= " and english version.";
                } else {
                    $hasError = true;
                    $userMsg = "The page is successfully updated but there was a problem with generating the pdf for this version. Please upload it manually. For english version.";
                }
                Log::error($e->getMessage());
                Log::error($e->getTraceAsString());
            }

            if ($hasError) {
                $result = Response::json(RestController::generateJsonResponse(false, $userMsg, $page), 200);
            }
        }

        return $result;
    }

    protected function _copyPage(Page $srcPage) {
        $copyPage = $srcPage->replicate();
        $copyPage->active = 0;
        $copyPage->visits = 0;
        $copyPage->visits_search = 0;
        $copyPage->end_edit = null;
        $copyPage->user_edit_id = null;
        $copyPage->slug = $srcPage->slug . '_copy';
        $copyPage->created_by = Auth::user()->id;
        $copyPage->publish_date = DATE_TIME_ZERO;
        $copyPage->unpublish_date = DATE_TIME_ZERO;
        $copyPage->version = 0;
        $copyPage->is_visible = 1;

        return $copyPage;
    }

    /**
     * Return Copied Page like JSON
     * URL: pages/copy/
     * METHOD: POST
     */
    public function postCopy()
    {
        $updatepage = Request::json()->all();
        $pages = [];
        if (empty($updatepage['u_id'])) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        $page = Page::find($updatepage['u_id']);
        if (is_null($page)) {
            return Response::json(RestController::generateJsonResponse(true, 'Page not found.', $updatepage), 404);
        }
        if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
        }

        if ($updatepage['action'] == 'copyAll') {
            $descendens = Page::descendants($page->id)->get();
            foreach ($descendens as $child) {
                unset($child->level);
                $arrAccess = $child->getAccess();
                $copyPage = $this->_copyPage($child);
                $pages[] = (object)['oldPage' => $child, 'newPage' => $copyPage, 'arrAccess' => $arrAccess, 'arrTags' => $child->tags];
            }
        }
        unset($updatepage['action']);
        $page->fill($updatepage);

        $copyPage = $this->_copyPage($page);
        array_unshift($pages, (object)['oldPage' => $page, 'newPage' => $copyPage, 'arrAccess' => $page->getAccess(), 'arrTags' => $page->tags]);
        $parrentMapping = [];
        $parrentMapping[$page->parent_id] = $copyPage->parent_id;

        //FIXME: Empty Responce?
        $result;
        try {
            DB::beginTransaction();
            for ($i = 0; $i < count($pages); $i++) {
                $pages[$i]->newPage->parent_id = $parrentMapping[$pages[$i]->oldPage->parent_id];
                $pageResult = $this->trySave($pages[$i]->newPage, false, -1, -1, $pages[$i]->arrAccess, $pages[$i]->arrTags);

                if ($i == 0) {
                    $result = $pageResult;
                }

                $newPageID = json_decode($pageResult->getContent())->response->id;
                $parrentMapping[$pages[$i]->oldPage->id] = $newPageID;
                $contentArray = new Collection();
                foreach ($pages[$i]->oldPage->contents as $content) {
                    unset($content->id);
                    $newContent = new Content();
                    $newContent->fill($content->getAttributes());
                    $newContent->page_u_id = $newPageID;
                    $newContent->page_id = $newPageID;
                    $contentArray->add($newContent);
                }
                $contentResult = App::make(\App\Http\Controllers\ContentController::class)->trySave($contentArray, false);
            }
            DB::commit();
        } catch (ModelValidationException $mve) {
            DB::rollback();                                                                             

            throw $mve;
        } catch (\Exception $e) {
            DB::rollback();

            $message = $e->getMessage();
            throw new CopyPageException($message, 0, $e);
        }

        return $result;
    }

    /**
     * Return Copied Page like JSON
     * URL: pages/clearcache/
     * METHOD: POST
     */
    public function postClearcache()
    {
        try {
            Cache::flush();

            return Response::json(RestController::generateJsonResponse(false, 'Cache was cleared'), 200);
        } catch (\Exception $e) {
            $message = $e->getMessage();

            return Response::json(RestController::generateJsonResponse(true, $message), 404);
        }
    }

    private static function findCachedImageForSendAlert(string $mainEN, string $img_size, string $defThumb) {
        $filePathCached = null;
        $firstImg = HtmlHelper::getFirstImagePath($mainEN, $img_size);
        if ($firstImg !== null && strpos($firstImg, 'protected-file/') === false) {
            $filePathCached = public_path() . '/' . $firstImg;
            if (!is_file($filePathCached)) {
                $filePathCached = FileHelper::generateCacheImage('images', substr($firstImg, 13));
            }
        } 
        
        // Use thumb if image not found (protected image)
        if (is_null($filePathCached) || strpos($filePathCached, FileHelper::DEFAULT_IMAGE_NOT_FOUND_PATH) || !is_file($filePathCached)) {
            $filePathCached = public_path() . '/' . UriHelper::generateImgPath($defThumb, FileController::MODE_IMAGES, $img_size, FileHelper::getFileExtension($defThumb), false);
        }

        return $filePathCached;
    }

    /**
     * Send a notification email to users subscribed to the given page
     * URL: pages/sendalert/{id}
     * METHOD: PUT
     */
    public function putSendalert($id)
    {
        $count = 0;
        // Validation
        $page = Page::find($id);
        if (!$page || $page->count() == 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Page not found.'), 404);
        }
        $ascendants = Page::ascendants($page->id, $this->lang)->get();
        if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id, $ascendants)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
        }
        if ($page->active == 0 || $page->langs == 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Page is not active or has no languages.'), 400);
        }

        $updateAlert = Request::json()->all();
        $subscriptions = Subscription::where('page_id', $page->id)->where('active', 1)->with('user')->get();

        if (count($subscriptions) > 0) {
            $page->load(
                [
                    'contents' => function ($query) {
                        $query->where('section', 'title')->orWhere('section', 'main');
                    }
                ]
            );

            $max_len = UPDATE_ALERT_MAX_LEN;
            $filePathCached = null;
            $sectionId = $page->parent_id == HOME ? $page->id : DispatcherHelper::getActiveL2($ascendants);            
            $enIMG = $deIMG = $defThumb = DispatcherHelper::getBrandThumbnail($sectionId);
            $mainEN = '';
            $mainDE = '';
            $hasEN = false;
            $hasDE = false;
            $img_size = 'carousel';
            if ($page->template != 'downloads') {
                $mainENResult = $page->contents()->where('section', '=', 'main')->where('lang', '=', LANG_EN)->first();
                $mainDEResult = $page->contents()->where('section', '=', 'main')->where('lang', '=', LANG_DE)->first();

                if ($mainENResult !== null) {
                    $mainEN = $mainENResult->body;
                    $hasEN = true;
                }

                if ($mainDEResult !== null) {
                    $mainDE = $mainDEResult->body;
                    $hasDE = true;
                }

                $enIMG = self::findCachedImageForSendAlert($mainEN, $img_size, $defThumb);
                $deIMG = self::findCachedImageForSendAlert($mainDE, $img_size, $defThumb);
            } else {
                $fileItem = FileItem::with('info')
                    ->join('download', 'file.id', '=', 'download.file_id')
                    ->where('download.page_u_id', $page->u_id)
                    ->orderBy('download.position', 'asc')
                    ->whereNull('download.deleted_at')
                    ->select('file.*')
                    ->first();
                if ($fileItem) {
                    $file_info_EN = $fileItem->info->where('lang', '=', LANG_EN)->first();
                    if ($file_info_EN !== null) {
                        $mainEN = $file_info_EN->description;
                        $hasEN = true;
                    }
                    $file_info_DE = $fileItem->info->where('lang', '=', LANG_DE)->first();
                    if ($file_info_DE !== null) {
                        $mainDE = $file_info_DE->description;
                        $hasDE = true;
                    }

                    if ((!$fileItem->protected)) {
                        if ($fileItem->hasThumbSourceFile()) {
                            $imgSource = $fileItem->getCachedThumbPath('{type}');
                            $imageType = FileHelper::getFileExtension($fileItem->getThumbPath(FILEITEM_REAL_PATH));
                            
                            $deIMG = public_path() . '/' . UriHelper::generateImgPath($imgSource, FileController::MODE_DOWNLOADS, $img_size, $imageType, false);
                        }
                    }
                }
                if ($deIMG == $defThumb) {
                    $filePathCached = public_path() . '/' . UriHelper::generateImgPath($defThumb, FileController::MODE_DOWNLOADS, $img_size, FileHelper::getFileExtension($defThumb), false);
                    $deIMG = $filePathCached;
                }
                $enIMG = $deIMG;
            }

            $pathEN = asset((strlen($page->slug) ? $page->slug : $page->u_id) . '?lang=en');

            $pathDE = asset((strlen($page->slug) ? $page->slug : $page->u_id) . '?lang=de');

            if (isset($updateAlert['comments_en']) && $updateAlert['comments_en'] != '') {
                $descriptionEN = $updateAlert['comments_en'];
                $hasEN = true;
            } else {
                $descriptionEN = HtmlHelper::getPageMainSummary($mainEN);
                $descriptionEN = HtmlHelper::getSummaryText($descriptionEN, $max_len, false);
            }
            if (isset($updateAlert['comments_de']) && $updateAlert['comments_de'] != '') {
                $descriptionDE = $updateAlert['comments_de'];
                $hasDE = true;
            } else {
                $descriptionDE = HtmlHelper::getPageMainSummary($mainDE);
                $descriptionDE = HtmlHelper::getSummaryText($descriptionDE, $max_len, false);
            }
            $user = null;
            $title_de = $page->contents()->where('section', '=', 'title')->where('lang', '=', LANG_DE)->first();
            $title_en = $page->contents()->where('section', '=', 'title')->where('lang', '=', LANG_EN)->first();
            $params = [
                'FIRST_NAME' => '$user->first_name',
                'LAST_NAME' => '$user->last_name',
                'TITLE_DE' => $title_de !== null ? $title_de->body : '',
                'TITLE_EN' => $title_en !== null ? $title_en->body : '',
                'PATH_DE' => $pathDE,
                'PATH_EN' => $pathEN,
                'COMMENTS_DE' => $descriptionDE,
                'COMMENTS_EN' => $descriptionEN,
                'IMG_DE' => asset(str_replace(public_path(), '', $deIMG)),
                'IMG_EN' => asset(str_replace(public_path(), '', $enIMG)),
                'header' => asset('/img/template/CD_update-info_header_d_1320X590.jpg'),
                'header_mobile' => asset('/img/template/CD_update-info_header_m_340X250.jpg'),
                'btn_01' => asset('/img/template/btn-01.png'),
                'btn_02' => asset('/img/template/btn-02.png'),
                'btn_03' => asset('/img/template/btn-03.png'),
                'btn_04' => asset('/img/template/btn-04.png'),
                'arrow_01' => asset('/img/template/arrow-01.png'),
                'arrow_small' => asset('/img/template/arrow_small.png'),
                'HAS_EN' => $hasEN,
                'HAS_DE' => $hasDE,
            ];
            foreach ($subscriptions as $subscription) {
                $user = $subscription->user;
                $params['FIRST_NAME'] = $user->first_name;
                $params['LAST_NAME'] = $user->last_name;

                Mail::send('emails.update_alert', $params, function ($message) use ($user) {
                    $message->to($user->email)
                        ->subject('Daimler Brand & Design Navigator - Update Info')
                        ->from(config('mail.from.address'));
                });
                //return view('emails.update_alert',$params);
                $count++;
            }
        }

        return Response::json(RestController::generateJsonResponse(false, 'Update alert was sent.', ['count' => $count]), 200);
    }

    /**
     * Return save result
     */
    protected function trySave(Page $my_object, $isUpdate, $oldPos, $oldParent, $arrNewAccess = null, $arrNewTags = null)
    {
        try {
            // Start transaction!
            DB::beginTransaction();

            if ($isUpdate) {
                $message = "Page was updated.";
            } else {
                if ($my_object->position == 0) {
                    $my_object->position = Page::where('parent_id', '=', $my_object->parent_id)->count() + 1;
                }
                $message = "Page was added.";
                if ($isUpdate === false) {
                    // Copy u_id to id for new pages
                    $data = DB::select("SHOW TABLE STATUS LIKE 'page'");
                    $my_object->id = $data[0]->Auto_increment;
                }
            }

            if ($oldPos == $my_object->position && $oldParent == $my_object->parent_id) {
                //Skip hierarchy update
                DB::unprepared('SET @disable_trigger = 1;');
            }
            $my_object->save();

            if (isset($arrNewAccess)) {
                DB::unprepared('SET @disable_trigger = 1;');
                AccessController::updatePageAccess($my_object->id, $arrNewAccess);
                if (count($arrNewAccess) > 0) {
                    DB::table('page')
                        ->where('id', $my_object->id)
                        ->update(['authorization' => 1]);
                    $my_object->authorization = 1;
                } elseif ($isUpdate) {
                    DB::table('page')
                        ->where('id', $my_object->id)
                        ->update(['authorization' => 0]);
                    $my_object->authorization = 0;
                }
            }
            if (isset($arrNewTags)) {
                DB::unprepared('SET @disable_trigger = 1;');
                TagController::updatePageTags($my_object->u_id, $arrNewTags);
            }
            DB::unprepared('SET @disable_trigger = NULL;');

            if ($oldPos != $my_object->position || $oldParent != $my_object->parent_id) {
                $pages = Page::where('parent_id', '=', $my_object->parent_id)->where('id', '<>', $my_object->id)->groupBy('id')->orderBy('position', 'asc')->get();
                $pos = 1;
                //rearrange items after updating element
                DB::unprepared('SET @disable_trigger = 1;');
                foreach ($pages as $p) {
                    if ($pos == $my_object->position) {
                        $pos++; //skip our position
                    }
                    DB::table('page')
                        ->where('id', $p->id)
                        ->update(['position' => $pos]);
                    //$p->newQuery()->where($p->getKeyName(), '=', $p->getKey())->update(array('position' => $pos)); // Skeep History
                    //					$p->update(array('position' => $pos)); // With History
                    //					DB::unprepared('UPDATE page SET position = ' . $pos . ' WHERE id = ' . $p->id . ';');

                    $pos++;
                }
                // Update versions
                DB::table('page')
                    ->where('id', $my_object->id)
                    ->update(['position' => $my_object->position, Page::UPDATED_AT => \Carbon\Carbon::now()]);

                DB::unprepared('SET @disable_trigger = NULL;');
            }
            //	If Page is moved and have new parent
            if ($oldParent != $my_object->parent_id) {
                $pages = Page::where('parent_id', '=', $oldParent)->groupBy('id')->orderBy('position', 'asc')->get();
                //print_r($pages);die;
                $pos = 1;
                //rearrange items after updating element
                DB::unprepared('SET @disable_trigger = 1;');
                foreach ($pages as $p) {
                    DB::table('page')
                        ->where('id', $p->id)
                        ->update(['position' => $pos]);
                    //$p->newQuery()->where($p->getKeyName(), '=', $p->getKey())->update(array('position' => $pos)); // Skeep History
                    //					$p->update(array('position' => $pos)); // With History
                    //					DB::unprepared('UPDATE page SET position = ' . $pos . ' WHERE id = ' . $p->id . ';');
                    $pos++;
                }
                // Update Versions!
                DB::table('page')
                    ->where('id', $my_object->id)
                    ->update(['parent_id' => $my_object->parent_id, Page::UPDATED_AT => \Carbon\Carbon::now()]);
                DB::unprepared('SET @disable_trigger = NULL;');
            }
            //Success !!!
            // Commit the queries!
            DB::commit();

            if ($isUpdate && $my_object->active) {
                $date = date('Y-m-d H:i');
                if ($my_object->publish_date != DATE_TIME_ZERO && $my_object->publish_date > $date) {
                    $my_object->isNotActive = 1;
                } else {
                    if ($my_object->unpublish_date != DATE_TIME_ZERO && $my_object->unpublish_date < $date) {
                        $my_object->isNotActive = 1;
                    }
                }
            }
            $my_object->position = (int)$my_object->position;
            $my_object->restricted = count($my_object->getAccess()) > 0 ? 1 : 0;

            if (!$isUpdate) {
                $wf = new Workflow();
                $wf->page_id = $my_object->u_id;
                $wf->editing_state = STATE_DRAFT;
                $wf->save();
            }

            //tasks table after edit row require this information
            if ($my_object->workflow) {
                $user = User::find($my_object->workflow->assigned_by);
                if ($user) {
                    $my_object->assigned_by = $user->username . ' (' . $user->resolveRole() . ')';
                } else {
                    $my_object->assigned_by = '';
                }
                if (strtotime($my_object->due_date) == 0) {
                    $my_object->due_date = trans('backend_pages.no_due_date');
                } else {
                    $my_object->due_date = date('d.m.Y H:i', strtotime($my_object->due_date));
                }

                $my_object->editing_state = $my_object->workflow->editing_state;
            }
//			$queries = DB::getQueryLog();
//$last_query = end($queries);
//print_r($queries);die;
            $result = Response::json(RestController::generateJsonResponse(false, $message, $my_object));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            // Rollback and then return errors
            DB::rollback();

            throw new ModelValidationException($e->getErrors());
        } catch (\Exception $e) {
            // Rollback and then return errors
            DB::rollback();

            throw $e;
        }

        return $result;
    }

    /**
     * Return Update Page from JSON
     * URL: pages/delete/{ID}
     * METHOD: DELETE
     */
    public function deleteDelete($id = null)
    {
        // Validation
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        try {
            // Start transaction!
            DB::beginTransaction();
            $page = Page::find($id);

            if (is_null($page)) {
                return Response::json(RestController::generateJsonResponse(true, 'Page not found.', $page), 404); // in response $page not exists
            }
            if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id)) {
                return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
            }
            if (self::isPageInEditMode($page)) {
                return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.page.edited')), 400);
            }
            if ($page->active) {
                return Response::json(RestController::generateJsonResponse(true, "Page is active please deactivate it first."), 400);
            }

            $deletedpage = $page;

            // Delete all children
            if ($page->is_visible == 1) {
                $desc_pages = Page::descendants($page->id, 0, 0)->get();
                DB::unprepared('SET @disable_trigger = 1;');
                foreach ($desc_pages as $dp) {
                    $time = $dp->freshTimestamp();
                    DB::table('page')
                        ->where('id', $dp->id)
                        ->where('is_visible', 0)
                        ->update(['deleted_at' => $dp->fromDateTime($time), 'deleted' => 1, 'active' => 0]);
                    DB::table('page')
                        ->where('id', $dp->id)
                        ->where('is_visible', 1)
                        ->update(['deleted_at' => $dp->fromDateTime($time), 'active' => 0]);
                    DB::table('cart')->whereRaw("file_id IN (
                            SELECT file_id FROM download
                            WHERE page_u_id = " . $dp->u_id . "
                        )")->update(['deleted_at' => date('Y-m-d H:i:s'), 'active' => 0]);
                    DB::table('request_auth')
                        ->where('page_id', $dp->id)
                        ->update(['deleted_at' => $dp->fromDateTime($time), 'active' => 0]);
                    DB::table('subscription')->where("page_id", "=", $dp->id)
                        ->update(['deleted_at' => $dp->fromDateTime($time), 'active' => 0]);
                }
                DB::table('page')
                    ->where('id', $page->id)
                    ->where('u_id', '<>', $page->u_id)
                    ->update(['deleted_at' => $page->fromDateTime($page->freshTimestamp()), 'deleted' => 1, 'active' => 0]);
                DB::unprepared('SET @disable_trigger = NULL;');
                DB::table('cart')->whereRaw("file_id IN (
                            SELECT file_id FROM download
                            WHERE page_u_id = " . $page->u_id . "
                        )")->update(['deleted_at' => date('Y-m-d H:i:s'), 'active' => 0]);
                DB::table('request_auth')
                    ->where('page_id', $page->id)
                    ->update(['deleted_at' => $page->fromDateTime($page->freshTimestamp()), 'active' => 0]);
                DB::table('subscription')->where("page_id", "=", $page->id)
                    ->update(['deleted_at' => $page->fromDateTime($page->freshTimestamp()), 'active' => 0]);
                //Fix the ordering
                $page->delete();
                $pages = Page::where('parent_id', '=', $deletedpage->parent_id)->where('is_visible', '=', 1)->orderBy('position', 'asc')->get();
                $pos = 1;
                //rearrange items after deleting element
                DB::unprepared('SET @disable_trigger = 1;');
                foreach ($pages as $p) {
                    DB::table('page')
                        ->where('id', $p->id)
                        ->update(['position' => $pos]);
                    $pos++;
                }
            } else {
                DB::unprepared('SET @disable_trigger = 1;');
                DB::table('page')
                    ->where('u_id', $page->u_id)
                    ->update(['deleted_at' => $page->fromDateTime($page->freshTimestamp()), 'deleted' => 1, 'active' => 0]);
                //$page->delete();
            }
            DB::unprepared('SET @disable_trigger = NULL;');
            // Commit the queries!
            DB::commit();
            $result = Response::json(RestController::generateJsonResponse(false, 'Page was deleted.', $deletedpage));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            // Rollback and then return errors
            DB::rollback();

            throw new ModelValidationException($e->getErrors());
        } catch (\Exception $e) {
            // Rollback and then return errors
            DB::rollback();

            throw $e;
        }

        return $result;
    }

    /**
     * Return Update Page from JSON
     * URL: pages/permanentdelete/{ID}
     * METHOD: DELETE
     */
    public function deletePermanentdelete($id = null)
    {
        // Validation
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }

        $page = DB::table('page')->where('u_id', $id)->whereNotNull('deleted_at')->where('is_visible', '=', 1)->first();
        if (is_null($page)) {
            return Response::json(RestController::generateJsonResponse(true, 'Page not found.', $page), 404); // in response $page not exists
        }
        // Check access to the page
        if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
        }

        try {
            // Start transaction!
            DB::beginTransaction();

            // Delete all children
            $desc_pages = DB::table('page')
                ->join('hierarchy as h', 'h.page_id', '=', 'page.id')
                ->join('page as p', 'p.id', '=', 'h.parent_id')
                ->where('p.id', '=', $page->id)
                ->where('h.level', '>', '0')
                ->orderBy('level', 'asc')
                ->orderBy('position', 'asc')
                ->select('page.*', 'h.level')
                ->whereNotNull('p.deleted_at')
                ->where('page.deleted', 0)
                ->where('page.is_visible', 1)->get();
            DB::unprepared('SET @disable_trigger = 1;');
            foreach ($desc_pages as $dp) {
                DB::table('page')->where('id', $dp->id)->update(['deleted' => 1]);
            }
            DB::unprepared('SET @disable_trigger = NULL;');

            DB::table('page')->where('id', $page->id)->update(['deleted' => 1]);

            // Commit the queries!
            DB::commit();
            $desc_pages[] = $page;
            $result = Response::json(RestController::generateJsonResponse(false, 'Page was permanently deleted.', $desc_pages));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            // Rollback and then return errors
            DB::rollback();

            throw new ModelValidationException($e->getErrors());
        } catch (\Exception $e) {
            // Rollback and then return errors
            DB::rollback();

            throw $e;
        }

        return $result;
    }

    /**
     * Force Delete PAge for tests
     * URL: pages/forcedelete/{ID}
     * METHOD: DELETE
     */
    public function deleteForcedelete($id = null)
    {
        $page = Page::withTrashed()->find($id);
        $page->forceDelete();
    }

    /**
     * Return List of Descedents of Page
     * URL: pages/descedents/{$pageId}/?depth={$depth}&lang={$lang}
     * METHOD: GET
     */
    public function getDescendants($pageId)
    {

        if (!is_numeric($pageId) || $pageId < 0) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }

        $req = Request::all();

        $lang = isset($req['lang']) ? $req['lang'] : 0;
        $depth = isset($req['depth']) ? $req['depth'] : 0;

        $pages = Page::descendants($pageId, $lang, $depth)->get();
        //$queries = DB::getQueryLog();
        //$last_query = end($queries);
        //print_r($last_query);die;
        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of pages found.', $pages), 200);

        return $jsonResponse;
    }

    /**
     * Return List of Ascedents of Page
     * URL: pages/ascedents/{$pageId}/?depth={$depth}&lang={$lang}
     * METHOD: GET
     */
    public function getAscendants($pageId)
    {

        if (empty($pageId)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }

        $req = Request::all();

        $lang = isset($req['lang']) ? $req['lang'] : 0;
        $depth = isset($req['depth']) ? $req['depth'] : 0;

        $pages = Page::ascendants($pageId, $lang, $depth)->get();

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of pages found.', $pages), 200);

        return $jsonResponse;
    }

    /**
     * Return deleted pages
     * URL: pages/deleted/{lang}
     * METHOD: GET
     */
    public function getDeleted($lang = null)
    {
        if (is_null($lang)) {
            $lang = Session::get('lang');
        }
        $pages = Page::onlyTrashed()->where('deleted', 0)->where('is_visible', '=', 1)->orderBy('deleted_at', 'desc')->orderBy('parent_id', 'asc')->get();

        $locales = config('app.locales');
        foreach ($pages as $key => $p) {
            $ascendants = Page::withTrashed()->ascendants($p->id, $lang)->get();

            if (!UserAccessHelper::hasAccessForPageAndAscendants($p->id, $ascendants)) {
                $pages->forget($key);
                continue;
            }

            $content = Content::withTrashed()->where('section', '=', 'title')->where('lang', '=', $lang)->where('page_u_id', '=', $p->u_id)->first();
            if ($content) {
                $p->title = $content->body;
            } else {
                if ($lang == LANG_EN) {
                    $other_lang = LANG_DE;
                } else {
                    $other_lang = LANG_EN;
                }
                $content_lang = Content::withTrashed()->where('section', '=', 'title')->where('lang', '=', $other_lang)->where('page_u_id', '=', $p->u_id)->first();
                if ($content_lang) {
                    $p->title = $content_lang->body . " (" . strtoupper($locales[$other_lang - 1]) . ")";
                } else {
                    $p->title = $p->slug . " (SLUG)";
                }
            }

            $p->path = '/';
            if (Access::readbypage($p->u_id)->count() > 0) {
                $p->authorization = 1;
            }

            foreach ($ascendants as $pa) {
                if ($pa->id != HOME) {
                    $p->path .= $pa->title . '/';
                }
            }
        }

        $return = Response::json(RestController::generateJsonResponse(false, 'List of pages found.', array_values($pages->toArray())), 200);

        return $return;
    }

    /**
     * Return restored page
     * URL: pages/restore/
     * METHOD: PUT
     */
    public function putRestore()
    {
        $req = Request::json()->all();

        // Validation
        if (empty($req['id'])) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        
        try {
            // Start transaction!
            DB::beginTransaction();
            $page = Page::onlyTrashed()->find($req['id']);
            if ($page) {
                // Validate access to restor the page
                $ascendants = Page::ascendants($page->id, 0)->get();
                if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id, $ascendants)) {
                    // Rollback and then return errors
                    DB::rollback();

                    return Response::json(RestController::generateJsonResponse(true, 'You don\'t have access to restore this page!'), 401);
                }

                // Restore the page
                $page->deleted_at = null;
                $page->slug = $page->slug.'_restored';
                $restored_page_slug=$page->slug;
                $i=0;
                $results = DB::select('select p.id from page p where p.deleted_at is null and p.slug = :slug', ['slug' => $restored_page_slug]);
                while(!empty($results)) {
                    $i++;
                    $page->slug = $restored_page_slug.$i;
                    $results = DB::select('select p.id from page p where p.deleted_at is null and p.slug = :slug', ['slug' => $page->slug]);
                }
                
                $user_root_page_id = UserAccessHelper::getUserRootPageID();
                // Check for Admin and Aprover
                if ($user_root_page_id == "all") {
                    $user_root_page_id = HOME;
                };
                $page->parent_id = $user_root_page_id;
                $page->active = 0;
                $page->position = Page::where('parent_id', '=', $user_root_page_id)->count() + 1;
                $page->update();
                $descendants = Page::descendants($page->id)->withTrashed()->get();
                DB::unprepared('SET @disable_trigger = 1;');
                foreach ($descendants as $pd) {
                    $pd->restore();
                }
                if ($page->hasProtectedFiles() && count($page->getAccess()) == 0) {
                    AccessController::updatePageAccess($page->id, [['group_id' => ACCESS_MEMBER]]);
                }
                DB::unprepared('SET @disable_trigger = NULL;');
                $result = Response::json(RestController::generateJsonResponse(false, 'Restore page.', $page), 200);
            } else {
                $result = Response::json(RestController::generateJsonResponse(true, 'Restore page not found.', $page), 404);
            }
            // Commit the queries!
            DB::commit();
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            // Rollback and then return errors
            DB::rollback();

            throw new ModelValidationException($e->getErrors());
        } catch (\Exception $e) {
            // Rollback and then return errors
            DB::rollback();

            throw $e;
        }

        return $result;
    }

    /**
     * Return related pages by ID, lang
     * URL: pages/related/pageId/{lang}
     * METHOD: GET
     * @deprecated not in use in dbdn
     */
    public function getRelated($page_id = null, $lang = null)
    {
        // GET pages/related/pageId/lang
        if (empty($page_id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        if (empty($lang)) {
            $lang = Session::get('lang'); //lang from SESSION
        }

        $page = Page::find($page_id);

        if (is_null($page)) {
            return Response::json(RestController::generateJsonResponse(true, 'Related not found.'), 404);
        } else {
            if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id)) {
                return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
            }

            $_query = DB::table('related as r')
                ->join('content as c', 'c.page_u_id', '=', 'r.related_id')
                ->where('r.page_u_id', '=', $page_id)
                ->where('c.lang', '=', $lang)
                ->where('c.section', '=', 'title')
                ->select('r.related_id as id', 'c.body as title', 'r.position as position')
                ->orderBy('r.position', 'asc')
                ->orderBy('id', 'asc');
            $related = $_query->get();

            return Response::json(RestController::generateJsonResponse(false, 'List of related pages.', $related));
        }
    }

    /**
     * Return save related page from JSON
     * URL: pages/addrelated
     * METHOD: POST
     * @deprecated not in use in dbdn
     */
    public function postAddrelated()
    {

        $newrelated = Request::json()->all();
        if (empty($newrelated['page_id']) || empty($newrelated['related_id'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Input data missing!'), 400);
        }
        $related = new Related();
        $related->fill($newrelated);
        $related->position = Related::where('page_u_id', '=', $newrelated['page_id'])->max('position') + 1;

        return $this->trySaveRelated($related, false);
    }

    /**
     * Return save result
     * @deprecated not in use in dbdn
     */
    protected function trySaveRelated(Related $my_object, $isUpdate)
    {
        try {
            if ($isUpdate) {
                $message = "Page was updated.";
            } else {
                $message = "Related page was added.";
            }

            $my_object->save();

            //Success !!!
            $result = Response::json(RestController::generateJsonResponse(false, $message, $my_object));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            throw new ModelValidationException($e->getErrors());
        } catch (\Exception $e) {
            $message = $e->getMessage();
            if (0 === strpos($message, 'SQLSTATE[23000]')) {
                $result = Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.page.relation_exists')), 500);
            } else {
                $result = Response::json(RestController::generateJsonResponse(true, $message), 500);
            }
        }

        return $result;
    }

    /**
     * Return Delete Related from JSON
     * URL: pages/delete
     * METHOD: DELETE
     * @deprecated not in use in dbdn
     */
    public function deleteRemoverelated()
    {
        // DELETE pages/removerelated

        $delete_related = Request::json()->all();
        if (empty($delete_related['page_id']) || empty($delete_related['related_id'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Input data missing!'), 400);
        }

        //validate
        $validate = $this->validateRelated($delete_related, "Related");
        if (!empty($validate) && is_object($validate)) {
            return $validate;
        } elseif (!empty($validate) && is_array($validate)) {
            $delete_related = $validate;
        }

        $queryPageRelated = Related::where('page_id', '=', $delete_related['page_id'])->where('related_id', '=', $delete_related['related_id']);
        $related = $queryPageRelated->get()->first();
        if (empty($related)) {
            return Response::json(RestController::generateJsonResponse(true, 'Related not found!', $related), 404); // in response $page not exists
        }
        $deleted = $related;
        $queryPageRelated->delete();

        //arrange positions
        $updated_rows = DB::table('related')
            ->where('position', '>', $deleted->position)
            ->where('page_id', $delete_related['page_id'])
            ->decrement('position');

        return Response::json(RestController::generateJsonResponse(false, 'Related page was deleted.', $deleted));
    }

    /**
     * Validate Related
     * $fields - input data
     * $object - name of object (Page, Related ...)
     * @deprecated not in use in dbdn
     */
    protected function validateRelated($fields, $object)
    {
        try {
            $temp = new $object();
            $temp->fill($fields);
            $temp->validate();
            if (!empty($fields['position'])) {
                $max_position = Related::where('page_id', '=', $fields['page_id'])->max('position');
                if ($fields['position'] > $max_position) {
                    // set position to max posible
                    $fields['position'] = $max_position;

                    return $fields;
                }
            }

            return null;
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            throw new ModelValidationException($e->getErrors());
        }
    }

    /**
     * Return Update Related Page from JSON
     * URL: pages/editrelated
     * METHOD: PUT
     * @deprecated not in use in dbdn
     */
    public function putEditrelated()
    {
        // PUT pages/editrelated
        $update_related = Request::json()->all();

        if (empty($update_related['page_id']) || empty($update_related['related_id']) || empty($update_related['position'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Input data missing!'), 400);
        }

        //validate
        $validate = $this->validateRelated($update_related, "Related");
        if (!empty($validate) && is_object($validate)) {
            return $validate;
        } elseif (!empty($validate) && is_array($validate)) {
            $update_related = $validate;
        }

        $queryPageRelated = Related::where('page_u_id', '=', $update_related['page_id'])
                                    ->where('related_id', '=', $update_related['related_id']);

        $related = $queryPageRelated->get()->first();
        if (empty($related)) {
            return Response::json(RestController::generateJsonResponse(true, 'Related not found!', $related), 404); // in response $page not exists
        }
        if ($related->position == $update_related['position']) {
            return Response::json(RestController::generateJsonResponse(false, 'Not needed update.', $related), 200);
        }

        $update_query = DB::table('related')
            ->where('page_u_id', $update_related['page_id']);

        //arrange positions
        if ($update_related['position'] < $related->position) {
            $updated_rows = $update_query
                ->whereBetween('position', [$update_related['position'], $related->position - 1])
                ->increment('position');
        } else {
            $updated_rows = $update_query
                ->whereBetween('position', [$related->position + 1, $update_related['position']])
                ->decrement('position');
        }

        $updated_rows = DB::table('related')
            ->where('page_u_id', $update_related['page_id'])
            ->where('related_id', $update_related['related_id'])
            ->update(['position' => $update_related['position']]);

        if ($updated_rows == 1) {
            $related = $queryPageRelated->get()->first();

            return Response::json(RestController::generateJsonResponse(false, 'Related possition updated.', $related), 200); // in response $page not exists
        } else {
            return Response::json(RestController::generateJsonResponse(true, 'Error found when possition updated!'), 400);
        }
    }

    /**
     * Return is page available for editing
     * URL: pages/isavailable/{$pageId}
     * METHOD: GET
     */
    public function getIsavailable($pageId)
    {
        if (empty($pageId)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        $result = PageController::checkAvailability($pageId);
        if ($result->status) {
            return Response::json(RestController::generateJsonResponse(false, 'You can edit this page', $result), 200);
        } else {
            if (!isset($result->error)) {
                return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.page.edited')), 400);
            } else {
                if ($result->error == 'ws_general_controller.webservice.unauthorized') {
                    return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
                }
                if ($result->error == "admin_only") {
                    return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.page.state_error', ['name' => $result->name])), 401);
                } else {
                    if ($result->error == "") {
                        return Response::json(RestController::generateJsonResponse(true, 'Page not found.'), 404);
                    } else {
                        if (!$result->status && $result->error) {
                            return Response::json(RestController::generateJsonResponse(true, $result->error), 500);
                        } else {
                            return Response::json(RestController::generateJsonResponse(true, 'Page not found.'), 404);
                        }
                    }
                }
            }
        }
    }

    /**
     * Return is page available for editing
     * URL: pages/appendtime
     * METHOD: POST
     */
    public function postAppendtime()
    {
        $input = Request::json()->all();
        if (empty($input['pageId'])) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        $pageId = $input['pageId'];
        try {
            DB::beginTransaction();
            $page = Page::find($pageId);
            if (is_null($page)) {
                return Response::json(RestController::generateJsonResponse(true, 'Page not found.'), 404);
            } else {
                //Chekc for access to the page
                if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id)) {
                    return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
                }
    
                if (self::isPageInEditMode($page)) {
                    return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.page.edited')), 400);
                } else {
                    if ($page->end_edit) {
                        $page->end_edit = date('Y-m-d H:i:s', strtotime("+" . config('app.edit_max_time_seconds') . " seconds", strtotime($page->end_edit)));
                    } else {
                        $page->end_edit = date('Y-m-d H:i:s', strtotime("+" . config('app.edit_max_time_seconds') . " seconds"));
                    }
                    $page->user_edit_id = Auth::user()->id;
                    $page->save();
                    DB::commit();
                    $result = new \stdClass();
                    $result->status = true;
                    $result->time_left = self::calculateTimeLeftForEdit($page);

                    return Response::json(RestController::generateJsonResponse(false, 'You can edit this page', $result), 200);
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            if (config('app.debug')) {
                // Add stack trace
                $errorArray['debug'] = ['exception_message' => $message, 'stacktrace' => $e->getTraceAsString()];
            }

            return Response::json(RestController::generateJsonResponse(true, $message), 500);
        }
    }

    /**
     * Unlock page for editing
     * URL: pages/unlock
     * METHOD: POST
     */
    public function postUnlock()
    {
        $input = Request::json()->all();
        if (empty($input['pageId'])) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        $pageId = $input['pageId'];
        try {
            DB::beginTransaction();
            $page = Page::find($pageId);
            if (is_null($page)) {
                return Response::json(RestController::generateJsonResponse(true, 'Page not found.'), 404);
            } else {
                //Chekc for access to the page
                if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id)) {
                    return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
                }
                
                if (self::isPageInEditMode($page)) {
                    return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.page.edited')), 400);
                } else {
                    $page->end_edit = null;
                    $page->user_edit_id = null;
                    $page->save();
                    DB::commit();

                    return Response::json(RestController::generateJsonResponse(false, 'Page is unlocked', $page), 200);
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            if (config('app.debug')) {
                // Add stack trace
                $errorArray['debug'] = ['exception_message' => $message, 'stacktrace' => $e->getTraceAsString()];
            }

            return Response::json(RestController::generateJsonResponse(true, $message), 500);
        }
    }

    static function checkAvailability($pageId)
    {
        $result = new \stdClass();
        try {
            DB::beginTransaction();
            $page = Page::find($pageId);
            if (is_null($page)) {
                $result->status = false;
                $result->error = "";
            } else {
                //Check for access
                if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id)) {
                    $result->status = false;
                    $result->error = "ws_general_controller.webservice.unauthorized";
                    return $result;
                }

                if (self::isPageInEditMode($page)) {
                    $result->status = false;
                } else {
                    if (($page->workflow->editing_state != STATE_DRAFT && Auth::user()->role >= \USER_ROLE_EDITOR_SMART) ||
                        ($page->workflow->editing_state == STATE_DRAFT && Auth::user()->role >= \USER_ROLE_EDITOR_SMART && $page->workflow->user_responsible != Auth::user()->id && $page->workflow->user_responsible > 0)
                    ) {
                        $result->status = false;
                        $result->error = "admin_only";
                        $result->name = "";
                        if ($page->workflow->user_responsible > 0) {
                            $user = User::find($page->workflow->user_responsible);
                            $result->name = $user->first_name . ' ' . $user->last_name;
                        }
                    } else {
                        if ($page->end_edit && $page->user_edit_id == Auth::user()->id && strtotime($page->end_edit) > strtotime(EDIT_TIME_BUFFER_INTERVAL)) {
                            $result->status = true;
                        } else {
                            $page->end_edit = date('Y-m-d H:i:s', strtotime("+" . config('app.edit_max_time_seconds') . " seconds"));
                            $page->user_edit_id = Auth::user()->id;
                            // Skip full validation $page->hasSkipValidation = true;
                            $page->save(['parent_id' => ['required', 'integer']]);
                            DB::commit();
                            $result->status = true;
                        }
                        $result->time_left = self::calculateTimeLeftForEdit($page);
                    }
                }
            }
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            DB::rollback();

            if (config('app.debug')) {
                // Add stack trace
                $result->debug = ['exception_message' => trans('ws_general_controller.validation.failed'), 'stacktrace' => $e->getTraceAsString()];
            }
            $result->status = false;
            $result->error = $e->getErrors();
        } catch (\Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            if (config('app.debug')) {
                // Add stack trace
                $result->debug = ['exception_message' => $message, 'stacktrace' => $e->getTraceAsString()];
            }
            $result->status = false;
            $result->error = $message;
        }
        return $result;
    }

    /**
     * Get Notes for the page
     * URL: pages/notes/{page_uid}
     * METHOD: GET
     */
    public function getNotes($pageUId)
    {

        if (empty($pageUId)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }

        $page = Page::find($pageUId);
        // Check acsess
        if (is_null($page) || !UserAccessHelper::hasAccessForPageAndAscendants($page->id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
        }

        $notes = Note::where('page_id', '=', $pageUId)->with('user')->orderBy('created_at', 'DESC')->get();
        foreach ($notes as $key => $note) {
            $notes[$key]->created = date('d.m.Y H:i', strtotime($note->created_at));
            if ($note->user === null) {
                $dummyuser = new User();
                $notes[$key]->setUserAttribute($dummyuser->getDeletedUserData($note->user_id));
            }
        }

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of notes found.', $notes), 200);

        return $jsonResponse;
    }

    private static function isPageInEditMode($page)
    {                                       
        return ($page->end_edit && $page->user_edit_id != Auth::user()->id
            && strtotime($page->end_edit) > strtotime(EDIT_TIME_BUFFER_INTERVAL));
    }

    // Return the time left for edit in minutes
    public static function calculateTimeLeftForEdit($page)
    {
        $time_left = strtotime($page->end_edit) - strtotime("now");

        Log::debug("Page id: " . $page->u_id . " TimeLeft: " . $time_left . " End time: " . $page->end_edit . " / " . strtotime($page->end_edit));

        return $time_left;
    }

    /**
     * VERSIONS METHODS
     */
    /**
     * Return all Page Versions
     * URL: pages/versions/{ID}/
     * METHOD: GET
     */
    public function getVersions($id = null)
    {
        $req = Request::all();
        if ($id == null) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id'), []), 400);
        }
        if (empty($req['lang'])) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.lang')), 400);
        }

        if (!UserAccessHelper::hasAccessForPageAndAscendants($id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
        }
        $pages = Page::where('page.id', '=', $id)->with('workflow')
            ->join('content as c', 'c.page_u_id', '=', 'page.u_id')
            ->where('c.lang', '=', $req['lang'])
            ->where('c.section', '=', 'title')->select('page.*', 'c.body as title')->get();
        if (count($pages) == 0) {
            return Response::json(RestController::generateJsonResponse(true, "Page doesn't have versions.", []), 400);
        }
        if (Access::readbypage($id)->count() > 0) {
            foreach ($pages as $page) {
                $page->authorization = 1;
            }
        }

        return Response::json(RestController::generateJsonResponse(false, 'List of pages found.', array_values($pages->toArray())), 200);
    }

    /**
     * Return Copied Version Page like JSON
     * URL: pages/versioncopy
     * METHOD: POST
     */
    public function postVersioncopy()
    {
        $req = Request::all();
        
        if (empty($req['id'])) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        if (empty($req['lang'])) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        $id = $req['id'];
        $page = Page::find($id);
        if (is_null($page)) {
            return Response::json(RestController::generateJsonResponse(true, 'Page not found.', $id), 404);
        }
        if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
        }
                
        $copyPage = new Page();
        $copyPage->forceFill($page->getAttributes());
        unset($copyPage->u_id);
        $copyPage->active = 0;
        $copyPage->end_edit = null;
        $copyPage->user_edit_id = null;
        $copyPage->slug = $page->slug;
        $copyPage->created_by = Auth::user()->id;
        $copyPage->is_visible = 0;
        $copyPage->publish_date = DATE_TIME_ZERO;
        $copyPage->unpublish_date = DATE_TIME_ZERO;
        
        //FIXME: Related pages are not in use in dbdn!
        $related = Related::where('page_u_id', $page->u_id)->get();
        $relatedArray = [];

        $result;
        $galleryImgs = DB::table('gallery')->where('page_id', $page->u_id)
            ->join('file', 'file.id', '=', 'gallery.file_id')
            ->whereNull('file.deleted_at')
            ->where('file.deleted', 0)
            ->select('gallery.file_id', 'gallery.langs')
            ->get();
        $arr = $page->getAccess();
        
        try {
            DB::beginTransaction();
            $version = Page::select('version')->where('id', '=', $page->id)->orderBy('version', 'desc')->take(1)->get();
            $copyPage->version = (int)$version[0]->version + 1;
    
            $pageResult = $this->trySave($copyPage, null, $copyPage->position, $copyPage->parent_id, $arr, $page->tags);
            
            $newPageID = json_decode($pageResult->getContent())->response->u_id;
            $contentArray = new Collection();
            foreach ($page->contents as $content) {
                unset($content->id);
                $newContent = new Content();
                $newContent->fill($content->getAttributes());
                $newContent->page_u_id = $newPageID;
                $contentArray->add($newContent);
                if ($newContent->section == 'title' && $newContent->lang == $req['lang']) {
                    $copyPage->title = $newContent->body;
                }
            }

            $contentController = App::make(\App\Http\Controllers\ContentController::class);
            $contentResult = $contentController->trySave($contentArray, false);
            
            //FIXME: Related pages are not in use in dbdn!
            // Begin Related pages copy
            foreach ($related as $key => $value) {
                $newRelated = new Related();
                $newRelated->fill($value->toArray());
                $newRelated->page_u_id = $newPageID;
                $relatedArray[] = $newRelated;
            }
            
            if (count($relatedArray) > 0) {
                // Save the content
                $relatedresult = $contentController->trySave($relatedArray, true, true);                
            }
            // End related pages copy
            
            $insertData = [];
            if (!$galleryImgs->isEmpty()) {
                $nowTime = \Carbon\Carbon::now();
                foreach ($galleryImgs as $key => $galleryImg) {
                    $insertData[] = [
                        'file_id' => $galleryImg->file_id, 
                        'page_id' => $newPageID, 
                        'langs' => $galleryImg->langs,
                        Gallery::CREATED_AT => $nowTime, 
                        Gallery::UPDATED_AT => $nowTime 
                    ];
                }
            }
            if (count($insertData) > 0) {
                Gallery::insert($insertData);
            }
            DB::commit();
            $result = Response::json($copyPage, 200);
        } catch (CopyPageException $cpe) {
            DB::rollback();
            throw $cpe;
        } catch (\Exception $e) {
            DB::rollback();

            $message = $e->getMessage();
            throw new CopyPageException('Version copy error: ' . $message, 0, $e);
        }

        return $result;
    }
}

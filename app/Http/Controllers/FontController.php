<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Request;

class FontController extends Controller
{

    public function index($font)
    {
        $debug = Request::get('__debug');
        if ($debug) {
            print_r('Request:header:referer: ' . Request::header('referer'));
            $server_referer = 'empty';
            if (!empty($_SERVER['HTTP_REFERER'])) {
                $server_referer = $_SERVER['HTTP_REFERER'];
            }
            print_r('<br> _SERVE:referer: ' . $server_referer);
            die('<br> debug end!');
        }
        // End of debug

        if (empty($_SERVER['HTTP_REFERER'])) {
            header('HTTP/1.0 403 Forbidden');
            die('Forbidden!');
        }

        if (array_key_exists('HTTP_IF_MODIFIED_SINCE', $_SERVER)) {
            header('HTTP/1.1 304 Not Modified');
            header('Expires: ' . self::makeGMTime(time() + 365 * 24 * 60 * 60));
            header('Cache-Control: max-age=315360000');
            die;
        }

        $font     = basename($font);
        $fontFile = false;
//TODO: Load fonts form resources!
        $path = resource_path() . DIRECTORY_SEPARATOR . 'fonts';

        $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));

        foreach ($iterator as $object) {
            if ($object->getFilename() == $font) {
                $fontFile = $object->getPathname();
                break;
            }
        }

        if (!$fontFile) {
            header('HTTP/1.0 404 Not Found');
            // TODO - redirect to real 404 page (maybe)
            die('404 Not Found');
        }

        header('Pragma: cache');
        header('Accept-Ranges: bytes');
        header('Content-Type: ' . self::fontMimeType($fontFile));
        header('Content-Length: ' . filesize($fontFile));
        header('Last-Modified: ' . self::makeGMTime(filemtime($fontFile)));
        header('Vary:Accept-Encoding,User-Agent');

        readfile($fontFile);
        exit;
    }

    private static function makeGMTime($timestamp)
    {
        return gmdate('D, d M Y H:i:s', $timestamp) . ' GMT';
    }

    private static function fontMimeType($fontFile)
    {
        /*
          Here are the official MIME types for Web Fonts:

          .eot -> application/vnd.ms-fontobject (as from December 2005)
          .otf -> application/font-sfnt (as from March 2013)
          .svg -> image/svg+xml (as from August 2011)
          .ttf -> application/font-sfnt (as from March 2013)
          .woff -> application/font-woff (as from January 2013)
         */
        $ext = strtolower(pathinfo($fontFile, PATHINFO_EXTENSION));

        switch ($ext) {
            case 'svg':
                $type = 'image/svg+xml';
                break;
            case 'eot':
                $type = 'application/vnd.ms-fontobject';
                break;
            case 'otf':
                $type = 'font/opentype';
                break;
            case 'ttf':
                $type = 'application/x-font-ttf';
                break;
            case 'woff':
                $type = 'application/font-woff';
                break;
            case 'woff2':
                $type = 'font/woff2';
                break;
            default:
                $type = mime_content_type($fontFile);
        }

        return $type;
    }
}

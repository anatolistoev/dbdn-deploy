<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Models\Cart;
use App\Models\Download;
use App\Models\Page;
use App\Models\FileItem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Description of CartController
 *
 * @author i.traykov
 */
class DownloadController extends RestController
{
    /**
     * Return Dwonloads in Page
     * URL: download/downloads
     * METHOD: GET
     */

    public function getDownloads($page_id = 0)
    {

        $req = Request::all();

        //parameter userId must be set
        if ($page_id <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }

        if (!isset($req['lang']) || $req['lang'] < 0) {
            $file_ids = Download::where('page_u_id', '=', $page_id)->orderBy('position', 'asc')->get(); // get items on page
            $file_items = [];
            foreach ($file_ids as $file_id) {
                //get attributes with file dependency from model
                $file_item = $file_id->file()->first()->toArray();
                $file_items[] = $file_item;
            }
            $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of files found.', $file_items), 200);
        } else {
            //sql query to take file data and file info title and description
            $file_items = DB::table('file as f')
                ->select(DB::raw('f.*, f_i.title, f_i.description'))
                ->join('fileinfo as f_i', 'f_i.file_id', '=', 'f.id')
                ->join('download as d', 'd.file_id', '=', 'f.id')
                ->where('d.page_u_id', '=', $page_id)
                ->where('f_i.lang', '=', $req['lang'])
                ->whereNull('f.deleted_at')
                ->orderBy('d.position', 'asc')->get();
            foreach ($file_items as $key => $fi) {
                unset($file_items[$key]->deleted_at);
            }
            $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of files found.', $file_items), 200);
        }

        return $jsonResponse;
    }

    /**
     * Create Download
     * URL: download/createdownload
     * METHOD: POST
     */

    public function postCreatedownload()
    {

        $req = Request::all();
        $page = Page::find($req['page_u_id']);
        //parameter fileId must be set
        if (!isset($req['page_u_id']) || $req['page_u_id'] <= 0 || !$page) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }
        if (!isset($req['file_id']) || $req['file_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter file_id is required!'), 400);
        }
        $page = Page::find($req['page_u_id']);
        if(!$page){
            return Response::json(RestController::generateJsonResponse(true, 'Page not found!'), 400);
        }
        $isPageRestricted = count($page->getAccess()) > 0 ? true : false;

        $file = FileItem::find($req['file_id']);
        if(!$file){
            return Response::json(RestController::generateJsonResponse(true, 'File not found!'), 400);
        }
        if($file->protected && !$isPageRestricted){
            return Response::json(RestController::generateJsonResponse(true,  trans('js_localize.file.protected.error')), 400);
        }

        $download = new Download();
        $download->fill($req);
        $download->page_id = $page->id;
        $download->page_u_id = $page->u_id;
        if (!isset($req['position']) || $req['position'] < 0) {
            $max_pos = Download::where('page_u_id', '=', $page->u_id)->max('position');
            if (empty($max_pos)) {
                $max_pos = 0;
            }
            //set position with one after the last item
            $download->position = $max_pos + 1;
        }

        return $this->trySave($download, false);
    }

    /**
     * Return save result
     */
    protected function trySave(Download $download, $isUpdate)
    {
        try {
            $download_position = $download->position;

            if ($isUpdate) {
                // update query
                // laravel does not work with composite primary keys and update does not work
                Download::where('page_u_id', '=', $download->page_u_id)->where('file_id', '=', $download->file_id)->update(['position' => $download_position]);
                $message = "Download was updated.";
            } else {
                $download->save();
                unset($download->id);
                $message = "Download was added.";
            }

            //Success !!!
            $result = Response::json(RestController::generateJsonResponse(false, $message, $download));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            throw new ModelValidationException($e->getErrors());
        } catch (\Exception $e) {
            $message = $e->getMessage();
            if (0 === strpos($message, 'SQLSTATE[23000]')) {
                $downloadBuilder = Download::withTrashed()->where('file_id', '=', $download->file_id)->where('page_u_id', '=', $download->page_u_id);
                $download = $downloadBuilder->first();
                if ($download->trashed()) {
                    $res = $downloadBuilder->update(['deleted_at' => null, 'position' => $download_position]);
                    $message = "Download was added.";
                    $result = Response::json(RestController::generateJsonResponse(false, $message, $download));
                } else {
                    $result = Response::json(RestController::generateJsonResponse(true, 'Duplicate record.'), 500);
                }
            } else {
                $result = Response::json(RestController::generateJsonResponse(true, $message), 500);
            }
        }

        return $result;
    }


    /**
     * Modify Download
     * URL: download/modifydownload
     * METHOD: PUT
     */
    public function putModifydownload()
    {

        $req = Request::all();

        //parameters fileId and userId must be set
        if (!isset($req['page_u_id']) || $req['page_u_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }
        if (!isset($req['file_id']) || $req['file_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter file_id is required!'), 400);
        }

        if (!isset($req['position']) || $req['position'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter file_id is required!'), 400);
        }
        //find last position
        $max_pos = Download::where('page_u_id', '=', $req['page_u_id'])->max('position');

        $download = new Download();

        $download = Download::where('page_u_id', '=', $req['page_u_id'])->where('file_id', '=', $req['file_id'])->first();
        //set position to our element
        if ($req['position'] > $max_pos) {
            $download->position = $max_pos;
        } else {
            $download->position = $req['position'];
        }
        //reorder elements escept modifying element
        $page_downloads = Download::where('page_u_id', '=', $req['page_u_id'])->where('file_id', '<>', $req['file_id'])->orderBy('position', 'asc')->get();
        $pos = 1;
        foreach ($page_downloads as $pd) {
            //miss the position of modifying element
            if ($pos == $req['position']) {
                $pos++;
            }
            $pd->position = $pos;
            // update query because save wont work
            Download::where('page_u_id', '=', $pd->page_u_id)->where('file_id', '=', $pd->file_id)->update(['position' => $pos]);

            $pos++;
        }

        return $this->trySave($download, true);
    }


    /**
     * Remove Download From Page
     * URL: download/removedownload
     * METHOD: DELETE
     */
    public function deleteRemovedownload()
    {

        $req = Request::all();

        //parameters fileId and userId must be set
        if (!isset($req['page_u_id']) || $req['page_u_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }
        if (!isset($req['file_id']) || $req['file_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter file_id is required!'), 400);
        }

        $download = Download::where('page_u_id', '=', $req['page_u_id'])->where('file_id', '=', $req['file_id'])->get();

        if ($download->isEmpty()) {
            return Response::json(RestController::generateJsonResponse(true, 'Download not found.'), 404);
        }

        $deleteditem = $download[0];
        Download::where('page_u_id', '=', $deleteditem->page_u_id)->where('file_id', '=', $deleteditem->file_id)->delete();
        Cart::where('file_id', '=', $deleteditem->file_id)->forceDelete();

        $page_downloads = Download::where('page_u_id', '=', $req['page_u_id'])->orderBy('position', 'asc')->get();
        $pos = 1;
        //rearrange items after deleting element
        foreach ($page_downloads as $pd) {
            $pd->position = $pos;
            Download::where('page_u_id', '=', $pd->page_u_id)->where('file_id', '=', $pd->file_id)->update(['position' => $pos]);
            $pos++;
        }

        return Response::json(RestController::generateJsonResponse(false, 'Download was deleted!', $download[0]));
    }

    /**
     * Remove All Downloads From Page
     * URL: download/removedownloadbypage
     * METHOD: DELETE
     */

    public function deleteRemovedownloadbypage()
    {

        $req = Request::all();

        //parameters fileId and userId must be set
        if (!isset($req['page_u_id']) || $req['page_u_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }

        $download = Download::where('page_u_id', '=', $req['page_u_id'])->get();

        if ($download->isEmpty()) {
            return Response::json(RestController::generateJsonResponse(true, 'Download not found.'), 404);
        }

        Download::where('page_u_id', '=', $req['page_u_id'])->delete();

        return Response::json(RestController::generateJsonResponse(false, 'Page Downloads were deleted!'));
    }

    /**
     * Remove All Downloads From Page for unit tests
     * URL: download/removedownloadbypage
     * METHOD: DELETE
     */

    public function deleteForcedeletedownloadbypage(){

        $req = Input::all();

        //parameters fileId and userId must be set
        if(!isset($req['page_u_id']) || $req['page_u_id'] <= 0){
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }

        $download = Download::withTrashed()->where('page_u_id','=',$req['page_u_id'])->get();

        if ($download->isEmpty()) {
            return Response::json(RestController::generateJsonResponse(true, 'Download not found.'), 404);
        }

        Download::withTrashed()->where('page_u_id','=',$req['page_u_id'])->delete();

        return Response::json(RestController::generateJsonResponse(false, 'Page Downloads were deleted!'));
    }

    /**
     * delete/create only deleted/created downloads
     * URL: download/recreate
     * METHOD: POST
     */

    public function postRecreate()
    {

        $req = Request::all();
        //parameters fileId and userId must be set
        if (!isset($req['page_u_id']) || $req['page_u_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }
        $page = Page::find($req['page_u_id']);
        if(!$page){
            return Response::json(RestController::generateJsonResponse(true, 'Page not found!'), 400);
        }
        $isPageRestricted = count($page->getAccess()) > 0 ? true : false;

        $new_downloads = $req['downloads'];

        $cur_downloads = Download::where('page_u_id', '=', $req['page_u_id'])->pluck('file_id')->toArray();

        $add_ids = array_diff($new_downloads, $cur_downloads);
        $remove_ids = array_diff($cur_downloads, $new_downloads);
        //$upd_ids = array_intersect($cur_downloads, $new_downloads);

        foreach ($remove_ids as $fi) {
            Download::where('page_u_id', '=', $req['page_u_id'])->where('file_id', '=', $fi)->delete();
        }

        $page_downloads = Download::where('page_u_id', '=', $req['page_u_id'])->orderBy('position', 'asc')->get();
        $pos = 1;
        //rearrange items after deleting element
        foreach ($page_downloads as $pd) {
            $pd->position = $pos;
            Download::where('page_u_id', '=', $pd->page_u_id)->where('file_id', '=', $pd->file_id)->update(['position' => $pos]);
            $pos++;
        }
        foreach ($add_ids as $fi) {
            $file = FileItem::find($fi);
            if(!$file){
                return Response::json(RestController::generateJsonResponse(true, 'File not found!'), 400);
            }
            if($file->protected && !$isPageRestricted){
                 return Response::json(RestController::generateJsonResponse(true,  trans('js_localize.file.protected.error')), 400);
            }
            $max_pos = Download::where('page_u_id', '=', $req['page_u_id'])->max('position');
            $max_pos = $max_pos > 0 ? $max_pos : 0;
            $dw = new Download();
            $dw->page_id = $req['page_id'];
            $dw->page_u_id = $req['page_u_id'];
            $dw->file_id = $fi;
            $dw->position = $max_pos + 1;
            $this->trySave($dw, false);
        }
//		foreach($upd_ids as $fi){
//			$dw = Download::where('page_id','=',$req['page_id'])->where('file_id','=',$fi)->get();
//			$dw->position = array_search($fi,$new_downloads);
//			$this->trySave($dw,true);
//		}

        $downloads = Download::where('page_u_id', '=', $req['page_u_id'])->orderBy('position', 'asc')->with('file')->get();

        return Response::json(RestController::generateJsonResponse(false, 'Page Downloads were recreated!', $downloads));
    }
}

<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Helpers\FileHelper;
use App\Helpers\FileRecognizerHelper;
use App\Helpers\UploadHandler;
use App\Helpers\UriHelper;
use App\Helpers\UserAccessHelper;
use App\Models\Download;
use App\Models\FileInfo;
use App\Models\FileItem;
use App\Models\Gallery;
use App\Models\Page;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

/**
 * Description of FileController
 *
 * @author i.traykov
 */
class FileController extends RestController
{

    const FOLDER_NAME_CACHE = '__cache';
    const FOLDER_NAME_THUMBNAIL = '__thumbnail';
    const FOLDER_NAME_ZOOMPIC = '__zoompic';
    const MODE_IMAGES = 'images';
    const MODE_DOWNLOADS = 'downloads';

    protected $unallowed_dirs = [self::FOLDER_NAME_THUMBNAIL, self::FOLDER_NAME_ZOOMPIC, self::FOLDER_NAME_CACHE];
    protected $fm_mode_list = [self::MODE_IMAGES, self::MODE_DOWNLOADS];

    /*
     * Validate inputs
     */
    protected $file_rules = [
        self::MODE_DOWNLOADS => 'required|max:100000000',
//			self::MODE_DOWNLOADS	=> 'required|mimes:jpeg,bmp,png|max:40960', // mimes type guesture is not activated
        self::MODE_IMAGES => 'required|max:10240'
    ];
    protected $rules = [
        //'currentpath'	=> 'regexp:[\s\\/\?%*:|\"<>]', // Regexp validation is not available
        'file_thumb' => 'max:10240',
        'file_zoom' => 'max:10240',
    ];

    public static function getFullPath($filemanager_mode = 'images', $path = '', $isProtected = false, $isDeleted = false)
    {
        if ($isDeleted) {
            $relative_dest_path = '/deleted_files/' . $filemanager_mode . $path;
        } else {
            if ($isProtected) {
                $relative_dest_path = '/protected_files/' . $filemanager_mode . $path;
            } else {
                $relative_dest_path = '/files/' . $filemanager_mode . $path;
            }
        }
        $full_path = public_path() . $relative_dest_path;

        return $full_path;
    }

    protected static function getLastErrorMsg()
    {
        $e = error_get_last();
        if ($e) {
            return $e['message'];
        } else {
            return null;
        }
    }

    private static function isFileInEditMode($fileItem)
    {
        Log::debug("End time: " . $fileItem->end_edit . " datetime: " . strtotime($fileItem->end_edit) . " EditBuffer End: " . strtotime(EDIT_TIME_BUFFER_INTERVAL));

        return ($fileItem->end_edit && $fileItem->user_edit_id != Auth::user()->id
            && strtotime($fileItem->end_edit) > strtotime(EDIT_TIME_BUFFER_INTERVAL));
    }

    // Return the time left for edit in minutes
    private static function calculateTimeLeftForEdit($fileItem)
    {
        $time_left = strtotime($fileItem->end_edit) - strtotime("now");

        Log::debug(" TimeLeft: " . $time_left . " End time: " . $fileItem->end_edit . " / " . strtotime($fileItem->end_edit));

        return $time_left;
    }

    private static function bytes2HumanRedable($bytes)
    {
        $si_prefix = ['B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB'];
        $base = 1024;
        $class = min((int)log(abs($bytes), $base), count($si_prefix) - 1);

        return sprintf('%1.2f', $bytes / pow($base, $class)) . ' ' . $si_prefix[$class];
    }

    public static function getFolderStatus($filemanager_mode = FileController::MODE_IMAGES, $path = '', $isDeleted = false)
    {
        $objFolderStatus = new \stdClass();
        if ($path != '/') {
            $folderFullPath = self::getFullPath($filemanager_mode, $path, false, $isDeleted);
            $folder_space_bytes = self::foldersize($folderFullPath);
            $objFolderStatus->fs_folder_space_bytes = ($folder_space_bytes < 0) ? 0 : $folder_space_bytes;
            if ($filemanager_mode === FileController::MODE_DOWNLOADS) {
                $protectedFolderFullPath = self::getFullPath($filemanager_mode, $path, true, $isDeleted);
                $protected_folder_space_bytes = self::foldersize($protectedFolderFullPath);
                $objFolderStatus->fs_folder_space_bytes += ($protected_folder_space_bytes < 0) ? 0 : $protected_folder_space_bytes;
            }

            //Load file size from DB
            //TODO: Store size of thumbnail and zoompic
            $matchString = DB::raw('"%' . $path . '%" COLLATE utf8_bin');
            $filesInDB = FileItem::where('type', '=', $filemanager_mode)->
            where('deleted', 0)->where('filename', 'LIKE', $matchString);

            if ($isDeleted) {
                $filesInDB = $filesInDB->onlyTrashed();
            }

            $db_size = $filesInDB->sum('size');
            $objFolderStatus->db_folder_space_bytes = ($db_size) ? $db_size : 0;
        } else {
            $objFolderStatus->fs_folder_space_bytes = 0;
            $objFolderStatus->db_folder_space_bytes = 0;
        }
        if ($isDeleted && "all" == UserAccessHelper::getUserRootFolder()) {
            FileController::setRecycleStatus($objFolderStatus, $filemanager_mode, $path, $isDeleted);
        }

        return $objFolderStatus;
    }

    private static function setRecycleStatus($objFolderStatus, $filemanager_mode = 'images', $path = '', $isDeleted = false)
    {
        $brandRootFolder = UserAccessHelper::getUserRootFolder();
        $brandRootPageID = UserAccessHelper::getUserRootPageID();

        $isAdmin = false;
        if ($brandRootFolder == "all") {
            // Admin or Approver
            $isAdmin = true;
            // Get Brand folder!
            $brand = self::getBrnadOfFile($path);
            $brandRootPageID = $brand->ID;
            if ("all" == $brand->folder) {
                $brandRootFolder = '/';
            } else {
                $brandRootFolder = '/' . $brand->folder;
            }
        } else {
            $brandRootFolder = '/' . $brandRootFolder;
        }
        $brandRootFolderFullPath = self::getFullPath($filemanager_mode, $brandRootFolder, false, $isDeleted);
        self::prepareRecycleStatus($objFolderStatus, $brandRootFolderFullPath, $brandRootPageID, $brandRootFolder, $isAdmin);
    }

    private static function prepareRecycleStatus(
        $objRecycleStatus,
        $brandRootFolderFullPath,
        $brandRootPageID,
        $brandRootFolder,
        $hasIncludeTotalFree
    ) {

        $serverFreeBytes = disk_free_space($brandRootFolderFullPath);
        $serverTotalBytes = disk_total_space($brandRootFolderFullPath);

        $recycleBinQuotaTotal = config('app.recycle_quota_total') / 100;
        $recycle_quota_brands = config('app.recycle_quota_brands');
        $recycleBinQuotaBrand = $recycle_quota_brands[$brandRootPageID] / 100;
        $recycleBinQuotaTotalBytes = $serverTotalBytes * $recycleBinQuotaTotal;
        $recycleBinQuotaBrandBytes = $recycleBinQuotaTotalBytes * $recycleBinQuotaBrand;

        $objRecycleStatus->total_quota_space_bytes = $recycleBinQuotaTotalBytes;
        $objRecycleStatus->total_quota_space = $recycleBinQuotaTotal;
        $objRecycleStatus->brand_quota_space_bytes = $recycleBinQuotaBrandBytes;
        $objRecycleStatus->brand_quota_space = $recycleBinQuotaBrand;

        if ($brandRootFolder === '/') {
            $base_path = $brandRootFolderFullPath;
            $excludeList = [];
            $specialPages = config('app.pages');
            foreach ($specialPages as $key => $value) {
                if ($value->folder != "all") {
                    $excludeList[] = $value->folder;
                }
            }
        } else {
            $base_path = dirname($brandRootFolderFullPath);
            $excludeList = null;
        }

        // 2. Get the free space for the brand. Calculate size from DB or File. Cache?
        // File
        if ($hasIncludeTotalFree) {
            // Total Free Space
            $summaryTotalBytes = self::foldersize($base_path);
            $freeSpaceTotalBytes = $recycleBinQuotaTotalBytes - $summaryTotalBytes;

            $objRecycleStatus->total_free_space_bytes = min($freeSpaceTotalBytes, $serverFreeBytes);
            if ($recycleBinQuotaTotalBytes == 0) {
                $objRecycleStatus->total_free_space = 0;
            } else {
                $objRecycleStatus->total_free_space = $objRecycleStatus->total_free_space_bytes * 100 / $recycleBinQuotaTotalBytes;
            }
        } else {
            $objRecycleStatus->total_free_space_bytes = 0;
            $objRecycleStatus->total_free_space = 0;
        }

        // Brans Free Space
        $summaryBrandBytes = self::foldersize($brandRootFolderFullPath, $excludeList);
        //Log::debug("Brand path: " . $brandRootFolderFullPath . ' space: ' . $summaryBrandBytes);
        $freeSpaceBrandBytes = $recycleBinQuotaBrandBytes - $summaryBrandBytes;

        $objRecycleStatus->brand_free_space_bytes = min($freeSpaceBrandBytes, $serverFreeBytes);
        if ($recycleBinQuotaBrandBytes == 0) {
            $objRecycleStatus->brand_free_space = 0;
        } else {
            $objRecycleStatus->brand_free_space = $objRecycleStatus->brand_free_space_bytes * 100 / $recycleBinQuotaBrandBytes;
        }

        //Log::debug(print_r($objRecycleStatus, true));
        return $objRecycleStatus;
    }

    /**
     * Return File by ID
     * URL: files/read/{ID}
     * METHOD: GET
     */
    public function getRead($id = null)
    {
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }

        $fileItem = FileItem::find($id);

        if (strpos($fileItem->filename, "/" . UserAccessHelper::getUserRootFolder() . "/") === false && UserAccessHelper::getUserRootFolder() != "all") {
            return Response::json(RestController::generateJsonResponse(true, 'You don\'t have access to read this file!'), 404);
        }

        if (is_null($fileItem)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_file_controller.file.notFound')), 404);
        } else {
            $fileResult = $fileItem->toArray();
            $fileResult['fileInfo'] = $fileItem->info()->getResults()->toArray();

            return Response::json(RestController::generateJsonResponse(false, trans('ws_file_controller.file.found'), $fileResult));
        }
    }

    public function getDiscription($id = null)
    {
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        $lang = Request::has('lang') ? Request::get('lang') : 1;
        $fileItem = FileItem::join('fileinfo', 'file.id', '=', 'fileinfo.file_id')
            ->where('file.id', $id)
            ->where('fileinfo.lang', $lang)
            ->select('file.filename', 'fileinfo.description')
            ->first();

        if (strpos($fileItem->filename, "/" . UserAccessHelper::getUserRootFolder() . "/") === false && UserAccessHelper::getUserRootFolder() != "all") {
            return Response::json(RestController::generateJsonResponse(true, 'You don\'t have access to read this file!'), 404);
        }

        if (is_null($fileItem)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_file_controller.file.notFound')), 404);
        } else {
            return Response::json(RestController::generateJsonResponse(false, trans('ws_file_controller.file.found'), $fileItem));
        }
    }

    private function loadGallery($file_ids)
    {
        $filtered_files['images'] = [];

        $order_clause = 'FIELD(id';
        foreach ($file_ids as $fi) {
            $order_clause .= ',?';
        }
        $order_clause .= ')';
        $files = FileItem::with(
            [
                'info' => function ($query) {
                    $lang = Session::get('lang');
                    $query->where('lang', '=', $lang);
                }
            ]
        )->whereIn('id', $file_ids)->orderByRaw($order_clause, $file_ids)->get();

        foreach ($files as $file) {
            // Validate for unalowed files access
            if (strcasecmp($file->type, FileController::MODE_IMAGES) != 0) {
                Log::error('FileController::loadGallery Atempt to open Download file:' . $file->id . ' type: ' . $file->type);
                continue;
            }
            $file_info = $file->info->first();
            $file_describtion = ($file_info) ? $file_info->description : "";
            $originalFile = FileHelper::findSourceImage(FileController::FOLDER_NAME_ZOOMPIC, $file, $file->protected);
            $imageType = FileHelper::getFileExtension($originalFile);
            $imagePath = $file->getPath();
            if ($file->protected) {
                $lowresPath = $file->getCachedThumbPath('content');
            } else {
                $lowresPath = UriHelper::generateImgPath($imagePath, FileController::MODE_IMAGES, 'content', $imageType);
            }

            $filtered_files['images'][] = ['path' => asset($imagePath), 'descr' => $file_describtion, 'lowres' => $lowresPath];
        }

        return $filtered_files;
    }

    /**
     * Return Files for FE Gallery
     * URL: files/gallery
     * METHOD: POST
     */
    public function galleryData()
    {
        $req = Request::all();

        if (empty($req['files'])) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_file_controller.file.required')), 400);
        }

        $file_ids = $req['files'];
        foreach ($file_ids as $key => $value) {
            if (is_numeric($value)) {
                $value = [$value];
            }
            $filtered_files[] = $this->loadGallery($value);
            //TODO: Return information for protected files
        }

        if (count($filtered_files) == 0) {
            $jsonResponse = Response::json(RestController::generateJsonResponse(
                false,
                trans('ws_file_controller.file.list.not_found')
            ), 404);
        } else {
            $jsonResponse = Response::json(RestController::generateJsonResponse(
                false,
                trans('ws_file_controller.file.list.found'),
                $filtered_files
            ), 200);
        }

        return $jsonResponse;
    }

    public function explainerView()
    {
        $req = Request::all();

        if (empty($req['files'])) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_file_controller.file.required')), 400);
        }

        $file_ids = $req['files'];
        if (is_numeric($file_ids)) {
            $file_ids = [$file_ids];
        }
        //TODO: Return information for protected files
        $filtered_files = $this->loadGallery($file_ids);
        foreach ($filtered_files['images'] as $key => $image) {
            $filtered_files['images'][$key]['lowres'] = preg_replace('/content/', 'zoom', $image['lowres'], 1);
        }

        return view('explainer', ['files' => $filtered_files]);
    }

    /**
     * Return protected File Content for FE
     * URL: files/protected-file/{id}/{ftype}?policy=XXXXXXX
     * METHOD: GET
     *
     * $ftype = doc || brand_carousel || overview ... (see in app/config/assets.php for full list)
     */
    public function protectedFileContent($id, $ftype)
    {
        $fileItem = FileItem::find($id);
        //Log::debug("Protected fileID: " . $id . ' Type: ' . $ftype . ' Found File: \n' . $fileItem);
        if ($fileItem) {
            $policy = Request::get('policy');
            $protectedFile = $fileItem->protected != 0;
            $filemanager_mode = strtolower($fileItem->type);

            if ((Auth::check() && (!$protectedFile || $fileItem->checkPolicy($policy)))
                || ($fileItem->thumb_visible && $ftype != 'doc' && $ftype != 'thumb' && $fileItem->checkPolicy($policy))
                || ($filemanager_mode == FileController::MODE_IMAGES && $fileItem->checkPolicy($policy))
            ) {
                if ($ftype == 'doc' || $ftype == 'thumb') { // Download documents or Thumbnail
                    // Validate access to files
                    if (!UserAccessHelper::authUserHasAccessToFile($fileItem)) {
                        Log::error('FileController::protectedFileContent Missing access for file:' . $fileItem->id . ' protcted: ' . json_encode($protectedFile));
                        return redirect('/file-not-found');
                    }

                    $filePath = FileController::getFullPath(
                        $filemanager_mode,
                        ($ftype == 'thumb') ? $fileItem->thumbnail : $fileItem->filename,
                        $protectedFile
                    );
                    //Download files as Attachments
                    $response = Response::download($filePath);
                } else {
                    $sizeArr = config('assets.' . $filemanager_mode . '.sizes');
                    // Check for HiRes factor
                    list($imageSize, $hires_factor) = FileHelper::splitImageSizeAndHiRes($ftype);
                    if (isset($sizeArr[$imageSize])) {
                        $imgConf = $sizeArr[$imageSize];
                        // Make the url to image : {$imgConf['prefix']}/{$fileItem->filename}
                        // example: __cache/content/Daimler/best_practice/img_AR15_04_940X.jpg
                        $imageSizeAndHiRes = ltrim($imgConf['prefix'], '/') . FileHelper::HIRES_FACTOR_SEPARATOR . $hires_factor . 'x';
                        $originalFile = FileHelper::findSourceImage(
                            FileController::FOLDER_NAME_ZOOMPIC,
                            $fileItem,
                            $fileItem->protected
                        );
                        $imageType = FileHelper::getFileExtension($originalFile);
                        $file_path_cached = $imageSizeAndHiRes . $fileItem->filename . $imageType;
                        $response = $this->getResizedImage($filemanager_mode, $file_path_cached, $protectedFile);
                    } else {
                        Log::debug("Unknown filetype: " . $ftype . ' for fileid: ' . $fileItem->$id);
                        //TODO: There should be responce here!
                        $response = redirect('/file-not-found');
                    }
                }

                return $response;
            } else {
                Log::debug('No access. Auth::check(): ' . json_encode(Auth::check())
                    . ' Policy check: ' . json_encode($fileItem->checkPolicy($policy))
                    . ' ClientIP:' . UriHelper::getClientIpAddr(Request::instance()));
            }
        }

        return redirect('/file-not-found');
    }

    /**
     * Return resized image File Content for FE
     * URL: files/{filemanager_mode}/{file_path}
     * METHOD: GET
     *
     * $filemanager_mode = images|downloads
     *
     * $file_path_cached = {__cache/brand_carousel}/Daimler/best_practice/img_AR15_04_940X.jpg
     * || {__cache/brand_carousel~2x}/Daimler/best_practice/img_AR15_04_940X.jpg
     * || {__cache/content}/Daimler/best_practice/img_AR15_04_940X.jpg
     *
     */
    public function getResizedImage($filemanager_mode, $file_path_cached, $isProtected = false)
    {
        // We check if file is not generated few millis ago
        //Log::info("Image URL for cache: " . $file_path_cached);
        //Download Images as stream
        try {
            $filePathCached = FileHelper::generateCacheImage($filemanager_mode, $file_path_cached, $isProtected);

            $response = Response::download($filePathCached);
            $response->headers->set('Content-Disposition', null);

            return $response;
        } catch (\Exception $exc) {
            Log::warning("Problem with cached image: " . $filemanager_mode . '/' . $file_path_cached . ' Error: ' . $exc->getMessage());
            Log::debug('Server Referer:' . Request::server('HTTP_REFERER') . ' Referer: ' . Request::header('referer'));
            Log::debug($exc);

            return redirect('/file-not-found');
        }
    }

    /**
     * Return List of Files
     * URL: files/all/images, files/all/downloads
     * METHOD: GET
     */
    public function getAll($filemanager_mode)
    {

        if (!in_array($filemanager_mode, $this->fm_mode_list)) {
            return Response::json(
                RestController::generateJsonResponse(true, trans('ws_file_controller.filemanager.wrongMode')),
                400
            );
        }

        $sort_fields = ['filename', 'id', 'size', 'extension', 'dimensions', 'resoltion', 'color_mode', 'updated_at', 'created_at'];
        $req = Request::all();
        // Validation of imput
        if (!empty($req['init_selection']) && !preg_match(config('app.INIT_SELECTION_PATTERN'), $req['init_selection'])) {
            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_general_controller.invalid.parameter', ["attribute" => "init_selection"])
            ), 400);
        }

        if (empty($req['path'])) {
            $dirname = '/';
        } else {
            $dirname = $req['path'];
        }

        if (empty($req['isDeleted'])) {
            $isDeleted = false;
        } else {
            $isDeleted = $req['isDeleted'] == 'true' ? true : false;
        }

        if (!preg_match(config('app.PATH_NAME_PATTERN'), $dirname)) {
            /* $dirname contains at least one illegal character. */
            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_general_controller.illegalChar.parameter', ["attribute" => "path"])
            ), 400);
        }

        $root_path = FileController::getFullPath($filemanager_mode, $dirname, false, $isDeleted);
        // Validate 'path' if exists!
        if (!is_dir($root_path)) {
            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_file_controller.dir.root.notexist'),
                ['path' => $dirname]
            ), 400);
        }

        if (strpos($dirname, "/" . UserAccessHelper::getUserRootFolder() . "/") === false && UserAccessHelper::getUserRootFolder() != "all") {
            return Response::json(RestController::generateJsonResponse(true, 'You don\'t have access to this folder!'), 404);
        }

        if (empty($req['filter'])) {
            $filter = null;
        } else {
            $filter = trim($req['filter']);
            // Validate filter
            if (!preg_match(config('app.FILTER_PATTERN'), $filter)) {
                /* $filename contains at least one illegal character. */
                return Response::json(RestController::generateJsonResponse(
                    true,
                    trans('ws_general_controller.illegalChar.parameter', ["attribute" => "filter"])
                ), 400);
            }
        }
        if (!empty($req['sort-by']) && in_array($req['sort-by'], $sort_fields)) {
            $field = $req['sort-by'];
        } else {
            $field = 'filename';
        }
        if (!empty($req['sort-direction']) && strtoupper($req['sort-direction']) == "DESC") {
            $order = "DESC";
        } else {
            $order = "ASC";
        }

        // Build query for specific file type and not permanently deleted files.
        $fileItemQuery = FileItem::where('type', '=', $filemanager_mode)->where('deleted', 0);

        if ($isDeleted) {
            $fileItemQuery->onlyTrashed();
        }
        if (is_null($filter)) {
            // get files by path. Pattern: ^dirname[^/]+$
            $fileNamePattern = "^" . preg_quote($dirname) . "[^/]+$";
        } else {
            // Replace space " " with patern "[-_]."
            $filter_exp = preg_quote($filter);
            $filter_exp = str_replace(" ", "[-_]*", $filter_exp);
            // get files by path and filter. Pattern: ^dirname[^/]*filter[^/]*$
            $fileNamePattern = "^" . preg_quote($dirname) . "[^/]*" . $filter_exp . "[^/]*$";
        }

        $fileItemQuery->select(DB::raw('SUBSTRING(filename,' . (strlen($dirname) + 1) . ') as filename'))
            ->addSelect('id', 'extension', 'updated_at', 'type', 'size', 'protected')
            ->whereRaw('filename COLLATE utf8_bin REGEXP ?', [$fileNamePattern]);

        if (!empty($req['init_selection'])) {
            $arrSelection = explode(',', $req['init_selection']);
            $fileItemQuery->whereNotIn('id', $arrSelection);
            $filesInit = FileItem::whereIn('id', $arrSelection)->orderByRaw('FIELD (id,' . $req['init_selection'] . ')')->get();
        }

        if (!is_null($field)) {
            // sort by field
            $fileItemQuery->orderBy($field, $order);
        }

        $filtered_files = $fileItemQuery->get();

        if (!empty($req['init_selection'])) {
            $filtered_files = $filesInit->merge($filtered_files);
        }
//$queries = DB::getQueryLog();
//$last_query = end($queries);
//print_r($last_query);die;
        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of files found.', $filtered_files), 200);

        return $jsonResponse;
    }

    /**
     * Return List of Files filtered by extension
     * URL: files/filesByType?type={$type}
     * METHOD: GET
     */
    public function getFilesbytype()
    {

        $req = Request::all();

        $filtered_files = FileItem::where('extension', '=', $req['type'])->get();
        $jsonResponse = Response::json(RestController::generateJsonResponse(
            false,
            trans('ws_file_controller.file.list.found'),
            $filtered_files
        ), 200);

        return $jsonResponse;
    }

    /**
     * Return Save file from JSON
     * URL: files/createfile
     * METHOD: POST
     */
    public function postCreatefile()
    {

        $new_file = Request::all();
        
        // file_put_contents('/home/www/logs/purify.log', 'Create file before purify: '.var_export($new_file,true)."\n",FILE_APPEND);
        $new_file = $this->purifyFileData($new_file);
        // file_put_contents('/home/www/logs/purify.log', 'Create file after purify: '.var_export($new_file,true)."\n",FILE_APPEND);
        //TODO: Work with JSON description for file!
        $filemanager_mode = $new_file['mode'];
        if (!in_array($filemanager_mode, $this->fm_mode_list) || !UserAccessHelper::hasAccess($filemanager_mode)) {
            if (File::exists($new_file['file_title'])) {
                File::delete($new_file['file_title']);
            }

            return Response::json(
                RestController::generateJsonResponse(true, trans('ws_file_controller.filemanager.wrongMode')),
                400
            );
        }

        if (strpos($new_file['currentpath'], "/" . UserAccessHelper::getUserRootFolder() . "/") === false && UserAccessHelper::getUserRootFolder() != "all") {
            if (File::exists($new_file['file_title'])) {
                File::delete($new_file['file_title']);
            }

            return Response::json(RestController::generateJsonResponse(
                true,
                'You don\'t have access to create files in this folder!'
            ), 403);
        }

        $currentpath = $new_file['currentpath'];
        $destinationPath = '';
        $filename = '';

        /*
         * Validate inputs
         */
         
         
        // Validate currentpath, validation REGEXP is not working!
        if (!preg_match(config('app.PATH_NAME_PATTERN'), $currentpath)) {
            /* $currentpath contains at least one illegal character. */
            if (File::exists($new_file['file_title'])) {
                File::delete($new_file['file_title']);
            }

            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_general_controller.illegalChar.parameter', ["attribute" => "currentpath"])
            ), 400);
        }

        $this->rules['file'] = $this->file_rules[$filemanager_mode];

        $files = Request::file('files');
        $isInArray = false;
        if ($files) {
            if (isset($new_file['file_title']) && is_numeric($new_file['file_title'])) {
                $isInArray = true;
                $new_file['file'] = $files[$new_file['file_title']];
            }
            if (isset($new_file['file_zoom_title']) && is_numeric($new_file['file_zoom_title'])) {
                $new_file['file_zoom'] = $files[$new_file['file_zoom_title']];
            }
            if (isset($new_file['file_thumb_title']) && is_numeric($new_file['file_thumb_title'])) {
                $new_file['file_thumb'] = $files[$new_file['file_thumb_title']];
            }
            unset($new_file['files']);
        }
        if (!$isInArray && File::exists($new_file['file_title'])) {
            $file = new \Symfony\Component\HttpFoundation\File\UploadedFile(
                $new_file['file_title'],
                basename($new_file['file_title']),
                File::type($new_file['file_title']),
                File::size($new_file['file_title']),
                null,
                true
            );
            if (isset($new_file['file']) && $file->getClientOriginalName() != $new_file['file']) {
                $originalName = $new_file['file'];
            }
            $new_file['file'] = $file;
        }
        if (isset($new_file['upload'])) {
            unset($new_file['upload']);
        }
        $validator = Validator::make($new_file, $this->rules);

        //Validate the inputs. if file is uploaded and size of files.
        if ($validator->passes()) {
            $isProtected = isset($new_file['protected']);
            if ($filemanager_mode == FileController::MODE_DOWNLOADS) {
                if ($isProtected && isset($new_file['thumb_visible'])) {
                    $new_file['thumb_visible'] = 1;
                }
            } else {
                // Set Empty values for fields not used in Images
                $new_file['usage_terms_en'] = '';
                $new_file['usage_terms_de'] = '';
                $new_file['copyright_notes'] = '';
            }
            $upload_file = $new_file['file'];
            $destinationPath = FileController::getFullPath($filemanager_mode, $currentpath, $isProtected);
            if (isset($originalName)) {
                $filename = $originalName;
            } else {
                $filename = $upload_file->getClientOriginalName();
            }
            if (isset($new_file['file_thumb'])) {
                $upload_file_thumb = $new_file['file_thumb'];
                //TODO: Check $upload_file_thumb for null
                $destinationPath_thumb = $destinationPath . self::FOLDER_NAME_THUMBNAIL;
                $filename_thumb = $upload_file_thumb->getClientOriginalName();

                // Validate $filename_thumb
                if (!preg_match(config('app.FILE_NAME_PATTERN'), $filename_thumb)) {
                    /* $filename contains at least one illegal character. */
                    if (File::exists($new_file['file_title'])) {
                        File::delete($new_file['file_title']);
                    }

                    return Response::json(RestController::generateJsonResponse(
                        true,
                        trans(
                            'ws_general_controller.illegalChar.parameter',
                            ["attribute" => "file_thumb"]
                        )
                    ), 400);
                }
            }

            if (isset($new_file['file_zoom'])) {
                $upload_file_zoom = $new_file['file_zoom'];
                $destinationPath_zoom = $destinationPath . self::FOLDER_NAME_ZOOMPIC;
                $filename_zoom = $upload_file_zoom->getClientOriginalName();

                // Validate $filename_zoom
                if (!preg_match(config('app.FILE_NAME_PATTERN'), $filename_zoom)) {
                    /* $filename contains at least one illegal character. */
                    if (File::exists($new_file['file_title'])) {
                        File::delete($new_file['file_title']);
                    }

                    return Response::json(RestController::generateJsonResponse(
                        true,
                        trans('ws_general_controller.illegalChar.parameter', ["attribute" => "file_zoom"])
                    ), 400);
                }
            }


            // Validate 'path' if exists!
            if (!is_dir($destinationPath)) {
                if (File::exists($new_file['file_title'])) {
                    File::delete($new_file['file_title']);
                }

                return Response::json(RestController::generateJsonResponse(
                    true,
                    trans('ws_file_controller.dir.root.notexist'),
                    ['path' => $currentpath]
                ), 400);
            }

//			// Validate file name
//			if (!preg_match(config('app.FILE_NAME_PATTERN'), $filename)) {
//				/* $filename contains at least one illegal character. */
//				return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.illegalChar.parameter', array("attribute" => "filename"))
//								), 400);
//			}
            // Check for file with the same name. Casesensitive!
            $fileMatchCount = FileItem::where('type', '=', $filemanager_mode)
                                ->where('deleted', 0)
                                ->where(DB::raw('filename COLLATE utf8_bin'), '=', $currentpath . $filename)
                                ->count();
            
            
            
            $file_ext=strtolower(pathinfo($upload_file->getClientOriginalName(), PATHINFO_EXTENSION));
            
            if ($filemanager_mode == FileController::MODE_DOWNLOADS) {
                $allowed_exts=config('app.allow_file_ext');
                if ($file_ext=='zip') {
                    $za = new \ZipArchive();
                    $za->open($upload_file->getRealPath());
                    for( $i = 0; $i < $za->numFiles; $i++ ){ 
                        $stat = $za->statIndex( $i ); 
                        if(substr($stat['name'], -1) !== '/') {
                            $inZipFileExt=strtolower(pathinfo($stat['name'], PATHINFO_EXTENSION));
                            if (!in_array($inZipFileExt,$allowed_exts) || $inZipFileExt=='zip') {
                                if (File::exists($new_file['file_title'])) {
                                    File::delete($new_file['file_title']);
                                }

                                return Response::json(RestController::generateJsonResponse(
                                    true,
                                    trans('ws_file_controller.file.zipExtNotAllowed'),
                                    ["attribute" => "\n Affected file: ".$stat['name']]
                                ), 400);
                            } else {
                                $fp = $za->getStream($stat['name']);
                                // if(!$fp) exit("failed\n");
                                $fcontents='';
                                while (!feof($fp)) {
                                    $fcontents .= fread($fp, 2);
                                }
                                fclose($fp);
                                $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
                                $file_info=finfo_buffer($finfo, $fcontents);
                                finfo_close($finfo);
                                // file_put_contents('/home/www/logs/upload.log', 'MiME Type for '.$stat['name'].' in '.$upload_file->getClientOriginalName().': '.var_export($file_info,true)."\n",FILE_APPEND); 
                                $allowed_types=config('app.allow_file_content');
                                if (!in_array($file_info,$allowed_types[$inZipFileExt])) {
                                    if (File::exists($new_file['file_title'])) {
                                    File::delete($new_file['file_title']);
                                    }

                                    return Response::json(RestController::generateJsonResponse(
                                        true,
                                        trans('ws_file_controller.file.zipTypeNotAllowed'),
                                        ["attribute" => "\n Affected file: ".$stat['name']]
                                    ), 400);
                                }
                                
                                
                            }
                        }
                    }
                } 
                
            } else {
                $allowed_exts=config('app.allow_img_ext');
            }
            
            
            if (!in_array($file_ext,$allowed_exts)) {
                if (File::exists($new_file['file_title'])) {
                    File::delete($new_file['file_title']);
                }

                return Response::json(RestController::generateJsonResponse(
                    true,
                    trans('ws_file_controller.file.extNotAllowed'),
                    ["attribute" => "\n Affected file: ".$filename]
                ), 400);
            } else {
                
                // Check MIME Type
                $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
                $file_info=finfo_file($finfo, $upload_file->getRealPath());
                finfo_close($finfo);
                $allowed_types=config('app.allow_file_content');
                if (!in_array($file_info,$allowed_types[$file_ext])) {
                    if (File::exists($new_file['file_title'])) {
                    File::delete($new_file['file_title']);
                    }

                    return Response::json(RestController::generateJsonResponse(
                        true,
                        trans('ws_file_controller.file.typeNotAllowed'),
                        ["attribute" => "\n Affected file: ".$filename]
                    ), 400);
                }
            }
            // file_put_contents('/home/www/logs/upload.log', date('Ymd H:i:s').': New file: '.var_export($new_file,true)."\n",FILE_APPEND); 
            
            if (isset($new_file['file_thumb'])) {
                $thumb_ext=strtolower(pathinfo($new_file['file_thumb']->getClientOriginalName(), PATHINFO_EXTENSION));
                if (!in_array($thumb_ext,config('app.allow_img_ext'))) {
                    if (File::exists($new_file['file_title'])) {
                        File::delete($new_file['file_title']);
                    }

                    return Response::json(RestController::generateJsonResponse(
                        true,
                        trans('ws_file_controller.file.thumbExtNotAllowed'),
                        ["attribute" => "\n Affected file: ".$new_file['file_thumb']->getClientOriginalName()]
                    ), 400);
                } else {
                    // Check MIME Type
                    $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
                    $file_info=finfo_file($finfo, $new_file['file_thumb']->getRealPath());
                    finfo_close($finfo);
                    $allowed_types=config('app.allow_file_content');
                    if (!in_array($file_info,$allowed_types[$thumb_ext])) {
                        if (File::exists($new_file['file_title'])) {
                        File::delete($new_file['file_title']);
                        }

                        return Response::json(RestController::generateJsonResponse(
                            true,
                            trans('ws_file_controller.file.thumbTypeNotAllowed'),
                            ["attribute" => "\n Affected file: ".$new_file['file_thumb']->getClientOriginalName()]
                        ), 400);
                    }
                }
                // file_put_contents('/home/www/logs/upload.log', 'Uploaded thumb file: '.var_export($new_file['file_thumb']->getRealPath(),true)."\n",FILE_APPEND); 
            }
            
            if (isset($new_file['file_zoom'])) {
                $zoom_ext=strtolower(pathinfo($new_file['file_zoom']->getClientOriginalName(), PATHINFO_EXTENSION));
                if (!in_array($zoom_ext,config('app.allow_img_ext'))) {
                    if (File::exists($new_file['file_title'])) {
                        File::delete($new_file['file_title']);
                    }

                    return Response::json(RestController::generateJsonResponse(
                        true,
                        trans('ws_file_controller.file.zoomExtNotAllowed'),
                        ["attribute" => "\n Affected file: ".$new_file['file_zoom']->getClientOriginalName()]
                    ), 400);
                } else {
                    // Check MIME Type
                    $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
                    $file_info=finfo_file($finfo, $new_file['file_zoom']->getRealPath());
                    finfo_close($finfo);
                    $allowed_types=config('app.allow_file_content');
                    if (!in_array($file_info,$allowed_types[$zoom_ext])) {
                        if (File::exists($new_file['file_title'])) {
                        File::delete($new_file['file_title']);
                        }

                        return Response::json(RestController::generateJsonResponse(
                            true,
                            trans('ws_file_controller.file.zoomTypeNotAllowed'),
                            ["attribute" => "\n Affected file: ".$new_file['file_zoom']->getClientOriginalName()]
                        ), 400);
                    }
                }
                // file_put_contents('/home/www/logs/upload.log', 'Uploaded zoom file: '.var_export($new_file['file_zoom']->getRealPath(),true)."\n",FILE_APPEND); 
            }
            
            
            // file_put_contents('/home/www/logs/upload.log', 'Uploaded file: '.var_export($upload_file->getRealPath(),true)."\n",FILE_APPEND); 
            $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
            $file_info=finfo_file($finfo, $upload_file->getRealPath());
            // file_put_contents('/home/www/logs/upload.log', 'MiME Type: '.var_export($file_info,true)."\n",FILE_APPEND); 
            finfo_close($finfo);
            
            if ($fileMatchCount == 0) {
                $uploadSuccess = $upload_file->move($destinationPath, $filename);
                $fileItem = new FileItem();

                $fileItem->fill($new_file);


                if ($isProtected) {
                    $fileItem->protected = 1;
                }
                $fileItem->type = $filemanager_mode;
                unset($fileItem->mode);
                unset($fileItem->currentpath);
                unset($fileItem->file);
                unset($fileItem->file_title);
                $fileItem->filename = $currentpath . $filename;
                //$fileItem->size = $upload_file->getSize();
                $fileItem->size = @filesize($destinationPath . $filename); //for local test
                $fileItem->extension = $upload_file->getClientOriginalExtension();

                if (isset($new_file['file_thumb'])) {
                    $uploadSuccess = $upload_file_thumb->move($destinationPath_thumb, $filename_thumb);

                    $fileItem->thumbnail = $currentpath . self::FOLDER_NAME_THUMBNAIL . '/' . $filename_thumb;
                }

                if (isset($new_file['file_zoom'])) {
                    $uploadSuccess = $upload_file_zoom->move($destinationPath_zoom, $filename_zoom);

                    $fileItem->zoompic = $currentpath . self::FOLDER_NAME_ZOOMPIC . '/' . $filename_zoom;
                }

                unset($fileItem->file_thumb);
                unset($fileItem->file_zoom);
                unset($fileItem->file_thumb_title, $fileItem->file_zoom_title);
                unset($fileItem->description, $fileItem->title, $fileItem->lang_info);

                //Create FileInfo for all laguages!
                if (isset($new_file['title'])) {
                    $fileInfoTitle = $new_file['title'];
                } else {
                    $fileInfoTitle = array(LANG_EN => $filename, LANG_DE => $filename);
                }

                if (isset($new_file['description'])) {
                    $fileInfoDescription = $new_file['description'];
                } else {
                    $fileInfoDescription = array(LANG_EN => "", LANG_DE => "");
                }

                $fileInfo = [];
                // DE
                $fileInfo[0] = new FileInfo();
                $fileInfo[0]->title = $fileInfoTitle[LANG_DE];
                $fileInfo[0]->description = $fileInfoDescription[LANG_DE];
                $fileInfo[0]->lang = LANG_DE;

                $fileInfo[1] = new FileInfo();
                $fileInfo[1]->title = $fileInfoTitle[LANG_EN];
                $fileInfo[1]->description = $fileInfoDescription[LANG_EN];
                $fileInfo[1]->lang = LANG_EN;

                //Try to recognize file parameters
                FileRecognizerHelper::recoginizeFields($fileItem);

                $response = $this->trySave($fileItem, $fileInfo, false);

                if (!$response->isSuccessful()) {
                    //TODO: Clean the files
                }

                return $response;
            } else {
                if (File::exists($new_file['file_title'])) {
                    File::delete($new_file['file_title']);
                }

                return Response::json(RestController::generateJsonResponse(
                    true,
                    trans('ws_file_controller.file.exist'),
                    ["attribute" => $filename]
                ), 400);
            }
        } else {
            // Get messages
            $messages = $validator->messages();
            if (File::exists($new_file['file_title'])) {
                File::delete($new_file['file_title']);
            }

            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_general_controller.validation.failed'),
                $messages
            ), 400);
        }
    }

    public function postUpload()
    {
        if (!Request::ajax()) {
            return;
        }
        $currentpath = Request::get('currentpath');

        if (!preg_match(config('app.PATH_NAME_PATTERN'), $currentpath)) {
            /* $currentpath contains at least one illegal character. */
            return Response::json(
                RestController::generateJsonResponse(
                    true,
                    trans('ws_general_controller.illegalChar.parameter', ["attribute" => "currentpath"])
                ),
                400
            );
        }
        $filemanager_mode = Request::get('mode');
        if (!in_array($filemanager_mode, $this->fm_mode_list) || !UserAccessHelper::hasAccess($filemanager_mode)) {
            return Response::json(
                RestController::generateJsonResponse(true, trans('ws_file_controller.filemanager.wrongMode')),
                400
            );
        }

        if (strpos($currentpath, "/" . UserAccessHelper::getUserRootFolder() . "/") === false && UserAccessHelper::getUserRootFolder() != "all") {
            return Response::json(RestController::generateJsonResponse(
                true,
                'You don\'t have access to create files in this folder!'
            ), 403);
        }
        $this->rules['file'] = $this->file_rules[$filemanager_mode];
        $filename = Request::get('file');
        $fileMatchCount = FileItem::where('type', '=', $filemanager_mode)->where('deleted', 0)->where(
            DB::raw('filename COLLATE utf8_bin'),
            '=',
            $currentpath . $filename
        )->count();
        if ($fileMatchCount > 0) {
            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_file_controller.file.exist'),
                ["attribute" => $filename]
            ), 400);
        }
        foreach (config('app.jupload') as $key => $val) {
            $options[$key] = $val;
        }
        $upload_handler = new UploadHandler($options);
//       $result = $upload_handler->post($mode);
//       return $result;
    }

    public function postPdfupload()
    {
        if (!Request::ajax()) {
            return;
        }
        $u_id = Request::get('u_id');
        $file = Request::file('file');
        $page = Page::alive()->where('u_id', $u_id)->first();
        if ($file && $page) {
            $destPath = config('app.PDF_path.uploaded') . trans('template.currLocale') . '/';
            if (!is_dir($destPath)) {
                mkdir($destPath, FILE_DEFAULT_ACCESS, true);
            }
            try {
                $file->move($destPath, $page->slug . '.pdf');
                
                return Response::json(RestController::generateJsonResponse(false, 'PDF File was saved to: ' . trans('template.currLocale') . '/' . $page->slug . '.pdf'), 200);
            } catch (\Exception $e) {
                Log::error('Unable to move file ' . $page->slug . '.pdf' . ' Error: ' . $e->getMessage());

                return Response::json(RestController::generateJsonResponse(true, trans('ws_file_controller.file.notsave')), 400);
            }
        } else {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_file_controller.file.notsave')), 400);
        }
    }

    
    protected function purifyFileData($file_data) {
        if (isset($file_data['title']) && is_array($file_data['title'])) {
            foreach($file_data['title'] as $key=>$value) {
                $file_data['title'][$key]=\Purifier::clean($file_data['title'][$key]);
            }
        }
        if (isset($file_data['description']) && is_array($file_data['description'])) {
            foreach($file_data['description'] as $key=>$value) {
                $file_data['description'][$key]=\Purifier::clean($file_data['description'][$key]);
            }
        }
        
        if (isset($file_data['dimensions'])) {
            $file_data['dimensions']=\Purifier::clean($file_data['dimensions']);
        }
        
        if (isset($file_data['resolution'])) {
            $file_data['resolution']=\Purifier::clean($file_data['resolution']);
        }
        
        if (isset($file_data['lang'])) {
            $file_data['lang']=\Purifier::clean($file_data['lang']);
        }
        
        if (isset($file_data['pages'])) {
            $file_data['pages']=\Purifier::clean($file_data['pages']);
        }
        
        if (isset($file_data['version'])) {
            $file_data['version']=\Purifier::clean($file_data['version']);
        }
        
        if (isset($file_data['copyright_notes'])) {
            $file_data['copyright_notes']=\Purifier::clean($file_data['copyright_notes']);
        }
        
        if (isset($file_data['usage_terms_de'])) {
            $file_data['usage_terms_de']=\Purifier::clean($file_data['usage_terms_de']);
        }
        
        if (isset($file_data['usage_terms_en'])) {
            $file_data['usage_terms_en']=\Purifier::clean($file_data['usage_terms_en']);
        }
        
        return $file_data;
    }
    
    /**
     * Return Update File from JSON
     * URL: files/updatefile/{$id}
     * METHOD: POST
     */
    public function postUpdatefile($id = 0)
    {

        $update_file = Request::all();
         // file_put_contents('/home/www/logs/purify.log', 'Update file before purify: '.var_export($update_file,true)."\n",FILE_APPEND);
        $update_file = $this->purifyFileData($update_file);
         // file_put_contents('/home/www/logs/purify.log', 'Update file after purify: '.var_export($update_file,true)."\n",FILE_APPEND);
        //Validate
        if ($id <= 0) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        $fileItem = FileItem::find($id);
        if (is_null($fileItem)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_file_controller.file.notFound')), 404);
        }
        // Validate filemanager Mode
        $filemanager_mode = strtolower($fileItem->type);
        if (!in_array($filemanager_mode, $this->fm_mode_list) || !UserAccessHelper::hasAccess($filemanager_mode)) {
            return Response::json(
                RestController::generateJsonResponse(true, trans('ws_file_controller.filemanager.wrongMode')),
                400
            );
        }

        if (self::isFileInEditMode($fileItem)) {
            return Response::json(RestController::generateJsonResponse(
                true,
                trans(
                    'ws_general_controller.file.edited',
                    ['type' => ($fileItem->type == "Images" ? 'image' : 'file')]
                )
            ), 400);
        }
        $fileInfo = FileInfo::where('file_id', '=', $id)->get();
        if (is_null($fileInfo)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_file_controller.fileInfo.notFound')), 404);
        }

        if (strpos(FileHelper::safe_dirname($fileItem->filename) . "/", "/" . UserAccessHelper::getUserRootFolder() . "/") === false && UserAccessHelper::getUserRootFolder() != "all") {
            return Response::json(RestController::generateJsonResponse(
                true,
                'You don\'t have access to update files in this folder!'
            ), 404);
        }


        /*
         * Validate inputs
         */
        $validator = Validator::make($update_file, $this->rules);
        //Validate the inputs. if file is uploaded and size of files.
        if ($validator->passes()) {
            if (isset($update_file['protected'])) {
                //file_put_contents('/srv/www/dbdn-l55/storage/pdf/protected.log', var_export($update_file,true)."\n",FILE_APPEND);
                /*
                DB::listen(function ($query) {
                    file_put_contents('/srv/www/dbdn-l55/storage/pdf/protected.log',var_export($query->sql,true)."\n",FILE_APPEND);
                    file_put_contents('/srv/www/dbdn-l55/storage/pdf/protected.log',var_export($query->bindings,true)."\n",FILE_APPEND);
                }); 
                */
                $accesPages = DB::table('page')
                    ->join('download', 'page.u_id', '=', 'download.page_u_id')
                    ->leftJoin('pagegroup', 'page.id', '=', 'pagegroup.page_id')
                    ->where('download.file_id', '=', $id)
                    ->whereNull('download.deleted_at')
                    ->whereNull('page.deleted_at')
                    ->where(function ($query) {
                        $query->whereNull('pagegroup.page_id')
                            ->orWhereNotExists(function ($query) {
                                $query->select(DB::raw(('page_id')))->from('pagegroup')
                                    ->whereRaw('page.id = pagegroup.page_id')
                                    ->whereNull('pagegroup.deleted_at');
                            });
                    })->get();
                    
                if (count($accesPages) > 0) {
                    return Response::json(RestController::generateJsonResponse(
                        true,
                        trans('ws_file_controller.fileInfo.noAccessAdded'),
                        ['protected' => '']
                    ), 404);
                }
            }
            if (isset($update_file['thumb_visible']) && isset($update_file['protected'])) {
                $update_file['thumb_visible'] = 1;
            } else {
                $update_file['thumb_visible'] = 0;
            }
            $currentpath = FileHelper::safe_dirname($fileItem->filename) . "/";
            $destinationPath = FileController::getFullPath($filemanager_mode, $currentpath, $fileItem->protected);
            $files = Request::file('files');
            if ($files) {
                if (isset($update_file['file_zoom_title']) && is_numeric($update_file['file_zoom_title'])) {
                    $update_file['file_zoom'] = $files[$update_file['file_zoom_title']];
                }
                if (isset($update_file['file_thumb_title']) && is_numeric($update_file['file_thumb_title'])) {
                    $update_file['file_thumb'] = $files[$update_file['file_thumb_title']];
                }
                unset($update_file['files']);
            }
            if (isset($update_file['file_thumb'])) {
                $upload_file_thumb = $update_file['file_thumb'];
                $destinationPath_thumb = $destinationPath . self::FOLDER_NAME_THUMBNAIL;
                $filename_thumb = $upload_file_thumb->getClientOriginalName();
                
                
                $thumb_ext=strtolower(pathinfo($update_file['file_thumb']->getClientOriginalName(), PATHINFO_EXTENSION));
                if (!in_array($thumb_ext,config('app.allow_img_ext'))) {
                    return Response::json(RestController::generateJsonResponse(
                        true,
                        trans('ws_file_controller.file.thumbExtNotAllowed'),
                        ["attribute" => "\n Affected file: ".$update_file['file_thumb']->getClientOriginalName()]
                    ), 400);
                } else {
                    // Check MIME Type
                    $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
                    $file_info=finfo_file($finfo, $update_file['file_thumb']->getRealPath());
                    finfo_close($finfo);
                    $allowed_types=config('app.allow_file_content');
                    if (!in_array($file_info,$allowed_types[$thumb_ext])) {

                        return Response::json(RestController::generateJsonResponse(
                            true,
                            trans('ws_file_controller.file.thumbTypeNotAllowed'),
                            ["attribute" => "\n Affected file: ".$update_file['file_thumb']->getClientOriginalName()]
                        ), 400);
                    }
                }
                
                

                // Validate $filename_thumb
                if (!preg_match(config('app.FILE_NAME_PATTERN'), $filename_thumb)) {
                    /* $filename contains at least one illegal character. */
                    return Response::json(RestController::generateJsonResponse(
                        true,
                        trans(
                            'ws_general_controller.illegalChar.parameter',
                            ["attribute" => "file_thumb"]
                        )
                    ), 400);
                }
            }
            //if has uploadde zoompic populate info
            if (isset($update_file['file_zoom'])) {
                $upload_file_zoom = $update_file['file_zoom'];
                $destinationPath_zoom = $destinationPath . self::FOLDER_NAME_ZOOMPIC;
                $filename_zoom = $upload_file_zoom->getClientOriginalName();
                
                $zoom_ext=strtolower(pathinfo($update_file['file_zoom']->getClientOriginalName(), PATHINFO_EXTENSION));
                if (!in_array($zoom_ext,config('app.allow_img_ext'))) {
                    return Response::json(RestController::generateJsonResponse(
                        true,
                        trans('ws_file_controller.file.zoomExtNotAllowed'),
                        ["attribute" => "\n Affected file: ".$update_file['file_zoom']->getClientOriginalName()]
                    ), 400);
                } else {
                    // Check MIME Type
                    $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
                    $file_info=finfo_file($finfo, $update_file['file_zoom']->getRealPath());
                    finfo_close($finfo);
                    $allowed_types=config('app.allow_file_content');
                    if (!in_array($file_info,$allowed_types[$zoom_ext])) {

                        return Response::json(RestController::generateJsonResponse(
                            true,
                            trans('ws_file_controller.file.zoomTypeNotAllowed'),
                            ["attribute" => "\n Affected file: ".$update_file['file_zoom']->getClientOriginalName()]
                        ), 400);
                    }
                }

                // Validate $filename_thumb
                if (!preg_match(config('app.FILE_NAME_PATTERN'), $filename_zoom)) {
                    return Response::json(RestController::generateJsonResponse(
                        true,
                        trans('ws_general_controller.illegalChar.parameter', ["attribute" => "file_zoom"])
                    ), 400);
                }
            }

            // Update file properties
            //if has uploadde thumb populate info
            if (isset($update_file['file_thumb'])) {
                try {
                    $upload_file_thumb->move($destinationPath_thumb, $filename_thumb);
                } catch (\Exception $e) {
                    Log::error('Unable to move file ' . $filename_thumb . ' Error: ' . $e->getMessage());

                    return Response::json(
                        RestController::generateJsonResponse(true, trans('ws_file_controller.file.notsave')),
                        400
                    );
                }
                $oldThumbnailFile = $fileItem->thumbnail;
                $fileItem->thumbnail = $currentpath . self::FOLDER_NAME_THUMBNAIL . '/' . $filename_thumb;
                // Delete old thumb file if new dont overwrite it.
                if (strpos($oldThumbnailFile, self::FOLDER_NAME_THUMBNAIL) !== false && $oldThumbnailFile != $fileItem->thumbnail) {
                    $thumb_del_path = $this->getFullPath($filemanager_mode, $oldThumbnailFile, $fileItem->protected);
                }
            } elseif ($update_file['file_thumb_title'] == "" && $fileItem->thumbnail != "") {
                $oldThumbnailFile = $fileItem->thumbnail;
                if (strpos($oldThumbnailFile, self::FOLDER_NAME_THUMBNAIL) !== false && $oldThumbnailFile != $update_file['file_thumb_title']) {
                    $thumb_del_path = $this->getFullPath($filemanager_mode, $fileItem->thumbnail, $fileItem->protected);
                }
                $fileItem->thumbnail = "";
            }

            //if has uploadde zoompic populate info
            if (isset($update_file['file_zoom'])) {
                try {
                    $upload_file_zoom->move($destinationPath_zoom, $filename_zoom);
                } catch (\Exception $e) {
                    Log::error('Unable to move file ' . $filename_zoom . ' Error: ' . $e->getMessage());

                    return Response::json(
                        RestController::generateJsonResponse(true, trans('ws_file_controller.file.notsave')),
                        400
                    );
                }
                $oldZoompicFile = $fileItem->zoompic;
                $fileItem->zoompic = $currentpath . self::FOLDER_NAME_ZOOMPIC . '/' . $filename_zoom;
                // Delete old zoom file if new dont overwrite it.
                if (strpos($oldZoompicFile, self::FOLDER_NAME_ZOOMPIC) !== false && $oldZoompicFile != $fileItem->zoompic) {
                    $zoom_del_path = $this->getFullPath($filemanager_mode, $oldZoompicFile, $fileItem->protected);
                }
            } elseif ($update_file['file_zoom_title'] == "" && $fileItem->zoompic != "") {
                $oldZoompicFile = $fileItem->zoompic;

                if (strpos($oldZoompicFile, self::FOLDER_NAME_ZOOMPIC) !== false && $oldZoompicFile != $update_file['file_zoom_title']) {
                    $zoom_del_path = $this->getFullPath($filemanager_mode, $fileItem->zoompic, $fileItem->protected);
                }
                $fileItem->zoompic = "";
            }

            //Create FileInfo for all laguages!
            $fileInfoDescription = $update_file['description'];
            if (isset($update_file['title'])) {
                $fileInfoTitle = $update_file['title'];
            } else {
                $filename = basename($fileItem->filename);
                $fileInfoTitle = [LANG_EN => $filename, LANG_DE => $filename];
            }

            // DE or EN. Get The lang ID from the fileInfo item
            $lang_id = $fileInfo[0]->lang;
            $fileInfo[0]->title = $fileInfoTitle[$lang_id];
            $fileInfo[0]->description = $fileInfoDescription[$lang_id];

            // EN
            $lang_id = $fileInfo[1]->lang;
            $fileInfo[1]->title = $fileInfoTitle[$lang_id];
            $fileInfo[1]->description = $fileInfoDescription[$lang_id];

            //change protection level
            $isProtectedOldStatus = $fileItem->protected;
            $fileItem->protected = isset($update_file['protected']);
            if ($filemanager_mode == FileController::MODE_DOWNLOADS) {
                //$isProtectedOldStatus = $fileItem->protected;
                // file_put_contents('/srv/www/dbdn-l55/storage/pdf/filesave.log', "Setting protection: ".var_export($update_file,true)."\n",FILE_APPEND);
//                $fileItem->protected = isset($update_file['protected']); // && $update_file['protected'];
//                $fileItem->format = isset($update_file['format'])?$update_file['format']:'';
//                $fileItem->dimensions = isset($update_file['dimensions'])?$update_file['dimensions']:'';
//                $fileItem->dimension_units = isset($update_file['dimension_units'])?$update_file['dimension_units']:'';
//                $fileItem->resolution = isset($update_file['resolution'])?$update_file['resolution']:'';
//                $fileItem->resolution_units = isset($update_file['resolution_units'])?$update_file['resolution_units']:'';
//                $fileItem->dimensions = isset($update_file['dimensions'])?$update_file['dimensions']:'';
                unset($update_file['protected']);
                unset($update_file['description']);
                unset($update_file['title']);
                unset($update_file['file_title']);
                unset($update_file['file_zoom']);
                unset($update_file['file_zoom_title']);
                unset($update_file['file_thumb']);
                unset($update_file['file_thumb_title']);
                unset($update_file['currentpath']);
                //unset($update_file['usage_terms_en']);
                //unset($update_file['usage_terms_de']);
                //unset($update_file['copyright_notes']);
                unset($update_file['mode']);
                unset($update_file['file']);
                unset($update_file['zoompic']);
                unset($update_file['thumbnail']);
                $fileItem->fill($update_file);
            }

            try {
                DB::beginTransaction();
                $response = $this->trySave($fileItem, $fileInfo, true);

                if ($response->isSuccessful()) {
                    if (isset($thumb_del_path)) {
                        if (!@unlink($thumb_del_path)) {
                            Log::error('Unable to delete file ' . $thumb_del_path);
                        }
                    }
                    if (isset($zoom_del_path)) {
                        if (!@unlink($zoom_del_path)) {
                            Log::error('Unable to delete file ' . $zoom_del_path);
                        }
                    }

                    if ($isProtectedOldStatus != $fileItem->protected) {
                        FileHelper::moveProtectedFile($fileItem->protected, $fileItem, $filemanager_mode);
                    }
                }
                DB::commit();
            } catch (ModelValidationException $mve) {
                DB::rollback();
                throw $mve;
            } catch (\Exception $e) {
                DB::rollback();
                Log::error('Unable to save file: ' . $e->getMessage());
                try {
                    if ($isProtectedOldStatus != $fileItem->protected) {
                        FileHelper::moveProtectedFile($isProtectedOldStatus, $fileItem, $filemanager_mode);
                    }
                } catch (\Exception $ex) {
                    Log::error('Unable to move file ' . $ex->getMessage());
                }

                $response = Response::json(RestController::generateJsonResponse(true, $e->getMessage(), $fileItem), 400);
            }

            return $response;
        } else {
            // Get messages
            $messages = $validator->messages();

            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_general_controller.validation.failed'),
                $messages
            ), 400);
        }
    }

    private static function foldersize($path, $extraExcludeList = null)
    {
        $total_size = 0;
        $files = @scandir($path);
        if ($files) {
            $cleanPath = rtrim($path, '/') . '/';
            $excludeList = ['.', '..'];
            if ($extraExcludeList) {
                $excludeList = array_merge($excludeList, $extraExcludeList);
            }
            //Log::debug('Exclude list: ', $excludeList);
            foreach ($files as $t) {
                if (!in_array($t, $excludeList)) {
                    $currentFile = $cleanPath . $t;
                    if (is_dir($currentFile)) {
                        //Log::debug('Folder: ', [$t]);
                        $size = self::foldersize($currentFile);
                        $total_size += $size;
                    } else {
                        $size = filesize($currentFile);
                        $total_size += $size;
                    }
                }
            }

            return $total_size;
        } else {
            Log::error('Cannot open dir: ' . $path . ' ' . ' error: ' . self::getLastErrorMsg());

            return -1;
        }
    }

    /*
     * Return the brand configuraion (Brand constants ID, folder, thumbnail etc.) from file path.
     * possible file paths: folder2/subFolder1/Clipboard01.jpg; DTSS/subFolder1/Clipboard01.jpg; smart/Clipboard01.jpg
     */
    private static function getBrnadOfFile($filePaht)
    {
        $separator = '/'; // DIRECTORY_SEPARATOR
        $folderArray = explode($separator, $filePaht, 3);
        //Log::debug('Split by ' . $separator, $folderArray);
        $firstFolder = $folderArray[0];
        if (empty($firstFolder) && count($folderArray) >= 2) {
            $firstFolder = $folderArray[1];
        }
        $specialPages = config('app.pages');
        //Log::debug('Pages: ' , $specialPages);
        foreach ($specialPages as $key => $value) {
            if ($value->folder == $firstFolder && $value->folder != "all") {
                return $value;
            }
        }

        return $specialPages[DAIMLER];
    }

    private function hasRecycleQuota($filemanager_mode, $fileForDelete, $fileForDeleteFullPath)
    {
        // 1. Detect the Brand
        $brand = self::getBrnadOfFile($fileForDelete);
        $brandRootPageID = $brand->ID;
        if ("all" == $brand->folder) {
            $brandRootFolder = '/';
        } else {
            $brandRootFolder = '/' . $brand->folder;
        }
        // 2. Get Recycle Status
        $brandRootFolderFullPath = self::getFullPath($filemanager_mode, $brandRootFolder, false, true);
        $objRecycleStatus = new \stdClass();
        self::prepareRecycleStatus($objRecycleStatus, $brandRootFolderFullPath, $brandRootPageID, $brandRootFolder, false);
        //Log::debug('Quota Status: ' . print_r($objRecycleStatus, 1));

        // Check if there is free space for the file.
        return ($objRecycleStatus->brand_free_space_bytes > filesize($fileForDeleteFullPath));
    }

    private function moveDeletedFile(
        $fileItem,
        $filemanager_mode,
        $isDeleted,
        $isRestore = false,
        $rollbackFile = null,
        $checkExistance = true
    ) {
        $currentpath = $fileItem->filename;
        $currentFile = FileController::getFullPath($filemanager_mode, $currentpath, $fileItem->protected, !$isDeleted);
        if ($rollbackFile) {
            $deletedFile = FileController::getFullPath(
                $filemanager_mode,
                $rollbackFile->filename,
                $rollbackFile->protected,
                $isDeleted
            );
        } else {
            $deletedFile = FileController::getFullPath($filemanager_mode, $currentpath, $fileItem->protected, $isDeleted);
        }
        $currentZoompic = "";
        $deletedZoomFile = "";
        $isFileExists = File::exists($currentFile);
        if (!$checkExistance || $isFileExists) {
            if (!$rollbackFile) {
                $deletedFolder = substr($deletedFile, 0, strrpos($deletedFile, '/'));
                if (!File::exists($deletedFolder)) {
                    if ($filemanager_mode == FileController::MODE_DOWNLOADS && !$isDeleted) {
                        $full_path_protected = FileController::getFullPath(
                            $filemanager_mode,
                            substr($currentpath, 0, strrpos($currentpath, '/')),
                            true
                        );
                        $fileFolder = FileController::getFullPath(
                            $filemanager_mode,
                            substr($currentpath, 0, strrpos($currentpath, '/')),
                            false
                        );
                        //this mean we have protected folder in $deletedFolder
                        if ($deletedFolder != $fileFolder) {
                            //if normal folder exist we create only protected one
                            if (!File::exists($fileFolder)) {
                                $deletedFolder = $fileFolder;
                            } else {
                                //if normal folder exist we create only protected one
                                $full_path_protected = null;
                            }
                        } else {
                            if (File::exists($full_path_protected)) {
                                $full_path_protected = null;
                            }
                        }
                    }
                    $this->createFileFolderStructure($deletedFolder, isset($full_path_protected) ? $full_path_protected : null);
                }

                if (!$isRestore && $isFileExists) {
                    if (!self::hasRecycleQuota($filemanager_mode, $currentpath, $currentFile)) {
                        // Recycle Bin Quota exceed!
                        throw new \Exception("Recycle Bin quota exceeds for file: " . $currentFile, RECYCLE_BIN_EXCEED_EXCEPTION);
                    }
                }
                //Check is file name is duplicated
                $deletedFile = $this->checkNameAvailability($deletedFile, $fileItem, 'file');
            }
            if (!$isFileExists || File::move($currentFile, $deletedFile)) {
                if ($fileItem->zoompic != "") {
                    $currentZoompic = FileController::getFullPath(
                        $filemanager_mode,
                        $fileItem->zoompic,
                        $fileItem->protected,
                        !$isDeleted
                    );
                    $isZoomPicExists = File::exists($currentZoompic);
                    if (!$checkExistance || $isZoomPicExists) {
                        if ($rollbackFile) {
                            $deletedZoomFile = FileController::getFullPath(
                                $filemanager_mode,
                                $rollbackFile->zoompic,
                                $rollbackFile->protected,
                                $isDeleted
                            );
                        } else {
                            $deletedZoomFile = FileController::getFullPath(
                                $filemanager_mode,
                                $fileItem->zoompic,
                                $fileItem->protected,
                                $isDeleted
                            );
                            //Check is file name is duplicated
                            $deletedZoomFile = $this->checkNameAvailability($deletedZoomFile, $fileItem, 'zoompic');
                        }
                        if (!File::exists($deletedFolder . '/' . self::FOLDER_NAME_ZOOMPIC)) {
                            if (!mkdir($deletedFolder . '/' . self::FOLDER_NAME_ZOOMPIC, FILE_DEFAULT_ACCESS, true)) {
                                throw new \Exception("Zoom folder doesn't exist and cannot be created");
                            }
                        }
                        try {
                            if ($isZoomPicExists && !File::move($currentZoompic, $deletedZoomFile)) {
                                throw new \Exception("Zoom picture couldn't move to folder");
                            }
                        } catch (\Exception $e) {
                            if (!File::move($deletedFile, $currentFile)) {
                                throw new \Exception("Zoom picture couldn't move to folder and File couldn't be move back to original folder");
                            }
                            throw new \Exception($e);
                        }
                    }
                }
                if ($fileItem->thumbnail != "") {
                    $currentThumbnail = FileController::getFullPath(
                        $filemanager_mode,
                        $fileItem->thumbnail,
                        $fileItem->protected,
                        !$isDeleted
                    );
                    $isThumbnailExists = File::exists($currentThumbnail);
                    if (!$checkExistance || $isThumbnailExists) {
                        if ($rollbackFile) {
                            $deletedThumbnailFile = FileController::getFullPath(
                                $filemanager_mode,
                                $rollbackFile->thumbnail,
                                $rollbackFile->protected,
                                $isDeleted
                            );
                        } else {
                            $deletedThumbnailFile = FileController::getFullPath(
                                $filemanager_mode,
                                $fileItem->thumbnail,
                                $fileItem->protected,
                                $isDeleted
                            );
                            $deletedThumbnailFile = $this->checkNameAvailability($deletedThumbnailFile, $fileItem, 'thumbnail');
                        }
                        if (!File::exists($deletedFolder . '/' . self::FOLDER_NAME_THUMBNAIL)) {
                            if (!mkdir($deletedFolder . '/' . self::FOLDER_NAME_THUMBNAIL, FILE_DEFAULT_ACCESS, true)) {
                                throw new \Exception("Thumbnail folder doesn't exist and cannot be created");
                            }
                        }
                        try {
                            if ($isThumbnailExists && !File::move($currentThumbnail, $deletedThumbnailFile)) {
                                throw new \Exception("Thumbnail couldn't move to folder");
                            }
                        } catch (\Exception $e) {
                            if (!File::move($deletedFile, $currentFile)) {
                                throw new \Exception("Thumbnail couldn't move to deleted folder and File couldn't be move back to original folder");
                            }
                            if ($deletedZoomFile != "") {
                                if (!File::move($deletedZoomFile, $currentZoompic)) {
                                    throw new \Exception("Thumbnail couldn't move to deleted folder and Zoompic couldn't be move back to original folder");
                                }
                            }
                            throw new \Exception($e);
                        }
                    }
                }
            } else {
                throw new \Exception("File couldn't be move to folder");
            }
        } else {
            throw new \Exception("File doesn't exists: Path " . $currentFile);
        }
    }

    private function checkNameAvailability($path, $fileItem, $type)
    {
        $newPath = $path;
        $folder = substr($newPath, 0, strrpos($newPath, '/'));
        $extension = File::extension($newPath);
        $fileName = substr($newPath, strrpos($newPath, '/') + 1, (strrpos($newPath, $extension) - strrpos($newPath, '/') - 2));
        $pattern = "_";
        $index = 0;
        $newName = $fileName;

        while (File::exists($newPath)) {
            $index++;
            $newName = $fileName . $pattern . $index;
            $newPath = $folder . '/' . $newName . '.' . $extension;
        }
        if ($fileName != $newName) {
            switch ($type) {
                case 'file':
                    $fileItem->filename = substr_replace(
                        $fileItem->filename,
                        $newName,
                        strrpos($fileItem->filename, '/') + 1,
                        strlen($fileName)
                    );
                    break;
                case 'zoompic':
                    $fileItem->zoompic = substr_replace(
                        $fileItem->zoompic,
                        $newName,
                        strrpos($fileItem->zoompic, '/') + 1,
                        strlen($fileName)
                    );
                    break;
                case 'thumbnail':
                    $fileItem->thumbnail = substr_replace(
                        $fileItem->thumbnail,
                        $newName,
                        strrpos($fileItem->thumbnail, '/') + 1,
                        strlen($fileName)
                    );
                    break;
            }
        }

        return $newPath;
    }

    /**
     * Return save result
     */
    protected function trySave(FileItem $file, $fileInfo, $isUpdate)
    {
        try {
            // Start transaction!
            DB::beginTransaction();

            if ($isUpdate) {
                $message = "File was updated.";
            } else {
                $message = "File was added.";
            }

            $file->save();

            foreach ($fileInfo as $info) {
                $info->file_id = $file->id;
                $info->save();
            }
            //Success !!!
            // Commit the queries!
            DB::commit();

            $result = Response::json(RestController::generateJsonResponse(false, $message, $file), 201);
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            DB::rollback();

            //unlink(public_path().'/public/files/'.$file->filename);

            throw new ModelValidationException($e->getErrors());
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        return $result;
    }

    /**
     * Return Delete file
     * URL: files/deleteFile/{ID}
     * METHOD: DELETE
     */
    public function deleteDeletefile($id = null, $isPermanent = false, $skipRelationCheck = false)
    {
        // Get param: skipRelationCheck if not set!
        $skipRelationCheck = ($skipRelationCheck) ? $skipRelationCheck : json_decode(Request::get('skipRelationCheck', false));

        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }

        if ($isPermanent) {
            $file = FileItem::withTrashed()->find($id);
        } else {
            $file = FileItem::find($id);
        }

        if (is_null($file)) {
            return Response::json(
                RestController::generateJsonResponse(true, trans('ws_file_controller.file.notFound'), $file),
                404
            ); // in response $file not exists
        }

        if (self::isFileInEditMode($file)) {
            return Response::json(RestController::generateJsonResponse(
                true,
                trans(
                    'ws_general_controller.file.edited',
                    ['type' => ($file->type == "Images" ? 'image' : 'file')]
                )
            ), 400);
        }

        if (strpos($file->filename, "/" . UserAccessHelper::getUserRootFolder() . "/") === false && UserAccessHelper::getUserRootFolder() != "all") {
            return Response::json(RestController::generateJsonResponse(true, 'You don\'t have access to this folder!'), 404);
        }

        Log::debug(($isPermanent) ? 'Permanent' : '' . 'Delete file: ' . $file->id . ':' . $file->filename);

        $deletedfile = $file;
        $filemanager_mode = strtolower($deletedfile->type);

        if (!in_array($filemanager_mode, $this->fm_mode_list)) {
            return Response::json(
                RestController::generateJsonResponse(true, trans('ws_file_controller.filemanager.wrongMode')),
                400
            );
        }

        // Check if file is in use
        $count = 0;
        if ($filemanager_mode === self::MODE_DOWNLOADS) {
            $pages = Download::where('file_id', '=', $file->id)->join('page', 'page.u_id', '=', 'page_u_id')->where(
                'page.deleted',
                '=',
                0
            )->select('page.*')->get();
        } else {
            //TODO: GALLERY has connection to page_u_id but column name isnt'changed
            $pages = Gallery::where('file_id', '=', $file->id)->where('gallery.langs', '>', '0')->join(
                'page',
                'page.u_id',
                '=',
                'page_id'
            )->where('page.deleted', '=', 0)->select('page.*')->get();
        }

        $count = $pages->count();
        if ($count > 0) {
            $pageVersion = -1;
            $message = trans('ws_file_controller.file.inUse');
            for ($i = 0; $i < $count; ++$i) {
                $page = $pages[$i];
                $pageVersion = $page->version;
                $message .= 'page slug:' . $page->slug . ', ID:' . $page->u_id . ', version:' . $page->version . ($i < $count - 1 ? '</br>' : '.');
            }
            if ($pageVersion == 0) {
                if (!$skipRelationCheck) {
                    return Response::json(RestController::generateJsonResponse(true, $message, $file), 400); // 423 for yes/no question file is locked. It is used in page version 0
                }
            } else {
                return Response::json(RestController::generateJsonResponse(true, $message, $file), 400); // file is locked. It is used in page(s)
            }
        }

        //Load FileInfo
        $fileInfo = $file->info()->get();
        // Handle Errors! and Add Transaction and Delete FileInfo!
        $httpStatus = 200;
        $isFileMoved = false;
        $rollbackFile = $file->replicate();
        $isDeleted = !empty($deletedfile->deleted_at);
        $deleteSuccess = false;
        try {
            // Start transaction!
            DB::beginTransaction();

            foreach ($fileInfo as $info) {
                //$info->forceDelete();
                $info->delete();
            }
            if ($isPermanent) {
                // Set file to be permanently deleted
                $file->deleted = 1;
            } else {
                $this->moveDeletedFile($file, $filemanager_mode, true, false, null, false);
                $isFileMoved = true;
            }
            $file->save();
            if (!$isDeleted) {
                $file->delete();
            }
            // Commit the queries!
            DB::commit();
            //Success !!!
            $deleteSuccess = true;
        } catch (\Exception $e) {
            $deleteSuccess = false;
            Log::error($e);
            try {
                if ($isFileMoved) {
                    //File was moved to new destination and have to be rollbacked
                    //To rollback file $isDeleted must be called with false because of method's opposite logic, $isRestore have to be false
                    $this->moveDeletedFile($file, $filemanager_mode, false, false, $rollbackFile);
                }
                DB::rollback();
            } catch (\Exception $e) {
                Log::error($e);
            }
            if ($e->getCode() == RECYCLE_BIN_EXCEED_EXCEPTION) {
                $httpStatus = 507; // Insufficient Storage
            } else {
                $httpStatus = 500; // Internal Server Error
            }
        }

        if ($deleteSuccess) {
            if ($isPermanent) {
                //delete file, thub and zoom
                $error_array = $this->deleteFileThumbZoom($deletedfile, $isDeleted);
                //TODO: Report for errors!
            }

            return Response::json(RestController::generateJsonResponse(
                false,
                trans('ws_file_controller.file.deleted'),
                $deletedfile
            ));
        } else {
            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_file_controller.file.cannotDelete'),
                $deletedfile
            ), $httpStatus);
        }
    }

    /**
     * Permanently delete file from folder or Recycle Bin folder
     * Return Permanent Delete file
     * URL: files/permanentdelete/{ID}
     * METHOD: DELETE
     */
    public function deletePermanentdelete($id = null)
    {
        return $this->deleteDeletefile($id, true);
    }

    /**
     * Force Delete File or tests only
     * URL: pages/forcedeleteFile/{ID}
     * METHOD: DELETE
     */
    public function deleteForcedeletefile($id = null)
    {
        $fileItem = FileItem::withTrashed()->find($id);

        if (self::isFileInEditMode($fileItem)) {
            return Response::json(RestController::generateJsonResponse(
                true,
                trans(
                    'ws_general_controller.file.edited',
                    ['type' => ($fileItem->type == "Images" ? 'image' : 'file')]
                )
            ), 400);
        }

        //delete file, thub and zoom
        $error_array = $this->deleteFileThumbZoom($fileItem, false);
        if (count($error_array) == 0) {
            $fileItem->forceDelete();
        }
    }

    /**
     * Return restored file
     * URL: files/restore/{id}
     * METHOD: PUT
     */
    public function putRestore($id = null)
    {

        $req = Request::json()->all();
        if (empty($req['id'])) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }

        $file = FileItem::onlyTrashed()->where('deleted', 0)->find($req['id']);

        if (is_null($file)) {
            return Response::json(
                RestController::generateJsonResponse(true, trans('ws_file_controller.file.notFound'), $file),
                404
            ); // in response $file not exists
        }

        if (strpos($file->filename, "/" . UserAccessHelper::getUserRootFolder() . "/") === false && UserAccessHelper::getUserRootFolder() != "all") {
            return Response::json(RestController::generateJsonResponse(true, 'You don\'t have access to this folder!'), 404);
        }

        $filemanager_mode = strtolower($file->type);
        if (!in_array($filemanager_mode, $this->fm_mode_list)) {
            return Response::json(
                RestController::generateJsonResponse(true, trans('ws_file_controller.filemanager.wrongMode')),
                400
            );
        }

        //Load FileInfo
        //$fileInfo = FileInfo::where('file_id', '=', $id)->get();
        $fileInfo = $file->info()->onlyTrashed()->get();
        $isFileMoved = false;
        $rollbackFile = $file->replicate();
        try {
            // Start transaction!
            DB::beginTransaction();

            foreach ($fileInfo as $info) {
                //$info->forceDelete();
                $info->deleted_at = null;
                $info->save();
            }

            //$file->forceDelete();
            $file->deleted_at = null;
            $this->moveDeletedFile($file, $filemanager_mode, false, true);
            $isFileMoved = true;
            $file->save();
            // Commit the queries!
            DB::commit();
            //Success !!!
            $deleteSuccess = true;
        } catch (\Exception $e) {
            if ($isFileMoved) {
                //To rollback file from folder to deleted folder $file destionation is in default folder $isDeleted is true
                //$rollbackFile is in deleted folder
                $this->moveDeletedFile($file, $filemanager_mode, true, false, $rollbackFile);
            }
            DB::rollback();
            $deleteSuccess = false;
            Log::error($e);
        }

        if ($deleteSuccess) {
            return Response::json(RestController::generateJsonResponse(false, trans('ws_file_controller.file.restored'), $file));
        } else {
            return Response::json(
                RestController::generateJsonResponse(true, trans('ws_file_controller.file.cannotRestore'), $file),
                500
            );
        }
    }

    /**
     * Return Make directory from JSON
     * URL: files/makeDir/{FILEMANAGER_MODE}?dirname={$dirname}
     * METHOD: POST
     */
    public function postMakedir($filemanager_mode)
    {

        if (!in_array($filemanager_mode, $this->fm_mode_list)) {
            return Response::json(
                RestController::generateJsonResponse(true, trans('ws_file_controller.filemanager.wrongMode')),
                400
            );
        }

        $req = Request::all();
        $dirname = $req['dirname'];
        if (empty($req['path'])) {
            $current_path = '';
        } else {
            $current_path = $req['path'];
        }

        if ((strpos($current_path, "/" . UserAccessHelper::getUserRootFolder() . "/") === false || $dirname == "/" . UserAccessHelper::getUserRootFolder() . "/") && UserAccessHelper::getUserRootFolder() != "all") {
            return Response::json(RestController::generateJsonResponse(true, 'You don\'t have access to create this folder!'), 404);
        }

        // Validate folder name
        if (strlen($dirname) == 0) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_file_controller.dirname.required')), 400);
        }

        if (!preg_match(config('app.FILE_NAME_PATTERN'), $dirname)) {
            /* $dirname contains at least one illegal character. */
            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_general_controller.illegalChar.parameter', ["attribute" => "dirname"])
            ), 400);
        }

        if (!preg_match(config('app.PATH_NAME_PATTERN'), $current_path)) {
            /* $dirname contains at least one illegal character. */
            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_general_controller.illegalChar.parameter', ["attribute" => "path"])
            ), 400);
        }

        // Validate 'path' if exists!
        $root_path = FileController::getFullPath($filemanager_mode, $current_path);
        $root_path_protected = FileController::getFullPath($filemanager_mode, $current_path, true);

        if (!is_dir($root_path)) {
            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_file_controller.dir.root.notexist'),
                ['path' => $current_path]
            ), 400);
        }
        // Create folders in protected_files
        FileHelper::makedirs($root_path_protected, FILE_DEFAULT_ACCESS);

        $full_path = FileController::getFullPath($filemanager_mode, $current_path . $dirname);
        $full_path_protected = FileController::getFullPath($filemanager_mode, $current_path . $dirname, true);

        if (is_dir($full_path) || is_dir($full_path_protected)) {
            $result = Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_file_controller.dir.exist'),
                ['dirname' => $dirname]
            ), 400);
        } else {
            //create dir and subdirs
            try {
                $this->createFileFolderStructure($full_path, $full_path_protected);
                $result = Response::json(RestController::generateJsonResponse(
                    false,
                    trans('ws_file_controller.dir.create.success'),
                    ['dirname' => $dirname]
                ), 200);
            } catch (\Exception $e) {
                $result = Response::json(RestController::generateJsonResponse(true, $e->getMessage()), 400);
            }
        }

        return $result;
    }

    /**
     * Return Delete Directoey
     * URL: files/deleteDir/{FILEMANAGER_MODE}?dirname={$dirname}
     * METHOD: DELETE
     */
    public function deleteDeletedir($filemanager_mode)
    {

        if (!in_array($filemanager_mode, $this->fm_mode_list)) {
            return Response::json(
                RestController::generateJsonResponse(true, trans('ws_file_controller.filemanager.wrongMode')),
                400
            );
        }

        $req = Request::all();

        if (!isset($req['dirname']) || strlen($req['dirname']) == 0) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_file_controller.dirname.required')), 400);
        }

        $dirname = $req['dirname'];

        if (!preg_match(config('app.PATH_NAME_PATTERN'), $dirname)) {
            /* $dirname contains at least one illegal character. */
            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_general_controller.illegalChar.parameter', ["attribute" => "dirname"])
            ), 400);
        }

        if ((strpos($dirname, "/" . UserAccessHelper::getUserRootFolder() . "/") === false || $dirname == "/" . UserAccessHelper::getUserRootFolder() . "/") && UserAccessHelper::getUserRootFolder() != "all") {
            return Response::json(RestController::generateJsonResponse(true, 'You don\'t have access to delete this folder!'), 404);
        }
        // Check if it is Brand/Company folder
        $brand = self::getBrnadOfFile($dirname);
        if ("all" == $brand->folder) {
            $brandFolder = 'Daimler';
        } else {
            $brandFolder = $brand->folder;
        }
        if (strcasecmp(trim($dirname, "/"), $brandFolder) === 0) {
            return Response::json(RestController::generateJsonResponse(
                true,
                "You aren't allow to delete Brand/Company folder: {$dirname} !"
            ), 404);
        }

        $isDeleted = json_decode(Request::get('isDeleted', false));

        $absolute_folder_path = FileController::getFullPath($filemanager_mode, $dirname, false, $isDeleted);
        $absolute_folder_path_protected = null;
        if (!$isDeleted) {
            $absolute_folder_path_protected = FileController::getFullPath($filemanager_mode, $dirname, true);
        }
        if (!is_dir($absolute_folder_path) || (!is_null($absolute_folder_path_protected) && !is_dir($absolute_folder_path_protected))) {
            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_file_controller.dir.notExist'),
                ['dirname' => $dirname]
            ), 400);
        }

        $skipEmptyCheck = json_decode(Request::get('skipEmptyCheck', false));

        $filesInDB = FileItem::where('type', '=', $filemanager_mode)->
        where('deleted', 0)->where(
            function ($query) use ($dirname) {
                // make dirname case sensitive
                $matchString = DB::raw('"%' . $dirname . '%" COLLATE utf8_bin');
                $query->where('filename', 'LIKE', $matchString);
                $query->orWhere('thumbnail', 'LIKE', $matchString);
                $query->orWhere('zoompic', 'LIKE', $matchString);
            }
        );

        //DELETE files in the folder for skipEmptyCheck=true and RecicleBin
        if ((!$isDeleted) && $filesInDB->count() > 0) {
            if ($skipEmptyCheck) {
                $resultFiles = $this->deleteFiles($filesInDB->get(), $isDeleted);
                if ($resultFiles != null && !$resultFiles->isSuccessful()) {
                    return $resultFiles;
                }
            } else {
                Log::error('In DB exists files for directory: ' . $dirname);

                return Response::json(RestController::generateJsonResponse(
                    true,
                    trans('ws_file_controller.dir.notEmpty'),
                    ['dirname' => $dirname]
                ), 400);
            }
        }

        //check if folder is empty or clean the folder
        if ($this->checkOrMakeEmptyFolder($absolute_folder_path, $skipEmptyCheck)) {
            Log::debug('Exists files for directory: ' . $dirname);

            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_file_controller.dir.notEmpty'),
                ['dirname' => $dirname]
            ), 400);
        }

        //check if protected folder is empty
        if ($absolute_folder_path_protected && $this->checkOrMakeEmptyFolder($absolute_folder_path_protected, $skipEmptyCheck)) {
            Log::debug('In "protected_files" exists files for directory: ' . $dirname);

            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_file_controller.dir.notEmpty'),
                ['dirname' => $dirname]
            ), 400);
        }

        return $this->deleteFolders($absolute_folder_path, $absolute_folder_path_protected, $dirname);
    }

    private function deleteFiles($filesInDB, $isPermanent)
    {
        $lastError = null;
        $message = '';
        foreach ($filesInDB as $file) {
            $result = $this->deleteDeletefile($file->id, $isPermanent, true);
            if (!$result->isSuccessful()) {
                $lastError = $result;
                //TODO: Break and report!
                $error = json_decode($result->getContent());
                $debugmsg = "";
                if (isset($error->debug)) {
                    $debugmsg = ' Debug message: ' . $error->debug->exception_message;
                }
                $message = $message . $error->message . ' ' . $file->filename . $debugmsg . '<br>';
            }
        }
        if ($lastError) {
            return Response::json(RestController::generateJsonResponse(true, $message), 400);
        } else {
            return null;
        }
    }

    private function checkOrMakeEmptyFolder($absolute_folder_path, $skipEmptyCheck)
    {
        $result = false;
        Log::debug('Empty directory: ' . $absolute_folder_path);
        if (false !== ($handle = opendir($absolute_folder_path))) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != ".." && ($skipEmptyCheck || ($entry != self::FOLDER_NAME_THUMBNAIL && $entry != self::FOLDER_NAME_ZOOMPIC))) {
                    Log::debug('Exists file:' . $entry . ' for directory: ' . basename($absolute_folder_path));
                    if ($skipEmptyCheck) {
                        $main_file = $absolute_folder_path . $entry;
                        if (!@unlink($main_file)) {
                            if (is_dir($main_file)) {
                                // Remove the subfolder
                                $subResult = $this->checkOrMakeEmptyFolder($main_file . DIRECTORY_SEPARATOR, $skipEmptyCheck);
                                $result = $result && $subResult;
                                // Remove the folder
                                if (@rmdir($main_file)) {
                                    $result = $result && true;
                                } else {
                                    $result = false;
                                }
                            } else {
                                //Log for problems and return message to user.
                                Log::error(sprintf('Failed to remove file "%s".', $main_file));
                                $result = false;
                            }
                        } else {
                            $result = $result && true;
                        }
                    } else {
                        break;
                    }
                }
            }
            closedir($handle);
        } else {
            Log::debug('Cannot open the directory: ' . $absolute_folder_path);
        }

        return $result;
    }

    private function deleteFolders($mainFolderPath, $protectedFolderPath, $dirname)
    {
        $deletedFolders = [];
        try {
            //delete main folder and thumbnail and zoompic folder
            if (is_dir($mainFolderPath . self::FOLDER_NAME_THUMBNAIL)) {
                if (rmdir($mainFolderPath . self::FOLDER_NAME_THUMBNAIL)) {
                    $deletedFolders[$mainFolderPath . self::FOLDER_NAME_THUMBNAIL] = true;
                } else {
                    Log::error('Unable to delete dir ' . $mainFolderPath . self::FOLDER_NAME_THUMBNAIL);

                    return Response::json(RestController::generateJsonResponse(
                        true,
                        trans('ws_file_controller.dir.notEmpty'),
                        ['dirname' => $dirname]
                    ), 400);
                }
            }
            if (is_dir($mainFolderPath . self::FOLDER_NAME_ZOOMPIC)) {
                if (rmdir($mainFolderPath . self::FOLDER_NAME_ZOOMPIC)) {
                    $deletedFolders[$mainFolderPath . self::FOLDER_NAME_ZOOMPIC] = true;
                } else {
                    Log::error('Unable to delete dir ' . $mainFolderPath . self::FOLDER_NAME_ZOOMPIC);
                    $this->restoreFolder($deletedFolders);

                    return Response::json(RestController::generateJsonResponse(
                        true,
                        trans('ws_file_controller.dir.notEmpty'),
                        ['dirname' => $dirname]
                    ), 400);
                }
            }
            if (rmdir($mainFolderPath)) {
                $deletedFolders = array_merge([$mainFolderPath => true], $deletedFolders);
                //delete protected folder if exist in this mode
                if (!is_null($protectedFolderPath)) {
                    if (is_dir($protectedFolderPath . self::FOLDER_NAME_THUMBNAIL)) {
                        if (rmdir($protectedFolderPath . self::FOLDER_NAME_THUMBNAIL)) {
                            $deletedFolders[$protectedFolderPath . self::FOLDER_NAME_THUMBNAIL] = true;
                        } else {
                            $this->restoreFolder($deletedFolders);
                            Log::error('Unable to delete dir ' . $protectedFolderPath . self::FOLDER_NAME_THUMBNAIL);

                            return Response::json(RestController::generateJsonResponse(
                                true,
                                trans('ws_file_controller.dir.notEmpty'),
                                ['dirname' => $dirname]
                            ), 400);
                        }
                    }
                    if (is_dir($protectedFolderPath . self::FOLDER_NAME_ZOOMPIC)) {
                        if (rmdir($protectedFolderPath . self::FOLDER_NAME_ZOOMPIC)) {
                            $deletedFolders[$protectedFolderPath . self::FOLDER_NAME_ZOOMPIC] = true;
                        } else {
                            $this->restoreFolder($deletedFolders);
                            Log::error('Unable to delete dir ' . $protectedFolderPath . self::FOLDER_NAME_ZOOMPIC);

                            return Response::json(RestController::generateJsonResponse(
                                true,
                                trans('ws_file_controller.dir.notEmpty'),
                                ['dirname' => $dirname]
                            ), 400);
                        }
                    }
                    if (is_dir($protectedFolderPath)) {
                        if (rmdir($protectedFolderPath)) {
                            $deletedFolders = array_merge([$protectedFolderPath => true], $deletedFolders);
                        } else {
                            $this->restoreFolder($deletedFolders);
                            Log::error('Unable to delete dir ' . $protectedFolderPath);

                            return Response::json(RestController::generateJsonResponse(
                                true,
                                trans('ws_file_controller.dir.delete.fail')
                            ), 400);
                        }
                    }
                }
                Log::info(count($deletedFolders) . ' folders was deleted!');

                return Response::json(RestController::generateJsonResponse(
                    false,
                    trans('ws_file_controller.dir.delete.success'),
                    ['dirname' => $dirname]
                ), 200);
            } else {
                $this->restoreFolder($deletedFolders);

                return Response::json(RestController::generateJsonResponse(true, trans('ws_file_controller.dir.delete.fail')), 400);
            }
        } catch (\Exception $e) {
            $this->restoreFolder($deletedFolders);

            return Response::json(RestController::generateJsonResponse(true, $e->getMessage()), 400);
        }
    }

    private function restoreFolder($folders)
    {
        foreach ($folders as $folder => $value) {
            if ($value) {
                if (!mkdir($folder)) {
                    Log::error('Unable to create dir ' . $folder);
                }
            }
        }
    }

    public function getSubfolders()
    {

        $params = Request::all();
        $dirname = $params['path'];
        $searchedPath = $dirname;
        
        // Get the MODE image or download?
        $filemanager_mode = $params['mode'];
        //Convert string to boolean
        $isFullTree = filter_var(isset($params['isFullTree']) ? $params['isFullTree'] : false, FILTER_VALIDATE_BOOLEAN);

        if (!in_array($filemanager_mode, $this->fm_mode_list)) {
            return Response::json(
                RestController::generateJsonResponse(true, trans('ws_file_controller.filemanager.wrongMode')),
                400
            );
        }
        if (!preg_match(config('app.PATH_NAME_PATTERN'), $dirname)) {
            /* $dirname contains at least one illegal character. */
            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_general_controller.illegalChar.parameter', ["attribute" => "path"])
            ), 400);
        }

        $userRootFolder = UserAccessHelper::getUserRootFolder();
        //Log::debug('Dir: ' . $searchedPath . " User Root: " . $userRootFolder);

        if ($isFullTree) {
            $dirsArray = explode("/", substr($dirname, 1, -1));
            if ($userRootFolder != "all") {
                array_shift($dirsArray);
                $dirname = '/' . $userRootFolder . '/';
            } else {
                $dirname = '/';
            }
        }

        if (strpos($searchedPath, "/" . $userRootFolder . "/") === false && $userRootFolder != "all") {
            return Response::json(RestController::generateJsonResponse(true, 'You don\'t have access to this folder!'), 404);
        }

        $arrayFiles = [];
        $filesDir = [];
        if (empty($params['isDeleted'])) {
            $isDeleted = false;
        } else {
            $isDeleted = $params['isDeleted'] == 'true' ? true : false;
        }
        $current_path = FileController::getFullPath($filemanager_mode, $dirname, false, $isDeleted);
        if (!is_dir($current_path)) {
            if ($isDeleted) {
                // if it's recycle bin dir handle case when it's brand directory not created for the brand user
                mkdir($current_path, FILE_DEFAULT_ACCESS, true);
            } else {
                return Response::json(RestController::generateJsonResponse(
                    true,
                    trans('ws_file_controller.dir.notExist'),
                    ['dirname' => $dirname]
                ), 400);
            }
        }
        //TODO: Optimize load for folders
        if (!$handle = opendir($current_path)) {
            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_file_controller.dir.open.error'),
                ['dirname' => $dirname]
            ), 400);
        } else {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    array_push($filesDir, $file);
                }
            }
            closedir($handle);

            foreach ($filesDir as $file) {
                if (is_dir($current_path . '/' . $file)) {
                    if (!in_array($file, $this->unallowed_dirs)) {
                        $arrayFiles[$dirname . $file . '/'] = [
                            'Path' => $dirname . $file . '/',
                            'Filename' => $file,
                            'File Type' => 'dir',
                            'Properties' => [
                                'Date Created' => "",
                                'Date Modified' => "",
                                'filemtime' => "",
                                'Height' => null,
                                'Width' => null,
                                'Size' => null
                            ],
                            'Childs' => $isFullTree && $dirsArray[0] == $file ? $this->getSubfolderChild(
                                $dirname,
                                0,
                                $dirsArray,
                                $filemanager_mode
                            ) : false,
                            'Error' => "",
                            'Code' => 0
                        ];
                    }
                }
            }
        }

        $subfoldersResponse = ['files' => $arrayFiles];
        $subfoldersResponse['folder_info'] = FileController::getFolderStatus($filemanager_mode, $dirname, $isDeleted);

        return Response::json($subfoldersResponse, 200);
    }

    /**
     * Return is file available for editing
     * URL: files/isavailable/{$fileId}
     * METHOD: GET
     */
    public function getIsavailable($fileId)
    {

        if (empty($fileId)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        $result = $this->checkAvailability($fileId);
        if ($result->status) {
            return Response::json(RestController::generateJsonResponse(false, 'You can edit this file', $result), 200);
        } else {
            if (!$result->status && !isset($result->error)) {
                $file = FileItem::find($fileId);

                return Response::json(RestController::generateJsonResponse(
                    true,
                    trans(
                        'ws_general_controller.file.edited',
                        ['type' => ($file->type == "Images" ? 'image' : 'file')]
                    )
                ), 400);
            } else {
                if (!$result->status && $result->error == "") {
                    return Response::json(RestController::generateJsonResponse(true, 'File not found.'), 404);
                } else {
                    if (!$result->status && $result->error) {
                        return Response::json(RestController::generateJsonResponse(true, $result->error), 500);
                    } else {
                        return Response::json(RestController::generateJsonResponse(true, 'File not found.'), 404);
                    }
                }
            }
        }
    }

    /**
     * Return is file available for editing
     * URL: file/appendtime
     * METHOD: POST
     */
    public function postAppendtime()
    {
        $input = Request::json()->all();
        $fileId = $input['fileId'];
        if (empty($fileId)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        try {
            DB::beginTransaction();
            $file = FileItem::find($fileId);
            if (is_null($file)) {
                return Response::json(RestController::generateJsonResponse(true, 'File not found.'), 404);
            } else {
                if (self::isFileInEditMode($file)) {
                    return Response::json(RestController::generateJsonResponse(
                        true,
                        trans(
                            'ws_general_controller.file.edited',
                            ['type' => ($file->type == "Images" ? 'image' : 'file')]
                        )
                    ), 400);
                } else {
                    if ($file->end_edit) {
                        $file->end_edit = date(
                            'Y-m-d H:i:s',
                            strtotime("+" . config('app.edit_max_time_seconds') . " seconds", strtotime($file->end_edit))
                        );
                    } else {
                        $file->end_edit = date('Y-m-d H:i:s', strtotime("+" . config('app.edit_max_time_seconds') . " seconds"));
                    }
                    $file->user_edit_id = Auth::user()->id;
                    $file->save();
                    DB::commit();
                    $result = new \stdClass();
                    $result->status = true;
                    $result->time_left = self::calculateTimeLeftForEdit($file);

                    return Response::json(RestController::generateJsonResponse(false, 'You can edit this file', $result), 200);
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            if (config('app.debug')) {
                // Add stack trace
                $errorArray['debug'] = ['exception_message' => $message, 'stacktrace' => $e->getTraceAsString()];
            }

            return Response::json(RestController::generateJsonResponse(true, $message), 500);
        }
    }

    /**
     * Unlock file for editing
     * URL: files/unlockfile
     * METHOD: POST
     */
    public function postUnlock()
    {
        $input = Request::json()->all();
        $fileId = $input['fileId'];
        if (empty($fileId)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        try {
            DB::beginTransaction();
            $file = FileItem::find($fileId);
            if (is_null($file)) {
                return Response::json(RestController::generateJsonResponse(true, 'File not found.'), 404);
            } else {
                if (self::isFileInEditMode($file)) {
                    return Response::json(RestController::generateJsonResponse(
                        true,
                        trans(
                            'ws_general_controller.file.edited',
                            ['type' => ($file->type == "Images" ? 'image' : 'file')]
                        )
                    ), 400);
                } else {
                    $file->end_edit = null;
                    $file->user_edit_id = null;
                    $file->save();
                    DB::commit();

                    return Response::json(RestController::generateJsonResponse(false, 'File is unlocked', $file), 200);
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            if (config('app.debug')) {
                // Add stack trace
                $errorArray['debug'] = ['exception_message' => $message, 'stacktrace' => $e->getTraceAsString()];
            }

            return Response::json(RestController::generateJsonResponse(true, $message), 500);
        }
    }

    static function checkAvailability($fileId)
    {
        $result = new \stdClass();
        try {
            DB::beginTransaction();
            $file = FileItem::find($fileId);

            Log::debug("End time: " . $file->end_edit . " datetime: " . strtotime($file->end_edit));

            if (is_null($file)) {
                $result->status = false;
                $result->error = "";
            } else {
                if (self::isFileInEditMode($file)) {
                    $result->status = false;
                } else {
                    if ($file->end_edit && $file->user_edit_id == Auth::user()->id && strtotime($file->end_edit) > strtotime(EDIT_TIME_BUFFER_INTERVAL)) {
                        $result->status = true;
                    } else {
                        $file->end_edit = date(
                            'Y-m-d H:i:s',
                            strtotime("+" . config('app.edit_max_time_seconds') . " seconds")
                        );
                        $file->user_edit_id = Auth::user()->id;

                        Log::debug("End time: " . $file->end_edit . " datetime: " . strtotime($file->end_edit));

                        $file->save();
                        DB::commit();
                        $result->status = true;
                    }
                    $result->time_left = self::calculateTimeLeftForEdit($file);
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            if (config('app.debug')) {
                // Add stack trace
                $errorArray['debug'] = ['exception_message' => $message, 'stacktrace' => $e->getTraceAsString()];
            }
            $result->status = false;
            $result->error = $message;
        }

        return $result;
    }

    /*
     * Private methods
     */
    private function createFileFolderStructure($full_path, $full_path_protected)
    {
        if (mkdir($full_path, FILE_DEFAULT_ACCESS, true)) {
            mkdir($full_path . '/' . self::FOLDER_NAME_THUMBNAIL, FILE_DEFAULT_ACCESS, true);
            mkdir($full_path . '/' . self::FOLDER_NAME_ZOOMPIC, FILE_DEFAULT_ACCESS, true);
            if ($full_path_protected) {
                if (mkdir($full_path_protected, FILE_DEFAULT_ACCESS, true)) {
                    mkdir($full_path_protected . '/' . self::FOLDER_NAME_THUMBNAIL, FILE_DEFAULT_ACCESS, true);
                    mkdir($full_path_protected . '/' . self::FOLDER_NAME_ZOOMPIC, FILE_DEFAULT_ACCESS, true);
                } else {
                    if (rmdir($full_path)) {
                        if (file_exists($full_path . '/' . self::FOLDER_NAME_THUMBNAIL)) {
                            rmdir($full_path . '/' . self::FOLDER_NAME_THUMBNAIL);
                        }
                        if (file_exists($full_path . '/' . self::FOLDER_NAME_ZOOMPIC)) {
                            rmdir($full_path . '/' . self::FOLDER_NAME_ZOOMPIC);
                        }
                        if (file_exists($full_path_protected)) {
                            rmdir($full_path_protected);
                        }
                    }
                    throw new \Exception(trans('ws_file_controller.dir.create.fail'));
                }
            }
        } else {
            throw new \Exception(trans('ws_file_controller.dir.create.fail'));
        }
    }

    private function deleteFileThumbZoom($deletedfile, $isDeleted)
    {

        $filemanager_mode = strtolower($deletedfile->type);
        //delete main file
        $error_array = [];
        $main_file = FileController::getFullPath(
            $filemanager_mode,
            $deletedfile->filename,
            $deletedfile->protected,
            $isDeleted
        );
        if (!@unlink($main_file)) {
            //TODO: Log for problems and return message to user.
            $message = sprintf('Failed to remove main file "%s".', $main_file);
            $error_array[] = $message;
            Log::error($message);
        }
        //delete thumbnail and zoompic if exist
        $thumb_file = FileController::getFullPath($filemanager_mode, $deletedfile->thumbnail, $deletedfile->protected, $isDeleted);
        if (is_file($thumb_file)) {
            if (!@unlink($thumb_file)) {
                //TODO: Log for problems and return message to user.
                $message = sprintf('Failed to remove thumb file "%s".', $thumb_file);
                $error_array[] = $message;
                Log::error($message);
            }
        }
        $zoom_file = FileController::getFullPath($filemanager_mode, $deletedfile->zoompic, $deletedfile->protected, $isDeleted);
        if (is_file($zoom_file)) {
            if (!@unlink($zoom_file)) {
                //TODO: Log for problems and return message to user.
                $message = sprintf('Failed to remove zoompic file "%s".', $zoom_file);
                $error_array[] = $message;
                Log::error($message);
            }
        }

        return $error_array;
    }

    private function getSubfolderChild($dirname, $index, $dirsArray, $filemanager_mode)
    {
        if (!$dirsArray[$index]) {
            return false;
        }
        $dirname .= $dirsArray[$index] . '/';
        $index++;
        if (!preg_match(config('app.PATH_NAME_PATTERN'), $dirname)) {
            return false;
        }

        $userRootFolder = UserAccessHelper::getUserRootFolder();

        Log::debug('Dir - subDir: ' . $dirname . " User Root: " . $userRootFolder);

        if (strpos($dirname, "/" . $userRootFolder . "/") === false && $userRootFolder != "all") {
            return false;
        }

        $arrayFiles = [];
        $filesDir = [];

        $current_path = FileController::getFullPath($filemanager_mode, $dirname, false, false);

        if (!is_dir($current_path)) {
            return false;
        }
        if (!$handle = opendir($current_path)) {
            return false;
        } else {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    array_push($filesDir, $file);
                }
            }
            closedir($handle);

            foreach ($filesDir as $file) {
                if (is_dir($current_path . '/' . $file)) {
                    if (!in_array($file, $this->unallowed_dirs)) {
                        $arrayFiles[$dirname . $file . '/'] = [
                            'Path' => $dirname . $file . '/',
                            'Filename' => $file,
                            'File Type' => 'dir',
                            'Properties' => [
                                'Date Created' => "",
                                'Date Modified' => "",
                                'filemtime' => "",
                                'Height' => null,
                                'Width' => null,
                                'Size' => null
                            ],
                            'Childs' => array_key_exists($index, $dirsArray) && $dirsArray[$index] == $file ? $this->getSubfolderChild(
                                $dirname,
                                $index,
                                $dirsArray,
                                $filemanager_mode
                            ) : false,
                            'Error' => "",
                            'Code' => 0
                        ];
                    }
                }
            }

            return $arrayFiles;
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Models\Cart;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Description of CartController
 *
 * @author i.traykov
 */
class CartController extends RestController
{

    /**
     * Return File Items in Cart
     * URL: cart/getCartItems
     * METHOD: GET
     */

    public function getCartitems($userId = 0)
    {
        //parameter userId must be set
        if ($userId <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter userId is required!'), 400);
        }

        $file_items = Cart::cartItems($userId);
        if ($file_items == -1) {
            $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of files found.'), 200);
        } else {
            $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of files found.', $file_items), 200);
        }

        return $jsonResponse;
    }

    /**
     * Add File Item to Cart
     * URL: cart/additemtocart
     * METHOD: GET
     */

    public function postAdditemtocart()
    {

        $req = Request::all();

        //parameter fileId must be set
        if (!isset($req['user_id']) || $req['user_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter user_id is required!'), 400);
        }
        if (!isset($req['file_id']) || $req['file_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter file_id is required!'), 400);
        }

        $cart = new Cart();
        $cart->fill($req);

        //unset($cart->id);

        return $this->trySave($cart, false);
    }

    /**
     * Return save result
     */
    protected function trySave(Cart $cart, $isUpdate)
    {
        if ($isUpdate) {
            $message = "Cart Item was updated.";
        } else {
            $message = "Cart Item was added.";
        }

        try {
            $cart->save();
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            throw new ModelValidationException($e->getErrors());
        }

        unset($cart->id);

        //Success !!!

        return Response::json(RestController::generateJsonResponse(false, $message, $cart));
    }


    /**
     * Remove File Item to Cart
     * URL: cart/removecartitem
     * METHOD: DELETE
     */

    public function deleteRemovecartitem()
    {

        $req = Request::all();

        //parameters fileId and userId must be set
        if (!isset($req['user_id']) || $req['user_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter user_id is required!'), 400);
        }
        if (!isset($req['file_id']) || $req['file_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter file_id is required!'), 400);
        }

        $cartitem = Cart::where('user_id', '=', $req['user_id'])->where('file_id', '=', $req['file_id'])->get();

        if ($cartitem->isEmpty()) {
            return Response::json(RestController::generateJsonResponse(true, 'Cart Item not found.'), 404); // in response access not exists
        }

        $deleteditem = $cartitem[0];
        Cart::where('user_id', '=', $deleteditem->user_id)
            ->where('file_id', '=', $deleteditem->file_id)
            ->update([Cart::DELETED_AT => date('Y-m-d H:i:s'), 'active' => 0]);

        return Response::json(RestController::generateJsonResponse(false, 'Cart item was deleted!', $cartitem[0]));
    }

    /**
     * Empty Cart
     * URL: cart/emptycart
     * METHOD: DELETE
     */

    public function deleteEmptycart($user_id = 0)
    {

        //parameter userId must be set
        if ($user_id <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter user_id is required!'), 400);
        }

        $cartItems = Cart::where('user_id', '=', $user_id)->get();
        Cart::where('user_id', '=', $user_id)->delete();

        return Response::json(RestController::generateJsonResponse(false, 'Cart empty was successfull!', $cartItems));
    }
}

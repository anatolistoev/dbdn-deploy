<?php

namespace App\Http\Controllers;

use App\Helpers\CaptchaHelper;
use App\Helpers\ValidationHelper;
use App\Models\Page;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class DialogPageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Log the usage of captcha
        CaptchaHelper::logCaptchaUsage();

        CaptchaHelper::resetCaptcha();

        return view(
            'dialog',
            [
                'PAGE' => (object)['id' => 'dialog', 'slug' => 'dialog', 'brand' => 'daimler', 'template' => 'null'],
                'ASCENDANTS' => []
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function send($idslug, $brand = null)
    {
        $pagePath = $idslug;
        if (!is_null($brand)) {
            $pagePath = $idslug . "/" . $brand;
        }
        $rules = [
            'messageText' => ['required', 'max:1000', 'no_tags'],
            'name' => ['required', 'max:255', 'no_tags'],
            'email' => 'required|email',
            'captcha' => 'required|same:captchaSecret',
        ];
        $fields = [
            'email' => Request::get('email'),
            'name' => Request::get('name'),
            'messageText' => Request::get('messageText'),
            'captcha' => Request::get('captcha'),
            'captchaSecret' => Session::get('captcha'),
        ];
        $messages = ValidationHelper::getFEValidationTranslation();

        /** @var \Illuminate\Validation\Validator $inputValidator */
        $inputValidator = Validator::make($fields, $rules, $messages);

        // Log the usage of captcha
        CaptchaHelper::logCaptchaUsage();
        // Reset capcha to prevent reuse one captcha.
        CaptchaHelper::resetCaptcha();

        // Set custom attributes Names for FE
        if (config('app.isFE')) {
            if (isset($messages['attributes'])) {
                $inputValidator->setAttributeNames($messages['attributes']);
            }
        }

        if ($inputValidator->fails()) {
            $inputValidator->getMessageBag()->add('dialog_error', ' ');

            return redirect($pagePath)
                ->withErrors($inputValidator)
                ->withInput(Request::except(['captcha']));
        }

        $fields['pid'] = $pagePath;
        $fields['title'] = $this->getPageTitle($idslug);

        Mail::send('emails.dialog_inquiry', $fields, function ($message) {
            $message->to(config('mail.from.address'))->subject(trans('feedback.email.subject'));
            $message->from(Request::get('email'));
        });

        return redirect($pagePath)->withErrors(['success' => trans('dialog.sent')]);
    }

    /**
     * Get the title of a page
     * @param mixed $idslug The ID or the slug of the page
     * @return string
     */
    private function getPageTitle($idslug)
    {
        $lang = $this->lang;
        if (is_numeric($idslug)) {
            $subject = Page::with([
                'contents' => function ($query) use ($lang) {
                    return $query->where('lang', $lang)->where('section', 'title');
                }
            ])->where('id', $idslug)->take(1)->get();
        } else {
            $subject = Page::with([
                'contents' => function ($query) use ($lang) {
                    return $query->where('lang', $lang)->where('section', 'title');
                }
            ])->where('slug', $idslug)->take(1)->get();
        }

        if ($subject->first() && $subject->first()->contents->first()) {
            return $subject->first()->contents->first()->body;
        } else {
            return $idslug;
        }
    }

    /**
     * Show the form for asking help.
     *
     * @return Response
     */
    public function getHelpForm($brand = '')
    {
        CaptchaHelper::resetCaptcha();

        if (Session::get('lang') == 1) {
            $altLang = 2;
        } else {
            $altLang = 1;
        }
        $brandId = 0;
        $slug = 'help';
        if ($brand == 'smart') {
            $brandId = SMART;
            $slug .= '/smart';
        }

        return Response::view('help', [
            'PAGE' => (object)['id' => 'help', 'template' => 'help', 'slug' => $slug, 'langs' => 3, 'brand' => $brandId],
            'ALT_LANG' => $altLang
        ]);
    }
}

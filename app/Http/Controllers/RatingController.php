<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Models\Rating;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Description of RatingController
 *
 * @author i.traykov
 */
class RatingController extends RestController
{


    /**
     * Get Page rating from user
     * URL: rating/read
     * METHOD: GET
     */

    public function getRead()
    {

        $req = Request::all();

        //parameter page_id and user_id must be set
        if (!isset($req['page_id']) || $req['page_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }
        if (!isset($req['user_id']) || $req['user_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter user_id is required!'), 400);
        }

        $rating = Rating::read($req['page_id'], $req['user_id'])->get();

        if ($rating->isEmpty()) {
            return Response::json(RestController::generateJsonResponse(true, 'Rating not found.'), 404);
        }

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'Rating found.', [$rating[0]->vote]), 200);

        return $jsonResponse;
    }

    /**
     * Get Page Average rating for page
     * URL: rating/readavg
     * METHOD: GET
     */

    public function getReadavg($page_id)
    {

        $req = Request::all();

        //parameter page_id and user_id must be set
        if (!isset($page_id) || $page_id <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }

        $avg_r = Rating::Readavg($page_id);
        if ($avg_r == -1) {
            return Response::json(RestController::generateJsonResponse(true, 'Rating not found.'), 404);// as no rating was found on page
        }

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'Average Rating.', [$avg_r]), 200);

        return $jsonResponse;
    }

    /**
     * Create Rating
     * URL: rating/create
     * METHOD: POST
     */

    public function postCreate()
    {

        $req = Request::all();

        //parameter page_id and user_id must be set
        if (!isset($req['page_id']) || $req['page_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }
        if (!isset($req['user_id']) || $req['user_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter user_id is required!'), 400);
        }

        $rating = new Rating();
        $rating->fill($req);

        return $this->trySave($rating, false);
    }

    /**
     * Return save result
     */
    protected function trySave(Rating $rating, $isUpdate)
    {
        try {
            if ($isUpdate) {
                // update query
                // laravel does not work with composite primary keys and update does not work
                Rating::where('page_id', '=', $rating->page_id)->where('user_id', '=', $rating->user_id)->update(['vote' => $rating->vote]);
                $message = "Rating was updated.";
            } else {
                $rating->save();
                unset($rating->id);
                $message = "Rating was added.";
            }
            //Success !!!

            $result = Response::json(RestController::generateJsonResponse(false, $message, $rating));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            throw new ModelValidationException($e->getErrors());
        }
        return $result;
    }



    /**
     * Modify Rating
     * URL: rating/modify
     * METHOD: PUT
     */

    public function putModify()
    {

        $req = Request::all();

        //parameters page_id and user_id must be set
        if (!isset($req['page_id']) || $req['page_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }
        if (!isset($req['user_id']) || $req['user_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter file_id is required!'), 400);
        }

        if (!isset($req['vote']) || $req['vote'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter vote is required!'), 400);
        }

        $rating = Rating::where('page_id', '=', $req['page_id'])->where('user_id', '=', $req['user_id'])->first();

        $rating->vote = $req['vote'];

        return $this->trySave($rating, true);
    }


    /**
     * Remove Rating for Page
     * URL: rating/remove
     * METHOD: DELETE
     */

    public function deleteDelete()
    {

        $req = Request::all();

        //parameters user_id and page_id must be set
        if (!isset($req['page_id']) || $req['page_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }
        if (!isset($req['user_id']) || $req['user_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter user_id is required!'), 400);
        }

        $rating = Rating::where('page_id', '=', $req['page_id'])->where('user_id', '=', $req['user_id'])->get();

        if ($rating->isEmpty()) {
            return Response::json(RestController::generateJsonResponse(true, 'Rating not found.'), 404);
        }

        $deleteditem = $rating[0];
        Rating::where('page_id', '=', $deleteditem->page_id)->where('user_id', '=', $deleteditem->user_id)->delete();

        return Response::json(RestController::generateJsonResponse(false, 'Rating was deleted!', $rating[0]));
    }

    	/**
	 * Remove Rating for Page
	 * URL: rating/remove
	 * METHOD: DELETE
     * use for testing
	 */

	public function deleteForcedeletebypage(){
		$req = Input::all();

		//parameters user_id and page_id must be set
		if(!isset($req['page_id']) || $req['page_id'] <= 0){
			return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
		}

		$rating = Rating::withTrashed()->where('page_id','=',$req['page_id'])->get();

		if ($rating->isEmpty()) {
			return Response::json(RestController::generateJsonResponse(true, 'Rating not found.'), 404);
		}

		Rating::withTrashed()->where('page_id','=',$req['page_id'])->delete();

		return Response::json(RestController::generateJsonResponse(false, 'Rating was deleted!', $rating));
	}

}

<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Models\FileItem;
use App\Models\Gallery;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

/**
 * Description of GalleryController
 *
 * @author i.traykov
 */
class GalleryController extends RestController
{

    /**
     * Return Images in Page
     * URL: gallery/images
     * METHOD: GET
     */

    public function getImages($page_id = 0)
    {

        $req = Request::all();

        //parameter userId must be set
        if ($page_id <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }

        if (!isset($req['lang']) || $req['lang'] < 0) {
            $file_ids = Gallery::where('page_id', '=', $page_id)->whereRaw('langs >> (?-1) & 1 = 1', [Session::get('lang')])->orderBy('position', 'asc')->get(); // get items on page
            $file_items = [];
            foreach ($file_ids as $file_id) {
                //get attributes with file dependency from model
                $file_item = $file_id->file()->first()->toArray();
                $file_items[] = $file_item;
            }
            $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of files found.', $file_items), 200);
        } else {
            //sql query to take file data and file info title and description
            $file_items = DB::table('file as f')
                ->select(DB::raw('f.*, f_i.title, f_i.description'))
                ->join('fileinfo as f_i', 'f_i.file_id', '=', 'f.id')
                ->join('gallery as g', 'g.file_id', '=', 'f.id')
                ->where('g.page_id', '=', $page_id)
                                ->whereRaw('g.langs >> (?-1) & 1 = 1', [Session::get('lang')])
                ->where('f_i.lang', '=', $req['lang'])
                ->whereNull('f.deleted_at');
            foreach ($file_items as $key => $fi) {
                unset($file_items[$key]->deleted_at);
            }
            $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of files found.', $file_items), 200);
        }
        return $jsonResponse;
    }

    /**
     * Create Gallery item
     * URL: gallery/create
     * METHOD: POST
     */

    public function postCreate()
    {

        $req = Request::all();

        //parameter fileId must be set
        if (!isset($req['page_id']) || $req['page_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }
        if (!isset($req['file_id']) || $req['file_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter file_id is required!'), 400);
        }

        $gallery = new Gallery();
        $gallery->fill($req);

        return GalleryController::trySave($gallery, false);
    }

    /**
     * Return save result
     */
    public static function trySave(Gallery $gallery, $isUpdate)
    {
        try {
            $gallery->save();
            unset($gallery->id);
            $message = "Gallery was added.";

            //Success !!!
            $result = Response::json(RestController::generateJsonResponse(false, $message, $gallery));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            throw new ModelValidationException($e->getErrors());
        } catch (\Exception $e) {
            $message = $e->getMessage();
            if (0 === strpos($message, 'SQLSTATE[23000]')) {
                $gallery = Gallery::withTrashed()->where('file_id', '=', $gallery->file_id)->where('page_id', '=', $gallery->page_id)->first();
                if ($gallery->trashed()) {
                    Gallery::withTrashed()->where('file_id', '=', $gallery->file_id)->where('page_id', '=', $gallery->page_id)->update(['deleted_at' => null , 'langs' => $gallery->langs]);
                    $message = "Gallery was granted.";
                    $result = Response::json(RestController::generateJsonResponse(false, $message, $gallery), 200);
                } else {
                    if ($isUpdate) {
                        // Do Noting. Skeep the error.
                        $result = Response::json(RestController::generateJsonResponse(false, $message, $gallery), 200);
                    } else {
                        Log::error($e);

                        $result = Response::json(RestController::generateJsonResponse(true, 'Duplicate record.'), 500);
                    }
                }
            } else {
                Log::error($e);

                $result = Response::json(RestController::generateJsonResponse(true, $message), 500);
            }
        }
        return $result;
    }

    public function putUpdate($page_id = 0)
    {
        if (!isset($page_id) || $page_id <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }

        $req = Request::all();
        $arr_file_id = $req['files'];

        try {
            // Start transaction!
            DB::beginTransaction();

            $arrOldGallery = Gallery::where('page_id', '=', $page_id)->pluck('file_id')->toArray();

            $imgs = Gallery::where('page_id', '=', $page_id)->whereNotIn('file_id', $arr_file_id)->get();
            foreach ($imgs as $gallery) {
                if (($gallery->langs >> (Session::get('lang') - 1) & 1) == 1) {
                    $gallery->langs -= Session::get('lang');
                    Gallery::where('file_id', '=', $gallery->file_id)->where('page_id', '=', $gallery->page_id)->update(['langs' => $gallery->langs]);
                }
            }

            foreach ($arr_file_id as $file_id) {
                if ($file_id != "") {
                    if (!in_array($file_id, $arrOldGallery)) {
                        $file = FileItem::find($file_id);
                        if (!is_null($file)) {
                            $gallery = new Gallery();
                            $gallery->page_id = $page_id;
                            $gallery->langs = Session::get('lang');
                            $gallery->file_id = $file_id;
                            try {
                                $response = GalleryController::trySave($gallery, true);
                                if (!$response->isSuccessful()) {
                                    // Rollback and then return errors
                                    DB::rollback();
                                    return $response;
                                }
                            } catch (\Exception $e) {
                                DB::rollback();
                                throw $e;
                            }
                        } else {
                            return Response::json(RestController::generateJsonResponse(true, 'File image not found.', [$file_id]), 404);
                        }
                    } else {
                        $gallery = Gallery::where('page_id', '=', $page_id)->where('file_id', '=', $file_id)->first();
                        if (($gallery->langs >> (Session::get('lang') - 1) & 1) != 1) {
                            $gallery->langs += Session::get('lang');
                            Gallery::where('file_id', '=', $gallery->file_id)->where('page_id', '=', $gallery->page_id)->update(['langs' => $gallery->langs]);
                        }
                    }
                }
            }
            //Success !!!
            // Commit the queries!
            DB::commit();

            return Response::json(RestController::generateJsonResponse(false, 'Page imagas were updated.'), 200);
        } catch (\Exception $e) {
            // Rollback and then return errors
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove Gallery From Page
     * URL: gallery/remove
     * METHOD: DELETE
     */

    public function deleteRemove()
    {

        $req = Request::all();

        //parameters page_id and file_id must be set
        if (!isset($req['page_id']) || $req['page_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }
        if (!isset($req['file_id']) || $req['file_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter file_id is required!'), 400);
        }

        $gallery = Gallery::where('page_id', '=', $req['page_id'])->where('file_id', '=', $req['file_id'])->get();

        if ($gallery->isEmpty()) {
            return Response::json(RestController::generateJsonResponse(true, 'Gallery not found.'), 404);
        }

        $galleryitem = $gallery[0];
        Gallery::where('page_id', '=', $galleryitem->page_id)->where('file_id', '=', $galleryitem->file_id)->delete();

        return Response::json(RestController::generateJsonResponse(false, 'Gallery was deleted!', $galleryitem));
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\History;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Description of HistoryController
 *
 * @author i.traykov
 */
class HistoryController extends RestController
{

    /**
     * Return List of All History items
     * URL: history/all
     * METHOD: GET
     */
    public function getAll()
    {

        $req = Request::all();

        if (empty($req['from'])) {
            $from=null;
        } else {
            $from=$req['from'];
        }

        if (empty($req['user_id'])) {
            $user_id=null;
        } else {
            $user_id=$req['user_id'];
        }

        if (empty($req['page_id'])) {
            $page_id=null;
        } else {
            $page_id=$req['page_id'];
        }

        if (empty($req['content_id'])) {
            $content_id=null;
        } else {
            $content_id=$req['content_id'];
        }

        $filtered_history = History::showall($from, $page_id, $user_id, $content_id)->get();

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of history items found.', $filtered_history), 200);

        return $jsonResponse;
    }

    /**
     * Return History by ID
     * URL: history/read/{ID}
     * METHOD: GET
     */
    public function getRead($id = null)
    {
        if (is_null($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $history = History::find($id);

        if (is_null($history)) {
            return Response::json(RestController::generateJsonResponse(true, 'History not found.'), 404);
        }
        return Response::json(RestController::generateJsonResponse(false, 'History found.', $comment));
    }

    /**
     * Return Delte history item
     * URL: history/delete/{ID}
     * METHOD: DELETE
     */
    public function deleteDelete($id = null)
    {

        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $history = History::find($id);

        if (is_null($history)) {
            return Response::json(RestController::generateJsonResponse(true, 'History not found.', $content), 404);
        }
        $deletedhistory = $history;
        // TODO: Handle Errors!
        $history->delete();
        return Response::json(RestController::generateJsonResponse(false, 'History was deleted.', $deletedhistory));
    }

    /**
     * Return Delete history item
     * URL: history/deletebypage/{ID}
     * METHOD: DELETE
     * mainly for delete test history
     */
    public function deleteDeletebypage($id = null)
    {
        History::where('page_id', '=', $id)->delete();
    }

    /**
	 * Return Delete history item
	 * URL: history/forcedeletebypage/{ID}
	 * METHOD: DELETE
	 * mainly for delete test history
	 */
	public function deleteForcedeletebypage($id = null) {
		$histories= History::withTrashed()->where('page_id','=',$id)->get();
        foreach ($histories as $history) {
            $history->forceDelete();
        }
	}
}

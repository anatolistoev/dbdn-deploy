<?php

namespace App\Http\Controllers;

use App\Helpers\DispatcherHelper;
use App\Helpers\FileHelper;
use App\Helpers\UserAccessHelper;
use App\Helpers\FETeaserHelper;
use App\Models\Cart;
use App\Models\FileItem;
use App\Models\Page;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class MyDownloadsPageController extends Controller
{

    /**
     * Display a listing of file for download.
     *
     * @return View
     */
    public function listDownloads()
    {
        $downloads = Cart::with([
                        'file' => function ($query) {
                        },
                        'download.page' => function ($query) {
                            $query->where('page.active', '=', 1);
                        },
                    ])->where('user_id', '=', Auth::user()->id)
                ->where('cart.active', 1)
                ->orderBy('created_at', 'desc')
                ->get();
        //)->cartItems(Auth::user()->id);
        $auth_pages = Page::alive()
                ->select('page.*', 'download.file_id', 'file.size', 'file.extension', 'file.protected as file_protected')
                ->join('request_auth', 'request_auth.page_id', '=', 'page.id')
                ->where('request_auth.active', 1)
                ->where('request_auth.user_id', Auth::user()->id)
                ->join('download', 'download.page_u_id', '=', 'page.u_id')
                ->join('file', 'file.id', '=', 'download.file_id')
                ->whereNull('request_auth.deleted_at')
                ->whereNull('download.deleted_at')
                ->whereNull('file.deleted_at')
                ->groupBy('download.page_u_id')
                ->get();

        $blocks     = [];
        $idsToDig   = [];
        $ids        = [];
        $u_ids      = [];
        foreach ($downloads as $cartItem) {
            // Check if download and page exists!
            $download = $cartItem->download->first(function ($value, $key) {
                return !empty($value->page);
            });
            if ($download && $page = $download->page) {
                if ($cartItem->file!=null) {
                    $ids[]                            = $page->id;
                    $u_ids[]                          = $page->u_id;
                    $file                             = $cartItem->file;
                    $blocks[$page->id]['slug']        = $page->slug;
                    $blocks[$page->id]['title']       = $page->slug;
                    $blocks[$page->id]['h1']          = '';
                    $blocks[$page->id]['text']        = '';
                    $blocks[$page->id]['img']         = '';
                    $blocks[$page->id]['download_id'] = $file->id;
                    $blocks[$page->id]['tags']        = $page->joinTags(1);
                    $blocks[$page->id]['date']        = $page->updated_at;
                    $blocks[$page->id]['visits']      = $page->visits;
                    $blocks[$page->id]['comments']    = $page->comments()->count();
                    $blocks[$page->id]['ratings']     = $page->ratings()->count();
                    $blocks[$page->id]['has_access']  = UserAccessHelper::authUserHasAccess($page);
                    $blocks[$page->id]['auth_pages']  = false;
                    $idsToDig[]                       = $page->id;
                    $blocks[$page->id]['extension']   = ltrim($file->extension, '.');
                    $blocks[$page->id]['size']        = $file->size;
                    $blocks[$page->id]['protected']   = $file->protected;
                    $blocks[$page->id]['parent_id']   = $page->parent_id;
                }
            }
        }
        // Process pages with autorization
        foreach ($auth_pages as $page) {
            $ids[]                            = $page->id;
            $u_ids[]                          = $page->u_id;
            $blocks[$page->id]['slug']        = $page->slug;
            $blocks[$page->id]['title']       = $page->slug;
            $blocks[$page->id]['h1']          = '';
            $blocks[$page->id]['text']        = '';
            $blocks[$page->id]['img']         = '';
            $blocks[$page->id]['download_id'] = $page->file_id;
            $blocks[$page->id]['tags']        = $page->joinTags(1);
            $blocks[$page->id]['date']        = $page->updated_at;
            $blocks[$page->id]['visits']      = $page->visits;
            $blocks[$page->id]['comments']    = $page->comments()->count();
            $blocks[$page->id]['ratings']     = $page->ratings()->count();
            $blocks[$page->id]['has_access']  = UserAccessHelper::authUserHasAccess($page);
            $idsToDig[]                       = $page->id;
            $blocks[$page->id]['auth_pages']  = true;
            $blocks[$page->id]['extension']   = ltrim($page->extension, '.');
            $blocks[$page->id]['size']        = $page->size;
            $blocks[$page->id]['protected']   = $page->file_protected;
            $blocks[$page->id]['parent_id']   = $page->parent_id;
        }
        $founded = FETeaserHelper::generateDownloadOverviewTeasers($u_ids, $blocks);
        $idsToDig = array_diff($idsToDig, $founded);
        FETeaserHelper::addDownloadDetail($idsToDig, $blocks);
        if (count($idsToDig) > 0) {
            FETeaserHelper::generateDownloadExtensionImage($u_ids, $blocks);
        }
        // We don't know the brand
        foreach ($blocks as $page_id => $block) {
            if ($blocks[$page_id]['img'] == '') {
                $blocks[$page_id]['type'] = FileHelper::MODE_BRAND_THUMB;
                // Find the brand thumbnail
                $ascendants = Page::ascendants($page_id, $this->lang)->get();
                $sectionId = $blocks[$page_id]['parent_id'] == HOME ? $page_id : DispatcherHelper::getActiveL2($ascendants);
                $blocks[$page_id]['img'] = DispatcherHelper::getBrandThumbnail($sectionId);
            }
        }

        if (Session::get('lang') == 1) {
            $altLang = 2;
        } else {
            $altLang = 1;
        }
        $auth_blocks = [];
        foreach ($blocks as $key => $value) {
            if ($value['auth_pages']) {
                $auth_blocks[$key] = $value;
                unset($blocks[$key]);
            }
        }
        return view(
            'mydownloads',
            [
                'PAGE'           => (object)[
                    'id'        => 'Download_Cart',
                    'template'  => 'downloads',
                    'overview'  => 1,
                    'parent_id' => HOME,
                    'slug'      => 'Download_Cart',
                    'brand'     => '',
                    'langs'     => 3
                ],
                'ASCENDANTS'     => [],
                'ALT_LANG'       => $altLang,
                'DOWNLOAD_FILES' => $blocks,
                'AUTH_FILES'     => $auth_blocks,
            ]
        );
    }

    /**
     *
     *
     */
    public function removeDownloads($id = false)
    {
        try {
            $fileIDs = ($id === false ? Request::get('fid') : $id);
            if (is_array($fileIDs)) {
                DB::table('cart')
                        ->where('user_id', Auth::user()->id)
                        ->whereIn('file_id', $fileIDs)
                        ->update(['deleted_at' => date('Y-m-d H:i:s'), 'active' => 0]);
            } else if (is_numeric($fileIDs)) {
                DB::table('cart')
                        ->where('user_id', Auth::user()->id)
                        ->where('file_id', $fileIDs)
                        ->update(['deleted_at' => date('Y-m-d H:i:s'), 'active' => 0]);
            }
            $auIDs = Request::get('auid');
            if (is_array($auIDs)) {
                $auth_pages = Page::alive()
                        ->select('page.id')
                        ->join('request_auth', 'request_auth.page_id', '=', 'page.id')
                        ->join('download', 'download.page_u_id', '=', 'page.u_id')
                        ->where('request_auth.active', 1)
                        ->where('request_auth.user_id', Auth::user()->id)
                        ->whereNull('request_auth.deleted_at')
                        ->whereIn('download.file_id', $auIDs)
                        ->groupBy('download.page_u_id')
                        ->get();
                foreach ($auth_pages as $page) {
                    DB::table('request_auth')
                            ->where('page_id', $page->id)->where('user_id', Auth::user()->id)
                            ->update(['deleted_at' => date('Y-m-d H:i:s'), 'active' => 0]);
                }
            }
            $count = Cart::where('user_id', Auth::user()->id)->where('active', 1)->get()->count() + DB::table('request_auth')->where(
                'user_id',
                Auth::user()->id
            )->where('active', 1)->whereNull('deleted_at')->count();
            Session::put('myDownloadsCount', $count);
            return redirect('/Download_Cart');
        } catch (\Exception $e) {
            Log::error($e);
            abort(500, trans('system.mydownloads.couldnt_save'));
        }
    }

    /**
     *
     *
     */
    public function addDownload($id = false)
    {
        try {
            $fileID = $id === false ? Request::get('fid') : $id;
            if (Auth::check() && is_numeric($fileID)) {
                $fileItem = FileItem::find($fileID);
                if (!$fileItem) {
                    Log::error('MyDawonloads::addDawnload Try to add unknown file:' . $fileID);
                    return Response::json(RestController::generateJsonResponse(true, trans('system.mydownloads.noneselected')), 400);
                }
                if (!UserAccessHelper::authUserHasAccessToFile($fileItem)) {
                    Log::error('MyDawonloads::addDawnload Missing access for protected file:' . $fileID);
                    return Response::json(RestController::generateJsonResponse(true, trans('system.mydownloads.noneselected')), 400);
                }

                $cartItem   = Cart::where('user_id', Auth::user()->id)->where('file_id', $fileID)->withTrashed()->get();
                $auth_pages = Page::alive()
                        ->select('page.id')
                        ->join('request_auth', 'request_auth.page_id', '=', 'page.id')
                        ->join('download', 'download.page_u_id', '=', 'page.u_id')
                        ->where('request_auth.active', 1)
                        ->where('request_auth.user_id', Auth::user()->id)
                        ->whereNull('request_auth.deleted_at')
                        ->where('download.file_id', $fileID)
                        ->groupBy('download.page_u_id')
                        ->get();
                if (count($auth_pages) == 0) {
                    if ($cartItem->count() == 0) {
                        $cart          = new Cart;
                        $cart->user_id = Auth::user()->id;
                        $cart->file_id = $fileID;
                        $cart->save();
                    } elseif ($cartItem[0]->active == 0) {
                        DB::table('cart')
                                ->where('user_id', Auth::user()->id)
                                ->where('file_id', $fileID)
                                ->update(['active' => 1, 'deleted_at' => null]);
                    }
                }
            }
            $count = Cart::where('user_id', Auth::user()->id)->where('active', 1)->get()->count() + DB::table('request_auth')->where(
                'user_id',
                Auth::user()->id
            )->where('active', 1)->whereNull('deleted_at')->count();
            Session::put('myDownloadsCount', $count);
            return Response::json(RestController::generateJsonResponse(false, trans('system.downloads_list.added'), ['myDownloadsCount'=> Session::get('myDownloadsCount')]), 200);
        } catch (\Exception $e) {
            Log::error($e);
            return Response::json(RestController::generateJsonResponse(true, trans('system.mydownloads.couldnt_save')), 400);
        }
    }

    private static function bytes2HumanRedable($bytes)
    {
        $si_prefix = ['B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB'];
        $base      = 1024;
        $class     = min((int)log($bytes, $base), count($si_prefix) - 1);
        return sprintf('%1.2f', $bytes / pow($base, $class)) . ' ' . $si_prefix[$class];
    }

    /**
     *
     *
     */
    public function startDownload()
    {
        // Get IDs of files
        $fileIDs = Request::get('fid');
        if ($fileIDs) {
            // add files that require autentification for app\views\partials\mydownloads_list.blade.php
            $au_filesIDs = Request::get('auid');
            if (!empty($au_filesIDs)) {
                if (is_array($au_filesIDs)) {
                    foreach ($au_filesIDs as $key => $value) {
                        $fileIDs[] = $value;
                    }
                } else {
                    Log::warning('The parameter: auid is not array!');
                }
            }
        } else {
            $fileIDs = Request::get('auid');
        }

        if (is_array($fileIDs)) {
            $files = FileItem::whereIn('id', $fileIDs)->get();
        } else {
            return '<script>parent.msg("' . trans('system.mydownloads.noneselected') . '")</script>';
        }

        if (!$files || $files->count() == 0) {
            echo '<script>parent.msg("' . trans('system.mydownloads.notfound') . '")</script>';
            Log::warning('Files are missing File-IDS: ' . implode($fileIDs, ' | '));
        } else if ($files->count() == 1) {
            $file     = $files->first();
            // Validate access to files
            if (!UserAccessHelper::authUserHasAccessToFile($file)) {
                Log::error('MyDawonloads::startDownload Missing access for protected file:' . $file->id);
                return response('<script>parent.msg("' . trans('system.mydownloads.notfound') . '")</script>', 403);
            }

            $filePath = realpath(public_path($file->getPath(FILEITEM_REAL_PATH)));
            if (!$filePath) {
                Log::warning("File physically doesn't exist: " .$file->getPath(FILEITEM_REAL_PATH));
                return response('<script>parent.msg("' . trans('system.mydownloads.notfound') . '")</script>', 404);
            }
            // Release session to be able to go to other pages
            session_write_close();

            $finfo     = new \finfo(FILEINFO_MIME);
            $mime_type = $finfo->file($filePath);
            $fileSize  = @filesize($filePath);
            return response()->stream(function() use ($filePath, $fileSize) {
                    $stream = fopen($filePath, 'rb');
                    // Disable output buffering if it is enabled and remember for later use
                    while (ob_get_level() > 0) { 
                        ob_end_flush();
                    }
                    $countBytes = fpassthru($stream);
                    if ($fileSize <> $countBytes) {
                        Log::error('Wrong streamed bytes for file: ' . $filePath . ' size/streamed: ' . $fileSize . '/' . $countBytes . ' error: ' . self::getLastErrorMsg());
                    }
                    if (is_resource($stream)) {
                        fclose($stream);
                    }
                }, 200, [
                    'Cache-Control'         => 'must-revalidate, post-check=0, pre-check=0',
                    'Content-Type'          => $mime_type,
                    'Content-Length'        => $fileSize,
                    'Content-Disposition'   => 'attachment; filename="' . basename($filePath) . '"',
                    'Pragma'                => 'public',
                ]);
        } else {
            return $this->packFilesAndStream($files);
        }
    }

    private function packFilesAndStream($files): \Symfony\Component\HttpFoundation\Response {
        // Ignore user aborts and continue run script to delete temp zip file
        $previous_user_abbort = ignore_user_abort(true);
        //Log::info('Ignore user aborts and continue run script. (previous settings = ' . $previous_user_abbort);
        $zip_temp_folder      = FileHelper::getSysTempDir(false);

        // Check free space
        $temp_total_bytes     = disk_total_space($zip_temp_folder);
        $temp_free_bytes      = disk_free_space($zip_temp_folder);
        Log::info(sprintf(
            'Zip temp Free/Total: %s / %s',
            self::bytes2HumanRedable($temp_free_bytes),
            self::bytes2HumanRedable($temp_total_bytes)
        ));

        // Scan the content of temp folder
        //$temp_files = scandir($zip_temp_folder);
        //Log::info('Zip temp files: ' . print_r($temp_files, true) );
        // Get total size of files
        $total = 0;
        foreach ($files as $item) {
            // Validate access to files
            if (!UserAccessHelper::authUserHasAccessToFile($item)) {
                Log::error('MyDawonloads::startDownload Missing access for protected file:' . $item->id);
                return response('<script>parent.msg("' . trans('system.mydownloads.notfound') . '")</script>', 403);
            }

            $total += $item->size;
        }
        unset($item);
        // Check if total is bigger then limit or 90% of temp free space
        if ($total > min(config('app.downloads_limit', 120000000), 0.9 * $temp_free_bytes)) {
            return response('<script>parent.msg("' . json_encode(trans('system.downloads_list.overlimit')) . '")</script>', 400);
        }

        // Release session to be able to go to other pages
        session_write_close();

        // Create temporary zip file
        $download_filename = 'Daimler_BDN_Downloads_' . date("ymd_Hi") . '.zip';
        $zipfile           = tempnam($zip_temp_folder, "zip");
        Log::info('Create temp zip file: ' . $zipfile . ' for file: ' . $download_filename . ' error: ' . self::getLastErrorMsg());
        $zip               = new \ZipArchive();
        // Zip will open and overwrite the file, rather than try to read it.
        if ($zip->open($zipfile, \ZipArchive::OVERWRITE) === true) {
            foreach ($files as $file) {
                $filePath  = realpath(public_path($file->getPath(FILEITEM_REAL_PATH)));
                $isSuccess = $zip->addFile($filePath, basename($filePath));
                if ($isSuccess !== true) {
                    Log::error('Unable to add file: ' . $filePath . ' to zip: ' . $zipfile . ' error: ' . self::getLastErrorMsg());
                }
            }
            if (true === $zip->close()) {
                // Stream the file to the client
                $fileSize  = @filesize($zipfile);
                return response()->stream(function() use ($zipfile, $fileSize) {
                        $stream = fopen($zipfile, 'rb');
                        // Disable output buffering if it is enabled and remember for later use
                        while (ob_get_level() > 0) {
                            ob_end_flush();
                        }
                        $countBytes = fpassthru($stream);
                        if ($fileSize <> $countBytes) {
                            Log::error('Wrong streamed bytes for zip file: ' . $filePath . ' size/streamed: ' . $fileSize . '/' . $countBytes . ' error: ' . self::getLastErrorMsg());
                        } else {
                            Log::info('Just read the temp zip file: ' . $zipfile . ' size: ' . @filesize($zipfile) . ' error: ' . self::getLastErrorMsg());
                        }
                        if (is_resource($stream)) {
                            fclose($stream);
                        }
                        self::cleanTempZipFile($zipfile);
                    }, 200, [
                        'Cache-Control'         => 'must-revalidate, post-check=0, pre-check=0',
                        'Content-Type'          => 'application/zip; charset=binary',
                        'Content-Length'        => $fileSize,
                        'Content-Disposition'   => 'attachment; filename="' . $download_filename . '"',
                        'Pragma'                => 'public',
                    ]);
            } else {
                $err_msg = "" . self::getLastErrorMsg();
                $err_msg .= " zip_err: " . $zip->getStatusString();
                Log::error('Unable to close temp zip file: ' . $zipfile . ' size: ' . @filesize($zipfile) . ' error: ' . $err_msg);
                self::cleanTempZipFile($zipfile);
                return response('<script>parent.msg("' . json_encode(trans('system.downloads_list.overlimit')) . '")</script>', 400);
            }
        } else {
            Log::error('Unable to open temp zip file: ' . $zipfile . ' error: ' . self::getLastErrorMsg());
            return response('<script>parent.msg("' . json_encode(trans('system.mydownloads.failed')) . '")</script>', 400);
        }
    }

    private static function cleanTempZipFile(string $zipfile) {
        // Delete temporary zip file
        if (true !== @unlink($zipfile)) {
            Log::error('Unable to delete temp zip file: ' . $zipfile . ' size: ' . @filesize($zipfile) . ' error: ' . self::getLastErrorMsg());
        } else {
            Log::info('Just deleted the temp zip file: ' . $zipfile . ' size: ' . @filesize($zipfile) . ' error: ' . self::getLastErrorMsg());
        }
    }
}

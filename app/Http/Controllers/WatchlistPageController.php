<?php

namespace App\Http\Controllers;

use App\Helpers\FileHelper;
use App\Helpers\FETeaserHelper;
use App\Helpers\UserAccessHelper;
use App\Models\Page;
use App\Models\Subscription;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;

class WatchlistPageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $subscriptions = Subscription::page()
            ->where('subscription.user_id', '=', Auth::user()->id)
            ->orderBy('subscription.updated_at', 'desc')
            ->select('page.u_id')
            ->get();

        $blocks = [];
        $idsToDig = [];
        $idsMap = [];
        $u_ids = [];
        foreach ($subscriptions as $sb) {
            $page = Page::find($sb->u_id);
            if ($page) {
                $idsMap[$page->id]                   = $page->u_id;
                $u_ids[]                             = $page->u_id;
                $idsToDig[]                          = $page->id;

                $blocks[$page->id]['slug']           = $page->slug;
                $blocks[$page->id]['title']          = $page->slug;
                $blocks[$page->id]['h1']             = '';
                $blocks[$page->id]['text']           = '';
                $blocks[$page->id]['img']            = '';
                $blocks[$page->id]['tags']           = $page->joinTags(1);
                $blocks[$page->id]['date']           = $page->updated_at;
                $blocks[$page->id]['template']       = $page->template;
                $blocks[$page->id]['visits']         = $page->visits;
                $blocks[$page->id]['comments']       = $page->comments()->count();
                $blocks[$page->id]['ratings']        = $page->ratings()->count();
                $blocks[$page->id]['type']           = $page->template == 'downloads'? FileController::MODE_DOWNLOADS : FileController::MODE_IMAGES;
                $blocks[$page->id]['protected']      = ($page->authorization == 1);
                $blocks[$page->id]['parent_id']      = $page->parent_id;
                $blocks[$page->id]['has_access']     = UserAccessHelper::authUserHasAccess($page);
            }
        }

        if (count($blocks) > 0) {
            $founded = FETeaserHelper::generateDownloadOverviewTeasers($u_ids, $blocks);
            $idsToDig = array_diff($idsToDig, $founded);

            $founded = FETeaserHelper::generateOverviewTeasers($u_ids, $blocks);
            $idsToDig = array_diff($idsToDig, $founded);

            FETeaserHelper::addDownloadDetail($idsToDig, $blocks);
            FETeaserHelper::addDescendantDetails($idsToDig, $blocks);
            if (count($idsToDig)> 0) {
                FETeaserHelper::generateDownloadExtensionImage($u_ids, $blocks);
            }

            foreach ($blocks as $page_id => $block) {
                if ($blocks[$page_id]['img'] == '') {
                    $blocks[$page_id]['type'] = FileHelper::MODE_BRAND_THUMB;
                    // Find the brand thumbnail
                    $ascendants = Page::ascendants($page_id, $this->lang)->get();
                    $sectionId = $blocks[$page_id]['parent_id'] == HOME ? $page_id : DispatcherHelper::getActiveL2($ascendants);
                    $blocks[$page_id]['img'] = DispatcherHelper::getBrandThumbnail($sectionId);
                }
            }
        }
        //echo '<!-- ';
        //$queries = DB::getQueryLog();
        //var_dump($queries);
        //echo '-->';

        if (Session::get('lang') == 1) {
            $altLang = 2;
        } else {
            $altLang = 1;
        }

        return view(
            'watchlist',
            [  'PAGE'          => (object)['id' => 'Watchlist', 'slug'=>'Watchlist', 'template'=>'downloads', 'brand' => '', 'langs' => 3],
                                    'ALT_LANG' => $altLang,
                                    'SUBSCRIPTIONS' => $blocks,
                            ]
        );
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($id = false)
    {
        try {
            $pageID = $id === false ? Request::get('pageid') : $id;
            if (is_numeric($pageID)) {
                $subscription = Subscription::where('user_id', Auth::user()->id)->where('page_id', $pageID)->withTrashed()->first();
                if ($subscription) {
                    Subscription::where('user_id', Auth::user()->id)->where('page_id', $pageID)->withTrashed()->update(['active' => 1, 'deleted_at'=>null]);
                } else {
                    $subscription = new Subscription;
                    $subscription->user_id = Auth::user()->id;
                    $subscription->page_id = $pageID;
                    $subscription->save();
                }
            }
            Session::put('watchlistCount', Subscription::where('user_id', '=', Auth::user()->id)->where('active', 1)->get()->count());

        //	return Response::json( array('watchlistCount'=> Session::get('watchlistCount')) );
        //return redirect('/watchlist');
            if (isset($_SERVER['HTTP_REFERER'])) {
                return Redirect::back()->withErrors(['success' => trans('system.watchlist.added')]);
            } else {
                return redirect('Watchlist');
            }
        } catch (\Exception $e) {
            Log::error($e);
            if (isset($_SERVER['HTTP_REFERER'])) {
                return Redirect::back()->withErrors([trans('system.watchlist.couldnt_save')]);
            } else {
                return redirect('Watchlist');
            }
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id = false)
    {
        $pageIDs = ($id === false ? Request::get('pageid') : $id);
        if (is_array($pageIDs)) {
            DB::table('subscription')
                    ->where('user_id', '=', Auth::user()->id)
                    ->whereIn('page_id', $pageIDs)
                    ->update(['deleted_at' =>date('Y-m-d H:i:s'), 'active' => 0]);
        } else if (is_numeric($pageIDs)) {
            DB::table('subscription')
                    ->where('user_id', '=', Auth::user()->id)
                    ->where('page_id', $pageIDs)
                    ->update(['deleted_at' =>date('Y-m-d H:i:s'), 'active' => 0]);
        }
        #Session::put('myDownloadsCount', Cart::where('user_id','=',Auth::user()->id)->get()->count() );
        Session::put('watchlistCount', Subscription::where('user_id', '=', Auth::user()->id)->where('active', 1)->get()->count());
        if (isset($_SERVER['HTTP_REFERER'])) {
            return Redirect::back()->withErrors(['success' => trans('system.watchlist.removed')]);
        } else {
            return redirect('Watchlist');
        }
        //return redirect('/watchlist');
        //return Response::json( array('watchlistCount'=> Session::get('watchlistCount')) );
    }
}

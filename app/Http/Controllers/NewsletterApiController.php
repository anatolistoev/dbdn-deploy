<?php

namespace App\Http\Controllers;

use App\Exception\CopyPageException;
use App\Exception\ModelValidationException;
use App\Models\Newsletter;
use App\Models\NewsletterContent;
use App\Models\NewsletterSent;
use App\Models\Page;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use LaravelArdent\Ardent\InvalidModelException;

/**
 * Description of PageController
 *
 * @author m.stefanov
 */
class NewsletterApiController extends RestController
{

    /**
     * Return Newsletter by ID
     * URL: newsletter/read/{ID}
     * METHOD: GET
     */
    public function getRead($id = null)
    {

        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }

        $newsletter = Newsletter::find($id);

        if (is_null($newsletter)) {
            return Response::json(RestController::generateJsonResponse(true, 'Newsletter not found.'), 404);
        } else {
            $titles = NewsletterContentController::getNewsletterContent($id, 'title');
            if (count($titles) > 0) {
                $newsletter->titles = $titles;
            }
            return Response::json(RestController::generateJsonResponse(false, 'Newsletter found.', $newsletter));
        }
    }

    /**
     * Return List of Newsletter the user is Responsible for
     * URL: newsletter/responsible/
     * METHOD: GET
     */
    public function getResponsible()
    {
        $req = Request::all();
        
        if (empty($req['lang'])) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }

        $query = Newsletter::join('newsletter_content as nc', 'nc.newsletter_id', '=', 'newsletter.id')
                        ->where('nc.lang', '=', $req['lang'])
                        ->where('nc.position', '=', 'title')
                        ->where('newsletter.user_responsible', '=', Auth::user()->id)
                        ->where('newsletter.editing_state', '<>', STATE_APPROVED)
                        ->select('newsletter.*', 'nc.content as title');
        $newsletters = $query->get();
        foreach ($newsletters as $key => $n) {
            $user = User::find($n->assigned_by);
            if ($user) {
                $n->assigned_by = $user->username.' ('.$user->resolveRole().')';
            } else {
                $n->assigned_by = '';
            }

            if (strtotime($n->due_date) <= 0) {
                $n->due_date = trans('backend_newsletter.no_due_date');
            } else {
                $n->due_date = date('d.m.Y H:i', strtotime($n->due_date));
            }
        }

        $return = Response::json(RestController::generateJsonResponse(false, 'List of newsletter found.', $newsletters->toArray()), 200);
        return $return;
    }

    /**
     * Return List of Newsletters
     * URL: newsletter/all/
     * METHOD: GET
     */
    public function getAll()
    {
        $req = Request::all();

        $sentCountQuery = NewsletterSent::select(DB::raw('count(*)'))
            ->where('newsletter_id', DB::raw('newsletter.id'))
            ->where(function ($query) {
                        $query->where('status', 1)
                            ->orWhere('retries', '>', 2);
            });

        // Sent Count SQL
        $sqlSentCount = getSql($sentCountQuery);
        // Total Subscribers
        $subscribersCount = User::where('newsletter', 1)->where('newsletter_include', 1)->count();

        $query = Newsletter::join('newsletter_content as nc', 'nc.newsletter_id', '=', 'newsletter.id')
                        ->where('nc.lang', '=', $req['lang'])
                        ->where('nc.position', '=', 'title')
                        ->select([
                            DB::raw('newsletter.*'),
                            DB::raw('nc.content as title'),
                            DB::raw('('. $sqlSentCount .') as sent_emails'),
                            DB::raw($subscribersCount .' as all_subscribers')]);
        $nls = $query->get();

        $return = Response::json(RestController::generateJsonResponse(false, 'List of newsletter found.', array_values($nls->toArray())), 200);
        return $return;
    }

    /**
     * Return Search result of pages
     * URL: newsletter/search/
     * METHOD: GET
     */
    public function getSearch()
    {

        $req = Request::all();

        if (empty($req['filter'])) {
            $filter = null;
        } else {
            $filter = $req['filter'];
        }

        $query = Newsletter::join('newsletter_content as nc', 'nc.newsletter_id', '=', 'newsletter.id')
                        ->where('nc.lang', '=', $req['lang'])
                        ->where('nc.position', '=', 'title')
                        ->select(DB::raw('newsletter.*,nc.content as title'))
                        ->where('nc.content', 'LIKE', '%' . $filter . '%');
        $newsletters = $query->get();

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of newsletters found.', array_values($newsletters->toArray())), 200);
        return $jsonResponse;
    }

    /**
     * Return Send external newsletter to users
     * URL: newsletter/send
     * METHOD: POST
     */
    public function postSend()
    {

        $req = Request::json()->all();
        if (empty($req['id'])) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }

        $newsletter = Newsletter::find($req['id']);

        if (!$newsletter) {
            return Response::json(RestController::generateJsonResponse(true, "Newsletter with this ID doesn't exists"), 400);
        }

        if (!in_array(Auth::user()->role, config('auth.CMS_NL_APPROVER'))) {
            return Response::json(RestController::generateJsonResponse(true, "You are not atorised to send newsletters"), 400);
        }

        //Mark newsletter for sending
        $newsletter->status = STATE_SENDING;
        $this->trySave($newsletter, true);

        $limit = 500; // 100000
        //There is timeout after 600 sec. For this time we send about ~690 emails

        $params = [
                    '--id'         => $newsletter->id,
                    '--test-mode'  => false,
                    '--test-list'  => false,
                    '--dry-run'    => false,
                    '--send-limit' => $limit,
                ];

        $result = Artisan::call('SendNewsletterEmails', $params);

        $cres = config('commandResult');

		Log::info(" Newsletter result: " . print_r($cres, 1));        

        if (0 == $result) {
            return Response::json(RestController::generateJsonResponse(
                false,
                "The newsletter has been sent successfully to {$cres['sent']} subscribers.<br>There are {$cres['duplicate']} duplications.<br>The newsletter sending has been failed to {$cres['fail']} subscribers.",
                $newsletter
            ));
        } else {
            return Response::json(RestController::generateJsonResponse(
                true,
                "Newsletter couldn't send to recipients"
            ), 400);
        }
    }

    /**
     * Send newsletter to test emails
     * URL: newsletter/sendtest
     * METHOD: POST
     */
    public function postSendtest()
    {

        $req = Request::json()->all();
        // Validation
        if (!in_array(Auth::user()->role, config('auth.CMS_NL_EDITOR'))) {
            return Response::json(RestController::generateJsonResponse(true, "You are not autorised to send newsletters."), 400);
        }
        if (empty($req['id'])) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        if (empty($req['test_list']) && empty($req['emails'])) {
            return Response::json(RestController::generateJsonResponse(true, "Emails are requared."), 400);
        }

        $newsletter = Newsletter::with(['contents' => function ($query) {
                            return $query->orderBy('lang');
        }
                    ])->find($req['id']);
        //$newsletter = Newsletter::find($req['id']);
        if (!$newsletter) {
            return Response::json(RestController::generateJsonResponse(true, "Newsletter with this ID doesn't exists"), 400);
        }
        $fullContent = [1 => ['title','header_d','header_m','footer_i','footer_e','content'], 2=> ['title','header_d','header_m','footer_i','footer_e','content'], 3=>['footer_e_bl']];
        foreach ($newsletter->contents as $key => $content) {
            $sKey = array_search($content->position, $fullContent[$content->lang]);
            if ($sKey !== false) {
                unset($fullContent[$content->lang][$sKey]);
            }
        }
        if (count($fullContent[1]) > 0 || count($fullContent[2]) > 0 || count($fullContent[3]) > 0) {
            return Response::json(RestController::generateJsonResponse(true, "Please note, a preview message cannot be sent yet, because the newsletter contains empty fields in both or in one of the language versions."), 400);
        }

        if (empty($req['test_list'])) {
            $test_list = false;
            $send_limit = 1;
        } else {
            $test_list = true;
            $send_limit = 100;
        }

        $params = [
                    '--id'         => $newsletter->id,
                    '--test-mode'  => false,
                    '--test-list'  => $test_list,
                    '--dry-run'    => false,
                    '--send-limit' => $send_limit,
                ];

        if ($test_list) {
            $result = Artisan::call('SendNewsletterEmails', $params);

            $cres = config('commandResult');

            $sended = $cres['sent'];
            $msg = "There ".($sended > 1 ? 'are '.$sended.' emails sent':'is '.$sended.' email sent')
                    .( $cres['fail'] > 0 ?' and '.$cres['fail'].' emails are invalid.' :'.');
        } else {
            $emails = explode(',', $req['emails']);
            $sended = 0;
            $invalidAddres = [];
            foreach ($emails as $key => $email) {
                $trimmed = trim($email);
                if (filter_var($trimmed, FILTER_VALIDATE_EMAIL)) {
                    $params['--email'] = $trimmed;
                    $params['--name'] = 'Test user';
                    $result = Artisan::call('SendNewsletterEmails', $params);

                    $cres = config('commandResult');

                    // if($result){
                    if (1 == $cres['sent']) {
                        $sended ++;
                    } else {
                        $invalidAddres[] = $trimmed;
                    }
                } else {
                    $invalidAddres[] = $trimmed;
                }
            }
            $msg = "There ".($sended > 1 ? 'are '.$sended.' emails sent':'is '.$sended.' email sent')
                    .(count($invalidAddres) > 0 ?' and emails: '.implode(", ", $invalidAddres).' are invalid.' :'.');
        }

        return Response::json(RestController::generateJsonResponse(false, $msg));
    }

    /**
     * Return zippped newsletter for internal users
     * URL: newsletter/package/{ID}
     * METHOD: POST
     */
    public function getPackage($id = null)
    {

        if ($id != null && !is_numeric($id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }

        $newsletter = Newsletter::find($id);
        if (!$newsletter) {
            return Response::json(RestController::generateJsonResponse(true, "Newsletter with this ID doesn't exists"), 400);
        }
        if (!($newsletter->editing_state == STATE_APPROVED  && $newsletter->active == 1)) {
            return Response::json(RestController::generateJsonResponse(true, "Newsletter is not in correct state to be sent"), 400);
        }
        if (!in_array(Auth::user()->role, config('auth.CMS_NL_APPROVER'))) {
            return Response::json(RestController::generateJsonResponse(true, "You are not atorised to send newsletters"), 400);
        }
        $newsletterController = App::make(\App\Http\Controllers\NewsletterController::class);
        return $newsletterController->tozip($newsletter->id);
    }

    /**
     * Return Save newsletter from JSON
     * URL: newsletter/create
     * METHOD: POST
     */
    public function postCreate()
    {

        $new_newsletter = Request::json()->all();

        $newsletter = new Newsletter();
        $newsletter->fill($new_newsletter);
        $newsletter->created_by = Auth::user()->id;
        //while workflow is not in Approved state page must be unactive
        $newsletter->active = 0;

        return $this->trySave($newsletter, false);
    }
    /**
     * Return Update Newsletter from JSON
     * URL: newsletter/update/
     * METHOD: PUT
     */
    public function putUpdate()
    {
        $updatenewsletter = Request::json()->all();
        //TODO validate
        if (empty($updatenewsletter['id'])) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }

        try {
            DB::beginTransaction();
            $newsletter = Newsletter::find($updatenewsletter['id']);
            if (is_null($newsletter)) {
                return Response::json(RestController::generateJsonResponse(true, 'Newsletter not found.'), 404);
            }
            if (self::isNewsletterInEditMode($newsletter)) {
                return Response::json(RestController::generateJsonResponse(true, trans('backend_newsletter.editing')), 400);
            }
            if (isset($updatenewsletter['active']) && $updatenewsletter['active'] && $newsletter->editing_state != STATE_APPROVED) {
                    return Response::json(RestController::generateJsonResponse(true, trans('backend_newsletter.active.not_approved')), 400);
            }
            if (isset($updatenewsletter['editing_state'])) {
                $approverRoles = config('auth.CMS_NL_APPROVER');
                if (!in_array(Auth::user()->role, $approverRoles)) {
                    if ( $newsletter->editing_state == \STATE_APPROVED
                        || ($newsletter->editing_state != \STATE_DRAFT && $updatenewsletter['editing_state'] != \STATE_REVIEW)) {
                            return Response::json(RestController::generateJsonResponse(true, trans('backend_newsletter.workflow.editor.change_state')), 403);
                    }
                }

                if ($updatenewsletter['editing_state'] != STATE_DRAFT) {
                    $user = User::find($updatenewsletter['user_responsible']);
                    if (is_null($user) || !in_array($user->role, $approverRoles)) {
                        return Response::json(RestController::generateJsonResponse(true, trans('backend_newsletter.workflow.attached.approver')), 403);
                    }

                    $fullContent = [1 => ['title','header_d','header_m','footer_i','footer_e','content'], 2=> ['title','header_d','header_m','footer_i','footer_e','content'], 3 => ['footer_e_bl']];
                    foreach ($newsletter->contents as $key => $content) {
                        $sKey = array_search($content->position, $fullContent[$content->lang]);
                        if ($sKey !== false) {
                            unset($fullContent[$content->lang][$sKey]);
                        }
                    }
                    if (count($fullContent[1]) > 0 || count($fullContent[2]) > 0 || count($fullContent[3]) > 0) {
                        return Response::json(RestController::generateJsonResponse(true, "The status cannot be changed until the newsletter contains empty fields in both or in one of the language versions."), 400);
                    }
                }
            }

            if (isset($updatenewsletter['user_responsible']) && $updatenewsletter['user_responsible'] != $newsletter->user_responsible) {
                $user = User::find($updatenewsletter['user_responsible']);
                $editorsRoles = config('auth.CMS_NL_EDITOR');
                if (is_null($user) || !in_array($user->role, $editorsRoles)) {
                    return Response::json(RestController::generateJsonResponse(true, trans('backend_newsletter.workflow.attached.approver')), 403);
                }
                $updatenewsletter['assigned_by'] = Auth::user()->id;
            }

            $newsletter->fill($updatenewsletter);
            $result = $this->trySave($newsletter, true);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        return $result;
    }

    /**
     * Return Copied Newsletter like JSON
     * URL: newsletter/copy/
     * METHOD: POST
     */
    public function postCopy()
    {
        $updatenewsletter = Request::json()->all();
        $pages = [];
        if (empty($updatenewsletter['id'])) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        $newsletter = Newsletter::find($updatenewsletter['id']);
        if (is_null($newsletter)) {
            return Response::json(RestController::generateJsonResponse(true, 'Newsletter not found.', $updatenewsletter), 404);
        }

        unset($updatenewsletter['action']);
        $newsletter->fill($updatenewsletter);
        $copyNewsletter = new Newsletter();
        $copyNewsletter->fill($newsletter->getAttributes());
        $copyNewsletter->active = 0;
        $copyNewsletter->end_edit = null;
        $copyNewsletter->user_edit_id = null;
        $copyNewsletter->user_edit_id = null;
        $copyNewsletter->assigned_by = 0;
        $copyNewsletter->user_responsible = 0;
        $copyNewsletter->slug = $newsletter->slug . '_copy';
        $copyNewsletter->created_by = Auth::user()->id;

        //FIXME: Empty Response?
        $result;
        try {
            DB::beginTransaction();

            $nlResult = $this->trySave($copyNewsletter, false);
            if (!$nlResult->isSuccessful()) {
                $error = json_decode($nlResult->getContent());
                $debugmsg = "";
                if (isset($error->debug)) {
                    $debugmsg = ' Debug message: ' . $error->debug->exception_message;
                }
                $message = $error->message . $debugmsg;
                throw new CopyPageException($message);
            }

            $result = $nlResult;

            $newNlID = json_decode($nlResult->getContent())->response->id;
            $copyNewsletter->id = $newNlID;
            $contentArray = new Collection();
            foreach ($newsletter->contents as $content) {
                unset($content->id);
                $newContent = new NewsletterContent();
                $newContent->fill($content->getAttributes());
                $newContent->newsletter_id = $newNlID;
                $contentArray->add($newContent);
            }

            $contentResult = App::make(\App\Http\Controllers\NewsletterContentController::class)->trySave($contentArray, false);

            if (!$contentResult->isSuccessful()) {
                $error = json_decode($contentResult->getContent());
                $debugmsg = "";
                if (isset($error->debug)) {
                    $debugmsg = ' Debug message: ' . $error->debug->exception_message;
                }

                $message = $error->message . $debugmsg;
                throw new CopyPageException($message);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            throw $e;
        }

        return $result;
    }

    public function putChangeresponsible()
    {
        $update_wf = Request::json()->all();

        if (empty($update_wf['newsletter_id'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter Newsletter ID is required!'), 400);
        }

        $nl = Newsletter::find($update_wf['newsletter_id']);
        if (is_null($nl)) {
            return Response::json(RestController::generateJsonResponse(true, 'Newsletter not found.', $update_wf), 404);
        }

        $user = User::find($update_wf['user_responsible']);
        if ($nl->editing_state != STATE_DRAFT) {
            if (is_null($user) || !in_array($user->role, config('auth.CMS_NL_APPROVER'))) {
                return Response::json(RestController::generateJsonResponse(true, trans('backend_newsletter.attached.approver')), 403);
            }
        }

        $editorsRoles = config('auth.CMS_NL_EDITOR');
        if (is_null($user) || !in_array($user->role, $editorsRoles)) {
            return Response::json(RestController::generateJsonResponse(true, trans('backend_newsletter.workflow.attached.approver')), 403);
        }

        $nl->user_responsible = $user->id;
        $nl->assigned_by      = Auth::user()->id;
        return $this->trySave($nl, true);
    }

    /**
     * Return save result
     */
    protected function trySave(Newsletter $my_object, $isUpdate)
    {
        try {
            // Start transaction!
            DB::beginTransaction();

            if ($isUpdate) {
                $message = "Newsletter was updated.";
            } else {
                $message = "Newsletter was added.";
            }

            if (!$isUpdate) {
                $my_object->status = STATE_DRAFT;
                $my_object->editing_state = STATE_DRAFT;
            }

            $my_object->save();

            //tasks table after edit row require this information
            if ($my_object) {
                $user = User::find($my_object->assigned_by);
                if ($user) {
                    $my_object->assigned_by = $user->username.' ('.$user->resolveRole().')';
                } else {
                    $my_object->assigned_by = '';
                }
                if (strtotime($my_object->due_date) == 0) {
                    $my_object->due_date = trans('backend_pages.no_due_date');
                } else {
                    $my_object->due_date = date('d.m.Y H:i', strtotime($my_object->due_date));
                }
            }

            DB::commit();

            $result = Response::json(RestController::generateJsonResponse(false, $message, $my_object));
        } catch (InvalidModelException $e) {
            // Rollback and then return errors
            DB::rollback();

            throw new ModelValidationException($e->getErrors());
        } catch (\Exception $e) {
            // Rollback and then return errors
            DB::rollback();

            $message = $e->getMessage();
            if (0 === strpos($message, 'SQLSTATE[23000]')) {
                $errorArray = RestController::generateJsonResponse(true, 'Duplicate record.');
            } else {
                $errorArray = RestController::generateJsonResponse(true, $message);
            }
            if (config('app.debug')) {
                // Add stack trace
                $errorArray['debug'] = ['exception_message' => $message, 'stacktrace' => $e->getTraceAsString()];
            }

            $result = Response::json($errorArray, 500);
        }
        return $result;
    }

    /**
     * Return Delte Newsletter from JSON
     * URL: pages/delete/{ID}
     * METHOD: DELETE
     */
    public function deleteDelete($id = null)
    {
        // Validation
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        try {
            // Start transaction!
            DB::beginTransaction();
            $newsletter = Newsletter::find($id);

            if (is_null($newsletter)) {
                return Response::json(RestController::generateJsonResponse(true, 'Newsletter not found.', $newsletter), 404); // in response $page not exists
            }
            if (self::isnewsletterInEditMode($newsletter)) {
                 return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.newsletter.edited')), 400);
            }

            $deletednewsletter = $newsletter;
            $newsletter->delete();
            // Commit the queries!
            DB::commit();

            $result = Response::json(RestController::generateJsonResponse(false, 'Newsletter was deleted.', $deletednewsletter));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            // Rollback and then return errors
            DB::rollback();

            throw new ModelValidationException($e->getErrors());
        } catch (\Exception $e) {
            // Rollback and then return errors
            DB::rollback();

            $message = $e->getMessage();
            $errorArray = RestController::generateJsonResponse(true, $message);
            if (config('app.debug')) {
                // Add stack trace
                $errorArray['debug'] = ['exception_message' => $message, 'stacktrace' => $e->getTraceAsString()];
            }

            $result = Response::json($errorArray, 500);
        }

        return $result;
    }

    /**
     * Force Delete PAge for tests
     * URL: pages/forcedelete/{ID}
     * METHOD: DELETE
     */
    public function deleteForcedelete($id = null)
    {
        $page = Page::withTrashed()->find($id);
        $page->forceDelete();
    }

    /**
     * Return is newsletter available for editing
     * URL: newsletter/isavailable/{$newsletterId}
     * METHOD: GET
     */
    public function getIsavailable($newsletterId)
    {

        if (empty($newsletterId)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        $result = $this->checkAvailability($newsletterId);
        if ($result->status) {
            return Response::json(RestController::generateJsonResponse(false, 'You can edit this newsletter', $result), 200);
        } else if (!$result->status && !isset($result->error)) {
            return Response::json(RestController::generateJsonResponse(true, trans('backend_newsletter.edited')), 400);
        } else if (!$result->status && $result->error == "admin_only") {
            return Response::json(RestController::generateJsonResponse(true, trans('backend_newsletter.state_error', ['name'=>$result->name])), 404);
        } else if (!$result->status && $result->error == "") {
            return Response::json(RestController::generateJsonResponse(true, trans('backend_newsletter.not_found')), 404);
        } else if (!$result->status && $result->error) {
             return Response::json(RestController::generateJsonResponse(true, $result->error), 500);
        } else {
            return Response::json(RestController::generateJsonResponse(true, trans('backend_newsletter.not_found')), 404);
        }
    }

    /**
     * Return is newsletter available for editing
     * URL: newsletter/appendtime
     * METHOD: POST
     */
    public function postAppendtime()
    {
        $input = Request::json()->all();
        $newsletterId = $input['newsletterId'];
        if (empty($newsletterId)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        try {
            DB::beginTransaction();
            $newsletter = Newsletter::find($newsletterId);
            if (is_null($newsletter)) {
                return Response::json(RestController::generateJsonResponse(true, 'Newsletter not found.'), 404);
            } else if (self::isNewsletterInEditMode($newsletter)) {
                 return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.newsletter.edited')), 400);
            } else {
                if ($newsletter->end_edit) {
                    $newsletter->end_edit = date('Y-m-d H:i:s', strtotime("+" . config('app.edit_max_time_seconds') . " seconds", strtotime($newsletter->end_edit)));
                } else {
                    $newsletter->end_edit = date('Y-m-d H:i:s', strtotime("+" . config('app.edit_max_time_seconds') . " seconds"));
                }
                $newsletter->user_edit_id = Auth::user()->id;
                $newsletter->save();
                DB::commit();
                $result = new \stdClass();
                $result->status = true;
                $result->time_left = self::calculateTimeLeftForEdit($newsletter);
                return Response::json(RestController::generateJsonResponse(false, 'You can edit this newsletter', $result), 200);
            }
        } catch (\Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            if (config('app.debug')) {
                // Add stack trace
                $errorArray['debug'] = ['exception_message' => $message, 'stacktrace' => $e->getTraceAsString()];
            }
            return Response::json(RestController::generateJsonResponse(true, $message), 500);
        }
    }

    /**
     * Unlock newsletter for editing
     * URL: newsletter/unlock
     * METHOD: POST
     */
    public function postUnlock()
    {
        $input = Request::json()->all();
        $newsletterId = $input['newsletterId'];
        if (empty($newsletterId)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }
        try {
            DB::beginTransaction();
            $newsletter = Newsletter::find($newsletterId);
            $response = '';
            if (is_null($newsletter)) {
                $response = Response::json(RestController::generateJsonResponse(true, 'Newsletter not found.'), 404);
            } else if (self::isNewsletterInEditMode($newsletter)) {
                $response = Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.newsletter.edited')), 400);
            } else {
                $newsletter->end_edit = null;
                $newsletter->user_edit_id = null;
                $newsletter->save();
                $response = Response::json(RestController::generateJsonResponse(false, 'Newsletter is unlocked', $newsletter), 200);
            }
            DB::commit();
            return $response;
        } catch (\Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            if (config('app.debug')) {
                // Add stack trace
                $errorArray['debug'] = ['exception_message' => $message, 'stacktrace' => $e->getTraceAsString()];
            }
            return Response::json(RestController::generateJsonResponse(true, $message), 500);
        }
    }

    static function checkAvailability($newsletterId)
    {
        $result = new \stdClass();
        try {
            DB::beginTransaction();
            $newsletter = Newsletter::find($newsletterId);
            if (is_null($newsletter)) {
                $result->status = false;
                $result->error = "";
            } else if (self::isNewsletterInEditMode($newsletter)) {
                $result->status = false;
            } else if (($newsletter->editing_state != STATE_DRAFT && !in_array(Auth::user()->role, config('auth.CMS_NL_APPROVER'))) ||
                    ($newsletter->user_responsible > 0 && $newsletter->editing_state == STATE_DRAFT && Auth::user()->role == USER_ROLE_NEWSLETTER_EDITOR && $newsletter->user_responsible != Auth::user()->id )) {
                $result->status = false;
                $result->error = "admin_only";
                $result->name = "";
                if ($newsletter->user_responsible>0) {
                    $user = User::find($newsletter->user_responsible);
                    $result->name = $user->first_name.' '.$user->last_name;
                }
            } else {
                if ($newsletter->end_edit && $newsletter->user_edit_id == Auth::user()->id && strtotime($newsletter->end_edit) > strtotime(EDIT_TIME_BUFFER_INTERVAL)) {
                    $result->status = true;
                } else {
                    $newsletter->end_edit = date('Y-m-d H:i:s', strtotime("+" . config('app.edit_max_time_seconds') . " seconds"));
                    $newsletter->user_edit_id = Auth::user()->id;
                    $newsletter->save();
                    $result->status = true;
                }
                $result->time_left = self::calculateTimeLeftForEdit($newsletter);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            if (config('app.debug')) {
                // Add stack trace
                $errorArray['debug'] = ['exception_message' => $message, 'stacktrace' => $e->getTraceAsString()];
            }
            $result->status = false;
            $result->error = $message;
        }
        return $result;
    }

    private static function isNewsletterInEditMode($newsletter)
    {
        return ($newsletter->end_edit && $newsletter->user_edit_id != Auth::user()->id
                && strtotime($newsletter->end_edit) > strtotime(EDIT_TIME_BUFFER_INTERVAL));
    }

    // Return the time left for edit in minutes
    public static function calculateTimeLeftForEdit($newsletter)
    {
        $time_left = strtotime($newsletter->end_edit) - strtotime("now");

        Log::debug(" TimeLeft: " . $time_left . " End time: " . $newsletter->end_edit . " / " . strtotime($newsletter->end_edit));

        return $time_left;
    }
}

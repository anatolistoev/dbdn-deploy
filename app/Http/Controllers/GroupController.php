<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Models\Group;
use App\Models\Usergroup;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Description of GroupController
 *
 * @author i.traykov
 */
class GroupController extends RestController
{

    /**
     * Return All Groups
     * URL: group/all
     * METHOD: GET
     */

    public function getAll()
    {

        $req = Request::all();

        $groups = Group::allgroups($req);

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of Groups found', $groups), 200);

        return $jsonResponse;
    }

    /**
     * Return Group by Id
     * URL: user/group/{id}
     * METHOD: GET
     */
    public function getRead($id = null)
    {
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $group = Group::find($id);

        if (is_null($group)) {
            return Response::json(RestController::generateJsonResponse(true, 'Group not found'), 404);
        } else {
            return Response::json(RestController::generateJsonResponse(false, 'Group found', $group));
        }
    }

    /**
     * Create Group
     * URL: group/create
     * METHOD: POST
     */

    public function postCreate()
    {

        $req = Request::all();

        $group = new Group();
        $group->fill($req);

        return $this->trySave($group, false);
    }

    /**
     * Return save result
     */
    protected function trySave($group, $isUpdate)
    {
        if ($isUpdate) {
            $message = "Group was updated.";
        } else {
            $message = "Group was added.";
        }

        try {
            $group->save();
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            throw new ModelValidationException($e->getErrors());
        }

        //Success !!!

        return Response::json(RestController::generateJsonResponse(false, $message, $group));
    }


    /**
     * Return Update Group from JSON
     * URL: group/update/{$id}
     * METHOD: PUT
     */
    public function putUpdate()
    {
        $update_group = Request::json()->all();
        //TODO validate
        if (empty($update_group['id'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $group = Group::find($update_group['id']);
        if (is_null($group)) {
            return Response::json(RestController::generateJsonResponse(true, 'Group not found.', $update_group), 404);
        }

        $group->fill($update_group);

        return $this->trySave($group, true);
    }

    /**
     * Delete Group
     * URL: group/delete
     * METHOD: DELETE
     */

    public function deleteDelete($id = null)
    {
        //parameters id must be set
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $group = Group::find($id);

        if (is_null($group)) {
            return Response::json(RestController::generateJsonResponse(true, 'Group not found.'), 404); // in response group not exists
        }

        Usergroup::where('group_id', '=', $group->id)->delete();

        $deleted_group = $group;

        $deleted_group->delete();

        return Response::json(RestController::generateJsonResponse(false, 'Group was deleted!', $group));
    }

    /**
     * Force Delete Group for test only
     * URL: group/forcedelete
     * METHOD: DELETE
     */

    public function deleteForcedelete($id = null)
    {

        $group = Group::withTrashed()->find($id);
        $group->forceDelete();
    }

    /**
     * Return Group from User Id
     * URL: group/readbyuserid/{$id}
     * METHOD: GET
     */

    public function getReadbyuserid($id = null)
    {

        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $usergroups = Usergroup::where('user_id', '=', $id)->get();

        if (empty($usergroups)) {
            return Response::json(RestController::generateJsonResponse(false, 'Groups found'));
        } else {
            $groups = [];
            foreach ($usergroups as $ug) {
                $groups[] = Group::find($ug->group_id)->toArray();
            }

            return Response::json(RestController::generateJsonResponse(false, 'Groups found', $groups));
        }
    }

    /**
     * Add member to group
     * URL: group/addmember/
     * METHOD: GET
     */

    public function postAddmember()
    {

        $req = Request::all();

        //parameter user Id must be set
        if (!isset($req['user_id']) || $req['user_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter user_id is required!'), 400);
        }
        if (!isset($req['group_id']) || $req['group_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter group_id is required!'), 400);
        }

        $user_added = Usergroup::where('user_id', '=', $req['user_id'])->where('group_id', '=', $req['group_id'])->get();
        if (!$user_added->isEmpty()) {
            return Response::json(RestController::generateJsonResponse(true, "User was added to group."), 200);
        }

        $usergroup_item = new Usergroup();
        $usergroup_item->fill($req);

        try {
            $usergroup_item->save();
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            throw new ModelValidationException($e->getErrors());
        }

        unset($usergroup_item->id);
        $message = "User was added to group.";

        //Success !!!

        return Response::json(RestController::generateJsonResponse(false, $message, $usergroup_item));

    }

    /**
     * Remove User from Group
     * URL: group/removemember
     * METHOD: DELETE
     */

    public function deleteRemovemember()
    {

        $req = Request::all();

        //parameter user Id must be set
        if (!isset($req['user_id']) || $req['user_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter user_id is required!'), 400);
        }
        if (!isset($req['group_id']) || $req['group_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter group_id is required!'), 400);
        }

        $usergroup = Usergroup::where('user_id', '=', $req['user_id'])->where('group_id', '=', $req['group_id'])->first();

        if (is_null($usergroup)) {
            return Response::json(RestController::generateJsonResponse(true, 'User not found in group.'), 404);
        }

        $deleted_usergroup = $usergroup;
        Usergroup::where('user_id', '=', $deleted_usergroup->user_id)->where('group_id', '=', $deleted_usergroup->group_id)->delete();

        return Response::json(RestController::generateJsonResponse(false, 'User was deleted from group!', $usergroup));
    }

    /**
     * Force Remove User from Group for Test
     * URL: group/removemember
     * METHOD: DELETE
     */

    public function deleteForceremovemember()
    {

        $req = Request::all();

        //parameter user Id must be set
        if (!isset($req['user_id']) || $req['user_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter user_id is required!'), 400);
        }
        if (!isset($req['group_id']) || $req['group_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter group_id is required!'), 400);
        }

        $usergroup = Usergroup::withTrashed()->where('user_id', '=', $req['user_id'])->where('group_id', '=', $req['group_id'])->first();

        Usergroup::where('user_id', '=', $usergroup->user_id)->where('group_id', '=', $usergroup->group_id)->forceDelete();
    }
}

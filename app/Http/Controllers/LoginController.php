<?php

namespace App\Http\Controllers;


use App\Helpers\CaptchaHelper;
use App\Helpers\ValidationHelper;
use App\Models\Cart;
use App\Models\Page;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Notifications\Messages\Message;
use Hautelook\Phpass\PasswordHash;

class LoginController extends Controller
{
    const NEWSLETTER_CODE_STRING_LENGTH = 10;
    const UNSUBSCRIBE_CODE_STRING_LENGTH = 10;

    /************************************************************************
     *
     *
     ********************************************************************** */
    public static function login($idslug = null)
    {
        if (Session::get('lang') == 1) {
            $altLang = 2;
        } else {
            $altLang = 1;
        }

        if (Auth::check()) {
            $user = Auth::user();
            return Response::view('profile', [
                'PAGE' => (object)['id' => 'profile', 'template' => 'profile', 'slug' => 'profile', 'langs' => 3],
                'USER' => $user,
                'ALT_LANG' => $altLang,
            ]);
        }


        if ($idslug) {
            if ($idslug == 'Download_Cart' || $idslug == 'Watchlist' || $idslug == 'profile') {
                $page = new \stdClass();
            } else {
                $page = Page::where('slug', '=', $idslug)
                    ->alive()
                    ->haveLang()
                    ->first();
            }
        }


        if (!isset($page)) {
            $idslug = 'home';
        }

        CaptchaHelper::resetCaptcha();

        return Response::view('login', [
            'PAGE' => (object)['id' => $idslug, 'template' => 'login', 'slug' => 'login', 'langs' => 3],
            'ALT_LANG' => $altLang,
            'L2ID' => 0,
        ]);
    }

    /************************************************************************
     *
     *
     ********************************************************************** */
    public static function doLogin($idslug)
    {

        // create our user data for the authentication
        $userdata = [
            'username' => Request::get('username'),
            'password' => Request::get('password'),
            'active' => 1
        ];
        $rules = [
            'captcha' => 'required|same:captchaSecret',
        ];
	
        $fields = [
            'captcha' => Request::get('captcha'),
            'captchaSecret' => Session::get('captcha'),
        ];

        $inputValidator = Validator::make($fields, $rules, ValidationHelper::getFEValidationTranslation());
        
	// Reset capcha to prevent reuse one captcha.
        CaptchaHelper::resetCaptcha();
        
	if ($inputValidator->fails()) {
            return redirect('login/' . $idslug)
                ->withErrors(['captcha' => trans('profile.enter.code')])
                ->withInput(Request::except('password')); // send back the input (minus the password) so that w
        }


        // attempt to do the login
        if (Auth::attempt($userdata)) {
            $user = Auth::user();
            
	    // validation successful!
            $count = Cart::where('user_id', $user->id)->where('active', 1)->get()->count() + DB::table('request_auth')->where('user_id', $user->id)->where('active', 1)->whereNull('deleted_at')->count();
            Session::put('myDownloadsCount', $count);
            Session::put('watchlistCount', Subscription::where('user_id', '=', $user->id)->where('active', 1)->get()->count());
            $user->logins++;
            $user->last_login = date('Y-m-d H:i:s');
            $user->save();
            // Change Sessio ID to prevent session fixation
            Session::regenerate();

            return redirect($idslug);
        } else {
            // die();
            // validation not successful, send back to form
            return redirect('login/' . $idslug)
                ->withErrors(['username' => trans('template.login.error')])
                ->withInput(Request::except('password')); // send back the input (minus the password) so that we can repopulate the form
        }
    }

    /************************************************************************
     *
     *
     ***********************************************************************/
    public static function registrationForm()
    {
        $user = new User();

        CaptchaHelper::resetCaptcha();

        if (Session::get('lang') == 1) {
            $altLang = 2;
        } else {
            $altLang = 1;
        }

        return Response::view('profile', [
            'PAGE' => (object)['id' => 'registration', 'template' => 'registration', 'slug' => 'registration', 'langs' => 3],
            'USER' => $user,
            'ALT_LANG' => $altLang
        ]);
    }

    /************************************************************************
     *
     *
     ***********************************************************************/
    protected function sendActivationMail($user)
    {
        $param = [
            'FIRST_NAME' => $user->first_name,
            'LAST_NAME' => $user->last_name,
            'USERNAME' => $user->username,
            'CONFIRMATION_LINK' => url(App::getLocale() . '/user/activate', ['unlock' => $user->unlock_code]),
            'MAILTYPE' => 'activation_email'
        ];
//		$param = array('MESSAGE' => Lang::get('activation_email.message', $fields));
        Mail::send('emails.user_activate', $param, function ($message) use ($user) {
            $message->to($user->email)
                ->subject(trans('activation_email.subject'))
                ->from(config('mail.from.address'));
        });
    }

    /************************************************************************
     *
     *
     ***********************************************************************/
    protected function sendOptInMail($user)
    {
        $param = [
            'FIRST_NAME' => $user->first_name,
            'LAST_NAME' => $user->last_name,
            'CONFIRMATION_LINK' => url(App::getLocale() . '/optin', ['ncode' => $user->newsletter_code]),
            'MAILTYPE' => 'optin'
        ];
        //Log::debug('Confirm Code for optin  ' . $user->username . ' is ' . url(App::getLocale().'/optin',array('ncode'=> $user->newsletter_code)).' time is '.$user->newsletter_time);
        # re-use email.user_activate view as a wrapper
        Mail::send('emails.user_activate', $param, function ($message) use ($user) {
            $message->to($user->email)
                ->subject(trans('optin.subject'))
                ->from(config('mail.from.address'));
        });
    }

    /************************************************************************
     *
     *
     ***********************************************************************/
    protected function sendOptOutMail($user)
    {
        $param = [
            'FIRST_NAME' => $user->first_name,
            'LAST_NAME' => $user->last_name,
            'MAILTYPE' => 'optout'
        ];

        # re-use email.user_activate view as a wrapper
        Mail::send('emails.user_activate', $param, function ($message) use ($user) {
            $message->to($user->email)
                ->subject(trans('optout.subject'))
                ->from(config('mail.from.address'));
        });
    }

    protected function makeConfirmationValidatorForClientSide()
    {
        $rules = [
	    'agreed' => 'accepted',
            'captcha' => 'required|same:captchaSecret',
        ];
        $fields = [
            'captcha' => Request::get('captcha'),
            'captchaSecret' => Session::get('captcha'),
            'agreed' => Request::get('agreed'),
        ];
        $messages = ValidationHelper::getFEValidationTranslation();

        $inputValidator = Validator::make($fields, $rules, $messages);

        return $inputValidator;
    }

    /************************************************************************
     *
     *
     ***********************************************************************/
    public function register()
    {
        $user = new User();
        $inputErrorMsg = [];
        try {
            // Confirm client side validation
            $inputValidator = $this->makeConfirmationValidatorForClientSide();
            // Log the usage of captcha
            CaptchaHelper::logCaptchaUsage();
            // Reset capcha to prevent reuse one captcha.
            CaptchaHelper::resetCaptcha();

            if ($inputValidator->fails()) {
                $inputErrorMsg = $inputValidator->messages()->toArray();
	    	return Redirect::back()->withErrors($inputValidator)->withInput(Request::all());
	    }

            // Create user
            $user->fill(
                Request::only('username', 'password', 'password_confirmation', 'first_name', 'last_name', 'company', 'department', 'position', 'address', 'code', 'city', 'country', 'email', 'phone', 'fax', 'newsletter')
            );

            // Generate random string with length for unlock code
            $user->unlock_code = str_random(self::NEWSLETTER_CODE_STRING_LENGTH);
            $user->newsletter_lang = $this->lang;

            if ($user->newsletter) {
                // Generate random string with length for newsletter code
                $newsletterCode = str_random(self::NEWSLETTER_CODE_STRING_LENGTH);
                $user->newsletter_code = $newsletterCode;
		$newsletterExpiryDate = Carbon::now()
                    ->addHours(config('app.expire_token_time'))
                    ->format("Y-m-d H:i:s");
                $user->newsletter_time = $newsletterExpiryDate;
            } else {
                $user->newsletter = 0;
                $user->newsletter_include = 0;
                $user->newsletter_code = '';
            }
	
	
            // Save the user details
	    if(!$user->save()) {
		return Redirect::back()->withErrors($user->validationErrors)->withInput(Request::all());
	    }

            Session::forget('captcha');

            $this->sendActivationMail($user);

            // If the user subscribed for newsletter, send him an email
            if ($user->newsletter) {
                $this->sendOptInMail($user);
            }

            return Response::view('system', [
                'PAGE' => (object)['id' => 'registration_success', 'template' => 'registration', 'slug' => 'registration'],
            ]);
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            //combine error msg from user and captcha and agreed
            $userErrorMsg = $e->getErrors()->getMessages();
            return redirect('registration')
                ->withErrors(array_merge_recursive($userErrorMsg, $inputErrorMsg))
                ->withInput(Request::except(['password', 'password_confirmation']));
        } catch (\Exception $e) {
            Log::error($e);

            return Response::view('system', [
                'PAGE' => (object)['id' => 'activation_failure', 'template' => 'registration', 'slug' => 'registration'],
            ]);
        }
    }

    /************************************************************************
     *
     *
     ***********************************************************************/
    public function unlockUser($code)
    {
	
       $dateFormat = 'Y-m-d H:i:s';
       try {
            $user = User::where('unlock_code', '=', $code)->where('active', 0)->first();
            if (!$user) {
                return Response::view('system', [
                    'PAGE' => (object)['id' => 'activation_failure', 'template' => 'registration', 'slug' => 'registration'],
                ]);
            }

            //$expitation_time = date('Y-m-d H:i:s',
            //    strtotime('+' . config('app.expire_token_time') . ' hours', strtotime($user->updated_at))
            //);
	    $expiration_time = Carbon::parse($user->updated_at)->addHour( config('app.expire_token_time') )->format($dateFormat);
            if ($expiration_time >= Carbon::now()->format($dateFormat)) {
                $user->unlock_code = null;
                $user->active = true;
                $user->save();

                return Response::view('system', [
                    'PAGE' => (object)['id' => 'activation_success', 'template' => 'registration', 'slug' => 'registration'],
                ]);
            } else {
                return Response::view('system', [
                    'PAGE' => (object)['id' => 'activation_failure', 'template' => 'registration', 'slug' => 'registration'],
                ]);
            }
        } catch (\Exception $e) {
            Log::error($e);
            
	    return Response::view('system', [
                'PAGE' => (object)['id' => 'activation_failure', 'template' => 'registration', 'slug' => 'registration'],
            ]);
        }
    }

    /************************************************************************
     *
     *
     ***********************************************************************/
    public static function profileForm()
    {
        $user = Auth::user();

        CaptchaHelper::resetCaptcha();

        if (Session::get('lang') == 1) {
            $altLang = 2;
        } else {
            $altLang = 1;
        }

        return Response::view('profile', [
            'PAGE' => (object)['id' => 'profile', 'template' => 'profile', 'slug' => 'profile', 'langs' => 3],
            'USER' => $user,
            'ALT_LANG' => $altLang,
        ]);
    }


    /**
     * Performs the actual erase of the profile, after we are redirected from the email
     * @param $userid The user ID to be deleted
     * @param $token The token the user provides
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function performGDPRErase($userid, $token) {
        $errors = null;

        $user = $user = User::where('id', '=', $userid)->first();
        if (!$user) {
            //no such user - redirect with error message
            return redirect('/home')->withErrors(['error' => trans('gdpr.request.nouser')]);
        }
        if (!$user->validateGDPREraseToken($token)) {
            //error in the token - redirect with error message
            return redirect('/home')->withErrors(['error' => trans('gdpr.request.wrongtoken')]);
        }

        //logout the user
        Auth::logout();

        //else perform the delete
        $user->GDPRErase();

        //then redirect with an info text
        return redirect('/home')->with('gdpr_val',true);
    }

    /************************************************************************
     *
     *
     ***********************************************************************/
    public function updateProfile()
    {
        try {
            $user = Auth::user();
            $oldNewsletter = $user->newsletter;

            if (Request::get('password') == '') {
                return redirect('profile')
                    ->withErrors(['password' => trans('profile.password_empty')])
                    ->withInput(Request::except(['captcha', 'password']));

            } else {
                $hasher = new PasswordHash(8, true);
                if (!$hasher->CheckPassword(Request::get('password'), $user->password)) {
                    return redirect('profile')
                        ->withErrors(['password' => trans('profile.password_wrong')])
                        ->withInput(Request::except(['captcha', 'password']));
                }
            }

            $user->fill(Request::only('first_name', 'last_name', 'company', 'department', 'position', 'address', 'code', 'city', 'country', 'email', 'phone', 'fax', 'newsletter'));

            if (!Request::has('newsletter')) {
                Request::instance()->query->set('newsletter', 0);
                $user->newsletter = 0;
            }
            $rules = [
                'captcha' => 'required|same:captchaSecret',
            ];
            $fields = [
                'captcha' => Request::get('captcha'),
                'captchaSecret' => Session::get('captcha'),
            ];
            $inputValidator = Validator::make($fields, $rules, ValidationHelper::getFEValidationTranslation());

            // Reset capcha to prevent reuse one captcha.
            CaptchaHelper::resetCaptcha();

            if ($inputValidator->fails()) {
                return redirect('profile')
                    ->withErrors($inputValidator)
                    ->withInput(Request::except(['password', 'password_confirmation']));
            }

            if (Request::has('gdprerase') && Request::get('gdprerase') == 1) {

                $user->requestGDPRErase();

                return Response::view('profile_gdprerase',[
                    'PAGE' => (object)[
                        'id' => 'profile',
                        'template' => 'profile',
                        'slug' => 'profile'
                    ]
                ]);
            }

            $isOptingOut = false;
            $isOptingIn = false;

            if ($user->newsletter != $oldNewsletter
                || (!$user->newsletter_include && $user->newsletter == 1 && $user->newsletter_time < date('Y-m-d H:i:s'))
            ) {
                $newsletterCode = str_random(self::NEWSLETTER_CODE_STRING_LENGTH);
                $user->newsletter_code = $newsletterCode;
		if ($user->newsletter) { // opt-in
                    $isOptingIn = true;
                    $newsletter_time = Carbon::now()
                        ->addHours(config('app.expire_token_time'))
                        ->format("Y-m-d H:i:s");
                    $user->newsletter_time = $newsletter_time;
                    $user->newsletter_code = $newsletterCode;
		    $this->sendOptInMail($user);
		} else { // opt-out
	            $isOptingOut = true;
                    $user->newsletter_include = 0;
                    $user->newsletter_time = date(DATE_TIME_ZERO);
                    $user->unsubscribe_code = '';
                    $this->sendOptOutMail($user);
                }
            }
	    $user->save();		


            return Response::view('profile_saved', [
                'PAGE' => (object)[
                    'id' => 'profile',
                    'template' => 'profile',
                    'slug' => 'profile'
                ],
                'IS_OPT_IN' => $isOptingIn,
                'IS_OPT_OUT' => $isOptingOut,
            ]);
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            return redirect('profile')
                ->withErrors($e->getErrors())
                ->withInput(Request::except(['password', 'password_confirmation']));
        } catch (\Exception $e) {
            Log::error($e);

            return redirect('profile');
        }
    }

    /************************************************************************
     *
     *
     ***********************************************************************/
    public static function forgottenForm()
    {
        CaptchaHelper::resetCaptcha();

        if (Session::get('lang') == 1) {
            $altLang = 2;
        } else {
            $altLang = 1;
        }

        return Response::view('forgotten', [
            'PAGE' => (object)['id' => 'forgotten', 'template' => 'forgotten', 'slug' => 'forgotten', 'langs' => 3],
            'ALT_LANG' => $altLang,
        ]);
    }

    /************************************************************************
     *
     *
     ***********************************************************************/
    public static function createReset()
    {
        if (Session::has('error') || Session::has('success')) {
            return self::forgottenForm();
        }
        try {
            $rules = [
                'captcha' => 'required|same:captchaSecret',
            ];
            $fields = [
                'captcha' => Request::get('captcha'),
                'captchaSecret' => Session::get('captcha'),
            ];
            $inputValidator = Validator::make(
                $fields,
                $rules,
                ValidationHelper::getFEValidationTranslation()
            );

            // Log the usage of captcha
            CaptchaHelper::logCaptchaUsage();
            // Reset capcha to prevent reuse one captcha.
            CaptchaHelper::resetCaptcha();

            if ($inputValidator->fails()) {
                return redirect('forgotten')
                    ->withErrors($inputValidator->errors())
                    ->withInput(Request::except(['captcha']));
            }

            $user = User::where('username', Request::get('username'))->first();
            if ($user && !$user->active) {
                return redirect('forgotten')
                    ->withErrors(['error' => trans('ws_general_controller.wait_for_activation')])
                    ->withInput(Request::except(['captcha']));
            }

            $credentials = ['username' => Request::get('username')];

            $response = Password::sendResetLink($credentials); #returns redirect to the last route

            //TODO: Handle different errors trace L4.0
            switch ($response) {
                case Password::RESET_LINK_SENT:
                    return redirect('forgotten')->with('success', true);

                case Password::INVALID_USER:
                    return redirect()->back()
                        ->withErrors(['username' => trans($response)])
                        ->withInput(Request::except(['captcha']));

                default:
                    return redirect('forgotten')
                        ->withErrors(true)
                        ->withInput(Request::except(['captcha']));
            }
        } catch (\Exception $e) {
            Log::error($e);

            return is_array($e->getMessage()) ? implode(',', $e->getMessage()) : $e->getMessage();
        }
    }

    /************************************************************************
     *
     *
     ***********************************************************************/
    public static function resetForm($token = false)
    {
        if ($token === false) {
            return redirect('home');
        }

        if ($token == 'done') {
            return Response::view('system', [
                'PAGE' => (object)['id' => 'reset_success', 'template' => 'system', 'slug' => 'reset']
            ]);
        } else {
            CaptchaHelper::resetCaptcha();

            return Response::view('reset', [
                'PAGE' => (object)['id' => 'reset_form', 'template' => 'reset_form', 'slug' => 'reset'],
                'TOKEN' => $token
            ]);
        }
    }

    /************************************************************************
     *
     *
     ***********************************************************************/
    public static function doReset($token)
    {
        if (Session::has('error') || Session::has('errors')) {
            //return self::resetForm(Request::get('token'));
            return self::resetForm($token);
        }

        try {
            $rules = [
                'username' => 'required',
                'password' => 'required|same:password_confirmation',
                'password_confirmation' => 'required',
                'captcha' => 'required|same:captchaSecret'
            ];
            $fields = [
                'captcha' => Request::get('captcha'),
                'captchaSecret' => Session::get('captcha'),
                'username' => Request::get('username'),
                'password' => Request::get('password'),
                'password_confirmation' => Request::get('password_confirmation')
            ];

            // Reset capcha to prevent reuse one captcha.
            CaptchaHelper::resetCaptcha();

            $customMessages = ValidationHelper::getFEValidationTranslation();
            $customAttributes = $customMessages['attributes'];
            $inputValidator = Validator::make($fields, $rules, $customMessages, $customAttributes);

            if ($inputValidator->fails()) {
                return redirect('reset/' . $token)
                    ->withErrors($inputValidator)
                    ->withInput(Request::except(['captcha', 'password', 'password_confirmation']));
            }

            $credentials = [
                'token' => $token,
                'username' => Request::get('username'),
                'password' => Request::get('password'),
                'password_confirmation' => Request::get('password_confirmation')
            ];

            $response = Password::reset($credentials, function ($user, $password) {
                $user->password = $password;
                $user->password_confirmation = Request::get('password_confirmation');
                $user->save();
            });

            //TODO: Handle different errors trace L4.0

            switch ($response) {
                case Password::PASSWORD_RESET:
                    return redirect('reset/done')
                        ->with('success', true);

                case Password::INVALID_TOKEN:
                    return redirect('reset/' . $token)
                        ->withErrors(['token' => trans($response)])
                        ->withInput(Request::except(['captcha']));

                case Password::INVALID_USER:
                    return redirect('reset/' . $token)
                        ->withErrors(['username' => trans($response)])
                        ->withInput(Request::except(['captcha']));

                case Password::INVALID_PASSWORD:
                    return redirect('reset/' . $token)
                        ->withErrors(['password' => trans($response)])
                        ->withInput(Request::except(['captcha']));

                default:
                    return redirect('reset/' . $token)
                        ->withErrors(true)
                        ->withInput(Request::except(['captcha']));
            }
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            //combine error msg from user and captcha and agreed
            $userErrorMsg = $e->getErrors()->getMessages();
            return redirect('reset/' . $token)
                ->withErrors($userErrorMsg)
                ->withInput(Request::except(['password', 'password_confirmation']));
        } catch (\Exception $e) {
            Log::error($e);
            return redirect('reset/' . $token);
        }
    }

    /************************************************************************
     * Opt-in
     *
     * @return Response
     ***********************************************************************/
    public function optin($code)
    {
        try {
            $user = User::where('newsletter_code', '=', $code)->take(1)->get();
            if ($user->count() == 1 && $user[0]->newsletter_time >= date('Y-m-d H:i:s')) {
                $user = $user->first();
                if ($user->active) {
                    $user->newsletter_code = '';
                    $user->newsletter_include = 1;
                    $user->newsletter_time = date(DATE_TIME_ZERO);
                    $user->unsubscribe_code = str_random(self::UNSUBSCRIBE_CODE_STRING_LENGTH);
                    $user->save();

                    return Response::view('optin', [
                        'PAGE' => (object)['id' => 'optin', 'template' => 'optin', 'slug' => 'optin'],
                        'TITLE_KEY' => 'optin.success_title',
                        'SUBHEADING_KEY' => 'optin.success_subheading',
                        'MESSAGE_KEY' => 'optin.success_message'
                    ]);
                } else {
                    return redirect('home?lang=' . App::getLocale())->withErrors(['error' => trans('optin.failure_not_active')]);
                }
            } else {
                return Response::view('optin', [
                    'PAGE' => (object)['id' => 'optin', 'template' => 'optin', 'slug' => 'optin'],
                    'TITLE_KEY' => 'optin.failure_title',
                    'SUBHEADING_KEY' => 'optin.failure_subheading',
                    'MESSAGE_KEY' => 'optin.failure_message'
                ]);
            }
        } catch (\Exception $e) {
            Log::error($e);

            return Response::view('optin', [
                'PAGE' => (object)['id' => 'optin', 'template' => 'optin', 'slug' => 'optin'],
                'TITLE_KEY' => 'optin.failure_title',
                'SUBHEADING_KEY' => 'optin.failure_subheading',
                'MESSAGE_KEY' => 'optin.failure_message'
            ]);
        }
    }

    /************************************************************************
     * Unsubscribe user from newsletter
     *
     * @return Response
     ***********************************************************************/
    public function unsubscribe($code)
    {
        try {
            $user = User::where('unsubscribe_code', '=', $code)->take(1)->get();

            if ($user->count() == 1) {
                $user = $user->first();
                if (NewsletterController::usrCrcCode($user->id) !== Request::get('u')) {
                    return Response::view('optin', [
                        'PAGE' => (object)['id' => 'optin', 'template' => 'optin', 'slug' => 'optin'],
                        'TITLE_KEY' => 'optin.failure_title',
                        'SUBHEADING_KEY' => 'optin.failure_subheading',
                        'MESSAGE_KEY' => 'optin.failure_message'
                    ]);
                }
                $user->newsletter = 0;
                $user->newsletter_include = 0;
                $user->unsubscribe_code = '';
                $user->newsletter_time = date(DATE_TIME_ZERO);
                $this->sendOptOutMail($user);
                $user->save();

                return Response::view('profile_saved', [
                    'PAGE' => (object)[
                        'id' => 'profile',
                        'template' => 'profile',
                        'slug' => 'profile'
                    ],
                    'IS_OPT_IN' => false,
                    'IS_OPT_OUT' => true,
                ]);
            } else {
                return Response::view('optin', [
                    'PAGE' => (object)['id' => 'optin', 'template' => 'optin', 'slug' => 'optin'],
                    'TITLE_KEY' => 'optin.no_user_title',
                    'SUBHEADING_KEY' => 'optin.no_user_subheading',
                    'MESSAGE_KEY' => 'optin.no_user_message'
                ]);
            }
        } catch (\Exception $e) {
            Log::error($e);

            return Response::view('optin', [
                'PAGE' => (object)['id' => 'optin', 'template' => 'optin', 'slug' => 'optin'],
                'TITLE_KEY' => 'optin.failure_title',
                'SUBHEADING_KEY' => 'optin.failure_subheading',
                'MESSAGE_KEY' => 'optin.failure_message'
            ]);
        }
    }

    /************************************************************************
     * Remove the specified resource from storage.
     *
     * @return Response
     ***********************************************************************/
    public static function logout()
    {
        Session::flush(); // clear the session

        return redirect('home?lang=' . App::getLocale()); // redirect the user to home page
    }

    /************************************************************************
     * Generates a CAPTCHA image
     *
     * @return image/png
     ***********************************************************************/
    public static function generateCAPTCHA()
    {
        $pixel_ratio = 2;
        $size_x = $pixel_ratio * 140;
        $size_y = $pixel_ratio * 50;
        $font_size = $pixel_ratio * 28;
        $text_y = $pixel_ratio * 35;
        $noise_line_len = $pixel_ratio * 8;

        $code = CaptchaHelper::getCaptcha();

        $space_per_char = $size_x / (strlen($code) + 1);

        $brandName = Request::get('brand', 'daimler');
        $brand = ($brandName == 'smart') ? SMART : DAIMLER;

        //*************************

        $img = imagecreatetruecolor($size_x, $size_y);
        $background = imagecolorallocate($img, 230, 230, 230); // Light Grey
        $border = imagecolorallocate($img, 200, 200, 200); // Light Grey +20K
        $font_path = resource_path('fonts/daimler/DaimlerCS-Regular.ttf');

        if ($brand == SMART) {
            $colors[] = imagecolorallocate($img, 246, 186, 53); // Original Orange
            //$colors[] = imagecolorallocate($img, 238, 238, 238); // Light Silver
            $colors[] = imagecolorallocate($img, 150, 150, 150); // Dark Silver
            $colors[] = imagecolorallocate($img, 243, 168, 1); // Dark Orange
            $colors[] = imagecolorallocate($img, 255, 255, 255); // Black
            $colors[] = imagecolorallocate($img, 0, 0, 0); // Withe
            $font_path = resource_path('fonts/smart/FORsmartSpecial.ttf');
        } else {
            //		$colors[] = $border; // Light Grey +20K
            $colors[] = imagecolorallocate($img, 158, 158, 158); // Light Grey +40K
            $colors[] = imagecolorallocate($img, 112, 112, 112); // Light Grey +60K
            $colors[] = imagecolorallocate($img, 0, 103, 127); // Petrol
            $colors[] = imagecolorallocate($img, 0, 122, 147); // Petrol 80%
            //$colors[] = imagecolorallocate($img, 80, 151, 171); // Petrol 60%
            $colors[] = imagecolorallocate($img, 121, 174, 191); // Petrol 40%
            //$colors[] = imagecolorallocate($img, 166, 202, 216); // Petrol 20%
        }

        $colors_count = count($colors);

        // fill the background
        imagefilledrectangle($img, 1, 1, $size_x - 2, $size_y - 2, $background);
        imagerectangle($img, 0, 0, $size_x - 1, $size_y - 1, $border);

        // creat 1000 lines all over the picture
        // correction - 200, reducing the noise
        $half_line_len = $noise_line_len / 2;
        for ($i = 0; $i < 150; $i++) {
            $x1 = rand(5, $size_x - 5);
            $y1 = rand(5, $size_y - 5);
            $x2 = $x1 - $half_line_len + rand(0, $noise_line_len);
            $y2 = $y1 - $half_line_len + rand(0, $noise_line_len);
            imageline($img, $x1, $y1, $x2, $y2, $colors[rand(0, $colors_count - 1)]);
        }

        // draw the text
        for ($i = 0; $i < strlen($code); $i++) {
            //$color = $colors[$i % count($colors)];
            $color = $colors[rand(0, ($colors_count - 1))];
            imagettftext($img, $font_size + rand(0, 8), -10 + rand(0, 20), ($i + 0.3) * $space_per_char, $text_y + rand(0, 10), $color, $font_path, $code{$i});
        }
        // add some random disturbances
        imageantialias($img, true);

        header('Content-type:image/png');
        imagepng($img);
        imagedestroy($img);
    }

    /************************************************************************
     *
     *
     ***********************************************************************/
    public static function resetCaptcha()
    {
        CaptchaHelper::resetCaptcha();

        return Response::json(RestController::generateJsonResponse(false, 'Captcha changed'), 200);
    }

    /************************************************************************
     * check required data to grant user permision to a page and send email
     * URL: pages/requestaccess/idslug
     * METHOD: POST
     ***********************************************************************/
    public function requestAccess($idslug)
    {
        $data = Request::all();

        $rules = [
            'captcha' => 'required|same:captchaSecret',
            'agreed_access' => 'accepted',
            'contact_email' => 'required|email',
            'contact_name' => ['required', 'no_tags'],
            'text_area' => ['required', 'no_tags'],
            'rbChoice' => 'required',
        ];
        $fields = [
            'captcha' => Request::get('captcha'),
            'agreed_access' => Request::get('agreed'),
            'captchaSecret' => Session::get('captcha'),
            'contact_email' => Request::get('contact_email'),
            'contact_name' => Request::get('contact_name'),
            'text_area' => Request::get('text_area'),
            'rbChoice' => Request::get('rbChoice'),
        ];

        // Log the usage of captcha
        CaptchaHelper::logCaptchaUsage();
        // Reset capcha to prevent reuse one captcha.
        CaptchaHelper::resetCaptcha();

        if (Request::has('rbChoice') && Request::get('rbChoice') == '1') {
            //rules
            unset($rules['contact_email']);
            unset($rules['contact_name']);
            // fileds
            unset($fields['contact_email']);
            unset($fields['contact_name']);
        }
        $messages = [
            'email' => trans('system.no_access.message.warning_email'),
            'required' => trans('system.no_access.message.warning'),
            'captcha.same' => trans('system.no_access.message.captcha'),
            'captcha.required' => trans('system.no_access.message.required'),
            'agreed_access.accepted' => trans('system.no_access.agreed_access.accepted'),
        ];

        $customMessages = ValidationHelper::getFEValidationTranslation();
        $customAttributes = $customMessages['attributes'];

        $messages = array_merge($messages, $customMessages);

        $validator = Validator::make($fields, $rules, $messages, $customAttributes);
        if ($validator->fails()) {
            $messages = ValidationHelper::super_unique($validator->messages()->toArray(), $validator->messages()->toArray());
            // Redirect to page it self to recive the errors for the testing.
            $messages = array_reverse($messages);
            if (config('app.debug') && config('app.testing')) {
                return Redirect::to($idslug)
                    ->withErrors($messages)
                    ->withInput(Request::except(['captcha']));
            } else {
                return Redirect::back()
                    ->withErrors($messages)
                    ->withInput(Request::except(['captcha']));
            }
        }

        $page = Page::find($idslug);
        $data['slug'] = $page->slug;
        $data['email'] = Auth::user()->email;
        $data['user_first_name'] = Auth::user()->first_name;
        $data['user_last_name'] = Auth::user()->last_name;
        $data['user_name'] = Auth::user()->username;
        $data['template'] = $page->template;

        //For debug and testing send BCC copy of the emails
        $hasSendDebugEmail = config('app.debug') && config('app.testing');
        $debugEmail = $hasSendDebugEmail ? 'm.stefanov@esof.net' : null;
        Mail::send('emails.request_access_message', $data, function ($message) use ($debugEmail) {
            if ($debugEmail) {
                $message->bcc($debugEmail);
            }
            $message->to(config('mail.from.address'))->subject('Daimler Brand & Design Navigator – Zugriffserweiterung/Granting Access');
            $message->from(Auth::user()->email);
        });
        Mail::send('emails.request_access_message_confirm', $data, function ($message) use ($debugEmail) {
            if ($debugEmail) {
                $message->bcc($debugEmail);
            }
            $message->to(Auth::user()->email)->subject(trans('emails.request_access_confirm.subject'));
            $message->from(config('mail.from.address'));
        });

        $request_auth = DB::table('request_auth')->where('page_id', $page->id)->where('user_id', Auth::user()->id)->first();
        $nowTime = \Carbon\Carbon::now();
        if ($request_auth) {
            if (!is_null($request_auth->deleted_at)) {
                DB::table('request_auth')
                    ->where('page_id', $page->id)->where('user_id', Auth::user()->id)
                    ->update(['active' => 0, 'deleted_at' => null, 'lang' => App::getLocale(), Model::UPDATED_AT => $nowTime]);
                $request_auth->active = 0;
            }
            if ($request_auth->active) {
                DB::table('request_auth')
                    ->where('page_id', $page->id)->where('user_id', Auth::user()->id)
                    ->update(['active' => 0, 'deleted_at' => null, 'lang' => App::getLocale(), Model::UPDATED_AT => $nowTime]);
            }
        } else {
            DB::table('request_auth')->insert(
                [
                    'page_id' => $page->id,
                    'user_id' => Auth::user()->id,
                    'lang' => App::getLocale(),
                    Model::CREATED_AT => $nowTime,
                    Model::UPDATED_AT => $nowTime
                ]
            );
        }

        $path = 'home';
        if (Session::has('redirect_url')) {
            $path = strlen(Session::get('redirect_url')) > 1 ? Session::get('redirect_url') : 'home';
            Session::forget('redirect_url');
        }

        return redirect($path)->withErrors(['success' => trans('system.no_access.success.message')]);
    }

    public static function updateNewsletterUnsubscribeCode()
    {
        $users = User::where('unsubscribe_code', '')->where('newsletter_include', 1)->where('newsletter', 1)->get();
        //$users = User::where('newsletter_include', 1)->where('newsletter', 1)->get();
        $loginController = App::make(\App\Http\Controllers\LoginController::class);
        foreach ($users as $key => $user) {
            try {
                $user->unsubscribe_code = str_random(10);
                $user->save();
                echo 'User ' . $user->username . " was updated.</br>";
            } catch (\Exception $ex) {
                echo 'User ' . $user->username . " wasn't updated. -> Exception " . $ex->getMessage() . " </br>";
            }
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Group;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class BackendDispatcher extends Controller
{

    /*	 * **********************************************************************
	 * Get backend users management view
	 * ********************************************************************* */
    public function getUsersBEView()
    {
        $groups = Group::where('id', '>', 1)->get();
        return view(
            'backend/be_users',
            ['GROUPS' => $groups, 'ACTIVE' => 'users',
                    'LANG'   => $this->lang]
        );
    }

    /*	 * **********************************************************************
	 * Get backend groups management view
	 * ********************************************************************* */
    public function getGroupsBEView()
    {
        return view('backend/be_groups', ['ACTIVE' => 'groups']);
    }

    /*	 * **********************************************************************
	 * Get backend groups management view
	 * ********************************************************************* */
    public function getCommentsBEView()
    {
        $username    = '';
        $valid_user  = false;
        if (Request::has('username')) {
            $username = htmlentities(Request::get('username'), ENT_QUOTES);
            if (User::where('username', 'LIKE', '%' . $username . '%')->count() > 0) {
                $valid_user = true;
            }
        }

        return view(
            'backend/be_comments',
            ['ACTIVE'   => 'comments',
                    'LANG'       => $this->lang,
                    'USERNAME'   => $username,
                    'VALID_USER' => $valid_user == true ? 1 : 0]
        );
    }

    /*	 * **********************************************************************
	 * Get backend history management view
	 * ********************************************************************* */
    public function getHistoryBEView()
    {
        return view('backend/be_history', ['ACTIVE' => 'history']);
    }

    /*	 * **********************************************************************
	 * Get backend glossary management view
	 * ********************************************************************* */
    protected function getGlossaryBEView()
    {
        return view('backend/be_glossary', ['ACTIVE' => 'glossary', 'LANG' => $this->lang]);
    }

    /*	 * **********************************************************************
	 * Get backend faqs management view
	 * ********************************************************************* */
    protected function getFaqsBEView()
    {
        return view('backend/be_faqs', ['ACTIVE' => 'faqs', 'LANG' => $this->lang]);
    }

    /* ***********************************************************************
	 * Get backend newsletter list view
	 * ********************************************************************* */
    public function getNewsletterUsersBEView()
    {
            return view('backend/be_newsletter_user', ['ACTIVE' => 'newsletter_users', 'LANG' => $this->lang]);
    }

    public function getNewsletterListUsersBEView()
    {
        return view('backend/be_newsletter_user', ['ACTIVE' => 'newsletter_users_test', 'LANG' => $this->lang]);
    }

    // Check for valid session. Tested with file session only. 
    // Not working if session is empty!
    protected static function isSessionValid($session_id)
    {
        if (empty($session_id)) {
            return false;
        }

        if (!empty(Session::getHandler()->read($session_id))) {
            Log::debug(' Valid Session ID ' . $session_id);
            return true;
        }
        return false;

// Old implementation
//        $save_path = ini_get('session.save_path');
        $save_path = config('session.files');

        if (!$save_path) {
            $save_path = '.'; // if this vlaue is blank, it defaults to the current directory
        }
        $session_path = $save_path . '/' . $session_id;
        Log::debug('New ID: ' . Session::getId() . ' OLD ID: ' . $session_id);

        if (file_exists($session_path)) {
            //Check if session is still valid
            $maxlifetime = config('session.lifetime') * 60;
            //Log::debug('File time - maxlifetime: ' . (filemtime($session_path) + $maxlifetime) . ' Time: ' . time());
            if ((filemtime($session_path) + $maxlifetime) >= time()) {
                Log::debug(' Valid Session ID ' . $session_id);
                return true;
            }
        }
        return false;
    }

    /*	 * **********************************************************************
	 * Login user for backend managment
	 * ********************************************************************* */
    public function loginBE()
    {
        // create our user data for the authentication
        $userdata = [
            'username'   => Request::get('username'),
            'password'   => Request::get('password'),
            'active'     => 1
        ];

        // attempt to do the login
        if (Auth::attempt($userdata)) {
            $user = Auth::user();
            if ($user->role <= USER_ROLE_MEMBER) {
                return redirect('/cms')->withErrors(['username' => trans('backend.login.forbidden')]);
            }
            // Prevent multiple login
            if (self::isSessionValid($user->last_session) && Session::getId() != $user->last_session) {
                $redirect_url = Session::get('redirect_url');
                Session::flush(); // clear the session
                Auth::logout();
                return redirect(empty($redirect_url) ? 'cms': $redirect_url)
                        ->withErrors(['username' => trans('backend.login.twice')])
                        ->with(['username' => Request::get('username')]);
            }
            $count = Cart::where('user_id', Auth::user()->id)->where('active', 1)->get()->count() + DB::table('request_auth')->where('user_id', Auth::user()->id)->where('active', 1)->whereNull('deleted_at')->count();
            Session::put('myDownloadsCount', $count);
            Session::put('watchlistCount', Subscription::where('user_id', '=', $user->id)->where('active', 1)->get()->count());
            $user->logins++;
            $user->last_login = date('Y-m-d H:i:s');
            $user->save();

            // validation successful!
            if (config('app.testing')) {
                Session::put('isConfirmed', 1);
                // Change Sessio ID to prevent session fixation
                Session::regenerate();
                // Save last Session ID
                $user->last_session = Session::getId();
                $user->save();
            } else {
                $confirm_code    = UserController::generateRandomString();
                Session::put('confirm_code', $confirm_code);
                Log::debug('Confirm Code for user  ' . $user->username . ' is ' . $confirm_code . ' (' . date('Y-m-d H:i:s') . ')');
                try {
                    $param = ['FIRST_NAME'  => $user->first_name,
                                'LAST_NAME'      => $user->last_name,
                                'USERNAME'       => $user->username,
                                'CONFIRM_CODE'   => $confirm_code
                    ];
                    //$param = array('MESSAGE' => Lang::get('confirmation_email.message', $fields));
                    Mail::send(
                        'backend.be_login_confirm',
                        $param,
                        function ($message) use ($user) {
                            $message->to($user->email)
                                    ->subject(trans('confirmation_email.subject'))
                                    ->from(config('mail.from.address'));
                        }
                    );
                } catch (\Exception $exc) {
                    Log::error($exc);
                }
            }
            $redirect_url = Session::get('redirect_url');
            return redirect(empty($redirect_url) ? 'cms': $redirect_url);
        } else {
            // validation not successful, send back to form
            return redirect('/cms')
                    ->withErrors(['username' => trans('backend.login.error')])
                    ->with(['username' => Request::get('username')]);
        }
    }

    /*	 * **********************************************************************
	 * Login user for backend managment
	 * ********************************************************************* */
    public static function confirmBE()
    {
        // attempt to do the login
        if (Request::get('confirm_code') != "" && Request::get('confirm_code') == Session::get('confirm_code')) {
            $user = Auth::user();
            // Prevent multiple login
            if (self::isSessionValid($user->last_session) && Session::getId() != $user->last_session) {
                // Logout the user
                Session::flush(); // clear the session
                Auth::logout();
                return redirect('/cms')
                        ->withErrors(['username' => trans('backend.login.twice')])
                        ->with(['username' => $user->username]);
            }

            Session::put('isConfirmed', 1);
            // Change Sessio ID to prevent session fixation
            Session::regenerate();
            // Save last Session ID
            $user->last_session = Session::getId();
            $user->save();
            $redirect_url = Session::get('redirect_url');
            return redirect(empty($redirect_url) ? 'cms': $redirect_url);
        } else {
            // validation not successful, send back to form
            return redirect('/cms')->withErrors(['confirm_code' => trans('backend.confirm.error')]);
        }
    }

    /*	 * **********************************************************************
	 * Logout User from the backend
	 * ********************************************************************* */
    public static function logoutBE()
    {
        $user                = Auth::user();
        $user->last_session  = null;
        $user->save();
        Auth::logout();
        Session::flush(); // clear the session
        return redirect('/cms'); // redirect the user to home page
    }

    /*	 * **********************************************************************
	 * Welcome screen
	 * ********************************************************************* */
    public function welcomeBE()
    {
        return view(
            'backend/be_welcome',
            ['ACTIVE' => 'workflow', 'GROUPS' => Group::all(), 'LANG' => $this->lang]
        );
    }

    /**
     * Simple Action to keep the user loged in. Reset session timeout!
     * URL: cms/keepLogin
     * METHOD: GET
     */
    public function keepLoginBE()
    {
        $jsonResponse = Response::json(RestController::generateJsonResponse(false, "keepLogin", null), 200);
        return $jsonResponse;
    }

    /*	 * **********************************************************************
	 * Page BE View screen
	 * ********************************************************************* */
    protected function pageBEView()
    {
        //TODO: Check if Group::all() are correct for all roles!
        return view('backend/be_pages', 
                    ['ACTIVE' => 'content', 'GROUPS' => Group::all(), 'LANG' => $this->lang]
        );
    }

    /*	 * **********************************************************************
	 * Downloads BE View screen
	 * ********************************************************************* */
    protected function downloadsBEView()
    {
        return view(
            'backend/be_downloads',
            [
                    'ACTIVE'             => 'downloads',
                    'LANG'               => $this->lang,
                    'FILEMANAGER_MODE'   => 'downloads',
                    'FOLDER_STATUS'  => FileController::getFolderStatus('downloads', '', true),
            ]
        );
    }

    /*	 * *********************************************************************
	 * Page Recycle Bin BE View screen
	 * ********************************************************************* */
    public function recyclebinBEView()
    {
        return view('backend/be_recyclebin', 
                    ['ACTIVE' => 'recycle_bin', 'LANG' => $this->lang]
        );
    }

    /*	 * *********************************************************************
	 * Page Tags BE View screen
	 * ********************************************************************* */
    public function tagsBEView()
    {
        return view('backend/be_tags', 
                    ['ACTIVE' => 'tags', 'LANG' => $this->lang]
        );
    }

    protected function imagesBEView()
    {
        return view('backend/be_images', [
            'ACTIVE' => 'images',
            'LANG' => $this->lang,
            'FILEMANAGER_MODE' => 'images',
            'FOLDER_STATUS' => FileController::getFolderStatus('images', '', true)
            ]
        );
    }

    /*     * **********************************************************************
     * File Manager BE View screen
     * ********************************************************************* */

    protected function filemanagerBEView()
    {
        $filemanager_mode = Request::get('filemanager_mode');
        if (!isset($filemanager_mode)) {
            $filemanager_mode = 'images';
        }
        return view('backend/be_filemanager', [
            'LANG' => $this->lang,
            'FILEMANAGER_MODE' => $filemanager_mode,
            'FOLDER_STATUS' => null,
            'IS_NEWSLETTER' => Request::has('isNewsletter')? Request::get('isNewsletter'): false
            ]
        );
    }

    /* ***********************************************************************
     * Image Hotspot creation BE View screen
     * ********************************************************************* */

    protected function hotspotBEView()
    {
        return view('backend/be_hotspot', 
                    ['LANG' => $this->lang]
        );
    }

    /* ***********************************************************************
     * newsletter manage BE View screen
     * ********************************************************************* */

    protected function newsletterBEView()
    {
        return view('backend/be_newsletter', 
                    ['ACTIVE' => 'newsletter', 'LANG' => $this->lang]
        );
    }
}

<?php

namespace App\Http\Controllers;

use App\Helpers\FileHelper;
use App\Models\Newsletter;
use App\Models\User;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use ZipArchive;

// Workaround for Old links crypted with L40 Encrypter
use App\Helpers\Encrypter as CryptL40;

if (!defined('DBDN_BASE')) {
    define('DBDN_BASE', 'https://designnavigator.daimler.com');
}

if (!defined('NEWSLETTER_BASE')) {
    define('NEWSLETTER_BASE', 'https://designnavigator.daimler.com/newsletter/');
}

if (!defined('CDNEWS_BASE')) {
    define('CDNEWS_BASE', 'https://designnavigator.daimler.com/CD-News/');
}

if (!defined('NEWSLETTER_FONT_BASE')) {
    define('NEWSLETTER_FONT_BASE', NEWSLETTER_BASE.'fonts/');
}

if (!defined('NEWSLETTER_IMG_BASE')) {
    define('NEWSLETTER_IMG_BASE', DBDN_BASE.'/newsletter/');
}


class NewsletterController extends Controller
{

    public static $suffix = [1 => '_en', 2 => '_de', 3 => ''];

    /**
     * Display newsletter
     *
     * @return Response
     */
    public function index($slug = false)
    {
        // Check for unsubscribe
        if (Request::get('unsubscribe', false)) {
            //TODO: Implement unsubscribe for user
            // If no user then redirect to general unsubscribe page
            return redirect('Newsletter_Opt_Out');
        } else {
            try {
                $nl = Newsletter::getBySlug($slug);
            } catch (\Exception $exception) {
                Log::info("Newsletter with slug : " . $slug . ' error: ' . self::getLastErrorMsg());
                return Response::view('errors.fatal', ['PAGE'=> (object)['id' => 404, 'slug' => 'fatal']], 404);
            }

            if ($nl->active == 0) {
                return Response::view('errors.fatal', ['PAGE'=> (object)['id' => 404, 'slug' => 'fatal']], 404);
            }
            if (Request::get('e')) {
                $params = self::buildParams($nl, 'footer_e');
            } else {
                $params = self::buildParams($nl);
            }

            if (Request::get('u')) {
                $id = base64_decode(Request::get('u'));
                if (!$id) {
                    Log::error("Base64_decode faled on : " . $id);
                    return Response::view('errors.fatal', ['PAGE'=> (object)['id' => 404, 'slug' => 'fatal']], 404);
                }
                try {
                    $id = Crypt::decrypt($id);
                } catch (DecryptException $e) {
                    // Workaround for Old newsletter links
                    try {
                        $id = CryptL40::decrypt($id);
                    } catch (DecryptException $e) {
                        Log::error("Decrypt faled on : " . $id);
                        return Response::view('errors.fatal', ['PAGE'=> (object)['id' => 404, 'slug' => 'fatal']], 404);
                    }
                }
                $real_user = User::find($id);
                if (!$real_user) {
                    return Response::view('errors.fatal', ['PAGE'=> (object)['id' => 404, 'slug' => 'fatal']], 404);
                }
                $user = (object)[
                        'id'    => $real_user->id,
                        'email' => trim($real_user->email),
                        'name'  => html_entity_decode(trim($real_user->first_name) . ' ' . trim($real_user->last_name)),
                        'lang'  => empty($real_user->newsletter_lang) ? 3 : $real_user->newsletter_lang,
                        'unsubscribe_code' => $real_user->unsubscribe_code,
                    ];
                $params = self::personalizeParams($params, $user);
            } else if (Request::get('t')) {
                $name =  base64_decode(Request::get('t'));
                if (!$name) {
                    Log::error("Base64_decode faled on : " . $name);
                    return Response::view('errors.fatal', ['PAGE'=> (object)['id' => 404, 'slug' => 'fatal']], 404);
                }
                try {
                    $name = Crypt::decrypt($name);
                } catch (DecryptException $e) {
                    // Workaround for Old newsletter links
                    try {
                        $name = CryptL40::decrypt($name);
                    } catch (DecryptException $e) {
                        Log::error("Decrypt faled on : " . $name);
                        return Response::view('errors.fatal', ['PAGE'=> (object)['id' => 404, 'slug' => 'fatal']], 404);
                    }
                }
                $user = new \stdClass;
                $user->name = $name;
                $params = self::personalizeParams($params, $user);
            } elseif (Request::get('e')) {
                $params = self::personalizeParams($params);
            } else {
                $params = self::personalizeParams($params);
                $params['user_greet_en'] = Lang::get('backend_newsletter.greet_coomon', [], 'en');
                $params['user_greet_de'] = Lang::get('backend_newsletter.greet_coomon', [], 'de');
            }
            $params['content_en'] = str_replace('{{$user_greet_en}}', $params['user_greet_en'], $params['content_en']);
            $params['content_de'] = str_replace('{{$user_greet_de}}', $params['user_greet_de'], $params['content_de']);

            $html = view('emails.newsletter', $params)->render();

            $html = str_replace(NEWSLETTER_FONT_BASE, '/webfonts/', $html);

            return $html;
        }
    }

    /**
     * Download newsletter as ZIP file
     */
    public function tozip($slug = false)
    {

        $nl = is_numeric($slug) ? Newsletter::findOrFail($slug) : Newsletter::getBySlug($slug);

        $params = self::buildParams($nl, 'footer_e');
        $params = self::personalizeParams($params);

        $tmpViewHtml = view('emails.newsletter', $params)->render();

        // embed base64 encoded images in html
        //$tmpViewHtml = self::embedBase64Img($tmpViewHtml);

        $dir = FileHelper::createTempDir('newsletterzip'.abs(crc32(microtime(true))), true);
        self::downloadImages($tmpViewHtml, $dir);

        // fast and ugly hack
        // $tmpViewHtml = str_replace('src="'.NEWSLETTER_BASE, 'src="./', $tmpViewHtml);
        // replaced by "real" code
        $tmpViewHtml = self::modifyImgSrc([
                'html'      =>  $tmpViewHtml,
                'prepend'   =>  './',
                'stripPath' => true,
            ]);

        file_put_contents($dir.'index.html', $tmpViewHtml);

        $tmpTextonlyTxt = view('emails.newsletter_zip_textonly', $params)->render();

        file_put_contents($dir.'notice.txt', $tmpTextonlyTxt);

        $zipFile = FileHelper::getSysTempDir(true).'newsletter.zip';

        $zipArchive = new ZipArchive();

        if (!$zipArchive->open($zipFile, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE)) {
            throw new \Exception('Failed to create archive '.$zipFile, 1);
        }

        $options = ['add_path' => 'newsletter/','remove_all_path' => true];
        $zipArchive->addGlob($dir.'*.*', GLOB_BRACE, $options);

        // download fonds and put them in /fonts folder in zip file
        /*
        if(!is_dir($dir.'fonts'))
            mkdir($dir.'fonts');

        $r = preg_match_all('/url\(\'([^\']*)\'\)/i', $tmpViewHtml, $result);

        foreach($result[1] as $idx => $file)
        {
            $fname = basename($file);
            $fname = explode('?',$fname)[0];
            $fname = explode('#',$fname)[0];

            if(!file_exists($dir.'fonts/'.$fname))
                file_put_contents($dir.'fonts/'.$fname, file_get_contents($file));

            $tmpViewHtml = str_replace($file, './fonts/'.$fname, $tmpViewHtml);
        }

        $options = ['add_path' => 'newsletter/fonts/','remove_all_path' => true];
        $zipArchive->addGlob($dir.'fonts/*.*', GLOB_BRACE, $options);
        */

        if (!$zipArchive->status == ZIPARCHIVE::ER_OK) {
            throw new \Exception('Failed to write files to zip', 1);
        }

        if (!$zipArchive->close()) {
            throw new \Exception('Failed to close zip file', 1);
        }

        foreach (glob($dir.'*.*') as $crap) {
            unlink($crap);
        }
        rmdir($dir);

        if (Request::wantsJson()) {
            // TODO: here we must generate error message
            throw new \Exception("Error Processing Request", 1);
        } else {
            FileHelper::sendFileHeaders($zipFile, true);
            readfile($zipFile);
        }
        exit;
    }

    public static function usrCrcCode($user_id)
    {
        return dechex(crc32($user_id));
    }

    public static function embedBase64Img($html)
    {
        $img_tags = self::getImgTags($html);

        // early return
        if (!count($img_tags)) {
            return $html;
        }

        $tempDir = FileHelper::createTempDir('newsletter', true);

        foreach ($img_tags as $img_tag) {
            if (preg_match('/src="([^"]*)"/i', $img_tag, $result)) {    /* $result = [ 0 => 'src="header.jpg"', 1 => 'header.jpg' ]; */
                list($src, $img) = $result;

                $img_file = $tempDir.basename($img);

                if (!file_exists($img_file)) {
                    file_put_contents($img_file, self::url_get_content($img));
                }

                $iType = exif_imagetype($img_file);

                if (!$iType) {
                    throw new \Exception('Could not detect image type of ' . basename($url), 1);
                }

                switch ($iType) {
                    case IMAGETYPE_GIF:
                        $filetype = 'gif';
                        break;
                    case IMAGETYPE_JPEG:
                        $filetype = 'jpeg';
                        break;
                    case IMAGETYPE_PNG:
                        $filetype = 'png';
                        break;
                    /*
                    case IMAGETYPE_SWF: $filetype = ''; break;
                    case IMAGETYPE_PSD: $filetype = ''; break;
                    case IMAGETYPE_BMP: $filetype = ''; break;
                    case IMAGETYPE_TIFF_II: $filetype = ''; break;
                    case IMAGETYPE_TIFF_MM: $filetype = ''; break;
                    case IMAGETYPE_JPC: $filetype = ''; break;
                    case IMAGETYPE_JP2: $filetype = ''; break;
                    case IMAGETYPE_JPX: $filetype = ''; break;
                    case IMAGETYPE_JB2: $filetype = ''; break;
                    case IMAGETYPE_SWC: $filetype = ''; break;
                    case IMAGETYPE_IFF: $filetype = ''; break;
                    case IMAGETYPE_WBMP:    $filetype = ''; break;
                    case IMAGETYPE_XBM: $filetype = ''; break;
                    case IMAGETYPE_ICO: $filetype = ''; break;
                    */
                    default:
                        throw new \Exception('Unsupported image type', 1);
                }

                $new_img = 'data:image/' . $filetype . ';base64,' . base64_encode(file_get_contents($img_file));

                $html = str_replace($src, 'src="'.$new_img.'"', $html);
            }   // if(preg_match('/src="([^"]*)"/i',$img_tag, $src))
        }   // foreach( $img_tags as $img_tag)

        return $html;
    }

    public static function url_get_content($url)
    {
        try {
            $local_path = str_replace(DBDN_BASE, public_path(), $url);
            return file_get_contents($local_path);
        } catch (\Exception $exc) {
            Log::error($exc);
        }
        return "";
    }

    public static function getImgTags($html)
    {
        $i = preg_match_all('/<img[^>]+>/i', $html, $result);
        return $i ? $result[0] : [];
    }

    public static function downloadImages($html, $targetDir = false)
    {
        $images = [];
        $tempDir = $targetDir ? $targetDir : FileHelper::getSysTempDir(true);
        $img_tags = self::getImgTags($html);

        foreach ($img_tags as $img_tag) {
            if (preg_match('/src="([^"]*)"/i', $img_tag, $result)) {    /* $result = [ 0 => 'src="header.jpg"', 1 => 'header.jpg' ]; */
                list($src, $img) = $result;
                $img_file = $tempDir.basename($img);

                if (!file_exists($img_file)) {
                    file_put_contents($img_file, self::url_get_content($img));
                }

                $images[] = $img_file;
            }
        }

        return array_unique($images);
    }

    /**
     *  modify imgages src attribute in html
     *
     *  $params = [
     *      $html           html to modify,
     *      $prepend        prepend to img src,
     *      $append         append to img src
     *      $ifNotStartWith check if src start with,
     *      $copyLocal      copy image to temp dir (true, false)
     *  ]
     *
     */
    public static function modifyImgSrc($params)
    {
        foreach (['html','prepend','append','ifNotStartWith','copyLocal','stripPath'] as $p) {
            $$p = empty($params[$p]) ? false : $params[$p];
        }

        $img_tags = self::getImgTags($html);

        if (count($img_tags)) {
            $tempDir  = $copyLocal ? FileHelper::createTempDir('newsletter', true) : false;

            foreach ($img_tags as $img_tag) {
                if (preg_match('/src="([^"]*)"/i', $img_tag, $src)) {   /* $src = [ 0 => 'src="header.jpg"', 1 => 'header.jpg' ]; */
                    list($tag, $url) = $src;
                    if (false === $ifNotStartWith || 0 !== stripos($url, $ifNotStartWith)) {
                        if ($copyLocal) {
                            $img_file = $tempDir.basename($url);
                            if (!file_exists($img_file)) {
                                file_put_contents($img_file, self::url_get_content($url));
                            }
                            $url = $img_file;
                        }

                        if ($stripPath) {
                            $url = basename($url);
                        }

                        $html = str_replace($tag, 'src="'.$prepend.$url.$append.'"', $html);
                    }
                }   // if(preg_match('/src="([^"]*)"/i',$img_tag, $src))
            }   // foreach( $img_tags as $img_tag)
        }   // if($i = preg_match_all('/<img[^>]+>/i', $html, $result))

        return $html;
    }

    public static function personalizeParams($params, $user = false)
    {
        if (!$user) {
            $user = new \stdClass;
        }

        // 3 == without lang
        $user->lang = empty($user->lang) ? 3 : $user->lang;

        // there can be only one ;)
        $fallBackToOne = ['title','header_d','header_m', 'footer'];

        if (1 == $user->lang) {            // EN
            foreach ($fallBackToOne as $key) {
                $params[$key] = $params[$key.'_en'];
            }

            $params['content_de'] = '';
            $params['langswitch_de'] = '';
            $params['langswitch_en'] = '';
        } elseif (2 == $user->lang) {        // DE
            foreach ($fallBackToOne as $key) {
                $params[$key] = $params[$key.'_de'];
            }

            $params['content_en'] = '';
            $params['langswitch_en'] = '';
            $params['langswitch_de'] = '';
        } elseif (3 == $user->lang) {        // Whatever
        // Remove footer from array to protect multilang foter
            unset($fallBackToOne[3]);
            foreach ($fallBackToOne as $key) {
                $params[$key] = $params[$key.'_en'];
            }
        } else {
            throw new \Exception("Invalid user language", 1);
        }

        if (!empty($user->unsubscribe_code) && !empty($user->id)) {
            $params['unsubscribe_link'] = asset('/unsubscribe/' . $user->unsubscribe_code . '?u='.self::usrCrcCode($user->id).'&');
            $params['unsubscribe']      = view('partials.newsletter_unsubscribe', $params)->render();
            try {
                $id = Crypt::encrypt($user->id);
                $id = base64_encode($id);
                $params['newsletter_link'] = $params['newsletter_link'].'?u='.$id;
            } catch (DecryptException $e) {
                Log::error("Encrypt faled on id: " . $id);
            }
        }

        if (!empty($user->name)) {
            $params['user_greet_en'] = Lang::get('backend_newsletter.greet_personal', ['name'=> $user->name], 'en');
            $params['user_greet_de'] = Lang::get('backend_newsletter.greet_personal', ['name'=> $user->name], 'de');
            if (empty($user->id)) {
                try {
                    $name = Crypt::encrypt($user->name);
                    $name = base64_encode($name);
                    $params['newsletter_link'] = $params['newsletter_link'].'?t='.$name;
                } catch (DecryptException $e) {
                    Log::error("Encrypt faled on name: " . $user->name);
                }
            }
        } else {
            $params['user_greet_en'] = Lang::get('js_localize.tiny_mce.init.nl_greet', [], 'en');
            $params['user_greet_de'] = Lang::get('js_localize.tiny_mce.init.nl_greet', [], 'de');
        }

        /*
        echo "\n\n=====================================\n\n";
        print_r(array_keys($params));
        print_r($user);
        echo "\n\n=====================================\n\n";
        die;
        //*/
        return $params;
    }

    public static function buildParams($nl, $exclude = 'footer_i')
    {

        if ($nl instanceof Newsletter) {
            // ignore
        } elseif (is_numeric($nl)) {
            $nl = Newsletter::findOrFail($nl);
        } else {
            throw new \Exception("Error Processing Request", 1);
        }

        $elements = $nl->contents()->where('position', '<>', $exclude);
        if ($exclude == 'footer_e') {
            $elements = $elements->where('position', '<>', 'footer_e_bl');
        }
        $elements = $elements->get();

        $cont = [];
        foreach ($elements as $el) {
            $element = (object)$el->getAttributes();

            // stupid per-case processing (very bad practice!) because images are with full <img> tag
            if (0 === strpos($element->position, 'header_') && preg_match('/src="([^"]*)"/i', $element->content, $src)) {
                $element->content = (0 === strpos($src[1], 'http')) ? $src[1] : DBDN_BASE . '/' . $src[1];
            } elseif (0 === strpos($element->position, 'footer_')) {
                    $element->position = 'footer';
            }
            $cont[] = $element;
        }

        $params = [
                'piwik'             => '<!-- __PIWIK__ -->',
                'copyright_year'    => date('Y', strtotime($nl->updated_at)),
                'newsletter_link'   => asset('CD-News/'.$nl->slug),
                'img_base'          => NEWSLETTER_IMG_BASE,
                'font_base'         => NEWSLETTER_FONT_BASE,
                'langswitch_de'     => view('partials.newsletter_langswitch_de')->render(),
                'langswitch_en'     => view('partials.newsletter_langswitch_en')->render(),
            ];

        if ('footer_i' == $exclude) {    // external
            $params['unsubscribe_link']  = asset('/Newsletter_Opt_Out?');
            $params['unsubscribe']       = view('partials.newsletter_unsubscribe', $params)->render();
        } else {                        // internal (ZIP file)
            $params['unsubscribe_link']  = '';
            $params['unsubscribe']       = '';
            $params['newsletter_link']   =  $params['newsletter_link']. '?e=internal';
        }

        foreach ($cont as $c) {
            if (true) {
                $content =  self::modifyImgSrc([
                        'html'          =>  $c->content,
                        'prepend'       =>  DBDN_BASE.'/',
                        'ifNotStartWith'=>  'http',
                    ]);
            } else {
                $content = $c->content;
            }

            if ($exclude == 'footer_e' && strrpos($c->position, 'footer') > -1) {
                $params['footer'] = $content;
            } else {
                $suffix = self::$suffix[$c->lang];
                $params[$c->position . $suffix] = $content;
            }
        }
        if ($exclude != 'footer_e') {
            $greet_key_en = Lang::get('js_localize.tiny_mce.init.nl_greet', [], 'en');
            $greet_key_de = Lang::get('js_localize.tiny_mce.init.nl_greet', [], 'de');

            $pos_en = strpos($params['content_en'], $greet_key_en);
            if ($pos_en !== false) {
                $params['content_en'] = substr_replace($params['content_en'], '{{$user_greet_en}}', $pos_en, strlen($greet_key_en));
            }

            $pos_de = strpos($params['content_de'], $greet_key_de);
            if ($pos_de !== false) {
                $params['content_de'] = substr_replace($params['content_de'], '{{$user_greet_de}}', $pos_de, strlen($greet_key_de));
            }
        }
        return $params;
    }
}

<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Description of CommentController
 *
 * @author i.traykov
 */
class CommentController extends RestController
{


    /**
     * Return List of All Comments Pages From lang
     * URL: comments/all/{$lang}
     * METHOD: GET
     */
    public function getAll($lang = 0)
    {

        $sort_fields = ['page_id', 'user_id', 'subject', 'body', 'active', 'updated_at', 'created_at'];
        $req = Request::all();

        if (empty($req['filter'])) {
            $filter = null;
        } else {
            $filter = $req['filter'];
        }
        if (!empty($req['sort-by']) && in_array($req['sort-by'], $sort_fields)) {
            $field = $req['sort-by'];
        } else {
            $field = null;
        }
        if (!empty($req['sort-direction']) && strtoupper($req['sort-direction']) == "DESC") {
            $order = "DESC";
        } else {
            $order = "ASC";
        }

        if (is_null($filter) && is_null($field)) {
            $comments_query = Comment::query();
        } elseif (!is_null($filter) && is_null($field)) {
            $comments_query = Comment::where('subject', 'like', "%" . $filter . "%")->orderBy('subject', $order);
        } elseif (!is_null($filter) && !is_null($field)) {
            $comments_query = Comment::where($field, 'like', "%" . $filter . "%")->orderBy($field, $order);
        }

        if ($lang > 0) {
            $comments_query->where('comment.lang', '=', $lang);
        }

        $comments_query->leftJoin('user', 'comment.user_id', '=', 'user.id')
            ->join('page', 'comment.page_id', '=', 'page.id')->where('page.active', '=', 1)
            ->join('content', 'page.u_id', '=', 'content.page_u_id')->where('section', '=', 'title');
        $comments_query->whereRaw('comment.lang=content.lang');

        $comments_query->select('comment.*', 'username', 'content.body as page_title');

        $filtered_comments = $comments_query->get();

        foreach($filtered_comments as $comment) {
            if ($comment->user == null) {
                $dummyuser = new User();
                $comment->setUserAttribute($dummyuser->getDeletedUserData($comment->user_id));
            }
        }

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of comments found.', $filtered_comments), 200);

        return $jsonResponse;
    }

    /**
     * Return Comment by ID
     * URL: comments/read/{ID}
     * METHOD: GET
     */
    public function getRead($id = null)
    {
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $comment = Comment::find($id);

        if (is_null($comment)) {
            return Response::json(RestController::generateJsonResponse(true, 'Comment not found.'), 404);
        } else {
            $content = $comment->page()->where("page.active", '=', 1)->first()->contents()->where('section', '=', 'title')->first();
            if ($content) {
                $comment->page_title = $content->body;
            }
            $user = $comment->user()->first();
            if ($user) {
                $comment->username = $user->username;
            } else {
                $comment->username = "anonymous";
            }

            return Response::json(RestController::generateJsonResponse(false, 'Comment found.', $comment));
        }
    }

    /**
     * Return Comments for Page
     * URL: comments/readbypage
     * METHOD: GET
     */

    public function getReadbypage()
    {

        $req = Request::all();

        if (empty($req['page_id'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }

        if (!isset($req['lang']) || $req['lang'] == 0) {
            $comments = Comment::where('page_id', '=', $req['page_id'])->get();
        } else {
            $comments = Comment::where('page_id', '=', $req['page_id'])->where('lang', '=', $req['lang'])->get();
        }

        foreach ($comments as $comment) {
            $content = $comment->page()->where("page.active", '=', 1)->first()->contents()->where('section', '=', 'title')->first();
            if ($content) {
                $comment->page_title = $comment->page()->first()->contents()->where('section', '=', 'title')->first()->body;
            }
            $user = $comment->user()->first();
            if ($user) {
                $comment->username = $user->username;
            } else {
                $comment->username = "anonymous";
            }
        }

        return Response::json(RestController::generateJsonResponse(false, 'Page Comments found.', $comments));
    }

    /**
     * Return Comments for User
     * URL: comments/readbyuser
     * METHOD: GET
     */

    public function getReadbyuser()
    {

        $req = Request::all();

        if (empty($req['user_id'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter user_id is required!'), 400);
        }

        if (!isset($req['lang']) || $req['lang'] == 0) {
            $comments = Comment::where('user_id', '=', $req['user_id'])->get();
        } else {
            $comments = Comment::where('user_id', '=', $req['user_id'])->where('lang', '=', $req['lang'])->get();
        }

        foreach ($comments as $comment) {
            $content = $comment->page()->where("page.active", '=', 1)->first()->contents()->where('section', '=', 'title')->first();
            if ($content) {
                $comment->page_title = $comment->page()->first()->contents()->where('section', '=', 'title')->first()->body;
            }
            $user = $comment->user()->first();
            if ($user) {
                $comment->username = $user->username;
            } else {
                $comment->username = "anonymous";
            }
        }

        return Response::json(RestController::generateJsonResponse(false, 'User Comments found.', $comments));
    }

    /**
     * Return Save comment from JSON
     * URL: comments/create
     * METHOD: POST
     */
    public function postCreate()
    {

        $newcomment = Request::json()->all();

        if (empty($newcomment['page_id'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }

        if (empty($newcomment['user_id'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter user_id is required!'), 400);
        }

        $comment = new Comment();
        $comment->fill($newcomment);

        return $this->trySave($comment, false);
    }

    /**
     * Return Update Comment from JSON
     * URL: comments/update/
     * METHOD: PUT
     */
    public function putUpdate()
    {
        $updatecomment = Request::json()->all();
        //TODO validate
        if (empty($updatecomment['id'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }
        $comment = Comment::find($updatecomment['id']);
        if (is_null($comment)) {
            return Response::json(RestController::generateJsonResponse(true, 'Comment not found.', $updatecomment), 404);
        }

        $comment->fill($updatecomment);

        return $this->trySave($comment, true);
    }

    /**
     * Return save result
     */
    protected function trySave(Comment $comment, $isUpdate)
    {
        if ($isUpdate) {
            $message = "Comment was updated.";
        } else {
            $message = "Comment was added.";
        }

        try {
            $comment->save();
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            throw new ModelValidationException($e->getErrors());
        }

        if ($comment->page()->first()->contents()->where('section', '=', 'title')->first()) {
            $comment->page_title = $comment->page()->first()->contents()->where('section', '=', 'title')->first()->body;
        }
        if ($comment->user()->first()) {
            $comment->username = $comment->user()->first()->username;
        } else {
            $comment->username = "anonymous";
        }

        //Success !!!
        return Response::json(RestController::generateJsonResponse(false, $message, $comment));
    }

    /**
     * Delete a comment by ID
     * 
     * Return Deleted Comment
     * URL: comments/delete/{$id}
     * METHOD: DELETE
     */
    public function deleteDelete($id = null)
    {
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $comment = Comment::find($id);
        if (is_null($comment)) {
            return Response::json(RestController::generateJsonResponse(true, 'Comment not found.'), 404);
        }
        $page = $comment->page()->first();
        if (is_null($page) || !UserAccessHelper::hasAccessForPageAndAscendants($page->id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
        }

        // Save Page Title and User before delete
        $contentTitle = $page->contents()->where('section', '=', 'title')->first();
        if (!is_null($contentTitle)) {
            $comment->page_title = $contentTitle->body;
        }
        if ($comment->user()->first()) {
            $comment->username = $comment->user()->first()->username;
        } else {
            $comment->username = "anonymous";
        }
        $deletedcomment = $comment;

        $comment->delete();

        return Response::json(RestController::generateJsonResponse(false, 'Comment was deleted.', $deletedcomment));
    }

    /**
     * Force Delete Comment only for tests
     * URL: comments/forcedelete/{$id}
     * METHOD: DELETE
     */
    public function deleteForcedelete($id = null)
    {

        $comment = Comment::withTrashed()->find($id);
        $comment->forceDelete();
    }
}

<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Models\Access;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Description of CartController
 *
 * @author i.traykov
 */
class AccessController extends RestController
{

    /**
     * Return Page access Groups
     * URL: access/getReadbypage
     * METHOD: GET
     */

    public function getReadbypage($pageId = 0)
    {
        //parameter pageId must be set
        if ($pageId <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter pageId is required!'), 400);
        }

        $access_items = Access::readbypage($pageId)->get();

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of accesses found.', $access_items), 200);

        return $jsonResponse;
    }


    /**
     * Grant Group access to Page
     * URL: access/grant
     * METHOD: GET
     */

    public function postGrant()
    {

        $req = Request::all();

        //parameter fileId must be set
        if (!isset($req['group_id']) || $req['group_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter group_id is required!'), 400);
        }
        if (!isset($req['page_id']) || $req['page_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }

        $access = new Access();
        $access->fill($req);

        //unset($cart->id);

        return $this->trySave($access, false);
    }

    /**
     * Return save result
     * @param Access $access
     * @param $isUpdate
     * @return
     */
    protected function trySave($access, $isUpdate)
    {
        try {
            if ($isUpdate) {
                $message = "Access was updated.";
            } else {
                $message = "Access was granted.";
            }

            $access->save();

            unset($access->id);
            //Success !!!

            $result = Response::json(RestController::generateJsonResponse(false, $message, $access));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
			// Get messages
			$messages = $e->getErrors();

			$result = Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.validation.failed'), $messages), 400);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            if (0 === strpos($message, 'SQLSTATE[23000]')) {
                $access = Access::withTrashed()->where('group_id', '=', $access->group_id)->where('page_id', '=', $access->page_id)->first();
                if ($access->trashed()) {
                    Access::withTrashed()->where('group_id', '=', $access->group_id)->where('page_id', '=', $access->page_id)->update(['deleted_at' => null]);
                    $message = "Access was granted.";
                    $result = Response::json(RestController::generateJsonResponse(false, $message, $access));
                } else {
                    $result = Response::json(RestController::generateJsonResponse(true, 'Duplicate record.'), 500);
                }
            } else {
                $result = Response::json(RestController::generateJsonResponse(true, $message), 500);
            }
        }

        return $result;
    }


    /**
     * Revoke Group access to Page
     * URL: access/revoke
     * METHOD: DELETE
     */

    public function deleteRevoke()
    {

        $req = Request::all();

        //parameters page_id and group_id must be set
        if (!isset($req['group_id']) || $req['group_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter group_id is required!'), 400);
        }
        if (!isset($req['page_id']) || $req['page_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }

        $accessitem = Access::where('group_id', '=', $req['group_id'])->where('page_id', '=', $req['page_id'])->get();
        if ($accessitem->isEmpty()) {
            return Response::json(RestController::generateJsonResponse(true, 'Access not found.'), 404);    // in response access not exists
        }
        $deleteditem = $accessitem[0];
        Access::where('group_id', '=', $deleteditem->group_id)->where('page_id', '=', $deleteditem->page_id)->delete();

        return Response::json(RestController::generateJsonResponse(false, 'Access was revoked!', $accessitem[0]));
    }

    /**
     * Revoke Group access to Page for unit tests
     * URL: access/forcedeletebypage
     * METHOD: DELETE
     */
    public function deleteForcedeletebypage(){
        $req = Input::all();

        if(!isset($req['page_id']) || $req['page_id'] <= 0){
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }
        $accessitem = Access::withTrashed()->where('page_id','=',$req['page_id'])->get();
        if ($accessitem->isEmpty()) {
            return Response::json(RestController::generateJsonResponse(true, 'Access not found.'), 404);    // in response access not exists
        }

        Access::withTrashed()->where('page_id','=',$req['page_id'])->delete();

        return Response::json(RestController::generateJsonResponse(false, 'Access was revoked!', $accessitem));
    }
 
    public static function updatePageAccess($pageId, $arrNewAccess)
    {
        $access_items = Access::withTrashed()->readbypage($pageId)->get();
        // Update page access
        foreach ($access_items as $access) {
            // Check if old access still exists
            $bKeepAccess = false;
            $indexAccess = -1;
            foreach ($arrNewAccess as $key => $pageGroup) {
                if ($access->group_id == $pageGroup['group_id']) {
                    $indexAccess = $key;
                    $bKeepAccess = true;
                    break;
                }
            }
            if ($bKeepAccess) {
                // Remove it from New Access
                unset($arrNewAccess[$indexAccess]);
                if ($access->trashed()) {
                    Access::withTrashed()
                        ->where('group_id', '=', $access->group_id)
                        ->where('page_id', '=', $access->page_id)
                        ->restore();
                } else {
                    // Notting ot maybe Update!
                }
            } else {
                if (!$access->trashed()) {
                    // Delete the Access
                    Access::where('group_id', '=', $access->group_id)
                        ->where('page_id', '=', $access->page_id)
                        ->delete();
                }
            }
        }

        // Save new access
        foreach ($arrNewAccess as $pageGroup) {
            $access = new Access();
            $access->page_id = $pageId;
            $access->group_id = $pageGroup['group_id'];
            $access->save();
        }
        //Start implementation of page access change for user wich had added page previously
//        $page_access = Access::select('group_id')->readbypage($pageId)->pluck('group_id');
//        $deletedCarts = Cart::withTrashed()
//                ->join('download','download.file_id', '=', 'cart.file_id')
//                ->join('page', 'page.u_id' ,'=', 'download.page_u_id')
//                ->join('user', 'user.id', '=', 'cart.user_id')
//                ->join('usergroup', 'usergroup.user_id', '=', 'user.id')
//                ->where('page.id', $pageId)
//                ->where('page.active', 1)
//                ->whereNull('page.deleted_at')
//                ->whereNull('download.deleted_at')
//                ->whereIn('user.group_id', $page_access)
//                ->whereNotNull('cart.deleted_at')
//                ->select('cart.*')
//                ->get();
//        foreach ($deletedCarts as $deletedCart) {
//            $deletedCart->restore();
//        }
    }
}

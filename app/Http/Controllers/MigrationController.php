<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Helpers\Encrypter;


class MigrationController extends Controller
{

    public function migrate()
    {

//        $me = User::where("username", "test2")->first();
//        dd(
//            Encrypter::decrypt($me->newsletter_code)
//        );
        $users = User::all();
        foreach ($users as $user) {
            printf("Working on <strong>%s</strong>. ", $user->username);

            $user->first_name = Encrypter::decrypt($user->first_name);
            $user->last_name = Encrypter::decrypt($user->last_name);
            $user->email = Encrypter::decrypt($user->email);
            $user->company = Encrypter::decrypt($user->company);
            $user->position = Encrypter::decrypt($user->position);
            $user->department = Encrypter::decrypt($user->department);
            $user->code = Encrypter::decrypt($user->code);
            $user->city = Encrypter::decrypt($user->city);
            $user->country = Encrypter::decrypt($user->country);
            $user->phone = Encrypter::decrypt($user->phone);
            $user->fax = Encrypter::decrypt($user->fax);
            $user->newsletter_code = Encrypter::decrypt($user->newsletter_code);
            if ($user->save()) {
                printf("User <strong>%s</strong> data was decrypted.</br>", $user->username);
                continue;
            }
            printf("An error occurred while decrypting the user's data. Check the individual table record by name = %s .\n\r", $user->username);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Models\Favorite;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Description of CartController
 *
 * @author i.traykov
 */
class FavoriteController extends RestController
{

    /**
     * Return User favorite Pages
     * URL: favorite/getFavorites
     * METHOD: GET
     */

    public function getFavorites($userId = 0)
    {
        //parameter userId must be set
        if ($userId <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter userId is required!'), 400);
        }

        $page_ids = Favorite::where('user_id', '=', $userId)->get();
        $page_items = [];
        foreach ($page_ids as $page_id) {
            $page_item = $page_id->page()->first()->toArray();
            $page_items[] = $page_item;
        }
        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of pages found.', $page_items), 200);

        return $jsonResponse;
    }

    /**
     * Add Page to User Favorites
     * URL: favorite/createfavorite
     * METHOD: GET
     */

    public function postCreatefavorite()
    {

        $req = Request::all();

        //parameter fileId must be set
        if (!isset($req['user_id']) || $req['user_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter user_id is required!'), 400);
        }
        if (!isset($req['page_id']) || $req['page_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }

        $favorite = new Favorite();
        $favorite->fill($req);

        //unset($cart->id);

        return $this->trySave($favorite, false);
    }

    /**
     * Return save result
     */
    protected function trySave(Favorite $favorite, $isUpdate)
    {
        if ($isUpdate) {
            $message = "Favorite was updated.";
        } else {
            $message = "Favorite was added.";
        }

        try {
            $favorite->save();
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            throw new ModelValidationException($e->getErrors());
        }

        unset($favorite->id);

        //Success !!!

        return Response::json(RestController::generateJsonResponse(false, $message, $favorite));

    }


    /**
     * Remove Page from user favorites
     * URL: favorite/removefavorite
     * METHOD: DELETE
     */

    public function deleteRemovefavorite()
    {

        $req = Request::all();

        //parameters fileId and userId must be set
        if (!isset($req['user_id']) || $req['user_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter user_id is required!'), 400);
        }
        if (!isset($req['page_id']) || $req['page_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }

        $favoriteitem = Favorite::where('user_id', '=', $req['user_id'])->where('page_id', '=', $req['page_id'])->get();

        if ($favoriteitem->isEmpty()) {
            return Response::json(RestController::generateJsonResponse(true, 'Favorite not found.', $favoriteitem), 404);   // in response favorite not exists
        }

        $deleteditem = $favoriteitem[0];
        Favorite::where('user_id', '=', $deleteditem->user_id)->where('page_id', '=', $deleteditem->page_id)->delete();

        return Response::json(RestController::generateJsonResponse(false, 'Favorite was deleted!', $favoriteitem[0]));
    }

    /* Insert test user for test */

    public function postInsertuser()
    {

        DB::unprepared("INSERT INTO `user` (`id`, `username`, `pwd`, `first_name`, `last_name`, `email`, `company`, `position`, `department`, `address`, `code`, `city`, `country`, `phone`, `fax`, `active`, `last_login`, `logins`, `unlock_code`, `role`, `newsletter`, `reset_key`, `reset_expires`, `force_pwd_change`, `created_at`, `updated_at`) VALUES
(1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '2013-12-16 00:00:00', 1, '', 1, 1, '', '" . DATE_TIME_ZERO . "', 0, '" . DATE_TIME_ZERO . "', '" . DATE_TIME_ZERO . "');");
    }

    /* Insert test user for test */

    public function postDeleteuser()
    {

        DB::unprepared("DELETE FROM `user` WHERE id=1;");
    }
}

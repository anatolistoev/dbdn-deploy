<?php

namespace App\Http\Controllers;

use App\Helpers\FESQLHelper;
use App\Helpers\DispatcherHelper;
use App\Helpers\HtmlHelper;
use App\Models\Content;
use App\Models\Tag;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class SearchPageController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int  $qparam
     * @return Response
     */
    public function showResults($qparam = false)
    {
        $q = ($qparam !== false ? urldecode($qparam) : Request::get('search'));

        if ($this->lang == 1) {
            $altLang = 2;
        } else {
            $altLang = 1;
        }

        
        if ($q!=strip_tags($q)) {
            // Disallow any tags in search query
            return view(
            "search_results",
            [
                    'PAGE'         => (object)['id' => 'search', 'slug' => 'search', 'brand' => DAIMLER, 'template' => 'search_results',
                        'langs' => 3],
                    'ROWS'         => array(),
                    'Q'            => "",
                    'BRAND_TITLES' => array(),
                    'ALT_LANG'     => $altLang,
                    'FILTERED'     => false,
                        ]
            );
        }
        
        $brand_keys_array = array_keys(config('app.pages'));
        $brand_titles     = Content::select(DB::raw('content.body as title'), 'page.id as page_id')
                        ->join('page', 'page.u_id', '=', 'content.page_u_id')
                        ->whereIn('page.id', $brand_keys_array)
                        ->where('page.active', '=', 1)
                        ->where('content.lang', '=', $this->lang)
                        ->where('content.section', '=', 'title')
                        ->orderBy('title', 'ASC')->get();

        if (Request::has('filter') && Request::get('filter') == 'active') {
            $blade    = 'partials.results';
            $filtered = true;
        } else {
            $blade    = 'search_results';
            $filtered = true;
        }

        if (strlen($q) < 3) {
            return view(
                $blade,
                [
                        'PAGE'         => (object)['id' => 'search', 'slug' => 'search', 'brand' => DAIMLER, 'template' => 'search_results',
                            'langs' => 3],
                        'ROWS'         => [],
                        'PAGES'        => [],
                        'ASCENDANTS'   => [],
                        'Q'            => $q,
                        'BRAND_TITLES' => $brand_titles,
                        'ALT_LANG'     => $altLang,
                        'FILTERED'     => $filtered,
                            ]
            );
        }
        // Removes the results count string - [N] from the search term.
        $q           = preg_replace('%\s*\[\d+\]\s*$%i', '', $q);
        // $q           = str_replace(array( '(', ')' ), '', $q);
        $searchTerms = explode(' ', $q, 5);
        $lang        = Session::get('lang');
        $query       = DB::table('content')
                ->select(
                    'page.id',
                    'page.u_id',
                    'page.slug',
                    'page.template',
                    'ctitle.body AS title',
                    'cdescr.body AS description',
                    'page.visits',
                    'page.authorization',
                    'page.updated_at',
                    DB::raw('(SELECT count(*) FROM rating WHERE page_id = page.id) as rating'),
                    DB::raw('(SELECT tag.NAME FROM pagetag
                                     JOIN tag on tag.id = pagetag.tag_id
                                     WHERE pagetag.page_id = page.u_id
                                     AND pagetag.deleted_at IS NULL
                                     AND tag.deleted_at IS NULL
                                     AND (page.template = "downloads" OR page.template = "best_practice")
                                     AND tag.lang = ' . $lang . ' ORDER BY pagetag.updated_at LIMIT 1) as tags ')
                    //                            DB::raw('(SELECT GROUP_CONCAT(tag.name ORDER BY pagetag.updated_at SEPARATOR ", ")
                    //                                        FROM pagetag JOIN tag on tag.id = pagetag.tag_id
                    //                                        WHERE pagetag.page_id = page.u_id
                    //                                        AND pagetag.deleted_at IS NULL
                    //                                        AND tag.deleted_at IS NULL
                    //                                        AND (page.template = "downloads" OR page.template = "best_practice")
                    //                                        AND tag.lang = '.$lang.'
                    //                                        GROUP BY pagetag.page_id LIMIT 1) as tags ')
                )
                ->leftJoin('page', function ($join) {
                    return $join->on('page.u_id', '=', 'content.page_u_id');
                })
                ->leftJoin('content as ctitle', function ($join) {
                    return $join->on('ctitle.page_u_id', '=', 'page.u_id')
                            ->on('ctitle.lang', '=', DB::raw(Session::get('lang')))
                            ->on('ctitle.section', '=', DB::raw("'title'"));
                })
                ->leftJoin('content as cdescr', function ($join) {
                    return $join->on('cdescr.page_u_id', '=', 'page.u_id')
                            ->on('cdescr.lang', '=', DB::raw(Session::get('lang')))
                            ->on('cdescr.section', '=', DB::raw("'main'"));
                })
                ->whereRaw('page.langs >> (?-1) & 1 = 1', [$lang])
                ->where('page.id', '<>', '1')
                ->where('page.overview', 0)
                ->where('page.template', '<>', 'brand')
                ->where('content.lang', $lang)
                ->where('page.active', 1)
                ->whereRaw("(page.publish_date = '" . DATE_TIME_ZERO . "' OR page.publish_date < '" . date('Y-m-d H:i:s') . "')
							AND (page.unpublish_date = '" . DATE_TIME_ZERO . "' OR page.unpublish_date > '" . date('Y-m-d H:i:s') . "')")
                ->where('page.searchable', 1)
                ->where('content.searchable', 1)
                ->whereRaw('((SELECT Count(*)
                        FROM hierarchy hAlive
                        LEFT JOIN page pAlive ON hAlive.parent_id = pAlive.id
                        WHERE hAlive.page_id = page.id AND pAlive.active = 1 AND pAlive.id <> page.id
                        AND hAlive.deleted_at is NULL
                        AND ( pAlive.publish_date = "' . DATE_TIME_ZERO . '" OR pAlive.publish_date < "' . date('Y-m-d H:i:s') . '")
                        AND ( pAlive.unpublish_date = "' . DATE_TIME_ZERO . '"	OR pAlive.unpublish_date > "' . date('Y-m-d H:i:s') . '"))
                        =
                        (Select hierarchy.level FROM hierarchy where hierarchy.page_id = page.id AND hierarchy.parent_id = 1))')
                ->whereRaw("EXISTS (
						SELECT * FROM hierarchy h3
						WHERE h3.page_id = page.id
                        AND h3.deleted_at is NULL
						AND h3.parent_id = 1
					)");
        $query->addSelect(DB::raw("(SELECT GROUP_CONCAT(content.body ORDER BY hierarchy.LEVEL DESC SEPARATOR ' / ')
                                                FROM hierarchy
                                                JOIN page as p1 ON hierarchy.parent_id = p1.id
                                                JOIN content ON content.page_u_id = p1.u_id
                                                WHERE hierarchy.page_id = page.id
                                                    AND hierarchy.deleted_at IS NULL
                                                    AND hierarchy.LEVEL <> 0
                                                    AND hierarchy.parent_id <> 1
                                                    AND content.lang = " . $lang ."
                                                    AND content.section = 'title'
                                                    AND content.deleted_at IS NULL
                                                    AND p1.deleted_at IS NULL
                                                    AND p1.active = 1) as path"));
//                                                    AND (p1.publish_date = '".DATE_TIME_ZERO."' OR p1.publish_date < '".date('Y-m-d H:i:s')."')
//                                                    AND (p1.unpublish_date = '".DATE_TIME_ZERO."' OR p1.unpublish_date > '".date('Y-m-d H:i:s')."')
//                                                    AND ((SELECT Count(*) FROM hierarchy hAlive
//                                                            LEFT JOIN page pAlive ON hAlive.parent_id = pAlive.id
//                                                            WHERE hAlive.page_id = p1.id AND pAlive.active = 1 AND pAlive.id <> page.id
//                                                            AND ( pAlive.publish_date = '".DATE_TIME_ZERO."' OR pAlive.publish_date < '".date('Y-m-d H:i:s')."')
//                                                            AND ( pAlive.unpublish_date = '".DATE_TIME_ZERO."'	OR pAlive.unpublish_date > '".date('Y-m-d H:i:s')."'))
//                                                        =
//                                                        (Select hierarchy.level FROM hierarchy where hierarchy.page_id = page.id AND hierarchy.parent_id = 1)) as path"));

        if (Request::has('brand') && Request::get('brand') != '') {
            $brands = explode(',', Request::get('brand'));
            $clause = '(';
            $count  = 0;
            foreach ($brands as $br) {
                $clause .= ($count != 0 ? ',' : '') . '?';
                $count++;
            }
            $clause .= ')';
            $query->whereRaw("EXISTS (
						SELECT * FROM hierarchy h4
						WHERE h4.page_id = page.id
                        AND h4.deleted_at is NULL
						AND h4.parent_id IN " . $clause . "
					)", $brands);
        }
        if (Request::has('time_from') && Request::get('time_from') != '') {
            $query = $query->where('page.updated_at', '>=', date('Y-m-d H:i:s', strtotime(Request::get('time_from'))));
        }
        if (Request::has('time_to') && Request::get('time_to') != '') {
            $query = $query->where('page.updated_at', '<=', date('Y-m-d H:i:s', strtotime(Request::get('time_to'))));
        }

        // Prepare sort order
        $sort_field = 'updated_at';
        $sort_a_z   = false;
        if (Request::has('time') && Request::get('time')) {
            $query = $query->orderBy('page.updated_at', Request::get('time'));
            $sort_a_z   = ('asc' == Request::get('time'));
        } elseif (Request::has('viewed') && Request::get('viewed')) {
            $sort_field = 'visits';
            $query = $query->orderBy('page.visits', Request::get('viewed'));
        } elseif (Request::has('rating') && Request::get('rating')) {
            $sort_field = 'rating';
            $query = $query->orderBy('rating', Request::get('rating'));
        } else {
            $query = $query->orderBy('page.updated_at', 'desc');
        }

        // Downloads
        $fileQuery = clone $query;

        // Tags
        $tagQuery = clone $query;
        $tagQuery->join('pagetag', 'page.u_id', '=', 'pagetag.page_id')
                ->join('tag', 'tag.id', '=', 'pagetag.tag_id')
                ->whereNull('pagetag.deleted_at')
                ->whereNull('tag.deleted_at');
        $tag_id   = Request::get('tag_id');
        if ($tag_id && $tag_id != "") {
            $tagQuery->where('pagetag.tag_id', '=', $tag_id);
        } else {
            $tagQuery->where('tag.name', '=', $q)
                    ->where('tag.lang', '=', $lang);
        }
        $tagResult  = $tagQuery->distinct()->get();
        $term_regex = '';
        foreach ($searchTerms as $term) {
            /*file_put_contents('/home/www/logs/search.log', 'Before: '.var_export($term,true)."\n",FILE_APPEND);
            $term2=preg_quote($term);
            file_put_contents('/home/www/logs/search.log', 'After: '.var_export($term2,true)."\n",FILE_APPEND);*/
            $term = str_replace(array( '(', ')' ), '', $term);
            $term_regex .= '[[:<:]]' . preg_quote($term) . '[[:>:]].*';
            //$query->whereRaw("content.body REGEXP ?",array($term_regex));
        }
        
        $query->whereRaw("content.body REGEXP ?", [$term_regex]);
        // file_put_contents('/home/www/logs/search.log', 'Full regex2: '.var_export($term_regex,true)."\n",FILE_APPEND);
        // Downloads
        
        $fileQuery->join('download', 'page.u_id', '=', 'download.page_u_id')
                ->join('fileinfo', 'fileinfo.file_id', '=', 'download.file_id')
                ->whereNull('download.deleted_at')
                ->whereNull('fileinfo.deleted_at')
                ->where('fileinfo.lang', '=', $lang)
                ->whereRaw("fileinfo.description REGEXP ?", [$term_regex])
                ->groupBy('download.page_u_id')
                ->addSelect('download.page_u_id', 'download.page_id as id', 'fileinfo.file_id', 'fileinfo.description', 'fileinfo.updated_at as file_updated_at');

        $fileResults = $fileQuery->distinct()->get();

        $contentResults = $query->distinct()->get();

        $tempArr = array_udiff($contentResults->toArray(), $tagResult->toArray(), 'self::compareResults');
        $results = array_merge($tagResult->toArray(), $tempArr);
        $tempArr = array_udiff($results, $fileResults->toArray(), 'self::compareResults');
        $results = array_merge($fileResults->toArray(), $tempArr);
        //echo '<!--';
        //var_dump(DB::getQueryLog());
        //echo '-->';
        $downloads_to_load = [];
        $blocks  = [];
        $dom     = HtmlHelper::getDOMDocument();

        foreach ($results as $row) {
            $blocks[$row->id]['slug']       = $row->slug;
            $blocks[$row->id]['title']      = HtmlHelper::fixDoubleDaggerForDaimler($row->title);
            $blocks[$row->id]['rating']     = $row->rating;
            $blocks[$row->id]['tags']       = $row->tags;
            // $blocks[$row->id]['tags']       = HtmlHelper::generateTagsDots($row->tags);
            $blocks[$row->id]['visits']     = $row->visits;
            $blocks[$row->id]['path']       = HtmlHelper::fixDoubleDaggerForDaimler($row->path);
            $blocks[$row->id]['template']   = $row->template;
            $blocks[$row->id]['updated_at'] = \Carbon\Carbon::createFromTimeStamp(strtotime($row->updated_at));

            if ($row->template == 'downloads') {
                $blocks[$row->id]['text']       = HtmlHelper::getSummaryText($row->description, 500, false);
                $blocks[$row->id]['protected']  = ($row->authorization == 1);
                if (isset($row->file_updated_at)) {
                    $blocks[$row->id]['updated_at'] = \Carbon\Carbon::createFromTimeStamp(strtotime($row->file_updated_at));
                } else {
                    $downloads_to_load[] = $row->u_id;
                }
            } else {
                $text = trim($row->description);
                if (strlen($text)>10) {
                    $html = mb_convert_encoding($text, 'HTML-ENTITIES', 'UTF-8');
                    $dom->loadHTML($html);
                    $finder = new \DOMXPath($dom);

                    $code = $dom->documentElement;
                    // set $blocks[$row->id]['text']
                    HtmlHelper::getSummaryFromNode($code, $row->id, $searchTerms, $blocks, $finder, false, 500, false);
                    unset($finder);
                } else {
                    $blocks[$row->id]['text'] = $text;
                    Log::debug("Missing text for row: " . print_r($row, 1));
                }
            }
        }
        //DispatcherHelper::seekInHTML($html, $blocks, false, false, null, $searchTerms, 500, false);

        $foundDownloadsCount = $this->addSearchTextToDownloads($downloads_to_load, $blocks);

        if (count($downloads_to_load) != $foundDownloadsCount) {
            Log::warning("Dawnloads Found: " . $foundDownloadsCount . " from " . count($downloads_to_load));
        }

        // Sort the result
        usort($blocks, function ($item1, $item2) use ($sort_field, $sort_a_z) {
            if (!isset($item1[$sort_field])) {
                Log::warning("Item1. Missing field: " . $sort_field . " " . print_r($item1, 1));
                return -1;
            }
            if (!isset($item2[$sort_field])) {
                Log::warning("Item2. Missing field: " . $sort_field . " " . print_r($item2, 1));
                return 1;
            }
            if ($item1[$sort_field] == $item2[$sort_field]) {
                return 0;
            }
            if ($sort_a_z) {
                return $item1[$sort_field] < $item2[$sort_field] ? -1 : 1;
            } else {
                return $item1[$sort_field] < $item2[$sort_field] ? 1 : -1;
            }
        });

        // Mark Tags
        foreach ($blocks as & $row) {
            foreach ($searchTerms as $term) {
                $row['text'] = preg_replace(
                    '/\b(' . preg_quote($term, '/') . ')\b/ui',
                    '<span class="searched-tag">' . htmlentities('$1') . '</span>',
                    $row['text']
                );
            }
        }

        return view(
            $blade,
            [
                    'PAGE'         => (object)['id' => 'search', 'slug' => 'search', 'brand' => DAIMLER, 'template' => 'search_results',
                        'langs' => 3],
                    'ROWS'         => $blocks,
                    'Q'            => htmlspecialchars($q),
                    'BRAND_TITLES' => $brand_titles,
                    'ALT_LANG'     => $altLang,
                    'FILTERED'     => $filtered,
                        ]
        );
    }

    /*     * **********************************************************
     * Advanced Search form
     * ********************************************************** */
    public function advancedForm()
    {
        return view(
            'advanced_search',
            [
                    'PAGE'       => (object)['id' => 'advanced_search', 'slug' => 'advanced_search'],
                    'ASCENDANTS' => [],
                        ]
        );
    }

    public static function compareResults($obj_a, $obj_b)
    {
        return $obj_a->id - $obj_b->id;
    }

    public function searchAutocomplete($type, $brand = '')
    {
        $req = Request::all();
        if ($type == 'faq') {
            if ($brand == 'smart') {
                $brand_id = SMART;
            } else {
                $brand_id = DAIMLER;
            }
            //TODO: Implement counter faq_count!
            $tagsQuery = Tag::select(DB::raw("concat(`name`, ' [', `page_count`,']') as `value`"), 'tag.id')
                ->join('faqtag', 'tag.id', '=', 'faqtag.tag_id')
                ->join('faq', 'faq.id', '=', 'faqtag.faq_id')
                ->whereNull('faqtag.deleted_at')
                ->whereNull('faq.deleted_at')
                ->where('faq.brand_id', '=', $brand_id);
        } else {
            $tagsQuery = Tag::select(DB::raw("concat(`name`, ' [', `page_count`,']') as `value`"), 'tag.id')
                ->join('pagetag', 'tag.id', '=', 'pagetag.tag_id')
                ->join('page', 'page.u_id', '=', 'pagetag.page_id')
                ->whereRaw('page.active = 1')
                ->whereNull('pagetag.deleted_at')
                ->where('tag.page_count', '>', '0');
        }

        $tags = $tagsQuery->where('tag.lang', '=', $this->lang)
                ->where('tag.name', 'like', "%" . $req['term'] . "%")
                ->groupBy('tag.id')
                ->orderBy('tag.name', 'ASC')
                ->get();

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of Tags found', $tags), 200);

        return $jsonResponse;
    }


    private static function addSearchTextToDownloads($searchedU_ids, &$blocks)
    {
        if (count($searchedU_ids) == 0) {
            return;
        }

        $results = FESQLHelper::getPagesFileData($searchedU_ids);
        foreach ($results as $key => $row) {
            $blocks[$row->id]['text']       = HtmlHelper::getSummaryText($row->description, 500, false);
            $blocks[$row->id]['updated_at'] = \Carbon\Carbon::createFromTimeStamp(strtotime($row->updated_at));
        }

        return count($results);
    }

}

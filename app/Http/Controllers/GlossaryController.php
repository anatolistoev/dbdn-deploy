<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Models\Glossary;
use App\Models\Term;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Description of CartController
 *
 * @author i.traykov
 */
class GlossaryController extends RestController
{

    /**
     * Return Glossary in Page
     * URL: glossary/glossary/{$page_id}?lang={$lang}
     * METHOD: GET
     */

    public function getGlossary($page_id = 0)
    {

        $req = Request::all();

        //parameter userId must be set
        if ($page_id <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }

        if (!isset($req['lang']) || $req['lang'] < 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter lang is required!'), 400);
        }

        $term_items = Term::ofLang($req['lang'])->glossaryOfPage($page_id)->get();
        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of terms found.', $term_items), 200);

        return $jsonResponse;
    }

    /**
     * Create Glossary
     * URL: glossary/create
     * METHOD: POST
     */

    public function postCreate()
    {

        $req = Request::all();

        //parameter fileId must be set
        if (!isset($req['page_id']) || $req['page_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }
        if (!isset($req['term_id']) || $req['term_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter term_id is required!'), 400);
        }

        $glossary = new Glossary();
        $glossary->fill($req);

        $max_pos = Glossary::where('page_id', '=', $req['page_id'])->max('position');
        //set position with one after the last item
        $glossary->position = $max_pos + 1;

        return $this->trySave($glossary, false);
    }

    /**
     * Return save result
     */
    protected function trySave(Glossary $glossary, $isUpdate)
    {
        try {
            if ($isUpdate) {
                // update query
                // laravel does not work with composite primary keys and update does not work
                Glossary::where('page_id', '=', $glossary->page_id)->where('term_id', '=', $glossary->term_id)->update(['position' => $glossary->position]);
                $message = "Glossary was updated.";
            } else {
                $glossary->save();
                unset($glossary->id);
                $message = "Glossary was added.";
            }
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            throw new ModelValidationException($e->getErrors());
        }

        return Response::json(RestController::generateJsonResponse(false, $message, $glossary));
    }


    /**
     * Modify Glossary
     * URL: glossary/modify
     * METHOD: PUT
     */

    public function putModify()
    {

        $req = Request::all();
        

        //parameters termId and userId must be set
        if (!isset($req['page_id']) || $req['page_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }
        if (!isset($req['term_id']) || $req['term_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter term_id is required!'), 400);
        }

        if (!isset($req['position']) || $req['position'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter position is required!'), 400);
        }
        //find last position
        $max_pos = Glossary::where('page_id', '=', $req['page_id'])->max('position');

        $glossary = Glossary::where('page_id', '=', $req['page_id'])->where('term_id', '=', $req['term_id'])->first();
        //set position to our element
        if ($req['position'] > $max_pos) {
            $glossary->position = $max_pos;
        } else {
            $glossary->position = $req['position'];
        }
        //reorder elements escept modifying element
        $page_glossaries = Glossary::where('page_id', '=', $req['page_id'])->where('term_id', '<>', $req['term_id'])->orderBy('position', 'asc')->get();
        $pos = 1;
        foreach ($page_glossaries as $pg) {
            //miss the position of modifying element
            if ($pos == $req['position']) {
                $pos++;
            }
            $pg->position = $pos;
            // update query because save wont work
            Glossary::where('page_id', '=', $pg->page_id)->where('term_id', '=', $pg->term_id)->update(['position' => $pos]);

            $pos++;
        }

        return $this->trySave($glossary, true);
    }


    /**
     * Remove Term From Page
     * URL: glossary/remove
     * METHOD: DELETE
     */

    public function deleteRemove()
    {

        $req = Request::all();

        //parameters fileId and userId must be set
        if (!isset($req['page_id']) || $req['page_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }
        if (!isset($req['term_id']) || $req['term_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter term_id is required!'), 400);
        }

        $glossary = Glossary::where('page_id', '=', $req['page_id'])->where('term_id', '=', $req['term_id'])->get();

        if ($glossary->isEmpty()) {
            return Response::json(RestController::generateJsonResponse(true, 'Glossary not found.'), 404);
        }

        $deleteditem = $glossary[0];
        Glossary::where('page_id', '=', $deleteditem->page_id)->where('term_id', '=', $deleteditem->term_id)->delete();

        $page_glossaries = Glossary::where('page_id', '=', $req['page_id'])->orderBy('position', 'asc')->get();
        $pos = 1;
        //rearrange items after deleting element
        foreach ($page_glossaries as $pg) {
            $pg->position = $pos;
            Glossary::where('page_id', '=', $req['page_id'])->where('term_id', '=', $req['term_id'])->update(['position' => $pos]);
            $pos++;
        }

        return Response::json(RestController::generateJsonResponse(false, 'Glossary was deleted!', $glossary[0]));
    }
}

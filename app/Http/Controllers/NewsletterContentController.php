<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Models\Newsletter;
use App\Models\NewsletterContent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Description of NewsletterContentController
 *
 * @author i.traykov
 */
class NewsletterContentController extends RestController
{

    /**
     * Return Content by ID
     * URL: nl_content/read/{ID}
     * METHOD: GET
     */
    public function getRead($id = null)
    {
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $content = NewsletterContent::find($id);

        if (is_null($content)) {
            return Response::json(RestController::generateJsonResponse(true, 'Content not found.'), 404);
        } else {
            return Response::json(RestController::generateJsonResponse(false, 'Content found.', $content));
        }
    }

    /**
     * Return Content for Language
     * URL: nl_contents/newsletter-id/{pageId}/{lang}?section={section}
     * METHOD: GET
     */
    public static function getNewsletterContent($nlId, $section = null, $lang = 0)
    {
        // Build query
        $contentsQuery = NewsletterContent::where('newsletter_id', '=', $nlId);

        if ($lang <> 0) {
            $contentsQuery->where('lang', '=', $lang);
        }

        if ($section) {
            $contentsQuery->where('position', '=', $section);
        }

        $contents = $contentsQuery->get()->toArray();

        return $contents;
        //$jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of contents found.', $contents), 200);

        //return $jsonResponse;
    }

    /**
     * Save content from JSON
     * URL: nl_contents/create
     * METHOD: POST
     */
    public function postCreate()
    {
        $newContent = Request::json()->all();

        return $this->createMany([$newContent]);
    }

    /**
     * Save content from JSON
     * URL: nl_contents/create-many
     * METHOD: POST
     */
    public function postCreateMany()
    {
        $newContentImput = Request::json()->all();

        return $this->createMany($newContentImput);
    }

    protected function createMany($arrCreateContent)
    {
        // Validate
        $arrNlID = [];
        foreach ($arrCreateContent as $newContent) {
            if (empty($newContent['newsletter_id'])) {
                return Response::json(RestController::generateJsonResponse(true, 'Parameter page ID is required!'), 400);
            }

            $nlID = $newContent['newsletter_id'];
            $arrNlID[$nlID] = $nlID;
        }

        $arrUniqueNlID = array_keys($arrNlID);
        $arrDBNl = Newsletter::findMany($arrUniqueNlID);
        if (empty($arrDBNl) || count($arrDBNl) != count($arrUniqueNlID)) {
            return Response::json(RestController::generateJsonResponse(true, 'Newsletter not found!'), 404);
        }

        $arrNewDBContent = new Collection();
        foreach ($arrCreateContent as $newContent) {
            // Clean the the ID
            unset($newContent['id']);

            // Purify HTML content in 'content'
            if (isset($newContent['content'])) {
                $newContent['content']=\Purifier::clean($newContent['content']);
            }
    
            $content = new NewsletterContent();
            $content->fill($newContent);
            $arrNewDBContent->add($content);
        }

        return $this->trySave($arrNewDBContent, false);
    }
    /**
     * Return Update content from JSON
     * URL: nl_contents/update/
     * METHOD: PUT
     */
    public function putUpdate()
    {
        $updateContentInput = Request::json()->all();
        
        return $this->updateMany([$updateContentInput]);
    }

    /**
     * Return Update content from JSON
     * URL: nl_contents/update-many/
     * METHOD: PUT
     */
    public function putUpdateMany()
    {
        $updateContentInput = Request::json()->all();
        
        return $this->updateMany($updateContentInput);
    }


    protected function updateMany($arrUpdateContent)
    {
        // Validate
        $arrUpdateContentID = [];
        foreach ($arrUpdateContent as $updateContent) {
            if (empty($updateContent['id'])) {
                return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
            }
            $arrUpdateContentID[] = $updateContent['id'];
        }
        // Load contents for update
        $arrDBContent = NewsletterContent::findMany($arrUpdateContentID);

        foreach ($arrUpdateContent as $updateContent) {
            $content = null;
            foreach ($arrDBContent as $struct) {
                if ($updateContent['id'] == $struct->id) {
                    $content = $struct;
                    break;
                }
            }

            if (is_null($content)) {
                return Response::json(RestController::generateJsonResponse(true, 'Content not found.', $updateContent), 404);
            }

            // Purify HTML content in 'content'
            if (isset($updateContent['content'])) {
                $updateContent['content']=\Purifier::clean($updateContent['content']);
            }
    
            $safeUpdateContent = array_intersect_key($updateContent, array_flip(NewsletterContent::$updateFillable));
            $content->fill($safeUpdateContent);
        }

        return $this->trySave($arrDBContent, true);
    }

    /**
     * Return save result
     */
    public function trySave(Collection $arrContent, $isUpdate = false)
    {
        try {
            // Start transaction!
            DB::beginTransaction();

            if ($isUpdate) {
                $message = "Content was updated.";
            } else {
                $message = "Content was added.";
            }

            foreach ($arrContent as $content) {
                $content->save();
            }

            //Success !!!
            // Commit the queries!
            DB::commit();

            $result = Response::json(RestController::generateJsonResponse(false, $message, $arrContent));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            // Rollback and then return errors
            DB::rollback();

            throw new ModelValidationException($e->getErrors());
        } catch (\Exception $e) {
            // Rollback and then throw the exception
            DB::rollback();

            throw $e;
        }

        return $result;
    }

    /**
     * Return Update content from JSON
     * URL: nl_contents/delete/{ID}
     * METHOD: DELETE
     */
    public function deleteDelete($id = null)
    {

        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $content = NewsletterContent::find($id);

        if (is_null($content)) {
            return Response::json(RestController::generateJsonResponse(true, 'Content not found.', $content), 404);
        }
        $deletedcontent = $content;
        // TODO: Handle Errors!
        $content->delete();
        return Response::json(RestController::generateJsonResponse(false, 'Content was deleted.', $deletedcontent));
    }

    /**
     * Force Delete Only for tests
     * URL: contents/forcedelete/{ID}
     * METHOD: DELETE
     */
    public function deleteForcedelete($id = null)
    {
        $content = NewsletterContent::withTrashed()->find($id);
        $content->delete();
    }

    /**
     * Copy Footers (external/internal) from the last newsletter
     * URL: nl_contents/copy-footer/{$lang}/{$newsletter_id}
     * METHOD: GET
     */

    public function getCopyFooter($lang = 0, $newsletter_id)
    {
        // Build query
        $contentsQuery = NewsletterContent::where('newsletter_id', DB::raw('(select max(`id`) as aggregate from `newsletter` where `id` <>'.$newsletter_id.')'));

        if ($lang <> 0) {
            $contentsQuery->where('lang', '=', $lang);
        }

        $contents = $contentsQuery->whereIn('position', ['footer_e', 'footer_i'])->get();
        if (count($contents) > 0) {
            $result = [];
            foreach ($contents as $fc) {
                $result[$fc->position] = $fc->content;
            }
            $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of footers found.', $result), 200);
        } else {
            $jsonResponse = Response::json(RestController::generateJsonResponse(true, "Footers didn't found!"), 400);
        }

        return $jsonResponse;
    }
}

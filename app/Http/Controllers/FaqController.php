<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Models\Faq;
use App\Http\Middleware\BeAccessFaqs;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Description of FaqController
 *
 * @author g.boteva
 */
class FaqController extends RestController
{

    /**
     * Return Faq
     * URL: faq/faqbyid/{$id}
     * METHOD: GET
     */

    public function getFaqbyid($id = null)
    {
        //parameter id must be set
        if ($id <= 0 || $id == null) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter id is required!'), 400);
        }

        $faq_item = Faq::with('tags')->find($id);

        if (!$this->hasAccess($faq_item->brand_id)) {
            return Response::json(RestController::generateJsonResponse(true, "You don't have access to FAQ for this brand!"), 400);
        }

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'FAQ is found.', $faq_item), 200);

        return $jsonResponse;
    }

    /**
     * Return all Faqs by lang
     * URL: faq/allfaqs/{$lang}
     * METHOD: GET
     */

    public function getAllfaqs($lang = 0)
    {
        if (!$this->hasAccess(SMART)) {
            return Response::json(RestController::generateJsonResponse(true, "You don't have access to FAQs!"), 400);
        }

        if ($lang <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter lang is required!'), 400);
        }
        $query = Faq::ofLang($lang);
        if (Auth::user()->role == USER_ROLE_EDITOR_SMART) {
            $query = $query->where('brand_id', '=', SMART);
        }
        $faq_items = $query->get();
        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of FAQs found.', $faq_items), 200);

        return $jsonResponse;
    }

    /**
     * Create Faq
     * URL: faq/create
     * METHOD: POST
     */

    public function postCreate()
    {
        $req = Request::all();
        
        // file_put_contents('/home/www/logs/purify.log', 'Create faq before purify: '.var_export($req,true)."\n",FILE_APPEND);
        
        if (isset($req['question'])) {
            $req['question']=\Purifier::clean($req['question']);
        }
        
        if (isset($req['answer'])) {
            $req['answer']=\Purifier::clean($req['answer']);
        }
        
        // file_put_contents('/home/www/logs/purify.log', 'Create faq after purify: '.var_export($req,true)."\n",FILE_APPEND);
        

        if (!isset($req['brand_id']) || $req['brand_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter brand_id is required!'), 400);
        }
        if (!$this->hasAccess($req['brand_id'])) {
            return Response::json(RestController::generateJsonResponse(true, "You don't have access to create FAQs for this brand!"), 400);
        }
        if (!isset($req['category_id']) || $req['category_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter topic is required!'), 400);
        }

        if (!isset($req['lang']) || $req['lang'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter lang is required!'), 400);
        }

        if (!isset($req['question']) || strlen(trim($req['question'])) < 5) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter question is required!'), 400);
        }
        if (!isset($req['answer']) || strlen(trim($req['answer'])) < 5) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter answer is required!'), 400);
        }
        $arrTags = null;
        if (isset($req['keywords']) && count($req['keywords']) > 0) {
            $arrTags = $req['keywords'];
        }
        unset($req['keywords']);
        $faq = new Faq();
        $faq->fill($req);
        $time = $faq->freshTimestamp();
        $faq->created_at = $time;
        $faq->updated_at = $time;

        return $this->trySave($faq, false, $arrTags);
    }

    /**
     * Modify Faq
     * URL: faq/update
     * METHOD: PUT
     */

    public function putUpdate()
    {
        $req = Request::all();

        // file_put_contents('/home/www/logs/purify.log', 'Update faq before purify: '.var_export($req,true)."\n",FILE_APPEND);
        
        if (isset($req['question'])) {
            $req['question']=\Purifier::clean($req['question']);
        }
        
        if (isset($req['answer'])) {
            $req['answer']=\Purifier::clean($req['answer']);
        }
        
        // file_put_contents('/home/www/logs/purify.log', 'Update faq after purify: '.var_export($req,true)."\n",FILE_APPEND);
        
        //parameters termId and userId must be set
        if (!isset($req['id']) || $req['id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter id is required!'), 400);
        }
        if (!isset($req['brand_id']) || $req['brand_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter brand_id is required!'), 400);
        }
        if (!$this->hasAccess($req['brand_id'])) {
            return Response::json(RestController::generateJsonResponse(true, "You don't have access to edit FAQs for this brand!"), 400);
        }

        if (!isset($req['category_id']) || $req['category_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter topic is required!'), 400);
        }

        if (!isset($req['lang']) || $req['lang'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter lang is required!'), 400);
        }

        if (!isset($req['question']) || strlen(trim($req['question'])) < 5) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter question is required!'), 400);
        }
        if (!isset($req['answer']) || strlen(trim($req['answer'])) < 5) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter answer is required!'), 400);
        }

        $faq = Faq::find($req['id']);
        $arrTags = [];
        if (is_null($faq)) {
            return Response::json(RestController::generateJsonResponse(true, 'FAQ not found.'), 404);
        }

        if (!$this->hasAccess($faq->brand_id)) {
            return Response::json(RestController::generateJsonResponse(true, "You don't have access to edit FAQs for this brand!"), 400);
        }

        if (isset($req['keywords']) && count($req['keywords']) > 0) {
            $arrTags = $req['keywords'];
        }
        unset($req['keywords']);
        $faq->fill($req);

        return $this->trySave($faq, true, $arrTags);
    }


    /**
     * Remove Faq
     * URL: faq/delete/$id
     * METHOD: DELETE
     */

    public function deleteDelete($id = null)
    {
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $faq = Faq::find($id);
        if (is_null($faq)) {
            return Response::json(RestController::generateJsonResponse(true, 'FAQ not found.'), 404);
        }
        if (!$this->hasAccess($faq->brand_id)) {
            return Response::json(RestController::generateJsonResponse(true, "You don't have access to delete FAQs for this brand!"), 400);
        }
        $deletedfaq = $faq;
        $deletedfaq->delete();

        return Response::json(RestController::generateJsonResponse(false, 'FAQ was deleted!', $faq));
    }

    /**
     * Return save result
     */
    protected function trySave(Faq $faq, $isUpdate, $arrTags = null)
    {
        try {
            $faq->save();
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            throw new ModelValidationException($e->getErrors());
        }

        if (is_array($arrTags)) {
            TagController::updateFaqTags($faq->id, $arrTags);
        }
        if ($isUpdate) {
            $message = "FAQ was updated.";
        } else {
            $message = "FAQ was added.";
        }

        //Success !!!

        return Response::json(RestController::generateJsonResponse(false, $message, $faq));
    }

    protected function hasAccess(string $brand_id): bool
    {
        $role = Auth::user()->role;
        if (in_array($role, BeAccessFaqs::getRoles())) {
            // USER_ROLE_EDITOR_SMART Can access only SMART brand
            return !($brand_id != SMART && $role == USER_ROLE_EDITOR_SMART);
        }

        return false;
    }
}

<?php

namespace App\Http\Controllers;

use App\Helpers\ContentUpdater;
use App\Helpers\DispatcherHelper;
use App\Helpers\FileHelper;
use App\Helpers\HtmlHelper;
use App\Helpers\UserAccessHelper;
use App\Models\Group;
use App\Models\Newsletter;
use App\Models\Page;
use App\Models\User;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class BackendContentDispatcher extends Controller
{

    function closetag($html)
    {
        $html_new = $html;
        preg_match_all("#<([a-z]+)( .*)?(?!/)>#iU", $html, $result1);
        preg_match_all("#</([a-z]+)>#iU", $html, $result2);
        $results_start = $result1[1];
        $results_end = $result2[1];
        foreach ($results_start as $startag) {
            if (!in_array($startag, $results_end)) {
                $html_new = str_replace('<' . $startag . '>', '', $html_new);
            }
        }
        foreach ($results_end as $endtag) {
            if (!in_array($endtag, $results_start)) {
                $html_new = str_replace('</' . $endtag . '>', '', $html_new);
            }
        }
        return $html_new;
    }

    protected function getWorkflowAdminList(Page $page)
    {
        $role_array = [];
        // if edditing user is approver/administrator => include editor groups allowed to edit this page
        if (in_array(Auth::user()->role, config('auth.CMS_APPROVER'))) {
            $ascendants   = Page::ascendants($page->id, 0)->get();
            $configAccess = config('auth.ACCESS');

            foreach ($configAccess as $key => $ca) {
                $user_root_page_id = $ca['page'];
                if (!in_array($key, [USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR])) {
                    if ($user_root_page_id == $page->id) {
                        $role_array[] = $key;
                    } else {
                        $page_ids = array_map(function ($asc) {
                            return $asc['id'];
                        }, $ascendants->toArray());
                        if (in_array($user_root_page_id, $page_ids)) {
                            $role_array[] = $key;
                        }
                    }
                }
            }
        }

        //add approver group
        $role_array[]  = USER_ROLE_EDITOR;
        $admin_users = User::whereIn('role', $role_array)
                        ->orWhere('id', config('app.default_workflow_admin'))
                        ->orWhere('id', config('app.default_workflow_admin2'))
                        ->orderBy('role', 'DESC')->get();

        // Include user if it is Approver and not default workflow admin but not in the list
        if (in_array(Auth::user()->role, config('auth.CMS_APPROVER'))
                && Auth::user()->id != config('app.default_workflow_admin')
                && Auth::user()->id != config('app.default_workflow_admin2')
                && (!$admin_users->contains('id', Auth::user()->id))) {
            $admin_users[] = Auth::user();
        // Include user if it is Editor and already responsible but not in the list
        } else if (Auth::user()->role >= USER_ROLE_EDITOR_SMART && 
            Auth::user()->id == $page->workflow->user_responsible &&
            (!$admin_users->contains('id', Auth::user()->id)))  {
            $admin_users[] = Auth::user();
        }
        
        return $admin_users;
    }

    /*     * **********************************************************************
     * Generate a view from a page object
     * @param $page 	Page model object or null/false
     * @return View containing the current page contents or custom 404 page
     * ********************************************************************* */

    protected function getContents($pageid)
    {
        if (!$pageid) {
            return Response::view('backend.be_missing_page', ['ACTIVE'=> ''], 404);
        }
        $page = Page::find($pageid);
        if (is_null($page) || $page->deleted) {
            return Response::view('backend.be_missing_page', ['ACTIVE'=> ""], 404);
        }

        $ascendants = Page::ascendants($page->id, $this->lang)->get();
        if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id, $ascendants)) {
            $error_message = trans('ws_general_controller.webservice.unauthorized');
            return Response::view('backend.be_unauthorized_access', ['error' => $error_message], 401);
        }

        if ($page->workflow->editing_state == STATE_APPROVED) {
            return Response::view('backend.be_approved', ['ACTIVE'=> ''], 404);
        }
        $result = PageController::checkAvailability($pageid);

        if (!$result->status && !isset($result->error)) {
            return Response::view('errors.editing', ['PAGE' => (object) ['id' => 404], 'ACTIVE'=>''], 404);
        } else if (!$result->status && $result->error == "admin_only") {
            return Response::view('errors.editing', ['ACTIVE'=>'', 'error' => true, 'message'=> trans('ws_general_controller.page.state_error', ['name'=>$result->name])], 404);
        } else if (!$result->status && $result->error) {
            return Response::view('errors.editing', ['ACTIVE'=>'', 'error' => true, 'message'=> $result->error], 404);
        }

        $pageContents = $page->contents()->where('lang', '=', $this->lang)->get();

        $sectionContents = ['title' => '', 'banner' => '', 'main' => '', 'right' => '', 'farRight' => '', 'author' => '', 'mobile_banner'=>'','tablet_banner'=>'']; #setting the default contents
        $sectionIds = ['title' => '', 'banner' => '', 'main' => '', 'right' => '', 'farRight' => '', 'author' => '', 'mobile_banner'=> '','tablet_banner'=>'']; #setting the default contents

        $doc = new \DOMDocument();

        foreach ($pageContents as $row) {
            if ($row->section != 'title' && !empty($row->body)) {
                $tmp_body = mb_convert_encoding($row->body, 'HTML-ENTITIES', "UTF-8");
                if (@$doc->loadHTML($tmp_body) === true) {
                    //TODO: Implement protected images
                    HtmlHelper::processImages($doc, $this->lang, \PAGE_MODE_PRINT);
                    $row->body = $doc->saveHTML();
                }
            }
            $sectionContents[$row->section] = $row->body;
            $sectionIds[$row->section] = $row->id;
        }

        $ascendantPIdIndex = DispatcherHelper::getParentIndex($ascendants);
        $ascendantIdIndex = $this->getIdIndex($ascendants);

        $pathIDs = DispatcherHelper::getPathIDs($ascendants, $page->id);

        $sectionId = $page->parent_id == HOME ? $page->id : DispatcherHelper::getActiveL2($ascendants);
        $sectionPages = Page::descendants($sectionId, $this->lang, 0)->alive()->get();
        $sectionPIndex = DispatcherHelper::getParentIndex($sectionPages);
        if (isset($sectionPIndex[$sectionId])) {
            arsort($sectionPIndex[$sectionId]);
        }
        $allRelated = $page->getPageRelatedArray($this->lang, $sectionId);
        //print_r($allRelated);die;
        # generate [id=>title, ...] array of:
        $relatedPages = []; // related pages
        $position = [];
        $relatedDownloads = []; // related downloads
        $positionDownload = [];
        $relatedBP = [];// related BP pages
        $positionBP = [];
        foreach ($allRelated as $relative) {
            if (!$relative->body) {
                continue; #skip if there's no title
            }
            # separate related records to "pages" & "downloads" & 'best_practise'
            if ($relative->template == 'best_practice') {
                if (is_null($relative->position)) {
                    $relatedBP[] = $relative;
                } else {
                    $positionBP[] = $relative;
                }
            } else if ($relative->template == 'downloads') {
                if (is_null($relative->position)) {
                    $relatedDownloads[] = $relative;
                } else {
                    $positionDownload[] = $relative;
                }
            } else {
                if (is_null($relative->position)) {
                    $relatedPages[] = $relative;
                } else {
                    $position[] = $relative;
                }
            }
        }
        $allRelated = null;
        $relatedPages = ContentUpdater::sortRelated($position, $relatedPages);
        $relatedDownloads = ContentUpdater::sortRelated($positionDownload, $relatedDownloads);
        $download_files = $page->getDownloadFiles(false);
        $relatedBP = ContentUpdater::sortRelated($positionBP, $relatedBP);
        //get structure[id, title, position, template] from related pages pages
//        $allRelated = $page->getRelatedArray($this->lang);
//
//        # generate [id=>title, ...] array of:
//        $relatedPages = array(); // related pages
//        $relatedDownloads = array(); // related downloads
//        $smartDownloadPageId = config('app.smart_downloads_id');

//        foreach ($allRelated as $relative) {
//            $rp_ascendants = Page::ascendants($relative->id, $this->lang)->get();
//            foreach ($rp_ascendants as $pa) {
//                if ($pa->id == $smartDownloadPageId) {
//                    $relative->template = 'downloads';
//                    break;
//                }
//            }
//            # separate related records to "pages" & "downloads"
//            if ($relative->template != 'downloads') { //strpos(strtolower($rp->slug), 'smart_downloads') === FALSE
//                $relatedPages[$relative->id] = array('title' => $relative->title, 'position' => $relative->position);
//            } else {
//                $relatedDownloads[$relative->id] = array('title' => $relative->title, 'position' => $relative->position);
//            }
//        }

        $menuSettings = ['colLimit' => 14, 'lineSize3' => 30, 'lineSize4' => 40];

        if ($page->workflow->user_responsible != Auth::user()->id && ($page->workflow->editing_state != STATE_DRAFT || $page->workflow->user_responsible != 0)) {
            $change_workflow_responsible = true;
        } else {
            $change_workflow_responsible = false;
        }

        $workflowAdmins = $this->getWorkflowAdminList($page);

        return view('backend/content_basic', [
                    'CONTENTS'                      => $sectionContents,
                    'CONTENTIDS'                    => $sectionIds,
                    'ASCENDANTS'                    => $ascendants,
                    'ASCENDANT_PID_INDEX'           => $ascendantPIdIndex,
                    'ASCENDANT_ID_INDEX'            => $ascendantIdIndex,
                    'SECTION_PID_INDEX'             => $sectionPIndex,
                    'L2ID'                          => $sectionId,
                    'PATH_IDS'                      => $pathIDs,
                    'SETTINGS'                      => $menuSettings,
                    'PAGE'                          => $page,
                    'LEVEL'                         => ($page->parent_id != 0 && count($ascendantIdIndex) > 1) ? $ascendantIdIndex[1]->level : 0,
//                    'RELATED_PAGES' => $relatedPages,
//                    'RELATED_DOWNLOADS' => $relatedDownloads,
                    //'MESSAGES' => isset($errors) ? $errors->all() : null,
                    'ACTIVE'                        => 'content',
                    'GROUPS'                        => Group::all(),
                    'FILEMANAGER_MODE'              => 'downloads',
                    'LANG'                          => $this->lang,
                    'IS_PAGE_RESTRICTED'            => count($page->getAccess()) > 0 ? 1 : 0,
                    'TIME_LEFT'                     => $result->time_left ,
                    'CHANGE_WORKFLOW_RESPONSIBLE'   => $change_workflow_responsible,
                    'RELATED_PAGES'                 => $relatedPages,
                    'RELATED_DOWNLOADS'             => $relatedDownloads,
                    'RELATED_BP'                    => $relatedBP,
                    'WORKFLOW_ADMINS'               => $workflowAdmins,
                    'DOWNLOAD_FILES'                => $download_files
                        ]);
    }

    /*     * **********************************************************************
     * Generate an indexed array of Page object, using id as a key
     * @param $pages 	an array of objects to use as a source
     * @return array(id => $page,...)
     * ********************************************************************* */

    public function getIdIndex($pages)
    {
        $arrIndex = [];
        foreach ($pages as $page) {
            $arrIndex[$page->id] = $page;
        }
        return $arrIndex;
    }

    /*
	 * This is used for delivery images in the backend edit view
     * TODO: Refactore with Dispatcher::getFile()
	 */
    public function getFile($file)
    {
        try {
            if (strpos($file, '__cache/') !== false) {
                $filemanager_mode = FileController::MODE_IMAGES;
                $file_path_cached = substr($file, strlen($filemanager_mode) +1);
                $full_path = FileHelper::generateCacheImage($filemanager_mode, $file_path_cached);
            } else {
                $relative_dest_path = '/files/' . $file;
                $full_path = public_path() . $relative_dest_path;
            }
            header("Content-Type: application/zip");
            header("Content-Length: " . @filesize($full_path));
            header('Content-Disposition: attachment; filename="'. basename($full_path) .'"');
            readfile($full_path);
        } catch (FileNotFoundException $exception) {
            Log::info("The file doesn't exist " . $full_path . ' error: ' . self::getLastErrorMsg());
            if (config('app.debug')) {
                Log::debug($exception);
            }
        } catch (\Exception $exception) {
            Log::info("The file doesn't exist " . $file . ' error: ' . self::getLastErrorMsg());
            if (config('app.debug')) {
                Log::debug($exception);
            }
        }
    }

    private function getNewsletterWorkflowAdminList()
    {
        $role_array   = [];
        $role_array[] = USER_ROLE_NEWSLETTER_APPROVER;
        if (in_array(Auth::user()->role, config('auth.CMS_NL_APPROVER'))) {
            $role_array[] = USER_ROLE_NEWSLETTER_EDITOR;
        }
        $admin_users = User::whereIn('role', $role_array)
                        ->orWhere('id', config('app.default_workflow_admin'))
                        ->orWhere('id', config('app.default_workflow_admin2'))
                        ->orderBy('role', 'DESC')->get();

        if (in_array(Auth::user()->role, config('auth.CMS_NL_APPROVER'))
                && Auth::user()->id != config('app.default_workflow_admin')
                && Auth::user()->id != config('app.default_workflow_admin2')
                && (!$admin_users->contains('id', Auth::user()->id))) {
            $admin_users[] = Auth::user();
        }

        return $admin_users;
    }

    /*     * **********************************************************************
     * Generate a view from a newsletter object
     * @param $newsletterid 	newsletter id
     * @return View containing the current newsletter contents or custom 404 page
     * ********************************************************************* */

    protected function getNewsletterContents($newsletterid)
    {
        if (!$newsletterid) {
            return Response::view('errors.missing', ['PAGE' => (object) ['id' => 404]], 404);
        }

        $result = NewsletterApiController::checkAvailability($newsletterid);

        if (!$result->status && !isset($result->error)) {
            return Response::view('errors.editing', ['PAGE' => (object) ['id' => 404], 'ACTIVE'=>''], 404);
        } else if (!$result->status && $result->error == "admin_only") {
            return Response::view('errors.editing', ['ACTIVE'=>'', 'error' => true, 'message'=> trans('ws_general_controller.page.state_error', ['name'=>$result->name])], 404);
        } else if (!$result->status && $result->error) {
            return Response::view('errors.editing', ['ACTIVE'=>'', 'error' => true, 'message'=> $result->error], 404);
        }

        $nl = Newsletter::find($newsletterid);
        if (is_null($nl)) {
            return Response::view('backend.be_missing_page', ['ACTIVE' => ""], 404);
        }
        $lang = $this->lang;
        $nlContents = $nl->contents()
                            ->where(function ($query) use ($lang) {
                                return  $query->where('lang', '=', $lang)
                                ->orWhere(function ($query) {
                                    return $query->where('position', 'footer_e_bl')
                                    ->where('lang', 3);
                                });
                            })->get();
        $sectionContents = ['title' => '', 'header_d' => '', 'header_m' => '', 'content' => '', 'footer_e' => '','footer_e_bl'=>'' ,'footer_i' => '']; #setting the default contents
        $sectionIds = ['title' => '', 'header_d' => '', 'header_m' => '', 'content' => '', 'footer_e' => '','footer_e_bl'=>'' ,'footer_i' => '']; #setting the default contents

        $doc = new \DOMDocument();

        foreach ($nlContents as $row) {
            if ($row->position != 'title' && !empty($row->content)) {
                $tmp_body = mb_convert_encoding($row->body, 'HTML-ENTITIES', "UTF-8");
                if (@$doc->loadHTML($tmp_body) === true) {
                    $row->content = $doc->saveHTML();
                }
            }
            $sectionContents[$row->position] = $row->content;
            $sectionIds[$row->position] = $row->id;
        }


        $menuSettings = ['colLimit' => 14, 'lineSize3' => 30, 'lineSize4' => 40];

        if ($nl->user_responsible != Auth::user()->id && ($nl->editing_state != STATE_DRAFT || $nl->user_responsible != 0)) {
            $change_workflow_responsible = true;
        } else {
            $change_workflow_responsible = false;
        }

        $workflowAdmins = $this->getNewsletterWorkflowAdminList();

        return view('backend/newsletter_basic', [
                    'CONTENTS' => $sectionContents,
                    'CONTENTIDS' => $sectionIds,
                    'SETTINGS' => $menuSettings,
                    'NEWSLETTER' => $nl,
                    'ACTIVE' => 'newsletter',
                    'FILEMANAGER_MODE' => 'downloads',
                    'LANG' => $this->lang,
                    'TIME_LEFT' => PageController::calculateTimeLeftForEdit($nl) ,
                    'CHANGE_WORKFLOW_RESPONSIBLE' => $change_workflow_responsible,
                    'WORKFLOW_ADMINS' => $workflowAdmins
                        ]);
    }
}

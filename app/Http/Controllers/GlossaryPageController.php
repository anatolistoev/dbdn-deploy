<?php

namespace App\Http\Controllers;

use App\Models\Term;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class GlossaryPageController extends Controller
{

    /************************************************************************
	 *
	 *
	 ***********************************************************************/
    /**
     * Get all first letters of terms and number of their occurences
     *
     */
    protected function getLetters()
    {

    /*
		SELECT CONVERT(LEFT(`name`, 1) USING latin1) as letter, COUNT(`name`) as occurences FROM `term`
		WHERE lang=2
		GROUP BY letter COLLATE latin1_german2_ci
	 */
        /*return DB::table('term')
                     ->select(DB::raw('LEFT( name, 1 ) AS letter, COUNT(*) as occurences'))
                     ->where('lang', '=', $this->lang)
                     ->groupBy('letter')
                     ->orderBy('letter')
                     ->get(); */
        return DB::table('term')
                     ->select(DB::raw('CONVERT(LEFT(`name`, 1) USING latin1) AS letter, COUNT(*) as occurences'))
                     ->where('lang', '=', $this->lang)
                     ->whereNull('deleted_at')
                     ->groupBy(DB::raw('letter COLLATE latin1_german2_ci'))
                     //->orderBy('letter')
                     ->get();
    }
    /**
     * Get all terms starting with the given letter
     *
     */
    protected function getTerms($letter)
    {

        return DB::table('term')
                ->select('id', 'name', 'description')
                ->where('lang', '=', $this->lang)
                ->whereNull('deleted_at')
                //->where('name', 'LIKE', $letter.'%')
                ->whereRaw('`name` LIKE CONVERT(? USING utf8) COLLATE utf8_bin', [$letter.'%'])
                ->orderBy('name')
                ->get();
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($idOrLetter = '', $template = 'glossary')
    {
        $term = null;
        $terms = null;

        if ($idOrLetter !== false && is_numeric($idOrLetter) && $idOrLetter >= 10) {
            $term = Term::find($idOrLetter);
            //$terms = isset($term->name) && strlen($term->name) ? $this->getTerms($term->name[0]) : $this->getTerms('A');
            $terms = isset($term->name) && strlen($term->name) ? $this->getTerms(mb_substr($term->name, 0, 1)) : [];
        } elseif ($idOrLetter!== false && $idOrLetter != '') {
            $terms =  $this->getTerms($idOrLetter);
        }

        if (Session::get('lang') == 1) {
            $altLang = 2;
        } else {
            $altLang = 1;
        }

        return view($template, [
                                            'PAGE'      => (object) ['id'  => 'glossary', 'slug' => 'glossary'.($idOrLetter !== false && $idOrLetter !== '' ? '/'.$idOrLetter : ''), 'template' => $template, 'langs' => 3],
                                            'ASCENDANTS' => [],
                                            'LETTERS'   => $this->getLetters(),
                                            'TERMS'     => $terms,
                                            'TERM'      => $term,
                                            'ALT_LANG'  => $altLang,
                                           ]);
    }
    /************************************************************************
	 *
	 *
	 ***********************************************************************/
    public function forPrint($idOrLetter = 'A')
    {
        //return $this->index($idOrLetter, 'glossary_print');
        return $this->index($idOrLetter, 'print');
    }
    /************************************************************************
	 *
	 *
	 ***********************************************************************/
    static function embedGlossary($html, &$terms)
    {
        try {
            if (strlen($html)<3) {
                $terms = new \Illuminate\Database\Eloquent\Collection;
                return $html;
            }

            libxml_use_internal_errors(true);
            $dom = new \DOMDocument('1.0', 'UTF-8');
            $dom->substituteEntities = false;
            $dom->recover = true;
            $dom->strictErrorChecking = false;
            $dom->formatOutput = false;

            //$dom->loadHTML(utf8_decode($html)); #NB: utf8_decode is required otherwise the encoding of the special symbols will be broken
            $dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
            $count = $terms->count();
            foreach ($terms as $key => $term) {
                if (0 === GlossaryPageController::replaceWithNodes($dom->documentElement, $term)) {
                    //echo 'Trying to forget '.$term->name.'<br>';
                    $terms->forget($key);
                }
            }

            # if nothing was found forget all terms
            if ($count == $terms->count()) {
                $terms = new \Illuminate\Database\Eloquent\Collection;
                return $html;
            }
            $result = $dom->saveHTML($dom->documentElement->firstChild);
            if (strpos($result, '<body>') !== false) {
                return substr($result, 6, -7);
            } else {
                return $result;
            }
        } catch (\Exception $e) {
            //echo '<!-- ex: '.$e->getLine().'|'.$e->getMessage()." -->\n";
            $terms = new \Illuminate\Database\Eloquent\Collection;
            return $html;
        }
    }
    /************************************************************************
	 *
	 *
	 ***********************************************************************/
    static function replaceWithNodes(&$oldNode, $term)
    {
        try {
            $dom = $oldNode->ownerDocument;
            $count = 0;
            $pos = 0;
            $excludeParents = ['a', 'b', 'strong', 'h1', 'h2', 'small'];
            $excludeClasses = ['titleOfText', 'textAfterTitle', 'gallery961'];
            $pos = $matches = null;
            $termName = null;
            if (!empty($oldNode->childNodes) && $oldNode->childNodes != null) {
                mb_internal_encoding('utf-8');
                mb_regex_encoding('UTF-8');
                for ($n = 0; $n < $oldNode->childNodes->length; ++$n) {
                    $node = $oldNode->childNodes->item($n);
                    if (in_array($node->parentNode->nodeName, $excludeParents) ||
                        (
                            $node->parentNode->nodeName == 'span' &&
                            in_array($node->parentNode->getAttribute('class'), $excludeClasses)
                        )
                    ) {
                        return 0;
                    }
                    if ($node instanceof \DOMText) {
                        try {
                            $termName = html_entity_decode($term->name, ENT_COMPAT, 'UTF-8');
                            $pattern = '/\b('.preg_quote($termName, '/').')\b/ui';
                            if (preg_match($pattern, $node->nodeValue, $matches, PREG_OFFSET_CAPTURE) === 1 && isset($matches[0][1])) {
                                $pos = mb_strlen(substr($node->nodeValue, 0, $matches[0][1]));

                                $firstPart = mb_substr($node->nodeValue, 0, $pos);
                                $firstNode = $dom->createTextNode($firstPart);

                                $midNode = $dom->createElement('a', htmlentities($matches[0][0], ENT_QUOTES, 'UTF-8'));
                                $midNode->appendChild($dom->createAttribute('href'))->value  = URL::to('glossary/'.$term->id);
                                $midNode->appendChild($dom->createAttribute('class'))->value = 'tooltip';
                                $midNode->appendChild($dom->createAttribute('glossary_id'))->value = $term->id;

                                $span = $midNode->appendChild($dom->createElement('span'));
                                $span = $span->appendChild($dom->createElement('span'));
                                $span->appendChild($dom->createElement('b', htmlentities($termName, ENT_QUOTES, 'UTF-8')));
                                $span->appendChild($dom->createElement('br'));

                                if (mb_strlen($term->description) > 100) {
                                    $descr = ( preg_match('/^.{1,100}\b/su', html_entity_decode($term->description, null, 'UTF-8'), $match) ?
                                            $match[0].'...'
                                            :
                                            mb_substr(html_entity_decode($term->description, null, 'UTF-8'), 0, 100, 'UTF-8').'...');
                                } else {
                                    $descr = html_entity_decode($term->description, null, 'UTF-8');
                                }

                                $span->appendChild($dom->createTextNode($descr));


                                $lastPart = mb_substr($node->nodeValue, $pos + mb_strlen($termName));

                                $node->nodeValue = $lastPart;
                                $parent = $node->parentNode;
                                $parent->insertBefore($firstNode, $node);
                                $parent->insertBefore($midNode, $node);
                                return 1;
                            }
                        } catch (\Exception $innerE) {
                            //echo '<!-- inner ex: '.$innerE->getLine().'|'.$innerE->getMessage()." -->\n";
                        }
                    } else {
                        $count = GlossaryPageController::replaceWithNodes($node, $term);
                    }
                    if ($count > 0) {
                        return $count;
                    }
                }
            }
            return 0;
        } catch (\Exception $e) {
            //echo '<!-- ex: '.$e->getLine().'|'.$e->getMessage()." -->\n";
            return 0;
        }
    }
}

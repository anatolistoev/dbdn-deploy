<?php

namespace App\Http\Controllers;

use App\Helpers\CaptchaHelper;
use App\Helpers\UserAccessHelper;
use App\Models\Faq;
use App\Models\Faqrate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class FaqPageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($brand = '', $faq_id = 0)
    {
        // Reset capcha to prevent reuse one captcha.
        CaptchaHelper::resetCaptcha();

        $req = Request::all();

        if ($brand == 'smart') {
            $brand = 'smart';
            $brand_id = SMART;
        } else {
            $brand = 'daimler';
            $brand_id = DAIMLER;
        }

        $open_group = null;

        if ($faq_id > 0) {
            $faq = Faq::find($faq_id);
            DB::update('UPDATE faq SET views = views + 1 WHERE id = ? LIMIT 1', [$faq->id]);
            $open_group = UserAccessHelper::getFaqsCategoryById($faq->category_id);
        }

        $q = '';
        if (isset($req['action']) && $req['action'] = 'search') {
            Session::forget('faq_filter');
            $q = htmlentities($req['search'], ENT_QUOTES, "UTF-8");
        }

        $faqs = $this->generateFaqBlocks($brand_id, $req);

        $nav_tags = $this->generateNavigationTags($faqs);
    //	$queries = DB::getQueryLog();
    //	var_dump($queries);die;
    //    $last_query = end($queries);
    //    print_r($last_query);die;
        if (isset($req['action']) && $req['action'] = 'search') {
            $dw_filters = [];
            $filtered_blocks = true;
        } elseif ($this->isFaqFilterSet()) {
            $dw_filters = Session::get('faq_filter');
            $filtered_blocks = true;
        } else {
            $dw_filters = [];
            $filtered_blocks = false;
        }

        if (Session::get('lang') == 1) {
            $altLang = 2;
        } else {
            $altLang = 1;
        }

        return view('faq', [
            'PAGE'      => (object) ['id'  => 'faq', 'slug' => 'faq/'.$brand, 'template' => 'faq', 'brand' => $brand_id, 'langs' => 3],
            'FAQS'      => $faqs,
            'FAQ_ID'    => $faq_id,
            'OPEN_GROUP' => $open_group,
            'NAV_TAGS'  => $nav_tags,
            'FILTERED_BLOCKS' => $filtered_blocks,
            'DW_FILTERS'    => $dw_filters,
            'ALT_LANG'  => $altLang,
            'Q'         => $q ]);
    }

    public function isFaqFilterSet()
    {

        if (Session::has('faq_filter')
                && is_array(Session::get('faq_filter'))
                && count(Session::get('faq_filter')) > 0) {
            return true;
        } else {
            return false;
        }
    }

    protected function generateFaqBlocks($brand_id, $req = null)
    {
        if ($this->isFaqFilterSet()) {
            $filters = Session::get('faq_filter');
        } else {
            $filters = [];
        }
        $query = Faq::select('faq.*', DB::raw('(SELECT count(*) FROM faqrate WHERE faq_id = faq.id) as rate'))
                ->ofLang($this->lang)
                ->where('faq.brand_id', '=', $brand_id);

        if (isset($req['search']) && $req['search'] != '') {
            $searchTerms = explode(' ', $req['search'], 5);
            foreach ($searchTerms as $term) {
                $term_regex = '[[:<:]]'.$term.'[[:>:]]';
                $query->where(function ($query) use ($term_regex) {
                    $query->whereRaw("faq.question REGEXP ?", [$term_regex]);
                    $query->orWhereRaw("faq.answer REGEXP ?", [$term_regex]);
                });
            }
        }
        if (isset($filters['tags']) && is_array($filters['tags']) && count($filters['tags']) > 0) {
            $query->join('faqtag as ft', 'ft.faq_id', '=', 'faq.id')
                    ->join('tag as t', 't.id', '=', 'ft.tag_id')
                    ->where('t.lang', '=', $this->lang)
                    ->whereIn('t.name', $filters['tags']);
        }
        if (isset($filters['time_from']) && $filters['time_from'] != '') {
            $query = $query->where('faq.updated_at', '>=', date('Y-m-d H:i:s', strtotime($filters['time_from'])));
        }
        if (isset($filters['time_to']) && $filters['time_to'] != '') {
            $query = $query->where('faq.updated_at', '<=', date('Y-m-d H:i:s', strtotime($filters['time_to'])));
        }
        if (isset($filters['title']) && $filters['title'] != '') {
            $query = $query->orderBy('question', $filters['title']);
        } elseif (isset($filters['time']) && $filters['time'] != '') {
            $query = $query->orderBy('updated_at', $filters['time']);
        } elseif (isset($filters['viewed']) && $filters['viewed'] != '') {
            $query = $query->orderBy('views', $filters['viewed']);
        } elseif (isset($filters['rating']) && $filters['rating'] != '') {
            $query = $query->orderBy('rate', $filters['rating']);
        } else {
            $query = $query->orderBy('updated_at', 'DESC');
        }
        $faqs = $query->get();

        // Load related pages
        $faq_ids = [];
        $faqmap = [];
        foreach ($faqs as $faq) {
            $faq_ids[] = $faq->id;
            $faqmap[$faq->id] = $faq;
            $faq->related_pages = [];
        }

        $queryRelatedPages = Faq::getPageRelatedQuery($faq_ids, $this->lang, $brand_id);

        $relatedPages = $queryRelatedPages->get();

        foreach ($relatedPages as $faqRelPage) {
            if (isset($faqmap[$faqRelPage->faq_id])) {
                $faq = $faqmap[$faqRelPage->faq_id];
                $rel_pages = $faq['related_pages'];
                array_push($rel_pages, $faqRelPage);
                $faq['related_pages'] = $rel_pages;
            } else {
                Log::warning('Missing FAQ with faq_id: '. $faqRelPage->faq_id .' for relations: ');
            }
        }

        $faqs_by_category = $this->groupByCategory($faqs);

        return $faqs_by_category;
    }

    public function generateFaqFilter()
    {
        $req = Request::except('_token');

        $search = [];
        $search['search'] = $req['search'];
        unset($req['search']);

        if (isset($req['tags'])) {
            $req['tags'] = explode(',', $req['tags']);
        }
        $brand_id = $req['brand_id'];
        unset($req['brand_id']);

        Session::put('faq_filter', $req);

        if ($this->isFaqFilterSet()) {
            $filtered_blocks = true;
        } else {
            $filtered_blocks = false;
        }

        $faqs = $this->generateFaqBlocks($brand_id, $search);

        return view('partials.faq_list', ['FAQS' => $faqs,'FILTERED_BLOCKS' => $filtered_blocks, 'OPEN_GROUP' => '', 'FAQ_ID' => 0]);
    }

    //TODO: Optimize loading of tags
    private function generateNavigationTags($faqs)
    {
        $nav_tags = [];
        foreach ($faqs as $faq_group) {
            foreach ($faq_group as $faq) {
                foreach ($faq->tags()->get() as $tag) {
                    $nav_tags[] = $tag->name;
                }
            }
        }

        $nav_tags = array_unique($nav_tags);
        usort($nav_tags, function ($a, $b) {
            return strtolower($a) > strtolower($b);
        });
        //asort($nav_tags,SORT_STRING |  SORT_NATURAL);

        return $nav_tags;
    }

    private function groupByCategory($faqs)
    {
        $grouped_faqs = [];
        foreach ($faqs as $faq) {
            if ($this->isFaqFilterSet()) {
                $grouped_faqs[0][] = $faq;
            } else {
                $key = UserAccessHelper::getFaqsCategoryById($faq->category_id);
                $grouped_faqs[$key][] = $faq;
                ksort($grouped_faqs);
            }
        }
        return $grouped_faqs;
    }

    public function updateViews($faq_id = null)
    {
        if (is_null($faq_id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter id is required!'), 400);
        }
        DB::update('UPDATE faq SET views = views + 1 WHERE id = ? LIMIT 1', [$faq_id]);
        return Response::json(RestController::generateJsonResponse(true, 'success'));
    }

    public function updateLikes($faq_id = null)
    {
        try {
            if (is_numeric($faq_id)) {
                $exists = Faqrate::where('faq_id', '=', $faq_id)->where('user_id', '=', Auth::user()->id)->count();
                if ($exists > 0) {
                    return Response::json(RestController::generateJsonResponse(true, trans('system.faq_rating.rated')), 400);
                } else {
                    $rating = new Faqrate();
                    $rating->user_id = Auth::user()->id;
                    $rating->faq_id = $faq_id;
                    $rating->save();

                    $faq_rates = Faqrate::where('faq_id', '=', $faq_id)->count();
                    return Response::json(RestController::generateJsonResponse(true, trans('system.faq_rating.added'), ['faq_rates' => $faq_rates]));
                }
            }
            return Response::json(RestController::generateJsonResponse(true, trans('system.faq_rating.failed')), 400);
        } catch (\Exception $e) {
            Log::error($e);
            return Response::json(RestController::generateJsonResponse(true, trans('system.faq_rating.failed')), 400);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Models\NewsletterSubscriber;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Bussines logic for Newsletter Lists
 *
 * @author m.stefanov
 */
class NewsletterListController extends RestController
{
    const ID_PRODUCTIVE_LIST     = 1;
    const ID_TEST_LIST           = 2;
    const NAME_PRODUCTIVE_LIST   = 'productive_list';
    const NAME_TEST_LIST         = 'test_list';

    public static $NewsletterIDs = [self::ID_PRODUCTIVE_LIST, self::ID_TEST_LIST];

    /**
     * Return All subscribers for list
     * URL: newsletter_list/all/{newsletter_list}
     * METHOD: GET
     */
    public function getAll($newsletter_list)
    {
        // Validation
        if (empty($newsletter_list) || !in_array($newsletter_list, [self::NAME_PRODUCTIVE_LIST, self::NAME_TEST_LIST])) {
            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_general_controller.invalid.parameter', ["attribute" => "newsletter_list"])
            ), 400);
        }

        $sort_fields = ['id', 'username', 'first_name', 'last_name', 'email', 'company', 'position', 'department', 'address',
            'code', 'city', 'country', 'phone', 'fax', 'updated_at', 'created_at'];
        $req = Request::all();

        if (empty($req['filter'])) {
            $filter = null;
        } else {
            $filter = $req['filter'];
        }

        if (!empty($req['sort-by']) && in_array($req['sort-by'], $sort_fields)) {
            $field = $req['sort-by'];
        } else {
            $field = null;
        }
        if (!empty($req['sort-direction']) && strtoupper($req['sort-direction']) == "DESC") {
            $order = "DESC";
        } else {
            $order = "ASC";
        }
        if ($newsletter_list == self::NAME_TEST_LIST) {
            $usersQuery = User::join('newsletter_subscriber', 'newsletter_subscriber.user_id', '=', 'user.id')
                            ->where('newsletter_subscriber.list_id', self::ID_TEST_LIST)
                            ->whereNull('newsletter_subscriber.' . User::DELETED_AT);
        } else {
            $usersQuery = User::where('newsletter', 1)->where('newsletter_include', 1);
        }
        if (!is_null($filter) && is_null($field)) {
            $usersQuery = $usersQuery->where('username', 'like', "%" . $filter . "%")->orderBy('username', $order);
        } elseif (!is_null($filter) && !is_null($field)) {
            $usersQuery = $usersQuery->where('username', 'like', "%" . $filter . "%")->orderBy($field, $order);
        }
        // Id	Username	Name	Role	Newsletter Newsletter_include	Active	Last Login	Logins Countt
        $columns = ['user.id', 'username', 'first_name', 'last_name', 'role',
            'newsletter', 'active', 'last_login', 'logins'];
//		$columns = array('*');

        $users = $usersQuery->get($columns);

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of Users found', $users), 200);

        return $jsonResponse;
    }

    /**
     * Add subscriber to newsletter list
     * URL: newsletter_list/add-subscriber/{list_name}
     * METHOD: POST
     */
    public function postAddSubscriber($list_name)
    {
        $subscriptionData = Request::json()->all();
        //Validate
        if (empty($list_name) || !in_array($list_name, [self::NAME_PRODUCTIVE_LIST, self::NAME_TEST_LIST])) {
            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_general_controller.invalid.parameter', ["attribute" => "newsletter_list"])
            ), 400);
        }

        if (empty($subscriptionData['username'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter "username or id" is required!'), 400);
        }
        // Id	Username	Name	Role	Newsletter Newsletter_include	Active	Last Login	Logins Countt
        $columns = ['id', 'username', 'first_name', 'last_name', 'role',
            'newsletter', 'newsletter_include', 'active', 'last_login', 'logins'];
//		$columns = array('*');
        $usernameOrId = $subscriptionData['username'];
        //Check for ID
        $isNumber = filter_var($usernameOrId, FILTER_VALIDATE_INT);
        if ($isNumber !== false) {
            $user = User::find($usernameOrId, $columns);
            if (is_null($user)) {
                return Response::json(RestController::generateJsonResponse(true, 'User not found. ID:' . $usernameOrId), 404);
            }
        } else {
            // Get first user
            $user = User::where('username', 'like', $usernameOrId)->get($columns)->first();
            if (is_null($user)) {
                return Response::json(RestController::generateJsonResponse(true, 'User not found. Username:' . $usernameOrId), 404);
            }
        }

        $newsletter_list_id = static::_resolveNewsletterID($list_name);

        $querySubscriber = NewsletterSubscriber::withTrashed()->
                where('user_id', $user->id)->
                where('list_id', $newsletter_list_id);
        $subscriber = $querySubscriber->first();
        if (empty($subscriber)) {
            $subscriber = new NewsletterSubscriber();
            $subscriber->list_id = $newsletter_list_id;
            $subscriber->user_id = $user->id;
            $subscriber->created_by = Auth::user()->id;
        } else {
            if ($subscriber->trashed()) {
                $querySubscriber->restore();
                return Response::json(RestController::generateJsonResponse(false, 'User is subscribed.', $user));
            } else {
                $errMessage = trans(
                    'backend_newsletter.add_subscriber.duplication',
                    ["attribute" => "username"]
                );
                $errorArray = RestController::generateJsonResponse(true, $errMessage);
                return Response::json($errorArray, 500);
            }
        }
        try {
            DB::beginTransaction();

            $subscriber->save();
            //Success !!!
            DB::commit();

            $result = Response::json(RestController::generateJsonResponse(false, 'User is subscribed.', $user));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            DB::rollback();

            throw new ModelValidationException($e->getErrors());
        } catch (\Exception $e) {
            // Rollback and then return errors
            DB::rollback();

            $message = $e->getMessage();
            if (0 === strpos($message, 'SQLSTATE[23000]')) {
                $errMessage = trans(
                    'backend_newsletter.add_subscriber.duplication',
                    ["attribute" => "username"]
                );
                $errorArray = RestController::generateJsonResponse(true, $errMessage);
            } else {
                $errorArray = RestController::generateJsonResponse(true, $message);
            }
            if (config('app.debug')) {
                // Add stack trace
                $errorArray['debug'] = ['exception_message' => $message, 'stacktrace' => $e->getTraceAsString()];
            }

            $result = Response::json($errorArray, 500);
        }

        return $result;
    }

    /**
     * Remove subscriber from newsletter list
     * URL: newsletter_list/remove-subscriber/{list_name}/{user_id}
     * METHOD: DELETE
     */
    public function deleteRemoveSubscriber($list_name, $user_id = null)
    {
        // Validation
        if (empty($list_name) || !in_array($list_name, [self::NAME_PRODUCTIVE_LIST, self::NAME_TEST_LIST])) {
            return Response::json(RestController::generateJsonResponse(
                true,
                trans('ws_general_controller.invalid.parameter', ["attribute" => "newsletter_list"])
            ), 400);
        }

        if (empty($user_id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.require.id')), 400);
        }

        $newsletter_list_id = self::_resolveNewsletterID($list_name);

        try {
            // Start transaction!
            DB::beginTransaction();
            $querySubscriber = NewsletterSubscriber::where('user_id', $user_id)->
                    where('list_id', $newsletter_list_id);
            $subscriber = $querySubscriber->first();

            if (is_null($subscriber)) {
                return Response::json(RestController::generateJsonResponse(true, 'Newsletter not found for ID:' . $id), 404);
            }

            $deleted_subscriber = $subscriber;
            $querySubscriber->delete();
            // Commit the queries!
            DB::commit();

            $result = Response::json(RestController::generateJsonResponse(false, 'Subscriber was unsubscribed.', $deleted_subscriber));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            // Rollback and then return errors
            DB::rollback();

            throw new ModelValidationException($e->getErrors());
        } catch (\Exception $e) {
            // Rollback and then return errors
            DB::rollback();

            $message = $e->getMessage();
            $errorArray = RestController::generateJsonResponse(true, $message);
            if (config('app.debug')) {
                // Add stack trace
                $errorArray['debug'] = ['exception_message' => $message, 'stacktrace' => $e->getTraceAsString()];
            }

            $result = Response::json($errorArray, 500);
        }

        return $result;
    }

    private static function _resolveNewsletterID($list_name)
    {
        $newsletter_list_id = self::ID_TEST_LIST;
        if ($list_name == self::NAME_TEST_LIST) {
            $newsletter_list_id = self::ID_TEST_LIST;
        } else if ($list_name == self::NAME_PRODUCTIVE_LIST) {
            $newsletter_list_id = self::ID_PRODUCTIVE_LIST;
        }
        return $newsletter_list_id;
    }
}

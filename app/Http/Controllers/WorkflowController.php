<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Http\Middleware\BeAccessApprover;
use App\Helpers\UserAccessHelper;
use App\Models\FileItem;
use App\Models\Note;
use App\Models\Page;
use App\Models\User;
use App\Models\Workflow;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Description of WorkflowController
 *
 * @author i.traykov
 */
class WorkflowController extends RestController
{

    /**
     * Return Workflow
     * URL: workflow/read/{$id}
     * METHOD: GET
     */
    public function getRead($id = 0)
    {
        //parameter id must be set
        if ($id <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter id is required!'), 400);
        }

        $wf = Workflow::find($id);

        if (is_null($wf)) {
            return Response::json(RestController::generateJsonResponse(true, 'Workflow not found'), 404);
        } else {
            return Response::json(RestController::generateJsonResponse(false, 'Workflow found.', $wf));
        }
    }

    /**
     * Create Page Workflow Object
     * URL: workflow/create
     * METHOD: GET
     */
    public function postCreate()
    {

        $req = Request::all();

        $wf = new Workflow();
        $wf->fill($req);

        return $this->trySave($wf, false);
    }

    /**
     * Return save result
     */
    protected function trySave(Workflow $wf, $isUpdate, $note = null)
    {
        try {
            if ($isUpdate) {
                $message = "Workflow was updated.";
            } else {
                $message = "Workflow was added.";
            }

            $wf->save();

            if ($wf->editing_state != STATE_APPROVED && $wf->page->active == 1) {
                $wf->page->active = 0;
                $wf->page->save();
            }

            if (!is_null($note)) {
                $note->save();
            }

            if ($wf->editing_state == STATE_APPROVED && strtotime($wf->page->publish_date) > 0 && strtotime($wf->page->publish_date) < strtotime(date('Y-m-d H:i:s', strtotime('+1 hour')))) {
                $message .= " The page cannot be activated automatically, please activate it manualy.";
            }

            $result = Response::json(RestController::generateJsonResponse(false, $message, $wf));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            throw new ModelValidationException($e->getErrors());
        }
        return $result;
    }

    /**
     * Return Update Workflow from JSON
     * URL: workflow/update
     * METHOD: PUT
     * 
     * Workflow transitions
     * [   
     *     \STATE_DRAFT => [
     *         ['role' => 'APPROVER', 'new_state' => [\STATE_DRAFT, \STATE_REVIEW, \STATE_APPROVED]], 
     *         ['role' => 'EDITOR', 'new_state' => [\STATE_DRAFT, \STATE_REVIEW]], 
     *     ],
     *     \STATE_REVIEW => [
     *         ['role' => 'APPROVER', 'new_state' => [\STATE_DRAFT, \STATE_REVIEW, \STATE_APPROVED]], 
     *         ['role' => 'EDITOR', 'new_state' => []], 
     *     ],
     *     \STATE_APPROVED => [
     *         ['role' => 'APPROVER', 'new_state' => [\STATE_DRAFT, \STATE_REVIEW, \STATE_APPROVED]], 
     *         ['role' => 'EDITOR', 'new_state' => []], 
     *     ]
     * ];
     *
     */
    public function putUpdate()
    {
        $update_wf = Request::json()->all();
        $userResponsible = null;
        
        if (isset($update_wf['note'])) {
            $update_wf['note']=\Purifier::clean($update_wf['note']);
        }
        
        //Validate
        if (empty($update_wf['id'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $wf = Workflow::find($update_wf['id']);
        if (is_null($wf)) {
            return Response::json(RestController::generateJsonResponse(true, 'Workflow not found.', $update_wf), 404);
        }
        $page = $wf->page;
        // Check access of user
        $ascendants = Page::ascendants($page->id, 0)->get();
        if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id, $ascendants)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
        }

        // Only Approver could change workflow that is not Draft!
        $approverRoles = BeAccessApprover::getRoles();
        if ( $wf->editing_state != \STATE_DRAFT 
             && !in_array(Auth::user()->role, $approverRoles) ) 
        {
                return Response::json(RestController::generateJsonResponse(
                    true,
                    trans('backend_pages.workflow.editor.change_state')
                ), 403);
        }
        $workflowState = $wf->editing_state;

        // Update edit state
        if (isset($update_wf['editing_state']) && $update_wf['editing_state'] != $wf->editing_state) {
            // Editor can change from Draft to Review only!
            if (!in_array(Auth::user()->role, $approverRoles)) {
                if ($update_wf['editing_state'] != \STATE_REVIEW) {
                    return Response::json(RestController::generateJsonResponse(
                        true,
                        trans('backend_pages.workflow.editor.change_state')
                    ), 403);
                }
            }

            // For Review and Approved responsible must be Approver!
            if ($update_wf['editing_state'] != STATE_DRAFT) {
                $userResponsible = User::find($update_wf['user_responsible']);
                if (is_null($userResponsible) || !in_array($userResponsible->role, $approverRoles)) {
                    return Response::json(RestController::generateJsonResponse(
                        true,
                        trans('backend_pages.workflow.attached.approver')
                    ), 403);
                }
            }
            // If set state Approved then check for missing images
            if ($update_wf['editing_state'] == STATE_APPROVED) {
                $message = $this->checkPageForMissingImages($wf->page_id);
                if (!empty($message)) {
                    return Response::json(RestController::generateJsonResponse(true, $message), 404);
                }
            }

            $workflowState = $update_wf['editing_state'];
        }

        // Set Responsible
        if (isset($update_wf['user_responsible']) && $update_wf['user_responsible'] != $wf->user_responsible) {
            if (is_null($userResponsible)) {
                $userResponsible = User::find($update_wf['user_responsible']);
            }

            //Check if the responsible is valid
            if ($workflowState == \STATE_DRAFT) {
                $responsibleRoles = config('auth.CMS_EDITOR');
            } else {
                $responsibleRoles = $approverRoles;
            }
            if ( (is_null($userResponsible) && $workflowState !== \STATE_DRAFT) 
                || !is_null($userResponsible) && (
                     !in_array($userResponsible->role, $responsibleRoles) 
                   || !UserAccessHelper::hasAccessForPageAndAscendants($page->id, $ascendants, $userResponsible->role))
               ) {
                return Response::json(RestController::generateJsonResponse(true, trans('backend_pages.workflow.attached.approver')), 403);
            }

            // Set the user asigned the responsible
            $wf->assigned_by = Auth::user()->id;
        }

        $note = null;
        if (isset($update_wf['note']) && $update_wf['note'] != "") {
            $note          = new Note();
            $note->text    = $update_wf['note'];
            $note->page_id = $wf->page_id;
            $note->user_id = Auth::user()->id;
            $note->date    = date('Y-m_d H:i:s');
        }
        unset($update_wf['note']);

        $wf->fill($update_wf);
        return $this->trySave($wf, true, $note);
    }

    public function putChangeresponsible()
    {
        $update_wf = Request::json()->all();

        if (empty($update_wf['page_id'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter Page ID is required!'), 400);
        }

        $wf = Workflow::where('page_id', '=', $update_wf['page_id'])->first();
        if (is_null($wf)) {
            return Response::json(RestController::generateJsonResponse(true, 'Workflow not found.', $update_wf), 404);
        }
        $page = $wf->page;
        // Get the page!
        $ascendants = Page::ascendants($page->id, 0)->get();
        if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id, $ascendants)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 403);
        }

        $userResponsible = User::find($update_wf['user_responsible']);
        if ($wf->editing_state != STATE_DRAFT) {
            $approverRoles = BeAccessApprover::getRoles();
            if (is_null($userResponsible) || !in_array($userResponsible->role, $approverRoles)) {
                    return Response::json(RestController::generateJsonResponse(
                        true,
                        trans('backend_pages.workflow.attached.approver')
                    ), 403);
            }
        }

        //Check if the responsible is valid
        if ($wf->editing_state == \STATE_DRAFT) {
            $responsibleRoles = config('auth.CMS_EDITOR');
        } else {
            $responsibleRoles = $approverRoles;
        }

        if (is_null($userResponsible) 
            || !in_array($userResponsible->role, $responsibleRoles) 
            || !UserAccessHelper::hasAccessForPageAndAscendants($page->id, $ascendants, $userResponsible->role)) {
            return Response::json(RestController::generateJsonResponse(true, trans('backend_pages.workflow.attached.approver')), 403);
        }
        
        $wf->user_responsible = $userResponsible->id;
        $wf->assigned_by      = Auth::user()->id;
        return $this->trySave($wf, true);
    }

    /**
     * Remove Workflow Object
     * URL: workflow/delete/{$id}
     * METHOD: DELETE
     */
    public function deleteDelete($id = 0)
    {

        $req = Request::all();

        if (is_null($id) || $id <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter id is required!'), 400);
        }

        $wf = Workflow::find($id);

        if (is_null($wf)) {
            return Response::json(RestController::generateJsonResponse(true, 'Workflow not found.', $wf), 404); // in response workflow not exists
        }

        $deleteditem = $wf;
        $wf->delete();

        return Response::json(RestController::generateJsonResponse(false, 'Workflow was deleted!', $deleteditem));
    }

    /**
	 * Force Delete all page workflows for tests
	 * URL: workflow/forcedelete/{ID}
	 * METHOD: DELETE
	 */
    public function deleteForcedelete($id = null) {
        if (!UserAccessHelper::hasAccessForPageAndAscendants($id)) {
            return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
        }
        $workflows = Workflow::withTrashed()->where('page_id', $id)->get();
        foreach ($workflows as $workflow) {
            $workflow->forceDelete();
        }
    }

    /** 
    * Check for missing images
    */
    protected function checkPageForMissingImages(int $page_uid): string {
        $images = FileItem::join('gallery', 'file.id', '=', 'file_id')
            ->where('page_id', '=', $page_uid)
            ->whereNotNull('gallery.deleted_at')
            ->get();
        $message = '';

        if ($images->count()) {
            //Check if files: main, thumb, zoom exists
            foreach ($images as $fileItem) {
                $fileMessage = [];
                if (!is_file(public_path() . '/' . $fileItem->getPath(FILEITEM_REAL_PATH))) {
                    $fileMessage[] = "Master: " . $fileItem->getPath(FILEITEM_REAL_PATH);
                }

                if ($fileItem->thumbnail != '' && !is_file(public_path() . '/' . $fileItem->getThumbPath(FILEITEM_REAL_PATH))) {
                    $fileMessage[] = "Thumb: " . $fileItem->getThumbPath(FILEITEM_REAL_PATH);
                }

                if ($fileItem->zoompic != '' && !is_file(public_path() . '/' . $fileItem->getZoomPath(FILEITEM_REAL_PATH))) {
                    $fileMessage[] = "Zoom: " . $fileItem->getZoomPath(FILEITEM_REAL_PATH);
                }
                if (!empty($fileMessage)) {
                    $missingTypes = trans(
                        'backend_pages.workflow.missing.image',
                        ['file_id' => $fileItem->id, 'missing_files' => join(', ', $fileMessage)]
                    );
                    $message      .= $missingTypes . '<br>';
                    Log::error($missingTypes);
                }
            }
        }
        return $message;
    }
}

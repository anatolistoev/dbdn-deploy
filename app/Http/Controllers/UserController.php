<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Models\User;
use App\Models\Usergroup;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Description of UserController
 *
 * @author i.traykov
 */
class UserController extends RestController
{

    /**
     * Return All Users
     * URL: user/all
     * METHOD: GET
     */
    public function getAll()
    {

        $sort_fields = ['id', 'username', 'first_name', 'last_name', 'email', 'company', 'position', 'department', 'address',
            'code', 'city', 'country', 'phone', 'fax', 'updated_at', 'created_at'];
        $req = Request::all();

        if (empty($req['filter'])) {
            $filter = null;
        } else {
            $filter = $req['filter'];
        }

        if (!empty($req['sort-by']) && in_array($req['sort-by'], $sort_fields)) {
            $field = $req['sort-by'];
        } else {
            $field = null;
        }
        if (!empty($req['sort-direction']) && strtoupper($req['sort-direction']) == "DESC") {
            $order = "DESC";
        } else {
            $order = "ASC";
        }

        if (is_null($filter) && is_null($field)) {
            $usersQuery = User::query();
        } else{
            if (is_null($field)) {
                $field = 'username';
            }
            $usersQuery = User::where('username', 'like', "%" . $filter . "%")->orderBy($field, $order);
        }
        // Id	Username	Name	Role	Newsletter Newsletter_include	Active	Last Login	Logins Countt
        $columns = ['id', 'username', 'first_name', 'last_name', 'role',
            'newsletter', 'newsletter_include', 'active', 'last_login', 'logins'];
//		$columns = array('*');

        $users = $usersQuery->get($columns);
        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of Users found', $users), 200);

        return $jsonResponse;
    }

    /**
     * Return User by Id
     * URL: user/read/{id}
     * METHOD: GET
     */
    public function getRead($id = null)
    {
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $user = User::find($id);
        $user->unlock_code = '';
        $user->newsletter_code = '';
        if (is_null($user)) {
            return Response::json(RestController::generateJsonResponse(true, 'User not found'), 404);
        } else {
            return Response::json(RestController::generateJsonResponse(false, 'User found', $user));
        }
    }
    /**
     * Return All Users
     * URL: user/newsletterusers
     * METHOD: GET
     */
    public function getNewsletterusers()
    {

        $sort_fields = ['id', 'username', 'first_name', 'last_name', 'email', 'company', 'position', 'department', 'address',
            'code', 'city', 'country', 'phone', 'fax', 'updated_at', 'created_at'];
        $req = Request::all();

        if (empty($req['filter'])) {
            $filter = null;
        } else {
            $filter = $req['filter'];
        }

        if (!empty($req['sort-by']) && in_array($req['sort-by'], $sort_fields)) {
            $field = $req['sort-by'];
        } else {
            $field = null;
        }
        if (!empty($req['sort-direction']) && strtoupper($req['sort-direction']) == "DESC") {
            $order = "DESC";
        } else {
            $order = "ASC";
        }
        $usersQuery = User::where('newsletter', 1)->where('newsletter_include', 1);
        if (!is_null($filter) && is_null($field)) {
            $usersQuery = $usersQuery->where('username', 'like', "%" . $filter . "%")->orderBy('username', $order);
        } elseif (!is_null($filter) && !is_null($field)) {
            $usersQuery = $usersQuery->where('username', 'like', "%" . $filter . "%")->orderBy($field, $order);
        }
        // Id	Username	Name	Role	Newsletter Newsletter_include	Active	Last Login	Logins Countt
        $columns = ['id', 'username', 'first_name', 'last_name', 'role',
            'newsletter', 'active', 'last_login', 'logins'];
//		$columns = array('*');

        $users = $usersQuery->get($columns);

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of Users found', $users), 200);

        return $jsonResponse;
    }
    /**
     * Create User
     * URL: user/create
     * METHOD: POST
     */
    public function postCreate()
    {

        $req = Request::all();

        $user = new User();
        $user->fill($req);
        $user->unlock_code = $this->generateRandomString();
	
        return $this->trySave($user, false);
    }

    /**
     * Generate random string with length of 15
     */
    public static function generateRandomString()
    {

        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $string = '';
        for ($i = 0; $i < 15; $i++) {
            $string .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $string;
    }

    /**
     * Return save result
     */
    protected function trySave($user, $isUpdate)
    {
        try {
            $groups = $user->group;
            $groups[] = 1; // add the default member group
            unset($user->group);

            if ($isUpdate) {
                $message = "User was updated.";
            } else {
                $message = "User was added.";
            }

            DB::beginTransaction();
		
            //$user->save();
	    if(!$user->save()) {
		$result = Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.validation.failed'), $user->validationErrors), 400);
		return $result;
	    }
	    
	    Usergroup::where('user_id', '=', $user->id)->forceDelete();

            foreach ($groups as $group_id) {
                $usergroup_item = new Usergroup();
                $usergroup_item->user_id = $user->id;
                $usergroup_item->group_id = $group_id;

                $usergroup_item->save();
                unset($usergroup_item->id);
                //$user->group[] = $usergroup;
            }
            if ($isUpdate) {
                DB::table('request_auth')
                        ->where('request_auth.user_id', $user->id)
                        ->whereNull('request_auth.deleted_at')
                        ->update(['active' =>
                            DB::raw('EXISTS (
                                        SELECT `pagegroup`.`page_id`
                                        FROM pagegroup
                                        JOIN `page` ON `page`.`id` = `pagegroup`.`page_id`
                                        WHERE `pagegroup`.`page_id` = `request_auth`.`page_id`
                                        AND `pagegroup`.`group_id` IN ('.implode(", ", $groups).')
                                        AND `pagegroup`.`deleted_at` IS NULL
                                        AND `page`.`deleted_at` IS NULL
                                        AND `page`.`active` = 1
                                    )OR NOT EXISTS(
                                        SELECT `page`.`id` FROM `page`
                                        JOIN `pagegroup` ON `page`.`id` = `pagegroup`.`page_id`
                                        WHERE `page`.`id` = `request_auth`.`page_id`
                                        AND `page`.`deleted_at` IS NULL
                                        AND `page`.`active` = 1
                                        AND `pagegroup`.`deleted_at` IS NULL
                                    )')]);
                DB::table('subscription')
                        ->where('subscription.user_id', $user->id)
                        ->whereNull('subscription.deleted_at')
                        ->update(['active' =>
                            DB::raw('EXISTS(
                                        SELECT `pagegroup`.`page_id`
                                        FROM pagegroup
                                        JOIN `page` ON `page`.`id` = `pagegroup`.`page_id`
                                        WHERE `pagegroup`.`page_id` = `subscription`.`page_id`
                                        AND `pagegroup`.`group_id` IN ('.implode(", ", $groups).')
                                        AND `pagegroup`.`deleted_at` IS NULL
                                        AND `page`.`deleted_at` IS NULL
                                        AND `page`.`active` = 1
                                    ) OR NOT EXISTS(
                                        SELECT `page`.`id` FROM `page`
                                        JOIN `pagegroup` ON `page`.`id` = `pagegroup`.`page_id`
                                        WHERE `page`.`id` = `subscription`.`page_id`
                                        AND `page`.`deleted_at` IS NULL
                                        AND `page`.`active` = 1
                                        AND `pagegroup`.`deleted_at` IS NULL
                                    )')]);
                DB::table('cart')
                        ->where('cart.user_id', $user->id)
                        ->whereNull('cart.deleted_at')
                        ->update(['active' =>
                            DB::raw('EXISTS(
                                    SELECT `pagegroup`.`page_id`
                                    FROM `pagegroup`
                                    JOIN `download` ON `download`.`page_id` = `pagegroup`.`page_id`
                                    JOIN `page` ON `download`.`page_u_id` = `page`.`u_id`
                                    WHERE `download`.`file_id` = `cart`.`file_id`
                                        AND `pagegroup`.`group_id` IN ('.implode(", ", $groups).')
                                        AND `pagegroup`.`deleted_at` IS NULL
                                        AND `download`.`deleted_at` IS NULL
                                        AND `page`.`deleted_at` IS NULL
                                        AND `page`.`active` = 1
                                        AND `pagegroup`.`page_id` NOT IN(
                                            SELECT `request_auth`.`page_id` FROM `request_auth`
                                            WHERE `request_auth`.`user_id` = '.$user->id.'
                                            AND `request_auth`.`page_id` = `pagegroup`.`page_id`
                                            AND `request_auth`.`active` = 1
                                            AND `request_auth`.`deleted_at` IS NULL)
                                    ) OR NOT EXISTS(SELECT `download`.`page_id` from `download`
                                                JOIN `page` ON `download`.`page_u_id` = `page`.`u_id`
                                                JOIN `pagegroup` ON `page`.`id` = `pagegroup`.`page_id`
                                                WHERE `download`.`file_id` = `cart`.`file_id`
                                                AND `download`.`deleted_at` IS NULL
                                                AND `page`.`deleted_at` IS NULL
                                                AND `page`.`active` = 1
                                                AND `pagegroup`.`deleted_at` IS NULL
                                    )')]);
//                $queries = DB::getQueryLog();
//$last_query = end($queries);
//print_r($last_query);die;
            }
            //Success !!!
            DB::commit();
            if ($isUpdate) {
                $user->unlock_code = '';
                $user->newsletter_code = '';
            }

            $result = Response::json(RestController::generateJsonResponse(false, $message, $user));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            DB::rollback();
          
			// Get messages
			$messages = $e->getErrors();

			$result = Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.validation.failed'), $messages), 400);
	} catch (\Exception $e) {
            // Rollback and then return errors
            DB::rollback();

            $message = $e->getMessage();
            if (0 === strpos($message, 'SQLSTATE[23000]')) {
                $errMessage = trans('validation.unique', ["attribute" => "username"]);
                $errorArray = RestController::generateJsonResponse(true, $errMessage);
            } else {
                $errorArray = RestController::generateJsonResponse(true, $message);
            }
            if (config('app.debug')) {
                // Add stack trace
                $errorArray['debug'] = ['exception_message' => $message, 'stacktrace' => $e->getTraceAsString()];
            }

            $result = Response::json($errorArray, 500);
        }

        return $result;
    }

    /**
     * Return Update User from JSON
     * URL: user/update/{$id}
     * METHOD: PUT
     */
    public function putUpdate()
    {
        $update_user = Request::json()->all();
        //TODO validate
        if (empty($update_user['id'])) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $user = User::find($update_user['id']);
        if (is_null($user)) {
            return Response::json(RestController::generateJsonResponse(true, 'User not found.', $update_user), 404);
        }
        if (!isset($update_user['password']) || $update_user['password'] == "") {
            unset($update_user['password']);
            unset($update_user['password_confirmation']);
        }

        
        $user->fill($update_user);
		// Set newsletter to newsletter_include no need of optin in BE. 
        $user->newsletter = $user->newsletter_include;
        
                
        $pageAuth = [];
        $pageAuthList = [];

        if (isset($update_user['group']) && count($update_user['group']) > 0) {
            $pageAuthList = DB::table('request_auth')
                    ->join('pagegroup', 'pagegroup.page_id', '=', 'request_auth.page_id')
                    ->join('page', 'page.id', '=', 'request_auth.page_id')
                    ->join('content', 'page.u_id', '=', 'content.page_u_id')
                    ->whereIn('pagegroup.group_id', $update_user['group'])
                    ->where('request_auth.active', 0)
                    ->where('request_auth.user_id', $update_user['id'])
                    ->whereNull('pagegroup.deleted_at')
                    ->whereNull('request_auth.deleted_at')
                    ->whereNull('page.deleted_at')
                    ->where('page.active', 1)
                    ->where('content.section', 'title')
                    ->select('page.slug', 'page.template', 'content.body as title', 'content.lang as clang', 'request_auth.lang')
                    ->get()->toArray();
        }
        $result = $this->trySave($user, true);

        
        if ($result->isSuccessful() && count($pageAuthList) > 0) {
            $this->sendEmailsForGrantAccess($user, $pageAuthList);
        }

        return $result;
    }

    private function sendEmailsForGrantAccess(User $user, array $pageAuthList)
    {
        // Detect user language
        $localeStat = [];
        foreach ($pageAuthList as $pageAuth) {
            if (empty($localeStat[$pageAuth->lang])) {
                $localeStat[$pageAuth->lang] = 1;
            } else {
                $localeStat[$pageAuth->lang]++;
            }
        }

        $max_used_lang = array_keys($localeStat, max($localeStat));
        $langId = getLangId($max_used_lang[0]);

        // Email for Download pages list
        $downloadAuthList = array_filter($pageAuthList, function ($e) use ($langId) {
            return $e->template == 'download' && $e->clang == $langId;
        });
        if (count($downloadAuthList) > 0) {
            $params = [
                'FIRST_NAME' => $user->first_name,
                'LAST_NAME'  => $user->last_name,
                'PAGES'      => $downloadAuthList,
                'LANG'       => $downloadAuthList[0]->lang
            ];
            Mail::send('emails.request_auth_access_grant', $params, function ($message) use ($user, $params) {
                $message->to($user->email)
                        ->subject(Lang::get('emails.request_access_grant.subject', [], $params['LANG']))
                        ->from(config('mail.from.address'))
                        ->cc(config('mail.from.address'));
            });
        }

        //Emails for Basic pages list
        $basicAuthList = array_filter($pageAuthList, function ($e) use ($langId) {
            return $e->template != 'download' && $e->clang == $langId;
        });
        if (count($basicAuthList) > 0) {
            foreach ($basicAuthList as $basicAuth) {
                $params = [
                    'FIRST_NAME' => $user->first_name,
                    'LAST_NAME'  => $user->last_name,
                    'PAGE'       => $basicAuth,
                    'LANG'       => $basicAuth->lang
                ];
                Mail::send('emails.request_auth_access_grant', $params, function ($message) use ($user, $params) {
                    $message->to($user->email)
                            ->subject(Lang::get('emails.request_access_grant.subject', [], $params['LANG']))
                            ->from(config('mail.from.address'))
                            ->cc(config('mail.from.address'));
                });
            }
        }
    }

    /**
     * Delete User
     * URL: user/delete
     * METHOD: DELETE
     */
    public function deleteDelete($id = null)
    {

        $req = Request::all();

        //parameters id must be set
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        if ($id == config('app.default_workflow_admin') || $id == config('app.default_workflow_admin2')) {
            return Response::json(RestController::generateJsonResponse(true, 'You can not delete the main administrator!'), 400);
        }

        $user = User::find($id);

        if (is_null($user)) {
            return Response::json(RestController::generateJsonResponse(true, 'User not found.'), 404); // in response user not exists
        }
        $deleted_user = $user;
        $deleted_user->GDPRErase();
        $user->unlock_code = '';
        $user->newsletter_code = '';
        return Response::json(RestController::generateJsonResponse(false, 'User was deleted!', $user));
    }

    /**
     * Force Delete User only for Tests
     * URL: user/delete
     * METHOD: DELETE
     */
    public function deleteForcedelete($id = null)
    {
        Usergroup::withTrashed()->where('user_id', '=', $id)->forceDelete();
        $user = User::withTrashed()->find($id);
        $user->forceDelete();
    }

    /**
     * Return Users from Group
     * URL: user/readfromgroup/{$id}
     * METHOD: GET
     */
    public function getReadfromgroup($id = null)
    {
        if (empty($id)) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter ID is required!'), 400);
        }

        $user_ids = Usergroup::where('group_id', '=', $id)->get(); // get users in group
        $users = [];
        foreach ($user_ids as $user_id) {
            //get attributes with user dependency from model
            $user_item = $user_id->user()->first()->toArray();
            $users[] = $user_item;
        }

        if (empty($users)) {
            return Response::json(RestController::generateJsonResponse(true, 'Group Users not found'), 404);
        } else {
            return Response::json(RestController::generateJsonResponse(false, 'Group Users found', $users));
        }
    }

    /**
     * Retrieve the list of all users subscribed to the newsletter
     *
     */
    public function getNewsletterList()
    {
        $subscribed = User::where('newsletter', 1)
                            ->where('newsletter_include', 1)
                            ->orderBy('id', 'asc')
                            ->get(['email']);

        $additional_addresses = '';
        $filename = app_path() .'/config/newsletter_appendix.csv';
        if (file_exists($filename)) {
            $additional_addresses = file_get_contents($filename);
        }

        return '<div style="white-space:pre;">'.implode("\r\n", $subscribed->pluck('email')->toArray()) . "\r\n". $additional_addresses.'</div>';
    }
}

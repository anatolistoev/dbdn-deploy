<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Helpers\ValidationHelper;
use App\Models\Comment;
use App\Models\Page;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CommentsPageController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($idslug)
    {
        $rules = [
            'headline' => ['required','no_tags'],
            'comments' => ['required','no_tags'],
            'comment_agree' => 'accepted',
        ];
        $fields = [
            'headline' => Request::get('headline'),
            'comments' => Request::get('comments'),
            'comment_agree' => Request::get('comment_agree'),
        ];
        
        $customMessages = ValidationHelper::getFEValidationTranslation();
        $customAttributes = $customMessages['attributes'];
        $inputValidator = Validator::make($fields, $rules, $customMessages, $customAttributes);

        if ($inputValidator->fails()) {
            if (isset($_SERVER['HTTP_REFERER'])) {
                return Redirect::back()
                    ->withErrors($inputValidator)
                    ->withInput(Request::all());
            } else {
                return redirect('home')
                    ->withErrors($inputValidator)
                    ->withInput(Request::all());
            }
        }

        // check if the page exists, is active and has version in the current language
        if (is_numeric($idslug)) {
            $page = Page::alive()
                ->haveLang()
                ->find($idslug)
                ->first();
        } else {
            $page = Page::alive()
                ->haveLang()
                ->where('slug', $idslug)
                ->first();
        }

        if ($page) {
            $comment = new Comment();
            $comment->user_id = Auth::user()->id;
            $comment->page_id = $page->id;
            $comment->lang = Session::get('lang');
            $comment->subject = Request::get('headline');
            $comment->body = Request::get('comments');
            $comment->active = 0;

            try {
                $comment->save();
            } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
                if (isset($_SERVER['HTTP_REFERER'])) {
                    return Redirect::back()
                        ->withErrors($e->getErrors())
                        ->withInput(Request::all());
                } else {
                    return redirect('home')
                        ->withErrors($e->getErrors())
                        ->withInput(Request::all());
                }
            }

            return Redirect::back()->withErrors(['success' => trans('template.comments.thanks')]);
        } # if the page is not valid silently ignore and go back

        return Redirect::back();
    }
}

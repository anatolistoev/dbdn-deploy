<?php namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Page;
use App\Models\Subscription;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $lang;

    /************************************************************************
	 *
	 *
	 ***********************************************************************/
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $lang = $request->get('lang');
            if ($lang) {
                $this->switchLanguage($lang);
            }
            $this->lang = Session::get('lang');
            //print_r(setlocale(LC_TIME, '0'));die;
            if (Auth::check()) {
                $userID = Auth::user()->id;
                $count = Cart::where('user_id', $userID)->where('active',1)->get()->count() + 
                    Page::alive()
                    ->select('download.page_u_id')
                    ->join('request_auth', 'request_auth.page_id', '=', 'page.id')
                    ->where('request_auth.active', 1)
                    ->where('request_auth.user_id', $userID)
                    ->join('download', 'download.page_u_id', '=', 'page.u_id')
                    ->join('file', 'file.id', '=', 'download.file_id')
                    ->whereNull('request_auth.deleted_at')
                    ->whereNull('download.deleted_at')
                    ->whereNull('file.deleted_at')
                    ->groupBy('download.page_u_id')
                    ->get()
                    ->count();

                Session::put('myDownloadsCount', $count);
                Session::put('watchlistCount', Subscription::where('user_id', '=', $userID)->where('active', 1)->get()->count());
            }
            // Init menu
            //TODO: Load active status of menu items more clever just before use it
            if (!in_array(Request::path(), array('captcha'))) {
                $this->loadBrandMenu();
            }
            
            return $next($request);
        });
    }
    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (! is_null($this->layout)) {
            $this->layout = view($this->layout);
        }
    }
    /************************************************************************
	 *
	 *	Switch the current language
	 *
	 ***********************************************************************/
    protected function switchLanguage($locale)
    {
        $key = array_search($locale, config('app.locales'));
        if ($key !== false && $key !== null) {
            Session::put('lang', $key + 1);
            App::setLocale($locale);
            $time_locales = config('app.time_locales');
            call_user_func_array('setlocale', array_merge([LC_TIME], $time_locales[$key]));
            $this->lang = Session::get('lang');
        }
    }

	/************************************************************************
	 *
	 *	Load dynamic attributes for pages of brand menu
	 *
	 ***********************************************************************/
    protected function loadBrandMenu() {
        $specialPages = config('app.pages');
        $brand_keys_array = array_keys($specialPages);
        $brand_pages      = Page::select('page.id as page_id', 'page.active')
                        ->whereIn('page.id', $brand_keys_array)
                        ->where('page.active', 1)
                        ->pluck('page.active', 'page_id')
                        ->toArray();
                        
        // Update active status
        foreach ($specialPages as $page) {
            $page->active = array_key_exists( $page->ID, $brand_pages );
        }        
    }

    protected static function getLastErrorMsg()
    {
        $e = error_get_last();
        if ($e) {
            return $e['message'];
        } else {
            return null;
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Helpers\CaptchaHelper;
use App\Helpers\ValidationHelper;
use App\Models\Page;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class FeedbackPageController extends Controller
{

    /**
     * Get the title of a page
     * @param mixed $idslug The ID or the slug of the page
     * @return string
     */
    private function getPageTitle($idslug)
    {
        $lang = $this->lang;
        if (is_numeric($idslug)) {
            $subject = Page::with([
                'contents' => function ($query) use ($lang) {
                    return $query->where('lang', $lang)->where('section', 'title');
                }
            ])->where('id', $idslug)->take(1)->get();
        } else {
            $subject = Page::with([
                'contents' => function ($query) use ($lang) {
                    return $query->where('lang', $lang)->where('section', 'title');
                }
            ])->where('slug', $idslug)->take(1)->get();
        }

        if ($subject->first() && $subject->first()->contents->first()) {
            return $subject->first()->contents->first()->body;
        } else {
            return $idslug;
        }
    }

    /**
     * Display a feedback form.
     *
     * @return Response
     */
    public function index($idslug)
    {
        CaptchaHelper::resetCaptcha();

        $title = $this->getPageTitle($idslug);

        return view(
            'feedback',
            [
                'PAGE' => (object)['id' => 'feedback/' . $idslug, 'slug' => 'feedback', 'template' => 'system'],
                'SUBJECT_TITLE' => $title,
            ]
        );
    }

    /**
     * Send feedback message.
     *
     * @param  mixed $idslug
     * @return Response
     */
    public function send($idslug)
    {
        $rules = [
            'email' => 'required|email',
            'comments' => 'required',
            'captcha' => 'required|same:captchaSecret',
        ];
        $fields = [
            'email' => Request::get('email'),
            'comments' => Request::get('comments'),
            'captcha' => Request::get('captcha'),
            'captchaSecret' => Session::get('captcha'),
        ];
        $inputValidator = Validator::make($fields, $rules, ValidationHelper::getFEValidationTranslation());

        // Log the usage of captcha
        CaptchaHelper::logCaptchaUsage();
        // Reset capcha to prevent reuse one captcha.
        CaptchaHelper::resetCaptcha();

        if ($inputValidator->fails()) {
            if (isset($_SERVER['HTTP_REFERER'])) {
                return Redirect::back()
                    ->withErrors($inputValidator)
                    ->withInput(Request::except(['captcha']));
            } else {
                return redirect('home')
                    ->withErrors($inputValidator)
                    ->withInput(Request::except(['captcha']));
            }
        }

        $fields['pid'] = $idslug;
        $fields['title'] = $this->getPageTitle($idslug);

        Mail::send('emails.feedback_message', $fields, function ($message) {
            $message->to(config('mail.from.address'))->subject('Daimler Brand & Design Navigator - Feedback');
            $message->from(Request::get('email'));
        });

        return view(
            'feedback',
            [
                'PAGE' => (object)['id' => 'feedback/' . $idslug, 'slug' => 'feedback', 'template' => 'system'],
                'SENT' => true,
            ]
        );
    }
}

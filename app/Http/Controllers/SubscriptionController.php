<?php

namespace App\Http\Controllers;

use App\Exception\ModelValidationException;
use App\Models\Subscription;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

/**
 * Description of SubscriptionController
 *
 * @author i.traykov
 */
class SubscriptionController extends RestController
{

    /**
     * Return User subscripted Pages
     * URL: subscription/readbyuser
     * METHOD: GET
     */

    public function getReadbyuser($userId = 0)
    {
        //parameter userId must be set
        if ($userId <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter userId is required!'), 400);
        }

        $subscriptions_items = Subscription::where('active', 1)->readbyuser($userId)->get();
		$page_items = array();
		foreach($subscriptions_items as $subscription){
			$page_item = $subscription->page()->first()->attributesToArray();
			$page_items[] = $page_item;
		}         

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of pages found.', $page_items), 200);

        return $jsonResponse;
    }

    /**
     * Return Page subscripted Users
     * URL: subscription/readbypage
     * METHOD: GET
     */

    public function getReadbypage($pageId = 0)
    {
        //parameter pageId must be set
        if ($pageId <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter pageId is required!'), 400);
        }

		$subs_items = Subscription::where('active', 1)->readbypage($pageId)->get();
		$user_items = array();
		foreach($subs_items as $subs){
			$user_item = $subs->user()->first()->attributesToArray();
			$user_items[] = $user_item;
        }

        $jsonResponse = Response::json(RestController::generateJsonResponse(false, 'List of users found.', $user_items), 200);

        return $jsonResponse;
    }

    /**
     * Subscript User to Page
     * URL: subscription/create
     * METHOD: POST
     */

    public function postCreate()
    {
        $req = Request::all();

        //parameter user_id and page_id must be set
        if (!isset($req['user_id']) || $req['user_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter user_id is required!'), 400);
        }
        if (!isset($req['page_id']) || $req['page_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }

        $subscription = new Subscription();
        $subscription->fill($req);

        return $this->trySave($subscription, false);
    }

    /**
     * Return save result
     */
    protected function trySave(Subscription $subscription, $isUpdate)
    {
        try {
            if ($isUpdate) {
                $message = "Subscription was updated.";
            } else {
                $message = "Subscription was added.";
            }

            $subscription->save();
            unset($subscription->id);
            //Success !!!

            $result = Response::json(RestController::generateJsonResponse(false, $message, $subscription));
        } catch (\LaravelArdent\Ardent\InvalidModelException $e) {
            throw new ModelValidationException($e->getErrors());
        }
        return $result;
    }


    /**
     * Remove Subscription
     * URL: subscription/delete
     * METHOD: DELETE
     */

    public function deleteDelete()
    {

        $req = Request::all();

        //parameters user_id and page_id must be set
        if (!isset($req['user_id']) || $req['user_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter user_id is required!'), 400);
        }
        if (!isset($req['page_id']) || $req['page_id'] <= 0) {
            return Response::json(RestController::generateJsonResponse(true, 'Parameter page_id is required!'), 400);
        }

        $subscriptionitem = Subscription::where('user_id', '=', $req['user_id'])->where('page_id', '=', $req['page_id'])->get();

        if ($subscriptionitem->isEmpty()) {
            return Response::json(RestController::generateJsonResponse(true, 'Subscription not found.'), 404);  // in response subscription not exists
        }

        $deleteditem = $subscriptionitem[0];
        Subscription::where('user_id', '=', $deleteditem->user_id)->where('page_id', '=', $deleteditem->page_id)->delete();

        return Response::json(RestController::generateJsonResponse(false, 'Subscription was deleted!', $subscriptionitem[0]));
    }
}

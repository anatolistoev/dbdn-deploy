<?php

namespace App\Http\Controllers;

use App\Helpers\DispatcherHelper;
use App\Helpers\FileHelper;
use App\Models\Download;
use App\Models\Gallery;
use App\Models\Newsletter;
use App\Models\Page;
use App\Helpers\UserAccessHelper;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

include_once(base_path('vendor/ifsnop/mysqldump-php/src/Ifsnop/Mysqldump/Mysqldump.php'));

class SystemController extends Controller
{

    // 'artisan/{command}'
    public function runArtisanCommand($command)
    {
        if (config('app.debug') && App::environment('testing')) {
            return Artisan::call($command);
        } else {
            return "Not available in runtime!";
        }
    }

    public function clearViews()
    {
        foreach (new \DirectoryIterator(storage_path() . '/views') as $fileInfo) {
            if ($fileInfo->isFile()) {
                print_r("\n file: " . $fileInfo->getFilename());
                if (substr($fileInfo->getFilename(), 0, 1) != '.') {
                    print_r(" deleted: " . @unlink($fileInfo->getPathname()));
                }
            }
        }
    }

    public function displayView($filename)
    {
        $fileInfo = storage_path() . '/views/' .$filename;
        if (file_exists($fileInfo)) {
            $result = file_get_contents($fileInfo);
            print_r(nl2br(htmlentities($result)));
        }
    }

    public function clearFonts()
    {
        // Fonts
        $fontsPath = storage_path('fonts/');
        if (file_exists($fontsPath)) {
            foreach (new \DirectoryIterator($fontsPath) as $fileInfo) {
                if ($fileInfo->isFile()) {
                    print_r("<br/> file: " . $fileInfo->getFilename());
                    if (substr($fileInfo->getFilename(), 0, 1) != '.') {
                        print_r(" deleted: " . @unlink($fileInfo->getPathname()));
                    }
                }
            }
        }
        print_r("<br/> Clear dir: $fontsPath :" . @unlink($fontsPath));
        print_r("<br/> Create dir fonts: " . FileHelper::makedirs($fontsPath));
    }

    /**
     * Return Cache Directoey
     * URL: system/clear/cache/{FILEMANAGER_MODE}
     * METHOD: GET DELETE
     */
    public function deleteCache($filemanager_mode)
    {
        
        if (!UserAccessHelper::isAdmin()) {
            return Response::json(RestController::generateJsonResponse(true, 'You don\'t have access to Clear Cache!'), 404);
        }
        
        $cacheFolderPath = 'files/' . $filemanager_mode . '/' . FileController::FOLDER_NAME_CACHE;
        $absPath = public_path() . '/' . $cacheFolderPath;
        if (file_exists($absPath)) {
            //print_r('<br>Delete cache: ' . $cacheFolderPath);
            try {
                $result = FileHelper::safeDeleteFolder($absPath);
            } catch (\Exception $exc) {
                Log::debug($exc);
                $result = $exc->getMessage();
                return Response::json(RestController::generateJsonResponse(
                    true,
                    $result
                ), 400);
            }
        } else {
            Log::warning('The cache folder is empty: ' . $absPath);
        }
        //print_r('<br>Delete result: ' . $result);

        // Clear protected files cache
        $cacheFolderPath = 'protected_files/' . $filemanager_mode . '/' . FileController::FOLDER_NAME_CACHE;
        $absPath = public_path() . '/' . $cacheFolderPath;
        if (file_exists($absPath)) {
            //print_r('<br>Delete cache: ' . $cacheFolderPath);
            try {
                $result = FileHelper::safeDeleteFolder($absPath);
            } catch (\Exception $exc) {
                Log::debug($exc);
                $result = $exc->getMessage();
                return Response::json(RestController::generateJsonResponse(
                    true,
                    $result
                ), 400);
            }
        } else {
            Log::warning('The cache folder is empty: ' . $absPath);
        }
        //print_r('<br>Delete result: ' . $result);
        Response::json(RestController::generateJsonResponse(
            false,
            'Cache in ' . $filemanager_mode . ' was cleared.'
        ), 200);
    }

    /**
     * Synchronize files: Images or Downloads from other site
     * URL: system/sync-files-local/{FILEMANAGER_MODE}
     * METHOD: GET
     */
    public function syncFilesLocal($filemanager_mode)
    {
        return true;
        $srcFolderBasePath = '/data/dbdn/vhosts/vhost1/htdocs/public/';
        //$srcFolderBasePath = 'C:\\Martin_Workspace\\NetBeans\\dbdn_dev_dbdn15\\public\\';
        $destFolderPath    = 'files/' . $filemanager_mode;
        $srcFolderPath     = $srcFolderBasePath . $destFolderPath;
        print_r('Synch folder: ' . $destFolderPath . ' from folder: ' . $srcFolderPath);
        $errors            = [];
        try {
            $result = FileHelper::xcopy(
                $srcFolderPath,
                public_path() . '/' . $destFolderPath,
                FILE_DEFAULT_ACCESS,
                true,
                $errors
            );
        } catch (\Exception $exc) {
            $result = $exc->getMessage();
        }
        print_r('<br>Sync result: ' . $result);
        if (count($errors) > 0) {
            print_r('<br>');
            print_r($errors);
        }

        if ($filemanager_mode == FileController::MODE_DOWNLOADS) {
            // Clear protected files cache
            $destFolderPath = 'protected_files/' . $filemanager_mode;
            $srcFolderPath  = $srcFolderBasePath . $destFolderPath;
            print_r('<br>Synch folder: ' . $destFolderPath . ' from folder: ' . $srcFolderPath);
            $errors         = [];
            try {
                $result = FileHelper::xcopy(
                    $srcFolderPath,
                    public_path() . '/' . $destFolderPath,
                    FILE_DEFAULT_ACCESS,
                    true,
                    $errors
                );
            } catch (\Exception $exc) {
                $result = $exc->getMessage();
            }
            print_r('<br>Sync result: ' . $result);
            if (count($errors) > 0) {
                print_r('<br>');
                print_r($errors);
            }
        }
    }

    /**
     * Synchronize files: Images or Downloads for page from other site
     * URL: system/sync-page-files/{PAGE_ID}
     * METHOD: GET
     */
    public function syncPageFiles($page_id)
    {
        return true;
        $srcFolderBasePath = '/data/dbdn/vhosts/vhost1/htdocs/public/';
        //$srcFolderBasePath = 'C:\\Martin_Workspace\\NetBeans\\dbdn_dev_dbdn15\\public\\';
        $destFolderPath    = 'files/' . $filemanager_mode;
        $srcFolderPath     = $srcFolderBasePath . $destFolderPath;
        print_r('Synch folder: ' . $destFolderPath . ' from folder: ' . $srcFolderPath);
        $errors            = [];
        try {
            $result = FileHelper::xcopy(
                $srcFolderPath,
                public_path() . '/' . $destFolderPath,
                FILE_DEFAULT_ACCESS,
                true,
                $errors
            );
        } catch (\Exception $exc) {
            $result = $exc->getMessage();
        }
        print_r('<br>Sync result: ' . $result);
        if (count($errors) > 0) {
            print_r('<br>');
            print_r($errors);
        }

        if ($filemanager_mode == FileController::MODE_DOWNLOADS) {
            // Clear protected files cache
            $destFolderPath = 'protected_files/' . $filemanager_mode;
            $srcFolderPath  = $srcFolderBasePath . $destFolderPath;
            print_r('<br>Synch folder: ' . $destFolderPath . ' from folder: ' . $srcFolderPath);
            $errors         = [];
            try {
                $result = FileHelper::xcopy(
                    $srcFolderPath,
                    public_path() . '/' . $destFolderPath,
                    FILE_DEFAULT_ACCESS,
                    true,
                    $errors
                );
            } catch (\Exception $exc) {
                $result = $exc->getMessage();
            }
            print_r('<br>Sync result: ' . $result);
            if (count($errors) > 0) {
                print_r('<br>');
                print_r($errors);
            }
        }
    }

    /**
     * Synchronize files: Images and Downloads from other site by URL
     * URL: system/sync-files-url/{param}
     * METHOD: GET
     */
    public function syncFilesURL($param)
    {
        //TODO: Simple protection!
        $params = ["server" => $param];
        if (Request::has('src-url')) {
            $params['--src-url'] = Request::get('src-url');
        }
        if (Request::has('max-size')) {
            $params['--max-size'] = Request::get('max-size');
        }
        if (Request::has('force')) {
            $params['--force'] = Request::get('force');
        }
        if (Request::has('crul')) {
            $params['--curl'] = Request::get('curl');
        }

        set_time_limit(3600);// to 1h for example
        $outputStream = new \Symfony\Component\Console\Output\StreamOutput(
            fopen('php://output', 'w')
        );

//        $outputStream = new \Symfony\Component\Console\Output\ConsoleOutput();

        //ob_start();
        $res = Artisan::call('dbdn:sync-files', $params, $outputStream);
        //$commandOutput = ob_get_clean();
        return $res;
    }

    /**
     * Send Newsletter Emails
     * URL: system/send-newsletter-emails/slug?{params}
     * METHOD: GET
     */
    public function sendNewsletterEmails($slug)
    {

        $input = Request::all();

        $params = [];

        if ('sending' != $slug) {
            $nl = Newsletter::getBySlug($slug);
            $params['--id'] = $nl->id;
        }

        $params['--email'] = Request::get('e', false);

        if (Request::has('nm')) {
            $params['--name'] = Request::get('nm');
        }

        $params['--send-limit'] = Request::get('m', 1);

        $params['--test-mode'] = Request::get('t', true);

        $params['--test-list'] = Request::get('l', true);

        $params['--dry-run'] = Request::get('d', true);

        $params['--verbose'] = true;

        $outputStream = new \Symfony\Component\Console\Output\StreamOutput(
            fopen('php://output', 'w')
        );


        echo '<pre>';

        $res = Artisan::call('SendNewsletterEmails', $params, $outputStream);

        echo '</pre>';

        //echo "\n==========\n"; var_dump($res); die;

        return $res;
    }


    /**
     * Return system load.
     * URL: system/system-load
     * METHOD: GET
     */
    public function systemLoad()
    {
        print_r('System load: <br>');
        $stat = SystemController::getServerLoad(true);
        if ($stat) {
            print_r($stat);
        } else {
            print_r(" Cannot detect server load.");
        }
    }

    public static function getServerLoad($windows = false)
    {
        $os = strtolower(PHP_OS);
        if (strpos($os, 'win') === false) {
            if (function_exists("sys_getloadavg")) {
                // sys_getloadavg() will return an array with [0] being load within the last minute.
                $load    = sys_getloadavg();
                $load[0] = round($load[0], 4);
            } elseif (file_exists('/proc/loadavg')) {
                $load = file_get_contents('/proc/loadavg');
                $load = explode(' ', $load);
                $load = round($load[0], 4);
            } elseif (function_exists('shell_exec')) {
                $load = explode(' ', `uptime`);
                $load = $load[count($load) - 1];
            } else {
                return false;
            }

            if (function_exists('shell_exec')) {
                $cpu_count = shell_exec('cat /proc/cpuinfo | grep processor | wc -l');
            }

            $memory = ['mem' => memory_get_peak_usage(), 'allMem' => memory_get_peak_usage(true)];

            return ['load' => $load, 'procs' => $cpu_count, 'memory' => $memory];
        } elseif ($windows) {
            if (class_exists('COM')) {
                $wmi       = new COM('WinMgmts:\\\\.');
                $cpus      = $wmi->InstancesOf('Win32_Processor');
                $load      = 0;
                $cpu_count = 0;
                if (version_compare('4.50.0', PHP_VERSION) == 1) {
                    while ($cpu = $cpus->Next()) {
                        $load += $cpu->LoadPercentage;
                        $cpu_count++;
                    }
                } else {
                    foreach ($cpus as $cpu) {
                        $load += $cpu->LoadPercentage;
                        $cpu_count++;
                    }
                }
                return ['load' => $load, 'procs' => $cpu_count];
            }
            return false;
        }
        return false;
    }

    private function getPageFiles($page_id, $filemanager_mode)
    {
        $file_items = [];
        $file_ids   = null;
        if ($filemanager_mode == FileController::MODE_DOWNLOADS) {
            $file_ids = Gallery::where('page_id', '=', $page_id)->orderBy(
                'position',
                'asc'
            )->get(); // get items on page
        } else {
            $file_ids = Download::where('page_u_id', '=', $page_id)->orderBy(
                'position',
                'asc'
            )->get(); // get items on page
        }
        foreach ($file_ids as $file_id) {
            //get attributes with file dependency from model
            $file_item    = $file_id->file()->first()->toArray();
            $file_items[] = $file_item;
        }
        return $file_items;
    }

    public function countPagesByTag()
    {
        $updatedRows = Artisan::call('CountPagesByTag');
        return $updatedRows;
    }

    public function cleanPDFCache()
    {
        $dir = config('app.PDF_path.generated');
        $it = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator(
            $it,
            \RecursiveIteratorIterator::CHILD_FIRST
        );
        $msg = '';
        foreach ($files as $file) {
            if ($file->isDir()) {
                $msg .= 'dir removed: '.$file->getRealPath().'</br>';
                rmdir($file->getRealPath());
            } else {
                 $msg .= 'file removed: '.$file->getRealPath().'</br>';
                unlink($file->getRealPath());
            }
        }

        echo $msg;
        die;
    }

    /**
     * Generate all PDFs
     */
    public static function generateAllPDFs($force = false)
    {

        $force = filter_var($force, FILTER_VALIDATE_BOOLEAN);
        $pages = Page::whereIn('page.template', ['smart', 'basic', 'exposed', 'best_practice'])
                ->where('overview', '=', 0)
                ->get();

        print_r('Generate pdf for pages. Count:' . $pages->count());

        $brands = array_keys(config('app.pages'));
        array_unshift($brands, HOME);

        $before = microtime(true);
        foreach ($pages as $page) {
            if ($page->active == 1 && !in_array($page->id, $brands)) {
                print_r("<br/>page:" . $page->id . ' slug:' . $page->slug . ' DE:');
                try {
                    $time = DispatcherHelper::genereateAndCachePdfByLang($page, LANG_DE, $force);
                    print_r('OK ' . number_format($time, 4) . '  ');
                } catch (\Exception $e) {
                    print_r('Problem:' . $e->getMessage());
                    Log::error('page:' . $page->id . ' slug:' . $page->slug . ' DE: Problem');
                    Log::error($e->getMessage());
                    Log::error($e->getTraceAsString());
                }

                try {
                    print_r(' EN:');
                    $time = DispatcherHelper::genereateAndCachePdfByLang($page, LANG_EN, $force);
                    print_r('OK ' . number_format($time, 4) . ' s');
                } catch (\Exception $e) {
                    print_r('Problem:' . $e->getMessage());
                    Log::error('page:' . $page->id . ' slug:' . $page->slug . ' EN: Problem');
                    Log::error($e->getMessage());
                    Log::error($e->getTraceAsString());
                }

                //break;
            }
        }
        $after = microtime(true);
        print_r('<br/>Generate pdf for pages. Average time:' . number_format(($after-$before)/$pages->count(), 4) . ' s Total time: ' . number_format(($after-$before), 4) . ' s');
    }

    /**
     * Backup DB
     * URL: system/backup-db?{params}
     * METHOD: GET
     */

    public function backupDb($params)
    {
        $databaseDriver = config('database.default', false);
        $dbConfig = config('database.connections.' . $databaseDriver);
        $connString = 'mysql:host='. $dbConfig['host'] .';dbname=' . $dbConfig['database'];

        $dumpSettings = [
            'compress' => \Ifsnop\Mysqldump\Mysqldump::GZIP,
            'add-drop-table' => true,
            'add-drop-trigger' => true,
            'routines' => true,
            'events' => true,
            'routines' => true,
        ];

        $dump = new \Ifsnop\Mysqldump\Mysqldump($connString, $dbConfig['username'], $dbConfig['password'], $dumpSettings);

        $backupPath = storage_path('backup');

        $date = new \DateTime();
        $dumpFileName = 'dump_dbdn_db_'. $date->format('Ymd_His') . '.sql';

        FileHelper::makedirs($backupPath);
        $dump->start($backupPath . '/' . $dumpFileName);
    }
}

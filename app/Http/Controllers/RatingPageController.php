<?php

namespace App\Http\Controllers;

use App\Models\Rating;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

class RatingPageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    //public function index()
    //{
    //	return view('ratingpages.index');
    //}

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            $pageID = Request::get('pageid');
            $vote = Request::get('vote');
            if (is_numeric($pageID) && is_numeric($vote)) {
                $rating = Rating::firstOrNew([
                                                    'user_id' => Auth::user()->id,
                                                    'page_id' => $pageID
                                                  ]);
                $rating->vote = $vote;
                $rating->save();

                return Redirect::back()->withErrors(['success' => trans('system.rating.added')]);
            }
            return Redirect::back()->withErrors([trans('system.rating.failed')]);
        } catch (\Exception $e) {
            return Redirect::back()->withErrors([trans('system.rating.failed')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    /*
	public function show($id)
	{
		try{
			$rating = Rating::where('page_id','=',$id)->where('user_id','=',Auth::user()->id)->get();
			if(!$rating->isEmpty())
				return Response::json( $rating->first() );
			else
				return Response::json( array('retrieving' => 'none') );
		}catch(Exception $e){
			return Response::json( array('retrieving' => 'failed') );
		}
	}
	*/


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    /*
	public function destroy($id)
	{
		//
	}
	*/
}

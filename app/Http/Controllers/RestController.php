<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class RestController extends Controller
{

    /**
     * Catch request to missing WS..
     */
    public function missingMethod($parameters = [])
    {
        // Return error if WS not exists.
        return Response::json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.notImplemented')), 404);
    }

    public static function generateJsonResponse($hasError, $message, $response = null)
    {
        $jsonResponse = null;
        if (is_null($response)) {
            $jsonResponse = ['error' => $hasError, 'message' => $message];
        } else {
            if ($response instanceof \Illuminate\Database\Eloquent\Model
                || $response instanceof \Illuminate\Database\Eloquent\Collection
                || $response instanceof  \Illuminate\Support\MessageBag) {
                $response = $response->toArray();
            } else {
                //TODO: Handling for other objects
                if (is_array($response) || $response instanceof \Illuminate\Support\Collection) {
                    // TODO: Array to json?
                } else if ($response instanceof \stdClass) {
                    $response = (array)$response;
                } else {
                    Log::error('Cannot make JSON for class: ' . get_class($response) . ' type: ' . gettype($response));
                }
            }
            $jsonResponse = ['error' => $hasError, 'message' => $message, 'response' => $response];
        }

        return $jsonResponse;
    }
}

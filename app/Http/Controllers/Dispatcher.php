<?php

namespace App\Http\Controllers;

use App\Helpers\CaptchaHelper;
use App\Helpers\ContentUpdater;
use App\Helpers\DispatcherHelper;
use App\Helpers\FileHelper;
use App\Helpers\FEFilterHelper;
use App\Helpers\UserAccessHelper;
use App\Models\Page;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class Dispatcher extends Controller
{

    /**
     * The default thumbnail for the brand.
     *
     * @var string
     */
    protected $defaultBrandThumbnail = null;

    public function __construct()
    {
        parent::__construct();
    }

    /************************************************************************
     * Main dispatching method. Handles old (pre-6.9.6) URLs as well as homepage requests
     * @return View
     ***********************************************************************/
    public function index()
    {
        if (Request::has('id') && ($id = Request::get('id')) && is_numeric($id)
            && Request::has('lang') && ($lang = Request::get('lang')) && in_array($lang, config('app.locales'))
          ) {
            return $this->changeLang($lang, $id);
        } else if (Request::has('id') && ($id = Request::get('id')) && is_numeric($id) && $id != 1) {
            return $this->getPageById($id);
        } else {
            $page = Page::alive()->haveLang()->where('id', '=', 1)->where('is_visible', 1)->first();
            return DispatcherHelper::getView($page, $this->lang);
        }
    }

    public function getFrontBlocksView($pageId, $type)
    {
        $front_blocks = DispatcherHelper::getFrontblocks($this->lang, $type);
        return view('partials.front_blocks', ['HOMEBLOCKS' => $front_blocks]);
    }


    /************************************************************************
     * Switch the current language and redirect to id/slug
     * @param $locale           Locale string
     * @param $id               ID of the page
     * @return Redirect::to
     ***********************************************************************/
    public function changeLang($locale, $idslug)
    {
        parent::switchLanguage($locale);
        return redirect(asset($idslug));
    }
    /************************************************************************
     * Get a view representing the page with the given ID
     * @param $id   ID of the page
     * @return View
     ***********************************************************************/
    public function getPageById($id)
    {
        $page = Page::alive()->haveLang()->find($id);
        return DispatcherHelper::getView($page, $this->lang);
    }
    /************************************************************************
     * Get a view representing a page by a slug
     * @param $slug     String to search for in page.slug
     * @return View
     ***********************************************************************/
    public function getPageBySlug($slug)
    {
        $page = Page::where('slug', '=', $slug)
                    ->alive()
                    ->haveLang()
                    ->first();
        return DispatcherHelper::getView($page, $this->lang);
    }

    /************************************************************************
     * Redirect to home page with started guided tour
     * @return View
     ***********************************************************************/
    public function getGuideTour()
    {
        $cookie = Cookie::forget("guided_tour_cookie");
        return redirect('home')->withCookie($cookie);
    }


    /************************************************************************
     * Get a view representing the page menu
     * @param $what is id of the page
     * @return View
     ***********************************************************************/

    public function getPageMenu($what)
    {
        if (0 !== strpos($what, 'L2ID')) {
            throw new \Exception("Unsupported menu request", 1);
        }

        $cacheKey = 'pageMenu'.$what.'_'.$this->lang;

        //Cache::flush();

        if (false && Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $id = (int)str_replace('L2ID', '', $what);

        $page = Page::alive()->haveLang()->where('id', $id)->first();
//TODO: Check if page exist and return message(page) to the user!
        $menu_template = ('basic' == $page->template) ? 'basic_menu' : 'filter_menu';

        $ascendants = Page::ascendants($page->id, $this->lang)->get();
        $ascendantIdIndex  = DispatcherHelper::getIdIndex($ascendants);
        $sectionId = $page->parent_id == HOME ? $page->id : DispatcherHelper::getActiveL2($ascendants);
        $sectionPages = Page::descendants($sectionId, $this->lang, 0)->alive()->haveLang()->get();
        $sectionPIndex = DispatcherHelper::getParentIndex($sectionPages);
        $level = $page->parent_id != 0 ? (isset($ascendantIdIndex[1]) ? $ascendantIdIndex[1]->level : 0) : 0;
        $pathIDs = DispatcherHelper::getPathIDs($ascendants, $page->id);
        $menuCarousel = DispatcherHelper::getMenuCarousel($level, $sectionPIndex, $sectionId, $page);

        $data = [
                'PAGE'              => $page,
                'L2ID'              => $sectionId,
                'LEVEL'             => $level,
                'SECTION_PID_INDEX' => $sectionPIndex,
                'PATH_IDS'          => $pathIDs,
                'MENUBLOCKS'        => $menuCarousel,
            ];

        $parced = view("partials.{$menu_template}", $data)->render();

        Cache::put($cacheKey, $parced, 30);

        return $parced;
    }

    /***********************************************************************
     * Generate a preview of a page from POST data
     * @param $pageID   Page ID
     * @return View containing the rendered page or custom 404 page
     ***********************************************************************/
    protected function getPreview($id)
    {
        $page = Page::find($id);
        $isPrint = Request::has('isPrint') ? true : false;
        if (!$page) {
            //return Response::view('errors.missing', array('PAGE'=> (object)array('id'=>404)), 404);
            return Response::view('errors.fatal', ['PAGE'=> (object)['id'=>404, 'slug' => 'fatal']], 404);
        }
            //abort(404, 'Page not found');
        #attempt to login
        if (!Auth::check() && Request::has('username')) {
            return LoginController::doLogin();
        }
        #page requires authentication
        if (!Auth::check()) {
            return view(
                'login_required',
                [
                    'PAGE' => (object) ['id' => $page->id,
                                             'slug'=> $page->slug,
                                             'title' => '',
                                             'template' => 'system'
                                            ],
                ]
            );
        #user doesn't have access
        } else {
            if (Auth::user()->role < 1) {
                return view(
                    'no_access',
                    [
                        'PAGE' => (object) ['id' => $page->id,
                                                'u_id' => $page->u_id,
                                                'slug'=> $page->slug,
                                                'title' => '',
                                                'template' => 'system'
                                                ],
                    ]
                );
            } else {
                if (!UserAccessHelper::hasAccessForPageAndAscendants($page->id)) {
                    $error_message = trans('ws_general_controller.webservice.unauthorized');
                    return response()->view('backend.be_unauthorized_access', [ 'error' => $error_message], 401);
                }
            }
        }

        // Load page content
        $contents = $page->contents()->where('lang', '=', $this->lang)->get()->toArray();
        $banner = $main = $right = $farRight = $author = $tablet_banner = $mobile_banner = null;
        foreach ($contents as $value) {
            if ($value['section'] == 'banner') {
                $banner = Request::has('banner')? \Purifier::clean(Request::get('banner')) : $value['body'];
            } elseif ($value['section'] == 'main') {
                $main = Request::has('main') ? \Purifier::clean(Request::get('main')) :  $value['body'];
            } elseif ($value['section'] == 'right') {
                $right = $value['body'];
            } elseif ($value['section'] == 'farRight') {
                $farRight = $value['body'];
            } elseif ($value['section'] == 'author') {
                $author = Request::has('author')?  \Purifier::clean(Request::get('author')) : $value['body'];
            } elseif ($value['section'] == 'tablet_banner') {
                $tablet_banner = Request::has('tablet_banner')?  \Purifier::clean(Request::get('tablet_banner')) : $value['body'];
            } elseif ($value['section'] == 'mobile_banner') {
                $mobile_banner = Request::has('mobile_banner')?  \Purifier::clean(Request::get('mobile_banner')) : $value['body'];
            }
        }       
        $sectionContents = [
            'banner'=> $banner ? $banner : '', 
            'main'=> $main ? $main : '', 
            'right'=> $right ? $right: '', 
            'farRight'=> $farRight? $farRight: '', 
            'author'=> $author? $author: '', 
            'tablet_banner'=> $tablet_banner ? $tablet_banner:'', 
            'mobile_banner'=> $mobile_banner? $mobile_banner : '']; #setting the default contents

        $pageTitle = array_values(array_filter($contents, function ($e) {
            return $e['section'] == 'title';
        }));
        $sectionContents['title'] = count($pageTitle)> 1? $pageTitle[0]['body'] : '';

        $pageContents = DispatcherHelper::addPageContentByTemplate($page, $this->lang, $sectionContents, true);

        $page->brand  = $page->parent_id == HOME ? $page->id : DispatcherHelper::getActiveL2(Page::ascendants($page->id, $this->lang)->get());
        //print_r(array_merge(array('CONTENTS' => $sectionContents,'PAGE'=> $page),$pageContents));die;
        if ($isPrint) {
            $ascendants = Page::ascendants($page->id, $this->lang)->get();
            $sectionId = $page->parent_id == HOME ? $page->id : DispatcherHelper::getActiveL2($ascendants);
            if ($sectionId == -1) {
                 return Response::view('errors.fatal', ['PAGE'=> (object)['id'=>404, 'slug' => 'fatal']], 404);
            }
            $download_files = null;
            if (in_array($page->template, ['downloads'])) {
                $download_files = $page->getDownloadFiles();
            }
            header("X-XSS-Protection: 0");
            return view(
                'print',
                [
                    'CONTENTS'            => $sectionContents,
                    'PAGE'                => $page,
                    'DOWNLOAD_FILES'      => $download_files,
                    'DEFAULT_THUMBNAIL'   => DispatcherHelper::getBrandThumbnail($sectionId),
                    'PRINT_VIEW'          => 1
                    ]
            );
        }
        
        return Response::view($page->template, array_merge(['CONTENTS' => $sectionContents,'PAGE'=> $page], $pageContents))->header('X-XSS-Protection', '0');
    }


    /************************************************************************
	 *
	 *
	 ***********************************************************************/
    public function getPrintById($id)
    {
        $page = Page::alive()->haveLang()->find($id);
        return DispatcherHelper::getPrintView($page, 'print', Session::get('lang'));
    }
    /************************************************************************
	 *
	 *
	 ***********************************************************************/
    public function getPrintBySlug($slug)
    {
        $page = Page::alive()->haveLang()->where('slug', '=', $slug)->first();
        return DispatcherHelper::getPrintView($page, 'print', Session::get('lang'));
    }

    /************************************************************************
	 *
	 *
	 ***********************************************************************/
    public function getPdfPreviewById($u_id)
    {
        $page = Page::haveLang()->where('u_id', $u_id)->first();
        return DispatcherHelper::generatePDFResponse($page, true);
    }
    /************************************************************************
	 *
	 *
	 ***********************************************************************/
    public function getPdfPrintBySlug($slug)
    {
        $page = Page::alive()->haveLang()->where('slug', '=', $slug)->first();
        $isHTMLDebug = Request::get('isHTMLDebug', false);
        $isPreview = $isHTMLDebug;
        return DispatcherHelper::generatePDFResponse($page, $isPreview, $isHTMLDebug);
    }
    /************************************************************************
	 *
	 *
	 ***********************************************************************/
    public function getPdfPrintById($u_id)
    {
        $page = Page::alive()->haveLang()->where('u_id', $u_id)->first();

        return DispatcherHelper::generatePDFResponse($page);
    }
    /************************************************************************
	 * Page for external links
	 * @param $url 	the external url
	 * @return view
	 ***********************************************************************/
    function externalPage($url = false)
    {
        if (!$url) {
            $url = Request::get('url');
        }

        if (Session::get('lang') == 1) {
            $altLang = 2;
        } else {
            $altLang = 1;
        }
        
        // tags and JS Injection prevention!
        $url = htmlspecialchars(strip_tags(\Purifier::clean($url)));

        return view('external_link', [
                                        'PAGE'  => (object)['id'=>'external', 'template'=>'system','slug' => 'external','langs' => 3],
                                        'URL'   => $url,
                                        'ALT_LANG' => $altLang
                                    ]);
    }

    /************************************************************************
     * Get a view representing the filter download page menu
     * @return View
     ***********************************************************************/
    public function generateDownloadsFilter()
    {

        $data = FEFilterHelper::getFilterBlocks($this->lang);
        //var_dump($data);die;
        if (!$data['FILTERED_BLOCKS']) {
            if (!$data['IS_GALLERY']) {
                $data['ALLBLOCKSTITLE']   = trans('template.all_downloads');
            }
        }

        return view('partials.download_blocks', $data);
    }

    /************************************************************************
     * Get a view representing the filter best practice page menu
     * @return View
     ***********************************************************************/
    public function generateBestPracticeFilter()
    {
        $data = FEFilterHelper::getFilterBlocks($this->lang);

        if (!$data['FILTERED_BLOCKS']) {
            if (!$data['IS_GALLERY']) {
                $data['ALLBLOCKSTITLE']   = trans('template.all_best_practice');
            }
        }

        return view('partials.best_practice_blocks', $data);
    }

    /************************************************************************
     * Maintainance helper
     * @return
     ***********************************************************************/

    /*
     * This is used for delivery images in the backend edit view and Print
     */
    public function getFile($filemanager_mode, $file)
    {
        $full_path = null;
        try {
            if (strpos($file, '__cache/') !== false) {
                $full_path = FileHelper::generateCacheImage($filemanager_mode, $file);
            } else {
                $relative_dest_path = '/files/' . $filemanager_mode . '/' . $file;
                $full_path = public_path() . $relative_dest_path;
            }

            // header("Content-Type: application/zip");
            // header("Content-Length: " . @filesize($full_path));
            // header('Content-Disposition: attachment; filename="'. basename($full_path) .'"');
            FileHelper::sendFileHeaders($full_path);
            readfile($full_path);
        } catch (\Exception $exception) {
            Log::info("The file doesn't exist " . $full_path . ' error: ' . self::getLastErrorMsg());
            if (config('app.debug')) {
                Log::debug($exception);
            }
        }
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class BeAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // We are in BE set isFE to false
        Config::set('app.isFE', false);

        if (Auth::guest() || Auth::user()->role <= USER_ROLE_MEMBER) { //Guest and user->role (0) is Member should login as Admin or Approve to access BE
            $path = $request->fullUrl();
            Session::put("redirect_url", $path);
            return response()->view('backend/be_login');
        } elseif (!Session::get('isConfirmed')) {
            $path = $request->fullUrl();
            Session::put("redirect_url", $path);
            return response()->view('backend/be_confirm');
        }

        return $next($request);
    }
}

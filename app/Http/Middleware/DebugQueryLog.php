<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DebugQueryLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $bLogQueries = false;
        $bDebugQuery = config('app.debug') && ($request->has('__debugQuery') || $bLogQueries);

        if ($bDebugQuery) {
            DB::enableQueryLog();
        }

        $response = $next($request);

        if ($bDebugQuery) {
            $debugQuery = $request->get('__debugQuery');
            $queries = DB::getQueryLog();

            if (!empty($debugQuery)) {
                if ($debugQuery == "hidden") {
                    echo '<!-- ';
                    print_r($queries);
                    echo '-->';
                } else {
                    var_dump($queries);
                }
            }

            if ($bLogQueries) {
                Log::info('Debug Query for: "'. $request->fullUrl() .'" '."\n". print_r($queries, true));
            }
        }

        return $response;
    }
}

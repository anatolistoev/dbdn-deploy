<?php

namespace App\Http\Middleware;

class BeAccessNl extends BeAccess
{
    /**
     * Return List of Groups that is allowed.
     * 
     * @return array
     */
    protected static function getGroups(): array {
        return ['auth.CMS_NL_EDITOR'];
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class CheckPassOverload
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Session::has('passOverloadCheck')) {
            $server_load = $this->getServerLoad();
            if ($server_load['load'][0] > MAX_SERVER_LOAD && !$request->has('__SkipOverloadCheck')) {
                // Log server overload
                Log::critical("Redirect because of overload: load: " . print_r(implode(' ', $server_load['load']), 1) . ' memory: ' . print_r(implode(' ', $server_load['memory']), 1));

                // Redirect to Overload page
                return redirect('static/server_overload.html');
            } else {
                Session::put('passOverloadCheck', true);
            }
        }

        return $next($request);
    }

    private function getServerLoad()
    {
        if (function_exists("sys_getloadavg")) {
            $load = sys_getloadavg();
            $load[0] = round($load[0], 4);
        } else {
            $load = [2, 0, 0];
        }
        $memory = ['mem' => memory_get_peak_usage(), 'allMem' => memory_get_peak_usage(true)];

        return ['load' => $load, 'memory' => $memory];
    }
}

<?php

namespace App\Http\Middleware;

class BeAccessNlOrCms extends BeAccess
{
    /**
     * Return List of Groups that is allowed.
     * 
     * @return array
     */
    public static function getGroups(): array {
        return ['auth.CMS_EDITOR', 'auth.CMS_NL_EDITOR'];
    }

}

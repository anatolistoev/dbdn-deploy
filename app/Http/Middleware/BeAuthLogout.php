<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class BeAuthLogout
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guest() || Auth::user()->role <= USER_ROLE_MEMBER) { //Guest and user->role (0) is Member should login as Admin or Approver to access BE
            return response()->view('backend/be_login');
        }

        return $next($request);
    }
}

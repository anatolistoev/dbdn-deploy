<?php

namespace App\Http\Middleware;

use Closure;

class DebugEnvironment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check if user is Authorized
        if (config('app.debug') != true) {
            $error_message = trans('ws_general_controller.webservice.debug_disabled');
            if ($request->wantsJson()) {
                return response()->json(RestController::generateJsonResponse(true, $error_message), 401);
            } else {
                return response()->view('errors.fatal', ['PAGE'=> (object)['id' => 404, 'slug' => 'fatal']], 404);
            }
        }

        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthExpertLogout
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {               
        if (Auth::check() && Auth::user()->role == USER_ROLE_EXPORTER) {
            // TODO: This is not working for basic login!
            // http://stackoverflow.com/questions/18295994/logging-out-with-http-basic-auth-in-laravel
            return Auth::logout();
        }

        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //Anonimus
        '/gallery',
        '/print/gallery'
    ];

    /**
     * Determine if the application is running unit tests.
     *
     * @return bool
     */
    protected function runningUnitTests()
    {
        if (env('TEST_CSRF', false) && env('APP_ENV', 'local') == 'testing') {
            return false;
        } else {
            return parent::runningUnitTests();
        }
    }
}

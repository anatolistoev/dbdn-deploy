<?php

namespace App\Http\Middleware;

class BeAccessFaqs extends BeAccess
{
    /**
     * Return List of Groups that is allowed.
     * 
     * @return array
     */
    protected static function getGroups(): array {
        return ['auth.CMS_APPROVER'];
    }

    /**
     * Return List of Roles that is allowed.
     * 
     * @return array
     */
    public static function getRoles(): array {
        $roles = parent::getRoles();
        array_push($roles, USER_ROLE_EDITOR_SMART);
        return $roles;
    }
}

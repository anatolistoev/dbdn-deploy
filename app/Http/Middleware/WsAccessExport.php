<?php

namespace App\Http\Middleware;

use App\Http\Controllers\RestController;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class WsAccessExport
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check if user is Authorized for Export
        if (Auth::guest()
            || (!Session::get('isConfirmed') && Auth::user()->role != USER_ROLE_EXPORTER)
            || (in_array(Auth::user()->role, config('auth.CMS_EDITOR')) && !Session::get('isConfirmed'))
        ) {
            return response()->json(RestController::generateJsonResponse(true, trans('ws_general_controller.webservice.unauthorized')), 401);
        }

        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
///        $locales = Config::get('app.locales');
///        $time_locales = Config::get('app.time_locales');

        $locales = config('app.locales');
        $time_locales = config('app.time_locales');

/// If not stored in Session, current language will be the application default one
        if (Session::has('lang')) {
            $lang = $locales[Session::get('lang') - 1];
            App::setLocale($lang);
            call_user_func_array('setlocale', array_merge([LC_TIME], $time_locales[Session::get('lang') - 1]));
        } else {
            // Set Application default language
            $lang = App::getLocale();
            $key = array_search($lang, $locales);
            if ($key !== false && $key !== null) {
                Session::put('lang', $key + 1);
                call_user_func_array('setlocale', array_merge([LC_TIME], $time_locales[$key]));
            }
        }

        if ($request->has('lang')) {
            $lang = substr($request->get('lang'), 0, 2);
            $key = array_search($lang, config('app.locales'));
            if ($key !== false && $key !== null) {
                if (Session::get('lang') != $key + 1) {
                    Session::put('lang', $key + 1);
                    App::setLocale($lang);
                    $time_locales = config('app.time_locales');
                    call_user_func_array('setlocale', array_merge([LC_TIME], $time_locales[$key]));
                }
            }
        }

        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use App\Http\Controllers\RestController;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

abstract class BeAccess
{
    /**
     * Return List of Groups that is allowed.
     * 
     * @return array
     */
    abstract protected static function getGroups(): array;

    /**
     * Return List of Roles that is allowed.
     * 
     * @return array
     */
    public static function getRoles(): array {
        $roles = [];
        foreach (static::getGroups() as $group) {
            $roles = array_merge($roles, config($group));
        }

        $roles = array_unique($roles);
        return $roles;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {            
        // We are in BE set isFE to false
        Config::set('app.isFE', false);

        $roles = static::getRoles();

        // Check if user is Authorized
        if (Auth::guest() || !in_array(Auth::user()->role, $roles) || !Session::get('isConfirmed')) {
            if (Auth::guest()) {
                $error_message = trans('ws_general_controller.webservice.session_expired');
            } else {
                $error_message = trans('ws_general_controller.webservice.unauthorized');
            }
            if ($request->wantsJson()) {
                return response()->json(RestController::generateJsonResponse(true, $error_message), 401);
            } else {
                return response()->view('backend.be_unauthorized_access', [ 'error' => $error_message], 401);
            }
        }

        return $next($request);
    }
}

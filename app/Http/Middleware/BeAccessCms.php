<?php

namespace App\Http\Middleware;

class BeAccessCms extends BeAccess
{
    /**
     * Return List of Groups that is allowed.
     * 
     * @return array
     */
    public static function getGroups(): array {
        return ['auth.CMS_EDITOR'];
    }
}
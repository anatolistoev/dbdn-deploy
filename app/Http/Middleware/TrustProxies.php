<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Fideloper\Proxy\TrustProxies as Middleware;

class TrustProxies extends Middleware
{
    /**
     * The trusted proxies for this application.
     *
     * @var array
     */
    protected $proxies = [
        // load balancer for staging and production
        '141.113.16.240',
        '141.113.16.241',
        '141.113.16.242',
        '141.113.16.243',
        '141.113.16.244',
        '141.113.16.245',
        '141.113.16.246',
        '141.113.16.247',
        '141.113.16.248',
        '141.113.16.249',
        '141.113.16.250',
        '141.113.16.251',
        '141.113.16.252',
        '141.113.16.253',
        '141.113.16.254',
    ];

    /**
     * The headers that should be used to detect proxies.
     *
     * @var string
     */
    protected $headers = Request::HEADER_X_FORWARDED_FOR;
}

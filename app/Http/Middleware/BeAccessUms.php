<?php

namespace App\Http\Middleware;

class BeAccessUms extends BeAccess
{
    /**
     * Return List of Groups that is allowed.
     * 
     * @return array
     */
    public static function getGroups(): array {
        return ['auth.CMS_ADMIN'];
    }
}

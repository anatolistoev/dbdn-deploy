<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
//        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
        \App\Http\Middleware\SecureHeaders::class
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \App\Http\Middleware\SetLocale::class,
            \App\Http\Middleware\DebugQueryLog::class,
            \App\Http\Middleware\CheckPassOverload::class,
        ],

        'api' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \App\Http\Middleware\SetLocale::class,
            \App\Http\Middleware\DebugQueryLog::class,
            \App\Http\Middleware\CheckPassOverload::class,
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,

        'be_auth' => \App\Http\Middleware\BeAuth::class,
        'be_auth_logout' => \App\Http\Middleware\BeAuthLogout::class,
        'auth.expert.logout' => \App\Http\Middleware\AuthExpertLogout::class,
        'ws_access_export' => \App\Http\Middleware\WsAccessExport::class,
        'ws_error' => \App\Http\Middleware\WsError::class,
        'be_access_cms' => \App\Http\Middleware\BeAccessCms::class,
        'be_access_ums' => \App\Http\Middleware\BeAccessUms::class,
        'be_access_nl' => \App\Http\Middleware\BeAccessNl::class,
        'be_access_nl_or_cms' => \App\Http\Middleware\BeAccessNlOrCms::class,
        'be_access_approver' => \App\Http\Middleware\BeAccessApprover::class,
        'be_access_faqs' => \App\Http\Middleware\BeAccessFaqs::class,
        'instructSearchEnginesNotToIndex' => \App\Http\Middleware\NoIndexNoFollow::class,
        'debug_enviroment' => \App\Http\Middleware\DebugEnvironment::class,
        'test_enviroment' => \App\Http\Middleware\TestEnvironment::class

    ];
}

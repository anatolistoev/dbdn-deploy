<?php

namespace App\Helpers;

use App\Helpers\FEFilterHelper;
use App\Helpers\FEOverviewPageHelper;
use App\Helpers\FESQLHelper;
use App\Helpers\FETeaserHelper;
use App\Helpers\FileHelper;
use App\Helpers\HtmlHelper;
use App\Helpers\UserAccessHelper;
use App\Http\Controllers\FileController;
use App\Http\Controllers\RestController;
use App\Http\Controllers\GlossaryPageController;
use App\Http\Controllers\LoginController;
use App\Models\Content;
use App\Models\FileItem;
use App\Models\Page;
use App\Models\Term;
use App\Models\Faq;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DispatcherHelper
{

    /************************************************************************
	 * PDF FUNCTIONS
	 ***********************************************************************/

    private static function getGeneratedPDFPath($lang = null)
    {
        if ($lang == LANG_DE) {
            $path_lang = 'de';
        } elseif ($lang == LANG_EN) {
            $path_lang = 'en';
        }

        $base_path = config('app.PDF_path.generated') . $path_lang;
        if (!is_dir($base_path)) {
            mkdir($base_path, FILE_DEFAULT_ACCESS, true);
        }
        return $base_path;
    }

    private static function initPDFFromPage($page, $lang = null)
    {
        $pdf = App::make('dompdf.wrapper');
//        Log::info(print_r($pdf->getDomPDF()->getOptions(), 1));
        if (!$lang) {
            $lang = Session::get('lang');
        }
        $view = DispatcherHelper::getPrintView($page, 'pdf', $lang);

        if ($view instanceof \Illuminate\Http\JsonResponse) {
            return $view;
        }
        ini_set('max_execution_time', 300);
        //DEBUG: Save/Load to/from HTML file
        // $generatedViewPath = DispatcherHelper::getGeneratedPDFPath($lang) . '/' . $page->slug . '.html';
        // $contents = $view->render();
        // file_put_contents($generatedViewPath, $contents);

// DEBUG: Load from file!
//        $generatedViewPath = DispatcherHelper::getGeneratedPDFPath($lang) . '/' . $page->slug . '.html';
//        $pdf->loadFile($generatedViewPath);

        // $pdf->loadHTML(htmlspecialchars_decode($view));
        // file_put_contents('/home/www/logs/pdfgen.html', str_replace('&#039;',"'",htmlspecialchars_decode($view)));
        
        $pdf->loadHTML(str_replace('&#039;',"'",htmlspecialchars_decode($view)));
// $pdf->loadHTML('test');

        return $pdf;
    }

    public static function genereateAndCachePdfByLang($page, $lang, $force = true)
    {
        $before = microtime(true);

        $generatedPth = DispatcherHelper::getGeneratedPDFPath($lang) . '/' . $page->slug . '.pdf';

        if ($force || !file_exists($generatedPth)) {
            $pdf = DispatcherHelper::initPDFFromPage($page, $lang);
            if ($pdf instanceof \Illuminate\Http\JsonResponse) {
                $error = json_decode($pdf->getContent());
                throw new \Exception($error->message);
            }

            $pdf->save($generatedPth);
        }
        $after = microtime(true);
        return ($after-$before);
    }

    /************************************************************************
	 * Get a view representing the page in printer-friendly format for Print and PDF
	 * @param $page 	the page object
	 * @return View
	 ***********************************************************************/
    public static function getPrintView($page, $view, $lang = null, $isPDFHTML = false)
    {
        if (!$page) {
            //return Response::view('errors.missing', array(), 404);
            if (config('app.isFE')) {
                return Response::view('errors.fatal', ['PAGE'=> (object)['id' => 404, 'slug' => 'fatal']], 404);
            } else {
                return Response::json(RestController::generateJsonResponse(true, 'Page not found.'), 404);
            }
        }
        $brands = array_keys(config('app.pages'));
        array_unshift($brands, HOME);

        if ($page->overview == 1 || in_array($page->id, $brands)) {
            if (config('app.isFE')) {
                return Response::view('errors.fatal', ['PAGE'=> (object)['id' => 404, 'slug' => 'fatal']], 404);
            } else {
                return Response::json(RestController::generateJsonResponse(true, "Overview pages doesn't have pdfs"), 404);
            }
        }

        $pageContents = $page->contents()->where('lang', '=', $lang)->get();

        $sectionContents = ['banner'=>'', 'main'=>'', 'right'=>'', 'farRight'=>'', 'author'=>'']; #setting the default contents
        foreach ($pageContents as $row) {
            $sectionContents[$row->section] = $row->body;
        }

        #page requires authentication
        if (!Auth::check() && $page->authorization == 1) {
            if (config('app.isFE')) {
                            return view(
                                'login_required',
                                [
                                        'PAGE' => (object) ['id' => $page->id,
                                                                 'title' => $sectionContents['title'],
                                                                 'template' => 'print'
                                                                ],
                                    ]
                            );
            } else {
                return Response::json(RestController::generateJsonResponse(true, "You have to log in to see this page."), 404);
            }
        } elseif (!UserAccessHelper::authUserHasAccess($page)) {
            if (config('app.isFE')) {
                return view(
                    Auth::check() ? 'no_access' : 'login_required',
                    [
                                            'PAGE' => (object) ['id' => $page->id,
                                                                     'u_id' => $page->u_id,
                                                                     'title' => $sectionContents['title'],
                                                                     'template' => 'print'
                                                                    ],
                                        ]
                );
            } else {
                return Response::json(RestController::generateJsonResponse(true, "You don't have access for this page."), 404);
            }
        }

        $ascendants = Page::ascendants($page->id, $lang)->get();

        $sectionId = $page->parent_id == 1 ? $page->id : DispatcherHelper::getActiveL2($ascendants);

        if ($sectionId == -1) {
            if (config('app.isFE')) {
                return Response::view('errors.fatal', ['PAGE'=> (object)['id'=>404]], 404);
            } else {
                return Response::json(RestController::generateJsonResponse(true, "Wrong sectionID."), 400);
            }
        }

        $download_files = null;
        if (in_array($page->template, ['downloads'])) {
            $download_files = $page->getDownloadFiles();
        } else if ($view == 'pdf') {
            //TODO: Convert URLs of images if protected
//            $sectionContents['main'] = HtmlHelper::pregeneratePageImg($sectionContents['main'], $lang);
            $sectionContents['main'] = HtmlHelper::processPageContent($sectionContents['main'], $lang, \PAGE_MODE_PDF);
        } else if ($view == 'print') {
            //TODO: Convert URLs of images if protected
            $sectionContents['main'] = HtmlHelper::processPageContent($sectionContents['main'], $lang, \PAGE_MODE_PRINT);
        }

        $page->brand  = $sectionId;
        
        return view(
            $view,
            [
                                  'CONTENTS'            => $sectionContents,
                                  'PAGE'                => $page,
                                  'DOWNLOAD_FILES'      => $download_files,
                                  'DEFAULT_THUMBNAIL'   => DispatcherHelper::getBrandThumbnail($sectionId),
                                  'PRINT_VIEW'          => 1,
                                  'PDF_HTML'            => $isPDFHTML
                                  ]
        );
    }

     /*
	 * This is used for generate PDF response
	 */
    public static function generatePDFResponse($page, $isPreview = false, $isHTMLDebug = false)
    {
        if ($page) {
            $filePath = trans('template.currLocale').'/'.$page->slug.'.pdf';
            $generatedPth = config('app.PDF_path.generated').$filePath;
            // Return uploded or generated PDF for production
            if (!$isPreview) {
                $uploadedPath = config('app.PDF_path.uploaded').$filePath;
                $response = null;
                if (file_exists($uploadedPath)) {
                    // If it's the same year - use cached PDF since the copyright footer should be correct
                    //TODO: Check if $generatedPth is correct here!
                    if (date("Y")==date("Y", filectime($generatedPth))) {
                        $response = Response::download($uploadedPath, $page->slug.'_'.date('ymd_Hi').'.pdf');
                    }
                } else if (file_exists($generatedPth)) {
                    // If it's the same year - use cached PDF since the copyright footer should be correct
                    if (date("Y")==date("Y", filectime($generatedPth))) {
                        $response = Response::download($generatedPth, $page->slug.'_'.date('ymd_Hi').'.pdf');
                    }
                }
                if ($response) {
                    return $response;
                }
            }
            if ($isPreview) {
                if ($isHTMLDebug) {
                    $view = self::getPrintView($page, 'pdf', Session::get('lang'), true);
                    return $view;
                }

                $pdf = self::initPDFFromPage($page);
                if ($pdf instanceof \Illuminate\Http\JsonResponse) {
                    return $pdf;
                }
                return $pdf->download('Daimler_BDN_'.$page->slug.'_'.date('ymd_Hi').'.pdf');
            } else {
                if (!is_dir(config('app.PDF_path.generated').trans('template.currLocale'))) {
                    mkdir(config('app.PDF_path.generated').trans('template.currLocale'), FILE_DEFAULT_ACCESS, true);
                }
                $pdf = self::initPDFFromPage($page);
                if ($pdf instanceof \Illuminate\Http\JsonResponse) {
                    return $pdf;
                }

                $pdf->save($generatedPth);
                $response = Response::download($generatedPth, $page->slug.'_'.date('ymd_Hi').'.pdf');
                return $response;
            }
        } else {
            return Response::json(RestController::generateJsonResponse(true, 'Page not found.'), 404);
        }
    }

    /************************************************************************
	 * Help FUNCTIONS
	 ***********************************************************************/
    /*     * **********************************************************************
     * Get the active Level 2
     * @param $ascendants	array of Page objects
     * @return int or 1 (home ID)
     * ********************************************************************* */
    public static function getActiveL2($ascendants)
    {
        foreach ($ascendants as $ancestor) {
            if ($ancestor->parent_id == HOME) {
                return $ancestor->id;
            }
        }
        return -1;
    }

    public static function isDesignManuals($pageId)
    {
        return in_array($pageId, config('app.design_manuals_pages'));
    }

    /************************************************************************
     * Generate an indexed array of Page object, using parent_id as keys
     * @param $pages    (nested) array of objects to use as a source
     * @return array(parent_id => $page,...)
     ***********************************************************************/
    public static function getParentIndex($pages)
    {
        $arrIndex = [];
        foreach ($pages as $page) {
            $arrIndex[$page->parent_id][] = $page;
        }
        return $arrIndex;
    }

     /************************************************************************
	 * Given a collection of Page objects and the ID of the current page, generates an array with IDs, from 1 (home) to current ID
	 * @param $ascendants	array of Page objects
	 * @param $id 			(int) ID of the current page
	 * @return array(int[,int...])
	 ***********************************************************************/
    public static function getPathIDs($ascendants, $id)
    {
        $ids = [];
        foreach ($ascendants as $ancestor) {
            $ids[] = $ancestor->id;
        }
        $ids[] = $id;
        return $ids;
    }

        /************************************************************************
     * Generate an indexed array of Page object, using id as a key
     * @param $pages    an array of objects to use as a source
     * @return array(id => $page,...)
     ***********************************************************************/
    public static function getIdIndex($pages)
    {
        $arrIndex = [];
        foreach ($pages as $page) {
            //$arrIndex[$page->id] = $page;
            $arrIndex[$page->id] = (object)$page->getAttributes();
        }
        return $arrIndex;
    }

    /************************************************************************
	 * Given the path to brand thumbnail
	 * @param $sectionId 	(int) ID of the brand root page (section)
	 * @return string
	 ***********************************************************************/

    public static function getBrandThumbnail($sectionId)
    {
        $specialPages = config('app.pages');
        if (isset($specialPages[$sectionId])) {
            return $specialPages[$sectionId]->brandThumb;
        } else {
            return $specialPages[DAIMLER]->brandThumb;
        }
    }

    /************************************************************************
	 * CONTENT CREATE FUNCTIONS
	 ***********************************************************************/

     /************************************************************************
     * Generate a view from a page object
     * @param $page     Page model object or null/false
     * @return View containing the current page contents or custom 404 page
     ***********************************************************************/
    public static function getView($page, $lang)
    {
        if (!$page) {
            //return Response::view('errors.missing', array('PAGE'=> (object)array('id'=>404)), 404);
            return Response::view('errors.fatal', ['PAGE'=> (object)['id' => 404, 'slug' => 'fatal']], 404);
        }
            //abort(404, 'Page not found');

        if (Request::has('filter')) {
            // Set filter in session if parameter is set ( ?filter=viewed:desc )
            FEFilterHelper::addFilterToPage($page, $lang);
        }

        // #attempt to login
        if (!Auth::check() && Request::has('username')) {
            return LoginController::doLogin();
        }

        $pageContents = $page->contents()->where('lang', '=', $lang)->get();

        $sectionContents = ['banner'=>'', 'main'=>'', 'right'=>'', 'farRight'=>'', 'author'=>'', 'tablet_banner'=>'', 'mobile_banner'=>'']; #setting the default contents
        foreach ($pageContents as $row) {
            $sectionContents[$row->section] = $row->body;
        }
        // #page requires authentication
        if (!Auth::check() && $page->authorization == 1) {
            self::generateRedirectURL();
            return redirect('login/'.$page->slug)->withErrors(['success' =>trans('system.login_required.contents')]);
        // #user doesn't have access
        } elseif (!UserAccessHelper::authUserHasAccess($page)) {
            Session::put('captcha', strtolower(str_random(5)));
            if (Session::get('lang') == 1) {
                $altLang = 2;
            } else {
                $altLang = 1;
            }
            self::generateRedirectURL();
            if (Auth::check()) {
                return view(
                    'no_access',
                    [
                        'PAGE' => (object) ['id' => $page->id,
                                                 'u_id' => $page->u_id,
                                                 'title' => $sectionContents['title'],
                                                 'template' => 'system',
                                                 'slug' => $page->slug,
                                                 'langs' => 3
                                                ],
                        'OLD_rbChoice'  => Request::old('rbChoice'),
                        'OLD_email' => Request::old('email'),
                        'OLD_text_area' => strip_tags(Request::old('text_area')),
                        'OLD_contact_name'  => strip_tags(Request::old('contact_name')),
                        'OLD_contact_email' => strip_tags(Request::old('contact_email')),
                        'frmRA_success'     => Request::old('frmRA_success'),
                        'ALT_LANG' => $altLang
                    ]
                );
            } else {
                 return redirect('login/'.$page->slug)->withErrors(['success' =>trans('system.login_required.contents')]);
            }
        }

        self::updateVisits($page);
        // TODO check for deeper logic of increment (like page visits?)
        if (strpos(Request::header("referer"), 'search') !== false) {
            FESQLHelper::updateVisitsSearch($page);
        }

        $ascendants = Page::ascendants($page->id, $lang)->get();

        $page->brand  = $page->parent_id == HOME ? $page->id : self::getActiveL2($ascendants);
        $pageContents = self::addPageContentByTemplate($page, $lang, $sectionContents);
        $relatedTags = $page->getFagRelated(Session::get('lang'), $page->brand);
        Session::put('captcha', strtolower(str_random(5)));
        //print_r(array_merge(array('CONTENTS' => $sectionContents,'PAGE'=> $page),$pageContents));die;
        //print_r($page->comments);die;

        $view = view(
            $page->template,
            array_merge(
                ['CONTENTS' => $sectionContents, 'PAGE' => $page, 'RELATED_TAGS' => $relatedTags],
                $pageContents
            )
        );
        if ($page->overview == 1 || $page->template == 'brand') {
            $response = response($view, 200);
            $response->header("Cache-Control", "max-age=0, no-cache, no-store, must-revalidate");
            $response->header("Pragma", "no-cache");
            $response->header("Expires", "Wed, 11 Jan 1984 05:00:00 GMT");
            return $response;
        } else {
            return $view;
        }
    }

     /************************************************************************
     *
	 * Depend on template collect data for display page
     * @return content by reference
	 *
	 ***********************************************************************/
    public static function addPageContentByTemplate($page, $lang, &$sectionContents, $isPreview = false){
        switch ($page->template) {
            case 'home':
                $pageContents = self::getHomepageContent($page, $lang);
                $sectionContents['banner'] = HtmlHelper::generateBannerCarousel($sectionContents['banner'], $sectionContents['tablet_banner'], true, $sectionContents['mobile_banner']);
                break;
            case 'brand':
                $pageContents = self::getBrandContent($page, $lang);
                $sectionContents['banner'] = HtmlHelper::generateBannerCarousel($sectionContents['banner'], $sectionContents['tablet_banner']);
                break;
            default:
                $sectionContents['banner'] = HtmlHelper::pregenerateFullsizeBannerImg($sectionContents['banner'], $sectionContents['tablet_banner'], $lang);
                $pageContents = self::getDefaultContent($page, $sectionContents, $lang);
                break;
        }
        return $pageContents;
    }

    public static function getMostSomething($pageId, $mostWhat, $excludeIds = [], $limit = false){

        try {
            $lang =  Session::get('lang');

            // hack
            if (!count($excludeIds)) {
                $excludeIds[] = 0;
            }

            // defaults
            $imageSize = 'carousel';

            switch ($mostWhat) {
                case 'recent':
                    $limit = $limit ? $limit : 12;
                    break;

                case 'viewed':
                    $limit = $limit ? $limit : 3;
                    $limit = TABLET ? 4 : $limit;
                    $imageSize = 'overview';
                    break;

                case 'rated':
                    $limit = $limit ? $limit : 3;
                    $limit = TABLET ? 4 : $limit;
                    $imageSize = 'overview';
                    break;

                case 'downloaded':
                    $limit = $limit ? $limit : 3;
                    $limit = TABLET ? 4 : $limit;
                    $imageSize = 'overview';
                    break;

                case 'best_practice':
                default:
                    $limit = $limit ? $limit : 12;
            }


            $query = Page::alive()
                    ->mostSomething($pageId, $excludeIds, $mostWhat);

            if (MOBILE) {
                // probably carousel; disable it.
                if ($limit > 3) {
                    $limit = 2;
                }

                $inAccordion = $query->take($limit)->get();
                $imageSize = 'carousel';
            } elseif (TABLET) {
                $inAccordion = $query->take($limit)->get();
                $imageSize = 'carousel';
            } else {
                $inAccordion = $query->take($limit)->get();
                $imageSize = 'carousel';
            }

            return FETeaserHelper::getFirstImgInPage($inAccordion, $imageSize, $pageId, $lang);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            // if ($mostWhat=='best_practice') { echo('<pre>');var_dump($e->getMessage());echo('</pre>');die; }
            $inAccordion = [];
            return $inAccordion;
        }
    }

    /************************************************************************
     * Get title, 1st img & 1st span from the pages in the homepage front block
     * @return array
     ***********************************************************************/
    public static function getFrontblocks($lang, $criteria = null)
    {

        $inBlocks = FESQLHelper::getPagesByCriteria($criteria);

        $blocks = [];
        foreach ($inBlocks as $row) {
            $page = Page::with(['tags' => function ($query) {
                $query->where('lang', '=', Session::get('lang'));
            }])->find($row->u_id);

            $blocks[$page->id] = $blockpage = $page;
            $ascendants = Page::ascendants($page->id, $lang)->get();
            $sectionId = $page->parent_id == HOME ? $page->id : self::getActiveL2($ascendants);
            $defaultBrandThumbnail = self::getBrandThumbnail($sectionId);

            $page->brandId = $sectionId;
            $page->brandName = self::getBrandTitle($sectionId);

            if ($page->template == 'downloads') {
                $ids = [$page->id];
                $u_ids = [$page->u_id];
                $blocks[$page->id]['img'] =  $defaultBrandThumbnail;
                $blocks[$page->id]['file_type'] = 'downloads';
                $blocks[$page->id]['has_access'] = UserAccessHelper::authUserHasAccess($page);
                $founded = FETeaserHelper::generateDownloadOverviewTeasers($u_ids, $blocks);
                $ids = array_diff($ids, $founded);
                FETeaserHelper::addDownloadDetail($ids, $blocks, $defaultBrandThumbnail);
                if (count($ids)> 0) {
                    FETeaserHelper::generateDownloadExtensionImage($u_ids, $blocks, $defaultBrandThumbnail);
                }
                if ($page->overview && $page->tags->count() == 0) {
                    $sectionPages  = Page::descendants($sectionId, $lang, 0)->alive()->haveLang()->get();
                    $sectionPIndex = self::getParentIndex($sectionPages);
                    $child         = $sectionPIndex[$page->id];
                    $firstChild    = reset($child);
                    $tag           = (object)['name' => $firstChild->joinTags(1)];
                    $page->tags    = [0 => $tag];
                }
            } else {
                $blockpage['protected'] = ($page->authorization == 1);
                $blockpage['file_type'] = 'images';
            }
            $contents = $page->contents()->where('lang', '=', Session::get('lang'))->where(function ($query) {
                return $query->where('section', '=', 'title')
                      ->orWhere('section', '=', 'main');
            })->get();

            $content_main = null;
            foreach ($contents as $content){
                if($content->section == 'title'){
                    $content_title = $content->body;
                }elseif($content->section == 'main'){
                    $content_main = $content->body;
                }
            }
            $blockpage->content_title = HtmlHelper::fixDoubleDaggerForDaimler($content_title);
            $dom = null;
            if (!(isset($blockpage['img']) && $blockpage['img'] != '')) {
                $html = '';
                if ($content_main) {
                    $html = mb_convert_encoding($content->body, 'HTML-ENTITIES', 'UTF-8');
                }
                if(trim($html) != ""){
                    libxml_use_internal_errors(true);
                    $dom = new \DOMDocument();
                    $dom->substituteEntities = false;
                    $dom->recover = true;
                    $dom->strictErrorChecking = false;
                    $dom->loadHTML($html);
                }
            }

            FETeaserHelper::setThumbForPage($blockpage, $dom, $defaultBrandThumbnail, 'small_carousel', $page->overview);
        }
        unset($inBlocks);
        return $blocks;
    }

        /************************************************************************
	 * get pages for menu carousel with flag front_block
	 * @param $sectionId (int) ID of the brand root page (section)
     * @param $level level of page
     * @param $sectionPIndex array of all pages
     * @param $page current displayed pages
     * @param $ascendantIdIndex parents of current page
	 * @return array of pages
	 ***********************************************************************/
    public static function getMenuCarousel($level, $sectionPIndex, $sectionId, $page)
    {
        //Remove it when it's ready
        //return array();
        $blocks = [];
        if ($page && isset($sectionPIndex[$page->id]) && $level == 2) {
            $slugLower   = strtolower($page->slug);
            $isDownloads = false;
            if (strrpos($slugLower, 'downloads') !== false) {
                $isDownloads = true;
            }
            foreach ($sectionPIndex[$page->id] as $key => $child) {
                if ($child->featured_menu == 1) {
                    $serachedPageChidls[] = $child;
                }
                if ($child->overview == 1 && isset($sectionPIndex[$child->id])) {
                    foreach ($sectionPIndex[$child->id] as $key => $grandChild) {
                        if ($grandChild->featured_menu == 1) {
                            $serachedPageChidls[] = $grandChild;
                        }
                        if ($grandChild->overview == 1 && isset($sectionPIndex[$grandChild->id])) {
                            foreach ($sectionPIndex[$grandChild->id] as $key => $grandGrandChild) {
                                if ($grandGrandChild->featured_menu == 1) {
                                    $serachedPageChidls[] = $grandGrandChild;
                                }
                            }
                        }
                    }
                }
            }
            if (isset($serachedPageChidls) && count($serachedPageChidls) > 0) {
                $defaultBrandThumbnail = DispatcherHelper::getBrandThumbnail($sectionId);
                foreach ($serachedPageChidls as $key => $searchedPage) {
                    $u_ids[]                                = $searchedPage->u_id;
                    $idsToDig[]                             = $searchedPage->id;
                    $blocks[$searchedPage->id]['slug']      = $searchedPage->slug;
                    $blocks[$searchedPage->id]['title']     = $searchedPage->slug;
                    $blocks[$searchedPage->id]['img']       = $defaultBrandThumbnail;
                    $blocks[$searchedPage->id]['type']      = FileHelper::MODE_BRAND_THUMB;
                    $blocks[$searchedPage->id]['parent_id'] = $searchedPage->parent_id;
                    $blocks[$searchedPage->id]['u_id']      = $searchedPage->u_id;
                    $blocks[$searchedPage->id]['updated_at']  = $searchedPage->updated_at;
                    $blocks[$searchedPage->id]['protected'] = ($searchedPage->authorization == 1);
                    $blocks[$searchedPage->id]['has_access']  = UserAccessHelper::authUserHasAccess($searchedPage);
                }
            }
            if (count($blocks) > 0) {
                if ($isDownloads) {
                    $founded  = FETeaserHelper::generateDownloadOverviewTeasers($u_ids, $blocks);
                    $idsToDig = array_diff($idsToDig, $founded);
                    FETeaserHelper::addDownloadDetail($idsToDig, $blocks, $defaultBrandThumbnail);
                    if (count($idsToDig) > 0) {
                        FETeaserHelper::generateDownloadExtensionImage($u_ids, $blocks, $defaultBrandThumbnail);
                    }
                    $name     = trans('template.download_blocks');
                } else {
                    $founded  = FETeaserHelper::generateOverviewTeasers($u_ids, $blocks, $defaultBrandThumbnail);
                    $idsToDig = array_diff($idsToDig, $founded);
                    FETeaserHelper::addDescendantDetails($idsToDig, $blocks, $defaultBrandThumbnail);
                    //$name     = trans('template.design_manuals');
                }
                // If missing thumb set the brand tumb!
                foreach ($blocks as $key => $block) {
                    if ($block['img'] == '') {
                        $blocks[$key]['type'] = FileHelper::MODE_BRAND_THUMB;
                        $blocks[$key]['img']  = $defaultBrandThumbnail;
                    }
                }
            }
        }
        /* Sorting to newsest first*/
        usort($blocks, function ($a, $b) {
            $t1 = strtotime($a['updated_at']);
            $t2 = strtotime($b['updated_at']);
            return -($t1 - $t2);
        });
        return $blocks;
    }

    /************************************************************************
     * Generate prev and next page for bottom page buttons
     * @return references to next and prev page
     ***********************************************************************/
    public static function generatePrevNextPage($sectionPIndex, $page, &$prev_page, &$next_page) {
        $next_pos = 1;
        $prev_pos = -1;
        if (isset($sectionPIndex[$page->parent_id]) && is_array($sectionPIndex[$page->parent_id])) {
            foreach ($sectionPIndex[$page->parent_id] as $key => $level_page) {
                if ($level_page->id == $page->id) {
                    if ($prev_pos != -1) {
                        $prev_page = $sectionPIndex[$page->parent_id][$prev_pos];
                    }
                    if ($next_pos <  count($sectionPIndex[$page->parent_id])) {
                        $next_page = $sectionPIndex[$page->parent_id][$next_pos];
                    }

                    break;
                }
                $next_pos++;
                $prev_pos++;
            }
        }

    }

    /************************************************************************
     *
	 * Order all related pages(related, downlowd, best practise pages) to page
     * @param $relatedPages - related pages,
     * @param relatedDownloads - related downloads
     * @param relatedBP - related best practise
     * @return them by reference
	 *
	 ***********************************************************************/
    public static function orderAllRelatedPages($page, $sectionId, $lang, &$relatedPages, &$relatedDownloads, &$relatedBP) {
        $allRelated = [];
        if ($page->template != 'best_practice') {
            $allRelated = $page->getPageRelatedArray($lang, $sectionId);
        }
        //print_r($allRelated);die;
        # generate [id=>title, ...] array of:
        $position = [];
        $positionDownload = [];
        foreach ($allRelated as $relative) {
            if (!$relative->body) {
                continue; #skip if there's no title
            }
            ## separate related records to "pages" & "downloads" & 'best_practise'
            if ($relative->template == 'downloads') {
                if (is_null($relative->position)) {
                    $relatedDownloads[] = $relative;
                } else {
                    $positionDownload[] = $relative;
                }
            } else {
                if (is_null($relative->position)) {
                    $relatedPages[] = $relative;
                } else {
                    $position[] = $relative;
                }
            }
        }
        // related BP pages
        $allRelatedBP = [];
        if ($sectionId == DAIMLER && ($page->template == 'basic' || $page->template == 'best_practice' || $page->template == 'exposed')) {
            $allRelatedBP = $page->getBestPractiseRelated($lang, $sectionId);
            $allRelatedBP = FETeaserHelper::getFirstImgInPage($allRelatedBP, 'small_carousel', $sectionId, $lang);
        }
        $positionBP = [];
        foreach ($allRelatedBP as $relative) {
            if (!$relative->title) {
                continue;
            }
            if (is_null($relative->position)) {
                $relatedBP[] = $relative;
            } else {
                $positionBP[] = $relative;
            }
            $relative->updated_at = \Carbon\Carbon::createFromTimeStamp(strtotime($relative->updated_at));
        }
        $allRelated = null;
        $relatedPages = ContentUpdater::sortRelated($position, $relatedPages);
        $relatedDownloads = ContentUpdater::sortRelated($positionDownload, $relatedDownloads);
        $relatedBP = ContentUpdater::sortRelated($positionBP, $relatedBP);
        //Remove hidden to protect correct display in carousel
        $relatedBP = array_filter($relatedBP, function ($el) {
            return !$el->is_hidden;
        });
    }

    /************************************************************************
     * Update the number of visits
     * @param $page     Page model object or null/false
     * @return
     ***********************************************************************/
    public static function updateVisits($page)
    {
        if (Session::has('visited') && is_array(Session::get('visited')) && in_array($page->id, Session::get('visited'))) {
            return false;
        }
        $page->visits++;
        //$page->save();
        //Skeep hierarchy update
        FESQLHelper::updateVisits($page);

        Session::push('visited', $page->id);
    }

    /************************************************************************
     * Retrieve homepage contents
     * @return array
     ***********************************************************************/
    private static function getHomepageContent($page, $isPrint = false)
    {
        $lang = Session::get('lang');

        $altLang = (1 == $lang) ? 2 : 1;

        $thingsOfTheDay = self::getThingsOfTheDay($lang);

        return [   'SECTION_PID_INDEX' => [],
                        'L2ID'              => 0,
                        'HOMEBLOCKS'        => self::getMostSomething(DAIMLER, 'best_practice'),
                        'RECENT'            => self::getFrontblocks($lang),
                        'SEARCHED'          => self::getFrontblocks($lang, 'searched'),
                        'VIEWED'            => self::getFrontblocks($lang, 'viewed'),
                        'RATED'             => self::getFrontblocks($lang, 'rated'),
                        'DOWNLOADED'        => self::getFrontblocks($lang, 'downloaded'),
                        'ALT_LANG'          => $altLang,
                        'FAQ_OF_THE_DAY'    => $thingsOfTheDay['faq'],
                        'TERM_OF_THE_DAY'   => $thingsOfTheDay['term'],
                    ];
    }

    /**
     * [getThingsOfTheDay description]
     * @param  integer $lang [description]
     * @return array       [description]
     */
    private static function getThingsOfTheDay($lang)
    {

        $cacheKey = 'thingsOfTheDay'.$lang;
        $curDate = date('Ymd');

        if (Cache::has($cacheKey)) {
            $thingsOfTheDay = Cache::get($cacheKey);

            if ($curDate == $thingsOfTheDay['date']) {
                return $thingsOfTheDay;
            }
        } else {
            $thingsOfTheDay = [ 'faqIds' => [], 'termIds' => [] ];
        }

        $thingsOfTheDay['date'] = $curDate;

        $queryAddon = count($thingsOfTheDay['faqIds']) ? '`faq`.`id` not in ('.implode(',', $thingsOfTheDay['faqIds']).') and ' : '';
        $faqOfTheDay = Faq::faqoftheday($lang, $queryAddon)->first();
        $thingsOfTheDay['faq'] = ['q' => $faqOfTheDay->question, 'a' => $faqOfTheDay->answer];
        $thingsOfTheDay['faqIds'] = array_slice($thingsOfTheDay['faqIds'], 0, 9);
        array_unshift($thingsOfTheDay['faqIds'], $faqOfTheDay->id);

        $queryAddon = count($thingsOfTheDay['termIds']) ? '`term`.`id` not in ('.implode(',', $thingsOfTheDay['termIds']).') and ' : '';
        $termOfTheDay = Term::termoftheday($lang, $queryAddon)->first();
        $thingsOfTheDay['term'] = ['q' => $termOfTheDay->name, 'a' => $termOfTheDay->description];
        $thingsOfTheDay['termIds'] = array_slice($thingsOfTheDay['termIds'], 0, 9);
        array_unshift($thingsOfTheDay['termIds'], $termOfTheDay->id);

        Cache::forever($cacheKey, $thingsOfTheDay);

        return $thingsOfTheDay;
    }

        /************************************************************************
     * Retrieve brand homepage contents
     * @return array
     ***********************************************************************/

    private static function getBrandContent($page, $lang)
    {
        $ascendants = Page::ascendants($page->id, $lang)->get();
        $pathIDs = self::getPathIDs($ascendants, $page->id);
        $sectionId = $page->parent_id == HOME ? $page->id : self::getActiveL2($ascendants);
        if ($sectionId == -1) {
            return Response::view('errors.fatal', ['PAGE'=> (object)['id'=>404, 'slug' => 'fatal']], 404);
        }

        $sectionPages = Page::descendants($sectionId, $lang, 0)->alive()->haveLang()->get();
        $sectionPIndex = self::getParentIndex($sectionPages);
        if (isset($sectionPIndex[$sectionId])) {
            usort($sectionPIndex[$sectionId], function ($a, $b) {
                if ($a->position == $b->position) {
                    return 0;
                }
                return ($a->position > $b->position) ? +1 : -1;
            });
        }

        $menuSettings = ['colLimit' => 14, 'lineSize3' => 30, 'lineSize4' => 42];

        if ($lang == 1) {
            $altLang = 2;
        } else {
            $altLang = 1;
        }

        // $HOMEBLOCKS = $sectionId == DAIMLER ? $this->getBestPractise($page->id) : NULL;
        $HOMEBLOCKS = $sectionId == DAIMLER ? self::getMostSomething($page->id, 'best_practice') : null;

        $excludeIds = [];

        if (!is_null($HOMEBLOCKS)) {

            foreach ($HOMEBLOCKS as $pageBlock) {
                $excludeIds[] = $pageBlock->getAttribute('id');
            }
        }

        $data = [  'SECTION_PID_INDEX'     => $sectionPIndex,
                        'L2ID'                  => $sectionId,
                        'PATH_IDS'              => $pathIDs,
                        'SETTINGS'              => $menuSettings,
                        'HOMEBLOCKS'            => $HOMEBLOCKS,
                        'MOSTRECENTBLOCKS'      => self::getMostSomething($page->id, 'recent', $excludeIds),
                        'MOSTSOMETHING'         => [
                                                    // getMostSomething($pageId, $mostWhat, $excludeIds = array())
                                                    trans('template.interest.most_viewed')     => self::getMostSomething($page->id, 'viewed'),
                                                    trans('template.interest.most_rated')      => self::getMostSomething($page->id, 'rated'),
                                                    trans('template.interest.most_downloaded') => self::getMostSomething($page->id, 'downloaded'),
                                                ],
                        'ALT_LANG'              => $altLang,
                          ];

        if (MOBILE) {
            $data['BRAND_NAV'] = [];
            if (isset($sectionPIndex[$sectionId])) {
                foreach ($sectionPIndex[$sectionId] as $L2page) {
                    if ($L2page->in_navigation == 1) {
                        $data['BRAND_NAV'][$L2page->id] = self::getMobilePageMenu($L2page->id, $lang);
                    }
                }
            }
        }



        return $data;
    }

    /************************************************************************
     * Retrieve pages contents
     * @return array
     ***********************************************************************/

    private static function getDefaultContent($page, $sectionContents, $lang, $isPreview = false)
    {
        $ascendants = Page::ascendants($page->id, $lang)->get();
        $ascendantPIdIndex = self::getParentIndex($ascendants);
        $ascendantIdIndex = self::getIdIndex($ascendants);

        $pathIDs = self::getPathIDs($ascendants, $page->id);

        $sectionId = $page->parent_id == HOME ? $page->id : self::getActiveL2($ascendants);
        if ($sectionId == -1) {
            return Response::view('errors.fatal', ['PAGE'=> (object)['id'=>404, 'slug' => 'fatal']], 404);
        }

        if ($isPreview) {
            $descendantsQuery = Page::descendants($sectionId, $lang, 0)->haveLang();
        } else {
            $descendantsQuery = Page::descendants($sectionId, $lang, 0)->alive()->haveLang();
        }

        $sectionPages = $descendantsQuery->get();
        $sectionPIndex = self::getParentIndex($sectionPages);
        $relatedPages= [];
        $relatedDownloads = [];
        $relatedBP =[];

        self::orderAllRelatedPages($page, $sectionId, $lang, $relatedPages, $relatedDownloads, $relatedBP);

        if ($page->inline_glossary == 1) {
            //$terms = Term::where('lang', '=', App::getLocale())->orderBy('name')->get();
            $terms = Term::where('lang', Session::get('lang'))->orderBy('name')->get();
            $sectionContents['main'] = GlossaryPageController::embedGlossary($sectionContents['main'], $terms);
        } else {
            $terms = null;
        }
        $level                       = $page->parent_id != 0 ? (isset($ascendantIdIndex[1]) ? $ascendantIdIndex[1]->level : 0) : 0;
        $blocks                      = []; #overview blocks
        $blocks_count                = 0;
        $nav_tags                    = [];
        //if($page->overview != 'No'){
        $defaultBrandThumbnail =  self::getBrandThumbnail($sectionId);
        //Session::forget('download_filter_'.$sectionId);
        $GLOBALS['usage_filters']    = [];
        $GLOBALS['file_types']       = [];
        $isFilterSet = FEFilterHelper::isDownloadFilterSet($sectionId, $page->template);
        $filter_info = new \stdClass();
        if ($page->overview == 1) {
            if ($isFilterSet) {
                $dw_filters = Session::get(FEFilterHelper::getFilterKey($sectionId, $page->template));
                $blocks     = FEFilterHelper::filterOverviewBlocks($page, $dw_filters, $filter_info , $defaultBrandThumbnail, $lang);
            } else {
                $blocks = FEOverviewPageHelper::getOverview($page, $sectionPIndex, $sectionId, $nav_tags, $level);
            }
        }
        if ($page->template == 'best_practice' || $page->template == 'downloads') {
            //TODO: check if nav tags are showing only on preview page??
            $nav_tags = FEFilterHelper::getPageNavigationTagsFilters($page, $sectionPIndex, $ascendants, $sectionId, $nav_tags, $isFilterSet);
        }

        if ($isFilterSet) {
            $dw_filters      = Session::get(FEFilterHelper::getFilterKey($sectionId, $page->template));
            $filtered_blocks = true;
        } else {
            $dw_filters      = [];
            $filtered_blocks = false;
        }

        $subscribed = $page->subscriptions()->authuser()->get()->count();
        $rating = $page->ratings()->authuser()->get();
        $userRating = $rating->first() ? $rating->first()->vote : 0;
        $menuSettings = ['colLimit' => 14, 'lineSize3' => 30, 'lineSize4' => 42];
        $download_files = null;
        //TODO this maybe is not need to show file updateted at not the page?!?!
        if (in_array($page->template, ['downloads', 'smart']) && $page->overview == 0) {
            $download_files = $page->getDownloadFiles();
        }

        if (Session::get('lang') == 1) {
            $altLang = 2;
        } else {
            $altLang = 1;
        }
        $prev_page = null;
        $next_page = null;

        if ($page->overview != 1 && ($page->template == 'basic' || $page->template == 'best_practice' || $page->template == 'exposed')) {
            self::generatePrevNextPage($sectionPIndex, $page, $prev_page, $next_page);
            $sectionContents['main'] = HtmlHelper::wrapPageImages($page, $sectionContents['main']);
        }

        $returnData = [
                                'CONTENTS'              => $sectionContents,
                                'ASCENDANTS'            => $ascendants,
                                'ASCENDANT_PID_INDEX'   => $ascendantPIdIndex,
                                'ASCENDANT_ID_INDEX'    => $ascendantIdIndex,
                                'SECTION_PID_INDEX'     => $sectionPIndex,
                                'L2ID'                  => $sectionId,
                                'PATH_IDS'              => $pathIDs,
                                'SETTINGS'              => $menuSettings,
                                'LEVEL'                 => $level,
                                'RELATED_PAGES'         => $relatedPages,
                                'RELATED_DOWNLOADS'     => $relatedDownloads,
                                'RELATED_BP'            => $relatedBP,
                                'TERMS'                 => $terms,
                                'MESSAGES'              => isset($errors) ? $errors->all() : null,
                                'SUBSCRIBED'            => $subscribed,
                                'RATING'                => $userRating,
                                'BLOCKS'                => $blocks,
                                'BLOCKS_COUNT'          => $blocks_count,
                                'NAV_TAGS'              => $nav_tags,
                                'FILTERED_BLOCKS'       => $filtered_blocks,
                                'DW_FILTERS'            => $dw_filters,
                                'USAGE_FILTERS'         => $GLOBALS['usage_filters'],
                                'FILE_TYPES'            => $GLOBALS['file_types'],
                                'DOWNLOAD_FILES'        => $download_files,
                                'DEFAULT_THUMBNAIL'     => $defaultBrandThumbnail,
                                'ALT_LANG'              => $altLang,
                                'NEXT_PAGE'             => $next_page,
                                'PREV_PAGE'             => $prev_page,
                                'HOMEBLOCKS'            => isset($homeblocks) ? $homeblocks : null,
                        ];

        if (1 == $page->overview && $level >= 2) {
            FEOverviewPageHelper::generateMostBlocksInOverview($page, $filtered_blocks, $filter_info, $blocks, $returnData);
        }   // if(1 == $page->overview)

        if (MOBILE) {
            $returnData['BRAND_NAV'] = [];
            if (!empty($sectionPIndex[$sectionId])) {
                foreach ($sectionPIndex[$sectionId] as $L2page) {
                    if ($L2page->in_navigation == 1) {
                        $returnData['BRAND_NAV'][$L2page->id] = self::getMobilePageMenu($L2page->id, $lang);
                    }
                }
            }
        }
        // die('<pre>'.print_r($page,true));
        return $returnData;
    }

     /************************************************************************
     * Get a view representing the page menu in mobile view
     * @param $what is id of the page
     * @return View
     ***********************************************************************/
    private static function getMobilePageMenu($id, $lang)
    {
        if (!is_int($id)) {
            throw new \Exception("Unsupported menu request", 1);
        }

        $cacheKey = 'pageMenuMobile'.$id.'_'.$lang;

        //Cache::flush();

        if (false && Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $page = Page::alive()->haveLang()->where('id', $id)->first();
        $ascendants = Page::ascendants($page->id, $lang)->get();
        $ascendantIdIndex = DispatcherHelper::getIdIndex($ascendants);
        $sectionId = 1 === $page->parent_id ? $page->id : DispatcherHelper::getActiveL2($ascendants);
        $sectionPages = Page::descendants($sectionId, $lang, 0)->alive()->haveLang()->get();
        $sectionPIndex = DispatcherHelper::getParentIndex($sectionPages);
        $level = $page->parent_id != 0 ? (isset($ascendantIdIndex[1]) ? $ascendantIdIndex[1]->level : 0) : 0;
        $pathIDs = DispatcherHelper::getPathIDs($ascendants, $page->id);

        $data = [
                'PAGE'              => $page,
                'L2ID'              => $sectionId,
                'LEVEL'             => $level,
                'SECTION_PID_INDEX' => $sectionPIndex,
                'PATH_IDS'          => $pathIDs,
            ];


        Cache::put($cacheKey, $data, 30);

        return $data;
    }

    private static function getBrandTitle($sectionId)
    {
        $pages = config('app.pages');
        if (isset($pages[$sectionId])) {
            return $pages[$sectionId]->title;
        }
        return $pages[DAIMLER]->title;
    }

    private static function generateRedirectURL()
    {
        $prevUrl =  URL::previous();
        $serverUrl = url()->current();

        if (stripos($prevUrl, $serverUrl) !== false) {
            if (!Session::has('redirect_url')) {
                $redirect = str_replace($serverUrl.'/', '', $prevUrl);
                if (strlen($redirect) > 0) {
                     Session::put('redirect_url', $redirect);
                }
            }
        }
    }
}

<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Encryption\DecryptException;

class Encrypter
{
    protected static $_crypt = null;

    protected $key;
    protected $cipher = 'rijndael-256';
    protected $mode = 'cbc';
    protected $block = 32;

    public function __construct($key)
    {
        $this->key = $key;
    }

    public static function getCrypt()
    {
        if (empty(self::$_crypt)) {
            self::$_crypt = new Encrypter(Config::get('app.old_key'));
        }
        return self::$_crypt;
    }

    public static function encrypt($value)
    {
        return self::getCrypt()->_encrypt($value);
    }
    public static function decrypt($payload)
    {

        //WORKARAOUND: Load not encripted values!
        try {
            return self::getCrypt()->_decrypt($payload);
        } catch(\Exception $de) {
            Log::warning('Decription problem: ' . $de);
            if (config('app.debug') && config('app.testing')) {
                throw $de;
            } else {
                return $payload;
            }
        }

        /*
         * Additional lines of coded added by Tsvetan Dimitrov on 01.05.2019
         * */
        if(empty($payload)) {
            return "";
        }
        try {
            self::getCrypt()->_decrypt($payload);
        }
        catch(DecryptException $e) {
            return $payload;
        }

        return self::getCrypt()->_decrypt($payload);
    }

    public function _encrypt($value)
    {
        $iv = mcrypt_create_iv($this->getIvSize(), $this->getRandomizer());
        $value = base64_encode($this->padAndMcrypt($value, $iv));
        $mac = $this->hash($iv = base64_encode($iv), $value);
        return base64_encode(json_encode(compact('iv', 'value', 'mac')));
    }
    protected function padAndMcrypt($value, $iv)
    {
        $value = $this->addPadding(serialize($value));
        return mcrypt_encrypt($this->cipher, $this->key, $value, $this->mode, $iv);
    }
    public function _decrypt($payload)
    {
        $payload = $this->getJsonPayload($payload);
        $value = base64_decode($payload['value']);
        $iv = base64_decode($payload['iv']);
        return unserialize($this->stripPadding($this->mcryptDecrypt($value, $iv)));
    }
    protected function mcryptDecrypt($value, $iv)
    {
        $level = error_reporting();
        error_reporting($level & ~E_DEPRECATED);

        //return openssl_decrypt($value, $this->cipher, $this->key, OPENSSL_RAW_DATA, $iv);
        return mcrypt_decrypt($this->cipher, $this->key, $value, $this->mode, $iv);
        error_reporting($level);
    }
    protected function getJsonPayload($payload_coded)
    {
        // file_put_contents('/srv/www/dbdn-l55/storage/pdf/payloads.log', var_export(base64_decode($payload_coded),true),FILE_APPEND);
        $payload = json_decode(base64_decode($payload_coded), true);
        if (!$payload or $this->invalidPayload($payload)) {
            throw new DecryptException('Invalid data: '. $payload_coded);
        }
        if (!$this->validMac($payload)) {
            throw new DecryptException('MAC is invalid.');
        }
        return $payload;
    }
    protected function validMac(array $payload)
    {
        return $payload['mac'] === $this->hash($payload['iv'], $payload['value']);
    }
    protected function hash($iv, $value)
    {
        return hash_hmac('sha256', $iv . $value, $this->key);
    }
    protected function addPadding($value)
    {
        $pad = $this->block - strlen($value) % $this->block;
        return $value . str_repeat(chr($pad), $pad);
    }
    protected function stripPadding($value)
    {
        $pad = ord($value[($len = strlen($value)) - 1]);
        return $this->paddingIsValid($pad, $value) ? substr($value, 0, strlen($value) - $pad) : $value;
    }
    protected function paddingIsValid($pad, $value)
    {
        $beforePad = strlen($value) - $pad;
        return substr($value, $beforePad) == str_repeat(substr($value, -1), $pad);
    }
    protected function invalidPayload($data)
    {
        return !is_array($data) or !isset($data['iv']) or !isset($data['value']) or !isset($data['mac']);
    }
    protected function getIvSize()
    {
        $level = error_reporting();
        error_reporting($level & ~E_DEPRECATED);
        return mcrypt_get_iv_size($this->cipher, $this->mode);
        error_reporting($level);
    }
    protected function getRandomizer()
    {
        if (defined('MCRYPT_DEV_URANDOM')) {
            return MCRYPT_DEV_URANDOM;
        }
        if (defined('MCRYPT_DEV_RANDOM')) {
            return MCRYPT_DEV_RANDOM;
        }
        mt_srand();
        return MCRYPT_RAND;
    }
    public function setKey($key)
    {
        $this->key = $key;
    }
    public function setCipher($cipher)
    {
        $this->cipher = $cipher;
    }
    public function setMode($mode)
    {
        $this->mode = $mode;
    }
}

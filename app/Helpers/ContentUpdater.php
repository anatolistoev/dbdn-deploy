<?php

namespace App\Helpers;

use App\Http\Controllers\GalleryController;
use App\Http\Controllers\FileController;
use App\Models\Content;
use App\Models\FileItem;
use App\Models\Gallery;
use App\Models\Page;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ContentUpdater
{

    /**
     * Get all pages which are not deleted
     */
    public static function updateImgTags()
    {
        $pages = DB::table('page')->whereIn('page.template', ['basic', 'exposed', 'best_practice'])->where('overview', '=', 0)->whereNull('page.deleted_at')
                        ->join('content', 'content.page_id', '=', 'page.id')->where('content.section', '=', 'main')->select('page.id', 'content.id as content_id', 'content.section', 'content.body')->get();
        foreach ($pages as $page) {
            if (strlen($page->body) > 0) {
                ContentUpdater::generateNewtags($page);
            }
        }
    }

    public static function updateSrcsetTags()
    {
        $mainContents = Content::where('section', '=', 'main')->get();
        foreach ($mainContents as $content) {
//            $pattern = '/srcset\s*=\s*".*"/';
            $pattern = '/(srcset\s*=\s*"[^"]+")/';
            if (strlen($content->body) > 0) {
//            if($content->id == 17408){
//                $count = preg_match_all($pattern, $content->body, $out);
//                print_r("matches $count");
//                print_r($out);die;
                $body = preg_replace($pattern, "", $content->body);
//              print_r($body);die;
                if (strlen($body) < strlen($content->body)) {
                    $page = (object) ['id' => $content->page_id, 'content_id' => $content->id];
                    ContentUpdater::saveNewMain($page, $body);
                }
            }
        }
    }

    public static function updateSrcTags()
    {
        $contents = Content::where('body', 'LIKE', '%<img%.png.jpg%')->get();
        foreach ($contents as $content) {
            $body = str_replace('.png.jpg', '.png.png', $content->body);
            $page = (object) ['id' => $content->page_id, 'content_id' => $content->id];
            ContentUpdater::saveNewMain($page, $body);
        }
    }

    public static function sortRelated($position, $related)
    {
        usort($position, function ($a, $b) {
            if ($a->position == $b->position) {
                return 0;
            }
            return ($a->position > $b->position) ? +1 : -1;
        });
        foreach ($position as $pos) {
            array_splice($related, $pos->position, 0, [$pos]);
        }
        return $related;
    }

    /**
     * Fix src of img tags with cashed path and srcset by 3
     */
    private static function generateNewtags($page)
    {
        libxml_use_internal_errors(true);
        $dom = new \DOMDocument();
        $dom->substituteEntities = false;
        $dom->recover = true;
        $dom->strictErrorChecking = false;
        $html = mb_convert_encoding($page->body, 'HTML-ENTITIES', 'UTF-8');
        $dom->loadHTML($html);
        $imgs = $dom->getElementsByTagName('img');
        $haveToSave = false;
        if ($imgs->length > 0) {
            foreach ($imgs as $image) {
                $srcset = $image->getAttribute('srcset');
                if ($image->getAttribute('class') != 'anchor_nav' && empty($srcset)) {
                    $originalSrc = $image->getAttribute('src');
                    $src = $originalSrc;
                    if (empty($originalSrc) || strrpos($originalSrc, '/__cache/') === false) {
                        if (substr($originalSrc, 0, strlen('/')) === '/') {
                            $originalSrc = substr($originalSrc, 1);
                            Log::info('*ContentUpdater* Img path REMOVE SLASH page id: ' . $page->id . ' old path: ' . $src);
                            print_r('*ContentUpdater* Img path REMOVE SLASH page id: ' . $page->id . ' old path: ' . $src);
                            echo '</br>';
                        }
                        $src = UriHelper::generateImgPath($originalSrc, FileController::MODE_IMAGES, 'content');
                        $image->setAttribute('src', $src);
                    }
                    //$srcset = HtmlHelper::generateImgSrcSet($src, 'content', 3);
                    //$image->setAttribute('srcset',$srcset);
                    Log::info('*ContentUpdater* ADD SRCSET page id: ' . $page->id . ' srcset: ' . $srcset);
                    print_r('*ContentUpdater* ADD SRCSET page id: ' . $page->id . ' srcset: ' . $srcset);
                    echo '</br>';
                    $haveToSave = true;
                }
            }
        }
        if ($haveToSave) {
            $domBody = $dom->getElementsByTagName('body')->item(0);
            $body = $dom->saveHTML($domBody);
            $body = str_replace(['<body>', '</body>'], '', $body);
            ContentUpdater::saveNewMain($page, $body);
        }
    }

    private static function saveNewMain($page, $body)
    {
        DB::table('content')
                ->where('content.page_id', $page->id)
                ->where('content.id', $page->content_id)
                ->update(['body' => $body]);
        Log::info('*ContentUpdater* PAGE SAVED to database page id: ' . $page->id . " content id: " . $page->content_id);
        print_r('*ContentUpdater* PAGE SAVED to database page id: ' . $page->id . " content id: " . $page->content_id);
        echo '</br>';
    }


    /**
     * Fix relations page to file
     */
    public static function fixFileRelationsToPage()
    {
        $pages = Page::where('version', '>', 0)
                    ->join('content', 'content.page_id', '=', 'page.id')->whereIn('content.section', ['main', 'banner'])
                    ->select('page.u_id', 'page.version', 'content.id as content_id', 'content.lang', 'content.body', 'content.section')
                    ->orderBy('page.id')->orderBy('page.u_id')->orderBy('page.version')->get();

        $countProblems = 0;
        foreach ($pages as $page) {
            try {
                $relImages = ContentUpdater::getFileRelationsFromHTML($page->body);
            } catch (\Exception $ex) {
                $message = $ex->getMessage();
                print_r('<br> Page: ' . $page->u_id . ' Version: ' . $page->version . ' Lang: ' . $page->lang . ' Section: ' . $page->section . '<br> Problem: ' . $message);
                Log::error($ex);
                continue;
            }
            if (count($relImages)) {
                $images        = Gallery::where('page_id', '=', $page->u_id)->pluck('file_id')->toArray();
                $diffRelImages = array_diff($relImages, $images);
 //           print_r($diffRelImages);
//            break;
                if (count($diffRelImages)) {
            //DB::beginTransaction();
                    $fixedRelations = ContentUpdater::updatePageImageRelation($page, $diffRelImages);
            //DB::rollback();
            //DB::commit();
                    print_r('<br> Page: ' . $page->u_id . ' Version: ' . $page->version . ' Lang: ' . $page->lang . '<br> Images: ' . join(
                        ',',
                        $relImages
                    ));
                    print_r(' Diff: ' . join(',', $diffRelImages) . ' Fixed: ' . $fixedRelations . '<br>');
                    $countProblems++;

                    //break;
                }
            }
        }

        print_r('<br> Problems: ' . $countProblems);
    }

    public static function getFileRelationsFromHTML($pageBody)
    {
        libxml_use_internal_errors(true);
        $dom                      = new \DOMDocument();
        $dom->substituteEntities  = false;
        $dom->recover             = true;
        $dom->strictErrorChecking = false;
        $html                     = mb_convert_encoding($pageBody, 'HTML-ENTITIES', 'UTF-8');
        $dom->loadHTML($html);
        $imgs                     = $dom->getElementsByTagName('img');
        $arrRelatedImages         = [];
        if ($imgs->length > 0) {
            foreach ($imgs as $image) {
                if ($image->hasAttribute('data-gallery')) {
                    $galeryImg = explode(',', $image->getAttribute('data-gallery'));
                    if (count($galeryImg) > 0) {
                        $arrRelatedImages = array_merge($arrRelatedImages, $galeryImg);
                    }
                }
            }
            $arrRelatedImages = array_unique($arrRelatedImages);
        }

        return $arrRelatedImages;
    }

    public static function updatePageImageRelation($page, $diffRelImages)
    {
        $fixedRelations = 0;
        foreach ($diffRelImages as $file_id) {
            $file = FileItem::find($file_id);
            if (!is_null($file)) {
                $gallery          = new Gallery();
                $gallery->page_id = $page->u_id;
                $gallery->langs   = $page->lang;
                $gallery->file_id = $file_id;

                $response = GalleryController::trySave($gallery, true);
                if ($response->isSuccessful()) {
                    $fixedRelations++;
                } else {
                    print_r('Save Problem: ');
                    print_r($response);
                }
            } else {
                print_r('File image ' . $file_id . ' not found. <br>');
            }
        }

        return $fixedRelations;
    }

    /**
     * Generate resized thumbs for all assets.
     * Source images in folder: $srcDirPathAbs
     * Force generation of existing thumbs with $updateExisting = true
     */
    public static function generateTumbnailsForDownloads($updateExisting = false)
    {
        $filemanager_mode = FileController::MODE_DOWNLOADS;
//        $filemanager_mode = FileController::MODE_IMAGES;
        $sizeArr          = config('assets.'. $filemanager_mode . '.sizes');

        //Folder with source files for thumbs
        $srcDirPathAbs    = resource_path('assets/img/template/thumbnails');
        $destDirPathAbs   = public_path('img/template/thumbnails');

        // Check if all tumbs has images
        $specialPages = config('app.pages');
        foreach ($specialPages as $page) {
            $thumbPath = realpath($srcDirPathAbs . '/' . basename($page->brandThumb));
            if (!is_file($thumbPath)) {
                Log::warning("Missing Source Thumb file: " . basename($page->brandThumb));
            }
        } 

        $files            = @scandir($srcDirPathAbs);
        if ($files) {
            $cleanPath   = rtrim($srcDirPathAbs, '/') . '/';
            $excludeList = ['.', '..'];
            foreach ($files as $t) {
                if (!in_array($t, $excludeList)) {
                    $srcFilePathAbs = realpath($cleanPath . $t);
                    if (!is_file($srcFilePathAbs)) {
                        if (is_dir($srcFilePathAbs)) {
                            continue;
                        } else {
                            Log::warning("Missing Source Thumb file: " . $srcFilePathAbs);
                            return;
                        }
                    }

                    foreach ($sizeArr as $imageSize => $imgConf) {
                        if ($imageSize == 'fullsize') {
                            // We dont use fullsize for thumbs
                            continue;
                        }
                        if (empty($imgConf['prefix'])) {
                            Log::warning("Empty prefix for asset: " . $imageSize);
                            continue;
                        }
                        $file_path_cached = $imgConf['prefix'] . '/' . basename($srcFilePathAbs);
                        $filePathCached = $destDirPathAbs . $file_path_cached;

                        if (!is_file($filePathCached)) {
                            $dirPathCached = dirname($filePathCached);
                            FileHelper::makedirs($dirPathCached, FILE_DEFAULT_ACCESS);
                        } else {
                            if ($updateExisting) {
                                // Delate this file to generate new one
                                if (!unlink($filePathCached)) {
                                    Log::warning("Cannot remove cached Thumb file: " . $filePathCached);
                                }
                            } else {
                                // Skip this file
                                continue;
                            }
                        }

                        //Resize the thumb
                        FileHelper::resizeImage($srcFilePathAbs, $filePathCached, $imgConf, 2);
                    }
                }
            }
        } else {
            Log::warning("Missing Source Thumb files in : " . $srcDirPathAbs);
        }
    }
}

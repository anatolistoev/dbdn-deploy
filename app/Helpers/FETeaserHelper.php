<?php

namespace App\Helpers;

use App\Http\Controllers\FileController;
use App\Models\Content;
use App\Models\FileItem;
use App\Models\Page;
use Illuminate\Support\Facades\Auth;

/**
 * Description of FETeaserHelper
 *
 * @author Gergana
 */
class FETeaserHelper
{

    /************************************************************************
     *
     * Search for info for non download pages from childs
     *
     ***********************************************************************/
    public static function addDescendantDetails(&$ids, &$blocks, $defaultBrandThumbnail = '')
    {
        if (sizeof($ids) == 0) {
            return [];
        }
        $rows = FESQLHelper::getDescendantDetails($ids);

        if (!is_array($rows) || sizeof($rows) == 0) {
            return false;
        }

        $html = '<root>';
        foreach ($rows as $row) {
            $html.='<dncode id="'.$row->parent_id.'">'.$row->body.'</dncode>';
        }
        unset($rows, $row);
        $html.= '</root>';
        $founded =  HtmlHelper::seekInHTML($html, $blocks, false, true, $defaultBrandThumbnail);
        $ids = array_diff($ids, $founded);
    }

    /************************************************************************
     *
     * Search from downloads pages childs for teaser image
     *
     ***********************************************************************/
    public static function addDownloadDetail(&$ids, &$blocks, $defaultBrandThumbnail = '')
    {
        if (sizeof($ids) == 0) {
            return [];
        }
        $rows = FESQLHelper::getPagesChildFiles($ids);
        //print_r($rows);die;
        if (!is_array($rows) || sizeof($rows) == 0) {
            return false;
        }
        $fileItem = new FileItem;
        $founded = [];
        foreach ($rows as $row) {
            if ($blocks[$row->parent_id]['img'] == $defaultBrandThumbnail) {
                // Set default barnd thumbnail for protected files.
                //$blocks[$row->parent_id]['date'] = \Carbon\Carbon::createFromTimeStamp(strtotime($row->updated_at));
                if (!$row->protected || $row->thumb_visible || $blocks[$row->parent_id]['has_access']) {
                    $fileItem->id = $row->id;
                    $fileItem->protected = $row->protected;
                    $fileItem->filename = $row->filename;
                    $fileItem->thumbnail = $row->thumbnail;
                    $fileItem->type = $row->type;
                    if ($fileItem->hasThumbSourceFile()) {
                        if ($row->protected && $row->thumb_visible) {
                            $blocks[$row->parent_id]['thumb_visible'] = $row->thumb_visible;
                        }
                        $blocks[$row->parent_id]['img'] = $fileItem->getCachedThumbPath('{type}');
                        $blocks[$row->parent_id]['type'] = $row->type;
                        $blocks[$row->parent_id]['imgext'] = '.' . pathinfo($fileItem->getThumbPath(FILEITEM_REAL_PATH), PATHINFO_EXTENSION);
                        $founded[] = $row->parent_id;
                    }
                }
            }
        }
        $ids = array_diff($ids, $founded);
    }
    /************************************************************************
     *
     * Search from downloads pages for teaser svg if all images are missing
     *
     ***********************************************************************/
    public static function generateDownloadExtensionImage($u_ids, &$blocks, $defaultBrandThumbnail = '')
    {
        if (sizeof($u_ids) == 0) {
            return [];
        }

        $rows = FESQLHelper::getPagesFilesExtension($u_ids);

        if (!is_array($rows) || sizeof($rows) == 0) {
            return false;
        }

        foreach ($rows as $row) {
            $block = $blocks[$row->page_id];
            if ($block['img'] == $defaultBrandThumbnail) {
                $has_access = isset($block['has_access']) && $block['has_access'];
                $thumb_visible = isset($block['thumb_visible']) && $block['thumb_visible'];
                if ( !empty($block['img']) 
                    && (!$has_access && !$thumb_visible)) {
                    // Skip SVG for visible thumbs
                    continue;
                }
                $ext = explode(',', $row->extension);
                $ext_svg_file = '/img/template/svg/file_type/black/'.$ext[0].'.svg';
                if (is_file(public_path().$ext_svg_file)) {
                    $blocks[$row->page_id]['img'] = $ext_svg_file;
                }
            }
        }
    }
    /************************************************************************
     *
     * Generate info for library teasers
     *
     ***********************************************************************/
    public static function generateLibsBlocksInfo($idsLibs, &$blocks)
    {
        if (sizeof($idsLibs) == 0) {
            return [];
        }
        $rows = FESQLHelper::getLibraryData($idsLibs);

        foreach ($rows as $key => $row) {
            $blocks[$row->parent_id]['count'] = $row->count;
            $blocks[$row->parent_id]['size'] = $row->size;
            $blocks[$row->parent_id]['extension'] = FileHelper::formatExtension($row->ext);
            $blocks[$row->parent_id]['protected'] = $row->protected != 0 ? 1: 0;
        }
//        print_r($rows);die;
    }

    /************************************************************************
     *
     * Generate download pages info
     *
     ***********************************************************************/
    public static function generateDownloadOverviewTeasers(&$u_ids, &$blocks)
    {
        if (sizeof($u_ids) == 0) {
            return [];
        }

        $rows = FESQLHelper::getPagesFiles($u_ids);

        if (!is_array($rows) || sizeof($rows) == 0) {
            return [];
        }

        $fileItem = new FileItem;
        $founded = [];
        $founded_u_id = [];
        foreach ($rows as $row) {
            if ($row->title) {
                $blocks[$row->page_id]['title']   = $row->title;
            }
            if (!empty($row->updated_at) && !empty($row->type)) { // There is file for this page.
                // Set the modification date of File for last Modification
                $blocks[$row->page_id]['type']      = $row->type;
                $blocks[$row->page_id]['date']      = \Carbon\Carbon::createFromTimeStamp(strtotime($row->updated_at));
                $blocks[$row->page_id]['protected'] = $row->protected;
                $blocks[$row->page_id]['size']      = $row->size;
                $blocks[$row->page_id]['extension'] = FileHelper::formatExtension($row->extension);
                $blocks[$row->page_id]['file_id']   = $row->id; //file id
                if (!$row->protected || $row->thumb_visible || $blocks[$row->page_id]['has_access'])  {
                    $fileItem->id        = $row->id;
                    $fileItem->protected = $row->protected;
                    $fileItem->filename  = $row->filename;
                    $fileItem->thumbnail = $row->thumbnail;
                    $fileItem->type      = $row->type;
                    if ($fileItem->hasThumbSourceFile()) {
                        if ($row->protected && $row->thumb_visible) {
                            $blocks[$row->page_id]['thumb_visible'] = $row->thumb_visible;
                        }
                        $blocks[$row->page_id]['img']    = $fileItem->getCachedThumbPath('{type}');
                        $blocks[$row->page_id]['imgext'] = '.' . pathinfo($fileItem->getThumbPath(FILEITEM_REAL_PATH), PATHINFO_EXTENSION);
                        $founded[] = $row->page_id;
                        $founded_u_id[] = $row->page_u_id;
                    }
                }
            }
        }
        $u_ids = array_diff($u_ids, $founded_u_id);
        return $founded;
    }

    /************************************************************************
     *
     * Search for info for non download pages
     *
     ***********************************************************************/
    public static function generateOverviewTeasers($u_ids, &$blocks, $defaultBrandThumbnail = '')
    {
        if (count($u_ids) == 0) {
            return [];
        }
        $rows = Content::whereIn('page_u_id', $u_ids)
                                    ->currentLang()
                                    ->whereIn('section', ['title','main'])
                                    ->orderBy('page_id')
                                    ->orderBy('position')
                                    ->get();
        if ($rows->count() == 0) {
            return [];
        }
        $html = '<root>';
        foreach ($rows as $key => $row) {
            if ($row->section == 'title') {
                //$blocks[$row->page_id]['title'] = iconv('ISO-8859-1', 'UTF-8', $row->body);
                $blocks[$row->page_id]['title'] = $row->body;
            } else {
                $html.='<dncode id="'.$row->page_id.'">'.$row->body.'</dncode>';
            }
        }
        unset($rows, $row);
        $html.= '</root>';
        $founded = HtmlHelper::seekInHTML($html, $blocks, true, true, $defaultBrandThumbnail);
        return $founded;
    }

    /************************************************************************
     * Set in blockpage thumb for page
     * **********************************************************************/

    public static function setThumbForPage(&$blockpage, $dom, $defaultBrandThumbnail, $imageSize, $overview = false)
    {
        if (isset($blockpage['img']) && $blockpage['img'] != '') {
            $ext = pathinfo($blockpage['img'], PATHINFO_EXTENSION);
            if (strtolower($ext) == 'svg') {
                $blockpage['img'] = FileHelper::getSVGContent(public_path() . $blockpage['img']);
            } else {
                if (isset($blockpage['imgext'])) {
                    $imageType = $blockpage['imgext'];
                } else {
                    $imageType = FileHelper::getFileExtension($blockpage['img']);
                }
                if ($blockpage['img'] != $defaultBrandThumbnail) {
                    $blockpage['img'] = HtmlHelper::generateImg($blockpage['img'], $blockpage['file_type'], $imageSize, $imageType, isset($blockpage['thumb_visible']));
                } else {
                    //Image Sizes the same for 1x and 2x
                    $blockpage['img'] = HtmlHelper::generateImg($blockpage['img'], FileHelper::MODE_BRAND_THUMB, $imageSize, $imageType);
                }
            }
        } else {
            $hasThumb = false;
            if ($dom && !$overview) {
                $imgs = $dom->getElementsByTagName('img');
                if ($imgs->length > 0) {
                    foreach ($imgs as $image) {
                        if ($image->getAttribute('class') != 'anchor_nav') {
                            // Check for protected image
                            $galeryImg = explode(',', $image->getAttribute('data-gallery'));
                            $fileItem = null;

                            if (is_numeric($galeryImg[0])) {
                                $fileItem = FileItem::where('id', $galeryImg[0])->first();
                            }
                            if ($fileItem && $fileItem->protected) {
                                $blockpage['img'] = HtmlHelper::generateImg(
                                    $fileItem->getCachedThumbPath('{type}'), 
                                    $blockpage['file_type'], 
                                    $imageSize, 
                                    null, 
                                    true
                                );
                            } else {
                                $imgSource = UriHelper::getMasterFilePath($image->getAttribute('src'));
                                $imageType = FileHelper::getFileExtension($imgSource);
                                $blockpage['img'] = HtmlHelper::generateImg($imgSource, $blockpage['file_type'], $imageSize, $imageType);
                            }

                            $hasThumb = true;
                            break;
                        }
                    }
                }
            }
            if (!$hasThumb) {
                //Image Sizes the same for 1x and 2x
                $imageType = FileHelper::getFileExtension($defaultBrandThumbnail);
                $blockpage['img'] = HtmlHelper::generateImg($defaultBrandThumbnail, FileHelper::MODE_BRAND_THUMB, $imageSize, $imageType);
            }
        }
        unset($dom, $imgs);
    }

    /************************************************************************
     *
     * Add info to pages for the image and download file
     *
     ***********************************************************************/
    public static function getFirstImgInPage($inAccordion, $imageSize, $pageId, $lang)
    {
        foreach ($inAccordion as $key => $page) {
            $haveThumb = false;
            // Set lock image for pages with autorization.
            $page->protected = ($page->authorization == 1);

            if ($page->template == 'downloads') {
                $page->has_access = UserAccessHelper::authUserHasAccess($page);
                $download_files = $page->getDownloadFiles();
                if ($download_files && $download_files->count()) {
                    $fileItem = $download_files->first()->file;
                    $page->file_id = $fileItem->id;
                    $page->is_file_added_cart = Auth::check() && isset($fileItem->carts()->where('active', 1)->whereNull('deleted_at')->where('user_id', Auth::user()->id)->first()->file_id);

                    // Set default barnd thumbnail for protected files.
                    $page->protected = $fileItem->protected;
                    $page->size      = $fileItem->size;
                    $page->extension = FileHelper::formatExtension($fileItem->extension);
                    if (!$fileItem->protected || $fileItem->thumb_visible || $page->has_access) {
                        if ($fileItem->hasThumbSourceFile()) {
                            if ($fileItem->protected && $fileItem->thumb_visible) {
                                $page->thumb_visible = $fileItem->thumb_visible;
                            }
                            $imgSource = $fileItem->getCachedThumbPath('{type}');
                            $imageType = FileHelper::getFileExtension($fileItem->getThumbPath(FILEITEM_REAL_PATH));
                            $page->img = HtmlHelper::generateImg($imgSource, FileHelper::MODE_BRAND_THUMB, $imageSize, $imageType, $fileItem->protected);
                            $haveThumb = true;
                        } else {
                            $ext = explode(',', $fileItem->extension);
                            $ext_svg_file = public_path().'/img/template/svg/file_type/black/'.$ext[0].'.svg';
                            if (is_file($ext_svg_file)) {
                                $page->img = FileHelper::getSVGContent($ext_svg_file);
                                $haveThumb = true;
                            }
                        }
                    }
                }
            } elseif (strlen($page->main) > 0) {
                if (!isset($dom)) {
                    libxml_use_internal_errors(true);
                    $dom                      = new \DOMDocument();
                    $dom->substituteEntities  = false;
                    $dom->recover             = true;
                    $dom->strictErrorChecking = false;
                }
                $page->main               = mb_convert_encoding($page->main, 'HTML-ENTITIES', 'UTF-8');
                $dom->loadHTML($page->main);
                $imgs                     = $dom->getElementsByTagName('img');
                if ($imgs->length > 0) {
                    foreach ($imgs as $image) {
                        if ($image->getAttribute('class') != 'anchor_nav') {
                            // Check for protected image
                            $galeryImg = explode(',', $image->getAttribute('data-gallery'));
                            $fileItem = null;

                            if (is_numeric($galeryImg[0])) {
                                $fileItem = FileItem::where('id', $galeryImg[0])->first();
                            }
                            if ($fileItem && $fileItem->protected) {
                                $page->img = HtmlHelper::generateImg(
                                    $fileItem->getCachedThumbPath('{type}'),
                                    FileController::MODE_IMAGES,
                                    $imageSize,
                                    null,
                                    true
                                );
                            } else {
                                $imgSource = UriHelper::getMasterFilePath($image->getAttribute('src'));
                                $imageType = FileHelper::getFileExtension($imgSource);
                                $page->img = HtmlHelper::generateImg(
                                    $imgSource,
                                    FileController::MODE_IMAGES,
                                    $imageSize,
                                    $imageType
                                );
                            }
                
                            $haveThumb = true;
                            break;
                        }
                    }
                }
            }

            if (!$haveThumb) {
                $page->brand_id  = $page->parent_id == 1 ? $page->id : DispatcherHelper::getActiveL2(Page::ascendants($page->id, $lang)->get());

                $thumb     = DispatcherHelper::getBrandThumbnail($page->brand_id); // $pageId
                $imageType = FileHelper::getFileExtension($thumb);
                $page->img = HtmlHelper::generateImg(
                    $thumb,
                    FileController::MODE_IMAGES,
                    $imageSize,
                    $imageType
                );
            }
        }

        unset($dom, $imgs);
        return $inAccordion;
    }
}
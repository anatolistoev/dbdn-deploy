<?php

namespace App\Helpers;

use App\Http\Controllers\FileController;
use App\Http\Middleware\BeAccessFaqs;
use App\Http\Middleware\BeAccessNlOrCms;
use App\Http\Middleware\BeAccessUms;
use App\Models\FileItem;
use App\Models\Page;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


/**
 * Helper class to controll access to a view and its elements
 *
 * @author m.stefanov
 */
class UserAccessHelper
{

    public static $__viewAccessList;
    static function hasAccess($view_name)
    {
  // Check for access of role to view
        $hasAccessToView = false;
        if (!Auth::guest()) {
            $viewAccessList = UserAccessHelper::getViewAccessList();
            if (!empty($viewAccessList[$view_name])) {
                $rolesList   = $viewAccessList[$view_name];
                $role        = Auth::user()->role;
                $hasAccessToView = in_array($role, $rolesList);
            }
        }
        return $hasAccessToView;
    }

    /**
     * Rerturn folder of role for the user
     * 
     * @return string
     */
    public static function getUserRootFolder()
    {
        if (Auth::guest()) {
            //FIXME: What is the folder of Guest!
            return '';
        } else {
            $role           = Auth::user()->role;
            $configAccess   = config('auth.ACCESS');
            $root_folder    = $configAccess[$role]['folder'];
            return $root_folder;
        }
    }

    static function getUserRootPageID($role = null)
    {
  // Rerturn page id of role for the user
        //FIXME: What is the page id of Guest!
        if (is_null($role)) {
            $role      = Auth::user()->role;
        }
        $configAccess  = config('auth.ACCESS');
        $root_page     = $configAccess[$role]['page'];
        return $root_page;
    }

    static function hasAccessForPageAndAscendants($page_id, Collection $ascendants = null, $role = null)
    {
        $user_root_page_id = UserAccessHelper::getUserRootPageID($role);
        if ($user_root_page_id == $page_id || $user_root_page_id == "all") {
            return true;
        } else {
            if (is_null($ascendants)) {
                $ascendants = Page::ascendants($page_id, 0)->get();
            }
            if ($ascendants->isEmpty()) {
                return false;
            }
            $matchedAscendant = $ascendants->first(function ($asc, $key) use ($user_root_page_id) {
                return $asc['id'] == $user_root_page_id;
            }, null);

            return ($matchedAscendant !== null);
        }
    }

    static function getSpecialPagesByType($type)
    {
        $result = [];
        $specialPages = config('app.pages');
        foreach ($specialPages as $key => $value) {
            if ($value->type == $type) {
                $result[] = $value->ID;
            }
        }
        return $result;
    }

    static function getSpecialPage($const)
    {
        $specialPages = config('app.pages');
        return $specialPages[$const];
    }

    static function hasAccessAction($action_name)
    {
        if ($action_name == PERMANENT_DELETE) {
            if (Auth::guest() || !in_array(Auth::user()->role, config('auth.CMS_APPROVER')) || !Session::get('isConfirmed')) {
                return false;
            }
            return true;
        }
    }

//    static function getFaqsCategoryByLang($lang){
//        $result = array();
//        $specialPages = config('app.category');
//        foreach ($specialPages as $key => $value) {
//			if ($value->lang == $lang){
//				$result[] = $value;
//			}
//        }
//        return $result;
//    }

    static function getFaqsCategoryById($id)
    {
        $specialPages = config('app.parameters.faq_category');
//        foreach ($specialPages as $key => $value) {
//			if ($value->ID == $id){
//				$result = $value->name;
//			}
//        }
        return trans('ws_general_controller.faq_category_'.$specialPages[$id]);
    }

    static function getPageConfiguration($modul, $value)
    {
        $array = [];
        switch ($modul) {
            case 'usage':
                $array = config('app.page_usage');
                break;
            case 'file_type':
                $array = config('app.page_file_type');
                break;
            case 'file_format':
                $array = config('app.parameters.file_format');
                break;
            case 'file_dimension':
                $array = config('app.parameters.file_dimension');
                break;
            case 'file_resolution':
                $array = config('app.parameters.file_resolution');
                break;
            case 'file_color_mode':
                $array =  config('app.parameters.file_color_mode');
                break;
        }
        return $array[$value];
    }
    
    public static function isAdmin() {
        if (Auth::user()->role == USER_ROLE_ADMIN) {
            return true;
        } else {
            return false;
        }
    }

    /**
     ************************************************************************
	 * Check the access chain:
	 * page -> pagegroup -> group -> usergroup -> user
	 *
	 * @param $page 	Current page object
	 * @return bool
     ************************************************************************
     */
    public static function authUserHasAccess(Page $page): bool
    {
        $pageGroups = $page->groups();
//        $queries = DB::getQueryLog();
//$last_query = end($queries);
//            print_r($last_query);die;
//print_r($pageGroups->pluck('group_id'));die;
        if ($pageGroups->count() == 0) {
            return true;
        }
        if (!Auth::check()) {
            return false;
        }
        if (Auth::user()->role > USER_ROLE_MEMBER) {
            return true;
        }
        $userGroups = Auth::user()->groups();
        $colUserGroupsIds = $userGroups->pluck('group_id');
        $colCommonGroups = $colUserGroupsIds->intersect($pageGroups->pluck('group_id'));
        return  $colCommonGroups->count() > 0;
    }

    /**
     ************************************************************************
	 * Check the write for page
	 * page -> workflow -> editing_state
	 *
	 * @param $page 	Current page object
	 * @return bool
     ***********************************************************************
     */
    public static function hasWritePage(Page $page): bool
    {
        return self::hasWritePageOrContent($page, false);
    }

    /**
     ************************************************************************
	 * Check the write for page and content
	 * page -> workflow -> editing_state
	 *
	 * @param  Page $page 	         Current page object
     * @param  bool $hasWriteContent Check for Content od the page
	 * @return bool
     ***********************************************************************
     */
    public static function hasWritePageOrContent(Page $page, bool $isForContent): bool
    {
        if (!is_null($page) && Auth::check() && Session::get('isConfirmed', false)) {
            if ($page->workflow->editing_state == STATE_APPROVED
                && in_array(Auth::user()->role, config('auth.CMS_APPROVER'))
            ) {
                return !$isForContent;
            } else if ($page->workflow->editing_state == STATE_REVIEW
                && in_array(Auth::user()->role, config('auth.CMS_APPROVER'))
            ) {
                return true;
            } else if ($page->workflow->editing_state == STATE_DRAFT 
                && in_array(Auth::user()->role, config('auth.CMS_EDITOR'))
            ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if user has access to root of the file
     * 
	 * @param $file 	FileItem object
	 * @return bool
     */
    public static function userHasAccessToFileRoot(FileItem $file)
    {
        $userRoot = UserAccessHelper::getUserRootFolder();
        return $userRoot == "all" 
                || strpos(FileHelper::safe_dirname($file->filename) . "/", "/" . $userRoot . "/") !== false ;
    }

    /************************************************************************
	 * Check the access chain:
	 * file -> page -> pagegroup -> group -> usergroup -> user
	 *
	 * @param $file 	FileItem object
	 * @return bool
	 ************************************************************************/
    public static function authUserHasAccessToFile(FileItem $file)
    {
        $noAccessToFile = !UserAccessHelper::userHasAccessToFileRoot($file);

        if ($noAccessToFile || Auth::user()->role <= USER_ROLE_MEMBER) {
            $filemanager_mode = strtolower($file->type);
            if ($filemanager_mode === FileController::MODE_DOWNLOADS) {
                $allowedPagesQuery = Page::alive()
                    ->join('download', 'page.u_id', '=', 'download.page_u_id')
                    ->whereNull('download.deleted_at')
                    ->where('download.file_id', '=', $file->id);
                
                if ($file->protected) {
                    $allowedPagesQuery = $allowedPagesQuery
                        ->join('pagegroup', 'page.id', '=', 'pagegroup.page_id')
                        ->join('usergroup', 'usergroup.group_id', '=', 'pagegroup.group_id')
                        ->whereNull('pagegroup.deleted_at')
                        ->whereNull('usergroup.deleted_at')                
                        ->where('usergroup.user_id', '=', Auth::user()->id);
                }
            } else {
                //TODO: GALLERY has connection to page_u_id but column name isnt'changed
                $allowedPagesQuery = //Page::alive()
                Page::join('gallery', 'page.u_id', '=', 'gallery.page_id')
                    ->whereNull('gallery.deleted_at')
                    ->where('gallery.file_id', '=', $file->id);
                
                //TODO: Check for protected pages!
            }

            $allowedPages = $allowedPagesQuery
                ->where('page.deleted', '=', 0)
                ->groupBy('page.u_id')
                ->count();
            if (!$allowedPages || ($allowedPages == 0)) {
                return false;
            }
        }
        return true;
    }

    public static function getViewAccessList()
    {
        if (empty(UserAccessHelper::$__viewAccessList)) {
            UserAccessHelper::$__viewAccessList = [
                'workflow'   => BeAccessNlOrCms::getRoles(),
                'pages'      => config('auth.CMS_EDITOR'),
                'downloads'  => config('auth.CMS_EDITOR'),
                'images'     => BeAccessNlOrCms::getRoles(),
                'faqs'       => BeAccessFaqs::getRoles(),
                'glossary'   => config('auth.CMS_APPROVER'),
                'users'      => BeAccessUms::getRoles(),
                'groups'     => BeAccessUms::getRoles(),
                'tags'       => config('auth.CMS_APPROVER'),
                'newsletter' => config('auth.CMS_NL_EDITOR'),
            ];
        }
        return UserAccessHelper::$__viewAccessList;
    }
        
}

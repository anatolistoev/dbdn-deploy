<?php
namespace App\Helpers;

use App\Helpers\FEFilterHelper;
use App\Helpers\DispatcherHelper;
use App\Helpers\UserAccessHelper;
use App\Models\Content;
use App\Models\FileItem;
use App\Models\Page;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class FEOverviewPageHelper {

    /************************************************************************
     *
	 * Generate blocks in low level overview page
     * @return data by reference
     ************************************************************************/
    public static function generateMostBlocksInOverview($page, $filtered_blocks, $filter_info, $blocks, &$returnData) {
            
            if (DispatcherHelper::isDesignManuals($page->id)) {
                $returnData['ALLBLOCKSTITLE']   = trans('template.all_design_manuals');
//                $returnData['MOSTRECENTTITLE']  = trans('template.latest_design_manuals');

                $returnData['MOSTRECENTBLOCKS'] = DispatcherHelper::getMostSomething($page->id, 'recent', [], 2);
                $returnData['MOSTSOMETHING']    = [
                                                        trans('template.interest.most_viewed')     => DispatcherHelper::getMostSomething($page->id, 'viewed'),
                                                        trans('template.interest.most_rated')      => DispatcherHelper::getMostSomething($page->id, 'rated'),
                                                    ];
           // } elseif ('best_practice' == $page->template) {
            } elseif ('best_practice' === $page->template && 'Folder' === $page->type) {
                if ($filtered_blocks) {
                    $returnData['ALLBLOCKSTITLE']   = FEFilterHelper::getFiltredResultTitle($filter_info, $blocks);
                } else {
                    $returnData['ALLBLOCKSTITLE']   = trans('template.all_best_practice');
                }

                $returnData['MOSTRECENTBLOCKS'] =  DispatcherHelper::getMostSomething($page->id, 'recent', [], 2);
                $returnData['MOSTSOMETHING']    = [
                                                        trans('template.interest.most_viewed')     => DispatcherHelper::getMostSomething($page->id, 'viewed'),
                                                        trans('template.interest.most_rated')      => DispatcherHelper::getMostSomething($page->id, 'rated'),
                                                    ];
            } elseif ('best_practice' === $page->template && $returnData['LEVEL']==3 && count($blocks)>1) {
                if ($filtered_blocks) {
                    $returnData['ALLBLOCKSTITLE']   = FEFilterHelper::getFiltredResultTitle($filter_info, $blocks);
                } else {
                    $content_title = $page->contents()->where('lang', '=', Session::get('lang'))->where('section', '=', 'title')->first();
                    $title_count=0;
                    foreach($blocks as $block) {
                        $title_count+=count($block);
                    }
                    $returnData['ALLBLOCKSTITLE']   = $content_title->body." [$title_count]";
                }

            } elseif ('downloads' === $page->template && 'Folder' === $page->type) {
                if ($filtered_blocks) {
                    $returnData['ALLBLOCKSTITLE']   = FEFilterHelper::getFiltredResultTitle($filter_info, $blocks);
                } else {
                    $returnData['ALLBLOCKSTITLE']   = trans('template.all_downloads');
                }

                $returnData['MOSTRECENTBLOCKS'] = DispatcherHelper::getMostSomething($page->id, 'recent', [], 2);
                $returnData['MOSTSOMETHING']    = [
                                                        trans('template.interest.most_downloaded') => DispatcherHelper::getMostSomething($page->id, 'downloaded'),
                                                    ];
            } elseif ('downloads' === $page->template && $returnData['LEVEL']==3 && count($blocks)>1) {
                if ($filtered_blocks) {
                    $returnData['ALLBLOCKSTITLE']   = FEFilterHelper::getFiltredResultTitle($filter_info, $blocks);
                } else {
                    $content_title = $page->contents()->where('lang', '=', Session::get('lang'))->where('section', '=', 'title')->first();
                    $title_count=0;
                    foreach($blocks as $block) {
                        $title_count+=count($block);
                    }
                    $returnData['ALLBLOCKSTITLE']   = $content_title->body." [$title_count]";
                }

                /*$returnData['MOSTRECENTBLOCKS'] = $this->getMostSomething($page->id, 'recent', [], 2);
                $returnData['MOSTSOMETHING']    = [
                                                        trans('template.interest.most_downloaded') => $this->getMostSomething($page->id, 'downloaded'),
                                                    ];*/
            }

    }
        /************************************************************************
     *
     *
     ***********************************************************************/
    public static function getOverview($page, $sectionPIndex, $sectionId, &$nav_tags, $level)
    {

        $blocks             = [];
        $u_ids              = [];
        $idsToDig           = [];
        $idsLibs            = [];
        $tags               = [];

        if (!isset($sectionPIndex[$page->id])) {
            return $blocks;
        }

        $children = $sectionPIndex[$page->id];

        if (count($children) < 1) {
            return $blocks;
        }
        $defaultBrandThumbnail = DispatcherHelper::getBrandThumbnail($sectionId);
        foreach ($children as $child) {
            if ($child->overview == 1) {
                if (isset($sectionPIndex[$child->id])) {
                    $grandchildren = $sectionPIndex[$child->id];

                    foreach ($grandchildren as $grandchild) {
                        $idsToDig[] = $grandchild->id;
                        $u_ids[]    = $grandchild->u_id;

                        $tmpPage    = self::generateOverviewPageData($grandchild, $defaultBrandThumbnail);

                        if ($page->template == 'best_practice' || $page->template == 'downloads') {
                            if ($level == 2) {
                                $tmpPage['parent_slug'] = $child->slug;
                            }

                            $tmpPage['has_access'] = UserAccessHelper::authUserHasAccess($grandchild);

                            if ($grandchild->overview) {
                                $tmpPage['overview'] = true;
                                $idsLibs[]           = $grandchild->id;

                                if (isset($sectionPIndex[$grandchild->id])) {
                                    $greatGrandchilds = $sectionPIndex[$grandchild->id];
                                    $firstChild       = reset($greatGrandchilds);
                                    $tmpPage['tags']  = $firstChild->joinTags(1);

                                    foreach ($greatGrandchilds as $key => $greatGrandchild) {
                                        $tags[] = $greatGrandchild->joinTags(1);
                                    }
                                }
                            }
                        }

                        $tags[]                  = $tmpPage['tags'];
                        $blocks[$grandchild->id] = $tmpPage;
                    } // foreach($grandchildren as $grandchild)
                } // if(isset($sectionPIndex[$child->id]))
            } // if($child->overview == 1)
            else {
                $u_ids[]            = $child->u_id;
                $idsToDig[]         = $child->id;

                $tmpPage            = self::generateOverviewPageData($child, $defaultBrandThumbnail);

                if ($page->template == 'downloads') {
                    $tmpPage['has_access'] = UserAccessHelper::authUserHasAccess($child);
                }

                $tags[]             = $tmpPage['tags'];
                $blocks[$child->id] = $tmpPage;
            }
        } //foreach($children as $child)

        $nav_tags = FEFilterHelper::generateNavigationTags($tags);

        if ($page->template == 'downloads') {
            if (count($idsLibs)> 0) {
                FETeaserHelper::generateLibsBlocksInfo($idsLibs, $blocks);
            }

            $founded = FETeaserHelper::generateDownloadOverviewTeasers($u_ids, $blocks);
            $idsToDig = array_diff($idsToDig, $founded);
            FETeaserHelper::addDownloadDetail($idsToDig, $blocks, $defaultBrandThumbnail);

            if (count($idsToDig) > 0) {
                FETeaserHelper::generateDownloadExtensionImage($u_ids, $blocks, $defaultBrandThumbnail);
            }
        } else {
            $founded = FETeaserHelper::generateOverviewTeasers($u_ids, $blocks, $defaultBrandThumbnail);
            $idsToDig = array_diff($idsToDig, $founded);
            FETeaserHelper::addDescendantDetails($idsToDig, $blocks, $defaultBrandThumbnail);
        }

        self::groupOverviewBlocks($blocks, $page);
        return $blocks;
    }

    public static function generateOverviewPageData(Page $page, $defaultBrandThumbnail)
    {
        $tmpPage              = [];
        $tmpPage['slug']      = $page->slug;
        $tmpPage['title']     = $page->slug;
        $tmpPage['h1']        = '';
        $tmpPage['text']      = '';
        $tmpPage['img']       = $defaultBrandThumbnail;
        $tmpPage['type']      = FileHelper::MODE_BRAND_THUMB;
        $tmpPage['parent_id'] = $page->parent_id;
        $tmpPage['tags']      = $page->joinTags(1);
        $tmpPage['date']      = $page->updated_at;
        $tmpPage['visits']    = $page->visits;
        $tmpPage['comments']  = $page->activeComments()->count();
        $tmpPage['ratings']   = $page->ratings()->count();
        $tmpPage['protected'] = ($page->authorization == 1);

        if ($page->template == 'downloads') {
            $tmpPage['DOWNLOAD_FILES'] = $page->getDownloadFiles();

            if ($tmpPage['DOWNLOAD_FILES'] && $tmpPage['DOWNLOAD_FILES']->count()) {
                $file = $tmpPage['DOWNLOAD_FILES']->first()->file;

                if ($file) {
                    $tmpPage['size']      = $file->size;
                    $tmpPage['extension'] = FileHelper::formatExtension($file->extension);
                    $tmpPage['protected'] = $file->protected;
                } else {
                    Log::warning("Missing download file for page:" . $page->u_id . "generateOverviewPageData($page) overview=0");
                }
            }
        }

        return $tmpPage;
    }

    /************************************************************************
     *
	 * Generate pages blocks for overview
	 *
	 ***********************************************************************/
    private static function groupOverviewBlocks(&$blocks, $page)
    {

        $blocks_old  = $blocks;
        $blocks      = [];
        $parent_info = [];
        $child_count = [];

        foreach ($blocks_old as $block) {
                $parent_id = $block['parent_id'];
                $parent_info[$parent_id] = '';

            if ($page->template == 'downloads' || $page->template == 'best_practice') {
                if (isset($child_count[$parent_id])) {
                    $child_count[$parent_id] += isset($block['count']) ? $block['count'] : 1;
                } else {
                    $child_count[$parent_id] = isset($block['count']) ? $block['count'] : 1;
                }
            }
        }

        if (count($parent_info) > 0) {
            $rows = Content::join('page', 'page.u_id', '=', 'content.page_u_id')
                    ->whereIn('page.id', array_keys($parent_info))
                    ->where('page.active', '=', 1)
                    ->currentLang()
                    ->where('section', '=', 'title')
                    ->get();

            foreach ($rows as $key => $row) {
                $parent_info[$row->page_id] = $row->body;

                if ($page->template == 'downloads' || $page->template == 'best_practice') {
                    $parent_info[$row->page_id] .= ' ['.$child_count[$row->page_id].']';
                }
            }

            foreach ($blocks_old as $block) {
                $blocks[$parent_info[$block['parent_id']]][] = $block;
            }
        }
    }
}

<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class FESQLHelper {

    /************************************************************************
     *
	 * Get first file in array of pages
	 *
	 ***********************************************************************/
    public static function getPagesFiles($u_ids){
        $rows = DB::select(
            "SELECT c.page_id, c.page_u_id, c.body as title,
                                   f.id, f.filename, f.thumbnail, f.protected, f.type, f.updated_at, f.thumb_visible, f.size, f.extension
                    FROM content c
					LEFT JOIN `page` p ON c.page_u_id = p.u_id AND p.deleted_at IS NULL
					LEFT JOIN `download` d ON d.page_u_id = c.page_u_id AND d.deleted_at IS NULL
					LEFT JOIN `file` f ON d.file_id = f.id AND f.deleted_at IS NULL
					WHERE c.lang = ?
					AND c.section IN ('title')
					AND c.page_u_id IN (".implode(',', $u_ids).")
					AND c.deleted_at IS NULL
                    AND (
						d.position IS NULL OR
						d.position = ( SELECT MIN(d2.position) FROM download d2 WHERE d2.page_u_id = c.page_u_id AND d2.deleted_at IS NULL)
						)
					GROUP BY c.page_id
					ORDER BY p.position, c.page_id ASC",
            [Session::get('lang')]
        );
        return $rows;
    }

    /************************************************************************
     *
	 * Get first child page file in array of pages
	 *
	 ***********************************************************************/
    public static function getPagesChildFiles($ids){

        $rows = DB::select("SELECT h.page_id, h.parent_id, f.id, f.filename, f.thumbnail, f.protected, f.thumb_visible, f.type, f.updated_at FROM hierarchy h
							LEFT JOIN `page` p ON h.page_id = p.id AND p.deleted_at IS NULL
							LEFT JOIN `download` d ON d.page_u_id = p.u_id AND d.deleted_at IS NULL
							LEFT JOIN `file` f ON d.file_id = f.id AND f.deleted_at IS NULL
							WHERE h.parent_id IN (".implode(',', $ids).")
								AND h.deleted_at IS NULL
								AND p.active = 1
								AND (
									p.publish_date = '".DATE_TIME_ZERO."'
									OR p.publish_date < '".date('Y-m-d H:i:s')."'
								)
								AND (
									p.unpublish_date = '".DATE_TIME_ZERO."'
									OR p.unpublish_date > '".date('Y-m-d H:i:s')."'
								)
								AND f.thumbnail IS NOT NULL
								AND (
									d.position IS NULL OR
									d.position = ( SELECT MIN(d2.position) FROM download d2 WHERE d2.page_u_id = d.page_u_id AND d2.deleted_at IS NULL)
									)
								AND p.position = ( SELECT MIN(page.position) FROM page WHERE page.parent_id = p.parent_id AND page.active = 1 AND page.deleted_at IS NULL)
							GROUP BY h.parent_id");
        return $rows;
    }


    /************************************************************************
     *
	 * Get file extension on page in array of pages
	 *
	 ***********************************************************************/
    public static function getPagesFilesExtension($u_ids){
        $rows = DB::select(
            "SELECT c.page_id, f.extension FROM content c
					LEFT JOIN `page` p ON c.page_u_id = p.u_id AND p.deleted_at IS NULL
					LEFT JOIN `download` d ON d.page_u_id = c.page_u_id AND d.deleted_at IS NULL
					LEFT JOIN `file` f ON d.file_id = f.id AND f.deleted_at IS NULL
					WHERE c.lang = ?
					AND c.section = 'title'
					AND c.page_u_id IN (".implode(',', $u_ids).")
					AND c.deleted_at IS NULL
                    AND (
						d.position IS NULL OR
						d.position = ( SELECT MIN(d2.position) FROM download d2 WHERE d2.page_u_id = c.page_u_id AND d2.deleted_at IS NULL)
						)
					GROUP BY c.page_id, c.section
					ORDER BY p.position, c.page_id ASC",
            [Session::get('lang')]
        );
        return $rows;
    }
    /************************************************************************
     *
	 * Get information for labraries pages
	 *
	 ***********************************************************************/
  public static function getLibraryData($idsLibs){
      $rows = DB::table('hierarchy as h')
                ->leftJoin('page as p', 'p.id', '=', 'h.page_id')
                ->leftJoin('download as d', 'd.page_u_id', '=', 'p.u_id')
                ->leftJoin('file as f', 'd.file_id', '=', 'f.id')
                ->whereIn('h.parent_id', $idsLibs)
                ->whereNull('p.deleted_at')
                ->whereNull('d.deleted_at')
                ->whereNull('f.deleted_at')
                ->whereNull('h.deleted_at')
                ->whereNotNull('f.thumbnail')
                ->where('p.active', 1)
                ->whereRaw('(p.publish_date = "'.DATE_TIME_ZERO.'" OR p.publish_date < "'.date('Y-m-d H:i:s').'")
								AND (p.unpublish_date = "'.DATE_TIME_ZERO.'" OR p.unpublish_date > "'.date('Y-m-d H:i:s').'")')
                ->whereRaw('(d.position IS NULL
                    OR d.position = ( SELECT MIN(d2.position) FROM download d2 WHERE d2.page_u_id = d.page_u_id AND d2.deleted_at IS NULL))')
                ->select(DB::raw("SUM(f.size) as size, GROUP_CONCAT(f.extension) as ext, Count(p.parent_id) as count, SUM(f.protected) as protected, p.parent_id"))
                ->groupBy('p.parent_id')
                ->get();
      return $rows;
  }

    /************************************************************************
     *
	 * Get pages descendants details for array of pages
	 *
	 ***********************************************************************/
  public static function getDescendantDetails($ids){
   $rows = DB::select(
            "SELECT h.parent_id, pr.id, c.section, c.body
							FROM hierarchy h
							LEFT JOIN `page` pr
								ON pr.id = h.page_id
									AND pr.overview = 0
									AND pr.active = 1
									AND (
										pr.publish_date = '".DATE_TIME_ZERO."'
										OR pr.publish_date < '".date('Y-m-d H:i:s')."'
									)
									AND (
										pr.unpublish_date = '".DATE_TIME_ZERO."'
										OR pr.unpublish_date > '".date('Y-m-d H:i:s')."'
									)
									AND pr.deleted_at IS NULL
							LEFT JOIN content c
								ON pr.u_id = c.page_u_id
								AND section = 'main' AND c.lang = ?
								AND c.deleted_at IS NULL
							WHERE h.parent_id IN (".implode(',', $ids).")
								AND h.deleted_at IS NULL
								AND h.level > 0
								AND pr.id IS NOT NULL
							GROUP BY h.parent_id, c.section",
            [Session::get('lang')]
                   //AND pr.in_navigation = 1
        );
   return $rows;
  }

/************************************************************************
*
* Get pages file details for array of pages
*
***********************************************************************/
  public static function getPagesFileData($ids) {
      $rows = DB::table('download')
                ->join('fileinfo', 'fileinfo.file_id', '=', 'download.file_id')
                ->whereIn('download.page_u_id', $ids)
                ->whereNull('download.deleted_at')
                ->whereNull('fileinfo.deleted_at')
                ->where('fileinfo.lang', '=', Session::get('lang'))
                ->orderBy('download.page_u_id')
                ->orderBy('download.position')
                ->groupBy('download.page_u_id')
                ->select('download.page_u_id', 'download.page_id as id', 'fileinfo.file_id', 'fileinfo.description', 'fileinfo.updated_at')
                ->get();
      return $rows;
  }

/************************************************************************
*
* Get pages by search criteria
*
***********************************************************************/
  public static function getPagesByCriteria($criteria) {
        $lang = Session::get('lang');
        //TODO: Remove all deleted objects from select! Now it is not all!

        $query = DB::table(DB::raw('page as p'))
                    ->leftJoin(DB::raw('content c'), function ($join) use ($lang) {
                        $join->on('c.page_u_id', '=', 'p.u_id');
                    })
                    ->leftJoin(DB::raw('hierarchy h'), function ($join) {
                        $join->on('h.page_id', '=', 'p.id');
                    })
                    ->leftJoin(DB::raw('hierarchy h1'), function ($join) {
                        $join->on('h1.page_id', '=', 'h.page_id');
                        $join->on('h1.level', '=', 'h.level');
                    })
                    ->leftJoin(DB::raw('page p1'), function ($join) use ($lang) {
                        $join->on('h1.parent_id', '=', 'p1.id');
                    })
                    ->leftJoin(DB::raw('content c1'), function ($join) use ($lang) {
                        $join->on('p1.u_id', '=', 'c1.page_u_id');
                    })
                    ->where('c.lang', '=', $lang)
                    ->where('h.parent_id', '>', 1)
                    ->where('p1.active', '=', 1)
                    ->where('p1.deleted', '=', 0)
                    ->where('c1.lang', '=', $lang)
                    ->where('c1.section', '=', 'title')
                    ->whereNull('c.deleted_at')
                    ->whereNull('h.deleted_at')
                    ->whereNull('h1.deleted_at')
                    ->whereNull('c1.deleted_at');

        if ($criteria == 'rated') {
            $query = $query->join('rating', 'rating.page_id', '=', 'p.id');
        }

        $query = $query->where('p.active', '=', 1)
                       ->where('p.deleted', '=', 0)
                       ->whereNull('p.deleted_at')
                       ->whereRaw("(p.publish_date = '".DATE_TIME_ZERO."' OR p.publish_date < '".date('Y-m-d H:i:s')."')
                                AND (p.unpublish_date = '".DATE_TIME_ZERO."' OR p.unpublish_date > '".date('Y-m-d H:i:s')."')")
                        ->where('p.in_navigation', '=', 1)
                        ->whereRaw('p.langs >> (?-1) & 1 != 0', [$lang])
                        ->where('p.id', '<>', 1)
                        ->where('p.template', '<>', 'brand')
                        ->whereRaw('((SELECT Count(*)
                                FROM hierarchy hAlive
                                LEFT JOIN page pAlive ON hAlive.parent_id = pAlive.id
                                WHERE hAlive.page_id = p.id AND pAlive.active = 1 AND pAlive.id <> p.id
                                AND ( pAlive.publish_date = "'.DATE_TIME_ZERO.'" OR pAlive.publish_date < "'.date('Y-m-d H:i:s').'")
                                AND ( pAlive.unpublish_date = "'.DATE_TIME_ZERO.'"  OR pAlive.unpublish_date > "'.date('Y-m-d H:i:s').'"))
                                =
                                (Select hierarchy.level FROM hierarchy where hierarchy.page_id = p.id AND hierarchy.parent_id = 1))')
                        ->whereIn('c.section', ['title', 'main']);

        if ($criteria) {
            $query = $query->where('p.overview', '<>', 1);
        }

        switch ($criteria) {
            case 'searched':
                break;
            case 'downloaded':
                $query = $query->where('p.template', '=', 'downloads')->where('p.overview', '=', 0);
                break;
            case 'viewed':
                break;
            case 'rated':
                break;
            default:
                $query = $query->where('p.front_block', '=', 1);
        }

        switch ($criteria) {
            case 'searched':
                $query = $query->groupBy("h.page_id", "c.section")->orderBy('p.visits_search', 'DESC');
                break;
            case 'downloaded':
                $query = $query->groupBy('h.page_id', 'c.section')->orderBy('p.visits', 'DESC');
                break;
            case 'viewed':
                $query = $query->groupBy('h.page_id', 'c.section')->orderBy('p.visits', 'DESC');
                break;
            case 'rated':
                $query = $query->groupBy('h.page_id', 'c.section', 'rating.vote')->orderByRaw('COUNT(rating.vote) DESC');
                break;
            default:
                $query = $query->groupBy('h.page_id', 'c.section')->orderBy('p.updated_at', 'DESC');
                break;
        }

        $query = $query->take(12);

        $inBlocks = $query->select(DB::raw("DISTINCT(p.u_id)"))->get();

        return $inBlocks;
  }

    public static function updateVisits($page) {
        DB::unprepared('SET @disable_trigger = 1;');
        DB::update('UPDATE page SET visits = visits + 1 WHERE id = ? ', [$page->id]);
        DB::unprepared('SET @disable_trigger = NULL;');
    }

    public static function updateVisitsSearch($page) {
        DB::unprepared('SET @disable_trigger = 1;');
        DB::update('UPDATE page SET visits_search = visits_search + 1 WHERE id = ? ', [$page->id]);
        DB::unprepared('SET @disable_trigger = NULL;');
    }
}

<?php

namespace App\Helpers;

use App\Helpers\UriHelper;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;

class CaptchaHelper
{
    public static function resetCaptcha(): string {
        $code = strtolower(str_random(5));
        Session::put('captcha', $code);
        return $code;
    }

    public static function getCaptcha(): string {
        if (!Session::has('captcha')) {
            $code = self::resetCaptcha();
        } else {
            $code = substr(Session::get('captcha'), 0, 8);
        }        
        return $code;
    }

    public static function logCaptchaUsage() {
        Log::debug('Send email with captcha:  ' . Request::get('captcha') . ' / ' . Session::get('captcha') 
                    . ' email: ' . Request::get('email') . ' IP: ' . UriHelper::getClientIpAddr(Request::instance()));
    }

}

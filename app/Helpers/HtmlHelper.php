<?php

namespace App\Helpers;

use App\Http\Controllers\FileController;
use App\Helpers\UriHelper;
use App\Models\FileItem;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;

class HtmlHelper
{

    public static function isIE()
    {
        if (array_key_exists('HTTP_USER_AGENT', $_SERVER)) {
            return (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') || strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0'));
        } else {
            Log::notice("Missing 'HTTP_USER_AGENT' for request: " . Request::path() . " IP: " . UriHelper::getClientIpAddr(Request::instance()));
        }
    }

    public static function generateImgSrcSet($srcPath, $searchParam, $maxPixelRation)
    {
        $srcset = '';
        for ($i = 2; $i <= $maxPixelRation; $i++) {
            $paramValue = $searchParam . '~' . $i . 'x';
            if ($i > 2) {
                $srcset = $srcset . ', ';
            }
            $srcset = $srcset . str_replace($searchParam, $paramValue, $srcPath) . ' ' . $i . 'x';
        }
        return $srcset;
    }

    public static function generateImg($path, $mode, $imageSize, $imageType, $isProtected = null, $style = null, $maxPixelRatio = 2, $isPrintView = false)
    {
        $fixedMode = strtolower($mode);
        if ($fixedMode == FileHelper::MODE_BRAND_THUMB) {
            $fixedMode = FileController::MODE_DOWNLOADS;
        }
//        if ($isProtected === null) {
            if (strrpos($path, "protected-file/") !== false) {
                $isProtected = true;
            } else {
                $isProtected = false;
            }
//        }
        $src = UriHelper::generateImgPath($path, $fixedMode, $imageSize, $imageType, $isProtected);
        $cssStyle = "";
        if ($style) {
            $cssStyle = 'style="';
            foreach ($style as $key => $value) {
                $cssStyle .= $key . ':' . $value . ';';
            }
            $cssStyle.='"';
        }
        if ($isPrintView) {
            return '<img ' . $cssStyle . ' src="' . $src . '" >';
        } else {
            $srcBlurred = UriHelper::getBlurredPreviewUrl($src);
            return '<img ' . $cssStyle . ' src="' . $srcBlurred . '" class="lazyload-thumb" data-original="' . $src . '" data-check="09" >';
        }
    }

    public static function generateImgFromBlock($block, $imageSize, $style = null)
    {
        $mode = isset($block['type']) ? $block['type'] : \App\Http\Controllers\FileController::MODE_IMAGES;
        $imageType = isset($block['imgext'])? $block['imgext'] : \App\Helpers\FileHelper::getFileExtension($block['img']);
        $isProtected = null;
        return HtmlHelper::generateImg($block['img'], $mode, $imageSize, $imageType, $isProtected, $style);
    }

    public static function generateBrandMenuItem($pageID, $L2ID = null, $hasBold = false) {
        $page = UserAccessHelper::getSpecialPage($pageID);
        $classes = (($hasBold)? '': 'brand_child') . (($page->active) ? ' active': ' inactive');
        $itemHtml = '<li class="' . $classes . '">' .
                        (($hasBold)? '<h3>': '') .
                        '<a href="'. asset($page->slug) . '"' . (isset($L2ID) && $L2ID == $page->ID ? ' class="L1Active"' : '') . '>'
                            . $page->title .
                        '</a>' .
                        (($hasBold)? '</h3>': '') .
                        ($pageID == DAIMLER_MOBILITY && $page->active ?
                        '<ul class="brand_subchild">'
                        . '<li><a href="'.asset($page->slug).'">Daimler Financial Services</a></li>'
                        . '<li><a href="'.asset($page->slug).'">Daimler Insurance Services</a></li>'
                        . '<li><a href="'.asset($page->slug).'">Daimler Vorsorge und Versicherungsdienst</a></li>'
                        . '</ul>':'').
                    '</li>';
        return $itemHtml;
    }

    public static function fixDoubleDaggerForDaimler($content)
    {
        return str_replace('‡', '>>', $content);
    }

    /************************************************************************
	 * Format titles for smart
	 *
     * @return string
     *
	 ***********************************************************************/
    public static function formatTitle($title, $brand)
    {                                 
        if (SMART == $brand) {
            if (LANG_EN === Session::get('lang')) {
                $smartExceptions=trans('template.smartExceptions');
                if (isset($smartExceptions[$title])) {
                    return $smartExceptions[$title];
                }
            }
        }
        return $title;
   }

    /**
     ************************************************************************
	 * Create and init DOMDocumet
	 *
     * @return DOMDocument
     *
	 ***********************************************************************/
    public static function getDOMDocument(): \DOMDocument
    {
        libxml_use_internal_errors(true);
        $dom = new \DOMDocument();
        $dom->substituteEntities = false;
        $dom->recover = false;
        $dom->strictErrorChecking = false;
        return $dom;
    }

    public static function pregenerateFullsizeBannerImg($html, $tabletHTML, $lang)
    {
        $desktop_image = self::generateBannerContentImageData($html, $lang);
        $tablet_image = self::generateBannerContentImageData($tabletHTML, $lang);
        $newHTML = view('partials.content_banner', ['desktop_image' => $desktop_image, 'tablet_image'=> $tablet_image])->render();
        return $newHTML;
    }

    public static function generateBannerCarousel($html, $tabletHtml, $isHome = false, $mobileHtml = null)
    {
        $image = self::generateBannerHomeImageData($html);
        $tablet_image = self::generateBannerHomeImageData($tabletHtml);
        $mobile_image = self::generateBannerHomeImageData($mobileHtml);
        $newHTML = view('partials.home_banner', ['image' => $image,'tablet_image'=> $tablet_image,'mobile_image'=> $mobile_image, 'isHome' => $isHome])->render();

        return $newHTML;
    }

    private static function generateImagesNodesForPDF($key, $file, $file_path_cached, $node, $dom, $width): int
    {
        $createdImgs = 0;
        if ($key == 0) {
            $node->setAttribute('src', $file_path_cached);
            $node->removeAttribute('srcset');
        } else {
            $img = $dom->createElement('img');
            $img->setAttribute('src', $file_path_cached);
            $node->parentNode->appendChild($img);
            $createdImgs++;
        }
        if (trim($file->info->first->description) != "" && !$node->hasAttribute('data-hotspot')) {
            $div = $dom->createElement('div');
            $div->setAttribute('class', 'text');
            $div->setAttribute('style', "width: ".$width."px;");
            $domText = self::getDOMDocument();
            $html = mb_convert_encoding($file->info[0]->description, 'HTML-ENTITIES', 'UTF-8');
            $domText->loadHTML($html);
            $elements = $domText->getElementsByTagName('h1');
            for ($i = $elements->length; --$i >= 0;) {
                $h1 = $elements->item($i);
                $h1->parentNode->removeChild($h1);
            }
            $body = $domText->getElementsByTagName('body')->item(0);
            foreach ($body->childNodes as $child) {
                $div->appendChild($dom->importNode($child, true));
            }

            $node->parentNode->appendChild($div);
        }

        return $createdImgs;
    }

    public static function getPageMainSummary($html)
    {
        $result = '';
        if (strlen($html) < 10) {
            return $result;
        }
        libxml_use_internal_errors(true);
        //$dom = new \DOMDocument('1.0','UTF-8');
        $dom = new \DOMDocument();
        $dom->substituteEntities = false;
        $dom->recover = true;
        $dom->strictErrorChecking = false;
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        # *******************************************
        //$html = utf8_decode($html); #!!!
        # *******************************************

        $dom->loadHTML($html);
        $finder = new \DOMXPath($dom);
        $lastSummery = false;
        $nodes = $finder->query(".//*[contains(concat(' ', normalize-space(@class), ' '), ' summary ')]");
        //$accordions[$codeID]['description'] = ''; # set default value
        if ($nodes !== false && $nodes->length > 0) {
            foreach ($nodes as $node) {
                if ($node === $lastSummery) {
                    continue;
                }
                $lastSummery = $node;
                $summary = $node->textContent . '';
                if (mb_strlen($summary)) {
                    $result = $summary;
                    $summary = '';
                    break;
                }
            }
        } elseif (($nodes = $dom->getElementsByTagName('p')) && $nodes->length > 0) {
            $firstParagraph = $nodes->item(0)->textContent;
            $result = $firstParagraph;
        }
        return $result;
    }

    /** 
     * Find first image in the content
     * 
     * @param $html
     * @param $img_size
     * 
     */
    public static function getFirstImagePath(string $html, string $img_size): string
    {
        $result = null;
        if (strlen($html) < 10) {
            return $result;
        }
        $dom = self::getDOMDocument();
        $dom->recover = true;
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');

        $dom->loadHTML($html);
        $imgs = $dom->getElementsByTagName('img');
        if ($imgs->length > 0) {
            foreach ($imgs as $image) {
                if ($image->getAttribute('class') != 'anchor_nav') {
                    if ($image->hasAttribute('data-gallery')) {
                        $galeryImg = explode(',', $image->getAttribute('data-gallery'));

                        if (is_numeric($galeryImg[0])) {
                            $fileItem = FileItem::where('id', $galeryImg[0])->first();
                        }

                        if ($fileItem) {                    
                            $result = FileHelper::getCacheURLForImage(FileController::MODE_IMAGES, $fileItem, $img_size);
                        }
                    } 
                    
                    if (is_null($result)) {
                        $imgSource = UriHelper::getMasterFilePath($image->getAttribute('src'));
                        $imageType = FileHelper::getFileExtension($imgSource);
                        $result = UriHelper::generateImgPath($imgSource, FileController::MODE_IMAGES, $img_size, $imageType, false);
                    }
                    break;
                }
            }
        }
        return $result;
    }

    public static function generatePDFDownloadImg($download)
    {
        //$path = $download->file->getThumbPath(FILEITEM_REAL_PATH, 'pdf_content');
        $originalFile = FileHelper::findSourceImage(FileController::FOLDER_NAME_ZOOMPIC, $download->file, $download->file->protected);
        $imageType = FileHelper::getFileExtension($originalFile);
        $file_path_cached = UriHelper::generateImgPath($download->file->getPath(), FileController::MODE_DOWNLOADS, 'pdf_content', $imageType);
        $filemanager_mode = FileController::MODE_DOWNLOADS;
        $file_path = substr($file_path_cached, strlen($filemanager_mode) + 7);
        FileHelper::generateCacheImage($filemanager_mode, $file_path);
        $img = "<img src='$file_path_cached'>";
        return $img;
    }

    public static function wrapPageImages($page, $content)
    {
        if (strlen(trim($content)) < 10) {
            return false;
        }
        libxml_use_internal_errors(true);
        $dom = new \DOMDocument();
        $dom->substituteEntities = false;
        $dom->recover = false;
        $dom->strictErrorChecking = false;
        $content = mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8');
        //$dom->loadHTML($html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $dom->loadHTML($content);
        $imgs = $dom->getElementsByTagName('img');
        $firstImg = true;
        $count = $imgs->length;
        $index = 0;
        
        for ($i = 0; $i < $count; $i++) {
            $image = $imgs->item($i);
            $imgClass = $image->getAttribute('class');
            if (false === strrpos($imgClass, 'anchor_nav')) {
                $arrayClasses = explode(' ', $imgClass);
                if (count(array_intersect(['zoom' , 'inline_zoom', 'simple_no_zoom'], $arrayClasses)) > 0) {
                    HtmlHelper::generateHTMLForImages($image, $dom, $firstImg, $index, $page, $imgClass);
                    $index++;
                } else if ($imgClass == 'explainer') {
                    HtmlHelper::generateHTMLForExplainer($image, $dom, $firstImg, $index, $page);
                    $index++;
                } else if ($imgClass == 'gallery') {
                }
                if ($firstImg) {
                    $imgClass = 'first_image ' . $imgClass;
                    $firstImg = false;
                }

                // Add lazy loading
                $imgClass = $imgClass .' lazyload-thumb';

                $src = $image->getAttribute('src');
                $image->setAttribute('data-original', $src);
                $image->setAttribute('src', UriHelper::getBlurredPreviewUrl($src));
                $image->setAttribute('class', $imgClass);
                $image->setAttribute('data-check', '03');

                $imgPath = public_path().'/'.$src;
                if (!file_exists($imgPath)) {
                    $imgPath = FileHelper::generateCacheImage('images', substr($src, 13));
                }
                $image_info = @getimagesize($imgPath);
                if ($image_info) {
                    $width = $image_info[0];
                    $height = $image_info[1];
                } else {
                    $defaultSize = config('assets.images.sizes.content');
                    $width = $defaultSize['width'];
                    $height = $defaultSize['height'];
                }
                if (TABLET) {
                    $image->setAttribute('data-original_width', $width);
                    $image->setAttribute('data-original_height', $height);
                    $ratio = $width / $height;
                    if ($width > TABLET_MAX_WIDTH) {
                        $width = TABLET_MAX_WIDTH;
                        $height = round(TABLET_MAX_WIDTH / $ratio, 2);
                    }
                    if ($height > TABLET_MAX_HEIGHT) {
                        $height = TABLET_MAX_HEIGHT;
                        $width = round($ratio * $height, 2);
                    }
                }
                $image->setAttribute('width', $width);
                $image->setAttribute('height', $height);
            }
        }
        $dom->removeChild($dom->doctype);
        $htmlString = $dom->saveHTML();
        return str_replace(['<html>', '</html>', '<body>','</body>'], "", $htmlString);
    }

    public static function generateEmbedImg($src)
    {
        $contents = file_get_contents($src);
        $base64   = base64_encode($contents);
        $mime = 'image/'.pathinfo($src, PATHINFO_EXTENSION);
        return ('data:' . $mime . ';base64,' . $base64);
    }

    public static function generateTagsDots($tags)
    {
        if ($tags == null) {
            return '';
        }
        $tagsArray = explode(',', $tags);
        $result = '';
        $numItems = count($tagsArray);
        $i = 0;
        foreach ($tagsArray as $key => $tag) {
            if (++$i === $numItems) {
                $result .= '<span>'.$tag.'</span>';
            } else {
                $result .= '<span>'.$tag.',</span>';
            }
        }
        return $result;
    }

    /************************************************************************
     * Parse HTML and seek for
     * 	Page title in: 	h1,
     * 	Summary in: 	*[class=summary],
     * 	Thumbnail in: 	img:first
     *
     ***********************************************************************/
    public static function seekInHTML($html, &$blocks, $searchForH1 = true, $searchForThumbs = true, $defaultBrandThumbnail = null, array $searchTerms = null, $max_count = 355, $firstParagraph = true)
    {
        if (strlen(trim($html))<10) {
            return false;
        }

        $dom = HtmlHelper::getDOMDocument();
    # *******************************************
        //$html = utf8_decode(htmlentities($html));
        //$html = utf8_decode($html); #!!!
        //$html = htmlentities($html);
    # *******************************************
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        $dom->loadHTML($html);
        $finder = new \DomXPath($dom);

        $codeID = null;
        //$codes = $dom->documentElement->getElementsByTagName('dncode');
        $codes = $finder->query('//dncode');

        $imgs = $image = null;
        $founded = [];
        //$digInTags = array('b', 'p', 'strong');

        //for($c=0; $c < $codes->length; $c++){
        for ($c=$codes->length-1; $c >= 0; $c--) {
            $code = $codes->item($c);
            $codeID = $code->getAttribute('id');

            self::getSummaryFromNode($code, $codeID, $searchTerms, $blocks, $finder, $searchForH1, $max_count, $firstParagraph);

            if ($searchForThumbs && (!isset($blocks[$codeID]['img']) || $blocks[$codeID]['img'] == $defaultBrandThumbnail) ) {
                $imgs = $code->getElementsByTagName('img');
                if ($imgs->length > 0) {
                    foreach ($imgs as $image) {
                        //$image = $imgs->item(0);
                        if ($image->getAttribute('class') != 'anchor_nav') {
                            //Check for protected images!
                            $fileItem = null;
                            if ($image->hasAttribute('data-gallery')) {
                                $blocks[$codeID]['img-data-gallery'] = $image->getAttribute('data-gallery');
                                $galeryImg = explode(',', $blocks[$codeID]['img-data-gallery']);

                                if (is_numeric($galeryImg[0])) {
                                    $fileItem = FileItem::where('id', $galeryImg[0])->first();
                                }
                            }
                            if ($fileItem && $fileItem->protected) {
                                $blocks[$codeID]['img'] = UriHelper::generateImgPath($fileItem->getCachedThumbPath('{type}'), FileController::MODE_IMAGES, '{type}', null, true);
                            } else {
                                $blocks[$codeID]['img'] = UriHelper::getMasterFilePath($image->getAttribute('src'));
                            }
                            $blocks[$codeID]['type'] = FileController::MODE_IMAGES;
                            $founded[] = $codeID;
                            $image = null;
                            $imgs = null;
                            break;
                        }
                    }
                }
            }

            $dom->documentElement->removeChild($code);
        }
        return $founded;
    }
      /************************************************************************
     *
	 * Getting summary text from html
     * (support function for seekInHTML)
	 *
	 ***********************************************************************/
    public static function getSummaryFromNode(\DOMElement $code, $codeID, array $searchTerms = null, array &$blocks, \DOMXPath $finder, $searchForH1, $max_len, $firstParagraph)
    {
        $min_len = 100;
        $classname = "summary";
        $summary = '';
        $nodes = null;
        $lastNode = false;
        $isCompleateSummary = false;
        $xpath = $xpathMaxMatch = './/p';

        if ($searchForH1) {
            $h1s = $code->getElementsByTagName('h1');
            $blocks[$codeID]['h1'] = '';
            if ($h1s->length > 0) {
                foreach ($h1s->item(0)->childNodes as $subH1) {
                    if ($subH1->nodeType == XML_TEXT_NODE) {
                        //$blocks[$codeID]['h1'] .= $subH1->nodeValue;
                        $blocks[$codeID]['h1'] .= $subH1->textContent;
                        //break;
                    }
                }
            }

            unset($h1s);
        }

        if (!is_array($searchTerms) || count($searchTerms) == 0) {
            //$nodes = $finder->query(".//*[@class = 'summary']", $code);
            $nodes = $finder->query(".//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]", $code);
            //$blocks[$codeID]['text'] = ''; # set default value
            if ($nodes !== false && $nodes->length > 0) {
                foreach ($nodes as $node) {
                    if ($node === $lastNode) {
                        #todo: get first paragraph
                        continue;
                    }
                    $lastNode = $node;
                    $summary .= self::getNodeText($node);
                    if (mb_strlen($summary) > $min_len) {
                        $text = self::getSummaryText($summary, $max_len, $firstParagraph);
                        if (mb_strlen($text) >= $min_len) {
                            $isCompleateSummary = true;
                            break;
                        }
                    }
                }
            }

            if (!$isCompleateSummary && $lastNode && mb_strlen($summary) < $max_len) {
                $node = self::findNextNotEmptySibling($lastNode);
                if ($node) {
                    $summary .= self::getNodeText($node);
                    $text = self::getSummaryText($summary, $max_len, $firstParagraph);
                    if (mb_strlen($text) >= $min_len) {
                        $isCompleateSummary = true;
                    }
                }
            }
        } else {
            $xpathMaxMatch = $xpath . self::generateXPathQuery($searchTerms, 'and', 2);
        }

        if (!$isCompleateSummary && ($nodes = $finder->query($xpathMaxMatch, $code)) && $nodes->length > 0) {
            $summary = '';
            for ($i = 0; $i<$nodes->length; $i++) {
                $lastNode = $nodes->item($i);
                $nodeText = self::getNodeText($lastNode);

                if (trim($nodeText, " \t\n\r\0\x0B\xC2\xA0") != '') {
                    $summary .= $nodeText;
                }

                if (mb_strlen($summary) > $min_len) {
                    $text = self::getSummaryText($summary, $max_len, $firstParagraph);
                    if (mb_strlen($text) >= $min_len) {
                        $isCompleateSummary = true;
                        break;
                    }
                }
            }

            $node = $lastNode;
            while (!$isCompleateSummary && mb_strlen($summary) < $max_len && $node) {
                $node = self::findNextNotEmptySibling($node);
                if ($node) {
                    $summary .= self::getNodeText($node);
                    $text = self::getSummaryText($summary, $max_len, $firstParagraph);
                    if (mb_strlen($text) >= $min_len) {
                        $isCompleateSummary = true;
                    }
                }
            }

            $node = $lastNode;
            while (!$isCompleateSummary && mb_strlen($summary) < $max_len && $node) {
                $node = self::findPreviusNotEmptySibling($node);
                if ($node) {
                    $summary = self::getNodeText($node) . $summary;
                    $text = self::getSummaryText($summary, $max_len, $firstParagraph);
                    if (mb_strlen($text) >= $min_len) {
                        $isCompleateSummary = true;
                    }
                }
            }
        }

        if (!$isCompleateSummary && $xpathMaxMatch !== $xpath) {
            if (count($searchTerms) > 1) {
                $xpathMinMatch = $xpath . self::generateXPathQuery($searchTerms, 'or');
            } else {
                $xpathMinMatch = $xpath;
            }

            if (($nodes = $finder->query($xpathMinMatch, $code)) && $nodes->length > 0) {
                $summary = '';
                for ($i = 0; $i<$nodes->length; $i++) {
                    $lastNode = $nodes->item($i);
                    $nodeText = self::getNodeText($lastNode);

                    if (trim($nodeText, " \t\n\r\0\x0B\xC2\xA0") != '') {
                        $summary .= $nodeText;
                    }

                    if (mb_strlen($summary) > $min_len) {
                        $text = self::getSummaryText($summary, $max_len, $firstParagraph);
                        if (mb_strlen($text) >= $min_len) {
                            $isCompleateSummary = true;
                            break;
                        }
                    }
                }
            }
        }

        if (!$isCompleateSummary) {
            $text = self::getSummaryText($summary, $max_len, $firstParagraph);
        }

        $blocks[$codeID]['text'] = $text;
    }

    /************************************************************************
     *
     * Getting summary text from html
     * (support function for seekInHTML)
     *
     * @return string
     *
	 ***********************************************************************/
    public static function getSummaryText($text, $count = 335, $firstParagraph = true)
    {
        $result = $text;
        if ($firstParagraph) {
            $start = mb_strpos($result, '<p>', 0, 'utf-8');
            if (false !== $start) {
                $end = mb_strpos($result, '</p>', $start, 'utf-8');

                $result = mb_substr($result, $start, $end-$start+4, 'utf-8');
            }
        }
        $result = strip_tags($result, '<br>');
        if (mb_strlen($result, 'utf-8') > $count) {
            $result = mb_substr($result, 0, $count, 'utf-8');
            $pos = mb_strrpos($result, ' ', 0, 'utf-8');
            $result = mb_substr($result, 0, $pos, 'utf-8');
            $result = $result . "...";
        }

        return $result;
    }

    /**
     * Process the page content before dispaly
     * 
     * @param $html 
     * @param $lang
     * @param $page_mode PAGE_MODE_BROWSE|PAGE_MODE_PDF|PAGE_MODE_PRINT
     */
    public static function processPageContent(string $html, $lang, string $page_mode)
    {
        if (strlen(trim($html)) < 10) {
            return false;
        }
        // Parse the HTML to Dom
        $dom = self::getDOMDocument();
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        $dom->loadHTML($html);
        
        // Process images
        self::processImages($dom, $lang, $page_mode);

        if ($page_mode == \PAGE_MODE_PDF) {
            // Process menu elements
            $uls =  $dom->getElementsByTagName('ul');
            self::addClassForListElemntInParagraf($uls, ' no-margin');
            $ols =  $dom->getElementsByTagName('ol');
            self::addClassForListElemntInParagraf($ols, ' no-margin');
        } else if ($page_mode == \PAGE_MODE_PRINT) {
            // Remove iframes for print
            $iframes = $dom->getElementsByTagName("iframe");
            while ($iframes->length > 0) {
                $iframetag = $iframes->item(0);
                $iframetag->parentNode->removeChild($iframetag);
            }
        }

        // Convert back to HTML
        $dom->removeChild($dom->doctype);
        $htmlString = $dom->saveHTML();
        return str_replace(['<html>', '</html>', '<body>','</body>'], "", $htmlString);
    }

    //
    // Private Functions
    //

    private static function addClassForListElemntInParagraf(\DOMNodeList $nodeList, string $classes)
    {
        $nodesCount =  $nodeList->length;
        for ($i=0; $i< $nodesCount; $i++) {
            $sibling = $nodeList->item($i)->previousSibling->previousSibling;
            if (isset($sibling->tagName) && $sibling->tagName == "p") {
                $class = $sibling->getAttribute('class');
                $sibling->setAttribute('class', $class . $classes);
            }
        }
    }

    // Process images for Print and PDF
    public static function processImages(\DOMDocument $dom, $lang, string $page_mode)
    {
        $imgs = $dom->getElementsByTagName('img');
        $firstImg = true;
        $count = $imgs->length;
        for ($i = 0; $i < $count; $i++) {
            $tmpImg = $imgs->item($i);
            if ($tmpImg->getAttribute('class') != 'anchor_nav') {
                if ($firstImg) {
                    $firstImg = false;
                    if ($page_mode == \PAGE_MODE_PDF) {
                        $class = 'first_image';
                        if ($tmpImg->hasAttribute('class')) {
                            $class .=' ' . $tmpImg->getAttribute('class');
                        }
                        $tmpImg->setAttribute('class', $class);
                    }
                }
                if ($tmpImg->hasAttribute('data-gallery')) {
                    // Process image/gallery
                    $newImgs = 0;
                    if ($page_mode == \PAGE_MODE_PDF) {
                        // Wrap the image with div!!!
                        $div = $dom->createElement('div');
                        $div->setAttribute('class', 'img');
                        $oldValue = $tmpImg->cloneNode(true);
                        $div->appendChild($oldValue);
                        $tmpImg->parentNode->parentNode->replaceChild($div, $tmpImg->parentNode);
                        //DOM was modified take the image again?
                        $tmpImg = $imgs->item($i);

                        $newImgs = self::processImageForPDF($dom, $tmpImg, $div, $oldValue, $lang);
                    } else if ($page_mode == \PAGE_MODE_PRINT) {
                        //Check for protected images!
                        $galeryImg = explode(',', $tmpImg->getAttribute('data-gallery'));

                        if (is_numeric($galeryImg[0])) {
                            $fileItem = FileItem::where('id', $galeryImg[0])->first();
                        }

                        if ($fileItem) {
                            if ($fileItem->protected) {
                                $imgSrc = UriHelper::generateImgPath($fileItem->getCachedThumbPath('{type}'), FileController::MODE_IMAGES, 'content', null, true);
                            } else {
                                $imagePath = $fileItem->getPath();
                                $imageType = FileHelper::getFileExtension($imagePath);
                                $imgSrc = UriHelper::generateImgPath($imagePath, FileController::MODE_IMAGES, 'content', $imageType);
                            }
                            $tmpImg->setAttribute('src', $imgSrc);
                        }
                    }
                    // Update $count with new images
                    $count = $count + $newImgs;
                    // Update index of images with new images
                    $i = $i + $newImgs;
                }
            }
        }
    }

    // TODO: Do we need $oldValue?
    private static function processImageForPDF(\DOMDocument $dom, \DOMElement $tmpImg, \DOMElement $div, $oldValue, $lang): int
    {
        $galeryImg = explode(',', $oldValue->getAttribute('data-gallery'));
        $width = $height = 0;
        $newImgs = 0;
        // Get image/gallery form DB
        $fileItems = FileItem::whereIn('id', $galeryImg)
            ->with(['info' => function ($query) use ($lang) {
                        return $query->where('lang', $lang);
            }])
            ->orderByRaw(DB::raw("FIELD(id, ".implode(',', $galeryImg).")"))
            ->get();

        foreach ($fileItems as $key => $file) {
            // Get tha path to cached image
            $filemanager_mode = FileController::MODE_IMAGES;
            $file_path = FileHelper::getCachePathForImage($filemanager_mode, $file, 'pdf_content');

            // Get the path to cached image and generate it if not exists
            $file_path_cached_full = FileHelper::generateCacheImage($filemanager_mode, $file_path, $file->protected);
            // Remove base path
            $file_path_cached = substr($file_path_cached_full, strlen(public_path()) + 1);

            if ($key == 0) {
                // Get image size of first image
                list($width, $height) = FileHelper::getImageSize($file_path_cached_full);
            }
            $newImgs+= HtmlHelper::generateImagesNodesForPDF($key, $file, $file_path_cached, $oldValue, $dom, $width, $height);
        }
        
        // Process hotspots
        self::processImageHotspotsForPDF($dom, $tmpImg, $div, $width, $height);

        return $newImgs;
    }


    private static function processImageHotspotsForPDF(\DOMDocument $dom, \DOMElement $tmpImg, \DOMElement $div, $width, $height)
    {
        if ($tmpImg->hasAttribute('data-hotspot')) {
            $hst_jsont = json_decode($tmpImg->getAttribute('data-hotspot'));
            $pois = $dom->createElement('div');
            $pois->setAttribute('class', 'pois');
            $pois->setAttribute('style', "width:".$width."px; height:".$height."px;");
            for ($y = 0; $y < count($hst_jsont->points); $y++) {
                $el = $dom->createElement('div');
                $position = $hst_jsont->points[$y]->pos;
                $el->setAttribute('class', 'poi '.$position);
                $left = ($width * floatval($hst_jsont->points[$y]->left) / 100);
                $top = ($height *floatval($hst_jsont->points[$y]->top) / 100);
                switch ($position) {
                    case 'up':
                        $left -= 10;
                        $top += 15;
                        break;
                    case 'right':
                        $left -= 35;
                        $top -= 10;
                        break;
                    case 'down':
                        $left -= 10;
                        $top -= 35;
                        break;
                    case 'left':
                        $left += 15;
                        $top -= 10;
                        break;
                }
                //FIXME: Move the hotspot here because margin has bug in Dompdf 0.8
                // class .poi.no_arrow replacement. It has to be commented until fix in Dompdf
                $left -= 10;
                $top -= 10;

                $el->setAttribute('style', "left: ".$left."px; top: ".$top."px;");
                $text = $dom->createTextNode($y + 1);
                $el->appendChild($text);
                $pois->appendChild($el);
            }
            $div->appendChild($pois);
            if (strlen($hst_jsont->description) > 3) {
                $div->setAttribute('class', $div->getAttribute('class'). ' img_hotspot');
                $descr = $dom->createElement('div');
                $descr->setAttribute('class', 'hotspot_descr');
                $descr->setAttribute('style', "width: ".$width."px;");

                $domText = self::getDOMDocument();
                $html = mb_convert_encoding($hst_jsont->description, 'HTML-ENTITIES', 'UTF-8');
                $domText->loadHTML($html);
                $body = $domText->getElementsByTagName('body')->item(0);
                foreach ($body->childNodes as $child) {
                    $descr->appendChild($dom->importNode($child, true));
                }
                if (!$div->nextSibling) {
                    $dom->appendChild($descr);
                } else {
                    $next = $div->nextSibling->nextSibling;
                    $div->parentNode->insertBefore($descr, $next);
                }
            }
        }
    }

    private static function generateHTMLForImages($domImg, $dom, $firstImg, $index, $page, $imgClass)
    {
        $id = $domImg->getAttribute('data-gallery');
        $file = FileItem::with(
            ['info' => function ($query) {
                            $lang = Session::get('lang');
                            $query->where('lang', '=', $lang);
            }]
        )->find(intval($id));
        // Load special image if not exists
        if ($file == null) {
            $file_description = "";
            $file = FileItem::with(
            ['info' => function ($query) {
                            $lang = Session::get('lang');
                            $query->where('lang', '=', $lang);
            }]
            )->find(intval(13533));
        }
        $file_info = $file->info->first();
        $file_description = ($file_info) ? $file_info->description : "";

        // Generate image URL. if image is protected it is different!
        if ($file->protected) {
            $imageURL = $file->getCachedThumbPath('content');
        } else {
            $imageURL = $domImg->getAttribute('src');
        }
        $domImg->setAttribute('src', $imageURL);

        // Generate HTML
        $container = $dom->createElement('div');
        $container->setAttribute('class', 'image_wrapper cf'.($firstImg == true?' first_image':''));
        $wrapper = $domImg->parentNode;
        if ($wrapper->tagName=='body') {
            $wrapper->replaceChild($container, $domImg);
        } else {
            $wrapper->removeChild($domImg);
            $wrapper->parentNode->replaceChild($container, $wrapper);
        }
        $galleryTitle = 'lytebox[gallery_'.time().'_'. $index. ']';
        $newHTML = '';
        $image_item = $dom->createElement('div');
        $image_item->setAttribute('class', 'image_item'.(($index % 2 == 1 && $page->brand != SMART) ? " right_image" : ""));
        $image_item->setAttribute('unselectable', 'on');
        $container->appendChild($image_item);
        $image_container = $dom->createElement('div');
        $image_container->setAttribute('class', 'image_container');
        $image_item->appendChild($image_container);
        $aTag = $dom->createElement('a');

        if (strpos($imgClass, 'simple_no_zoom') === false) {
            if (strpos($imgClass, 'inline_zoom') !== false) {
                $aTag->setAttribute('href', $file->getZoomPath());
            } else {
                $aTag->setAttribute('href', $file->getPath());
            }
            $aTag->setAttribute('unselectable', 'on');
            $aTag->setAttribute('rel', $galleryTitle);
            $aTag->setAttribute('class', 'image_layover');
        }
        $aTag->setAttribute('data-title', str_replace('"/g', '&quot;', $file_description));
        $image_container->appendChild($aTag);

        if ($file_description != '' || strrpos($imgClass, 'hotspot')) {
            $image_text = $dom->createElement('div');
            $image_text->setAttribute('class', 'image_text');
            $image_item->appendChild($image_text);
            if ($file_description != ''&& strrpos($imgClass, 'hotspot') === false) {
                $textDiv = $dom->createElement('div');
                $image_text->appendChild($textDiv);
                $textDiv->appendChild($dom->importNode(HtmlHelper::generateNodeFromHTML($file_description)->documentElement, true));
            } else if (strrpos($imgClass, 'hotspot')!= false) {
                $hst_json = json_decode($domImg->getAttribute('data-hotspot'));
                //file_put_contents('/home/www/logs/printblade.log', 'class: '.var_export($imgClass,true)."\n",FILE_APPEND);
                //file_put_contents('/home/www/logs/printblade.log', 'HST_JSON: '.var_export($hst_json,true)."\n",FILE_APPEND);
                
                $html = '<div id="LE-pois-0" class="pois">';
                if (isset($hst_json)) {
                    for ($i = 0; $i < count($hst_json->points); $i++) {
                        $html .= '<div class="poi '.($hst_json->points[$i]->pos) . '" style="left: ' . ($hst_json->points[$i]->left) . '; top: ' . ($hst_json->points[$i]->top). ';">' . ($i + 1) . '</div>';
                    }
                }
                $html .= '</div>';

                $aTag->appendChild($dom->importNode(HtmlHelper::generateNodeFromHTML($html)->documentElement, true));
                if (isset($hst_json)) {
                    if (strlen($hst_json->description) > 0) {
                        $image_text->appendChild($dom->importNode(HtmlHelper::generateNodeFromHTML($hst_json->description)->documentElement, true));
                    }
                }
            }
        }
        $aTag->appendChild($domImg);
        $domImg->setAttribute('unselectable', 'on');
        $domImg->setAttribute('border', '0');
    }

    private static function generateHTMLForExplainer($domImg, $dom, $firstImg, $index, $page)
    {
        $container = $dom->createElement('div');
        $container->setAttribute('class', 'image_wrapper cf'.($firstImg == true?' first_image':''));
        $wrapper = $domImg->parentNode;
        $wrapper->removeChild($domImg);
        $wrapper->parentNode->replaceChild($container, $wrapper);
        $explainerTitle = 'explainer_'.time();
        $explainer_gallery = $dom->createElement('div');
        $explainer_gallery->setAttribute('class', 'explainer_gallery image_item'.(($index % 2 == 1 && $page->brand != SMART) ? " right_image" : ""));
        $container->appendChild($explainer_gallery);
        $image_container = $dom->createElement('div');
        $image_container->setAttribute('class', 'image_container');
        $explainer_gallery->appendChild($image_container);
        $aTag = $dom->createElement('a');
        $aTag->setAttribute('class', 'explainer_layover');
        $aTag->setAttribute('rev', 'width:1285px;height:771px;scrolling:no;');
        $aTag->setAttribute('rel', "lyteframe");
        $aTag->setAttribute('id', $explainerTitle.'_0');
        $aTag->setAttribute('unselectable', "on");
        $aTag->setAttribute('data-info', "");
        $image_container->appendChild($aTag);
        $aTag->appendChild($domImg);
        $domImg->setAttribute('unselectable', 'on');
        $domImg->setAttribute('border', '0');
        $image_text = $dom->createElement('div');
        $image_text->setAttribute('class', 'image_text');
        $image_text->appendChild($dom->createTextNode(trans('js_localize_fe.layout_explainer.default_text')));
        $explainer_gallery->appendChild($image_text);
    }

    private static function generateNodeFromHTML($html)
    {
        if (strlen(trim($html)) < 1) {
            return '';
        }
        $domText = new \DOMDocument();
        $domText->substituteEntities = false;
        $domText->recover = false;
        $domText->strictErrorChecking = false;
        $htmlEncoded = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        $domText->loadHTML($htmlEncoded);
        $domText->removeChild($domText->doctype);
        return $domText;
    }

    private static function generateBannerContentImageData($html, $lang)
    {
        $image = null;
        if (strlen(trim($html)) < 10) {
            return $image;
        }
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        $dom = self::getDOMDocument();
        $dom->loadHTML($html);
        $imgs = $dom->getElementsByTagName('img');

        if ($imgs->length > 0) {
            $fileItem = null;
            $img = $imgs->item(0);
            $image = ['hasDescription'=> false, 'class'=> '', 'alt'=>'', 'data_gallery'=> ''];
            foreach ($img->attributes as $attr) {
                $image[str_replace('-', '_', $attr->nodeName)] = $attr->nodeValue;
            }

            if ($img->hasAttribute('data-gallery')) {
                $galeryImg = explode(',', $img->getAttribute('data-gallery'));

                if (count($galeryImg)>0) {
                    $fileItem = FileItem::where('id', $galeryImg[0])->with(['info' => function ($query) use ($lang) {
                        return $query->where('lang', $lang);
                    }])->first();


                    if ($fileItem && trim($fileItem->info[0]->description) != "") {
                        $image['hasDescription'] = true;
                        $image['description'] = $fileItem->info[0]->description;
                    }
                }
            }
            if ($fileItem && $fileItem->protected) {
                $path = UriHelper::generateImgPath($fileItem->getCachedThumbPath('{type}'), FileController::MODE_IMAGES, 'fullsize_banner', null, true);
            } else {
                $imageSrc  = $img->getAttribute('src');
                $imageType = FileHelper::getFileExtension($imageSrc);
                $path = UriHelper::generateImgPath($imageSrc, FileController::MODE_IMAGES, 'fullsize_banner', $imageType);
            }
            $image['data_original'] = $path;
        }
        return $image;
    }


    private static function generateBannerHomeImageData($html)
    {
        if (strlen($html) < 10) {
            return null;
        }
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        $dom  = self::getDOMDocument();
        $dom->loadHTML($html);
        $imgs = $dom->getElementsByTagName('img');

        $images  = [];
        $linkSrc = [];

        $length      = $imgs->length - 1;
        $randomIndex = mt_rand(0, $length);
        $img         = $imgs->item($randomIndex);
        //Implemented protected images!
        $imageSrc    = $img->getAttribute('src');
        $imageType   = FileHelper::getFileExtension($imageSrc);
        $path        = UriHelper::generateImgPath($imageSrc, FileController::MODE_IMAGES, 'brand_banner', $imageType);

        $tmp = ['link' => $img->parentNode->getAttribute('href'), 'class'=> '', 'alt'=>'', 'data_gallery'=> ''];

        foreach ($img->attributes as $attr) {
            $tmp[str_replace('-', '_', $attr->nodeName)] = $attr->nodeValue;
        }

        $fileItem = FileItem::with(
            ['info' => function ($query) {
                                $lang = Session::get('lang');
                                $query->where('lang', '=', $lang);
            }]
        )->where('id', $tmp['data_gallery'])->first();

        $tmp['btw_button'] = trans('template.learn_more');

        if ($fileItem) {
            if ($fileItem->protected) {
                $path = UriHelper::generateImgPath($fileItem->getCachedThumbPath('{type}'), FileController::MODE_IMAGES, 'brand_banner', $imageType, true);
            }
            $info               = $fileItem->info->first();
            $tmp['title']       = $info->title;
            $tmp['description'] = $info->description;

            $lines = preg_split('/\<\/p\>/', $tmp['description'], -1, PREG_SPLIT_NO_EMPTY);
            $l     = count($lines) > 1 ? array_pop($lines) : false;

            if ($l && strlen($l) < 100) {
                $tmp['btw_button']  = $l                  = strip_tags(trim($l, "\n\r\t "));
                $tmp['description'] = preg_replace(
                    '/\<p\>\s*?' . preg_quote($l, '/') . '\s*?\<\/p\>/',
                    '',
                    $tmp['description']
                );
            }
        } else {
            $tmp['title']       = '';
            $tmp['description'] = '';
        }

        $img->setAttribute('src', $path);

        // no text - no button
        if (empty($tmp['description'])) {
            $tmp['btw_button'] = false;
        }
        return $tmp;
    }

    /************************************************************************
     *
     * Generate XPath query for search terms
     *
     * @return string
     *
     ***********************************************************************/
    private static function generateXPathQuery(array $searchTerms, $logical_operator, $max_terms = 100)
    {
        $xpathQuery = '[';
        $termsCount = 0;
        foreach ($searchTerms as $term) {
            $termsCount = $termsCount + 1;
            if ($termsCount > 1) {
                $xpathQuery .= ' ' . $logical_operator . ' ';
            }
            //$xpath .= "//text()[contains(.,'Alliance') or contains(.,'Consulting')]";
            $xpathQuery .= "contains(.,'" . $term . "')";
            if ($termsCount >= $max_terms) {
                break;
            }
        }
        $xpathQuery .= ']';

        return $xpathQuery;
    }

     /************************************************************************
     *
	 * Get text of Node with childs
     *
     * @return string
	 *
	 ***********************************************************************/
    private static function getNodeText(\DOMNode $node)
    {
        if ($node->hasChildNodes()) {
            $html = $node->ownerDocument->saveHTML($node);
            if ($html) {
                $html = strip_tags(preg_replace("'|\<strong.*\>(.*\n*)\</strong\>|isU'", "", $html));
            } else {
                $html = '';
            }
        } else {
            $html = $node->textContent;
        }
        return $html;
    }

     /************************************************************************
     *
	 * Find previus not empty Sibling Node
     *
     * @return DOMNode
	 *
	 ***********************************************************************/
    private static function findPreviusNotEmptySibling(\DOMNode $node)
    {
        $curNode = $node;
        while ($curNode && $curNode->previousSibling) {
            $text = $curNode->previousSibling->textContent . '';
            if (trim($text, " \t\n\r\0\x0B\xC2\xA0") != '') {
                return $curNode->previousSibling;
            }
            $curNode = $curNode->previousSibling;
        }

        return null;
    }

     /************************************************************************
     *
	 * Find next not empty Sibling Node
     *
     * @return DOMNode
	 *
	 ***********************************************************************/
    private static function findNextNotEmptySibling(\DOMNode $node)
    {
        $curNode = $node;
        while ($curNode && $curNode->nextSibling) {
            $text = $curNode->nextSibling->textContent . '';
            if (trim($text, " \t\n\r\0\x0B\xC2\xA0") != '') {
                return $curNode->nextSibling;
            }
            $curNode = $curNode->nextSibling;
        }

        return null;
    }
}

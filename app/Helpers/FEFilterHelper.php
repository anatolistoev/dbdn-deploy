<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Helpers;

use App\Models\Page;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;


class FEFilterHelper {

    public static function getFilterKey(string $sectionId, string $page_template): string {
        $sessKey = 'download_filter_' . $sectionId . '_' . $page_template . '_' . Session::get('lang');
        return $sessKey;
    }

    public static function isDownloadFilterSet(string $sectionId, string $page_template): bool
    {
        $download_filter_key = self::getFilterKey($sectionId, $page_template);
        if (Session::has($download_filter_key)
                && is_array(Session::get($download_filter_key))
                && count(Session::get($download_filter_key)) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /************************************************************************
	 * Check for filters and set it in session they are set
	 ***********************************************************************/
    public static function addFilterToPage($page, $lang) {
        $tmp = Request::input('filter');

        if (1 == substr_count($tmp, ':') || 'none' == $tmp) {
            $ascendants = Page::ascendants($page->id, $lang)->get();
            $sectionId = $page->parent_id == HOME ? $page->id : DispatcherHelper::getActiveL2($ascendants);

            $sessKey = self::getFilterKey($sectionId, $page->template);
            if ('none' == $tmp) {
                Session::forget($sessKey);
            } else {
                list($f,$v) = explode(':', $tmp);
                $tmp = Session::get($sessKey);
                Session::put($sessKey, [$f => $v]);
            }
        }
    }

    /************************************************************************
	 * Get filters for download or best practice page
	 ***********************************************************************/
    public static function getFilterBlocks($lang)
    {
        $req = Request::except('_token');
        $filtersAndSort = [];

        $page = Page::alive()->haveLang()->where('id', $req['pageid'])->first();
        if (!$page || (!Auth::check() && $page->authorization == 1)) {
            //TODO return response when page needs authorization to be viewed
            return [];
        }

        foreach (['time_from', 'time_to', 'title', 'time', 'viewed', 'rating', 'comments' ] as $k) {
            if (isset($req[$k])) {
                $filtersAndSort[$k] = $req[$k];
            }
        }

        if (isset($req['usage'])) {
            $filtersAndSort['usage'] = explode(',', $req['usage']);
        }
        if (isset($req['file_type'])) {
            $filtersAndSort['file_type'] = explode(',', $req['file_type']);
        }
        if (isset($req['tags'])) {
            $filtersAndSort['tags'] = explode(',', $req['tags']);
        }

        $ascendants = Page::ascendants($page->id, $lang)->get();
        $sectionId = (1 == $page->parent_id) ? $page->id : DispatcherHelper::getActiveL2($ascendants);

        $page->brand  = $page->parent_id == HOME ? $page->id : DispatcherHelper::getActiveL2($ascendants);

        if (-1 == $sectionId) {
            return [];
        }

        $sessionKey = self::getFilterKey($sectionId, $page->template);

        if (0 == $page->overview) {
            if (count($filtersAndSort) > 0) {
                Session::put($sessionKey, $filtersAndSort);
            } else {
                Session::forget($sessionKey);
            }
            return [];
        }

        $data = ['PAGE'    => $page];
        $defaultBrandThumbnail =  DispatcherHelper::getBrandThumbnail($sectionId);
        $data['IS_GALLERY']  = false;
        if (!count($filtersAndSort)) {
            Session::forget($sessionKey);
            $sectionPages     = Page::descendants($sectionId, $lang, 0)->alive()->haveLang()->get();
            $sectionPIndex    = DispatcherHelper::getParentIndex($sectionPages);
            $ascendantIdIndex = DispatcherHelper::getIdIndex($ascendants);
            $level            = $page->parent_id != 0 ? (isset($ascendantIdIndex[1]) ? $ascendantIdIndex[1]->level : 0) : 0;
            $nav_tags         = [];
            $blocks           = FEOverviewPageHelper::getOverview($page, $sectionPIndex, $sectionId, $nav_tags, $level); #overview blocks
            $data['FILTERED_BLOCKS']  = false;
            $data['IS_GALLERY']  =  $level == 2 ? false:true;
        } else {
            Session::put($sessionKey, $filtersAndSort);
            $filter_info             = new \stdClass();
            $blocks                  = self::filterOverviewBlocks($page, $filtersAndSort, $filter_info, $defaultBrandThumbnail, $lang);
            $data['FILTERED_BLOCKS'] = true;
            $data['ALLBLOCKSTITLE']  = self::getFiltredResultTitle($filter_info, $blocks);
        }
        $data['BLOCKS'] = $blocks;

        return $data;
    }

    public static function filterOverviewBlocks(Page $page, $filtersAndSort, &$filter_info, $defaultBrandThumbnail, $lang)
    {
        if (!$filter_info) {
            $filter_info = new \stdClass();
        }
        $blocks     = [];
        $query = Page::alive()->haveLang()
                ->select('page.*')
                ->join('hierarchy', 'hierarchy.page_id', '=', 'page.id')
                ->where('hierarchy.parent_id', $page->id)
                ->where('hierarchy.level', '<>', 0)
                ->where('page.overview', 0);

        if ($page->template == 'downloads') {
            $query->join('download as dw', 'dw.page_u_id', '=', 'page.u_id')
                    ->whereRaw("dw.position = ( SELECT MIN(d2.position) FROM download d2 WHERE d2.page_u_id = page.u_id AND d2.deleted_at IS NULL)")
                    ->whereNull('dw.deleted_at')
                    ->join('file as f', 'f.id', '=', 'dw.file_id')
                    ->whereNull('f.deleted_at');
        }

        if (isset($filtersAndSort['tags']) && is_array($filtersAndSort['tags']) && count($filtersAndSort['tags']) > 0) {
            $filter_info->filter_fields[] = 'tags';
            $query->join('pagetag as pt', 'pt.page_id', '=', 'page.u_id')
                    ->whereNull('pt.deleted_at')
                    ->join('tag as t', 't.id', '=', 'pt.tag_id')
                    ->whereNull('t.deleted_at')
                    ->where('t.lang', '=', Session::get('lang'))
                    ->whereIn('t.name', $filtersAndSort['tags']);
        }

        // Period filter
        if (isset($filtersAndSort['time_from']) && $filtersAndSort['time_from'] != '') {
            $filter_info->filter_fields[] = 'time_from';
            $date = date('Y-m-d H:i:s', strtotime($filtersAndSort['time_from']));
            if ($page->template == 'downloads') {
                $query = $query->where('f.updated_at', '>=', $date);
            } else {
                $query = $query->where('page.updated_at', '>=', $date);
            }
        }

        if (isset($filtersAndSort['time_to']) && $filtersAndSort['time_to'] != '') {
            $filter_info->filter_fields[] = 'time_to';
            $date = date('Y-m-d H:i:s', strtotime($filtersAndSort['time_to']. ' +1 day'));
            if ($page->template == 'downloads') {
                $query = $query->where('f.updated_at', '<=', $date);
            } else {
                $query = $query->where('page.updated_at', '<=', $date);
            }
        }

        if (isset($filtersAndSort['file_type']) && is_array($filtersAndSort['file_type']) && count($filtersAndSort['file_type']) > 0) {
            $filter_info->filter_fields[] = 'file_type';
            $query = $query->whereIn('page.file_type', $filtersAndSort['file_type']);
        }

        if (isset($filtersAndSort['usage']) && is_array($filtersAndSort['usage']) && count($filtersAndSort['usage']) > 0) {
            $filter_info->filter_fields[] = 'usage';
            $query = $query->whereIn('page.usage', $filtersAndSort['usage']);
        }

        $serachedPages = $query->get();

        foreach ($serachedPages as $key => $pageChild) {
            $u_ids[] = $pageChild->u_id;

            $tmpPage = FEOverviewPageHelper::generateOverviewPageData($pageChild, $defaultBrandThumbnail);

            if ($page->template == 'downloads') {
                $tmpPage['has_access'] = UserAccessHelper::authUserHasAccess($pageChild);
            }

            $blocks[$pageChild->id] = $tmpPage;
        }

        if (count($serachedPages)> 0) {
            if ($page->template == 'downloads') {
                FETeaserHelper::generateDownloadOverviewTeasers($u_ids, $blocks);
                FETeaserHelper::generateDownloadExtensionImage($u_ids, $blocks, $defaultBrandThumbnail);
            } else {
                FETeaserHelper::generateOverviewTeasers($u_ids, $blocks, $defaultBrandThumbnail);
            }
        }

        $groupBlocks = [];

        if (count($blocks) > 0) {
            $groupBlocks[0] = $blocks;
        }

        // Sort the result
        list($sort_field, $sort_type) = $filter_info->filter_sort = self::getFiltredSortFieldAndType($filtersAndSort);

        if ($sort_field != '' && isset($groupBlocks[0]) && count($groupBlocks[0]) > 0) {
            usort(
                $groupBlocks[0],
                function ($a, $b) use ($sort_field, $sort_type) {
                    if ($a[$sort_field] == $b[$sort_field]) {
                        return 0;
                    }
                    if ($sort_field == 'date') {
                        if ($sort_type == 'asc') {
                            return (strtotime($a[$sort_field]) > strtotime($b[$sort_field])) ? +1 : -1;
                        } else {
                            return (strtotime($a[$sort_field]) < strtotime($b[$sort_field])) ? +1 : -1;
                        }
                    } else {
                        if ($sort_type == 'asc') {
                            return ($a[$sort_field] > $b[$sort_field]) ? +1 : -1;
                        } else {
                            return ($a[$sort_field] < $b[$sort_field]) ? +1 : -1;
                        }
                    }
                }
            );
        }

        return $groupBlocks;
    }
    /************************************************************************
     * Generate tag filters for page download and best practise
     * @return ordered tags
     ***********************************************************************/
    public static function getPageNavigationTagsFilters($page, $sectionPIndex, $ascendants, $sectionId, $nav_tags, $isFilterSet){
        $children = [];
        if ($page->overview) {
            if (isset($sectionPIndex[$page->id])) {
                $children = $sectionPIndex[$page->id];
            }
        } else {
            foreach ($ascendants as $ancestor) {
                if ($ancestor->parent_id == $sectionId) {
                    if (isset($sectionPIndex[$ancestor->id])) {
                        $children = $sectionPIndex[$ancestor->id];
                    }
                }
            }
        }
        $tagsBlock = [];
        foreach ($children as $child) {
            if ($child->overview == 1) {
                if (isset($sectionPIndex[$child->id])) {
                    $grandchildren = $sectionPIndex[$child->id];
                    foreach ($grandchildren as $grandchild) {
                        if ($grandchild->overview == 1 && isset($sectionPIndex[$grandchild->id])) {
                            $greatGrandchildrens = $sectionPIndex[$grandchild->id];
                            foreach ($greatGrandchildrens as $key => $greatGrandchildren) {
                                if ($page->overview == 0 || $isFilterSet) {
                                    $tagsBlock[] = $greatGrandchildren->joinTags();
                                }
                                $GLOBALS['usage_filters'][$greatGrandchildren->usage]  = $greatGrandchildren->usage;
                                $GLOBALS['file_types'][$greatGrandchildren->file_type] = $greatGrandchildren->file_type;
                            }
                        }
                        if ($page->overview == 0 || $isFilterSet) {
                            $tagsBlock[] = $grandchild->joinTags();
                        }
                        $GLOBALS['usage_filters'][$grandchild->usage]  = $grandchild->usage;
                        $GLOBALS['file_types'][$grandchild->file_type] = $grandchild->file_type;
                    }
                }
            } else {
                if ($page->overview == 0 || $isFilterSet) {
                    $tagsBlock[] = $child->joinTags();
                }
                $GLOBALS['usage_filters'][$child->usage]  = $child->usage;
                $GLOBALS['file_types'][$child->file_type] = $child->file_type;
            }
        }
        unset($GLOBALS['usage_filters'][0]);
        unset($GLOBALS['file_types'][0]);
        $GLOBALS['usage_filters'] = array_filter($GLOBALS['usage_filters'], 'strlen');
        $GLOBALS['file_types']    = array_filter($GLOBALS['file_types'], 'strlen');
        if (count($nav_tags) == 0) {
            $nav_tags = self::generateNavigationTags($tagsBlock);
        }
        return $nav_tags;
    }
    /*     * **********************************************************************
     * Generate title for filtered result
     * @param $filter_info	Object for filter fileds and sort
     * @param $blocks	array of results
     * @return Sting Title for Best Prictice and Downloads
     * ********************************************************************* */
    public static function getFiltredResultTitle($filter_info, array $blocks)
    {
        $count = 0;
        foreach ($blocks as $key => $block_group) {
            $count += count($block_group);
        }

        $hasFilterFields = false;
        $hasFilterSort   = false;
        if (isset($filter_info->filter_fields) && count($filter_info->filter_fields) > 0) {
            $hasFilterFields = true;
        }
        if (isset($filter_info->filter_sort) && count($filter_info->filter_sort) > 0 && $filter_info->filter_sort[0] != '') {
            $hasFilterSort   = true;
        }
        if ($hasFilterFields && $hasFilterSort) {
            return trans('template.filter_result.fields_sort', ['result_count' => $count]);
        } elseif ($hasFilterFields) {
            return trans('template.filter_result.fields', ['result_count' => $count]);
        } else {
            return trans('template.filter_result.sort', ['result_count' => $count]);
        }
    }

        /************************************************************************
     * Order navigation tags
     * @return
     ***********************************************************************/
    public static function generateNavigationTags($blocks)
    {
        $nav_tags = [];
        //print_r($blocks);die;
        foreach ($blocks as $key => $block) {
            if ($block != '') {
                $tag_array = explode(',', $block);
                $nav_tags[] = $tag_array[0];
            }
        }
        $nav_tags = array_unique($nav_tags);
        usort($nav_tags, function ($a, $b) {
            return strtolower($a) > strtolower($b);
        });
        return $nav_tags;
        //asort($nav_tags,SORT_STRING |  SORT_NATURAL);
    }

    private static function getFiltredSortFieldAndType(array $req)
    {
        $sort_field = '';
        $sort_type  = '';

        if (isset($req['title']) && $req['title'] != '') {
            $sort_field = 'title';
            $sort_type  = $req['title'];
        } elseif (isset($req['time']) && $req['time'] != '') {
            $sort_field = 'date';
            $sort_type  = $req['time'];
        } elseif (isset($req['viewed']) && $req['viewed'] != '') {
            $sort_field = 'visits';
            $sort_type  = $req['viewed'];
        } elseif (isset($req['rating']) && $req['rating'] != '') {
            $sort_field = 'ratings';
            $sort_type  = $req['rating'];
        } elseif (isset($req['comments']) && $req['comments'] != '') {
            $sort_field = 'comments';
            $sort_type  = $req['comments'];
        }

        return [$sort_field, $sort_type];
    }
}

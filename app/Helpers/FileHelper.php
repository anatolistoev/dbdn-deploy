<?php

namespace App\Helpers;

use App\Models\FileItem;
use App\Http\Controllers\FileController;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;

use Intervention\Image\ImageManagerStatic as Image;

// import the Intervention Image Manager Class

/**
 * Description of FileReconizerHelper
 *
 * @author m.stefanov
 */
class FileHelper
{
    const MODE_BRAND_THUMB = 'brand_thumb';

    const MIN_IMAGE_FILE_SIZE = 5;
    const MAX_PIXEL_RATIO = 3;
    const HIRES_FACTOR_SEPARATOR = '~';
    const HIRES_FACTOR_STRING = '~2x';
    const HIRES_FACTOR_3x_STRING = '~3x';
    const DEFAULT_CACHE_IMAGE_FORMAT = '.jpg';
    const PNG_CACHE_IMAGE_FORMAT = '.png';

    const CACHE_FOLDER = '/__cache/';
    const DEFAULT_IMAGE_NOT_FOUND_PATH = '/img/image_not_found.jpg';

    // Split Image size and HiRes factor
    public static function splitImageSizeAndHiRes($imageSizeAndHiRes)
    {
        $imageSize_array = explode(self::HIRES_FACTOR_SEPARATOR, $imageSizeAndHiRes);
        if (count($imageSize_array) >= 2) {
            $imageSize = $imageSize_array[0];
            $hires_factor = intval(ltrim($imageSize_array[1], 'x'));
            if ($hires_factor < 1) {
                $hires_factor = 1;
            }
            if ($hires_factor > 3) {
                $hires_factor = 3;
            }
        } else {
            $imageSize = $imageSizeAndHiRes;
            $hires_factor = 1;
        }

        return [$imageSize, $hires_factor];
    }

    // Store Zoom for Download
    public static function storeDownloadFileImage($srcImageFilePath, &$fileItem)
    {
        $success = true;
        Log::debug('Store Zoom for Download from: ' . $srcImageFilePath);
        $isConvertImage = false;

        $filePath = $fileItem->filename;
        $fileDir = dirname($filePath) . '/';
        $fileName = basename($srcImageFilePath);
        $fileItem->zoompic = $fileDir . FileController::FOLDER_NAME_ZOOMPIC . '/' . $fileName;

        // Generate thumb name
        $zoomPath = public_path() . '/' . $fileItem->getZoomPath(FILEITEM_REAL_PATH);
        // Zoom image
        if ($isConvertImage) {
            $image = Image::make($srcImageFilePath);

            $refSizeArr = config('assets.' . $filemanager_mode . '.file_sizes');
            $sizeArr = config('assets.' . $filemanager_mode . '.sizes');
            self::_resizeImage($image, $zoomPath, $sizeArr[$refSizeArr['zoom_pic']]);
            $image->destroy();
            $success = $success && true;
        } else {
            self::saveFile($srcImageFilePath, $zoomPath);
        }
        return $success;
    }

    public static function getCachePathForImage(string $filemanager_mode, FileItem $file, string $imageSize)
    {
        $originalFile = FileHelper::findSourceImage(FileController::FOLDER_NAME_ZOOMPIC, $file, $file->protected);
        $file_path = substr($originalFile, strlen(public_path()) + 1);
        $imageType = FileHelper::getFileExtension($originalFile);
        //Get path for pdf for protected images
        if ($file->protected) {
            $replace = config('assets.'.$filemanager_mode.'.sizes.'.$imageSize.'.prefix');
            $file_path_cached = substr_replace($file_path, $replace, 22, 0) . $imageType;
        } else {
            $file_path_cached = UriHelper::generateImgPath($file_path, $filemanager_mode, $imageSize, $imageType, false);
        }

        $posCache = strlen($filemanager_mode) + (($file->protected)? 17 : 7);
        $file_path = substr($file_path_cached, $posCache);
        return $file_path; 
    }

    public static function getCacheURLForImage(string $filemanager_mode, FileItem $file, string $imageSize)
    {
        //Get URL for $imageSize for image
        if ($file->protected) {
            $imgUrl = UriHelper::generateImgPath($file->getCachedThumbPath('{type}'), FileController::MODE_IMAGES, $imageSize, null, true);
        } else {
            $imagePath = $file->getPath();
            $imageType = FileHelper::getFileExtension($imagePath);
            $imgUrl = UriHelper::generateImgPath($imagePath, FileController::MODE_IMAGES, $imageSize, $imageType);
        }

        return $imgUrl;
    }

    public static function generateCacheImage($filemanager_mode, $file_path_cached, $isProtected = false)
    {
        //TODO: Check if $file_path_cached is for file from the cache!
        $filePathCached = FileController::getFullPath($filemanager_mode, '/' . $file_path_cached, $isProtected);

        if (!is_file($filePathCached)) { // Check if file exists
            $file_path_cached_array      = explode('/', $file_path_cached);
            $file_path_cached_array_size = count($file_path_cached_array);

            // Check for cached image
            if ($file_path_cached_array_size >= 3 && $file_path_cached_array[0] === FileController::FOLDER_NAME_CACHE) {
                $imageSizeAndHiRes = $file_path_cached_array[1];
                $sizeArr           = config('assets.' . $filemanager_mode . '.sizes');

                // Check for HiRes factor
                list($imageSize, $hires_factor) = self::splitImageSizeAndHiRes($imageSizeAndHiRes);

                if (isset($sizeArr[$imageSize])) {
                    $imgConf                                                  = $sizeArr[$imageSize];
                    $srcFileName                                              = $file_path_cached_array[$file_path_cached_array_size - 1];
                    // Calculate Master File Name
                    $file_path_cached_array[$file_path_cached_array_size - 1] = pathinfo($srcFileName, PATHINFO_FILENAME);
                    $master_file_path                                         = implode('/', array_slice($file_path_cached_array, 2));
                    $masterFilePathAbs                                        = FileController::getFullPath(
                        $filemanager_mode,
                        '/' . $master_file_path,
                        $isProtected
                    );
                    $notFoundFilePathCachedAbs = FileController::getFullPath($filemanager_mode, $imgConf['prefix'] . self::DEFAULT_IMAGE_NOT_FOUND_PATH, $isProtected);
                    if (is_file($masterFilePathAbs)) {
                        //Log::info("Image master path: " . $masterFilePathAbs);
                        //Load $fileItem from DB
                        $searchField = 'filename';
                        $zoomOrThumb = $file_path_cached_array[$file_path_cached_array_size - 2];
                        if ($zoomOrThumb == FileController::FOLDER_NAME_ZOOMPIC) {
                            $searchField = 'zoompic';
                        } else if ($zoomOrThumb == FileController::FOLDER_NAME_THUMBNAIL) {
                            $searchField = 'thumbnail';
                        }
                        $fileItem = FileItem::where($searchField, '/' . $master_file_path)->first();

                        if ($fileItem==null) {
                            $fileItem = FileItem::where('id', '14552')->first();
                        }
//                        if (empty($fileItem->thumbnail) || empty($fileItem->zoompic)) {
//                            if (strtolower($fileItem->type) === FileController::MODE_DOWNLOADS) {
//                                //TODO: Search in Zip for Image
//                                //Save modified $fileItem to DB
//                                //$fileItem->save();
//                            }
//                        }

                        $srcFilePathAbs = self::findSourceImage($imgConf['file_folder'], $fileItem, $isProtected);

                        if (!is_file($srcFilePathAbs) || (filesize($srcFilePathAbs) <= self::MIN_IMAGE_FILE_SIZE) ) {
                            Log::warning("Missing Source Image file: " . $srcFilePathAbs);
                            $isImageNotFound = true;
                        } else {
                            $isImageNotFound = false;
                        }
                    } else {
                        Log::warning("Missing master path: " . $masterFilePathAbs);
                        $isImageNotFound = true;
                    }

                    if ($isImageNotFound) {
                        if (is_file($notFoundFilePathCachedAbs)) {
                            return $notFoundFilePathCachedAbs;
                        }
                        $filePathCached = $notFoundFilePathCachedAbs;
                        $srcFilePathAbs = public_path() . self::DEFAULT_IMAGE_NOT_FOUND_PATH;
                    }
                    //Create the dir path to the file
                    $dirPathCached = dirname($filePathCached);
                    self::makedirs($dirPathCached, FILE_DEFAULT_ACCESS);

                    //Resize the image
                    self::resizeImage($srcFilePathAbs, $filePathCached, $imgConf, $hires_factor);
                } else {
                    Log::warning("Missing asset path: " . $file_path_cached);
                }
            } else {
                //TODO: File Not found! Check for download file or image!
                //Response::
                //throw new \Exception();
                //$filePathCached = public_path() . self::DEFAULT_IMAGE_NOT_FOUND_PATH;
            }
        }

        return $filePathCached;
    }

    static function findSourceImage($srcImageType, FileItem $fileItem, $isProtected)
    {
        $filemanager_mode = strtolower($fileItem->type);
        if ($srcImageType == FileController::FOLDER_NAME_ZOOMPIC) {
            //TODO: Implement this for Preview of Images
            $srcImageName = $fileItem->zoompic;
            if ($filemanager_mode == FileController::MODE_DOWNLOADS) {
                if (empty($fileItem->zoompic)) {
                    if (empty($fileItem->thumbnail)) {
                        $srcImageName = $fileItem->filename;
                    }
                }
            } else {
                if (empty($fileItem->zoompic)) {
                    $srcImageName = $fileItem->filename;
                }
            }
        } elseif ($srcImageType == FileController::FOLDER_NAME_THUMBNAIL) {
            $srcImageName = $fileItem->thumbnail;
            if (empty($srcImageName)) {
                if ($filemanager_mode == FileController::MODE_DOWNLOADS) {
                    $srcImageName = $fileItem->zoompic;
                    if (empty($srcImageName)) {
                        $srcImageName = $fileItem->filename;
                        //TODO: Check if Download file is image!
                    }
                } else {
                    $srcImageName = $fileItem->filename;
                }
            }
        } else {
            $srcImageName = $fileItem->filename;
        }
        $srcFilePathAbs = FileController::getFullPath($filemanager_mode, $srcImageName, $isProtected);
        //Log::debug("New Source Image path: " . $srcFilePathAbs);
        return $srcFilePathAbs;
    }

    public static function resizeImage($srcFilePath, $destFilePath, $imgConf, $hires_factor = 1)
    {
        // load image
        $image = Image::make($srcFilePath);

        if (isset($imgConf['crop'])) {
            // Resize down only and keep aspect ratio! Crop the image.
            $imageZoom = $image->fit($imgConf['width']*$hires_factor, $imgConf['height']*$hires_factor, function ($constraint) {
                //$constraint->upsize();
            }, $imgConf['crop']);
        } else {
            // Resize down only and keep aspect ratio!
            $imageZoom = $image->resize($imgConf['width']*$hires_factor, $imgConf['height']*$hires_factor, function ($constraint) {
                $constraint->aspectRatio();
                //$constraint->upsize();
            });
        }
        // save file as jpg with given quality
        $imageZoom->save($destFilePath, $imgConf['quality']);
        $imageZoom->destroy();
    }

    /*
	 * FileSystem Functions
	 */
    public static function tempdir($dir, $prefix = '', $mode = 0700)
    {
        if (substr($dir, -1) != '/') {
            $dir .= '/';
        }

        do {
            $path = $dir.$prefix.mt_rand(0, 9999999);
        } while (!mkdir($path, $mode));

        return $path;
    }

    public static function safeDeleteFolder($dir)
    {
        $result = false;
        if (!is_dir($dir)) {
            Log::warning('Missing folder: ' . $dir );
            return;
        }
        // Rename folder
        $path = rtrim($dir, '/\\').'_temp_'.mt_rand(0, 9999999);
        if (rename($dir, $path)) {
            $result = self::delTree($path);
        } else {
            $path = $dir;
            $result = self::delTree($path);
        }
        if (!$result) {
            $files = array_diff(scandir($path), ['.','..']);
            $folderContent = '';
            foreach ($files as $file) {
                $curPath = "$path/$file";
                if (is_dir($curPath)) {
                    $folderContent .= '\nD ' . $file;
                } else {
                    $folderContent .= '\nF ' . $file;
                }
            }
            Log::debug('The content of folder: ' . $path . ' is ' . $folderContent);
        }
        return $result;
    }

    public static function delTree($dir)
    {
        $files = array_diff(scandir($dir), ['.','..']);
        foreach ($files as $file) {
            $path = "$dir/$file";
            if (is_dir($path)) {
                self::delTree($path);
            } else {
                unlink($path);
            }
        }
        return rmdir($dir);
    }

    public static function makedirs($dirpath, $mode = 0777)
    {
        return is_dir($dirpath) || mkdir($dirpath, $mode, true);
    }

    /**
     * Copy a file, or recursively copy a folder and its contents
     * @author      Aidan Lister <aidan@php.net>
     * @version     1.0.1
     * @link        http://aidanlister.com/2004/04/recursively-copying-directories-in-php/
     * @param       string   $source    Source path
     * @param       string   $dest      Destination path
     * @param       string   $permissions New folder creation permissions
     * @return      bool     Returns true on success, false on failure
     */
    public static function xcopy($source, $dest, $permissions = 0755, $skipErrors = true, &$errors = [])
    {
        try {
            // Check for symlinks
            if (is_link($source)) {
                return symlink(readlink($source), $dest);
            }

            // Simple copy for a file
            if (is_file($source)) {
                return copy($source, $dest);
            }

            // Make destination directory
            if (!is_dir($dest)) {
                mkdir($dest, $permissions);
            }
        } catch (\Exception $exc) {
            $errors[] = $exc->getMessage();
            if (!$skipErrors) {
                throw $exc;
            }
        }

        // Loop through the folder
        $dir = dir($source);
        while (false !== $entry = $dir->read()) {
            // Skip pointers
            if ($entry == '.' || $entry == '..') {
                continue;
            }

            try {
                // Deep copy directories
                self::xcopy("$source/$entry", "$dest/$entry", $permissions, $skipErrors, $errors);
            } catch (\Exception $exc) {
                $errors[] = $exc->getMessage();
                if (!$skipErrors) {
                    throw $exc;
                }
            }
        }

        // Clean up
        $dir->close();
        return true;
    }

    public static function getFileExtension($path)
    {
        $imageType = strtolower('.'.pathinfo($path, PATHINFO_EXTENSION)) == FileHelper::PNG_CACHE_IMAGE_FORMAT?FileHelper::PNG_CACHE_IMAGE_FORMAT: FileHelper::DEFAULT_CACHE_IMAGE_FORMAT;
        return $imageType;
    }

    public static function saveFile($sourcePath, $localPath)
    {
        $result = @copy($sourcePath, $localPath);
        if (!$result) {
            $errors= error_get_last();
            Log::error("COPY ERROR: ".$errors['type'] . "\n".$errors['message']);
        } else {
            Log::info("File: " . $localPath . " copied succesfuly.");
        }

        return $result;
    }

    public static function formatExtension($extension)
    {
        $extension = preg_split('/\s*,\s*/', trim($extension));
        return implode(', ', array_unique($extension));
    }

    public static function getFirstExtension($extensions)
    {
        $extension = preg_split('/\s+/', trim($extensions));
        $result = '';
        if (count($extension)> 0) {
            $result = strtoupper($extension[0]);
        }
        return $result;
    }

    public static function formatSize($size)
    {
        $result = $size;
        if (is_numeric($size) && ($base = log(intval($size)) / log(1024)) &&($SIZE_LABELS = ["B", "KB", "MB", "GB", "TB"])) {
            $result = round(pow(1024, $base - floor($base)), 2) .' '. $SIZE_LABELS[floor($base)];
        }
        return $result;
    }

    public static function getImageSize($imagePath): array
    {
        $image_info = @getimagesize($imagePath);
        if ($image_info) {
            $width = $image_info[0];
            $height = $image_info[1];
        } else {
            //TODO: Get size from Asset
            $width = 492;
            $height = 345;
        }

        if ($width > 492 || $height > 345) {
            $ratio = min(492 / $width, 345 / $height);
            $width = round($width*$ratio);
            $height = round($height*$ratio);
        }

        return array($width, $height);
    }

    public static function getSysTempDir($withDS = false)
    {
        // In function sys_get_temp_dir() there is bug in PHP 5.3 we cannot use if temp is changed
        $temp_folder = ini_get('upload_tmp_dir');

        if (empty($temp_folder) || !is_dir($temp_folder) || !is_writable($temp_folder)) {
            $temp_folder = sys_get_temp_dir();
        }

        if (empty($temp_folder) || !is_dir($temp_folder) || !is_writable($temp_folder)) {
            $temp_folder = '/tmp';
        }

        $temp_folder = rtrim($temp_folder, '\\/');

        if (!is_dir($temp_folder) || !is_writable($temp_folder)) {
            throw new \Exception('Could not get system temp folder', 1);
        }

        return $temp_folder . ($withDS ? DIRECTORY_SEPARATOR : '');
    }

    public static function createTempDir($name, $withDS = false, $permissions = 0777, $emptyContent = false)
    {
        // you can pass null for some arguments just to use next one(s)
        if (null === $withDS) {
            $withDS = false;
        }
        if (null === $permissions) {
            $permissions = 0777;
        }

        $dir = self::getSysTempDir(true).$name;

        if (!is_dir($dir)) {
            // funny - why within try.. catch?
            try {
                if (!mkdir($dir, $permissions, true)) {
                    throw new \Exception('Could not create temp folder '.$dir, 1);
                }
            } catch (\Exception $e) {
                throw new \Exception('Could not create temp folder '.$dir, 1);
            }
        }

        chmod($dir, $permissions);

        return $dir . ($withDS ? DIRECTORY_SEPARATOR : '');
    }

    public static function sendFileHeaders($file, $forceDownload = false)
    {
        if (!file_exists($file)) {
            throw new \Exception('File does not exists '.$file, 1);
        }
        $strictDownload = [
                'application/zip',
                'application/pdf',
            ];

        $mime = mime_content_type($file);

        if (ob_get_level()) {
            ob_end_clean();
        }
        session_write_close();

        header('Content-type: ' . $mime);
        header('Content-Length: ' . filesize($file));

        if ($forceDownload || in_array($mime, $strictDownload)) {
            header('Content-Transfer-Encoding: Binary');
            header('Content-Disposition: attachment; filename="'. basename($file) .'"');
        } else {
            header('Content-Disposition: inline');
        }
    }

    public static function getSVGContent($path)
    {
        $svg_file = file_get_contents($path);
        $find_string = '<svg';
        $position = strpos($svg_file, $find_string);
        $svg_file_new = substr($svg_file, $position);
        return $svg_file_new;
    }

    public static function safe_dirname($path)
    {
        $dirname = dirname($path);

        return $dirname == DIRECTORY_SEPARATOR ? '' : $dirname;
    }

    public static function moveProtectedFile(bool $isProtected, FileItem $fileItem, string $filemanager_mode)
    {
        $currentpath = $fileItem->filename;
        $currentFile = FileController::getFullPath($filemanager_mode, $currentpath, !$isProtected);
        if (File::exists($currentFile)) {
            $destFilePath = FileController::getFullPath($filemanager_mode, $currentpath, $isProtected);
            if (FileHelper::moveFile($currentFile, $destFilePath)) {
                if ($fileItem->zoompic != "") {
                    $currentZoompic = FileController::getFullPath(
                        $filemanager_mode,
                        $fileItem->zoompic,
                        !$isProtected
                    );
                    if (File::exists($currentZoompic)) {
                        if (!FileHelper::moveFile(
                            $currentZoompic,
                            FileController::getFullPath(
                                $filemanager_mode,
                                $fileItem->zoompic,
                                $isProtected
                            )
                        )
                        ) {
                            throw new \Exception("Zoom picture couldn't move to folder");
                        }
                    } else {
                        throw new \Exception("Zoompic doesn't exists: Path " . $currentZoompic);
                    }
                }
                if ($fileItem->thumbnail != "") {
                    $currentThumbnail = FileController::getFullPath(
                        $filemanager_mode,
                        $fileItem->thumbnail,
                        !$isProtected
                    );
                    if (File::exists($currentThumbnail)) {
                        if (!FileHelper::moveFile(
                            $currentThumbnail,
                            FileController::getFullPath(
                                $filemanager_mode,
                                $fileItem->thumbnail,
                                $isProtected
                            )
                        )
                        ) {
                            throw new \Exception("Thumbnail couldn't move to folder");
                        }
                    } else {
                        throw new \Exception("Thumbnail doesn't exists: Path " . $currentThumbnail);
                    }
                }
                if($filemanager_mode == FileController::MODE_IMAGES){
                    self::deleteImageCashe($filemanager_mode, $fileItem);
                }
            } else {
                throw new \Exception("File couldn't be move to folder");
            }
        } else {
            throw new \Exception("File doesn't exists: Path " . $currentFile);
        }
    }

    /**
	 *  Move file to new location and create the path to new location.
     * 
     * @param       string   $srcFilePath    Source path
     * @param       string   $destFilePath   Destination path
     * @return      bool     Returns true on success, false on failure
	 */
    public static function moveFile(string $srcFilePath, string $destFilePath): bool
    {
        //Create the dir path to the file
        $dirPathCached = dirname($destFilePath);
        self::makedirs($dirPathCached, FILE_DEFAULT_ACCESS);
        return File::move($srcFilePath, $destFilePath);
    }

    /*
	 *  Private Functions
	 */
    private static function _resizeImage($srcImage, $saveImagePath, $imgConf, $hires_factor = 2)
    {
        if (!is_file($saveImagePath)) {
            //Create the dir path to the file
            $dirPath = dirname($saveImagePath);
            self::makedirs($dirPath, FILE_DEFAULT_ACCESS);

            // Resize down only and keep aspect ratio!
            $imageResized = $srcImage->resize($imgConf['width'] * $hires_factor, $imgConf['height'] * $hires_factor, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            // save file as jpg with given quality
            $imageResized->save($saveImagePath, $imgConf['quality']);
            $imageResized->destroy();
        }
    }

    private static function deleteImageCashe(string $filemanager_mode, FileItem $fileItem){
        $sizeArr = config('assets.' . $filemanager_mode . '.sizes');
        $arrFactor = ['', self::HIRES_FACTOR_SEPARATOR.'1x', self::HIRES_FACTOR_STRING, self::HIRES_FACTOR_3x_STRING];
        foreach ($sizeArr as $size){
            foreach ($arrFactor as $factor) {
                $imageType = FileHelper::getFileExtension($fileItem->filename);
                $relativeCashePath = $size['prefix'].$factor.$fileItem->filename.$imageType;
                $cashedFileFullPath = FileController::getFullPath($filemanager_mode, $relativeCashePath, !$fileItem->protected);
                Log::debug('Delete image cashe path: ' . $cashedFileFullPath);
                if (file_exists($cashedFileFullPath)) {
                    unlink($cashedFileFullPath);
                }
            }
        }

    }

}

<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request as HTTPRequest;

class UriHelper
{
    
    public static function converturi($str)
    {
        $str = trim($str);
        $s = htmlentities($str, ENT_NOQUOTES, 'UTF-8');

      //German special letters
        $s = str_replace("&szlig;", "ss", $s);
        $s = str_replace("&auml;", "a", $s);
        $s = str_replace("&ouml;", "o", $s);
        $s = str_replace("&uuml;", "u", $s);
        $s = str_replace("&Auml;", "a", $s);
        $s = str_replace("&Ouml;", "o", $s);
        $s = str_replace("&Uuml;", "u", $s);


      //French special letters
        $s = str_replace("&Agrave;", "a", $s);
        $s = str_replace("&agrave;", "a", $s);
        $s = str_replace("&Acirc;", "a", $s);
        $s = str_replace("&acirc;", "a", $s);
        $s = str_replace("&AElig;", "ae", $s);
        $s = str_replace("&aelig;", "ae", $s);
        $s = str_replace("&Ccedil;", "c", $s);
        $s = str_replace("&ccedil;", "c", $s);
        $s = str_replace("&Egrave;", "e", $s);
        $s = str_replace("&egrave;", "e", $s);
        $s = str_replace("&Eacute;", "e", $s);
        $s = str_replace("&eacute;", "e", $s);
        $s = str_replace("&Ecirc;", "e", $s);
        $s = str_replace("&ecirc;", "e", $s);
        $s = str_replace("&Euml;", "e", $s);
        $s = str_replace("&euml;", "e", $s);
        $s = str_replace("&Icirc;", "i", $s);
        $s = str_replace("&icirc;", "i", $s);
        $s = str_replace("&Iuml;", "i", $s);
        $s = str_replace("&iuml;", "i", $s);
        $s = str_replace("&Ocirc;", "o", $s);
        $s = str_replace("&ocirc;", "o", $s);
        $s = str_replace("&OElig;", "oe", $s);
        $s = str_replace("&oelig;", "oe", $s);
        $s = str_replace("&Ugrave;", "u", $s);
        $s = str_replace("&ugrave;", "u", $s);
        $s = str_replace("&Ucirc;", "u", $s);
        $s = str_replace("&ucirc;", "u", $s);
        $s = str_replace("&Uuml;", "u", $s);
        $s = str_replace("&uuml;", "u", $s);

        $s = str_replace("&lsquo;", "-", $s);
        $s = str_replace("&rsquo;", "-", $s);

        $s = strtolower($s);
      //$GLOBALS['log']->log('s: '.$s, PEAR_LOG_WARNING);
        $e = '';
        for ($i=0; $i<strlen($s); $i++) {
            $c = $s[$i];
            $o = ord($c);
            if (($o <= 122 && $o >= 97) || ($o <= 57 && $o >= 48) || ($o == 95) || ($o == 150) || ($o == 45)) {
                $e .= $c;
            } elseif ($o == 32 || $o == 160) {
                //$e .= '_';
                $e .= '-';
            } // ה -> ae
            elseif ($o == 228) {
                $e .= 'ae';
            } // צ -> oe
            elseif ($o == 246) {
                $e .= 'oe';
            } //  -> ue
            elseif ($o == 252) {
                $e .= 'ue';
            } // א,ב,ג,ד,ו,ז -> a
            elseif ($o <= 230 && $o >= 224) {
                $e .= 'a';
            } //ט,י,ך,כ -> e
            elseif ($o <= 235 && $o >= 232) {
                $e .= 'e';
            } //ל,ם,מ,ן -> i
            elseif ($o <= 235 && $o >= 232) {
                $e .= 'i';
            } //ח -> c
            elseif ($o == 231) {
                $e .= '$o';
            } //ס -> n
            elseif ($o == 241) {
                $e .= 'n';
            } //ע,ף,פ,ץ,צ -> o
            elseif ($o <= 246 && $o >= 242) {
                $e .= 'o';
            } //ש,ת,,
            elseif ($o <= 252 && $o >= 249) {
                $e .= 'u';
            } // lines
            elseif ($o == 8211 || $o == 8212) {
                $e .= '-';
            }
            // all others
    //		else
    //			$e .= '-';
        }
        while (0 < strpos($e, "--")) {
            $e = str_replace("--", "-", $e);
        }

        return $e;
    }
    
    /*
     * Make Image URL from Brnd Thumbs, Images and Downloads
     * 
     * Brand Thumb: img/template/thumbnails/{type}/Daimler_Thumbnail.png
     *      Images: files/images/{type}/695/Daimler_Brochures_DINA5_P.zip.jpg
     *   Downloads: files/downloads/{type}/695/Daimler_Brochures_DINA5_P.zip.jpg
     * 
     *      {type}: __cache/content ; __cache/content~2x
     * 
     */
    public static function generateImgPath($path, $mode, $imageSize, $fileExtension, $isProtected = false, $hiResFactor = 1)
    {
        $result = "";
        if ($isProtected) {
            if (strrpos($path, "/{type}")) {
                $result = str_replace('{type}', $imageSize, $path);
            } else {
                $result = $path;
            }
        } else {
            $replace = config('assets.'.$mode.'.sizes.'.$imageSize.'.prefix');
            if ($hiResFactor == 2) {
                $replace .= FileHelper::HIRES_FACTOR_STRING;
            } else if ($hiResFactor == 3) {
                $replace .= FileHelper::HIRES_FACTOR_3x_STRING;
            }
            if (strrpos($path, "/{type}")) {
                $result = str_replace('/{type}', $replace, $path);
            } else {
                // Workaround for path from cache
                if (strpos($path, FileHelper::CACHE_FOLDER) !== false) {
                    $result = $path;
                } else {
                    $result = substr_replace($path, $replace, config('assets.'.$mode.'.cache_insert_index'), 0) . $fileExtension;
                }
            }
        }
        
        return $result;
    }
    
    public static function getMasterFilePath($cachedPath, $imageSize = 'content')
    {
        $imgSource = str_replace(config('assets.images.sizes.' . $imageSize . '.prefix'), '', $cachedPath);
        if (strlen($imgSource) < strlen($cachedPath)) {
            $imgSource = substr($imgSource, 0, strrpos($imgSource, '.'));
        }

        return $imgSource;
    }

    public static function getBlurredPreviewUrl($url)
    {
        // TASK: replace the folder after __cache (here carousel) with blurred_preview
        //  sample URL: files/downloads/__cache/carousel/Daimler/Electric_Charging_Stations/Daimler_Elektroladestationen_DE.zip.jpg
        //              protected-file/11047/content?policy=$2y$10$od5q3GCzbGSkEuAuiuTYZetSGVMORTFhH/rMjm/uv1t8SouVLxJA6
        if ($url == null) {
            return '';
        } else {
            return UriHelper::changeImageSizeUrl($url, 'blurred_preview');
        }
    }

    /** 
     * Replace the imageSize after '__cache' or 'protected-file/{int}/' with $newImgSize
     * 
     *  sample URL: files/downloads/__cache/carousel/Daimler/Electric_Charging_Stations/Daimler_Elektroladestationen_DE.zip.jpg
     *              protected-file/11047/content?policy=$2y$10$od5q3GCzbGSkEuAuiuTYZetSGVMORTFhH/rMjm/uv1t8SouVLxJA6
     **/ 
    public static function changeImageSizeUrl(string $url, string $newImgSize): string
    {
        if (empty($url)) {
            $e = new \Exception("Empty image url: " . $url);
            Log::error($e);
        }

        $result = null;
        if (strrpos($url, "/{type}") !== false) {
            $result = str_replace('{type}', $newImgSize, $url);
        } else {
            //Check for protected images!
            $patternProtected = 'protected-file/';
            $posProtected = strrpos($url, $patternProtected);
            if ($posProtected !== false) {
                $startIndex = strpos($url, '/',  $posProtected + strlen($patternProtected));
                $length = strpos($url, '?', $startIndex) - $startIndex;
                if (0 < $startIndex && 0 < $length) {
                    $searchImgSize = substr($url, $startIndex + 1, $length - 1);
                    $toReplace = '/' . $searchImgSize . '?';
                    $result = str_replace($toReplace, '/' . $newImgSize . '?', $url);
                }
            } else {
                $cacheFolderName = '__cache/';
                $posCache = strpos($url, $cacheFolderName);
                if ($posCache !== false) {
                    $startIndex = $posCache + strlen($cacheFolderName);
                    $length = strpos($url, '/', $startIndex) - $startIndex;
                    if (0 < $startIndex && 0 < $length) {
                        $folderToReplace = substr($url, $startIndex, $length);
                        $result = str_replace($cacheFolderName.$folderToReplace, $cacheFolderName.$newImgSize, $url);
                    }
                }
            }
        }

        if ($result == null || $result == $url) {
            $result = $url;
            Log::debug('Cannot change URL: ' . $url);
        }
        return $result;
    }

    public static function getClientIpAddr(HTTPRequest $request) : string {
        $ip = $request->headers->get("X-Forwarded-For");

        if ($ip == null || strlen($ip) == 0 || strcasecmp($ip, "unknown")) {
           $ip = $request->headers->get("HTTP_X_FORWARDED_FOR");  
        }

        if ($ip == null || strlen($ip) == 0 || strcasecmp($ip, "unknown")) {
           $ip = $request->getClientIp();
        }

        return $ip;
     }

}

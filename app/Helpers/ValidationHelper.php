<?php

namespace App\Helpers;

/**
 * Base validation service
 *
 * @author Martin Stefanov
 */
class ValidationHelper
{
    public static function getFEValidationTranslation()
    {
        $locale = \Illuminate\Support\Facades\App::getLocale();

        return \Illuminate\Support\Facades\File::getRequire(base_path() . '/resources/lang/' . $locale . '/validation_fe.php');
    }

    /************************************************************************
     *
     *
     ***********************************************************************/
    public static function super_unique($array, $array2): array
    {
        $result = array_map("unserialize", array_unique(array_map("serialize", $array2)));

        foreach ($array as $key => $str) {
            if (!array_key_exists($key, $result)) {
                $array[$key] = ' ';
            }
        }

        return $array;
    }
}

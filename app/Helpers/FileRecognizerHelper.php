<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;

/**
 * Description of FileReconizerHelper
 *
 * @author m.stefanov
 */
class FileRecognizerHelper
{
    protected static $ignored_dirs = ['__MACOSX'];
    protected static $ignored_ext = ['ds_store', 'tmp'];

    /**
     * Dimensions of paper sizes in points
     *
     * @var array;
     */
    protected static $PAPER_SIZES = [
      "4a0" => [0,0,4767.87,6740.79],
      "2a0" => [0,0,3370.39,4767.87],
      "a0" => [0,0,2383.94,3370.39],
      "a1" => [0,0,1683.78,2383.94],
      "a2" => [0,0,1190.55,1683.78],
      "a3" => [0,0,841.89,1190.55],
      "a4" => [0,0,595.28,841.89],
      "a5" => [0,0,419.53,595.28],
      "a6" => [0,0,297.64,419.53],
      "a7" => [0,0,209.76,297.64],
      "a8" => [0,0,147.40,209.76],
      "a9" => [0,0,104.88,147.40],
      "a10" => [0,0,73.70,104.88],
      "b0" => [0,0,2834.65,4008.19],
      "b1" => [0,0,2004.09,2834.65],
      "b2" => [0,0,1417.32,2004.09],
      "b3" => [0,0,1000.63,1417.32],
      "b4" => [0,0,708.66,1000.63],
      "b5" => [0,0,498.90,708.66],
      "b6" => [0,0,354.33,498.90],
      "b7" => [0,0,249.45,354.33],
      "b8" => [0,0,175.75,249.45],
      "b9" => [0,0,124.72,175.75],
      "b10" => [0,0,87.87,124.72],
      "c0" => [0,0,2599.37,3676.54],
      "c1" => [0,0,1836.85,2599.37],
      "c2" => [0,0,1298.27,1836.85],
      "c3" => [0,0,918.43,1298.27],
      "c4" => [0,0,649.13,918.43],
      "c5" => [0,0,459.21,649.13],
      "c6" => [0,0,323.15,459.21],
      "c7" => [0,0,229.61,323.15],
      "c8" => [0,0,161.57,229.61],
      "c9" => [0,0,113.39,161.57],
      "c10" => [0,0,79.37,113.39],
      "ra0" => [0,0,2437.80,3458.27],
      "ra1" => [0,0,1729.13,2437.80],
      "ra2" => [0,0,1218.90,1729.13],
      "ra3" => [0,0,864.57,1218.90],
      "ra4" => [0,0,609.45,864.57],
      "sra0" => [0,0,2551.18,3628.35],
      "sra1" => [0,0,1814.17,2551.18],
      "sra2" => [0,0,1275.59,1814.17],
      "sra3" => [0,0,907.09,1275.59],
      "sra4" => [0,0,637.80,907.09],
      "letter" => [0,0,612.00,792.00],
      "legal" => [0,0,612.00,1008.00],
      "ledger" => [0,0,1224.00, 792.00],
      "tabloid" => [0,0,792.00, 1224.00],
      "executive" => [0,0,521.86,756.00],
      "folio" => [0,0,612.00,936.00],
      "commercial #10 envelope" => [0,0,684,297],
      "catalog #10 1/2 envelope" => [0,0,648,864],
      "8.5x11" => [0,0,612.00,792.00],
      "8.5x14" => [0,0,612.00,1008.0],
      "11x17"  => [0,0,792.00, 1224.00],
    ];

    //Auto fill Fields: ....
    // Possible files are:
    // - zip - Get the first file in root folder (alphabeticaly sorted)
    // - images - PNG, JPG
    public static function recoginizeFields($fileItem)
    {
        $resRecognition = false;
        //
        if (strtolower($fileItem->extension) == 'zip') {
            // Unzip the file
            $zipFileItem = new \ZipArchive();
            //open archive from storage folder
            $res = $zipFileItem->open(public_path() . '/' . $fileItem->getPath(FILEITEM_REAL_PATH));
            if ($res === true) {
                Log::info('Zip files count: ' . $zipFileItem->numFiles);
                //Find files and extensions in ZIP
                $arrFiles = [];
                $arrExtensions = [];
                if (self::_fillFilesAndFileTypesFromZip($zipFileItem, $arrFiles, $arrExtensions)) {
                    // TODO: Main file
                    natcasesort($arrFiles);
                    $recognitionAchiveFileName = $arrFiles[0];
                    // Get list of extensions in Zip file
                    $fileItem->extension = implode(', ', array_keys($arrExtensions));

                    //TODO: Extract main file
                    $juploadConfig = config('app.jupload');
                    try {
                        $tempDirName = FileHelper::tempdir($juploadConfig['upload_dir'], 'dbdn_tmp_');

                        $zipFileItem->extractTo($tempDirName, [$recognitionAchiveFileName]);
                        $tempFileName = $tempDirName.'/'.$recognitionAchiveFileName;

                        //Recognize fields of the file:
                        $resRecognition = self::recoginizeFileFields($tempFileName, $fileItem);

                        //Find first image
                        $imageFileName = self::_findFirstImage($arrFiles, config('assets.image_formats'));
                        if ($imageFileName) {
                            $zipFileItem->extractTo($tempDirName, [$imageFileName]);
                            $imageFilePath = $tempDirName.'/'.$imageFileName;
                            //Generate Images
                            FileHelper::storeDownloadFileImage($imageFilePath, $fileItem, true);
                        }
                    } catch (\Exception $exc) {
                        Log::error('Unable recognize fields: ' . $exc->getTraceAsString());
                    }
                    FileHelper::delTree($tempDirName);
                } else {
                    //TODO: No files found
                    $resRecognition = false;
                }

                $zipFileItem->close();
            } else {
                //TODO: Log for the problem!
                $resRecognition = $res;
            }
        } else {
            $recognitionFilePath = public_path() . '/' . $fileItem->getPath(FILEITEM_REAL_PATH);
            //Recognize fields of the file
            $resRecognition = self::recoginizeFileFields($recognitionFilePath, $fileItem);
        }

        return $resRecognition;
    }

    // Recognize file fields
    public static function recoginizeFileFields($recognitionFilePath, &$fileItem)
    {
        Log::info('Recognize file: ' . $recognitionFilePath);
            // format - Not supported for Images
//TODO?            [version]
        $resRecognition = false;
        $fileExt = strtolower(pathinfo($recognitionFilePath, PATHINFO_EXTENSION));
        switch ($fileExt) {
            case 'png':
            case 'jpg':
            case 'jpeg':
            case 'tiff':
            case 'tif':
                            $resRecognition = self::_recoginizeImageFields($recognitionFilePath, $fileItem);
                break;
            case "pdf":
                $resRecognition = self::_recoginizePdfFields($recognitionFilePath, $fileItem);
                break;
            case 'mov':
            case 'mp4':
            case 'avi':
            case 'wmv':
                        $resRecognition = self::_recoginizeVideoFields($recognitionFilePath, $fileItem);
                break;
        }

        return $resRecognition;
    }

    /*
	 *  Private Functions
	 */

    //Auto fill Fields: ....
    // Possible files are:
    // - images - PNG, JPG, TIFF
    private static function _recoginizeImageFields($recognitionFilePath, $fileItem)
    {
        // resolution_units
        $fileItem->resolution_units = 1; // DPI (ResolutionUnit = 2) we ignore ResolutionUnit = 1 centimeters

        try {
            $image_extra_info = [];
            $image_info = @getimagesize($recognitionFilePath, $image_extra_info);
            if ($image_info) {
                $img_info_values = "IMG:\n";
                foreach ($image_info as $key => $val) {
                    $img_info_values .= "$key: $val\n";
                }
                Log::info($img_info_values);

                //  dimensions and dimension_units
                $fileItem->dimensions = $image_info[0]. 'x' . $image_info[1];
                $fileItem->dimension_units = 2; // px

                if ($image_info['bits'] !== 1 && isset($image_info['channels'])) {
                    // channels will be 3 for RGB pictures and 4 for CMYK pictures.
                    switch ($image_info['channels']) {
                        case 3:
                            $fileItem->color_mode = 3;
                            break;
                        case 4:
                            $fileItem->color_mode = 4;
                            break;
                        default:
                            $fileItem->color_mode = 1;
                    }
                } else {
                    $fileItem->color_mode = 6;
                }
            }

            // EXIF Analisis
            $exif = @exif_read_data($recognitionFilePath, 0, true);
            if ($exif) {
                //TODO: Remove this for production
                $exif_values = "EXIF:\n";
                foreach ($exif as $key => $section) {
                    foreach ($section as $name => $val) {
                        $val = (is_array($val)) ? implode(', ', $val) : $val;
                        $exif_values .= "$key.$name: $val\n";
                    }
                }
                Log::info($exif_values);

                // resolution
                if (!empty($exif['IFD0']) && !empty($exif['IFD0']['XResolution'])) {
                    $fileItem->resolution = $exif['IFD0']['XResolution'];
                }

                // color_mode
                if (empty($fileItem->color_mode)) {
                    if (!empty($exif['COMPUTED']) && $exif['COMPUTED']['IsColor'] == 0) {
                        $fileItem->color_mode =  6;
                    } else if (!empty($exif['EXIF'])) {
                        // EXIF.ColorSpace = 1 - RGB => 3
                        $fileItem->color_mode =  ($exif['EXIF']['ColorSpace'] == 1) ? 3 : 2;
                    }
                }
            }
        } catch (\Exception $exc) {
            Log::error('Unable recognize image fields: ' . $exc->getTraceAsString());
            return false;
        }

        return true;
    }

    // PDF fields
    // paper size - /MediaBox [0 0 595 842]
    private static function _recoginizePdfFields($recognitionFilePath, $fileItem)
    {
        //TODO: format -

        //TODO: resolution_units
        $fileItem->resolution_units = 1; // DPI (ResolutionUnit = 2) we ignore ResolutionUnit = 1 centimeters

        //TODO: resolution
        //$fileItem->resolution = $exif['IFD0']['XResolution'];

        //TODO: color_mode
        //$fileItem->color_mode =  ($exif['EXIF']['ColorSpace'] == 1) ? 3 : 2;

        return true;
    }

    // Video fields
    // Format, Size, Resolution, Color Mode
    private static function _recoginizeVideoFields($recognitionFilePath, $fileItem)
    {
        //TODO: format     1 => '4:3', 2 => '16:9', 3 => '21:9'
        $fileItem->resolution_units = 2;

        //TODO: resolution_units
        $fileItem->resolution_units = 1; // DPI (ResolutionUnit = 2) we ignore ResolutionUnit = 1 centimeters

        //TODO: resolution
        // $fileItem->resolution =;

        // color_mode
        //$fileItem->color_mode =  ($exif['EXIF']['ColorSpace'] == 1) ? 3 : 2;

        return true;
    }

    /** Retrun extension types in zip archive * */
    private static function _fillFilesAndFileTypesFromZip($zipFileItem, array &$arrFiles, array &$arrExtensions)
    {
        $count = 0;
        $fname = "";
        $ext = "";
        $stats = '';
        for ($i = 0; $i < $zipFileItem->numFiles; $i++) {
            $stats = $zipFileItem->statIndex($i);
            if (!self::_findAnyWord(self::$ignored_dirs, $stats['name'])) {
                //get file name and extension
                $fname = pathinfo($stats['name'], PATHINFO_FILENAME);
                $ext = strtolower(pathinfo($stats['name'], PATHINFO_EXTENSION));
                if ($fname != "" && $ext != "" && !in_array($ext, self::$ignored_ext)) {
                    $arrExtensions[$ext] = 1;
                    $arrFiles[] = $stats['name'];
                    $count++;
                }
            }
        }

        return $count;
    }

    private static function _findAnyWord($arrSearchWords, $string)
    {
        foreach ($arrSearchWords as $word) {
            if (strpos($string, $word) !== false) {
                return true;
            }
        }
        return false;
    }

    private static function _findFirstImage($arrFiles, $image_formats)
    {
        foreach ($arrFiles as $fileName) {
            $fileExtension = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
            if (in_array($fileExtension, $image_formats)) {
                return $fileName;
            }
        }
        return false;
    }
}

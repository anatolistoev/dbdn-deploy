<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class GDPREraseRequests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:gdpr_requests';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute Cron job to delete the requests of users which have not confirmed their GDPR account deletion';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dt = new \DateTime();
        $dt->modify('-15 minutes');

        $users = User::where('role',USER_ROLE_MEMBER)
            ->whereNotNull('requested_delete_at')
            ->where('requested_delete_at','<',$dt->format('Y-m-d H:i:s'))
            ->get();

        if ($users->count()) {
            foreach($users as $user) {
                $user->requested_delete_at = null;
                $user->save();
            }
        }
    }
}

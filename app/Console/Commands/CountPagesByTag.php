<?php

namespace App\Console\Commands;

use App\Models\Tag;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class CountPagesByTag extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'CountPagesByTag';
	
	/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pagesByTag:count';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int updated tags count
     */
    public function handle()
    {
        $this->info('Current application environment: ' . $this->laravel['env']);

        set_time_limit(0);
        $startTS = microtime(true);
//        $subquery = DB::table('content')
//                ->join('page', function($join) {
//                        $join->on('content.page_u_id', '=', 'page.u_id');
//                })
//                ->where('page.active', 1)
//                ->where('page.id', '<>', 1)
//                ->where('page.overview', 0)
//                ->where('page.template', '<>', 'brand')
//                ->whereNull('page.deleted_at')
//                ->where('page.searchable', 1)
//                ->where('page.publish_date', '<', date('Y-m-d H:i:s'))
//                ->where(function($query) {
//                    $query->where('page.unpublish_date', DATE_TIME_ZERO)
//                    ->orWhere('page.unpublish_date', '>', date('Y-m-d H:i:s'));
//                })
//                ->where('content.searchable', 1)
//                ->whereNull('content.deleted_at')
//                ->where('content.lang', 'tag.lang')
//                ->whereNotIn('page.u_id', function($query) {
//                    $query->select('pagetag.page_id')
//                    ->from('pagetag')
//                    ->where('pagetag.tag_id', 'tag.id')
//                    ->whereNull('pagetag.deleted_at');
//                })
//                ->whereRaw("content.body REGEXP CONCAT ('[[:<:]]',REPLACE(tag.NAME, ' ', '[[:>:]].*[[:<:]]'),'[[:>:]]')")
//                ->whereRaw("((
//                    SELECT Count(*)
//					FROM hierarchy hAlive
//					LEFT JOIN page pAlive ON hAlive.parent_id = pAlive.id
//					WHERE hAlive.page_id = page.id
//						AND pAlive.active = 1
//						AND pAlive.id <> page.id
//						AND (pAlive.publish_date < '".date('Y-m-d H:i:s')."')
//						AND (
//							pAlive.unpublish_date = '".DATE_TIME_ZERO."'
//							OR pAlive.unpublish_date > '".date('Y-m-d H:i:s')."'
//							)
//					) =(
//                    SELECT hierarchy.LEVEL
//					FROM hierarchy
//					WHERE hierarchy.page_id = page.id
//						AND hierarchy.parent_id = 1
//					))")
//                ->select(DB::raw("count(DISTINCT content.page_u_id) + (
//				SELECT count(pagetag.page_id)
//				FROM pagetag
//				INNER JOIN page ON page.u_id = pagetag.page_id
//					AND page.active = 1
//					AND page.publish_date < now()
//					AND (
//						page.unpublish_date = '".DATE_TIME_ZERO."'
//						OR page.unpublish_date > '".date('Y-m-d H:i:s')."'
//						)
//					AND page.id <> 1
//					AND page.overview = 0
//					AND page.template <> 'brand'
//					AND page.deleted_at IS NULL
//					AND page.searchable = 1
//					AND (
//						(
//							SELECT Count(*)
//							FROM hierarchy hAlive
//							LEFT JOIN page pAlive ON hAlive.parent_id = pAlive.id
//							WHERE hAlive.page_id = page.id
//								AND pAlive.active = 1
//								AND pAlive.id <> page.id
//								AND (pAlive.publish_date < now())
//								AND (
//									pAlive.unpublish_date = '".DATE_TIME_ZERO."'
//									OR pAlive.unpublish_date > '".date('Y-m-d H:i:s')."'
//									)
//							) = (
//							SELECT hierarchy.LEVEL
//							FROM hierarchy
//							WHERE hierarchy.page_id = page.id
//								AND hierarchy.parent_id = 1
//							)
//						)
//				WHERE pagetag.tag_id = tag.id
//					AND pagetag.deleted_at IS NULL
//				)"));
        $sqlTagCount = "(
            SELECT count(DISTINCT content.page_u_id) + (
                    SELECT count(pagetag.page_id)
                    FROM pagetag
                    INNER JOIN page ON page.u_id = pagetag.page_id
                        AND page.active = 1
                        AND page.publish_date < '".date('Y-m-d H:i:s')."'
                        AND (
                            page.unpublish_date = '".DATE_TIME_ZERO."'
                            OR page.unpublish_date > '".date('Y-m-d H:i:s')."'
                            )
                        AND page.id <> 1
                        AND page.overview = 0
                        AND page.template <> 'brand'
                        AND page.deleted_at IS NULL
                        AND page.searchable = 1
                        AND (
                            (
                                SELECT Count(*)
                                FROM hierarchy hAlive
                                LEFT JOIN page pAlive ON hAlive.parent_id = pAlive.id
                                WHERE hAlive.page_id = page.id
                                    AND pAlive.active = 1
                                    AND pAlive.id <> page.id
                                    AND (pAlive.publish_date < '".date('Y-m-d H:i:s')."')
                                    AND (
                                        pAlive.unpublish_date = '".DATE_TIME_ZERO."'
                                        OR pAlive.unpublish_date > '".date('Y-m-d H:i:s')."'
                                        )
                                ) = (
                                SELECT hierarchy.LEVEL
                                FROM hierarchy
                                WHERE hierarchy.page_id = page.id
                                    AND hierarchy.parent_id = 1
                                )
                            )
                    WHERE pagetag.tag_id = tag.id
                        AND pagetag.deleted_at IS NULL
                    )
            FROM content
            INNER JOIN page ON content.page_u_id = page.u_id
                AND content.searchable = 1
                AND content.deleted_at IS NULL
                AND page.active = 1
                AND page.id <> 1
                AND page.overview = 0
                AND page.template <> 'brand'
                AND page.deleted_at IS NULL
                AND page.searchable = 1
                AND page.publish_date < '".date('Y-m-d H:i:s')."'
                AND page.deleted_at IS NULL
                AND (
                    page.unpublish_date = '".DATE_TIME_ZERO."'
                    OR page.unpublish_date > '".date('Y-m-d H:i:s')."'
                    )
            LEFT JOIN download ON page.u_id = download.page_u_id
            LEFT JOIN fileinfo ON fileinfo.file_id = download.file_id
            AND download.deleted_at IS NULL
            AND fileinfo.deleted_at IS NULL
            WHERE content.lang = tag.lang
                AND (fileinfo.lang  = tag.lang OR fileinfo.lang IS NULL)
                AND page.u_id NOT IN (
                    SELECT pagetag.page_id
                    FROM pagetag
                    WHERE pagetag.tag_id = tag.id
                        AND pagetag.deleted_at IS NULL
                    )
                AND (content.body REGEXP CONCAT (
                        '[[:<:]]'
                        ,REPLACE(tag.NAME, ' ', '[[:>:]].*[[:<:]]')
                        ,'[[:>:]]'
                        )
                    OR fileinfo.description REGEXP CONCAT (
                        '[[:<:]]'
                        ,REPLACE(tag.NAME, ' ', '[[:>:]].*[[:<:]]')
                        ,'[[:>:]]'
                        )
                    )
                AND (
                    (
                        SELECT Count(*)
                        FROM hierarchy hAlive
                        LEFT JOIN page pAlive ON hAlive.parent_id = pAlive.id
                        WHERE hAlive.page_id = page.id
                            AND pAlive.active = 1
                            AND pAlive.id <> page.id
                            AND (pAlive.publish_date < '".date('Y-m-d H:i:s')."')
                            AND (
                                pAlive.unpublish_date = '".DATE_TIME_ZERO."'
                                OR pAlive.unpublish_date > '".date('Y-m-d H:i:s')."'
                                )
                        ) = (
                        SELECT hierarchy.LEVEL
                        FROM hierarchy
                        WHERE hierarchy.page_id = page.id
                            AND hierarchy.parent_id = 1
                        )
                    )
            )";

        $nowTime = \Carbon\Carbon::now();
        $update = Tag::query()
            ->update(
                [
                    'page_count'=> DB::raw($sqlTagCount),
                    Tag::UPDATED_AT => $nowTime
                ]
            );

        $execution_time = microtime(true) - $startTS;
        $execution_time = sprintf('Execution time: %.6f seconds.', $execution_time);
        $this->info($execution_time);
        $this->info("{$update} tags were updated");
        return $update;
        //
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['example', InputArgument::OPTIONAL, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

        /**
     *  $methods = line, info, comment, question, error
     */
    private function verbose($string, $method = 'line')
    {
        if ($this->option('verbose')) {
            $this->{$method}($string);
        }
    }

    public function info($string, $verbosity = null)
    {
        Log::info($string);

        parent::info($string, $verbosity);
    }
}

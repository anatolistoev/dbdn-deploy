<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class GDPRErase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:gdpr';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute GDPR Cron JOB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dt = new \DateTime();
        $dt->modify('-6 months');

        $dt24 = new \DateTime();
        $dt24->modify('-1 day');

        $users = User::where('role',USER_ROLE_MEMBER)
            ->where(
                function($query) use ($dt,$dt24) {
                    $query->where(
                        function ($fc) use ($dt) {
                            $fc->where('last_login','=','0000-00-00 00:00:00')
                                ->where('created_at','<',$dt->format('Y-m-d H:i:s'));
                        }
                    )
                        ->orWhere(
                            function ($sc) use ($dt) {
                                $sc->where('last_login','<>','0000-00-00 00:00:00')
                                    ->where('last_login','<',$dt->format('Y-m-d H:i:s'));
                            }
                        )
                        ->orWhere(
                            function ($sc) use ($dt,$dt24) {
                                $sc->where('active','=','0')
                                    ->where('created_at','<',$dt24->format('Y-m-d H:i:s'));
                            }
                        );
                }
            )
            ->get();

        foreach($users as $user) {
            $user->GDPRErase();
        }
    }
}

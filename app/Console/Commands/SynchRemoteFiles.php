<?php

namespace App\Console\Commands;

use App\Helpers\FileHelper;
use App\Http\Controllers\FileController;
use App\Models\FileItem;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

/**
 * Description of SynchRemoteFiles
 * From command prompt
 * > nohup php artisan dbdn:sync-files stage --src-url=https://designnavigator.daimler.com/ >/srv/www/htdocs_dbdn15/sync.log 2>&1 &
 *
 * From /cms/web_console
 * 
 * $syncTask = new App\Console\Commands\SynchRemoteFiles();
 * $output = new Symfony\Component\Console\Output\ConsoleOutput();
 * $syncTask->setOption('output', $output); 
 * 
 * $syncTask->setOption('srcURL', 'https://designnavigator.daimler.com/');
 * $syncTask->setOption('maxFileSize', 100 * 1024 * 1024); // 100 MB
 * $syncTask->setOption('forceDownload', true);
 * 
 * $fileItem = App\Models\FileItem::find(12717);
 * $syncTask->syncRemouteFileItem($fileItem);
 * 
 * @author m.stefanov
 */
class SynchRemoteFiles extends Command
{

    protected $name        = 'dbdn:sync-files';
    protected $description = 'Sync remote files with DB.';
    protected $srcURL;
    protected $cUrl;
    protected $maxFileSize;
    protected $forceDownload;
    //TODO:
    // 1. Get files form DB
    // 2. Download/Update files: https://github.com/farbod69/ResGet/blob/master/resget.php

    public function __construct()
    {
        parent::__construct();

        //$this->client = Client::factory();
    }

    public function setOption($name, $value){
        $this->{$name} = $value;
    }

    public function info($string, $verbosity = null)
    {
        Log::info($string);

        parent::info($string, $verbosity);
    }

    public function error($string, $verbosity = null)
    {
        Log::error($string);

        parent::error($string, $verbosity);
    }

    public function handle()
    {
        $this->info('Current application environment: ' . $this->laravel['env']);
        $this->info("DB: " . $this->getDatabase());
        $this->srcURL = $this->option('src-url');
        if ($this->srcURL) {
            $this->info("Syncing from URL: {$this->srcURL}");
            $this->maxFileSize   = $this->option('max-size');
            $this->info("Files will be truncated to: {$this->maxFileSize} bytes");
            $this->forceDownload = $this->option('force');
            $this->info("Force Download: {$this->forceDownload}");
            $this->cUrl = $this->option('curl');
            $this->info("Dawnload with CURL: {$this->cUrl}");

            $this->syncPublicFiles();
            $this->syncProtectedFiles();
            return;
        }

        $this->info("Missing arguments");
    }

    /**
     * Get the name of the database connection to use.
     *
     * @return string
     */
    protected function getDatabase()
    {
        $database = null; //$this->input->getOption('database');

        return $database ? : $this->laravel['config']['database.default'];
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //array('server', InputArgument::REQUIRED, '(required) Server name: productive or stage'),
            ['server', InputArgument::OPTIONAL, '(optional) Server name: productive or stage'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['src-url', null, InputOption::VALUE_REQUIRED, 'The URL to the source site.',
                null],
            ['max-size', null, InputOption::VALUE_OPTIONAL, 'The max file size to be downloaded. Default value 20MB.',
                20 * 1024 * 1024],
            ['force', null, InputOption::VALUE_OPTIONAL, "Force files download. Default value 'false'.",
                false],
            ['curl', null, InputOption::VALUE_OPTIONAL, "Download with CURL. Default value 'true'.",
                true],
        ];
    }

    /**
     * Sync the public files.
     *
     * @return array
     */
    protected function syncPublicFiles()
    {
        $this->info("==================");
        $this->info("Sync images:");
        $filemanager_mode = FileController::MODE_IMAGES;
        // Build query for specific file type and not permanently deleted files.
        $imageQuery       = FileItem::where('type', '=', $filemanager_mode)->where('protected', '=', 0)->orderBy('id', 'DESC');
        ;
        $this->info("Sync $filemanager_mode: {$imageQuery->count()}");
        $images           = $imageQuery->get();
        foreach ($images as $image) {
            $this->syncRemouteFileItem($image);
        }

        $this->info("==================");
        $this->info("Sync public files:");
        $filemanager_mode = FileController::MODE_DOWNLOADS;
        // Build query for specific file type and not permanently deleted files.
        $downloadQuery    = FileItem::where('type', '=', $filemanager_mode)->where('protected', '=', 0);
        $this->info("Sync $filemanager_mode: {$downloadQuery->count()}");
        $downloads        = $downloadQuery->get();
        foreach ($downloads as $download) {
            $this->syncRemouteFileItem($download);
        }
    }

    /**
     * TODO: Sync the protected files.
     *
     * @return array
     */
    protected function syncProtectedFiles()
    {
        $this->info("==================");
        $this->info("TODO: Sync protected files:");
        $filemanager_mode = FileController::MODE_DOWNLOADS;
        // Build query for specific file type and not permanently deleted files.
        $fileItemQuery    = FileItem::where('type', '=', $filemanager_mode)->where('protected', '=', 1)->count();
        $this->info("Sync $filemanager_mode: $fileItemQuery");
    }

    /**
     * Sync with remoute FileItem.
     *
     * @return array
     */
    public function syncRemouteFileItem($fileItem)
    {
        $fileLastUpdateTime = $fileItem[FileItem::UPDATED_AT]->timestamp;
        //$this->info('2 Time: '. print_r($fileLastUpdateTime, 1));
        //Get file
        $this->downloadFile(
            'file',
            $fileItem->getPath(FILEITEM_REAL_PATH),
            $fileItem->getPath(FILEITEM_URL_PATH),
            $fileLastUpdateTime
        );

        //Get Zoom
        if ($fileItem->zoompic != '') {
            $this->downloadFile(
                'zoom',
                $fileItem->getZoomPath(FILEITEM_REAL_PATH),
                $fileItem->getZoomPath(FILEITEM_URL_PATH),
                $fileLastUpdateTime
            );
        }

        //Get Thumb
        if ($fileItem->thumbnail != '') {
            $this->downloadFile(
                'thumb',
                $fileItem->getThumbPath(FILEITEM_REAL_PATH),
                $fileItem->getThumbPath(FILEITEM_URL_PATH),
                $fileLastUpdateTime
            );
        }
    }

    /**
     * Download remoute file
     *
     */
    protected function downloadFile($type, $filePath, $fileURL, $fileLastUpdateTime)
    {
        try {
            $cache_life = '7200'; //caching time, in seconds

            $fullPath        = public_path() . '/' . $filePath;
            //Create the dir path to the file
            $dirPathFileItem = dirname($fullPath);
            FileHelper::makedirs($dirPathFileItem, FILE_DEFAULT_ACCESS);
            // Create protected folders for downloads
            if (strpos($dirPathFileItem, 'public/files/downloads')) {
                $dirPathProtected = str_replace('/files/', '/protected_files/', $dirPathFileItem);
                FileHelper::makedirs($dirPathProtected, FILE_DEFAULT_ACCESS);
            }

            $filemtime = @filemtime($fullPath);  // returns FALSE if file does not exist
            //        $this->info('1 Time: '. print_r($filemtime, 1));
            //        $this->info('2 Time: '. print_r($fileLastUpdateTime, 1));
            //        $fileItemSize = $fileItem->size;
            //        $this->info('1 Size: '. print_r(@filesize($fullPath), 1));
            //        $this->info('2 Size: '. print_r($fileItemSize, 1));

            if ($this->forceDownload || !$filemtime || ($fileLastUpdateTime - $filemtime >= $cache_life) /* || ( $fileItemSize != @filesize($fullPath)) */) {
                $encodeFileURL = $this->safeURLEncode($fileURL);
                if ($this->cUrl) {
                    $reslut = $this->copySecureFileCURL($type, $encodeFileURL, $fullPath);
                    if ($reslut) {
                        $this->info("Downloaded $type File: $filePath Size: " . @filesize($fullPath));
                    } else {
                        $this->error("Error Download $type file: " . $filePath);
                    }
                } else {
                    $this->copySecureFileFOPEN($type, $encodeFileURL, $fullPath);
                }
            } else {
                $this->info("Up to date $type >>{$fileURL}");
            }
        } catch (\Exception $e) {
            $this->error("Fail to open $type:" . $e->getMessage());
            return;
        }
    }

    protected function safeURLEncode($url)
    {
        $parts = pathinfo($url);
        $url   = $parts['dirname'] . '/' . rawurlencode($parts['basename']);
        return $url;
    }

    protected function copySecureFileFOPEN($type, $fileURL, $fullPath)
    {
        $fullURL = $this->srcURL . $fileURL;
        if ($hSrcFile = fopen($fullURL, 'rb')) {
            $this->info("Download $type >>" . $fileURL);
            $hLocalFile      = fopen($fullPath, "w");
            $firstRead       = true;
            $downloadedBytes = 0;
            while (!feof($hSrcFile)) {
                $fileContent = fread($hSrcFile, 65535);
                if ($firstRead && $fileContent == false) {
                    $this->error("Fail to get $type >>{$fullURL}");
                }
                $downloadedBytes += fwrite($hLocalFile, $fileContent);
                if ($downloadedBytes > $this->maxFileSize) {
                    $this->info("Trancated size: " . $downloadedBytes);
                    break;
                }
                $firstRead = false;
            }
            fclose($hSrcFile);
            fclose($hLocalFile);
        } else {
            $this->error("Fail to open $type >>{$fullURL}");
        }
    }

    /**
     * Copy File from HTTPS/SSL location with CURL
     *
     * @param string $FromLocation
     * @param string $ToLocation
     * @return boolean
     */
    protected function copySecureFileCURL($type, $FromLocation, $ToLocation, $VerifyPeer = false, $VerifyHost = true)
    {
        $fullURL = $this->srcURL . $FromLocation;
        // Initialize CURL with providing full https URL of the file location
        $Channel = curl_init($fullURL);
        if ($Channel === false) {
            $this->error("Fail to init CURL $type >>{$fullURL}");
            return false;
        }

        // Open file handle at the location you want to copy the file: destination path at local drive
        $File = fopen($ToLocation, "w");
        $this->info("Open file: $File");

        // Set CURL options
        curl_setopt($Channel, CURLOPT_FILE, $File);

        // We are not sending any headers
        curl_setopt($Channel, CURLOPT_HEADER, 0);

        // Disable PEER SSL Verification: If you are not running with SSL or if you don't have valid SSL
        curl_setopt($Channel, CURLOPT_SSL_VERIFYPEER, $VerifyPeer);

        // Disable HOST (the site you are sending request to) SSL Verification,
        // if Host can have certificate which is nvalid / expired / not signed by authorized CA.
        curl_setopt($Channel, CURLOPT_SSL_VERIFYHOST, $VerifyHost);

        // Set time for open the connection
        curl_setopt($Channel, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($Channel, CURLOPT_TIMEOUT, 4 * 60); //timeout in seconds  to download the file.

        // Execute CURL command
        $result = curl_exec($Channel);
        if (!$result) {
            $this->error("Dwonload error: " . curl_error($Channel));
        }

        // Close the CURL channel
        curl_close($Channel);

        // Close file handle
        fclose($File);

        // return true if file download is successfull
        return $result && file_exists($ToLocation);
    }
}

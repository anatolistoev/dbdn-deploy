<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ActivateFuturePublishPages extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ActivateFuturePublishPages';
	
	/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'futurePublishPages:activate';
	
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Activate pages with feature publish date.';

    private $loggedUserId;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $this->loggedUserId = config('app.commandUserId');
    }

    /**
     * Execute the console command.
     *
     * @return int count of activated pages
     */
    public function handle()
    {
        $activatedPages = 0;
        Auth::loginUsingId($this->loggedUserId);

        $this->info('Current application environment: ' . $this->laravel['env']);
        $currentunixTimestamp = time();
        $minuteOfCurrentHour = (int)strftime('%M', $currentunixTimestamp);
        $date = date('Y-m-d H:i:00', $currentunixTimestamp);
        $dateWithHour = date('Y-m-d H:i:00', ($currentunixTimestamp + 3600));
        $dateWithMinute = date('Y-m-d H:i:00', ($currentunixTimestamp + 60));
        if (0 == $minuteOfCurrentHour) {
            $activatedPages = $this->collectPagesForActivation($date, $dateWithMinute, $dateWithHour);
        } else {
            $activatedPages = $this->activatePagesEveryMinute($date, $dateWithMinute);
        }
        //$this->info('Current time: ' . $date);
        Auth::logout();

        return $activatedPages;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['example', InputArgument::OPTIONAL, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

    public function info($string, $verbosity = null)
    {
        Log::info($string);

        parent::info($string, $verbosity);
    }

    private function collectPagesForActivation($date, $dateWithMinute, $dateWithHour): int
    {
        $pages = DB::table('page')
                ->select('page.u_id', 'page.publish_date')
                ->join('workflow', 'workflow.page_id', '=', 'page.u_id')
                        ->where('page.publish_date', '>=', $date)
                        ->where('page.publish_date', '<', $dateWithHour)
                        ->whereRaw('((SELECT Count(*)
                                FROM hierarchy hAlive
                                LEFT JOIN page pAlive ON hAlive.parent_id = pAlive.id
                                WHERE hAlive.page_id = page.id AND pAlive.active = 1 AND pAlive.id <> page.id
                                AND ( pAlive.publish_date = "'.DATE_TIME_ZERO.'" OR pAlive.publish_date < "'.date('Y-m-d H:i:s').'")
                                AND ( pAlive.unpublish_date = "'.DATE_TIME_ZERO.'"	OR pAlive.unpublish_date > "'.date('Y-m-d H:i:s').'"))
                                =
                                (Select hierarchy.level FROM hierarchy where hierarchy.page_id = page.id AND hierarchy.parent_id = 1))')
                ->where('page.active', 0)
                ->where('workflow.editing_state', STATE_APPROVED)
                ->get();

        if (count($pages) > 0) {
            $pageForActivation = [];
            $nowTime = \Carbon\Carbon::now();
            foreach ($pages as $page) {
                DB::table('page_activation')->insert(
                    [
                        'page_u_id' => $page->u_id, 
                        'publish_date' => $page->publish_date,
                        Model::CREATED_AT => $nowTime, 
                        Model::UPDATED_AT => $nowTime                        
                    ]
                );

                if ($page->publish_date >= $date && $page->publish_date < $dateWithMinute) {
                    $page->page_u_id =  $page->u_id;
                    $pageForActivation[] = $page;
                }
            }

            return $this->activatePages($pageForActivation);
        }

        return 0;
    }

    private function activatePagesEveryMinute($date, $dateWithMinute): int
    {
        $pageForActivation = DB::table('page_activation')
                ->where('publish_date', '>=', $date)
                ->where('publish_date', '<', $dateWithMinute)
                ->get();
        
        return $this->activatePages($pageForActivation);
    }

    private function activatePages($pageForActivation)
    {
        $activatedPages = 0;
        if (count($pageForActivation) > 0) {
            $pageController = App::make(\App\Http\Controllers\PageController::class);
            foreach ($pageForActivation as $page) {
                $params = ['active'=> 1, 'u_id'=> $page->page_u_id];
                $result = $pageController->putUpdate($params);
                if (!$result->isSuccessful()) {
                    $error = json_decode($result->getContent());
                    $debugmsg = "";
                    if (isset($error->debug)) {
                        $debugmsg = ' Debug message: ' . $error->debug->exception_message;
                    }
                    $message = $error->message . $debugmsg;
                    $this->error($message);
                    Log::error($message);
                } else {
                    $activatedPages++;
                    $this->info("Page with u_id {$page->page_u_id} was activated.");
                }
            }
        }
        return $activatedPages;
    }
}

<?php

namespace App\Console\Commands;

use App\Helpers\FileHelper;
use App\Http\Controllers\NewsletterController;
use App\Http\Controllers\NewsletterListController;
use App\Models\Newsletter;
use App\Models\NewsletterSent;
use App\Models\NewsletterSubscriber;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class SendNewsletterEmails extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'SendNewsletterEmails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';


    /**
     *  CONFIGURATION
     */
    protected $dry_run      = true;
    protected $test_list    = true;
    protected $test_mode    = true;
    protected $test_email   = false;
    protected $test_name   = false;
    protected $send_limit   = false;



    protected $meta = [
            'subject'   => '',
            'fromName'  => 'Daimler AG - Corporate Design',
            'fromEmail' => 'corporate.design@daimler.com',
            'returnPath'=> 'corporate.design@daimler.com',
            'replyTo'   => 'corporate.design@daimler.com',
        //  'receiptTo' => 'd.denev@esof.net',
        ];

//TODO: Remove it! We use DB test list
    protected $test_users = [
            //'corporate.design@daimler.com'    => 'Anton Tontchev',
            //'anton.tontchev@interton1.com'    => 'Anton Tontchev',
        ];


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->initOptions();

        $this->commandResult = [
                'status'    => '',
                'sent'      => 0,
                'fail'      => 0,
                'duplicate'	=> 0,
                // ... extend as you like ;)
            ];

        $this->verbose("\n======================\n", 'info');
        $this->verbose('OPTIONS: '.print_r($this->option(), true), 'info');
        $this->verbose('CONFIG: '.print_r(config('mail'), true), 'info');
        $this->verbose("\n======================\n", 'info');

        if ($this->nl_id) {
            $newsletter = Newsletter::find($this->nl_id);
        } else {
            $newsletter = Newsletter::sending();
        }

        if (!$newsletter) {
            $this->verbose('No sending newsletter', 'info');
            return $this->commandExit('No sending newsletter');
        }

        $sentQuery = '
                    select `newsletter_sent`.`user_id`
                    from `newsletter_sent`
                    where
                        `newsletter_sent`.`newsletter_id` = '.$newsletter->id.'
                        and (`newsletter_sent`.`status` = 1 or `newsletter_sent`.`retries` > 2)
                ';

        $users = $this->getUsersForSending($sentQuery);
        
        // Mark newsletter as sent and exits if there are no users to send to
        if (0 == count($users)) {
            $newsletter->markSent();
            $this->verbose('No users to send to. Newsletter marked as sent.', 'info');
            return $this->commandExit('No users to send to. Newsletter marked as sent.');
        }

        // Load sent emails to avoid duplication
        $sentEmails = $this->getSentEmails($sentQuery);
        
        /**
         *  image embedding disabled due to tracking pixel issue ;)
         *  otherwise use:
         *  $tmpViewHtml = self::embedImages($tmpViewHtml);
         */

        $tmpViewName = 'newslettertempview';
        $tmpViewFile = $this->tmpViewFile($tmpViewName);

        $views = [$tmpViewName, 'emails.newsletter_textonly'];

        $view_params = NewsletterController::buildParams($newsletter->id);

        // tracker test to localhost ;)
        $view_params['piwik'] = '';

        $meta = (object)$this->meta;

        foreach ($users as $idx => $user) {
            $email = trim($user->email);
            if (array_key_exists($email, $sentEmails)) {
                $sentEmails[$email]++;
				
                ++$this->commandResult['duplicate'];
				
                $this->verbose('Skip duplicate email:'.$user->email . ' - ' . $user->name);
                continue;
            }

            $user_params = array_merge([], $view_params);
            $user_params = NewsletterController::personalizeParams($user_params, $user);
            $meta->subject = $user_params['title'];

            $tmpViewHtml = view('emails.newsletter', $user_params)->render();
            $tmpViewHtml = str_replace('<!-- __PIWIK__ -->', '{{$piwik}}', $tmpViewHtml);
            file_put_contents($tmpViewFile, $tmpViewHtml);

            if ($this->dry_run) {
                $this->verbose('[DRY-RUN] NOT sending to '.$user->email . ' - ' . $user->name);
                $result = false;
            } else {
                $this->verbose('Sending to '.$user->email . ' - ' . $user->name);

                $result = Mail::send($views, $user_params, function ($message) use ($user, $meta) {
                    $message
                        ->from($meta->fromEmail, $meta->fromName)
                        ->to($user->email, $user->name)
                        ->subject($meta->subject);

                    if (!empty($meta->replyTo)) {
                        $message->replyTo($meta->replyTo);
                    }

                    if (!empty($meta->returnPath)) {
                        $message->returnPath($meta->returnPath);
                    }

                    if (!empty($meta->receiptTo)) {
                        $message->setReadReceiptTo($meta->receiptTo);
                    }
                });
                

                // if (1 == $result) {
                if(count(Mail::failures())==0){
                    ++$this->commandResult['sent'];
                } else {
                    ++$this->commandResult['fail'];
                }

                if (!$this->test_list && !empty($user->id)) {   // empty user ID means test user
                    $sent = NewsletterSent::firstOrNew([
                                'newsletter_id' => $newsletter->id,
                                'user_id'       => $user->id
                            ]);

                    if (null !== $sent->retries) {
                        ++$sent->retries;
                    }
                    $sent->status = $result;
                    $sent->save();
                }
            }

            // Mark email as sent
            $sentEmails[$email] = 1;

            $this->verbose('Mail result '.($idx + 1).': '. var_export($result, true));
        }

        if(0 == $this->commandResult['fail'] && 0 == $this->commandResult['sent'] && $this->commandResult['duplicate'] > 0)
        {
            $newsletter->markSent();
            $this->verbose('No send tousers because of duplications. Newsletter marked as sent.', 'info');
        }

        return $this->commandExit(0);
    }

    private function commandExit($status = false)
    {
        if ($status && !is_numeric($status)) {
            $this->commandResult['status'] = $status;
        } else {
            $status = (int)$status;
        }

        Config::set('commandResult', $this->commandResult);
        return $status;
    }

    private function tmpViewPath()
    {
        $tmpViewPath = FileHelper::createTempDir('newsletter');

        View::addLocation($tmpViewPath);

        return $tmpViewPath;
    }


    private function tmpViewFile($tmpViewName)
    {
        $tmpViewPath = $this->tmpViewPath();
        $tmpViewFile = $tmpViewPath.DIRECTORY_SEPARATOR.$tmpViewName.'.blade.php';
        file_put_contents($tmpViewFile, '');
        chmod($tmpViewFile, 0666);
        $this->verbose('Created temp file '.$tmpViewFile);
        return $tmpViewFile;
    }

    private function getUsersForSending($sentQuery)
    {

        $users = [];

        if ($this->test_email) {   // override user list for test email
            return [(object)['email' => $this->test_email, 'name' => $this->test_name]];
        }

        if ($this->test_list) {    // override user list for test mode
        //            foreach($this->test_users as $email => $name)
//                $users[] = (object)array('email' => $email, 'name' => $name);

//            return $users;

//TODO: Implement loading test list from DB.
            $user_ids = NewsletterSubscriber::where('list_id', NewsletterListController::ID_TEST_LIST)
                        ->orderBy('user_id', 'DESC')
                        ->take($this->send_limit)
                        ->pluck('user_id');
        } else {
            $user_ids = DB::table('user')
                        ->select('user.id')
                        ->whereRaw('`user`.`newsletter` = 1')
                        ->whereRaw('`user`.`newsletter_include` = 1')
                        ->whereRaw('`user`.`id` not in ('.$sentQuery.')')
                        ->whereNull('deleted_at')
                        ->orderBy('user.id', 'DESC')
                        ->take($this->send_limit)
                        ->pluck('user.id');
        }

        foreach ($user_ids as $uId) {
            $user = User::find($uId);

            if (!$user) {
                $this->verbose('[ERROR] Could not find user '.$uId, 'error');
                continue;
            }

            $user = $user->getAttributes();

            //Add unsubscribe_code to work email generation properly
            if (empty($user['unsubscribe_code'])) {
                $user['unsubscribe_code'] = str_random(10);
                User::where('id', $user['id'])->update(['unsubscribe_code' => $user['unsubscribe_code']]);
            }

            $users[] = (object)[
                        'id'    => $user['id'],
                        'email' => trim($user['email']),
                        'name'  => html_entity_decode(trim($user['first_name']) . ' ' . trim($user['last_name'])),
                        'lang'  => empty($user['newsletter_lang']) ? 3 : $user['newsletter_lang'],
                        'unsubscribe_code' => $user['unsubscribe_code'],
                    ];
        }

        return $users;
    }

    private function getSentEmails($sentQuery)
    {
        $users = [];

        if ($this->test_list) {    // override user list for test mode
            return [];
        } elseif ($this->test_email) {   // override user list for test email
            return [];
        }

        $users_email = User::select('email')
                    ->whereRaw('`user`.`newsletter` = 1')
                    ->whereRaw('`user`.`newsletter_include` = 1')
                    ->whereRaw('`user`.`id` in ('.$sentQuery.')')
                    ->whereNull('deleted_at')
                    ->pluck('email');

        foreach ($users_email as $email) {
            $users[trim($email)] = 1;
        }

        return $users;
    }

    /**
     *  $methods = line, info, comment, question, error
     */
    private function verbose($string, $method = 'line')
    {
        if ($this->option('verbose')) {
            $this->{$method}($string);
        }
    }

    private static function embedImages($html)
    {
        return NewsletterController::modifyImgSrc([
                'html'          =>  $html,
                'prepend'       =>  '<?php echo $message->embed(\'',
                'append'        =>  '\');?>',
                'ifNotStartWith'=>  '<?php',
                'copyLocal'     =>  true,
            ]);
    }


    private function initOptions()
    {
        $this->nl_id        = $this->option('id');
        $this->send_limit   = $this->option('send-limit');
        $this->dry_run      = $this->option('dry-run') ? true : false;
        $this->test_mode    = $this->option('test-mode') ? true : false;
        $this->test_list    = $this->option('test-list') ? true : false;
        $this->test_email   = $this->option('email') ? $this->option('email') : false;
        $this->test_name   = $this->option('name') ? $this->option('name') : false;

        if ($this->test_email && filter_var($this->test_email, FILTER_VALIDATE_EMAIL) === false) {
            $this->verbose("[ERROR] {$this->test_email} is not valid e-mail address", 'error');
            die;
        }

        if ($this->test_email && $this->test_list) {
            $this->verbose('[ERROR] Can not send to list AND email', 'error');
            die;
        }

        if ($this->test_mode) {
            $this->setTestMode();
        }
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            // array('example', InputArgument::OPTIONAL, 'An example argument.'),
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['id', 'i', InputOption::VALUE_OPTIONAL, 'Newsletter ID.', null],
            ['email', 'e', InputOption::VALUE_OPTIONAL, 'Test e-mail address.', null],
            ['name', 'u', InputOption::VALUE_OPTIONAL, 'User name for test e-mail.', 'Test user'],
            ['send-limit', 'm', InputOption::VALUE_OPTIONAL, 'Send limit.', 1],
            ['test-mode', 't', InputOption::VALUE_NONE, 'Test mode.', null],
            ['test-list', 'l', InputOption::VALUE_OPTIONAL, 'Test list.', true],
            ['dry-run', 'd', InputOption::VALUE_OPTIONAL, 'Dry run.', true],
        ];
    }
}


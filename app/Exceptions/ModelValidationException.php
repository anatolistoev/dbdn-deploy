<?php

namespace App\Exception;

use App\Http\Controllers\RestController;

class ModelValidationException extends \Exception
{
    /**
     * @var \Illuminate\Support\MessageBag
     */
    private $errors;

    /**
     * ModelValidationException constructor.
     * @param \Illuminate\Support\MessageBag $errors
     */
    public function __construct(\Illuminate\Support\MessageBag $errors)
    {
        $this->errors = $errors;
    }

    public function render($request)
    {
        return response()->json(RestController::generateJsonResponse(true, trans('ws_general_controller.validation.failed'), $this->errors), 400);
    }
    
    public function getErrors() {
        return $this->errors;
    }

}

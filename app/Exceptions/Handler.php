<?php namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

use App\Http\Controllers\RestController;

class Handler extends ExceptionHandler
{

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Illuminate\Validation\ValidationException::class,
        'Symfony\Component\HttpKernel\Exception\HttpException',
        \App\Exception\ModelValidationException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, \Exception $e)
    {
        $shouldReport = $this->shouldReport($e);
        if ($shouldReport) {
            Log::error('Dump request', array($request->__toString()));
            Log::error($e);
        }

        if ($e instanceof \Illuminate\Session\TokenMismatchException) {
            if (strpos($request->getRequestUri(), '/cms') !== false) {
                return redirect('/cms')->withErrors(['username' => trans('backend.session.expired')]);
            } else {
                return redirect('/')->withErrors(['success' => trans('system.session.expired')]);
            }
        }

        if (\session('ws_error', false)){
            if (method_exists($e, 'render') && $response = $e->render($request)) {
                return Router::toResponse($request, $response);
            } else {
                //TODO: Improve Error handling?
                $message = 'Sorry! An error has occurred!';
                if (config('app.debug')) {
                    $msg = $e->getMessage();
                    $message = (!empty($msg)) ? $msg : $message;
                }
                if ($e instanceof \PDOException) {
                    if (0 === strpos($message, 'SQLSTATE[42S22]')) {
                        $errorArray = RestController::generateJsonResponse(true, 'Unknown column.');
                    } elseif (0 === strpos($message, 'SQLSTATE[23000]')) {
                        $errorArray = RestController::generateJsonResponse(true, 'Duplicate record.');
                    } else {
                        $errorArray = RestController::generateJsonResponse(true, $message);
                    }
                } else {
                    $errorArray = RestController::generateJsonResponse(true, $message);
                }
                if (config('app.debug')) {
                    // Add stack trace
                    $errorArray['debug'] = ['exception_message' => $message, 'stacktrace' => $e->getTraceAsString()];
                }

                $jsonResponse = Response::json($errorArray, 500);
                return $jsonResponse;
            }
        } else {
            if ($shouldReport && !config('app.debug')) {
                return Response::view('errors.fatal', ['PAGE'=> (object)['id' => 404, 'slug' => 'fatal']], 500);
            }
        }

        return parent::render($request, $e);
    }
    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $e
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $e)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        } else {                              
            return redirect()->guest('login'.'/'.$request->path());
        }
    }
}
<?php

namespace App\Exception;

use App\Http\Controllers\RestController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class CopyPageException extends \Exception
{
    public function render($request)
    {
        $errorArray = RestController::generateJsonResponse(true, $this->getMessage());
        if (config('app.debug')) {
            // Add stack trace
            $prev = $this->getPrevious();
            $errorArray['debug'] = ['exception_message' => $this->getMessage(), 'stacktrace' => ($prev?$prev->getTraceAsString():'')];
        }

        return response()->json($errorArray, 500);
    }
}

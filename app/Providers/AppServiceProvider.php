<?php namespace App\Providers;

use App\Helpers\MobileDetectHelper;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $detect = new MobileDetectHelper();
        Schema::defaultStringLength(191);
        // N.B. isTablet must be before isMobile because isMobile returns true for tablets
        if (!defined('TABLET')) {
            if ($detect->isTablet()) {
                define('TABLET', true);
                define('MOBILE', false);
            } elseif ($detect->isMobile()) {
                define('TABLET', false);
                define('MOBILE', true);
            } else {
                define('TABLET', false);
                define('MOBILE', false);
            }
        }

        unset($detect);

        // Set the HTTPS header in App request. We need this for production env!
        if (!$this->app->environment('local')) {
            URL::forceScheme('https');
        }
    }

    /**
     * Register any application services.
     *
     * This service provider is a great spot to register your various container
     * bindings with the application. As you can see, we are registering our
     * "Registrar" implementation here. You can add your own bindings too!
     *
     * @return void
     */
    public function register()
    {
    }
}

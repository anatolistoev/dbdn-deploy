<?php namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // For Page and Newsletter
        Validator::extend('is_slug', function ($attribute, $value, $parameters, $validator) {
            return preg_match("/[^A-Za-z0-9_\-\~]/", $value) !== 1;
        });

        // For DialogPageController
        Validator::extend('negated_regex', function ($attribute, $value, $parameters, $validator) {
            return !preg_match($parameters[0], $value);
        });
        
        // For DialogPageController
        Validator::extend('no_tags', function ($attribute, $value, $parameters, $validator) {
            return (strip_tags($value)==$value);
        });
    }

    /**
     * Register any application services.
     *
     * This service provider is a great spot to register your various container
     * bindings with the application. As you can see, we are registering our
     * "Registrar" implementation here. You can add your own bindings too!
     *
     * @return void
     */
    public function register()
    {
    }
}

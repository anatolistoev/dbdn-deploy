<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MailResetPasswordToken extends ResetPassword
{
    use Queueable;

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = parent::toMail($notifiable);
        $message->view('emails.reset_password');
        $message->subject(trans('passwords.email.subject'));
        $message->viewData['user'] = $notifiable;
        $message->viewData['token'] = $this->token;
        return $message;
    }
}

<?php

use App\Helpers\UserAccessHelper;

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Defaults
    |--------------------------------------------------------------------------
    |
    | This option controls the default authentication "guard" and password
    | reset options for your application. You may change these defaults
    | as required, but they're a perfect start for most applications.
    |
    */

    'defaults' => [
        'guard' => 'web',
        'passwords' => 'users',
    ],

    /*
    |--------------------------------------------------------------------------
    | Authentication Guards
    |--------------------------------------------------------------------------
    |
    | Next, you may define every authentication guard for your application.
    | Of course, a great default configuration has been defined for you
    | here which uses session storage and the Eloquent user provider.
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | Supported: "session", "token"
    |
    */

    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'session',
            'provider' => 'users',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | User Providers
    |--------------------------------------------------------------------------
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | If you have multiple user tables or models you may configure multiple
    | sources which represent each model / table. These sources may then
    | be assigned to any extra authentication guards you have defined.
    |
    | Supported: "database", "eloquent"
    |
    */

    'providers' => [
        'users' => [
//            'driver' => 'eloquent',
            'driver' => 'phpass',
            'model' => App\Models\User::class,
        ],

        // 'users' => [
        //     'driver' => 'database',
        //     'table' => 'users',
        // ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Resetting Passwords
    |--------------------------------------------------------------------------
    |
    | You may specify multiple password reset configurations if you have more
    | than one user table or model in the application and you want to have
    | separate password reset settings based on the specific user types.
    |
    | The expire time is the number of minutes that the reset token should be
    | considered valid. This security feature keeps tokens short-lived so
    | they have less time to be guessed. You may change this as needed.
    |
    */

    
    'passwords' => [
        
        'users' => [
            'provider' => 'users',
#            'table' => 'password_resets',
            'table' => 'password_reminders',
       	    'expire' => 15,
        ],
    ],

    /*
  |--------------------------------------------------------------------------
  | CMS user Groups
  |--------------------------------------------------------------------------
  |
  | Define which roles belongs to CMS User Groups
  |
 */
    'CMS_ADMIN' => [USER_ROLE_ADMIN],
    'CMS_APPROVER' => [USER_ROLE_ADMIN, USER_ROLE_EDITOR],
    'CMS_EDITOR' => [
        USER_ROLE_ADMIN, USER_ROLE_EDITOR, USER_ROLE_EDITOR_SMART,
        USER_ROLE_EDITOR_MB, USER_ROLE_EDITOR_DFS, USER_ROLE_EDITOR_DFM,
        USER_ROLE_EDITOR_DTF, USER_ROLE_EDITOR_FF, USER_ROLE_EDITOR_DP, USER_ROLE_EDITOR_TSS, USER_ROLE_EDITOR_BKK,
        USER_ROLE_EDITOR_DB, USER_ROLE_EDITOR_EB, USER_ROLE_EDITOR_SETRA, USER_ROLE_EDITOR_OP, USER_ROLE_EDITOR_BS, USER_ROLE_EDITOR_FUSO,
        USER_ROLE_EDITOR_DT, USER_ROLE_EDITOR_DMO
    ],
    'CMS_NL_APPROVER' => [USER_ROLE_ADMIN, USER_ROLE_NEWSLETTER_APPROVER],
    'CMS_NL_EDITOR' => [USER_ROLE_ADMIN, USER_ROLE_NEWSLETTER_APPROVER, USER_ROLE_NEWSLETTER_EDITOR],
    /*
      |--------------------------------------------------------------------------
      | User Page and Folder Roots
      |--------------------------------------------------------------------------
      |
      | Define user roles root page in hierarchy and root folder in files structure
      |
     */
    'ACCESS' => [
        USER_ROLE_MEMBER        => ["page" => "", "folder" => ""],
        USER_ROLE_EDITOR        => ["page" => "all", "folder" => "all"],
        USER_ROLE_ADMIN         => ["page" => "all", "folder" => "all"],
        USER_ROLE_EDITOR_SMART  => ["page" => SMART, "folder" => UserAccessHelper::getSpecialPage(SMART)->folder],
        USER_ROLE_EDITOR_MB     => ["page" => MERCEDES_BENZ, "folder" => UserAccessHelper::getSpecialPage(MERCEDES_BENZ)->folder],
        USER_ROLE_EDITOR_DFS    => ["page" => FINANCIAL_SERVICES, "folder" => UserAccessHelper::getSpecialPage(FINANCIAL_SERVICES)->folder],
        USER_ROLE_EDITOR_DFM    => ["page" => FLEET_MANAGEMENT, "folder" => UserAccessHelper::getSpecialPage(FLEET_MANAGEMENT)->folder],
        USER_ROLE_EDITOR_DTF    => ["page" => TRUCK_FINANCIAL, "folder" => UserAccessHelper::getSpecialPage(TRUCK_FINANCIAL)->folder],
        USER_ROLE_EDITOR_FF     => ["page" => FUSO_FINANCIAL, "folder" => UserAccessHelper::getSpecialPage(FUSO_FINANCIAL)->folder],
        USER_ROLE_EDITOR_DP     => ["page" => DAIMLER_PROTICS, "folder" => UserAccessHelper::getSpecialPage(DAIMLER_PROTICS)->folder],
        USER_ROLE_EDITOR_TSS    => ["page" => DAIMLER_TSS, "folder" => UserAccessHelper::getSpecialPage(DAIMLER_TSS)->folder],
        USER_ROLE_EDITOR_BKK    => ["page" => DAIMLER_BKK, "folder" => UserAccessHelper::getSpecialPage(DAIMLER_BKK)->folder],
        USER_ROLE_NEWSLETTER_APPROVER => ["page" => DAIMLER, "folder" => "Newsletter"],
        USER_ROLE_NEWSLETTER_EDITOR => ["page" => DAIMLER, "folder" => "Newsletter"],
        // New Roles for 2018
        USER_ROLE_EDITOR_DB     => ["page" => DAIMLER_BUSES, "folder" => UserAccessHelper::getSpecialPage(DAIMLER_BUSES)->folder],
        USER_ROLE_EDITOR_EB     => ["page" => EVOBUS, "folder" => UserAccessHelper::getSpecialPage(EVOBUS)->folder],
        USER_ROLE_EDITOR_SETRA  => ["page" => SETRA, "folder" => UserAccessHelper::getSpecialPage(SETRA)->folder],
        USER_ROLE_EDITOR_OP     => ["page" => OMNIPLUS, "folder" => UserAccessHelper::getSpecialPage(OMNIPLUS)->folder],
        USER_ROLE_EDITOR_BS     => ["page" => BUSSTORE, "folder" => UserAccessHelper::getSpecialPage(BUSSTORE)->folder],
        USER_ROLE_EDITOR_FUSO   => ["page" => FUSO, "folder" => UserAccessHelper::getSpecialPage(FUSO)->folder],
        USER_ROLE_EDITOR_DT     => ["page" => DAIMLER_TRUCKS, "folder" => UserAccessHelper::getSpecialPage(DAIMLER_TRUCKS)->folder],
        USER_ROLE_EDITOR_DMO    => ["page" => DAIMLER_MOBILITY, "folder" => UserAccessHelper::getSpecialPage(DAIMLER_MOBILITY)->folder],
    ],
];

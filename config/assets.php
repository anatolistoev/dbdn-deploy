<?php

return [

    'image_formats' => ['jpg', 'jpeg', 'tiff', 'tif', 'png'],
    /*
	|--------------------------------------------------------------------------
	| Configuration for images
	|--------------------------------------------------------------------------
	|
	|
	*/

    'images' => [
        'cache_insert_index' => strlen("files/images"),
        'file_sizes' => [
            'zoom_pic' => 'fullsize',
            'thumbnail' => 'fullsize'
        ],
        'sizes' => [
            'brand_banner' => [
                'file_folder' => '',
                'prefix' => '/__cache/brand_banner',
                'width' => 1920,
                'height' => 740,
                'quality' => 99
            ],
            'brand_carousel' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/brand_carousel',
                'width' => 160,
                'height' => 107,
                'quality' => 99
            ],
            'overview' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/watchlist',
                'width' => 380,
                'height' => 253,
                'quality' => 99
            ],
            'watchlist' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/watchlist',
                'width' => 380,
                'height' => 253,
                'quality' => 99
            ],
            'carousel' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/carousel',
                'width' => 580,
                'height' => 387,
                'quality' => 99
            ],
            'small_carousel' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/small_carousel',
                'width' => 280,
                'height' => 186,
                'quality' => 99,
                'crop' => 'center',
            ],
            'content' => [
                'file_folder' => '',
                'prefix' => '/__cache/content',
                'width' => 780,
                'height' => 520,
                'quality' => 99
            ],
            'zoom' => [
                'file_folder' => '__zoompic',
                'prefix' => '/__cache/zoom',
                'width' => 1190,
                'height' => 840,
                'quality' => 99
            ],
            'fullsize' => [
                'file_folder' => '__zoompic',
                'prefix' => '',
                'width' => 1480,
                'height' => 1040,
                'quality' => 99
            ],
            'pdf_content' => [
                'file_folder' => '',
                'prefix' => '/__cache/pdf_content',
                'width' => 1040,
                'height' => 694,
                'quality' => 99
            ],
            'fullsize_banner' => [
                'file_folder' => '',
                'prefix' => '/__cache/fullsize_banner',
                'width' => 1920,
                'height' => 740,
                'quality' => 99
            ],
            'menu_carousel' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/menu_carousel',
                'width' => 220,
                'height' => 146,
                'quality' => 99,
                'crop' => 'center',
            ],
            'blurred_preview' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/blurred_preview',
                'width' => 21,
                'height' => 14,
                'quality' => 80
            ],
            

        ]
    ],

    /*
	|--------------------------------------------------------------------------
	| Configuration for downloads
	|--------------------------------------------------------------------------
	|
	|
	*/

    'downloads' => [
        'cache_insert_index' => strlen("files/downloads"),
        'file_sizes' => [
            'zoom_pic' => 'fullsize',
            'thumbnail' => 'fullsize'
        ],
        'sizes' => [
            'brand_carousel' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/brand_carousel',
                'width' => 160,
                'height' => 107,
                'quality' => 99,
                'crop' => 'center'
            ],
            'overview' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/watchlist',
                'width' => 380,
                'height' => 253,
                'quality' => 99,
                'crop' => 'center'
            ],
            'watchlist' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/watchlist',
                'width' => 380,
                'height' => 253,
                'quality' => 99,
                'crop' => 'center'
            ],
            'carousel' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/carousel',
                'width' => 580,
                'height' => 387,
                'quality' => 99,
                'crop' => 'center',
            ],
            'small_carousel' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/small_carousel',
                'width' => 280,
                'height' => 186,
                'quality' => 99,
                'crop' => 'center',
            ],
            'content' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/content',
                'width' => 480,
                'height' => 320,
                'quality' => 99,
                'crop' => 'center'
            ],
            'zoom' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/zoom',
                'width' => 1190,
                'height' => 840,
                'quality' => 99
            ],
            'fullsize' => [
                'file_folder' => '__zoompic',
                'prefix' => '',
                'width' => 1480,
                'height' => 1040,
                'quality' => 99
            ],
            'pdf_content' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/pdf_content',
                'width' => 640,
                'height' => 427,
                'quality' => 99,
                'crop' => 'center'
            ],
            'lib_display' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/lib_display',
                'width' => 180,
                'height' => 120,
                'quality' => 99,
                'crop' => 'center'
            ],
            'menu_carousel' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/menu_carousel',
                'width' => 220,
                'height' => 146,
                'quality' => 99,
                'crop' => 'center',
            ],
            'blurred_preview' => [
                'file_folder' => '__thumbnail',
                'prefix' => '/__cache/blurred_preview',
                'width' => 21,
                'height' => 14,
                'quality' => 80,
                'crop' => 'center',
            ],
        ]
    ],

];

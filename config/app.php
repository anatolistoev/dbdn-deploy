<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('APP_NAME', 'Laravel'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'Europe/Berlin',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'de',
    'locales' => ['en', 'de'],
    'time_locales' => [['en', 'en_GB', 'English', 'en_GB.utf8'], ['de_DE.UTF8', 'de', 'de_DE', 'de_DE.UTF-8']],

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

	/*
	  |--------------------------------------------------------------------------
	  | Application Testing Configuration
	  |--------------------------------------------------------------------------
	  |
	  | The application testing units variable. When starting tests set variable to true
	  | to avoid backend login confirmation.
	  |
	 */

    'testing' => env('APP_TESTING', false),

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    'old_key' => 'nclj0g%76MQZD8$v4b13pho@s!_5eaux',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    /* 'log' => env('APP_LOG', 'daily'),

    'log_level' => env('APP_LOG_LEVEL', 'debug'),

    'log_max_files' => 3650, */

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Package Service Providers...
         */
        Barryvdh\DomPDF\ServiceProvider::class,
        Collective\Html\HtmlServiceProvider::class,

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,
        App\Providers\ValidationServiceProvider::class,

        /*
         * Laravel 5 Console
         */
        Teepluss\Console\ConsoleServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,

        'Form' => 'Collective\Html\FormFacade',
        'Html' => 'Collective\Html\HtmlFacade',
    ],


    // DBDN Config Options

    /*
  |--------------------------------------------------------------------------
  | Smart Downloads template
  |--------------------------------------------------------------------------
  |
  |	Children pages of a parent with this id,
  |	will be recognized in both related blocks as pages with downloads template.
  |
  |	This concerns the visual outlook of the site tree in both related blocks.
  |
 */
    'smart_downloads_id' => 2067,

    /*
	  |--------------------------------------------------------------------------
	  | Access default group id
	  |--------------------------------------------------------------------------
	  |
	  |	Now this is member id
	  |
	 */
    'access_default_group_id' => 1,

    /*
	  |--------------------------------------------------------------------------
	  | File manager reqular expression patterns
	  |--------------------------------------------------------------------------
	  |
	  |
	  |
	 */
    'INIT_SELECTION_PATTERN' => '/^[0-9, ]+$/',
    'PATH_NAME_PATTERN' => '/^[-a-zA-Z0-9_.\/]+$/',
    'FILE_NAME_PATTERN' => '/^[-a-zA-Z0-9_.]+$/',
    'FILTER_PATTERN' => '/^[-a-zA-Z0-9_. ]+$/',

    /*
	|--------------------------------------------------------------------------
	| Access to application from FE or BE
	|--------------------------------------------------------------------------
	|	At the moment we use this to load different validation's messages for FE and BE
	|
	*/
    'isFE' => true,

    /*
	|--------------------------------------------------------------------------
	| Downloads file size limit
	|--------------------------------------------------------------------------
	|	Used to stop attempts to download anything bigger than the specified amount (in bytes).
	|
	*/
    'downloads_limit' => 786432000, // 750MB

    /*
	|--------------------------------------------------------------------------
	| Default thumbnail of brands
	|--------------------------------------------------------------------------
	|	Used to have thumbnail always.
	|
	*/
    'pages' => [
        DAIMLER => (object)array('ID'=> DAIMLER, 'active'=> true, 'brandThumb'=> 'img/template/thumbnails/{type}/Daimler_Thumbnail.png', 'navThumb'=>'/template/nav/daimler.png',"folder" => "all", 'type'=> BRAND, 'slug'=>'Daimler', 'title'=> 'Daimler'),
        MERCEDES_BENZ => (object)array('ID'=> MERCEDES_BENZ, 'active'=> true, 'brandThumb'=> 'img/template/thumbnails/{type}/thumb_mercedes-benz.jpg', 'navThumb'=>'/template/nav/mercedes.png',"folder" =>"MB", 'type'=> BRAND,'slug'=>'Mercedes-Benz', 'title'=> 'Mercedes-Benz'),
        SMART => (object)array('ID'=> SMART, 'active'=> true, 'brandThumb'=> 'img/template/thumbnails/{type}/smart_Thumbnail.png', 'navThumb'=>'/template/nav/smart.png',"folder" => "smart", 'type'=> BRAND,'slug'=>'smart', 'title'=> 'smart'),
        TRUCK_FINANCIAL => (object)array('ID'=> TRUCK_FINANCIAL, 'active'=> true, 'brandThumb'=> 'img/template/thumbnails/{type}/DTF_Thumbnail.png', 'navThumb'=>'/template/nav/dtf.png',"folder" =>"DTF", 'type'=> BRAND, 'slug'=>'Daimler_Truck_Financial', 'title'=> 'Daimler Truck Financial'),
        FLEET_MANAGEMENT => (object)array('ID'=> FLEET_MANAGEMENT, 'active'=> true, 'brandThumb'=> 'img/template/thumbnails/{type}/DFM_Thumbnail.png', 'navThumb'=>'/template/nav/dfm.png',"folder" =>"DFM", 'type'=> BRAND, 'slug'=>'Daimler_Fleet_Management','title'=> 'Daimler Fleet Management'),
        FUSO_FINANCIAL => (object)array('ID'=> FUSO_FINANCIAL, 'active'=> true, 'brandThumb'=> 'img/template/thumbnails/{type}/FUSO_Financial_Thumbnail.png', 'navThumb'=>'/template/nav/fuso.png',"folder" =>"FF", 'type'=> BRAND, 'slug'=>'FUSO_Financial','title'=> 'FUSO Financial'),
        DAIMLER_MOBILITY => (object)array('ID'=> DAIMLER_MOBILITY, 'active'=> true, 'brandThumb'=> 'img/template/thumbnails/{type}/Daimler_Financial_Services_Thumbnail.png', 'navThumb'=>'/template/nav/2_1.jpg',"folder" =>"DMO", 'type'=> COMPANY, 'slug'=>'Daimler_Mobility', 'title'=> 'Daimler Mobility'),
        FINANCIAL_SERVICES => (object)array('ID'=> FINANCIAL_SERVICES, 'active'=> true, 'brandThumb'=> 'img/template/thumbnails/{type}/Daimler_Financial_Services_Thumbnail.png', 'navThumb'=>'/template/nav/2_1.jpg',"folder" =>"DFS", 'type'=> COMPANY, 'slug'=>'Daimler_Financial_Services', 'title'=> 'Daimler Financial Services'),
        DAIMLER_PROTICS => (object)array('ID'=> DAIMLER_PROTICS, 'active'=> true, 'brandThumb'=> 'img/template/thumbnails/{type}/Daimler_Protics_Thumbnail.png', 'navThumb'=>'/template/nav/2_2.jpg',"folder" =>"DP", 'type'=> COMPANY, 'slug'=>'Daimler_Protics'),
        DAIMLER_TSS => (object)array('ID'=> DAIMLER_TSS, 'active'=> true, 'brandThumb'=> 'img/template/thumbnails/{type}/Daimler_TSS_Thumbnail.png', 'navThumb'=>'/template/nav/2_3.jpg',"folder" =>"DTSS", 'type'=> COMPANY, 'slug'=>'Daimler_TSS'),
        DAIMLER_BKK => (object)array('ID'=> DAIMLER_BKK, 'active'=> true, 'brandThumb'=> 'img/template/thumbnails/{type}/Daimler_BKK_Thumbnail.png', 'navThumb'=>'/template/nav/2_4.jpg',"folder" =>"DBKK", 'type'=> COMPANY,'slug'=>'Daimler_BKK','title' => 'Daimler BKK'),
// New Brands for 2018
        DAIMLER_BUSES => (object)array('ID'=> DAIMLER_BUSES, 'active'=> false, 'brandThumb'=> 'img/template/thumbnails/{type}/Daimler_Buses_Thumbnail.png', 'navThumb'=>'/template/nav/daimler.png',"folder" =>"DaimlerBuses", 'type'=> COMPANY,'slug'=>'Daimler_Buses','title' => 'Daimler Buses'),
        EVOBUS      => (object)array('ID'=> EVOBUS, 'active'=> false, 'brandThumb'=> 'img/template/thumbnails/{type}/EvoBus_Thumbnail.png', 'navThumb'=>'/template/nav/daimler.png',"folder" =>"EvoBus", 'type'=> COMPANY,'slug'=>'EvoBus','title' => 'EvoBus'),
        SETRA       => (object)array('ID'=> SETRA, 'active'=> false, 'brandThumb'=> 'img/template/thumbnails/{type}/Setra_Thumbnail.png', 'navThumb'=>'/template/nav/daimler.png',"folder" =>"Setra", 'type'=> COMPANY,'slug'=>'Setra','title' => 'Setra'),
        OMNIPLUS    => (object)array('ID'=> OMNIPLUS, 'active'=> false, 'brandThumb'=> 'img/template/thumbnails/{type}/OMNIplus_Thumbnail.png', 'navThumb'=>'/template/nav/daimler.png',"folder" =>"OMNIplus", 'type'=> COMPANY,'slug'=>'OMNIplus','title' => 'OMNIplus'),
        BUSSTORE    => (object)array('ID'=> BUSSTORE, 'active'=> false, 'brandThumb'=> 'img/template/thumbnails/{type}/BusStore_Thumbnail.png', 'navThumb'=>'/template/nav/daimler.png',"folder" =>"BusStore", 'type'=> COMPANY,'slug'=>'BusStore','title' => 'BusStore'),
        FUSO        => (object)array('ID'=> FUSO, 'active'=> false, 'brandThumb'=> 'img/template/thumbnails/{type}/FUSO_Thumbnail.png', 'navThumb'=>'/template/nav/daimler.png',"folder" =>"FUSO", 'type'=> BRAND,'slug'=>'FUSO','title' => 'FUSO'),
        DAIMLER_TRUCKS => (object)array('ID'=> DAIMLER_TRUCKS, 'active'=> false, 'brandThumb'=> 'img/template/thumbnails/{type}/Daimler_Trucks_Thumbnail.png', 'navThumb'=>'/template/nav/daimler.png',"folder" =>"DaimlerTrucks", 'type'=> COMPANY,'slug'=>'Daimler_Trucks','title' => 'Daimler Trucks'),
    ],

    /*
	|--------------------------------------------------------------------------
	| DesignManual pages
	|--------------------------------------------------------------------------
	|
	*/
// TODO: New Brands for 2018
    'design_manuals_pages' => [2094, 2097, 2098, 2099, 2100, 2194, 2222, 2490],

    /*
	|--------------------------------------------------------------------------
	| Recycle bin total quota
	|--------------------------------------------------------------------------
	|	Amount of disk space from server disk space in % (0-100).
	|
	*/
    'recycle_quota_total' => 10,

    /*
	|--------------------------------------------------------------------------
	| Recycle bin brand's quotas
	|--------------------------------------------------------------------------
	|	Amount of disk space from recycle bin total quota in % (0-100) for every brand.
	|
	*/
    'recycle_quota_brands' => [
        DAIMLER => 10,
        MERCEDES_BENZ => 10,
        SMART => 10,
        TRUCK_FINANCIAL => 10,
        FLEET_MANAGEMENT => 10,
        FUSO_FINANCIAL => 10,
        DAIMLER_MOBILITY => 10,
        FINANCIAL_SERVICES => 10,
        DAIMLER_PROTICS => 10,
        DAIMLER_TSS => 10,
        DAIMLER_BKK => 10,
// TODO: New Brands for 2018 what sizes?
        DAIMLER_BUSES => 10,
        EVOBUS        => 10,
        SETRA         => 10,
        OMNIPLUS      => 10,
        BUSSTORE      => 10,
        FUSO          => 10,
        DAIMLER_TRUCKS=> 10,
    ],

    /*
	|--------------------------------------------------------------------------
	| Minutes allowed to edit page,download,image
	|--------------------------------------------------------------------------
	|
	*/
    'edit_max_time_seconds' => 30 * 60,

    /*
	|--------------------------------------------------------------------------
	| Minutes before end of edit to show alert for extension of time
	|--------------------------------------------------------------------------
	|
	*/
    'edit_time_extension_alert_seconds' => 5 * 60,

    /*
	|--------------------------------------------------------------------------
	| Alert for completing edit time. Time in seconds.
	|--------------------------------------------------------------------------
	|
	*/
    'edit_compleated_alert_seconds' => 10,

    /*
	|--------------------------------------------------------------------------
	| Token for newsletter and activation expire time. Time in hours.
	|--------------------------------------------------------------------------
	|
	*/
    'expire_token_time' => 24,

    /*
	|--------------------------------------------------------------------------
	| Default Administrator to be added in workflow responsible dropdown
	|--------------------------------------------------------------------------
	|
	*/
    'default_workflow_admin' => 5,
    'default_workflow_admin2' => 6,

    /*
	|--------------------------------------------------------------------------
	|Tag categories
	|--------------------------------------------------------------------------
	|
	*/
//    'category' => array(
//        (object)array('ID'=> 0, 'name'=>'Alle', 'lang'=> LANG_DE),
//        (object)array('ID'=> 1, 'name'=>'Allgemeine Fragen', 'lang'=> LANG_DE),
//        (object)array('ID'=> 2, 'name'=>'Audiovisuelle Medien', 'lang'=> LANG_DE),
//        (object)array('ID'=> 3, 'name'=>'Basiselemente', 'lang'=> LANG_DE),
//        (object)array('ID'=> 4, 'name'=>'Benutzerfunktionen', 'lang'=> LANG_DE),
//        (object)array('ID'=> 5, 'name'=>'Bildschirmmedien', 'lang'=> LANG_DE),
//        (object)array('ID'=> 6, 'name'=>'Elektronische Korrespondenz', 'lang'=> LANG_DE),
//        (object)array('ID'=> 7, 'name'=>'Download-Dateien', 'lang'=> LANG_DE),
//        (object)array('ID'=> 8, 'name'=>'Online Medien', 'lang'=> LANG_DE),
//        (object)array('ID'=> 9, 'name'=>'Dreidimensionale Medien', 'lang'=> LANG_DE),
//        (object)array('ID'=> 10, 'name'=>'Druckmedien', 'lang'=> LANG_DE),
//        (object)array('ID'=> 11, 'name'=>'Spezialanwendungen', 'lang'=> LANG_DE),
//        (object)array('ID'=> 12, 'name'=>'Weitere Themen', 'lang'=> LANG_DE),
//        (object)array('ID'=> 13, 'name'=>'All', 'lang'=> LANG_EN),
//        (object)array('ID'=> 14, 'name'=>'Generall questions', 'lang'=> LANG_EN),
//        (object)array('ID'=> 15, 'name'=>'Audio visual media', 'lang'=> LANG_EN),
//        (object)array('ID'=> 16, 'name'=>'Basic elements', 'lang'=> LANG_EN),
//        (object)array('ID'=> 17, 'name'=>'User functions', 'lang'=> LANG_EN),
//        (object)array('ID'=> 18, 'name'=>'Onscreen media', 'lang'=> LANG_EN),
//        (object)array('ID'=> 19, 'name'=>'Email correspondence', 'lang'=> LANG_EN),
//        (object)array('ID'=> 20, 'name'=>'Download files', 'lang'=> LANG_EN),
//        (object)array('ID'=> 21, 'name'=>'Online Media', 'lang'=> LANG_EN),
//        (object)array('ID'=> 22, 'name'=>'Three-dimensional media', 'lang'=> LANG_EN),
//        (object)array('ID'=> 23, 'name'=>'Printed media', 'lang'=> LANG_EN),
//        (object)array('ID'=> 24, 'name'=>'Special applications', 'lang'=> LANG_EN),
//        (object)array('ID'=> 25, 'name'=>'Miscellaneous', 'lang'=> LANG_EN),
//    ),
//
    /*
	|--------------------------------------------------------------------------
	|Download categories
	|--------------------------------------------------------------------------
	|
	*/
    'page_usage' => [
        1 => 'no_category',
        2 => 'audio_visual_media',
        3 => 'basic_elements',
        4 => 'onscreen_media',
        5 => 'web_media',
        6 => 'three_dimensional_media',
        7 => 'printed_media',
        8 => 'special_applications',
        9 => 'miscellaneous',
    ],

    'page_file_type' => [
        1 => 'no_category',
        2 => 'audio_asset',
        3 => 'image_asset',
        4 => 'brand_label',
        5 => 'brand_mark',
        6 => 'office_templates',
        7 => 'typeface',
        8 => 'corporate_logotype',
        9 => 'video_asset',
        10 => 'print_template',
        11 => 'blank_form',
        12 => 'web',
        13 => 'other_materials',
    ],

    'jupload' => [
        'script_url' => '/upload',
        'upload_dir' => ini_get('upload_tmp_dir') . "/",
        'upload_url' => '/bundles/jupload/uploads/files/',
        'delete_type' => 'POST',
        'image_versions' => [
            'thumbnail' => [
                'upload_dir' => '/bundles/jupload/uploads/thumbnails/',
                'upload_url' => '/bundles/jupload/uploads/thumbnails/',
            ],
        ],

    ],

    'parameters' => [
        'file_format' => [
            1 => '4:3',
            2 => '16:9',
            3 => '21:9',
            4 => 'DIN A0',
            5 => 'DIN A1',
            6 => 'DIN A2',
            7 => 'DIN A3',
            8 => 'DIN A4',
            9 => 'DIN A5',
            10 => 'DIN A6',
            11 => 'DIN lang (DL) ',
            12 => 'DIN B5',
            13 => 'DIN B4',
            14 => 'DIN B3',
            15 => 'DIN C6',
            16 => 'DIN C5',
            17 => 'DIN C4',
            18 => 'US Letter',
            19 => 'US Legal',
            20 => 'US Bi-fold',
            21 => 'US Tri-fold',
            22 => 'HDTV 1080p',
            23 => 'HDTV 720p',
        ],
        'file_dimension' => [
            1 => 'mm',
            2 => 'px',
            3 => 'in',
            4 => 'm',
            5 => 'cm',
            6 => 'pt',
        ],
        'file_resolution' => [
            1 => 'dpi',
            2 => 'lpi',
            3 => 'ppi',
        ],
        'file_color_mode' => [
            1 => 'bitmap',
            2 => 'grayscale',
            3 => 'RGB',
            4 => 'CMYK',
            5 => 'lab',
            6 => 'monochrome',
        ],
        'faq_category' => [
            1 => 'all',
            2 => 'generall_questions',
            3 => 'audio_visual_media',
            4 => 'basic_elements',
            5 => 'user_functions',
            6 => 'onscreen_media',
            7 => 'email_correspondence',
            8 => 'download_files',
            9 => 'online_media',
            10 => 'three_dimensional',
            11 => 'printed_media',
            12 => 'special_applications',
            13 => 'miscellaneous',
        ]
    ],

    'PDF_path' => [
        'uploaded' => storage_path('pdf/uploaded/'),
        'generated' => storage_path('pdf/generated/')
    ],
    
    // Allowed file extensions for upload !!! always use lowercase !!!
    'allow_file_ext' => ['ai',
                         'bmp',
                         'docx',
                         'dotm',
                         'dotx',
                         'emf',
                         'eot',
                         'eps',
                         'gif',
                         'idml',
                         'indd',
                         'indt',
                         'jpg',
                         'mov',
                         'mp3',
                         'mp4',
                         'otf',
                         'pdf',
                         'png',
                         'potx',
                         'pptx',
                         'ps',
                         'psd',
                         'svg',
                         'tif',
                         'ttf',
                         'wav',
                         'wmf',
                         'wmv',
                         'woff',
                         'woff2',
                         'xlam',
                         'xltx',
                         'zip',
                         'ds_store',
                         'db'],
    'allow_img_ext' =>  ['gif',
                         'jpg',
                         'png',
                         'svg'],
    'allow_file_content' => [
                          'ai' => ['application/pdf'],
                          'bmp' => ['image/x-ms-bmp'],
                          'docx' => ['application/vnd.openxmlformats-officedocument.wordprocessingml.document'],
                          'dotm' => ['application/vnd.openxmlformats-officedocument.wordprocessingml.document'],
                          'dotx' => ['application/vnd.openxmlformats-officedocument.wordprocessingml.document'],
                          'emf' => ['application/octet-stream'],
                          'eot' => ['application/vnd.ms-fontobject'],
                          'eps' => ['application/octet-stream','application/postscript'],
                          'gif' => ['image/gif'],
                          'idml' => ['application/zip'],
                          'indd' => ['application/octet-stream'],
                          'indt' => ['application/octet-stream'],
                          'jpg' => ['image/jpeg'],
                          'mov' => ['video/quicktime'],
                          'mp3' => ['audio/mpeg'],
                          'mp4' => ['video/mp4','video/x-m4v'],
                          'otf' => ['application/vnd.ms-opentype'],
                          'pdf' => ['application/pdf'],
                          'png' => ['image/png'],
                          'potx' => ['application/octet-stream','application/vnd.openxmlformats-officedocument.presentationml.presentation'],
                          'pptx' => ['application/vnd.openxmlformats-officedocument.presentationml.presentation'],
                          'ps' => ['application/postscript'],
                          'psd' => ['image/vnd.adobe.photoshop'],
                          'svg' => ['image/svg+xml'],
                          'tif' => ['image/tiff'],
                          'ttf' => ['application/x-font-ttf'],
                          'wav' => ['audio/x-wav'],
                          'wmf' => ['application/octet-stream'],
                          'wmv' => ['video/x-ms-asf'],
                          'woff' => ['application/octet-stream'],
                          'woff2' => ['application/octet-stream'],
                          'xlam' => ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
                          'xltx' => ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
                          'zip' => ['application/zip'],
                          'ds_store' => ['application/octet-stream'],
                          'db' => ['application/octet-stream'],
                          ],

    /*
	  |--------------------------------------------------------------------------
	  | User for Artisan Commands
	  |--------------------------------------------------------------------------
	  |
	  | Some operations need user.
	  | For example: ActivateFuturePublishPages
	  |
	 */
    'commandUserId' => 6
];

# DBDN L5.5 

**Build on [laravel/framework](http://github.com/laravel/framework).**

### DBDN Unit tests

#### Run DBDN Unit tests

> ./vendor/phpunit/phpunit/phpunit

#### For more details about tests use

> ./vendor/phpunit/phpunit/phpunit --debug -v

#### For specific file with tests use

> ./vendor/phpunit/phpunit/phpunit ./tests/Feature/BEViewAccessNewsletterTest.php

#### For specific test in file with tests use

> ./vendor/phpunit/phpunit/phpunit ./tests/Feature/FEAccessProtectedFilesTest.php --filter testRouteDownloadFileNotAuthorizedMember


### Thumbnails of Brand

#### For new brand thumbs put them in folder: with names from app.php pages->brandThumb
Generate cache assets with: ContentUpdater::generateTumbnailsForDownloads(false);

#### For update of brand thumbs overwite them in folder: with names from app.php pages->brandThumb
Generate cache assets with: ContentUpdater::generateTumbnailsForDownloads(true);
